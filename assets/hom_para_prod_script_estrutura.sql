CREATE TABLE sistema_historico_sincronizador (
	id int(11)  NOT NULL ,
	sistema_tabela_id_INT int(11) ,
	id_tabela_INT int(11) ,
	sistema_tipo_operacao_id_INT int(11) ,
	data_DATETIME datetime,
	corporacao_id_INT int(11)  NOT NULL ,
	usuario_INT int(11) ,
	CONSTRAINT pk_sistema_histori6500 PRIMARY KEY (id),
	FOREIGN KEY (sistema_tabela_id_INT) REFERENCES sistema_tabela (id) ON DELETE SET NULL ON UPDATE SET NULL,
	FOREIGN KEY (sistema_tipo_operacao_id_INT) REFERENCES sistema_tipo_operacao_banco (id) ON DELETE SET NULL ON UPDATE SET NULL,
	FOREIGN KEY (corporacao_id_INT) REFERENCES corporacao (id) ON DELETE CASCADE ON UPDATE CASCADE
);

                    CREATE TABLE veiculo_registro (
	id int(11)  NOT NULL ,
	entrada_BOOLEAN int(1) ,
	veiculo_usuario_id_INT int(11) ,
	data_DATETIME datetime,
	latitude_INT int(6) ,
	longitude_INT int(6) ,
	CONSTRAINT pk_veiculo_registr882141 PRIMARY KEY (id)
);

                    