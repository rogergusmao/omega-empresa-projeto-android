package app.omegasoftware.pontoeletronico.TabletActivities;


import android.os.Bundle;
import android.widget.Button;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.listener.BrowserOpenerListener;
import app.omegasoftware.pontoeletronico.listener.CriptBrowserOpenerListener;
import app.omegasoftware.pontoeletronico.listener.EmailClickListener;
import app.omegasoftware.pontoeletronico.listener.PhoneClickListener;

public class AboutActivityMobile extends OmegaRegularActivity {
	public static String TAG = "AboutActivityMobile";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		//Log.d(TAG, "onCreate(): Activity created");
		setContentView(R.layout.about_activity_mobile_layout);
		
		new CustomDataLoader(this).execute();
		
	}
	
	
	@Override
	public boolean loadData() {
		//
		
		return true;
	}

	@Override
	public void initializeComponents() {
		

		Button omegaPhoneNumberButton = (Button) findViewById(R.id.omega_phone_button);
		omegaPhoneNumberButton.setText(R.string.omega_phone_number);
		omegaPhoneNumberButton.setOnClickListener(new PhoneClickListener(R.id.omega_phone_button));
		
		Button vendasWebMailButton = (Button) findViewById(R.id.omega_email_button);
		vendasWebMailButton.setText(R.string.omega_email_button);
		vendasWebMailButton.setOnClickListener(new EmailClickListener(R.id.omega_email_button));
				
		
		Button atDomiciliarBrowser = (Button) findViewById(R.id.website_omega_button);		
		atDomiciliarBrowser.setOnClickListener(new BrowserOpenerListener(getResources().getString(R.string.omega_site_url)));
		
		Button vSisteSistemaAndroidBrowser = (Button) findViewById(R.id.website_sistema_android_button);		
//		vSisteSistemaAndroidBrowser.setOnClickListener(new BrowserOpenerListener(getResources().getString(R.string.sistema_android_site_url)));
		vSisteSistemaAndroidBrowser.setOnClickListener(
				new CriptBrowserOpenerListener(
						AboutActivityMobile.this,
						OmegaConfiguration.BASE_URL_OMEGA_SOFTWARE() + "client_area/actions.php?class=Servicos_web&action=actionlogaNoSistemaDaCorporacaoCript",
						new String[] {"email", "corporacao", "senha"},
						//new String[] {email, corporacao, senha},
						new String[] {OmegaSecurity.getEmail(), OmegaSecurity.getCorporacao(), OmegaSecurity.getSenha()},
						getResources().getString(R.string.sistema_android_site_url)
						));
		
		Button btFinanceiro = (Button) findViewById(R.id.website_cobranca_button);		
		//btFinanceiro.setOnClickListener(new BrowserOpenerListener(getResources().getString(R.string.website_sistema_cobranca)));
		
		btFinanceiro.setOnClickListener(
				new CriptBrowserOpenerListener(
						AboutActivityMobile.this,
						OmegaConfiguration.BASE_URL_OMEGA_SOFTWARE() + "client_area/actions.php?class=Servicos_web&action=actionPacotes",
						new String[] {"email", "corporacao", "senha"},
						//new String[] {email, corporacao, senha},
						new String[] {OmegaSecurity.getEmail(), OmegaSecurity.getCorporacao(), OmegaSecurity.getSenha()},
						getResources().getString(R.string.sistema_android_site_url)
						));
		
		
	}
	
}
