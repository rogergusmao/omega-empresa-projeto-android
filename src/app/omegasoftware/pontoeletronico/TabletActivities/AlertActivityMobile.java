package app.omegasoftware.pontoeletronico.TabletActivities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;


public class AlertActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "AlertActivityMobile";
	
	String message;
	//Search button
	private Button okButton;
	
	
	private TextView messageTextView; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.alert_activity_mobile_layout);


		Bundle vParameter = getIntent().getExtras();
		if(vParameter != null)
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_MESSAGE)){
				message = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_MESSAGE);
			}
		new CustomDataLoader(this).execute();

	}

	@Override
	public boolean loadData() {

		return true;

	}



	@Override
	public void initializeComponents() {
		
		this.messageTextView = (TextView) findViewById(R.id.message_textview);
		if(message != null){
			if(message.length()  > 0 ){
				messageTextView.setText(message);
				
			}
		}
			
		//Attach search button to its listener
		this.okButton = (Button) findViewById(R.id.ok_button);
		this.okButton.setOnClickListener(new OkButtonListener());
		
		
	}	

	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onOkButtonClicked()
	{
		Intent intent = getIntent();
		
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO, true);
		setResult(RESULT_OK, intent);
		finish();
		
	}

	private class OkButtonListener implements OnClickListener
	{
		public void onClick(View v) {
			onOkButtonClicked();
		}
	}	
	
	private void onCancelarButtonClicked()
	{
		Intent intent = getIntent();
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO, false);
		setResult(RESULT_OK, intent);
		finish();
		
	}

	protected class CancelarButtonListener implements OnClickListener
	{
		public void onClick(View v) {
			onCancelarButtonClicked();
		}
	}
}


