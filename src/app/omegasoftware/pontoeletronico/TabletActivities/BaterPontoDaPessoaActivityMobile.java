package app.omegasoftware.pontoeletronico.TabletActivities;

import java.util.Timer;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListPessoaEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectEmpresaPontoEletronicoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.PessoaPontoAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoPonto;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSServiceLocationListener;
import app.omegasoftware.pontoeletronico.gpsnovo.ContainerLocalizacao;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSControler;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class BaterPontoDaPessoaActivityMobile extends OmegaRegularActivity {

	// Previous operator: '+', '-', '*', '/', '=' or ' ' (no operator)
	Database db=null;
	EXTDAOPessoa objPessoa;
	EXTDAOProfissao objProfissao;
	// Search mode toggle buttons
	// private CheckBox entradaCk;
	// private CheckBox saidaCk;

	// private Button microphoneButton;

	boolean isToTakePicture = false;
	String idEmpresa;
	String idPessoaEmpresa;
	String empresa;
	String pessoa;
	String idProfissao;
	String idPessoa;
	String profissao;

	String ultimoIdPonto;
	String dataDoUltimoPonto;
	String horaDoUltimoPonto;
	String ultimoArquivoDeFoto;
	String ultimaDescricaoPonto;
	View itemListPontoPessoa;
	String PREFIXO_NAO_SINCRONIZADO;
	Timer uiUpdaterTimer;

	public TIPO tipo;

	public static final int FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA = 988;
	public static final int FORM_ERROR_DIALOG_GPS_FORA_DE_AREA = 989;
	// Constants
	public static final String TAG = "BaterPontoDaPessoaActivityMobile";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.bater_ponto_da_pessoa_activity_mobile);
		PREFIXO_NAO_SINCRONIZADO = this.getString(R.string.prefixoNaoSincronizado);

		Bundle vParameter = getIntent().getExtras();

		idPessoaEmpresa = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA_EMPRESA);

		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {

		return true;
	}

	public void refreshPessoaPonto() {

		if (idPessoaEmpresa != null) {
			EXTDAOPonto objPonto = new EXTDAOPonto(db);
			EXTDAOPonto.PontoPessoa pontoPessoa = objPonto.getUltimoPontoDaPessoaNaEmpresa(idPessoaEmpresa, idPessoa,
					idEmpresa, idProfissao);
			if (pontoPessoa == null)
				pontoPessoa = objPonto.factoryPontoPessoa(idPessoaEmpresa);

			pontoPessoa.pessoa = pessoa;
			pontoPessoa.empresa = empresa;
			pontoPessoa.profissao = profissao;
			pontoPessoa.idPessoa = idPessoa;
			pontoPessoa.idEmpresa = idEmpresa;
			pontoPessoa.idProfissao = idProfissao;
			pontoPessoa.idPessoaEmpresa = idPessoaEmpresa;

			PessoaPontoAdapter adapter = new PessoaPontoAdapter(BaterPontoDaPessoaActivityMobile.this, pontoPessoa);

			adapter.formatarView(itemListPontoPessoa);
			itemListPontoPessoa.setVisibility(View.VISIBLE);

		} else {

			itemListPontoPessoa.setVisibility(View.GONE);
		}

	}

	public void refreshHeaderSecundario() {
		View headerSecundario = findViewById(R.id.header);
		if (headerSecundario != null) {
			View viewTitle = headerSecundario.findViewById(R.id.tv_title);
			if (viewTitle != null) {
				TextView tvTitle = (TextView) viewTitle;

				tvTitle.setText(pessoa);
			}
		}
	}

	@Override
	public void initializeComponents() {
		try {

			CustomServiceListener listener = new CustomServiceListener();

			db = new DatabasePontoEletronico(this);
			objPessoa = new EXTDAOPessoa(db);
			objProfissao = new EXTDAOProfissao(db);

			

			Database db = new DatabasePontoEletronico(this);
			EXTDAOPessoaEmpresa objPessoaEmpresa = new EXTDAOPessoaEmpresa(db);
			objPessoaEmpresa.select(idPessoaEmpresa);
			objProfissao = objPessoaEmpresa.getObjProfissao();
			objPessoa = objPessoaEmpresa.getObjPessoa();
			profissao = objProfissao.getStrValueOfAttribute(EXTDAOProfissao.NOME);
			idProfissao = objProfissao.getId();
			idPessoa = objPessoa.getId();
			idEmpresa = objPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.EMPRESA_ID_INT);

			pessoa = objPessoa.getStrValueOfAttribute(EXTDAOPessoa.NOME);
			if (pessoa == null || pessoa.length() == 0)
				pessoa = objPessoa.getStrValueOfAttribute(EXTDAOPessoa.EMAIL);

			EXTDAOEmpresa vObjEmpresa = new EXTDAOEmpresa(db);
			vObjEmpresa.setAttrValue(EXTDAOEmpresa.ID, idEmpresa);
			if (vObjEmpresa.select()) {
				empresa = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.NOME);
			}
			
			itemListPontoPessoa = findViewById(R.id.item_list_ponto_pessoa);

			String strBomDia = getApplicationContext().getString(R.string.bom_dia_ponto) + " " + empresa;
			((TextView) findViewById(R.id.tv_bom_dia)).setText(strBomDia);
			

			refreshPessoaPonto();

			
			((Button) findViewById(R.id.bt_bater_ponto_entrada)).setOnClickListener(listener);
			((Button) findViewById(R.id.bt_bater_ponto_saida)).setOnClickListener(listener);
			((Button) findViewById(R.id.btnTrocarEmpresa)).setOnClickListener(listener);
			refreshHeaderSecundario();
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);

		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		try {
			switch (id) {
			case FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA:
				vTextView.setText(getResources().getString(R.string.ponto_pessoa_inexistente_na_empresa));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA);

					}
				});
				break;
			case FORM_ERROR_DIALOG_GPS_FORA_DE_AREA:
				vTextView.setText(getResources().getString(R.string.gps_fora_de_area));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_GPS_FORA_DE_AREA);

					}
				});
				break;

			default:
				return super.onCreateDialog(id);
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
			return null;
		}
		return vDialog;

	}

	private String getTextoDescritivo(final boolean entrada) {
		try {

			String vEntradaOuSaida = "";
			if (entrada)
				vEntradaOuSaida = getResources().getString(R.string.ponto_entrada);
			else
				vEntradaOuSaida = getResources().getString(R.string.ponto_saida);

			return vEntradaOuSaida.toUpperCase() + " - " + pessoa.toUpperCase() + " - " + empresa.toUpperCase();

		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);

		}
		return null;

	}

	private void baterPonto(final View view, final boolean entrada) {
		String vTextConfirmacao = getTextoDescritivo(entrada);
		ultimaDescricaoPonto = vTextConfirmacao;
		if (!GPSServiceLocationListener.isActiveGPS())
			showDialog(FORM_ERROR_DIALOG_GPS_FORA_DE_AREA);
		else if (vTextConfirmacao == null || vTextConfirmacao.length() == 0)
			showDialog(FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA);
		else {
			Intent intent = new Intent(this, ConfirmarPontoTimerActivityMobile.class);
			intent.putExtra(OmegaConfiguration.PARAM_ENTRADA, entrada);
			intent.putExtra(OmegaConfiguration.PARAM_DESCRICAO_PONTO, vTextConfirmacao);

			startActivityForResult(intent, OmegaConfiguration.ACTIVITY_CONFIRMAR_PONTO);
		}
	}

	private class CustomServiceListener implements OnClickListener {

		// On-click event handler for all the buttons

		public void onClick(final View view) {
			try {

				switch (view.getId()) {

				// Operator buttons: '+', '-', '*', '/' and '='
				case R.id.bt_temporizador:
				case R.id.bt_bater_ponto_entrada:

					baterPonto(view, true);

					break;
				case R.id.bt_bater_ponto_saida:

					baterPonto(view, false);

					break;
				case R.id.btnBuscarId:
					Intent intent = new Intent(BaterPontoDaPessoaActivityMobile.this,
							ListPessoaEmpresaActivityMobile.class);

					intent.putExtra(OmegaConfiguration.PARAM_SELECIONAR_ID, true);

					startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FILTER_PESSOA_EMPRESA);

					break;
				case R.id.btnTrocarEmpresa:

					
					Intent intent2 = new Intent(getApplicationContext(), SelectEmpresaPontoEletronicoActivityMobile.class);
					intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, idEmpresa);
					startActivityForResult(intent2, OmegaConfiguration.STATE_SELECT_EMPRESA);
					break;

				}
			} catch (Exception ex) {
				SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);

			}
		}

	}

	private void finalizaActivity() {

		Intent intent = getIntent();

		if (ultimoIdPonto != null && ultimoIdPonto.length() > 0) {
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, ultimoIdPonto);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, true);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, idPessoa);
			setResult(RESULT_OK, intent);
			finish();
		} else {
			setResult(RESULT_CANCELED, intent);
			finish();
		}
	}

	private void salvarNoBancoDeDados(final boolean entrada) throws Exception {

		EXTDAOPonto objPonto = new EXTDAOPonto(db);
		objPonto.setAttrValue(EXTDAOPonto.PESSOA_ID_INT, idPessoa);
		EXTDAOTipoPonto.TIPO_PONTO tipoPonto = EXTDAOTipoPonto.TIPO_PONTO.TURNO;

		objPonto.setAttrValue(EXTDAOPonto.FOTO, ultimoArquivoDeFoto);

		HelperDate helperDate = new HelperDate();
		dataDoUltimoPonto = helperDate.getDateDisplay();
		horaDoUltimoPonto = helperDate.getTimeDisplay();

		long utc = HelperDate.getTimestampSegundosUTC(this);
		int offset = HelperDate.getOffsetSegundosTimeZone();
		objPonto.setAttrValue(EXTDAOPonto.DATA_SEC, String.valueOf(utc));
		objPonto.setAttrValue(EXTDAOPonto.DATA_OFFSEC, String.valueOf(offset));
		objPonto.setAttrValue(EXTDAOPonto.EMPRESA_ID_INT, idEmpresa);
		String idUsuario = OmegaSecurity.getIdUsuario();
		objPonto.setAttrValue(EXTDAOPonto.USUARIO_ID_INT, idUsuario);
		objPonto.setAttrValue(EXTDAOPonto.IS_ENTRADA_BOOLEAN, HelperString.valueOfBooleanSQL(entrada));

		String strIdTipoPonto = String.valueOf(tipoPonto.getId());
		objPonto.setAttrValue(EXTDAOPonto.TIPO_PONTO_ID_INT, strIdTipoPonto);
		int latitude = 0;
		int longitude = 0;
		ContainerLocalizacao vContainerLocalizacao = GPSControler.getLocalizacao();
		if (vContainerLocalizacao != null) {
			latitude = vContainerLocalizacao.getLatitude();
			longitude = vContainerLocalizacao.getLongitude();

			objPonto.setAttrValue(EXTDAOPonto.LATITUDE_INT, String.valueOf(latitude));
			objPonto.setAttrValue(EXTDAOPonto.LONGITUDE_INT, String.valueOf(longitude));

		}

		objPonto.setAttrValue(EXTDAOPonto.PROFISSAO_ID_INT, idProfissao);

		objPonto.formatToSQLite();

		ultimoIdPonto = String.valueOf(objPonto.insert(true));

		if (ultimoIdPonto == null)
			throw new Exception("Falha ao inserir o registro de ponto");
		
		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SERVICE_PAINEL_USUARIO.toString());
		sendBroadcast(updateUIIntent);
		
		EXTDAOPonto.factoryToastPontoBatido(getApplicationContext());
		finalizaActivity();

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		try {
			switch (requestCode) {

			case OmegaConfiguration.ACTIVITY_CONFIRMAR_PONTO:
				Bundle pParameter = intent.getExtras();
				if (!pParameter.getBoolean(OmegaConfiguration.PARAM_CANCELAR, false)) {
					Boolean entrada = pParameter.getBoolean(OmegaConfiguration.PARAM_ENTRADA);

					salvarNoBancoDeDados(entrada);
					
				}
				break;

			case OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW:
			case OmegaConfiguration.STATE_FORM_MARCA_PONTO:
				if (resultCode == RESULT_OK) {
					ultimoIdPonto = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID);
				}

				break;
			case OmegaConfiguration.STATE_SELECT_EMPRESA:
				if(resultCode == RESULT_OK){
					String idNovaEmpresa = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA);
					Long idPessoaEmpresaNovaEmpresa =EXTDAOPessoaEmpresa.getPessoaEmpresaParaBaterPontoDaPessoa(
							db , 
							idPessoa, 
							idNovaEmpresa, 
							idProfissao);
					if(idPessoaEmpresaNovaEmpresa == null)
						throw new Exception("Ocorreu uma falha durante o selecao/cadastramento automatico da pessoa_empresa");
					idPessoaEmpresa =  idPessoaEmpresaNovaEmpresa.toString();
					getIntent().removeExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA_EMPRESA);
					getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA_EMPRESA, idPessoaEmpresa);
					new CustomDataLoader(this).execute();					
				}
				break;
			default:
				break;
			}
		} catch (Exception ex) {
			SingletonLog.openDialogError(this, ex, TIPO.PAGINA);
		}

	}

}
