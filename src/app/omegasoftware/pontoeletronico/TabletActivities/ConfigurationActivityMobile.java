package app.omegasoftware.pontoeletronico.TabletActivities;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.barcode.BarcodeReaderTestActivity;
import app.omegasoftware.pontoeletronico.cam.CameraPreview;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;

public class ConfigurationActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "ConfigurationActivityMobile";
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		try{
			setContentView(R.layout.configuration_activity_mobile_layout);	
			
		} catch(Exception ex){

		}

		new CustomDataLoader(this).execute();

	}


	@Override
	public boolean loadData() {
		
			return true;	
		


	}

//	@Override
//	public void initializeComponents() {
//
//		Intent intent = new Intent("com.google.zxing.client.android.SCAN");
//		intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
//		startActivityForResult(intent, 0);
//	}

	@Override
	public void initializeComponents() {
		
		//Now we to connect each button in this screen to its onClickListener
		Button vConfigurationCamButton = (Button) findViewById(R.id.button_configuration_cam);
		Button vConfigurationQRReaderButton = (Button) findViewById(R.id.button_configuration_qr_reader);
		Button vConfigurationCamTakePictureButton = (Button) findViewById(R.id.button_configuration_cam_take_picture);
		
		//Activity listener
		CustomServiceListener listener = new CustomServiceListener();
		
		vConfigurationCamButton.setOnClickListener(listener);
		vConfigurationQRReaderButton.setOnClickListener(listener);
		vConfigurationCamTakePictureButton.setOnClickListener(listener);
		
						
	}
	
	private class CustomServiceListener implements OnClickListener
	{

		Intent intent = null;
		
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.button_configuration_cam:
				intent = new Intent(getApplicationContext(), CameraPreview.class);
				break;
			case R.id.button_configuration_qr_reader:
				intent = new Intent(getApplicationContext(), BarcodeReaderTestActivity.class);
				break;
			case R.id.button_configuration_cam_take_picture:

				
//				intent = new Intent(getApplicationContext(), FuncionarioTakePictureActivityMobile.class);
				break;
			default:
				break;
			}
			
			if(intent != null)
			{
				startActivity(intent);	
			}
			
		}
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		   if (requestCode == 0) {
		      if (resultCode == RESULT_OK) {
//		         String contents = intent.getStringExtra("SCAN_RESULT");
//		         String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
		         // Handle successful scan
		         //Log.d("onActivityResult", "RESULTADO OK");
		         
		      } else if (resultCode == RESULT_CANCELED) {
		         // Handle cancel
		    	  //Log.d("onActivityResult", "RESULTADO CANCELED");
		      }
		   }
		}
	
	public void executeCustomDataLoader(){
		try{
			new CustomDataLoader(this).execute();
		}
		catch(Exception e)
		{
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
	}
	

}
