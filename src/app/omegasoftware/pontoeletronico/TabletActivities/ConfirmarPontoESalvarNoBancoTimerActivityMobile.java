package app.omegasoftware.pontoeletronico.TabletActivities;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.widget.UltimoPontoWidget.UpdateService;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemConfiguracao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaAuthenticatorActivity.ContainerAuthentication;
import app.omegasoftware.pontoeletronico.common.controler.ControlerProgressDialog;
import app.omegasoftware.pontoeletronico.common.controler.LoaderAsyncTask;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoPonto;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.gpsnovo.ContainerLocalizacao;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSControler;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class ConfirmarPontoESalvarNoBancoTimerActivityMobile extends OmegaRegularActivity {

	// Constants
	public static final String TAG = "ConfirmarPontoESalvarNoBancoTimerActivityMobile";
	private Timer timerContagem = new Timer();
	HandlerContagem handlerContagem;
	TextView tvContagem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.confirmar_ponto_activity_mobile_layout);

		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {

		return true;
	}

	class HandlerContagem extends Handler {
		boolean started = false;
		int contSegundos;
		boolean executando = false;

		public HandlerContagem(int contSegundos) {

			this.contSegundos = contSegundos;
			started = false;
		}

		@Override
		public void handleMessage(Message msg) {
			if (!executando) {
				executando = true;
				try {
					if (!started) {
						started = true;
					}
					if (this.contSegundos > 0) {
						tvContagem.setText(String.valueOf(this.contSegundos));
						this.contSegundos--;
					} else {
						tvContagem.setText("X");

						timerContagem.cancel();
						started = false;
						sairConfirmando();
					}

				} catch (Exception ex) {
					SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
				} finally {
					executando = false;
				}
			}
		}
	}

	@Override
	public void initializeComponents() {

		String desc = getIntent().getStringExtra(OmegaConfiguration.PARAM_DESCRICAO_PONTO);
		View v = findViewById(R.id.tv_descricao_ponto);
		TextView tv = (TextView) v;
		tv.setText(desc);

		boolean isEntrada = getIntent().getBooleanExtra(OmegaConfiguration.PARAM_ENTRADA, false);
		if (!isEntrada) {
			tv.setTextColor(getResources().getColor(R.color.red));
		} else {
			tv.setTextColor(getResources().getColor(R.color.green));
		}
		((Button) findViewById(R.id.bt_confirmar)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				sairConfirmando();

			}
		});

		((Button) findViewById(R.id.bt_cancelar)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				sairCancelando();

			}
		});

		tvContagem = (TextView) findViewById(R.id.tv_contagem);
		handlerContagem = new HandlerContagem(1);

		setTimerContagem();
		return;
	}

	@Override
	public void onBackPressed() {
		sairConfirmando();
	}

	private void sairCancelando() {
		Intent intent = getIntent();
		intent.putExtra(OmegaConfiguration.PARAM_CANCELAR, true);
		setResult(RESULT_OK, intent);
		finish();
	}

	private void sairConfirmando() {
		new SalvarNoBancoTask().execute();
	}

	@Override
	public void finish() {

		super.finish();
	}

	private void setTimerContagem() {

		// Starts ui updater timer
		this.timerContagem.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				handlerContagem.sendEmptyMessage(0);

			}
		}, 1000, 1000);
	}

	public class SalvarNoBancoTask extends AsyncTask<String, Integer, Boolean> {

		Exception __ex2 = null;
		Intent intent = null;
		
		public SalvarNoBancoTask() {
			
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}		

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				salvarNoBancoDeDados();

			} catch (Exception e) {
				__ex2 = e;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {

				if (this.__ex2 != null) {
					SingletonLog.factoryToast(ConfirmarPontoESalvarNoBancoTimerActivityMobile.this, __ex2,
							SingletonLog.TIPO.UTIL_ANDROID);
					setResult(RESULT_CANCELED, intent);
				} else {
					Intent intent = getIntent();
					setResult(RESULT_OK, intent);

					EXTDAOPonto.factoryToastPontoBatido(ConfirmarPontoESalvarNoBancoTimerActivityMobile.this);
					
					Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SERVICE_PAINEL_USUARIO.toString());
					sendBroadcast(updateUIIntent);
					
					ConfirmarPontoESalvarNoBancoTimerActivityMobile.this.startService(new Intent(ConfirmarPontoESalvarNoBancoTimerActivityMobile.this, UpdateService.class));
				}

			} catch (Exception ex) {
				SingletonLog.openDialogError(ConfirmarPontoESalvarNoBancoTimerActivityMobile.this, ex,
						SingletonLog.TIPO.PAGINA);
			} finally {
				
				ConfirmarPontoESalvarNoBancoTimerActivityMobile.this.finish();
			}

		}

	}

	private void salvarNoBancoDeDados() throws Exception {
		boolean entrada = getIntent().getBooleanExtra(OmegaConfiguration.PARAM_IS_ENTRADA, false);

		String idEmpresa = getIntent().getStringExtra(OmegaConfiguration.PARAM_ID_EMPRESA);
		String idPessoa = getIntent().getStringExtra(OmegaConfiguration.PARAM_ID_PESSOA);
		String idProfissao = getIntent().getStringExtra(OmegaConfiguration.PARAM_ID_PROFISSAO);
		String idCorporacao = getIntent().getStringExtra(OmegaConfiguration.PARAM_ID_CORPORACAO);

		Database db = new DatabasePontoEletronico(this);
		EXTDAOPonto objPonto = new EXTDAOPonto(db);
		objPonto.setAttrValue(EXTDAOPonto.PESSOA_ID_INT, idPessoa);
		EXTDAOTipoPonto.TIPO_PONTO tipoPonto = EXTDAOTipoPonto.TIPO_PONTO.TURNO;
		long utc = HelperDate.getTimestampSegundosUTC(this);
		int offset = HelperDate.getOffsetSegundosTimeZone();
		objPonto.setAttrValue(EXTDAOPonto.DATA_SEC, String.valueOf(utc));
		objPonto.setAttrValue(EXTDAOPonto.DATA_OFFSEC, String.valueOf(offset));
		objPonto.setAttrValue(EXTDAOPonto.EMPRESA_ID_INT, idEmpresa);
		String idUsuario = OmegaSecurity.getIdUsuario();
		objPonto.setAttrValue(EXTDAOPonto.USUARIO_ID_INT, idUsuario);
		objPonto.setAttrValue(EXTDAOPonto.IS_ENTRADA_BOOLEAN, HelperString.valueOfBooleanSQL(entrada));

		String strIdTipoPonto = String.valueOf(tipoPonto.getId());
		objPonto.setAttrValue(EXTDAOPonto.TIPO_PONTO_ID_INT, strIdTipoPonto);
		int latitude = 0;
		int longitude = 0;
		ContainerLocalizacao vContainerLocalizacao = GPSControler.getLocalizacao();
		if (vContainerLocalizacao != null) {
			latitude = vContainerLocalizacao.getLatitude();
			longitude = vContainerLocalizacao.getLongitude();

			objPonto.setAttrValue(EXTDAOPonto.LATITUDE_INT, String.valueOf(latitude));
			objPonto.setAttrValue(EXTDAOPonto.LONGITUDE_INT, String.valueOf(longitude));
		}

		objPonto.setAttrValue(EXTDAOPonto.PROFISSAO_ID_INT, idProfissao);
		objPonto.setAttrValue(EXTDAOPonto.CORPORACAO_ID_INT, idCorporacao);

		objPonto.formatToSQLite();

		String ultimoIdPonto = String.valueOf(objPonto.insert(true));

		if (ultimoIdPonto == null) {
			throw new Exception("Falha ao inserir o registro de ponto");
			
		}

	}

}
