package app.omegasoftware.pontoeletronico.TabletActivities;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;

public class ConfirmarPontoTimerActivityMobile extends OmegaRegularActivity {

	// Constants
	public static final String TAG = "ConfirmarPontoTimerActivityMobile";
	private Timer timerContagem = new Timer();
	HandlerContagem handlerContagem;
	TextView tvContagem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.confirmar_ponto_activity_mobile_layout);

		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {

		return true;
	}

	class HandlerContagem extends Handler {
		boolean started = false;
		int contSegundos;
		boolean executando = false;

		public HandlerContagem(int contSegundos) {

			this.contSegundos = contSegundos;
			started = false;
		}

		@Override
		public void handleMessage(Message msg) {
			if (!executando) {
				executando = true;
				try {
					if (!started) {
						started = true;
					}
					if (this.contSegundos > 0) {
						tvContagem.setText(String.valueOf(this.contSegundos));
						this.contSegundos--;
					} else {
						tvContagem.setText("X");

						timerContagem.cancel();
						started = false;
						sairConfirmando();
					}

				} catch (Exception ex) {
					SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
				} finally {
					executando = false;
				}
			}
		}
	}

	@Override
	public void initializeComponents() {
		
		String desc=  getIntent().getStringExtra(OmegaConfiguration.PARAM_DESCRICAO_PONTO);
		View v= findViewById(R.id.tv_descricao_ponto);
		TextView tv =(TextView) v;
		tv.setText(desc);
		
		boolean isEntrada =  getIntent().getBooleanExtra(OmegaConfiguration.PARAM_ENTRADA, false);
		if(!isEntrada){
			tv.setTextColor(getResources().getColor(R.color.red));
		}else{
			tv.setTextColor(getResources().getColor( R.color.green));
		}
		((Button) findViewById(R.id.bt_confirmar)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				sairConfirmando();

			}
		});

		((Button) findViewById(R.id.bt_cancelar)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				sairCancelando();

			}
		});

		tvContagem = (TextView) findViewById(R.id.tv_contagem);
		handlerContagem = new HandlerContagem(1);

		setTimerContagem();
		return;
	}

	@Override
	public void onBackPressed() {
		sairConfirmando();
	}

	private void sairCancelando() {
		Intent intent = getIntent();
		intent.putExtra(OmegaConfiguration.PARAM_CANCELAR, true);
		setResult(RESULT_OK, intent);
		finish();
	}

	private void sairConfirmando() {
		Intent intent = getIntent();

		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	public void finish() {

		super.finish();
	}

	private void setTimerContagem() {

		// Starts ui updater timer
		this.timerContagem.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				handlerContagem.sendEmptyMessage(0);

			}
		}, 1000, 1000);
	}

}
