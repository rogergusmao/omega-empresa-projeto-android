package app.omegasoftware.pontoeletronico.TabletActivities;


import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.http.HelperHttp;

public class ConnectionServerInformationActivityMobile extends OmegaRegularActivity {
	
	
	TextView informationServerTextView;
	TextView labelCausaServerTextView;
	TextView causaServerTextView;
	private Typeface customTypeFace;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.connection_server_information_activity_mobile_layout);
		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		
		informationServerTextView = (TextView) findViewById(R.id.connection_server_information_status_textview);
		labelCausaServerTextView = (TextView) findViewById(R.id.connection_server_information_label_causa);
		causaServerTextView = (TextView) findViewById(R.id.connection_server_information_causa);
		
		new CustomDataLoader(this).execute();
	}
	
	@Override
	public boolean loadData() {
		return true;
	}

	@Override
	public void initializeComponents() {
		
		if(HelperHttp.isConnected(this)){
			if(HelperHttp.isConnected(this)){
				labelCausaServerTextView.setVisibility(View.GONE);
				causaServerTextView.setVisibility(View.GONE);
				informationServerTextView.setText("Internet online e servidor online");
				informationServerTextView.setTypeface(this.customTypeFace);
			} else{
				causaServerTextView.setText("Servidor esta fora do ar, tente novamente mais tarde.");
				informationServerTextView.setText("Internet online e servidor offline");
			}
		} else{
			informationServerTextView.setText("Internet offline");
		}
		labelCausaServerTextView.setTypeface(this.customTypeFace);
		causaServerTextView.setTypeface(this.customTypeFace);
		informationServerTextView.setTypeface(this.customTypeFace);
	}
	
}
