package app.omegasoftware.pontoeletronico.TabletActivities;

import java.util.LinkedHashMap;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.dialog.HelperDialog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculoUsuario;

public class DirigirVeiculoActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "DirigirVeiculoActivityMobile";

	//Search button
	
	public final static int DIALOG_COMECOU_A_DIRIGIR = 654987;
	public final static int DIALOG_SAIU_DO_VEICULO = 654988;
	public final static int SEARCH_ERROR_DIALOG_VEICULO_EM_USO  = 1;
	public final static int SEARCH_ERROR_DIALOG_FUNCIONARIO_ID  = 2;
	public final static int SEARCH_ERROR_DIALOG_SERVIDOR_OFFLINE  = 3;
	public final static int SEARCH_ERROR_DIALOG_FUNCIONARIO_VEICULO  = 4;
	public final static int SEARCH_ERROR_DIALOG_MISSING_VEICULO = 5;
	
	HandlerRefreshDirigindoVeiculo handlerRefreshDirigindoVeiculo;
	HandlerRefresDirigir handlerRefreshDirigir;
	String idUsuario;
	EXTDAOVeiculoUsuario.VeiculoDirigido objVeiculoDirigido;
	EXTDAOVeiculoUsuario objVeiculoUsuario;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.dirigir_veiculo_activity_mobile_layout);
		if(getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_ID_USUARIO)){
			idUsuario = getIntent().getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_USUARIO);
		} else 
			idUsuario = OmegaSecurity.getIdUsuario();
		
		new CustomDataLoader(this).execute();
		
	}
	Database db=null;
	LinkedHashMap<String, String > hashVeiculoUsuario;
	@Override
	public boolean loadData() {
		try{
		db = new DatabasePontoEletronico(this);
		
		
		objVeiculoUsuario = new EXTDAOVeiculoUsuario(db);
		hashVeiculoUsuario = EXTDAOVeiculoUsuario.getAllVeiculoDoUsuario(db, OmegaSecurity.getIdUsuario());
		objVeiculoDirigido = EXTDAOVeiculoUsuario.getVeiculoSendoDirigidoPeloUsuario(db, idUsuario);
		return true;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
		return false;
		}
	}


	
	public class HandlerRefresDirigir extends Handler{
		@Override
		public void handleMessage(Message msg)
		{	
			refreshDirigir();
		}
	}
	public class HandlerRefreshDirigindoVeiculo extends Handler{
		@Override
		public void handleMessage(Message msg)
		{	
			refreshDirigindoVeiculo();
		}
	}
	
	private void refreshDirigir(){
		LinearLayout llDirigir = (LinearLayout) findViewById(R.id.ll_dirigir);
		llDirigir.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try{
				DirigirVeiculoActivityMobile.this.setBancoFoiModificado();
				liberarVeiculoDirigidoSeExistente();
				String selectedVeiculo = String.valueOf(veiculoUsuarioSpinner.getSelectedItemId());
				if(selectedVeiculo.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{
					
					showDialog(SEARCH_ERROR_DIALOG_MISSING_VEICULO);
					
				} else{
					Table regVeiculoUsuario = objVeiculoUsuario.dirigirVeiculo(selectedVeiculo, idUsuario);
					if(regVeiculoUsuario == null){
						
//						showDialog(FORM_ERROR_DIALOG_INESPERADO);
					} else {
						sendBroadcastUpdatePainelUsuario(regVeiculoUsuario.getId());
						EXTDAOVeiculoUsuario objVeiculoUsuario = (EXTDAOVeiculoUsuario) regVeiculoUsuario;
						EXTDAOVeiculo objVeiculo = (EXTDAOVeiculo)objVeiculoUsuario.getObjDaChaveExtrangeira(EXTDAOVeiculoUsuario.VEICULO_ID_INT);
						objVeiculoDirigido = objVeiculoUsuario.factoryVeiculoDirigido(objVeiculoUsuario, objVeiculo);
						handlerRefreshDirigir.sendEmptyMessage(0);
						showDialog(DIALOG_COMECOU_A_DIRIGIR);
					}
				}
				}catch(Exception ex){
					SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
				
				}
			}
		});
	}
	
	
	private void refreshDirigindoVeiculo(){
		try{
		LinearLayout llDirigindoOVeiculo = (LinearLayout) findViewById(R.id.ll_dirigindo_o_veiculo);
		TextView tvDirigindoVeiculo = (TextView) findViewById(R.id.tv_dirigindo_o_veiculo);
		TextView tvDirigirOutroVeiculo = (TextView) findViewById(R.id.tv_dirigir_outro_veiculo);
		
		if(objVeiculoDirigido != null){
			String desc = getResources().getString(R.string.descricao_parar_de_dirigir);
			desc += " " +objVeiculoDirigido.objVeiculo.getDescricao(); 
			tvDirigindoVeiculo.setText(desc);
			llDirigindoOVeiculo.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					DirigirVeiculoActivityMobile.this.setBancoFoiModificado();
					liberarVeiculoDirigidoSeExistente();
					showDialog(DIALOG_SAIU_DO_VEICULO);
					
				}
			});
			llDirigindoOVeiculo.setVisibility(View.VISIBLE);
			tvDirigirOutroVeiculo.setVisibility(View.VISIBLE);
		} else {
			
			tvDirigirOutroVeiculo.setVisibility(View.GONE);
			llDirigindoOVeiculo.setVisibility(View.GONE);
		}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
		
		}
	}
	
	private void liberarVeiculoDirigidoSeExistente(){
		if(objVeiculoDirigido != null){
			objVeiculoDirigido.objVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.IS_ATIVO_BOOLEAN, "0");
			objVeiculoDirigido.objVeiculoUsuario.formatToSQLite();
			objVeiculoDirigido.objVeiculoUsuario.update(true);
			objVeiculoDirigido = null;
			handlerRefreshDirigindoVeiculo.sendEmptyMessage(0);
			sendBroadcastUpdatePainelUsuario(null);
			
		}
		
	}
	Spinner veiculoUsuarioSpinner = null;
	private void refreshVeiculoUsuarioSpinner(){
		if(veiculoUsuarioSpinner == null){
			veiculoUsuarioSpinner = (Spinner) findViewById(R.id.spinner_veiculo_usuario);
			veiculoUsuarioSpinner.setAdapter(
					new CustomSpinnerAdapter(
							getApplicationContext(),
							hashVeiculoUsuario,
							R.layout.spinner_item,
							R.string.selecione));
			((CustomSpinnerAdapter) veiculoUsuarioSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
		}
		
		
		
		
	}
	@Override
	public void initializeComponents() {
		setTituloCabecalho(getResources().getString(R.string.dirigir));
		handlerRefreshDirigindoVeiculo = new HandlerRefreshDirigindoVeiculo();
		handlerRefreshDirigir = new HandlerRefresDirigir();
		refreshVeiculoUsuarioSpinner();
		refreshDirigindoVeiculo();
		refreshDirigir();
	}


	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (id) {
		case SEARCH_ERROR_DIALOG_VEICULO_EM_USO:
			vTextView.setText(getResources().getString(R.string.error_veiculo_em_uso));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(SEARCH_ERROR_DIALOG_VEICULO_EM_USO);
				}
			});
			break;
			
		case DIALOG_SAIU_DO_VEICULO:
			vTextView.setText(getResources().getString(R.string.saiu_do_veiculo));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(DIALOG_SAIU_DO_VEICULO);
					DirigirVeiculoActivityMobile.this.setResult(RESULT_OK);
					DirigirVeiculoActivityMobile.this.finish();
				}
			});
			break;
		case DIALOG_COMECOU_A_DIRIGIR:
			vTextView.setText(getResources().getString(R.string.comecou_a_dirigir));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(DIALOG_COMECOU_A_DIRIGIR);
				}
			});
			break;
		case SEARCH_ERROR_DIALOG_FUNCIONARIO_ID:
			vTextView.setText(getResources().getString(R.string.error_id_funcionario_invalido));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(SEARCH_ERROR_DIALOG_FUNCIONARIO_ID);
				}
			});
			break;
		case SEARCH_ERROR_DIALOG_SERVIDOR_OFFLINE:
			vTextView.setText(getResources().getString(R.string.error_servidor_offline));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(SEARCH_ERROR_DIALOG_SERVIDOR_OFFLINE);
				}
			});
			break;
		case SEARCH_ERROR_DIALOG_FUNCIONARIO_VEICULO:
			vTextView.setText(getResources().getString(R.string.error_missing_funcionario_veiculo));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(SEARCH_ERROR_DIALOG_FUNCIONARIO_VEICULO);
				}
			});
			break;
		case SEARCH_ERROR_DIALOG_MISSING_VEICULO:
			vTextView.setText(getResources().getString(R.string.error_missing_funcionario_veiculo));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(SEARCH_ERROR_DIALOG_MISSING_VEICULO);
				}
			});
			break;
			
		default:
			return HelperDialog.getDialog(this, id);
		}

		return vDialog;

	}
	

	private void sendBroadcastUpdatePainelUsuario(String idVeiculoUsuario)
	{
		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SERVICE_PAINEL_USUARIO);
		updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DIRIGIR_VEICULO, idVeiculoUsuario);
		sendBroadcast(updateUIIntent);
	}



}


