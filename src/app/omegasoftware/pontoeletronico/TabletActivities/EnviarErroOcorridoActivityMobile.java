package app.omegasoftware.pontoeletronico.TabletActivities;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.dialog.HelperDialog;
import app.omegasoftware.pontoeletronico.webservice.ServicosPontoEletronico;

public class EnviarErroOcorridoActivityMobile extends OmegaRegularActivity {

	// Constants
	public static final String TAG = "EnviarErroOcorridoActivityMobile";

	private final int FORM_ERROR_DIALOG_MISSING_MENSAGEM = 91;

	// Search button
	private Button cadastrarButton;

	Drawable originalDrawable;

	String tagTela;
	// Spinners

	private EditText mensagemEditText;

	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.enviar_erro_ocorrido_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "trebucbd.ttf");
		Bundle vParameter = getIntent().getExtras();
		if (vParameter != null) {
			if (vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_TAG)) {
				tagTela = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_TAG);
			}
		}
		//
		new CustomDataLoader(this).execute();

	}

	@Override
	public boolean loadData() {
		return true;
	}

	public void formatarStyle() {
		// Search mode buttons
		((TextView) findViewById(R.id.mensagem_enviar_erro_ocorrido_textview)).setTypeface(this.customTypeFace);

		// Botuo cadastrar:
		((Button) findViewById(R.id.enviar_button)).setTypeface(this.customTypeFace);
	}

	@Override
	public void initializeComponents() {
		try {
			mensagemEditText = (EditText) findViewById(R.id.mensagem_edittext);

			originalDrawable = this.mensagemEditText.getBackground();
			// Attach search button to its listener
			this.cadastrarButton = (Button) findViewById(R.id.enviar_button);
			this.cadastrarButton.setOnClickListener(new EnviarButtonListener(this));

			this.formatarStyle();
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;

		switch (id) {
		default:
			vDialog = this.getErrorDialog(id);
			break;
		}
		return vDialog;
	}

	public Dialog getErrorDialog(int dialogType) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {

		case FORM_ERROR_DIALOG_MISSING_MENSAGEM:
			vTextView.setText(getResources().getString(R.string.error_dialog_missing_mensagem));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_MENSAGEM);
				}
			});
			break;

		default:
			return super.onCreateDialog(dialogType);

		}

		return vDialog;
	}

	// ---------------------------------------------------------------
	// ----------------- Methods related to search action ------------
	// ---------------------------------------------------------------
	// Handles search button click
	private void onEnviarButtonClicked(Activity pActivity) {
		new EnvioButtonLoader(this).execute();
	}

	private void clearComponents() {

		if (!this.mensagemEditText.getText().toString().equals("")) {
			this.mensagemEditText.setText("");
			this.mensagemEditText.setBackgroundDrawable(originalDrawable);
		}
	}

	// ---------------------------------------------------------------
	// ------------------ Methods related to planos ------------------
	// ---------------------------------------------------------------

	private class EnviarButtonListener implements OnClickListener {
		Activity activity;

		public EnviarButtonListener(Activity pActivity) {
			activity = pActivity;
		}

		public void onClick(View v) {
			onEnviarButtonClicked(activity);

		}

	}

	ClearComponentsHandler handlerClearComponents = new ClearComponentsHandler();

	public class ClearComponentsHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			clearComponents();
		}
	}

	public class EnvioButtonLoader extends AsyncTask<String, Integer, Boolean> {

		Integer dialogId = null;
		Activity activity;

		public EnvioButtonLoader(Activity pActivity) {
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				ServicosPontoEletronico servicosPE = new ServicosPontoEletronico(EnviarErroOcorridoActivityMobile.this);
				Integer codRetorno = servicosPE.relatoDeErro(mensagemEditText.getContext().toString(),
						originalDrawable.toString(), tagTela);
				if (codRetorno == null) {
					dialogId = HelperDialog.FORM_ERROR_DIALOG_TELEFONE_OFFLINE;
					return true;
				} else {
					dialogId = HelperDialog.FORM_DIALOG_AGRADECIMENTO;
					return false;
				}

			} catch (Exception e) {
				dialogId = HelperDialog.FORM_DIALOG_AGRADECIMENTO;
				SingletonLog.insereErro(e, TIPO.PAGINA);
				return false;
			}

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				if (result)
					handlerClearComponents.sendEmptyMessage(0);
				if (dialogId != null)
					showDialog(dialogId);
			} catch (Exception ex) {
				SingletonLog.openDialogError(EnviarErroOcorridoActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally {
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}
}
