package app.omegasoftware.pontoeletronico.TabletActivities;


import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.MenuAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCustomAdapterRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;

public class FactoryMenuActivityMobile extends OmegaCustomAdapterRegularActivity {
	public static String TAG = "FactoryMenuActivityMobile";
	
	
	//private String idParent = null;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		//Database.saveDatabaseNoArquivoTemporario(this, "BancoMenuPrincipal" + HelperDate.getDataParaNomeDeArquivo());
		

	}

	
	@Override
	public void initializeComponents() {
		

		
	}
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data){
//		super.onActivityResult(requestCode, resultCode, data);
//		try{
//			controlerProgressDialog.dismissProgressDialog();	
//		}catch(Exception ex){
//			int i = 0;
//			i += 1;
//		}
//		
//	}
	
	

	@Override
	protected void buildAdapter() throws OmegaDatabaseException {
		
		adapter = new MenuAdapter(
				this,
				nomeTag);
	}

	@Override
	protected void loadCommomParameters(Bundle pParameter) {
		
		if(pParameter != null){
			if(pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_NOME))
				nomeTag = pParameter.getString(OmegaConfiguration.SEARCH_FIELD_NOME);
//			if(pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID))
//				idParent = pParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		}
		
		
	}
	
	
}
