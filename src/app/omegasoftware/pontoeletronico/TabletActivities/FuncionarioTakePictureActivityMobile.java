package app.omegasoftware.pontoeletronico.TabletActivities;

//import java.util.Timer;
//import java.util.TimerTask;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.widget.TextView;
//import app.omegasoftware.pontoeletronico.R;
//import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
//import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
//import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
//import app.omegasoftware.pontoeletronico.cam.ActivityCameraTakePictureWithTimer;
//import app.omegasoftware.pontoeletronico.cam.CameraConfiguration;
//import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
//import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
//import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
//import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
//import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto;
//import app.omegasoftware.pontoeletronico.date.HelperDate;
//import app.omegasoftware.pontoeletronico.file.HelperFile;
//import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
//
//public class FuncionarioTakePictureActivityMobile extends OmegaRegularActivity {
//
//	// Close button
//	// private Button closeButton;
//	private static final String TAG = "FuncionarioDetailsActivityMobile.java";
//
//	private DatabasePontoEletronico db=null;
//	private EXTDAOPessoa funcionario;
//
//	private String funcionarioId;
//	boolean isEntrada;
//	//private EXTDAOPonto ponto;
//	private Typeface customTypeFace;
//	private static boolean isDirectoryOfPhotoCheck = false;
//	private Timer uiTimerToFinish = new Timer();
//	public static int QR_READER_ACTIVITY = 0;
//	public static int MENU_PONTO_ELETRONICO_ACTIVITY = 1;
//
//	protected int nextActivity = 0;
//	boolean isActivityStarted = false;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
//		super.onCreate(savedInstanceState);
//
//		setContentView(R.layout.marca_ponto_activity_mobile_layout);
//
//		this.customTypeFace = Typeface.createFromAsset(getApplicationContext()
//				.getAssets(), "trebucbd.ttf");
//		OmegaFileConfiguration config = new OmegaFileConfiguration();
//		String path = config.getPath(OmegaFileConfiguration.TIPO.FOTO);
//		Bundle vParameters = getIntent().getExtras();
//
//		// Gets funcionario id from the Intent used to create this activity
//		// TODO: Test connecting between ListResultActivity and
//		// FuncionarioDetailsActivity
//		try {
//			this.funcionarioId = vParameters
//					.getString(OmegaConfiguration.SEARCH_FUNCIONARIO_ID);
//			this.isEntrada = vParameters
//					.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_ENTRADA);
//
//		} catch (Exception ex) {
//
//		}
//
//		try {
//			this.db = new DatabasePontoEletronico(this);
//		} catch (OmegaDatabaseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		//ponto = new EXTDAOPonto(this.db);
//
//		new CustomDataLoader(this).execute();
//
//	}
//
//	@Override
//	protected void onDestroy() {
//		if (this.db != null) {
//			this.db.close();
//		}
//
//		super.onDestroy();
//	}
//
//	private void startCamTakePictureActivity() {
//
//		Intent intent = null;
//		// intent.putExtra(CameraConfiguration.VIEW_CAMERA_TAKE_PICTURE,
//		// R.layout.funcionario_filter_activity_mobile_layout);
//		intent = new Intent(getApplicationContext(),
//				ActivityCameraTakePictureWithTimer.class);
//
//		try {
//			OmegaFileConfiguration config = new OmegaFileConfiguration();
//			String path = config.getPath(OmegaFileConfiguration.TIPO.FOTO);
//			intent.putExtra(CameraConfiguration.PATH_DIRECTORY_FIELD,
//					path);
//
//			startActivityForResult(intent,
//					OmegaConfiguration.STATE_FUNCIONARIO_TAKE_PICTURE);
//			isActivityStarted = true;
//			// startActivity(intent);
//		} catch (Exception ex) {
//			// Log.w( "onActivityResult()", "Error: " + ex.getMessage() +
//			// " \n StackTrace:" + ex.toString());
//		}
//	}
//
//	/**
//	 * Updates title bar (Mudico or Mudica)
//	 */
//	private void refreshPageTitle(boolean pIsEntrada) {
//		if (pIsEntrada) {
//			if (pIsEntrada) {
//				// TextView textView = ((TextView)
//				// findViewById(R.id.funcionario_details_bar_title));
//				// textView.setText(getResources().getString(R.string.ponto_entrada));
//			} else {
//				// TextView textView = ((TextView)
//				// findViewById(R.id.funcionario_details_bar_title));
//				// textView.setText(getResources().getString(R.string.ponto_saida));
//			}
//
//		}
//
//	}
//
//	/**
//	 * Updates the TextView which holds the funcionario name
//	 */
//	private void refreshFuncionarioName() {
//		// Log.d(TAG,"refreshFuncionarioName()");
//		String nomeFuncionario = this.funcionario.getAttribute(
//				EXTDAOPessoa.NOME).getStrValue();
//		if (nomeFuncionario != null) {
//			nomeFuncionario = nomeFuncionario.toUpperCase();
//			((TextView) findViewById(R.id.tv_pessoa)).setText(nomeFuncionario);
//			((TextView) findViewById(R.id.tv_pessoa))
//					.setTypeface(this.customTypeFace);
//		}
//	}
//
//	/**
//	 * Updates the TextView which holds the funcionario profissao
//	 */
//	private void refreshHorarioPonto(String pData, String pHora) {
//		// Log.d(TAG,"refreshHorarioPonto()");
//
//		if (pData != null && pHora != null) {
//			((TextView) findViewById(R.id.ponto_data)).setText(pData);
//			((TextView) findViewById(R.id.ponto_data))
//					.setTypeface(this.customTypeFace);
//
//		}
//
//	}
//
//
//
//	@Override
//	public boolean loadData() {
//try{
//		isActivityStarted = true;
//
//		if (this.funcionarioId == null)
//			return true;
//		// Log.d(TAG,"loadData(): Retrieving funcionario data from the database");
//		this.funcionario = new EXTDAOPessoa(this.db);
//		this.funcionario.setAttrValue(EXTDAOPessoa.ID, this.funcionarioId);
//		this.funcionario.select();
//
//		startCamTakePictureActivity();
//		return true;
//}catch(Exception ex){
//	SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
//	//corrigir o tipo de erro de retorno
//return false;
//}
//	}
//
//	@Override
//	public void initializeComponents() {
//
//		if (this.funcionarioId == null)
//			return;
//
//		// Log.d(TAG,"refreshAllInformation(): Start refreshing all information");
//		try {
//			if (this.funcionarioId == null) {
//				return;
//			}
//			this.funcionario = new EXTDAOPessoa(this.db);
//			this.funcionario.setAttrValue(EXTDAOPessoa.ID, this.funcionarioId);
//			this.funcionario.select();
//
//			refreshPageTitle(true);
//			refreshFuncionarioName();
//
//		} catch (Exception ex) {
//			// Log.e("initializeComponents", ex.getMessage());
//		}
//
//		// Attach search button to its listener
//		// this.closeButton = (Button)
//		// findViewById(R.id.unimedbh_dialog_fechar_button);
//		// this.closeButton.setOnClickListener(new
//		// CloseButtonActivityClickListenerTablet(this));
//
//	}
//
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//		isActivityStarted = false;
//		if (requestCode == OmegaConfiguration.STATE_FUNCIONARIO_TAKE_PICTURE) {
//
//			try {
//				String pathPicture = intent
//						.getStringExtra(CameraConfiguration.PARAMETER_PICTURE);
//
//				EXTDAOPonto ponto = new EXTDAOPonto(this.db);
//				ponto.setAttrValue(EXTDAOPonto.PESSOA_ID_INT, funcionarioId);
//				if (pathPicture != null) {
//
//					ponto.setAttrValue(EXTDAOPonto.FOTO, pathPicture);
//					HelperDate helperDate = new HelperDate();
//					String vDate = HelperDate.getDataFormatadaParaExibicao( helperDate.getDateDisplay());
//					String vHour = helperDate.getTimeDisplay();
//					ponto.setAttrValue(EXTDAOPonto.DATA_SEC, vDate);
//					ponto.setAttrValue(EXTDAOPonto.DATA_OFFSEC, vHour);
//					refreshPageTitle(isEntrada);
//					refreshHorarioPonto(vDate, vHour);
//				}
//				ponto.formatToSQLite();
//				ponto.insert(true);
//
//			} catch (Exception ex) {
//				SingletonLog.insereErro(ex, TIPO.PAGINA);
//			}
//
//		}
//		//Intent myIntent = this.getIntent();
//
//		// setResult(OmegaConfiguration.STATE_INTIALIZE_READ_QR_CODE, myIntent);
//		setTimerToFinish();
//	}
//
//	private void setTimerToFinish() {
//
//		// Starts ui updater timer
//		this.uiTimerToFinish.scheduleAtFixedRate(new TimerTaskToFinish(this),
//				OmegaConfiguration.INITIAL_INFORMATION_PONTO_ELETRONICO,
//				OmegaConfiguration.INFORMATION_PONTO_ELETRONICO_TIMER);
//	}
//
//	class TimerTaskToFinish extends TimerTask {
//		Activity activity;
//
//		public TimerTaskToFinish(Activity pActivity) {
//			activity = pActivity;
//		}
//
//		@Override
//		public void run() {
//			this.cancel();
//			activity.finish();
//		}
//	}
//
//}
