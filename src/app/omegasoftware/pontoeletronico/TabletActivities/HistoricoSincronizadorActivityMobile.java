package app.omegasoftware.pontoeletronico.TabletActivities;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.HistoricoSincronizadorAdapter;
import app.omegasoftware.pontoeletronico.common.activity.HelperFonte;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaHistoricoSincronizador;

public class HistoricoSincronizadorActivityMobile extends OmegaRegularActivity {
	public static String TAG = "HistoricoSincronizadorActivityMobile";
	Database db=null;
	EXTDAOSistemaHistoricoSincronizador obj;
	List<Table> ultimosRegistros;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.historico_sincronizador_activity_mobile_layout);
		
		CustomDataLoader custom = new CustomDataLoader(this);
		custom.setMostrarCarregando(false);
		custom.execute();
	}

	ArrayList<View> viewsItens = new ArrayList<View>();

	@Override
	public boolean loadData() throws OmegaDatabaseException {
		db = new DatabasePontoEletronico(this);
		obj = new EXTDAOSistemaHistoricoSincronizador(db);
		ultimosRegistros = obj.getListTable(
				new String[] { Table.ID_UNIVERSAL }, "20", true, false);

		HistoricoSincronizadorAdapter adapter = new HistoricoSincronizadorAdapter(
				db, this);
		if (ultimosRegistros != null && ultimosRegistros.size() > 0) {
			String ultimoIdLido = ultimosRegistros.get(0).getId();
			obj.atualizaUltimaNotificacaoLida(ultimoIdLido);
			boolean corSim = false;
			for (int i = 0; i < ultimosRegistros.size(); i++) {
				Table registro = ultimosRegistros.get(i);
				View viewItem = adapter
						.getView((EXTDAOSistemaHistoricoSincronizador) registro);
				if (viewItem != null) {
					
					if(corSim){
						viewItem.setBackgroundResource(R.drawable.ui_button_blue);
					} else {
						viewItem.setBackgroundResource(R.drawable.ui_button_white);
					}
					corSim = !corSim;
					HelperFonte.setFontAllView(viewItem);
					
					viewsItens.add(viewItem);
				}

			}
		}
		return true;
	}

	@Override
	public void initializeComponents() {
		LinearLayout llRegistros = (LinearLayout) findViewById(R.id.ll_registros);
		LinearLayout llSemNoticias = (LinearLayout) findViewById(R.id.ll_sem_noticias);
		if (ultimosRegistros == null || ultimosRegistros.size() == 0) {

			llSemNoticias.setVisibility(View.VISIBLE);
			llRegistros.setVisibility(View.GONE);
		} else {
			llSemNoticias.setVisibility(View.GONE);
			llRegistros.setVisibility(View.VISIBLE);
		}

		for (int i = 0; i < viewsItens.size(); i++) {

			llRegistros.addView(viewsItens.get(i));
		}
		((View) findViewById(R.id.pb_carregando)).setVisibility(View.GONE);
	}

}
