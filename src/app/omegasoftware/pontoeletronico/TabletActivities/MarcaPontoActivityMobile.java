package app.omegasoftware.pontoeletronico.TabletActivities;


import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoPonto;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.file.HelperFile;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class MarcaPontoActivityMobile extends OmegaRegularActivity{


	//Close button
	//	private Button closeButton;
	private static final String TAG = "MarcaPontoActivityMobile";

	private String idPessoa;
	private String nomeEmpresa;
	private String idEmpresa;
	boolean isEntrada;
	private Integer idTipoPonto;
	private String latitude;
	private String longitude;
	private String arquivoFoto;
	private String idPonto;
	Button okButton;
	private TextView tvTurnoIntervalo;
	private static  boolean isDirectoryOfPhotoCheck = false;
	private Timer uiTimerToFinish = new Timer();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.marca_ponto_activity_mobile_layout);

		Bundle vParameters = getIntent().getExtras();

		//Gets funcionario id from the Intent used to create this activity

		
		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA))
			this.idPessoa = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA);
		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA))
			this.idEmpresa = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA);
		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_NOME_EMPRESA))
			this.nomeEmpresa = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_EMPRESA);
		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_ENTRADA))
			this.isEntrada = vParameters.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_ENTRADA);
		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_TIPO_PONTO))
			this.idTipoPonto = vParameters.getInt(OmegaConfiguration.SEARCH_FIELD_TIPO_PONTO);
		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LATITUDE))
			this.latitude = String.valueOf(vParameters.getInt(OmegaConfiguration.SEARCH_FIELD_LATITUDE));
		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LONGITUDE))
			this.longitude = String.valueOf(vParameters.getInt(OmegaConfiguration.SEARCH_FIELD_LONGITUDE));
		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_FILE))
			this.arquivoFoto = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_FILE);
		
		new CustomDataLoader(this).execute();

	}
	@Override
	public void initializeComponents() {

		try{
			if(this.idPessoa == null){
				return;
			}
			tvTurnoIntervalo = (TextView) findViewById(R.id.tv_turno_intervalo);
			
			refreshTipoPonto();
			okButton = (Button) findViewById(R.id.ok_button);
			okButton.setOnClickListener(new CustomServiceListener(this));
			Database db = new DatabasePontoEletronico(this);
			EXTDAOPessoa objPessoa = new EXTDAOPessoa(db);
			objPessoa.setAttrValue(EXTDAOPessoa.ID, this.idPessoa);
			if(objPessoa.select()){
				
				refreshNomePessoa(objPessoa.getStrValueOfAttribute(EXTDAOPessoa.NOME));
				refreshNomeEmpresa(nomeEmpresa);
				marcaPonto(null, db);
			}
			db.close();
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}

	}

	private class CustomServiceListener implements OnClickListener
	{
		Activity activity;
		public CustomServiceListener(Activity pActivity){
			activity = pActivity;
		}

		// On-click event handler for all the buttons

		
		public void onClick(View view) {
			uiTimerToFinish.cancel();

			Intent intent = activity.getIntent();
				
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, idPonto);
			activity.setResult(RESULT_OK, intent);
			activity.finish();		
		}
	}
	/**
	 * Updates title bar (Mudico or Mudica)
	 */
	private void refreshTipoPonto()
	{	
		if(isEntrada){
			TextView textView = ((TextView) findViewById(R.id.tipo_ponto));
			textView.setText(getResources().getString(R.string.ponto_entrada));	
		}	else{
			TextView textView = ((TextView) findViewById(R.id.tipo_ponto));
			textView.setText(getResources().getString(R.string.ponto_saida));
		}	
		if(idTipoPonto == EXTDAOTipoPonto.TIPO_PONTO.INTERVALO.getId()){
			tvTurnoIntervalo.setText(getResources().getString(R.string.intervalo));
				
		} else if(idTipoPonto == EXTDAOTipoPonto.TIPO_PONTO.TURNO.getId()){
			tvTurnoIntervalo.setText(getResources().getString(R.string.funcionario_turno));
				
		}  
		
	}

	/**
	 * Updates the TextView which holds the funcionario name
	 */
	private void refreshNomePessoa(String pNome)
	{
		//Log.d(TAG,"refreshFuncionarioName()");
		
		if(pNome != null){
			pNome = pNome.toUpperCase();
			((TextView) findViewById(R.id.tv_pessoa)).setText(pNome);
	
		}
	}
	private void refreshNomeEmpresa(String pNome)
	{
		//Log.d(TAG,"refreshFuncionarioName()");
		
		if(pNome != null){
			pNome = pNome.toUpperCase();
			((TextView) findViewById(R.id.empresa_textview)).setText(pNome);
	
		}
	}
	

	/**
	 * Updates the TextView which holds the funcionario profissao
	 */
	private void refreshHorarioPonto(String pData, String pHora)
	{

		if(pData != null && pHora != null){
			String data= HelperDate.getDataFormatadaParaExibicao(pData) + " " + HelperDate.getHoraFormatadaExibicao(pHora);
			((TextView) findViewById(R.id.ponto_data)).setText(data);
			
	
		}
	}

	@Override
	public boolean loadData() {
		return true;

	}

	private void marcaPonto(String pathPicture, Database db){

		EXTDAOPonto vObjPonto = new EXTDAOPonto(db);
		vObjPonto.setAttrValue(EXTDAOPonto.PESSOA_ID_INT, idPessoa);
		if(pathPicture != null && pathPicture.length() > 0 ){
			vObjPonto.setAttrValue(EXTDAOPonto.FOTO, pathPicture);
		}
		
		
		long utc = HelperDate.getTimestampSegundosUTC(this);
		int offset = HelperDate.getOffsetSegundosTimeZone();
		vObjPonto.setAttrValue(EXTDAOPonto.DATA_SEC, String.valueOf( utc));
		vObjPonto.setAttrValue(EXTDAOPonto.DATA_OFFSEC,String.valueOf( offset));
		vObjPonto.setAttrValue(EXTDAOPonto.EMPRESA_ID_INT, idEmpresa);
		vObjPonto.setAttrValue(EXTDAOPonto.PESSOA_ID_INT, idPessoa);
		vObjPonto.setAttrValue(EXTDAOPonto.USUARIO_ID_INT, OmegaSecurity.getIdUsuario());
		vObjPonto.setAttrValue(EXTDAOPonto.IS_ENTRADA_BOOLEAN, HelperString.valueOfBooleanSQL(isEntrada));
		if(idTipoPonto != null)
		vObjPonto.setAttrValue(EXTDAOPonto.TIPO_PONTO_ID_INT, String.valueOf(idTipoPonto));
		if(arquivoFoto != null && arquivoFoto.length() > 0)
		vObjPonto.setAttrValue(EXTDAOPonto.FOTO, arquivoFoto);
		
		if(latitude != null && longitude != null){
			vObjPonto.setAttrValue(EXTDAOPonto.LATITUDE_INT, latitude);
			vObjPonto.setAttrValue(EXTDAOPonto.LONGITUDE_INT, longitude);	
		}
		
		
		EXTDAOPessoaEmpresa vObjPessoaEmpresa = new EXTDAOPessoaEmpresa(db);
		vObjPessoaEmpresa.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, idPessoa);
		vObjPessoaEmpresa.setAttrValue(EXTDAOPessoaEmpresa.EMPRESA_ID_INT, idEmpresa);
		Table vTupla = (EXTDAOPessoaEmpresa)vObjPessoaEmpresa.getFirstTupla();
		if(vTupla != null){
			String vIdProfissao = vTupla.getStrValueOfAttribute(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT);
			vObjPonto.setAttrValue(EXTDAOPonto.PROFISSAO_ID_INT, vIdProfissao);	
		}
		
		vObjPonto.formatToSQLite();
		vObjPonto.insert(true);
		idPonto = vObjPonto.getId();
		Date d = HelperDate.getDateFromSecOfssec(utc, null);
		refreshHorarioPonto(
				HelperDate.getStringDate(this, d), 
				HelperDate.getStringTime(this, d));
		
		
		
		setTimerToFinish();
	}


	private void setTimerToFinish(){

		//Starts ui updater timer
		this.uiTimerToFinish.scheduleAtFixedRate( 
				new TimerTaskToFinish(this),
				OmegaConfiguration.INITIAL_INFORMATION_PONTO_ELETRONICO,
				OmegaConfiguration.INFORMATION_PONTO_ELETRONICO_TIMER);
	}

	class TimerTaskToFinish extends TimerTask{
		Activity activity;
		public TimerTaskToFinish(Activity pActivity){
			activity = pActivity;
		}

		@Override
		public void run() {
			this.cancel();
			
			
			Intent intent = activity.getIntent();
				
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, idPonto);
			activity.setResult(RESULT_OK, intent);
			activity.finish();
		}
	}

}
