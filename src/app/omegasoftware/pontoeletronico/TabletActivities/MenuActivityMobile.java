package app.omegasoftware.pontoeletronico.TabletActivities;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.widget.UltimoPontoWidget.UpdateService;
import app.omegasoftware.pontoeletronico.appconfiguration.ConstantesMenuPrograma;
import app.omegasoftware.pontoeletronico.appconfiguration.ConstantesMenuPrograma.TIPO_DIMENSAO_IMAGEM;
import app.omegasoftware.pontoeletronico.appconfiguration.ControladorMenuDoPrograma;
import app.omegasoftware.pontoeletronico.appconfiguration.ItemMenu;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.HelperFonte;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.controler.LoaderRunnable;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaHistoricoSincronizador;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculoUsuario;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.listener.CriptBrowserOpenerListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.service.ESTADO_SINCRONIZADOR;
import app.omegasoftware.pontoeletronico.service.ServiceMyPosition.ESTADO_RASTREADOR;

public class MenuActivityMobile extends OmegaRegularActivity {
	public static String TAG = "MenuActivityMobile";
	final int TOTAL_LINHAS_CONSIDERADAS = 4;
	final int TOTAL_COLUNAS_CONSIDERADAS = 2;
	TextView[][] matrizTV = new TextView[TOTAL_LINHAS_CONSIDERADAS][TOTAL_COLUNAS_CONSIDERADAS];

	SincronizadorReceiver sincronizadorReceiver;
	RastreadorReceiver rastreadorReceiver;
	
	EXTDAOSistemaHistoricoSincronizador obj;
	ImageView[][] matrizIV = new ImageView[TOTAL_LINHAS_CONSIDERADAS][TOTAL_COLUNAS_CONSIDERADAS];
	RelativeLayout[][] matrizRL = new RelativeLayout[TOTAL_LINHAS_CONSIDERADAS][TOTAL_COLUNAS_CONSIDERADAS];
	LinearLayout[] linhas = new LinearLayout[TOTAL_LINHAS_CONSIDERADAS];
	ItemMenu[][] matrizItemMenu = new ItemMenu[TOTAL_LINHAS_CONSIDERADAS][TOTAL_COLUNAS_CONSIDERADAS];
	static Integer abaAtual = null;
	int abasPorIndiceDosItens[][];
	// btn_voltar_desktop
	Database db = null;
	Button btnAjudaSecundario;
	Button btnAjudaPrimario;
	ImageButton btnVoltarDesktop;
	// ll_titulo_secundario
	LinearLayout llTituloSecundario;
	// tv_titulo_secundario
	TextView tvTituloSecundario;
	View ucUsuarioLogado;
	// android:id="@+id/rl_menu_principal"
	View headerMenuPrincipal;
	View headerMenuSecundario;
	Timer notificacaoTimer = null;
	ImageButton ivSincronizador, ivNovaVersao, ivSuporte, ivRastreador,
			ivMensagens, ivCamera;
	Button ivWww;
	TextView tvNotificacao;
	LinearLayout llNotificacao;

	ControladorMenuDoPrograma controlador;
	ItemMenu[] itens = null;
	NotificacaoHandler handlerNotificacao = new NotificacaoHandler();
	DesenhaViewHandler handlerDesenha = new DesenhaViewHandler();
	ApagaCelulaViewHandler handlerApagaCelula = new ApagaCelulaViewHandler();
	ApagaLinhaViewHandler handlerApagaLinha = new ApagaLinhaViewHandler();
	HandlerAtualizaUsuarioLogado handlerAtualizaUsuarioLogado = new HandlerAtualizaUsuarioLogado();
	MostraLinhaViewHandler handlerMostraLinha = new MostraLinhaViewHandler();
	TrocaIconeDesktopViewHandler handlerTrocaCorDesktop = new TrocaIconeDesktopViewHandler();
	ReceiverBroadcastPainelUsuario receiverBrodcastDirigirVeiculo = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.menu_principal);
		try{
//			if(OmegaConfiguration.DEBUGGING_TODOREMOVER)
//			{
//				throw new Exception("ASDF");
//			}
			formatarStyle();
			inicializaComponentesPadroes();

			controlador = ControladorMenuDoPrograma
					.getControladorMenuPrograma(this);
			if (controlador == null) {
				controlador = ControladorMenuDoPrograma.inicializaControlador(this);
				itens = controlador.getVetorItensRaiz();
			} else {
				itens = controlador.getVetorItensAtual();
			}
		
			if (abaAtual == null) {

				abaAtual = 0;
			} else {
				abaAtual = controlador.getIdAbaAtual();
			}
			initializeComponents();
			inicializaAbas();
			desenhaMatriz(true, abaAtual);

			RotinaSincronizador
					.criaDialogoParaResetarOBancoDeDadosSeNecessario(MenuActivityMobile.this);
			if(receiverBrodcastDirigirVeiculo == null)
				receiverBrodcastDirigirVeiculo = new ReceiverBroadcastPainelUsuario(this);
			
			
		}catch(Exception ex){
			SingletonLog.openDialogError(this, ex, SingletonLog.TIPO.PAGINA);
		}
		
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if (requestCode == OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW) {

			try {
				if (resultCode == RESULT_OK) {
					boolean vIsModificated = true;
					Bundle pParameter = intent.getExtras();

					if (pParameter != null)
						if (pParameter
								.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED))
							vIsModificated = pParameter
									.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED);
					if (vIsModificated) {
						foiModificado = true;
						handlerAtualizaUsuarioLogado.sendEmptyMessage(0);
					}
				}

			} catch (Exception ex) {
				SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
			}
		} else {
			super.onActivityResult(requestCode, resultCode, intent);
		}
	}

	public void inicializaDadosUsuarioLogado() {
		try{
		View viewUsuario = ucUsuarioLogado;
		
		EXTDAOUsuario objUsuario = new EXTDAOUsuario(db);
		String idUsuario = OmegaSecurity.getIdUsuario();
		objUsuario.select(idUsuario);
		
		EXTDAOPessoa objPessoa = (EXTDAOPessoa) objUsuario.getObjPessoa();
		if (objPessoa != null) {
			((TextView) viewUsuario.findViewById(R.id.tv_nome))
					.setText(objUsuario
							.getStrValueOfAttribute(EXTDAOUsuario.NOME));
			((TextView) viewUsuario.findViewById(R.id.tv_email))
					.setText(objUsuario
							.getStrValueOfAttribute(EXTDAOUsuario.EMAIL));

			EXTDAOPonto objPonto = new EXTDAOPonto(db);
			final EXTDAOPonto.PontoPessoa pontoPessoa = objPonto
					.getUltimoPontoDaPessoa(objPessoa.getId());

			EXTDAOVeiculoUsuario.VeiculoDirigido objVeiculoDirigido = EXTDAOVeiculoUsuario
					.getVeiculoSendoDirigidoPeloUsuario(db, idUsuario);
			
			TextView tvCorporacao = ((TextView) viewUsuario
					.findViewById(R.id.tv_corporacao));
			tvCorporacao.setText(OmegaSecurity.getCorporacao());
			
			TextView tvVeiculo = ((TextView) viewUsuario
					.findViewById(R.id.tv_veiculo));
			if (objVeiculoDirigido != null && objVeiculoDirigido.objVeiculo != null) {
				String dirigindo = getResources().getString(
						R.string.dirigindo_o_veiculo);
				String descVeiculo = objVeiculoDirigido.objVeiculo
						.getDescricao();
				tvVeiculo.setText(dirigindo + " " + descVeiculo);
			} else {

				tvVeiculo.setText(getResources().getString(
						R.string.usuario_logado_nao_esta_dirigindo));
			}
			((TextView) viewUsuario
					.findViewById(R.id.tv_data_ponto_intervalo))
					.setText("");
			if (pontoPessoa.intervalo != null) {
				((TextView) viewUsuario
						.findViewById(R.id.tv_data_ponto_intervalo))
						.setText("");
				if (pontoPessoa.intervalo.entrada != null) {
					((TextView) viewUsuario
							.findViewById(R.id.tv_data_ponto_intervalo))
							.append(pontoPessoa.intervalo.entrada.data
									+ " "
									+ pontoPessoa.intervalo.entrada.hora);
				}
				if (pontoPessoa.intervalo.saida != null) {
					((TextView) viewUsuario
							.findViewById(R.id.tv_data_ponto_intervalo))
							.append(pontoPessoa.intervalo.saida.data + " "
									+ pontoPessoa.intervalo.saida.hora);
				}
			}
			((TextView) viewUsuario
					.findViewById(R.id.tv_data_ponto_turno)).setText("");
			((TextView) viewUsuario
					.findViewById(R.id.tv_data_ponto_turno_saida)).setText("");
			if (pontoPessoa.turno != null) {
				((TextView) viewUsuario
						.findViewById(R.id.tv_data_ponto_turno))
						.setText("");
				if (pontoPessoa.turno.entrada != null) {
					((TextView) viewUsuario
							.findViewById(R.id.tv_data_ponto_turno))
							.append(pontoPessoa.turno.entrada.data + " "
									+ pontoPessoa.turno.entrada.hora);
				}
				if (pontoPessoa.turno.saida != null) {
					String desc = (pontoPessoa.turno.entrada != null) ? pontoPessoa.turno.saida.hora :
							pontoPessoa.turno.saida.data + " "
							+ pontoPessoa.turno.saida.hora;
					((TextView) viewUsuario
							.findViewById(R.id.tv_data_ponto_turno_saida))
							.append(desc);
				}
			}
//			Button baterPontoBt = (Button) viewUsuario.findViewById(R.id.bater_ponto_button);
//			if(pontoPessoa.idPessoaEmpresa != null){
//				
//				baterPontoBt.setVisibility(View.VISIBLE);
//				baterPontoBt.setOnClickListener(new OnClickListener() {
//					
//					@Override
//					public void onClick(View v) {
//						
//						Intent intent = new Intent(
//								MenuActivityMobile.this.getApplicationContext(), 
//								DetailUsuarioActivityMobile.class);
//						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA_EMPRESA, pontoPessoa.idPessoaEmpresa);
//						MenuActivityMobile.this.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
//						
//					}
//				});
//			} else {
//				baterPontoBt.setVisibility(View.GONE);
//			}
			
		}
	
		viewUsuario.setClickable(true);
		OnClickListener listener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MenuActivityMobile.this,
						DetailUsuarioActivityMobile.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID,
						OmegaSecurity.getIdUsuario());
				MenuActivityMobile.this.startActivityForResult(intent,
						OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
			}
		};
		viewUsuario.setOnClickListener(listener);
		((Button) viewUsuario.findViewById(R.id.bt_bater_ponto)).setOnClickListener(listener);
		
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);

			
		}
	}
	
	

	public void inicializaAbas() {

		int totalItensVisiveisPorAba = itens.length > (TOTAL_COLUNAS_CONSIDERADAS * TOTAL_LINHAS_CONSIDERADAS) ? TOTAL_COLUNAS_CONSIDERADAS
				* TOTAL_LINHAS_CONSIDERADAS
				: itens.length;

		int totalAbas = HelperInteger.arredondarParaCima(itens.length,
				totalItensVisiveisPorAba);

		abasPorIndiceDosItens = new int[totalAbas][];
		int ponteiroItens = 0;
		for (int k = 0; k < totalAbas; k++) {
			int itensRestantes = itens.length - ponteiroItens;
			int tamanhoAba = (itensRestantes < totalItensVisiveisPorAba) ? itensRestantes
					: totalItensVisiveisPorAba;
			abasPorIndiceDosItens[k] = new int[tamanhoAba];
			for (int i = 0; i < tamanhoAba; i++)
				abasPorIndiceDosItens[k][i] = ponteiroItens++;
		}
		final Class<?> classeAjuda = controlador.getClasseAjudaDaTelaAtual();

		if (classeAjuda != null) {
			btnAjudaSecundario
					.setVisibility(View.VISIBLE);
			btnAjudaSecundario.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(MenuActivityMobile.this,
							classeAjuda);
					startActivity(intent);

				}
			});
			btnAjudaPrimario
					.setVisibility(View.VISIBLE);
			btnAjudaPrimario.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(MenuActivityMobile.this,
							classeAjuda);
					startActivity(intent);

				}
			});
		} else {
			btnAjudaSecundario
					.setVisibility(View.GONE);
			btnAjudaSecundario.setOnClickListener(null);
			btnAjudaPrimario
					.setVisibility(View.GONE);
			btnAjudaPrimario.setOnClickListener(null);
		}
		if (controlador.getIdPermissaoTelaAtual() == ConstantesMenuPrograma.ID_RAIZ) {
			atualizaEstadoPrincipal();
		} else {

			atualizaEstadoSecundario();
		}

	}

	public int getProximaAba() {
		if (abaAtual < abasPorIndiceDosItens.length - 1) {
			return abaAtual + 1;
		} else
			return 0;
	}

	public void desenhaNovaAba(int idIcone, int novaAba) {
		controlador.atualizaValorDaAbaAtual(novaAba);
		abaAtual = novaAba;

	}

	public void desenhaMatriz(boolean inicializando, int aba) {

		int iconeIconeDesktop = 0;
		int drawableMenu = 0;
		switch (aba) {
		case 0:
			iconeIconeDesktop = R.drawable.desktop_verde;
			drawableMenu = R.drawable.menu_windows_phone_blue_button;
			break;

		default:
			iconeIconeDesktop = R.drawable.desktop_azul;
			drawableMenu = R.drawable.menu_windows_phone_green_button;
			break;
		}
		Message msg = new Message();
		msg.what = iconeIconeDesktop;
		msg.arg1 = aba;
		handlerTrocaCorDesktop.sendMessage(msg);

		apagaMatrizItemMenu();

		int totalItensVisiveis = abasPorIndiceDosItens[aba].length;

		if (inicializando) {
			if (TOTAL_LINHAS_CONSIDERADAS < matrizRL.length) {
				for (int k = TOTAL_LINHAS_CONSIDERADAS; k < matrizRL.length; k++) {
					apagaLinha(k);
				}
			}
		}

		int totalDeLinhasOcupadas = totalItensVisiveis > TOTAL_LINHAS_CONSIDERADAS ? TOTAL_LINHAS_CONSIDERADAS
				: totalItensVisiveis;

		@SuppressWarnings("unused")
		int totalDeLinhasNaoOcupadas = TOTAL_LINHAS_CONSIDERADAS
				- totalDeLinhasOcupadas;
		int totalDeColunasOcupadasEmTodasAsLinhas = totalItensVisiveis
				/ TOTAL_LINHAS_CONSIDERADAS;

		int totalItensDaSobra = totalItensVisiveis
				- (totalDeLinhasOcupadas * totalDeColunasOcupadasEmTodasAsLinhas);
		int sobraDeItensPorLinha = totalItensDaSobra > 0 ? 1 : 0;
		int ponteiro = 0;
		int i = 0;
		for (; i < totalDeLinhasOcupadas; i++) {
			procedimentoMostraLinha(i, inicializando);
			int j = 0;
			int totalDeColunasDessaLinha = (totalItensDaSobra > 0) ? totalDeColunasOcupadasEmTodasAsLinhas
					+ sobraDeItensPorLinha
					: totalDeColunasOcupadasEmTodasAsLinhas;
			TIPO_DIMENSAO_IMAGEM tdi = TIPO_DIMENSAO_IMAGEM.PEQUENO;
			if (totalDeColunasDessaLinha == TOTAL_COLUNAS_CONSIDERADAS
					&& totalDeLinhasOcupadas == TOTAL_LINHAS_CONSIDERADAS)

				tdi = TIPO_DIMENSAO_IMAGEM.PEQUENO;

			else if (totalDeColunasDessaLinha == TOTAL_COLUNAS_CONSIDERADAS
					&& totalDeLinhasOcupadas < TOTAL_LINHAS_CONSIDERADAS)
				tdi = TIPO_DIMENSAO_IMAGEM.PEQUENO;

			else if (totalDeColunasDessaLinha < TOTAL_COLUNAS_CONSIDERADAS
					&& totalDeLinhasOcupadas == TOTAL_LINHAS_CONSIDERADAS)
				tdi = TIPO_DIMENSAO_IMAGEM.PEQUENO;

			else if (totalDeColunasDessaLinha < TOTAL_COLUNAS_CONSIDERADAS
					&& totalDeLinhasOcupadas < TOTAL_LINHAS_CONSIDERADAS)
				tdi = TIPO_DIMENSAO_IMAGEM.MEDIO;

			for (; j < totalDeColunasOcupadasEmTodasAsLinhas; j++) {
				int indiceItem = abasPorIndiceDosItens[aba][ponteiro];
				procedimentoDesenhaCelula(i, j, itens[indiceItem], indiceItem,
						tdi, inicializando, drawableMenu);
				ponteiro++;
			}

			if (totalItensDaSobra > 0) {
				for (; j < (sobraDeItensPorLinha + totalDeColunasOcupadasEmTodasAsLinhas)
						&& totalItensDaSobra > 0; j++) {
					int indiceItem = abasPorIndiceDosItens[aba][ponteiro];
					procedimentoDesenhaCelula(i, j, itens[indiceItem],
							indiceItem, tdi, inicializando, drawableMenu);
					ponteiro++;
					totalItensDaSobra--;
				}
			}
			for (; j < TOTAL_COLUNAS_CONSIDERADAS; j++) {
				procedimentoEscondeCelula(i, j, inicializando);
			}
		}
		for (; i < TOTAL_LINHAS_CONSIDERADAS; i++) {
			procedimentoApagaLinha(i, inicializando);
		}
	}

	public void procedimentoApagaLinha(int i, boolean inicializando) {
		if (inicializando) {

			apagaLinha(i);
		} else {
			for (int j = 0; j < TOTAL_COLUNAS_CONSIDERADAS; j++)
				matrizItemMenu[i][j] = null;

			Message msg = new Message();
			msg.arg1 = i;

			handlerApagaLinha.sendMessage(msg);
		}

	}

	public void procedimentoMostraLinha(int i, boolean inicializando) {

		if (linhas[i].getVisibility() == View.VISIBLE)
			return;
		if (inicializando) {

			mostraLinha(i);
		} else {

			Message msg = new Message();
			msg.arg1 = i;

			handlerMostraLinha.sendMessage(msg);
		}

	}

	public void procedimentoEscondeCelula(int i, int j, boolean inicializando) {

		if (inicializando) {

			apagaView(i, j);
		} else {
			matrizItemMenu[i][j] = null;
			Message msg = new Message();
			msg.arg1 = i;
			msg.arg2 = j;
			handlerApagaCelula.sendMessage(msg);
		}

	}

	public void procedimentoDesenhaCelula(int i, int j, ItemMenu item,
			int indexItem, TIPO_DIMENSAO_IMAGEM tdi, boolean inicializando,
			int drawableMenu) {

		item.tipoDimensaoImagem = tdi;
		item.drawableMenu = drawableMenu;
		matrizItemMenu[i][j] = item;

		if (inicializando) {
			desenhaView(i, j, item);
		} else {
			Message msg = new Message();
			msg.arg1 = i;
			msg.arg2 = j;
			msg.what = indexItem;
			handlerDesenha.sendMessage(msg);
		}
	}
	
	public class NotificacaoHandler extends Handler {

		public NotificacaoHandler() {
		}

		@Override
		public void handleMessage(Message msg) {
			Long total;
			try {
				total = obj.getTotalNotificacaoNaoLida();
			
			if(total != null  ){
				if(total > 0){
					tvNotificacao.setText("+" + total.toString() );
					llNotificacao.setBackgroundResource(R.drawable.ui_button_red);
				} else {
					//android:background="@drawable/ui_button_red"
					llNotificacao.setBackgroundResource(R.drawable.ui_button_gray);
					tvNotificacao.setText("0" );
				}	
			}
			} catch (OmegaDatabaseException e) {
				// TODO Colocar um estado no painel que exibe a quantidade de notificacao
				// uma mensagme de erro padrao
				SingletonLog.insereErro(e, TIPO.PAGINA);
			}

		}
	}


	public class DesenhaViewHandler extends Handler {

		public DesenhaViewHandler() {
		}

		@Override
		public void handleMessage(Message msg) {
			int i = msg.arg1;
			int j = msg.arg2;
			int indexItem = msg.what;

			ItemMenu item = itens[indexItem];

			try {

				desenhaView(i, j, item);

			} catch (Exception e) {

				SingletonLog.insereErro(e, TIPO.PAGINA);
			}

		}
	}

	public void desenhaView(int i, int j, ItemMenu item) {
		if (matrizRL[i][j].getVisibility() != View.VISIBLE) {
			matrizRL[i][j].setVisibility(View.VISIBLE);
		}
		// Integer drawable = item.getIdDrawable();
		// if( drawable == null){
		// matrizIV[i][j].setVisibility(View.GONE);
		// } else {
		// if(matrizIV[i][j].getVisibility() !=
		// View.VISIBLE)
		// matrizIV[i][j].setVisibility(View.VISIBLE);
		// matrizIV[i][j].setBackgroundDrawable(getResources().getDrawable((int)
		// drawable));
		//
		// }
		if (matrizTV[i][j].getVisibility() != View.VISIBLE)
			matrizTV[i][j].setVisibility(View.VISIBLE);
		matrizTV[i][j].setText(item.nome);
		
		Integer drawable = item.getIdDrawable();
		
		boolean validade = false;
		if( drawable != null){
			Drawable d = getResources().getDrawable(drawable);
			if(matrizIV[i][j] != null && d!=null){
				matrizIV[i][j].setImageDrawable(d);
				matrizIV[i][j].setVisibility(View.VISIBLE);
				validade = true;
			}
		}
		if(!validade && matrizIV[i][j] != null)
			matrizIV[i][j].setVisibility(View.GONE);
		matrizRL[i][j].setBackgroundDrawable(getResources().getDrawable(item.drawableMenu));
		
		// matrizTV[i][j].setTextAppearance(this,
		// item.tipoDimensaoImagem.getStyleFonte(this));

		HelperFonte.setFontView(matrizTV[i][j]);

		

	}

	public void apagaMatrizItemMenu() {
		for (int i = 0; i < matrizItemMenu.length; i++) {
			for (int j = 0; j < matrizItemMenu[i].length; j++) {
				matrizItemMenu[i][j] = null;
			}
		}
	}

	public class TrocaIconeDesktopViewHandler extends Handler {

		public TrocaIconeDesktopViewHandler() {
		}

		@Override
		public void handleMessage(Message msg) {
			int cor = msg.what;
			int novaAba = msg.arg1;

			desenhaNovaAba(cor, novaAba);

		}
	}

	public class MostraLinhaViewHandler extends Handler {

		public MostraLinhaViewHandler() {
		}

		@Override
		public void handleMessage(Message msg) {
			int i = msg.arg1;

			mostraLinha(i);

		}
	}

	public class ApagaLinhaViewHandler extends Handler {

		public ApagaLinhaViewHandler() {
		}

		@Override
		public void handleMessage(Message msg) {
			int i = msg.arg1;

			apagaLinha(i);

		}
	}

	public class HandlerAtualizaUsuarioLogado extends Handler {

		public HandlerAtualizaUsuarioLogado() {
		}

		@Override
		public void handleMessage(Message msg) {
			MenuActivityMobile.this.inicializaDadosUsuarioLogado();

		}
	}

	public class ApagaCelulaViewHandler extends Handler {

		public ApagaCelulaViewHandler() {
		}

		@Override
		public void handleMessage(Message msg) {
			int i = msg.arg1;
			int j = msg.arg2;

			apagaView(i, j);

		}
	}

	public void apagaView(int i, int j) {
		try {
			try {
				matrizRL[i][j]
						.setVisibility(View.GONE);
				// matrizTV[i][j].setText("");

			} catch (Exception e) {
				// Log.e("Error reading file", e.toString());
			}
		} catch (Exception e) {

			SingletonLog.insereErro(e, TIPO.PAGINA);
		}
	}

	public void mostraLinha(int i) {
		try {
			try {

				linhas[i].setVisibility(View.VISIBLE);

			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.PAGINA);
			}
		} catch (Exception e) {

			SingletonLog.insereErro(e, TIPO.PAGINA);
		}
	}

	public void apagaLinha(int i) {
		try {
			try {
				linhas[i].setVisibility(View.GONE);
				// matrizTV[i][j].setText("");

			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.PAGINA);
			}
		} catch (Exception e) {

			SingletonLog.insereErro(e, TIPO.PAGINA);
		}
	}

	@Override
	public void onBackPressed() {

		voltar();

	}

	public void voltar() {
		try {
			if (controlador.getIdPermissaoTelaAtual() == ConstantesMenuPrograma.ID_RAIZ)

				super.onBackPressed();
			else {
				itens = controlador.voltarParaAPaginaAncestral();

				inicializaAbas();

				desenhaMatriz(false, controlador.getIdAbaAtual());
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
	}

	// static boolean executed = false;

	@Override
	protected void formatarStyle() {
		matrizTV[0] = new TextView[] {
				((TextView) findViewById(R.id.celula_11_textview)),
				((TextView) findViewById(R.id.celula_12_textview)) };
		matrizTV[1] = new TextView[] {
				((TextView) findViewById(R.id.celula_21_textview)),
				((TextView) findViewById(R.id.celula_22_textview)) };
		matrizTV[2] = new TextView[] {
				((TextView) findViewById(R.id.celula_31_textview)),
				((TextView) findViewById(R.id.celula_32_textview)) };
		matrizTV[3] = new TextView[] {
				((TextView) findViewById(R.id.celula_41_textview)),
				((TextView) findViewById(R.id.celula_42_textview)) };
		
		matrizRL[0] = new RelativeLayout[] {
				((RelativeLayout) findViewById(R.id.rl_celula_11)),
				((RelativeLayout) findViewById(R.id.rl_celula_12)) };
		matrizRL[1] = new RelativeLayout[] {
				((RelativeLayout) findViewById(R.id.rl_celula_21)),
				((RelativeLayout) findViewById(R.id.rl_celula_22)) };
		matrizRL[2] = new RelativeLayout[] {
				((RelativeLayout) findViewById(R.id.rl_celula_31)),
				((RelativeLayout) findViewById(R.id.rl_celula_32)) };
		matrizRL[3] = new RelativeLayout[] {
				((RelativeLayout) findViewById(R.id.rl_celula_41)),
				((RelativeLayout) findViewById(R.id.rl_celula_42)) };
		
		matrizIV[0] = new ImageView[] {
				((ImageView) findViewById(R.id.iv_celula_11)),
				((ImageView) findViewById(R.id.iv_celula_12)) };
		matrizIV[1] = new ImageView[] {
				((ImageView) findViewById(R.id.iv_celula_21)),
				((ImageView) findViewById(R.id.iv_celula_22)) };
		matrizIV[2] = new ImageView[] {
				((ImageView) findViewById(R.id.iv_celula_31)),
				((ImageView) findViewById(R.id.iv_celula_32)) };
		matrizIV[3] = new ImageView[] {
				((ImageView) findViewById(R.id.iv_celula_41)),
				((ImageView) findViewById(R.id.iv_celula_42)) };
		
		ViewGroup vg = (ViewGroup) findViewById(R.id.view_pai);
		HelperFonte.setFontAllView(vg);

		for (int i = 0; i < matrizRL.length; i++) {
			for (int j = 0; j < matrizRL[i].length; j++) {
				matrizRL[i][j].setOnClickListener(new CelulaOnClickListener(i,
						j));
				;
			}
		}
		linhas = new LinearLayout[] {
				((LinearLayout) findViewById(R.id.ll_linha_1)),
				((LinearLayout) findViewById(R.id.ll_linha_2)),
				((LinearLayout) findViewById(R.id.ll_linha_3)),
				((LinearLayout) findViewById(R.id.ll_linha_4)),
				((LinearLayout) findViewById(R.id.ll_linha_5)) };

	}

	@Override
	public boolean loadData() {

		
		return true;
	}

	@Override
	public void initializeComponents() throws OmegaDatabaseException {

		btnAjudaPrimario = (Button) findViewById(R.id.btn_ajuda_primario);
		btnAjudaSecundario = (Button) findViewById(R.id.btn_ajuda_secundario);
		headerMenuPrincipal = (View) findViewById(R.id.header);
		ucUsuarioLogado = (View) findViewById(R.id.ic_usuario_logado);
		headerMenuSecundario = (View) findViewById(R.id.header_secundario);
		ivSincronizador = (ImageButton) findViewById(R.id.iv_sincronizador);
		tvNotificacao = (TextView) findViewById(R.id.tv_notificacao);
		llNotificacao = (LinearLayout) findViewById(R.id.ll_notificacao);
		llNotificacao.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MenuActivityMobile.this,
						HistoricoSincronizadorActivityMobile.class);
				startActivity(intent);

			}
		});
		ivSincronizador.setImageDrawable(getResources().getDrawable(
				ESTADO_SINCRONIZADOR.getEstadoAtual().getDrawable()));
		ivSincronizador.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MenuActivityMobile.this,
						SynchronizeReceiveActivityMobile.class);
				startActivity(intent);

			}
		});

		ivNovaVersao = (ImageButton) findViewById(R.id.iv_nova_versao);
		ivSuporte = (ImageButton) findViewById(R.id.iv_suporte);

		ivRastreador = (ImageButton) findViewById(R.id.iv_rastreamento);
		ivRastreador.setImageDrawable(getResources().getDrawable(
				ESTADO_RASTREADOR.getEstadoAtual().getDrawable()));
		ivRastreador.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MenuActivityMobile.this,
						MenuServicoOnlineUsuarioActivityMobile.class);
				startActivity(intent);

			}
		});
		
		ivWww = (Button) findViewById(R.id.iv_www);
		//ivWww.setOnClickListener(new BrowserOpenerListener(getResources().getString(R.string.sistema_android_site_url)));
//		String email =  "rtt2@g.c";
//		String corporacao = "RTT2";
//		String senha = "123";
		ivWww.setOnClickListener(
				new CriptBrowserOpenerListener(
						MenuActivityMobile.this,
						OmegaConfiguration.BASE_URL_OMEGA_SOFTWARE() + "client_area/actions.php?class=Servicos_web&action=actionlogaNoSistemaDaCorporacaoCript",
						new String[] {"email", "corporacao", "senha"},
						//new String[] {email, corporacao, senha},
						new String[] {OmegaSecurity.getEmail(), OmegaSecurity.getCorporacao(), OmegaSecurity.getSenha()},
						getResources().getString(R.string.sistema_android_site_url)
						));
		
		ivMensagens = (ImageButton) findViewById(R.id.iv_mensagens);
		ivCamera = (ImageButton) findViewById(R.id.iv_camera);

		btnVoltarDesktop = (ImageButton) findViewById(R.id.btn_voltar_desktop);
		btnVoltarDesktop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				voltar();

			}
		});
		tvTituloSecundario = (TextView) findViewById(R.id.tv_titulo_secundario);
		llTituloSecundario = (LinearLayout) findViewById(R.id.ll_titulo_secundario);

		if (sincronizadorReceiver == null) {
			sincronizadorReceiver = new SincronizadorReceiver();
			IntentFilter uiUpdateIntentFilter = new IntentFilter(
					OmegaConfiguration.REFRESH_ESTADO_SINCRONIZADOR);
			registerReceiver(this.sincronizadorReceiver, uiUpdateIntentFilter);
		}
		if (rastreadorReceiver == null) {
			rastreadorReceiver = new RastreadorReceiver();
			IntentFilter uiUpdateIntentFilter2 = new IntentFilter(
					OmegaConfiguration.REFRESH_ESTADO_RASTREADOR);
			registerReceiver(this.rastreadorReceiver, uiUpdateIntentFilter2);
		}

		db = new DatabasePontoEletronico(MenuActivityMobile.this);
		obj = new EXTDAOSistemaHistoricoSincronizador(db);
		setTimerNotificacao();
		
		inicializaDadosUsuarioLogado();
	}
	
	public void atualizaEstadoSecundario() {
		String tituloScundario = controlador.getTituloDaTelaAtual();
		if (tituloScundario != null && tituloScundario.length() > 0)
			tvTituloSecundario.setText(tituloScundario);
		llTituloSecundario.setVisibility(View.VISIBLE);

		headerMenuSecundario
				.setVisibility(View.VISIBLE);

		headerMenuPrincipal.setVisibility(View.GONE);
		ucUsuarioLogado.setVisibility(View.GONE);
	}

	public void atualizaEstadoPrincipal() {
		tvTituloSecundario.setText("");
		llTituloSecundario.setVisibility(View.GONE);
		headerMenuSecundario
				.setVisibility(View.GONE);
		headerMenuPrincipal
				.setVisibility(View.VISIBLE);
		ucUsuarioLogado.setVisibility(View.VISIBLE);

	}

	@Override
	protected void onDestroy() {
		try{
			if(receiverBrodcastDirigirVeiculo != null ){
				unregisterReceiver(receiverBrodcastDirigirVeiculo);
				receiverBrodcastDirigirVeiculo = null;
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
		try {
			if(notificacaoTimer != null){
				notificacaoTimer.cancel();
			}
			if (this.sincronizadorReceiver != null) {
				unregisterReceiver(this.sincronizadorReceiver);
				sincronizadorReceiver = null;
			}
			if (this.rastreadorReceiver != null) {
				unregisterReceiver(this.rastreadorReceiver);
				rastreadorReceiver = null;
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
		super.onDestroy();
	}

	@Override
	public void finish() {

		try {
			if (this.sincronizadorReceiver != null) {
				unregisterReceiver(this.sincronizadorReceiver);
				sincronizadorReceiver = null;
			}
			if (this.rastreadorReceiver != null) {
				unregisterReceiver(this.rastreadorReceiver);
				rastreadorReceiver = null;
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
		try {
			super.finish();
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}

	}

	private class BarraDireitaListener implements OnLongClickListener {

		@Override
		public boolean onLongClick(View v) {
			final Vibrator vibe = (Vibrator) MenuActivityMobile.this
					.getSystemService(Context.VIBRATOR_SERVICE);
			vibe.vibrate(100);
			Intent intent = null;

			intent = new Intent(MenuActivityMobile.this,
					SettingActivityMobile.class);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);

			MenuActivityMobile.this.startActivity(intent);
			return true;

		}
	}

	public class CelulaOnClickListener implements OnClickListener {

		int i, j;

		public CelulaOnClickListener(int i, int j) {
			this.i = i;
			this.j = j;

		}

		public void onClick(View v) {
			ItemMenu item = matrizItemMenu[i][j];
			switch (item.tipoPermissao) {
			case ACESSO_A_FUNCIONALIDADE_DO_SISTEMA:
				SingletonLog.insereErro(new Exception(
						"Menu nuo deveria estar aqui: " + item.id + "@"
								+ item.tag), TIPO.PAGINA);
				break;
			case ACESSO_A_TELA:
				new LoaderRunnable(MenuActivityMobile.this, item.classe,
						item.tag).execute();
				break;
			case MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS:
				itens = controlador.avancaParaAPagina(item.id);

				inicializaAbas();
				abaAtual = 0;
				desenhaMatriz(false, 0);
				break;
			case MENU_COM_ICONE_DOS_FILHOS_DIFERENCIADA:
				itens = controlador.avancaParaAPagina(item.id);

				inicializaAbas();
				abaAtual = 0;
				desenhaMatriz(false, 0);
				break;
			default:
				break;
			}

		}

	}

	public class AbaOnClickListener implements OnClickListener {

		int aba;

		public AbaOnClickListener(int aba) {
			this.aba = aba;

		}

		public void onClick(View v) {
			desenhaMatriz(false, aba);

		}

	}

	public class AtualizaBackgroundView implements Runnable {

		private Drawable drawable;
		private View v;

		public AtualizaBackgroundView(Drawable drawable, View v) {
			this.drawable = drawable;
			this.v = v;
		}

		@Override
		public void run() {
			v.setBackgroundDrawable(drawable);

		}
	}

	private class AtualizaImageDrawable implements Runnable {

		private Drawable drawable;
		private ImageButton v;

		public AtualizaImageDrawable(Drawable drawable, ImageButton v) {
			this.drawable = drawable;
			this.v = v;
		}

		@Override
		public void run() {
			v.setImageDrawable(drawable);

		}
	}

	private class RastreadorReceiver extends BroadcastReceiver {

		//public static final String TAG = "RastreadorReceiver";

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				Drawable d = context.getResources().getDrawable(
						ESTADO_RASTREADOR.getEstadoAtual().getDrawable());
				runOnUiThread(new AtualizaImageDrawable(d, ivRastreador));
			}

		}
	}

	private class SincronizadorReceiver extends BroadcastReceiver {

		//public static final String TAG = "SincronizadorReceiver";

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				if (intent
						.hasExtra(OmegaConfiguration.SEARCH_FIELD_ESTADO_SINCRONIZADOR)) {
					int intEstado = intent
							.getIntExtra(
									OmegaConfiguration.SEARCH_FIELD_ESTADO_SINCRONIZADOR,
									-1);
					if (intEstado > 0) {
						ESTADO_SINCRONIZADOR estadoSincronizador = ESTADO_SINCRONIZADOR
								.getEstado(intEstado);
						Drawable d = context.getResources().getDrawable(
								estadoSincronizador.getDrawable());
						runOnUiThread(new AtualizaImageDrawable(d,
								ivSincronizador));
					}
				}
			}

		}
	}

	

	private void setTimerNotificacao(){
	//Starts reporter timer
		if(this.notificacaoTimer != null){
			this.notificacaoTimer.cancel();
			this.notificacaoTimer = null;
		}
		this.notificacaoTimer = new Timer();
	this.notificacaoTimer.scheduleAtFixedRate(new TimerTask() {
		@Override
		public void run() {
			handlerNotificacao.sendEmptyMessage(0);
			//sendBroadCastToVeiculoMapActivity();
		}
	},
	OmegaConfiguration.SERVICE_NOTIFICACAO_INITIAL_DELAY,
	OmegaConfiguration.SERVICE_NOTIFICACAO_TIMER_INTERVAL);
}

	public class NotificacaoReceiver extends BroadcastReceiver {

		public static final String TAG = "SincronizadorReceiver";

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				if (intent
						.hasExtra(OmegaConfiguration.SEARCH_FIELD_ESTADO_SINCRONIZADOR)) {
					int intEstado = intent
							.getIntExtra(
									OmegaConfiguration.SEARCH_FIELD_ESTADO_SINCRONIZADOR,
									-1);
					if (intEstado > 0) {
						ESTADO_SINCRONIZADOR estadoSincronizador = ESTADO_SINCRONIZADOR
								.getEstado(intEstado);
						Drawable d = context.getResources().getDrawable(
								estadoSincronizador.getDrawable());
						runOnUiThread(new AtualizaImageDrawable(d,
								ivSincronizador));
					}
				}
			}

		}
	}


	public class ReceiverBroadcastPainelUsuario extends BroadcastReceiver {
		
	
		public ReceiverBroadcastPainelUsuario(Activity pActivity){
			IntentFilter uiUpdateIntentFilter = new IntentFilter(OmegaConfiguration.REFRESH_SERVICE_PAINEL_USUARIO);
			try{
				pActivity.registerReceiver(this, uiUpdateIntentFilter);	
			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.SERVICO_ANDROID);
			}
		}

		@Override
		public void onReceive(Context arg0, Intent intent) {
			

			if ( intent.getAction().equals(OmegaConfiguration.REFRESH_SERVICE_PAINEL_USUARIO)){

				//Bundle vParameter = intent.getExtras();

				//String idVeiculoUsuario = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_DIRIGIR_VEICULO);
				
				handlerAtualizaUsuarioLogado.sendEmptyMessage(0);
				startService(new Intent(MenuActivityMobile.this, UpdateService.class));
			}
		}
	}
	
}
