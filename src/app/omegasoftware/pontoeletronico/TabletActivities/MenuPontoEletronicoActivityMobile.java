package app.omegasoftware.pontoeletronico.TabletActivities;


import java.util.List;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoPonto;
import app.omegasoftware.pontoeletronico.gpsnovo.ContainerLocalizacao;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSControler;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSServiceLocationListener;

public class MenuPontoEletronicoActivityMobile extends OmegaRegularActivity {
	private TextView txtResult;  // Reference to EditText of result

	private String inStr = "";  // Current input string
	// Previous operator: '+', '-', '*', '/', '=' or ' ' (no operator)
	Database db=null;
	EXTDAOPessoa objPessoa;	
	//Search mode toggle buttons
	private ToggleButton entradaButton;
	private ToggleButton saidaButton;
	private ToggleButton vetorTipoPontoButton[] = new ToggleButton[3];
	private int tipoPonto;
//	private Button microphoneButton;
	boolean isEntrada = false;
	boolean isToTakePicture = false;
	String idEmpresa;
	String nomeEmpresa;
	
	
	private String PREFIXO_NAO_SINCRONIZADO = null; 
	public static final int FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA = 988;
	public static final int FORM_ERROR_DIALOG_GPS_FORA_DE_AREA = 989;
	//Constants
	public static final String TAG = "MenuPontoEletronicoActi vityMobile";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		//Log.d(TAG, "onCreate(): Activity created");
		setContentView(R.layout.menu_ponto_eletronico_activity_mobile_layout);
		PREFIXO_NAO_SINCRONIZADO = this.getString(R.string.prefixoNaoSincronizado);
		Bundle vParameter = getIntent().getExtras();
		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID))
			idEmpresa =vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		
		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {

		return true;	
	}

	@Override
	public void initializeComponents() {
		try{
		db = new DatabasePontoEletronico(this);
		objPessoa = new EXTDAOPessoa(db);
		
		EXTDAOEmpresa vObjEmpresa = new EXTDAOEmpresa(db);
		vObjEmpresa.setAttrValue(EXTDAOEmpresa.ID, idEmpresa);
		if(vObjEmpresa.select()){
			nomeEmpresa =vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.NOME); 
		}
		// Retrieve a reference to the EditText field for displaying the result.
		txtResult = (EditText)findViewById(R.id.txtResultId);
		txtResult.setText("");
		
		
		vetorTipoPontoButton[0] = (ToggleButton) findViewById(R.id.cafe_button);
		vetorTipoPontoButton[0].setOnCheckedChangeListener(new TipoPontoTypeChangeListener(0));
		
		vetorTipoPontoButton[1] = (ToggleButton) findViewById(R.id.almoco_button);
		vetorTipoPontoButton[1].setOnCheckedChangeListener(new TipoPontoTypeChangeListener(1));
		
		vetorTipoPontoButton[2] = (ToggleButton) findViewById(R.id.turno_button);
		vetorTipoPontoButton[2].setOnCheckedChangeListener(new TipoPontoTypeChangeListener(2));
		
		vetorTipoPontoButton[2].setChecked(true);
		//Activity listener
		CustomServiceListener listener = new CustomServiceListener();
		//Attach medicSearchButton to MedicSearchTypeChangeListener()
		this.entradaButton = (ToggleButton) findViewById(R.id.entrada_button);
		this.entradaButton.setOnCheckedChangeListener(new EntradaTypeChangeListener());

		//Attach placeSearchButton to PlaceSearchTypeChangeListener()
		this.saidaButton = (ToggleButton) findViewById(R.id.saida_button);
		this.saidaButton.setOnCheckedChangeListener(new SaidaTypeChangeListener());
		
		
		
		this.entradaButton.setChecked(true);
		//			this.microphoneButton = (Button) findViewById(R.id.microphone_button);
		//			this.microphoneButton.setOnCheckedChangeListener(new MicrophoneTypeChangeListener());

		// Register listener (this class) for all the buttons
		((Button)findViewById(R.id.btnNum0Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum1Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum2Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum3Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum4Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum5Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum6Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum7Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum8Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnNum9Id)).setOnClickListener(listener);
		((Button)findViewById(R.id.cadastrar_button)).setOnClickListener(listener);
		((Button)findViewById(R.id.backspace_button)).setOnClickListener(listener);
		((Button)findViewById(R.id.apagar_tudo_button)).setOnClickListener(listener);
		((Button)findViewById(R.id.btnTeclaNaoSincronizado)).setOnClickListener(listener);
		
		db.close();
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);

		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		try{
			switch (id) {
			case FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA:
				vTextView.setText(getResources().getString(R.string.ponto_pessoa_inexistente_na_empresa));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA);
						
					}
				});
				break;
			case FORM_ERROR_DIALOG_GPS_FORA_DE_AREA:
				vTextView.setText(getResources().getString(R.string.gps_fora_de_area));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_GPS_FORA_DE_AREA);
						
					}
				});
				break;	
				
				
			default:
				//			vDialog = this.getErrorDialog(id);
				return null;
			}
		} catch(Exception ex){
			return null;
		}
		return vDialog;

	}


	private class TipoPontoTypeChangeListener implements OnCheckedChangeListener 
	{
		int index;
		public TipoPontoTypeChangeListener(int pIndex){
			index = pIndex;
		}
		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
			
			if(isChecked)
			{
				for(int i = 0; i < vetorTipoPontoButton.length; i++){
					if(i == index) continue;
					else{
						vetorTipoPontoButton[i].setChecked(false);
						vetorTipoPontoButton[i].setTextColor(R.color.gray_215);		
					}
				}
				tipoPonto = index;
				isEntrada = true;
				vetorTipoPontoButton[index].setTextColor(R.color.white);
			}
		}

	}
	
	private class EntradaTypeChangeListener implements OnCheckedChangeListener 
	{

		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
			
			if(isChecked)
			{
				saidaButton.setChecked(false);
				saidaButton.setTextColor(R.color.gray_215);
				isEntrada = true;

				entradaButton.setTextColor(R.color.white);			
			}
			else
			{
				saidaButton.setChecked(true);
			}
		}

	}



	private class SaidaTypeChangeListener implements OnCheckedChangeListener
	{

		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
			if(isChecked)
			{
				entradaButton.setChecked(false);
				entradaButton.setTextColor(R.color.gray_215);

				saidaButton.setTextColor(R.color.white);
				isEntrada = false;

			}
			else
			{
				entradaButton.setChecked(true);
			}
		}

	}



	protected class MicrophoneTypeChangeListener implements OnClickListener
	{
		public void onClick(View view) {
			PackageManager pm = getPackageManager();
			@SuppressWarnings("rawtypes")
			List activities = pm.queryIntentActivities(
					new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
			if (activities.size() != 0) {

			}
//
//			microphoneButton.setEnabled(false);
//			microphoneButton.setText("Recognizer not present");

		}

	}
	
	private String getTextDescritivo(String pIdPessoa){
		try{
			
			if(pIdPessoa == null || pIdPessoa.length() == 0 ) return null;
			
			objPessoa.clearData();
			objPessoa.setAttrValue(EXTDAOPessoa.ID, pIdPessoa);
			objPessoa.formatToSQLite();
			Table vTuplaPessoa =  objPessoa.getFirstTupla();
			if(vTuplaPessoa != null){
				String vEntradaOuSaida = "";
				if(isEntrada)
					vEntradaOuSaida = getResources().getString(R.string.ponto_entrada);
				else vEntradaOuSaida = getResources().getString(R.string.ponto_saida);
				
				String vTipoPonto = "";
				switch(vetorTipoPontoButton[tipoPonto].getId()){
					case R.id.almoco_button:
						
						vTipoPonto = getResources().getString(R.string.ponto_almoco);
						break;
					case R.id.turno_button:
						vTipoPonto = getResources().getString(R.string.ponto_turno);
						break;
					case R.id.cafe_button:
						vTipoPonto = getResources().getString(R.string.ponto_cafe);
						break;
				}
				
				String vSufixoTexto = getResources().getString(R.string.confirmar_marca_ponto);
				
				String vSufixoTexto2 =getResources().getString(R.string.confirmar_marca_ponto_pessoa);
				
				String pNomePessoa = vTuplaPessoa.getStrValueOfAttribute(EXTDAOPessoa.NOME);
				
				if(pNomePessoa == null)
					pNomePessoa = "'sem nome cadastrado'";
				return vSufixoTexto + " " + vEntradaOuSaida.toUpperCase() + " no " + vTipoPonto.toUpperCase() + " na empresa " + nomeEmpresa.toUpperCase() + " da " + vSufixoTexto2 + " " + pNomePessoa.toUpperCase() + "?";
			} return null;
		}catch(Exception ex){
			db.close();
			
		} finally{
			db.close();
		}
		return null;
		
	}

	private class CustomServiceListener implements OnClickListener
	{


		// On-click event handler for all the buttons
		
		
		public void onClick(View view) {
			int vStartCursor = 0;
			int vEndCursor = 0;
			switch (view.getId()) {
			case R.id.btnTeclaNaoSincronizado:
				if(inStr.length() > 1){
					if(inStr.charAt(0)==MenuPontoEletronicoActivityMobile.this.PREFIXO_NAO_SINCRONIZADO.charAt(0) )
						break;
				}
				inStr = MenuPontoEletronicoActivityMobile.this.PREFIXO_NAO_SINCRONIZADO + inStr;
				txtResult.setText(inStr);
				break;
			// Number buttons: '0' to '9'
			case R.id.btnNum0Id:
			case R.id.btnNum1Id:
			case R.id.btnNum2Id:
			case R.id.btnNum3Id:
			case R.id.btnNum4Id:
			case R.id.btnNum5Id:
			case R.id.btnNum6Id:
			case R.id.btnNum7Id:
			case R.id.btnNum8Id:
			
			case R.id.btnNum9Id:
				String inDigit = ((Button)view).getText().toString();
				vStartCursor = txtResult.getSelectionStart();
				vEndCursor = txtResult.getSelectionEnd();
				if(vStartCursor != vEndCursor){
					String vInfText = inStr.substring(0, vStartCursor);
					String vSupText = inStr.substring( vEndCursor) ;
					inStr = vInfText + vSupText;
//						Atualizando o ponteiro para a nova posicao
					vStartCursor = vEndCursor - (vEndCursor - vStartCursor);
				}
				if(vStartCursor == inStr.length()){
					inStr += inDigit;
					txtResult.append(inDigit);
//						txtResult.setText(inStr);	
				}
				else{						
					String vInfText = inStr.substring(0, vStartCursor) + inDigit;
					String vSupText = inStr.substring(vStartCursor);
					
					txtResult.setText("");
					txtResult.append(vInfText);
					inStr = vInfText + vSupText;   // accumulate input digit
					txtResult.setTextKeepState(inStr);
				}
				
				break;

				// Operator buttons: '+', '-', '*', '/' and '='
			case R.id.cadastrar_button:
				
				String vTextConfirmacao = getTextDescritivo(txtResult.getText().toString());
				
				if(!GPSServiceLocationListener.isActiveGPS())
					showDialog(FORM_ERROR_DIALOG_GPS_FORA_DE_AREA);
				else if(vTextConfirmacao == null || vTextConfirmacao.length() == 0 )
					showDialog(FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA);
				else{
//					Intent vNewIntent = new Intent(getApplicationContext(), FormConfirmacaoActivityMobile.class);
//					vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_MESSAGE, vTextConfirmacao);
//					startActivityForResult(vNewIntent, OmegaConfiguration.STATE_FORM_CONFIRMACAO_PONTO);
					
					DialogInterface.OnClickListener dialogConfirmacaoPontoClickListener = new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog, int which) {
					        switch (which){
					        case DialogInterface.BUTTON_POSITIVE:
					        	procedimentoConfirmacaoPonto();
					            break;

					        case DialogInterface.BUTTON_NEGATIVE:
					        	
					            break;
					        }
					    }
					};
					
					FactoryAlertDialog.showAlertDialog(
						MenuPontoEletronicoActivityMobile.this, 
						vTextConfirmacao,
						dialogConfirmacaoPontoClickListener);
				}
				break;
			case R.id.backspace_button:
				vStartCursor = txtResult.getSelectionStart();
				vEndCursor = txtResult.getSelectionEnd();
				if(vStartCursor != vEndCursor){
					String vInfText = inStr.substring(0, vStartCursor);
					String vSupText = inStr.substring( vEndCursor) ;
					inStr = vInfText + vSupText;
//					Atualizando o ponteiro para a nova posicao
					vStartCursor = vEndCursor - (vEndCursor - vStartCursor);
				}
				else if(vStartCursor == inStr.length() && inStr.length() > 0){
					inStr = inStr.substring(0, inStr.length() - 1);
					txtResult.setText("");
					txtResult.append(inStr);
//					txtResult.setText(inStr);	
				}
				else if(vStartCursor > 0){
					String vInfText = inStr.substring(0, vStartCursor - 1);
					String vSupText = inStr.substring(vStartCursor);
					txtResult.setText("");
					txtResult.append(vInfText);
					inStr = vInfText + vSupText;   // accumulate input digit
					txtResult.setTextKeepState(inStr);
				}
				break;

				// Clear button
			case R.id.apagar_tudo_button:   

				inStr = "";
				txtResult.setText("");
				break;
			}
		}
	}
	
	private void procedimentoConfirmacaoPonto(){
		int vLatitude = 0;
		int vLongitude = 0;
		ContainerLocalizacao vContainerLocalizacao = GPSControler.getLocalizacao();
		vLatitude = vContainerLocalizacao.getLatitude();
		vLongitude = vContainerLocalizacao.getLongitude();
		
		
		Intent vNewIntent = new Intent(getApplicationContext(), MarcaPontoActivityMobile.class);
		vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA, txtResult.getText().toString());
		vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, idEmpresa);
		vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_EMPRESA, nomeEmpresa);
		vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_ENTRADA, isEntrada);
		vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_TIPO_PONTO, EXTDAOTipoPonto.getTipoPontoDoIdToggleButton(vetorTipoPontoButton[tipoPonto].getId()));
		vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_LATITUDE, vLatitude);
		vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_LONGITUDE, vLongitude);
		
		startActivityForResult(vNewIntent, OmegaConfiguration.STATE_FORM_MARCA_PONTO);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		switch (requestCode) {
		case OmegaConfiguration.STATE_FORM_CONFIRMACAO_PONTO:
			if(!GPSControler.hasLocation()){
				showDialog(FORM_ERROR_DIALOG_GPS_FORA_DE_AREA);
			}
			else if (resultCode == RESULT_OK) {
				Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO , false);
				if(vConfirmacao){
					procedimentoConfirmacaoPonto();
				}
			}
			break;
		case OmegaConfiguration.STATE_FORM_MARCA_PONTO:
			inStr = "";
			txtResult.setText("");
			break;
		default:
			break;
		}
	}

}
