package app.omegasoftware.pontoeletronico.TabletActivities;


//import java.util.ArrayList;
//import java.util.Timer;
//import java.util.TimerTask;
//
//import android.app.Activity;
//import android.app.Dialog;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Matrix;
//import android.hardware.Camera;
//import android.hardware.Camera.CameraInfo;
//import android.hardware.Camera.PictureCallback;
//import android.hardware.Camera.ShutterCallback;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.Window;
//import android.widget.Button;
//import android.widget.CheckBox;
//import android.widget.CompoundButton;
//import android.widget.CompoundButton.OnCheckedChangeListener;
//import android.widget.EditText;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import app.omegasoftware.pontoeletronico.R;
//import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPessoaEmpresaActivityMobile;
//import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListPessoaEmpresaActivityMobile;
//import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
//import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
//import app.omegasoftware.pontoeletronico.cam.HelperCamera;
//import app.omegasoftware.pontoeletronico.cam.Preview;
//import app.omegasoftware.pontoeletronico.common.Adapter.PessoaPontoAdapter;
//import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
//import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
//import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
//import app.omegasoftware.pontoeletronico.database.Database;
//import app.omegasoftware.pontoeletronico.database.Table;
//import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
//import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
//import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
//import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
//import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto;
//import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;
//import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoPonto;
//import app.omegasoftware.pontoeletronico.date.HelperDate;
//import app.omegasoftware.pontoeletronico.file.HelperFile;
//import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
//import app.omegasoftware.pontoeletronico.gpsnovo.ContainerLocalizacao;
//import app.omegasoftware.pontoeletronico.gpsnovo.GPSControler;
//import app.omegasoftware.pontoeletronico.gpsnovo.GPSServiceLocationListener;
//import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class MenuPontoEletronicoComFotoActivityMobile {
//public class MenuPontoEletronicoComFotoActivityMobile extends OmegaRegularActivity {
//	private TextView txtResult;  // Reference to EditText of result
//
//	private String inStr = "";  // Current input string
//	// Previous operator: '+', '-', '*', '/', '=' or ' ' (no operator)
//	Database db=null;
//	EXTDAOPessoa objPessoa;
//	EXTDAOProfissao objProfissao;
//	//Search mode toggle buttons
//	private CheckBox entradaButton;
//	private CheckBox saidaButton;
//	private CheckBox turnoButton;
//	private CheckBox intervaloButton;
//
//	private String PREFIXO_NAO_SINCRONIZADO = null;
//
////	private Button microphoneButton;
//	boolean isEntrada = true;
//	boolean isTurno = true;
//	boolean isToTakePicture = false;
//	String idEmpresa;
//	String idPessoaEmpresa;
//	String empresa;
//	String pessoa;
//	String idProfissao;
//	String idPessoa;
//	String profissao;
//	Button btTemporizador;
//	Button btSelecionarPessoa;
//	LinearLayout llCamera;
//	ImageView ivFoto;
//	HandlerFoto handlerFoto;
//	HandlerTeclado handlerTeclado;
//	HandlerContagem handlerContagem;
//	Bitmap ultimaFoto;
//	String ultimoIdPonto;
//	String dataDoUltimoPonto;
//	String horaDoUltimoPonto;
//	String ultimoArquivoDeFoto;
//	String ultimaDescricaoPonto;
//	View itemListPontoPessoa;
//	LinearLayout llTeclado;
//	LinearLayout flFoto;
//	LinearLayout llControleCamera;
//	Button baterPonto;
//	Button btTrocarCamera;
//	Preview preview;
//	Timer uiUpdaterTimer;
//
//	FrameLayout flPreview;
//	TextView tvRelogio;
//	TextView tvContagem;
//	TextView tvEmpresa;
//	LinearLayout llTipoPontoEletronico;
//	private Timer timerRelogio = null;
//	private Timer timerContagem = new Timer();
//
//	private Timer uiTimerToFinish = new Timer();
//	public enum TIPO{
//		BATER_PONTO_PARA_UMA_PESSOA_EMPRESA_ESPECIFICA,
//		BATER_PONTO_PARA_UMA_EMPRESA
//	}
//	public enum ESTADO_BATER_PONTO_PARA_UMA_EMPRESA{
//		//PASSO 1
//		TECLADO_ATIVO,
//		//PASSO 2
//		TIRANDO_FOTO
//	}
//
//	ESTADO_BATER_PONTO_PARA_UMA_EMPRESA estadoBaterPontoParaUmaEmpresa = ESTADO_BATER_PONTO_PARA_UMA_EMPRESA.TECLADO_ATIVO;
//
//	public TIPO tipo ;
//
//	public static final int FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA = 988;
//	public static final int FORM_ERROR_DIALOG_GPS_FORA_DE_AREA = 989;
//	//Constants
//	public static final String TAG = "MenuPontoEletronicoComFotoActivityMobile";
//	HandlerRelogio handlerRelogio;
//	HandlerTrocarCamera handlerTrocarCamera;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
//		super.onCreate(savedInstanceState);
//
//		setContentView(R.layout.menu_ponto_eletronico_activity_mobile_layout);
//		PREFIXO_NAO_SINCRONIZADO = this.getString(R.string.prefixoNaoSincronizado);
//
//		Bundle vParameter = getIntent().getExtras();
//		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA)){
//			idEmpresa =vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA);
//			tipo = TIPO.BATER_PONTO_PARA_UMA_EMPRESA;
//		}
//
//		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA_EMPRESA)){
//			idPessoaEmpresa = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA_EMPRESA);
//			tipo = TIPO.BATER_PONTO_PARA_UMA_PESSOA_EMPRESA_ESPECIFICA;
//		}
//
//		tvRelogio = (TextView)  findViewById(R.id.tv_relogio);
//		tvRelogio.setText(HelperDate.getDatetimeAtualFormatadaParaExibicao(this));
//
//		new CustomDataLoader(this).execute();
//	}
//	public void inicializaComponentesCamera(){
//		((Button) findViewById(R.id.btnBuscarId)).setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//				Intent intent = new Intent(MenuPontoEletronicoComFotoActivityMobile.this, ListPessoaEmpresaActivityMobile.class);
//				//intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, idEmpresa);
//				startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FILTER_PESSOA_EMPRESA);
//			}
//		});
//
//		if(flPreview == null){
//			flPreview =((FrameLayout) findViewById(R.id.camera_preview_surface));
//			llControleCamera = ((LinearLayout)findViewById(R.id.ll_controle_camera));
//			btTrocarCamera =  (Button)findViewById(R.id.bt_trocar_camera);
//			llCamera = (LinearLayout)findViewById(R.id.ll_camera);
//			tvContagem = (TextView) findViewById(R.id.tv_contagem);
//
//		}
//		if(tipoCamera == null){
//			tipoCamera = HelperCamera.existeCameraDaFrente() ?  CameraInfo.CAMERA_FACING_FRONT : CameraInfo.CAMERA_FACING_BACK;
//		}
//	}
//	public void inicializaCamera(){
//
//		inicializaComponentesCamera();
//
//
//		if(preview != null)
//			return;
//
//
//		preview = new Preview(MenuPontoEletronicoComFotoActivityMobile.this, flPreview, tipoCamera, false);
//		flPreview.addView(preview);
//
//		llControleCamera.bringToFront();
//		tvContagem.setVisibility(View.GONE);
//		flPreview.setVisibility(View.VISIBLE);
//		llCamera.setVisibility(View.VISIBLE);
//		if(preview.possuiOutraCamera()){
//
//			btTrocarCamera.setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View v) {
//					handlerTrocarCamera.sendEmptyMessage(0);
//				}
//			});
//		} else {
//			btTrocarCamera.setVisibility(View.GONE);
//		}
//	}
//
//	@Override
//	public boolean loadData() {
//
//		return true;
//	}
//
//	public void refreshPessoaPonto(){
//
//		if(idPessoaEmpresa != null){
//			EXTDAOPonto objPonto = new EXTDAOPonto(db);
//			EXTDAOPonto.PontoPessoa pontoPessoa = objPonto.getUltimoPontoDaPessoaNaEmpresa(idPessoaEmpresa, idPessoa, idEmpresa, idProfissao);
//			if(pontoPessoa == null)
//				pontoPessoa = objPonto.factoryPontoPessoa(idPessoaEmpresa);
//
//			pontoPessoa.pessoa = pessoa;
//			pontoPessoa.empresa = empresa;
//			pontoPessoa.profissao = profissao;
//			pontoPessoa.idPessoa = idPessoa;
//			pontoPessoa.idEmpresa = idEmpresa;
//			pontoPessoa.idProfissao = idProfissao;
//			pontoPessoa.idPessoaEmpresa = idPessoaEmpresa;
//
//			PessoaPontoAdapter adapter = new PessoaPontoAdapter(
//					MenuPontoEletronicoComFotoActivityMobile.this,
//					pontoPessoa);
//
//			adapter.formatarView(itemListPontoPessoa);
//			itemListPontoPessoa.setVisibility(View.VISIBLE);
//			if(pontoPessoa != null){
//				if(pontoPessoa.intervalo != null && pontoPessoa.intervalo.saida == null){
//					//a pessoa estara voltando de um intervalo
//					intervaloButton.setChecked(true);
//					saidaButton.setChecked(true);
//				} else if(pontoPessoa.turno != null && pontoPessoa.turno.saida == null){
//					turnoButton.setChecked(true);
//					saidaButton.setChecked(true);
//				}else if(pontoPessoa.turno != null && pontoPessoa.turno.saida != null){
//					turnoButton.setChecked(true);
//					entradaButton.setChecked(true);
//				}else if(pontoPessoa.turno == null ){
//					turnoButton.setChecked(true);
//					entradaButton.setChecked(true);
//				}
//			}
//
//		} else {
//
//			itemListPontoPessoa.setVisibility(View.GONE);
//		}
//
//	}
//
//	public void refreshHeaderSecundario(){
//		View headerSecundario = findViewById(R.id.header);
//		if(headerSecundario != null){
//			View viewTitle = headerSecundario.findViewById(R.id.tv_title);
//			if(viewTitle != null){
//				TextView tvTitle = (TextView) viewTitle;
//				if(tipo == TIPO.BATER_PONTO_PARA_UMA_EMPRESA)
//					tvTitle.setText(empresa);
//				else if(tipo == TIPO.BATER_PONTO_PARA_UMA_PESSOA_EMPRESA_ESPECIFICA)
//					tvTitle.setText(pessoa);
//			}
//		}
//	}
//
//	@Override
//	public void initializeComponents() {
//		try{
//		CustomServiceListener listener = new CustomServiceListener();
//		inicializaComponentesCamera();
//		db = new DatabasePontoEletronico(this);
//		objPessoa = new EXTDAOPessoa(db);
//		objProfissao = new EXTDAOProfissao(db);
//		handlerFoto = new HandlerFoto();
//		handlerTeclado = new HandlerTeclado();
//		handlerContagem = new HandlerContagem(3);
//		handlerRelogio = new HandlerRelogio();
//		handlerTrocarCamera = new HandlerTrocarCamera();
//
//		btSelecionarPessoa = (Button)findViewById(R.id.selecionar_pessoa_button);
//		btSelecionarPessoa.setOnClickListener(listener);
//
//
//		itemListPontoPessoa = findViewById(R.id.item_list_ponto_pessoa);
//
//		turnoButton = (CheckBox)findViewById(R.id.turno_button);
//		intervaloButton = (CheckBox)findViewById(R.id.intervalo_button);
//
//		setTimerRelogio();
//		llTipoPontoEletronico = (LinearLayout)findViewById(R.id.ll_tipo_ponto_eletronico);
//
//
//		llTeclado = (LinearLayout)findViewById(R.id.ll_teclado);
//		if(tipo == TIPO.BATER_PONTO_PARA_UMA_PESSOA_EMPRESA_ESPECIFICA){
//			Database db = new DatabasePontoEletronico(this);
//			EXTDAOPessoaEmpresa objPessoaEmpresa= new EXTDAOPessoaEmpresa(db);
//			objPessoaEmpresa.select(idPessoaEmpresa);
//			objProfissao = objPessoaEmpresa.getObjProfissao();
//			objPessoa = objPessoaEmpresa.getObjPessoa();
//			profissao = objProfissao.getStrValueOfAttribute(EXTDAOProfissao.NOME);
//			idProfissao = objProfissao.getId();
//			idPessoa = objPessoa.getId();
//			idEmpresa = objPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.EMPRESA_ID_INT);
//
//			pessoa = objPessoa.getStrValueOfAttribute(EXTDAOPessoa.NOME);
//			if(pessoa == null || pessoa.length() == 0)
//				pessoa = objPessoa.getStrValueOfAttribute(EXTDAOPessoa.EMAIL);
//
//
//			llTeclado.setVisibility(View.GONE);
//			inicializaCamera();
//		} else {
//
//			llTeclado.setVisibility(View.VISIBLE);
//			releaseCamera();
//		}
//
//
//		EXTDAOEmpresa vObjEmpresa = new EXTDAOEmpresa(db);
//		vObjEmpresa.setAttrValue(EXTDAOEmpresa.ID, idEmpresa);
//		if(vObjEmpresa.select()){
//			empresa =vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.NOME);
//		}
//
//
//
//		// Retrieve a reference to the EditText field for displaying the result.
//		txtResult = (EditText)findViewById(R.id.txtResultId);
//		txtResult.setText("");
//
//		turnoButton.setOnCheckedChangeListener(new TurnoTypeChangeListener());
//		intervaloButton.setOnCheckedChangeListener(new IntervaloTypeChangeListener());
//
//
//		//Attach medicSearchButton to MedicSearchTypeChangeListener()
//		entradaButton = (CheckBox) findViewById(R.id.entrada_button);
//		entradaButton.setOnCheckedChangeListener(new EntradaTypeChangeListener());
//
//		//Attach placeSearchButton to PlaceSearchTypeChangeListener()
//		saidaButton = (CheckBox) findViewById(R.id.saida_button);
//		saidaButton.setOnCheckedChangeListener(new SaidaTypeChangeListener());
//
//
//		turnoButton.setChecked(true);
//		entradaButton.setChecked(true);
//
//
//		refreshPessoaPonto();
//		btTemporizador = ((Button)findViewById(R.id.bt_temporizador));
//		ivFoto = ((ImageView)findViewById(R.id.iv_foto));
//		flFoto = ((LinearLayout)findViewById(R.id.ll_foto));
//
//
//		btTemporizador.setOnClickListener(listener);
//		((Button)findViewById(R.id.btnNum0Id)).setOnClickListener(listener);
//		((Button)findViewById(R.id.btnNum1Id)).setOnClickListener(listener);
//		((Button)findViewById(R.id.btnNum2Id)).setOnClickListener(listener);
//		((Button)findViewById(R.id.btnNum3Id)).setOnClickListener(listener);
//		((Button)findViewById(R.id.btnNum4Id)).setOnClickListener(listener);
//		((Button)findViewById(R.id.btnNum5Id)).setOnClickListener(listener);
//		((Button)findViewById(R.id.btnNum6Id)).setOnClickListener(listener);
//		((Button)findViewById(R.id.btnNum7Id)).setOnClickListener(listener);
//		((Button)findViewById(R.id.btnNum8Id)).setOnClickListener(listener);
//		((Button)findViewById(R.id.btnNum9Id)).setOnClickListener(listener);
//		((Button)findViewById(R.id.btnTeclaNaoSincronizado)).setOnClickListener(listener);
//
//		((Button)findViewById(R.id.bt_bater_ponto)).setOnClickListener(listener);
//		((Button)findViewById(R.id.backspace_button)).setOnClickListener(listener);
//		((Button)findViewById(R.id.apagar_tudo_button)).setOnClickListener(listener);
//
//		refreshHeaderSecundario();
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);
//
//		}
//	}
//
//
//	public class HandlerRelogio extends Handler{
//
//		@Override
//		public void handleMessage(Message msg)
//		{
//
//			tvRelogio.setText(HelperDate.getDatetimeAtualFormatadaParaExibicao(MenuPontoEletronicoComFotoActivityMobile.this));
//		}
//	}
//	public class HandlerFoto extends Handler{
//		public boolean ativo = false;
//		@Override
//		public void handleMessage(Message msg)
//		{
//
//			if(!ativo){
//				ativo = true;
//				llTipoPontoEletronico.setVisibility(View.GONE);
//				refreshPessoaPonto();
//				timerRelogio.cancel();
//				ivFoto.setVisibility(View.VISIBLE);
//				ivFoto.setImageBitmap(ultimaFoto);
//
//				flFoto.setVisibility(View.VISIBLE);
//				flPreview.setVisibility(View.GONE);
//				tvRelogio.setText(HelperDate.getDataFormatadaParaExibicao( dataDoUltimoPonto ) + " " + horaDoUltimoPonto);
//				preview.release();
//
//				setTimerToFinish();
//			} else {
//				ivFoto.setVisibility(View.GONE);
//				ivFoto.destroyDrawingCache();
//				if(ultimaFoto != null){
//					ultimaFoto.recycle();
//					ultimaFoto = null;
//				}
//
//				flFoto.setVisibility(View.GONE);
//				llTipoPontoEletronico.setVisibility(View.VISIBLE);
//				setTimerRelogio();
//				ativo = false;
//			}
//
//		}
//	}
//
//	public class HandlerTeclado extends Handler{
//
//		@Override
//		public void handleMessage(Message msg)
//		{
//			if(estadoBaterPontoParaUmaEmpresa == ESTADO_BATER_PONTO_PARA_UMA_EMPRESA.TECLADO_ATIVO){
//				refreshPessoaPonto();
//				estadoBaterPontoParaUmaEmpresa = ESTADO_BATER_PONTO_PARA_UMA_EMPRESA.TIRANDO_FOTO;
//				llTeclado.setVisibility(View.GONE);
//				inicializaCamera();
//			} else {
//				idProfissao = null;
//				profissao = null;
//				idPessoa = null;
//				pessoa = null;
//				idPessoaEmpresa = null;
//				apagarTeclado();
//				refreshPessoaPonto();
//				estadoBaterPontoParaUmaEmpresa = ESTADO_BATER_PONTO_PARA_UMA_EMPRESA.TECLADO_ATIVO;
//				llTeclado.setVisibility(View.VISIBLE);
//
//				releaseCamera();
//
//			}
//		}
//	}
//	Integer tipoCamera = null;
//
//	class HandlerContagem extends Handler{
//		boolean started = false;
//		int contSegundos;
//		boolean executando = false;
//		public HandlerContagem(int contSegundos){
//
//			this.contSegundos = contSegundos;
//			started = false;
//		}
//		@Override
//		public void handleMessage(Message msg)
//		{
//			if(!executando){
//				executando = true;
//				try{
//					if(!started){
//						tvContagem.bringToFront();
//						tvContagem.setVisibility(View.VISIBLE);
//						started = true;
//					}
//					if(this.contSegundos > 0 ){
//						tvContagem.setText(String.valueOf(this.contSegundos));
//						this.contSegundos--;
//					}else {
//						tvContagem.setText("X");
//
//						timerContagem.cancel();
//						started = false;
//						procedimentoConfirmacaoPonto();
//					}
//
//				}catch(Exception ex){
//					SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//				} finally{
//					executando = false;
//				}
//			}
//		}
//	}
//
//	public class HandlerTrocarCamera extends Handler{
//
//		public HandlerTrocarCamera(){
//
//		}
//		@Override
//		public void handleMessage(Message msg)
//		{
//
//
//			if(tipoCamera == CameraInfo.CAMERA_FACING_BACK){
//				tipoCamera = CameraInfo.CAMERA_FACING_FRONT;
//			}else {
//				tipoCamera = CameraInfo.CAMERA_FACING_BACK;
//			}
//
//			releaseCamera();
//			inicializaCamera();
//
//
//		}
//	}
//	private void releaseCamera(){
//		if(preview != null){
//			preview.release();
//			flPreview.removeView(preview);
//		}
//		if(llCamera != null)
//		llCamera.setVisibility(View.GONE);
//		preview = null;
//	}
//
//	private void setTimerContagem(){
//
//		//Starts ui updater timer
//		this.timerContagem.scheduleAtFixedRate(
//				new TimerTask() {
//
//					@Override
//					public void run() {
//						handlerContagem.sendEmptyMessage(0);
//
//					}
//				},
//				1000,
//				1000);
//	}
//	private void setTimerRelogio(){
//		this.timerRelogio = new Timer();
//		//Starts ui updater timer
//		this.timerRelogio.scheduleAtFixedRate(
//				new TimerTask() {
//
//					@Override
//					public void run() {
//						handlerRelogio.sendEmptyMessage(0);
//
//					}
//				},
//				1000,
//				1000);
//	}
//
//	private void setTimerToFinish(){
//
//		this.uiTimerToFinish.schedule(
//				new TimerTaskToFinish(this),
//				5000);
//	}
//
//	@Override
//	protected Dialog onCreateDialog(int id) {
//
//		Dialog vDialog = new Dialog(this);
//		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//
//		vDialog.setContentView(R.layout.dialog);
//
//		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
//		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);
//
//		try{
//			switch (id) {
//			case FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA:
//				vTextView.setText(getResources().getString(R.string.ponto_pessoa_inexistente_na_empresa));
//				vOkButton.setOnClickListener(new OnClickListener() {
//					public void onClick(View v) {
//						dismissDialog(FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA);
//
//					}
//				});
//				break;
//			case FORM_ERROR_DIALOG_GPS_FORA_DE_AREA:
//				vTextView.setText(getResources().getString(R.string.gps_fora_de_area));
//				vOkButton.setOnClickListener(new OnClickListener() {
//					public void onClick(View v) {
//						dismissDialog(FORM_ERROR_DIALOG_GPS_FORA_DE_AREA);
//
//					}
//				});
//				break;
//
//
//			default:
//				return super.onCreateDialog(id);
//			}
//		} catch(Exception ex){
//			SingletonLog.insereErro(ex,SingletonLog.TIPO.PAGINA);
//			return null;
//		}
//		return vDialog;
//
//	}
//
//
//
//	private class EntradaTypeChangeListener implements OnCheckedChangeListener
//	{
//
//		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
//
//			if(isChecked)
//			{
//				saidaButton.setChecked(false);
//				isEntrada = true;
//			}
//			else
//			{
//				saidaButton.setChecked(true);
//			}
//		}
//	}
//
//	private class IntervaloTypeChangeListener implements OnCheckedChangeListener
//	{
//
//		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
//
//			if(isChecked)
//			{
//				turnoButton.setChecked(false);
//				isTurno = false;
//			}
//			else
//			{
//				turnoButton.setChecked(true);
//			}
//		}
//
//	}
//	private class TurnoTypeChangeListener implements OnCheckedChangeListener
//	{
//		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
//			if(isChecked)
//			{
//				intervaloButton.setChecked(false);
//				isTurno = true;
//			}
//			else
//			{
//				intervaloButton.setChecked(true);
//			}
//		}
//	}
//	private class SaidaTypeChangeListener implements OnCheckedChangeListener
//	{
//
//		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
//			if(isChecked)
//			{
//				entradaButton.setChecked(false);
//				isEntrada = false;
//			}
//			else
//			{
//				entradaButton.setChecked(true);
//			}
//		}
//	}
//
//
//	private String getTextoDescritivo(){
//		try{
//
//				String vEntradaOuSaida = "";
//				if(isEntrada)
//					vEntradaOuSaida = getResources().getString(R.string.ponto_entrada);
//				else vEntradaOuSaida = getResources().getString(R.string.ponto_saida);
//
//				String vTipoPonto = "";
//				if(isTurno){
//					vTipoPonto = getResources().getString(R.string.ponto_turno);
//				}else{
//					vTipoPonto = getResources().getString(R.string.intervalo);
//
//				}
//
//				String vSufixoTexto = getResources().getString(R.string.confirmar_marca_ponto);
//
//				String vSufixoTexto2 =getResources().getString(R.string.confirmar_marca_ponto_pessoa);
//
//
//				return vSufixoTexto + " " + vEntradaOuSaida.toUpperCase()
//						+ " no " + vTipoPonto.toUpperCase()
//						+ " na empresa " + empresa.toUpperCase()
//						+ " da " + vSufixoTexto2 + " " + pessoa.toUpperCase() + "?";
//
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//
//		}
//		return null;
//
//	}
//
//	private class CustomServiceListener implements OnClickListener
//	{
//
//
//		// On-click event handler for all the buttons
//
//
//		public void onClick(final View view) {
//			try{
//			int vStartCursor = 0;
//			int vEndCursor = 0;
//			switch (view.getId()) {
//			case R.id.btnTeclaNaoSincronizado:
//				if(!inStr.contains(MenuPontoEletronicoComFotoActivityMobile.this.PREFIXO_NAO_SINCRONIZADO)){
//					inStr = MenuPontoEletronicoComFotoActivityMobile.this.PREFIXO_NAO_SINCRONIZADO + inStr;
//					txtResult.setText("");
//					txtResult.append(inStr);
//				} else {
//					inStr = inStr.replace(PREFIXO_NAO_SINCRONIZADO, "");
//					txtResult.setText("");
//					txtResult.append(inStr);
//				}
//
//
//
//				break;
//			// Number buttons: '0' to '9'
//			case R.id.btnNum0Id:
//			case R.id.btnNum1Id:
//			case R.id.btnNum2Id:
//			case R.id.btnNum3Id:
//			case R.id.btnNum4Id:
//			case R.id.btnNum5Id:
//			case R.id.btnNum6Id:
//			case R.id.btnNum7Id:
//			case R.id.btnNum8Id:
//			case R.id.btnNum9Id:
//				if(inStr.length() > 11)
//					break;
////				boolean naoSincronizado = false;
////				if(inStr.contains(MenuPontoEletronicoComFotoActivityMobile.this.PREFIXO_NAO_SINCRONIZADO)){
////					inStr = inStr.replace('*', '\0');
////					naoSincronizado = true;
////				}
//				String inDigit = ((Button)view).getText().toString();
//
////				String bkp = inStr;
////				boolean sincronizado = inStr.contains(MenuPontoEletronicoComFotoActivityMobile.this.PREFIXO_NAO_SINCRONIZADO);
////				inStr = inStr.replace(MenuPontoEletronicoComFotoActivityMobile.this.PREFIXO_NAO_SINCRONIZADO, "");
//				vStartCursor = txtResult.getSelectionStart();
//				vEndCursor = txtResult.getSelectionEnd();
//
//
//				if(vStartCursor != vEndCursor){
//					String vInfText = inStr.substring(0, vStartCursor);
//					String vSupText = inStr.substring( vEndCursor) ;
//					inStr = vInfText + vSupText;
////						Atualizando o ponteiro para a nova posicao
//					vStartCursor = vEndCursor - (vEndCursor - vStartCursor);
//				}
//				if(vStartCursor == inStr.length()){
//					inStr += inDigit;
////					if(naoSincronizado)
////					inStr = '*' + inStr;
//					txtResult.append(inDigit);
////						txtResult.setText(inStr);
//				}
//				else{
//					String vInfText = inStr.substring(0, vStartCursor) + inDigit;
//					String vSupText = inStr.substring(vStartCursor);
//
//					txtResult.setText("");
//					txtResult.append(vInfText);
//					inStr = vInfText + vSupText;   // accumulate input digit
//					txtResult.setTextKeepState(inStr);
//				}
//
//				break;
//
//			case R.id.selecionar_pessoa_button:
//				idPessoa = txtResult.getText().toString();
//				idPessoa = idPessoa.replace(PREFIXO_NAO_SINCRONIZADO, "-");
//				if(!objPessoa.select(idPessoa)){
//					FactoryAlertDialog.showAlertDialog(
//							MenuPontoEletronicoComFotoActivityMobile.this,
//							MenuPontoEletronicoComFotoActivityMobile.this.getString(R.string.id_nao_encontrado),
//							new DialogInterface.OnClickListener() {
//
//								@Override
//								public void onClick(DialogInterface dialog, int which) {
//									if(which == DialogInterface.BUTTON_POSITIVE){
//										Intent intent = new Intent(MenuPontoEletronicoComFotoActivityMobile.this, ListPessoaEmpresaActivityMobile.class);
//
//										intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, idEmpresa);
//										startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FILTER_PESSOA_EMPRESA);
//									}
//
//								}
//						});
//				} else {
//					ArrayList<Table> registros = objPessoa.getPessoasEmpresa(idEmpresa);
//					if(registros != null && registros.size() > 0){
//
//						Table registroPessoaEmpresa =registros.get(0);
//
//						idPessoaEmpresa = registroPessoaEmpresa.getId();
//						idProfissao= registroPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT);
//						Table registroProfissao = registroPessoaEmpresa.getObjDaChaveExtrangeira(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT);
//						if(registroProfissao != null)
//							profissao = registroProfissao.getStrValueOfAttribute(EXTDAOProfissao.NOME);
//						pessoa = objPessoa.getDescricao();
//
//						handlerTeclado.sendEmptyMessage(0);
//
//					} else {
//						FactoryAlertDialog.showAlertDialog(
//								MenuPontoEletronicoComFotoActivityMobile.this,
//								"A pessoa nuo possui nenhum cargo na empresa em que estu sendo registrado o ponto. Deseja cadastrar uma profissuo para ela?",
//								new DialogInterface.OnClickListener() {
//
//									@Override
//									public void onClick(DialogInterface dialog, int which) {
//										if(which == DialogInterface.BUTTON_POSITIVE){
//											Intent intent = new Intent(MenuPontoEletronicoComFotoActivityMobile.this, FormPessoaEmpresaActivityMobile.class);
//											intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA, idPessoa );
//											intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, idEmpresa);
//											startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_PESSOA_EMPRESA);
//										}
//									}
//							});
//					}
//				}
//				break;
//
//				// Operator buttons: '+', '-', '*', '/' and '='
//			case R.id.bt_temporizador:
//			case R.id.bt_bater_ponto:
//
//				String vTextConfirmacao = getTextoDescritivo();
//				ultimaDescricaoPonto =vTextConfirmacao ;
//				if(!GPSServiceLocationListener.isActiveGPS())
//					showDialog(FORM_ERROR_DIALOG_GPS_FORA_DE_AREA);
//				else if(vTextConfirmacao == null || vTextConfirmacao.length() == 0 )
//					showDialog(FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA);
//				else{
////					Intent vNewIntent = new Intent(getApplicationContext(), FormConfirmacaoActivityMobile.class);
////					vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_MESSAGE, vTextConfirmacao);
////					startActivityForResult(vNewIntent, OmegaConfiguration.STATE_FORM_CONFIRMACAO_PONTO);
//
//					DialogInterface.OnClickListener dialogConfirmacaoPontoClickListener = new DialogInterface.OnClickListener() {
//					    public void onClick(DialogInterface dialog, int which) {
//					        switch (which){
//					        case DialogInterface.BUTTON_POSITIVE:
//					        	if(view.getId() == R.id.bt_temporizador)
//					        		setTimerContagem();
//					        	else {
//					        		procedimentoConfirmacaoPonto();
//					        	}
//
//					            break;
//
//					        case DialogInterface.BUTTON_NEGATIVE:
//
//					            break;
//					        }
//					    }
//					};
//
//					FactoryAlertDialog.showAlertDialog(
//						MenuPontoEletronicoComFotoActivityMobile.this,
//						vTextConfirmacao,
//						dialogConfirmacaoPontoClickListener);
//				}
//				break;
//			case R.id.backspace_button:
//				if(txtResult.length() == 0) finish();
//				vStartCursor = txtResult.getSelectionStart();
//				vEndCursor = txtResult.getSelectionEnd();
//				if(vStartCursor != vEndCursor){
//					String vInfText = inStr.substring(0, vStartCursor);
//					String vSupText = inStr.substring( vEndCursor) ;
//					inStr = vInfText + vSupText;
////					Atualizando o ponteiro para a nova posicao
//					vStartCursor = vEndCursor - (vEndCursor - vStartCursor);
//				}
//				else if(vStartCursor == inStr.length() && inStr.length() > 0){
//					inStr = inStr.substring(0, inStr.length() - 1);
//					txtResult.setText("");
//					txtResult.append(inStr);
////					txtResult.setText(inStr);
//				}
//				else if(vStartCursor > 0){
//					String vInfText = inStr.substring(0, vStartCursor - 1);
//					String vSupText = inStr.substring(vStartCursor);
//					txtResult.setText("");
//					txtResult.append(vInfText);
//					inStr = vInfText + vSupText;   // accumulate input digit
//					txtResult.setTextKeepState(inStr);
//				}
//
//				break;
//
//				// Clear button
//			case R.id.apagar_tudo_button:
//
//				apagarTeclado();
//				break;
//			}
//			}catch(Exception ex){
//				SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);
//
//			}
//		}
//
//	}
//
//	private void apagarTeclado(){
//		inStr = "";
//		txtResult.setText("");
//	}
//
//	class TimerTaskToFinish extends TimerTask{
//		Activity activity;
//		public TimerTaskToFinish(Activity pActivity){
//			activity = pActivity;
//		}
//
//		@Override
//		public void run() {
//			this.cancel();
//
//
//			if(tipo == TIPO.BATER_PONTO_PARA_UMA_PESSOA_EMPRESA_ESPECIFICA){
//				finalizaActivity();
//
//			}else {
//				handlerTeclado.sendEmptyMessage(0);
//				handlerFoto.sendEmptyMessage(0);
//			}
//
//
//		}
//	}
//
//	private void finalizaActivity(){
//
//
//		Intent intent = getIntent();
//
//		if(ultimoIdPonto != null && ultimoIdPonto.length() > 0){
//			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, ultimoIdPonto);
//			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, true);
//			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, idPessoa);
//			setResult(RESULT_OK, intent);
//			finish();
//		} else {
//			setResult(RESULT_CANCELED, intent);
//			finish();
//		}
//	}
//	private void procedimentoConfirmacaoPonto(){
//
//		if(shutterCallback != null &&
//				rawCallback != null &&
//				jpegCallback != null){
//			preview.camera.takePicture(
//				shutterCallback,
//				rawCallback,
//				jpegCallback);
//		}
//	}
//	private void marcaPonto(){
//
//		EXTDAOPonto objPonto = new EXTDAOPonto(db);
//		objPonto.setAttrValue(EXTDAOPonto.PESSOA_ID_INT, idPessoa);
//		EXTDAOTipoPonto.TIPO_PONTO tipoPonto = null;
//		if(isTurno)
//			tipoPonto = EXTDAOTipoPonto.TIPO_PONTO.TURNO;
//		else
//			tipoPonto = EXTDAOTipoPonto.TIPO_PONTO.INTERVALO;
//
//		objPonto.setAttrValue(EXTDAOPonto.FOTO, ultimoArquivoDeFoto);
//
//		HelperDate helperDate = new HelperDate();
//		dataDoUltimoPonto = helperDate.getDateDisplay();
//		horaDoUltimoPonto = helperDate.getTimeDisplay();
//
//		long utc = HelperDate.getTimestampSegundosUTC(this);
//		int offset = HelperDate.getOffsetSegundosTimeZone();
//		objPonto.setAttrValue(EXTDAOPonto.DATA_SEC, String.valueOf( utc));
//		objPonto.setAttrValue(EXTDAOPonto.DATA_OFFSEC,String.valueOf( offset));
//		objPonto.setAttrValue(EXTDAOPonto.EMPRESA_ID_INT, idEmpresa);
//		objPonto.setAttrValue(EXTDAOPonto.PESSOA_ID_INT, idPessoa);
//		objPonto.setAttrValue(EXTDAOPonto.USUARIO_ID_INT, OmegaSecurity.getIdUsuario());
//		objPonto.setAttrValue(EXTDAOPonto.IS_ENTRADA_BOOLEAN, HelperString.valueOfBooleanSQL(isEntrada));
//
//		objPonto.setAttrValue(EXTDAOPonto.TIPO_PONTO_ID_INT, String.valueOf(tipoPonto.getId()));
//		int latitude = 0;
//		int longitude = 0;
//		ContainerLocalizacao vContainerLocalizacao = GPSControler.getLocalizacao();
//		if(vContainerLocalizacao != null){
//			latitude = vContainerLocalizacao.getLatitude();
//			longitude = vContainerLocalizacao.getLongitude();
//
//			objPonto.setAttrValue(EXTDAOPonto.LATITUDE_INT, String.valueOf(latitude));
//			objPonto.setAttrValue(EXTDAOPonto.LONGITUDE_INT, String.valueOf(longitude));
//
//		}
//
//		objPonto.setAttrValue(EXTDAOPonto.PROFISSAO_ID_INT, idProfissao);
//
//		objPonto.formatToSQLite();
//		objPonto.insert(true);
//		ultimoIdPonto = objPonto.getId();
//		handlerFoto.sendEmptyMessage(0);
//	}
//
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//		try{
//			switch (requestCode) {
//
//			case OmegaConfiguration.STATE_FORM_CONFIRMACAO_PONTO:
//				if(!GPSControler.hasLocation()){
//					showDialog(FORM_ERROR_DIALOG_GPS_FORA_DE_AREA);
//				}
//				else if (resultCode == RESULT_OK) {
//					Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO , false);
//					if(vConfirmacao){
//						procedimentoConfirmacaoPonto();
//					}
//				}
//				break;
//			case OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW:
//			case OmegaConfiguration.STATE_FORM_MARCA_PONTO:
//				if(resultCode == RESULT_OK){
//					ultimoIdPonto = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID);
//					handlerFoto.sendEmptyMessage(0);
//					apagarTeclado();
//				}
//
//				break;
//			default:
//				break;
//			}
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//		}
//
//	}
//
//
//	class TakePictureTimer extends TimerTask{
//		Activity activity;
//		public TakePictureTimer(Activity pActivity){
//			activity = pActivity;
//		}
//
//		@Override
//		public void run() {
//			procedimentoConfirmacaoPonto();
//			this.cancel();
//		}
//	}
//
//	ShutterCallback shutterCallback = new ShutterCallback() {
//		public void onShutter() {
//
//		}
//	};
//
//	/** Handles data for raw picture */
//	PictureCallback rawCallback = new PictureCallback() {
//		public void onPictureTaken(byte[] data, Camera camera) {
//
//		}
//	};
//
//	/** Handles data for jpeg picture */
//	PictureCallback jpegCallback = new PictureCallback() {
//		public void onPictureTaken(byte[] data, Camera camera) {
//			//Decode the data obtained by the camera into a Bitmap
//			ultimaFoto = BitmapFactory.decodeByteArray(data, 0, data.length);
//
//			try {
//
//				String vFileName = HelperFile.getFileWithUniqueName() + ".jpg";
//				OmegaFileConfiguration ofc = new OmegaFileConfiguration();
//				String dir = ofc.getPath(OmegaFileConfiguration.TIPO.FOTO);
//
//				HelperCamera.savePhotoInFile(
//						dir,
//						vFileName,
//						ultimaFoto,
//						OmegaFileConfiguration.PERCENT_QUALITY,
//						OmegaFileConfiguration.COMPRESS_FORMAT);
//				ultimoArquivoDeFoto = vFileName;
//				//ExifInterface exif = new ExifInterface(ultimoArquivoDeFoto);
//				//int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
//				int rotate = -90;
//
//				Matrix matrix = new Matrix();
//				matrix.postRotate(rotate);
//				ultimaFoto = Bitmap.createBitmap(ultimaFoto, 0, 0, ultimaFoto.getWidth(), ultimaFoto.getHeight(), matrix, true);
//				marcaPonto();
//
//			} catch (Exception e) {
//				SingletonLog.insereErro(e, SingletonLog.TIPO.PAGINA);
//			}
//		}
//	};

}
