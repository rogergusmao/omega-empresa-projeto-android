package app.omegasoftware.pontoeletronico.TabletActivities;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresaRotina;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoPonto;
import app.omegasoftware.pontoeletronico.gpsnovo.ContainerLocalizacao;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSControler;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSServiceLocationListener;
import app.omegasoftware.pontoeletronico.listener.MinhaPessoaEmpresaButtonClickListener;

public class MenuPontoEletronicoRotinaActivityMobile extends OmegaRegularActivity {
	

	
	EXTDAOPessoa objPessoa;
	Table tuplaPessoa;
	//Search mode toggle buttons
	private ToggleButton entradaButton;
	private ToggleButton saidaButton;
	private ToggleButton vetorTipoPontoButton[] = new ToggleButton[3];
	private int tipoPonto;
//	private Button microphoneButton;
	boolean isEntrada = false;
	boolean isToTakePicture = false;
	String idEmpresa;
	String idPessoaEmpresaRotina;
	String nomeEmpresa;
	String idPessoa;
	
	Button ultimosPontosBatidosButton;
	
	
	public static final int FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA = 988;
	public static final int FORM_ERROR_DIALOG_GPS_FORA_DE_AREA = 989;
	//Constants
	public static final String TAG = "MenuPontoEletronicoRotinaActivityMobile";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		//Log.d(TAG, "onCreate(): Activity created");
		setContentView(R.layout.menu_ponto_eletronico_rotina_activity_mobile_layout);
		Bundle vParameter = getIntent().getExtras();
		
		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID))
			idPessoaEmpresaRotina =vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {
		Database db = null;
		try{
			db = new DatabasePontoEletronico(this);
			objPessoa = new EXTDAOPessoa(db);
			
			
			EXTDAOPessoaEmpresaRotina vObjPER = new EXTDAOPessoaEmpresaRotina(db);
			if(vObjPER.select(idPessoaEmpresaRotina)){
				idEmpresa =  vObjPER.getValorDoAtributoDaChaveExtrangeira(EXTDAOPessoaEmpresaRotina.PESSOA_EMPRESA_ID_INT, EXTDAOPessoaEmpresa.EMPRESA_ID_INT);
				
				if(idEmpresa != null){
					EXTDAOEmpresa vObjEmpresa = new EXTDAOEmpresa(db);
					vObjEmpresa.setAttrValue(EXTDAOEmpresa.ID, idEmpresa);
					if(vObjEmpresa.select()){
						nomeEmpresa =vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.NOME); 
					}
					
					idPessoa = vObjPER.getValorDoAtributoDaChaveExtrangeira(EXTDAOPessoaEmpresaRotina.PESSOA_EMPRESA_ID_INT, EXTDAOPessoaEmpresa.PESSOA_ID_INT);
					objPessoa.setAttrValue(EXTDAOPessoa.ID, idPessoa);
					objPessoa.formatToSQLite();
					tuplaPessoa =  objPessoa.getFirstTupla();
				}else{
					Log.e(TAG, "Error empresa inexistente.");
				}
			}
			
				
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}finally{
			if(db != null) db.close();
		}
		
		
		return true;	
	}

	@Override
	public void initializeComponents() {
		
		ultimosPontosBatidosButton = (Button) findViewById(R.id.ultimos_pontos_batidos_button);
		ultimosPontosBatidosButton.setOnClickListener(new MinhaPessoaEmpresaButtonClickListener(this));
		
		vetorTipoPontoButton[0] = (ToggleButton) findViewById(R.id.cafe_button);
		vetorTipoPontoButton[0].setOnCheckedChangeListener(new TipoPontoTypeChangeListener(0));
		
		vetorTipoPontoButton[1] = (ToggleButton) findViewById(R.id.almoco_button);
		vetorTipoPontoButton[1].setOnCheckedChangeListener(new TipoPontoTypeChangeListener(1));
		
		vetorTipoPontoButton[2] = (ToggleButton) findViewById(R.id.turno_button);
		vetorTipoPontoButton[2].setOnCheckedChangeListener(new TipoPontoTypeChangeListener(2));
		
		vetorTipoPontoButton[2].setChecked(true);
		//Activity listener
		CustomServiceListener listener = new CustomServiceListener();
		
		this.entradaButton = (ToggleButton) findViewById(R.id.entrada_button);
		this.entradaButton.setOnCheckedChangeListener(new EntradaTypeChangeListener());
		
		this.saidaButton = (ToggleButton) findViewById(R.id.saida_button);
		this.saidaButton.setOnCheckedChangeListener(new SaidaTypeChangeListener());
		
		this.entradaButton.setChecked(true);
		
		((Button)findViewById(R.id.cadastrar_button)).setOnClickListener(listener);
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		try{
			switch (id) {
			case FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA:
				vTextView.setText(getResources().getString(R.string.ponto_pessoa_inexistente_na_empresa));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA);
						
					}
				});
				break;
			case FORM_ERROR_DIALOG_GPS_FORA_DE_AREA:
				vTextView.setText(getResources().getString(R.string.gps_fora_de_area));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_GPS_FORA_DE_AREA);
						
					}
				});
				break;	
				
				
			default:
				//			vDialog = this.getErrorDialog(id);
				return super.onCreateDialog(id);
			}
		} catch(Exception ex){
			return null;
		}
		return vDialog;

	}


	private class TipoPontoTypeChangeListener implements OnCheckedChangeListener 
	{
		int index;
		public TipoPontoTypeChangeListener(int pIndex){
			index = pIndex;
		}
		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
			
			if(isChecked)
			{
				for(int i = 0; i < vetorTipoPontoButton.length; i++){
					if(i == index) continue;
					else{
						vetorTipoPontoButton[i].setChecked(false);
						vetorTipoPontoButton[i].setTextColor(R.color.gray_215);		
					}
				}
				tipoPonto = index;
				isEntrada = true;
				vetorTipoPontoButton[index].setTextColor(R.color.white);
			}
		}

	}
	
	private class EntradaTypeChangeListener implements OnCheckedChangeListener 
	{

		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
			
			if(isChecked)
			{
				saidaButton.setChecked(false);
				saidaButton.setTextColor(R.color.gray_215);
				isEntrada = true;

				entradaButton.setTextColor(R.color.white);			
			}
			else
			{
				saidaButton.setChecked(true);
			}
		}

	}



	private class SaidaTypeChangeListener implements OnCheckedChangeListener
	{

		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
			if(isChecked)
			{
				entradaButton.setChecked(false);
				entradaButton.setTextColor(R.color.gray_215);

				saidaButton.setTextColor(R.color.white);
				isEntrada = false;

			}
			else
			{
				entradaButton.setChecked(true);
			}
		}

	}

	private String getTextoDescritivo(String pIdPessoa){
		Database db = null;
		try{
			db = new DatabasePontoEletronico(this);
			
			if(pIdPessoa == null || pIdPessoa.length() == 0 ) return null;
			
			if(tuplaPessoa != null){
				String vEntradaOuSaida = "";
				if(isEntrada)
					vEntradaOuSaida = getResources().getString(R.string.ponto_entrada);
				else vEntradaOuSaida = getResources().getString(R.string.ponto_saida);
				
				String vTipoPonto = "";
				switch(vetorTipoPontoButton[tipoPonto].getId()){
					case R.id.almoco_button:
						
						vTipoPonto = getResources().getString(R.string.ponto_almoco);
						break;
					case R.id.turno_button:
						vTipoPonto = getResources().getString(R.string.ponto_turno);
						break;
					case R.id.cafe_button:
						vTipoPonto = getResources().getString(R.string.ponto_cafe);
						break;
				}
				
				String vSufixoTexto = getResources().getString(R.string.confirmar_marca_ponto);
				
				String vSufixoTexto2 =getResources().getString(R.string.confirmar_marca_ponto_pessoa);
				
				String pNomePessoa = tuplaPessoa.getStrValueOfAttribute(EXTDAOPessoa.NOME);
				
				if(pNomePessoa == null)
					pNomePessoa = "'sem nome cadastrado'";
				return vSufixoTexto + " " + vEntradaOuSaida.toUpperCase() + " no " + vTipoPonto.toUpperCase() + " na empresa " + nomeEmpresa.toUpperCase() + " da " + vSufixoTexto2 + " " + pNomePessoa.toUpperCase() + "?";
			} return null;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			
		} finally{
			if(db != null)
			db.close();
		}
		return null;
	}

	public class MarcaPontoLoader extends AsyncTask<String, Integer, Boolean>
	{
		
		Integer dialogId = null;
		String vTextConfirmacao;
//		Intent intent = null;
		public MarcaPontoLoader(){
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}
		DialogInterface.OnClickListener dialogConfirmacaoPontoClickListener;
		@Override
		protected Boolean doInBackground(String... params) {
			try{	
				boolean gpsObrigatorio = true;
				if(OmegaConfiguration.DEBUGGING){
					if(!OmegaConfiguration.DEBUGGING_GPS_OBRIGATORIO_PARA_PONTO){
						gpsObrigatorio = false;
					}
				}
				if(!GPSServiceLocationListener.isActiveGPS() && gpsObrigatorio)
					dialogId = FORM_ERROR_DIALOG_GPS_FORA_DE_AREA;
				else {
					vTextConfirmacao = getTextoDescritivo(idPessoa);
					if(vTextConfirmacao == null || vTextConfirmacao.length() == 0 )
						dialogId = FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA;
					else{
//						intent = new Intent(getApplicationContext(), FormConfirmacaoActivityMobile.class);
//						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_MESSAGE, vTextConfirmacao);
//						
						dialogConfirmacaoPontoClickListener = new DialogInterface.OnClickListener() {
						    public void onClick(DialogInterface dialog, int which) {
						        switch (which){
						        case DialogInterface.BUTTON_POSITIVE:
						        	procedimentoConfirmacaoPonto(RESULT_OK, true);
						            break;

						        case DialogInterface.BUTTON_NEGATIVE:
						        	procedimentoConfirmacaoPonto(RESULT_OK, false);
						            break;
						        }
						    }
						};
						
						
					}
				}
			}
			catch(Exception e)
			{
				SingletonLog.insereErro(e, TIPO.FORMULARIO);
			}finally{
				
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
				
			if(dialogId != null)
				showDialog(dialogId);
			else{
				FactoryAlertDialog.showAlertDialog(
						MenuPontoEletronicoRotinaActivityMobile.this, 
						vTextConfirmacao,
						dialogConfirmacaoPontoClickListener);
//				startActivityForResult(intent, OmegaConfiguration.STATE_FORM_CONFIRMACAO_PONTO);
			}
			} catch (Exception ex) {
				SingletonLog.openDialogError(MenuPontoEletronicoRotinaActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}	
	
	private class CustomServiceListener implements OnClickListener
	{


		// On-click event handler for all the buttons

		
		public void onClick(View view) {
			
			switch (view.getId()) {
			// Number buttons: '0' to '9'
			
				// Operator buttons: '+', '-', '*', '/' and '='
			case R.id.cadastrar_button:
				
				
				new MarcaPontoLoader().execute();
//				if(!GPSServiceLocationListener.isAtive())
//					showDialog(FORM_ERROR_DIALOG_GPS_FORA_DE_AREA);
//				else {
//					String vTextConfirmacao = getTextDescritivo(idPessoa);
//					if(vTextConfirmacao == null || vTextConfirmacao.length() == 0 )
//						showDialog(FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA);
//					else{
//						Intent vNewIntent = new Intent(getApplicationContext(), FormConfirmacaoActivityMobile.class);
//						vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_MESSAGE, vTextConfirmacao);
//						startActivityForResult(vNewIntent, OmegaConfiguration.STATE_FORM_CONFIRMACAO_PONTO);
//					}
//				}
				break;
			
			}
		}
	}
	public void procedimentoConfirmacaoPonto(int resultCode, Boolean confirmacao){
		boolean gpsObrigatorio = true;
		if(OmegaConfiguration.DEBUGGING){
			if(!OmegaConfiguration.DEBUGGING_GPS_OBRIGATORIO_PARA_PONTO){
				gpsObrigatorio = false;
			}
		}
		if(!GPSControler.hasLocation() && gpsObrigatorio)
			showDialog(FORM_ERROR_DIALOG_GPS_FORA_DE_AREA);
		else if (resultCode == RESULT_OK) {
			
			if(confirmacao){
				Integer vLatitude = null;
				Integer vLongitude = null;
				ContainerLocalizacao vContainerLocalizacao = GPSControler.getLocalizacao();
				if(vContainerLocalizacao != null){
					vLatitude = vContainerLocalizacao.getLatitude();
					vLongitude = vContainerLocalizacao.getLongitude();	
				}
				
				
					
				Intent vNewIntent = new Intent(getApplicationContext(), MarcaPontoActivityMobile.class);
				vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA, idPessoa);
				vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, idEmpresa);
				vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_EMPRESA, nomeEmpresa);
				vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_ENTRADA, isEntrada);
				vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_TIPO_PONTO, EXTDAOTipoPonto.getTipoPontoDoIdToggleButton(vetorTipoPontoButton[tipoPonto].getId()));
				if(vLatitude != null && vLongitude != null){
					vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_LATITUDE, vLatitude);
					vNewIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_LONGITUDE, vLongitude);	
				}
				
				
				startActivityForResult(vNewIntent, OmegaConfiguration.STATE_FORM_MARCA_PONTO);
			}
		}
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		switch (requestCode) {
		case OmegaConfiguration.STATE_FORM_CONFIRMACAO_PONTO:
			Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO , false);
			procedimentoConfirmacaoPonto(resultCode, vConfirmacao);
			break;
		case OmegaConfiguration.STATE_FORM_MARCA_PONTO:
			//Quando retorna da marcacao de ponto
			break;
		default:
			break;
		}
	}

}
