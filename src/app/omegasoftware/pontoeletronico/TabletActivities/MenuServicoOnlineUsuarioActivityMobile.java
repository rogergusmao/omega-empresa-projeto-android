package app.omegasoftware.pontoeletronico.TabletActivities;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.ContainerPrograma;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.controler.ControlerUsuarioServico;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.service.ServiceMyPosition;
import app.omegasoftware.pontoeletronico.service.ServiceTiraFoto;

public class MenuServicoOnlineUsuarioActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "MenuServicoOnlineUsuarioActivityMobile";
	DatabasePontoEletronico db=null;
	private ToggleButton monitoramentoRemotoInternoToggleButton;
	private ToggleButton monitoramentoRemotoExternoToggleButton;
	private ToggleButton localizacaoToggleButton;
	private Button salvarButton;
	//private LinearLayout llFoto;
	CustomDataLoader dataLoader;
	String idUsuario;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if(!getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_TAG))
			getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		Bundle vParameter = getIntent().getExtras();
		if(vParameter!= null){
			if( vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID))
				idUsuario = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		}
		setContentView(R.layout.menu_servico_online_usuario_activity_mobile_layout);
		// Se nuo for a acitivy do grupo
		if(idUsuario == null){
			
			idUsuario = OmegaSecurity.getIdUsuario();
		} 
		
		dataLoader = new CustomDataLoader(this);
		dataLoader.execute();

	}
	final int FORM_DIALOG_ATUALIZACAO_OK = 1;

	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog parentDialog = super.onCreateDialog(id);
		if(parentDialog != null) return parentDialog;
		
		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (id) {
		
		case FORM_DIALOG_ATUALIZACAO_OK:
			vTextView.setText(getResources().getString(R.string.atualizacao_ok));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_DIALOG_ATUALIZACAO_OK);
				}
			});
			break;
		
		default:
			return null;
			
		}
		
		return vDialog;
	}
	
	@Override
	public boolean loadData() {
		
		return true;	
	}

	@Override
	public void finish() {
		try{
			if(db != null)
				db.close();
			super.finish();
		}catch(Exception ex){

		}
	}
	ToggleButton vVetorToggleButton[] ;
	boolean listFlagServicoAtivo[];
	Integer vVetorServicoExistente[] ;
	@Override
	public void initializeComponents() {
		try{

			db = new DatabasePontoEletronico(this);
			
			//Now we to connect each button in this screen to its onClickListener
			monitoramentoRemotoInternoToggleButton = (ToggleButton) findViewById(R.id.menu_servico_online_monitoramento_remoto_togglebutton);
			if(!ContainerPrograma.isServicoPermitido(ServiceTiraFoto.ID_INTERNO))
				monitoramentoRemotoInternoToggleButton.setVisibility(View.GONE);
			
			monitoramentoRemotoExternoToggleButton = (ToggleButton) findViewById(R.id.menu_servico_online_monitoramento_remoto_externo_togglebutton);
			if(!ContainerPrograma.isServicoPermitido(ServiceTiraFoto.ID_EXTERNO))
				monitoramentoRemotoExternoToggleButton.setVisibility(View.GONE);
			
			localizacaoToggleButton = (ToggleButton) findViewById(R.id.menu_servico_online_localizacao_togglebutton);
			if(!ContainerPrograma.isServicoPermitido(ServiceMyPosition.ID))
				localizacaoToggleButton.setVisibility(View.GONE);
			
			salvarButton= (Button) findViewById(R.id.salvar_button);
			salvarButton.setOnClickListener(new SalvarButtonListener());
			
			monitoramentoRemotoInternoToggleButton.setOnCheckedChangeListener(new ServicoListener(this, monitoramentoRemotoInternoToggleButton, ServiceTiraFoto.ID_INTERNO));
			monitoramentoRemotoExternoToggleButton.setOnCheckedChangeListener(new ServicoListener(this, monitoramentoRemotoExternoToggleButton, ServiceTiraFoto.ID_EXTERNO));
			localizacaoToggleButton.setOnCheckedChangeListener(new ServicoListener(this, localizacaoToggleButton, ServiceMyPosition.ID));

			if(!ContainerPrograma.isServicoPermitido(ServiceTiraFoto.ID_EXTERNO))
				monitoramentoRemotoExternoToggleButton.setVisibility(View.GONE);
			
			vVetorServicoExistente = new Integer[]{ 
					ServiceMyPosition.ID, 
					ServiceTiraFoto.ID_INTERNO, 
					ServiceTiraFoto.ID_EXTERNO };
			vVetorToggleButton = new ToggleButton[]{
					localizacaoToggleButton,
					monitoramentoRemotoInternoToggleButton, 
					monitoramentoRemotoExternoToggleButton };
			listFlagServicoAtivo = new boolean[] {false, false, false}; 
			setCheckedServicoButtonAtivo(vVetorToggleButton, vVetorServicoExistente, idUsuario);
			
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		} finally{
			if(db != null)db.close();
		}

	}
	public void onSalvarButtonClicked(){
		if(db == null)
			try {
				db = new DatabasePontoEletronico(MenuServicoOnlineUsuarioActivityMobile.this);
			} catch (OmegaDatabaseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		try{
			for(int i = 0 ; i < vVetorToggleButton.length; i++){
				if(vVetorToggleButton[i].getVisibility() == View.VISIBLE){
					boolean ativo = vVetorToggleButton[i].isChecked();
					if(listFlagServicoAtivo[i] != ativo){
						
						if(ativo){
							ControlerUsuarioServico.saveServicoStatus(
									MenuServicoOnlineUsuarioActivityMobile.this, 
									db, 
									idUsuario, 
									vVetorServicoExistente[i], 
									true, 
									true);
							if(idUsuario.compareTo(OmegaSecurity.getIdUsuario()) == 0)
							sendBroadCastToAtivarServico(vVetorServicoExistente[i]);
							
						}else{
							ControlerUsuarioServico.saveServicoStatus(
									MenuServicoOnlineUsuarioActivityMobile.this, 
									db, 
									idUsuario, 
									vVetorServicoExistente[i], 
									false, 
									true);
							if(idUsuario.compareTo(OmegaSecurity.getIdUsuario()) == 0)
								sendBroadCastToInativarServico(vVetorServicoExistente[i]);	
						}
						listFlagServicoAtivo[i] = ativo;
					}
				}
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			
		} finally{
			if(db != null)
				db.close();
		}
		
		
	}
	//---------------------------------------------------------------
		//------------------ Methods related to planos ------------------
		//---------------------------------------------------------------
		private class SalvarButtonListener implements OnClickListener
		{
			
			public SalvarButtonListener(){
			}

			public void onClick(View v) {
				new SalvarLoader().execute();

			}
		}	
	
	
	private void setCheckedServicoButtonAtivo(ToggleButton pVetorToggleButton[] , Integer pVetorIdServico[], String pIdUsuario){
		Integer[] vListIdServicoAtivo = ControlerUsuarioServico.getListaIdServicoAtivoNoStatus(this, db, pIdUsuario, true);
		if(vListIdServicoAtivo == null) return;
		if(pVetorIdServico.length == pVetorToggleButton.length){
			for (int i = 0 ; i < pVetorIdServico.length; i ++) {
				Integer vIdServicoExistente = pVetorIdServico[i];
				ToggleButton vToggle = pVetorToggleButton[i];
				
				Integer vIndex = HelperInteger.getIndexOfContent(vListIdServicoAtivo, vIdServicoExistente);
				if(vIndex != null){
					listFlagServicoAtivo[i] = true;
					vToggle.setChecked(true);
				}	

			}
		}
		//else Log.e(TAG, "Proporcao incorreta dos vetores de entrada da funcao");
	}


	private void sendBroadCastToAtivarServico(int pIdServico)
	{
		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SERVICE_STATUS_SERVICO);
		updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_ATIVO, true);
		updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_SERVICO, pIdServico);
		sendBroadcast(updateUIIntent);
	}

	private void sendBroadCastToInativarServico(int pIdServico)
	{
		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SERVICE_STATUS_SERVICO);
		updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_ATIVO, false);
		updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_SERVICO, pIdServico);
		sendBroadcast(updateUIIntent);
	}


	private class ServicoListener implements OnCheckedChangeListener 
	{
		int idServico;
		Activity activity;
		ToggleButton toggleButton;
		public ServicoListener(Activity pActivity, ToggleButton pToggleButton, int pIdServico){
			activity = pActivity;
			idServico = pIdServico;
			toggleButton = pToggleButton;
		}

		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
			new HandlerToogle(activity, toggleButton, idServico, isChecked).execute();
		}
	}	

	public class HandlerToogle 
	{

		Integer dialogId = null;
		Activity activity = null;
		boolean isChecked = false;
		Integer idServico = null;
		ToggleButton toggleButton = null;
		HandlerToggleButton handlerToggleButton;
		public HandlerToogle(Activity pActivity, ToggleButton pToggleButton, int pIdServico, boolean pIsChecked){
			activity = pActivity;
			isChecked = pIsChecked;
			toggleButton = pToggleButton;
			idServico = pIdServico;
			handlerToggleButton = new HandlerToggleButton(toggleButton);
		}


		public class HandlerToggleButton extends Handler{
			static public final int WHAT_FALSE = 0;
			static public final int WHAT_TRUE = 1; 
			ToggleButton togglerButton;
			public HandlerToggleButton(ToggleButton pToggleButton){
				togglerButton = pToggleButton;
			}

			@Override
			public void handleMessage(Message msg)
			{	
				if(msg != null){
					switch (msg.what) {
					case WHAT_FALSE:
						toggleButton.setChecked(false);
						toggleButton.setTextColor(R.color.gray_215);
						break;
					case WHAT_TRUE:
						toggleButton.setTextColor(R.color.white);
						toggleButton.setChecked(true);
						break;
					default:
						break;
					}
				}
			}
		}
		
		protected Boolean execute() {
			try{	
				if(toggleButton != null && idServico != null){
					if(!isChecked)
					{
						handlerToggleButton.sendEmptyMessage(HandlerToggleButton.WHAT_FALSE);
						
					}
					else
					{
						handlerToggleButton.sendEmptyMessage(HandlerToggleButton.WHAT_TRUE);
						
					}
				}
				return true;
			}
			catch(Exception e){
				SingletonLog.insereErro(e, TIPO.FORMULARIO);
			}
			return false;
		}

		
	}

	public class SalvarLoader extends AsyncTask<String, Integer, Boolean>
	{

		
		public SalvarLoader(){
			
		
		}


		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}



		@Override
		protected Boolean doInBackground(String... params) {
			try{	
				onSalvarButtonClicked();
				return true;
			}
			catch(Exception e){
				SingletonLog.insereErro(e, TIPO.FORMULARIO);
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);

			try{
				showDialog(FORM_DIALOG_ATUALIZACAO_OK);
			} catch (Exception ex) {
				SingletonLog.openDialogError(MenuServicoOnlineUsuarioActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}


}
