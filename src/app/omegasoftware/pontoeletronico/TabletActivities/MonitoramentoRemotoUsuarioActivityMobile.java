package app.omegasoftware.pontoeletronico.TabletActivities;


import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.OmegaImageView;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.service.ServiceListTuplaUsuarioFotoMobile;
import app.omegasoftware.pontoeletronico.service.ServiceUltimaTuplaUsuarioFotoMobile;

public class MonitoramentoRemotoUsuarioActivityMobile extends OmegaRegularActivity{
	//Close button
	//	private Button closeButton;
	public static final String TAG = "MonitoramentoRemotoUsuarioActivityMobile";
	BroadcastReceiverMonitoramentoRemotoUsuarioFoto serviceReceiver;
	private DatabasePontoEletronico db=null;
	private String usuarioId;
	boolean isFotoInterna;
	boolean isLoadingFoto = false;
	boolean isPause = false;
	
	private Timer loadImageViewTimer = null;
	private ArrayList<String> listNameImageStored = new ArrayList<String>();
	private ArrayList<String> listDatetimeStored = new ArrayList<String>();
	OmegaFileConfiguration config = new OmegaFileConfiguration();
	Intent serviceReceiveFileOfService;
	Intent serviceUltimaTuplaUsuarioFoto;
	Intent serviceListTuplaUsuarioFoto;
	private static final int NAO_INICIALIZADO = -3 ;
	
	private static final int INICIANDO = -1 ;

	private int pointerImageLoaded = NAO_INICIALIZADO;

	private static final int TYPE_ONLINE = 0 ;
	private static final int TYPE_OFFLINE = 1 ;
	private int type;
	//se for nulo entao eh monitorado as ultimas fotos do usuario em questao
	//caso contrario sera carregado o historico de fotos desejado
	private String vetorIdFotoUsuario[];
	private String vetorDataHoraFotoUsuario [];

	Button play;
	Button stop;
	Button pause;
	Button fwd;
	Button rew;
	Button start;
	Button end;

	TextView nomeArquivoTextView;
	TextView datatimeTextView;
	TextView barra1TextView;
	TextView barra2TextView;
	TextView tracoTextView;
	TextView bufferTextView;
	TextView pointerTextView;
	TextView totalTextView;

	private Typeface customTypeFace;

	private OmegaImageView imageView;
	boolean isActivityStarted = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.monitoramento_remoto_usuario_activity_mobile_layout);
		
		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		

		Bundle vParameters = getIntent().getExtras();

		//Gets funcionario id from the Intent used to create this activity
		//TODO: Test connecting between ListResultActivity and FuncionarioDetailsActivity
		try {
			this.usuarioId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
			this.isFotoInterna = vParameters.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_CAMERA_INTERNA);

			if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_VETOR_FOTO_USUARIO_ID)){
				
				
				this.vetorIdFotoUsuario = vParameters.getStringArray(OmegaConfiguration.SEARCH_FIELD_VETOR_FOTO_USUARIO_ID);
				this.vetorDataHoraFotoUsuario = vParameters.getStringArray(OmegaConfiguration.SEARCH_FIELD_VETOR_FOTO_USUARIO_DATA_HORA);
				
				serviceListTuplaUsuarioFoto = new Intent(this, ServiceListTuplaUsuarioFotoMobile.class);
				serviceListTuplaUsuarioFoto.putExtra(OmegaConfiguration.SEARCH_FIELD_VETOR_FOTO_USUARIO_ID, vetorIdFotoUsuario);
				serviceListTuplaUsuarioFoto.putExtra(OmegaConfiguration.SEARCH_FIELD_VETOR_FOTO_USUARIO_DATA_HORA, vetorDataHoraFotoUsuario);
				
				
				startService(serviceListTuplaUsuarioFoto);
				type = TYPE_OFFLINE;
			}
			else{
				serviceUltimaTuplaUsuarioFoto = new Intent(this, ServiceUltimaTuplaUsuarioFotoMobile.class);
				serviceUltimaTuplaUsuarioFoto.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, usuarioId);
				startService(serviceUltimaTuplaUsuarioFoto);
				this.vetorIdFotoUsuario = null;
				type = TYPE_ONLINE;
			}

			stop = ((Button) findViewById(R.id.stop_button));
			play = ((Button) findViewById(R.id.play_button));
			pause = ((Button) findViewById(R.id.pause_button));
			fwd = ((Button) findViewById(R.id.fwd_button));
			rew = ((Button) findViewById(R.id.rew_button));
			start = ((Button) findViewById(R.id.start_button));
			end = ((Button) findViewById(R.id.end_button));
			if (this.serviceReceiver == null){
				this.serviceReceiver = new BroadcastReceiverMonitoramentoRemotoUsuarioFoto();
			}
			IntentFilter uiUpdateIntentFilter = new IntentFilter(OmegaConfiguration.REFRESH_ACTIVITY_MONITORAMENTO_REMOTO_USUARIO);
			registerReceiver(this.serviceReceiver, uiUpdateIntentFilter);
		}catch(Exception ex){ }

		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		new CustomDataLoader(this).execute();

	}
	
	private Integer before(){
		Integer vLastPointer = null;
		Integer vNewPointer = pointerImageLoaded;
		if(vNewPointer != INICIANDO){
			vLastPointer = vNewPointer;
			
			if(vNewPointer - 1 >= 0 && 
					vNewPointer - 1 < listNameImageStored.size())
				vNewPointer -= 1;
		} else{
			if(0 < listNameImageStored.size()){
				vNewPointer = 0;
			} 
		}
		if(vLastPointer != null){
			if(vLastPointer == vNewPointer)
				return null;
		}
		return vNewPointer;
	}
	
	private Integer next(){
		Integer vLastPointer = null;
		Integer vNewPointer = pointerImageLoaded;
		if(vNewPointer != INICIANDO){
			vLastPointer = vNewPointer;
			if(type == TYPE_ONLINE){
				if(vNewPointer < listNameImageStored.size())
					vNewPointer =listNameImageStored.size() - 1;
			}
			else if(type == TYPE_OFFLINE){
				if(vNewPointer >= 0 && 
						vNewPointer + 1 < listNameImageStored.size())
					vNewPointer += 1;
			}
		} else{
			if(0 < listNameImageStored.size()){
				vNewPointer = 0;
			} 
		}
		if(vLastPointer != null){
			if(vLastPointer == vNewPointer)
				return null;
		}
		return vNewPointer;
	}
	
	private void loadPointerImageView(Integer pPointer){
		if(pPointer == null) return;
		pointerImageLoaded = pPointer;
		if(pointerImageLoaded < listNameImageStored.size() && 
				pointerImageLoaded >= 0){
			myTextViewHandler.sendEmptyMessage(TextViewHandler.TAG_POINTER);
			String vNamePhoto = listNameImageStored.get(pointerImageLoaded);
			String vDatetime = listDatetimeStored.get(pointerImageLoaded);
			loadImageView(vNamePhoto, vDatetime);
		}
	}
	
	
	private void setTimerIfNotPause()
	{
		if(isPause) return;
			//Starts reporter timer
		if(type == TYPE_OFFLINE){
			this.loadImageViewTimer = new Timer();
			this.loadImageViewTimer.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					//se nao estao pausado
					if(isLoadingFoto) return;
					if(pointerImageLoaded >= 0 || pointerImageLoaded == INICIANDO){
						Integer vNewPointer = next();
						if(vNewPointer != null)
							loadPointerImageView(vNewPointer);
					}
				}
			},
			OmegaConfiguration.LOAD_IMAGE_VIEW_OFFLINE_INITIAL_DELAY,
			OmegaConfiguration.LOAD_IMAGE_VIEW_OFFLINE_TIMER_INTERVAL);
		} else {
			this.loadImageViewTimer = new Timer();
			this.loadImageViewTimer.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					//se nao estao pausado
					if(isLoadingFoto) return;
					if(pointerImageLoaded >= 0 || pointerImageLoaded == INICIANDO){
						Integer vNewPointer = next();
						if(vNewPointer != null)
							loadPointerImageView(vNewPointer);
					}
				}
			},
			OmegaConfiguration.LOAD_IMAGE_VIEW_ONLINE_INITIAL_DELAY,
			OmegaConfiguration.LOAD_IMAGE_VIEW_ONLINE_TIMER_INTERVAL);
		}
	}

	@Override
	public void finish(){
		try{
			if(serviceReceiver != null)
				unregisterReceiver(this.serviceReceiver);
			if(serviceReceiveFileOfService != null)
				this.stopService(serviceReceiveFileOfService);
			if(serviceUltimaTuplaUsuarioFoto != null)
				this.stopService(serviceUltimaTuplaUsuarioFoto);
			if(serviceListTuplaUsuarioFoto != null)
				this.stopService(serviceListTuplaUsuarioFoto);
			
			if(this.loadImageViewTimer != null)
				this.loadImageViewTimer.cancel();
	
			if(this.db != null){
				this.db.close();
			}
			super.finish();
		}catch(Exception ex){

		}
	}


	@Override
	public boolean loadData() {

		isActivityStarted = true;

		return true;

	}
	
	
	TextViewHandler myTextViewHandler = new TextViewHandler(); 
	public class TextViewHandler extends Handler{
		public static final int TAG_TOTAL = 0;
		public static final int TAG_BUFFER = 1;
		public static final int TAG_POINTER = 2;
		public static final int TAG_NOME_ARQUIVO = 3;
		
		@Override
        public void handleMessage(Message msg)
        {
			switch (msg.what) {
			
			
			case TAG_TOTAL:
				totalTextView.setText(String.valueOf(vetorIdFotoUsuario.length));
				totalTextView.setTypeface(customTypeFace);
				totalTextView.postInvalidate();
				break;
			case TAG_BUFFER:
				bufferTextView.setText(String.valueOf(listNameImageStored.size()));
				bufferTextView.setTypeface(customTypeFace);
				bufferTextView.postInvalidate();			
				break;
			case TAG_POINTER:
				pointerTextView.setText(String.valueOf(pointerImageLoaded + 1));
				pointerTextView.setTypeface(customTypeFace);
				pointerTextView.postInvalidate();
				String vNomeArquivo = listNameImageStored.get(pointerImageLoaded);
				nomeArquivoTextView.setText(vNomeArquivo);
				nomeArquivoTextView.setTypeface(customTypeFace);
				nomeArquivoTextView.postInvalidate();
				break;

			default:
				break;
			}
			
        }
	}
	
	public class ImageViewHandler extends Handler{
		String namePhoto;
		String datetime;
		Context context;
		boolean isFirstTime = true;
		public ImageViewHandler(Context pContext){
			context = pContext;
		}
		
		public void setContent(String pNamePhoto, String pDatetime){
			namePhoto = pNamePhoto;
			datetime = pDatetime;
		}
		
		@Override
        public void handleMessage(Message msg)
        {
			isLoadingFoto = true;
			
			
			OmegaFileConfiguration vConfig = new OmegaFileConfiguration();
			String vPath = vConfig.getPath(OmegaFileConfiguration.TIPO.ANEXO);
			try {
				try {
					Bitmap bMap = BitmapFactory.decodeFile(vPath + namePhoto);
					imageView.setImageBitmap(bMap);
					datatimeTextView.setText( datetime);
					datatimeTextView.setTypeface(customTypeFace);
					datatimeTextView.postInvalidate();
					if(isFirstTime){
						isFirstTime = false;
						play.setVisibility(View.GONE);
						pause.setVisibility(View.VISIBLE);
					}
					
				} catch (Exception e) {
					//Log.e("Error reading file", e.toString());
				}
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			isLoadingFoto = false;
		}		
	}
	
//	public class MyRunnable implements Runnable{
//
//		String namePhoto;
//		Context context;
//		public MyRunnable(String p_namePhoto, Context pContext){
//			namePhoto = p_namePhoto;
//			context = pContext;
//		}
//		public void run() {
//			isLoadingFoto = true;
//			
//
//			String vPath = config.getPathFilesToReceive();
//			FileInputStream vInputPhoto;
//			try {
//				vInputPhoto = new FileInputStream(new File(vPath + namePhoto));
//				//		        BufferedInputStream buf;
//				try {
//
//					//		            buf = new BufferedInputStream(vInputPhoto);
//					Bitmap bMap = BitmapFactory.decodeStream(vInputPhoto);
//					imageView.setVisibility(View.VISIBLE);
//					imageView.setImageBitmap(bMap);
//					imageView.invalidate();
//					
//					if (vInputPhoto != null) {
//						vInputPhoto.close();
//					}
//				} catch (Exception e) {
//					//Log.e("Error reading file", e.toString());
//				}
//			} catch (Exception e) {
//				
//				e.printStackTrace();
//			}
//			isLoadingFoto = false;
//		}
//	}
	ImageViewHandler handler = new ImageViewHandler(this);
	private void loadImageView(String pNamePhoto, String pDatetime){
		try{
			handler.setContent(pNamePhoto, pDatetime);
			handler.sendEmptyMessage(0);
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
		}
	}
	LinearLayout linearLayoutImage; 
	
	

	
	@Override
	public void initializeComponents() {

		linearLayoutImage = ((LinearLayout) findViewById(R.id.linearLayoutimage));
		//	        ((OmegaImageView) findViewById(R.id.preview));
		datatimeTextView = ((TextView) findViewById(R.id.data_monitoramento_remoto_usuario_data_textview));
		bufferTextView = ((TextView) findViewById(R.id.buffer_monitoramento_remoto_usuario_data_textview));
		totalTextView = ((TextView) findViewById(R.id.total_monitoramento_remoto_usuario_data_textview));
		nomeArquivoTextView = ((TextView) findViewById(R.id.arquivo_monitoramento_remoto_usuario_data_textview));
		barra1TextView = ((TextView) findViewById(R.id.barra_1_monitoramento_remoto_usuario_data_textview));
		barra2TextView = ((TextView) findViewById(R.id.barra_2_monitoramento_remoto_usuario_data_textview));
		pointerTextView = ((TextView) findViewById(R.id.ponteiro_monitoramento_remoto_usuario_data_textview));
		
		
		
		imageView = new OmegaImageView(this);
		imageView.setVisibility(View.VISIBLE);
		
		LayoutParams vParam = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT); 
		imageView.setLayoutParams(vParam);
		linearLayoutImage.addView(imageView);
		
		CustomServiceListener vCustom = new CustomServiceListener();
		rew.setOnClickListener(vCustom);
		fwd.setOnClickListener(vCustom);
		start.setOnClickListener(vCustom);
		end.setOnClickListener(vCustom);
		switch (type) {
		case TYPE_OFFLINE:
			myTextViewHandler.sendEmptyMessage(TextViewHandler.TAG_TOTAL);
			play.setVisibility(View.VISIBLE);
			play.setOnClickListener(vCustom);
			
			stop.setVisibility(View.VISIBLE);
			stop.setOnClickListener(vCustom);
			pause.setVisibility(View.GONE);
			pause.setOnClickListener(vCustom);
			break;
		case TYPE_ONLINE:
			barra2TextView.setVisibility(View.GONE);
			totalTextView.setVisibility(View.GONE);
			play.setVisibility(View.VISIBLE);
			play.setOnClickListener(vCustom);
			stop.setVisibility(View.GONE);
			pause.setVisibility(View.GONE);
			pause.setOnClickListener(vCustom);
			break;

		default:
			break;
		}
	}
	
	private void cancelTimerIfActive(){
		if(loadImageViewTimer != null){
			loadImageViewTimer.cancel();
			loadImageViewTimer = null;
		}
	}

	private class CustomServiceListener implements OnClickListener
	{

		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.stop_button:
				cancelTimerIfActive();
				if(listNameImageStored.size() > 0 ){
					pointerImageLoaded = INICIANDO ;
				}				
				break;
			
			case R.id.fwd_button:
				cancelTimerIfActive();
				Integer index = next();
				loadPointerImageView(index);
				setTimerIfNotPause();
				break;
			case R.id.start_button:
				cancelTimerIfActive();
				loadPointerImageView(0);
				setTimerIfNotPause();
				break;
			case R.id.end_button:
				cancelTimerIfActive();
				loadPointerImageView(listNameImageStored.size() - 1);
				setTimerIfNotPause();
				break;
			case R.id.rew_button:
				cancelTimerIfActive();
				Integer index2 = before();
				loadPointerImageView(index2);
				setTimerIfNotPause();
				break;
			case R.id.play_button:
				isPause = false;
				if(listNameImageStored.size() > 0 )	
					setTimerIfNotPause();
				play.setVisibility(View.GONE);
				pause.setVisibility(View.VISIBLE);
				break;
			case R.id.pause_button:
				cancelTimerIfActive();
				isPause = true;
				play.setVisibility(View.VISIBLE);
				pause.setVisibility(View.GONE);
				break;
			default:
				break;
			}

		}
	}


	//Receber a confirmacao da imagem baixada
	//Receber a ordem para baixar a do imagem servidor
	public class BroadcastReceiverMonitoramentoRemotoUsuarioFoto extends BroadcastReceiver{


		public static final String TAG = "BroadcastReceiverMonitoramentoRemotoUsuarioFoto";

		@Override
		public void onReceive(Context context, Intent intent) {

			//Log.d(TAG,"onReceive(): Intent received - " + intent.getAction());

			if(intent.getAction().equals(OmegaConfiguration.REFRESH_ACTIVITY_MONITORAMENTO_REMOTO_USUARIO)) {
				Bundle vParameter = intent.getExtras();
				String vId = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID);
				String vDatetime = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_DATE_TIME);
				listNameImageStored.add(vId + ".jpg");
				listDatetimeStored.add(vDatetime);
				myTextViewHandler.sendEmptyMessage(TextViewHandler.TAG_BUFFER);
				if(pointerImageLoaded == NAO_INICIALIZADO ){
					pointerImageLoaded = INICIANDO;
					setTimerIfNotPause();
				} 
			}
		}
	}

	
}
