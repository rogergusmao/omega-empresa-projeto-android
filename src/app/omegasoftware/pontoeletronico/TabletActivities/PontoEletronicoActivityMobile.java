package app.omegasoftware.pontoeletronico.TabletActivities;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.Settings;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.cam.CameraConfiguration;
import app.omegasoftware.pontoeletronico.cam.CameraTakePictureActivity;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.gps.GPSConfiguration;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSContainer;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSService;
import app.omegasoftware.pontoeletronico.gpsnovo.HelperGPS;

public class PontoEletronicoActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "PontoEletronicoActivityMobile";

	private GPSContainer gpsContainer;	
	Intent gpsServiceIntent = null;
	GPSServiceBroadcastReceiver gpsServiceReceiver;

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		//Log.d(TAG, "onCreate(): Activity created");
		setContentView(R.layout.splash_mobile);
		gpsContainer = new GPSContainer();
		try {
			//Starts the TruckTrackerService
			gpsServiceIntent = new Intent(this, GPSService.class);
			this.startService(gpsServiceIntent);
		
	}catch(Exception ex){
		SingletonLog.insereErro(ex, TIPO.PAGINA);
	}

		new CustomDataLoader(this).execute();
	}

	@Override
	public void finish(){
		try{
			if(gpsServiceIntent != null)
				stopService(gpsServiceIntent);

		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
		try{
			if(this.gpsServiceReceiver != null)
				unregisterReceiver(this.gpsServiceReceiver);
		} catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
		try{
			super.finish();
		} catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}

	}

	@Override
	public boolean loadData() {
		return true;	
	}

	@Override
	public void initializeComponents() {
		try{
			Intent intent = new Intent("com.google.zxing.client.android.SCAN");
			intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
			startActivityForResult(intent, OmegaConfiguration.STATE_READ_QR_CODE);
		} catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {


		if (requestCode == OmegaConfiguration.STATE_READ_QR_CODE) {
			if (resultCode == RESULT_OK) {
				String funcionarioId = intent.getStringExtra("SCAN_RESULT");
				String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
				//		        showDialog(R.string.result_succeeded, "Format: " + format + "\nContents: " + contents);

				//Log.d("onAcitivityResult", protocol.getPost());
				Intent intent2 = new Intent( this, null);
//				Intent intent2 = new Intent( this, FuncionarioTakePictureActivityMobile.class);
				boolean vIsEntrada = false;
				if(funcionarioId != null){
					String[] vetor =funcionarioId.split("e:");

					if(vetor.length == 2){
						funcionarioId =  vetor[1];
						vIsEntrada = true;
					}	
					else{
						vetor = funcionarioId.split("s:");
						if(vetor.length == 2){
							funcionarioId =  vetor[1];
							vIsEntrada = false;	
						}

					}

				}

				intent2.putExtra(OmegaConfiguration.SEARCH_FUNCIONARIO_ID, funcionarioId);
				intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_ENTRADA, vIsEntrada);

				try{
					startActivityForResult(intent2, OmegaConfiguration.STATE_FUNCIONARIO_TAKE_PICTURE);
				}catch(Exception ex){
					SingletonLog.insereErro(ex, TIPO.PAGINA);
				}
				return;

			} else if (resultCode == RESULT_CANCELED) {
				//	        showDialog(R.string.result_failed, getString(R.string.result_failed_why));
			}
		}else if(requestCode == OmegaConfiguration.STATE_FUNCIONARIO_TAKE_PICTURE){
			try{
				Intent intent2 = new Intent("com.google.zxing.client.android.SCAN");
				intent2.putExtra("SCAN_MODE", "QR_CODE_MODE");
				startActivityForResult(intent2, OmegaConfiguration.STATE_READ_QR_CODE);
			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.PAGINA);
			}
		} else{
			finish();
		}

	}

	protected void gpsInitializeObjects(){
		if(!HelperGPS.checkIfGPSIsEnabled(getApplicationContext()))
		{
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);	
		}

		if (this.gpsServiceReceiver == null){
			this.gpsServiceReceiver = new GPSServiceBroadcastReceiver();
		}
		IntentFilter uiUpdateIntentFilter = new IntentFilter(GPSConfiguration.REFRESH_UI_INTENT);
		//    	IntentFilter pictureTakerIntentFilter = new IntentFilter(GPSConfiguration.TAKE_PICTURE_INTENT);
		registerReceiver(this.gpsServiceReceiver, uiUpdateIntentFilter);
		//    	registerReceiver(this.updateReceiver, pictureTakerIntentFilter);
	}
	@Override
	protected void onResume() {
		super.onResume();
		//Log.d(TAG, "onCreate(): Activity resumed");
		if(!HelperGPS.checkIfGPSIsEnabled(getApplicationContext()))
		{
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);	
		}
		
		if (this.gpsServiceReceiver == null){
			this.gpsServiceReceiver = new GPSServiceBroadcastReceiver();
		}
		IntentFilter uiUpdateIntentFilter = new IntentFilter(GPSConfiguration.REFRESH_UI_INTENT);
		registerReceiver(this.gpsServiceReceiver, uiUpdateIntentFilter);

	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(this.gpsServiceReceiver);
	}

	public class GPSServiceBroadcastReceiver extends BroadcastReceiver{


		public static final String TAG = "GPSServiceBroadcastReceiver";

		@Override
		public void onReceive(Context context, Intent intent) {

			//Log.d(TAG,"onReceive(): Intent received - " + intent.getAction());

			if(intent.getAction().equals(GPSConfiguration.REFRESH_UI_INTENT)) {
				gpsContainer.loadIntent(intent);

			}
			else if(intent.getAction().equals(CameraConfiguration.TAKE_PICTURE_INTENT))
			{
				Intent camera = new Intent(getApplicationContext(), CameraTakePictureActivity.class);
				startActivity(camera);
			}
		}

	}

}
