package app.omegasoftware.pontoeletronico.TabletActivities;

import android.content.Intent;
import android.os.Bundle;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;

public class PontoEletronicoPostActivity  extends OmegaRegularActivity{

	//Constants
	private static final String TAG = "PontoEletronicoPostActivity";

	public final int UNIFACIL_ALERT_DIALOG = 11;
	public final static int MEDICO_DETAIL_DIALOG = 4;
	public final static int PLACE_DETAIL_DIALOG = 5;
	public final static int MAP_DIALOG= 6;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		try{
			setContentView(R.layout.splash_mobile);	

		
	
			Bundle vParameters = getIntent().getExtras();
	
			String vetorKey[] = {"xml"};
			String vetorContent[] = vParameters.getStringArray("content");
			//	String login = login("rogerfsg@gmail.com", "123456");
			postBaterPonto(vetorKey, vetorContent);
			
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
	}

	public String postBaterPonto(String[] p_vetorKey, String[] p_vetorContent) throws Exception{

		throw new Exception("Not implemented");
	}
	

	@Override
	public boolean loadData() {

		return true;	
	}

	@Override
	public void initializeComponents() {
		//
		//		Intent intent = new Intent("com.google.zxing.client.android.SCAN");
		//		intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
		//		startActivityForResult(intent, 0);
		
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				// Handle successful scan
				//Log.d("onActivityResult", "RESULTADO OK");

			} else if (resultCode == RESULT_CANCELED) {
				// Handle cancel
				//Log.d("onActivityResult", "RESULTADO CANCELED");
			}
		}
	}

}
