package app.omegasoftware.pontoeletronico.TabletActivities;

//public class PontoMapActivityMobile extends MapActivity{
//	protected static String TAG = "PontoMapActivity";
//
//	public MapView map=null;
//	//	private MyLocationOverlay me=null;
//	public static MyLocationOverlay me=null;
//	protected final int FORM_ERROR_DIALOG_EMPRESA_SEM_COORDENADA = 851;
//	protected final int FORM_ERROR_DIALOG_EMPRESA_SEM_ENDERECO = 852;
//	protected final int FORM_ERROR_DIALOG_GPS_OFFLINE = 853;
//	boolean isRouteDisplay = false;
//	private TextView distanciaTextView;
//	private Typeface customTypeFace;
//
//	protected static ArrayList<CustomOverlayItem> listContainerMarcador = new ArrayList<CustomOverlayItem>();
//
//	private List<Overlay> mapOverlays;
//	
//	//Quando -1 sera o veiculo principal
//
//	Button meButton;
//	Button originButton;
//	Button destinationButton;
//
//	static GeoPoint empresaGeoPoint;
//	static GeoPoint pontoGeoPoint;
//
//	final public static int TYPE_MONITORAMENTO_ORIGEM = 2;
//	final public static int TYPE_MONITORAMENTO_PROPRIO = 3;
//	final public static int TYPE_MONITORAMENTO_DESTINO = 4;
//
//	
//	
//	protected int typePontoMonitorado = TYPE_MONITORAMENTO_DESTINO;
//
//	protected static Integer indexPontoMonitorado = null; 
//
//
//	ContainerAtualizaEndereco containerAtualizaEndereco;
//	static boolean isInitializing = true;
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		try{
//		Bundle vParameters = getIntent().getExtras();
//		int vIdLayout = R.layout.map_route_layout_mobile;
//
//		setContentView(vIdLayout);
//		map=(MapView)findViewById(R.id.mapview);
//		mapOverlays = map.getOverlays();
//
//
//		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
//		distanciaTextView = (TextView)findViewById(R.id.distancia_textview);
//		meButton=(Button)findViewById(R.id.me_button);
//		meButton.setOnClickListener(new  MeButtonOnClickListener(this));
//		meButton.setTypeface(customTypeFace);
//
//		originButton=(Button)findViewById(R.id.origin_place_button);
//		originButton.setOnClickListener(new  OriginButtonOnClickListener(this));
//		originButton.setTypeface(customTypeFace);
//
//		destinationButton=(Button)findViewById(R.id.destination_place_button);
//		destinationButton.setOnClickListener(new  DestinationButtonOnClickListener(this));
//		destinationButton.setTypeface(customTypeFace);
//
//		Database db = new DatabasePontoEletronico(this);
//		EXTDAOPonto vObjPonto = new EXTDAOPonto(db);
//		String vIdPonto = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
//		if(vObjPonto.select(vIdPonto)){
//			EXTDAOEmpresa vObjEmpresa = new EXTDAOEmpresa(db);
//			String vIdEmpresa = vObjPonto.getStrValueOfAttribute(EXTDAOPonto.EMPRESA_ID_INT);
//
//			if(vIdEmpresa != null && 
//					vObjEmpresa.select(vIdEmpresa)){
//				String vLatDest = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.LATITUDE_INT);
//				String vLonDest = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.LONGITUDE_INT);
//		
//				
//				if(vLatDest != null && vLonDest != null){
//					empresaGeoPoint = HelperMapa.getGeoPoint(HelperInteger.parserInt(vLatDest), HelperInteger.parserInt(vLonDest));
//				} else{
//					containerAtualizaEndereco = vObjEmpresa.atualizaLatitudeELongitude(this, db, vIdEmpresa);
//					switch (containerAtualizaEndereco.tipoAtualizacaoEndereco) {
//					case SEM_COORDENADA_NO_ENDERECO:
//						showDialog(FORM_ERROR_DIALOG_EMPRESA_SEM_COORDENADA);
//						break;
//					case SEM_ENDERECO:
//						showDialog(this.FORM_ERROR_DIALOG_EMPRESA_SEM_ENDERECO);
//						break;
//					default:
//						empresaGeoPoint = containerAtualizaEndereco.geoPoint;
//						break;
//					}
//				}
//				if(empresaGeoPoint != null){
//					CustomOverlayItem vCustom= new CustomOverlayItem(
//							empresaGeoPoint, 
//							"Ponto ", 
//							"Ponto ", 
//							getResources().getDrawable(R.drawable.menu_cadastro_empresa));
//					listContainerMarcador.add(vCustom);
//				}
//			}
//
//
//
//			String vLatDest = vObjPonto.getStrValueOfAttribute(EXTDAOPonto.LATITUDE_INT);
//			String vLonDest = vObjPonto.getStrValueOfAttribute(EXTDAOPonto.LONGITUDE_INT);
//
//			if(vLatDest != null && vLonDest != null){
//				pontoGeoPoint = HelperMapa.getGeoPoint(HelperInteger.parserInt(vLatDest), HelperInteger.parserInt(vLonDest));
//				CustomOverlayItem vCustom= new CustomOverlayItem(
//						pontoGeoPoint, 
//						"Ponto ", 
//						"Ponto ", 
//						getResources().getDrawable(R.drawable.marcador_agulha_verde));
//				listContainerMarcador.add(vCustom);
//
//			}
//		}
//
//		db.close();
//
//		loadListCustomItem();
//
//		if(isInitializing){
//
//			focaPontoMonitorado();
//			refreshDistanciaTextView();
//		} 
//
//		isInitializing = false;
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//
//		}
//	}
//
//	private void refreshDistanciaTextView(){
//		if(empresaGeoPoint == null || pontoGeoPoint == null) return;
//		Double latitudeEmpresa = HelperGPS.parserCoordenadaStringToDouble(String.valueOf(empresaGeoPoint.getLatitudeE6()));
//		Double longitudeEmpresa = HelperGPS.parserCoordenadaStringToDouble(String.valueOf(empresaGeoPoint.getLongitudeE6()));
//		
//		Double latitudePonto = HelperGPS.parserCoordenadaStringToDouble(String.valueOf(pontoGeoPoint.getLatitudeE6()));
//		Double longitudePonto = HelperGPS.parserCoordenadaStringToDouble(String.valueOf(pontoGeoPoint.getLongitudeE6()));
//		
//		
//		Double vDistanciaEntreCoordenadas  = null; 
//		if(latitudePonto != null && longitudePonto != null && latitudePonto != null && longitudePonto != null){
//			
//				vDistanciaEntreCoordenadas = HelperGPS.getDistancia(latitudeEmpresa, longitudeEmpresa, latitudePonto, longitudePonto, false);
//			
//		}
//		distanciaTextView.setText(String.valueOf(vDistanciaEntreCoordenadas));
//		distanciaTextView.setTypeface(customTypeFace);
//	}
//	public GeoPoint getGeoPointEmpresa(){
//		if(empresaGeoPoint == null){
//			switch (containerAtualizaEndereco.tipoAtualizacaoEndereco) {
//			case SEM_COORDENADA_NO_ENDERECO:
//				showDialog(FORM_ERROR_DIALOG_EMPRESA_SEM_COORDENADA);
//				break;
//			case SEM_ENDERECO:
//				showDialog(this.FORM_ERROR_DIALOG_EMPRESA_SEM_ENDERECO);
//				break;
//			default:
//				break;
//			
//			}
//			return null;
//		} else return empresaGeoPoint;
//	}
//
//	@Override
//	protected Dialog onCreateDialog(int id) {
//		Dialog vDialog = new Dialog(this);
//		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		
//		vDialog.setContentView(R.layout.dialog);
//
//		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
//		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);
//
//		switch (id) {
//			case FORM_ERROR_DIALOG_GPS_OFFLINE:
//			vTextView.setText(getResources().getString(R.string.gps_fora_de_area));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_GPS_OFFLINE);
//				}
//			});
//			break;
//		case FORM_ERROR_DIALOG_EMPRESA_SEM_COORDENADA:
//			vTextView.setText(getResources().getString(R.string.endereco_empresa_invalido));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_EMPRESA_SEM_COORDENADA);
//				}
//			});
//			break;
//			
//		case FORM_ERROR_DIALOG_EMPRESA_SEM_ENDERECO:
//			vTextView.setText(getResources().getString(R.string.empresa_sem_endereco));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_EMPRESA_SEM_ENDERECO);
//				}
//			});
//			break;	
//			
//		default:
//			return super.onCreateDialog(id);
//			
//		}
//		
//		return vDialog;
//	}
//
//	public class MeButtonOnClickListener implements OnClickListener{
//		Activity activity;
//		public MeButtonOnClickListener(Activity pActivity){
//			activity = pActivity;
//		}
//		public void onClick(View arg0) {
//			
//			typePontoMonitorado = TYPE_MONITORAMENTO_PROPRIO;
//			focaPontoMonitorado();	
//		}
//
//	}
//
//	public class OriginButtonOnClickListener implements OnClickListener{
//		Activity activity;
//		public OriginButtonOnClickListener(Activity pActivity){
//			activity = pActivity;
//		}
//		public void onClick(View arg0) {
//			
//			typePontoMonitorado = TYPE_MONITORAMENTO_ORIGEM;
//
//			focaPontoMonitorado();	
//		}
//
//	}
//
//	public class DestinationButtonOnClickListener implements OnClickListener{
//		Activity activity;
//		public DestinationButtonOnClickListener(Activity pActivity){
//			activity = pActivity;
//		}
//		public void onClick(View arg0) {
//			
//			typePontoMonitorado = TYPE_MONITORAMENTO_DESTINO;
//			focaPontoMonitorado();	
//		}
//
//	}
//
//	public static GeoPoint getMyGeoPoint(){
//		if(me != null){
//
//			return me.getMyLocation();
//		} else return null;
//	}
//
//	@Override
//	public void finish(){
//
//		try {
//			empresaGeoPoint = null;
//			pontoGeoPoint = null;
//			isInitializing = true;
//			listContainerMarcador.clear();
//
//			
//			isRouteDisplay = false;
//			if(me != null){
//				me.disableMyLocation();
//				me = null;
//			}
//			super.finish();
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, TIPO.PAGINA);
//		}
//	}
//	public void setPontoMonitorado(Integer pIndexMonitorado, int pTypeMonitorado){
//		indexPontoMonitorado = pIndexMonitorado;
//		typePontoMonitorado = pTypeMonitorado;
//	}
//
//	public void focaPontoMonitorado(){
//		GeoPoint vGeoPoint = null;
//
//		switch (typePontoMonitorado) {
//
//		case TYPE_MONITORAMENTO_PROPRIO :
//
//			if(me != null)
//				vGeoPoint = me.getMyLocation();
//			if(vGeoPoint == null)
//				showDialog(FORM_ERROR_DIALOG_GPS_OFFLINE);
//			break;
//		case TYPE_MONITORAMENTO_ORIGEM:
//			vGeoPoint = getGeoPointEmpresa();
//			break;
//		case TYPE_MONITORAMENTO_DESTINO:
//			vGeoPoint = pontoGeoPoint;
//			break;
//		default:
//			break;
//		}
//		if(vGeoPoint != null)
//			focaPonto(vGeoPoint);
//	}
//
//
//
//	protected SitesOverlay addListCustomItemInListContainerMarcador(int pVetorLatitude[], int pVetorLongitude[], int pMarkerId, boolean clearPoints){
//		if(pVetorLatitude != null && pVetorLongitude != null ){
//			if(pVetorLatitude.length == pVetorLongitude.length){
//				Drawable vMarker = this.getResources().getDrawable(pMarkerId);
//				ArrayList<CustomOverlayItem> vListCustom = new ArrayList<CustomOverlayItem>(); 
//				if(vMarker == null) return null;
//				for (int i = 0 ; i < pVetorLatitude.length; i++) {
//
//					int vLatitude = pVetorLatitude[i];
//					int vLongitude = pVetorLongitude[i];
//					CustomOverlayItem vCustom= new CustomOverlayItem(
//							HelperMapa.getGeoPoint(vLatitude, vLongitude), 
//							"Ponto " + String.valueOf(i), 
//							"Ponto " + String.valueOf(i), 
//							vMarker);
//					listContainerMarcador.add(vCustom);
//					vListCustom.add(vCustom);
//				}
//				SitesOverlay vSite = new SitesOverlay(vListCustom);
//				if(clearPoints)
//					mapOverlays.clear();
//				mapOverlays.add(vSite);
//				map.postInvalidate();
//				return vSite;
//			}
//		}
//		return null;
//	}
//
//	public void removeSitesOverlay(SitesOverlay pSite){
//		if(mapOverlays.remove(pSite))
//			map.postInvalidate();
//	}
//
//	protected void loadListCustomItem(){
//		
//
//		if(me  == null){
//			me=new MyLocationOverlay(this, map);
//			me.enableMyLocation();
//		}
//		map.getOverlays().add(me);
//
//
//		if(listContainerMarcador != null){
//			SitesOverlay vSite = new SitesOverlay(listContainerMarcador);
//			mapOverlays.add(vSite);
//		}
//
//		map.postInvalidate();
//
//	}
//
//
//	public void focaPonto(GeoPoint pGeoPoint){
//		map.getController().setCenter(pGeoPoint);
//		map.getController().setZoom(17);
//		map.setBuiltInZoomControls(true);  
//	}
//
//
//	public boolean onCreateOptionsMenu(Menu menu) {
//		super.onCreateOptionsMenu(menu);
//
////		MenuInflater inflater = getMenuInflater();
////		inflater.inflate(R.menu.mobile_menu, menu);
//		return true;
//
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//
//		switch (item.getItemId()) {
//
//		default:
//			return super.onOptionsItemSelected(item);
//
//		}
//	}
//
//
//	@Override
//	public void onResume() {
//		super.onResume();
//	}  
//
//	@Override
//	public void onPause() {
//		super.onPause();
//
//		//		me.disableCompass();
//	}  
//
//
//	@Override
//	protected boolean isRouteDisplayed() {
//		return(false);
//	}
//
//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		if (keyCode == KeyEvent.KEYCODE_S) {
//			map.setSatellite(!map.isSatellite());
//			return(true);
//		}
//		else if (keyCode == KeyEvent.KEYCODE_Z) {
//			map.displayZoomControls(true);
//			return(true);
//		}
//		else if (keyCode == KeyEvent.KEYCODE_H) {
//			//			sites.toggleHeart();
//
//			return(true);
//		}
//
//		return(super.onKeyDown(keyCode, event));
//	}
//
//	private class SitesOverlay extends ItemizedOverlay<OverlayItem> {
//
//		private ArrayList<CustomOverlayItem> items=new ArrayList<CustomOverlayItem>();
//		//	    private PopupPanel panel=new PopupPanel(R.layout.unifacil_alert_dialog);
//
//		public SitesOverlay(CustomOverlayItem pOverlayItem) {
//			super(null);			
//			items.add(pOverlayItem);
//			populate();
//		}
//
//		public SitesOverlay(ArrayList<CustomOverlayItem> pListOverlayItem) {
//			super(null);			
//			if(pListOverlayItem != null)
//				for (CustomOverlayItem overlayItem : pListOverlayItem) {
//					overlayItem.setMarker(this.getMarker(overlayItem.getMarker()));
//					items.add(overlayItem);
//
//				}
//			populate();
//		}
//
//		@SuppressWarnings("unused")
//		public void addOverlay(CustomOverlayItem overlay){
//			items.add(overlay);
//			populate();
//		}
//
//		// Removes overlay item i
//		public void removeItem(int i){
//			this.items.remove(i);
//			populate();
//		}
//
//		@SuppressWarnings("unused")
//		public void clear(){
//			int i = items.size() - 1 ;
//			for (; i >= 0 ; i--) {
//				removeItem(i);
//			}
//		}
//
//		@Override
//		protected CustomOverlayItem createItem(int i) {
//			return(items.get(i));
//		}
//		@Override
//		public void draw(Canvas canvas, MapView mapView,
//				boolean shadow) {
//			super.draw(canvas, mapView, shadow);
//
//		}
//
//		@Override
//		protected boolean onTap(int i) {
//			try{
//				OverlayItem item=getItem(i);
//				GeoPoint geo=item.getPoint();
//				map.getProjection().toPixels(geo, null);
//			}catch(Exception ex){
//				SingletonLog.insereErro(ex, TIPO.PAGINA);
//			}
//			return(true);
//		}
//
//		@Override
//		public int size() {
//			return(items.size());
//		}
//
//
//		private Drawable getMarker(Drawable marker) {
//			marker.setBounds(0, 0, marker.getIntrinsicWidth(),
//					marker.getIntrinsicHeight());
//			boundCenter(marker);
//
//			return(marker);
//		}
//
//	}
//}
