package app.omegasoftware.pontoeletronico.TabletActivities;

import android.content.Context;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSContainer;




public class ProtocolPostPontoEletronico {

	private String usr;
	private HelperDate helperDate;
	private final String delimiter = ";";
	private GPSContainer gps ;
	String foto;
	public ProtocolPostPontoEletronico (Context p_context, GPSContainer p_gps, String p_usr, String p_foto){
		usr = p_usr;
		helperDate = new HelperDate();
		gps = p_gps;
		foto = p_foto;
	}
	
	
	public String getPost(){
		
		String[] coluna = null;
		String latitude = "null";
		String longitude = "null";
		if(gps != null){
			if(gps.isInitialized()){
				coluna = new String[] {usr, 
						helperDate.getDateDisplay(),
						String.valueOf(gps.latitude),
						String.valueOf(gps.longitude)};
				longitude = String.valueOf(gps.longitude );
				latitude = String.valueOf(gps.latitude) ; 
			} 
		}
		coluna = new String[] {usr, 
				helperDate.getDateDisplay(),
				latitude,
				longitude, 
				foto};
		
		String post = "";
		for(int i = 0 ; i < coluna.length; i++){
			if(post.length() == 0 ){
				post += coluna[i];
			} else{
				post += delimiter + coluna[i];	
			}
		}
		
		return post;
	}
}

