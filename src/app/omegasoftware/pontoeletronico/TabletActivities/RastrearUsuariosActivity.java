package app.omegasoftware.pontoeletronico.TabletActivities;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.dialog.HelperDialog;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Tuple;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.gpsnovo.ContainerLocalizacao;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSControler;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSServiceLocationListener;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference;

import app.omegasoftware.pontoeletronico.routine.RotinaCleanDir;
import app.omegasoftware.pontoeletronico.service.ControlerServicoInterno;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiveBroadcastAutenticacaoInvalida;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiverBroadcastServiceConnectionServerInformation;
import app.omegasoftware.pontoeletronico.webservice.ServicosPontoEletronico;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class RastrearUsuariosActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private ArrayList<Tuple<Double,Double>> coordenadas;
    double[][] zonas = new double[4] [];

    public RastrearUsuariosActivity() {
        coordenadas = new ArrayList<Tuple<Double,Double>>();
        //                   { x,y }
        zonas[0] = new double[] {-1, 1};
        zonas[1] = new double[] {1, 1};
        zonas[2] = new double[] {1, -1};
        zonas[3] = new double[] {-1, -1};

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rastrear_usuarios_mobile_layout);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        new Loader().execute();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    public void adicionaMarcadores(List<MarkerOptions> markers){
        if(markers == null || markers.size() == 0 ) return;
        for (MarkerOptions mo:
             markers) {
            mMap.addMarker(mo);

        }

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));


        } catch (Exception e) {
            SingletonLog.insereErro(e, SingletonLog.TIPO.PAGINA);
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(markers.get(0).getPosition()));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(13));

    }

    public void inicializaComponentesPadroes() {
        try{
            View voltar = findViewById(R.id.btn_voltar_desktop);
            if (voltar != null) {
                voltar.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        onBackPressed();

                    }
                });
            }
        }catch(Exception ex){
            SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
        }

    }

    static final double COORDINATE_OFFSET = 0.00001f; // You can change this value according to your need
    static final double DIFF = 0.001001f;
    public boolean estaProxima(double lat, double lng){
        for (Tuple<Double, Double> t :
                coordenadas) {

            if (Math.abs(t.x - lat) < DIFF
                    && Math.abs(t.y - lng) < DIFF){
                return true;
            }
        }
        return false;
    }

    public LatLng factoryLatLng(int intLat, int intLng){

        double lat = (double)intLat/1000000;
        double lng = (double)intLng/1000000;
        boolean validade = true;
        if(estaProxima(lat,lng)){
            validade = false;
            int cont = 0;

            for(int i = 1 ; i < 10; i++){
                lat += i * COORDINATE_OFFSET * zonas[cont++ % zonas.length][0];

                for(int j = i  ; j < i + 1; j++){
                    validade = true;
                    if(estaProxima(
                        lat
                        ,lng)){
                        lng += j * COORDINATE_OFFSET * zonas[cont++ % zonas.length][1];
                        validade = false;
                    }
                }
                if(validade) break;
            }
        }

        if(validade){
            LatLng latlng = new LatLng(lat, lng);
            coordenadas.add(new Tuple<Double,Double>(lat, lng));
            return latlng;
        } else
            return new LatLng(lat, lng);

    }

    private void initUsuarios(
            List<MarkerOptions> markers
            , JSONArray jsonArray) throws JSONException {

        SimpleDateFormat sdf=new SimpleDateFormat();
        for(int i = 0 ; i < jsonArray.length(); i++){
            JSONObject json = jsonArray.getJSONObject(i);

            LatLng latlng = factoryLatLng(json.getInt("latitude"), json.getInt("longitude"));
            String fonteInformacao = json.isNull("fonteInformacao") ? "-" : ( json.getInt("fonteInformacao") == 1 ? "GPS" : "AGPS");

            Date d = HelperDate.getDateFromSecOfssec(json.getInt("cadastroSec"), json.getInt("cadastroOffsec"), sdf);
            String strDatetime = HelperDate.getStringDateTime(RastrearUsuariosActivity.this, d);
            markers.add(
                    new MarkerOptions()
                            .position(latlng)
                            .title(json.getString("nomeUsuario"))
                            .snippet(json.getInt("velocidade") + " Km/h"
                                    + " " + fonteInformacao
                                    + " "+ strDatetime
                            )
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_male))
                            .alpha(0.7f)
                            .flat(true) // icone vai girar com a tela

            );
        }
    }

    private void initEmpresas(
            List<MarkerOptions> markers
            , JSONArray jsonArray) throws JSONException {

//        SimpleDateFormat sdf=new SimpleDateFormat();
        for(int i = 0 ; i < jsonArray.length(); i++){
            JSONObject json = jsonArray.getJSONObject(i);
            if(json.isNull("latitude")) continue;
            LatLng latlng = factoryLatLng(json.getInt("latitude"), json.getInt("longitude")) ;

            markers.add(
                    new MarkerOptions()
                            .position(latlng)
                            .title(json.getString("nomeEmpresa"))
                            .icon(factoryBitmap(R.drawable.map_company_3d)) );
        }
    }

    public BitmapDescriptor factoryBitmap(int idResource){

        int size = 90;
//        CameraPosition cp= mMap.getCameraPosition();
//        if(cp.zoom > 15 ){
//            size = 20;
//        }

        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(idResource);
        Bitmap b=bitmapdraw.getBitmap();

//        DisplayMetrics metrics = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(metrics);
//        dispWidth=metrics.widthPixels;
//        dispheight=metrics.heightPixels;

        Bitmap smallMarker = Bitmap.createScaledBitmap(b, size, size, false);
        smallMarker = changeImageColor(smallMarker, R.color.yellow);
        return BitmapDescriptorFactory.fromBitmap(smallMarker);
    }

    public void intiMinhaPosicao(List<MarkerOptions> markers){
        ContainerLocalizacao cl = GPSControler.getLocalizacao();
        if(cl != null){
            markers.add(
                    new MarkerOptions()
                            .position(
                                    factoryLatLng(cl.getLatitude(), cl.getLongitude())
                            )
                            .title(RastrearUsuariosActivity.this.getString( R.string.voce))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_voce))

            );
        } else {
            Toast.makeText(
                    RastrearUsuariosActivity.this
                    , RastrearUsuariosActivity.this.getString( R.string.area_sombra)
                    , Toast.LENGTH_LONG).show();
        }
    }

    public class Loader extends AsyncTask<String, Integer, Integer> {
        List<MarkerOptions> markers = null;
        Exception e2 = null;
        JSONObject obj = null;
        public Loader() {

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(String... params) {
            try {
                int codRetorno = PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId();
                try{
                    inicializaComponentesPadroes();
                    ServicosPontoEletronico s = new ServicosPontoEletronico(RastrearUsuariosActivity.this);
                    obj = s.getDadosMapa();
                    if(obj == null) return null;
                    codRetorno = obj.getInt("mCodRetorno");
                    markers = new ArrayList<MarkerOptions>();
                    if(codRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()){
                        JSONObject jsonObj = obj.getJSONObject("mObj");

                        RastrearUsuariosActivity.this.initUsuarios(markers, jsonObj.getJSONArray("usuarios"));
                        RastrearUsuariosActivity.this.initEmpresas(markers, jsonObj.getJSONArray("empresas"));

                        RastrearUsuariosActivity.this.intiMinhaPosicao(markers);

                        //RastrearUsuariosActivity.this.adicionaMarcadores(markers);
                    }
                }catch(Exception ex){
                    SingletonLog.factoryToast(RastrearUsuariosActivity.this, ex, SingletonLog.TIPO.PAGINA);
                }
                return codRetorno;
            }catch(Exception ex){
                this.e2=ex;
                return PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId();
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            try {
                if(result == null){
                    Toast.makeText(RastrearUsuariosActivity.this, R.string.error_form_desconectado, Toast.LENGTH_LONG).show();
                }
                else if(result == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()){
                    RastrearUsuariosActivity.this. adicionaMarcadores(markers);
                } else if(result ==PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId() ){
                    SingletonLog.factoryToast(RastrearUsuariosActivity.this, new Exception(obj.toString()), SingletonLog.TIPO.PAGINA);
                } else {
                    Toast.makeText(RastrearUsuariosActivity.this, obj.getString("mMensagem"), Toast.LENGTH_LONG).show();
                }
                if(this.e2!=null){
                    SingletonLog.openDialogError(RastrearUsuariosActivity.this, this.e2, SingletonLog.TIPO.PAGINA);
                }

            }
            catch (Exception ex) {
                SingletonLog.openDialogError(RastrearUsuariosActivity.this, ex, SingletonLog.TIPO.PAGINA);
            }

        }

    }


    public static class GetDadosMapaService extends Service {

        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }
        @Override
        public void onStart(Intent intent, int startId) {



        }


    }

    public static Bitmap changeImageColor(Bitmap sourceBitmap, int color) {
        Bitmap resultBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0,
                sourceBitmap.getWidth() - 1, sourceBitmap.getHeight() - 1);
        Paint p = new Paint();
        ColorFilter filter = new LightingColorFilter(color, 1);
        p.setColorFilter(filter);
        Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(resultBitmap, 0, 0, p);
        return resultBitmap;
    }
    public static Drawable covertBitmapToDrawable(Context context, Bitmap bitmap) {
        Drawable d = new BitmapDrawable(context.getResources(), bitmap);
        return d;
    }
    public static Bitmap convertDrawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

}