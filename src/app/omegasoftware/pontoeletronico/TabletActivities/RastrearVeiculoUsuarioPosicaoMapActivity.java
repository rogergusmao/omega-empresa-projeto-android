package app.omegasoftware.pontoeletronico.TabletActivities;

//public class RastrearVeiculoUsuarioPosicaoMapActivity extends MapaFixedListMarkerActivity{
//
//	public static ArrayList<EXTDAOUsuarioPosicao> listEXTDAOVeiculoUsuarioPosicao = new ArrayList<EXTDAOUsuarioPosicao>();
//	Intent serviceReceiveUsuarioPosicaoRastreamento = null;
//	int lastIdTuplaLoadedOfEXTDAO;
//	ReceiveRastreamentoUsuarioIntentsReceiver serviceReceiver;
//	EXTDAOUsuarioPosicaoRastreamento objUsuarioPosicaoRastreamento;
//	ArrayList<Table> listTuplaUsuarioPosicaoRastreamento;
////	|0 .... |limiteInferiorBuffer ... limiteSuperiorBuffer | ... | totalTuplasBaixadas | ... | totalTuplas
//	int limiteInferiorInclusoBuffer = 0;
//	int pointerBuffer = 0 ;
//	int pointerTotal = 0 ;
//	int totalTuplasBaixadas = ContainerRastreamento.LIMIT_TUPLAS;
//	int totalTuplas = ContainerRastreamento.LIMIT_TUPLAS;
//	public final int TAXA_LOAD_BUFFER = 100;
//	private Timer loadPosicaoTimer = null;
//
//
//	boolean isPause = false;
//	private Typeface customTypeFace;
//	
//
//	Button play;
//	
//	Button pause;
//	Button fwd;
//	Button rew;
//	Button start;
//	Button end;
//
//	TextView velocidadeTextView;
//	TextView datatimeTextView;
//
//	TextView ponteiroTotalTextView;
//	TextView limiteInferiorBufferTextView;
//	TextView limiteSuperiorBufferTextView;
//	TextView totalBaixadoTextView;
//	TextView totalTextView;
//
//	DatabasePontoEletronico db=null;
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		//		setContentView(R.layout.rastrear_veiculo_usuario_map_layout_mobile);
//		try {
//			db = new DatabasePontoEletronico(this);
//		} catch (OmegaDatabaseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
//
//		objUsuarioPosicaoRastreamento = new EXTDAOUsuarioPosicaoRastreamento(db);
//		if (this.serviceReceiver == null){
//			this.serviceReceiver = new ReceiveRastreamentoUsuarioIntentsReceiver();
//		}
//
//		Bundle vParameter = getIntent().getExtras();
//		Integer vTotalTuplasBaixadas = null;
//		if(vParameter != null){
//			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_TOTAL_TUPLAS_BAIXADAS)){
//				vTotalTuplasBaixadas = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_TOTAL_TUPLAS_BAIXADAS);
//				//				Caso ocorra essa situacao => exitem mais tuplas usuario_posicao a serem baixadas
//				if(vTotalTuplasBaixadas == ContainerRastreamento.LIMIT_TUPLAS){
//					String vFimData = null;
//					String vFimHora = null;
//					String vInicioData = null;
//					String vInicioHora = null;
//					String vIdUsuario = null;
//					String vIdVeiculo = null;
//					String vVelocidadeMinima = null;
//					boolean vSomenteComFoto = false;
//
//					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_SOMENTE_COM_FOTO))
//						vSomenteComFoto = vParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_SOMENTE_COM_FOTO);
//					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_USUARIO))
//						vIdUsuario = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_USUARIO);
//					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_VEICULO))
//						vIdVeiculo = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_VEICULO);
//					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_INICIO_DATA))
//						vInicioData = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_INICIO_DATA);
//					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_INICIO_HORA))
//						vInicioHora = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_INICIO_HORA);
//					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_FIM_DATA))
//						vFimData = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_FIM_DATA);
//					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_FIM_HORA))
//						vFimHora = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_FIM_HORA);
//					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_VELOCIDADE_MINIMA))
//						vVelocidadeMinima = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_VELOCIDADE_MINIMA);
//
//					Integer vTotal = ContainerRastreamento.getTotalTuplasRastrearRotaUsuario(
//							this, 
//							objUsuarioPosicaoRastreamento, 
//							vIdUsuario, 
//							vIdVeiculo, 
//							vInicioData, 
//							vInicioHora, 
//							vFimData, 
//							vFimHora, 
//							vVelocidadeMinima, 
//							vSomenteComFoto);
//					if(vTotal != null)
//						if(vTotal > 0){
//							if(vTotal < ContainerRastreamento.LIMIT_TUPLAS){
//								
//								totalTuplas = vTotalTuplasBaixadas;
//								totalTuplasBaixadas = vTotalTuplasBaixadas;
//							} else {
//								
//								totalTuplasBaixadas = vTotalTuplasBaixadas;
//								totalTuplas = vTotal;
//								//Inicializa o servico de download das tuplas usuario posicao na tabela usuario_posicao_rastreamento
//								serviceReceiveUsuarioPosicaoRastreamento = new Intent(this, ServiceReceiveUsuarioPosicaoRastreamento.class);
//
//								serviceReceiveUsuarioPosicaoRastreamento.putExtra(OmegaConfiguration.SEARCH_FIELD_SOMENTE_COM_FOTO, vSomenteComFoto);
//								serviceReceiveUsuarioPosicaoRastreamento.putExtra(OmegaConfiguration.SEARCH_FIELD_USUARIO, vIdUsuario);
//								serviceReceiveUsuarioPosicaoRastreamento.putExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO, vIdVeiculo);
//								serviceReceiveUsuarioPosicaoRastreamento.putExtra(OmegaConfiguration.SEARCH_FIELD_INICIO_DATA, vInicioData);
//								serviceReceiveUsuarioPosicaoRastreamento.putExtra(OmegaConfiguration.SEARCH_FIELD_INICIO_HORA, vInicioHora);
//								serviceReceiveUsuarioPosicaoRastreamento.putExtra(OmegaConfiguration.SEARCH_FIELD_FIM_DATA, vFimData);
//								serviceReceiveUsuarioPosicaoRastreamento.putExtra(OmegaConfiguration.SEARCH_FIELD_FIM_HORA, vFimHora);
//								serviceReceiveUsuarioPosicaoRastreamento.putExtra(OmegaConfiguration.SEARCH_FIELD_VELOCIDADE_MINIMA, vVelocidadeMinima);
//
//								startService(serviceReceiveUsuarioPosicaoRastreamento);
//							}
//						}
//				}
//			}
//		}
//
//		
//		loadDataUsuarioPosicao(0, ContainerRastreamento.LIMIT_TUPLAS);
//		
//		loadListCustomItem();
//	}
////	String.valueOf(limiteInferiorBuffer) + ", " + ContainerRastreamento.LIMIT_TUPLAS_STR
//	public boolean loadDataUsuarioPosicao(int pLimiteInferior, int pQuantidadeTupla){
//		if(pQuantidadeTupla > totalTuplas) pQuantidadeTupla  = totalTuplas; 
//		listTuplaUsuarioPosicaoRastreamento = objUsuarioPosicaoRastreamento.getListTable(
//				null, 
//				String.valueOf(pLimiteInferior) + ", " + String.valueOf(pQuantidadeTupla), 
//				true );
//		if(listTuplaUsuarioPosicaoRastreamento == null) return false;
//		else if(listTuplaUsuarioPosicaoRastreamento.size() == 0 )return false;
//		
//		limiteInferiorInclusoBuffer = pLimiteInferior;
//		
//		pointerTotal = limiteInferiorInclusoBuffer ;
//		listContainerMarcador.clear();
//		
//		if(listTuplaUsuarioPosicaoRastreamento != null)
//			if(listTuplaUsuarioPosicaoRastreamento.size() > 0){
//				ArrayList<Integer> vListLatitude = new ArrayList<Integer>();
//				ArrayList<Integer> vListLongitude = new ArrayList<Integer>();
//				int i = 0 ;
//				for (Table vObj : listTuplaUsuarioPosicaoRastreamento) {
//					String vStrLatitude = vObj.getStrValueOfAttribute(EXTDAOUsuarioPosicaoRastreamento.LATITUDE_INT);
//					String vStrLongitude = vObj.getStrValueOfAttribute(EXTDAOUsuarioPosicaoRastreamento.LATITUDE_INT);
//					if(vStrLongitude != null && vStrLatitude != null){
//						vListLongitude.add(HelperInteger.parserInt(vStrLongitude));
//						vListLatitude.add( HelperInteger.parserInt(vStrLatitude));
//						i += 1;
//					}
//				}
//				Drawable vMarkerPartida = this.getResources().getDrawable(R.drawable.marcador_partida);
//				Drawable vMarkerChegada= this.getResources().getDrawable(R.drawable.marcador_chegada);
//				Drawable vMarkerAgulha= this.getResources().getDrawable(R.drawable.marcador_agulha_verde);
//
//				indexPontoMonitorado = 0;
//				for (i = 0 ; i < vListLatitude.size(); i++) {
//
//					int vLatitude = vListLatitude.get(i);
//					int vLongitude = vListLongitude.get(i);
//					Drawable vMarker = null;
//					if(i == 0 ) vMarker = vMarkerPartida;
//					else if(i == vListLatitude.size() - 1) vMarker = vMarkerChegada;
//					else vMarker = vMarkerAgulha;
//					listContainerMarcador.add(
//							new CustomOverlayItem(
//									HelperMapa.getGeoPoint(vLatitude, vLongitude), 
//									"Ponto " + String.valueOf(i), 
//									"Ponto " + String.valueOf(i), 
//									vMarker));
//				}
//
//			}		
//		loadListCustomItem();
//		return true;
//	}
//
//	public void loadDataApresentacao(){
//		if(pointerBuffer < listTuplaUsuarioPosicaoRastreamento.size()){
//			Table vTupla = listTuplaUsuarioPosicaoRastreamento.get(pointerBuffer);
//			
//			Message vMsg = new Message();
//			vMsg.what = TextViewHandler.CABECALHO_ESCOPO_USUARIO_POSICAO; 
//			Bundle vParameterCabecalho = new Bundle();
//			String vData = vTupla.getStrValueOfAttribute(EXTDAOUsuarioPosicaoRastreamento.DATA_DATE);
//			String vHora = vTupla.getStrValueOfAttribute(EXTDAOUsuarioPosicaoRastreamento.HORA_TIME);
//			String vVelocidade = vTupla.getStrValueOfAttribute(EXTDAOUsuarioPosicaoRastreamento.VELOCIDADE_INT);
//			if(vData != null && vHora != null){
//				vParameterCabecalho.putString(OmegaConfiguration.SEARCH_FIELD_DATE_TIME, vData + " " + vHora);
//			}
//			if(vVelocidade != null)
//				vParameterCabecalho.putString(OmegaConfiguration.SEARCH_FIELD_VELOCIDADE, vVelocidade + " km");
//			vMsg.setData(vParameterCabecalho);
//			myTextViewHandler.sendMessage(vMsg);
//			
//		}
//		
//	}
//
//
//	public void initializeComponents(){
//		
//		play = ((Button) findViewById(R.id.play_button));
//		pause = ((Button) findViewById(R.id.pause_button));
//		fwd = ((Button) findViewById(R.id.fwd_button));
//		rew = ((Button) findViewById(R.id.rew_button));
//		start = ((Button) findViewById(R.id.start_button));
//		end = ((Button) findViewById(R.id.end_button));
//		CustomServiceListener vCustom = new CustomServiceListener();
//		rew.setOnClickListener(vCustom);
//		fwd.setOnClickListener(vCustom);
//		start.setOnClickListener(vCustom);
//		end.setOnClickListener(vCustom);
//
//
////		myTextViewHandler.sendEmptyMessage(TextViewHandler.TAG_TOTAL);
//		play.setVisibility(View.VISIBLE);
//		play.setOnClickListener(vCustom);
//
//		pause.setVisibility(View.GONE);
//		pause.setOnClickListener(vCustom);
//
//		ponteiroTotalTextView = ((TextView) findViewById(R.id.ponteiro_total_textview));
//		limiteInferiorBufferTextView = ((TextView) findViewById(R.id.limite_inferior_buffer_textview));
//		limiteSuperiorBufferTextView = ((TextView) findViewById(R.id.limite_superior_buffer_textview));
//		totalTextView = ((TextView) findViewById(R.id.total_textview));
//		totalBaixadoTextView = ((TextView) findViewById(R.id.total_baixado_textview));
//
//		map=(MapView)findViewById(R.id.mapview);
//	}
//
//	@Override
//	public void finish(){
//		try{
//			if(this.loadPosicaoTimer != null)
//				this.loadPosicaoTimer.cancel();
//
//			if(serviceReceiver != null)
//				unregisterReceiver(this.serviceReceiver);
//			//			if(serviceSynchronizeFastDatabase != null)
//			//				stopService( serviceSynchronizeFastDatabas
//			if(serviceReceiveUsuarioPosicaoRastreamento != null)
//				stopService( serviceReceiveUsuarioPosicaoRastreamento);
//			super.finish();
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, TIPO.PAGINA);
//		}
//	}
//
//	TextViewHandler myTextViewHandler = new TextViewHandler(); 
//	public class TextViewHandler extends Handler{
//
//		public static final int TAG_TOTAL_TUPLAS = -1;
//		public static final int TAG_TOTAL_TUPLA_BAIXADA = 0;
//		public static final int TAG_LIMITE_INFERIOR_BUFFER = 1;
//		public static final int TAG_LIMITE_SUPERIOR_BUFFER = 2;
//		public static final int TAG_POINTER_BUFFER = 3;
//		public static final int TAG_POINTER_TOTAL = 4;
//		public static final int CABECALHO_ESCOPO_USUARIO_POSICAO = 5;
//
//		public void loadListaBuffer(int pLimiteInferior, int pLimiteSuperior){
//
//		}
//
//		public void loadContainerUsuarioPosicaoRastreamento(int pIndexListaTupla){
//
//		}
//
//		@Override
//		public void handleMessage(Message msg)
//		{
//			switch (msg.what) {
//			case CABECALHO_ESCOPO_USUARIO_POSICAO:
//				Bundle vParameter = msg.getData();
//				if(vParameter != null){
//					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_DATE_TIME) ){
//						String vDateTime = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_DATE_TIME);
//						if(vDateTime != null)
//							if(vDateTime.length() > 0 )
//								datatimeTextView.setText(vDateTime);
//					}
//					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_VELOCIDADE) ){
//						String vVelocidade = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_VELOCIDADE);
//						if(vVelocidade != null)
//							if(vVelocidade.length() > 0 )
//								velocidadeTextView.setText(vVelocidade);
//					}
//				}
//				
//				break;
//			
//			case TAG_TOTAL_TUPLAS:
//				totalTextView.setText(String.valueOf(totalTuplas));
//				totalTextView.setTypeface(customTypeFace);
//				totalTextView.postInvalidate();
//				break;
//
//			case TAG_TOTAL_TUPLA_BAIXADA:
//				totalBaixadoTextView.setText(String.valueOf(totalTuplasBaixadas));
//				totalBaixadoTextView.setTypeface(customTypeFace);
//				totalBaixadoTextView.postInvalidate();			
//				break;
//			case TAG_LIMITE_INFERIOR_BUFFER:
//				limiteInferiorBufferTextView.setText(String.valueOf(limiteInferiorInclusoBuffer));
//				limiteInferiorBufferTextView.setTypeface(customTypeFace);
//				limiteInferiorBufferTextView.postInvalidate();
//				break;
//			case TAG_LIMITE_SUPERIOR_BUFFER:
//				limiteSuperiorBufferTextView.setText(String.valueOf(listTuplaUsuarioPosicaoRastreamento.size()));
//				limiteSuperiorBufferTextView.setTypeface(customTypeFace);
//				limiteSuperiorBufferTextView.postInvalidate();
//				break;
//
//			case TAG_POINTER_TOTAL:
//				ponteiroTotalTextView.setText(String.valueOf(pointerTotal));
//				ponteiroTotalTextView.setTypeface(customTypeFace);
//				ponteiroTotalTextView.postInvalidate();
//				break;
//			default:
//				break;
//			}
//
//		}
//	}
//
//	private void cancelTimerIfActive(){
//		if(loadPosicaoTimer != null){
//			loadPosicaoTimer.cancel();
//			loadPosicaoTimer = null;
//		}
//	}
//	private void setTimerIfNotPause()
//	{
//		if(isPause) return;
//		//Starts reporter timer
//		this.loadPosicaoTimer = new Timer();
//		this.loadPosicaoTimer.scheduleAtFixedRate(new TimerTask() {
//			@Override
//			public void run() {
//				//se nao estao pausado
//				
//				if(pointerTotal < totalTuplasBaixadas && pointerTotal >= 0){
//					
//					if(next())
//						loadDataApresentacao();
//				}
//			}
//		},
//		OmegaConfiguration.LOAD_IMAGE_VIEW_OFFLINE_INITIAL_DELAY,
//		OmegaConfiguration.LOAD_IMAGE_VIEW_OFFLINE_TIMER_INTERVAL);
//		 
//	}
////	|		|		....			TAXA_LOAD_BUFFER	....					   |
////	|0 .... |limiteInferiorBuffer ...pointerBuffer ...  limiteSuperiorBuffer | ... | totalTuplasBaixadas | ... | totalTuplas
////	|             .....                pointerTotal
////	int pointerBuffer = 0 ;
////	int pointerTotal = 0 ;
//	private boolean before(){
//		if(pointerTotal - 1 >= limiteInferiorInclusoBuffer && 
//				pointerTotal - 1 >= 0){
//			pointerTotal -= 1;
//			pointerBuffer -= 1;
//			return true;
//		} else if(pointerTotal - 1 >= 0 && pointerTotal - 1 < limiteInferiorInclusoBuffer){
//			loadDataUsuarioPosicao(limiteInferiorInclusoBuffer - TAXA_LOAD_BUFFER, TAXA_LOAD_BUFFER);
//			pointerTotal = listTuplaUsuarioPosicaoRastreamento.size() - 1;
//			pointerBuffer = TAXA_LOAD_BUFFER - 1;
//			return true;
//		} else return false;
//	}
////	|		|		....			TAXA_LOAD_BUFFER	....					   |
////	|0 .... |limiteInferiorBuffer ...pointerBuffer ...  limiteSuperiorBuffer | ... | totalTuplasBaixadas | ... | totalTuplas
////	|             .....                pointerTotal
//	private boolean next(){
//		
//		if(pointerTotal + 1 < listTuplaUsuarioPosicaoRastreamento.size() ){
//			pointerTotal += 1;
//			pointerBuffer += 1;
//			return true;
//		} else if(pointerTotal + 1 < totalTuplasBaixadas && 
//				pointerTotal + 1 >= listTuplaUsuarioPosicaoRastreamento.size()){
//			if(loadDataUsuarioPosicao(listTuplaUsuarioPosicaoRastreamento.size(), TAXA_LOAD_BUFFER)){
//				pointerTotal = listTuplaUsuarioPosicaoRastreamento.size() - 1;
//				pointerBuffer = 0;
//			}
//			return true;
//		} else return false;
//	}
//	
//	private class CustomServiceListener implements OnClickListener
//	{
//
//		public void onClick(View v) {
//
//			switch (v.getId()) {
//			
//
//			case R.id.fwd_button:
//				cancelTimerIfActive();
//				if(next()){
//					loadDataApresentacao();
//				}
//				
//				setTimerIfNotPause();
//				break;
//			case R.id.start_button:
//				cancelTimerIfActive();
//				if(limiteInferiorInclusoBuffer > 0 )
//					loadDataUsuarioPosicao(limiteInferiorInclusoBuffer - TAXA_LOAD_BUFFER + 1, TAXA_LOAD_BUFFER);
//				setTimerIfNotPause();
//				break;
//			case R.id.end_button:
//				cancelTimerIfActive();
//				if(listTuplaUsuarioPosicaoRastreamento.size() - 1 < totalTuplasBaixadas )
//					loadDataUsuarioPosicao(listTuplaUsuarioPosicaoRastreamento.size(), TAXA_LOAD_BUFFER);
//				
//				setTimerIfNotPause();
//				break;
//			case R.id.rew_button:
//				cancelTimerIfActive();
//				if(before())
//					loadDataApresentacao();
//				setTimerIfNotPause();
//				break;
//			case R.id.play_button:
//				isPause = false;
//				if(listTuplaUsuarioPosicaoRastreamento.size() > 0 )	
//					setTimerIfNotPause();
//				play.setVisibility(View.GONE);
//				pause.setVisibility(View.VISIBLE);
//				break;
//			case R.id.pause_button:
//				cancelTimerIfActive();
//				isPause = true;
//				play.setVisibility(View.VISIBLE);
//				pause.setVisibility(View.GONE);
//				break;
//			default:
//				break;
//			}
//
//		}
//	}
//
//
//	private class ReceiveRastreamentoUsuarioIntentsReceiver extends BroadcastReceiver {
//
////		public static final String TAG = "ReceiveRastreamentoUsuarioIntentsReceiver";
//
//		@Override
//		public void onReceive(Context context, Intent intent) {
//
//			//Log.d(TAG,"onReceive(): Intent received - " + intent.getAction());
//			if(intent != null){
//				if(intent.getAction().equals(OmegaConfiguration.REFRESH_SERVICE_RECEIVE_USUARIO_RASTREAMENTO_POSICAO)) {
//
//					Bundle vParameter = intent.getExtras();
//					if(vParameter != null){
//						Integer vTotalTuplasBaixadas = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_TOTAL_TUPLAS_BAIXADAS);
//						if(vTotalTuplasBaixadas != null){
//							totalTuplasBaixadas += vTotalTuplasBaixadas;
//						}
//					}
//				}
//			}
//		}
//	}
//
//}
