package app.omegasoftware.pontoeletronico.TabletActivities;

import java.util.ArrayList;
import java.util.Timer;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListPessoaEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectEmpresaPontoEletronicoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.DesbloquearRelogioDePonto;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoPonto;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.gpsnovo.ContainerLocalizacao;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSControler;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSServiceLocationListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class RelogioDePontoActivityMobile extends OmegaRegularActivity {
	private TextView txtResult; // Reference to EditText of result

	HandlerBloquear handlerBloquear ;
	
	private String inStr = ""; // Current input string
	// Previous operator: '+', '-', '*', '/', '=' or ' ' (no operator)
	Database db=null;
	EXTDAOPessoa objPessoa;
	EXTDAOProfissao objProfissao;
	// Search mode toggle buttons
	// private CheckBox entradaCk;
	// private CheckBox saidaCk;

	private String PREFIXO_NAO_SINCRONIZADO = null;

	// private Button microphoneButton;

	boolean isToTakePicture = false;
	String idEmpresa;
	String idPessoaEmpresa;
	String empresa;
	String pessoa;
	String idProfissao;
	String idPessoa;
	String profissao;

	HandlerTeclado handlerTeclado;
	String ultimoIdPonto;
	String dataDoUltimoPonto;
	String horaDoUltimoPonto;
	String ultimoArquivoDeFoto;
	String ultimaDescricaoPonto;
	
	LinearLayout llTeclado;

	// Button baterPonto;

	Timer uiUpdaterTimer;

	

	TextView tvEmpresa;
	LinearLayout llTipoPontoEletronico;
	

	


	public enum ESTADO_BATER_PONTO_PARA_UMA_EMPRESA {
		// PASSO 1
		TECLADO_ATIVO,
		// PASSO 2
		TIRANDO_FOTO
	}

	ESTADO_BATER_PONTO_PARA_UMA_EMPRESA estadoBaterPontoParaUmaEmpresa = ESTADO_BATER_PONTO_PARA_UMA_EMPRESA.TECLADO_ATIVO;


	public static final int FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA = 988;
	public static final int FORM_ERROR_DIALOG_GPS_FORA_DE_AREA = 989;
	// Constants
	public static final String TAG = "MenuPontoEletronicoSemFotoActivityMobile";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.relogio_de_ponto_activity_mobile_layout);
		PREFIXO_NAO_SINCRONIZADO = this.getString(R.string.prefixoNaoSincronizado);

		


		new CustomDataLoader(this).execute();
	}

	public void refreshBotaoBuscarId() {
		View vButton = null;
		try{
			vButton = findViewById(R.id.btn_ajuda_secundario);
			((Button)vButton ).setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View arg0) {
					Intent intent = new Intent(RelogioDePontoActivityMobile.this,
							ListPessoaEmpresaActivityMobile.class);
					// intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA,
					// idEmpresa);
					intent.putExtra(OmegaConfiguration.PARAM_SELECIONAR_ID, true);
					
					startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FILTER_PESSOA_EMPRESA);
				}
			});
		}catch(Exception ex){
			FactoryAlertDialog.makeToast(this, ex, null);
		}

	}

	@Override
	public boolean loadData() throws OmegaDatabaseException {
		Bundle vParameter = getIntent().getExtras();
		if (vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA)) {
			idEmpresa = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA);
		}else
			idEmpresa = EXTDAOEmpresa.getIdEmpresaParaRelogioDePonto(new DatabasePontoEletronico(this));
		return true;
	}

	public void refreshPessoaPonto() {

		if (idPessoaEmpresa != null) {
			EXTDAOPonto objPonto = new EXTDAOPonto(db);
			EXTDAOPonto.PontoPessoa pontoPessoa = objPonto.getUltimoPontoDaPessoaNaEmpresa(idPessoaEmpresa, idPessoa,
					idEmpresa, idProfissao);
			if (pontoPessoa == null)
				pontoPessoa = objPonto.factoryPontoPessoa(idPessoaEmpresa);

			pontoPessoa.pessoa = pessoa;
			pontoPessoa.empresa = empresa;
			pontoPessoa.profissao = profissao;
			pontoPessoa.idPessoa = idPessoa;
			pontoPessoa.idEmpresa = idEmpresa;
			pontoPessoa.idProfissao = idProfissao;
			pontoPessoa.idPessoaEmpresa = idPessoaEmpresa;
		} 

	}

	public void refreshHeaderSecundario() {
		View headerSecundario = findViewById(R.id.header);
		if (headerSecundario != null) {
			View viewTitle = headerSecundario.findViewById(R.id.tv_title);
			if (viewTitle != null) {
				TextView tvTitle = (TextView) viewTitle;
				
				tvTitle.setText(empresa);
				
			}
		}
	}
	final  int BLOQUEAR = 0;
	final int DESBLOQUEAR = 1;
	private void refreshBloquearDesbloquear(){

		try{

			((Button) findViewById(R.id.bloquear_button)).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					handlerBloquear.sendEmptyMessage(BLOQUEAR);
				}
			});
			
			((Button) findViewById(R.id.desbloquear_button)).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					try{
					
						
						Intent intent = new Intent(RelogioDePontoActivityMobile.this,
								DesbloquearRelogioDePonto.class);

						startActivityForResult(intent, OmegaConfiguration.ACTIVITY_DESBLOQUEAR_RELOGIO_DE_PONTO);
					}catch(Exception ex){
						FactoryAlertDialog.makeToast(RelogioDePontoActivityMobile.this, ex, null);
					}
				}
			});
			
		}catch(Exception ex){
			FactoryAlertDialog.makeToast(this, ex, null);
		}
	}
	
	@Override
	public void initializeComponents() {
		try {
			CustomServiceListener listener = new CustomServiceListener();
			
			refreshBotaoBuscarId();
			refreshBloquearDesbloquear();
			
			db = new DatabasePontoEletronico(this);
			objPessoa = new EXTDAOPessoa(db);
			objProfissao = new EXTDAOProfissao(db);

			handlerTeclado = new HandlerTeclado();

			
			handlerBloquear = new HandlerBloquear();
			
			llTipoPontoEletronico = (LinearLayout) findViewById(R.id.ll_tipo_ponto_eletronico);

			llTeclado = (LinearLayout) findViewById(R.id.ll_teclado);
			



			EXTDAOEmpresa vObjEmpresa = new EXTDAOEmpresa(db);
			vObjEmpresa.setAttrValue(EXTDAOEmpresa.ID, idEmpresa);
			if (vObjEmpresa.select()) {
				empresa = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.NOME);
			}

			// Retrieve a reference to the EditText field for displaying the
			// result.
			txtResult = (EditText) findViewById(R.id.txtResultId);
			apagarTeclado();

			String strBomDia = getApplicationContext().getString(R.string.bom_dia_ponto)+ " " + empresa;
			((TextView) findViewById(R.id.tv_bom_dia)).setText(strBomDia);

			refreshPessoaPonto();

			((Button) findViewById(R.id.btnNum0Id)).setOnClickListener(listener);
			((Button) findViewById(R.id.btnNum1Id)).setOnClickListener(listener);
			((Button) findViewById(R.id.btnNum2Id)).setOnClickListener(listener);
			((Button) findViewById(R.id.btnNum3Id)).setOnClickListener(listener);
			((Button) findViewById(R.id.btnNum4Id)).setOnClickListener(listener);
			((Button) findViewById(R.id.btnNum5Id)).setOnClickListener(listener);
			((Button) findViewById(R.id.btnNum6Id)).setOnClickListener(listener);
			((Button) findViewById(R.id.btnNum7Id)).setOnClickListener(listener);
			((Button) findViewById(R.id.btnNum8Id)).setOnClickListener(listener);
			((Button) findViewById(R.id.btnNum9Id)).setOnClickListener(listener);
			((Button) findViewById(R.id.btnTeclaNaoSincronizado)).setOnClickListener(listener);

			((Button) findViewById(R.id.bt_bater_ponto_entrada)).setOnClickListener(listener);
			((Button) findViewById(R.id.bt_bater_ponto_saida)).setOnClickListener(listener);
			((Button) findViewById(R.id.backspace_button)).setOnClickListener(listener);
			((Button) findViewById(R.id.btnTrocarEmpresa)).setOnClickListener(listener);

			refreshHeaderSecundario();
			
			boolean bloqueado = getIntent().getBooleanExtra(OmegaConfiguration.PARAM_BLOQUEADO, false);
			
			handlerBloquear.sendEmptyMessage(bloqueado ? BLOQUEAR : DESBLOQUEAR);
			
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);

		}
	}
	
	
	public class HandlerTeclado extends Handler {

		@Override
		public void handleMessage(Message msg) {
		
			if (estadoBaterPontoParaUmaEmpresa == ESTADO_BATER_PONTO_PARA_UMA_EMPRESA.TECLADO_ATIVO) {
				// refreshPessoaPonto();
				estadoBaterPontoParaUmaEmpresa = ESTADO_BATER_PONTO_PARA_UMA_EMPRESA.TIRANDO_FOTO;
				llTeclado.setVisibility(View.GONE);

			} else {
				idProfissao = null;
				profissao = null;
				idPessoa = null;
				pessoa = null;
				idPessoaEmpresa = null;
				apagarTeclado();
				refreshPessoaPonto();
				estadoBaterPontoParaUmaEmpresa = ESTADO_BATER_PONTO_PARA_UMA_EMPRESA.TECLADO_ATIVO;
				llTeclado.setVisibility(View.VISIBLE);
			}
		}	
	}

	Integer tipoCamera = null;

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		try {
			switch (id) {
			case FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA:
				vTextView.setText(getResources().getString(R.string.ponto_pessoa_inexistente_na_empresa));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA);

					}
				});
				break;
			case FORM_ERROR_DIALOG_GPS_FORA_DE_AREA:
				vTextView.setText(getResources().getString(R.string.gps_fora_de_area));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_GPS_FORA_DE_AREA);

					}
				});
				break;

			default:
				return super.onCreateDialog(id);
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
			return null;
		}
		return vDialog;

	}

	

	private void baterPonto(final View view, final boolean entrada) {
		String vTextConfirmacao = EXTDAOPonto.getTextoDescritivo(this, entrada, pessoa, empresa);
		ultimaDescricaoPonto = vTextConfirmacao;
		if (!GPSServiceLocationListener.isActiveGPS())
			showDialog(FORM_ERROR_DIALOG_GPS_FORA_DE_AREA);
		else if (vTextConfirmacao == null || vTextConfirmacao.length() == 0)
			showDialog(FORM_ERROR_DIALOG_PESSOA_INEXISTENTE_NA_EMPRESA);
		else {
			Intent intent = new Intent(this, ConfirmarPontoTimerActivityMobile.class);
			intent.putExtra(OmegaConfiguration.PARAM_ENTRADA, entrada);
			intent.putExtra(OmegaConfiguration.PARAM_DESCRICAO_PONTO, vTextConfirmacao);

			startActivityForResult(intent, OmegaConfiguration.ACTIVITY_CONFIRMAR_PONTO);

		}
	}

	

	private void baterPontoParaUmaEmpresa(View view, boolean entrada) throws Exception {

		idPessoa = txtResult.getText().toString();
		idPessoa = idPessoa.replace(PREFIXO_NAO_SINCRONIZADO, "-");
		if (!objPessoa.select(idPessoa)) {
			FactoryAlertDialog.showAlertDialog(RelogioDePontoActivityMobile.this,
					RelogioDePontoActivityMobile.this.getString(R.string.id_nao_encontrado),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							if (which == DialogInterface.BUTTON_POSITIVE) {
								Intent intent = new Intent(RelogioDePontoActivityMobile.this,
										ListPessoaEmpresaActivityMobile.class);

								intent.putExtra(OmegaConfiguration.PARAM_SELECIONAR_ID,  true);
								startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FILTER_PESSOA_EMPRESA);
							}

						}
					});
		} else {
			ArrayList<Table> registros = objPessoa.getPessoasEmpresa(idEmpresa);
			if (registros == null || registros.size() == 0) {
				registros = new ArrayList<Table>();
				registros.add(EXTDAOPessoaEmpresa.cadastrarProfissaoIndefinidaDaPessoa(db, idEmpresa, idPessoa));
			}

			Table registroPessoaEmpresa = registros.get(0);

			idPessoaEmpresa = registroPessoaEmpresa.getId();
			idProfissao = registroPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT);
			Table registroProfissao = registroPessoaEmpresa
					.getObjDaChaveExtrangeira(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT);
			if (registroProfissao != null)
				profissao = registroProfissao.getStrValueOfAttribute(EXTDAOProfissao.NOME);
			pessoa = objPessoa.getDescricao();

			// handlerTeclado.sendEmptyMessage(0);
			baterPonto(view, entrada);

		}
	}

	private class CustomServiceListener implements OnClickListener {

		// On-click event handler for all the buttons

		public void onClick(final View view) {
			try {
				int vStartCursor = 0;
				int vEndCursor = 0;
				switch (view.getId()) {
				case R.id.btnTeclaNaoSincronizado:
					if (!inStr.contains(RelogioDePontoActivityMobile.this.PREFIXO_NAO_SINCRONIZADO)) {
						inStr = RelogioDePontoActivityMobile.this.PREFIXO_NAO_SINCRONIZADO + inStr;
						txtResult.setText("");
						txtResult.append(inStr);
					} else {
						inStr = inStr.replace(PREFIXO_NAO_SINCRONIZADO, "");
						txtResult.setText("");
						txtResult.append(inStr);
					}

					break;
				// Number buttons: '0' to '9'
				case R.id.btnNum0Id:
				case R.id.btnNum1Id:
				case R.id.btnNum2Id:
				case R.id.btnNum3Id:
				case R.id.btnNum4Id:
				case R.id.btnNum5Id:
				case R.id.btnNum6Id:
				case R.id.btnNum7Id:
				case R.id.btnNum8Id:
				case R.id.btnNum9Id:
					if (inStr.length() > 11)
						break;
					// boolean naoSincronizado = false;
					// if(inStr.contains(MenuPontoEletronicoSemFotoActivityMobile.this.PREFIXO_NAO_SINCRONIZADO)){
					// inStr = inStr.replace('*', '\0');
					// naoSincronizado = true;
					// }
					String inDigit = ((Button) view).getText().toString();

					// String bkp = inStr;
					// boolean sincronizado =
					// inStr.contains(MenuPontoEletronicoSemFotoActivityMobile.this.PREFIXO_NAO_SINCRONIZADO);
					// inStr =
					// inStr.replace(MenuPontoEletronicoSemFotoActivityMobile.this.PREFIXO_NAO_SINCRONIZADO,
					// "");
					vStartCursor = txtResult.getSelectionStart();
					vEndCursor = txtResult.getSelectionEnd();

					if (vStartCursor != vEndCursor) {
						String vInfText = inStr.substring(0, vStartCursor);
						String vSupText = inStr.substring(vEndCursor);
						inStr = vInfText + vSupText;
						// Atualizando o ponteiro para a nova posicao
						vStartCursor = vEndCursor - (vEndCursor - vStartCursor);
					}
					if (vStartCursor == inStr.length()) {
						inStr += inDigit;
						// if(naoSincronizado)
						// inStr = '*' + inStr;
						txtResult.append(inDigit);
						// txtResult.setText(inStr);
					} else {
						String vInfText = inStr.substring(0, vStartCursor) + inDigit;
						String vSupText = inStr.substring(vStartCursor);

						txtResult.setText("");
						txtResult.append(vInfText);
						inStr = vInfText + vSupText; // accumulate input digit
						txtResult.setTextKeepState(inStr);
					}

					break;

				// Operator buttons: '+', '-', '*', '/' and '='
				case R.id.bt_temporizador:
				case R.id.bt_bater_ponto_entrada:
					baterPontoParaUmaEmpresa(view, true);
					break;
				case R.id.bt_bater_ponto_saida:
					
					baterPontoParaUmaEmpresa(view, false);
					

					break;

				case R.id.backspace_button:
					if (txtResult.length() == 0)
						return;
					vStartCursor = txtResult.getSelectionStart();
					vEndCursor = txtResult.getSelectionEnd();
					if (vStartCursor != vEndCursor) {
						String vInfText = inStr.substring(0, vStartCursor);
						String vSupText = inStr.substring(vEndCursor);
						inStr = vInfText + vSupText;
						// Atualizando o ponteiro para a nova posicao
						vStartCursor = vEndCursor - (vEndCursor - vStartCursor);
					} else if (vStartCursor == inStr.length() && inStr.length() > 0) {
						inStr = inStr.substring(0, inStr.length() - 1);
						txtResult.setText("");
						txtResult.append(inStr);
						// txtResult.setText(inStr);
					} else if (vStartCursor > 0) {
						String vInfText = inStr.substring(0, vStartCursor - 1);
						String vSupText = inStr.substring(vStartCursor);
						txtResult.setText("");
						txtResult.append(vInfText);
						inStr = vInfText + vSupText; // accumulate input digit
						txtResult.setTextKeepState(inStr);
					}

					break;

				// Clear button
				case R.id.apagar_tudo_button:

					apagarTeclado();
					break;
				case R.id.btnTrocarEmpresa:

					
					Intent intent = new Intent(getApplicationContext(), SelectEmpresaPontoEletronicoActivityMobile.class);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, idEmpresa);
					startActivityForResult(intent, OmegaConfiguration.STATE_SELECT_EMPRESA);
					break;
				}
			} catch (Exception ex) {
				SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);

			}
		}

	}

	private void setIdPessoaNoTeclado(long id) {
		String strId = String.valueOf(id);
		if(id<0) strId = strId.replace('-', '#');
		inStr = strId;
		txtResult.setText(strId);
	}
	
	private void apagarTeclado() {
		inStr = "";
		txtResult.setText("");
	}

	
	private void salvarNoBancoDeDados(final boolean entrada) throws Exception {

		EXTDAOPonto objPonto = new EXTDAOPonto(db);
		objPonto.setAttrValue(EXTDAOPonto.PESSOA_ID_INT, idPessoa);
		EXTDAOTipoPonto.TIPO_PONTO tipoPonto = EXTDAOTipoPonto.TIPO_PONTO.TURNO;

		objPonto.setAttrValue(EXTDAOPonto.FOTO, ultimoArquivoDeFoto);

		HelperDate helperDate = new HelperDate();
		dataDoUltimoPonto = helperDate.getDateDisplay();
		horaDoUltimoPonto = helperDate.getTimeDisplay();

		long utc = HelperDate.getTimestampSegundosUTC(this);
		int offset = HelperDate.getOffsetSegundosTimeZone();
		objPonto.setAttrValue(EXTDAOPonto.DATA_SEC, String.valueOf(utc));
		objPonto.setAttrValue(EXTDAOPonto.DATA_OFFSEC, String.valueOf(offset));
		objPonto.setAttrValue(EXTDAOPonto.EMPRESA_ID_INT, idEmpresa);
		String idUsuario = OmegaSecurity.getIdUsuario();
		objPonto.setAttrValue(EXTDAOPonto.USUARIO_ID_INT, idUsuario);
		objPonto.setAttrValue(EXTDAOPonto.IS_ENTRADA_BOOLEAN, HelperString.valueOfBooleanSQL(entrada));

		String strIdTipoPonto = String.valueOf(tipoPonto.getId());
		objPonto.setAttrValue(EXTDAOPonto.TIPO_PONTO_ID_INT, strIdTipoPonto);
		int latitude = 0;
		int longitude = 0;
		ContainerLocalizacao vContainerLocalizacao = GPSControler.getLocalizacao();
		if (vContainerLocalizacao != null) {
			latitude = vContainerLocalizacao.getLatitude();
			longitude = vContainerLocalizacao.getLongitude();

			objPonto.setAttrValue(EXTDAOPonto.LATITUDE_INT, String.valueOf(latitude));
			objPonto.setAttrValue(EXTDAOPonto.LONGITUDE_INT, String.valueOf(longitude));

		}
		Long lIdProfissao = EXTDAOPessoaEmpresa.getIdProfissaoDaPessoa(db, idEmpresa, idPessoa);
		//Cadastra o registro de pessoa caso nao exista
		idProfissao = lIdProfissao  != null ? lIdProfissao .toString() : null;
				
		objPonto.setAttrValue(EXTDAOPonto.PROFISSAO_ID_INT, idProfissao);

		objPonto.formatToSQLite();

		
		
		ultimoIdPonto = String.valueOf(objPonto.insert(true));

		if (ultimoIdPonto == null)
			throw new Exception("Falha ao inserir o registro de ponto");

		EXTDAOPonto.factoryToastPontoBatido(getApplicationContext());
		apagarTeclado();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		try {
			switch (requestCode) {
			case OmegaConfiguration.ACTIVITY_FILTER_PESSOA_EMPRESA:
				if(resultCode == RESULT_OK){
					String strIdPessoa= intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA);
					
					Long idPessoa = HelperInteger.parserLong(strIdPessoa);
					if(idPessoa != null){
						setIdPessoaNoTeclado(idPessoa);
					}
				}
				break;
			case OmegaConfiguration.ACTIVITY_CONFIRMAR_PONTO:
				Bundle pParameter = intent.getExtras();
				if (!pParameter.getBoolean(OmegaConfiguration.PARAM_CANCELAR, false)) {
					Boolean entrada = pParameter.getBoolean(OmegaConfiguration.PARAM_ENTRADA);
					
					salvarNoBancoDeDados(entrada);
					EXTDAOPonto.factoryToastPontoBatido(getApplicationContext());
				}
				break;

			case OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW:
			case OmegaConfiguration.STATE_FORM_MARCA_PONTO:
				if (resultCode == RESULT_OK) {
					ultimoIdPonto = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID);

					apagarTeclado();
				}

				break;
			case OmegaConfiguration.STATE_SELECT_EMPRESA:
				if(resultCode == RESULT_OK){
					idEmpresa = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA);
					getIntent().removeExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA);
					getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, idEmpresa);
					new CustomDataLoader(this).execute();					
				}
				break;
			case OmegaConfiguration.ACTIVITY_DESBLOQUEAR_RELOGIO_DE_PONTO:
				if(resultCode == RESULT_OK){
					handlerBloquear.sendEmptyMessage(DESBLOQUEAR);					
				}
			default:
				break;
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
		}

	}

	
	
	
	
	public class HandlerBloquear extends Handler {		
		
		@Override
		public void handleMessage(Message msg) {
		
			try{
				boolean bloqueado = msg.what == BLOQUEAR ? true : false;
				
				
				getIntent().putExtra(OmegaConfiguration.PARAM_BLOQUEADO, bloqueado);
				
				if(bloqueado){
					findViewById(R.id.setting_button).setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
					findViewById(R.id.btn_voltar_desktop).setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
					
					findViewById(R.id.btnTrocarEmpresa).setVisibility(View.GONE);
					
					findViewById(R.id.desbloquear_button).setVisibility(View.VISIBLE);
					findViewById(R.id.bloquear_button).setVisibility(View.GONE);
					
					
					
					
					
				} else {
					findViewById(R.id.setting_button).setVisibility(View.VISIBLE);
					findViewById(R.id.btn_voltar_desktop).setVisibility(View.VISIBLE);
					findViewById(R.id.btnTrocarEmpresa).setVisibility(View.VISIBLE);
					
					findViewById(R.id.desbloquear_button).setVisibility(View.GONE);
					findViewById(R.id.bloquear_button).setVisibility(View.VISIBLE);
					
					
					
					
					
				}
			}catch(Exception ex){
				FactoryAlertDialog.showDialog(RelogioDePontoActivityMobile.this, ex);
			}
		}	
	}
	

    @Override
	public void onBackPressed(){
    	boolean bloqueado = getIntent().getBooleanExtra(OmegaConfiguration.PARAM_BLOQUEADO, false);
		if(bloqueado){
			FactoryAlertDialog.showDialog(this, getString(R.string.tela_relogio_ponto_bloqueada));
		}else 		
			super.onBackPressed();
	}
	
}
