package app.omegasoftware.pontoeletronico.TabletActivities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.common.thread.HelperThread;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class ResetarActivityMobile  extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "ResetarActivityMobile";
	StatusHandler statusHandler;
	private TextView carregandoTextView;
	private ProgressBar progressBar;
	//Download button
	private Button resetarButton;
	
	
	
	public static final int FALHA_AO_RESETAR_DESEJA_RESETAR_A_FORCA = 1;
	ResetarButtonHandler handlerTexto;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.resetar_activity_mobile_layout);
		

		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {

		return true;
	}

	
	DialogInterface.OnClickListener dialogResetarClickListener;
	@Override
	public void initializeComponents() {
		statusHandler= new StatusHandler();
		dialogResetarClickListener = new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int which) {
		        switch (which){
		        case DialogInterface.BUTTON_POSITIVE:
		        	procedimentoResetar(true);
		            break;

		        case DialogInterface.BUTTON_NEGATIVE:
		        	handlerTexto.sendEmptyMessage(ResetarButtonHandler.FALSE);
		            break;
		        }
		    }
		};
		
		
		setTituloCabecalho(R.string.resetar);
		resetarButton = (Button) findViewById(R.id.resetar_button);
		progressBar = (ProgressBar) findViewById(R.id.progress_bar);
		carregandoTextView = (TextView) findViewById(R.id.carregando_textview);
		carregandoTextView.setVisibility(View.VISIBLE);
		
		progressBar.setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
	
		handlerTexto = new ResetarButtonHandler(this);
		
		
		resetarButton.setOnClickListener(new ResetarButtonListener());
		
		

		return ;					
	}

	@Override
	public void onBackPressed(){
		Intent intent = getIntent();
		
		setResult(RESULT_OK, intent);
		finish();
	}
	
	@Override
	public void finish(){
		
		super.finish();
	}

	public class StatusHandler extends Handler{
		
		@Override
        public void handleMessage(Message msg)
        {
			Bundle b= msg.getData();
			carregandoTextView.setText(b.getString("status"));
        }
	}
	
	public class ResetarButtonHandler extends Handler{
		
		public static final int TRUE = 1;
		public static final int FALSE = 0;
		
		Activity activity;
		public ResetarButtonHandler(Activity pActivity){
			activity = pActivity;
		}
		
		@Override
        public void handleMessage(Message msg)
        {
			switch (msg.what) {
			case TRUE:
				resetarButton.setVisibility(View.GONE);
				progressBar.setVisibility(View.VISIBLE);
				break;
			case FALSE:
				resetarButton.setVisibility(View.VISIBLE);
				progressBar.setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
				break;
			default:
				break;
			}
        }
	}
	
	public void procedimentoResetar(boolean resetarAForca){
		try{
			new ResetarLoader(resetarAForca).execute();
			handlerTexto.sendEmptyMessage(ResetarButtonHandler.TRUE);
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}
		
	}
	

	public class ResetarLoader extends AsyncTask<Bundle, Integer, InterfaceMensagem>
	{
		
		
		boolean resetarAForca = false;
		
		
		public ResetarLoader( boolean resetarAForca ) throws Exception{
			
			this.resetarAForca = resetarAForca;
		
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try{
			atualizaStatusSincronizacao("Inicializando sincronizacao...");
			}catch(Exception ex){
				SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
			}
		}
		
		@Override
		protected InterfaceMensagem doInBackground(Bundle... params) {
			
			
			return RotinaSincronizador.procedimentoResetar(ResetarActivityMobile.this, resetarAForca);
		}

		@Override
		protected void onPostExecute(InterfaceMensagem msg) {
			super.onPostExecute(msg);
			try{
			if(msg == null )
				msg= new Mensagem(
						PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, 
						ResetarActivityMobile.this.getString(R.string.erro_fatal));
			
			atualizaStatusSincronizacao(msg.mMensagem);

			if( msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()
					&& !resetarAForca){
				String strMsg = "";
				if(msg.erro())
					strMsg = "";
				else strMsg = msg.mMensagem + ". " ;
				FactoryAlertDialog.showAlertDialog(
						ResetarActivityMobile.this, 
						strMsg+ ResetarActivityMobile.this.getString(R.string.resetar_erro),
						dialogResetarClickListener);
				
			}else if(msg.ok()){
				HelperThread.sleep(3000);
				RotinaSincronizador.actionLogout(ResetarActivityMobile.this, true);
			} else {
				SingletonLog.insereErro(new Exception("Situacao nao planejada"), TIPO.UTIL_ANDROID);
			}
			} catch (Exception ex) {
				SingletonLog.openDialogError(ResetarActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
		}
		
		
		private void atualizaStatusSincronizacao(String mensagem){
			try{
				if(mensagem == null || mensagem.length() == 0) mensagem = "-";
				Message message = new Message();
				Bundle b = new Bundle();
				b.putString("status", mensagem);
				message.setData(b);
				statusHandler.sendMessage(message );	
			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
			}
			
		}
	}
	
//	
//	public class ResetarLoader extends AsyncTask<Bundle, Integer, Boolean>
//	{
//		
//		InterfaceMensagem msg = null;
//		boolean resetarAForca = false;
//		Context context = null;
//		public ResetarLoader( boolean resetarAForca ,Context context ) throws Exception{
//			
//			this.resetarAForca = resetarAForca;
//			this.context = context;
//		}
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			atualizaStatusSincronizacao("Inicializando sincronizacao...");
//		}
//		
//		@Override
//		protected Boolean doInBackground(Bundle... params) {
//			msg = null;
//			ControlerServicoInterno controladorServicoInterno = null;
//			ControladorServicoInterno controladorSI = null;
//			try{
//				boolean validade = false;
//				//Pausando todos os serviuos do OmegaEmpresa
//				controladorServicoInterno = ControlerServicoInterno.constroi(ResetarActivityMobile.this);	
//				controladorServicoInterno.stopAllServices();
//				//Pausando todos os serviuos da Biblioteca Nuvem
//				controladorSI = ControladorServicoInterno.constroi(ResetarActivityMobile.this);
//				controladorSI.stopAllServices();	
//				
//				Thread.sleep(1000);
//				
//				Database db = null;
//				try{
//					STATE_SINCRONIZADOR state = RotinaSincronizador.getStateSincronizador(context, null);
//					if(state == STATE_SINCRONIZADOR.COMPLETO){
//						db = new DatabasePontoEletronico(context);
//						SincronizadorEspelho syncEspelho ;	
//						syncEspelho = SincronizadorEspelho.getInstance(ResetarActivityMobile.this);
//						SincronizadorEspelho.Item item = syncEspelho.getItem(ResetarActivityMobile.this);
//						if(item.estado != null){
//							if(item.estado == ESTADO_SINCRONIZACAO_MOBILE.FILA_ESPERA_PARA_SINCRONIZACAO){
//								msg = cicloSincronizacao(syncEspelho);
//								if(msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId()){
//									msg = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, "A sincronizacao dos dados locais nuo foi realizada. Devido a uma falha do fluxo interno.");
//								} else 
//									msg = cicloSincronizacao(syncEspelho);
//							} if(item.estado == ESTADO_SINCRONIZACAO_MOBILE.OCORREU_UM_ERRO_FATAL
//									|| item.estado == ESTADO_SINCRONIZACAO_MOBILE.OCORREU_UM_ERRO_LEVE_QUE_SO_ENVIARA_OS_DADOS_LOCAIS_QUANDO_RESETAR
//									|| item.estado == ESTADO_SINCRONIZACAO_MOBILE.SETANDO_ERRO_FATAL_NA_WEB){
//								msg = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, "A sincronizacao dos dados locais nuo foi realizada. Devido a uma falha do fluxo interno.");
//							} else if(!msg.ok()){
//								msg = cicloSincronizacao(syncEspelho);
//							}
//						}
//						if(msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()
//								|| msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO_A_AREA_LOGADA.getId()
//								|| resetarAForca){
//							validade = RotinaSincronizador.resetar(
//									ResetarActivityMobile.this,
//									resetarAForca);
//							if(!validade){
//								msg = new Mensagem(
//									PROTOCOLO_SISTEMA.TIPO.ERRO_PROCESSO_RESETAR, 
//									"Ocorreu um erro durante o processo de formatauuo.");
//							}
//							else {
//								msg = new Mensagem(
//									PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, 
//									"O processo de resetar o banco foi realizado com sucesso.");
//							}
//							resetarAForca = false;
//							
//							return validade;
//						} else return false;
//					} else {
//						validade = RotinaSincronizador.resetar(
//								ResetarActivityMobile.this,
//								true);
//						if(!validade){
//							msg = new Mensagem(
//								PROTOCOLO_SISTEMA.TIPO.ERRO_PROCESSO_RESETAR, 
//								"Ocorreu um erro durante o processo de formatauuo.");
//						}
//						else {
//							msg = new Mensagem(
//								PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, 
//								"O processo de resetar o banco foi realizado com sucesso.");
//						}
//						return validade;
//					}
//				}finally{
//					if(db != null) db.close();
//				}
//			}
//			catch(Exception e)
//			{
//				
//				SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
//				boolean validade = false;
//				if(resetarAForca){
//					validade = RotinaSincronizador.resetar(
//							ResetarActivityMobile.this,   
//							true);
//					if(validade){
//						msg = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId(), "Reset realizado com sucesso");
//					}	
//				}
//				if(!validade)
//					msg = new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_PROCESSO_RESETAR.getId(), "Falha durante a sincronizacao, para concluir a operauuo de reset desinstale e instale novamente a aplicauuo");
//				
//				return false;
//			} finally{
//				HelperThread.sleep(1000);
//				
//				if(msg == null 
//					|| (
//						msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()
//						&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.ERRO_PROCESSO_RESETAR.getId())){
//					RotinaSincronizador.logout(ResetarActivityMobile.this);
//					if(controladorServicoInterno != null)
//						controladorServicoInterno.startAllServicesNaoAutenticados();
//				}
//			}
//		}
//
//		@Override
//		protected void onPostExecute(Boolean result) {
//			super.onPostExecute(result);
//			
//			if(msg != null )atualizaStatusSincronizacao(msg.mMensagem);
//			if(msg != null && msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ERRO_PROCESSO_RESETAR.getId() && !resetarAForca){
//				FactoryAlertDialog.showAlertDialog(
//						ResetarActivityMobile.this, 
//						msg.mMensagem + ". " + ResetarActivityMobile.this.getString(R.string.resetar_erro),
//						dialogResetarClickListener);
//				
//			}else if(msg == null || msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()){
//				RotinaSincronizador.logout(ResetarActivityMobile.this);
//			}
//			handlerTexto.sendEmptyMessage(ResetarButtonHandler.FALSE);
//		}
//		
//		public InterfaceMensagem cicloSincronizacao(SincronizadorEspelho syncEspelho ) throws InterruptedException{
//			InterfaceMensagem msg = null;
//			boolean executada = false;
//			while(msg == null 
//					||
//					(
//						msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId()
//						&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO.getId()
//						&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()
//						&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.SERVIDOR_FORA_DO_AR.getId()
//						&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.SEM_CONEXAO_A_INTERNET.getId()
//						&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO_A_AREA_LOGADA.getId()
//					)
//			){
//				if(executada) Thread.sleep(1000);
//				else executada = true;
//				msg = syncEspelho.executa(true, true);	
//			}
//			return msg;
//		}
//		
//		private void atualizaStatusSincronizacao(String mensagem){
//			if(mensagem == null || mensagem.length() == 0) return;
//			Message message = new Message();
//			Bundle b = new Bundle();
//			b.putString("status", mensagem);
//			message.setData(b);
//			statusHandler.sendMessage(message );
//		}
//	}
//	
	private class ResetarButtonListener implements OnClickListener
	{
		

		public void onClick(View v) {
			procedimentoResetar(false);
		}

	}	


}
