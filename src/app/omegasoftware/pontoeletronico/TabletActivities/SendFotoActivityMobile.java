package app.omegasoftware.pontoeletronico.TabletActivities;

import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;

public class SendFotoActivityMobile  extends OmegaRegularActivity {

	@Override
	public boolean loadData() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void initializeComponents() {
		// TODO Auto-generated method stub
		
	}

	//Constants
//	public static final String TAG = "SendFotoActivityMobile";
//	public static final String LIMIT_TUPLA = "15";
//	private TextView carregandoTextView;
//	private ProgressBar progressBar;
//	//Download button
//	private Button enviaFotosButton;
//	boolean isEnviandoFoto = false;
//	int totalFileToSend;
//	private TextView numeroFotoTextView;
//	Database db ;
//	Context context ;
//	private Typeface customTypeFace;
//	private NumeroFotoHandler handlerNumeroFoto;
//	EnviaFotoButtonHandler handlerEnviaFoto;
//	EXTDAOSistemaSincronizadorArquivo objSincronizadorFile;
//	FileWriterHttpPostManager filesManager ;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
//		super.onCreate(savedInstanceState);
//
//		setContentView(R.layout.send_foto_activity_mobile_layout);
//
//		db = new DatabasePontoEletronico(this);
//		objSincronizadorFile = new EXTDAOSistemaSincronizadorArquivo(db);
//
//		this.customTypeFace = Typeface.createFromAsset(this.getAssets(),"trebucbd.ttf");
//
//		new CustomDataLoader(this).execute();
//	}
//
//	@Override
//	public boolean loadData() {
//
//		return true;
//	}
//
//	public void formatarStyle(){
//		
//		((TextView) findViewById(R.id.numero_foto_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.titulo_numero_foto_textview)).setTypeface(this.customTypeFace);
//	}
//	
//	@Override
//	public void initializeComponents() {
//		
//		numeroFotoTextView = ((TextView) findViewById(R.id.numero_foto_textview));
//		enviaFotosButton = (Button) findViewById(R.id.envia_fotos_button);
//		progressBar = (ProgressBar) findViewById(R.id.progress_bar);
//		carregandoTextView = (TextView) findViewById(R.id.carregando_textview);
//		carregandoTextView.setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
//		progressBar.setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
//		handlerNumeroFoto = new NumeroFotoHandler();
//		handlerEnviaFoto = new EnviaFotoButtonHandler(this);
//		numeroFotoTextView.setText(String.valueOf( getNumberFileToUpload()));
//		
//		OmegaFileConfiguration vConfiguration = new OmegaFileConfiguration();
////		filesManager = new FileWriterHttpPostManager(
////				this,
////				OmegaConfiguration.URL_REQUEST_SEND_FILE_PHOTO(),
////				vConfiguration.getPathFilePhotoToSend(), 
////				"");
//		
//		enviaFotosButton.setOnClickListener(new EnviaFotoButtonListener(this, R.id.envia_fotos_button));
//		formatarStyle();
//		//Log.d(TAG,"initializeComponents(): Start refreshing all information");
//		return ;					
//	}
//
//	@Override
//	public void onBackPressed(){
//		Intent intent = getIntent();
//		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO, totalFileToSend);
//		setResult(RESULT_OK, intent);
//		finish();
//	}
//	
//	public int getNumberFileToUpload(){
//		
//		totalFileToSend = objSincronizadorFile.getCountOfFileToSend();
//		
//		return totalFileToSend;
//	}
//	@Override
//	public void finish(){
//		db.close();
//		super.finish();
//	}
//	
//	public class SearchPerformer extends AsyncTask<Bundle, Integer, Boolean>
//	{
//		Activity activity;
//		int idBotao;
//		public SearchPerformer(Activity pActivity, int pButtonId){
//			activity = pActivity;
//			idBotao = pButtonId;
//		}
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//		}
//		
//		@Override
//		protected Boolean doInBackground(Bundle... params) {
//			try{
//				switch (idBotao) {
//
//				case R.id.envia_fotos_button:
//					
//					while(isEnviandoFoto && totalFileToSend > 0){
//						int vNumberFileSent = EXTDAOSistemaSincronizadorArquivo.procedureSendFileAndRemoveTuplaInDatabase(objSincronizadorFile, filesManager, LIMIT_TUPLA);
//						totalFileToSend -= vNumberFileSent;
//						if(totalFileToSend <= 0 )
//							totalFileToSend = 0;
//						handlerNumeroFoto.sendEmptyMessage(0);
//					}
//					break;
//
//				default:
//					break;
//				}
//
//				return true;
//			}
//			catch(Exception e)
//			{
//				SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
//				//Log.e(TAG, e.getMessage());			
//			}
//
//			return false;
//
//		}
//
//		@Override
//		protected void onPostExecute(Boolean result) {
//			super.onPostExecute(result);
//			isEnviandoFoto = false;
//			handlerEnviaFoto.sendEmptyMessage(EnviaFotoButtonHandler.FALSE);
//						
//		}
//	}
//	public class NumeroFotoHandler extends Handler{
//		
//		@Override
//        public void handleMessage(Message msg)
//        {
//			numeroFotoTextView.setText(String.valueOf(totalFileToSend));
//        }
//	}
//	
//	public class EnviaFotoButtonHandler extends Handler{
//		
//		public static final int TRUE = 1;
//		public static final int FALSE = 0;
//		Activity activity;
//		public EnviaFotoButtonHandler(Activity pActivity){
//			activity = pActivity;
//		}
//		
//		@Override
//        public void handleMessage(Message msg)
//        {
//			switch (msg.what) {
//			case TRUE:
//				String vStrParar = activity.getResources().getString(R.string.parar);
//				enviaFotosButton.setText(vStrParar);
//				enviaFotosButton.setBackgroundResource(R.drawable.pause);
//				carregandoTextView.setVisibility(View.VISIBLE);
//				progressBar.setVisibility(View.VISIBLE);
//				break;
//			case FALSE:
//				String vStrEnviaFotos = activity.getResources().getString(R.string.envia_fotos);
//				enviaFotosButton.setText(vStrEnviaFotos);
//				//enviaFotosButton.setBackgroundResource(R.drawable.search_button);
//				carregandoTextView.setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
//				progressBar.setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
//				break;
//			default:
//				break;
//			}
//        }
//	}
//	
//	private class EnviaFotoButtonListener implements OnClickListener
//	{
//		Activity activity;
//		int idBotao;
//		public EnviaFotoButtonListener(Activity pActivity, int pIdBotao){
//			activity = pActivity;
//			idBotao = pIdBotao;
//		}
//
//		public void onClick(View v) {
//			if(!isEnviandoFoto){
//				isEnviandoFoto = true;
//				new SearchPerformer(activity,idBotao).execute();
//				handlerEnviaFoto.sendEmptyMessage(EnviaFotoButtonHandler.TRUE);
//			} else {
//				isEnviandoFoto = false;
//				handlerEnviaFoto.sendEmptyMessage(EnviaFotoButtonHandler.FALSE);
//			}
//		}
//
//	}	


}
