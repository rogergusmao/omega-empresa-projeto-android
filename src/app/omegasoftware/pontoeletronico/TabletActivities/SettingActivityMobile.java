package app.omegasoftware.pontoeletronico.TabletActivities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SuporteActivityMobile;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.AlterarSenhaUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;

public class SettingActivityMobile extends OmegaRegularActivity {

	// Constants
	public static final String TAG = "SettingActivityMobile";

	private final int FORM_ERROR_DIALOG_REMOCAO_OK = 3;

	private final int FORM_ERROR_DIALOG_SERVER_OFFLINE = 5;
	private final int FORM_ERROR_DIALOG_REGISTRO_JA_REMOVIDO = 6;
	protected final int FORM_ERROR_DIALOG_POST = 76;

	// EditText
	protected Button trocarUsuarioButton;
	protected Button sairButton;
	protected Button enviarErroButton;
	protected Button enviarMelhoriaButton;
	protected Button suporteButton;
	// protected Button ajudaButton;

	private String tagTela;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_activity_mobile_layout);
		Bundle vParameter = getIntent().getExtras();
		if (vParameter != null) {
			if (vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_TAG))
				tagTela = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_TAG);
		}
		

		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {

		return true;
	}


	@Override
	public void initializeComponents() {
		ServicoClickListener vClickListener = new ServicoClickListener(this);
		trocarUsuarioButton= (Button) findViewById(R.id.trocar_usuario_button);
		sairButton= (Button) findViewById(R.id.sair_button);
//		ajudaButton= (Button) findViewById(R.id.ajuda_button);
		enviarErroButton = (Button) findViewById(R.id.setting_enviar_erro_ocorrido_button);
		enviarMelhoriaButton = (Button) findViewById(R.id.setting_enviar_ideia_melhoria_button);
		suporteButton= (Button) findViewById(R.id.setting_suporte_button);
		
		((Button) findViewById(R.id.alterar_senha_button)).setOnClickListener(vClickListener);
		
		((Button) findViewById(R.id.sincronizar_button)).setOnClickListener(vClickListener);
		
		sairButton.setOnClickListener(vClickListener);
//		ajudaButton.setOnClickListener(vClickListener);
		enviarErroButton.setOnClickListener(vClickListener);
		enviarMelhoriaButton.setOnClickListener(vClickListener);
		suporteButton.setOnClickListener(vClickListener);
		trocarUsuarioButton.setOnClickListener(vClickListener);
		this.formatarStyle();
	}

	private void procedimentoLogout() {
		try {
			// MenuAdapter.clear();

			RotinaSincronizador.actionLogout(this, false);
			Database.closeLog();
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			// Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n
			// StackTrace:" + ex.toString());
		}
	}

	private void procedimentoTrocarUsuario() {
		try {
			// MenuAdapter.clear();

			RotinaSincronizador.actionLogout(this, true);

			Database.closeLog();
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			// Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n
			// StackTrace:" + ex.toString());
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if (requestCode == OmegaConfiguration.ACTIVITY_SETTING_SAIR) {

			procedimentoLogout();
		} else if (requestCode == OmegaConfiguration.ACTIVITY_SETTING_AJUDA) {

		}
	}

	protected void procedureAlertAfterDeleteTupla(String pId) {

	}

	private class ServicoClickListener implements OnClickListener {
		Activity activity;

		public ServicoClickListener(Activity pActivity) {
			activity = pActivity;
		}

		public void onClick(View v) {
			Intent intent = null;
			switch (v.getId()) {
			// case R.id.ajuda_button:
			// break;
			case R.id.trocar_usuario_button:
				procedimentoTrocarUsuario();
				break;
			case R.id.sair_button:

				// DialogInterface.OnClickListener dialogLogoutClickListener =
				// new DialogInterface.OnClickListener() {
				// public void onClick(DialogInterface dialog, int which) {
				// switch (which){
				// case DialogInterface.BUTTON_POSITIVE:
				// procedimentoLogout();
				// break;
				//
				// case DialogInterface.BUTTON_NEGATIVE:
				//
				// break;
				// }
				// }
				// };
				//
				// FactoryAlertDialog.showAlertDialog(
				// SettingActivityMobile.this,
				// getResources().getString(R.string.confirmar_logout),
				// dialogLogoutClickListener);
				procedimentoLogout();

				break;
				
			case R.id.alterar_senha_button:
				intent = new Intent(activity, AlterarSenhaUsuarioActivityMobile.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, tagTela);
				activity.startActivity(intent);
				break;
			case R.id.setting_enviar_erro_ocorrido_button:
				intent = new Intent(activity, EnviarErroOcorridoActivityMobile.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, tagTela);
				activity.startActivity(intent);
				break;
			case R.id.setting_enviar_ideia_melhoria_button:
				intent = new Intent(activity, EnviarMelhoriaActivityMobile.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, tagTela);
				activity.startActivity(intent);
				break;
			case R.id.setting_suporte_button:
				intent = new Intent(activity, SuporteActivityMobile.class);
				activity.startActivity(intent);
				break;
			case R.id.sincronizar_button:
				intent = new Intent(activity, SynchronizeReceiveActivityMobile.class);
				activity.startActivity(intent);
				break;
			default:
				break;
			}
		}

	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;

		switch (id) {

		default:
			vDialog = this.getErrorDialog(id);
			break;

		}

		return vDialog;

	}

	private Dialog getErrorDialog(int dialogType) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
		case FORM_ERROR_DIALOG_REMOCAO_OK:
			vTextView.setText(getResources().getString(R.string.error_remocao));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_REMOCAO_OK);
				}
			});
			break;
		case FORM_ERROR_DIALOG_POST:
			vTextView.setText(getResources().getString(R.string.error_post));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_POST);
				}
			});
			break;
		case FORM_ERROR_DIALOG_SERVER_OFFLINE:
			vTextView.setText(getResources().getString(R.string.error_remocao_servidor_offline));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_SERVER_OFFLINE);
				}
			});
			break;
		case FORM_ERROR_DIALOG_REGISTRO_JA_REMOVIDO:
			vTextView.setText(getResources().getString(R.string.error_remocao_registro_ja_removido));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_REGISTRO_JA_REMOVIDO);
				}
			});
			break;

		default:
			return null;
		}

		return vDialog;
	}

}
