package app.omegasoftware.pontoeletronico.TabletActivities;

import java.util.ArrayList;

import org.json.JSONException;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.ControladorServicoInterno;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.HelperFonte;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador.STATE_SINCRONIZADOR;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizadorTabela;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorAndroidParaWeb;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.database.synchronize.ControladorSincronizador;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho.Item;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.file.FileWriterHttpPostManager;
import app.omegasoftware.pontoeletronico.pontoeletronico.PrincipalActivity;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.service.ControlerServicoInterno;
import app.omegasoftware.pontoeletronico.service.ControlerServicoInterno.TYPE_SERVICO;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiveBroadcastSincronizador;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class SynchronizeReceiveActivityMobile extends OmegaRegularActivity {

	// Constants
	public static final String TAG = "SynchronizeReceiveActivityMobile";
	public static final int DIALOG_SINCRONIZACAO_REALIZADA_COM_SUCESSO = 9111;
	public static final int DIALOG_SINCRONIZACAO_ERROR = 9112;
	// Download button
	private Button synchButton;
	Button resetarButton;

	private TextView statusTextView;

	ToggleButton sincronizacaoAutomaticaToggleButton;
	SincronizacaoLoader sincronizacaoLoader;
	FileWriterHttpPostManager filesManager;

	HandlerPbSincronizando handlerPbSincronizando;
	ProgressBar pbSincronizandor;
	TextView historicoTextView;
	TextView proximaDataSincronizacao;
	StatusHandler statusHandler;
	HandlerHistoricoSincronizador handlerHistoricoSincronizador;
	ReceiveBroadcastSincronizador receiverBroadcastSincronizador;
	TextView tvNaoSincronizado;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.synchronize_receive_activity_mobile_layout);

		new CustomDataLoader(this).execute();

	}

	public static boolean updateDataDaSincronizacaoDaCorporacaoSincronizador(Context pActivity, Database db,
			String pDate, String idCorporacaoSincronizador) {

		ArrayList<String> vListTableOrder = db.getListNameHierarquicalOrderOfVetorTable(
				OmegaConfiguration.LISTA_TABELA_SINCRONIZACAO_DOWNLOAD_UNICO(db));

		for (String strNomeTabela : vListTableOrder) {

			String vIdTabelaSincronizador = EXTDAOSistemaTabela.getIdSistemaTabela(db, strNomeTabela);
			if (vIdTabelaSincronizador == null) {

				SingletonLog.insereErro(TAG, "updateDataDaSincronizacaoDaCorporacaoSincronizador",
						"Tabela : " + strNomeTabela + ". Nao eh existente na sistema_tabela.",
						SingletonLog.TIPO.SINCRONIZADOR);

				return false;
			}

			EXTDAOSistemaCorporacaoSincronizadorTabela vObjCSTS = new EXTDAOSistemaCorporacaoSincronizadorTabela(db);
			vObjCSTS.setAttrValue(EXTDAOSistemaCorporacaoSincronizadorTabela.SISTEMA_CORPORACAO_SINCRONIZADOR_ID_INT,
					idCorporacaoSincronizador);
			vObjCSTS.setAttrValue(EXTDAOSistemaCorporacaoSincronizadorTabela.SISTEMA_TABELA_ID_INT,
					vIdTabelaSincronizador);

			EXTDAOSistemaCorporacaoSincronizadorTabela vTuplaCSTS = (EXTDAOSistemaCorporacaoSincronizadorTabela) vObjCSTS
					.getFirstTupla();

			if (vTuplaCSTS != null) {
				vTuplaCSTS.setAttrValue(EXTDAOSistemaCorporacaoSincronizadorTabela.DIA_DATE, pDate);
				vTuplaCSTS.update(false);
			} else {
				vObjCSTS.setAttrValue(EXTDAOSistemaCorporacaoSincronizadorTabela.DIA_DATE, pDate);
				vObjCSTS.insert(false);

				vObjCSTS.clearData();
			}
		}
		return true;
	}

	public static String[] updateDataDaSincronizacaoDaCorporacaoSincronizador(Context pActivity, Database db) {

		EXTDAOSistemaCorporacaoSincronizador vObjCorporacaoSincronizador = new EXTDAOSistemaCorporacaoSincronizador(db);

		EXTDAOSistemaCorporacaoSincronizador vTuplaCorporacaoSincronizador = (EXTDAOSistemaCorporacaoSincronizador) vObjCorporacaoSincronizador
				.getFirstTupla();
		String vDiaDate = null;
		HelperDate helperDate = new HelperDate();
		// TODO rotina nuo u mais necessuria
		// ContainerReceiveInformationDatabase.saveUltimoIdSincronizadorFromServer(pActivity,
		// db);
		String idCorporacao = OmegaSecurity.getIdCorporacao();
		vObjCorporacaoSincronizador.setAttrValue(EXTDAOSistemaCorporacaoSincronizador.CORPORACAO_ID_INT, idCorporacao);
		vDiaDate = helperDate.getDateDisplay();
		if (vTuplaCorporacaoSincronizador != null) {

			vTuplaCorporacaoSincronizador.setAttrValue(EXTDAOSistemaCorporacaoSincronizador.DIA_DATE, vDiaDate);
			if (vTuplaCorporacaoSincronizador.update(false))
				return new String[] { vDiaDate, vTuplaCorporacaoSincronizador.getId() };
			else
				return null;

		} else {

			vObjCorporacaoSincronizador.setAttrValue(EXTDAOSistemaCorporacaoSincronizador.DIA_DATE, vDiaDate);
			// Insere a tupla referente ao controle de sincronizacao total dos
			// dados da corporacao no banco local do android
			Long vId = vObjCorporacaoSincronizador.insert(false);
			if (vId != null) {
				return new String[] { vDiaDate, String.valueOf(vId) };
			} else
				return null;
		}
	}

	@Override
	public boolean loadData() {

		return true;
	}

	@Override
	public void initializeComponents() {

		statusTextView = (TextView) findViewById(R.id.status_textview);
		pbSincronizandor = (ProgressBar) findViewById(R.id.pb_sincronizando);
		historicoTextView = (TextView) findViewById(R.id.historico_textview);
		proximaDataSincronizacao = (TextView) findViewById(R.id.proxima_sincronizacao_textview);
		tvNaoSincronizado = (TextView) findViewById(R.id.total_nao_sincronizado_textview);
		this.synchButton = (Button) findViewById(R.id.synchronize_download_button);
		this.resetarButton = (Button) findViewById(R.id.resetar_button);
		if (RotinaSincronizador.getStateSincronizador(this,
				OmegaSecurity.getIdCorporacao()) == STATE_SINCRONIZADOR.RESETAR) {
			this.resetarButton.setVisibility(View.VISIBLE);
		} else {
			this.resetarButton.setVisibility(View.GONE);
		}
		this.pbSincronizandor = (ProgressBar) findViewById(R.id.pb_sincronizando);

		handlerHistoricoSincronizador = new HandlerHistoricoSincronizador();
		receiverBroadcastSincronizador = new ReceiveBroadcastSincronizador(this, handlerHistoricoSincronizador);

		Item item = null;
		try {
			item = SincronizadorEspelho.getItem(this, null);
			if (item != null) {

				historicoTextView.setText(item.getMensagemDialogSincronizador());
			}

		} catch (JSONException e) {
			SingletonLog.insereErro(e, TIPO.PAGINA);
		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.PAGINA);
		}

		handlerPbSincronizando = new HandlerPbSincronizando();
		statusHandler = new StatusHandler();

		this.resetarButton.setOnClickListener(new ResetarButtonListener());
		if (RotinaSincronizador.getStateSincronizador(this,
				OmegaSecurity.getIdCorporacao()) != STATE_SINCRONIZADOR.COMPLETO)
			this.resetarButton.setVisibility(View.GONE);

		this.synchButton.setOnClickListener(new TaskButtonListener(this, R.id.synchronize_download_button));

		sincronizacaoAutomaticaToggleButton = (ToggleButton) findViewById(R.id.sincronizacao_automatica_togglebutton);
		sincronizacaoAutomaticaToggleButton.setOnCheckedChangeListener(new SincronizacaoAutomaticaListener());

		statusTextView = (TextView) findViewById(R.id.status_textview);

		updateTotalNaoEnviado(item);

		if (getSavedSincronizacaoAutomatica(this)) {
			sincronizacaoAutomaticaToggleButton.setChecked(true);
		}
		// ESTADO_SINCRONIZACAO_MOBILE estado =
		// SincronizadorEspelho.getEstadoSalvo(this);
		// if(estado != null && estado ==
		// ESTADO_SINCRONIZACAO_MOBILE.OCORREU_UM_ERRO_FATAL){
		// resetarButton.setVisibility(View.VISIBLE);
		// } else {
		// resetarButton.setVisibility(View.GONE);
		// }

		// Log.d(TAG,"refreshAllInformation(): Start refreshing all
		// information");
		return;
	}

	protected void updateTotalNaoEnviado(Item item) {
		try {
			Database db = new DatabasePontoEletronico(this);
			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb obj = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(
					db);
			Long ultimoId = null;
			try {
				ultimoId = HelperInteger.parserLong(obj.getUltimoId(null));
				ultimoId = ultimoId == null ? 0 : ultimoId;
			} catch (JSONException e) {
				SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
			} catch (Exception e) {
				SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
			}
			if (item != null) {
				Long ultimoEnviado = item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb;
				ultimoEnviado = ultimoEnviado == null ? 0 : ultimoEnviado;

				long totalNaoEnviado = ultimoId - ultimoEnviado;
				tvNaoSincronizado.setText(String.valueOf(totalNaoEnviado));
			} else
				tvNaoSincronizado.setText("-");
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}

	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		dialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) dialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) dialog.findViewById(R.id.dialog_ok_button);
		try {
			switch (id) {

			case DIALOG_SINCRONIZACAO_ERROR:
				vTextView.setText(getResources().getString(R.string.sincronizacao_error));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(DIALOG_SINCRONIZACAO_ERROR);

					}
				});
				break;
			case DIALOG_SINCRONIZACAO_REALIZADA_COM_SUCESSO:

				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(DIALOG_SINCRONIZACAO_REALIZADA_COM_SUCESSO);
					}
				});
				break;
			default:
				// vDialog = this.getErrorDialog(id);
				return super.onCreateDialog(id);
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			return null;
		}
		return dialog;

	}

	public class SincronizacaoLoader extends AsyncTask<Bundle, Integer, Boolean> {
		Activity activity;

		public SincronizacaoLoader(Activity pActivity, int pButtonId) {
			activity = pActivity;

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try{
				Message message = new Message();
				atualizaStatusSincronizacao("Inicializando sincronizacao...");
				message.arg1 = View.VISIBLE;
				handlerPbSincronizando.sendMessage(message);	
			}catch(Exception ex){
				
				SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
			}
			
		}

		@Override
		protected Boolean doInBackground(Bundle... params) {
			InterfaceMensagem msg = null;
			try {

				while (msg == null || (msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId()
						&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO.getId()
						&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.ERRO_BANCO_OMEGASOFTWARE.getId()
						&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()
						&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.SERVIDOR_FORA_DO_AR.getId()
						&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.SEM_CONEXAO_A_INTERNET.getId()
						&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO_A_AREA_LOGADA.getId()
						&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.AGUARDANDO_USUARIO_RESETAR.getId())) {
					SincronizadorEspelho syncEspelho = SincronizadorEspelho
							.getInstance(SynchronizeReceiveActivityMobile.this);
					msg = syncEspelho.executa(true, false);
					if (msg != null)
						atualizaStatusSincronizacao(msg.mMensagem);

					Thread.sleep(1000);
				}
				return true;
			} catch (Exception e) {
				msg = new Mensagem(e);
				SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				Message message = new Message();
				message.arg1 = View.GONE;
				handlerPbSincronizando.sendMessage(message);
			} catch (Exception ex) {
				SingletonLog.openDialogError(SynchronizeReceiveActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally {
				controlerProgressDialog.dismissProgressDialog();
			}
		}

		private void atualizaStatusSincronizacao(String mensagem) {
			if (mensagem == null || mensagem.length() == 0)
				return;
			Message message = new Message();
			Bundle b = new Bundle();
			b.putString("status", mensagem);
			message.setData(b);
			statusHandler.sendMessage(message);
		}
	}

	// private class EnviaFotoButtonListener implements OnClickListener
	// {
	// Activity activity;
	// public EnviaFotoButtonListener(Activity pActivity){
	// activity = pActivity;
	// }
	// public void onClick(View v) {
	// Intent intent = new Intent( activity, SendFotoActivityMobile.class);
	// startActivityForResult(intent, OmegaConfiguration.STATE_SEND_FOTO );
	// }
	// }

	private class TaskButtonListener implements OnClickListener {
		Activity activity;
		int idBotao;

		public TaskButtonListener(Activity pActivity, int pIdBotao) {
			activity = pActivity;
			idBotao = pIdBotao;
		}

		public void onClick(View v) {
			sincronizacaoLoader = new SincronizacaoLoader(activity, idBotao);
			sincronizacaoLoader.execute();
		}

	}

	private class ResetarButtonListener implements OnClickListener {

		public ResetarButtonListener() {

		}

		public void onClick(View v) {
			Intent vIntent2 = new Intent(SynchronizeReceiveActivityMobile.this, ResetarActivityMobile.class);
			vIntent2.putExtra(OmegaConfiguration.SEARCH_FIELD_MESSAGE,
					getResources().getString(R.string.resetar_informacao));

			startActivityForResult(vIntent2, OmegaConfiguration.STATE_FORM_CONFIRMACAO_RESETAR);
		}

	}

	// Save Cidade ID in the preferences
	private void saveSincronizacaoAutomatica(boolean pIsSincronizacaoAutomatica) {

		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putBoolean(OmegaConfiguration.SAVED_SINCRONIZACAO_AUTOMATICA, pIsSincronizacaoAutomatica);
		vEditor.commit();

	}

	public static boolean getSavedSincronizacaoAutomatica(Context pContext) {
		SharedPreferences vSavedPreferences = pContext.getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		if (vSavedPreferences.contains(OmegaConfiguration.SAVED_SINCRONIZACAO_AUTOMATICA))
			return vSavedPreferences.getBoolean(OmegaConfiguration.SAVED_SINCRONIZACAO_AUTOMATICA, false);
		else
			return true;
	}

	private class SincronizacaoAutomaticaListener implements OnCheckedChangeListener {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

			if (!isChecked) {
				sincronizacaoAutomaticaToggleButton.setChecked(false);
				sincronizacaoAutomaticaToggleButton.setTextColor(R.color.gray_215);
				ControlerServicoInterno controleSI = ControlerServicoInterno.constroi(SynchronizeReceiveActivityMobile.this);
				controleSI.stopService(TYPE_SERVICO.SYNCH);
				
				saveSincronizacaoAutomatica(false);
			} else {
				sincronizacaoAutomaticaToggleButton.setChecked(true);
				sincronizacaoAutomaticaToggleButton.setTextColor(R.color.white);
				
				ControlerServicoInterno controleSI = ControlerServicoInterno.constroi(SynchronizeReceiveActivityMobile.this);
				controleSI.startService(TYPE_SERVICO.SYNCH);
				
				saveSincronizacaoAutomatica(true);
			}

		}
	}

	public class HandlerPbSincronizando extends Handler {

		@Override
		public void handleMessage(Message msg) {
			if (msg.arg1 == View.GONE) {
				pbSincronizandor.setVisibility(View.INVISIBLE);
				synchButton.setVisibility(View.VISIBLE);
			} else {
				pbSincronizandor.setVisibility(View.VISIBLE);
				synchButton.setVisibility(View.INVISIBLE);
			}
			// synchronize_download_button

		}
	}

	public class HandlerHistoricoSincronizador extends Handler {

		@Override
		public void handleMessage(Message msg) {
			Item item = null;
			try {
				item = SincronizadorEspelho.getItem(SynchronizeReceiveActivityMobile.this, null);
				if (item != null) {

					historicoTextView.setText(item.getMensagemDialogSincronizador());
				}

			} catch (JSONException e) {
				SingletonLog.insereErro(e, TIPO.PAGINA);
			} catch (Exception e) {
				SingletonLog.insereErro(e, TIPO.PAGINA);
			}
			try {
				if (msg.obj != null) {
					String strData = (String) msg.obj;
					proximaDataSincronizacao.setText(strData);

				}
			} catch (Exception e) {
				SingletonLog.insereErro(e, TIPO.PAGINA);
			}
			updateTotalNaoEnviado(item);
		}
	}

	public class StatusHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			Bundle b = msg.getData();
			statusTextView.setText(b.getString("status"));
			HelperFonte.setFontView(statusTextView);

		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		switch (requestCode) {
		case OmegaConfiguration.STATE_SEND_FOTO:
			switch (resultCode) {
			case RESULT_OK:
				// int vTotal =
				// intent.getIntExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO, 0
				// );

				break;

			default:
				break;
			}
			break;

		default:

			break;
		}

	}

	@Override
	protected void onDestroy() {
		if (receiverBroadcastSincronizador != null)
			receiverBroadcastSincronizador.unregisterActivity();
		super.onDestroy();
	}
}
