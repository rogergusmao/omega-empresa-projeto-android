package app.omegasoftware.pontoeletronico.TabletActivities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.Tuple;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho.ESTADO_SINCRONIZACAO_MOBILE;
import app.omegasoftware.pontoeletronico.service.ControlerServicoInterno;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class TrocarCorporacaoActivityMobile extends OmegaRegularActivity {

	// Constants
	public static final String TAG = "TrocarCorporacaoActivityMobile";
	StatusHandler statusHandler;
	private TextView carregandoTextView;
	private ProgressBar progressBar;
	// Download button
	private Button resetarButton;

	public static final int FALHA_AO_RESETAR_DESEJA_RESETAR_A_FORCA = 1;
	ResetarButtonHandler handlerTexto;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.trocar_corporacao_activity_mobile_layout);

		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {

		return true;
	}

	DialogInterface.OnClickListener dialogResetarClickListener;

	@Override
	public void initializeComponents() {
		statusHandler = new StatusHandler();
		dialogResetarClickListener = new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					procedimentoResetar(true);
					break;

				case DialogInterface.BUTTON_NEGATIVE:
					handlerTexto.sendEmptyMessage(ResetarButtonHandler.FALSE);
					break;
				}
			}
		};

		resetarButton = (Button) findViewById(R.id.resetar_button);
		progressBar = (ProgressBar) findViewById(R.id.progress_bar);
		carregandoTextView = (TextView) findViewById(R.id.carregando_textview);
		carregandoTextView.setVisibility(View.VISIBLE);
		progressBar.setVisibility(View.GONE);

		handlerTexto = new ResetarButtonHandler(this);

		resetarButton.setOnClickListener(new ResetarButtonListener());
		Tuple<Integer, String> registro = SincronizadorEspelho
				.getCorporacaoSincronizada(TrocarCorporacaoActivityMobile.this);
		if (registro != null && registro.y != null && registro.y.length() > 0) {
			String novaCorporacao = getIntent().getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CORPORACAO);
			String corporacao = registro.y;
			String str = getResources().getString(R.string.trocar_grupo_descricao);
			str = str.replace("{0}", novaCorporacao);
			str = str.replace("{1}", corporacao);
			((TextView) findViewById(R.id.resetar_textview)).setText(str);
		}

		return;
	}

	@Override
	public void onBackPressed() {
		Intent intent = getIntent();

		setResult(RESULT_CANCELED, intent);
		finish();
	}

	public class StatusHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			Bundle b = msg.getData();
			carregandoTextView.setText(b.getString("status"));
		}
	}

	public class ResetarButtonHandler extends Handler {

		public static final int TRUE = 1;
		public static final int FALSE = 0;

		Activity activity;

		public ResetarButtonHandler(Activity pActivity) {
			activity = pActivity;
		}

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case TRUE:
				resetarButton.setVisibility(View.GONE);
				progressBar.setVisibility(View.VISIBLE);
				break;
			case FALSE:
				resetarButton.setVisibility(View.VISIBLE);
				progressBar.setVisibility(View.GONE);
				break;
			default:
				break;
			}
		}
	}

	public void procedimentoResetar(boolean resetarAForca) {
		try {
			new ResetarLoader(resetarAForca).execute();
			handlerTexto.sendEmptyMessage(ResetarButtonHandler.TRUE);
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}

	}

	public class ResetarLoader extends AsyncTask<Bundle, Integer, Boolean> {

		InterfaceMensagem msg = null;
		boolean resetarAForca = false;
		SincronizadorEspelho syncEspelho;
		Exception e=null;
		public ResetarLoader(boolean resetarAForca) throws Exception {

			syncEspelho = SincronizadorEspelho.getInstance(TrocarCorporacaoActivityMobile.this);
			this.resetarAForca = resetarAForca;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try{
				atualizaStatusSincronizacao("Inicializando sincronizacao dos dados armazenados...");
			}catch(Exception ex){
				SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
			}
			
		}

		@Override
		protected Boolean doInBackground(Bundle... params) {
			msg = null;
			ControlerServicoInterno controladorServicoInterno = null;

			try {
				boolean validade = false;
				// Pausando todos os serviuos do OmegaEmpresa
				controladorServicoInterno = ControlerServicoInterno.constroi(TrocarCorporacaoActivityMobile.this);
				controladorServicoInterno.stopAllServices();
				Thread.sleep(2000);
				SincronizadorEspelho.Item item = syncEspelho.getItem(TrocarCorporacaoActivityMobile.this);
				if (item.estado != null) {
					if (item.estado == ESTADO_SINCRONIZACAO_MOBILE.FILA_ESPERA_PARA_SINCRONIZACAO) {
						msg = cicloSincronizacao();
						if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId()) {
							msg = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
									"A sincronizacao dos dados locais nuo foi realizada. Devido a uma falha do fluxo interno.");
						} else
							msg = cicloSincronizacao();
					}
					if (item.estado == ESTADO_SINCRONIZACAO_MOBILE.OCORREU_UM_ERRO_FATAL
							|| item.estado == ESTADO_SINCRONIZACAO_MOBILE.OCORREU_UM_ERRO_LEVE_QUE_SO_ENVIARA_OS_DADOS_LOCAIS_QUANDO_RESETAR
							|| item.estado == ESTADO_SINCRONIZACAO_MOBILE.SETANDO_ERRO_FATAL_NA_WEB) {
						msg = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
								"A sincronizacao dos dados locais nuo foi realizada. Devido a uma falha do fluxo interno.");
					} else {
						msg = cicloSincronizacao();
					}
				}
				if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()
						|| msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO_A_AREA_LOGADA.getId()
						|| resetarAForca) {
					validade = RotinaSincronizador.deletarBancoDeDados(TrocarCorporacaoActivityMobile.this,
							OmegaSecurity.getIdCorporacao(), resetarAForca);
					if (!validade) {
						msg = new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_PROCESSO_RESETAR,
								"Ocorreu um erro durante o processo a remouuo.");
					} else {
						msg = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
								"O grupo foi removido desse telefone.");
					}
					resetarAForca = false;

					return validade;
				} else
					return false;

			} catch (Exception e) {
				msg = new Mensagem(e);
				SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
				this.e=e;
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				atualizaStatusSincronizacao(msg != null ? msg.mMensagem : null);
				if(e!=null){
					SingletonLog.openDialogError(TrocarCorporacaoActivityMobile.this, e, SingletonLog.TIPO.PAGINA);
				} else {
					
					if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ERRO_PROCESSO_RESETAR.getId() && !resetarAForca) {
						FactoryAlertDialog.showAlertDialog(TrocarCorporacaoActivityMobile.this,
								msg.mMensagem + ". " + TrocarCorporacaoActivityMobile.this.getString(R.string.resetar_erro),
								dialogResetarClickListener);

					} else if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {

						RotinaSincronizador.logout(TrocarCorporacaoActivityMobile.this.getApplicationContext());
						setResult(RESULT_OK, getIntent());
						finish();
					}
					handlerTexto.sendEmptyMessage(ResetarButtonHandler.FALSE);	
				}
				
			} catch (Exception ex) {
				SingletonLog.openDialogError(TrocarCorporacaoActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			}
		}

		public InterfaceMensagem cicloSincronizacao() throws InterruptedException {
			InterfaceMensagem msg = null;
			boolean executada = false;
			while (msg == null || (msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId()
					&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO.getId()
					&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()
					&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.SERVIDOR_FORA_DO_AR.getId()
					&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.ERRO_BANCO_OMEGASOFTWARE.getId()
					&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.SEM_CONEXAO_A_INTERNET.getId()
					&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO_A_AREA_LOGADA.getId())) {
				if (executada)
					Thread.sleep(1000);
				else
					executada = true;
				msg = syncEspelho.executa(true, true);
				atualizaStatusSincronizacao(msg.mMensagem);
			}
			return msg;
		}

		private void atualizaStatusSincronizacao(String mensagem) {
			if (mensagem == null || mensagem.length() == 0)
				return;
			Message message = new Message();
			Bundle b = new Bundle();
			b.putString("status", mensagem);
			message.setData(b);
			statusHandler.sendMessage(message);
		}
	}

	private class ResetarButtonListener implements OnClickListener {

		public void onClick(View v) {
			procedimentoResetar(false);
		}

	}

}
