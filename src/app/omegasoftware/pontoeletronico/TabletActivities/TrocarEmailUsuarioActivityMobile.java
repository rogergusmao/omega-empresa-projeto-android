package app.omegasoftware.pontoeletronico.TabletActivities;

import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity.STATE_LOAD_USER_HTTP_POST;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.HelperCheckField;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.webservice.ServicosCobranca;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class TrocarEmailUsuarioActivityMobile extends OmegaRegularActivity {

	// Constants
	public static final String TAG = "TrocarEmailUsuarioActivityMobile";

	// private TextView carregandoTextView;

	private EditText emailEditText;
	private EditText senhaEditText;
	private Button alterarEmailButton;
	private Button trocarEmailButton;
	private LinearLayout senhaLinearLayout;
	CarregandoHandler carregandoHandler;

	public ESTADO estado = ESTADO.EMAIL_NAO_VERIFICADO;

	public enum ESTADO {
		EMAIL_NAO_VERIFICADO, EMAIL_VERIFICADO
	}

	ProgressBar carregandoProgressBar;
	SenhaHandler senhaHandler;
	String idUsuario;
	EXTDAOUsuario objUsuario;
	Database db=null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.trocar_email_usuario_activity_mobile_layout);

		idUsuario = getIntent().getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_USUARIO);

		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {
		try {
			db = new DatabasePontoEletronico(this);

			objUsuario = new EXTDAOUsuario(db);
			objUsuario.select(idUsuario);
			return true;
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
			return false;
		}
	}

	DialogInterface.OnClickListener dialogResetarClickListener;

	@Override
	public void initializeComponents() {

		senhaHandler = new SenhaHandler();
		carregandoHandler = new CarregandoHandler();
		emailEditText = (EditText) findViewById(R.id.email_edittext);
		senhaEditText = (EditText) findViewById(R.id.senha_edittext);
		senhaLinearLayout = (LinearLayout) findViewById(R.id.senha_linearlayout);
		trocarEmailButton = (Button) findViewById(R.id.trocar_email_button);
		
		
		alterarEmailButton = (Button) findViewById(R.id.alterar_email_button);
		alterarEmailButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				senhaHandler.sendEmptyMessage(0);

			}
		});

		carregandoProgressBar = (ProgressBar) findViewById(R.id.carregando_progressbar);
		trocarEmailButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					new VerificaEmailLoader().execute();
				} catch (Exception ex) {
					SingletonLog.insereErro(ex, TIPO.PAGINA);
				}

			}
		});
		
		

		return;
	}

	@Override
	public void onBackPressed() {
		Intent intent = getIntent();

		setResult(RESULT_OK, intent);
		finish();
	}

	public class CarregandoHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			if (msg.what == View.GONE) {
				alterarEmailButton.setVisibility(View.VISIBLE);
				trocarEmailButton.setVisibility(View.VISIBLE);
			} else {
				alterarEmailButton.setVisibility(View.GONE);
				trocarEmailButton.setVisibility(View.GONE);
			}
			carregandoProgressBar.setVisibility(msg.what);
		}
	}

	public class SenhaHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			Bundle b = msg.getData();
			if (estado == ESTADO.EMAIL_NAO_VERIFICADO) {
				emailEditText.setEnabled(false);
				estado = ESTADO.EMAIL_VERIFICADO;
				senhaLinearLayout.setVisibility(View.VISIBLE);
			} else {
				emailEditText.setEnabled(true);
				estado = ESTADO.EMAIL_NAO_VERIFICADO;
				senhaLinearLayout.setVisibility(View.GONE);
			}

			// carregandoTextView.setText(b.getString("status"));
		}
	}

	public class VerificaEmailLoader extends AsyncTask<Bundle, Integer, Boolean> {
		Integer dialogId = null;
		JSONObject ret = null;
		Exception ex;
		InterfaceMensagem msg = null;
		ServicosCobranca servicos = new ServicosCobranca(TrocarEmailUsuarioActivityMobile.this);

		public VerificaEmailLoader() throws Exception {
			carregandoHandler.sendEmptyMessage(View.VISIBLE);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected Boolean doInBackground(Bundle... params) {
			try {

				servicos.conecta();
				String email = emailEditText.getText().toString();
				if (email == null || email.trim().length() == 0) {
					dialogId = FORM_ERROR_DIALOG_MISSING_EMAIL;
					return false;
				} else if (!HelperCheckField.checkEmail(email)) {
					dialogId = ERROR_DIALOG_EMAIL_INVALIDO;
					return false;
				}
				String senha = senhaEditText.getText().toString();
				if (estado == ESTADO.EMAIL_NAO_VERIFICADO) {
					ret = servicos.verificaExistenciaDoCliente(email);
					if (ret != null && PROTOCOLO_SISTEMA.TIPO.SIM.isEqual(ret.getInt("mCodRetorno"))) {
						String nome = objUsuario.getStrValueOfAttribute(EXTDAOUsuario.NOME);
						String emailAntigo = objUsuario.getStrValueOfAttribute(EXTDAOUsuario.EMAIL);
						ret = editarUsuario(nome, emailAntigo, email, "");
					}
					return true;
				} else {
					if (senha == null || senha.trim().length() == 0) {
						dialogId = FORM_ERROR_DIALOG_MISSING_SENHA;
						return false;
					}

					String nome = objUsuario.getStrValueOfAttribute(EXTDAOUsuario.NOME);
					String emailAntigo = objUsuario.getStrValueOfAttribute(EXTDAOUsuario.EMAIL);
					ret = editarUsuario(nome, emailAntigo, email, senha);
					if (ret != null && PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
							.isEqual(ret.getInt("mCodRetorno"))) {
						if (objUsuario.getId().equalsIgnoreCase(OmegaSecurity.getIdUsuario())) {
							OmegaSecurity.loadUserFromDatabase(emailEditText.getText().toString(),
									OmegaSecurity.getCorporacao(),OmegaSecurity.getSenha(), 
									TrocarEmailUsuarioActivityMobile.this);
						}

					}
				}
				return true;
			} catch (Exception e) {
				ex = e;
				
				return false;
			}
		}

		public JSONObject editarUsuario(String nome, String emailAntigo, String email, String senha) throws Exception {
			ret = servicos.trocarEmailDoClientePeloSistema(nome, emailAntigo, email, senha);
			if (ret != null
					&& PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.isEqual(ret.getInt("mCodRetorno"))) {
				objUsuario.select(idUsuario);
				objUsuario.setAttrValue(EXTDAOUsuario.EMAIL, email);
				objUsuario.formatToSQLite();
				objUsuario.update(false);
			}
			return ret;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				if (ex != null) {
					SingletonLog.openDialogError(TrocarEmailUsuarioActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
				} else if (ret == null) {
					showDialog(FORM_ERROR_DIALOG_SERVER_OFFLINE);
					return;
				} else if (PROTOCOLO_SISTEMA.TIPO.NAO.isEqual(ret.getInt("mCodRetorno"))) {
					senhaHandler.sendEmptyMessage(0);
					return;
				} else if (PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.isEqual(ret.getInt("mCodRetorno"))) {
					showDialog(DIALOG_ERRO_FATAL);
				} else if (!PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.isEqual(ret.getInt("mCodRetorno"))) {
					String strMsg = ret.getString("mMensagem");
					if(strMsg != null && strMsg.length() > 0)
						FactoryAlertDialog.showDialog(TrocarEmailUsuarioActivityMobile.this, msg);
					else FactoryAlertDialog.showDialog(TrocarEmailUsuarioActivityMobile.this, getString(R.string.email_usuario_alterado));

				} else {
					Intent intent = new Intent(TrocarEmailUsuarioActivityMobile.this, AlertActivityMobile.class);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_MESSAGE, ret.getString("mMensagem"));
					startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ALERT_OPERACAO_REALIZADA_COM_SUCESSO);
				}
			} catch (Exception ex) {
				
				SingletonLog.openDialogError(TrocarEmailUsuarioActivityMobile.this, ex, TIPO.PAGINA);
			} finally {
				carregandoHandler.sendEmptyMessage(View.GONE);
			}
		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// Se encontrar o requestCode

		switch (requestCode) {
		case OmegaConfiguration.ACTIVITY_ALERT_OPERACAO_REALIZADA_COM_SUCESSO:
			Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SERVICE_PAINEL_USUARIO);
			sendBroadcast(updateUIIntent);
			finish();
			break;

		default:
			super.onActivityResult(requestCode, resultCode, intent);
			break;
		}

	}

	public class TrocarEmailButtonListener implements OnClickListener {

		public void onClick(View v) {
			try {
				new VerificaEmailLoader().execute();
			} catch (Exception e) {
				SingletonLog.insereErro(e, TIPO.PAGINA);
			}
		}

	}

}
