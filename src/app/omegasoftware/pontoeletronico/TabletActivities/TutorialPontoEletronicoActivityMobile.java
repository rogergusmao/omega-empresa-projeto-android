package app.omegasoftware.pontoeletronico.TabletActivities;


import android.os.Bundle;
import android.widget.Button;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.listener.BrowserOpenerListener;

public class TutorialPontoEletronicoActivityMobile extends OmegaRegularActivity {
	public static String TAG = "TutorialPontoEletronicoActivityMobile";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.tutorial_ponto_eletronico_activity_mobile_layout);
		
		Button vSisteSistemaAndroidBrowser = (Button) findViewById(R.id.website_sistema_android_button);		
		vSisteSistemaAndroidBrowser.setOnClickListener(new BrowserOpenerListener(getResources().getString(R.string.sistema_android_site_url)));
		
		initializeComponents();
		
	}
	
	
	
	@Override
	public boolean loadData() {
		//
		
		return true;
	}

	@Override
	public void initializeComponents() {
		
		
		
	}
	
}
