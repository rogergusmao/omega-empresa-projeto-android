package app.omegasoftware.pontoeletronico.TabletActivities;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOWifi;
import app.omegasoftware.pontoeletronico.gpsnovo.ContainerLocalizacao;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSControler;

public class WifiActivity extends OmegaRegularActivity {
	public static String TAG = "WifiActivity";
	WifiManager mWifiManager;
	List<ScanResult> results =null;
	String idEmpresa;
	public static Comparator<ScanResult> ScanResultComparator
			= new Comparator<ScanResult>() {

		public int compare(ScanResult sr1, ScanResult sr2) {

			if(sr1.level == sr2.level) return 0;
			//ascending order
			return sr1.level > sr2.level ? -1 : 1;

			//descending order
			//return fruitName2.compareTo(fruitName1);
		}

	};
	ReentrantLock re = new ReentrantLock();
	private final BroadcastReceiver mWifiScanReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context c, Intent intent) {

			if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
				initResults();
			}
		}
	};

	private void initResults(){
		if(re.tryLock()){
			try{
				results = mWifiManager.getScanResults();

				Database db = null;
				try {

					db = new DatabasePontoEletronico(WifiActivity.this);

					EXTDAOWifi wifi = new EXTDAOWifi(db);
					wifi.removerRegistrosDaEmpresa(idEmpresa);
					Collections.sort(results, ScanResultComparator);
					final int LIMITE = 3;
					int i = 0 ;
					for (ScanResult sr:
							results ) {
						wifi.clearData();
						wifi.setAttrValue(EXTDAOWifi.EMPRESA_ID_INT, idEmpresa);
						wifi.setAttrValue(EXTDAOWifi.SSID, sr.SSID);
						wifi.setAttrValue(EXTDAOWifi.BSSID, sr.BSSID);
						wifi.setAttrValue(EXTDAOWifi.FREQUENCY, String.valueOf( sr.frequency));
						wifi.setAttrValue(EXTDAOWifi.LINK_SPEED, String.valueOf( sr.level));
						wifi.formatToSQLite();
						Long res = wifi.insert(true);
						if(res == null)
							throw new Exception("Falha durante a inserção!");
						if(i++ >= LIMITE) break;
					}

					ContainerLocalizacao cl = GPSControler.getLocalizacao();
					if(cl != null){
						EXTDAOEmpresa objEmpresa = new EXTDAOEmpresa(db);
						if(objEmpresa.select(idEmpresa)){
//							if(objEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.LATITUDE_INT) == null) {
								objEmpresa.setAttrValue(
										EXTDAOEmpresa.LATITUDE_INT
										, String.valueOf( cl.getLatitude()));
								objEmpresa.setAttrValue(
										EXTDAOEmpresa.LONGITUDE_INT
										, String.valueOf( cl.getLongitude()));
								objEmpresa.formatToSQLite();
								objEmpresa.update(true);
//							}
						}
					}

					Toast.makeText(WifiActivity.this, R.string.empresa_mapeada, Toast.LENGTH_LONG).show();

					Intent intent2 = getIntent();
					intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED , true);
					WifiActivity.this.setResult(RESULT_OK, intent2);

					WifiActivity.this.finish();

				}catch(Exception ex){
					SingletonLog.factoryToast(WifiActivity.this, ex, SingletonLog.TIPO.PAGINA);
				}
				finally{
					if(db != null) db.close();
					mWifiScanReceiver.abortBroadcast();
				}
				// add your logic here

			}finally {
				re.unlock();
			}

		}
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		//Log.d(TAG, "onCreate(): Activity created");
		setContentView(R.layout.wifi_activity);
		
		new CustomDataLoader(this).execute();
		
	}

	
	@Override
	public boolean loadData() {
		idEmpresa = getIntent().getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA);
		mWifiManager = (WifiManager) WifiActivity.this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		registerReceiver(mWifiScanReceiver,
				new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		mWifiManager.startScan();



		return true;
	}

	@Override
	public void initializeComponents() {



	}
	
}
