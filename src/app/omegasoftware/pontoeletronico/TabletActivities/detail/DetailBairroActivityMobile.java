package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.AppointmentPlaceAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOBairro;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;

public class DetailBairroActivityMobile extends OmegaRegularActivity{


	//Close button
//	private Button closeButton;
	private static final String TAG = "DetailBairroActivityMobile";

	private DatabasePontoEletronico db=null;
	private EXTDAOBairro bairro;
	private AppointmentPlaceAdapter appointmentPlaces;
	private String bairroId;
	
	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_bairro_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		
		Bundle vParameters = getIntent().getExtras();

		//Gets bairro id from the Intent used to create this activity
		//TODO: Test connecting between ListResultActivity and EmpresaDetailsActivity
		//this.empresaId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		this.bairroId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);		
		new CustomDataLoader(this).execute();

	}


	/**
	 * Updates the TextView which holds the bairro name
	 */
	private void refreshBairroName()
	{
		//Log.d(TAG,"refreshBairroName()");
		String nomeBairro = this.bairro.getAttribute(EXTDAOBairro.NOME).getStrValue();
		if(nomeBairro != null){
			nomeBairro = nomeBairro.toUpperCase();
			((TextView) findViewById(R.id.bairro_textview)).setText(nomeBairro);
			((TextView) findViewById(R.id.bairro_textview)).setTypeface(this.customTypeFace);
		}
	}

	private void refreshCidadeEEstadoName()
	{
		try{
		//Log.d(TAG,"refreshCidadeEEstadoName()");
		EXTDAOCidade vObjCidade = new EXTDAOCidade(bairro.getDatabase());
		String vIdCidade = bairro.getStrValueOfAttribute(EXTDAOBairro.CIDADE_ID_INT);
		String vNomeEstado = null;
		String vNomeCidade = null;
		String vNomePais = null;
		if(vIdCidade != null){
			vObjCidade.setAttrValue(EXTDAOCidade.ID, vIdCidade);
			vObjCidade.select();
			vNomeCidade = vObjCidade.getStrValueOfAttribute(EXTDAOCidade.NOME);
			
			String vIdEstado = vObjCidade.getStrValueOfAttribute(EXTDAOCidade.UF_ID_INT);
			if(vIdEstado != null){
				EXTDAOUf vObjUf = new EXTDAOUf(vObjCidade.getDatabase());
				vObjUf.setAttrValue(EXTDAOUf.ID, vIdEstado);
				vObjUf.select();
				vNomeEstado = vObjUf.getStrValueOfAttribute(EXTDAOUf.NOME);
				vNomePais = vObjUf.getNomeDaChaveExtrangeira(EXTDAOUf.PAIS_ID_INT);
			}
			
		}
		if(vNomeCidade != null){
			
			((TextView) findViewById(R.id.cidade_textview)).setText(vNomeCidade);
			((TextView) findViewById(R.id.cidade_textview)).setTypeface(this.customTypeFace);
			((TextView) findViewById(R.id.titulo_cidade_textview)).setTypeface(this.customTypeFace);	
		} else{
			((TextView) findViewById(R.id.titulo_cidade_textview)).setVisibility(View.GONE);
			((TextView) findViewById(R.id.cidade_textview)).setVisibility(View.GONE);
		}
		if(vNomeEstado != null){
			((TextView) findViewById(R.id.estado_textview)).setText(vNomeEstado);
			((TextView) findViewById(R.id.estado_textview)).setTypeface(this.customTypeFace);
			((TextView) findViewById(R.id.titulo_estado_textview)).setTypeface(this.customTypeFace);
		} else{
			((TextView) findViewById(R.id.titulo_estado_textview)).setVisibility(View.GONE);
			((TextView) findViewById(R.id.estado_textview)).setVisibility(View.GONE);
		}
		
		if(vNomePais != null){
			((TextView) findViewById(R.id.pais_textview)).setText(vNomePais);
			((TextView) findViewById(R.id.pais_textview)).setTypeface(this.customTypeFace);
			((TextView) findViewById(R.id.titulo_pais_textview)).setTypeface(this.customTypeFace);
		} else{
			((TextView) findViewById(R.id.titulo_pais_textview)).setVisibility(View.GONE);
			((TextView) findViewById(R.id.pais_textview)).setVisibility(View.GONE);
		}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);

		}
	}
	

	@Override
	public boolean loadData() {

		//Log.d(TAG,"loadData(): Retrieving bairro data from the database");

		
		
		return true;

	}

	@Override
	public void initializeComponents() {

		//Log.d(TAG,"refreshAllInformation(): Start refreshing all information");
		try{
			this.db = new DatabasePontoEletronico(this);
			this.bairro = new EXTDAOBairro(this.db);
			this.bairro.setAttrValue(EXTDAOBairro.ID, this.bairroId);
			this.bairro.select();
			this.appointmentPlaces = this.bairro.getAppointmentPlacesAdapter(this);
			refreshBairroName();
			refreshCidadeEEstadoName();

			db.close();
		} catch(Exception ex){
			//Log.e("initializeComponents", ex.getMessage());
		}
		
	}


}
