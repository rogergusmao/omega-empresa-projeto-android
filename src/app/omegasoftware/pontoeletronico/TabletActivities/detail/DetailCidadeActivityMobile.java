package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.AppointmentPlaceAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;

public class DetailCidadeActivityMobile extends OmegaRegularActivity{


	//Close button
//	private Button closeButton;
	private static final String TAG = "DetailCidadeActivityMobile";

	private DatabasePontoEletronico db=null;
	private EXTDAOCidade cidade;
//	private AppointmentPlaceAdapter appointmentPlaces;
	private String cidadeId;
	private AppointmentPlaceAdapter appointmentPlaces;
	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_cidade_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		
		Bundle vParameters = getIntent().getExtras();

		//Gets bairro id from the Intent used to create this activity
		//TODO: Test connecting between ListResultActivity and EmpresaDetailsActivity
		//this.empresaId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		this.cidadeId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);		
		new CustomDataLoader(this).execute();

	}


	/**
	 * Updates the TextView which holds the bairro name
	 */
	private void refreshCidadeName()
	{
		//Log.d(TAG,"refreshCidadeName()");
		String nomeCidade = this.cidade.getAttribute(EXTDAOCidade.NOME).getStrValue();
		if(nomeCidade != null){
			nomeCidade = nomeCidade.toUpperCase();
			((TextView) findViewById(R.id.cidade_textview)).setText(nomeCidade);
			((TextView) findViewById(R.id.cidade_textview)).setTypeface(this.customTypeFace);
		}
	}

	private void refreshEstadoName()
	{
		try{
		//Log.d(TAG,"refreshCidadeEEstadoName()");
		
		String vNomeEstado = null;
		String vIdEstado = cidade.getStrValueOfAttribute(EXTDAOCidade.UF_ID_INT);
		if(vIdEstado != null){
			EXTDAOUf vObjUf = new EXTDAOUf(cidade.getDatabase());
			vObjUf.setAttrValue(EXTDAOUf.ID, vIdEstado);
			vObjUf.select();
			vNomeEstado = vObjUf.getStrValueOfAttribute(EXTDAOUf.NOME);
		}
		
		if(vNomeEstado != null){
			((TextView) findViewById(R.id.estado_textview)).setText(vNomeEstado);
			((TextView) findViewById(R.id.estado_textview)).setTypeface(this.customTypeFace);
			((TextView) findViewById(R.id.titulo_estado_textview)).setTypeface(this.customTypeFace);
		} else{
			((TextView) findViewById(R.id.titulo_estado_textview)).setVisibility(View.GONE);
			((TextView) findViewById(R.id.estado_textview)).setVisibility(View.GONE);
		}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);

		}
	}
	

	private void refreshPaisName()
	{
		try{
		//Log.d(TAG,"refreshCidadeEPaisName()");
		
		String vNomePais = null;
		String vIdPais = cidade.getStrValueOfAttribute(EXTDAOCidade.UF_ID_INT);
		if(vIdPais != null){
			EXTDAOUf vObjUf = new EXTDAOUf(cidade.getDatabase());
			vObjUf.setAttrValue(EXTDAOUf.ID, vIdPais);
			vObjUf.select();
			vNomePais = vObjUf.getStrValueOfAttribute(EXTDAOUf.NOME);
		}
		
		if(vNomePais != null){
			((TextView) findViewById(R.id.pais_textview)).setText(vNomePais);
			((TextView) findViewById(R.id.pais_textview)).setTypeface(this.customTypeFace);
			((TextView) findViewById(R.id.titulo_pais_textview)).setTypeface(this.customTypeFace);
		} else{
			((TextView) findViewById(R.id.titulo_pais_textview)).setVisibility(View.GONE);
			((TextView) findViewById(R.id.pais_textview)).setVisibility(View.GONE);
		}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);

		}
	}
	
	/**
	 * Updates the LinearLayout with the bairro appointment places 
	 */
//	private void refreshAppointmentPlaces()
//	{
//		Log.d(TAG,"refreshAppointmentPlaces()");
//		LinearLayout vAppointmentPlacesLinearLayout = (LinearLayout) findViewById(R.id.empresa_details_atendimento_linearlayout);
//		for(int i=0;i<this.appointmentPlaces.getCount();i++)
//		{
//			vAppointmentPlacesLinearLayout.addView(this.appointmentPlaces.getView(i, null, null));
//		}
//		((TextView) findViewById(R.id.appointment_places_list_title)).setTypeface(this.customTypeFace);
//	}

	public void finish(){
		try{
			db.close();
			super.finish();
		} catch(Exception ex){

		}
	}

	@Override
	public boolean loadData() {
try{
		//Log.d(TAG,"loadData(): Retrieving bairro data from the database");
		this.cidade = new EXTDAOCidade(this.db);
		this.cidade.setAttrValue(EXTDAOCidade.ID, this.cidadeId);	
		this.cidade.select();
//		this.appointmentPlaces = this.cidade.getAppointmentPlacesAdapter(this);

		return true;
}catch(Exception ex){
	SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
return false;
}
	}

	@Override
	public void initializeComponents() {

		//Log.d(TAG,"refreshAllInformation(): Start refreshing all information");
		try{
			this.db = new DatabasePontoEletronico(this);
			this.cidade = new EXTDAOCidade(this.db);
			this.cidade.setAttrValue(EXTDAOCidade.ID, this.cidadeId);	
			this.cidade.select();
			this.appointmentPlaces = this.cidade.getAppointmentPlacesAdapter(this);
			
			refreshCidadeName();
			refreshEstadoName();
			refreshPaisName();
			
			
			db.close();
		} catch(Exception ex){
			//Log.e("initializeComponents", ex.getMessage());
		}
		
		//Attach search button to its listener
//		this.closeButton = (Button) findViewById(R.id.unimedbh_dialog_fechar_button);
//		this.closeButton.setOnClickListener(new CloseButtonActivityClickListenerTablet(this));

	}



}
