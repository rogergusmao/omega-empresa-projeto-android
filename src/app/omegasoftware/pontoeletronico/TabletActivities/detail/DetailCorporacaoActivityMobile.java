package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCategoriaPermissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCorporacao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCorporacaoSincronizadorPhp;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorAndroidParaWeb;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioCategoriaPermissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioCorporacao;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.http.HelperHttp;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.primitivetype.HelperBoolean;
import app.omegasoftware.pontoeletronico.webservice.ServicosCobranca;
import app.omegasoftware.pontoeletronico.webservice.ServicosPontoEletronico;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;

public class DetailCorporacaoActivityMobile extends OmegaRegularActivity{


	//Close button
	//	private Button closeButton;
	private static final String TAG = "DetailCorporacaoActivityMobile";

	protected final int FORM_ERROR_DIALOG_DESCONECTADO = 93;
	
	protected final int FORM_REMOCAO_OK = 91;
	protected final int FORM_LIMPAR_DADOS_OK = 94;
	protected final int FORM_ERROR_DIALOG_USUARIO_NAO_ADM = 96;
	RemoveButtonHandler removeButtonHandler = new RemoveButtonHandler();
	

	private DatabasePontoEletronico db=null;
	private EXTDAOCorporacao corporacao;

	private LinearLayout removeLinearLayout;
	private String corporacaoId;
	private Button removeButton;
	private Button limparDadosButton;
	private TextView numeroModificacoesTextView;

	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_corporacao_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		//limpar_dados_da_corporacao_no_telefone
		Bundle vParameters = getIntent().getExtras();

		//Gets bairro id from the Intent used to create this activity
		//TODO: Test connecting between ListResultActivity and EmpresaDetailsActivity
		//this.empresaId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		this.corporacaoId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);


		new CustomDataLoader(this).execute();

	}


	/**
	 * Updates the TextView which holds the bairro name
	 */
	private void refreshCorporacaoName()
	{
		//Log.d(TAG,"refreshCorporacaoName()");
		String nomeBairro = this.corporacao.getAttribute(EXTDAOCorporacao.NOME).getStrValue();
		if(nomeBairro != null){
			nomeBairro = nomeBairro.toUpperCase();
			((TextView) findViewById(R.id.corporacao_textview)).setText(nomeBairro);
			((TextView) findViewById(R.id.corporacao_textview)).setTypeface(this.customTypeFace);
		}
	}


	public void finish(){
		try{
			db.close();
			super.finish();
		} catch(Exception ex){

		}
	}
	@Override
	public boolean loadData() {

		Log.d(TAG,"loadData(): Retrieving corporacao data from the database");

		return true;

	}
	boolean isAdm = false;
	boolean isMinhaCorporacao = false;
	int numeroDadosNaoSincronizados = 0;
	boolean possuiDadosNaoSincronizados = false;
	@Override
	public void initializeComponents() {

		//Log.d(TAG,"refreshAllInformation(): Start refreshing all information");
		try{
			((TextView) findViewById(R.id.remove_button)).setTypeface(this.customTypeFace);
			((TextView) findViewById(R.id.limpar_dados_button)).setTypeface(this.customTypeFace);
			((TextView) findViewById(R.id.titulo_numero_modificacao_textview)).setTypeface(this.customTypeFace);
			((TextView) findViewById(R.id.sub_titulo_numero_modificacao_textview)).setTypeface(this.customTypeFace);

			numeroModificacoesTextView = (TextView) findViewById(R.id.numero_modificacao_textview);

			this.db = new DatabasePontoEletronico(this);
			this.corporacao = new EXTDAOCorporacao(this.db);
			this.corporacao.setAttrValue(EXTDAOCorporacao.ID, this.corporacaoId);	
			this.corporacao.select();

			EXTDAOUsuarioCorporacao vObjUsuarioCorporacao = new EXTDAOUsuarioCorporacao(this.db);
			vObjUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.CORPORACAO_ID_INT, corporacaoId);
			vObjUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.USUARIO_ID_INT, OmegaSecurity.getIdUsuario());
			EXTDAOUsuarioCorporacao vTuplaUsuarioCorporacao = (EXTDAOUsuarioCorporacao) vObjUsuarioCorporacao.getFirstTupla();

			if(vTuplaUsuarioCorporacao != null){
				isMinhaCorporacao = true;
			}

			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb vObjSincronizador = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(this.db);
			vObjSincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT, corporacaoId);
			ArrayList<Table> vListDadosNaoSincronizados = vObjSincronizador.getListTable();
//			String vNomeTabelaNaoSincronizada = "";
//			for (Table table : vListDadosNaoSincronizados) {
//				EXTDAOSincronizador vTuplaSincronizador =(EXTDAOSincronizador)table;
//				
//				vNomeTabelaNaoSincronizada += vTuplaSincronizador.getStrValueOfAttribute(EXTDAOSincronizador.NOME_TABELA);
//			}
			 
			if(vListDadosNaoSincronizados != null)
				if(vListDadosNaoSincronizados.size() > 0 )
					numeroDadosNaoSincronizados = vListDadosNaoSincronizados.size();
			numeroModificacoesTextView.setText(String.valueOf(numeroDadosNaoSincronizados));
			removeLinearLayout = (LinearLayout) findViewById(R.id.menu_remove_corporacao_linearLayout);
			removeButton = (Button) findViewById(R.id.remove_button);
			removeButton.setVisibility(View.GONE);
			removeLinearLayout.setVisibility(View.GONE);
			TextView  vTextViewAdm = (TextView) findViewById(R.id.is_administrador_textview);
			TextView  vTextViewServer = (TextView) findViewById(R.id.server_textview);
			String vStrAdministrador = getResources().getString(R.string.is_administrador);
			boolean isConnected = true;
			if(!HelperHttp.isConnected(this)){
				isConnected = false;

				String vDesconectado = 	 getResources().getString(R.string.is_desconectado);
				vTextViewServer.setText(vDesconectado);
			}
			if(!isConnected){
				modoOffline();
			}
			else{
				ServicosPontoEletronico servicoPE = new ServicosPontoEletronico(this);
				
				Mensagem msg =servicoPE.isAdministradorDaCorporacao();
				if(msg == null){
					modoOffline();
					
				} else {
					vTextViewServer.setVisibility(View.GONE);
					
					if(msg.ok()){
						isAdm = true;

						vTextViewAdm.setText(vStrAdministrador);
						((TextView) findViewById(R.id.is_administrador_textview)).setTypeface(this.customTypeFace);
					} else if(msg.resultadoVazio()){
						String vStrNaoAdministrador = getResources().getString(R.string.is_not_administrador);
						vTextViewAdm.setText(vStrNaoAdministrador);
						((TextView) findViewById(R.id.is_administrador_textview)).setTypeface(this.customTypeFace);
					}

					if( isAdm){
						removeLinearLayout.setVisibility(View.VISIBLE);
						removeButton.setVisibility(View.VISIBLE);
						removeButton.setOnClickListener(new RemoveCorporacaoButtonListener(this));
					}
				}
			}

			limparDadosButton = (Button) findViewById(R.id.limpar_dados_button);
			limparDadosButton.setOnClickListener(new LimparDadosButtonListener(this));

			refreshCorporacaoName();

			db.close();
		} catch(Exception ex){
			//Log.e("initializeComponents", ex.getMessage());
		}

	}
	private void modoOffline() throws Exception{
		TextView  vTextViewAdm = (TextView) findViewById(R.id.is_administrador_textview);
		EXTDAOUsuarioCorporacao vObjUC = new EXTDAOUsuarioCorporacao(db);
		vObjUC.setAttrValue(EXTDAOUsuarioCorporacao.USUARIO_ID_INT, OmegaSecurity.getIdUsuario());
		vObjUC.setAttrValue(EXTDAOUsuarioCorporacao.CORPORACAO_ID_INT, corporacaoId);
		EXTDAOUsuarioCorporacao vTuplaUC = (EXTDAOUsuarioCorporacao) vObjUC.getFirstTupla();
		String vStrAdministrador = getResources().getString(R.string.is_administrador);
		if(vTuplaUC != null){
			String vStrIsAdm = vTuplaUC.getStrValueOfAttribute(EXTDAOUsuarioCorporacao.IS_ADM_BOOLEAN);
			Boolean vIsAdm = HelperBoolean.valueOfStringSQL(vStrIsAdm);
			if(vIsAdm != null && vIsAdm){
				vTextViewAdm.setText(vStrAdministrador);
			}else {
				EXTDAOUsuarioCategoriaPermissao vUsuarioObjCategoriaPermissao = new EXTDAOUsuarioCategoriaPermissao(db);
				EXTDAOUsuarioCategoriaPermissao vTuplaCP = (EXTDAOUsuarioCategoriaPermissao) vUsuarioObjCategoriaPermissao.getFirstTupla();
				if(vTuplaCP != null){
					String vIdCategoriaPermissao = vTuplaCP.getStrValueOfAttribute(EXTDAOUsuarioCategoriaPermissao.CATEGORIA_PERMISSAO_ID_INT);
					if(vIdCategoriaPermissao != null){
						EXTDAOCategoriaPermissao vObjCategoriaPermissao = new EXTDAOCategoriaPermissao(db);
						vObjCategoriaPermissao.setAttrValue(EXTDAOCategoriaPermissao.ID, vIdCategoriaPermissao);
						if(vObjCategoriaPermissao.select()){
							String vStrCP = vObjCategoriaPermissao.getStrValueOfAttribute(EXTDAOCategoriaPermissao.NOME);
							vTextViewAdm.setText(vStrCP);
						}

					}
				}
			}
		} else{
			String vStrNaoPertenceACorporacao = getResources().getString(R.string.mensagem_nao_pertence_a_corporacao);
			vTextViewAdm.setText(vStrNaoPertenceACorporacao);
			
		}
	}

	@Override
	protected Dialog onCreateDialog(int dialogType) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		
		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {

		case FORM_ERROR_DIALOG_DESCONECTADO:
			vTextView.setText(getResources().getString(R.string.error_form_desconectado));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_DESCONECTADO);
				}
			});
			break;

		case FORM_ERROR_DIALOG_USUARIO_NAO_ADM:
			vTextView.setText(getResources().getString(R.string.usuario_nao_eh_adm_da_corporacao));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_USUARIO_NAO_ADM);
				}
			});
			break;
		case FORM_ERROR_DIALOG_POST:
			vTextView.setText(getResources().getString(R.string.error_post));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_POST);
				}
			});
			break;
		case FORM_LIMPAR_DADOS_OK:
			vTextView.setText(getResources().getString(R.string.limpar_dados_ok));
			vOkButton.setOnClickListener(new LogoutListener(this, FORM_LIMPAR_DADOS_OK));
			break;
		case FORM_REMOCAO_OK:
			vTextView.setText(getResources().getString(R.string.remover_corporacao_ok));
			vOkButton.setOnClickListener(new LogoutListener(this, FORM_REMOCAO_OK));
			break;

		default:
			return super.onCreateDialog(dialogType);

		}

		return vDialog;
	}	


	private class LogoutListener implements OnClickListener{
		Activity activity;
		int dialogId;
		public LogoutListener(Activity pActivity, int pDialogId){
			activity = pActivity;
			dialogId = pDialogId;
		}
		public void onClick(View arg0) {
			
			dismissDialog(dialogId);
			RotinaSincronizador.actionLogout(activity, true);
			
		}
	}

	private void limparDadosCorporacao(){
		try{

			//TODO analisar esta classe, vc devera utilizar o receiver
			
	
			EXTDAOCorporacaoSincronizadorPhp vObjCorporacaoSincronizadorPHP = new EXTDAOCorporacaoSincronizadorPhp(db);
			vObjCorporacaoSincronizadorPHP.setAttrValue(EXTDAOCorporacaoSincronizadorPhp.CORPORACAO_ID_INT, corporacaoId);
			ArrayList<Table> vListTupla = vObjCorporacaoSincronizadorPHP.getListTable();
			if(vListTupla != null){
				if(vListTupla.size() > 0 ){
					for (Table vTupla : vListTupla) {
						
							vTupla.remove(false);
						
					}
				}
			}
	
			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb vObjSincronizador = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(db);
			vObjSincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			ArrayList<Table> vListTuplaSincronizador = vObjSincronizador.getListTable();
			if(vListTuplaSincronizador != null){
				if(vListTuplaSincronizador.size() > 0 ){
					for (Table vTupla : vListTuplaSincronizador) {
						vTupla.remove(false);
					}
				}
			}
			EXTDAOCorporacao vObjCorporacao = new EXTDAOCorporacao(db);
			vObjCorporacao.setAttrValue(EXTDAOCorporacao.ID, corporacaoId);
			vObjCorporacao.remove(false);
			db.close();
		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
		}
	}

	
	public class CheckLimparDadosLoader extends AsyncTask<String, Integer, Boolean>
	{

		Integer dialogId = null;
		Activity activity;
		DialogInterface.OnClickListener dialogLimparDados;
		String pergunta;
		public CheckLimparDadosLoader(Activity pActivity){
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{	
				dialogLimparDados = new DialogInterface.OnClickListener() {
				    public void onClick(DialogInterface dialog, int which) {
				    	
//				    	EXTDAOVeiculoUsuarioServico vObj = EXTDAOVeiculoUsuarioServico.getObjVeiculoUsuarioServico(db, OmegaSecurity.getIdUsuario());
						
				        switch (which){
				        case DialogInterface.BUTTON_POSITIVE:
				        	new LimparDadosLoader(DetailCorporacaoActivityMobile.this).execute();
				            break;

				        case DialogInterface.BUTTON_NEGATIVE:
				        	
				            break;
				        }
						
				    }
				};
				
				
//				Intent intent = new Intent( activity, FormConfirmacaoActivityMobile.class);
				pergunta = "";
				if(!isMinhaCorporacao && numeroDadosNaoSincronizados > 0)
					pergunta = getResources().getString(R.string.confirmacao_limpar_dados_corporacao_nao_minha);
				else if (!isMinhaCorporacao && numeroDadosNaoSincronizados == 0 )
					pergunta = getResources().getString(R.string.confirmacao_limpar_dados_corporacao);
				else if(!HelperHttp.isConnected(activity))
					pergunta = getResources().getString(R.string.confirmacao_limpar_dados_corporacao_offline);
				else
					pergunta = getResources().getString(R.string.confirmacao_limpar_dados_corporacao);

//				startActivityForResult(intent, OmegaConfiguration.STATE_FORM_CONFIRMACAO_LIMPAR_DADOS );
				
				
				
			}
			catch(Exception e)
			{
				//if(e != null) Log.e(TAG, e.getMessage());
				//else Log.e(TAG, "Error desconhecido");
			}finally{
				db.close();
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			controlerProgressDialog.dismissProgressDialog();
			
			FactoryAlertDialog.showAlertDialog(
					DetailCorporacaoActivityMobile.this, 
					pergunta,
					dialogLimparDados);
			
		}

	}
	
	public class CheckRemoveLoader extends AsyncTask<String, Integer, Boolean>
	{

		Integer dialogId = null;
		Activity activity;
		DialogInterface.OnClickListener dialogRemoverCorporacao = null;
		Mensagem msg = null;
		public CheckRemoveLoader(Activity pActivity){
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{	
				
				ServicosPontoEletronico servicosPE = new ServicosPontoEletronico(activity);
				Mensagem msg = servicosPE.isAdministradorDaCorporacao();
				
				if(msg != null){
					if(msg.ok()){
//						Intent intent = new Intent( activity, FormConfirmacaoActivityMobile.class);
//						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_MESSAGE, getResources().getString(R.string.confirmacao_remover_corporacao));
//						startActivityForResult(intent, OmegaConfiguration.STATE_FORM_CONFIRMACAO_REMOVER_CORPORACAO );
						
						dialogRemoverCorporacao = new DialogInterface.OnClickListener() {
						    public void onClick(DialogInterface dialog, int which) {
								
						        switch (which){
						        case DialogInterface.BUTTON_POSITIVE:
						        	new RemoveLoader(DetailCorporacaoActivityMobile.this).execute();
						            break;

						        case DialogInterface.BUTTON_NEGATIVE:
						            break;
						        }
						    }
						};
						
						
						
					}  
					else{
						this.msg=msg;
						dialogId=null;
					}
				}
			}
			catch(Exception e)
			{
				SingletonLog.insereErro(e, TIPO.PAGINA);
			}finally{
				db.close();
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			controlerProgressDialog.dismissProgressDialog();
			if(dialogId != null)
				showDialog(dialogId);
			else if(this.msg != null){
				FactoryAlertDialog.showDialog(activity, msg);
			}
			else if(dialogRemoverCorporacao != null){
				FactoryAlertDialog.showAlertDialog(
						DetailCorporacaoActivityMobile.this, 
						getResources().getString(R.string.confirmacao_remover_corporacao),
						dialogRemoverCorporacao);
			}
			
		}

	}
	
	public class RemoveLoader extends AsyncTask<String, Integer, Boolean>
	{

		Integer dialogId = null;
		Activity activity;
		Mensagem msg = null;
		public RemoveLoader(Activity pActivity){
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{	
				ServicosCobranca servicoCO = new ServicosCobranca(activity);
				Mensagem msg = servicoCO.cancelarAssinaturaDoCliente("Sem motivo");
				
				if(msg != null){
					if(msg.ok()){
						dialogId = FORM_REMOCAO_OK;
						
						limparDadosCorporacao();
						db.close();
						return true;
					} else {
						this.msg = msg;
						dialogId = null;
						return false;
					}
				} else {
					dialogId = FORM_ERROR_DIALOG_POST;
					return false;
				}
			}
			catch(Exception e)
			{
				//if(e != null) Log.e(TAG, e.getMessage());
				//else Log.e(TAG, "Error desconhecido");
			}finally{
				db.close();
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			controlerProgressDialog.dismissProgressDialog();
			if(dialogId != null)
				showDialog(dialogId);
			else if(this.msg != null){
				FactoryAlertDialog.showDialog(activity, msg);
			}
			if(result){ 
				removeButtonHandler.sendEmptyMessage(0);
			}
		}

	}

	public class RemoveButtonHandler extends Handler{

		@Override
		public void handleMessage(Message msg)
		{	
			removeButton.setVisibility(View.GONE);
			removeLinearLayout.setVisibility(View.GONE);
		}
	}

	public class LimparDadosLoader extends AsyncTask<String, Integer, Boolean>
	{

		Integer dialogId = null;
		Activity activity;
		public LimparDadosLoader(Activity pActivity){
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{	
				limparDadosCorporacao();
			}
			catch(Exception e)
			{
				//if(e != null) Log.e(TAG, e.getMessage());
				//else Log.e(TAG, "Error desconhecido");
			}finally{
				db.close();
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			controlerProgressDialog.dismissProgressDialog();
			if(dialogId != null)
				showDialog(dialogId);
		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {


		if (requestCode == OmegaConfiguration.STATE_FORM_CONFIRMACAO_REMOVER_CORPORACAO) {
			if (resultCode == RESULT_OK) {
				Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO , false);
				if(vConfirmacao){
					new RemoveLoader(this).execute();
				}
			}
		} else if (requestCode == OmegaConfiguration.STATE_FORM_CONFIRMACAO_LIMPAR_DADOS) {
			if (resultCode == RESULT_OK) {
				Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO , false);
				if(vConfirmacao){

					new LimparDadosLoader(this).execute();
				}
			}
		}

	}

	private class RemoveCorporacaoButtonListener implements OnClickListener
	{
		Activity activity;
		public RemoveCorporacaoButtonListener(Activity pActivity){
			activity = pActivity;
		}
		public void onClick(View v) {
			new CheckRemoveLoader(activity).execute();
		}
	}

	private class LimparDadosButtonListener implements OnClickListener
	{
		Activity activity;
		public LimparDadosButtonListener(Activity pActivity){
			activity = pActivity;
		}
		public void onClick(View v) {
			new CheckLimparDadosLoader(activity).execute();
		}
	}


}

