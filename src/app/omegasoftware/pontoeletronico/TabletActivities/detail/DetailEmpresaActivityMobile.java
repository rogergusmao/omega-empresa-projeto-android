package app.omegasoftware.pontoeletronico.TabletActivities.detail;

import java.util.ArrayList;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.RelogioDePontoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.WifiActivity;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutPessoaEmpresa;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPessoaEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListPontoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.AtualizarApkActivityMobile;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.AppointmentPlaceAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.DetailPessoaEmpresaAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.EmpresaProfissaoItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.EmpresaProfissaoItemList.ACAO_DETALHES;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDetailActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOWifi;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaPerfil;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOOperadora;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPerfil;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class DetailEmpresaActivityMobile extends OmegaDetailActivity {

	private static final String TAG = "DetailEmpresaActivityMobile";

	private DatabasePontoEletronico db=null;
	private EXTDAOEmpresa empresa;
	private AppointmentPlaceAdapter emailTelefoneEEndereco;


	@Override
	public void procedureBeforeLoadData() throws OmegaDatabaseException {
		super.procedureBeforeLoadData();
	}

	ContainerLayoutPessoaEmpresa containerPessoaEmpresa = new ContainerLayoutPessoaEmpresa(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_CLASS, FormEmpresaActivityMobile.class);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_empresa_activity_mobile_layout);

		new CustomDataLoader(this).execute();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
	}

	private void refreshMapearEmpresa() {
		try{
			Button bUltimosPontos = (Button) findViewById(R.id.bt_mapear_empresa);
			String strWifi = EXTDAOWifi.getNomesWifi(db, empresa.getId());
			if(strWifi != null && strWifi.length() > 0 ){
				((TextView)findViewById(R.id.tv_wifi)).setText(strWifi);
			} else {
				((TextView)findViewById(R.id.tv_wifi)).setText("-");
			}
			final DialogInterface.OnClickListener dialogConfirmarSaida = new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					try {
						switch (which) {
							case DialogInterface.BUTTON_POSITIVE:

									Bundle vParams = new Bundle();
									vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, id);

									Intent vIntent;
									vIntent = new Intent(DetailEmpresaActivityMobile.this, WifiActivity.class);

									vIntent.putExtras(vParams);
									startActivityForResult(vIntent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
								break;

							case DialogInterface.BUTTON_NEGATIVE:
								break;
						}
					} catch (Exception ex) {
						SingletonLog.openDialogError(DetailEmpresaActivityMobile.this, ex, TIPO.PAGINA);
					}
				}
			};

			bUltimosPontos.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {

					FactoryAlertDialog.showAlertDialog(
							DetailEmpresaActivityMobile.this
							, DetailEmpresaActivityMobile.this.getString(R.string.voce_esta_na_empresa)
							, dialogConfirmarSaida
					);

				}
			});


		}catch(Exception ex){
			FactoryAlertDialog.makeToast(this.getApplicationContext(), ex , null);
		}
	}

	private void refreshPontoEletronicoDaEmpresa() {
		try{
			Button bUltimosPontos = (Button) findViewById(R.id.bt_ultimos_pontos);
			bUltimosPontos.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					try {
						Bundle vParams = new Bundle();
						vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, id);

						Intent vIntent;
						vIntent = new Intent(DetailEmpresaActivityMobile.this, ListPontoActivityMobile.class);

						vIntent.putExtras(vParams);
						startActivityForResult(vIntent, OmegaConfiguration.ACTIVITY_FORM_PONTO);

					} catch (Exception ex) {
						SingletonLog.openDialogError(DetailEmpresaActivityMobile.this, ex, TIPO.PAGINA);

					}
				}
			});

			Button bPainel = (Button) findViewById(R.id.painel_ponto_button);
			bPainel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					try {
						Bundle vParams = new Bundle();
						vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, id);

						Intent vIntent;
						vIntent = new Intent(DetailEmpresaActivityMobile.this,
								RelogioDePontoActivityMobile.class);

						vIntent.putExtras(vParams);
						startActivityForResult(vIntent, OmegaConfiguration.ACTIVITY_FORM_PONTO);

					} catch (Exception ex) {
						SingletonLog.openDialogError(DetailEmpresaActivityMobile.this, ex, TIPO.PAGINA);

					}
				}
			});
		}catch(Exception ex){
			FactoryAlertDialog.makeToast(this.getApplicationContext(), ex , null);
		}

	}

	private void refreshEmpresaOperadora() {
		try {
			String operadora = this.empresa.getAttribute(EXTDAOEmpresa.OPERADORA_ID_INT).getStrValue();
			boolean vValidade = false;
			if (operadora != null && operadora.length() > 0) {
				EXTDAOOperadora vObjOperadora = new EXTDAOOperadora(db);
				vObjOperadora.setAttrValue(EXTDAOOperadora.ID, operadora);
				if (vObjOperadora.select()) {
					vValidade = true;
					String vNomeOperadora = vObjOperadora.getAttribute(EXTDAOOperadora.NOME).getStrValue();
					vNomeOperadora = vNomeOperadora.toUpperCase();
					((TextView) findViewById(R.id.conteudo_operadora)).setText(vNomeOperadora);
				}
			}

			if (!vValidade)
				((LinearLayout) findViewById(R.id.linearLayout_operadora))
						.setVisibility(View.GONE);
		}catch(Exception ex){
			FactoryAlertDialog.makeToast(this, ex , null);
		}
	}

	/**
	 * Updates the TextView which holds the empresa name
	 */
	private void refreshEmpresaName() {
		try{
			String nomeEmpresa = this.empresa.getAttribute(EXTDAOEmpresa.NOME).getStrValue();
			if (nomeEmpresa != null) {
				nomeEmpresa = HelperString.ucFirstForEachToken(nomeEmpresa);
				((TextView) findViewById(R.id.tv_title)).setText(nomeEmpresa);

			}
		}catch(Exception ex){
			FactoryAlertDialog.makeToast(this, ex , null);
		}
	}

	private void refreshTipoDocumento() {
		try {

			String nomeTipoDocumento = this.empresa.getNomeDaChaveExtrangeira(EXTDAOEmpresa.TIPO_DOCUMENTO_ID_INT);
			String numeroDocumento = this.empresa.getStrValueOfAttribute(EXTDAOEmpresa.NUMERO_DOCUMENTO);
			TextView tvTipoDoc = ((TextView) findViewById(R.id.empresa_details_tipo_documento));
			TextView tvNumDoc = ((TextView) findViewById(R.id.empresa_details_numero_documento));
			View llTipoDoc = findViewById(R.id.ll_tipo_documento);
			if (nomeTipoDocumento != null && numeroDocumento != null && nomeTipoDocumento.length() > 0
					&& numeroDocumento.length() > 0) {

				tvTipoDoc.setText(nomeTipoDocumento);

				tvNumDoc.setText(numeroDocumento);
				tvNumDoc.setVisibility(View.VISIBLE);
				tvTipoDoc.setVisibility(View.VISIBLE);
				llTipoDoc.setVisibility(View.VISIBLE);
			} else {

				tvTipoDoc.setVisibility(View.GONE);
				tvNumDoc.setVisibility(View.GONE);
				llTipoDoc.setVisibility(View.GONE);

			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);

		}
	}

	private void refreshPerfilEmpresa() {

		try {

			EXTDAOEmpresaPerfil vObjEP = new EXTDAOEmpresaPerfil(db);
			vObjEP.setAttrValue(EXTDAOEmpresaPerfil.EMPRESA_ID_INT, id);
			vObjEP.setAttrValue(EXTDAOEmpresaPerfil.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			ArrayList<Table> vListTuplaEmpresaPerfil = vObjEP.getListTable();
			String vTotalStrPerfil = "";
			if (vListTuplaEmpresaPerfil != null)
				for (Table vTuplaEmpresaPefil : vListTuplaEmpresaPerfil) {
					String vIdPerfil = vTuplaEmpresaPefil.getStrValueOfAttribute(EXTDAOEmpresaPerfil.PERFIL_ID_INT);
					String vStrPerfil = EXTDAOPerfil.getNomePerfil(this, HelperInteger.parserInteger(vIdPerfil));
					if (vStrPerfil != null) {
						if (vTotalStrPerfil.length() > 0)
							vTotalStrPerfil += ", " + vStrPerfil;
						else
							vTotalStrPerfil += vStrPerfil;
					}
				}
			if (vTotalStrPerfil.length() > 0) {

				((TextView) findViewById(R.id.empresa_details_empresa_perfil_empresa)).setText(vTotalStrPerfil);

			} else
				((TextView) findViewById(R.id.empresa_details_empresa_perfil_empresa))
						.setVisibility(View.GONE);

		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);

		}

	}

	private void refreshNovoFuncionario() {
		LinearLayout ll = (LinearLayout) findViewById(R.id.ll_cadastrar);
		ll.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(DetailEmpresaActivityMobile.this, FormPessoaEmpresaActivityMobile.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, id);
				startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);

			}
		});
	}

	private void refreshTipoEmpresa() {
		try {

			String vTipoEmpresa = this.empresa.getNomeDaChaveExtrangeira(EXTDAOEmpresa.TIPO_EMPRESA_ID_INT);
			boolean vValidade = false;
			if (vTipoEmpresa != null && vTipoEmpresa.length() > 0) {
				vValidade = true;
				vTipoEmpresa = HelperString.ucFirstForEachToken(vTipoEmpresa);
				((TextView) findViewById(R.id.empresa_details_empresa_tipo_empresa)).setText(vTipoEmpresa);

			}
			if (!vValidade)
				((TextView) findViewById(R.id.empresa_details_empresa_tipo_empresa))
						.setVisibility(View.GONE);
		}catch(Exception ex){
			FactoryAlertDialog.makeToast(this, ex , null);
		}
	}

	private void refreshPessoaEmpresa() {
		try {
			EXTDAOPessoaEmpresa vFuncionarioEmpresa = new EXTDAOPessoaEmpresa(this.db);

			vFuncionarioEmpresa.setAttrValue(EXTDAOPessoaEmpresa.EMPRESA_ID_INT, id);

			ArrayList<Table> vList = vFuncionarioEmpresa.getListTable();
			boolean vValidade = false;
			if (vList != null && vList.size() > 0) {
				((LinearLayout) findViewById(R.id.linearlayout_empresa_funcionario))
						.setVisibility(View.VISIBLE);
				((LinearLayout) findViewById(R.id.linearlayout_empresa_funcionario)).removeAllViews();
				containerPessoaEmpresa.clear();
				vValidade = true;
				EXTDAOProfissao objProfissao = new EXTDAOProfissao(this.db);
				EXTDAOPessoa objPessoa = new EXTDAOPessoa(this.db);
				EXTDAOPessoaUsuario objPessoaUsuario = new EXTDAOPessoaUsuario(this.db);

				for (Table table : vList) {
					EXTDAOPessoaEmpresa objPessoaEmpresa = (EXTDAOPessoaEmpresa) table;
					String idProfissao = objPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT);
					// String idEmpresa =
					// objPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.EMPRESA_ID_INT);
					String idPessoa = objPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.PESSOA_ID_INT);

					objProfissao.clearData();
					objProfissao.setAttrValue(EXTDAOProfissao.ID, idProfissao);
					objProfissao.select();

					objPessoa.clearData();
					objPessoa.setAttrValue(EXTDAOPessoa.ID, idPessoa);
					objPessoa.select();

					objPessoaUsuario.clearData();
					objPessoaUsuario.setAttrValue(EXTDAOPessoa.ID, idPessoa);
					Table registro = objPessoaUsuario.getFirstTupla();
					String idUsuario = null;
					if (registro != null) {
						idUsuario = registro.getStrValueOfAttribute(EXTDAOPessoaUsuario.USUARIO_ID_INT);
					}

					addLayoutEmpresaPessoa(idUsuario, idPessoa, objPessoaEmpresa.getId(), idProfissao,
							objPessoa.getDescricao(), objProfissao.getStrValueOfAttribute(EXTDAOProfissao.NOME));
				}
			}
			if (!vValidade) {

				((LinearLayout) findViewById(R.id.linearlayout_empresa_funcionario))
						.setVisibility(View.GONE);
			}

		}catch(OmegaDatabaseException ode){
			SingletonLog.factoryToast(this, ode, TIPO.PAGINA);
		}
		catch(Exception ex){
			FactoryAlertDialog.makeToast(this, ex , null);
		}
	}

	public void addLayoutEmpresaPessoa(String idUsuario, String idPessoa, String idPessoaEmpresa, String idProfissao,
									   String nomePessoa, String nomeProfissao) throws OmegaDatabaseException {
		if (idPessoa == null || idProfissao == null || idPessoa.length() == 0 || idProfissao.length() == 0) {
			return;
		}

		ACAO_DETALHES acaoDetalhes = ACAO_DETALHES.PESSOA;
		if (idUsuario != null)
			acaoDetalhes = ACAO_DETALHES.USUARIO;
		EmpresaProfissaoItemList empresaProfissaoItemList = new EmpresaProfissaoItemList(
				containerPessoaEmpresa.getUltimoIdEmpresa(), idUsuario, idPessoa, id, idProfissao,
				String.valueOf(idPessoaEmpresa), null, nomeProfissao, nomePessoa, acaoDetalhes);
		LinearLayout linearLayoutEmpresaFuncionario = (LinearLayout) findViewById(
				R.id.linearlayout_empresa_funcionario);

		ArrayList<EmpresaProfissaoItemList> places = new ArrayList<EmpresaProfissaoItemList>();
		places.add(empresaProfissaoItemList);

		DetailPessoaEmpresaAdapter adapter = new DetailPessoaEmpresaAdapter(this, places, containerPessoaEmpresa);

		View vNewView = adapter.getView(0, null, null);
		containerPessoaEmpresa.add(idPessoaEmpresa, id, nomeProfissao, vNewView);

		linearLayoutEmpresaFuncionario.addView(vNewView);


	}


	@Override
	public boolean loadData() {

		// Log.d(TAG,"loadData(): Retrieving empresa data from the database");

		return true;

	}

	@Override
	protected void beforeInitializeComponents() {

		try {
			this.db = new DatabasePontoEletronico(this);
			this.empresa = new EXTDAOEmpresa(this.db);

			this.empresa.select(this.id);
			this.emailTelefoneEEndereco = this.empresa.getAppointmentPlacesAdapter(this);

			refreshEmpresaName();
			refreshEmpresaOperadora();
			refreshTipoDocumento();

			refreshTipoEmpresa();
			refreshNovoFuncionario();
			refreshPerfilEmpresa();
			refreshPessoaEmpresa();
			refreshPontoEletronicoDaEmpresa();
			refreshMapearEmpresa();
			refreshEmailEnderecoETelefone();
		}catch(Exception ex){
			FactoryAlertDialog.makeToast(this, ex , null);
		}

	}

	@Override
	public Table getEXTDAO() {
		return new EXTDAOEmpresa(db);
	}


	/**
	 * Updates the LinearLayout with the medic appointment places
	 */
	public void refreshEmailEnderecoETelefone() {
		try {

			LinearLayout emailEnderecoTelelefoneLinearLayout = (LinearLayout) findViewById(R.id.ll_email_telefone_endereco);
			if (this.emailTelefoneEEndereco != null && this.emailTelefoneEEndereco.getCount() > 0) {
				boolean validade = false;
				emailEnderecoTelelefoneLinearLayout.removeAllViews();
				for (int i = 0; i < this.emailTelefoneEEndereco.getCount(); i++) {
					View vView = this.emailTelefoneEEndereco.getView(i, null, null);

					if (vView != null) {
						emailEnderecoTelelefoneLinearLayout.addView(vView);
						validade = true;
					}
				}
				if (!validade)
					emailEnderecoTelelefoneLinearLayout.setVisibility(View.GONE);
				else {
					emailEnderecoTelelefoneLinearLayout.setVisibility(View.VISIBLE);
				}
			} else {
				emailEnderecoTelelefoneLinearLayout.setVisibility(View.GONE);
			}
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(DetailEmpresaActivityMobile.this, ex, null);
		}

	}
}
