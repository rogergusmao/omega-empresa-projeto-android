package app.omegasoftware.pontoeletronico.TabletActivities.detail;

import java.util.ArrayList;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutPessoaEmpresa;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.DetailPessoaEmpresaAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.EmpresaProfissaoItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.EmpresaProfissaoItemList.ACAO_DETALHES;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.ResultSet;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresaRotina;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class DetailEmpresaRotinaActivityMobile extends OmegaRegularActivity {

	//Constants
	private static final String TAG = "FormEmpresaRotinaActivityMobile";

	
//	Handler handlerView;
	//private Button cancelarButton;
	Drawable originalDrawable;

	int ano;
	int mes;
	int diaMes;
	int diaSemana;
	String idPessoa ;
	String idUsuario;
	int semanaRotina;
	private EXTDAOPessoa objPessoa;
	private EXTDAOUsuario objUsuario;
	EXTDAOPessoaEmpresa objPessoaEmpresa;
	EXTDAOPessoaEmpresaRotina objPessoaEmpresaRotina;
	Database db=null;
	ContainerLayoutPessoaEmpresa containerPessoaEmpresa ;	
	LinearLayout llPessoasEmpresas;
	//private Button btNovaPessoaEmpresa;
	ResultSet rsPessoaEmpresa = null;
	ResultSet rsPessoaEmpresaRotina = null;
	ArrayList<View> viewsPessoaEmpresa = new ArrayList<View>();
	DetailPessoaEmpresaAdapter adapter = null;
	boolean diaCarregado = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail_empresa_rotina_activity_mobile_layout);
		//TODO corrigir disposicao do try catch
		try{
	
			Bundle vParameter = getIntent().getExtras();
			if(vParameter != null){
				
				idUsuario = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_USUARIO);
				db = new DatabasePontoEletronico(this);			
				objUsuario = new EXTDAOUsuario(db);
				objUsuario.select(idUsuario);
				ano = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_ANO);
				mes = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_MES);
				diaMes = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_DIA_MES);
				diaSemana= vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_DIA_SEMANA);
				semanaRotina = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_ID_SEMANA_CICLO);
				
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			
		}
		new CustomDataLoader(this).execute();
	}



	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------



	@Override
	public boolean loadData() {
		try{
			db = new DatabasePontoEletronico(this);
			objUsuario = new EXTDAOUsuario(db);
			objUsuario.select(idUsuario);
			objPessoa = objUsuario.getObjPessoa();
			
			objPessoaEmpresa = new EXTDAOPessoaEmpresa(db);
			objPessoaEmpresa.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, objPessoa.getId());
			rsPessoaEmpresa = objPessoaEmpresa.getRegistrosDaPessoa(objPessoa.getId());
			
			objPessoaEmpresaRotina = new EXTDAOPessoaEmpresaRotina(db);
			
			rsPessoaEmpresaRotina = objPessoaEmpresaRotina.getRegistrosDaPessoa(
										objPessoa.getId(), 
										String.valueOf(semanaRotina), 
										String.valueOf(diaSemana));
			if(rsPessoaEmpresa != null && rsPessoaEmpresa.getTotalTupla() > 0){
				ArrayList<EmpresaProfissaoItemList> pListEmpresaProfissaoItemList = new ArrayList<EmpresaProfissaoItemList> ();
				
				for(int i = 0 ; i < rsPessoaEmpresa.getTotalTupla(); i++){
					String[] valores = rsPessoaEmpresa.getTupla(i);
					String pessoa =valores[0];
					String empresa =valores[1];
					String profissao =valores[2];
					
					
					String idPessoaEmpresa = valores[3];
					String idEmpresa = valores[4];
					String idPessoa = valores[5];
					String idProfissao = valores[6];
					String token = rsPessoaEmpresaRotina.findRegistro(Table.ID_UNIVERSAL,  idPessoaEmpresa);
					//Se o registro foi encontrado
					if( token != null){
						pListEmpresaProfissaoItemList.add(new EmpresaProfissaoItemList(
								HelperInteger.parserInteger( idPessoaEmpresa),
								idUsuario, 
								idPessoa, 
								idEmpresa, 
								idProfissao, 
								idPessoaEmpresa, 
								empresa, 
								profissao, 
								pessoa, 
								ACAO_DETALHES.SEM_ACAO));
					}
				}
				adapter = new DetailPessoaEmpresaAdapter(this, pListEmpresaProfissaoItemList, null);
			}
					
			
			return true;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			return false;	
		}
	}

	@Override
	public void initializeComponents() {
		setVisibilityBotoesToolbar(OmegaConfiguration.VISIBILITY_INVISIBLE , OmegaConfiguration.VISIBILITY_INVISIBLE );

 		llPessoasEmpresas = (LinearLayout) findViewById(R.id.linearlayout_empresa_funcionario);
 		llPessoasEmpresas.removeAllViews();
		if(adapter != null){
			for(int i = 0 ; i < adapter.getCount(); i++){
				View v= adapter.getView(i, null, llPessoasEmpresas);
				if(v != null)
				llPessoasEmpresas.addView(v);
			}
		}
		
		TextView tvMensagem = (TextView)findViewById(R.id.tv_mensagem);
		if(tvMensagem != null && ! diaCarregado){
			tvMensagem.append(String.valueOf(" " + diaMes ));
			diaCarregado = true;
		}
		
	}	
}


