package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.AppointmentPlaceAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;

public class DetailEstadoActivityMobile extends OmegaRegularActivity{


	//Close button
	//	private Button closeButton;
	private static final String TAG = "DetailEstadoActivityMobile";

	private DatabasePontoEletronico db=null;
	private EXTDAOUf uf;
	private AppointmentPlaceAdapter appointmentPlaces;
	private String cidadeId;

	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_estado_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");

		Bundle vParameters = getIntent().getExtras();

		//Gets bairro id from the Intent used to create this activity
		//TODO: Test connecting between ListResultActivity and EmpresaDetailsActivity
		//this.empresaId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		this.cidadeId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);

		
		new CustomDataLoader(this).execute();

	}


	/**
	 * Updates the TextView which holds the bairro name
	 */
	private void refreshEstadoName()
	{
		//Log.d(TAG,"refreshEstadoName()");
		String nomeBairro = this.uf.getAttribute(EXTDAOUf.NOME).getStrValue();
		if(nomeBairro != null){
			nomeBairro = nomeBairro.toUpperCase();
			((TextView) findViewById(R.id.estado_textview)).setText(nomeBairro);
			((TextView) findViewById(R.id.estado_textview)).setTypeface(this.customTypeFace);
		}
	}
	
	public void finish(){
		try{
			db.close();
			super.finish();
		} catch(Exception ex){

		}
	}
	@Override
	public boolean loadData() {

		//Log.d(TAG,"loadData(): Retrieving estado data from the database");
		
		return true;

	}
	/**
	 * Updates the LinearLayout with the bairro appointment places 
	 */

	@Override
	public void initializeComponents() {

		//Log.d(TAG,"refreshAllInformation(): Start refreshing all information");
		try{
			this.db = new DatabasePontoEletronico(this);
			this.uf = new EXTDAOUf(this.db);
			this.uf.setAttrValue(EXTDAOUf.ID, this.cidadeId);	
			this.uf.select();
			this.appointmentPlaces = this.uf.getAppointmentPlacesAdapter(this);

			refreshEstadoName();
			refreshPaisName();

			db.close();
		} catch(Exception ex){
			//Log.e("initializeComponents", ex.getMessage());
		}

	}
	private void refreshPaisName()
	{
		try{
		//Log.d(TAG,"refreshPaisName()");
		String nome = this.uf.getNomeDaChaveExtrangeira(EXTDAOUf.PAIS_ID_INT);
		if(nome != null){
			nome = nome.toUpperCase();
			((TextView) findViewById(R.id.pais_textview)).setText(nome);
			((TextView) findViewById(R.id.pais_textview)).setTypeface(this.customTypeFace);
		}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);

		}
	}

}
