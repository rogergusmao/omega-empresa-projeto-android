package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOModelo;

public class DetailModeloActivityMobile extends OmegaRegularActivity{


	//Close button
//	private Button closeButton;
	private static final String TAG = "DetailModeloActivityMobile";

	private DatabasePontoEletronico db=null;
	private EXTDAOModelo modelo;
	private String modeloId;
	
	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_modelo_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		
		Bundle vParameters = getIntent().getExtras();

		this.modeloId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);

		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		new CustomDataLoader(this).execute();

	}


	/**
	 * Updates the TextView which holds the Modelo name
	 */
	private void refreshModeloNome()
	{
		//Log.d(TAG,"refreshModeloName()");
		String nomeModelo = this.modelo.getAttribute(EXTDAOModelo.NOME).getStrValue();
		if(nomeModelo != null){
			nomeModelo = nomeModelo.toUpperCase();
			((TextView) findViewById(R.id.nome)).setText(nomeModelo);
			((TextView) findViewById(R.id.nome)).setTypeface(this.customTypeFace);
		}
	}


	/**
	 * Updates the TextView which holds the Modelo name
	 */
	private void refreshModeloAno()
	{
		//Log.d(TAG,"refreshModeloName()");
		String vAnoModelo = this.modelo.getAttribute(EXTDAOModelo.ANO_INT).getStrValue();
		if(vAnoModelo != null){
			vAnoModelo = vAnoModelo.toUpperCase();
			((TextView) findViewById(R.id.ano)).setText(vAnoModelo);
			((TextView) findViewById(R.id.ano)).setTypeface(this.customTypeFace);
		}
	}

	@Override
	public boolean loadData() {
try{
		//Log.d(TAG,"loadData(): Retrieving Modelo data from the database");
		this.modelo = new EXTDAOModelo(this.db);
		this.modelo.setAttrValue(EXTDAOModelo.ID, this.modeloId);	
		this.modelo.select();
//		this.appointmentPlaces = this.Modelo.getAppointmentPlacesAdapter(this);

		return true;
}catch(Exception ex){
	SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
return false;
}
	}

	@Override
	public void initializeComponents() {


		try{

			this.modelo = new EXTDAOModelo(this.db);
			this.modelo.setAttrValue(EXTDAOModelo.ID, this.modeloId);	
			this.modelo.select();

			
			refreshModeloNome();
			refreshModeloAno();

		} catch(Exception ex){

		}
		

	}


}

