package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.AppointmentPlaceAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPais;

public class DetailPaisActivityMobile extends OmegaRegularActivity{


	//Close button
	//	private Button closeButton;
	private static final String TAG = "DetailPaisActivityMobile";

	private DatabasePontoEletronico db=null;
	private EXTDAOPais pais;
	private AppointmentPlaceAdapter appointmentPlaces;
	private String paisId;

	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_pais_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");

		Bundle vParameters = getIntent().getExtras();

		//Gets bairro id from the Intent used to create this activity
		//TODO: Test connecting between ListResultActivity and EmpresaDetailsActivity
		//this.empresaId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		this.paisId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);

		new CustomDataLoader(this).execute();

	}


	/**
	 * Updates the TextView which holds the bairro name
	 */
	private void refreshPaisName()
	{
		//Log.d(TAG,"refreshPaisName()");
		String nomeBairro = this.pais.getAttribute(EXTDAOPais.NOME).getStrValue();
		if(nomeBairro != null){
			nomeBairro = nomeBairro.toUpperCase();
			((TextView) findViewById(R.id.pais_textview)).setText(nomeBairro);
			((TextView) findViewById(R.id.pais_textview)).setTypeface(this.customTypeFace);
		}
	}


	public void finish(){
		try{
			db.close();
			super.finish();
		} catch(Exception ex){

		}
	}
	@Override
	public boolean loadData() {

		//Log.d(TAG,"loadData(): Retrieving pais data from the database");
		
		return true;

	}
	/**
	 * Updates the LinearLayout with the bairro appointment places 
	 */

	@Override
	public void initializeComponents() {

		//Log.d(TAG,"refreshAllInformation(): Start refreshing all information");
		try{
			this.db = new DatabasePontoEletronico(this);
			this.pais = new EXTDAOPais(this.db);
			this.pais.setAttrValue(EXTDAOPais.ID, this.paisId);	
			this.pais.select();
			this.appointmentPlaces = this.pais.getAppointmentPlacesAdapter(this);

			refreshPaisName();

			db.close();
		} catch(Exception ex){
			//Log.e("initializeComponents", ex.getMessage());
		}

		//Attach search button to its listener
		//		this.closeButton = (Button) findViewById(R.id.unimedbh_dialog_fechar_button);
		//		this.closeButton.setOnClickListener(new CloseButtonActivityClickListenerTablet(this));

	}


}
