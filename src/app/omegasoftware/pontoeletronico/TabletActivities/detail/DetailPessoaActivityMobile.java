package app.omegasoftware.pontoeletronico.TabletActivities.detail;

import android.os.Bundle;
import android.view.View;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.DetailPessoaAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDetailActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class DetailPessoaActivityMobile extends OmegaDetailActivity {

	private static final String TAG = "DetailPessoaActivityMobile";
	private EXTDAOPessoa objPessoa;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_CLASS, FormPessoaActivityMobile.class);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_pessoa_activity_mobile_layout);

		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {
		try {
			this.objPessoa = new EXTDAOPessoa(this.db);
			if (!this.objPessoa.select(id)) {
				// registro que nao havia sido sincronizado
				int intid = HelperInteger.parserInt(id);
				if (intid < 0) {
					String email = getIntent().getStringExtra(OmegaConfiguration.SEARCH_FIELD_EMAIL);
					if (email != null && email.length() > 0) {
						Long id = this.db.getFirstLong(
								"SELECT id FROM pessoa WHERE email = ? and corporacao_id_INT = ?",
								new String[] { email, OmegaSecurity.getIdCorporacao() });
						if (id != null) {
							if (!this.objPessoa.select(id.toString())) {
								return false;
							}
						}
					}
				}
			}
			return true;
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
			return false;
		}
	}

	@Override
	protected void beforeInitializeComponents() {

		try {

			View v = findViewById(android.R.id.content);
			DetailPessoaAdapter adapter = new DetailPessoaAdapter(DetailPessoaActivityMobile.this, objPessoa, v);

			adapter.formatarView();

		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
	}

	@Override
	public Table getEXTDAO() {

		return new EXTDAOPessoa(db);
	}

}
