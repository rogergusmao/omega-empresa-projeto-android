package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import android.graphics.Typeface;
import app.omegasoftware.pontoeletronico.R;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.AppointmentPlaceAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;

public class DetailProfissaoActivityMobile extends OmegaRegularActivity{


	//Close button
	//	private Button closeButton;
	private static final String TAG = "DetailProfissaoActivityMobile";

	private DatabasePontoEletronico db=null;
	private EXTDAOProfissao Profissao;
	private AppointmentPlaceAdapter appointmentPlaces;
	private String ProfissaoId;

	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_profissao_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");

		Bundle vParameters = getIntent().getExtras();

		//Gets bairro id from the Intent used to create this activity
		//TODO: Test connecting between ListResultActivity and EmpresaDetailsActivity
		//this.empresaId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		this.ProfissaoId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);

		
		new CustomDataLoader(this).execute();

	}


	/**
	 * Updates the TextView which holds the bairro name
	 */
	private void refreshProfissaoName()
	{
		Log.d(TAG,"refreshProfissaoName()");
		String nomeBairro = this.Profissao.getAttribute(EXTDAOProfissao.NOME).getStrValue();
		if(nomeBairro != null){
			nomeBairro = nomeBairro.toUpperCase();
			((TextView) findViewById(R.id.profissao_textview)).setText(nomeBairro);
			((TextView) findViewById(R.id.profissao_textview)).setTypeface(this.customTypeFace);
		}
	}


	public void finish(){
		try{
			db.close();
			super.finish();
		} catch(Exception ex){

		}
	}
	@Override
	public boolean loadData() {

		//Log.d(TAG,"loadData(): Retrieving Profissao data from the database");
		
		return true;

	}

	@Override
	public void initializeComponents() {

		//Log.d(TAG,"refreshAllInformation(): Start refreshing all information");
		try{
			this.db = new DatabasePontoEletronico(this);
			this.Profissao = new EXTDAOProfissao(this.db);
			this.Profissao.setAttrValue(EXTDAOProfissao.ID, this.ProfissaoId);	
			this.Profissao.select();
			this.appointmentPlaces = this.Profissao.getAppointmentPlacesAdapter(this);

			refreshProfissaoName();

			db.close();
		} catch(Exception ex){
			//Log.e("initializeComponents", ex.getMessage());
		}

		//Attach search button to its listener
		//		this.closeButton = (Button) findViewById(R.id.unimedbh_dialog_fechar_button);
		//		this.closeButton.setOnClickListener(new CloseButtonActivityClickListenerTablet(this));

	}


}
