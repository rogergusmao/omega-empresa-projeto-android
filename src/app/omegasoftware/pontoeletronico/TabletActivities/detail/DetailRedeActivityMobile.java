package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import java.util.ArrayList;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormRedeActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.DetailEmpresaPontoAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.NoResultsAdapterAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.EmpresaPontoItemList;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDetailActivity;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORede;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORedeEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoPonto;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class DetailRedeActivityMobile extends OmegaDetailActivity{


	//Close button
	//	private Button closeButton;
	private static final String TAG = "DetailRedeActivityMobile";
	private ArrayList<EmpresaPontoItemList> empresasPonto ;
	ArrayList<Table > empresasRede ;
	
	private EXTDAORede rede;
	
	String nomesEmpresa = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_CLASS, FormRedeActivityMobile.class);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_rede_activity_mobile_layout);	
		
		new CustomDataLoader(this).execute();

	}
	/**
	 * Updates the TextView which holds the bairro name
	 */
	
	private void refreshListaEmpresa()
	{
		//TODO descomentar
//		
//		( (LinearLayout) findViewById(R.id.ll_empresas)).removeAllViews();
//		if(empresasRede == null || empresasRede.size() == 0){
//			( (LinearLayout) findViewById(R.id.ll_empresas)).setVisibility(View.GONE);
//		} else{
//			
//			DetailEmpresaPontoAdapter adapter = new DetailEmpresaPontoAdapter(DetailRedeActivityMobile.this, empresasPonto);
//			LinearLayout llEmpresas = (LinearLayout) findViewById(R.id.ll_empresas);
//			llEmpresas.removeAllViews();
//			for(int i = 0; i < empresasPonto.size(); i++){
//				View viewEmpresaPonto = adapter.getView(i, null, llEmpresas);
//				llEmpresas.addView(viewEmpresaPonto);
//			}
//			llEmpresas.setVisibility(View.VISIBLE);
//			TextView empresasTextView = ((TextView) findViewById(R.id.nome_empresa_textview));
//			empresasTextView.setText(HelperString.ucFirstForEachToken( nomesEmpresa));
//			
//		}
	}
	
	

	

	/**
	 * Updates the TextView which holds the rede name
	 */
	private void refreshRedeName()
	{

		String nomeRede = this.rede.getAttribute(EXTDAORede.NOME).getStrValue();
		View viewRede = findViewById(R.id.conteudo_rede);
		TextView tvRede = (TextView)viewRede;
		if(nomeRede != null){
			setTituloCabecalho(nomeRede);
			tvRede.setText(nomeRede);
		}
	}

	
	private void refreshCadastrarEmpresas(){
		try{
			View noResults = findViewById(R.id.in_noresults);
			NoResultsAdapterAdapter noResultsAdapter = new NoResultsAdapterAdapter(
					DetailRedeActivityMobile.this, 
					R.string.clique_aqui_cadastro_empresa, 
					FormRedeActivityMobile.class,
					id);
			noResultsAdapter.formatarView(noResults);
			noResults.setVisibility(View.VISIBLE);
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
			
		}
	}

	@Override
	public boolean loadData() {

		try{
			this.rede = new EXTDAORede(this.db);
			this.rede.select(id);
			
			EXTDAORedeEmpresa vObj = new EXTDAORedeEmpresa(db);
			vObj.setAttrValue(EXTDAORedeEmpresa.REDE_ID_INT, id);
			empresasRede = vObj.getListTable();
			nomesEmpresa = "";
			if(empresasRede != null && empresasRede.size() > 0){
				empresasPonto = new ArrayList<EmpresaPontoItemList>();
				EXTDAOEmpresa objEmpresa = new EXTDAOEmpresa(db);
				HelperDate agora = new HelperDate();
				for (Table table : empresasRede) {
					EXTDAORedeEmpresa redeEmpresa = (EXTDAORedeEmpresa) table;
					String nomeEmpresa = redeEmpresa.getValorDoAtributoDaChaveExtrangeira(EXTDAORedeEmpresa.EMPRESA_ID_INT, EXTDAOEmpresa.NOME);
					if(nomesEmpresa.length() > 0)
						nomesEmpresa += ", " + nomeEmpresa;
					else nomesEmpresa = nomeEmpresa;
					
					String idEmpresa =redeEmpresa.getStrValueOfAttribute(EXTDAORedeEmpresa.EMPRESA_ID_INT);
					objEmpresa.setId(idEmpresa);
					EmpresaPontoItemList empresaPontoItemList = new EmpresaPontoItemList(
							idEmpresa,
							nomeEmpresa,
							agora, 
							objEmpresa.getTotalFuncionarioNoIntervaloOuTurno(agora, EXTDAOTipoPonto.TIPO_PONTO.INTERVALO), 
							objEmpresa.getTotalFuncionarioNoIntervaloOuTurno(agora, EXTDAOTipoPonto.TIPO_PONTO.TURNO), 
							objEmpresa.getTotalFuncionarios(),
							objEmpresa.getTotalFuncionarioNaoCompareceram(agora));
					
					empresasPonto.add(empresaPontoItemList);
				}	
			}
			return true;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
			return false;
		}
	}
	
	@Override
	public void beforeInitializeComponents() {
		try{
			clearComponent(DetailRedeActivityMobile.this.findViewById(R.id.root));
			refreshRedeName();
			refreshListaEmpresa();
			refreshCadastrarEmpresas();
		} catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
		}
	}


	@Override
	public Table getEXTDAO() {

		return new EXTDAORede(db);
	}
	
}
