package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import java.util.ArrayList;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutRelatorioAudio;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutRelatorioFoto;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutRelatorioVideo;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.FormRelatorioAudioAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.FormRelatorioFotoAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.FormRelatorioVideoAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.RelatorioAnexoItemList;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORelatorio;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORelatorioAnexo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoAnexo;

public class DetailRelatorioActivityMobile extends OmegaRegularActivity{


	//Close button
	//	private Button closeButton;
	private static final String TAG = "DetailRelatorioActivityMobile";

	private DatabasePontoEletronico db=null;
	private EXTDAORelatorio objRelatorio;
	ContainerLayoutRelatorioFoto containerFoto = new ContainerLayoutRelatorioFoto(this);
	ContainerLayoutRelatorioVideo containerVideo = new ContainerLayoutRelatorioVideo(this);
	ContainerLayoutRelatorioAudio containerAudio = new ContainerLayoutRelatorioAudio(this);
	
	private String relatorioId;

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_relatorio_activity_mobile_layout);

		

		Bundle vParameters = getIntent().getExtras();

		//Gets bairro id from the Intent used to create this activity
		//TODO: Test connecting between ListResultActivity and EmpresaDetailsActivity
		//this.empresaId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		this.relatorioId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);

		
		new CustomDataLoader(this).execute();

	}


	/**
	 * Updates the TextView which holds the bairro name
	 */
	private void refreshProfissaoName()
	{
		Log.d(TAG,"refreshProfissaoName()");
		String nomeBairro = this.objRelatorio.getAttribute(EXTDAOProfissao.NOME).getStrValue();
		if(nomeBairro != null){
			nomeBairro = nomeBairro.toUpperCase();
			((TextView) findViewById(R.id.profissao_textview)).setText(nomeBairro);
			
		}
	}


	public void finish(){
		try{
			db.close();
			super.finish();
		} catch(Exception ex){

		}
	}
	@Override
	public boolean loadData() {

		//Log.d(TAG,"loadData(): Retrieving Profissao data from the database");
		
		return true;

	}
	
	@Override
	public void initializeComponents() {

		//Log.d(TAG,"refreshAllInformation(): Start refreshing all information");
		try{
			this.db = new DatabasePontoEletronico(this);
			this.objRelatorio = new EXTDAORelatorio(this.db);
			this.objRelatorio.setAttrValue(EXTDAORelatorio.ID, this.relatorioId);	
			this.objRelatorio.select();
			

			refreshProfissaoName();
			
			db.close();
		} catch(Exception ex){
			//Log.e("initializeComponents", ex.getMessage());
		}

	}
	
	public void addLayoutRelatorioAudio(
			String pNomeAudio){
		if(pNomeAudio == null ){
			return;
		} else if(pNomeAudio.length() == 0){
			return;	
		}

		

		RelatorioAnexoItemList empresaProfissaoItemList = new RelatorioAnexoItemList(
				getApplicationContext(), 
				containerAudio.getLastId(), 
				pNomeAudio);
		LinearLayout linearLayoutRelatorioAudio = (LinearLayout) findViewById(R.id.audio_linearlayout);

		ArrayList<RelatorioAnexoItemList> places = new ArrayList<RelatorioAnexoItemList>();
		places.add(empresaProfissaoItemList);

		FormRelatorioAudioAdapter adapter = new FormRelatorioAudioAdapter(
				this,
				places, 
				containerAudio);
		View vNewView = adapter.getView(0, null, null);
		containerAudio.add(pNomeAudio, vNewView);

		linearLayoutRelatorioAudio.addView(vNewView);
	}

	public void addLayoutRelatorioFoto(
			String pNomeFoto){
		if(pNomeFoto == null ){
			return;
		} else if(pNomeFoto.length() == 0){
			return;	
		}



		RelatorioAnexoItemList empresaProfissaoItemList = new RelatorioAnexoItemList(
				getApplicationContext(), 
				containerFoto.getLastId(), 
				pNomeFoto);
		LinearLayout linearLayoutRelatorioFoto = (LinearLayout) findViewById(R.id.foto_linearlayout);

		ArrayList<RelatorioAnexoItemList> places = new ArrayList<RelatorioAnexoItemList>();
		places.add(empresaProfissaoItemList);

		FormRelatorioFotoAdapter adapter = new FormRelatorioFotoAdapter(
				getApplicationContext(),
				places, 
				containerFoto);
		View vNewView = adapter.getView(0, null, null);
		containerFoto.add(pNomeFoto, vNewView);

		linearLayoutRelatorioFoto.addView(vNewView);
	}
	
	public void addLayoutRelatorioVideo(
			String pNomeVideo){
		if(pNomeVideo == null ){
			return;
		} else if(pNomeVideo.length() == 0){
			return;	
		}

		

		RelatorioAnexoItemList itemList = new RelatorioAnexoItemList(
				getApplicationContext(), 
				containerVideo.getLastId(), 
				pNomeVideo);
		LinearLayout linearLayoutRelatorioVideo = (LinearLayout) findViewById(R.id.video_linearlayout);

		ArrayList<RelatorioAnexoItemList> places = new ArrayList<RelatorioAnexoItemList>();
		places.add(itemList);

		FormRelatorioVideoAdapter adapter = new FormRelatorioVideoAdapter(
				this,
				places, 
				containerVideo);
		View vNewView = adapter.getView(0, null, null);
		containerVideo.add(pNomeVideo, vNewView);

		linearLayoutRelatorioVideo.addView(vNewView);
	}
	
	
	protected void refreshRelatorioTipoAnexo(EXTDAOTipoAnexo.TIPO_ANEXO pTipoAnexo)
	{	
		try{
		EXTDAORelatorioAnexo vObjRA = new EXTDAORelatorioAnexo(this.db);
		
		vObjRA.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, relatorioId);
		vObjRA.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo( pTipoAnexo));
		
		
		ArrayList<Table> vList = vObjRA.getListTable();
		boolean vValidade = false;
		if(vList != null && vList.size() > 0){
			vValidade = true;
			EXTDAOProfissao vObjProfissao = new EXTDAOProfissao(this.db);
			EXTDAOEmpresa vObjEmpresa = new EXTDAOEmpresa(this.db);
			for (Table table : vList) {
				EXTDAOPessoaEmpresa vObjFuncionarioEmpresa = (EXTDAOPessoaEmpresa) table;
				String vIdProfissao = vObjFuncionarioEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT);
				String vIdEmpresa = vObjFuncionarioEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.EMPRESA_ID_INT);
			
				vObjProfissao.clearData();
				vObjProfissao.setAttrValue(EXTDAOProfissao.ID, vIdProfissao);
				vObjProfissao.select();
			
				vObjEmpresa.clearData();
				vObjEmpresa.setAttrValue(EXTDAOEmpresa.ID, vIdEmpresa);
				vObjEmpresa.select();
				
//				addLayoutEmpresaPessoa(
//					vIdEmpresa, 
//					vIdProfissao, 
//					vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.NOME), 
//					vObjProfissao.getStrValueOfAttribute(EXTDAOProfissao.NOME)
//				);	
			}		
		}
		if(!vValidade){
		
			
			((LinearLayout) findViewById(R.id.linearlayout_empresa_funcionario)).setVisibility(View.GONE);
		}
			
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);

		}
	}
}
