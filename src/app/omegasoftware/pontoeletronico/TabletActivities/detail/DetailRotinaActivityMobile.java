package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import java.util.ArrayList;
import java.util.GregorianCalendar;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.CalendarioAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.CalendarioAdapter.OnFormataViewDia;
import app.omegasoftware.pontoeletronico.common.Adapter.CalendarioAdapter.OnFormataViewSemana;
import app.omegasoftware.pontoeletronico.common.Adapter.CalendarioAdapter.ViewDia;
import app.omegasoftware.pontoeletronico.common.Adapter.CalendarioAdapter.ViewSemana;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.ResultSet;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresaRotina;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORotina;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.date.ContainerClickListenerDate;
import app.omegasoftware.pontoeletronico.date.ContainerClickListenerDate.OnChangeDate;
import app.omegasoftware.pontoeletronico.listener.FactoryDateClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class DetailRotinaActivityMobile extends OmegaRegularActivity {
	public static String TAG = "DetailRotinaActivityMobile";

	//Spinners
	//private DatabasePontoEletronico db=null;
	
	
	View[] viewsSemana = new View[5]; 
	//private AutoCompleteTextView profissaoAutoCompletEditText;

	TableAutoCompleteTextView profissaoContainerAutoComplete = null;
	CalendarioAdapter calendarioAdapter = null;
	int mesSelecionado = -1;
	int anoSelecionado = -1;
	String idUsuario = null;
	EXTDAOPessoa objPessoa = null;
	EXTDAOUsuario objUsuario = null;
	String descricaoUsuario = null;
	Button btSelecionarMes;
	LinearLayout llUsuario = null;
	
	private ContainerClickListenerDate inicioDataProgramadaExibirClickListener;
	
	FactoryDateClickListener factoryDateClickListener;
	ResultSet rsRotinas;
	EXTDAOPessoaEmpresaRotina objPessoaEmpresaRotina;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, R.string.rotina);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail_rotina_activity_mobile_layout);

		new CustomDataLoader(this).execute();
		
	}
	
	
	
	@Override
	public boolean loadData() {
		try{
			setOnAtualizaView(new OnAtualizaView() {
				
				@Override
				public void onAtualizaView(Bundle bundle) {
					if(bundle != null){
						idUsuario = bundle.getString(OmegaConfiguration.SEARCH_FIELD_ID_USUARIO);
					}
					
				}
			});
			
			Intent intent = getIntent();
			if(intent != null){
				if(intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_ANO_SELECIONADO)
						&& intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_MES_SELECIONADO)){
					anoSelecionado = intent.getIntExtra(OmegaConfiguration.SEARCH_FIELD_ANO_SELECIONADO, -1);
					mesSelecionado = intent.getIntExtra(OmegaConfiguration.SEARCH_FIELD_MES_SELECIONADO, -1);
				} 
				if(intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_ID_USUARIO))
					idUsuario= intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_USUARIO);
					
			}
			if(idUsuario == null){
				idUsuario = OmegaSecurity.getIdUsuario();
			}
			
			if(anoSelecionado == -1 || mesSelecionado == -1){
				GregorianCalendar dataAtual = new GregorianCalendar();
				anoSelecionado =dataAtual.get(GregorianCalendar.YEAR);
				mesSelecionado =dataAtual.get(GregorianCalendar.MONTH);
				
			}
			Database db = new DatabasePontoEletronico(this);
			objUsuario = new EXTDAOUsuario(db);
			objUsuario.select(idUsuario);
			objPessoa = objUsuario.getObjPessoa();
			descricaoUsuario = objUsuario.getDescricao();
			factoryDateClickListener = new FactoryDateClickListener(this);
			objPessoaEmpresaRotina = new EXTDAOPessoaEmpresaRotina(db);
			rsRotinas = objPessoaEmpresaRotina.getRegistrosDaPessoa(objPessoa.getId());
			
			return true;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			return false;
		}
	}
	

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		switch (id) {

		case FactoryDateClickListener.DATE_DIALOG_ID:
			factoryDateClickListener.onPrepareDialog(id, dialog);
			break;
		}
	}


	@Override
	public void initializeComponents() {
		

		this.btSelecionarMes = (Button) findViewById(R.id.bt_selecionar_mes);
		
		inicioDataProgramadaExibirClickListener = factoryDateClickListener.setDateClickListener(
				btSelecionarMes,
				new OnChangeDate() {
					
					@Override
					public void onChange(DatePicker view, int year, int monthOfYear,
							int dayOfMonth) {
						Intent intent =DetailRotinaActivityMobile.this.getIntent();
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ANO_SELECIONADO, year);
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_MES_SELECIONADO, monthOfYear);
						
						
						//Atualiza a view
						new CustomDataLoader(DetailRotinaActivityMobile.this).execute();
						
					}
				});
		//inicioDataProgramadaExibirClickListener.setMascara("MMMM/yyyy");
		inicioDataProgramadaExibirClickListener.setDateOfView(
				this,
				anoSelecionado, 
				mesSelecionado, 
				1);
		
		
		llUsuario = (LinearLayout) findViewById(R.id.ll_usuario);
//		llUsuario.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(DetailRotinaActivityMobile.this, SelectUsuarioActivityMobile.class);
//				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_USUARIO, idUsuario);
//				DetailRotinaActivityMobile.this.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
//			}
//		});
		calendarioAdapter = new CalendarioAdapter( 
				this, 
				anoSelecionado, 
				mesSelecionado);
		
		calendarioAdapter.setOnFormataViewSemana(new OnFormataViewSemana() {

			@Override
			public void formataView(ViewSemana view) {
				
				int semana = view.getSemanaCiclo();
				view.setTituloSemana("Semana " + String.valueOf(semana));
				
				LinearLayout llTitulo = view.getLinearLayoutTitulo();

				Integer drawable = EXTDAORotina.getDrawableLayoutDaSemana(semana);
				if(drawable != null && llTitulo != null){
					llTitulo.setBackgroundDrawable(getResources().getDrawable(drawable));
				}


			}
		});
		calendarioAdapter.setOnFormataViewDia(new OnFormataViewDia() {
			
			@Override
			public void formataView(ViewDia view) {
				final ViewDia contexto = view;
				final RelativeLayout rlLayout = view.getRelativeLayout();
				
				
				ArrayList<String> empresas = new ArrayList<String>();
				if(rsRotinas != null){
					for(int i = 0; i < rsRotinas.getTotalTupla(); i++){
						String[] valores = rsRotinas.getTupla(i);
						int semana = HelperInteger.parserInt( valores[0]);
						int diaSemana = HelperInteger.parserInt( valores[1]);
						
						//Se existe uma rotina associada ao dia
						if(semana == contexto.getSemanaCiclo() 
							&& diaSemana == contexto.getDiaSemana())
							empresas.add(valores[3]);
						
					}	
				}

				if(empresas != null && empresas.size() > 0){
					//String token = HelperString.getStrSeparateByDelimiterColumn(empresas, ",");
					contexto.setDescricao("+ " + String.valueOf(empresas.size()) );	
				}
				
				rlLayout.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(DetailRotinaActivityMobile.this, DetailEmpresaRotinaActivityMobile.class);
						
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ANO, contexto.getAno());
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_MES, contexto.getMes());
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_DIA_MES, contexto.getDiaMes());
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_DIA_SEMANA, contexto.getDiaSemana());
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_USUARIO, idUsuario);
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_SEMANA_CICLO, contexto.getSemanaCiclo());
						
							
						
						DetailRotinaActivityMobile.this.startActivity(intent);
					}
				});
				
			}
		});
		calendarioAdapter.formatarView();
		TextView tvUsuario = (TextView) findViewById(R.id.tv_usuario);
		tvUsuario.setText(descricaoUsuario);
		
		
	}
	

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;

		switch (id) {
		case FactoryDateClickListener.DATE_DIALOG_ID:
			return factoryDateClickListener.onCreateDialog(id);

		default:
			vDialog = super.onCreateDialog(id);
			break;

		}

		return vDialog;

	}	

	
}
