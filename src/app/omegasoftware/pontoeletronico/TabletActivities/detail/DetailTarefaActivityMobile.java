package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.View;

import android.view.Window;
import android.widget.Button;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.Date;

import app.omegasoftware.pontoeletronico.R;

import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormTarefaActivityMobile;

import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.AppointmentPlaceAdapter;

import app.omegasoftware.pontoeletronico.common.ItemList.AppointmentPlaceItemList;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDetailActivity;

import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;

import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;

import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.gpsnovo.ContainerLocalizacao;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSControler;
import app.omegasoftware.pontoeletronico.gpsnovo.GeoPoint;
import app.omegasoftware.pontoeletronico.mapa.HelperMapa;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class DetailTarefaActivityMobile extends OmegaDetailActivity{

	//Close button
	//	private Button closeButton;
	private static final String TAG = "DetailTarefaAtivaActivityMobile";

	private DatabasePontoEletronico db=null;

    private ToolbarHandler handlerToolbar;
	static private Integer tarefaId = null;


	EXTDAOTarefa.ESTADO_TAREFA estadoTarefa = EXTDAOTarefa.ESTADO_TAREFA.AGUARDANDO_INICIALIZACAO;
	EXTDAOTarefa objTarefa ;

	Button iniciarTarefaButton;
	Button finalizarTarefaButton;
	Button cancelarTarefaButton;


	GeoPoint geoPointOrigem = null;

	GeoPoint geoPointDestino = null;


	static Document documentRouteInformationPontoProprioOrigem = null;
	static Document documentRouteInformationPontoOrigemDestino = null;

	public static final int FORM_ERROR_DIALOG_GPS_FORA_DO_AR = 100;

	String datatimeCadastro = null;
	String datatimePrazo = null;


	String datetimeProgramada = null;

	String datetimeInicioTarefa = null;
	String datetimeFimTarefa = null;

	String tituloTarefa = null;
	String descricaoTarefa = null;
    ContainerLocalizacao containerLocalizacao ;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, R.string.tarefa);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_CLASS, FormTarefaActivityMobile.class);

		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_tarefa_activity_mobile_layout);

        handlerToolbar = new ToolbarHandler();


		new CustomDataLoader(this).execute();
	}


	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog d= getMyDialog(id);
		if(d != null) return d;
		else {
			return super.onCreateDialog(id);
		}
	}

	private final static int FORM_DIALOG_CADASTRO_OK = 1;
	private final static int FORM_DIALOG_TAREFA_OCUPADA = 2;
	private final static int FORM_DIALOG_PARAMETRO_INVALIDO = 3;
	private final static int FORM_DIALOG_TAREFA_OCUPADA_POR_OUTRO_USUARIO = 4;
	private final static int FORM_DIALOG_TAREFA_INICIALIZADA = 5;
	private final static int FORM_DIALOG_TAREFA_FINALIZADA = 6;
	private Dialog getMyDialog(int dialogType)
	{

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
		case FORM_DIALOG_CADASTRO_OK:
			vTextView.setText(getResources().getString(R.string.cadastro_ok));
			vOkButton.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_DIALOG_CADASTRO_OK);
				}
			});
			break;

		case FORM_ERROR_DIALOG_GPS_FORA_DO_AR:
			vTextView.setText(getResources().getString(R.string.gps_fora_de_area));
			vOkButton.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_GPS_FORA_DO_AR);
				}
			});
			break;
		case FORM_DIALOG_TAREFA_OCUPADA:
			vTextView.setText(getResources().getString(R.string.tarefa_ocupada));
			vOkButton.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_DIALOG_TAREFA_OCUPADA);
				}
			});
			break;
		case FORM_DIALOG_TAREFA_FINALIZADA:
			vTextView.setText(getResources().getString(R.string.tarefa_finalizada));
			vOkButton.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_DIALOG_TAREFA_FINALIZADA);
				}
			});
			break;
		case FORM_DIALOG_TAREFA_INICIALIZADA:
			vTextView.setText(getResources().getString(R.string.tarefa_inicializada));
			vOkButton.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_DIALOG_TAREFA_INICIALIZADA);
				}
			});
			break;
		case FORM_DIALOG_TAREFA_OCUPADA_POR_OUTRO_USUARIO:
			vTextView.setText(getResources().getString(R.string.tarefa_ocupada_por_outro_usuario));
			vOkButton.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_DIALOG_TAREFA_OCUPADA_POR_OUTRO_USUARIO);
				}
			});
			break;

		case FORM_DIALOG_PARAMETRO_INVALIDO:
			vTextView.setText(getResources().getString(R.string.tarefa_parametro_invalido));
			vOkButton.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_DIALOG_PARAMETRO_INVALIDO);
				}
			});
			break;

		default:
			return null;

		}

		return vDialog;
	}

	/**
	 * Updates the LinearLayout with the empresa appointment places
	 */
	private void refreshAppointmentPlaceOrigem() throws Exception {
		AppointmentPlaceItemList a = objTarefa.getAppointementOrigem();
		EXTDAOTarefa.DadosEndereco dados = objTarefa.getDadosEnderecoOrigem();
		if(dados != null){

			a.setIdEntidade(
					dados.tipoEndereco
					, dados.id
					, dados.tipoEndereco != null
							? (
							dados.tipoEndereco == EXTDAOTarefa.TIPO_ENDERECO.PESSOA
									? DetailPessoaActivityMobile.class
									: DetailEmpresaActivityMobile.class
					) : null
					, dados.nome);
		}

		((LinearLayout)findViewById(R.id.detail_tarefa_endereco_origem_linearlayout)).removeAllViews();
		if(a != null){
			a.setTitulo(HelperString.toUpperCaseSansAccent( getString(R.string.endereco_origem)));
			AppointmentPlaceAdapter adapter = new AppointmentPlaceAdapter(this, a);
			View v = adapter.getView(0, null, null);
			if(v != null)
			((LinearLayout)findViewById(R.id.detail_tarefa_endereco_origem_linearlayout)).addView(v);
		} else {

			findViewById(R.id.ll_origem).setVisibility(View.GONE);;

		}
	}

	private void refreshAppointmentPlaceDestino() throws Exception {
		AppointmentPlaceItemList a = objTarefa.getAppointementDestino();
		EXTDAOTarefa.DadosEndereco dados = objTarefa.getDadosEnderecoDestino();
		if(dados != null){

			a.setIdEntidade(
					dados.tipoEndereco
					, dados.id
					, dados.tipoEndereco != null
							? (
							dados.tipoEndereco == EXTDAOTarefa.TIPO_ENDERECO.PESSOA
									? DetailPessoaActivityMobile.class
									: DetailEmpresaActivityMobile.class
					) : null
					, dados.nome);
		}
		((LinearLayout)findViewById(R.id.detail_tarefa_endereco_destino_linearlayout)).removeAllViews();
		if(a != null){
			a.setTitulo(HelperString.toUpperCaseSansAccent( getString(R.string.endereco_destino)));
			AppointmentPlaceAdapter adapter = new AppointmentPlaceAdapter(this, a);
			View v = adapter.getView(0, null, null);
			if(v != null)
				((LinearLayout)findViewById(R.id.detail_tarefa_endereco_destino_linearlayout)).addView(v);
		} else {
			findViewById(R.id.ll_destino).setVisibility(View.GONE);
		}
	}


	private void refreshDatetimeCadastroTarefa()
	{
		//Log.d(TAG,"refreshDatetimeTarefa()");

		if(this.datatimeCadastro != null){

			((TextView) findViewById(R.id.detail_tarefa_data_cadastro_textview)).setText( datatimeCadastro);

		}
	}

	private void refreshDatetimePrazo()
	{
		//Log.d(TAG,"refreshDatetimeInicioTarefa()");

		if(this.datatimePrazo != null){
			((TextView) findViewById(R.id.tv_prazo)).setText( this.datatimePrazo);
		} else {
			findViewById(R.id.ll_prazo).setVisibility(View.GONE);;
		}
	}
	private void refreshDatetimeInicioTarefa()
	{
		//Log.d(TAG,"refreshDatetimeInicioTarefa()");

		if(this.datetimeInicioTarefa != null){
			((TextView) findViewById(R.id.detail_tarefa_data_inicio_textview)).setText( this.datetimeInicioTarefa);
		} else {
			findViewById(R.id.ll_inicio).setVisibility(View.GONE);;
		}
	}

	private void refreshDatetimeFimTarefa()
	{
		//Log.d(TAG,"refreshDatetimeFimTarefa()");

		if(this.datetimeFimTarefa != null){

			((TextView) findViewById(R.id.detail_tarefa_data_fim_textview)).setText( this.datetimeFimTarefa);

		} else {
			findViewById(R.id.ll_fim).setVisibility(View.GONE);;
		}
	}

	private void refreshDatetimeInicioProgramado()
	{
		//Log.d(TAG,"refreshDatetimeTarefa()");

		if(this.datetimeProgramada != null){

			((TextView) findViewById(R.id.detail_tarefa_data_programada_textview)).setText( datetimeProgramada);

		} else {
			findViewById(R.id.ll_programada).setVisibility(View.GONE);;
		}
	}



	private void refreshUsuarioResponsavel()
	{
		//Log.d(TAG,"refreshNomeTarefa()");

		TextView vTextViewTituloUsuarioDaTarefa = (TextView) findViewById(R.id.detail_tarefa_usuario_tarefa_textview);
		TextView vTextViewUsuarioDaTarefa = (TextView) findViewById(R.id.details_tarefa_de_outro_usuario);
		EXTDAOTarefa.ContainerResponsavel vContainer = objTarefa.getContainerResponsavel(this);
		if(vContainer != null){
				vTextViewTituloUsuarioDaTarefa.setText(
						HelperString.toUpperCaseSansAccent(
								vContainer.getTituloResponsavel()));
			String nome =  vContainer.getNomeResponsavel();
			if(nome != null && nome.length()>0)
				vTextViewUsuarioDaTarefa.setText( nome.toLowerCase());

		}else{
			vTextViewUsuarioDaTarefa.setVisibility(View.GONE);
			vTextViewTituloUsuarioDaTarefa.setVisibility(View.GONE);
		}
	}

	private void refreshTituloTarefa()
	{
		//Log.d(TAG,"refreshNomeTarefa()");

		if(tituloTarefa != null){
			String vNomeTarefa = tituloTarefa.toUpperCase();
			((TextView) findViewById(R.id.tv_titulo)).setText( vNomeTarefa);

		}
	}

	private void refreshDescricaoTarefa()
	{
		//Log.d(TAG,"refreshNomeTarefa()");

		if(descricaoTarefa != null){
			String vNomeTarefa = descricaoTarefa.toUpperCase();
			((TextView) findViewById(R.id.descricao_tarefa)).setText( vNomeTarefa);

		}
	}

	@Override
	public boolean loadData() throws Exception {
	    try{
	        db = new DatabasePontoEletronico(this);
            containerLocalizacao = GPSControler.getLocalizacao();

            return initFields();
        }finally{
	        if(db != null) db.close();
        }
	}

	public void refreshBotoes(int idEstadoTarefa) throws Exception {
		try{
			if(!objTarefa.isTarefaMinhaOuAbertaATodoUsuario()){

				if(estadoTarefa ==  EXTDAOTarefa.ESTADO_TAREFA.AGUARDANDO_INICIALIZACAO ){
					iniciarTarefaButton.setVisibility(View.VISIBLE);
					finalizarTarefaButton.setVisibility(View.GONE);
					cancelarTarefaButton.setVisibility(View.GONE);
				} else {
					iniciarTarefaButton.setVisibility(View.GONE);
					finalizarTarefaButton.setVisibility(View.GONE);
					cancelarTarefaButton.setVisibility(View.GONE);
				}
			}
			else {
				if (idEstadoTarefa == EXTDAOTarefa.ESTADO_TAREFA.AGUARDANDO_INICIALIZACAO.getId()) {
					iniciarTarefaButton.setVisibility(View.VISIBLE);
					finalizarTarefaButton.setVisibility(View.GONE);
					cancelarTarefaButton.setVisibility(View.VISIBLE);
				} else if (idEstadoTarefa == EXTDAOTarefa.ESTADO_TAREFA.EM_EXECUCAO.getId()) {
					iniciarTarefaButton.setVisibility(View.GONE);
					finalizarTarefaButton.setVisibility(View.VISIBLE);
					cancelarTarefaButton.setVisibility(View.VISIBLE);
				} else if (idEstadoTarefa == EXTDAOTarefa.ESTADO_TAREFA.FINALIZADA.getId()) {
					iniciarTarefaButton.setVisibility(View.GONE);
					finalizarTarefaButton.setVisibility(View.GONE);
					cancelarTarefaButton.setVisibility(View.GONE);
				}
				else { //cancelada
					iniciarTarefaButton.setVisibility(View.GONE);
					finalizarTarefaButton.setVisibility(View.GONE);
					cancelarTarefaButton.setVisibility(View.GONE);
				}
			}
		}catch(Exception ex){
			SingletonLog.openDialogError(DetailTarefaActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
		}

	}
    public class ToolbarHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
        	try{
				refreshBotoes(msg.what);
			}catch(Exception ex){
        		SingletonLog.openDialogError(DetailTarefaActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			}

        }
    }
	private void refreshToolbar() throws Exception {
		final String id = objTarefa.getId();

        iniciarTarefaButton = (Button) findViewById(R.id.iniciar_tarefa_button);
        finalizarTarefaButton = (Button) findViewById(R.id.finalizar_tarefa_button);
		cancelarTarefaButton= (Button) findViewById(R.id.cancelar_tarefa_button);
		iniciarTarefaButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try{
					if(EXTDAOTarefa.iniciarTarefa(DetailTarefaActivityMobile.this, id)) {
                        Toast.makeText(DetailTarefaActivityMobile.this, R.string.tarefa_inicializada, Toast.LENGTH_LONG).show();
                        handlerToolbar.sendEmptyMessage(EXTDAOTarefa.ESTADO_TAREFA.EM_EXECUCAO.getId());
                    }
				}catch(Exception ex){
					SingletonLog.openDialogError(DetailTarefaActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
				}
			}
		});


		finalizarTarefaButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try{
					if(EXTDAOTarefa.finalizarTarefa(DetailTarefaActivityMobile.this, id)) {
                        Toast.makeText(DetailTarefaActivityMobile.this, R.string.tarefa_finalizada, Toast.LENGTH_LONG).show();
                        handlerToolbar.sendEmptyMessage(EXTDAOTarefa.ESTADO_TAREFA.FINALIZADA.getId());
                    }
                }catch(Exception ex){
					SingletonLog.openDialogError(DetailTarefaActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
				}
			}
		});

		cancelarTarefaButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try{
					if(EXTDAOTarefa.cancelarTarefa(DetailTarefaActivityMobile.this, id)) {
						Toast.makeText(DetailTarefaActivityMobile.this, R.string.tarefa_cancelada, Toast.LENGTH_LONG).show();
						handlerToolbar.sendEmptyMessage(EXTDAOTarefa.ESTADO_TAREFA.CANCELADA.getId());
					}
				}catch(Exception ex){
					SingletonLog.openDialogError(DetailTarefaActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
				}
			}
		});




	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    private void refreshCacheDadosRota(){
        Bundle vParameters = getIntent().getExtras();
        try{
            //this.veiculoId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
            Integer pTarefaId = HelperInteger.parserInteger(vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID));
            if(pTarefaId != tarefaId){
                if(documentRouteInformationPontoOrigemDestino != null) {
                    documentRouteInformationPontoOrigemDestino = null;
                }
                if(documentRouteInformationPontoProprioOrigem != null){
                    documentRouteInformationPontoProprioOrigem = null;
                }
                tarefaId = pTarefaId;
            }

        } catch(Exception ex){
            SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
        }
    }
    private boolean initFields() throws Exception {

        refreshCacheDadosRota();
        descricaoTarefa = null;
        tituloTarefa= null;
        this.geoPointOrigem =null;
        this.geoPointDestino =null;

        this.estadoTarefa=null;
        HelperDate hd = new HelperDate();
        objTarefa = new EXTDAOTarefa(db);
        if(objTarefa.select(String.valueOf(tarefaId))){
            descricaoTarefa = objTarefa.getStrValueOfAttribute(EXTDAOTarefa.DESCRICAO);
            tituloTarefa = objTarefa.getStrValueOfAttribute(EXTDAOTarefa.TITULO);

            String vLatitudeOrigem = objTarefa.getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_LATITUDE_INT);
            String vLongitudeOrigem = objTarefa.getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_LONGITUDE_INT);
            if(vLatitudeOrigem != null &&
                    vLongitudeOrigem != null){
                int latitudeOrigem = HelperInteger.parserInt(vLatitudeOrigem);
                int longitudeOrigem = HelperInteger.parserInt(vLongitudeOrigem);
                this.geoPointOrigem = HelperMapa.getGeoPoint(latitudeOrigem, longitudeOrigem);
            }

            String vLatitudeDestino = objTarefa.getStrValueOfAttribute(EXTDAOTarefa.DESTINO_LATITUDE_INT);
            String vLongitudeDestino = objTarefa.getStrValueOfAttribute(EXTDAOTarefa.DESTINO_LONGITUDE_INT);
            if(vLatitudeDestino != null &&
                    vLongitudeDestino != null){
                int latitudeDestino = HelperInteger.parserInt(vLatitudeDestino);
                int longitudeDestino = HelperInteger.parserInt(vLongitudeDestino);
                this.geoPointDestino = HelperMapa.getGeoPoint(latitudeDestino, longitudeDestino);
            }


				datetimeInicioTarefa = objTarefa.getDatePorExtensoValueOfAttribute(
						EXTDAOTarefa.INICIO_SEC, EXTDAOTarefa.INICIO_OFFSEC);

				datetimeFimTarefa = objTarefa.getDatePorExtensoValueOfAttribute(
					EXTDAOTarefa.FIM_SEC, EXTDAOTarefa.FIM_OFFSEC);

				datetimeProgramada = objTarefa.getDatePorExtensoValueOfAttribute(
					EXTDAOTarefa.INICIO_HORA_PROGRAMADA_SEC, EXTDAOTarefa.INICIO_HORA_PROGRAMADA_OFFSEC);

				datatimeCadastro= objTarefa.getDatePorExtensoValueOfAttribute(
						EXTDAOTarefa.CADASTRO_SEC, EXTDAOTarefa.CADASTRO_OFFSEC);
			datatimePrazo = objTarefa.getDatePorExtensoValueOfAttribute(
					EXTDAOTarefa.PRAZO_SEC, EXTDAOTarefa.PRAZO_OFFSEC);

//


//				if(objTarefa.getStrValueOfAttribute(EXTDAOTarefa.FIM_DATA_DATE) != null)
//					datetimeFimTarefa = objTarefa.getStrValueOfAttribute(EXTDAOTarefa.FIM_DATA_DATE) + " " +  objTarefa.getStrValueOfAttribute(EXTDAOTarefa.FIM_HORA_TIME);
            estadoTarefa = objTarefa.getEstadoTarefa();


            return true;
        } else
            return false;
    }

	@Override
	protected void beforeInitializeComponents() {
		try{
			db = new DatabasePontoEletronico(this);
			refreshToolbar();
			refreshTituloTarefa();
			refreshDescricaoTarefa();
			refreshDatetimeCadastroTarefa();
			refreshDatetimeInicioProgramado();

			refreshDatetimePrazo();
			refreshDatetimeInicioTarefa();
			refreshDatetimeFimTarefa();
			refreshUsuarioResponsavel();
			refreshDatetimeInicioTarefa();

			refreshAppointmentPlaceOrigem();
			refreshAppointmentPlaceDestino();

			refreshBotoes(estadoTarefa.getId());
//			refreshHeader();
			//refreshProprioAppointmentPlaces();
			//refreshRotaMeuPontoAteOrigem();

			//refreshDestinoAppointmentPlaces();
			//refreshRotaOrigemAteDestino();
			//refreshSumarioRota();
		} catch(Exception ex){
			SingletonLog.openDialogError(this, ex, SingletonLog.TIPO.PAGINA);
		}finally{
			if(db != null ) db.close();
		}
	}

	@Override
	public Table getEXTDAO() {
		return new EXTDAOTarefa(db);
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if (requestCode == OmegaConfiguration.STATE_RASTREAR_CARRO) {

			if(intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO)){
				String veiculoUsuarioId = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO);
				Intent intent2 = getIntent();
				intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, veiculoUsuarioId);
				setResult(OmegaConfiguration.STATE_RASTREAR_CARRO, intent2);
				finish();
			} else finish();
		} else super.onActivityResult(requestCode, resultCode, intent);
	}


//	private void refreshHeader() {
//		Bundle params = getIntent().getExtras();
//		try {
//			final String id = params.getString(OmegaConfiguration.SEARCH_FIELD_ID);
//			View viewHeader = findViewById(R.id.header);
//			if (viewHeader != null) {
//				ImageButton editarTopo = (ImageButton) viewHeader.findViewById(R.id.editar_topo_button);
//				editarTopo.setOnClickListener(new View.OnClickListener() {
//
//					@Override
//					public void onClick(View v) {
//						// editar_topo_button
//						Intent intent = new Intent(
//								DetailTarefaActivityMobile.this
//								, FormTarefaActivityMobile.class);
//						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, id);
//						DetailTarefaActivityMobile.this.startActivityForResult(intent,
//								OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
//
//					}
//				});
//			}
//		} catch (Exception ex) {
//			FactoryAlertDialog.makeToast(this, ex, null);
//		}
//	}

}
