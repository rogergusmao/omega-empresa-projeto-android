package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;

public class DetailTarefaMapActivityMobile extends OmegaRegularActivity{

	@Override
	public boolean loadData() {
		
		return false;
	}

	@Override
	public void initializeComponents() {
		
		
	}

//
//	//Close button
//	//	private Button closeButton;
//	private static final String TAG = "DetailTarefaMapActivityMobile";
//
//	private DatabasePontoEletronico db=null;
//	String idTabelaSincronizador = "";
//	private FactoryAppointmentPlaceAdapter appointmentPlacesOrigem;
//	private FactoryAppointmentPlaceAdapter appointmentPlacesDestino;
//	private FactoryAppointmentPlaceAdapter appointmentPlacesProprio;
//	String idSincronizador = null;
//	static private Integer tarefaId = null;
//	private Integer veiculoUsuarioId = null;
//
//	private String distanciaMinhaPosicaoAtePontoOrigem = null;
//	private String tempoEstimadoaMinhaPosicaoAtePontoOrigem = null;
//
//	private String distanciaAtePontoDestino = null;
//	private String tempoEstimadoaPontoOrigemAtePontoDestino = null;
//	
//	private String distanciaMinhaPosicaoAtePontoDestino = null;
//	private String tempoEstimadoMinhaPosicaoAtePontoDestino = null;
//	
//	private static String enderecoProprio = null;
//	private String enderecoOrigem = null;
//	private String enderecoDestino = null;
//
//	
//	Button selecionarTarefaButton ;
//	Button inicializarTarefaButton;
//	Button finalizarTarefaButton; 
//	
//	GeoPoint geoPointOrigem = null;
//	 
//	GeoPoint geoPointDestino = null;
//	
//	static GeoPoint geoPointProprio = null;
//	
//	static Document documentRouteInformationPontoProprioOrigem = null;
//	static Document documentRouteInformationPontoOrigemDestino = null;
//
//	TYPE_TAREFA typeTarefa = TIPO_TAREFA.ABERTA;
//	
//	String datatimeCadastro = null;
//	String dataAbertura = null;
//	
//	String dataProgramada = null;
//	
//	String datetimeInicioTarefa = null;
//	String datetimeFimTarefa = null;
//
//	String nomeTarefa = null;
//	
//	private Typeface customTypeFace;
//
//	@Override
//	protected void onStop(){
//		if(db != null)
//			db.close();
//		
//		super.onStop();
//	}
//	@Override
//	protected void onPause(){
//		if(db != null)
//			db.close();
//		
//		super.onPause();
//	}
//	
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
//		super.onCreate(savedInstanceState);
//
//		setContentView(R.layout.detail_tarefa_map_activity_mobile_layout);
//		this.db = new DatabasePontoEletronico(this);
//		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
//
//		Bundle vParameters = getIntent().getExtras();
//		try{
//			//Gets veiculo id from the Intent used to create this activity
//			//TODO: Test connecting between ListResultActivity and VeiculoDetailsActivity
//			//this.veiculoId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
//			Integer pTarefaId = HelperInteger.parserInteger(vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID));
//			if(pTarefaId != tarefaId){
//				if(documentRouteInformationPontoOrigemDestino != null) {
//					documentRouteInformationPontoOrigemDestino = null;
//				}
//				if(documentRouteInformationPontoProprioOrigem != null){
//					documentRouteInformationPontoProprioOrigem = null;
//				}
//				tarefaId = pTarefaId;
//			}
//			if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO))
//				this.veiculoUsuarioId  = vParameters.getInt(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO);
//			
//			if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_TITULO_TAREFA)){
//				this.nomeTarefa= vParameters.getString(OmegaConfiguration.SEARCH_FIELD_TITULO_TAREFA);
//			}
//			
//			this.typeTarefa= (TYPE_TAREFA)vParameters.get(OmegaConfiguration.SEARCH_FIELD_TYPE_TAREFA);
//			
//			
//			if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LATITUDE_PROPRIA) && 
//					vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LONGITUDE_PROPRIA)){
//				
//				Integer latitudePropria = vParameters.getInt(OmegaConfiguration.SEARCH_FIELD_LATITUDE_PROPRIA);
//				Integer longitudePropria = vParameters.getInt(OmegaConfiguration.SEARCH_FIELD_LONGITUDE_PROPRIA);
//				GeoPoint pGeoPointProprio = HelperMapa.getGeoPoint(latitudePropria, longitudePropria);
//				
//				if(geoPointProprio == null ){
//					geoPointProprio = pGeoPointProprio;
//					enderecoProprio = HelperMapa.getAddressFromPoint(this, latitudePropria, longitudePropria);
//				}
//				else if (pGeoPointProprio == null){
//					geoPointProprio = null;
//					enderecoProprio = null;
//				}
//				else if(! HelperMapa.isEqualGeoPoint(pGeoPointProprio, geoPointProprio)){
//					geoPointProprio = pGeoPointProprio;
//					enderecoProprio = HelperMapa.getAddressFromPoint(this, latitudePropria, longitudePropria);
//				}
//			}
//			if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_DATETIME_INICIO_TAREFA)){
//				this.datetimeInicioTarefa = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_DATETIME_INICIO_TAREFA);
//			}
//			
//			if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_DATETIME_FIM_TAREFA)){
//				this.datetimeFimTarefa = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_DATETIME_FIM_TAREFA);
//			}
//			
//			if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LATITUDE_ORIGEM) && 
//					vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LONGITUDE_ORIGEM)){
//				Integer latitudeOrigem = vParameters.getInt(OmegaConfiguration.SEARCH_FIELD_LATITUDE_ORIGEM);
//				Integer longitudeOrigem = vParameters.getInt(OmegaConfiguration.SEARCH_FIELD_LONGITUDE_ORIGEM);
//				this.geoPointOrigem = HelperMapa.getGeoPoint(latitudeOrigem, longitudeOrigem);
//			}
//
//			if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LATITUDE_DESTINO) && 
//					vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LONGITUDE_DESTINO)){
//				int latitudeDestino = vParameters.getInt(OmegaConfiguration.SEARCH_FIELD_LATITUDE_DESTINO); 
//				int longitudeDestino = vParameters.getInt(OmegaConfiguration.SEARCH_FIELD_LONGITUDE_DESTINO);
//				this.geoPointDestino= HelperMapa.getGeoPoint(latitudeDestino, longitudeDestino);
//			}
//
//			if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ENDERECO_ORIGEM) ){
//				this.enderecoOrigem = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ENDERECO_ORIGEM);
//				if(this.geoPointOrigem == null && enderecoOrigem != null){
//					this.geoPointOrigem  = HelperMapa.getGeoPointDoEndereco(enderecoOrigem);	
//				}
//			}
//
//			if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ENDERECO_DESTINO) ){
//				this.enderecoDestino = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ENDERECO_DESTINO);
//				if(this.geoPointDestino == null && enderecoDestino != null){
//					this.geoPointDestino  = HelperMapa.getGeoPointDoEndereco(enderecoDestino);	
//				}
//			}
//
//			if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_DATETIME_CADASTRO)){
//				this.datatimeCadastro = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_DATETIME_CADASTRO);	
//			}
//			
//			if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_DATE_ABERTURA)){
//				this.dataAbertura = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_DATE_ABERTURA);	
//			}
//			
//			if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_DATETIME_INICIO_PROGRAMADO_TAREFA)){
//				this.dataProgramada = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_DATETIME_INICIO_PROGRAMADO_TAREFA);	
//			}
//		} catch(Exception ex){
//
//		}
//		ContainerRouteInformation vDistanceProprioOrigem = null;
//		ContainerRouteInformation vDistanceOrigemDestino = null;
//		
//
//		if(geoPointOrigem != null && geoPointProprio != null){
//			if(documentRouteInformationPontoProprioOrigem == null)
//			documentRouteInformationPontoProprioOrigem = HelperRoutePath.getDocumentInformationRoute(geoPointProprio, geoPointOrigem, 999, MapaListMarkerActivity.map );
//			vDistanceProprioOrigem = HelperRoutePath.getDistanceOfRoute(documentRouteInformationPontoProprioOrigem);
//
//			if(vDistanceProprioOrigem != null){
//				distanciaMinhaPosicaoAtePontoOrigem = vDistanceProprioOrigem.printDistancia();
//				tempoEstimadoaMinhaPosicaoAtePontoOrigem = vDistanceProprioOrigem.printTempo();
//			}
//		} 
//		
//		if(geoPointOrigem != null && geoPointDestino != null){
//			if(documentRouteInformationPontoOrigemDestino == null)
//				documentRouteInformationPontoOrigemDestino= HelperRoutePath.getDocumentInformationRoute(geoPointOrigem, geoPointDestino, 999, MapaListMarkerActivity.map );
//			vDistanceOrigemDestino = HelperRoutePath.getDistanceOfRoute(documentRouteInformationPontoOrigemDestino);
//			if(vDistanceOrigemDestino != null){
//				distanciaAtePontoDestino = vDistanceOrigemDestino.printDistancia();
//				tempoEstimadoaPontoOrigemAtePontoDestino = vDistanceOrigemDestino.printTempo();
//			}
//		} 
//		
//		if(vDistanceOrigemDestino != null && 
//				vDistanceProprioOrigem != null){
//			ContainerDateHour vContainerDate = HelperDate.getStrSomaDiaDataHora(
//					vDistanceProprioOrigem.tempoDia, vDistanceProprioOrigem.tempoHora, vDistanceProprioOrigem.tempoMinuto, 
//					vDistanceOrigemDestino.tempoDia, vDistanceOrigemDestino.tempoHora, vDistanceOrigemDestino.tempoMinuto);
//			
//			Float vDistanciaOrigemDestino = HelperFisicaMecanica.getDistanciaMetro(vDistanceOrigemDestino.distancia, vDistanceOrigemDestino.unidadeDistancia);
//			Float vDistanciaProprioOrigem = HelperFisicaMecanica.getDistanciaMetro(vDistanceProprioOrigem.distancia, vDistanceOrigemDestino.unidadeDistancia);
//			Float vDistanciaMetroTotal = vDistanciaOrigemDestino + vDistanciaProprioOrigem;
//			vDistanciaMetroTotal =  vDistanciaMetroTotal/1000;
//			distanciaMinhaPosicaoAtePontoDestino = String.valueOf(vDistanciaMetroTotal) + " km";   
//			tempoEstimadoMinhaPosicaoAtePontoDestino = ContainerRouteInformation.printTempo(
//					vContainerDate.tempoDia, 
//					vContainerDate.tempoHora, 
//					vContainerDate.tempoMinuto); 
//			
//		}
//		
//		new CustomDataLoader(this).execute();
//	}
//	
//	@Override
//	public void onBackPressed(){
//		try{
////			super.onBackPressed();
//			finish();
//		} catch(Exception ex){
//			
//		}
//		
//	}
//	
//	private boolean isModificated = false;
//	
//	private class CustomListener implements OnClickListener
//	{		
//		Activity activity;
//		public CustomListener(Activity pContext){
//			activity = pContext;
//		}
//		public void onClick(View v) {
//
//			switch (v.getId()) {
//			case R.id.tracar_rota_origem_destino_button:
//				ArrayList<RotaOverlay> vListOverlay = HelperRoutePath.getListOverlayItemOfRoute(geoPointOrigem, geoPointDestino, 999, documentRouteInformationPontoOrigemDestino);
//				if(vListOverlay != null)
//					MapaListMarkerActivity.listRotaOverlayItem = vListOverlay;
//				Intent intent = getIntent();
//				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_TAREFA, tarefaId);
//				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_DISPLAY_ROTA, true);
//				setResult(OmegaConfiguration.STATE_TRACAR_ROTA, intent);
//				finish();
//				break;
//			case R.id.tracar_rota_meu_ponto_origem_button:
//				ArrayList<RotaOverlay> vListOverlay2 = HelperRoutePath.getListOverlayItemOfRoute(geoPointProprio, geoPointOrigem, 999, documentRouteInformationPontoProprioOrigem);
//				if(vListOverlay2 != null)
//					MapaListMarkerActivity.listRotaOverlayItem = vListOverlay2;
//				
//				Intent intent2 = getIntent();
//				intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_DISPLAY_ROTA, true);
//				intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_TAREFA, tarefaId);
//				setResult(OmegaConfiguration.STATE_TRACAR_ROTA, intent2);
//				finish();
//				break;
//			case R.id.escolher_tarefa_button:
//				
//				String vRetorno =  HelperHttpPost.getConteudoStringPost(
//						activity, 
//						OmegaConfiguration.URL_REQUEST_CHECK_VEICULO_USUARIO_TAREFA, 
//						new String[] {
//								"usuario",
//								"id_corporacao",
//								"corporacao",
//								"senha",
//								"imei",
//								"id_programa",
//								"sistema_tabela",
//								"veiculo_usuario", 
//								"tarefa"}, 
//						new String[] {
//								OmegaSecurity.getIdUsuario(),
//								OmegaSecurity.getIdCorporacao(),
//								OmegaSecurity.getCorporacao(),
//								OmegaSecurity.getSenha(),
//								HelperPhone.getIMEI(),
//								ContainerPrograma.getIdPrograma(),
//								idTabelaSincronizador,
//								String.valueOf(veiculoUsuarioId), 
//								String.valueOf(tarefaId)});
//				if(vRetorno != null){
//					if(vRetorno.compareTo("TRUE") == 0){
//						isModificated = true;
//						ServiceSynchronizeListTarefaAberta.trocaTuplaTarefaAbertaParaAbertaPropria(tarefaId);
//						
//						Button vSelecionarTarefaButton = (Button) findViewById(R.id.escolher_tarefa_button);
//						
//						vSelecionarTarefaButton.setVisibility(View.GONE);
//						inicializarTarefaButton.setVisibility(View.VISIBLE);
//						
//						Bundle vParameter = activity.getIntent().getExtras();
//						
//						showDialog(DetailTarefaMapActivityMobile.FORM_DIALOG_CADASTRO_OK);
//					} else if(vRetorno.contains("ERROR 1")){
//						showDialog(DetailTarefaMapActivityMobile.FORM_DIALOG_TAREFA_OCUPADA);
//						Integer vIndexVetorTarefaAberta = ServiceSynchronizeListTarefaAberta.getIndexTarefaAberta(tarefaId);
//						if( vIndexVetorTarefaAberta != null){
//							ServiceSynchronizeListTarefaAberta.removeTarefaAberta(vIndexVetorTarefaAberta);
//						}
//					} else {
//						showDialog(DetailTarefaMapActivityMobile.FORM_DIALOG_PARAMETRO_INVALIDO);
//					}	
//				}
//				
//				break;	
//			case R.id.iniciar_tarefa_button:
//				
//				String vRetorno2 =  HelperHttpPost.getConteudoStringPost(
//						activity, 
//						OmegaConfiguration.URL_REQUEST_CHECK_SET_DATA_INICIO_TAREFA, 
//						new String[] {
//								"usuario",
//								"corporacao",
//								"senha",
//								"imei",
//								"id_programa",
//								"sistema_tabela",
//								"veiculo_usuario", 
//								"tarefa"}, 
//						new String[] {
//								OmegaSecurity.getIdUsuario(),
//								OmegaSecurity.getIdCorporacao(),
//								OmegaSecurity.getSenha(),
//								HelperPhone.getIMEI(),
//								ContainerPrograma.getIdPrograma(),
//								idTabelaSincronizador,
//								String.valueOf(veiculoUsuarioId), 
//								String.valueOf(tarefaId)});
//				String vTokenValidade = "TRUE: ";
//				if(vRetorno2 != null){
//					String vSubstringDatetime =vRetorno2.substring(vTokenValidade.length(), vRetorno2.length()  - 1);
//					if(vRetorno2.startsWith(vTokenValidade) && vSubstringDatetime.length() > 0 ){
//						inicializarTarefaButton.setVisibility(View.GONE);
//						finalizarTarefaButton.setVisibility(View.VISIBLE);
//						
//						isModificated = true;
//						ServiceSynchronizeListTarefaAberta.trocaTuplaTarefaAbertaPropriaParaPropriaExecucao(tarefaId);
//						
//						activity.getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_DATETIME_INICIO_TAREFA, vSubstringDatetime);
//						
//						TarefaCustomOverlayItem vCustom = MapaListMarkerActivity.getCustomOverlayItemDaTarefa(tarefaId);
//						if(vCustom != null){
//							vCustom.setDatetimeInicioTarefa(vSubstringDatetime);
//						}
////						Integer vIndexVetorTarefaAberta = ServiceSynchronizeListTarefaAberta.getIndexTarefaAberta(tarefaId);
////						if( vIndexVetorTarefaAberta != null){
////							ServiceSynchronizeListTarefaAberta.removeTarefaAberta(vIndexVetorTarefaAberta);
////						}
//						
//						showDialog(DetailTarefaMapActivityMobile.FORM_DIALOG_TAREFA_INICIALIZADA);
//						
//					} else if(vRetorno2.contains("ERROR 1")){
//						showDialog(DetailTarefaMapActivityMobile.FORM_DIALOG_TAREFA_OCUPADA_POR_OUTRO_USUARIO);
//						Integer vIndexVetorTarefaAberta = ServiceSynchronizeListTarefaAberta.getIndexTarefaAberta(tarefaId);
//						if( vIndexVetorTarefaAberta != null){
//							ServiceSynchronizeListTarefaAberta.removeTarefaAberta(vIndexVetorTarefaAberta);
//						}
//					} else {
//						showDialog(DetailTarefaMapActivityMobile.FORM_DIALOG_PARAMETRO_INVALIDO);
//					}	
//				}
//				
//				break;
//			case R.id.finalizar_tarefa_button:
//				String vRetorno3 =  HelperHttpPost.getConteudoStringPost(
//						activity, 
//						OmegaConfiguration.URL_REQUEST_CHECK_SET_DATA_FIM_TAREFA, 
//						new String[] {
//								"usuario",
//								"corporacao",
//								"senha",
//								"imei",
//								"id_programa",
//								"sistema_tabela",
//								"veiculo_usuario", 
//								"tarefa"}, 
//						new String[] {
//								OmegaSecurity.getIdUsuario(),
//								OmegaSecurity.getIdCorporacao(),
//								OmegaSecurity.getSenha(),
//								HelperPhone.getIMEI(),
//								ContainerPrograma.getIdPrograma(),
//								idTabelaSincronizador,
//								String.valueOf(veiculoUsuarioId), 
//								String.valueOf(tarefaId)});
//				
//				String vTokenValidade3 = "TRUE: ";
//				if(vRetorno3 != null){
//					String vSubstringDatetime =vRetorno3.substring(vTokenValidade3.length(), vRetorno3.length()  - 1);
//					if(vRetorno3.startsWith(vTokenValidade3) && vSubstringDatetime.length() > 0 ){
//						isModificated = true;
//						inicializarTarefaButton.setVisibility(View.GONE);
//						finalizarTarefaButton.setVisibility(View.GONE);
//						ServiceSynchronizeListTarefaAberta.trocaTuplaTarefaPropriaExecucaoParaPropriaConcluida(tarefaId);
//						activity.getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_DATETIME_FIM_TAREFA, vSubstringDatetime);
//						TarefaCustomOverlayItem vCustom = MapaListMarkerActivity.getCustomOverlayItemDaTarefa(tarefaId);
//						if(vCustom != null){
//							vCustom.setDatetimeFimTarefa(vSubstringDatetime);
//						}
//						showDialog(DetailTarefaMapActivityMobile.FORM_DIALOG_TAREFA_FINALIZADA);
//					} else if(vRetorno3.contains("ERROR 1")){
//						showDialog(DetailTarefaMapActivityMobile.FORM_DIALOG_TAREFA_OCUPADA_POR_OUTRO_USUARIO);
//						Integer vIndexVetorTarefaAberta = ServiceSynchronizeListTarefaAberta.getIndexTarefaAberta(tarefaId);
//						if( vIndexVetorTarefaAberta != null){
//							ServiceSynchronizeListTarefaAberta.removeTarefaAberta(vIndexVetorTarefaAberta);
//						}
//					} else {
//						showDialog(DetailTarefaMapActivityMobile.FORM_DIALOG_PARAMETRO_INVALIDO);
//					}	
//				}
//				
//				break;	
//			default:
//				break;
//			}
//			
//			
//			
//		}
//		
//	}
//
//	
//	@Override
//	protected Dialog onCreateDialog(int id) {
//
//		Dialog vDialog = null;
//
//		switch (id) {
//
//		default:
//			vDialog = this.getErrorDialog(id);
//			break;
//		}
//		return vDialog;
//	}	
//
//	private final static int FORM_DIALOG_CADASTRO_OK = 1;
//	private final static int FORM_DIALOG_TAREFA_OCUPADA = 2;
//	private final static int FORM_DIALOG_PARAMETRO_INVALIDO = 3;
//	private final static int FORM_DIALOG_TAREFA_OCUPADA_POR_OUTRO_USUARIO = 4;
//	private final static int FORM_DIALOG_TAREFA_INICIALIZADA = 5;
//	private final static int FORM_DIALOG_TAREFA_FINALIZADA = 6;
//	private Dialog getErrorDialog(int dialogType)
//	{
//
//		Dialog vDialog = new Dialog(this);
//		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		
//		vDialog.setContentView(R.layout.dialog);
//
//		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
//		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);
//
//		switch (dialogType) {
//		case FORM_DIALOG_CADASTRO_OK:
//			vTextView.setText(getResources().getString(R.string.cadastro_ok));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_DIALOG_CADASTRO_OK);
//				}
//			});
//			break;
//			
//		case FORM_DIALOG_TAREFA_OCUPADA:
//			vTextView.setText(getResources().getString(R.string.tarefa_ocupada));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_DIALOG_TAREFA_OCUPADA);
//				}
//			});
//			break;
//		case FORM_DIALOG_TAREFA_FINALIZADA:
//			vTextView.setText(getResources().getString(R.string.tarefa_finalizada));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_DIALOG_TAREFA_FINALIZADA);
//				}
//			});
//			break;	
//		case FORM_DIALOG_TAREFA_INICIALIZADA:
//			vTextView.setText(getResources().getString(R.string.tarefa_inicializada));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_DIALOG_TAREFA_INICIALIZADA);
//				}
//			});
//			break;
//		case FORM_DIALOG_TAREFA_OCUPADA_POR_OUTRO_USUARIO:
//			vTextView.setText(getResources().getString(R.string.tarefa_ocupada_por_outro_usuario));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_DIALOG_TAREFA_OCUPADA_POR_OUTRO_USUARIO);
//				}
//			});
//			break;
//			
//		case FORM_DIALOG_PARAMETRO_INVALIDO:
//			vTextView.setText(getResources().getString(R.string.tarefa_parametro_invalido));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_DIALOG_PARAMETRO_INVALIDO);
//				}
//			});
//			break;
//				
//		default:
//			return null;
//			
//		}
//
//		return vDialog;
//	}
//	
//	/**
//	 * Updates the LinearLayout with the empresa appointment places 
//	 */
//	private void refreshAppointmentPlaceOrigem()
//	{
//		//Log.d(TAG,"refreshAppointmentPlaces()");
//		LinearLayout vAppointmentPlacesLinearLayout = (LinearLayout) findViewById(R.id.detail_tarefa_endereco_origem_linearlayout);
//		for(int i=0;i<this.appointmentPlacesOrigem.getCount();i++)
//		{
//			vAppointmentPlacesLinearLayout.addView(this.appointmentPlacesOrigem.getView(i, null, null));
//		}
//		((TextView) findViewById(R.id.detail_tarefa_titulo_endereco_de_origem_textview)).setTypeface(this.customTypeFace);
//	}
//	
//	private void refreshAppointmentPlaceProprio()
//	{
//		//Log.d(TAG,"refreshAppointmentPlaces()");
//		LinearLayout vAppointmentPlacesLinearLayout = (LinearLayout) findViewById(R.id.detail_tarefa_endereco_proprio_linearlayout);
//		for(int i=0;i<this.appointmentPlacesProprio.getCount();i++)
//		{
//			vAppointmentPlacesLinearLayout.addView(this.appointmentPlacesProprio.getView(i, null, null));
//		}
//		((TextView) findViewById(R.id.detail_tarefa_titulo_endereco_de_proprio_textview)).setTypeface(this.customTypeFace);
//	}
//
//	private void refreshAppointmentPlaceDestino()
//	{
//		//Log.d(TAG,"refreshAppointmentPlaces()");
//		LinearLayout vAppointmentPlacesLinearLayout = (LinearLayout) findViewById(R.id.detail_tarefa_endereco_destino_linearlayout);
//		for(int i=0;i<this.appointmentPlacesDestino.getCount();i++)
//		{
//			vAppointmentPlacesLinearLayout.addView(this.appointmentPlacesDestino.getView(i, null, null));
//		}
//		((TextView) findViewById(R.id.detail_tarefa_titulo_endereco_de_destino_textview)).setTypeface(this.customTypeFace);
//	}
//	
//	@Override
//	public void finish(){
//		
//		try{
//			if(isModificated){
//				Intent intent = getIntent();
//				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, isModificated);
//				setResult(OmegaConfiguration.STATE_RECARREGAR_ADAPTER, intent);
//			}
//			db.close();
//			super.finish();
//		}catch(Exception ex){
//
//		}
//	}
//
//	private void refreshDistanciaMinhaPosicaoPontoDestino()
//	{
//		//Log.d(TAG,"refreshNomeTarefa()");
//		
//		if(distanciaAtePontoDestino != null){
//			
//			((TextView) findViewById(R.id.detail_tarefa_distancia_minha_localizacao_ponto_destino_textview)).setText( distanciaMinhaPosicaoAtePontoDestino);
//			((TextView) findViewById(R.id.detail_tarefa_distancia_minha_localizacao_ponto_destino_textview)).setTypeface(this.customTypeFace);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_distancia_minha_localizacao_ponto_destino_textview)).setTypeface(this.customTypeFace);
//		}
//	}
//	
//	private void refreshDistanciaPontoOrigemPontoDestino()
//	{
//		//Log.d(TAG,"refreshNomeTarefa()");
//		
//		if(distanciaAtePontoDestino != null){
//			
//			((TextView) findViewById(R.id.detail_tarefa_distancia_ponto_origem_ponto_destino_textview)).setText( distanciaAtePontoDestino);
//			((TextView) findViewById(R.id.detail_tarefa_distancia_ponto_origem_ponto_destino_textview)).setTypeface(this.customTypeFace);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_distancia_ponto_origem_ponto_destino_textview)).setTypeface(this.customTypeFace);
//		}
//	}
//	
//	private void refreshDistanciaMinhaPosicaoPontoOrigem()
//	{
//		//Log.d(TAG,"refreshNomeTarefa()");
//		
//		if(distanciaMinhaPosicaoAtePontoOrigem != null){
//			
//			((TextView) findViewById(R.id.detail_tarefa_distancia_minha_posicao_ponto_origem_textview)).setText( distanciaMinhaPosicaoAtePontoOrigem);
//			((TextView) findViewById(R.id.detail_tarefa_distancia_minha_posicao_ponto_origem_textview)).setTypeface(this.customTypeFace);
//			((TextView) findViewById(R.id.titulo_detail_tarefa_distancia_minha_posicao_ponto_origem_textview)).setTypeface(this.customTypeFace);
//		}
//	}
//	
//	private void refreshDatetimeCadastroTarefa()
//	{
//		//Log.d(TAG,"refreshDatetimeTarefa()");
//		
//		if(this.datatimeCadastro != null){
//			
//			((TextView) findViewById(R.id.detail_tarefa_data_cadastro_textview)).setText( datatimeCadastro);
//			((TextView) findViewById(R.id.detail_tarefa_data_cadastro_textview)).setTypeface(this.customTypeFace);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_data_cadastro_textview)).setTypeface(this.customTypeFace);
//		} else{
//			((TextView) findViewById(R.id.detail_tarefa_data_cadastro_textview)).setVisibility(View.GONE);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_data_cadastro_textview)).setVisibility(View.GONE);
//		}
//	}
//	
//	private void refreshDatetimeInicioTarefa()
//	{
//		//Log.d(TAG,"refreshDatetimeInicioTarefa()");
//		
//		if(this.datetimeInicioTarefa != null){
//			
//			((TextView) findViewById(R.id.detail_tarefa_data_inicio_textview)).setText( this.datetimeInicioTarefa);
//			((TextView) findViewById(R.id.detail_tarefa_data_inicio_textview)).setTypeface(this.customTypeFace);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_data_inicio_textview)).setTypeface(this.customTypeFace);
//		} else{
//			((TextView) findViewById(R.id.detail_tarefa_data_inicio_textview)).setVisibility(View.GONE);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_data_inicio_textview)).setVisibility(View.GONE);
//		}
//	}
//	
//	private void refreshDatetimeFimTarefa()
//	{
//		//Log.d(TAG,"refreshDatetimeFimTarefa()");
//		
//		if(this.datetimeFimTarefa != null){
//			
//			((TextView) findViewById(R.id.detail_tarefa_data_fim_textview)).setText( this.datetimeFimTarefa);
//			((TextView) findViewById(R.id.detail_tarefa_data_fim_textview)).setTypeface(this.customTypeFace);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_data_fim_textview)).setTypeface(this.customTypeFace);
//		} else{
//			((TextView) findViewById(R.id.detail_tarefa_data_fim_textview)).setVisibility(View.GONE);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_data_fim_textview)).setVisibility(View.GONE);
//		}
//	}
//	
//	private void refreshDatetimeInicioProgramado()
//	{
//		//Log.d(TAG,"refreshDatetimeTarefa()");
//		
//		if(this.dataProgramada != null){
//			
//			((TextView) findViewById(R.id.detail_tarefa_data_programada_textview)).setText( dataProgramada);
//			((TextView) findViewById(R.id.detail_tarefa_data_programada_textview)).setTypeface(this.customTypeFace);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_data_programada_textview)).setTypeface(this.customTypeFace);
//		} else{
//			((TextView) findViewById(R.id.detail_tarefa_data_programada_textview)).setVisibility(View.GONE);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_data_programada_textview)).setVisibility(View.GONE);
//		}
//	}
//	
//	
//	private void refreshDateAberturaTarefa()
//	{
//		//Log.d(TAG,"refreshDatetimeTarefa()");
//		
//		if(this.dataAbertura != null){
//			
//			((TextView) findViewById(R.id.detail_tarefa_data_abertura_textview)).setText( dataAbertura);
//			((TextView) findViewById(R.id.detail_tarefa_data_abertura_textview)).setTypeface(this.customTypeFace);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_data_abertura_textview)).setTypeface(this.customTypeFace);
//		} else{
//			((TextView) findViewById(R.id.detail_tarefa_data_abertura_textview)).setVisibility(View.GONE);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_data_abertura_textview)).setVisibility(View.GONE);
//		}
//	}
//
//	
//	private void refreshTempoMinhaPosicaoPontoDestino()
//	{
//		//Log.d(TAG,"refreshNomeTarefa()");
//		
//		if(tempoEstimadoaMinhaPosicaoAtePontoOrigem != null){
//			
//			((TextView) findViewById(R.id.detail_tarefa_tempo_estimado_minha_localizacao_ponto_destino_textview)).setText( tempoEstimadoMinhaPosicaoAtePontoDestino);
//			((TextView) findViewById(R.id.detail_tarefa_tempo_estimado_minha_localizacao_ponto_destino_textview)).setTypeface(this.customTypeFace);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_tempo_estimado_minha_localizacao_ponto_destino_textview)).setTypeface(this.customTypeFace);
//		}
//	}
//	
//	private void refreshTempoMinhaPosicaoPontoOrigem()
//	{
//		//Log.d(TAG,"refreshNomeTarefa()");
//		
//		if(tempoEstimadoaMinhaPosicaoAtePontoOrigem != null){
//			
//			((TextView) findViewById(R.id.detail_tarefa_tempo_estimado_minha_posicao_ponto_origem_textview)).setText( tempoEstimadoaMinhaPosicaoAtePontoOrigem);
//			((TextView) findViewById(R.id.detail_tarefa_tempo_estimado_minha_posicao_ponto_origem_textview)).setTypeface(this.customTypeFace);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_tempo_estimado_minha_posicao_ponto_origem_textview)).setTypeface(this.customTypeFace);
//		}
//	}
//	
//	private void refreshTempoEstimadoMinhaPosicaoAtePontoDestino()
//	{
//		//Log.d(TAG,"refreshNomeTarefa()");
//		
//		if(tempoEstimadoaMinhaPosicaoAtePontoOrigem != null){
//			
//			((TextView) findViewById(R.id.detail_tarefa_tempo_estimado_minha_posicao_ponto_origem_textview)).setText( tempoEstimadoaMinhaPosicaoAtePontoOrigem);
//			((TextView) findViewById(R.id.detail_tarefa_tempo_estimado_minha_posicao_ponto_origem_textview)).setTypeface(this.customTypeFace);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_tempo_estimado_minha_posicao_ponto_origem_textview)).setTypeface(this.customTypeFace);
//		}
//	}
//	
//	private void refreshTempoPontoOrigemPontoDestino()
//	{
//		//Log.d(TAG,"refreshNomeTarefa()");
//		
//		if(tempoEstimadoaPontoOrigemAtePontoDestino != null){
//			
//			((TextView) findViewById(R.id.detail_tarefa_tempo_estimado_ponto_origem_ponto_destino_textview)).setText( tempoEstimadoaPontoOrigemAtePontoDestino);
//			((TextView) findViewById(R.id.detail_tarefa_tempo_estimado_ponto_origem_ponto_destino_textview)).setTypeface(this.customTypeFace);
//			((TextView) findViewById(R.id.detail_tarefa_titulo_tempo_estimado_ponto_origem_ponto_destino_textview)).setTypeface(this.customTypeFace);
//		}
//	}
//	
//	
//	
//	private void refreshNomeTarefa()
//	{
//		//Log.d(TAG,"refreshNomeTarefa()");
//		
//		if(nomeTarefa != null){
//			String vNomeTarefa = nomeTarefa.toUpperCase();
//			((TextView) findViewById(R.id.titulo_nome_tarefa_textview)).setText( vNomeTarefa);
//			((TextView) findViewById(R.id.titulo_nome_tarefa_textview)).setTypeface(this.customTypeFace);
//		}
//	}
//	
//	private void refreshProprioAppointmentPlaces()
//	{
//		//Log.d(TAG,"refreshNomeTarefa()");
//		if(enderecoProprio != null){
//			this.appointmentPlacesProprio = new FactoryAppointmentPlaceAdapter(
//					this, 
//					new AppointmentPlaceItemList(
//							this, 
//							tarefaId, 
//							enderecoProprio ), 
//					FactoryMapClickListener.TYPE_RASTREAR_TAREFA_MINHA_LOCALIZACAO);
//			
//			refreshAppointmentPlaceProprio();
//				
//			((TextView) findViewById(R.id.detail_tarefa_titulo_endereco_de_proprio_textview)).setTypeface(this.customTypeFace);
//		} else {
//			LinearLayout vAppointmentPlacesLinearLayout = (LinearLayout) findViewById(R.id.detail_tarefa_endereco_proprio_linearlayout);
//			TextView vText = ((TextView) findViewById(R.id.detail_tarefa_titulo_endereco_de_proprio_textview));
//			
//			vText.setVisibility(View.GONE);
//			vAppointmentPlacesLinearLayout.setVisibility(View.GONE);
//		}
//	}
//	
//	private void refreshOrigemAppointmentPlaces()
//	{
//		//Log.d(TAG,"refreshNomeTarefa()");
//		if(this.enderecoOrigem != null){
//			this.appointmentPlacesOrigem = new FactoryAppointmentPlaceAdapter(
//					this, 
//					new AppointmentPlaceItemList(
//							this, 
//							tarefaId, 
//							this.enderecoOrigem ), 
//					FactoryMapClickListener.TYPE_RASTREAR_TAREFA_ORIGEM);
//			
//			refreshAppointmentPlaceOrigem();
//				
//			((TextView) findViewById(R.id.detail_tarefa_titulo_endereco_de_origem_textview)).setTypeface(this.customTypeFace);
//		} else {
//			LinearLayout vAppointmentPlacesLinearLayout = (LinearLayout) findViewById(R.id.detail_tarefa_endereco_origem_linearlayout);
//			TextView vText = ((TextView) findViewById(R.id.detail_tarefa_titulo_endereco_de_origem_textview));
//			
//			vText.setVisibility(View.GONE);
//			vAppointmentPlacesLinearLayout.setVisibility(View.GONE);
//		}
//	}
//	
//	private void refreshDestinoAppointmentPlaces()
//	{
//		//Log.d(TAG,"refreshNomeTarefa()");
//		if(this.enderecoDestino != null){
//			this.appointmentPlacesDestino = new FactoryAppointmentPlaceAdapter(
//					this, 
//					new AppointmentPlaceItemList(
//							this, 
//							tarefaId, 
//							this.enderecoDestino), 
//							FactoryMapClickListener.TYPE_RASTREAR_TAREFA_DESTINO);
//			
//			refreshAppointmentPlaceDestino();
//			((TextView) findViewById(R.id.detail_tarefa_titulo_endereco_de_destino_textview)).setTypeface(this.customTypeFace);
//			
//			
//		} else {
//			LinearLayout vAppointmentPlacesLinearLayout = (LinearLayout) findViewById(R.id.detail_tarefa_endereco_destino_linearlayout);
//			TextView vText = ((TextView) findViewById(R.id.detail_tarefa_titulo_endereco_de_destino_textview));
//			vText.setVisibility(View.GONE);
//			vAppointmentPlacesLinearLayout.setVisibility(View.GONE);
//		}
//	}
//	
//
//
//	@Override
//	public boolean loadData() {
//
//		//Log.d(TAG,"loadData(): Retrieving veiculo data from the database");
//		//		this.veiculoUsuario = new EXTDAOTarefaAtiva(this.db);
//		//		this.veiculoUsuario.setAttrStrValue(EXTDAOTarefaAtiva.ID, this.tarefaId);	
//		//		this.veiculoUsuario.select();
//		//		this.appointmentPlaces = this.veiculo.getAppointmentPlacesAdapter(this);
//
//		return true;
//
//	}
//
//	
//	@Override
//	public void initializeComponents() {
//
//		//Log.d(TAG,"refreshAllInformation(): Start refreshing all information");
//		try{
//			db.openIfNecessary();
//			EXTDAOSistemaTabela vObjTS = new EXTDAOSistemaTabela(db);
//			idTabelaSincronizador = vObjTS.getIdSistemaTabela(EXTDAOTarefa.NAME);
//			
//			CustomListener listener = new CustomListener(this);
//			TextView vTextViewEnderecoDestino = (TextView) findViewById(R.id.detail_tarefa_titulo_endereco_de_destino_textview);
//			TextView vTituloViewEnderecoOrigemAteEnderecoDestino = (TextView) findViewById(R.id.detail_tarefa_ponto_origem_ate_ponto_destino_textview);
//			TextView vTituloViewMinhaPosicaoAteEnderecoOrigem = (TextView) findViewById(R.id.detail_tarefa_titulo_minha_localizacao_ate_ponto_origem);
//			
//			TextView vTextViewEnderecoProprio = (TextView) findViewById(R.id.detail_tarefa_titulo_endereco_de_proprio_textview);
//			
//			TextView vTextViewEnderecoOrigem = (TextView) findViewById(R.id.detail_tarefa_titulo_endereco_de_origem_textview);
//			TextView vTextViewTempoMinhaPosicaoAteEnderecoOrigem = (TextView) findViewById(R.id.detail_tarefa_titulo_tempo_estimado_minha_posicao_ponto_origem_textview);
//			TextView vTextViewDistanciaMinhaPosicaoAteEnderecoOrigem = (TextView) findViewById(R.id.titulo_detail_tarefa_distancia_minha_posicao_ponto_origem_textview);
//			
//			refreshDatetimeCadastroTarefa();
//			refreshDatetimeInicioProgramado();
//			
//			
//			refreshDateAberturaTarefa();
//			refreshTempoEstimadoMinhaPosicaoAtePontoDestino();
//			refreshNomeTarefa();
//			
//			selecionarTarefaButton = (Button) findViewById(R.id.escolher_tarefa_button);
//			
//			inicializarTarefaButton = (Button) findViewById(R.id.iniciar_tarefa_button);
//			finalizarTarefaButton = (Button) findViewById(R.id.finalizar_tarefa_button);
//			
//			refreshDatetimeInicioTarefa();
//			selecionarTarefaButton.setOnClickListener(listener);
//			finalizarTarefaButton.setOnClickListener(listener);
//			inicializarTarefaButton.setOnClickListener(listener);
//			if(typeTarefa != TYPE_TAREFA.ABERTA){
//				if(datetimeInicioTarefa != null &&  datetimeFimTarefa != null){
//					finalizarTarefaButton.setVisibility(View.GONE);
//					inicializarTarefaButton.setVisibility(View.GONE);
//				} else if(datetimeInicioTarefa != null){
//					finalizarTarefaButton.setVisibility(View.VISIBLE);
//					inicializarTarefaButton.setVisibility(View.GONE);
//				} else if(datetimeInicioTarefa == null && datetimeFimTarefa == null){
//					finalizarTarefaButton.setVisibility(View.GONE);
//					inicializarTarefaButton.setVisibility(View.VISIBLE);
//				} else{
//					finalizarTarefaButton.setVisibility(View.GONE);
//					inicializarTarefaButton.setVisibility(View.GONE);
//				}
//				selecionarTarefaButton.setVisibility(View.GONE);
//			} else{
//				selecionarTarefaButton.setVisibility(View.VISIBLE);
//				finalizarTarefaButton.setVisibility(View.GONE);
//				inicializarTarefaButton.setVisibility(View.GONE);
//					
//			}
//			
//			
//			refreshDatetimeFimTarefa();
//			
//			if(geoPointProprio != null){
//				vTextViewEnderecoProprio.setTypeface(this.customTypeFace);
//				refreshProprioAppointmentPlaces();
//			}else{
//				((LinearLayout) findViewById(R.id.linearLayout_minha_localizacao)).setVisibility(View.GONE);
//				vTextViewEnderecoProprio.setVisibility(View.GONE);
//			}
//			
//			Button vTracarRotaMeuPontoOrigemButton = (Button) findViewById(R.id.tracar_rota_meu_ponto_origem_button);
//			if(geoPointOrigem != null && geoPointProprio != null){
//				vTracarRotaMeuPontoOrigemButton.setOnClickListener(listener);
//				((TextView) findViewById(R.id.detail_tarefa_titulo_minha_localizacao_ate_ponto_origem)).setTypeface(this.customTypeFace);
//				vTituloViewMinhaPosicaoAteEnderecoOrigem.setTypeface(this.customTypeFace);
//				refreshDistanciaMinhaPosicaoPontoOrigem();
//				refreshTempoMinhaPosicaoPontoOrigem();
//				
//			} else{
//				((LinearLayout) findViewById(R.id.linearLayout_minha_localizacao_ate_origem)).setVisibility(View.GONE);
//				((LinearLayout) findViewById(R.id.linearLayout_minha_localizacao_ate_origem_2)).setVisibility(View.GONE);
//				
//				vTextViewTempoMinhaPosicaoAteEnderecoOrigem.setVisibility(View.GONE);
//				vTracarRotaMeuPontoOrigemButton.setVisibility(View.GONE);
//				vTextViewDistanciaMinhaPosicaoAteEnderecoOrigem.setVisibility(View.GONE);
//				vTituloViewMinhaPosicaoAteEnderecoOrigem.setVisibility(View.GONE);
//			}
//			
//			if(geoPointOrigem != null ){
//				vTextViewEnderecoOrigem.setTypeface(this.customTypeFace);
//				refreshOrigemAppointmentPlaces();
//			}else{
//				vTextViewEnderecoOrigem.setVisibility(View.GONE);
//			}
//			
//			Button vTracarRotaOrigemDestinoButton = (Button) findViewById(R.id.tracar_rota_origem_destino_button);
//			if(geoPointOrigem != null && geoPointDestino != null){
//				vTracarRotaOrigemDestinoButton.setOnClickListener(listener);
//				vTituloViewEnderecoOrigemAteEnderecoDestino.setTypeface(this.customTypeFace);
//				refreshDistanciaPontoOrigemPontoDestino();
//				refreshTempoPontoOrigemPontoDestino();
//			} else{
//				((LinearLayout) findViewById(R.id.linearLayout_ponto_origem_ate_ponto_destino)).setVisibility(View.GONE);
//				((LinearLayout) findViewById(R.id.linearLayout_ponto_origem_ate_ponto_destino_2)).setVisibility(View.GONE);
//				
//				vTituloViewEnderecoOrigemAteEnderecoDestino.setVisibility(View.GONE);
//				vTracarRotaOrigemDestinoButton.setVisibility(View.GONE);
//			}
//			
//			if(geoPointDestino != null){
//				refreshDestinoAppointmentPlaces();	
//			} else{
//				vTextViewEnderecoDestino.setVisibility(View.GONE);
//			}
//			
//			
//			if(documentRouteInformationPontoOrigemDestino != null &&
//					documentRouteInformationPontoProprioOrigem != null){
//				
//				refreshDistanciaMinhaPosicaoPontoDestino();
//				refreshTempoMinhaPosicaoPontoDestino();
//				
//				
//			} else{
//				
//				((LinearLayout) findViewById(R.id.linearLayout_ponto_origem_ate_ponto_destino_3)).setVisibility(View.GONE);
//				((LinearLayout) findViewById(R.id.linearLayout_minha_localizacao_ate_ponto_destino)).setVisibility(View.GONE);
//			}
//			db.close();
//		} catch(Exception ex){
//			//if(ex != null)
//			//Log.e("initializeComponents", ex.getMessage());
//		}
//
//		//Attach search button to its listener
//		//		this.closeButton = (Button) findViewById(R.id.unimedbh_dialog_fechar_button);
//		//		this.closeButton.setOnClickListener(new CloseButtonActivityClickListenerTablet(this));
//
//	}
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//
//
//		if (requestCode == OmegaConfiguration.STATE_RASTREAR_CARRO) {
//
//			if(intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO)){
//				String veiculoUsuarioId = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO);
//				Intent intent2 = getIntent();
//				intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, veiculoUsuarioId);
//				setResult(OmegaConfiguration.STATE_RASTREAR_CARRO, intent2);
//				finish();
//			} else finish();
//
//
//		}
//	}
}
