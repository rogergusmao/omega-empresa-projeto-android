package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoDocumento;

public class DetailTipoDocumentoActivityMobile extends OmegaRegularActivity{


	//Close button
	//	private Button closeButton;
	private static final String TAG = "DetailTipoDocumentoActivityMobile";

	private DatabasePontoEletronico db=null;
	private EXTDAOTipoDocumento tipo_documento;
	private String tipoDocumentoId;

	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_tipo_documento_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");

		Bundle vParameters = getIntent().getExtras();

		//Gets bairro id from the Intent used to create this activity
		//TODO: Test connecting between ListResultActivity and EmpresaDetailsActivity
		//this.empresaId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		this.tipoDocumentoId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);

		
		new CustomDataLoader(this).execute();

	}


	/**
	 * Updates the TextView which holds the bairro name
	 */
	private void refreshTipoDocumentoName()
	{
		//Log.d(TAG,"refreshTipoDocumentoName()");
		String nome = this.tipo_documento.getAttribute(EXTDAOTipoDocumento.NOME).getStrValue();
		if(nome != null){
			nome = nome.toUpperCase();
			((TextView) findViewById(R.id.tipo_documento_textview)).setText(nome);
			((TextView) findViewById(R.id.tipo_documento_textview)).setTypeface(this.customTypeFace);
		}
	}


	public void finish(){
		try{
			db.close();
			super.finish();
		} catch(Exception ex){

		}
	}
	@Override
	public boolean loadData() {

		//Log.d(TAG,"loadData(): Retrieving pais data from the database");
		
		return true;

	}
	/**
	 * Updates the LinearLayout with the bairro appointment places 
	 */
	
	@Override
	public void initializeComponents() {

		//Log.d(TAG,"refreshAllInformation(): Start refreshing all information");
		try{
			this.db = new DatabasePontoEletronico(this);
			this.tipo_documento = new EXTDAOTipoDocumento(this.db);
			this.tipo_documento.setAttrValue(EXTDAOTipoDocumento.ID, this.tipoDocumentoId);	
			this.tipo_documento.select();

			refreshTipoDocumentoName();
			
			db.close();
		} catch(Exception ex){
			//Log.e("initializeComponents", ex.getMessage());
		}

		//Attach search button to its listener
		//		this.closeButton = (Button) findViewById(R.id.unimedbh_dialog_fechar_button);
		//		this.closeButton.setOnClickListener(new CloseButtonActivityClickListenerTablet(this));

	}


}
