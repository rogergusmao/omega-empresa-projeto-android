package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoEmpresa;

public class DetailTipoEmpresaActivityMobile extends OmegaRegularActivity{


	//Close button
	//	private Button closeButton;
	private static final String TAG = "DetailTipoEmpresaActivityMobile";

	private DatabasePontoEletronico db=null;
	private EXTDAOTipoEmpresa tipo_empresa;
	private String tipoEmpresaId;

	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_pais_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");

		Bundle vParameters = getIntent().getExtras();

		//Gets bairro id from the Intent used to create this activity
		//TODO: Test connecting between ListResultActivity and EmpresaDetailsActivity
		//this.empresaId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		this.tipoEmpresaId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);

		
		new CustomDataLoader(this).execute();

	}


	/**
	 * Updates the TextView which holds the bairro name
	 */
	private void refreshTipoEmpresaName()
	{
		//Log.d(TAG,"refreshTipoEmpresaName()");
		String nome = this.tipo_empresa.getAttribute(EXTDAOTipoEmpresa.NOME).getStrValue();
		if(nome != null){
			nome = nome.toUpperCase();
			((TextView) findViewById(R.id.pais_textview)).setText(nome);
			((TextView) findViewById(R.id.pais_textview)).setTypeface(this.customTypeFace);
		}
	}


	public void finish(){
		try{
			db.close();
			super.finish();
		} catch(Exception ex){

		}
	}
	@Override
	public boolean loadData() {

		//Log.d(TAG,"loadData(): Retrieving pais data from the database");
		
		return true;

	}
	/**
	 * Updates the LinearLayout with the bairro appointment places 
	 */
	
	@Override
	public void initializeComponents() {

		//Log.d(TAG,"refreshAllInformation(): Start refreshing all information");
		try{
			this.db = new DatabasePontoEletronico(this);
			this.tipo_empresa = new EXTDAOTipoEmpresa(this.db);
			this.tipo_empresa.setAttrValue(EXTDAOTipoEmpresa.ID, this.tipoEmpresaId);	
			this.tipo_empresa.select();

			refreshTipoEmpresaName();
			
			db.close();
		} catch(Exception ex){
			//Log.e("initializeComponents", ex.getMessage());
		}

		//Attach search button to its listener
		//		this.closeButton = (Button) findViewById(R.id.unimedbh_dialog_fechar_button);
		//		this.closeButton.setOnClickListener(new CloseButtonActivityClickListenerTablet(this));

	}


}
