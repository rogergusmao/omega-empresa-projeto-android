package app.omegasoftware.pontoeletronico.TabletActivities.detail;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.DirigirVeiculoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.DetailPessoaAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.PhoneEmailAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDetailActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioCorporacao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculoUsuario;
import app.omegasoftware.pontoeletronico.primitivetype.HelperBoolean;

public class DetailUsuarioActivityMobile extends OmegaDetailActivity {

	// Close button
	// private Button closeButton;
	private static final String TAG = "DetailUsuarioActivityMobile";

	private EXTDAOUsuario objUsuario;
	private EXTDAOPessoaUsuario objPessoaUsuario;
	private EXTDAOPessoa objPessoa;
	EXTDAOVeiculoUsuario.VeiculoDirigido objVeiculoDirigido;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_CLASS, FormUsuarioActivityMobile.class);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_usuario_activity_mobile_layout);

		new CustomDataLoader(this).execute();

	}

	/**
	 * Updates the TextView which holds the usuario name
	 */
	private void refreshNome() {
		try {
			// Log.dTAG,"refreshUsuarioName()");
			String nomeUsuario = this.objUsuario.getAttribute(EXTDAOUsuario.NOME).getStrValue();
			TextView vTextView = ((TextView) findViewById(R.id.tv_title));

			if (nomeUsuario != null) {
				nomeUsuario = nomeUsuario.toUpperCase();
				vTextView.setText(nomeUsuario);

			} else {
				vTextView.setVisibility(View.GONE);

			}
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(this, ex, null);
		}
	}

	private void refreshCategoriaPermissao() {
		try {
			// Log.dTAG,"refreshCategoriaPermissao()");
			EXTDAOUsuarioCorporacao vObjUC = new EXTDAOUsuarioCorporacao(objUsuario.getDatabase());
			vObjUC.setAttrValue(EXTDAOUsuarioCorporacao.USUARIO_ID_INT, id);
			vObjUC.setAttrValue(EXTDAOUsuarioCorporacao.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			EXTDAOUsuarioCorporacao vTuplaUC = (EXTDAOUsuarioCorporacao) vObjUC.getFirstTupla();

			TextView vCategoriaPermissaoTextView = ((TextView) findViewById(R.id.categoria_permissao_textview));
			boolean vValidadeAdm = false;
			if (vTuplaUC != null) {
				String vIsAdm = vTuplaUC.getStrValueOfAttribute(EXTDAOUsuarioCorporacao.IS_ADM_BOOLEAN);
				if (HelperBoolean.valueOfStringSQL(vIsAdm)) {
					vValidadeAdm = true;
					String strIsAdm = getResources().getString(R.string.is_administrador);
					vCategoriaPermissaoTextView.setText(strIsAdm);
				}
			}

			if (!vValidadeAdm) {
				String nomeCP = this.objUsuario.getCategoriaPermissao();

				boolean vIsPreenchida = false;
				if (nomeCP != null) {
					if (nomeCP.length() > 0) {
						nomeCP = nomeCP.toUpperCase();
						vCategoriaPermissaoTextView.setText(nomeCP);
						vIsPreenchida = true;
					}
				}
				if (!vIsPreenchida) {

					vCategoriaPermissaoTextView.setVisibility(View.GONE);
				}
			}
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(this, ex, null);
		}
	}

	private void refreshStatus() {
		try {
			// Log.dTAG,"refreshStatus()");
			String nomeStatus = this.objUsuario.getAttribute(EXTDAOUsuario.STATUS_BOOLEAN).getStrValue();
			TextView statusTextView = ((TextView) findViewById(R.id.status_textview));

			boolean vIsPreenchida = false;
			if (nomeStatus != null) {
				String vTextStatus = "";
				Boolean vValidade = HelperBoolean.valueOfStringSQL(nomeStatus);
				if (vValidade == null)
					vValidade = false;
				if (vValidade) {
					vTextStatus = getResources().getString(R.string.status_ativo);
				} else {
					vTextStatus = getResources().getString(R.string.status_inativo);
				}
				vIsPreenchida = true;
				statusTextView.setText(vTextStatus);
			}

			if (!vIsPreenchida) {
				statusTextView.setVisibility(View.GONE);

			}
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(this, ex, null);
		}
	}

	/**
	 * Updates the LinearLayout with the usuario appointment places
	 */
	private void refreshEmail() {
		try {
			LinearLayout emailUsuarioLinearLayout = (LinearLayout) findViewById(R.id.email_usuario_linearlayout);

			emailUsuarioLinearLayout.removeAllViews();

			String vEmailUsuario = this.objUsuario.getStrValueOfAttribute(EXTDAOUsuario.EMAIL);

			if (vEmailUsuario != null && vEmailUsuario.length() > 0) {
				PhoneEmailAdapter emailAdapter = new PhoneEmailAdapter(this, vEmailUsuario,
						PhoneEmailAdapter.EMAIL_TYPE);
				emailUsuarioLinearLayout.addView(emailAdapter.getView(0, null, null));

			} else {
				emailUsuarioLinearLayout.setVisibility(View.GONE);
			}
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(this, ex, null);
		}
	}

	private void refreshDirigirVeiculo() {
		try {
			LinearLayout dirigirVeiculoLinearLayout = (LinearLayout) findViewById(R.id.ll_dirigir_veiculo);
			TextView tvDirigirVeiculo = (TextView) findViewById(R.id.tv_dirigir_veiculo);
			String desc = "";
			// se o usuurio estu dirigindo algum veuculo

			// se o usuurio do detalhe for o mesmo que o logado. habilita o
			// painel adm de dirigir veiculo
			if (id.compareTo(OmegaSecurity.getIdUsuario()) == 0) {
				dirigirVeiculoLinearLayout.setClickable(true);
				dirigirVeiculoLinearLayout.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent(DetailUsuarioActivityMobile.this,
								DirigirVeiculoActivityMobile.class);

						DetailUsuarioActivityMobile.this.startActivityForResult(intent,
								OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);

					}
				});

				if (objVeiculoDirigido != null) {
					desc = getResources().getString(R.string.descricao_parar_de_dirigir);
					desc += " " + objVeiculoDirigido.objVeiculo.getDescricao();
					tvDirigirVeiculo.setText(desc);
				} else {
					desc = getResources().getString(R.string.voce_nao_esta_dirigindo);

				}
			} else {

				dirigirVeiculoLinearLayout.setClickable(false);
				if (objVeiculoDirigido != null) {
					desc = getResources().getString(R.string.dirigindo_o_veiculo);
					desc += " " + objVeiculoDirigido.objVeiculo.getDescricao();
					tvDirigirVeiculo.setText(desc);
				} else {
					desc = getResources().getString(R.string.usuario_nao_esta_dirigindo);

				}
			}
			tvDirigirVeiculo.setText(desc);

			// emailUsuarioLinearLayout.removeAllViews();
			//
			// String vEmailUsuario =
			// this.objUsuario.getStrValueOfAttribute(EXTDAOUsuario.EMAIL);
			//
			// boolean vValidade = false;
			// if(vEmailUsuario != null && vEmailUsuario.length() > 0 ){
			// PhoneEmailAdapter emailAdapter = new PhoneEmailAdapter(this,
			// vEmailUsuario, PhoneEmailAdapter.EMAIL_TYPE);
			// emailUsuarioLinearLayout.addView(emailAdapter.getView(0, null,
			// null));
			//
			// }else {
			// emailUsuarioLinearLayout.setVisibility(View.GONE);
			// }
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(this, ex, null);
		}
	}

	@Override
	public boolean loadData() {

		try {
			// Log.dTAG,"loadData(): Retrieving usuario data from the
			// database");
			objUsuario = new EXTDAOUsuario(this.db);
			objUsuario.setAttrValue(EXTDAOUsuario.ID, this.id);
			objUsuario.select();
			objVeiculoDirigido = EXTDAOVeiculoUsuario.getVeiculoSendoDirigidoPeloUsuario(db, id);

			objPessoaUsuario = new EXTDAOPessoaUsuario(this.db);
			objPessoaUsuario.setAttrValue(EXTDAOPessoaUsuario.USUARIO_ID_INT, this.id);
			Table registro = objPessoaUsuario.getFirstTupla();
			if (registro != null) {
				objPessoa = (EXTDAOPessoa) registro.getObjDaChaveExtrangeira(EXTDAOPessoaUsuario.PESSOA_ID_INT);
			}
			if (objPessoa == null) {
				registro.remove(true);
				String emailUsuario = objUsuario.getStrValueOfAttribute(EXTDAOUsuario.EMAIL);
				objPessoa = new EXTDAOPessoa(db);
				objPessoa.setAttrValue(EXTDAOPessoa.EMAIL, emailUsuario);
				Table registroPessoa = objPessoa.getFirstTupla();
				if (registroPessoa == null) {
					objPessoa.clearData();
					objPessoa.setAttrValue(EXTDAOPessoa.EMAIL, objUsuario.getStrValueOfAttribute(EXTDAOUsuario.EMAIL));
					objPessoa.setAttrValue(EXTDAOPessoa.NOME, objUsuario.getStrValueOfAttribute(EXTDAOUsuario.NOME));
					objPessoa.formatToSQLite();
					objPessoa.insert(true);
					registroPessoa = objPessoa;
				} else {
					objPessoa = (EXTDAOPessoa) registroPessoa;
				}
				if (registroPessoa != null) {
					String idPessoa = registroPessoa.getId();

					objPessoaUsuario.clearData();
					objPessoaUsuario.setAttrValue(EXTDAOPessoaUsuario.PESSOA_ID_INT, idPessoa);
					objPessoaUsuario.setAttrValue(EXTDAOPessoaUsuario.USUARIO_ID_INT, id);
					objPessoaUsuario.formatToSQLite();
					objPessoaUsuario.insert(true);
				}

			}
			return true;
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(this, ex, null);
			return false;
		}
	}

	private void refreshHeader() {
		try {
			View viewHeader = findViewById(R.id.header);
			if (viewHeader != null) {
				ImageButton editarTopo = (ImageButton) viewHeader.findViewById(R.id.editar_topo_button);
				editarTopo.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// editar_topo_button
						Intent intent = new Intent(DetailUsuarioActivityMobile.this, FormUsuarioActivityMobile.class);
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, id);
						DetailUsuarioActivityMobile.this.startActivityForResult(intent,
								OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);

					}
				});

			}
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(this, ex, null);
		}
	}

	private void refreshPessoa() {
		try {
			if (objPessoa != null) {
				View v = findViewById(android.R.id.content);
				DetailPessoaAdapter adapter = new DetailPessoaAdapter(DetailUsuarioActivityMobile.this, objPessoa, v);

				adapter.refreshIdentificador();
				adapter.refreshNovaProfissao();
				adapter.refreshPessoaOperadora();
				adapter.refreshTipoDocumento();
				adapter.refreshPessoaEmpresa();
				adapter.refreshSexo();
				adapter.refreshEmailEnderecoETelefone(false);

			}

		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(this, ex, null);
		}
	}

	@Override
	protected void beforeInitializeComponents() {

		try {
			refreshNome();
			refreshCategoriaPermissao();
			refreshStatus();
			refreshEmail();
			refreshDirigirVeiculo();
			refreshHeader();
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(this, ex, null);
		}

		refreshPessoa();
	}

	@Override
	public Table getEXTDAO() {

		return new EXTDAOUsuario(db);
	}

}
