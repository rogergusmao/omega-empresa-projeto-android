package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import java.util.ArrayList;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormVeiculoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.DetailMotoristaAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.NoResultsAdapterAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.MotoristaItemList;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDetailActivity;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOModelo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculoUsuario;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class DetailVeiculoActivityMobile extends OmegaDetailActivity{


	private static final String TAG = "DetailVeiculoActivityMobile";

	private DatabasePontoEletronico db=null;
	private EXTDAOVeiculo veiculo;
	private EXTDAOModelo modelo;
	ArrayList<Table > veiculosUsuarios ;
	private ArrayList<MotoristaItemList> motoristasItemstList ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_CLASS, FormVeiculoActivityMobile.class);
		setContentView(R.layout.detail_veiculo_activity_mobile_layout);
		
		new CustomDataLoader(this).execute();

	}


	/**
	 * Updates the TextView which holds the veiculo name
	 */
	private void refreshModelo()
	{
		if(this.modelo != null){
			String desc = this.modelo.getDescricao();
			if(desc != null){
				desc = HelperString.ucFirstForEachToken(desc);
				((TextView) findViewById(R.id.veiculo_details_veiculo_modelo_nome_and_ano)).setText(desc);
				
			}	
		}
	}


	private void refreshMotoristas()
	{
		LinearLayout llMotoristas = ( (LinearLayout) findViewById(R.id.ll_motoristas));
		llMotoristas.removeAllViews();
		
		llMotoristas.setVisibility(View.GONE);
		
		View noResults = findViewById(R.id.in_noresults);
		NoResultsAdapterAdapter noResultsAdapter = new NoResultsAdapterAdapter(
				DetailVeiculoActivityMobile.this, 
				R.string.clique_aqui_cadastro_motorista, 
				FormVeiculoActivityMobile.class,
				id);
		noResultsAdapter.formatarView(noResults);
		noResults.setVisibility(View.VISIBLE);
		
		if(veiculosUsuarios != null && veiculosUsuarios.size() > 0){
			
			DetailMotoristaAdapter adapter = new DetailMotoristaAdapter(
					DetailVeiculoActivityMobile.this, motoristasItemstList);
			for(int i = 0; i < motoristasItemstList.size(); i++){
				View view = adapter.getView(i, null, llMotoristas);
				llMotoristas.addView(view);
			}
			llMotoristas.setVisibility(View.VISIBLE);
		}
	}
	
	
	private void refreshVeiculoPlaca()
	{
		
		String desc = this.veiculo.getAttribute(EXTDAOVeiculo.PLACA).getStrValue();
		if(desc != null){
			desc = desc.toUpperCase();
			setTituloCabecalho(desc);
			
			((TextView) findViewById(R.id.tv_veiculo)).setText(desc);
		}
	}


	@Override
	public boolean loadData() {
		try{
			this.db = new DatabasePontoEletronico(this);
			this.veiculo = new EXTDAOVeiculo(this.db);
				
			this.veiculo.select(id);
			Table objModelo = veiculo.getObjDaChaveExtrangeira(EXTDAOVeiculo.MODELO_ID_INT);
			if(objModelo != null){
				this.modelo = (EXTDAOModelo) objModelo;
			}
	
			EXTDAOVeiculoUsuario vObj = new EXTDAOVeiculoUsuario(db);
			vObj.setAttrValue(EXTDAOVeiculoUsuario.VEICULO_ID_INT, id);
			veiculosUsuarios = vObj.getListTable();
			if(veiculosUsuarios != null && veiculosUsuarios.size() > 0){
				motoristasItemstList = new ArrayList<MotoristaItemList>();
				EXTDAOUsuario objUsuario = new EXTDAOUsuario(db);
				
				for (Table table : veiculosUsuarios) {
					
					objUsuario.select(table.getStrValueOfAttribute(EXTDAOVeiculoUsuario.USUARIO_ID_INT));
					
					MotoristaItemList itemList = new MotoristaItemList(
							table.getId(),
							objUsuario.getStrValueOfAttribute(EXTDAOUsuario.NOME),
							objUsuario.getStrValueOfAttribute(EXTDAOUsuario.EMAIL),
							objUsuario.getId());
					
					motoristasItemstList.add(itemList);
					
				}	
			}
			
			return true;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
			return false;
		}
	}

	@Override
	public void beforeInitializeComponents() {


		try{

			refreshVeiculoPlaca();
			refreshModelo();
			refreshMotoristas();
		} catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);

		}

	}


	@Override
	public Table getEXTDAO() {

		return new EXTDAOVeiculo(db);
	}


}
