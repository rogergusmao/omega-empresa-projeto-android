package app.omegasoftware.pontoeletronico.TabletActivities.detail;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.VeiculoUsuarioAppointmentPlaceAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.AppointmentPlaceItemList;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculoUsuario;
import app.omegasoftware.pontoeletronico.mapa.HelperMapa;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class DetailVeiculoUsuarioAtivoActivityMobile extends OmegaRegularActivity{


	//Close button
//	private Button closeButton;
	private static final String TAG = "DetailVeiculoUsuarioAtivoActivityMobile";

	private DatabasePontoEletronico db=null;
	private EXTDAOVeiculoUsuario veiculoUsuario;
	private EXTDAOUsuario usuario;
	private EXTDAOVeiculo veiculo;
//	private VeiculoUsuarioAppointmentPlaceAdapter appointmentPlaces;
	private String veiculoUsuarioId;
	private String dateTime;
	private Integer latitude = null;
	private Integer longitude = null;
	private String nomeModeloVeiculo;
	private String address;
	
	
	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_veiculo_usuario_ativo_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		
		Bundle vParameters = getIntent().getExtras();

		//Gets veiculo id from the Intent used to create this activity
		//TODO: Test connecting between ListResultActivity and VeiculoDetailsActivity
		//this.veiculoId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		this.veiculoUsuarioId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		this.dateTime = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_DATE_TIME);
		this.nomeModeloVeiculo = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_MODELO);
		
		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LATITUDE) && 
				vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LONGITUDE)){
			this.latitude = vParameters.getInt(OmegaConfiguration.SEARCH_FIELD_LATITUDE);
			this.longitude = vParameters.getInt(OmegaConfiguration.SEARCH_FIELD_LONGITUDE);
		}
		//TODO resolver isso 
//		this.db = new DatabasePontoEletronico(this);
		new CustomDataLoader(this).execute();
	}

	
	public void finish(){
		try{
			db.close();
			super.finish();
		}catch(Exception ex){
			
		}
	}
	
	/**
	 * Updates the TextView which holds the veiculo name
	 */
	private void refreshNomeUsuario()
	{
		//Log.d(TAG,"refreshVeiculoName()");
		String vUsuario = this.usuario.getStrValueOfAttribute(EXTDAOUsuario.NOME);
		if(vUsuario != null){
			vUsuario = vUsuario.toUpperCase();
			((TextView) findViewById(R.id.nome_usuario)).setText( vUsuario);
			((TextView) findViewById(R.id.nome_usuario)).setTypeface(this.customTypeFace);
		}
	}
	
	/**
	 * Updates the TextView which holds the veiculo name
	 */
	private void refreshDateTime()
	{
		//Log.d(TAG,"refreshVeiculoName()");
		if(this.dateTime != null){
			((TextView) findViewById(R.id.datetime_textview)).setText( this.dateTime);
			((TextView) findViewById(R.id.datetime_textview)).setTypeface(this.customTypeFace);
			((TextView) findViewById(R.id.titulo_datetime_textview)).setTypeface(this.customTypeFace);
		}
	}
	
	private void refreshVeiculoNomeModelo()
	{
		//Log.d(TAG,"refreshVeiculoName()");
		
		if(this.nomeModeloVeiculo != null){
			this.nomeModeloVeiculo = nomeModeloVeiculo.toUpperCase();
			((TextView) findViewById(R.id.nome_veiculo_modelo)).setText(nomeModeloVeiculo );
			((TextView) findViewById(R.id.nome_veiculo_modelo)).setTypeface(this.customTypeFace);
		}
	}
	
	
	
	/**
	 * Updates the TextView which holds the veiculo name
	 */
	private void refreshVeiculoPlaca()
	{
		//Log.d(TAG,"refreshVeiculoName()");
		String nomeVeiculo = this.veiculo.getAttribute(EXTDAOVeiculo.PLACA).getStrValue();
		if(nomeVeiculo != null){
			nomeVeiculo = nomeVeiculo.toUpperCase();
			((TextView) findViewById(R.id.veiculo_placa)).setText(nomeVeiculo);
			((TextView) findViewById(R.id.veiculo_placa)).setTypeface(this.customTypeFace);
		}
	}

	@Override
	public boolean loadData() {
try{
		//Log.d(TAG,"loadData(): Retrieving veiculo data from the database");
		this.veiculoUsuario = new EXTDAOVeiculoUsuario(this.db);
		this.veiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.ID, this.veiculoUsuarioId);	
		this.veiculoUsuario.select();
//		this.appointmentPlaces = this.veiculo.getAppointmentPlacesAdapter(this);

		return true;
}catch(Exception ex){
	SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
return false;
}
	}
	

	@Override
	public void initializeComponents() {

		//Log.d(TAG,"refreshAllInformation(): Start refreshing all information");
		try{
    //      iFiles - 
			this.veiculoUsuario = new EXTDAOVeiculoUsuario(this.db);
			this.veiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.ID, this.veiculoUsuarioId);	
			this.veiculoUsuario.select();
			
			String vIdUsuario = this.veiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.USUARIO_ID_INT);
			String vIdVeiculo = this.veiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.VEICULO_ID_INT);
			
			this.usuario = new EXTDAOUsuario(this.db);
			this.usuario.setAttrValue(EXTDAOUsuario.ID, vIdUsuario);	
			this.usuario.select();
			
			this.veiculo = new EXTDAOVeiculo(this.db);
			this.veiculo.setAttrValue(EXTDAOVeiculo.ID, vIdVeiculo);	
			this.veiculo.select();
			
			if(latitude != null && longitude != null)
				this.address = HelperMapa.getAddressFromPoint(this, latitude, longitude);
			
			if(this.address != null){
				//this.appointmentPlaces = 
			new VeiculoUsuarioAppointmentPlaceAdapter(
					this, 
					new AppointmentPlaceItemList(
							this, 
							HelperInteger.parserInteger(this.veiculoUsuarioId), 
							this.address ));

			}
			refreshVeiculoPlaca();
			refreshVeiculoNomeModelo();
			refreshNomeUsuario();
			refreshDateTime();
			

		} catch(Exception ex){
			//Log.e("initializeComponents", ex.getMessage());
		}
		
		//Attach search button to its listener
//		this.closeButton = (Button) findViewById(R.id.unimedbh_dialog_fechar_button);
//		this.closeButton.setOnClickListener(new CloseButtonActivityClickListenerTablet(this));

	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {


		if (requestCode == OmegaConfiguration.STATE_RASTREAR_CARRO) {
		
			if(intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO)){
				String veiculoUsuarioId = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO);
				Intent intent2 = getIntent();
				intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, veiculoUsuarioId);
				setResult(OmegaConfiguration.STATE_RASTREAR_CARRO, intent2);
				finish();
			} else finish();
				
			
		}
	}
}
