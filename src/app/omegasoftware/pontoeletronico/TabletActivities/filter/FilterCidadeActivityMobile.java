package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import java.util.LinkedHashMap;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListCidadeActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.TextView.TableDependentAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPais;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;

public class FilterCidadeActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "FilterCidadeActivityMobile";
	private static final String TABELAS_RELACIONADAS[] = new String[]{EXTDAOPais.NAME, EXTDAOUf.NAME, EXTDAOCidade.NAME};
	
	CustomSpinnerAdapter customSpinnerAdapterPais;
	//Search button
		private Button buscarButton;
		
		Drawable originalDrawable;

	//Spinners
	private DatabasePontoEletronico db=null;
	
	private Spinner paisSpinner;
	private Spinner estadoSpinner;
	CustomSpinnerAdapter customSpinnerAdapterEstado;
	
	private AutoCompleteTextView cidadeAutoCompletEditText;
	TableDependentAutoCompleteTextView cidadeContainerAutoComplete = null;
	
	private Typeface customTypeFace;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_cidade_activity_mobile_layout);
		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();

	}

	
	@Override
	public boolean loadData() {
		return true;
	}


	public void formatarStyle()
	{
		//Search mode buttons
		((TextView) findViewById(R.id.pais_textview)).setTypeface(this.customTypeFace);
		((TextView) findViewById(R.id.estado_textview)).setTypeface(this.customTypeFace);
		((TextView) findViewById(R.id.cidade_textview)).setTypeface(this.customTypeFace);
		
		//Botuo cadastrar:
		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);
 
	}

	private LinkedHashMap<String, String> getAllPais()
	{
		EXTDAOPais vObj = new EXTDAOPais(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}
	
	@Override
	public void initializeComponents() {

		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);		

		this.paisSpinner = (Spinner) findViewById(R.id.pais_spinner);
		customSpinnerAdapterPais = new CustomSpinnerAdapter(
				getApplicationContext(), 
				this.getAllPais(),
				R.layout.spinner_item,
				R.string.form_all_string
		); 
		this.paisSpinner.setAdapter(customSpinnerAdapterPais);
		((CustomSpinnerAdapter) this.paisSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
		this.paisSpinner.setOnItemSelectedListener(new PaisSpinnerListener());

		this.estadoSpinner = (Spinner) findViewById(R.id.estado_spinner);
		customSpinnerAdapterEstado =new CustomSpinnerAdapter(
				getApplicationContext(), 
				new LinkedHashMap<String, String>(), 
				R.layout.spinner_item,R.string.form_all_string
		); 
		this.estadoSpinner.setAdapter(customSpinnerAdapterEstado);
		((CustomSpinnerAdapter) this.estadoSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
		this.estadoSpinner.setOnItemSelectedListener(new EstadoSpinnerListener());
				
		EXTDAOCidade vObjCidade = new EXTDAOCidade(this.db);
		
		this.cidadeContainerAutoComplete = new TableDependentAutoCompleteTextView(vObjCidade, EXTDAOCidade.UF_ID_INT);
		this.cidadeAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.cidade_autocompletetextview);
		new MaskUpperCaseTextWatcher(cidadeAutoCompletEditText, 100);
		

		//Attach search button to its listener
		this.buscarButton = (Button) findViewById(R.id.buscar_button);
		this.buscarButton.setOnClickListener(new CadastroButtonListener(this));

		this.formatarStyle();
		
		this.db.close();
	}
	

	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onSearchButtonClicked(Activity pActivity)
	{
		new SearchButtonLoader(this).execute();
	}


	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean>
	{
		Activity activity;
		Intent intent ;
		public SearchButtonLoader(Activity pActivity){
			activity = pActivity;
			intent = new Intent(activity, ListCidadeActivityMobile.class);
			
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				String selectedEstado = "";
				selectedEstado = String.valueOf(estadoSpinner.getSelectedItemId());
				if(! selectedEstado.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{	
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO, selectedEstado);
				}
				
				String selectedPais = String.valueOf(paisSpinner.getSelectedItemId());
				if(! selectedPais.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PAIS, selectedPais);
				}

				String nomeCidade = cidadeAutoCompletEditText.getText().toString();
				if( nomeCidade.length() > 0)
				{	
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE, nomeCidade);
				}
				startActivity(intent);
				return true;
			}
			catch(Exception e)
			{
				//if(e != null) Log.e(TAG, e.getMessage());
				//else Log.e(TAG, "Error desconhecido");
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			controlerProgressDialog.dismissProgressDialog();	
		}
	}

	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------

	private class PaisSpinnerListener implements OnItemSelectedListener
	{

		public void onItemSelected(AdapterView<?> parent, View view, int position,long id) {

			
			showEstadoDoPais(String.valueOf(id));
		}
		public void onNothingSelected(AdapterView<?> parent) {
			//Do nothing
		}
	}


	private class CadastroButtonListener implements OnClickListener
	{
		Activity activity;
		public CadastroButtonListener(Activity pActivity){
			activity = pActivity;
		}

		public void onClick(View v) {
			onSearchButtonClicked(activity);

		}
	}
	
	private void showCidadesDoEstado(String pEstadoId)
	{
		ArrayAdapter<String> estadoAdapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item,
				cidadeContainerAutoComplete.getVetorStrId(pEstadoId));
		cidadeAutoCompletEditText.setAdapter(estadoAdapter);
	}
	private class EstadoSpinnerListener implements OnItemSelectedListener
	{

		public void onItemSelected(AdapterView<?> parent, View view, int position,long id) {
			
			showCidadesDoEstado(String.valueOf(id));
		}

		public void onNothingSelected(AdapterView<?> parent) {
			//Do nothing
		}
	}
	

	
	private void showEstadoDoPais(String p_paisId)
	{
//		if(!this.db.isOpen())
//		{
//			this.db = new DatabasePontoEletronico(this);
//		}

		
		EXTDAOUf vEXTDAOUf = new EXTDAOUf(this.db);
		customSpinnerAdapterEstado = new CustomSpinnerAdapter(getApplicationContext(), 
				vEXTDAOUf.getUfFromPais(p_paisId), 
				R.layout.spinner_item,
				R.string.form_all_string);
		this.estadoSpinner.setAdapter(customSpinnerAdapterEstado);
		((CustomSpinnerAdapter) this.estadoSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		this.db.close();

	}
}


