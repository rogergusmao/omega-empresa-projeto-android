package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListCorporacaoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCorporacao;

public class FilterCorporacaoActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "FilterCorporacaoActivityMobile";
	
	

	//Search button
	private Button buscarButton;
	
	Drawable originalDrawable;

	//Spinners
	private DatabasePontoEletronico db=null;
	private CheckBox minhaCorporacaoCheckBox;
	private AutoCompleteTextView corporacaoAutoCompletEditText;
	TableAutoCompleteTextView corporacaoContainerAutoComplete = null;
	
	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_corporacao_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		//
		new CustomDataLoader(this).execute();

	}

	
	@Override
	public boolean loadData() {
		return true;
	}


	public void formatarStyle()
	{
		//Search mode buttons
		((TextView) findViewById(R.id.corporacao_autocompletetextview)).setTypeface(this.customTypeFace);

		//Botuo cadastrar:
		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);
		

	}

	@Override
	public void initializeComponents() {
//		TODO resolver
//		this.db = new DatabasePontoEletronico(this);

		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);


		EXTDAOCorporacao vObjCorporacao = new EXTDAOCorporacao(this.db);
		this.corporacaoAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.corporacao_autocompletetextview);
		this.corporacaoContainerAutoComplete = new TableAutoCompleteTextView(this, vObjCorporacao, corporacaoAutoCompletEditText);

		new MaskUpperCaseTextWatcher(corporacaoAutoCompletEditText, 100);
		
		((TextView) findViewById(R.id.mensagem_minha_corporacao_textview)).setTypeface(this.customTypeFace);
		minhaCorporacaoCheckBox = (CheckBox) findViewById(R.id.minha_corporacao_checkbox);
		
		//Attach search button to its listener
		this.buscarButton = (Button) findViewById(R.id.buscar_button);
		this.buscarButton.setOnClickListener(new CadastroButtonListener(this));


		this.formatarStyle();
		
		this.db.close();
	}

	private void onSearchButtonClicked(Activity pActivity)
	{
		new SearchButtonLoader(this).execute();
	}
	
	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean>
	{
		Activity activity;
		Intent intent ;
		public SearchButtonLoader(Activity pActivity){
			activity = pActivity;
			intent = new Intent(activity, ListCorporacaoActivityMobile.class);
			
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				//First we check the obligatory fields:
				//Plano, Especialidade, Cidade
				//Checking Cidade	
				String nameCorporacao = "";
				nameCorporacao = corporacaoAutoCompletEditText.getText().toString();
				if( nameCorporacao.length() > 0)
				{
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CORPORACAO, nameCorporacao);
				}
				
				if(minhaCorporacaoCheckBox.isChecked()){
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MINHA_CORPORACAO, true);
				}
				startActivity(intent);
				return true;
			}
			catch(Exception e)
			{
				//if(e != null) Log.e(TAG, e.getMessage());
				//else Log.e(TAG, "Error desconhecido");
			}
			
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			controlerProgressDialog.dismissProgressDialog();	
		}
	}
	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------



	private class CadastroButtonListener implements OnClickListener
	{
		Activity activity;
		public CadastroButtonListener(Activity pActivity){
			activity = pActivity;
		}

		public void onClick(View v) {
			onSearchButtonClicked(activity);

		}

	}	
	
//
//	private class DeleteViewEnderecoButtonListener implements OnClickListener
//	{
//		View view= null;
//		
//		public DeleteViewEnderecoButtonListener(View pView){
//			view = pView;
//		}
//		
//		public void onClick(View v) {
//			LinearLayout linearLayoutEmpresaFuncionario = (LinearLayout) findViewById(R.id.linearlayout_endereco);
//			linearLayoutEmpresaFuncionario.removeView(view);
//		}
//	}
}


