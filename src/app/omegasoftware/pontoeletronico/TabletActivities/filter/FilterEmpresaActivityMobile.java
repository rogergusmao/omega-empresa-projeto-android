package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import java.util.LinkedHashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutEndereco;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.FilterEnderecoAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.AppointmentPlaceItemList;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskLowerCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSexo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoEmpresa;

public class FilterEmpresaActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "FilterEmpresaActivityMobile";
	
	
	
	//Search button
	private Button searchButton;
	ContainerLayoutEndereco containerEndereco = null;
	//Spinners

	private Spinner tipoEmpresaSpinner;
	private DatabasePontoEletronico db=null;

//	CheckBox parceiroCheckBox;
	CheckBox clienteCheckBox;
	CheckBox fornecedorCheckBox;
	CheckBox empresaDaCorporacaoCheckBox;
	
	private Button enderecoButton;

	//EditText
	private EditText nomeEmpresaEditText;
	private EditText emailEditText;

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_empresa_activity_mobile_layout);

		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {

		return true;

	}

	@Override
	public void initializeComponents() throws OmegaDatabaseException {

		this.db = new DatabasePontoEletronico(this);

		this.enderecoButton = (Button) findViewById(R.id.endereco_button);
		this.enderecoButton.setOnClickListener(new CadastrarEnderecoListener());

		this.tipoEmpresaSpinner = (Spinner) findViewById(R.id.tipo_empresa_spinner);
		this.tipoEmpresaSpinner.setAdapter(new CustomSpinnerAdapter(getApplicationContext(), this.getAllTipoEmpresa(),R.layout.spinner_item,R.string.selecione));
		((CustomSpinnerAdapter) this.tipoEmpresaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);		

//		parceiroCheckBox = (CheckBox) findViewById(R.id.form_empresa_parceiro_checkbox);
		clienteCheckBox = (CheckBox) findViewById(R.id.form_empresa_cliente_checkbox);
		fornecedorCheckBox = (CheckBox) findViewById(R.id.form_empresa_fornecedor_checkbox);
		empresaDaCorporacaoCheckBox = (CheckBox) findViewById(R.id.form_empresa_empresa_corporacao_checkbox);
		
		this.nomeEmpresaEditText = (EditText) findViewById(R.id.nomedaempresa_edittext);
		new MaskUpperCaseTextWatcher(this.nomeEmpresaEditText, 255);
		
		emailEditText = (EditText) findViewById(R.id.email_edittext);
		new MaskLowerCaseTextWatcher(this.emailEditText, 255);
		
		//Attach search button to its listener
		this.searchButton = (Button) findViewById(R.id.buscar_button);
		this.searchButton.setOnClickListener(new SearchButtonListener());
				
		this.db.close();
	}

	private LinkedHashMap<String, String> getAllTipoEmpresa()
	{
		EXTDAOTipoEmpresa vObj = new EXTDAOTipoEmpresa(this.db);
		LinkedHashMap<String, String> hash = vObj.getHashMapIdByDefinition(true);
		return hash;
	}

	protected LinkedHashMap<String, String> getAllSexo()
	{
		EXTDAOSexo vObj = new EXTDAOSexo(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}


	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onSearchButtonClicked()
	{
		new SearchButtonLoader(this).execute();
	}


	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean>
	{
		
		
		public SearchButtonLoader(Activity pActivity){
		
		
			
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				Bundle vParams = new Bundle();

				//First we check the obligatory fields:
				//Plano, Especialidade, Cidade
				if(!nomeEmpresaEditText.getText().toString().equals(""))
				{
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_NOME_EMPRESA, nomeEmpresaEditText.getText().toString());	
				}
				
				if(!emailEditText.getText().toString().equals(""))
				{
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_EMAIL, emailEditText.getText().toString());	
				}
				
				//Checking TipoEmpresa
				String selectedTipoEmpresa = String.valueOf(tipoEmpresaSpinner.getSelectedItemId());
				if( ! selectedTipoEmpresa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{	
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_TIPO_EMPRESA, selectedTipoEmpresa);
				}

				if(containerEndereco != null){
					AppointmentPlaceItemList appointmentPlaceItemList = containerEndereco.getAppointmentPlaceItemList();
					if(appointmentPlaceItemList != null){
						vParams.putString(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO, appointmentPlaceItemList.getAddress());
						vParams.putString(OmegaConfiguration.SEARCH_FIELD_NUMERO, appointmentPlaceItemList.getNumber());
						vParams.putString(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO, appointmentPlaceItemList.getComplemento());
						vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE, appointmentPlaceItemList.getIdCity());
						vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO, appointmentPlaceItemList.getIdNeighborhood());
						vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO, appointmentPlaceItemList.getIdUf());
					}
				}

				if(empresaDaCorporacaoCheckBox.isChecked())
					vParams.putBoolean(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO, true);
//				if(parceiroCheckBox.isChecked())
//					vParams.putBoolean(OmegaConfiguration.SEARCH_FIELD_IS_PARCEIRO, true);
				if(fornecedorCheckBox.isChecked())
					vParams.putBoolean(OmegaConfiguration.SEARCH_FIELD_IS_FORNECEDOR, true);
				if(clienteCheckBox.isChecked())
					vParams.putBoolean(OmegaConfiguration.SEARCH_FIELD_IS_CLIENTE, true);
				
				

				Intent intent2 = getIntent();
				intent2.putExtras(vParams);
				
				setResult(RESULT_OK, intent2);
				
				return true;
			}
			catch(Exception e)
			{
				SingletonLog.insereErro(e, TIPO.FILTRO);
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
			

			
			finish();
			} catch (Exception ex) {
				SingletonLog.openDialogError(FilterEmpresaActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}
	
	@Override
	public void finish(){
		
		db.close();
		super.finish();
	}
	

	public ContainerLayoutEndereco addLayoutEndereco(
			String pIdBairro ,
			String pIdCidade ,
			String pIdEstado,
			String pIdPais,
			String pNomeBairro ,
			String pNomeCidade ,
			String pNomeEstado,
			String pNomePais,
			String pLogradouro ,
			String pNumero ,
			String pComplemento, 
			LinearLayout pLinearLayoutEndereco, 
			Button pAddButtonEndereco){

		

		AppointmentPlaceItemList appointmentPlaceItemList = new AppointmentPlaceItemList(
				getApplicationContext(), 
				0,
				pLogradouro ,
				pNumero ,
				pComplemento,
				pNomeBairro, 
				pNomeCidade,
				pNomeEstado,
				pNomePais,
				pIdBairro ,
				pIdCidade ,
				pIdEstado,
				pIdPais,
				null,
				null,
				null);
		//		LinearLayout linearLayoutEndereco = (LinearLayout) findViewById(R.id.linearlayout_endereco);

		ContainerLayoutEndereco vContainerEndereco = new ContainerLayoutEndereco(
				this, 
				appointmentPlaceItemList,
				pLinearLayoutEndereco);

		FilterEnderecoAdapter adapter = new FilterEnderecoAdapter(
				this, 
				vContainerEndereco, 
				pAddButtonEndereco);

		View vNewView = adapter.getView(0, null, null);

		pLinearLayoutEndereco.addView(vNewView);

		vContainerEndereco.setAppointmentPlace(vNewView);

		return vContainerEndereco;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		switch (requestCode) {
		case OmegaConfiguration.ACTIVITY_FORM_ENDERECO:
			switch (resultCode) {
			case OmegaConfiguration.ACTIVITY_FORM_DELETE_LAYOUT_ENDERECO:
				int vIdLinearLayout = intent.getIntExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, -1);
				if(vIdLinearLayout == R.id.linearlayout_endereco){
					containerEndereco.delete();
					if(enderecoButton != null)
						enderecoButton.setVisibility(View.VISIBLE);
				}
				break;
			case OmegaConfiguration.ACTIVITY_FORM_ADD_LAYOUT_ENDERECO:
				try{
					String vIdCidade = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE);
					String vIdBairro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO);
					String vIdEstado = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO);
					String vIdPais = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_PAIS);
					String vNomeCidade = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE);
					String vNomeBairro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_BAIRRO);
					String vNomeEstado = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO);
					String vNomePais = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS);
					String vLogradouro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO);
					String vNumero = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO);
					String vComplemento = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO);
					LinearLayout vLinearLayoutEndereco = (LinearLayout) findViewById(R.id.linearlayout_endereco);
					if(containerEndereco != null)
						containerEndereco.delete();
					containerEndereco = addLayoutEndereco(
							vIdBairro, 
							vIdCidade, 
							vIdEstado, 
							vIdPais,
							vNomeBairro, 
							vNomeCidade, 
							vNomeEstado, 
							vNomePais,
							vLogradouro, 
							vNumero, 
							vComplemento,
							vLinearLayoutEndereco, 
							enderecoButton);
				}catch(Exception ex){
					//Log.e( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
				}
				break;
			default:
				break;
			}
			break;
		
		default:
			super.onActivityResult(requestCode, resultCode, intent);
			break;
		}
	}

	//---------------------------------------------------------------
	//----------------------- Event listeners -----------------------


	private class CadastrarEnderecoListener implements OnClickListener
	{

		Intent intent = null;

		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.endereco_button:
				intent = new Intent(getApplicationContext(), FilterEnderecoActivityMobile.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.linearlayout_endereco);
				break;

			default:
				break;
			}

			if(intent != null)
			{				
				try{

					startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_ENDERECO);
				}catch(Exception ex){
					Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
				}
			}
		}
	}
	private class SearchButtonListener implements OnClickListener
	{

		public void onClick(View v) {
			onSearchButtonClicked();
		}

	}

}

