package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import java.util.LinkedHashMap;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOBairro;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPais;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;

public class FilterEnderecoActivityMobile extends OmegaRegularActivity {

	//Constants
	private static final String TAG = "FilterEnderecoActivityMobile";


	private final int FORM_ERROR_DIALOG_CADASTRO_OK = 3;
	private final int FORM_ERROR_DIALOG_CAMPO_INVALIDO = 4;

	//Search button
	private Button cadastrarButton;
	private Button cancelarButton;
	Drawable originalDrawable;

	//Spinners
	private DatabasePontoEletronico db=null;
	private boolean isEdit = false;
	private Integer idLinearLayoutEnderecoRetorno = null;
	private EditText logradouroEditText;
	private EditText numeroEditText;
	private EditText complementoEditText;
	private Spinner estadoSpinner;
	private Spinner paisSpinner;
	CustomSpinnerAdapter customSpinnerAdapterEstado;
	CustomSpinnerAdapter customSpinnerAdapterPais;

	String idPais;
	String idEstado;
	String idCidade;
	String idBairro;

	private Spinner cidadeSpinner;
	CustomSpinnerAdapter customSpinnerAdapterCidade;
	private Spinner bairroSpinner;
	CustomSpinnerAdapter customSpinnerAdapterBairro;

	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_endereco_activity_mobile_layout);

		Bundle vParameter = getIntent().getExtras();
		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT))
			idLinearLayoutEnderecoRetorno = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT);
		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_EDIT)){
			isEdit = vParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_EDIT);
		}


		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		//
		new CustomDataLoader(this).execute();

	}

	private void loadEdit(){
		try{
		Bundle vParameter = getIntent().getExtras();
		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO)){
			logradouroEditText.setText(vParameter.getString(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO));	
		}

		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_NUMERO)){
			numeroEditText.setText(vParameter.getString(OmegaConfiguration.SEARCH_FIELD_NUMERO));	
		}

		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO)){
			complementoEditText.setText(vParameter.getString(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO));	
		}

		

		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE)){
			idCidade = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE);
			if(idCidade != null){
				EXTDAOCidade vObjCidade = new EXTDAOCidade(this.db);
				vObjCidade.setAttrValue(EXTDAOCidade.ID, idCidade);
				if(vObjCidade.select()){
					String vUfId= vObjCidade.getStrValueOfAttribute(EXTDAOCidade.UF_ID_INT);
					EXTDAOUf vObjUF = new EXTDAOUf(db);
					vObjUF.setAttrValue(EXTDAOUf.ID, vUfId);
					if(vObjUF.select()){
						String vIdUf = vObjUF.getStrValueOfAttribute(EXTDAOUf.ID);
						if(vIdUf != null){

							String vIdPais = vObjUF.getStrValueOfAttribute(EXTDAOUf.PAIS_ID_INT);
							if(vIdPais!= null){
								int vPositionPais = customSpinnerAdapterPais.getPositionFromId(vIdPais);
								paisSpinner.setSelection(vPositionPais);
							}
							int vPositionUf = customSpinnerAdapterEstado.getPositionFromId(vIdUf);
							estadoSpinner.setSelection(vPositionUf);
						}
					}
					int vPositionCidade = customSpinnerAdapterCidade.getPositionFromId(idCidade);
					cidadeSpinner.setSelection(vPositionCidade);
				}
			}
		}
		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO)){
			
			idBairro = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO);
			if(idBairro != null){
				int vPositionBairro = customSpinnerAdapterBairro.getPositionFromId(idBairro);
				bairroSpinner.setSelection(vPositionBairro);
			}
		}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
	}

	@Override
	public boolean loadData() {
		return true;
	}


	public void formatarStyle()
	{
		//Search mode buttons

		((TextView) findViewById(R.id.logradouro_textview)).setTypeface(this.customTypeFace);
		((TextView) findViewById(R.id.numero_textview)).setTypeface(this.customTypeFace);
		((TextView) findViewById(R.id.complemento_textview)).setTypeface(this.customTypeFace);


		((TextView) findViewById(R.id.estado_textview)).setTypeface(this.customTypeFace);

		((TextView) findViewById(R.id.cidade_textview)).setTypeface(this.customTypeFace);

		//Bairro de preferuncia:
		((TextView) findViewById(R.id.bairro_textview)).setTypeface(this.customTypeFace);


		//Botuo cadastrar:
		((Button) findViewById(R.id.cadastrar_dependente_button)).setTypeface(this.customTypeFace);
		((Button) findViewById(R.id.cancelar_button)).setTypeface(this.customTypeFace);

	}

	@Override
	public void initializeComponents() {


		try {
			this.db = new DatabasePontoEletronico(this);

			((Button) findViewById(R.id.cadastrar_dependente_button)).setTypeface(this.customTypeFace);		
	
			this.logradouroEditText = (EditText) findViewById(R.id.logradouro_edittext);
			new MaskUpperCaseTextWatcher(this.logradouroEditText, 255);
			originalDrawable = this.logradouroEditText.getBackground();
	
			this.numeroEditText = (EditText) findViewById(R.id.numero_edittext);
			new MaskUpperCaseTextWatcher(this.numeroEditText, 30);
	
			this.complementoEditText = (EditText) findViewById(R.id.complemento_edittext);
			new MaskUpperCaseTextWatcher(this.complementoEditText, 255);
	
			this.paisSpinner = (Spinner) findViewById(R.id.pais_spinner);
			customSpinnerAdapterPais = new CustomSpinnerAdapter(
					getApplicationContext(), 
					this.getAllPais(),
					R.layout.spinner_item,
					R.string.selecione
					); 
			this.paisSpinner.setAdapter(customSpinnerAdapterPais);
			((CustomSpinnerAdapter) this.paisSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
			this.paisSpinner.setOnItemSelectedListener(new PaisSpinnerListener());
	
			this.estadoSpinner = (Spinner) findViewById(R.id.estado_spinner);
			customSpinnerAdapterEstado = new CustomSpinnerAdapter(
					getApplicationContext(), 
					this.getAllEstados(),
					R.layout.spinner_item,
					R.string.selecione
					); 
			this.estadoSpinner.setAdapter(customSpinnerAdapterEstado);
			((CustomSpinnerAdapter) this.estadoSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
			this.estadoSpinner.setOnItemSelectedListener(new EstadoSpinnerListener());
	
	
	
			this.cidadeSpinner = (Spinner) findViewById(R.id.cidade_spinner);
			customSpinnerAdapterCidade =new CustomSpinnerAdapter(
					getApplicationContext(), 
					new LinkedHashMap<String, String>(), 
					R.layout.spinner_item,R.string.form_all_string
					); 
			this.cidadeSpinner.setAdapter(customSpinnerAdapterCidade);
			((CustomSpinnerAdapter) this.cidadeSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
			this.cidadeSpinner.setOnItemSelectedListener(new CidadeSpinnerListener());
	
			this.bairroSpinner = (Spinner) findViewById(R.id.bairro_spinner);
			customSpinnerAdapterBairro = new CustomSpinnerAdapter(getApplicationContext(), new LinkedHashMap<String, String>(), R.layout.spinner_item,R.string.form_all_string);
			this.bairroSpinner.setAdapter(customSpinnerAdapterBairro);
			((CustomSpinnerAdapter) this.bairroSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
	
			String vSavedCidadeId = String.valueOf(this.getSavedCidade());
			this.setCidade(vSavedCidadeId);
	
			//Attach search button to its listener
			this.cadastrarButton = (Button) findViewById(R.id.cadastrar_dependente_button);
			this.cadastrarButton.setOnClickListener(new CadastroButtonListener(this));
	
			//Attach search button to its listener
			this.cancelarButton = (Button) findViewById(R.id.cancelar_button);
			this.cancelarButton.setOnClickListener(new CancelarButtonListener(this));
	
			if(isEdit)
				loadEdit();
	
			this.formatarStyle();
	
			this.db.close();
		} catch (OmegaDatabaseException e) {
			SingletonLog.factoryToast(this, e, SingletonLog.TIPO.PAGINA);
		}


	}

	private LinkedHashMap<String, String> getAllEstados()
	{
		EXTDAOUf vObj = new EXTDAOUf(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}

	private LinkedHashMap<String, String> getAllPais()
	{
		EXTDAOPais vObj = new EXTDAOPais(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}


	private void showNeighbourhoodFromCities(String p_cidadeId)
	{
//		if(!this.db.isOpen())
//		{
//			this.db = new DatabasePontoEletronico(this);
//		}

		String city = String.valueOf(p_cidadeId);
		EXTDAOBairro neighbourhoods = new EXTDAOBairro(this.db);
		customSpinnerAdapterBairro = new CustomSpinnerAdapter(getApplicationContext(), 
				neighbourhoods.getBairrosFromCidade(city), R.layout.spinner_item,R.string.form_all_string);
		this.bairroSpinner.setAdapter(customSpinnerAdapterBairro);
		((CustomSpinnerAdapter) this.bairroSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		this.db.close();

	}

	private void showCitiesFromState(String p_ufId)
	{
//		if(!this.db.isOpen())
//		{
//			this.db = new DatabasePontoEletronico(this);
//		}

		String city = String.valueOf(p_ufId);
		EXTDAOCidade vEXTDAOCity = new EXTDAOCidade(this.db);
		customSpinnerAdapterCidade = new CustomSpinnerAdapter(getApplicationContext(), 
				vEXTDAOCity.getCidadeFromEstado(city), 
				R.layout.spinner_item,
				R.string.form_all_string);
		this.cidadeSpinner.setAdapter(customSpinnerAdapterCidade);
		((CustomSpinnerAdapter) this.cidadeSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		this.db.close();

	}

	private void showStateFromCountry(String p_ufId)
	{
//		if(!this.db.isOpen())
//		{
//			this.db = new DatabasePontoEletronico(this);
//		}

		String state = String.valueOf(p_ufId);
		EXTDAOUf vEXTDAOUf = new EXTDAOUf(this.db);
		customSpinnerAdapterEstado = new CustomSpinnerAdapter(getApplicationContext(), 
				vEXTDAOUf.getUfFromPais(state), 
				R.layout.spinner_item,
				R.string.form_all_string);
		this.cidadeSpinner.setAdapter(customSpinnerAdapterEstado);
		((CustomSpinnerAdapter) this.cidadeSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		this.db.close();

	}

	private void setCidade(String p_id)
	{
		//Get the position from this id
		//And selects it in the spinner
		String vCity = String.valueOf(p_id);
		CustomSpinnerAdapter spinnerAdapter = (CustomSpinnerAdapter) this.cidadeSpinner.getAdapter();
		this.cidadeSpinner.setSelection(spinnerAdapter.getPositionFromId(vCity));
		this.showNeighbourhoodFromCities(p_id);
	}

	protected LinkedHashMap<String, String> getAllProfissao()
	{
		EXTDAOProfissao vObj = new EXTDAOProfissao(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}

	private class PaisSpinnerListener implements OnItemSelectedListener
	{

		public void onItemSelected(AdapterView<?> parent, View view, int position,long id) {

			saveSelectedPais(id);
			showStateFromCountry(String.valueOf(id));
			if(idEstado != null){
				int vPositionEstado = customSpinnerAdapterEstado.getPositionFromId(idEstado);
				estadoSpinner.setSelection(vPositionEstado);
			}
		}
		public void onNothingSelected(AdapterView<?> parent) {
			//Do nothing
		}
	}
	//	private class EstadoSpinnerListener implements OnItemSelectedListener
	//	{
	//
	//		public void onItemSelected(AdapterView<?> parent, View view, int position,long id) {
	//
	//			saveSelectedEstado(id);
	//			showCitiesFromState(String.valueOf(id));
	//			if(idCidade != null){
	//				int vPositionCidade = customSpinnerAdapterCidade.getPositionFromId(idCidade);
	//				cidadeSpinner.setSelection(vPositionCidade);
	//			}
	//		}
	//		public void onNothingSelected(AdapterView<?> parent) {
	//			//Do nothing
	//		}
	//	}

	private class EstadoSpinnerListener implements OnItemSelectedListener
	{

		public void onItemSelected(AdapterView<?> parent, View view, int position,long id) {

			saveSelectedEstado(id);
			showCitiesFromState(String.valueOf(id));
			if(idCidade != null){
				int vPosition = customSpinnerAdapterCidade.getPositionFromId(idCidade);
				cidadeSpinner.setSelection(vPosition);
			}
		}

		public void onNothingSelected(AdapterView<?> parent) {
			//Do nothing
		}
	}

	private class CidadeSpinnerListener implements OnItemSelectedListener
	{

		public void onItemSelected(AdapterView<?> parent, View view, int position,long id) {

			saveSelectedCidade(id);
			showNeighbourhoodFromCities(String.valueOf(id));
			if(idBairro != null){
				int vPosition = customSpinnerAdapterBairro.getPositionFromId(idBairro);
				bairroSpinner.setSelection(vPosition);
			}
		}

		public void onNothingSelected(AdapterView<?> parent) {
			//Do nothing
		}
	}

	//Save Cidade ID in the preferences
	private void saveSelectedEstado(long p_id)
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putInt(OmegaConfiguration.SAVED_ESTADO, (int)p_id);
		vEditor.commit();
	}
	private void saveSelectedPais(long p_id)
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putInt(OmegaConfiguration.SAVED_PAIS, (int)p_id);
		vEditor.commit();
	}

	//Save Cidade ID in the preferences
	private void saveSelectedCidade(long p_id)
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putInt(OmegaConfiguration.SAVED_CIDADE, (int)p_id);
		vEditor.commit();
	}
	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;

		switch (id) {
		default:
			vDialog = this.getErrorDialog(id);
			break;
		}
		return vDialog;
	}	

	private int getSavedCidade()
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		return vSavedPreferences.getInt(OmegaConfiguration.SAVED_CIDADE, 0);
	}


	private Dialog getErrorDialog(int dialogType)
	{

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
		case FORM_ERROR_DIALOG_CADASTRO_OK:
			vTextView.setText(getResources().getString(R.string.cadastro_ok));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_CADASTRO_OK);
				}
			});
			break;
		case FORM_ERROR_DIALOG_CAMPO_INVALIDO:
			vTextView.setText(getResources().getString(R.string.error_invalid_field));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_CAMPO_INVALIDO);
				}
			});
			break;
		default:
			return super.onCreateDialog(dialogType);
		}

		return vDialog;
	}

	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onSearchButtonClicked(Activity pActivity)
	{
		new SearchButtonLoader(this).execute();
	}


	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean>
	{
		Activity activity;
		Intent intent ;
		public SearchButtonLoader(Activity pActivity){
			activity = pActivity;
			intent = activity.getIntent();
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{

				boolean vValidade = true;
				//First we check the obligatory fields:
				//Plano, Especialidade, Cidade
				//Checking Cidade
				String selectedCity = String.valueOf(cidadeSpinner.getSelectedItemId());
				String nameSelectedCity = null;
				if(! selectedCity.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{	
					nameSelectedCity = String.valueOf(cidadeSpinner.getSelectedItem());
				} else {
					nameSelectedCity = null;
					selectedCity = null;
				}

				String selectedPais = String.valueOf(paisSpinner.getSelectedItemId());
				String nameSelectedPais = null;
				if(! selectedPais.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{	
					nameSelectedPais = String.valueOf(paisSpinner.getSelectedItem());
				} else{
					nameSelectedPais = null;
					selectedPais = null;
				}

				String selectedEstado = String.valueOf(estadoSpinner.getSelectedItemId());
				String nameSelectedEstado = null;
				if(! selectedEstado.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{	
					nameSelectedEstado = String.valueOf(estadoSpinner.getSelectedItem());
				} else{
					nameSelectedEstado = null;
					selectedEstado = null;
				}

				String selectedNeighbourhood = String.valueOf(bairroSpinner.getSelectedItemId());
				String nameSelectedNeighbourhood = null;
				if(!selectedNeighbourhood.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{
					nameSelectedNeighbourhood = String.valueOf(bairroSpinner.getSelectedItem());
				} else{
					selectedNeighbourhood = null;
					nameSelectedNeighbourhood = null;
				}


				String selectedLogradouro = "";
				if(!logradouroEditText.getText().toString().equals(""))
				{
					selectedLogradouro = logradouroEditText.getText().toString();
					//			vObj.setAttrStrValue(Table.LOGRADOURO_UNIVERSAL, logradouroEditText.getText().toString());
				}

				String selectedNumero = "";
				if(!numeroEditText.getText().toString().equals(""))
				{
					selectedNumero = numeroEditText.getText().toString();
					//			vObj.setAttrStrValue(Table.NUMERO_UNIVERSAL, numeroEditText.getText().toString());
				}

				String selectedComplemento ="";
				if(!complementoEditText.getText().toString().equals(""))
				{
					selectedComplemento = complementoEditText.getText().toString();
					//			vObj.setAttrStrValue(Table.COMPLEMENTO_UNIVERSAL, complementoEditText.getText().toString());
				}

				if(vValidade){					
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO, selectedEstado);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE, selectedCity);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO, selectedNeighbourhood);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PAIS, selectedPais);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE, nameSelectedCity);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_BAIRRO, nameSelectedNeighbourhood);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO, nameSelectedEstado);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS, nameSelectedPais);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO, selectedLogradouro);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO, selectedNumero );
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO, selectedComplemento );
					if(idLinearLayoutEnderecoRetorno != null)
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, idLinearLayoutEnderecoRetorno);
					
				}
				
				return true;
			}
			catch(Exception e)
			{
				//if(e != null) Log.e(TAG, e.getMessage());
				//else Log.e(TAG, "Error desconhecido");
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
				if(result){
					setResult(OmegaConfiguration.ACTIVITY_FORM_ADD_LAYOUT_ENDERECO, intent);
					activity.finish( );
				}
			} catch (Exception ex) {
				SingletonLog.openDialogError(FilterEnderecoActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
		
			
		}
	}

	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------


	private class CancelarButtonListener implements OnClickListener
	{

		Activity activity;

		public CancelarButtonListener(Activity pActivity){
			activity = pActivity;
		}
		public void onClick(View v) {
			activity.finish();

		}
	}	

	private class CadastroButtonListener implements OnClickListener
	{
		Activity activity;
		public CadastroButtonListener(Activity pActivity){
			activity = pActivity;
		}

		public void onClick(View v) {
			onSearchButtonClicked(activity);

		}

	}	

	//
	//	private class DeleteViewEnderecoButtonListener implements OnClickListener
	//	{
	//		View view= null;
	//		
	//		public DeleteViewEnderecoButtonListener(View pView){
	//			view = pView;
	//		}
	//		
	//		public void onClick(View v) {
	//			LinearLayout linearLayoutEmpresaFuncionario = (LinearLayout) findViewById(R.id.linearlayout_endereco);
	//			linearLayoutEmpresaFuncionario.removeView(view);
	//		}
	//	}
}


