package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import java.util.LinkedHashMap;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListEstadoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.TextView.TableDependentAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPais;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;

public class FilterEstadoActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "FilterEstadoActivityMobile";
	private static final String TABELAS_RELACIONADAS[] = new String[]{EXTDAOPais.NAME, EXTDAOUf.NAME};

	private Spinner paisSpinner;
	OnFocusChangeListener paisOnFocusChangeListener;
	//Search button
	private Button buscarButton;
	
	Drawable originalDrawable;

	//Spinners
	private DatabasePontoEletronico db=null;
	
	private AutoCompleteTextView estadoAutoCompletEditText;
	TableDependentAutoCompleteTextView estadoContainerAutoComplete = null;
	
	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_estado_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		//
		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();

	}

	@Override
	public boolean loadData() {
		return true;
	}

	private void onSearchButtonClicked()
	{
		new SearchButtonLoader(this).execute();
	}

	public void formatarStyle()
	{
		//Search mode buttons
		((TextView) findViewById(R.id.pais_textview)).setTypeface(this.customTypeFace);
		((TextView) findViewById(R.id.estado_textview)).setTypeface(this.customTypeFace);

		//Botuo cadastrar:
		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);
		
 
	}

	@Override
	public void initializeComponents() {
		
		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);		
		
		EXTDAOUf vObjEstado = new EXTDAOUf(this.db); 
		this.estadoContainerAutoComplete = new TableDependentAutoCompleteTextView(vObjEstado, EXTDAOUf.PAIS_ID_INT);
		this.estadoAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.estado_autocompletetextview);
		
		new MaskUpperCaseTextWatcher(estadoAutoCompletEditText, 100);
		
		
		this.paisSpinner = (Spinner) findViewById(R.id.pais_spinner);
		this.paisSpinner.setAdapter(new CustomSpinnerAdapter(
				getApplicationContext(), 
				this.getAllPais(),
				R.layout.spinner_item,R.string.form_all_string));
		((CustomSpinnerAdapter) this.paisSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
		this.paisSpinner.setOnItemSelectedListener(new PaisSpinnerListener());
		
		//Attach search button to its listener
		this.buscarButton = (Button) findViewById(R.id.buscar_button);
		this.buscarButton.setOnClickListener(new CadastroButtonListener());

		this.formatarStyle();
		
		this.db.close();
	}
	
	private LinkedHashMap<String, String> getAllPais()
	{
		EXTDAOPais vObj = new EXTDAOPais(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}



	protected LinkedHashMap<String, String> getAllProfissao()
	{
		EXTDAOProfissao vObj = new EXTDAOProfissao(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}


	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click

	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean>
	{
		Activity activity;
		Intent intent ;
		public SearchButtonLoader(Activity pActivity){
			activity = pActivity;
			intent = new Intent(activity, ListEstadoActivityMobile.class);
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				String selectedPais = String.valueOf(paisSpinner.getSelectedItemId());
				
				if(! selectedPais.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{	
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PAIS, selectedPais);
				} else{
					selectedPais = null;
				}
				
				String nameEstado = estadoAutoCompletEditText.getText().toString();
				if( nameEstado.length() > 0)
				{
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO, nameEstado);
				}
				startActivity(intent);
				return true;
			}
			catch(Exception e)
			{
				//if(e != null) Log.e(TAG, e.getMessage());
				//else Log.e(TAG, "Error desconhecido");
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			controlerProgressDialog.dismissProgressDialog();
							
		}
	}

	
	
	@Override
	public void finish(){
		try{
			this.db.close();
			//			Se for cadastro
			super.finish();			
		}catch(Exception ex){

		}
	}

	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------

	private class PaisSpinnerListener implements OnItemSelectedListener
	{

		public void onItemSelected(AdapterView<?> parent, View view, int position,long id) {

			
			showCitiesFromState(String.valueOf(id));
		}
		public void onNothingSelected(AdapterView<?> parent) {
			//Do nothing
		}
	}

	public class CancelarButtonListener implements OnClickListener
	{

		Activity activity;

		public CancelarButtonListener(Activity pActivity){
			activity = pActivity;
		}
		public void onClick(View v) {
			activity.finish();

		}
	}	

	private class CadastroButtonListener implements OnClickListener
	{
		
		public CadastroButtonListener(){
		
		}

		public void onClick(View v) {
			onSearchButtonClicked();

		}

	}	

	private void showCitiesFromState(String pPaisId)
	{
		ArrayAdapter<String> estadoAdapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item,
				estadoContainerAutoComplete.getVetorStrId(pPaisId));
		estadoAutoCompletEditText.setAdapter(estadoAdapter);
	}
	
}


