package app.omegasoftware.pontoeletronico.TabletActivities.filter;

//public class FilterMeuPontoActivityMobile extends OmegaRegularActivity {
//
//	//Constants
//	public static final String TAG = "FilterMeuPontoActivityMobile";
//	
//	//Search button
//	private Button searchButton;
//	
//	private FactoryDateClickListener factoryDateClickListener;
//	private FactoryHourClickListener factoryHourClickListener;
//	
//	private final int FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_QUE_DATA_FIM = 7;
//	
//	private String idMinhaPessoa = null;
//	private ContainerClickListenerDate dataInicioClickListener;
//	private ContainerClickListenerDate dataFimClickListener;
//	
//	private ShowDialogHandler showDialogHandler = new ShowDialogHandler();
//	private Button inicioDataButton;
//	private Button fimDataButton;
//	
//	private Spinner empresaSpinner;
//	private Spinner pessoaSpinner;
//	
//	final int ERROR_DIALOG_USUARIO_SEM_PESSOA_LINKADA = 939242074;
//	private DatabasePontoEletronico db=null;
//	
//
//	private Typeface customTypeFace;
//	
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.filter_ponto_activity_mobile_layout);
//
//		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
//		
//		new CustomDataLoader(this, EXTDAOPonto.TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();
//
//	}
//
//	@Override
//	public boolean loadData() {
//
//		return true;
//
//	}
//
//	public void formatarStyle()
//	{
//		
//		//Search mode buttons
//				//Cidade em que deseja ser atendido:
//		
//		((TextView) findViewById(R.id.data_inicio_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.data_fim_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.empresa_textview)).setTypeface(this.customTypeFace);
//		
//		((TextView) findViewById(R.id.pessoa_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.pessoa_textview)).setVisibility(View.GONE);
//		
//		//Botuo buscar:
//		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);
//		
//	}
//	
//	@Override
//	public void initializeComponents() {
//
//		try {
//			this.db = new DatabasePontoEletronico(this);
//			
//
//		factoryDateClickListener = new FactoryDateClickListener(this);
//		factoryHourClickListener = new FactoryHourClickListener(this);
//		this.inicioDataButton = (Button) findViewById(R.id.data_inicio_button);
//		this.fimDataButton = (Button) findViewById(R.id.data_fim_button);
//		dataInicioClickListener = factoryDateClickListener.setDateClickListener(inicioDataButton);
//		dataFimClickListener = factoryDateClickListener.setDateClickListener(fimDataButton);
//		
//		
//		this.empresaSpinner = (Spinner) findViewById(R.id.empresa_spinner);
//		this.empresaSpinner.setAdapter(new CustomSpinnerAdapter(getApplicationContext(), EXTDAOEmpresa.getAllEmpresa(db),R.layout.spinner_item,R.string.selecione));
//		((CustomSpinnerAdapter) this.empresaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
//		empresaSpinner.setVisibility(View.GONE);
//		
//		this.pessoaSpinner = (Spinner) findViewById(R.id.pessoa_spinner);
//		pessoaSpinner.setVisibility(View.GONE);
//
//		EXTDAOPessoaUsuario vObj = new EXTDAOPessoaUsuario(db);
//		String vIdPessoa = vObj.getIdPessoaDoUsuarioLogado();
//		
//		if(vIdPessoa ==null){
//			showDialogHandler.sendEmptyMessage(0);
//		} else {
//			idMinhaPessoa = vIdPessoa;
//		}
//		
//		this.searchButton = (Button) findViewById(R.id.buscar_button);
//		this.searchButton.setOnClickListener(new SearchButtonListener());
//		//	Attach search button to its listener
//		this.formatarStyle();		//Attach search button to its listener
//		
//		} catch (OmegaDatabaseException e) {
//			SingletonLog.factoryToast(this, e, SingletonLog.TIPO.PAGINA);
//		}finally{
//			this.db.close();
//		}
//	}
//
//	
//
//	public class ShowDialogHandler extends Handler{
//		
//		@Override
//        public void handleMessage(Message msg)
//        {	
//			
//			showDialog(ERROR_DIALOG_USUARIO_SEM_PESSOA_LINKADA);
//        }
//	}
//	
//	@Override
//	protected void onPrepareDialog(int id, Dialog dialog) {
//        switch (id) {
//           
//            case FactoryDateClickListener.DATE_DIALOG_ID:
//            	factoryDateClickListener.onPrepareDialog(id, dialog);
//            	break;
//            case FactoryHourClickListener.TIME_DIALOG_ID:
//            	factoryHourClickListener.onPrepareDialog(id, dialog);
//            	break;
//        }
//    }
//
//	
//	@Override
//	protected Dialog onCreateDialog(int id) {
//
//		switch (id) {
//		
//		case FactoryDateClickListener.DATE_DIALOG_ID:
//			return factoryDateClickListener.onCreateDialog(id);
//		case FactoryHourClickListener.TIME_DIALOG_ID:
//        	return factoryHourClickListener.onCreateDialog(id);
//		case ERROR_DIALOG_USUARIO_SEM_PESSOA_LINKADA:
//			Dialog vDialog = new Dialog(this);
//			vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//			
//			vDialog.setContentView(R.layout.dialog);
//
//			TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
//			Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);
//			vTextView.setText(getResources().getString(R.string.error_dialog_pessoa_usuario_rotina));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				
//				public void onClick(View arg0) {
//					
//					onBackPressed();
//				}
//			});
//			return vDialog;
//		default:
//			return getErrorDialog(id);
//		}
//	}
//	
//	public Dialog getErrorDialog(int dialogType)
//	{
//
//		Dialog vDialog = new Dialog(this);
//		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		
//		vDialog.setContentView(R.layout.dialog);
//
//		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
//		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);
//
//		switch (dialogType) {
//		
//		case FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_QUE_DATA_FIM:
//			vTextView.setText(getResources().getString(R.string.filter_dialog_data_vazia_inicial_maior_final));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_QUE_DATA_FIM);
//				}
//			});
//			break;
//		
//		default:
//			return super.onCreateDialog(dialogType);
//		}
//
//		return vDialog;
//	}
//	
//	//---------------------------------------------------------------
//	//----------------- Methods related to search action ------------
//	//---------------------------------------------------------------
//	//Handles search button click
//	private void onSearchButtonClicked()
//	{
//		new SearchButtonLoader(this).execute();
//	}
//	
//
//	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean>
//	{
//		Activity activity;
//		Intent intent ;
//		Integer dialogId = null;
//		
//		public SearchButtonLoader(Activity pActivity){
//			activity = pActivity;
//			intent = new Intent(activity, ListTarefaMinhaActivityMobile.class);
//		}
//		
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			controlerProgressDialog.createProgressDialog();
//		}
//
//		@Override
//		protected Boolean doInBackground(String... params) {
//			try{	
//				Bundle vParams = new Bundle();
//
//				//First we check the obligatory fields:
//				//Plano, Especialidade, Cidade
//				
//				if(FactoryDateClickListener.isDateButtonSet(activity, inicioDataButton))
//				{
//					String vInicioData = HelperDate.getDataFormatadaSQL(inicioDataButton.getText().toString());
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_INICIO_DATA_PROGRAMADA, vInicioData);	
//				} 
//				
//				if(FactoryDateClickListener.isDateButtonSet(activity, fimDataButton))
//				{
//					if(FactoryDateClickListener.isDateButtonSet(activity, inicioDataButton)){
//						Date vDateFim = dataFimClickListener.getDate();
//						Date vDateInicio = dataInicioClickListener.getDate();
//						if(vDateInicio.compareTo(vDateFim) > 0){
//							dialogId = FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_QUE_DATA_FIM;
//							return false;
//						}
//						vParams.putString(OmegaConfiguration.SEARCH_FIELD_FIM_DATA_PROGRAMADA, HelperDate.getDataFormatadaSQL(fimDataButton.getText().toString()));
//					}
//					
//				}
//				if(idMinhaPessoa == null){
//					showDialogHandler.sendEmptyMessage(0);
//					return true;
//				}
//				else{
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA, idMinhaPessoa);
//					intent.putExtras(vParams);
//					
//					startActivity(intent);
//					return true;
//				}
//			}
//			catch(Exception e)
//			{
//				//if(e != null) Log.e(TAG, e.getMessage());
//				//else Log.e(TAG, "Error desconhecido");
//			}
//			return false;
//		}
//
//		@Override
//		protected void onPostExecute(Boolean result) {
//			super.onPostExecute(result);
//			controlerProgressDialog.dismissProgressDialog();
//			if(dialogId != null)
//				showDialog(dialogId);
//
//		}
//	}
//
//	
//
//	
//	//---------------------------------------------------------------
//	//------------------ Methods related to planos ------------------
//	//---------------------------------------------------------------
//	private class SearchButtonListener implements OnClickListener
//	{
//
//		public void onClick(View v) {
//			onSearchButtonClicked();
//		}
//
//	}	
//
//}
//
//
