package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import java.util.LinkedHashMap;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListModeloActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOModelo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculo;

public class FilterModeloActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "FilterModeloActivityMobile";
	private static final String TABELAS_RELACIONADAS[] = new String[]{EXTDAOModelo.NAME, EXTDAOVeiculo.NAME};
	//Search button
	private Button searchButton;

	//EditText
	
	
	//Spinners
	private Spinner nomeModeloSpinner;
	private Spinner anoModeloSpinner;
	
	private DatabasePontoEletronico db=null;


	private Typeface customTypeFace;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_modelo_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
	
		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();

	}

	@Override
	public boolean loadData() {

		return true;

	}

	public void formatarStyle()
	{
		
		((TextView) findViewById(R.id.modelo_nome_textview)).setTypeface(this.customTypeFace);
		

		((TextView) findViewById(R.id.modelo_ano_textview)).setTypeface(this.customTypeFace);
		
		
		//Botuo buscar:
		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);
		
	}
	
	@Override
	public void initializeComponents() {
		
		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.formatarStyle();
		
		this.nomeModeloSpinner = (Spinner) findViewById(R.id.modelo_nome_spinner);
		CustomSpinnerAdapter customSpinnerAdapterCity = new CustomSpinnerAdapter(getApplicationContext(), this.getAllNomeModelo(),R.layout.spinner_item,R.string.selecione);
		this.nomeModeloSpinner.setAdapter(customSpinnerAdapterCity);
		((CustomSpinnerAdapter) this.nomeModeloSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
		this.nomeModeloSpinner.setOnItemSelectedListener(new NomeModeloSpinnerListener());


		this.anoModeloSpinner = (Spinner) findViewById(R.id.modelo_ano_spinner);
		this.anoModeloSpinner.setAdapter(new CustomSpinnerAdapter(getApplicationContext(), 
				new LinkedHashMap<String, String>(), R.layout.spinner_item,R.string.form_all_string));
		((CustomSpinnerAdapter) this.anoModeloSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		String vSavedCidadeId = String.valueOf(this.getSavedNomeModelo());
		this.setNomeModelo(vSavedCidadeId);

		
		//Attach search button to its listener
		this.searchButton = (Button) findViewById(R.id.buscar_button);
		this.searchButton.setOnClickListener(new SearchButtonListener());
		
		this.db.close();
	}
	
	
	private LinkedHashMap<String, String> getAllNomeModelo()
	{
		EXTDAOModelo vObj = new EXTDAOModelo(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}
	
	@Override
	public void finish(){
		try{
			db.close();
			super.finish();
		} catch(Exception ex){

		}
	}
		


	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onSearchButtonClicked()
	{
		new SearchButtonLoader(this).execute();
	}
	
	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean>
	{
		Activity activity;
		Intent intent ;
		public SearchButtonLoader(Activity pActivity){
			activity = pActivity;
			intent = new Intent(activity, ListModeloActivityMobile.class);
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				Bundle vParams = new Bundle();
				
				
				//Checking Cidade
				String selectedNomeModelo = String.valueOf(nomeModeloSpinner.getSelectedItemId());
				if(! selectedNomeModelo.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_NOME_MODELO, selectedNomeModelo);
				}
				String selectedAnoModelo = String.valueOf(anoModeloSpinner.getSelectedItemId());
				if(!selectedAnoModelo.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_NOME_BAIRRO,  selectedAnoModelo);
				}
				intent.putExtras(vParams);
				startActivity(intent);
				return true;
			}
			catch(Exception e)
			{
				//if(e != null) Log.e(TAG, e.getMessage());
				//else Log.e(TAG, "Error desconhecido");
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			controlerProgressDialog.dismissProgressDialog();
			
		}
	}

	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------

	
	private void setNomeModelo(String p_id)
	{
		//Get the position from this id
		//And selects it in the spinner
		String vCity = String.valueOf(p_id);
		CustomSpinnerAdapter spinnerAdapter = (CustomSpinnerAdapter) this.nomeModeloSpinner.getAdapter();
		this.nomeModeloSpinner.setSelection(spinnerAdapter.getPositionFromId(vCity));
		this.showAnoModeloFromNomeModelo(p_id);
	}


	
	private void showAnoModeloFromNomeModelo(String pAnoModelo)
	{
//		if(!this.db.isOpen())
//		{
//			this.db = new DatabasePontoEletronico(this);
//		}

		String city = String.valueOf(pAnoModelo);
		EXTDAOModelo objModelo = new EXTDAOModelo(this.db);
		
		this.anoModeloSpinner.setAdapter(
				new CustomSpinnerAdapter(getApplicationContext(), 
						objModelo.getAnoModeloFromNomeModelo(city), R.layout.spinner_item,R.string.form_all_string));
		((CustomSpinnerAdapter) this.anoModeloSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		this.db.close();

	}

	

	//Save Cidade ID in the preferences
	private void saveSelectedNomeModelo(long p_id)
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putInt(OmegaConfiguration.SAVED_NOME_MODELO, (int)p_id);
		vEditor.commit();
	}
	
	
	private int getSavedNomeModelo()
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		return vSavedPreferences.getInt(OmegaConfiguration.SAVED_NOME_MODELO, 0);
	}
	
	

	//---------------------------------------------------------------
	//----------------------- Event listeners -----------------------
	//---------------------------------------------------------------
	private class NomeModeloSpinnerListener implements OnItemSelectedListener
	{

		public void onItemSelected(AdapterView<?> parent, View view, int position,long id) {

			saveSelectedNomeModelo(id);
			showAnoModeloFromNomeModelo(String.valueOf(id));

		}

		public void onNothingSelected(AdapterView<?> parent) {
			//Do nothing
		}

	}


	private class SearchButtonListener implements OnClickListener
	{

		public void onClick(View v) {
			onSearchButtonClicked();
		}

	}	


}


