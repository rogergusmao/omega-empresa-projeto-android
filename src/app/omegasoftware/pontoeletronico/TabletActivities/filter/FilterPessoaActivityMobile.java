package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import java.util.LinkedHashMap;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutEndereco;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.FilterEnderecoAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.AppointmentPlaceItemList;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskLowerCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSexo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoEmpresa;

public class FilterPessoaActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "FilterPessoaActivityMobile";	
	
	Boolean inputIsEmpresaDaCorporacao = null;
	//Search button
	private Button searchButton;
	private ImageButton buscarTopoButton;
	ContainerLayoutEndereco containerEndereco = null;
	CheckBox isClienteDiretoCheckBox;
	//Spinners
	private Spinner sexSpinner;
	private Spinner tipoEmpresaSpinner;
	private Spinner profissaoSpinner;
	private DatabasePontoEletronico db=null;	
	
	private Button enderecoButton;
	CheckBox parceiroCheckBox;
	CheckBox clienteCheckBox;
	CheckBox fornecedorCheckBox;
	CheckBox empresaDaCorporacaoCheckBox;

	//EditText
	private EditText nomePessoaEditText;
	private EditText emailEditText;
	private EditText nomeEmpresaEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_pessoa_activity_mobile_layout);

		Bundle vParameter = getIntent().getExtras();
		if(vParameter != null){
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO)){
				inputIsEmpresaDaCorporacao = vParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO);
				
			}
		}
		
		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {

		return true;

	}


	@Override
	public void initializeComponents() throws OmegaDatabaseException {
		
		this.db = new DatabasePontoEletronico(this);
		
		this.enderecoButton = (Button) findViewById(R.id.endereco_button);
		this.enderecoButton.setOnClickListener(new CadastrarEnderecoListener());

		isClienteDiretoCheckBox = (CheckBox) findViewById(R.id.is_consumidor_checkbox);
		
		this.sexSpinner = (Spinner) findViewById(R.id.sexo_spinner);
		this.sexSpinner.setAdapter(new CustomSpinnerAdapter(getApplicationContext(), this.getAllSexo(), R.layout.spinner_item,R.string.sexo));
		((CustomSpinnerAdapter) this.sexSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);


		this.tipoEmpresaSpinner = (Spinner) findViewById(R.id.tipo_empresa_spinner);
		this.tipoEmpresaSpinner.setAdapter(new CustomSpinnerAdapter(getApplicationContext(), this.getAllTipoEmpresa(),R.layout.spinner_item,R.string.area_empresa));
		((CustomSpinnerAdapter) this.tipoEmpresaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);		
		
		Button mostrarEmpresa = (Button) findViewById(R.id.mostrar_empresa_button);
		mostrarEmpresa.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				v.setVisibility(View.GONE);
				LinearLayout llEmpresa = (LinearLayout) findViewById(R.id.ll_empresa);		
				llEmpresa.setVisibility(View.VISIBLE);
			}
		});
		
		this.profissaoSpinner = (Spinner) findViewById(R.id.profissao_spinner);
		this.profissaoSpinner.setAdapter(new CustomSpinnerAdapter(getApplicationContext(), this.getAllProfissao(), R.layout.spinner_item, R.string.profissao));
		((CustomSpinnerAdapter) this.profissaoSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		this.nomePessoaEditText = (EditText) findViewById(R.id.nome_edittext);
		new MaskUpperCaseTextWatcher(this.nomePessoaEditText, 255);
		
		this.emailEditText = (EditText) findViewById(R.id.email_edittext);
		new MaskLowerCaseTextWatcher(this.emailEditText, 255);
		
		
		this.nomeEmpresaEditText = (EditText) findViewById(R.id.nomedaempresa_edittext);
		new MaskUpperCaseTextWatcher(this.nomeEmpresaEditText, 255);
		
		parceiroCheckBox = (CheckBox) findViewById(R.id.form_empresa_parceiro_checkbox);
		clienteCheckBox = (CheckBox) findViewById(R.id.form_empresa_cliente_checkbox);
		fornecedorCheckBox = (CheckBox) findViewById(R.id.form_empresa_fornecedor_checkbox);
		empresaDaCorporacaoCheckBox = (CheckBox) findViewById(R.id.form_empresa_empresa_corporacao_checkbox);
		
		if(inputIsEmpresaDaCorporacao != null){
			if(inputIsEmpresaDaCorporacao){
				parceiroCheckBox.setVisibility(View.GONE);
				clienteCheckBox.setVisibility(View.GONE);
				fornecedorCheckBox.setVisibility(View.GONE);
				empresaDaCorporacaoCheckBox.setVisibility(View.GONE);
				isClienteDiretoCheckBox.setVisibility(View.GONE);
				((TextView) findViewById(R.id.perfil_empresa_da_pessoa_textview)).setVisibility(View.GONE);
			} else{
//				FilterPessoaContato
				empresaDaCorporacaoCheckBox.setVisibility(View.GONE);
				
			}	
		}
		
		
		//Attach search button to its listener
		this.searchButton = (Button) findViewById(R.id.buscar_button);
		this.searchButton.setOnClickListener(new SearchButtonListener());
		View vBuscarTopo = findViewById(R.id.buscar_topo_button);
		
		if(vBuscarTopo != null){
			this.buscarTopoButton= (ImageButton) vBuscarTopo;
			this.buscarTopoButton.setOnClickListener(new SearchButtonListener());
		}
		this.db.close();


	}

	
	private LinkedHashMap<String, String> getAllTipoEmpresa()
	{
		EXTDAOTipoEmpresa vObj = new EXTDAOTipoEmpresa(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}
	
	protected LinkedHashMap<String, String> getAllSexo()
	{
		EXTDAOSexo vObj = new EXTDAOSexo(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}

	private LinkedHashMap<String, String> getAllProfissao()
	{
		EXTDAOProfissao vObj = new EXTDAOProfissao(this.db);
		return vObj.getAllEspecialidades();
	}

	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onSearchButtonClicked()
	{
		new SearchButtonLoader().execute();
	}

	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean>
	{
		
		
		public SearchButtonLoader(){
			
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				Bundle vParams = new Bundle();
				String nomePessoa = nomePessoaEditText.getText().toString();
				if(!nomePessoa.equals(""))
				{
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_NOME_FUNCIONARIO, nomePessoaEditText.getText().toString());	
				}
				if(!emailEditText.getText().toString().equals(""))
				{
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_EMAIL, emailEditText.getText().toString());	
				}
				
				
				String selectedSex = String.valueOf(sexSpinner.getSelectedItemId());
				if(!selectedSex.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_SEXO_FUNCIONARIO, selectedSex);	
				}
				//Checking Profissao
				String selectedProfissao = String.valueOf(profissaoSpinner.getSelectedItemId());
				if( ! selectedProfissao.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{	
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_PROFISSAO, selectedProfissao);
				}

				if(!nomeEmpresaEditText.getText().toString().equals(""))
				{
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_NOME_EMPRESA, nomeEmpresaEditText.getText().toString());	
				}	
				//Checking TipoEmpresa
				String selectedTipoEmpresa = String.valueOf(tipoEmpresaSpinner.getSelectedItemId());
				if( ! selectedTipoEmpresa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{	
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_TIPO_EMPRESA, selectedTipoEmpresa);
				}
				if(containerEndereco != null){
					AppointmentPlaceItemList appointmentPlaceItemList = containerEndereco.getAppointmentPlaceItemList();
					if(appointmentPlaceItemList != null){
						vParams.putString(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO, appointmentPlaceItemList.getAddress());
						vParams.putString(OmegaConfiguration.SEARCH_FIELD_NUMERO, appointmentPlaceItemList.getNumber());
						vParams.putString(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO, appointmentPlaceItemList.getComplemento());
						vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE, appointmentPlaceItemList.getIdCity());
						vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO, appointmentPlaceItemList.getIdNeighborhood());
						vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO, appointmentPlaceItemList.getIdUf());
					}
				}
				if(parceiroCheckBox.getVisibility() == View.VISIBLE)
					if(parceiroCheckBox.isChecked())
						vParams.putBoolean(OmegaConfiguration.SEARCH_FIELD_IS_PARCEIRO, true);
				if(fornecedorCheckBox.getVisibility() == View.VISIBLE)
					if(fornecedorCheckBox.isChecked())
						vParams.putBoolean(OmegaConfiguration.SEARCH_FIELD_IS_FORNECEDOR, true);
				if(clienteCheckBox.getVisibility() == View.VISIBLE)
					if(clienteCheckBox.isChecked())
						vParams.putBoolean(OmegaConfiguration.SEARCH_FIELD_IS_CLIENTE, true);
				
				if(isClienteDiretoCheckBox.getVisibility() == View.VISIBLE)
					if(isClienteDiretoCheckBox.isChecked())
						vParams.putBoolean(OmegaConfiguration.SEARCH_FIELD_IS_CONSUMIDOR, true);
				
				if(inputIsEmpresaDaCorporacao != null){
					vParams.putBoolean(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO, inputIsEmpresaDaCorporacao);
				} else{
					if(empresaDaCorporacaoCheckBox.getVisibility() == View.VISIBLE)
						if(empresaDaCorporacaoCheckBox.isChecked())
							vParams.putBoolean(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO, true);
				}
//				Intent intent ;	
//				intent = new Intent(FilterPessoaActivityMobile.this, ListPessoaActivityMobile.class);
//				intent.putExtras(vParams);
//				startActivity(intent);
				
				Intent intent = getIntent();
				intent.putExtras(vParams);
				
				setResult(RESULT_OK, intent);
				
				return true;
			}
			catch(Exception e)
			{
				SingletonLog.insereErro(e, TIPO.FILTRO);
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
			
			finish();
			} catch (Exception ex) {
				SingletonLog.openDialogError(FilterPessoaActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}

	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------




	public ContainerLayoutEndereco addLayoutEndereco(
			String pIdBairro ,
			String pIdCidade ,
			String pIdEstado,
			String pIdPais,
			String pNomeBairro ,
			String pNomeCidade ,
			String pNomeEstado,
			String pNomePais, 
			String pLogradouro ,
			String pNumero ,
			String pComplemento, 
			LinearLayout pLinearLayoutEndereco, 
			Button pAddButtonEndereco){

		

		AppointmentPlaceItemList appointmentPlaceItemList = new AppointmentPlaceItemList(
				getApplicationContext(), 
				0,
				pLogradouro ,
				pNumero ,
				pComplemento,
				pNomeBairro, 
				pNomeCidade,
				pNomeEstado,
				pNomePais,
				pIdBairro ,
				pIdCidade ,
				pIdEstado,
				pIdPais,
				null,
				null,
				null);
		//		LinearLayout linearLayoutEndereco = (LinearLayout) findViewById(R.id.linearlayout_endereco);

		ContainerLayoutEndereco vContainerEndereco = new ContainerLayoutEndereco(
				this, 
				appointmentPlaceItemList,
				pLinearLayoutEndereco);

		FilterEnderecoAdapter adapter = new FilterEnderecoAdapter(
				this, 
				vContainerEndereco, 
				pAddButtonEndereco);

		View vNewView = adapter.getView(0, null, null);

		pLinearLayoutEndereco.addView(vNewView);

		vContainerEndereco.setAppointmentPlace(vNewView);

		return vContainerEndereco;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		switch (requestCode) {
		case OmegaConfiguration.ACTIVITY_FORM_ENDERECO:
			switch (resultCode) {
			case OmegaConfiguration.ACTIVITY_FORM_DELETE_LAYOUT_ENDERECO:
				int vIdLinearLayout = intent.getIntExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, -1);
				if(vIdLinearLayout == R.id.linearlayout_endereco){
					containerEndereco.delete();
					if(enderecoButton != null)
						enderecoButton.setVisibility(View.VISIBLE);
				}
				break;
			case OmegaConfiguration.ACTIVITY_FORM_ADD_LAYOUT_ENDERECO:
				try{
					String vIdCidade = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE);
					String vIdBairro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO);
					String vIdEstado = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO);
					String vIdPais = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_PAIS);
					String vNomeCidade = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE);
					String vNomeBairro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_BAIRRO);
					String vNomeEstado = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO);
					String vNomePais = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS);
					String vLogradouro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO);
					String vNumero = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO);
					String vComplemento = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO);
					LinearLayout vLinearLayoutEndereco = (LinearLayout) findViewById(R.id.linearlayout_endereco);
					if(containerEndereco != null)
						containerEndereco.delete();
					containerEndereco = addLayoutEndereco(
							vIdBairro, 
							vIdCidade, 
							vIdEstado, 
							vIdPais,
							vNomeBairro, 
							vNomeCidade, 
							vNomeEstado, 
							vNomePais,
							vLogradouro, 
							vNumero, 
							vComplemento,
							vLinearLayoutEndereco, 
							enderecoButton);
				}catch(Exception ex){
					//Log.e( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
				}
				break;
			default:
				break;
			}
			break;
		
		default:
			super.onActivityResult(requestCode, resultCode, intent);
			break;
		}
		
	}

	//---------------------------------------------------------------
	//----------------------- Event listeners -----------------------


	private class CadastrarEnderecoListener implements OnClickListener
	{

		Intent intent = null;

		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.endereco_button:
				intent = new Intent(getApplicationContext(), FilterEnderecoActivityMobile.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.linearlayout_endereco);
				break;

			default:
				break;
			}

			if(intent != null)
			{				
				try{

					startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_ENDERECO);
				}catch(Exception ex){
					//Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
				}
			}
		}
	}
	private class SearchButtonListener implements OnClickListener
	{

		public void onClick(View v) {
			onSearchButtonClicked();
		}

	}	


}


