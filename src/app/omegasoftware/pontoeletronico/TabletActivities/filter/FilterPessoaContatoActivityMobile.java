package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;

public class FilterPessoaContatoActivityMobile extends FilterPessoaActivityMobile {

	//Constants
	public static final String TAG = "FilterPessoaContatoActivityMobile";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO, false);
		super.onCreate(savedInstanceState);


	}

}


