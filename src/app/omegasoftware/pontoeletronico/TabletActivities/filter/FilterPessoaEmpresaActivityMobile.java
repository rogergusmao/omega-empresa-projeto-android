package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListPessoaEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOBairro;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPais;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;

public class FilterPessoaEmpresaActivityMobile extends OmegaRegularActivity {

	// Constants
	public static final String TAG = "FilterPessoaEmpresaActivityMobile";

	private static final String TABELAS_RELACIONADAS[] = new String[] { EXTDAOPessoa.NAME, EXTDAOPais.NAME,
			EXTDAOUf.NAME, EXTDAOCidade.NAME, EXTDAOBairro.NAME, EXTDAOEmpresa.NAME, EXTDAOPessoaEmpresa.NAME };

	// Search button
	private Button searchButton;

	// Spinners
	private Spinner empresaSpinner;
	private DatabasePontoEletronico db = null;

	// EditText
	private EditText nomePessoaEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, R.string.busca_identificador);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_pessoa_empresa_activity_mobile_layout);

		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();
	}

	@Override
	public boolean loadData() {

		return true;

	}

	@Override
	public void initializeComponents() throws OmegaDatabaseException {
		this.db = new DatabasePontoEletronico(this);

		this.empresaSpinner = (Spinner) findViewById(R.id.empresa_spinner);
		this.empresaSpinner.setAdapter(new CustomSpinnerAdapter(getApplicationContext(),
				EXTDAOEmpresa.getAllEmpresa(db), R.layout.spinner_item, R.string.selecione));
		((CustomSpinnerAdapter) this.empresaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		this.nomePessoaEditText = (EditText) findViewById(R.id.nomedopessoa_edittext);
		new MaskUpperCaseTextWatcher(this.nomePessoaEditText, 255);
		this.nomePessoaEditText.clearFocus();
		// Attach search button to its listener
		this.searchButton = (Button) findViewById(R.id.buscar_button);
		this.searchButton.setOnClickListener(new SearchButtonListener());

		this.db.close();
		
	}

	@Override
	public void finish() {
		try {

			this.db.close();
			// Se for cadastro
			super.finish();
		} catch (Exception ex) {

		}
	}

	// ---------------------------------------------------------------
	// ----------------- Methods related to search action ------------
	// ---------------------------------------------------------------
	// Handles search button click
	private void onSearchButtonClicked() {
		new SearchButtonLoader(this).execute();
	}

	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean> {
		Activity activity;
		Intent intent;
		Exception e;
		public SearchButtonLoader(Activity pActivity) {
			activity = pActivity;
			intent = new Intent(activity, ListPessoaEmpresaActivityMobile.class);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				Bundle vParams = new Bundle();

				if (!nomePessoaEditText.getText().toString().equals("")) {
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_NOME_PESSOA,
							nomePessoaEditText.getText().toString());
				}

				// Checking Profissao
				String selectedEmpresa = String.valueOf(empresaSpinner.getSelectedItemId());
				if (!selectedEmpresa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB)) {
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, selectedEmpresa);
				}
				intent.putExtras(vParams);
				startActivity(intent);
				return true;
			} catch (Exception e) {
				this.e=e;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			
			SingletonLog.openDialogError(activity, this.e, SingletonLog.TIPO.PAGINA);
			
			controlerProgressDialog.dismissProgressDialog();

		}
	}

	private class SearchButtonListener implements OnClickListener {

		public void onClick(View v) {
			onSearchButtonClicked();
		}

	}

}
