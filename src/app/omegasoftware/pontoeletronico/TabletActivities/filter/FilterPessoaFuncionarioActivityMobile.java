package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;

public class FilterPessoaFuncionarioActivityMobile extends FilterPessoaActivityMobile {

	//Constants
	public static final String TAG = "FilterPessoaFuncionarioActivityMobile";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO, true);
		super.onCreate(savedInstanceState);


	}

}


