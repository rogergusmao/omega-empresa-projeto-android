package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import java.util.Date;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListPontoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto;
import app.omegasoftware.pontoeletronico.date.ContainerClickListenerDate;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.listener.FactoryDateClickListener;
import app.omegasoftware.pontoeletronico.listener.FactoryHourClickListener;

public class FilterPontoActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "FilterPontoActivityMobile";
	
	//Search button
	private Button searchButton;
	
	private FactoryDateClickListener factoryDateClickListener;
	private FactoryHourClickListener factoryHourClickListener;
	
	private final int FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_QUE_DATA_FIM = 7;
	
	
	private ContainerClickListenerDate dataInicioClickListener;
	private ContainerClickListenerDate dataFimClickListener;
	
	private Button inicioDataButton;
	private Button fimDataButton;
	
	private Spinner empresaSpinner;
	private Spinner pessoaSpinner;
	
	private DatabasePontoEletronico db=null;

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_ponto_activity_mobile_layout);

		
		
		new CustomDataLoader(this, EXTDAOPonto.TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();

	}

	@Override
	public boolean loadData() {

		return true;

	}
	
	@Override
	public void initializeComponents() throws OmegaDatabaseException {

		this.db = new DatabasePontoEletronico(this);		

		factoryDateClickListener = new FactoryDateClickListener(this);
		factoryHourClickListener = new FactoryHourClickListener(this);
		this.inicioDataButton = (Button) findViewById(R.id.data_inicio_button);
		this.fimDataButton = (Button) findViewById(R.id.data_fim_button);
		dataInicioClickListener = factoryDateClickListener.setDateClickListener(inicioDataButton);
		dataFimClickListener = factoryDateClickListener.setDateClickListener(fimDataButton);
		
		
		this.empresaSpinner = (Spinner) findViewById(R.id.empresa_spinner);
		this.empresaSpinner.setAdapter(new CustomSpinnerAdapter(getApplicationContext(), EXTDAOEmpresa.getAllEmpresa(db),R.layout.spinner_item,R.string.selecione));
		((CustomSpinnerAdapter) this.empresaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
		
		this.pessoaSpinner = (Spinner) findViewById(R.id.pessoa_spinner);
		this.pessoaSpinner.setAdapter(new CustomSpinnerAdapter(getApplicationContext(), EXTDAOPessoa.getAllPessoa(db),R.layout.spinner_item,R.string.selecione));
		((CustomSpinnerAdapter) this.pessoaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
		
		this.searchButton = (Button) findViewById(R.id.buscar_button);
		this.searchButton.setOnClickListener(new SearchButtonListener());
		//	Attach search button to its listener

		this.db.close();
	}

	
	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
           
            case FactoryDateClickListener.DATE_DIALOG_ID:
            	factoryDateClickListener.onPrepareDialog(id, dialog);
            	break;
            case FactoryHourClickListener.TIME_DIALOG_ID:
            	factoryHourClickListener.onPrepareDialog(id, dialog);
            	break;
        }
    }

	@Override
	protected Dialog onCreateDialog(int id) {

		switch (id) {
		
		case FactoryDateClickListener.DATE_DIALOG_ID:
			return factoryDateClickListener.onCreateDialog(id);
		case FactoryHourClickListener.TIME_DIALOG_ID:
        	return factoryHourClickListener.onCreateDialog(id);
		
		default:
			return getErrorDialog(id);
		}
	}
	
	public Dialog getErrorDialog(int dialogType)
	{

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
		
		case FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_QUE_DATA_FIM:
			vTextView.setText(getResources().getString(R.string.filter_dialog_data_vazia_inicial_maior_final));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_QUE_DATA_FIM);
				}
			});
			break;
		
		default:
			return super.onCreateDialog(dialogType);
		}

		return vDialog;
	}
	
	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onSearchButtonClicked()
	{
		new SearchButtonLoader(this).execute();
	}
	

	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean>
	{
		Activity activity;
		Intent intent ;
		Integer dialogId = null;
		Exception e = null;
		
		public SearchButtonLoader(Activity pActivity){
			activity = pActivity;
			intent = new Intent(activity, ListPontoActivityMobile.class);
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{	
				Bundle vParams = new Bundle();

				//First we check the obligatory fields:
				//Plano, Especialidade, Cidade
				
				if(FactoryDateClickListener.isDateButtonSet(activity, inicioDataButton))
				{
					String vInicioData = HelperDate.getDataFormatadaSQL(inicioDataButton.getText().toString());
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_DATA_INICIAL, vInicioData);	
				} 
				
				if(FactoryDateClickListener.isDateButtonSet(activity, fimDataButton))
				{
					if(FactoryDateClickListener.isDateButtonSet(activity, inicioDataButton)){
						Date vDateFim = dataFimClickListener.getDate();
						Date vDateInicio = dataInicioClickListener.getDate();
						if(vDateInicio.compareTo(vDateFim) > 0){
							dialogId = FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_QUE_DATA_FIM;
							return false;
						}
						vParams.putString(OmegaConfiguration.SEARCH_FIELD_DATA_FINAL, HelperDate.getDataFormatadaSQL(fimDataButton.getText().toString()));
					}
					
				}
				
				String selectedEmpresa = String.valueOf(empresaSpinner.getSelectedItemId());
				if( ! selectedEmpresa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{	
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, selectedEmpresa);
				}
				
				String selectedPessoa = String.valueOf(pessoaSpinner.getSelectedItemId());
				if( ! selectedPessoa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{	
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, selectedPessoa);
				}
				
				intent.putExtras(vParams);
				
				startActivity(intent);
				return true;
			}
			catch(Exception e)
			{
				this.e = e;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
				if(this.e!=null){
					SingletonLog.openDialogError(FilterPontoActivityMobile.this, this.e, SingletonLog.TIPO.PAGINA);	
				} else  if(dialogId != null)
					showDialog(dialogId);
			} catch (Exception ex) {
				SingletonLog.openDialogError(FilterPontoActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}

	
	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------
	private class SearchButtonListener implements OnClickListener
	{

		public void onClick(View v) {
			onSearchButtonClicked();
		}

	}	

}


