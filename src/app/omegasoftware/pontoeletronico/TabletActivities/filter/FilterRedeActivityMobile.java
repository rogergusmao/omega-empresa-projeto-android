package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORede;

public class FilterRedeActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "FilterRedeActivityMobile";
	private static final String TABELAS_RELACIONADAS[] = new String[]{EXTDAORede.NAME};
		//Search button
	private Button buscarButton;
	
	Drawable originalDrawable;

	//Spinners
	private DatabasePontoEletronico db=null;
	
	private AutoCompleteTextView redeAutoCompletEditText;
	TableAutoCompleteTextView redeContainerAutoComplete = null;
	
	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_rede_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		//
		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();

	}

	
	@Override
	public boolean loadData() {
		return true;
	}


	public void formatarStyle()
	{
		//Search mode buttons
		((TextView) findViewById(R.id.rede_autocompletetextview)).setTypeface(this.customTypeFace);

		//Botuo cadastrar:
		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);
		

	}

	@Override
	public void initializeComponents() {
		

		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);		

//		this.redeAutoCompletEditText = (AutoCompleteTextView) findViewById(R.id.rede_autocompletetextview);
		 
//		this.redeContainerAutoComplete = new TableAutoCompleteTextView(vObjUf);
//		ArrayAdapter<String> redeAdapter = new ArrayAdapter<String>(
//				this,
//				R.layout.spinner_item,
//				redeContainerAutoComplete.getVetorStrId());
//		redeAutoCompletEditText.setAdapter(redeAdapter);
		EXTDAORede vObjRede = new EXTDAORede(this.db);
		this.redeAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.rede_autocompletetextview);
		this.redeContainerAutoComplete = new TableAutoCompleteTextView(this, vObjRede, redeAutoCompletEditText);
		new MaskUpperCaseTextWatcher(redeAutoCompletEditText, 100);
		
		
		//Attach search button to its listener
		this.buscarButton = (Button) findViewById(R.id.buscar_button);
		this.buscarButton.setOnClickListener(new CadastroButtonListener());


		this.formatarStyle();
		
		this.db.close();


	}

	@Override
	public void finish(){
		try{
					
			this.db.close();
			//			Se for cadastro
			super.finish();			
		}catch(Exception ex){

		}
	}

	
	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	
	private void onSearchButtonClicked()
	{
		new SearchButtonLoader().execute();
	}
	
	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean>
	{
		public SearchButtonLoader(){
			
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				Intent intent = getIntent();
				String nameRede = "";
				nameRede = redeAutoCompletEditText.getText().toString();
				if( nameRede.length() > 0)
				{
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS, nameRede);
				}
				
				
				
				setResult(RESULT_OK, intent);
				
				return true;
			}
			catch(Exception e)
			{
				//if(e != null) Log.e(TAG, e.getMessage());
				//else Log.e(TAG, "Error desconhecido");
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
				finish();
			} catch (Exception ex) {
				SingletonLog.openDialogError(FilterRedeActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}

	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------



	private class CadastroButtonListener implements OnClickListener
	{
		
		public CadastroButtonListener(){
			
		}

		public void onClick(View v) {
			onSearchButtonClicked();

		}

	}	
	
//
//	private class DeleteViewEnderecoButtonListener implements OnClickListener
//	{
//		View view= null;
//		
//		public DeleteViewEnderecoButtonListener(View pView){
//			view = pView;
//		}
//		
//		public void onClick(View v) {
//			LinearLayout linearLayoutEmpresaFuncionario = (LinearLayout) findViewById(R.id.linearlayout_endereco);
//			linearLayoutEmpresaFuncionario.removeView(view);
//		}
//	}
}


