package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import java.util.LinkedHashMap;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutEndereco;
//import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListTarefaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.FilterEnderecoAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.AppointmentPlaceItemList;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOBairro;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCategoriaPermissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPais;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa.TIPO_TAREFA;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculoUsuario;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.listener.FactoryDateClickListener;
import app.omegasoftware.pontoeletronico.listener.FactoryHourClickListener;

//public class FilterTarefaActivityMobile extends OmegaRegularActivity {
//
//	//Constants
//	public static final String TAG = "FilterTarefaActivityMobile";
//	
//	//Search button
//	private Button searchButton;
//	
//	private FactoryDateClickListener factoryDateClickListener;
//	private FactoryHourClickListener factoryHourClickListener;
//	
//	public static int ORIGEM = 0;
//	public static int DESTINO = 1;
//	
//
//	LoadTipoTarefaHandler handlerTipoTarefa;
//	LoadTipoEnderecoOrigemHandler handlerTipoEnderecoOrigem;
//	LoadTipoEnderecoDestinoHandler handlerTipoEnderecoDestino;
//	LoadEnderecoHandler handlerEndereco;
//	
//	private final int FORM_ERROR_DIALOG_ENDERECO_ORIGEM = 5;
//	private final int FORM_ERROR_DIALOG_DATA_EXIBIR_MAIOR_QUE_DATA_PROGRAMADA = 6;
//	private final int FORM_ERROR_DIALOG_USUARIO = 8;
//	private final int FORM_ERROR_DIALOG_VEICULO_USUARIO = 9;
//	private final int FORM_ERROR_DIALOG_VEICULO = 10;
//	private final int FORM_ERROR_DIALOG_TIPO_TAREFA = 11;
//	private final int FORM_ERROR_DIALOG_CATEGORIA_PERMISSAO = 12;
//	
//	private final int FORM_ERROR_DIALOG_TIPO_ENDERECO_PESSOA = 13;
//	private final int FORM_ERROR_DIALOG_TIPO_ENDERECO_EMPRESA = 14;
//
//	private CustomSpinnerAdapter identificadorTipoDestinoEnderecoCustomSpinnerAdapter;
//	private CustomSpinnerAdapter identificadorTipoOrigemEnderecoCustomSpinnerAdapter;
//	private CustomSpinnerAdapter identificadorTipoTarefaCustomSpinnerAdapter;
//	private CustomSpinnerAdapter tipoOrigemEnderecoCustomSpinnerAdapter;
//	private CustomSpinnerAdapter tipoDestinoEnderecoCustomSpinnerAdapter;
//	private CustomSpinnerAdapter tipoTarefaCustomSpinnerAdapter;
////	private CustomSpinnerAdapter estadoTarefaCustomSpinnerAdapter;
//	
//	private Spinner estadoTarefaSpinner;
//	
//	private Spinner criadoPeloUsuarioSpinner;
//	
//	private Spinner tipoTarefaSpinner;
//	
//	private Spinner identificadorTipoTarefaSpinner;
//	
//	private Spinner tipoOrigemEnderecoSpinner;
//	
//	private Spinner identificadorTipoOrigemEnderecoSpinner;
//	
//	private Spinner tipoDestinoEnderecoSpinner;
//	
//	private Spinner identificadorTipoDestinoEnderecoSpinner;
//	
//	private EditText tituloEditText;
//	//Spinners
//	
//	
//	private Button enderecoOrigemButton;
//	private Button enderecoDestinoButton;
//	
//	private Button inicioDataProgramadaButton;
//	private Button inicioHoraProgramadaButton;
//	
//	private Button inicioDataButton;
//	private Button inicioHoraButton;
//	private Button fimDataButton;
//	private Button fimHoraButton;
//	
//	private Button dataCadastroButton;
//	private Button horaCadastroButton;
//	private Button dataExibirButton;
//	private EditText descricaoEditText;
//	
//	ContainerLayoutEndereco containerEnderecoOrigem = null;
//	ContainerLayoutEndereco containerEnderecoDestino = null;
//	
//	private DatabasePontoEletronico db=null;
//
//
//	private Typeface customTypeFace;
//	
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.filter_tarefa_activity_mobile_layout);
//
//		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
//	
//		new CustomDataLoader(this, EXTDAOTarefa.TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();
//
//	}
//
//	@Override
//	public boolean loadData() {
//
//		return true;
//
//	}
//
//	public void formatarStyle()
//	{
//		
//		//Search mode buttons
//				//Cidade em que deseja ser atendido:
//		
//		((TextView) findViewById(R.id.estado_tarefa_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.criado_pelo_usuario_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.tipo_tarefa_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.identificador_tipo_tarefa_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.tipo_origem_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.identificador_tipo_origem_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.origem_endereco_textview)).setTypeface(this.customTypeFace);
//		
//		
//		((TextView) findViewById(R.id.origem_endereco_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.destino_endereco_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.data_exibir_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.data_exibir_button)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.inicio_data_programada_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.inicio_data_programada_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.data_exibir_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.inicio_data_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.inicio_hora_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.fim_data_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.fim_hora_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.data_cadastro_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.hora_cadastro_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.descricao_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.titulo_textview)).setTypeface(this.customTypeFace);
//		//Botuo buscar:
//		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);
//		
//	}
//	
//	@Override
//	public void initializeComponents() {
//
//		try {
//			this.db = new DatabasePontoEletronico(this);
//		} catch (OmegaDatabaseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}		
//
//		handlerEndereco  = new  LoadEnderecoHandler();
//		handlerTipoTarefa = new  LoadTipoTarefaHandler();
//		handlerTipoEnderecoOrigem = new  LoadTipoEnderecoOrigemHandler();
//		handlerTipoEnderecoDestino = new  LoadTipoEnderecoDestinoHandler();
//		
//		factoryDateClickListener = new FactoryDateClickListener(this);
//		factoryHourClickListener = new FactoryHourClickListener(this);
//		
//		tipoOrigemEnderecoSpinner = (Spinner) findViewById(R.id.tipo_origem_spinner);
//		tipoOrigemEnderecoCustomSpinnerAdapter  = new CustomSpinnerAdapter(
//				getApplicationContext(), 
//				EXTDAOTarefa.getAllTipoEndereco(this), 
//				R.layout.spinner_item,
//				R.string.selecione);
//		
//		this.tipoOrigemEnderecoSpinner.setAdapter(tipoOrigemEnderecoCustomSpinnerAdapter);
//		((CustomSpinnerAdapter) this.tipoOrigemEnderecoSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
//		this.tipoOrigemEnderecoSpinner.setOnItemSelectedListener(new TipoEnderecoOnClickListenner(this, true));
//		
//		identificadorTipoOrigemEnderecoSpinner = (Spinner) findViewById(R.id.identificador_tipo_origem_spinner);
//		this.identificadorTipoOrigemEnderecoSpinner.setOnItemSelectedListener(new EnderecoOnClickListenner(this, true));
//		
//		tipoDestinoEnderecoSpinner = (Spinner) findViewById(R.id.tipo_destino_spinner);
//		tipoDestinoEnderecoCustomSpinnerAdapter  = new CustomSpinnerAdapter(
//				getApplicationContext(), 
//				EXTDAOTarefa.getAllTipoEndereco(this), 
//				R.layout.spinner_item,
//				R.string.selecione);
//		
//		this.tipoDestinoEnderecoSpinner.setAdapter(tipoDestinoEnderecoCustomSpinnerAdapter);
//		((CustomSpinnerAdapter) this.tipoDestinoEnderecoSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
//		this.tipoDestinoEnderecoSpinner.setOnItemSelectedListener(new TipoEnderecoOnClickListenner(this, false));
//		
//		identificadorTipoDestinoEnderecoSpinner = (Spinner) findViewById(R.id.identificador_tipo_destino_spinner);
//		this.identificadorTipoDestinoEnderecoSpinner.setOnItemSelectedListener(new EnderecoOnClickListenner(this, false));
//
//		tipoTarefaSpinner = (Spinner) findViewById(R.id.tipo_tarefa_spinner);
//		tipoTarefaCustomSpinnerAdapter = new CustomSpinnerAdapter(
//				getApplicationContext(), 
//				EXTDAOTarefa.getAllTipoTarefaOrdenada(this), 
//				R.layout.spinner_item,
//				R.string.selecione);
//
//		this.tipoTarefaSpinner.setAdapter(tipoTarefaCustomSpinnerAdapter);
//		((CustomSpinnerAdapter) this.tipoTarefaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
//		this.tipoTarefaSpinner.setOnItemSelectedListener(new TipoTarefaOnClickListenner(this));
//		
//		identificadorTipoTarefaSpinner = (Spinner) findViewById(R.id.identificador_tipo_tarefa_spinner);
//		
//		
//		estadoTarefaSpinner = (Spinner) findViewById(R.id.estado_tarefa_spinner);
//		this.estadoTarefaSpinner.setAdapter(
//				new CustomSpinnerAdapter(
//						getApplicationContext(), 
//						EXTDAOTarefa.getAllEstadoTarefa(this), 
//						R.layout.spinner_item,
//						R.string.selecione));
//		((CustomSpinnerAdapter) this.estadoTarefaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
//		
//		criadoPeloUsuarioSpinner = (Spinner) findViewById(R.id.criado_pelo_usuario_spinner);
//		this.criadoPeloUsuarioSpinner.setAdapter(
//				new CustomSpinnerAdapter(
//						getApplicationContext(), 
//						EXTDAOUsuario.getAllUsuario(db), 
//						R.layout.spinner_item,
//						R.string.selecione));
//		((CustomSpinnerAdapter) this.criadoPeloUsuarioSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
//		
//		this.inicioDataProgramadaButton = (Button) findViewById(R.id.inicio_data_programada_button);
//		
//		
//		this.inicioHoraProgramadaButton = (Button) findViewById(R.id.inicio_hora_programada_button);
//		factoryHourClickListener.setHourClickListener(inicioHoraProgramadaButton);
//		
//		this.inicioDataButton = (Button) findViewById(R.id.inicio_data_button);
//		factoryDateClickListener.setDateClickListener(inicioDataButton);
//		
//		this.inicioHoraButton = (Button) findViewById(R.id.inicio_hora_button);
//		factoryHourClickListener.setHourClickListener(inicioHoraButton);
//		
//		this.dataCadastroButton = (Button) findViewById(R.id.data_cadastro_button);
//		factoryDateClickListener.setDateClickListener(dataCadastroButton);
//		
//		this.horaCadastroButton = (Button) findViewById(R.id.hora_cadastro_button);
//		factoryHourClickListener.setHourClickListener(horaCadastroButton);
//		
//		this.fimDataButton = (Button) findViewById(R.id.fim_data_button);
//		factoryDateClickListener.setDateClickListener(fimDataButton);
//		
//		this.fimHoraButton = (Button) findViewById(R.id.fim_hora_button);
//		factoryHourClickListener.setHourClickListener(fimHoraButton);
//		
//		this.dataExibirButton = (Button) findViewById(R.id.data_exibir_button);
//		
//		
//		this.tituloEditText = (EditText) findViewById(R.id.titulo_edittext);
//		new MaskUpperCaseTextWatcher(this.tituloEditText, 100);
//		
//		this.descricaoEditText = (EditText) findViewById(R.id.descricao_edittext);
//		new MaskUpperCaseTextWatcher(this.descricaoEditText, 512);
//		
//		this.searchButton = (Button) findViewById(R.id.buscar_button);
//		this.searchButton.setOnClickListener(new SearchButtonListener());
//		//	Attach search button to its listener
//
//
//		this.enderecoOrigemButton= (Button) findViewById(R.id.origem_endereco_button);
//		this.enderecoOrigemButton.setOnClickListener(new CadastrarEnderecoListener());
//
//		this.enderecoDestinoButton= (Button) findViewById(R.id.destino_endereco_button);
//		this.enderecoDestinoButton.setOnClickListener(new CadastrarEnderecoListener());
//		
//		
//		this.formatarStyle();		//Attach search button to its listener
//		this.db.close();
//	}
//	
//	
//	
//	private class CadastrarEnderecoListener implements OnClickListener
//	{
//
//		Intent intent = null;
//
//
//		public void onClick(View v) {
//
//			switch (v.getId()) {
//			case R.id.origem_endereco_button:
//
//				intent = new Intent(getApplicationContext(), FilterEnderecoActivityMobile.class);
//				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.endereco_origem_linearlayout);
//				break;
//
//			case R.id.destino_endereco_button:
//
//				intent = new Intent(getApplicationContext(), FilterEnderecoActivityMobile.class);
//				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.endereco_destino_linearlayout);
//
//				break;
//
//			default:
//				break;
//			}
//
//			if(intent != null)
//			{				
//				try{
//
//					startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_ENDERECO);
//				}catch(Exception ex){
//					//Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
//				}
//			}
//		}
//	}
//
//	
//	@Override
//	protected void onPrepareDialog(int id, Dialog dialog) {
//        switch (id) {
//           
//            case FactoryDateClickListener.DATE_DIALOG_ID:
//            	factoryDateClickListener.onPrepareDialog(id, dialog);
//            	break;
//            case FactoryHourClickListener.TIME_DIALOG_ID:
//            	factoryHourClickListener.onPrepareDialog(id, dialog);
//            	break;
//        }
//    }
//	public ContainerLayoutEndereco addLayoutEndereco(
//			String pIdBairro ,
//			String pIdCidade ,
//			String pIdEstado,
//			String pIdPais,
//			String pNomeBairro ,
//			String pNomeCidade ,
//			String pNomeEstado,
//			String pNomePais, 
//			String pLogradouro ,
//			String pNumero ,
//			String pComplemento, 
//			LinearLayout pLinearLayoutEndereco, 
//			Button pAddButtonEndereco){
//
//
//		
//
//		AppointmentPlaceItemList appointmentPlaceItemList = new AppointmentPlaceItemList(
//				getApplicationContext(), 
//				0,
//				pLogradouro ,
//				pNumero ,
//				pComplemento,
//				pNomeBairro, 
//				pNomeCidade,
//				pNomeEstado,
//				pNomePais,
//				pIdBairro ,
//				pIdCidade ,
//				pIdEstado,
//				pIdPais,
//				null,
//				null,
//				null);
//		//		LinearLayout linearLayoutEndereco = (LinearLayout) findViewById(R.id.linearlayout_endereco);
//
//		ContainerLayoutEndereco vContainerEndereco = new ContainerLayoutEndereco(
//				this, 
//				appointmentPlaceItemList,
//				pLinearLayoutEndereco);
//
//		FilterEnderecoAdapter adapter = new FilterEnderecoAdapter(
//				this, 
//				vContainerEndereco, 
//				pAddButtonEndereco);
//
//		View vNewView = adapter.getView(0, null, null);
//
//		pLinearLayoutEndereco.addView(vNewView);
//
//		vContainerEndereco.setAppointmentPlace(vNewView);
//		
//		return vContainerEndereco;
//	}
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//
//		if(requestCode == OmegaConfiguration.ACTIVITY_FORM_ENDERECO){
//			if(resultCode == OmegaConfiguration.ACTIVITY_FORM_DELETE_LAYOUT_ENDERECO){
//				int vIdLinearLayout = intent.getIntExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, -1);
//				if(vIdLinearLayout == R.id.endereco_destino_linearlayout && containerEnderecoDestino != null){
//					containerEnderecoDestino.delete();
//					if(enderecoDestinoButton != null)
//						enderecoDestinoButton.setVisibility(View.VISIBLE);
//
//				} else if(vIdLinearLayout == R.id.endereco_origem_linearlayout  && containerEnderecoOrigem != null){
//					containerEnderecoOrigem.delete();
//					if(enderecoOrigemButton != null)
//						enderecoOrigemButton.setVisibility(View.VISIBLE);
//
//				}
//			} else{
//				try{
//					String vIdCidade = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE);
//					String vIdBairro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO);
//					String vIdEstado = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO);
//					String vIdPais = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_PAIS);
//					String vNomeCidade = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE);
//					String vNomeBairro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_BAIRRO);
//					String vNomeEstado = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO);
//					String vNomePais = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS);
//					String vLogradouro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO);
//					String vNumero = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO);
//					String vComplemento = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO);
//					LinearLayout vLinearLayoutEndereco = (LinearLayout) findViewById(R.id.linearlayout_endereco);
//					Integer vEnderecoLayout = -1;
//
//					if(intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT))
//						vEnderecoLayout = intent.getIntExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, -1);
//
//					if(vEnderecoLayout != -1)
//						vLinearLayoutEndereco = (LinearLayout) findViewById(vEnderecoLayout);
//					else
//						vLinearLayoutEndereco = (LinearLayout) findViewById(R.id.linearlayout_endereco);
//
//					Button vButtonAddEndereco = null;
//
//					if(vEnderecoLayout == R.id.endereco_origem_linearlayout){
//						vButtonAddEndereco = this.enderecoOrigemButton;
//						if(containerEnderecoOrigem != null)
//							containerEnderecoOrigem.delete();
//						containerEnderecoOrigem = addLayoutEndereco(
//								vIdBairro, 
//								vIdCidade, 
//								vIdEstado,
//								vIdPais,
//								vNomeBairro, 
//								vNomeCidade, 
//								vNomeEstado,
//								vNomePais, 
//								vLogradouro, 
//								vNumero, 
//								vComplemento,
//								vLinearLayoutEndereco, 
//								vButtonAddEndereco);
//					}
//					else if(vEnderecoLayout == R.id.endereco_destino_linearlayout){
//						vButtonAddEndereco = this.enderecoDestinoButton;
//						if(containerEnderecoDestino != null)
//							containerEnderecoDestino.delete();
//						containerEnderecoDestino = addLayoutEndereco(
//								vIdBairro, 
//								vIdCidade, 
//								vIdEstado,
//								vIdPais,
//								vNomeBairro, 
//								vNomeCidade, 
//								vNomeEstado, 
//								vNomePais,
//								vLogradouro, 
//								vNumero, 
//								vComplemento,
//								vLinearLayoutEndereco, 
//								vButtonAddEndereco);
//					}
//
//				}catch(Exception ex){
//					//Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
//				}
//			}
//
//		} 
//	}
//
//	@Override
//	protected Dialog onCreateDialog(int id) {
//
//		switch (id) {
//		
//		case FactoryDateClickListener.DATE_DIALOG_ID:
//			return factoryDateClickListener.onCreateDialog(id);
//		case FactoryHourClickListener.TIME_DIALOG_ID:
//        	return factoryHourClickListener.onCreateDialog(id);
//		
//		default:
//			return getErrorDialog(id);
//		}
//	}
//	public Dialog getErrorDialog(int dialogType)
//	{
//
//		Dialog vDialog = new Dialog(this);
//		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		
//		vDialog.setContentView(R.layout.dialog);
//
//		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
//		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);
//
//		switch (dialogType) {
//		case FORM_ERROR_DIALOG_DATA_EXIBIR_MAIOR_QUE_DATA_PROGRAMADA:
//			vTextView.setText(getResources().getString(R.string.error_data_exibir_maior_que_data_programada));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_DATA_EXIBIR_MAIOR_QUE_DATA_PROGRAMADA);
//				}
//			});
//			break;
//		case FORM_ERROR_DIALOG_CATEGORIA_PERMISSAO:
//			vTextView.setText(getResources().getString(R.string.form_tarefa_error_dialog_tarefa_spinner_categoria_permissao));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_CATEGORIA_PERMISSAO);
//				}
//			});
//			break;
//		case FORM_ERROR_DIALOG_TIPO_ENDERECO_PESSOA:
//			vTextView.setText(getResources().getString(R.string.form_tarefa_error_dialog_tipo_endereco_pessoa));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_TIPO_ENDERECO_PESSOA);
//				}
//			});
//			break;
//		case FORM_ERROR_DIALOG_TIPO_ENDERECO_EMPRESA:
//			vTextView.setText(getResources().getString(R.string.form_tarefa_error_dialog_tipo_endereco_empresa));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_TIPO_ENDERECO_EMPRESA);
//				}
//			});
//			break;	
//		case FORM_ERROR_DIALOG_USUARIO:
//			vTextView.setText(getResources().getString(R.string.form_tarefa_error_dialog_tarefa_spinner_usuario));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_USUARIO);
//				}
//			});
//			break;
//		case FORM_ERROR_DIALOG_VEICULO:
//			vTextView.setText(getResources().getString(R.string.form_tarefa_error_dialog_tarefa_spinner_veiculo));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_VEICULO);
//				}
//			});
//			break;
//		case FORM_ERROR_DIALOG_VEICULO_USUARIO:
//			vTextView.setText(getResources().getString(R.string.form_tarefa_error_dialog_tarefa_spinner_veiculo_usuario));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_VEICULO_USUARIO);
//				}
//			});
//			break;
//		case FORM_ERROR_DIALOG_TIPO_TAREFA:
//			vTextView.setText(getResources().getString(R.string.form_tarefa_error_dialog_tipo_tarefa));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_TIPO_TAREFA);
//				}
//			});
//			break;
//		case FORM_ERROR_DIALOG_ENDERECO_ORIGEM:
//			vTextView.setText(getResources().getString(R.string.error_missing_endereco_origem));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_ENDERECO_ORIGEM);
//				}
//			});
//			break;
//		default:
//			return super.onCreateDialog(dialogType);
//		}
//
//		return vDialog;
//	}
//	
//	//---------------------------------------------------------------
//	//----------------- Methods related to search action ------------
//	//---------------------------------------------------------------
//	//Handles search button click
//	private void onSearchButtonClicked()
//	{
//		new SearchButtonLoader(this).execute();
//	}
//	
//
//	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean>
//	{
//		Activity activity;
//		Intent intent ;
//		public SearchButtonLoader(Activity pActivity){
//			activity = pActivity;
//			intent = new Intent(activity, ListTarefaActivityMobile.class);
//		}
//		
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			controlerProgressDialog.createProgressDialog();
//		}
//
//		@Override
//		protected Boolean doInBackground(String... params) {
//			try{	
//				Bundle vParams = new Bundle();
//
//				//First we check the obligatory fields:
//				//Plano, Especialidade, Cidade
//				int vVisibilityOrigemButton = enderecoOrigemButton.getVisibility();
//				if(vVisibilityOrigemButton == View.GONE )
//				{
//					AppointmentPlaceItemList appointmentPlaceItemList = containerEnderecoOrigem.getAppointmentPlaceItemList();
//					if(appointmentPlaceItemList != null){
//						vParams.putString(OmegaConfiguration.SEARCH_FIELD_ORIGEM_LOGRADOURO, appointmentPlaceItemList.getAddress());
//						vParams.putString(OmegaConfiguration.SEARCH_FIELD_ORIGEM_NUMERO, appointmentPlaceItemList.getNumber());
//						vParams.putString(OmegaConfiguration.SEARCH_FIELD_ORIGEM_COMPLEMENTO, appointmentPlaceItemList.getComplemento());
//						vParams.putString(OmegaConfiguration.SEARCH_FIELD_ORIGEM_ID_CIDADE, appointmentPlaceItemList.getIdCity());
//						vParams.putString(OmegaConfiguration.SEARCH_FIELD_ORIGEM_ID_BAIRRO, appointmentPlaceItemList.getIdNeighborhood());
//					}
//				} 
//				
//				if(FactoryDateClickListener.isDateButtonSet(activity, inicioDataProgramadaButton))
//				{
//					String vInicioData = HelperDate.getDataFormatadaSQL(inicioDataProgramadaButton.getText().toString());
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_INICIO_DATA_PROGRAMADA, vInicioData);	
//				}
//				
//				if(FactoryHourClickListener.isHourButtonSet(activity, inicioHoraProgramadaButton))
//				{
//					String vInicioHora = HelperDate.getHoraFormatadaSQL( inicioHoraProgramadaButton.getText().toString());
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_INICIO_HORA_PROGRAMADA, vInicioHora);	
//				}
//				
//				int vVisibilityDestinoButton = enderecoDestinoButton.getVisibility();
//				if(vVisibilityDestinoButton == View.GONE )
//				{
//					AppointmentPlaceItemList appointmentPlaceItemList = containerEnderecoDestino.getAppointmentPlaceItemList();
//					if(appointmentPlaceItemList != null){
//						vParams.putString(OmegaConfiguration.SEARCH_FIELD_DESTINO_LOGRADOURO, appointmentPlaceItemList.getAddress());
//						vParams.putString(OmegaConfiguration.SEARCH_FIELD_DESTINO_NUMERO, appointmentPlaceItemList.getNumber());
//						vParams.putString(OmegaConfiguration.SEARCH_FIELD_DESTINO_COMPLEMENTO, appointmentPlaceItemList.getComplemento());
//						vParams.putString(OmegaConfiguration.SEARCH_FIELD_DESTINO_ID_CIDADE, appointmentPlaceItemList.getIdCity());
//						vParams.putString(OmegaConfiguration.SEARCH_FIELD_DESTINO_ID_BAIRRO, appointmentPlaceItemList.getIdNeighborhood());
//					}
//
//				}
//				
//				if(FactoryDateClickListener.isDateButtonSet(activity, dataCadastroButton))
//				{
//					String vDataCadastro = HelperDate.getDataFormatadaSQL(dataCadastroButton.getText().toString());
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_DATA_CADASTRO, vDataCadastro);	
//				}
//				
//				if(FactoryHourClickListener.isHourButtonSet(activity, horaCadastroButton))
//				{
//					String vInicioHora = HelperDate.getHoraFormatadaSQL( horaCadastroButton.getText().toString());
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_HORA_CADASTRO, vInicioHora);	
//				}
//				
//				if(FactoryDateClickListener.isDateButtonSet(activity, inicioDataButton))
//				{
//					String vData = HelperDate.getDataFormatadaSQL(inicioDataButton.getText().toString());
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_INICIO_DATA, vData);	
//				}
//				
//				if(FactoryHourClickListener.isHourButtonSet(activity, inicioHoraButton))
//				{
//					String vInicioHora = HelperDate.getHoraFormatadaSQL( inicioHoraButton.getText().toString());
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_INICIO_HORA, vInicioHora);
//						
//				}
//				
//				if(FactoryDateClickListener.isDateButtonSet(activity, fimDataButton))
//				{
//					String vData = HelperDate.getDataFormatadaSQL(fimDataButton.getText().toString());
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_FIM_DATA, vData);
//				}
//				
//				if(FactoryHourClickListener.isHourButtonSet(activity, fimHoraButton))
//				{
//					String vHora = HelperDate.getDataFormatadaSQL(fimHoraButton.getText().toString());
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_FIM_HORA, vHora);
//				}
//				
//				if(FactoryDateClickListener.isDateButtonSet(activity, dataExibirButton))
//				{
//					String vData = HelperDate.getDataFormatadaSQL(dataExibirButton.getText().toString());
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_DATA_EXIBIR, vData);
//				}
//				
//				if(!descricaoEditText.getText().toString().equals(""))
//				{
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_DESCRICAO, descricaoEditText.getText().toString());	
//				}
//				
//				if(!tituloEditText.getText().toString().equals(""))
//				{
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_TITULO, tituloEditText.getText().toString());	
//				}
//
//				String selectedEstadoTarefa = String.valueOf(estadoTarefaSpinner.getSelectedItemId());
//				if(selectedEstadoTarefa.compareTo(OmegaConfiguration.UNEXISTENT_ID_IN_DB) != 0)
//				{
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_ESTADO_TAREFA,  selectedEstadoTarefa);
//				}
//				
//				String selectedCriadoPeloUsuario = String.valueOf(criadoPeloUsuarioSpinner.getSelectedItemId());
//				if(selectedCriadoPeloUsuario.compareTo(OmegaConfiguration.UNEXISTENT_ID_IN_DB) != 0)
//				{
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_CRIADO_PELO_USUARIO,  selectedCriadoPeloUsuario);
//				}
//				
//				String selectedTipoTarefa = String.valueOf(tipoTarefaSpinner.getSelectedItemId());
//				if(selectedTipoTarefa.compareTo(OmegaConfiguration.UNEXISTENT_ID_IN_DB) != 0)
//				{
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_TIPO_TAREFA,  selectedTipoTarefa);
//				}
//				
//				String selectedIdentificadorTipoTarefa = String.valueOf(identificadorTipoTarefaSpinner.getSelectedItemId());
//				if(selectedIdentificadorTipoTarefa.compareTo(OmegaConfiguration.UNEXISTENT_ID_IN_DB) != 0)
//				{
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_TAREFA,  selectedIdentificadorTipoTarefa);
//				}
//				
//				String selectedIdentificadorTipoDestinoEndereco = String.valueOf(identificadorTipoDestinoEnderecoSpinner.getSelectedItemId());
//				if(selectedIdentificadorTipoDestinoEndereco.compareTo(OmegaConfiguration.UNEXISTENT_ID_IN_DB) != 0)
//				{
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_ENDERECO_DESTINO,  selectedIdentificadorTipoDestinoEndereco);
//				}
//				
//				String selectedTipoDestinoEndereco = String.valueOf(tipoDestinoEnderecoSpinner.getSelectedItemId());
//				if(selectedTipoDestinoEndereco.compareTo(OmegaConfiguration.UNEXISTENT_ID_IN_DB) != 0)
//				{
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_TIPO_ENDERECO_DESTINO,  selectedTipoDestinoEndereco);
//				}
//				
//				String selectedIdentificadorTipoOrigemEndereco = String.valueOf(identificadorTipoOrigemEnderecoSpinner.getSelectedItemId());
//				if(selectedIdentificadorTipoOrigemEndereco.compareTo(OmegaConfiguration.UNEXISTENT_ID_IN_DB) != 0)
//				{
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_ENDERECO_ORIGEM,  selectedIdentificadorTipoOrigemEndereco);
//				}
//				
//				String selectedTipoOrigemEndereco = String.valueOf(tipoOrigemEnderecoSpinner.getSelectedItemId());
//				if(selectedTipoOrigemEndereco.compareTo(OmegaConfiguration.UNEXISTENT_ID_IN_DB) != 0)
//				{
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_TIPO_ENDERECO_ORIGEM,  selectedTipoOrigemEndereco);
//				}
//				
//				intent.putExtras(vParams);
//				
//				startActivity(intent);
//				return true;
//			}
//			catch(Exception e)
//			{
//				//if(e != null) Log.e(TAG, e.getMessage());
//				//else Log.e(TAG, "Error desconhecido");
//			}
//			return false;
//		}
//
//		@Override
//		protected void onPostExecute(Boolean result) {
//			super.onPostExecute(result);
//			controlerProgressDialog.dismissProgressDialog();
//
//		}
//	}
//
//	
//	//---------------------------------------------------------------
//	//------------------ Methods related to planos ------------------
//	//---------------------------------------------------------------
//	private class SearchButtonListener implements OnClickListener
//	{
//
//		public void onClick(View v) {
//			onSearchButtonClicked();
//		}
//
//	}	
//	
//
//	public class LoadEnderecoHandler extends Handler{
//		
//		
//		
//		@Override
//        public void handleMessage(Message msg)
//        {	
//			
//			try{
//			
//			
//	        String strTipo = null;
//	        if(msg.what == DESTINO)
//	        	strTipo = String.valueOf(tipoDestinoEnderecoSpinner.getSelectedItemId());
//	        else if(msg.what == ORIGEM)
//	        	strTipo = String.valueOf(tipoOrigemEnderecoSpinner.getSelectedItemId());
//			if(!strTipo.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
//			{
//				
//				EXTDAOTarefa.TIPO_ENDERECO vTipoTarefa = EXTDAOTarefa.getTipoEnderecoById(Integer.valueOf(strTipo));
//				String destinoEndereco = null;
//				if(msg.what == DESTINO)
//					destinoEndereco = String.valueOf(identificadorTipoDestinoEnderecoSpinner.getSelectedItemId());
//				else if(msg.what == ORIGEM)
//					destinoEndereco = String.valueOf(identificadorTipoOrigemEnderecoSpinner.getSelectedItemId());
//				if(!destinoEndereco.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB)){
//					
//					String vIdBairro = null;
//					String vIdCidade = null;
//					String vIdEstado = null;
//					String vIdPais = null;
//					String vNomeBairro = null;
//					String vNomeCidade = null;
//					String vNomeEstado = null;
//					String vNomePais = null;
//					String vLogradouro = null;
//					String vComplemento = null;
//					String vNumero = null;
//					Table vObj = null; 
//					switch (vTipoTarefa) {
//					case EMPRESA:
//						
//						vObj = new EXTDAOEmpresa(db);
//						vObj.setAttrValue(EXTDAOEmpresa.ID, destinoEndereco);
//						
//						break;
//					case PESSOA:
//						vObj = new EXTDAOPessoa(db);
//						vObj.setAttrValue(EXTDAOPessoa.ID, destinoEndereco);
//						break;
//					}
//					if(vObj != null && vObj.select()){
//						vIdBairro = vObj.getStrValueOfAttribute(Table.BAIRRO_ID_INT_UNIVERSAL);
//						if(vIdBairro != null){
//							EXTDAOBairro vObjBairro = new EXTDAOBairro(db);
//							vNomeBairro = vObjBairro.getStrValueOfAttribute(EXTDAOBairro.NOME);
//						}
//						vIdCidade = vObj.getStrValueOfAttribute(Table.CIDADE_ID_INT_UNIVERSAL);
//						if(vIdCidade != null){
//							EXTDAOCidade vObjCidade = new EXTDAOCidade(db);
//							vNomeCidade = vObjCidade.getStrValueOfAttribute(EXTDAOCidade.NOME);
//						}
//						vIdEstado = vObj.getStrValueOfAttribute(Table.ESTADO_ID_INT_UNIVERSAL);
//						if(vIdEstado != null){
//							EXTDAOUf vObjUf = new EXTDAOUf(db);
//							vNomeEstado = vObjUf.getStrValueOfAttribute(EXTDAOUf.NOME);
//						}
//						vIdPais = vObj.getStrValueOfAttribute(Table.PAIS_ID_INT_UNIVERSAL);
//						if(vIdPais != null){
//							EXTDAOPais vObjPais = new EXTDAOPais(db);
//							vNomePais = vObjPais.getStrValueOfAttribute(EXTDAOPais.NOME);
//						}
//						vLogradouro = vObj.getStrValueOfAttribute(Table.LOGRADOURO_UNIVERSAL);
//						vComplemento = vObj.getStrValueOfAttribute(Table.COMPLEMENTO_UNIVERSAL);
//						vNumero = vObj.getStrValueOfAttribute(Table.NUMERO_UNIVERSAL);
//						
//						
//						
//						Intent vIntent = new Intent();
//						vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE, vIdCidade);
//						vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO, vIdBairro);
//						vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO, vIdEstado);
//						vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PAIS, vIdPais);
//						vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE, vNomeBairro);
//						vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_BAIRRO, vNomeCidade);
//						vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO, vNomeEstado);
//						vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS, vNomePais);
//						vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO, vLogradouro);
//						vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO, vNumero);
//						vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO, vComplemento);
//						if(msg.what == DESTINO)
//							vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.endereco_destino_linearlayout);
//						else if(msg.what == ORIGEM)
//							vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.endereco_origem_linearlayout);
//						
//						onActivityResult(OmegaConfiguration.ACTIVITY_FORM_ENDERECO, OmegaConfiguration.ACTIVITY_FORM_ADD_LAYOUT_ENDERECO, vIntent) ;
//					}
//					db.close();
//				}
//				
//			} else{
//				Intent vIntent = new Intent();
//				if(msg.what == DESTINO)
//					vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.endereco_destino_linearlayout);
//				else if(msg.what == ORIGEM)
//					vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.endereco_origem_linearlayout);
//				onActivityResult(OmegaConfiguration.ACTIVITY_FORM_ENDERECO, OmegaConfiguration.ACTIVITY_FORM_DELETE_LAYOUT_ENDERECO, vIntent) ;
//			}
//			}catch(Exception ex){
//				SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//
//			}
//        }
//	}
//	public class TipoTarefaLoader extends AsyncTask<String, Integer, Boolean>
//	{
//
//		Integer dialogId = null;
//		Activity activity;
//		public TipoTarefaLoader(Activity pActivity){
//			activity = pActivity;
//		}
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			controlerProgressDialog.createProgressDialog();
//		}
//
//		@Override
//		protected Boolean doInBackground(String... params) {
//			try{
//				
//				handlerTipoTarefa.sendEmptyMessage(0);
//			}
//			catch(Exception e)
//			{
//				//if(e != null) Log.e(TAG, e.getMessage());
//				//else Log.e(TAG, "Error desconhecido");
//			}finally{
//				
//			}
//			return false;
//		}
//
//		@Override
//		protected void onPostExecute(Boolean result) {
//			super.onPostExecute(result);
//			controlerProgressDialog.dismissProgressDialog();
//			if(dialogId != null)
//				showDialog(dialogId);
//		}
//	}
//
//
//public class LoadTipoTarefaHandler extends Handler{
//		
//		@Override
//        public void handleMessage(Message msg)
//        {	
//			try{
////			EXTDAOTarefa vObj = new EXTDAOTarefa(db);
//			String strTipoTarefa = String.valueOf(tipoTarefaSpinner.getSelectedItemId());
//			if(!strTipoTarefa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
//			{
//				
//				TIPO_TAREFA vTipoTarefa = EXTDAOTarefa.getTipoTarefaById(Integer.valueOf(strTipoTarefa));
//				LinkedHashMap<String, String> vHash = null;
//				switch (vTipoTarefa) {
//				case ABERTA:
//					identificadorTipoTarefaSpinner.setVisibility(View.GONE);
//					break;
//				case USUARIO:
//					vHash = EXTDAOUsuario.getAllUsuario(db);
//					break;
//				case CATEGORIA_PERMISSAO:
//					vHash = EXTDAOCategoriaPermissao.getAllCategoriaPermissao(db);
//					break;
//				case VEICULO:
//					vHash = EXTDAOVeiculo.getAllVeiculo(db);
//					break;
//				case VEICULO_USUARIO:
//					vHash = EXTDAOVeiculoUsuario.getAllVeiculoUsuario(db);
//					break;
//				default:
//					break;
//				}
//				
//				if(vHash != null){
//					
//					identificadorTipoTarefaSpinner.setVisibility(View.VISIBLE);
//					identificadorTipoTarefaCustomSpinnerAdapter = new CustomSpinnerAdapter(
//							getApplicationContext(), 
//							vHash, 
//							R.layout.spinner_item,
//							R.string.selecione);
//					identificadorTipoTarefaSpinner.setAdapter(identificadorTipoTarefaCustomSpinnerAdapter);
//					((CustomSpinnerAdapter) identificadorTipoTarefaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);			
//				}
//				
//			} else{
//				String selectedTipoTarefa = String.valueOf(tipoTarefaSpinner.getSelectedItemId());
//				if(!selectedTipoTarefa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB) && tipoTarefaCustomSpinnerAdapter != null)
//				{
//					
//					int vPositionEmpty = tipoTarefaCustomSpinnerAdapter.getPositionFromId(OmegaConfiguration.UNEXISTENT_ID_IN_DB);
//					tipoTarefaSpinner.setSelection( vPositionEmpty);
//				}
//				
//				identificadorTipoTarefaSpinner.setVisibility(View.VISIBLE);
//				identificadorTipoTarefaCustomSpinnerAdapter = new CustomSpinnerAdapter(
//						getApplicationContext(), 
//						new LinkedHashMap<String, String>(), 
//						R.layout.spinner_item,
//						R.string.selecione);
//				
//				identificadorTipoTarefaSpinner.setAdapter(identificadorTipoTarefaCustomSpinnerAdapter);
//				((CustomSpinnerAdapter) identificadorTipoTarefaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
//			}
//			db.close();
//			}catch(Exception ex){
//				SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//			
//			}
//        }
//	}
//	
//	public class TipoTarefaOnClickListenner implements OnItemSelectedListener{
//
//		Activity activity;
//		public TipoTarefaOnClickListenner(Activity pActivity){
//			activity = pActivity;
//		}
//		
//		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
//				long arg3) {
//			
//			new TipoTarefaLoader(activity).execute();
//		}
//
//		public void onNothingSelected(AdapterView<?> arg0) {
//			
//			
//		}
//		
//	}
//	
//	public class TipoEnderecoOnClickListenner implements OnItemSelectedListener{
//
//		Activity activity;
//		boolean isOrigem;
//		public TipoEnderecoOnClickListenner(Activity pActivity, boolean pIsOrigem){
//			activity = pActivity;
//			isOrigem = pIsOrigem;
//		}
//		
//		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
//				long arg3) {
//			
//			new TipoEnderecoLoader(activity, isOrigem ).execute();
//		}
//
//		public void onNothingSelected(AdapterView<?> arg0) {
//			
//			
//		}
//		
//	}
//	public class EnderecoLoader extends AsyncTask<String, Integer, Boolean>
//	{
//
//		Integer dialogId = null;
//		Activity activity;
//		boolean isOrigem;
//		public EnderecoLoader(Activity pActivity, boolean pIsOrigem){
//			activity = pActivity;
//			isOrigem = pIsOrigem;
//		}
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			controlerProgressDialog.createProgressDialog();
//		}
//
//		@Override
//		protected Boolean doInBackground(String... params) {
//			try{
//				
//				if(isOrigem)
//					handlerEndereco.sendEmptyMessage(ORIGEM);
//				else
//					handlerEndereco.sendEmptyMessage(DESTINO);
//			}
//			catch(Exception e)
//			{
//				//if(e != null) Log.e(TAG, e.getMessage());
//				//else Log.e(TAG, "Error desconhecido");
//			}finally{
//				
//			}
//			return false;
//		}
//
//		@Override
//		protected void onPostExecute(Boolean result) {
//			super.onPostExecute(result);
//			controlerProgressDialog.dismissProgressDialog();
//			if(dialogId != null)
//				showDialog(dialogId);
//		}
//	}
//
//	public class EnderecoOnClickListenner implements OnItemSelectedListener{
//
//		Activity activity;
//		boolean isOrigem;
//		public EnderecoOnClickListenner(Activity pActivity, boolean pIsOrigem){
//			activity = pActivity;
//			isOrigem = pIsOrigem;
//		}
//		
//		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
//				long arg3) {
//			
//			new EnderecoLoader(activity, isOrigem ).execute();
//		}
//
//		public void onNothingSelected(AdapterView<?> arg0) {
//			
//			
//		}
//		
//	}
//
//	public class TipoEnderecoLoader extends AsyncTask<String, Integer, Boolean>
//	{
//
//		Integer dialogId = null;
//		Activity activity;
//		boolean isOrigem;
//		public TipoEnderecoLoader(Activity pActivity, boolean pIsOrigem){
//			activity = pActivity;
//			isOrigem = pIsOrigem;
//		}
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			controlerProgressDialog.createProgressDialog();
//		}
//
//		@Override
//		protected Boolean doInBackground(String... params) {
//			try{
//				
//				if(isOrigem)
//					handlerTipoEnderecoOrigem.sendEmptyMessage(0);
//				else
//					handlerTipoEnderecoDestino.sendEmptyMessage(0);
//			}
//			catch(Exception e)
//			{
//				//if(e != null) Log.e(TAG, e.getMessage());
//				//else Log.e(TAG, "Error desconhecido");
//			}finally{
//				
//			}
//			return false;
//		}
//
//		@Override
//		protected void onPostExecute(Boolean result) {
//			super.onPostExecute(result);
//			controlerProgressDialog.dismissProgressDialog();
//			if(dialogId != null)
//				showDialog(dialogId);
//		}
//	}
//	
//	
//	public class LoadTipoEnderecoOrigemHandler extends Handler{
//		
//		
//		public LoadTipoEnderecoOrigemHandler(){
//			
//		}
//		@Override
//        public void handleMessage(Message msg)
//        {	
//			
//	        String strTipo = String.valueOf(tipoOrigemEnderecoSpinner.getSelectedItemId());
//			if(!strTipo.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
//			{
//				
//				EXTDAOTarefa.TIPO_ENDERECO vTipoTarefa = EXTDAOTarefa.getTipoEnderecoById(Integer.valueOf(strTipo));
//				LinkedHashMap<String, String> vHash = null;
//				switch (vTipoTarefa) {
//				case EMPRESA:
//					vHash = EXTDAOEmpresa.getAllEmpresa(db);
//					break;
//				case PESSOA:
//					vHash = EXTDAOPessoa.getAllPessoa(db);
//					break;
//				}
//
//				if(vHash != null){
//					identificadorTipoOrigemEnderecoSpinner.setVisibility(View.VISIBLE);
//					identificadorTipoOrigemEnderecoCustomSpinnerAdapter = new CustomSpinnerAdapter(
//							getApplicationContext(), 
//							vHash, 
//							R.layout.spinner_item,
//							R.string.selecione);
//					identificadorTipoOrigemEnderecoSpinner.setAdapter(identificadorTipoOrigemEnderecoCustomSpinnerAdapter);
//					((CustomSpinnerAdapter) identificadorTipoOrigemEnderecoSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);			
//				}
//				
//			} else{
//				String selectedTipoTarefa = String.valueOf(identificadorTipoOrigemEnderecoSpinner.getSelectedItemId());
//				if(!selectedTipoTarefa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB) && identificadorTipoOrigemEnderecoCustomSpinnerAdapter != null)
//				{	
//					int vPositionEmpty = identificadorTipoOrigemEnderecoCustomSpinnerAdapter.getPositionFromId(OmegaConfiguration.UNEXISTENT_ID_IN_DB);
//					identificadorTipoOrigemEnderecoSpinner.setSelection( vPositionEmpty);
//				}
//				
//				identificadorTipoOrigemEnderecoSpinner.setVisibility(View.VISIBLE);
//				
//				identificadorTipoOrigemEnderecoCustomSpinnerAdapter = new CustomSpinnerAdapter(
//						getApplicationContext(), 
//						new LinkedHashMap<String, String>(), 
//						R.layout.spinner_item,
//						R.string.selecione);
//				identificadorTipoOrigemEnderecoSpinner.setAdapter(identificadorTipoOrigemEnderecoCustomSpinnerAdapter);
//				((CustomSpinnerAdapter) identificadorTipoOrigemEnderecoSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
//			}
//			db.close();
//        }
//	}
//	
//
//	public class LoadTipoEnderecoDestinoHandler extends Handler{
//	
//		
//		public LoadTipoEnderecoDestinoHandler(){
//			
//		}
//		@Override
//        public void handleMessage(Message msg)
//        {	
//			
//	        String strTipo = String.valueOf(tipoDestinoEnderecoSpinner.getSelectedItemId());
//			if(!strTipo.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
//			{
//				
//				EXTDAOTarefa.TIPO_ENDERECO vTipoTarefa = EXTDAOTarefa.getTipoEnderecoById(Integer.valueOf(strTipo));
//				LinkedHashMap<String, String> vHash = null;
//				switch (vTipoTarefa) {
//				case EMPRESA:
//					vHash = EXTDAOEmpresa.getAllEmpresa(db);
//					break;
//				case PESSOA:
//					vHash = EXTDAOUsuario.getAllUsuario(db);
//					break;
//				}
//
//				if(vHash != null){
//					identificadorTipoDestinoEnderecoSpinner.setVisibility(View.VISIBLE);
//					
//					identificadorTipoDestinoEnderecoCustomSpinnerAdapter = new CustomSpinnerAdapter(
//							getApplicationContext(), 
//							vHash, 
//							R.layout.spinner_item,
//							R.string.selecione);
//					identificadorTipoDestinoEnderecoSpinner.setAdapter(identificadorTipoDestinoEnderecoCustomSpinnerAdapter);
//					((CustomSpinnerAdapter) identificadorTipoDestinoEnderecoSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);			
//				}
//				
//			} else{
//				String selectedTipoTarefa = String.valueOf(identificadorTipoDestinoEnderecoSpinner.getSelectedItemId());
//				if(!selectedTipoTarefa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB) && identificadorTipoDestinoEnderecoCustomSpinnerAdapter != null)
//				{
//					int vPositionEmpty = identificadorTipoDestinoEnderecoCustomSpinnerAdapter.getPositionFromId(OmegaConfiguration.UNEXISTENT_ID_IN_DB);
//					identificadorTipoDestinoEnderecoSpinner.setSelection( vPositionEmpty);
//				}
//				
//				identificadorTipoDestinoEnderecoSpinner.setVisibility(View.VISIBLE);
//				identificadorTipoDestinoEnderecoCustomSpinnerAdapter = new CustomSpinnerAdapter(
//						getApplicationContext(), 
//						new LinkedHashMap<String, String>(), 
//						R.layout.spinner_item,
//						R.string.selecione);
//				identificadorTipoDestinoEnderecoSpinner.setAdapter(identificadorTipoDestinoEnderecoCustomSpinnerAdapter);
//				((CustomSpinnerAdapter) identificadorTipoDestinoEnderecoSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
//			}
//			db.close();
//        }
//	}
//
//}
//
//
