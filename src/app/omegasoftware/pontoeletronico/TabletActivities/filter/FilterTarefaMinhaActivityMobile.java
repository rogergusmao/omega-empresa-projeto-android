package app.omegasoftware.pontoeletronico.TabletActivities.filter;

//public class FilterTarefaMinhaActivityMobile extends OmegaRegularActivity {
//
//	//Constants
//	public static final String TAG = "FilterMinhasTarefasActivityMobile";
//	
//	public enum TIPO_FILTRO_MINHA_TAREFA{
//		REALIZADAS,
//		NAO_REALIZADAS,
//		EM_EXECUCAO,
//		DO_DIA
//	}
//	
//	TIPO_FILTRO_MINHA_TAREFA tipoFiltroMinhasTarefa;
//	//Search button
//	private Button searchButton;
//	
//	private FactoryDateClickListener factoryDateClickListener;
//	private FactoryHourClickListener factoryHourClickListener;
//	
//	private final int FORM_ERROR_DIALOG_TAREFA_INICIAL = 5;
//	private final int FORM_ERROR_DIALOG_TAREFA_FINAL = 6;
//	private final int FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_QUE_DATA_FIM = 7;
//	
//	
//	private ContainerClickListenerDate dataInicioClickListener;
//	private ContainerClickListenerDate dataFimClickListener;
//	
//	private Button inicioDataProgramadaButton;
//	private Button fimDataProgramadaButton;
//	
//	private String estadoTarefaId;
//	
//	private DatabasePontoEletronico db=null;
//
//	private Typeface customTypeFace;
//	
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.filter_tarefa_minha_activity_mobile_layout);
//
//		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
//		if(savedInstanceState != null){
//			if(savedInstanceState.containsKey(OmegaConfiguration.SEARCH_FIELD_ESTADO_TAREFA))
//			estadoTarefaId = savedInstanceState.getString(OmegaConfiguration.SEARCH_FIELD_ESTADO_TAREFA);
//			
//		}
//		
//		new CustomDataLoader(this, EXTDAOTarefa.TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();
//
//	}
//
//	@Override
//	public boolean loadData() {
//
//		return true;
//
//	}
//
//	public void formatarStyle()
//	{
//		
//		//Search mode buttons
//				//Cidade em que deseja ser atendido:
//		
//		((TextView) findViewById(R.id.data_inicio_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.data_fim_textview)).setTypeface(this.customTypeFace);
//		//Botuo buscar:
//		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);
//		
//	}
//	
//	@Override
//	public void initializeComponents() {
//
//		try {
//			this.db = new DatabasePontoEletronico(this);
//		} catch (OmegaDatabaseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}		
//
//		factoryDateClickListener = new FactoryDateClickListener(this);
//		factoryHourClickListener = new FactoryHourClickListener(this);
//		this.inicioDataProgramadaButton = (Button) findViewById(R.id.data_inicio_button);
//		this.fimDataProgramadaButton = (Button) findViewById(R.id.data_fim_button);
//		dataInicioClickListener = factoryDateClickListener.setDateClickListener(inicioDataProgramadaButton);
//		dataFimClickListener = factoryDateClickListener.setDateClickListener(fimDataProgramadaButton);
//		
//		this.searchButton = (Button) findViewById(R.id.buscar_button);
//		this.searchButton.setOnClickListener(new SearchButtonListener());
//		//	Attach search button to its listener
//		this.formatarStyle();		//Attach search button to its listener
//		this.db.close();
//	}
//
//	
//	@Override
//	protected void onPrepareDialog(int id, Dialog dialog) {
//        switch (id) {
//           
//            case FactoryDateClickListener.DATE_DIALOG_ID:
//            	factoryDateClickListener.onPrepareDialog(id, dialog);
//            	break;
//            case FactoryHourClickListener.TIME_DIALOG_ID:
//            	factoryHourClickListener.onPrepareDialog(id, dialog);
//            	break;
//        }
//    }
//
//	@Override
//	protected Dialog onCreateDialog(int id) {
//
//		switch (id) {
//		
//		case FactoryDateClickListener.DATE_DIALOG_ID:
//			return factoryDateClickListener.onCreateDialog(id);
//		case FactoryHourClickListener.TIME_DIALOG_ID:
//        	return factoryHourClickListener.onCreateDialog(id);
//		
//		default:
//			return getErrorDialog(id);
//		}
//	}
//	
//	public Dialog getErrorDialog(int dialogType)
//	{
//
//		Dialog vDialog = new Dialog(this);
//		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		
//		vDialog.setContentView(R.layout.dialog);
//
//		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
//		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);
//
//		switch (dialogType) {
//		case FORM_ERROR_DIALOG_TAREFA_INICIAL:
//			vTextView.setText(getResources().getString(R.string.filter_tarefa_error_dialog_data_vazia_inicial));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_TAREFA_INICIAL);
//				}
//			});
//			break;
//		case FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_QUE_DATA_FIM:
//			vTextView.setText(getResources().getString(R.string.filter_tarefa_error_dialog_data_vazia_inicial_maior_final));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_QUE_DATA_FIM);
//				}
//			});
//			break;
//		case FORM_ERROR_DIALOG_TAREFA_FINAL:
//			vTextView.setText(getResources().getString(R.string.filter_tarefa_error_dialog_data_vazia_final));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_TAREFA_FINAL);
//				}
//			});
//			break;
//		default:
//			return super.onCreateDialog(dialogType);
//		}
//
//		return vDialog;
//	}
//	
//	//---------------------------------------------------------------
//	//----------------- Methods related to search action ------------
//	//---------------------------------------------------------------
//	//Handles search button click
//	private void onSearchButtonClicked()
//	{
//		new SearchButtonLoader(this).execute();
//	}
//	
//
//	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean>
//	{
//		Activity activity;
//		Intent intent ;
//		Integer dialogId = null;
//		
//		public SearchButtonLoader(Activity pActivity){
//			activity = pActivity;
//			intent = new Intent(activity, ListTarefaMinhaActivityMobile.class);
//		}
//		
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			controlerProgressDialog.createProgressDialog();
//		}
//
//		@Override
//		protected Boolean doInBackground(String... params) {
//			try{	
//				Bundle vParams = new Bundle();
//
//				//First we check the obligatory fields:
//				//Plano, Especialidade, Cidade
//				
//				if(FactoryDateClickListener.isDateButtonSet(activity, inicioDataProgramadaButton))
//				{
//					String vInicioData = HelperDate.getDataFormatadaSQL(inicioDataProgramadaButton.getText().toString());
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_INICIO_DATA_PROGRAMADA, vInicioData);	
//				} else{
//					dialogId = FORM_ERROR_DIALOG_TAREFA_INICIAL;
//					return false;
//				}
//				
//				if(FactoryDateClickListener.isDateButtonSet(activity, fimDataProgramadaButton))
//				{
//					if(FactoryDateClickListener.isDateButtonSet(activity, inicioDataProgramadaButton)){
//						Date vDateFim = dataFimClickListener.getDate();
//						Date vDateInicio = dataInicioClickListener.getDate();
//						if(vDateInicio.compareTo(vDateFim) > 0){
//							dialogId = FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_QUE_DATA_FIM;
//							return false;
//						}
//						vParams.putString(OmegaConfiguration.SEARCH_FIELD_FIM_DATA_PROGRAMADA, HelperDate.getDataFormatadaSQL(fimDataProgramadaButton.getText().toString()));
//					}
//					
//				}
//				
//				vParams.putString(OmegaConfiguration.SEARCH_FIELD_TIPO_TAREFA, String.valueOf(EXTDAOTarefa.getIdTipoTarefa(TIPO_TAREFA.USUARIO)));
//				vParams.putString(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_TAREFA, OmegaSecurity.getIdUsuario());
//				
//				if(estadoTarefaId != null && estadoTarefaId.length() > 0)
//					vParams.putString(OmegaConfiguration.SEARCH_FIELD_ESTADO_TAREFA, String.valueOf(estadoTarefaId));
//				
//				intent.putExtras(vParams);
//				
//				startActivity(intent);
//				return true;
//			}
//			catch(Exception e)
//			{
//				//if(e != null) Log.e(TAG, e.getMessage());
//				//else Log.e(TAG, "Error desconhecido");
//			}
//			return false;
//		}
//
//		@Override
//		protected void onPostExecute(Boolean result) {
//			super.onPostExecute(result);
//			controlerProgressDialog.dismissProgressDialog();
//			if(dialogId != null)
//				showDialog(dialogId);
//
//		}
//	}
//
//	
//	//---------------------------------------------------------------
//	//------------------ Methods related to planos ------------------
//	//---------------------------------------------------------------
//	private class SearchButtonListener implements OnClickListener
//	{
//
//		public void onClick(View v) {
//			onSearchButtonClicked();
//		}
//
//	}	
//
//}
//
//
