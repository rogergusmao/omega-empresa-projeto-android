package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListTipoEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoEmpresa;

public class FilterTipoEmpresaActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "FilterTipoEmpresaActivityMobile";
	
	private static final String TABELAS_RELACIONADAS[] = new String[]{EXTDAOTipoEmpresa.NAME};

	//Search button
	private Button buscarButton;
	
	//Spinners
	private DatabasePontoEletronico db=null;
	
	private AutoCompleteTextView tipoEmpresaAutoCompletEditText;
	TableAutoCompleteTextView tipoEmpresaContainerAutoComplete = null;
	
	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_tipo_empresa_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		//
		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();

	}

	
	@Override
	public boolean loadData() {
		return true;
	}


	public void formatarStyle()
	{
		//Search mode buttons
		((TextView) findViewById(R.id.tipo_empresa_autocompletetextview)).setTypeface(this.customTypeFace);

		//Botuo cadastrar:
		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);
		

	}

	@Override
	public void initializeComponents() {
		
		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);		

//		this.tipoEmpresaAutoCompletEditText = (AutoCompleteTextView) findViewById(R.id.tipoEmpresa_autocompletetextview);
		 
//		this.tipoEmpresaContainerAutoComplete = new TableAutoCompleteTextView(vObjUf);
//		ArrayAdapter<String> tipoEmpresaAdapter = new ArrayAdapter<String>(
//				this,
//				R.layout.spinner_item,
//				tipoEmpresaContainerAutoComplete.getVetorStrId());
//		tipoEmpresaAutoCompletEditText.setAdapter(tipoEmpresaAdapter);
		EXTDAOTipoEmpresa vObjTipoEmpresa = new EXTDAOTipoEmpresa(this.db);
		this.tipoEmpresaAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.tipo_empresa_autocompletetextview);
		this.tipoEmpresaContainerAutoComplete = new TableAutoCompleteTextView(this, vObjTipoEmpresa, tipoEmpresaAutoCompletEditText);
		new MaskUpperCaseTextWatcher(tipoEmpresaAutoCompletEditText, 100);
		
		
		//Attach search button to its listener
		this.buscarButton = (Button) findViewById(R.id.buscar_button);
		this.buscarButton.setOnClickListener(new SearchButtonListener());


		this.formatarStyle();
		
		this.db.close();


	}

	@Override
	public void finish(){
		try{
			
			this.db.close();
			//			Se for cadastro
			super.finish();			
		}catch(Exception ex){

		}
	}


	private void onSearchButtonClicked()
	{
		new SearchButtonLoader(this).execute();
	}
	

	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean>
	{
		Activity activity;
		Intent intent ;
		
		public SearchButtonLoader(Activity pActivity){
			activity = pActivity;
			intent = new Intent(activity, ListTipoEmpresaActivityMobile.class);
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{	
				String nameTipoEmpresa = "";
				nameTipoEmpresa = tipoEmpresaAutoCompletEditText.getText().toString();
				if( nameTipoEmpresa.length() > 0)
				{
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_TIPO_EMPRESA, nameTipoEmpresa);
				}

				startActivity(intent);
				return true;
			}
			catch(Exception e)
			{
				//if(e != null) Log.e(TAG, e.getMessage());
				//else Log.e(TAG, "Error desconhecido");
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			controlerProgressDialog.dismissProgressDialog();
		}
	}


	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------



	private class SearchButtonListener implements OnClickListener
	{
		
		public SearchButtonListener(){
			
		}

		public void onClick(View v) {
			onSearchButtonClicked();

		}

	}	
	
//
//	private class DeleteViewEnderecoButtonListener implements OnClickListener
//	{
//		View view= null;
//		
//		public DeleteViewEnderecoButtonListener(View pView){
//			view = pView;
//		}
//		
//		public void onClick(View v) {
//			LinearLayout linearLayoutEmpresaFuncionario = (LinearLayout) findViewById(R.id.linearlayout_endereco);
//			linearLayoutEmpresaFuncionario.removeView(view);
//		}
//	}
}


