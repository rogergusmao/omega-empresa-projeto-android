package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskLowerCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCategoriaPermissao;

public class FilterUsuarioActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "FilterUsuarioActivityMobile";
//	private static final String TABELAS_RELACIONADAS[] = new String[]{
//		EXTDAOUsuario.NAME, 
//		EXTDAOUsuarioCorporacao.NAME, 
//		EXTDAOUsuarioTipo.NAME,
//		EXTDAOUsuarioTipoCorporacao.NAME, 
//		EXTDAOCategoriaPermissao.NAME, 
//		EXTDAOPermissaoCategoriaPermissao.NAME,
//		EXTDAOUsuarioCategoriaPermissao.NAME};
	//Search button
	private Button searchButton;

	private DatabasePontoEletronico db=null;

	//EditText
	private EditText nomeEditText;
	private EditText emailEditText;
	
	private Spinner categoriaPermissaoSpinner;
	

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_usuario_activity_mobile_layout);

	

		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();

	}

	@Override
	public boolean loadData() {

		return true;

	}

	@Override
	public void initializeComponents() throws OmegaDatabaseException {
		
		this.db = new DatabasePontoEletronico(this);

		

		this.categoriaPermissaoSpinner = (Spinner) findViewById(R.id.categoria_permissao_spinner);
		CustomSpinnerAdapter customSpinnerAdaptercategoriaPermissao = new CustomSpinnerAdapter(getApplicationContext(), EXTDAOCategoriaPermissao.getAllCategoriaPermissao(db),R.layout.spinner_item,R.string.selecione);
		this.categoriaPermissaoSpinner.setAdapter(customSpinnerAdaptercategoriaPermissao);
		((CustomSpinnerAdapter) this.categoriaPermissaoSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
		
		
		this.nomeEditText = (EditText) findViewById(R.id.nome_edittext);
		new MaskUpperCaseTextWatcher(this.nomeEditText, 255);
		this.emailEditText = (EditText) findViewById(R.id.email_edittext);
		new MaskLowerCaseTextWatcher(this.emailEditText, 255);
	

		//Attach search button to its listener
		this.searchButton = (Button) findViewById(R.id.buscar_button);
		this.searchButton.setOnClickListener(new SearchButtonListener());


		this.db.close();


	}

	@Override
	public void finish(){
		try{
					
			this.db.close();
			//			Se for cadastro
			super.finish();			
		}catch(Exception ex){

		}
	}

	private void onSearchButtonClicked()
	{
		new SearchButtonLoader().execute();
	}
	

	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean>
	{
		Exception e=null;
		public SearchButtonLoader(){
			
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{	
				Bundle vParams = new Bundle();

				//First we check the obligatory fields:
				//Plano, Especialidade, Cidade
				if(!nomeEditText.getText().toString().equals(""))
				{
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_NOME_USUARIO, nomeEditText.getText().toString());	
				}
				
				if(!emailEditText.getText().toString().equals(""))
				{
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_EMAIL, emailEditText.getText().toString());	
				}
				
				String selectedCategoriaPermissao = String.valueOf(categoriaPermissaoSpinner.getSelectedItemId());
				if(!selectedCategoriaPermissao.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_CATEGORIA_PERMISSAO, selectedCategoriaPermissao);	
				}
				Intent intent = getIntent();
				intent.putExtras(vParams);
				
				setResult(RESULT_OK, intent);
				return true;
			}
			catch(Exception e)
			{
				this.e=e;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
				if(this.e!=null){
					SingletonLog.openDialogError(FilterUsuarioActivityMobile.this, this.e, SingletonLog.TIPO.PAGINA);
				}
				finish();
			} catch (Exception ex) {
				SingletonLog.openDialogError(FilterUsuarioActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}

	
	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------

	
	private class SearchButtonListener implements OnClickListener
	{

		public void onClick(View v) {
			onSearchButtonClicked();
		}

	}	


}


