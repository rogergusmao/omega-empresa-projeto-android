package app.omegasoftware.pontoeletronico.TabletActivities.filter;

import java.util.LinkedHashMap;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListVeiculoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOModelo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculoUsuario;

public class FilterVeiculoUsuarioActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "FilterVeiculoUsuarioActivityMobile";
	private static final String TABELAS_RELACIONADAS[] = new String[]{EXTDAOModelo.NAME, EXTDAOVeiculo.NAME, EXTDAOUsuario.NAME, EXTDAOVeiculoUsuario.NAME};
	//Search button
	private Button searchButton;

	//EditText
	private EditText placaEditText;
	
	private AutoCompleteTextView nomeModeloAutoCompletEditText;
	TableAutoCompleteTextView nomeModeloContainerAutoComplete = null;
	NomeModeloOnItemSelectedListener nomeModeloOnFocusChangeListener;
	//Spinners
	private Spinner anoModeloSpinner;
	CustomSpinnerAdapter anoCustomSpinnerAdapter;
	
	private Spinner emailSpinner;
	
	private DatabasePontoEletronico db=null;


	private Typeface customTypeFace;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_veiculo_usuario_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
	
		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();

	}

	@Override
	public boolean loadData() {

		return true;

	}

	public void formatarStyle()
	{
		
		//Search mode buttons
				//Cidade em que deseja ser atendido:
		((TextView) findViewById(R.id.placa_textview)).setTypeface(this.customTypeFace);
		
		//Bairro de preferuncia:
		((TextView) findViewById(R.id.modelo_nome_textview)).setTypeface(this.customTypeFace);
		
		//Especialidade:
		((TextView) findViewById(R.id.modelo_ano_textview)).setTypeface(this.customTypeFace);
		
		((TextView) findViewById(R.id.titulo_email_usuario_textview)).setTypeface(this.customTypeFace);
		
		//		
		//Botuo buscar:
		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);
		
	}
	
	@Override
	public void initializeComponents() {
		
		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.formatarStyle();
		
		EXTDAOModelo vObjModelo = new EXTDAOModelo(this.db);
		this.nomeModeloAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.nome_modelo_autocompletetextview);
		this.nomeModeloContainerAutoComplete = new TableAutoCompleteTextView(this, vObjModelo, nomeModeloAutoCompletEditText);
		new MaskUpperCaseTextWatcher(nomeModeloAutoCompletEditText, 255);
		
		
		new MaskUpperCaseTextWatcher(nomeModeloAutoCompletEditText, 255);
		nomeModeloOnFocusChangeListener = new NomeModeloOnItemSelectedListener();
		nomeModeloAutoCompletEditText.setOnFocusChangeListener(nomeModeloOnFocusChangeListener);


		anoCustomSpinnerAdapter = new CustomSpinnerAdapter(getApplicationContext(), 
				new LinkedHashMap<String, String>(), R.layout.spinner_item,R.string.form_all_string);
		this.anoModeloSpinner = (Spinner) findViewById(R.id.modelo_ano_spinner);
		this.anoModeloSpinner.setAdapter(anoCustomSpinnerAdapter);
		((CustomSpinnerAdapter) this.anoModeloSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		
		this.emailSpinner = (Spinner) findViewById(R.id.email_usuario_spinner);
		CustomSpinnerAdapter emailCustomSpinnerAdapter = new CustomSpinnerAdapter(
				getApplicationContext(), 
				this.getAllEmail(),
				R.layout.spinner_item,R.string.form_all_string, 
				false);
		this.emailSpinner.setAdapter(emailCustomSpinnerAdapter);
		((CustomSpinnerAdapter) this.emailSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
		

		this.placaEditText = (EditText) findViewById(R.id.placa_edittext);
		new MaskUpperCaseTextWatcher(this.placaEditText, 20);
		//Attach search button to its listener
		this.searchButton = (Button) findViewById(R.id.buscar_button);
		this.searchButton.setOnClickListener(new SearchButtonListener());
		this.db.close();

	}
	
	private LinkedHashMap<String, String> getAllEmail()
	{
		EXTDAOUsuario vObj = new EXTDAOUsuario(this.db);
		return vObj.getHashMapIdByDefinition(false, EXTDAOUsuario.EMAIL);
	}
	
	private void onSearchButtonClicked()
	{
		new SearchButtonLoader(this).execute();
	}

	public class SearchButtonLoader extends AsyncTask<String, Integer, Boolean>
	{
		Activity activity;
		Intent intent ;
		public SearchButtonLoader(Activity pActivity){
			activity = pActivity;
			intent = new Intent(activity, ListVeiculoUsuarioActivityMobile.class);
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{	
				Bundle vParams = new Bundle();
				if(!placaEditText.getText().toString().equals(""))
				{
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_PLACA, placaEditText.getText().toString());	
				}

				//Checking Cidade
				String selectedEmail = String.valueOf(emailSpinner.getSelectedItemId());
				if(! selectedEmail.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_EMAIL, selectedEmail);
				}
				
				//Checking Cidade
				String selectedNomeModelo = nomeModeloAutoCompletEditText.getText().toString();
				if(selectedNomeModelo.length() > 0)
				{	
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_NOME_MODELO, selectedNomeModelo);
				}
				String idAnoModelo = String.valueOf(anoModeloSpinner.getSelectedItemId());
				if(!idAnoModelo.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{	
					vParams.putString(OmegaConfiguration.SEARCH_FIELD_ANO_MODELO,  idAnoModelo);
				}
				startActivity(intent);
				return true;
			}
			catch(Exception e)
			{
				//if(e != null) Log.e(TAG, e.getMessage());
				//else Log.e(TAG, "Error desconhecido");
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			controlerProgressDialog.dismissProgressDialog();
		}
	}

	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------


	@Override
	public void finish(){
		try{
					
			this.db.close();
			//			Se for cadastro
			super.finish();			
		}catch(Exception ex){

		}
	}

	
	
	private void showAnoModeloFromNomeModelo(String pNomeModelo)
	{
		
		EXTDAOModelo objModelo = new EXTDAOModelo(this.db);
		
		this.anoModeloSpinner.setAdapter(
				new CustomSpinnerAdapter(getApplicationContext(), 
						objModelo.getDistinctHashAnoModeloOfNomeModelo(pNomeModelo),
						R.layout.spinner_item,
						R.string.form_all_string));
		((CustomSpinnerAdapter) this.anoModeloSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
	}

	
	//---------------------------------------------------------------
	//----------------------- Event listeners -----------------------
	//---------------------------------------------------------------
	
	private class SearchButtonListener implements OnClickListener
	{

		public void onClick(View v) {
			onSearchButtonClicked();
		}

	}	

	private class NomeModeloOnItemSelectedListener implements OnFocusChangeListener {

		public void onFocusChange(View view, boolean hasFocus) {
			
			if(!hasFocus){
				String vVeiculoUsuario = nomeModeloAutoCompletEditText.getText().toString();
				if(vVeiculoUsuario != null){
					showAnoModeloFromNomeModelo(vVeiculoUsuario);
				}
			}
		}          
	}

}


