package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.common.ItemList.AppointmentPlaceItemList;

public class ContainerLayoutEndereco{
	AppointmentPlaceItemList appointmentPlaceItemList ;
	View view ;
	Activity activity;
	LinearLayout linearLayoutEndereco;
	public ContainerLayoutEndereco(
			Activity pActivity, 
			AppointmentPlaceItemList appointmentPlaceItemList,
			LinearLayout pLinearLayoutEndereco){
		activity = pActivity;
		this.appointmentPlaceItemList = appointmentPlaceItemList;
		linearLayoutEndereco = pLinearLayoutEndereco;
	}
	
	
	public void setAppointmentPlace(View pView){
		view = pView;
	}
	
	
	public AppointmentPlaceItemList getAppointmentPlaceItemList(){
		return appointmentPlaceItemList;
	}
	
	public int getIdLinearLayout(){
		return linearLayoutEndereco.getId();
	}
	
	public void delete(){
//		LinearLayout linearLayoutEmpresaFuncionario = (LinearLayout) activity.findViewById(R.id.linearlayout_endereco);
		linearLayoutEndereco.removeView(view);
		if(this.appointmentPlaceItemList != null )
			this.appointmentPlaceItemList.clear();
	}
	
	
}
