package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.util.ArrayList;


import android.app.Activity;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import android.view.View;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;

public class ContainerLayoutPessoaEmpresa{
	ArrayList<String> idsPessoaEmpresa = new ArrayList<String>();
	ArrayList<String> idsEmpresa = new ArrayList<String>();
	ArrayList<String> profissoes= new ArrayList<String>();
	ArrayList<View> views= new ArrayList<View>();
	Activity activity;
	
	public ContainerLayoutPessoaEmpresa(Activity pActivity){
		activity  = pActivity;
	}
	public boolean containsRegistro(String idPessoaEmpresa){
		return  idsPessoaEmpresa.contains(idPessoaEmpresa);
	}
	
	public boolean containsTuple(String pIdEmpresa, String pIdProfissao){
		
		if(getRegistro(pIdEmpresa, pIdProfissao) == null) 
			return false;
		else 
			return true;
	}
	
	public Integer getIndiceIdPessoaEmpresa(String idPessoaEmpresa){
		for(int i = 0 ; i < idsPessoaEmpresa.size(); i++){
			if(idsPessoaEmpresa.get(i) == null) continue;
			else if(idsPessoaEmpresa.get(i).equalsIgnoreCase(idPessoaEmpresa)) return i;
		}
		return null;
	}
	
	public void updateRegistro(String idPessoaEmpresa, String idEmpresa, String profissao){
		Integer indice = getIndiceIdPessoaEmpresa(idPessoaEmpresa);
		if(indice != null){
			idsEmpresa.set(indice, idEmpresa);
			profissoes.set(indice, profissao);
		}
	}
	
	public void addListTupleInUpdate(Database db, String pIdPessoa){
		try{
			EXTDAOPessoaEmpresa vObjEmpresaFuncionarioAntigo = new EXTDAOPessoaEmpresa(db);
			vObjEmpresaFuncionarioAntigo.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, pIdPessoa);
			vObjEmpresaFuncionarioAntigo.setAttrValue(EXTDAOPessoaEmpresa.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			ArrayList<Table> registros = vObjEmpresaFuncionarioAntigo.getListTable();
			EXTDAOPessoaEmpresa objPessoaEmpresaInsert = new EXTDAOPessoaEmpresa(db);
			EXTDAOProfissao objProfissao = new EXTDAOProfissao(db);
			boolean persistenciaRegistros[] = new boolean[idsPessoaEmpresa.size()];
			
			for(int i = 0 ; i <persistenciaRegistros.length; i++){
				persistenciaRegistros[i] = true;
			}
			
			//Removendo registros que nuo existem mais
			if(registros != null && registros.size() > 0){
				for(int j = 0 ; j < registros.size(); j++){
					Table reg = registros.get(j);
					String idPessoaEmpresaReg =reg.getId(); 
					boolean encontrou = false;
					int i = 0;
					for(i = 0 ; i < idsPessoaEmpresa.size(); i++){
						if(idsPessoaEmpresa.get(i) == null) continue;
						else if(idPessoaEmpresaReg.equalsIgnoreCase(idsPessoaEmpresa.get(i))){
							encontrou = true;
							break;
						}
					}
					if(!encontrou)
						objPessoaEmpresaInsert.remove(idPessoaEmpresaReg, true);
					else {
	
						String idEmpresa =idsEmpresa.get(i);
						String profissao =profissoes.get(i);
						String idProfissao = objProfissao.adicionarRegistroSeInexistente(profissao);
						String idEmpresaReg=  reg.getStrValueOfAttribute(EXTDAOPessoaEmpresa.EMPRESA_ID_INT);
						String idProfissaoReg=  reg.getStrValueOfAttribute(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT);
						//Verifica se houve modificacao
						if(!idProfissao.equalsIgnoreCase(idProfissaoReg)
								|| !idEmpresa.equalsIgnoreCase(idEmpresaReg) ){
							reg.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, pIdPessoa);
							reg.setAttrValue(EXTDAOPessoaEmpresa.EMPRESA_ID_INT, idEmpresa);
							reg.setAttrValue(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT, idProfissao);
							reg.formatToSQLite();
							reg.update(true);
						}
							
					}
				}	
			}	
			if(idsPessoaEmpresa.size() == 0){
				addProfissaoIndefinida(db);
			}
			for(int i = 0 ; i < idsPessoaEmpresa.size(); i++){
				if(idsPessoaEmpresa.get(i)== null){
					String idEmpresa = idsEmpresa.get(i);
					String profissao = profissoes.get(i);
					
					String idProfissao = objProfissao.adicionarRegistroSeInexistente(profissao);
					objPessoaEmpresaInsert.clearData();
					
					objPessoaEmpresaInsert.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, pIdPessoa);
					objPessoaEmpresaInsert.setAttrValue(EXTDAOPessoaEmpresa.EMPRESA_ID_INT, idEmpresa);
					objPessoaEmpresaInsert.setAttrValue(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT, idProfissao);
					objPessoaEmpresaInsert.formatToSQLite();
					objPessoaEmpresaInsert.insert(true);
				} 
			}
		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
		}
	}
	public void addProfissaoIndefinida(Database db)
	{

		String  token =db.getContext().getString(R.string.profissao_indefinida).toUpperCase();
		Long idEmpresaIndefinida = EXTDAOEmpresa.getIdEmpresaIndefinida(db);
		Long idProfissaoIndefinida = EXTDAOProfissao.getProfissaoindefinida(db);
		if(idEmpresaIndefinida != null && idProfissaoIndefinida != null)
			add(null, idEmpresaIndefinida.toString(), token, null);
		
	}
	public void addListTupleInInsert(Database db, String idPessoa){
		
		ArrayList<String> listNomeProfissao =getListProfissao();
		if(listNomeProfissao.size() == 0){
			addProfissaoIndefinida(db);
			listNomeProfissao =getListProfissao();
		}
		EXTDAOPessoaEmpresa objPessoaEmpresa = new EXTDAOPessoaEmpresa(db);
		ArrayList<String> vListEmpresa = getListEmpresa();
		
		
			
		EXTDAOProfissao vObjProfissao = new EXTDAOProfissao(db);
		
		TableAutoCompleteTextView profissaoContainerAutoComplete = new TableAutoCompleteTextView(activity, vObjProfissao, null);
		EXTDAOProfissao tuplaProfissao = new EXTDAOProfissao(db);

		for (int i = 0; i < listNomeProfissao.size(); i++) {
			String idEmpresa = vListEmpresa.get(i);
			String nomeProfissao = listNomeProfissao.get(i);
			tuplaProfissao.clearData();
			tuplaProfissao.setAttrValue(EXTDAOProfissao.NOME, nomeProfissao.toUpperCase());
			String idProfissao = profissaoContainerAutoComplete.addTupleIfNecessay(tuplaProfissao);
			if(idProfissao != null ){
			
				objPessoaEmpresa.clearData();
				objPessoaEmpresa.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, idPessoa);
				objPessoaEmpresa.setAttrValue(EXTDAOPessoaEmpresa.EMPRESA_ID_INT, idEmpresa);
				objPessoaEmpresa.setAttrValue(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT, idProfissao);
				objPessoaEmpresa.formatToSQLite();
				objPessoaEmpresa.insert(true);
			}
		}
	
	}
	
	public void clear(){
		idsPessoaEmpresa.clear();
		idsEmpresa.clear();
		profissoes.clear();
		
		for(int i = 0 ; i < views.size(); i ++){
			View vView = views.get(i);
			LinearLayout linearLayoutEmpresaFuncionario = (LinearLayout) activity.findViewById(R.id.linearlayout_empresa_funcionario);
			linearLayoutEmpresaFuncionario.removeView(vView);	
		}
	}
	
	public ArrayList<String> getListPessoaEmpresa(){
		return idsPessoaEmpresa;
	}
	
	public ArrayList<String> getListEmpresa(){
		return idsEmpresa;
	}
	
	public ArrayList<String> getListProfissao(){
		return profissoes;
	}
	
	public Integer getRegistro(String pIdEmpresa, String pNomeProfissao){
		int i =  0;
		for (String vIdEmpresa : idsEmpresa) {
			if(vIdEmpresa.compareTo(pIdEmpresa) == 0 ) {
				for (String vIdProfissao : profissoes) {
					if(vIdProfissao.compareTo(pNomeProfissao) == 0 ) {
						return i;
					} 
				}		
			} 
			i += 1;
		}	
		return null;
	}
	
	public void add(String idPessoaEmpresa, String pIdEmpresa, String pProfissao, View pView){
		if(!this.containsTuple(pIdEmpresa, pProfissao)){
			idsPessoaEmpresa.add(idPessoaEmpresa);
			idsEmpresa.add(pIdEmpresa);
			profissoes.add(pProfissao);
			views.add(pView);
		}
	}
	public View getViewDoIdPessoaEmpresa(String idPessoaEmpresa){
		for(int i = 0 ; i < idsPessoaEmpresa.size(); i++){
			if(idsPessoaEmpresa.get(i) == null) continue;
			else if(idsPessoaEmpresa.get(i).equalsIgnoreCase(idPessoaEmpresa))
				return views.get(i);
		}
		return null;
	}
	
	public void deletarRegistro(String pIdPessoaEmpresa){
		Integer i = getIndiceIdPessoaEmpresa(pIdPessoaEmpresa);
		if(i != null){
			idsPessoaEmpresa.remove((int)i);
			idsEmpresa.remove((int)i);
			profissoes.remove((int)i);
			View vView = views.get((int)i);
			LinearLayout linearLayoutEmpresaFuncionario = (LinearLayout) activity.findViewById(R.id.linearlayout_empresa_funcionario);
			linearLayoutEmpresaFuncionario.removeView(vView);
			views.remove((int)i);
		}
	}
	
	public void deletarRegistro(String pIdEmpresa, String pNomeProfissao){
		Integer i = getRegistro(pIdEmpresa, pNomeProfissao);
		if(i != null){
			idsPessoaEmpresa.remove((int)i);
			idsEmpresa.remove((int)i);
			profissoes.remove((int)i);
			View vView = views.get((int)i);
			LinearLayout linearLayoutEmpresaFuncionario = (LinearLayout) activity.findViewById(R.id.linearlayout_empresa_funcionario);
			linearLayoutEmpresaFuncionario.removeView(vView);
			views.remove((int)i);
		}
	}

	public int getUltimoIdEmpresa(){
		return idsEmpresa.size();
	}

}