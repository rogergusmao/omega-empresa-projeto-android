package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.util.ArrayList;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORedeEmpresa;

public class ContainerLayoutRedeEmpresa{
	ArrayList<String> idsRedeEmpresa = new ArrayList<String>();
	ArrayList<String> idsEmpresa = new ArrayList<String>();
	ArrayList<String> idsRedeEmpresaRemovidos = new ArrayList<String>();
	ArrayList<View> listView= new ArrayList<View>();
	Activity activity;
	
	public ContainerLayoutRedeEmpresa(Activity pActivity){
		activity  = pActivity;
	}
	public boolean containsTuple(String pIdEmpresa){
		
		if(getIndexTuple(pIdEmpresa) == null) 
			return false;
		else 
			return true;
	}
	
	public void addListTupleInUpdate(Database db, String pIdRede){
		try{
			EXTDAORedeEmpresa vObjRedeEmpresaAntigo = new EXTDAORedeEmpresa(db);
			for(int i = 0 ; i < idsRedeEmpresa.size(); i++){
				if(idsRedeEmpresa.get(i) == null){
					vObjRedeEmpresaAntigo.setId(null);
					vObjRedeEmpresaAntigo.setAttrValue(EXTDAORedeEmpresa.REDE_ID_INT, pIdRede);
					vObjRedeEmpresaAntigo.setAttrValue(EXTDAORedeEmpresa.EMPRESA_ID_INT, idsEmpresa.get(i));
					vObjRedeEmpresaAntigo.formatToSQLite();
					vObjRedeEmpresaAntigo.insert(true);
				} else {
					vObjRedeEmpresaAntigo.select(idsRedeEmpresa.get(i));
					String idEmpresaAtual = vObjRedeEmpresaAntigo.getStrValueOfAttribute(EXTDAORedeEmpresa.EMPRESA_ID_INT);
					if(idsEmpresa.get(i).compareTo(idEmpresaAtual)!= 0){
						vObjRedeEmpresaAntigo.select(idsRedeEmpresa.get(i));
						vObjRedeEmpresaAntigo.setAttrValue(EXTDAORedeEmpresa.EMPRESA_ID_INT, idsEmpresa.get(i));
						vObjRedeEmpresaAntigo.formatToSQLite();
						vObjRedeEmpresaAntigo.update(true);
					}
				}
			}
			for(int i = 0 ; i < idsRedeEmpresaRemovidos.size(); i++){
				vObjRedeEmpresaAntigo.remove(idsRedeEmpresaRemovidos.get(i), true);
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
	}
	
	public void addListTupleInInsert(Database db, String pIdRede){
		
		EXTDAORedeEmpresa vObjRedeEmpresa = new EXTDAORedeEmpresa(db);
		ArrayList<String> vListEmpresa = getListEmpresa();
		
		for (int i = 0; i < vListEmpresa.size(); i++) {
			String vStrEmpresa = vListEmpresa.get(i);
			
			vObjRedeEmpresa.clearData();
			vObjRedeEmpresa.setAttrValue(EXTDAORedeEmpresa.REDE_ID_INT, pIdRede);
			vObjRedeEmpresa.setAttrValue(EXTDAORedeEmpresa.EMPRESA_ID_INT, vStrEmpresa);
		
			vObjRedeEmpresa.formatToSQLite();
			vObjRedeEmpresa.insert(true);
			
		}
	}
	
	public void clear(){
		idsEmpresa.clear();
		
		for(int i = 0 ; i < listView.size(); i ++){
			View vView = listView.get(i);
			LinearLayout linearLayoutEmpresaRede = (LinearLayout) activity.findViewById(R.id.linearlayout_empresa_funcionario);
			linearLayoutEmpresaRede.removeView(vView);	
		}
	}
	
	public ArrayList<String> getListEmpresa(){
		return idsEmpresa;
	}
	
	public Integer getIndexTuple(String pIdEmpresa){
		int i =  0;
		for (String vIdEmpresa : idsEmpresa) {
			if(vIdEmpresa.compareTo(pIdEmpresa) == 0 ) {
				return i;
			} 
			i += 1;
		}	
		return null;
	}
	
	public void add(String idRedeEmpresa, String pIdEmpresa, View pView){
		
		if(!this.containsTuple(pIdEmpresa)){
			idsRedeEmpresa.add(idRedeEmpresa);
			idsEmpresa.add(pIdEmpresa);
			listView.add(pView);
		}
	}
	
	public void delete(String pIdEmpresa){
		Integer i = getIndexTuple(pIdEmpresa);
		if(i != null){
			idsEmpresa.remove((int)i);
			String idRedeEmpresa = idsRedeEmpresa.get((int)i);
			if(idRedeEmpresa != null)
				idsRedeEmpresaRemovidos.add(idRedeEmpresa);
			idsRedeEmpresa.remove((int)i);
			View vView = listView.get((int)i);
			LinearLayout linearLayoutEmpresaRede = (LinearLayout) activity.findViewById(R.id.linearlayout_empresa_funcionario);
			linearLayoutEmpresaRede.removeView(vView);
			listView.remove((int)i);
		}
	}

	public int getLastIdEmpresa(){
		return idsEmpresa.size();
	}

}