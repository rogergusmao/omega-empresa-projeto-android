package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORelatorioAnexo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaSincronizadorArquivo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoAnexo;
import app.omegasoftware.pontoeletronico.file.HelperFile;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;

public class ContainerLayoutRelatorioAnexo{
	   
	ArrayList<String> listAnexo = new ArrayList<String>();
	
	ArrayList<View> listView= new ArrayList<View>();
	Activity activity;
	OmegaFileConfiguration config = new OmegaFileConfiguration();
	public ContainerLayoutRelatorioAnexo(Activity pActivity){
		activity  = pActivity;
	}
	public boolean containsTuple(String pIdEmpresa){
		
		if(getIndexTuple(pIdEmpresa) == null) 
			return false;
		else 
			return true;
	}
	
	public void addListTupleInUpdate(Database db, String pIdRelatorio){
		try{
			EXTDAORelatorioAnexo vObj = new EXTDAORelatorioAnexo(db);
			vObj.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(EXTDAOTipoAnexo.TIPO_ANEXO.ANEXO));
			vObj.setAttrValue(EXTDAORelatorioAnexo.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			vObj.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pIdRelatorio);
			ArrayList<Table> vList = vObj.getListTable();
			if(vList != null && vList.size() > 0)
			for (Table vTupleRedeEmpresa : vList) {
				vTupleRedeEmpresa.remove(true);
			}
			addListTupleInInsert(db, pIdRelatorio);
		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
		}
		
	}
	
	public void addListTupleInInsert(Database db, String pIdRelatorio){
		
		EXTDAORelatorioAnexo vObjRelatorioAnexo = new EXTDAORelatorioAnexo(db);
		ArrayList<String> vListAnexo = getListAnexo();
		EXTDAOSistemaSincronizadorArquivo objSincronizaFile = new EXTDAOSistemaSincronizadorArquivo(db);
		String path = config.getPath(OmegaFileConfiguration.TIPO.ANEXO);
		for (int i = 0; i < vListAnexo.size(); i++) {
			String vAnexo = vListAnexo.get(i);
			
			vObjRelatorioAnexo.clearData();
			vObjRelatorioAnexo.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pIdRelatorio);
			vObjRelatorioAnexo.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(EXTDAOTipoAnexo.TIPO_ANEXO.ANEXO));
			vObjRelatorioAnexo.setAttrValue(EXTDAORelatorioAnexo.ARQUIVO, vAnexo);
			vObjRelatorioAnexo.setAttrValue(EXTDAORelatorioAnexo.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			vObjRelatorioAnexo.formatToSQLite();
			if(vObjRelatorioAnexo.insert(true) != null){
				File vArquivo = new File(vAnexo);
				
				File vDirectory = new File(path); 
				if(!vDirectory.exists())
					vDirectory.mkdirs();
				String vNomeDoArquivo = HelperFile.getNameOfFileInPath(vAnexo);
				File vNewArquivo = new File(path + vNomeDoArquivo);
				vArquivo.renameTo(vNewArquivo);
				
				objSincronizaFile.insertFileInDatabase(vNomeDoArquivo, false);
			}
			
		}
	}
	
	public void clear(){
		listAnexo.clear();
		
		for(int i = 0 ; i < listView.size(); i ++){
			View vView = listView.get(i);
			LinearLayout linearLayoutEmpresaRede = (LinearLayout) activity.findViewById(R.id.anexo_linearlayout);
			linearLayoutEmpresaRede.removeView(vView);	
		}
	}
	
	public ArrayList<String> getListAnexo(){
		return listAnexo;
	}
	
	public Integer getIndexTuple(String pAnexo){
		int i =  0;
		for (String vIdEmpresa : listAnexo) {
			if(vIdEmpresa.compareTo(pAnexo) == 0 ) {
				return i;
			} 
			i += 1;
		}	
		return null;
	}
	
	public void add(String pNomeArquivo, View pView){
		if(!this.containsTuple(pNomeArquivo)){
			listAnexo.add(pNomeArquivo);
			
			listView.add(pView);
		}
	}
	
	public void delete(String pAnexo){
		Integer i = getIndexTuple(pAnexo);
		if(i != null){
			String vNomeAnexo = listAnexo.get((int)i);
			OmegaFileConfiguration conf = new OmegaFileConfiguration();
			
			File vArquivo = new File(
					conf.getPath(OmegaFileConfiguration.TIPO.ANEXO),
					vNomeAnexo);
			if(vArquivo.exists())
				vArquivo.delete();
			
			listAnexo.remove((int)i);
			
			View vView = listView.get((int)i);
			LinearLayout linearLayoutEmpresaRede = (LinearLayout) activity.findViewById(R.id.anexo_linearlayout);
			linearLayoutEmpresaRede.removeView(vView);
			listView.remove((int)i);
		}
	}

	public int getLastId(){
		return listAnexo.size();
	}

}