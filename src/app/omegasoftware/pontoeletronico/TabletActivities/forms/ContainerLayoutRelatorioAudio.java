package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORelatorioAnexo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaSincronizadorArquivo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoAnexo;
import app.omegasoftware.pontoeletronico.file.HelperFile;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;

public class ContainerLayoutRelatorioAudio{
	
	ArrayList<String> listAudio = new ArrayList<String>();
	
	ArrayList<View> listView= new ArrayList<View>();
	Activity activity;
	OmegaFileConfiguration config = new OmegaFileConfiguration();
	public ContainerLayoutRelatorioAudio(Activity pActivity){
		activity  = pActivity;
	}
	public boolean containsTuple(String pIdEmpresa){
		
		if(getIndexTuple(pIdEmpresa) == null) 
			return false;
		else 
			return true;
	}
	
	public void addListTupleInUpdate(Database db, String pIdRelatorio){
		try{
		EXTDAORelatorioAnexo vObj = new EXTDAORelatorioAnexo(db);
		vObj.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(EXTDAOTipoAnexo.TIPO_ANEXO.AUDIO));
		vObj.setAttrValue(EXTDAORelatorioAnexo.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		vObj.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pIdRelatorio);
		ArrayList<Table> vList = vObj.getListTable();
		if(vList != null && vList.size() > 0)
		for (Table vTupleRedeEmpresa : vList) {
			vTupleRedeEmpresa.remove(true);
		}
		addListTupleInInsert(db, pIdRelatorio);
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}
	}
	
	public void addListTupleInInsert(Database db, String pIdRelatorio){
		
		EXTDAORelatorioAnexo vObjRelatorioAudio = new EXTDAORelatorioAnexo(db);
		ArrayList<String> vListAudio = getListAudio();
		EXTDAOSistemaSincronizadorArquivo objSincronizaFile = new EXTDAOSistemaSincronizadorArquivo(db);
		for (int i = 0; i < vListAudio.size(); i++) {
			String vAudio = vListAudio.get(i);
			
			vObjRelatorioAudio.clearData();
			vObjRelatorioAudio.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pIdRelatorio);
			vObjRelatorioAudio.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(EXTDAOTipoAnexo.TIPO_ANEXO.AUDIO));
			vObjRelatorioAudio.setAttrValue(EXTDAORelatorioAnexo.ARQUIVO, vAudio);
			vObjRelatorioAudio.setAttrValue(EXTDAORelatorioAnexo.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			vObjRelatorioAudio.formatToSQLite();
			if(vObjRelatorioAudio.insert(true) != null){
				File vArquivo = new File(vAudio);
				String path = config.getPath(OmegaFileConfiguration.TIPO.ANEXO);
				File vDirectory = new File(path); 
				if(!vDirectory.exists())
					vDirectory.mkdirs();
				String vNomeDoArquivo = HelperFile.getNameOfFileInPath(vAudio);
				File vNewArquivo = new File(path + vNomeDoArquivo);
				vArquivo.renameTo(vNewArquivo);
				
				objSincronizaFile.insertFileInDatabase(vNomeDoArquivo, false);
			}
			
		}
	}
	
	public void clear(){
		listAudio.clear();
		
		for(int i = 0 ; i < listView.size(); i ++){
			View vView = listView.get(i);
			LinearLayout linearLayoutEmpresaRede = (LinearLayout) activity.findViewById(R.id.audio_linearlayout);
			linearLayoutEmpresaRede.removeView(vView);	
		}
	}
	
	public ArrayList<String> getListAudio(){
		return listAudio;
	}
	
	public Integer getIndexTuple(String pAudio){
		int i =  0;
		for (String vIdEmpresa : listAudio) {
			if(vIdEmpresa.compareTo(pAudio) == 0 ) {
				return i;
			} 
			i += 1;
		}	
		return null;
	}
	
	public void add(String pNomeArquivo, View pView){
		if(!this.containsTuple(pNomeArquivo)){
			listAudio.add(pNomeArquivo);
			
			listView.add(pView);
		}
	}
	
	public void delete(String pAudio){
		Integer i = getIndexTuple(pAudio);
		if(i != null){
			String vNomeAudio = listAudio.get((int)i);
			
			File vArquivo = new File(config.getPath(OmegaFileConfiguration.TIPO.AUDIO), vNomeAudio);
			if(vArquivo.exists())
				vArquivo.delete();
			
			listAudio.remove((int)i);
			
			View vView = listView.get((int)i);
			LinearLayout linearLayoutEmpresaRede = (LinearLayout) activity.findViewById(R.id.audio_linearlayout);
			linearLayoutEmpresaRede.removeView(vView);
			listView.remove((int)i);
		}
	}

	public int getLastId(){
		return listAudio.size();
	}

}