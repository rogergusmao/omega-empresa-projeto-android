package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORelatorioAnexo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaSincronizadorArquivo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoAnexo;
import app.omegasoftware.pontoeletronico.file.HelperFile;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;

public class ContainerLayoutRelatorioFoto{
	
	ArrayList<String> listFoto = new ArrayList<String>();
	
	ArrayList<View> listView= new ArrayList<View>();
	Activity activity;
	OmegaFileConfiguration config = new OmegaFileConfiguration();
	public ContainerLayoutRelatorioFoto(Activity pActivity){
		activity  = pActivity;
	}
	public boolean containsTuple(String pIdEmpresa){
		
		if(getIndexTuple(pIdEmpresa) == null) 
			return false;
		else 
			return true;
	}
	
	public void addListTupleInUpdate(Database db, String pIdRelatorio){
		try{
		EXTDAORelatorioAnexo vObj = new EXTDAORelatorioAnexo(db);
		vObj.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(EXTDAOTipoAnexo.TIPO_ANEXO.FOTO));
		vObj.setAttrValue(EXTDAORelatorioAnexo.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		vObj.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pIdRelatorio);
		ArrayList<Table> vList = vObj.getListTable();
		if(vList != null && vList.size() > 0)
		for (Table vTupleRedeEmpresa : vList) {
			vTupleRedeEmpresa.remove(true);
		}
		addListTupleInInsert(db, pIdRelatorio);
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}
	}

	public void addListTupleInInsert(Database db, String pIdRelatorio){
		
		EXTDAORelatorioAnexo vObjRelatorioFoto = new EXTDAORelatorioAnexo(db);
		ArrayList<String> vListFoto = getListFoto();
		EXTDAOSistemaSincronizadorArquivo objSincronizaFile = new EXTDAOSistemaSincronizadorArquivo(db);
		String path = config.getPath(OmegaFileConfiguration.TIPO.ANEXO);
		for (int i = 0; i < vListFoto.size(); i++) {
			String vFoto = vListFoto.get(i);
			
			vObjRelatorioFoto.clearData();
			vObjRelatorioFoto.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pIdRelatorio);
			vObjRelatorioFoto.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(EXTDAOTipoAnexo.TIPO_ANEXO.FOTO));
			vObjRelatorioFoto.setAttrValue(EXTDAORelatorioAnexo.ARQUIVO, vFoto);
			vObjRelatorioFoto.setAttrValue(EXTDAORelatorioAnexo.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			vObjRelatorioFoto.formatToSQLite();
			if(vObjRelatorioFoto.insert(true) != null){
				File vArquivo = new File(vFoto);
				
				
				String vNomeDoArquivo = HelperFile.getNameOfFileInPath(vFoto);
				File vNewArquivo = new File(path+ vNomeDoArquivo);
				vArquivo.renameTo(vNewArquivo);
				
				objSincronizaFile.insertFileInDatabase(vNomeDoArquivo, false);
			}
			
		}
	}
	
	public void clear(){
		listFoto.clear();
		
		for(int i = 0 ; i < listView.size(); i ++){
			View vView = listView.get(i);
			LinearLayout linearLayoutEmpresaRede = (LinearLayout) activity.findViewById(R.id.foto_linearlayout);
			linearLayoutEmpresaRede.removeView(vView);	
		}
	}
	
	public ArrayList<String> getListFoto(){
		return listFoto;
	}
	
	public Integer getIndexTuple(String pFoto){
		int i =  0;
		for (String vIdEmpresa : listFoto) {
			if(vIdEmpresa.compareTo(pFoto) == 0 ) {
				return i;
			} 
			i += 1;
		}	
		return null;
	}
	
	public void add(String pIdEmpresa, View pView){
		if(!this.containsTuple(pIdEmpresa)){
			listFoto.add(pIdEmpresa);
			
			listView.add(pView);
		}
	}
	
	public void delete(String pFoto){
		Integer i = getIndexTuple(pFoto);
		if(i != null){
			String vNomeFoto = listFoto.get((int)i);
			File vArquivo = new File(config.getPath(OmegaFileConfiguration.TIPO.FOTO), vNomeFoto);
			if(vArquivo.exists())
				vArquivo.delete();
			
			listFoto.remove((int)i);
			
			View vView = listView.get((int)i);
			LinearLayout linearLayoutEmpresaRede = (LinearLayout) activity.findViewById(R.id.foto_linearlayout);
			linearLayoutEmpresaRede.removeView(vView);
			listView.remove((int)i);
		}
	}

	public int getLastId(){
		return listFoto.size();
	}

}