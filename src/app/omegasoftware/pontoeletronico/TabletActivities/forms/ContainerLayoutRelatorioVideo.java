package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORelatorioAnexo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaSincronizadorArquivo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoAnexo;
import app.omegasoftware.pontoeletronico.file.HelperFile;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;

public class ContainerLayoutRelatorioVideo{
	
	ArrayList<String> listVideo = new ArrayList<String>();
	
	ArrayList<View> listView= new ArrayList<View>();
	Activity activity;
	OmegaFileConfiguration config = new OmegaFileConfiguration();
	public ContainerLayoutRelatorioVideo(Activity pActivity){
		activity  = pActivity;
	}
	public boolean containsTuple(String pIdEmpresa){
		
		if(getIndexTuple(pIdEmpresa) == null) 
			return false;
		else 
			return true;
	}
	
	public void addListTupleInUpdate(Database db, String pIdRelatorio){
		try{
		EXTDAORelatorioAnexo vObj = new EXTDAORelatorioAnexo(db);
		vObj.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(EXTDAOTipoAnexo.TIPO_ANEXO.VIDEO));
		vObj.setAttrValue(EXTDAORelatorioAnexo.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		vObj.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pIdRelatorio);
		ArrayList<Table> vList = vObj.getListTable();
		if(vList != null && vList.size() > 0)
		for (Table vTupleRedeEmpresa : vList) {
			vTupleRedeEmpresa.remove(true);
		}
		addListTupleInInsert(db, pIdRelatorio);
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}
	}

	public void addListTupleInInsert(Database db, String pIdRelatorio){
		
		EXTDAORelatorioAnexo vObjRelatorioVideo = new EXTDAORelatorioAnexo(db);
		ArrayList<String> vListVideo = getListVideo();
		EXTDAOSistemaSincronizadorArquivo objSincronizaFile = new EXTDAOSistemaSincronizadorArquivo(db);
		String pathAnexo = config.getPath(OmegaFileConfiguration.TIPO.ANEXO);
		
		for (int i = 0; i < vListVideo.size(); i++) {
			String vVideo = vListVideo.get(i);
			
			vObjRelatorioVideo.clearData();
			vObjRelatorioVideo.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pIdRelatorio);
			vObjRelatorioVideo.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(EXTDAOTipoAnexo.TIPO_ANEXO.VIDEO));
			vObjRelatorioVideo.setAttrValue(EXTDAORelatorioAnexo.ARQUIVO, vVideo);
			vObjRelatorioVideo.setAttrValue(EXTDAORelatorioAnexo.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			vObjRelatorioVideo.formatToSQLite();
			if(vObjRelatorioVideo.insert(true) != null){
				File vArquivo = new File(vVideo);
				
				
				String vNomeDoArquivo = HelperFile.getNameOfFileInPath(vVideo);
				File vNewArquivo = new File(pathAnexo+ vNomeDoArquivo);
				vArquivo.renameTo(vNewArquivo);
				
				objSincronizaFile.insertFileInDatabase(vNomeDoArquivo, false);
			}
			
		}
	}
	
	public void clear(){
		listVideo.clear();
		
		for(int i = 0 ; i < listView.size(); i ++){
			View vView = listView.get(i);
			LinearLayout linearLayoutEmpresaRede = (LinearLayout) activity.findViewById(R.id.video_linearlayout);
			linearLayoutEmpresaRede.removeView(vView);	
		}
	}
	
	public ArrayList<String> getListVideo(){
		return listVideo;
	}
	
	public Integer getIndexTuple(String pVideo){
		int i =  0;
		for (String vIdEmpresa : listVideo) {
			if(vIdEmpresa.compareTo(pVideo) == 0 ) {
				return i;
			} 
			i += 1;
		}	
		return null;
	}
	
	public void add(String pIdEmpresa, View pView){
		if(!this.containsTuple(pIdEmpresa)){
			listVideo.add(pIdEmpresa);
			
			listView.add(pView);
		}
	}
	
	public void delete(String pVideo){
		Integer i = getIndexTuple(pVideo);
		if(i != null){
			String vNomeVideo = listVideo.get((int)i);
			File vArquivo = new File(config.getPath(OmegaFileConfiguration.TIPO.VIDEO), vNomeVideo);
			if(vArquivo.exists())
				vArquivo.delete();
			
			listVideo.remove((int)i);
			
			View vView = listView.get((int)i);
			LinearLayout linearLayoutEmpresaRede = (LinearLayout) activity.findViewById(R.id.video_linearlayout);
			linearLayoutEmpresaRede.removeView(vView);
			listView.remove((int)i);
		}
	}

	public int getLastId(){
		return listVideo.size();
	}

}