package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.util.ArrayList;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculoUsuario;

public class ContainerLayoutVeiculoUsuario{
	ArrayList<String> idsVeiculoUsuairoRemovidos = new ArrayList<String>();
	ArrayList<String> idsVeiculoUsuario = new ArrayList<String>();
	ArrayList<String> idsUsuario = new ArrayList<String>();
	
	ArrayList<View> listView= new ArrayList<View>();
	Activity activity;
	
	public ContainerLayoutVeiculoUsuario(Activity pActivity){
		activity  = pActivity;
	}
	public boolean containsTuple(String pIdEmpresa){
		
		if(getIndexTuple(pIdEmpresa) == null) 
			return false;
		else 
			return true;
	}
	
	public void addListTupleInUpdate(Database db, String pIdVeiculo){
		try{
			EXTDAOVeiculoUsuario objVeiculoUsuarioAux = new EXTDAOVeiculoUsuario(db);
			for(int i = 0 ; i < idsVeiculoUsuario.size(); i++){
				if(idsVeiculoUsuario.get(i) == null){
					objVeiculoUsuarioAux.setId(null);
					objVeiculoUsuarioAux.setAttrValue(EXTDAOVeiculoUsuario.VEICULO_ID_INT, pIdVeiculo);
					objVeiculoUsuarioAux.setAttrValue(EXTDAOVeiculoUsuario.USUARIO_ID_INT, idsUsuario.get(i));
					objVeiculoUsuarioAux.formatToSQLite();
					objVeiculoUsuarioAux.insert(true);
				} else {
					objVeiculoUsuarioAux.select(idsVeiculoUsuario.get(i));
					String idUsuarioAtual = objVeiculoUsuarioAux.getStrValueOfAttribute(EXTDAOVeiculoUsuario.USUARIO_ID_INT);
					if(idsUsuario.get(i).compareTo(idUsuarioAtual)!= 0){
						objVeiculoUsuarioAux.select(idsVeiculoUsuario.get(i));
						objVeiculoUsuarioAux.setAttrValue(EXTDAOVeiculoUsuario.USUARIO_ID_INT, idsUsuario.get(i));
						objVeiculoUsuarioAux.formatToSQLite();
						objVeiculoUsuarioAux.update(true);
					}
				}
			}
			for(int i = 0 ; i < idsVeiculoUsuairoRemovidos.size(); i++){
				objVeiculoUsuarioAux.remove(idsVeiculoUsuairoRemovidos.get(i), true);
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
	}
	
	public void addListTupleInInsert(Database db, String pIdVeiculo){
		
		EXTDAOVeiculoUsuario vObjVeiculoUsuario = new EXTDAOVeiculoUsuario(db);
		ArrayList<String> idsUsuario = getIdsUsuario();
		
		for (int i = 0; i < idsUsuario.size(); i++) {
			String idUsuario = idsUsuario.get(i);
			
			vObjVeiculoUsuario.clearData();
			vObjVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.VEICULO_ID_INT, pIdVeiculo);
			vObjVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.USUARIO_ID_INT, idUsuario);
		
			vObjVeiculoUsuario.formatToSQLite();
			vObjVeiculoUsuario.insert(true);
			
		}
	}
	
	public void clear(){
		idsUsuario.clear();
		
		for(int i = 0 ; i < listView.size(); i ++){
			View vView = listView.get(i);
			LinearLayout linearLayoutVeiculoUsuario = (LinearLayout) activity.findViewById(R.id.ll_motoristas);
			linearLayoutVeiculoUsuario.removeView(vView);	
		}
	}
	
	public ArrayList<String> getIdsUsuario(){
		return idsUsuario;
	}
	
	public Integer getIndexTuple(String pIdEmpresa){
		int i =  0;
		for (String vIdEmpresa : idsUsuario) {
			if(vIdEmpresa.compareTo(pIdEmpresa) == 0 ) {
				return i;
			} 
			i += 1;
		}	
		return null;
	}
	
	public void add(String idVeiculoUsuario, String pIdUsuario, View pView){
		
		if(!this.containsTuple(pIdUsuario)){
			idsVeiculoUsuario.add(idVeiculoUsuario);
			idsUsuario.add(pIdUsuario);
			listView.add(pView);
		}
	}
	
	public void delete(String pIdUsuario){
		Integer i = getIndexTuple(pIdUsuario);
		if(i != null){
			idsUsuario.remove((int)i);
			String idVeiculoUsuario = idsVeiculoUsuario.get((int)i);
			if(idVeiculoUsuario != null)
				idsVeiculoUsuairoRemovidos.add(idVeiculoUsuario);
			idsVeiculoUsuario.remove((int)i);
			View vView = listView.get((int)i);
			LinearLayout linearLayoutEmpresaRede = (LinearLayout) activity.findViewById(R.id.ll_motoristas);
			linearLayoutEmpresaRede.removeView(vView);
			listView.remove((int)i);
		}
	}

	public int getLastIdUsuario(){
		return idsUsuario.size();
	}

}