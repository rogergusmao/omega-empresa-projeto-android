package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.graphics.Color;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.container.AbstractContainerClassItem;
import app.omegasoftware.pontoeletronico.common.container.ContainerMenuOption;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCategoriaPermissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPermissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPermissaoCategoriaPermissao;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.webservice.ServicosPontoEletronico;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;


public class FormCategoriaPermissaoActivityMobile extends OmegaCadastroActivity {

	//Constants
	public static final String TAG = "FormCategoriaPermissaoActivityMobile";
	private static final String TABELAS_RELACIONADAS[] = new String[]{
		EXTDAOCategoriaPermissao.NAME, 
		EXTDAOPermissaoCategoriaPermissao.NAME};

	public final static int INITIAL_MARGIN_LEFT = 55;
	public final static int INCREMENT_MARGIN_LEFT = 10;

	private ArrayList<CheckBox> listCheckBox = new ArrayList<CheckBox>(); 

	private DatabasePontoEletronico db=null;

	//EditText
	private EditText nomeEditText;
	LinearLayout linearLayoutCheckBox;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_categoria_permissao_activity_mobile_layout);

		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();

	}

	@Override
	public boolean loadData() {
		return true;
	}

	public void addViewPermissao(String pIdPermissaoPai, CheckBox pCheckBoxPai, int pMarginLeft){
		EXTDAOPermissao vObjPaiPermissao = new EXTDAOPermissao(this.db);

		vObjPaiPermissao.clearData();
		vObjPaiPermissao.setAttrValue(EXTDAOPermissao.PAI_PERMISSAO_ID_INT, pIdPermissaoPai);

		ArrayList<Table> vListFilhoPermissao = vObjPaiPermissao.getListTable();
		ArrayList<CheckBox> vListCheckBox = new ArrayList<CheckBox>();
		for (Table vTuplaFilhoPermissao : vListFilhoPermissao) {
			String vIdPermissao = vTuplaFilhoPermissao.getStrValueOfAttribute(EXTDAOPermissao.ID);

			String vTagPermissao = vTuplaFilhoPermissao.getStrValueOfAttribute(EXTDAOPermissao.TAG);
			AbstractContainerClassItem vContainer = ContainerMenuOption.getContainerItem(vTagPermissao);

			if(vContainer == null)
				continue;
			String vNomePermissao = vContainer.getNome(this);
			CheckBox vCheckBox = new CheckBox(this);
			vListCheckBox.add(vCheckBox);
			listCheckBox.add(vCheckBox);
			vCheckBox.setId(HelperInteger.parserInt(vIdPermissao));
			vCheckBox.setText(vNomePermissao);
			vCheckBox.setTextSize(14);
			vCheckBox.setTextColor(Color.BLACK);
			ViewGroup.LayoutParams vParam = new ViewGroup.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
			//			vParam.setMargins(pMarginLeft, 0, 0, 0);
			vCheckBox.setLayoutParams(vParam);

			//			vCheckBox.setGravity(Gravity.CENTER);
			vCheckBox.setPadding(pMarginLeft, 0, 0, 0);
			vCheckBox.invalidate();
			linearLayoutCheckBox.addView(vCheckBox.getRootView());
			EXTDAOPermissao vObjFilhoPermissao = new EXTDAOPermissao(this.db);
			vObjFilhoPermissao.setAttrValue(EXTDAOPermissao.PAI_PERMISSAO_ID_INT, vIdPermissao);
			ArrayList<Table> vListNetoPermissao = vObjFilhoPermissao.getListTable();	
			if(vListNetoPermissao != null)
				for (Table vTuplaNetoPermissao : vListNetoPermissao) {
					addViewPermissao(vTuplaNetoPermissao.getStrValueOfAttribute(EXTDAOPermissao.ID),
							vCheckBox, 
							pMarginLeft + INCREMENT_MARGIN_LEFT);
				}
		}
		pCheckBoxPai.setOnCheckedChangeListener(new PaiCheckBoxListener(vListCheckBox));
		for (CheckBox vCheckBox : vListCheckBox) {
			vCheckBox.setOnCheckedChangeListener(new FilhoCheckBoxListener(vCheckBox, pCheckBoxPai, vListCheckBox));
		}
	}

	@Override
	protected void loadEdit(){
		try{
			if(id == null) return;
			isLoadingEdit = true;
			EXTDAOCategoriaPermissao vObj = new EXTDAOCategoriaPermissao(this.db);
			vObj.setAttrValue(EXTDAOCategoriaPermissao.ID, id);
			vObj.select();
			String vNome = vObj.getStrValueOfAttribute(EXTDAOCategoriaPermissao.NOME);
			this.nomeEditText.setText(vNome);
			EXTDAOPermissaoCategoriaPermissao vObjPermissaoCategoriaPermissao = new EXTDAOPermissaoCategoriaPermissao(this.db);
			vObjPermissaoCategoriaPermissao.setAttrValue(EXTDAOPermissaoCategoriaPermissao.CATEGORIA_PERMISSAO_ID_INT, id);
			ArrayList<Table> vListTable = vObjPermissaoCategoriaPermissao.getListTable();
			for (Table table : vListTable) {
				String vIdPermissao = table.getStrValueOfAttribute(EXTDAOPermissaoCategoriaPermissao.PERMISSAO_ID_INT);
				for (CheckBox vCheckBox : this.listCheckBox) {
					int vId = vCheckBox.getId();
					String vStrId = String.valueOf(vId);
					if(vStrId.compareTo(vIdPermissao) == 0 ){
						vCheckBox.setChecked(true);
					}
				}
			}
			
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}finally{
			isLoadingEdit = false;
		}
		
	}

	@Override
	protected void beforeInitializeComponents() {
		//TODO resolvre
		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		

		this.nomeEditText = (EditText) findViewById(R.id.nome_edittext);

		new MaskUpperCaseTextWatcher(this.nomeEditText);

		linearLayoutCheckBox= (LinearLayout) findViewById(R.id.principal_vertical_lineLayout);

		EXTDAOPermissao vObjPermissao = new EXTDAOPermissao(this.db);
		vObjPermissao.setAttrValue(EXTDAOPermissao.PAI_PERMISSAO_ID_INT, Database.NULL);
		ArrayList<Table> vListTable = vObjPermissao.getListTable();

		for (Table vTuplaPermissao : vListTable) {
			String vIdPermissao = vTuplaPermissao.getStrValueOfAttribute(EXTDAOPermissao.ID);

			String vTagPermissao = vTuplaPermissao.getStrValueOfAttribute(EXTDAOPermissao.TAG);
			AbstractContainerClassItem vContainer = ContainerMenuOption.getContainerItem(vTagPermissao);

			if(vContainer == null)
				continue;
			String vNomePermissao = vContainer.getNome(this);

			CheckBox vCheckBox = new CheckBox(this);
			listCheckBox.add(vCheckBox);

			vCheckBox.setId(HelperInteger.parserInt(vIdPermissao));
			vCheckBox.setText(vNomePermissao);

			//			<item name="android:textSize">14sp</item>
			//	        <item name="android:textColor">@color/black</item>
			vCheckBox.setTextSize(14);
			vCheckBox.setTextColor(Color.BLACK);
			vCheckBox.invalidate();
			linearLayoutCheckBox.addView(vCheckBox.getRootView());
			//			EXTDAOPermissao vObjPaiPermissao = 
			addViewPermissao(vIdPermissao, vCheckBox, INITIAL_MARGIN_LEFT);
		}
		
	}	

	protected LinkedHashMap<String, String> getAllPermissao()
	{
		EXTDAOPermissao vObj = new EXTDAOPermissao(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}

	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click


	@Override
	protected ContainerCadastro cadastrando() {
		try{	
			//				controlerProgressDialog.createProgressDialog();
			
//			if(!HelperHttp.isServerOnline(FormCategoriaPermissaoActivityMobile.this)){
//				return new ContainerCadastro(FORM_ERROR_DIALOG_SERVER_OFFLINE, false);
//			}
			ContainerCadastro container = null;
			if(id == null){
				container = procedureInsert();
				if(!container.result )return container;
					
			}
			else{
				container = procedureUpdate();
				if(!container.result )return container;
			}

			if(id == null){

				handlerClearComponents.sendEmptyMessage(0);
			} else setBancoFoiModificado();
			
			return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);
		}
		catch(Exception e)
		{
			SingletonLog.insereErro(e, TIPO.FORMULARIO);
			//if(e != null) Log.e(TAG, e.getMessage());
			//else Log.e(TAG, "Error desconhecido");
			return new ContainerCadastro(new Mensagem(e), false);
		}finally{
			db.close();
		}
		
	}

	private ContainerCadastro procedureUpdate(){
		try{
			ServicosPontoEletronico servicoPE = new ServicosPontoEletronico(this);
			Mensagem msg = servicoPE.removerTodasAsPermissoesDaCategoriaDePermissao(id);
		
			
			if(msg == null){
				return new ContainerCadastro(FORM_ERROR_DIALOG_DESCONECTADO, false);
			} else if(msg.erro()){
				return new ContainerCadastro(msg, false);
			} else if(!msg.ok()){
				ContainerCadastro cc=new ContainerCadastro(msg, false);
				cc.setMensagem(msg);
				return cc;
			}
	
			EXTDAOCategoriaPermissao vObjCategoriaPermissao = new EXTDAOCategoriaPermissao(db);
			vObjCategoriaPermissao.setAttrValue(EXTDAOCategoriaPermissao.ID, id);
			vObjCategoriaPermissao.select();
			String vNomeCategoriaPermissaoAtual = vObjCategoriaPermissao.getStrValueOfAttribute(EXTDAOCategoriaPermissao.NOME);
			String vNomeCategoriaPermissao = "";
			if(!nomeEditText.getText().toString().equals("")){
				vNomeCategoriaPermissao = nomeEditText.getText().toString();
			}else{
				
				return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_TITULO, false);
			} 
			if(vNomeCategoriaPermissaoAtual.compareTo(vNomeCategoriaPermissao) != 0 ){
				vObjCategoriaPermissao.setAttrValue(EXTDAOCategoriaPermissao.NOME, vNomeCategoriaPermissao);
				vObjCategoriaPermissao.formatToSQLite();
				vObjCategoriaPermissao.update(true);
			}
	
	
			EXTDAOPermissaoCategoriaPermissao vNewObjPermissaoCategoriaPermissao = new EXTDAOPermissaoCategoriaPermissao(db);
	
			ArrayList<Table> vListTableP = vNewObjPermissaoCategoriaPermissao.getListTable();
	
			for (Table vTablePCP : vListTableP) {
				try{
					vTablePCP.remove(false);	
				}catch(Exception ex){
					SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
				}
				
			}
	
			for (CheckBox vCheckBox : listCheckBox) {
				int vIdPermissao = vCheckBox.getId();
				if(vCheckBox.isChecked()){
					vNewObjPermissaoCategoriaPermissao.clearData();
					vNewObjPermissaoCategoriaPermissao.setAttrValue(EXTDAOPermissaoCategoriaPermissao.PERMISSAO_ID_INT, String.valueOf(vIdPermissao));
					vNewObjPermissaoCategoriaPermissao.setAttrValue(EXTDAOPermissaoCategoriaPermissao.CATEGORIA_PERMISSAO_ID_INT, id);
					vNewObjPermissaoCategoriaPermissao.setAttrValue(EXTDAOPermissaoCategoriaPermissao.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
					vNewObjPermissaoCategoriaPermissao.formatToSQLite();
					vNewObjPermissaoCategoriaPermissao.insert(true);
				}
			}
			return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);

		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			
			//TODO corrigir o tipo de erro de retorno
			return new ContainerCadastro(new Mensagem(ex), false);
		}
	}


	private ContainerCadastro procedureInsert(){
		String vNomeCategoriaPermissao = "";
		if(nomeEditText.getText().toString().length() > 0){
			vNomeCategoriaPermissao = nomeEditText.getText().toString();
		}else{
			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_TITULO, false);
		}
		String vIdCorporacao = OmegaSecurity.getIdCorporacao();

		EXTDAOCategoriaPermissao vObjCategoriaPermissao = new EXTDAOCategoriaPermissao(db);

		vObjCategoriaPermissao.setAttrValue(EXTDAOCategoriaPermissao.CORPORACAO_ID_INT, vIdCorporacao);
		vObjCategoriaPermissao.setAttrValue(EXTDAOCategoriaPermissao.NOME, vNomeCategoriaPermissao);

		vObjCategoriaPermissao.formatToSQLite();
		Long vIdCategoriaPermissao = vObjCategoriaPermissao.insert(true);

		String vStrIdCategoriaPermissao = null;
		if(vIdCategoriaPermissao != null ){
			return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
		}
		vStrIdCategoriaPermissao = String.valueOf(vIdCategoriaPermissao);

		EXTDAOPermissaoCategoriaPermissao vNewObjPermissaoCategoriaPermissao = new EXTDAOPermissaoCategoriaPermissao(db);
		//			vNewObjPermissaoCategoriaPermissao.setAttrStrValue(EXTDAOPermissaoCategoriaPermissao.CATEGORIA_PERMISSAO_ID_INT, vStrIdCategoriaPermissao);		
		for (CheckBox vCheckBox : listCheckBox) {
			if(vCheckBox.isChecked()){
				int vIdPermissao = vCheckBox.getId();
				vNewObjPermissaoCategoriaPermissao.clearData();
				vNewObjPermissaoCategoriaPermissao.setAttrValue(EXTDAOPermissaoCategoriaPermissao.CATEGORIA_PERMISSAO_ID_INT, vStrIdCategoriaPermissao);
				vNewObjPermissaoCategoriaPermissao.setAttrValue(EXTDAOPermissaoCategoriaPermissao.PERMISSAO_ID_INT, String.valueOf(vIdPermissao));
				vNewObjPermissaoCategoriaPermissao.setAttrValue(EXTDAOPermissaoCategoriaPermissao.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
				//					vNewObjPermissaoCategoriaPermissao.formatToSQLite();
				vNewObjPermissaoCategoriaPermissao.insert(true);
			}
		}
		return new ContainerCadastro(true);
	}


	boolean isLoadingEdit = false;
	private class PaiCheckBoxListener implements OnCheckedChangeListener
	{
		ArrayList<CheckBox> listCheckBox = null;
		public PaiCheckBoxListener(ArrayList<CheckBox> pListCheckBox){
			listCheckBox = pListCheckBox;
		}


		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			
			if(isLoadingEdit) return;
			boolean vValidade = false;
			if(isChecked){
				for (CheckBox vCheckBox : listCheckBox) {
					if(vCheckBox.isChecked()){
						vValidade = true;
						break;
					}
				}
				//				Se um deles estiver marcado entao nao ha a necessidade de marcar os outros
				if(! vValidade)
					for (CheckBox vCheckBox : listCheckBox) {
						vCheckBox.setChecked(isChecked);
					}
			} else {
				for (CheckBox vCheckBox : listCheckBox) {
					vCheckBox.setChecked(isChecked);
				}
			}
		}
	}

	private class FilhoCheckBoxListener implements OnCheckedChangeListener
	{
		ArrayList<CheckBox> listCheckBox = null;
		CheckBox paiCheckBox = null;
		CheckBox checkBox = null;
		public FilhoCheckBoxListener(CheckBox pCheckBox, CheckBox pPaiCheckBox, ArrayList<CheckBox> pListCheckBox){
			checkBox = pCheckBox;
			listCheckBox = pListCheckBox;
			paiCheckBox = pPaiCheckBox;
		}


		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if(isLoadingEdit) return;
			
			if(!isChecked){
				boolean vValidade = true;
				for (CheckBox vCheckBox : listCheckBox) {
					if(vCheckBox == checkBox) continue;
					if(vCheckBox.isChecked()){
						vValidade = false;
						break;
					}
				}
				if(vValidade)
					paiCheckBox.setChecked(false);
			} else {
				//			Se marcado
				if(!paiCheckBox.isChecked())
					paiCheckBox.setChecked(true);
			}

		}
	}
}


