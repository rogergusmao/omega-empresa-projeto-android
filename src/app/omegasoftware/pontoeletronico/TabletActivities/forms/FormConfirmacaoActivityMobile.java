package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;


public class FormConfirmacaoActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "FormConfirmacaoActivityMobile";
	
	String message;
	//Search button
	private Button okButton;
	private Button cancelarButton;
	private Typeface customTypeFace;
	private TextView messageTextView; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_confirmacao_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		Bundle vParameter = getIntent().getExtras();
		if(vParameter != null)
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_MESSAGE)){
				message = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_MESSAGE);
			}
		new CustomDataLoader(this).execute();

	}

	@Override
	public boolean loadData() {

		return true;

	}


	public void formatarStyle()
	{
		//Botuo cadastrar:
		((Button) findViewById(R.id.ok_button)).setTypeface(this.customTypeFace);
		((Button) findViewById(R.id.cancelar_button)).setTypeface(this.customTypeFace);
	}

	@Override
	public void initializeComponents() {
		
		((Button) findViewById(R.id.ok_button)).setTypeface(this.customTypeFace);

		
		
		this.messageTextView = (TextView) findViewById(R.id.message_textview);
		if(message != null){
			if(message.length()  > 0 ){
				messageTextView.setText(message);
				messageTextView.setTypeface(this.customTypeFace);
			}
		}
			
		//Attach search button to its listener
		this.okButton = (Button) findViewById(R.id.ok_button);
		this.okButton.setOnClickListener(new OkButtonListener());
		
		//Attach search button to its listener
		this.cancelarButton = (Button) findViewById(R.id.cancelar_button);
		this.cancelarButton.setOnClickListener(new CancelarButtonListener());		

		this.formatarStyle();
	}	

	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onOkButtonClicked()
	{
		Intent intent = getIntent();
		
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO, true);
		setResult(RESULT_OK, intent);
		finish();
		
	}

	private class OkButtonListener implements OnClickListener
	{
		public void onClick(View v) {
			onOkButtonClicked();
		}
	}	
	
	private void onCancelarButtonClicked()
	{
		Intent intent = getIntent();
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO, false);
		setResult(RESULT_OK, intent);
		finish();
		
	}

	private class CancelarButtonListener implements OnClickListener
	{
		public void onClick(View v) {
			onCancelarButtonClicked();
		}
	}
}


