package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.ContainerPrograma;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;


public class FormConfirmacaoCadastroFiltroActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "FormConfirmacaoCadastroFiltroActivityMobile";
	
	String message;
	//Search button
	private Button cadastroButton;
	private Button filtroButton;
	private Button cancelarButton;
	private Typeface customTypeFace;
	private TextView messageTextView; 
	String tagFiltro;
	String tagCadastro;
	Class<?> classCadastro;
	Class<?> classFiltro;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_confirmacao_cadastro_ou_filtro_activity_mobile_layout);
		
		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		
		
		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {

		return true;

	}


	public void formatarStyle()
	{
		//Botuo cadastrar:
		((Button) findViewById(R.id.cadastro_button)).setTypeface(this.customTypeFace);
		((Button) findViewById(R.id.filtro_button)).setTypeface(this.customTypeFace);
		((Button) findViewById(R.id.cancelar_button)).setTypeface(this.customTypeFace);
	}
	
	public static Bundle getBundleParameterToCallActivity(String pMessage, String tagCadastro, Class<?> classCadastro, String tagFiltro, Class<?> classFiltro, String pEntidadeSingular, Boolean pIsMasculino){
		Bundle vParameter = new Bundle();
		vParameter.putString(OmegaConfiguration.SEARCH_FIELD_ENTIDADE, pEntidadeSingular);
		vParameter.putString(OmegaConfiguration.SEARCH_FIELD_MESSAGE, pMessage);
		vParameter.putString(OmegaConfiguration.SEARCH_FIELD_TAG_CADASTRO, tagCadastro);
		vParameter.putSerializable(OmegaConfiguration.SEARCH_FIELD_CLASS_CADASTRO, classCadastro);
		vParameter.putString(OmegaConfiguration.SEARCH_FIELD_TAG_FILTRO, tagFiltro);
		vParameter.putSerializable(OmegaConfiguration.SEARCH_FIELD_CLASS_FILTRO, classFiltro);
		vParameter.putString(OmegaConfiguration.SEARCH_FIELD_ENTIDADE, pEntidadeSingular);
		vParameter.putBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MASCULINO, pIsMasculino);
		return vParameter;
	}
	String entidade;
	Boolean isMasculino;
	@Override
	public void initializeComponents() throws OmegaDatabaseException {
		
		
		this.cadastroButton = (Button) findViewById(R.id.cadastro_button);
		this.filtroButton = (Button) findViewById(R.id.filtro_button);
		Database db = new DatabasePontoEletronico(this);
		Bundle vParameter = getIntent().getExtras();
		if(vParameter != null){
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ENTIDADE))
				entidade = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ENTIDADE);
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_MASCULINO))
				isMasculino = vParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MASCULINO);
			
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_MESSAGE)){
				message = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_MESSAGE);
			}
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_TAG_CADASTRO)){
				tagCadastro = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_TAG_CADASTRO);
				if(ContainerPrograma.isCategoriaPermissaoPermitidaParaUsuarioLogado(db, tagCadastro)){
					classCadastro = (Class<?>)vParameter.get(OmegaConfiguration.SEARCH_FIELD_CLASS_CADASTRO);	
				} else {
					tagCadastro = null;
					classCadastro = null;
					cadastroButton.setVisibility(View.GONE);
				}
				
			}
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_TAG_FILTRO)){
				tagFiltro = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_TAG_FILTRO);
				if(ContainerPrograma.isCategoriaPermissaoPermitidaParaUsuarioLogado(db, tagFiltro)){
					classFiltro = (Class<?>)vParameter.get(OmegaConfiguration.SEARCH_FIELD_CLASS_FILTRO);
				} else {
					classFiltro = null;
					tagFiltro = null;
					filtroButton.setVisibility(View.GONE);
				}
			}
		}
		String sufixoMensagem = "";
		if(classCadastro == null && classFiltro == null){
			sufixoMensagem = getResources().getString(R.string.form_sem_permissao_cadastro_e_edicao);
			if(!isMasculino)
			sufixoMensagem += " a ";
			else sufixoMensagem += " o "; 
			sufixoMensagem +=  entidade;
			sufixoMensagem += " " + getResources().getString(R.string.form_sem_permissao_cadastro_e_edicao_consulte_adm) + ".";
		}
		
		this.messageTextView = (TextView) findViewById(R.id.message_textview);
		if(message != null){
			if(message.length()  > 0 ){
				messageTextView.setText(message + sufixoMensagem);
				messageTextView.setTypeface(this.customTypeFace);
			}
		}
			
		//Attach search button to its listener
		
		this.cadastroButton.setOnClickListener(new CadastroButtonListener());
		if(this.classFiltro != null)
			this.filtroButton.setOnClickListener(new FiltroButtonListener());
		else findViewById(R.id.filtro_button).setVisibility(View.GONE);
		//Attach search button to its listener
		this.cancelarButton = (Button) findViewById(R.id.cancelar_button);
		this.cancelarButton.setOnClickListener(new CancelarButtonListener());		

		this.formatarStyle();
	}	

	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	private class CadastroButtonListener implements OnClickListener
	{
		public void onClick(View v) {
			Intent vNewIntent = new Intent(getApplicationContext(), classCadastro);
			startActivityForResult(vNewIntent, OmegaConfiguration.STATE_FORM_CONFIRMACAO_CADASTRO_OU_FILTRO_EMPRESA);
		}
	}
	
	private class FiltroButtonListener implements OnClickListener
	{
		public void onClick(View v) {
			Intent vNewIntent = new Intent(getApplicationContext(), classFiltro);
			startActivityForResult(vNewIntent, OmegaConfiguration.STATE_FORM_CONFIRMACAO_CADASTRO_OU_FILTRO_EMPRESA);
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		switch (requestCode) {
		case OmegaConfiguration.STATE_FORM_CONFIRMACAO_CADASTRO_OU_FILTRO_EMPRESA:
			setResult(RESULT_OK);
			finish();
			
			break;
		
		default:
			break;
		}
	}
	
	private void onCancelarButtonClicked()
	{
		Intent intent = getIntent();
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO, false);
		setResult(RESULT_OK, intent);
		finish();
		
	}

	private class CancelarButtonListener implements OnClickListener
	{
		public void onClick(View v) {
			onCancelarButtonClicked();
		}
	}
}


