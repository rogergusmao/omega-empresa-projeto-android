package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.ContainerPrograma;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.phone.HelperPhone;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

//public class FormCorporacaoActivityMobile extends OmegaRegularActivity {
//
//	//Constants
//	public static final String TAG = "FormCorporacaoActivityMobile";
//	
//	private final int FORM_ERROR_DIALOG_MISSING_CORPORACAO = 108;
//	private final int  FORM_ERROR_DIALOG_CORPORACAO_DUPLICADA= 109;
//	protected Button cadastrarButton;
//	private EditText corporacaoEditText;	
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.form_corporacao_activity_mobile_layout);
//		
//		new CustomDataLoader(this).execute();
//	}
//
//
//	@Override
//	public boolean loadData() {
//		return true;
//	}
//
//	@Override
//	protected Dialog onCreateDialog(int dialogType) {
//		Dialog vDialog = new Dialog(this);
//		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		
//		vDialog.setContentView(R.layout.dialog);
//
//		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
//		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);
//
//		switch (dialogType) {
//		case FORM_ERROR_DIALOG_CORPORACAO_DUPLICADA:
//			vTextView.setText(getResources().getString(R.string.error_corporacao_duplicada));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_CORPORACAO_DUPLICADA);
//				}
//			});
//			break;
//		case FORM_ERROR_DIALOG_MISSING_CORPORACAO:
//			vTextView.setText(getResources().getString(R.string.error_missing_corporacao));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_MISSING_CORPORACAO);
//				}
//			});
//			break;
//		default:
//			return super.onCreateDialog(dialogType);
//		
//		}
//
//		return vDialog;
//	}
//	
//
//	//---------------------------------------------------------------
//	//----------------- Methods related to search action ------------
//	//---------------------------------------------------------------
//	//Handles search button click
//	private void onCadastroButtonClicked(Activity pActivity)
//	{
//		new CadastroButtonLoader(pActivity).execute();
//	}
//	
//	public class CadastroButtonLoader extends AsyncTask<String, Integer, Boolean>
//	{
//		DialogInterface.OnClickListener dialogConfirmacaoCadastroCorpocao = null;
//		Integer dialogId = null;
//		Activity activity = null;
//		public CadastroButtonLoader(Activity pActivity){
//			activity = pActivity;
//		}
//		
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			controlerProgressDialog.createProgressDialog();
//		}
//
//		@Override
//		protected Boolean doInBackground(String... params) {
//			try{	
//				String vStrNomeCorporacao = corporacaoEditText.getText().toString();
//				if(vStrNomeCorporacao.length() == 0)
//				{
//					dialogId = FORM_ERROR_DIALOG_MISSING_CORPORACAO;
//					return false;
//				}
//				
//				String vStrPost = HelperHttpPost.getConteudoStringPost(
//						activity, 
//						OmegaConfiguration.URL_REQUEST_IS_CORPORACAO_EXISTENTE(), 
//						new String [] {
//								"usuario", 
//								"id_corporacao",
//								"corporacao", 
//								"senha", 
//								"p_corporacao" }, 
//						new String [] {
//								OmegaSecurity.getIdUsuario(), 
//								OmegaSecurity.getIdCorporacao(),
//								OmegaSecurity.getCorporacao(), 
//								OmegaSecurity.getSenha(), 
//								vStrNomeCorporacao});
//				if(vStrPost != null){
//					if(vStrPost.startsWith("FALSE 1")){
////						Intent intent = new Intent( 
////								activity, 
////								FormConfirmacaoActivityMobile.class);						
////						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_MESSAGE, getResources().getString(R.string.confirmacao_cadastro_corporacao));
////						startActivityForResult(intent, OmegaConfiguration.STATE_FORM_CONFIRMACAO );
//						
//						dialogConfirmacaoCadastroCorpocao = new DialogInterface.OnClickListener() {
//						    public void onClick(DialogInterface dialog, int which) {
//						        switch (which){
//						        case DialogInterface.BUTTON_POSITIVE:
//						        	new ProcedureConfirmacaoCadastroButtonLoader(FormCorporacaoActivityMobile.this).execute();
//						            break;
//
//						        case DialogInterface.BUTTON_NEGATIVE:
//						        	
//						            break;
//						        }
//								
//						    }
//						};
//						
//
//						
//						return true;
//					} else if(vStrPost.startsWith("TRUE")){
//						dialogId = FORM_ERROR_DIALOG_CORPORACAO_DUPLICADA;
//					} else{
//						dialogId = FORM_ERROR_DIALOG_POST;
//					}
//				} else dialogId = FORM_ERROR_DIALOG_DESCONECTADO;
//			}
//			catch(Exception e)
//			{
//				//if(e != null) Log.e(TAG, e.getMessage());
//				//else Log.e(TAG, "Error desconhecido");
//			}finally{
//				
//			}
//			return false;
//		}
//
//		@Override
//		protected void onPostExecute(Boolean result) {
//			super.onPostExecute(result);
//			controlerProgressDialog.dismissProgressDialog();
//			if(dialogId != null)
//				showDialog(dialogId);
//			else if(dialogConfirmacaoCadastroCorpocao != null){
//				FactoryAlertDialog.showAlertDialog(
//						FormCorporacaoActivityMobile.this, 
//						getResources().getString(R.string.confirmacao_cadastro_corporacao),
//						dialogConfirmacaoCadastroCorpocao);
//			}
//		}
//	}
//
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//		if (requestCode == OmegaConfiguration.STATE_FORM_CONFIRMACAO) {
//			if (resultCode == RESULT_OK) {
//				Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO , false);
//				if(vConfirmacao){
//					new ProcedureConfirmacaoCadastroButtonLoader(this).execute();
//				}
//			}
//		} else{
//			super.onActivityResult(requestCode, resultCode, intent);
//		}
//	}
//	
//	
//	public class ProcedureConfirmacaoCadastroButtonLoader extends AsyncTask<String, Integer, Boolean>
//	{
//		
//		Integer dialogId = null;
//		Activity activity = null;
//		public ProcedureConfirmacaoCadastroButtonLoader(Activity pActivity){
//			activity = pActivity;
//		}
//		
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			controlerProgressDialog.createProgressDialog();
//		}
//
//		@Override
//		protected Boolean doInBackground(String... params) {
//			try{	
//
//				String vStrNomeCorporacao = corporacaoEditText.getText().toString();
//				if(vStrNomeCorporacao.length() == 0)
//				{
//					dialogId = FORM_ERROR_DIALOG_MISSING_CORPORACAO;
//					return false;
//				}
//				String vStrPost = HelperHttpPost.getConteudoStringPost(
//						activity, 
//						OmegaConfiguration.URL_REQUEST_CADASTRO_CORPORACAO(), 
//						new String [] {
//								"usuario", 
//								"id_corporacao",
//								"corporacao", 
//								"senha", 
//								"imei",
//								"id_programa",
//								"p_corporacao",
//								"p_corporacao_sem_acento"}, 
//						new String [] {
//								OmegaSecurity.getIdUsuario(), 
//								OmegaSecurity.getIdCorporacao(),
//								OmegaSecurity.getCorporacao(), 
//								OmegaSecurity.getSenha(),
//								HelperPhone.getIMEI(),
//								ContainerPrograma.getIdPrograma(),
//								vStrNomeCorporacao,
//								HelperString.getWordWithoutAccent(vStrNomeCorporacao)});
//				if(vStrPost != null){
//					if (vStrPost.compareTo("TRUE") == 0){
//						dialogId = DIALOG_CADASTRO_OK;
//						handlerClearComponents.sendEmptyMessage(0);
//						return true;
//					} else{
//						dialogId = FORM_ERROR_DIALOG_POST;
//					}
//				} else dialogId = FORM_ERROR_DIALOG_DESCONECTADO;
//			}
//			catch(Exception e){
//				SingletonLog.insereErro(e, TIPO.FORMULARIO);
//			}finally{
//				
//			}
//			return false;
//		}
//
//		@Override
//		protected void onPostExecute(Boolean result) {
//			super.onPostExecute(result);
//			controlerProgressDialog.dismissProgressDialog();
//			if(dialogId != null)
//				showDialog(dialogId);
//		}
//	}
//					
//	 
//
//	//---------------------------------------------------------------
//	//------------------ Methods related to planos ------------------
//	//---------------------------------------------------------------
//
//
//	private class CadastroButtonListener implements OnClickListener
//	{
//		Activity activity;
//		public CadastroButtonListener(Activity pActivity){
//			activity = pActivity;
//		}
//
//		public void onClick(View v) {
//			onCadastroButtonClicked(activity);
//
//		}
//
//	}
//
//	@Override
//	public void initializeComponents() {
//		
//		
//		corporacaoEditText = (EditText) findViewById(R.id.form_corporacao_edittext);
//		new MaskUpperCaseTextWatcher(corporacaoEditText, 100);
//
//		
//		//Attach search button to its listener
//		this.cadastrarButton = (Button) findViewById(R.id.cadastrar_button);
//		this.cadastrarButton.setOnClickListener(new CadastroButtonListener(this));
//
//		
//	}
//
//}
//
//
