package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.controler.LoaderRunnable;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioCorporacao;
import app.omegasoftware.pontoeletronico.webservice.ServicosCobranca;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;


public class FormEditActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "FormEditActivityMobile";

	private final int FORM_ERROR_DIALOG_REMOCAO_OK = 3;
	
	private final int FORM_ERROR_DIALOG_SERVER_OFFLINE = 5;
	private final int FORM_ERROR_DIALOG_REGISTRO_JA_REMOVIDO = 6;
	protected final int FORM_ERROR_DIALOG_POST = 76;
	//	Se online: remocao atomica
	//	Se offline: nao permite a remocao
//	public final int TYPE_REMOVE_ATOMIC = 1;

	//	Se online ou offline: Remove com o sincronizador
	public final int TYPE_REMOVE_WITH_SYNC = 2;

	//	se online: remocao atomica  
	//	se offline: remocao com o sincronizador
//	public final int TYPE_REMOVE_WITH_OR_WITHOUT_SYNC = 3;

	private DatabasePontoEletronico db=null;

	//EditText
	protected Button deleteButton;
	protected Button editButton;

	protected String tabela;
	protected String id;
	protected Class<?> classe;
	protected int typeRemove = TYPE_REMOVE_WITH_SYNC;
	
	boolean isRemovePermited;
	boolean isEditPermited;
	CustomDataLoader dataLoader = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_edit_activity_mobile_layout);

		

		Bundle vParameter = getIntent().getExtras();
		id = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		tabela = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_TABELA);
		//class to edit of field
		classe = (Class<?>) vParameter.get(OmegaConfiguration.SEARCH_FIELD_CLASS);
		isRemovePermited = vParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_REMOVE_PERMITED, true);
		isEditPermited = vParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_EDIT_PERMITED, true);

		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_TYPE_REMOVE))
			typeRemove = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_TYPE_REMOVE);

		new CustomDataLoader(this).execute();
		
	}

	@Override
	public boolean loadData() {

		return true;
	}

	@Override
	public void initializeComponents() throws OmegaDatabaseException {


		this.db = new DatabasePontoEletronico(this);

		this.deleteButton= (Button) findViewById(R.id.delete_button);
		if(isRemovePermited)
			this.deleteButton.setOnClickListener(new ServicoClickListener(this));
		
		else this.deleteButton.setVisibility(View.GONE);
		this.editButton= (Button) findViewById(R.id.edit_button);
		if(isEditPermited)
			this.editButton.setOnClickListener(new ServicoClickListener(this));
		else this.editButton.setVisibility(View.GONE);

		
		this.db.close();
	}	



	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		switch (requestCode) {
		case OmegaConfiguration.ACTIVITY_FORM_EDIT:
			switch (resultCode) {
			case RESULT_CANCELED:
			case RESULT_OK:
				try{
					boolean vIsModificated = true;
					if(intent != null){
						Bundle pParameter = intent.getExtras();
						if(pParameter != null)
							if(pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED))
								vIsModificated = pParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED);
					}
					Intent vIntent = getIntent();
					vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, vIsModificated);
					setResult(OmegaConfiguration.ACTIVITY_FORM_EDIT, vIntent);
					
					finish();

				}catch(Exception ex){
					SingletonLog.insereErro(ex, TIPO.FORMULARIO);
				}
				break;
			default:

				break;
			}
			break;
		case OmegaConfiguration.STATE_FORM_CONFIRMACAO_DELETAR_REGISTRO:
			switch (resultCode) {
			case RESULT_CANCELED:
			case RESULT_OK:
				Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO , false);
				if(vConfirmacao){ 
					procedureDeleteTupla();
				}
				break;

			default:
				break;
			}
			break;
		default:
			finish();
			break;

		}

	}


	protected void procedureAlertAfterDeleteTupla(String pId){

	}

	private class ServicoClickListener implements OnClickListener
	{

		Activity activity;


		public ServicoClickListener(Activity pActivity){
			activity = pActivity;
		}

		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.edit_button:
//				new ProcedureEditLoader(activity).execute();
				new LoaderRunnable(activity, classe, TAG);
//				DialogInterface.OnClickListener dialogConfirmarSaida = new DialogInterface.OnClickListener() {
//				    public void onClick(DialogInterface dialog, int which) {
//				        switch (which){
//				        case DialogInterface.BUTTON_POSITIVE:
//				        	try{
//								boolean vIsModificated = true;
//								if(intent != null){
//									Bundle pParameter = intent.getExtras();
//									if(pParameter != null)
//										if(pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED))
//											vIsModificated = pParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED);
//								}
//								Intent vIntent = getIntent();
//								vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, vIsModificated);
//								setResult(OmegaConfiguration.ACTIVITY_FORM_EDIT, vIntent);
//								
//								finish();
//
//							}catch(Exception ex){
//								SingletonLog.insereErro(ex, TIPO.FORMULARIO);
//							}
//				            break;
//
//				        case DialogInterface.BUTTON_NEGATIVE:
//				        	
//				            break;
//				        }
//				    }
//				};
//				
//				FactoryAlertDialog.showAlertDialog(
//						FormEditActivityMobile.this, 
//						getString(R.string.form_confirmacao_remocao),
//						dialogConfirmarSaida);
				break;
			case R.id.delete_button:
//				new ProcedureCheckConfirmacaoDeleteLoader(activity).execute();
				
				DialogInterface.OnClickListener dialogConfirmarSaida = new DialogInterface.OnClickListener() {
				    public void onClick(DialogInterface dialog, int which) {
				        switch (which){
				        case DialogInterface.BUTTON_POSITIVE:
				        	procedureDeleteTupla();
				            break;

				        case DialogInterface.BUTTON_NEGATIVE:
				        	
				            break;
				        }
				    }
				};
				
				FactoryAlertDialog.showAlertDialog(
						FormEditActivityMobile.this, 
						getString(R.string.form_confirmacao_remocao),
						dialogConfirmarSaida);
				
				break;
			default:
				break;
			}			
		}
	}


	public class ProcedureEditLoader extends AsyncTask<String, Integer, Boolean>
	{

		Integer dialogId = null;
		Activity activity = null;
		Exception e = null;
		public ProcedureEditLoader(Activity pActivity){
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}
		Intent intent = null;
		@Override
		protected Boolean doInBackground(String... params) {
			try{	
				
				intent = new Intent(getApplicationContext(), classe);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, id);
				
			}
			catch(Exception e){
				this.e=e;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
				if(this.e!=null)
					SingletonLog.openDialogError(activity, e, SingletonLog.TIPO.PAGINA);
				startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_EDIT);
			} catch (Exception ex) {
				SingletonLog.openDialogError(FormEditActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
			
		}
	}

	public class ProcedureCheckConfirmacaoDeleteLoader extends AsyncTask<String, Integer, Boolean>
	{

		Integer dialogId = null;
		Activity activity = null;
		Intent intent;
		Exception e=null;
		public ProcedureCheckConfirmacaoDeleteLoader(Activity pActivity){
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}
		
		@Override
		protected Boolean doInBackground(String... params) {
			try{	
				intent = new Intent( activity, FormConfirmacaoActivityMobile.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_MESSAGE, getResources().getString(R.string.form_confirmacao_remocao));
				activity.startActivityForResult(intent, OmegaConfiguration.STATE_FORM_CONFIRMACAO_DELETAR_REGISTRO );
				
				
			}
			catch(Exception e){
				this.e=e;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if(this.e!=null)
				SingletonLog.openDialogError(activity, e, SingletonLog.TIPO.PAGINA);
			controlerProgressDialog.dismissProgressDialog();
			
		}
	}


	private void procedureDeleteTupla(){
		try{
			new ProcedureDeleteLoader(this).execute();
		} catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.FORMULARIO);

		}
	}

	public class ProcedureDeleteLoader extends AsyncTask<String, Integer, Boolean>
	{

		Integer dialogId = null;
		Activity activity = null;
		Mensagem msg = null;
		public ProcedureDeleteLoader(Activity pActivity){
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{	
				
				Table vObj = db.factoryTable(tabela);

				vObj.setAttrValue(Table.ID_UNIVERSAL, id);
				if(tabela.compareTo(EXTDAOUsuario.NAME) == 0 ){

						EXTDAOUsuario vObjUsuario = (EXTDAOUsuario)vObj;
						EXTDAOUsuarioCorporacao vObjUC = vObjUsuario.getObjUsuarioCorporacao();
						String vIdUsuarioCorporacao = vObjUC.getStrValueOfAttribute(EXTDAOUsuarioCorporacao.ID);
						
						String vIdTabelaSincronizadorUsuarioCorporacao = EXTDAOSistemaTabela.getIdSistemaTabela(db, EXTDAOUsuarioCorporacao.NAME);
						ServicosCobranca servicoCobranca = new ServicosCobranca(this.activity);
						Mensagem msg = servicoCobranca.removerCliente(vObjUsuario.getStrValueOfAttribute(DAOUsuario.EMAIL));
	
						if(msg == null){
							dialogId=FORM_ERROR_DIALOG_DESCONECTADO;
							return false;
						} else if(msg.ok()){
							vObj.remove(false);
							return true;
						}
						else {
							this.msg = msg;
							dialogId=null;
							return false;
						}
//					}
				} else{
					if(!vObj.select()){
						dialogId=FORM_ERROR_DIALOG_REGISTRO_JA_REMOVIDO;
						return false;
					}else{

						procedureAlertAfterDeleteTupla(id);
						
						//String vIdTabelaSincronizador = EXTDAOSistemaTabela.getIdSistemaTabela(db, tabela);
						//EXTDAOSistemaRegistroSincronizadorAndroidParaWeb vObjSincronizador = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(db);
						//se a tupla nao estiver sincronizada ainda, entao cancela todas as 
						//operacoes no banco refrente a tupla em questao.
						Integer intValidade3 = vObj.remove(true);
						return intValidade3 == null ? null : (intValidade3 <= 0 ? false : true);
						
					}
				}
			}
			catch(Exception e){
				SingletonLog.insereErro(e, TIPO.FORMULARIO);
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);

			try{
				if(dialogId != null)
					showDialog(dialogId);
				else if(this.msg != null)
					FactoryAlertDialog.showDialog(activity, msg);
				else if(!result)
					showDialog(FORM_ERROR_DIALOG_REMOCAO_OK);
			} catch (Exception ex) {
				SingletonLog.openDialogError(FormEditActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}


	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (id) {
		case FORM_ERROR_DIALOG_REMOCAO_OK:
			vTextView.setText(getResources().getString(R.string.remocao_ok));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_REMOCAO_OK);

					Intent intent = getIntent();
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, true);
					setResult(OmegaConfiguration.ACTIVITY_FORM_EDIT_DELETE, intent);
					finish();
				}
			});
			break;
		case FORM_ERROR_DIALOG_POST:
			vTextView.setText(getResources().getString(R.string.error_post));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_POST);
				}
			});
			break;
		case FORM_ERROR_DIALOG_SERVER_OFFLINE:
			vTextView.setText(getResources().getString(R.string.error_remocao_servidor_offline));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_SERVER_OFFLINE);
				}
			});
			break;
		case FORM_ERROR_DIALOG_REGISTRO_JA_REMOVIDO:
			vTextView.setText(getResources().getString(R.string.error_remocao_registro_ja_removido));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_REGISTRO_JA_REMOVIDO);
				}
			});
			break;

		default:
			return super.onCreateDialog(id);
		}
		
		return vDialog;

	}	

}

