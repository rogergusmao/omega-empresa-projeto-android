package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;


public class FormEditEnderecoActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "FormEditEnderecoActivityMobile";


	//private DatabasePontoEletronico db=null;

	//EditText
	protected Button deleteButton;
	protected Button editButton;
	
	protected Typeface customTypeFace;
	
	protected int idLinearLayout;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairroId;
	private String cidadeId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_edit_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		
		Bundle vParameter = getIntent().getExtras();
		
		//class to edit of field
		idLinearLayout = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT);
		
		
		logradouro = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO);
		numero = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_NUMERO);
		complemento = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO);
		bairroId = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO);
		cidadeId = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE);
		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {

		return true;
	}

	public void formatarStyle()
	{
		//Botuo
		((Button) findViewById(R.id.delete_button)).setTypeface(this.customTypeFace);
		((Button) findViewById(R.id.edit_button)).setTypeface(this.customTypeFace);
		

	}

	@Override
	public void initializeComponents() {
//		SynchronizeFastReceiveDatabase vSynchronizeFastReceiveDatabase = new SynchronizeFastReceiveDatabase(
//				this,  
//				-1,
//				new String[]{EXTDAOPais.NAME, EXTDAOBairro.NAME, EXTDAOUf.NAME, EXTDAOCidade.NAME});
//		
//		vSynchronizeFastReceiveDatabase.run(false);
		
		//this.db = new DatabasePontoEletronico(this);

		((Button) findViewById(R.id.delete_button)).setTypeface(this.customTypeFace);
		((Button) findViewById(R.id.edit_button)).setTypeface(this.customTypeFace);

		this.deleteButton= (Button) findViewById(R.id.delete_button);
		this.deleteButton.setOnClickListener(new ServicoClickListener());
		
		this.editButton= (Button) findViewById(R.id.edit_button);
		this.editButton.setOnClickListener(new ServicoClickListener());
		
		this.formatarStyle();
	}	



	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		
		if(requestCode == OmegaConfiguration.ACTIVITY_FORM_ENDERECO && resultCode == RESULT_OK ){
			
			try{
				Bundle vParameter = intent.getExtras();
				if(vParameter != null){
					String vLogradouro = null;
					String vNumero = null; 
					String vComplemento = null;
					String vIdBairro = null;
					String vIdCidade = null;
					String vBairro= null;
					String vCidade = null;
					String vIdEstado = null;
					String vIdPais = null;
					String vEstado= null;
					String vPais = null;
					Integer vIdLayout = null; 
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO))
						vComplemento = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO);
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO))
						vLogradouro = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO);
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_NUMERO))
						vNumero = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_NUMERO);
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO))
						vIdBairro = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO);
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE))
						vIdCidade = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE);
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_NOME_BAIRRO))
						vBairro = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_NOME_BAIRRO);
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE))
						vCidade = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE);
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO))
						vIdEstado= vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO);
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO))
						vEstado = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO);
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_PAIS))
						vIdPais = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_PAIS);
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS))
						vPais = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS);
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT))
						vIdLayout = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT);
					
					Intent intent2 = null;
					intent2 = getIntent();
					if(vIdLayout != null)
						intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, idLinearLayout);
					if(vComplemento != null)
						intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO, vComplemento);
					if(vLogradouro != null)
						intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO, vLogradouro);
					if(vNumero != null)
						intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO, vNumero);
					if(vCidade != null)
						intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE, vCidade);
					if(vBairro != null)
						intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_BAIRRO, vBairro);
					if(vIdBairro != null)
						intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO, vIdBairro);
					if(vIdCidade != null)
						intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE, vIdCidade);
					
					if(vEstado != null)
						intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO, vEstado);
					if(vIdEstado != null)
						intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO, vIdEstado);
					if(vIdPais != null)
						intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PAIS, vIdPais);
					if(vPais != null)
						intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS, vPais);
					
					setResult(OmegaConfiguration.ACTIVITY_FORM_ADD_LAYOUT_ENDERECO, intent2);
					finish();
						
				}

			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.FORMULARIO);
	
			}
		} else {
			finish();
		}
	}

	private class ServicoClickListener implements OnClickListener
	{

		
		public ServicoClickListener(){
		}
		
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.edit_button:
				Intent intent = null;
				intent = new Intent(getApplicationContext(), FormEnderecoActivityMobile.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, idLinearLayout);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE, cidadeId);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO, bairroId);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO, logradouro);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO, numero);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO, complemento);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_EDIT, true);
				if(intent != null){				
					try{
						startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_ENDERECO);
//						setResult(OmegaConfiguration.ACTIVITY_FORM_ENDERECO, intent);
//						finish();
					}catch(Exception ex){
						//Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
					}
				}
				
				break;

			case R.id.delete_button:
				Intent intent2 = null;
				intent2 = getIntent();
				intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, idLinearLayout);
				
				setResult(OmegaConfiguration.ACTIVITY_FORM_DELETE_LAYOUT_ENDERECO, intent2);
				
				finish();
				break;
			default:
				break;
			}			
		}
		
	}

	
}

