package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.InformationDialog;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.FormEnderecoAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.AppointmentPlaceItemList;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskLowerCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.dialog.HelperDialog;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOBairro;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaPerfil;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPerfil;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoDocumento;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.listener.MenuCelularOnClickListener;
import app.omegasoftware.pontoeletronico.listener.MenuTelefoneOnClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class FormEmpresaActivityMobile extends OmegaCadastroActivity {

	// Constants
	public static final String TAG = "FormEmpresaActivityMobile";

	private Button enderecoButton;
	public static final int INFORMACAO_EMPRESA_DUPLICIDADE = 8811;
	private DatabasePontoEletronico db = null;

	final String PERTENCE_AO_GRUPO = "1";
	final String NAO_PERTENCE_AO_GRUPO = "2";

	// EditText
	private AutoCompleteTextView tipoDocumentoAutoCompletEditText;

	TableAutoCompleteTextView tipoDocumentoContainerAutoComplete = null;

	private EditText nomeEditText;

	private Button telefone1Button;
	private Button telefone2Button;
	private Button faxButton;
	private Button celularButton;

	private MenuTelefoneOnClickListener telefone1OnClickListener;
	private MenuTelefoneOnClickListener telefone2OnClickListener;
	private MenuTelefoneOnClickListener faxOnClickListener;
	private MenuCelularOnClickListener celularOnClickListener;
	// ViewFocusHandler viewFocusHandler;

	private EditText emailEditText;
	private Spinner isEmpresaDaCorporacaoSpinner;

	private LinearLayout perfilDaEmpresaLinearLayout;

	OnFocusChangeListener celularOnFocusChangeListener;

	private EditText numeroDocumentoEditText;

	private AutoCompleteTextView tipoEmpresaAutoCompleteEditText;
	TableAutoCompleteTextView tipoEmpresaContainerAutoComplete = null;

	CustomSpinnerAdapter isEmpresaDaCorporacaoCustomSpinnerAdapter;

	ContainerLayoutEndereco containerEndereco = null;

	CheckBox parceiroCheckBox;
	CheckBox clienteCheckBox;
	CheckBox fornecedorCheckBox;

	Button documentoButton;
	DocumentoHandler documentoHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_empresa_activity_mobile_layout);

		new CustomDataLoader(this).execute();
		showDialog(INFORMACAO_EMPRESA_DUPLICIDADE);
	}

	@Override
	public void clearComponents(ViewGroup vg) {

		celularOnClickListener.clear();
		telefone1OnClickListener.clear();
		telefone2OnClickListener.clear();
		faxOnClickListener.clear();

		documentoHandler.sendEmptyMessage(DocumentoHandler.CLEAR);

		// First we check the obligatory fields:
		if (containerEndereco != null) {
			containerEndereco.delete();
			enderecoButton.setVisibility(View.VISIBLE);
		}

		super.clearComponents(vg);

		clienteCheckBox.setChecked(true);
		// viewFocusHandler.sendEmptyMessage(0);

	}

	public class DocumentoHandler extends Handler {
		public static final int VISIBLE = 1;
		public static final int CLEAR = 2;

		public DocumentoHandler() {

		}

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {

			case CLEAR:
				documentoButton.setVisibility(View.VISIBLE);
				tipoDocumentoAutoCompletEditText.setVisibility(View.GONE);
				tipoDocumentoAutoCompletEditText.setText("");
				numeroDocumentoEditText.setVisibility(View.GONE);
				numeroDocumentoEditText.setText("");
				break;
			default:
			case VISIBLE:
				documentoButton.setVisibility(View.GONE);
				tipoDocumentoAutoCompletEditText.setVisibility(View.VISIBLE);
				numeroDocumentoEditText.setVisibility(View.VISIBLE);
				tipoDocumentoAutoCompletEditText.requestFocus();
				break;
			}

		}
	}

	@Override
	public boolean loadData() {

		return true;

	}

	@Override
	public void beforeInitializeComponents() throws OmegaDatabaseException {
		// if( id != null )=> Edicao => ja foi sincronizado no filter
		// Se cadastro entao sera feita a sincronizacao

		this.db = new DatabasePontoEletronico(this);

		parceiroCheckBox = (CheckBox) findViewById(R.id.form_empresa_parceiro_checkbox);
		clienteCheckBox = (CheckBox) findViewById(R.id.form_empresa_cliente_checkbox);
		fornecedorCheckBox = (CheckBox) findViewById(R.id.form_empresa_fornecedor_checkbox);

		perfilDaEmpresaLinearLayout = (LinearLayout) findViewById(R.id.linearlayout_empresa_perfil);
		perfilDaEmpresaLinearLayout.setVisibility(View.GONE);

		this.nomeEditText = (EditText) findViewById(R.id.nome_edittext);
		new MaskUpperCaseTextWatcher(this.nomeEditText, 100);
		this.nomeEditText.clearFocus();

		this.telefone1Button = (Button) findViewById(R.id.telefone_1_button);
		telefone1OnClickListener = new MenuTelefoneOnClickListener(this, telefone1Button,
				OmegaConfiguration.ACTIVITY_FORM_TECLADO_TELEFONE_1);
		telefone1Button.setOnClickListener(telefone1OnClickListener);

		this.telefone2Button = (Button) findViewById(R.id.telefone_2_button);
		telefone2OnClickListener = new MenuTelefoneOnClickListener(this, telefone2Button,
				OmegaConfiguration.ACTIVITY_FORM_TECLADO_TELEFONE_2);
		telefone2Button.setOnClickListener(telefone2OnClickListener);

		this.celularButton = (Button) findViewById(R.id.celular_button);
		celularOnClickListener = new MenuCelularOnClickListener(this, celularButton,
				OmegaConfiguration.ACTIVITY_FORM_TECLADO_CELULAR, null);
		celularButton.setOnClickListener(celularOnClickListener);
		this.documentoHandler = new DocumentoHandler();
		this.documentoButton = (Button) findViewById(R.id.documento_button);
		this.documentoButton.setVisibility(View.VISIBLE);
		this.documentoButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				documentoHandler.sendEmptyMessage(0);

			}
		});
		this.faxButton = (Button) findViewById(R.id.fax_button);
		faxOnClickListener = new MenuTelefoneOnClickListener(this, faxButton,
				OmegaConfiguration.ACTIVITY_FORM_TECLADO_FAX, R.string.fax);
		faxButton.setOnClickListener(faxOnClickListener);

		this.emailEditText = (EditText) findViewById(R.id.email_edittext);
		new MaskLowerCaseTextWatcher(this.emailEditText, 255);
		// viewFocusHandler = new ViewFocusHandler(this, this.emailEditText);

		this.numeroDocumentoEditText = (EditText) findViewById(R.id.numero_documento_edittext);
		new MaskUpperCaseTextWatcher(numeroDocumentoEditText, 30);

		EXTDAOTipoDocumento vObjTipoDocumento = new EXTDAOTipoDocumento(this.db);
		// this.tipoDocumentoContainerAutoComplete = new
		// TableAutoCompleteTextView(vObjTipoDocumento);
		// ArrayAdapter<String> tipoDocumentoAdapter = new ArrayAdapter<String>(
		// this,
		// R.layout.spinner_item,
		// tipoDocumentoContainerAutoComplete.getVetorStrId());

		this.tipoDocumentoAutoCompletEditText = (AutoCompleteTextView) findViewById(
				R.id.tipo_documento_autocompletetextview);
		this.tipoDocumentoContainerAutoComplete = new TableAutoCompleteTextView(this, vObjTipoDocumento,
				tipoDocumentoAutoCompletEditText);
		// tipoDocumentoAutoCompletEditText.setAdapter(tipoDocumentoAdapter);

		this.numeroDocumentoEditText.setOnFocusChangeListener(new NumeroDocumentoOnFocusChangeListener());
		this.numeroDocumentoEditText.setOnClickListener(new NumeroDocumentoOnClickListener());
		this.tipoDocumentoAutoCompletEditText.setOnFocusChangeListener(new TipoDocumentoOnFocusChangeListener());

		//
		// tipoDocumentoAutoCompletEditText.setOnFocusChangeListener(new
		// TipoDocumentoOnFocusChangeListener());

		// tipoDocumentoAutoCompletEditText.setOnKeyListener(new
		// TipoDocumentoOnKeyListener());
		new MaskUpperCaseTextWatcher(tipoDocumentoAutoCompletEditText, 100);
		// tipoDocumentoAutoCompletEditText.setOnFocusChangeListener(new
		// TipoDocumentoOnItemSelectedListener());

		// new CNPJTextWatcher(this.documentoEditText);

		EXTDAOTipoEmpresa vObjTipoEmpresa = new EXTDAOTipoEmpresa(this.db);
		this.tipoEmpresaAutoCompleteEditText = (AutoCompleteTextView) findViewById(
				R.id.tipo_empresa_autocompletetextview);
		this.tipoEmpresaContainerAutoComplete = new TableAutoCompleteTextView(this, vObjTipoEmpresa,
				tipoEmpresaAutoCompleteEditText);
		new MaskUpperCaseTextWatcher(tipoEmpresaAutoCompleteEditText, 100);

		this.isEmpresaDaCorporacaoSpinner = (Spinner) findViewById(R.id.is_empresa_da_corporacao_spinner);

		LinkedHashMap<String, String> vHashSimOuNao = new LinkedHashMap<String, String>();
		String vStrSim = getResources().getString(R.string.yes);
		String vStrNao = getResources().getString(R.string.no);
		vHashSimOuNao.put("2", vStrNao);
		vHashSimOuNao.put("1", vStrSim);

		isEmpresaDaCorporacaoCustomSpinnerAdapter = new CustomSpinnerAdapter(getApplicationContext(), vHashSimOuNao,
				R.layout.spinner_item, R.string.empresa_pertence_ao_grupo);
		isEmpresaDaCorporacaoSpinner.setAdapter(isEmpresaDaCorporacaoCustomSpinnerAdapter);
		isEmpresaDaCorporacaoSpinner.setSelection(1);
		clienteCheckBox.setChecked(true);
		((CustomSpinnerAdapter) this.isEmpresaDaCorporacaoSpinner.getAdapter())
				.setDropDownLayout(R.layout.spinner_item_selected);
		isEmpresaDaCorporacaoSpinner.setOnItemSelectedListener(new IsEmpresaDaCorporacaoOnItemSelectedListener());

		this.enderecoButton = (Button) findViewById(R.id.endereco_button);
		this.enderecoButton.setOnClickListener(new CadastrarEnderecoListener());

	}

	@Override
	protected void loadEdit() {
		try {
			EXTDAOEmpresa vObjEmpresa = new EXTDAOEmpresa(db);
			vObjEmpresa.setAttrValue(EXTDAOEmpresa.ID, id);
			vObjEmpresa.select();
			String vNomeEmpresa = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.NOME);
			if (vNomeEmpresa != null) {
				nomeEditText.setText(vNomeEmpresa);
			}

			String vTelefone1 = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.TELEFONE1);
			if (vTelefone1 != null) {
				telefone1Button.setText(vTelefone1);
				telefone1Button.setTextSize(20);
			}
			String vTelefone2 = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.TELEFONE2);
			if (vTelefone2 != null) {
				telefone2Button.setText(vTelefone2);
				telefone2Button.setTextSize(20);
			}
			String vFax = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.FAX);
			if (vFax != null) {
				faxButton.setText(vFax);
				faxButton.setTextSize(20);
			}
			String vEmail = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.EMAIL);
			if (vEmail != null) {
				emailEditText.setText(vEmail);
			}

			String vCelular = vObjEmpresa.getStrValueOfAttribute(EXTDAOPessoa.CELULAR);
			if (vCelular != null) {
				celularButton.setText(vCelular);
				celularButton.setTextSize(20);
			}

			// celularOnFocusChangeListener.onFocusChange(null, false);

			String vTipoEmpresa = vObjEmpresa.getNomeDaChaveExtrangeira(EXTDAOEmpresa.TIPO_EMPRESA_ID_INT);

			if (vTipoEmpresa != null && vTipoEmpresa.length() > 0) {
				tipoEmpresaAutoCompleteEditText.setText(vTipoEmpresa);
			}

			EXTDAOEmpresaPerfil vObjEP = new EXTDAOEmpresaPerfil(db);
			vObjEP.setAttrValue(EXTDAOEmpresaPerfil.EMPRESA_ID_INT, id);
			vObjEP.setAttrValue(EXTDAOEmpresaPerfil.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			ArrayList<Table> vListTuplaEmpresaPerfil = vObjEP.getListTable();
			boolean vIsEmpresaDaCorporacao = false;

			boolean vIsCliente = false;
			boolean vIsFornecedor = false;
			boolean vIsParceiro = false;

			for (Table vTuplaEmpresaPefil : vListTuplaEmpresaPerfil) {
				int vIdPerfil = HelperInteger
						.parserInt(vTuplaEmpresaPefil.getStrValueOfAttribute(EXTDAOEmpresaPerfil.PERFIL_ID_INT));

				switch (vIdPerfil) {
				case EXTDAOPerfil.ID_EMPRESA_CORPORACAO:
					vIsEmpresaDaCorporacao = true;
					break;
				case EXTDAOPerfil.ID_CLIENTE:
					vIsCliente = true;
					break;
				case EXTDAOPerfil.ID_FORNECEDOR:
					vIsFornecedor = true;
					break;
				case EXTDAOPerfil.ID_PARCEIRO:
					vIsParceiro = true;
					break;

				default:
					break;
				}
			}

			if (vIsEmpresaDaCorporacao) {
				String vStrSim = getResources().getString(R.string.yes);
				if (vStrSim != null) {

					int vPosition = isEmpresaDaCorporacaoCustomSpinnerAdapter.getPositionFromId(PERTENCE_AO_GRUPO);
					isEmpresaDaCorporacaoSpinner.setSelection(vPosition);

				}
			} else {
				String vStrNo = getResources().getString(R.string.no);
				if (vStrNo != null) {
					int vPosition = isEmpresaDaCorporacaoCustomSpinnerAdapter.getPositionFromId(NAO_PERTENCE_AO_GRUPO);
					isEmpresaDaCorporacaoSpinner.setSelection(vPosition);
				}
				if (vIsCliente)
					clienteCheckBox.setChecked(true);
				if (vIsFornecedor)
					fornecedorCheckBox.setChecked(true);
				if (vIsParceiro)
					parceiroCheckBox.setChecked(true);
			}

			String vTipoDocumento = vObjEmpresa.getNomeDaChaveExtrangeira(EXTDAOPessoa.TIPO_DOCUMENTO_ID_INT);

			if (vTipoDocumento != null && vTipoDocumento.length() > 0) {
				tipoDocumentoAutoCompletEditText.setText(vTipoDocumento);
				String vNumeroDocumento = vObjEmpresa.getStrValueOfAttribute(EXTDAOPessoa.NUMERO_DOCUMENTO);
				if (vNumeroDocumento != null && vNumeroDocumento.length() > 0) {
					numeroDocumentoEditText.setText(vNumeroDocumento);
				}
			}
			String vIdCidade = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.CIDADE_ID_INT);
			String vIdBairro = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.BAIRRO_ID_INT);

			EXTDAOCidade vObjCidade = new EXTDAOCidade(db);
			vObjCidade.setAttrValue(EXTDAOCidade.ID, vIdCidade);
			vObjCidade.select();
			String vNomeCidade = vObjCidade.getStrValueOfAttribute(EXTDAOCidade.NOME);
			String vIdEstado = vObjCidade.getStrValueOfAttribute(EXTDAOCidade.UF_ID_INT);
			String vNomeEstado = vObjCidade.getNomeDaChaveExtrangeira(EXTDAOCidade.UF_ID_INT);
			String vNomePais = null;
			String vIdPais = null;

			if (vIdEstado != null) {
				EXTDAOUf vUf = new EXTDAOUf(db);
				vUf.setAttrValue(EXTDAOUf.ID, vIdEstado);
				vUf.select();
				vNomePais = vUf.getNomeDaChaveExtrangeira(EXTDAOUf.PAIS_ID_INT);
				vIdPais = vUf.getStrValueOfAttribute(EXTDAOUf.PAIS_ID_INT);
			}

			EXTDAOBairro vObjBairro = new EXTDAOBairro(db);
			vObjBairro.select();
			String vNomeBairro = vObjBairro.getStrValueOfAttribute(EXTDAOBairro.NOME);

			String vLogradouro = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.LOGRADOURO);
			String vNumero = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.NUMERO);
			String vComplemento = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.COMPLEMENTO);
			LinearLayout vLinearLayoutEndereco = (LinearLayout) findViewById(R.id.linearlayout_endereco);
			if (vIdBairro != null || vIdCidade != null || vIdEstado != null || vIdPais != null) {
				containerEndereco = addLayoutEndereco(vIdBairro, vIdCidade, vIdEstado, vIdPais, vNomeBairro,
						vNomeCidade, vNomeEstado, vNomePais, vLogradouro, vNumero, vComplemento, vLinearLayoutEndereco,
						this.enderecoButton);
				vLinearLayoutEndereco.setVisibility(View.VISIBLE);
				showDialog(OmegaConfiguration.FORM_DIALOG_INFORMACAO_DIALOG_ENDERECO);
			} else {
				vLinearLayoutEndereco.setVisibility(View.GONE);
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);

		}
	}

	public ContainerLayoutEndereco addLayoutEndereco(String pIdBairro, String pIdCidade, String pIdEstado,
			String pIdPais, String pNomeBairro, String pNomeCidade, String pNomeEstado, String pNomePais,
			String pLogradouro, String pNumero, String pComplemento, LinearLayout pLinearLayoutEndereco,
			Button pAddButtonEndereco) throws OmegaDatabaseException {
		if (db == null)
			db = new DatabasePontoEletronico(this);

		AppointmentPlaceItemList appointmentPlaceItemList = new AppointmentPlaceItemList(getApplicationContext(), 0,
				pLogradouro, pNumero, pComplemento, pNomeBairro, pNomeCidade, pNomeEstado, pNomePais, pIdBairro,
				pIdCidade, pIdEstado, pIdPais, null, null, null);
		// LinearLayout linearLayoutEndereco = (LinearLayout)
		// findViewById(R.id.linearlayout_endereco);

		ContainerLayoutEndereco vContainerEndereco = new ContainerLayoutEndereco(this, appointmentPlaceItemList,
				pLinearLayoutEndereco);

		FormEnderecoAdapter adapter = new FormEnderecoAdapter(this, vContainerEndereco, pAddButtonEndereco);

		View vNewView = adapter.getView(0, null, null);

		pLinearLayoutEndereco.addView(vNewView);
		pLinearLayoutEndereco.setVisibility(View.VISIBLE);
		vContainerEndereco.setAppointmentPlace(vNewView);
		db.close();
		return vContainerEndereco;
	}

	@Override
	public void finish() {
		try {
			this.db.close();

			super.finish();
		} catch (Exception ex) {

		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		switch (requestCode) {
		case OmegaConfiguration.ACTIVITY_FORM_TECLADO_TELEFONE_1:
			telefone1OnClickListener.onActivityResult(intent);
			break;
		case OmegaConfiguration.ACTIVITY_FORM_TECLADO_TELEFONE_2:
			telefone2OnClickListener.onActivityResult(intent);
			break;
		case OmegaConfiguration.ACTIVITY_FORM_TECLADO_FAX:
			faxOnClickListener.onActivityResult(intent);
			break;
		case OmegaConfiguration.ACTIVITY_FORM_TECLADO_CELULAR:
			celularOnClickListener.onActivityResult(intent);
			break;
		// case OmegaConfiguration.ACTIVITY_FORM_TECLADO_CELULAR_SMS:
		// celularSMSOnClickListener.onActivityResult(intent);
		// break;
		case OmegaConfiguration.ACTIVITY_FORM_ENDERECO:
			switch (resultCode) {
			case OmegaConfiguration.ACTIVITY_FORM_DELETE_LAYOUT_ENDERECO:
				int vIdLinearLayout = intent.getIntExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, -1);
				if (vIdLinearLayout == R.id.linearlayout_endereco) {
					containerEndereco.delete();
					if (enderecoButton != null)
						enderecoButton.setVisibility(View.VISIBLE);
				}
				break;

			default:

				try {
					if (intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE)) {
						String vIdCidade = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE);
						String vIdBairro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO);
						String vIdEstado = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO);
						String vIdPais = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_PAIS);
						String vNomeCidade = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE);
						String vNomeBairro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_BAIRRO);
						String vNomeEstado = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO);
						String vNomePais = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS);
						String vLogradouro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO);
						String vNumero = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO);
						String vComplemento = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO);
						LinearLayout vLinearLayoutEndereco = (LinearLayout) findViewById(R.id.linearlayout_endereco);
						if (containerEndereco != null)
							containerEndereco.delete();
						containerEndereco = addLayoutEndereco(vIdBairro, vIdCidade, vIdEstado, vIdPais, vNomeBairro,
								vNomeCidade, vNomeEstado, vNomePais, vLogradouro, vNumero, vComplemento,
								vLinearLayoutEndereco, enderecoButton);
						showDialog(OmegaConfiguration.FORM_DIALOG_INFORMACAO_DIALOG_ENDERECO);
					}
				} catch (Exception ex) {
					SingletonLog.insereErro(ex, TIPO.FORMULARIO);
				}

			}
			break;
		default:
			super.onActivityResult(requestCode, resultCode, intent);
			break;
		}

	}

	private class CadastrarEnderecoListener implements OnClickListener {

		Intent intent = null;

		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.endereco_button:
				intent = new Intent(getApplicationContext(), FormEnderecoActivityMobile.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.linearlayout_endereco);
				if (containerEndereco != null) {
					AppointmentPlaceItemList vAppPlaceItemList = containerEndereco.getAppointmentPlaceItemList();
					vAppPlaceItemList.putDataInIntent(intent);
				}
				break;

			default:
				break;
			}

			if (intent != null) {
				startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_ENDERECO);
			}
		}
	}

	protected LinkedHashMap<String, String> getAllTipoEmpresa() {
		EXTDAOTipoEmpresa vObj = new EXTDAOTipoEmpresa(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		switch (id) {
		case INFORMACAO_EMPRESA_DUPLICIDADE:
			Dialog vDialog = new Dialog(this);
			vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			vDialog.setContentView(R.layout.dialog);

			InformationDialog vInformation = new InformationDialog(this, vDialog,
					TAG + "_dialog_informacao_duplicidade", INFORMACAO_EMPRESA_DUPLICIDADE);
			return vInformation.getDialogInformation(
					new int[] { R.string.informacao_uso_list_detalhes, R.string.informacao_email_cadastro_empresa });
		default:
			return this.getDialog(id);

		}
	}

	private void updateListTuplaEmpresaPerfil(boolean pIsEmpresaDaCorporacao, String pIdEmpresa) {
		EXTDAOEmpresaPerfil vObj = new EXTDAOEmpresaPerfil(db);
		vObj.setAttrValue(EXTDAOEmpresaPerfil.EMPRESA_ID_INT, pIdEmpresa);
		vObj.setAttrValue(EXTDAOEmpresaPerfil.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		ArrayList<Table> vList = vObj.getListTable();
		if (vList != null && vList.size() > 0)
			for (Table vTuple : vList) {
				try {
					vTuple.remove(true);
				} catch (Exception ex) {
					SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
				}
			}
		addListTuplaEmpresaPerfil(pIsEmpresaDaCorporacao, pIdEmpresa);
	}

	private void addListTuplaEmpresaPerfil(boolean pIsEmpresaDaCorporacao, String pIdEmpresa) {
		if (pIdEmpresa == null || pIdEmpresa.length() == 0)
			return;
		if (pIsEmpresaDaCorporacao) {
			EXTDAOEmpresaPerfil vObjEmpresaPerfil = new EXTDAOEmpresaPerfil(db);
			vObjEmpresaPerfil.setAttrValue(EXTDAOEmpresaPerfil.EMPRESA_ID_INT, String.valueOf(pIdEmpresa));
			vObjEmpresaPerfil.setAttrValue(EXTDAOEmpresaPerfil.PERFIL_ID_INT, EXTDAOPerfil.ID_STR_EMPRESA_CORPORACAO);
			vObjEmpresaPerfil.setAttrValue(EXTDAOEmpresaPerfil.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			vObjEmpresaPerfil.insert(true);
		} else {
			CheckBox vVetorCheckBoxPerfil[] = new CheckBox[] { parceiroCheckBox, fornecedorCheckBox, clienteCheckBox };
			String vVetorIdPerfil[] = new String[] { EXTDAOPerfil.ID_STR_PARCEIRO, EXTDAOPerfil.ID_STR_FORNECEDOR,
					EXTDAOPerfil.ID_STR_CLIENTE };
			for (int i = 0; i < vVetorIdPerfil.length; i++) {
				CheckBox vCheckBoxPerfil = vVetorCheckBoxPerfil[i];
				String vIdPerfil = vVetorIdPerfil[i];
				if (vCheckBoxPerfil.isChecked()) {
					EXTDAOEmpresaPerfil vObjEmpresaPerfil = new EXTDAOEmpresaPerfil(db);
					vObjEmpresaPerfil.setAttrValue(EXTDAOEmpresaPerfil.EMPRESA_ID_INT, pIdEmpresa);
					vObjEmpresaPerfil.setAttrValue(EXTDAOEmpresaPerfil.PERFIL_ID_INT, vIdPerfil);
					vObjEmpresaPerfil.setAttrValue(EXTDAOEmpresaPerfil.CORPORACAO_ID_INT,
							OmegaSecurity.getIdCorporacao());
					vObjEmpresaPerfil.insert(true);
				}
			}
		}
	}

	protected class NumeroDocumentoOnFocusChangeListener implements OnFocusChangeListener {
		//
		public void onFocusChange(View view, boolean hasFocus) {
			if (hasFocus) {
				String vTipoDocumento = tipoDocumentoAutoCompletEditText.getText().toString();
				if (vTipoDocumento.length() == 0) {
					showDialog(FORM_ERROR_DIALOG_MISSING_TIPO_DOCUMENTO);
					tipoDocumentoAutoCompletEditText.requestFocus();
					tipoDocumentoAutoCompletEditText.setSelection(0);

				}
			}
		}
	}

	protected class NumeroDocumentoOnClickListener implements OnClickListener {

		public void onClick(View arg0) {

			String vTipoDocumento = tipoDocumentoAutoCompletEditText.getText().toString();

			if (vTipoDocumento.length() == 0) {
				showDialog(FORM_ERROR_DIALOG_MISSING_TIPO_DOCUMENTO);
				tipoDocumentoAutoCompletEditText.requestFocus();
				tipoDocumentoAutoCompletEditText.setSelection(0);

			}

		}

	}

	protected class TipoDocumentoOnFocusChangeListener implements OnFocusChangeListener {

		public void onFocusChange(View view, boolean hasFocus) {

			if (!hasFocus) {
				String vTipoDocumento = tipoDocumentoAutoCompletEditText.getText().toString();

				if (vTipoDocumento.length() == 0) {
					numeroDocumentoEditText.setText("");
				}
			}
		}
	}

	protected class IsEmpresaDaCorporacaoOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

			String selectedItem = String.valueOf(arg2);
			if (selectedItem.compareTo(PERTENCE_AO_GRUPO) == 0) {
				perfilDaEmpresaLinearLayout.setVisibility(View.VISIBLE);
			} else if (selectedItem.compareTo(NAO_PERTENCE_AO_GRUPO) == 0) {
				perfilDaEmpresaLinearLayout.setVisibility(View.GONE);
			} else {
				perfilDaEmpresaLinearLayout.setVisibility(View.GONE);
			}

		}

		public void onNothingSelected(AdapterView<?> arg0) {

		}
	}

	@Override
	protected ContainerCadastro cadastrando() {
		try {
			Table vObj = new EXTDAOEmpresa(db);
			boolean vValidade = true;
			// First we check the obligatory fields:
			// Plano, Especialidade, Cidade
			// Checking Cidade
			if (id != null) {
				vObj.setAttrValue(EXTDAOEmpresa.ID, id);
				vObj.select();
			}

			if (!nomeEditText.getText().toString().trim().equals("")) {
				vObj.setAttrValue(Table.NOME_UNIVERSAL, nomeEditText.getText().toString());
			} else {

				vObj.setAttrValue(EXTDAOEmpresa.NOME,
						HelperString.formatarNomeDoEmail(emailEditText.getText().toString()));
			}
			// else{
			// return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_NOME,
			// false);
			// }

			String vStrNomeTipoDocumento = tipoDocumentoAutoCompletEditText.getText().toString();
			if (vStrNomeTipoDocumento.trim().length() > 0) {

				String vStrNumeroDocumento = numeroDocumentoEditText.getText().toString();
				if (vStrNumeroDocumento.length() == 0) {
					return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_NUMERO_DOCUMENTO, false);

				} else {
					vObj.setAttrValue(EXTDAOEmpresa.NUMERO_DOCUMENTO, vStrNumeroDocumento);
				}
			} else {
				vObj.setAttrValue(EXTDAOEmpresa.TIPO_DOCUMENTO_ID_INT, null);
				vObj.setAttrValue(EXTDAOEmpresa.NUMERO_DOCUMENTO, null);
			}

			if (telefone1OnClickListener.isNumeroSet())
				vObj.setAttrValue(EXTDAOEmpresa.TELEFONE_1_UNIVERSAL, telefone1Button.getText().toString());
			else
				vObj.setAttrValue(EXTDAOEmpresa.TELEFONE_1_UNIVERSAL, null);

			if (telefone2OnClickListener.isNumeroSet())
				vObj.setAttrValue(Table.TELEFONE_2_UNIVERSAL, telefone2Button.getText().toString());
			else
				vObj.setAttrValue(EXTDAOEmpresa.TELEFONE_2_UNIVERSAL, null);

			// if(celularSMSOnClickListener.isNumeroSet())
			// {
			// vObj.setAttrValue(Table.CELULAR_SMS_UNIVERSAL,
			// celularSMSButton.getText().toString());
			// }else vObj.setAttrValue(EXTDAOEmpresa.CELULAR_SMS_UNIVERSAL,
			// null);

			if (celularOnClickListener.isNumeroSet()) {
				vObj.setAttrValue(Table.CELULAR_UNIVERSAL, celularButton.getText().toString());
			} else
				vObj.setAttrValue(EXTDAOEmpresa.CELULAR_UNIVERSAL, null);

			if (faxOnClickListener.isNumeroSet()) {
				vObj.setAttrValue(Table.FAX_UNIVERSAL, faxButton.getText().toString());
			} else
				vObj.setAttrValue(EXTDAOEmpresa.FAX_UNIVERSAL, null);

			boolean vIsEmpresaDaCorporacao = false;
			String selectedEmpresaDaCorporacao = String.valueOf(isEmpresaDaCorporacaoSpinner.getSelectedItemId());
			if (!selectedEmpresaDaCorporacao.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB)) {
				if (selectedEmpresaDaCorporacao.compareTo(PERTENCE_AO_GRUPO) == 0)
					vIsEmpresaDaCorporacao = true;
				else if (selectedEmpresaDaCorporacao.compareTo(NAO_PERTENCE_AO_GRUPO) == 0) {
					vIsEmpresaDaCorporacao = false;
					if (!(parceiroCheckBox.isChecked() || fornecedorCheckBox.isChecked()
							|| clienteCheckBox.isChecked())) {
						return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_CHECK_BOX_NAO_EH_EMPRESA_DA_CORPORACAO,
								false);

					}
				}
			} else {
				isEmpresaDaCorporacaoSpinner.requestFocus();
				return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_EMPRESA_DA_CORPORACAO, false);
			}

			if (emailEditText.getText().toString().length() > 0) {
				String vToken = emailEditText.getText().toString();
				// if(! HelperCheckField.checkEmail(vToken)){

				// return new ContainerCadastro(ERROR_DIALOG_MISSING_EMAIL,
				// true);
				// }else{
				vObj.setAttrValue(Table.EMAIL_UNIVERSAL, vToken);
				// }
			} else
				vObj.setAttrValue(EXTDAOEmpresa.EMAIL_UNIVERSAL, null);

			String vStrNomeTipoEmpresa = tipoEmpresaAutoCompleteEditText.getText().toString();
			if (vStrNomeTipoEmpresa.length() > 0) {
				EXTDAOTipoEmpresa vObjTipoEmpresa = new EXTDAOTipoEmpresa(db);
				vObjTipoEmpresa.setAttrValue(EXTDAOTipoEmpresa.NOME, vStrNomeTipoEmpresa);
				String vIdTipoEmpresa = tipoEmpresaContainerAutoComplete.addTupleIfNecessay(vObjTipoEmpresa);
				if (vIdTipoEmpresa == null) {
					return new ContainerCadastro(FORM_ERROR_DIALOG_INSERCAO, false);

				} else {
					vObj.setAttrValue(EXTDAOEmpresa.TIPO_EMPRESA_ID_INT, vIdTipoEmpresa);
				}
			} else
				vObj.setAttrValue(EXTDAOEmpresa.TIPO_EMPRESA_ID_INT, null);

			if (containerEndereco != null && containerEndereco.getAppointmentPlaceItemList() != null) {
				AppointmentPlaceItemList appointmentPlaceItemList = containerEndereco.getAppointmentPlaceItemList();

				if (appointmentPlaceItemList.getComplemento() != null
						&& appointmentPlaceItemList.getComplemento().length() > 0)
					vObj.setAttrValue(Table.COMPLEMENTO_UNIVERSAL, appointmentPlaceItemList.getComplemento());
				else
					vObj.setAttrValue(EXTDAOPessoa.COMPLEMENTO_UNIVERSAL, null);

				if (appointmentPlaceItemList.getNumber() != null && appointmentPlaceItemList.getNumber().length() > 0)
					vObj.setAttrValue(Table.NUMERO_UNIVERSAL, appointmentPlaceItemList.getNumber());
				else
					vObj.setAttrValue(Table.NUMERO_UNIVERSAL, null);

				if (appointmentPlaceItemList.getIdCity() != null && appointmentPlaceItemList.getIdCity().length() > 0) {
					vObj.setAttrValue(Table.CIDADE_ID_INT_UNIVERSAL, appointmentPlaceItemList.getIdCity());
				} else
					vObj.setAttrValue(Table.CIDADE_ID_INT_UNIVERSAL, null);

				if (appointmentPlaceItemList.getIdNeighborhood() != null
						&& appointmentPlaceItemList.getIdNeighborhood().length() > 0)
					vObj.setAttrValue(Table.BAIRRO_ID_INT_UNIVERSAL, appointmentPlaceItemList.getIdNeighborhood());
				else
					vObj.setAttrValue(Table.BAIRRO_ID_INT_UNIVERSAL, null);

				if (appointmentPlaceItemList.getAddress() != null && appointmentPlaceItemList.getAddress().length() > 0)
					vObj.setAttrValue(Table.LOGRADOURO_UNIVERSAL, appointmentPlaceItemList.getAddress());
				else
					vObj.setAttrValue(Table.LOGRADOURO_UNIVERSAL, null);

			} else {
				vObj.setAttrValue(EXTDAOPessoa.COMPLEMENTO_UNIVERSAL, null);
				vObj.setAttrValue(EXTDAOPessoa.NUMERO_UNIVERSAL, null);
				vObj.setAttrValue(EXTDAOPessoa.CIDADE_ID_INT_UNIVERSAL, null);
				vObj.setAttrValue(EXTDAOPessoa.BAIRRO_ID_INT_UNIVERSAL, null);
				vObj.setAttrValue(EXTDAOPessoa.LOGRADOURO_UNIVERSAL, null);
			}

			vObj.setAttrValue(EXTDAOEmpresa.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			if (vValidade) {

				if (vStrNomeTipoDocumento.length() > 0) {
					EXTDAOTipoDocumento vObjTipoDocumento = new EXTDAOTipoDocumento(db);
					vObjTipoDocumento.setAttrValue(EXTDAOTipoDocumento.NOME, vStrNomeTipoDocumento);
					vObjTipoDocumento.setAttrValue(EXTDAOTipoDocumento.CORPORACAO_ID_INT,
							OmegaSecurity.getIdCorporacao());
					String vIdTipoDocumento = tipoDocumentoContainerAutoComplete.addTupleIfNecessay(vObjTipoDocumento);
					if (vIdTipoDocumento == null) {
						return new ContainerCadastro(DIALOG_ERRO_FATAL, false);

					} else {
						vObj.setAttrValue(EXTDAOEmpresa.TIPO_DOCUMENTO_ID_INT, vIdTipoDocumento);
					}
				}

				if (id != null) {
					vObj.setAttrValue(EXTDAOEmpresa.ID, id);
					vObj.formatToSQLite();
					if (vObj.update(true)) {
						setBancoFoiModificado();
						// Apaga todas as tuplas do perfil
						EXTDAOEmpresaPerfil vObjEP = new EXTDAOEmpresaPerfil(db);
						vObjEP.setAttrValue(EXTDAOEmpresaPerfil.EMPRESA_ID_INT, id);
						vObjEP.setAttrValue(EXTDAOEmpresaPerfil.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
						ArrayList<Table> vListTuplaEmpresaPerfil = vObjEP.getListTable();
						for (Table vTupla : vListTuplaEmpresaPerfil) {
							try {
								vTupla.remove(true);
							} catch (Exception ex) {
								SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
							}
						}
						updateListTuplaEmpresaPerfil(vIsEmpresaDaCorporacao, id);

						return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);

					} else {
						return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
					}

				} else {

					vObj.setLongAttrValue(EXTDAOEmpresa.CADASTRO_SEC, HelperDate.getTimestampSegundosUTC(this));
					vObj.setIntegerAttrValue(EXTDAOEmpresa.CADASTRO_OFFSEC, HelperDate.getOffsetSegundosTimeZone());

					vObj.formatToSQLite();
					Long vIdEmpresa = vObj.insert(true);
					if (vIdEmpresa != null) {
						setBancoFoiModificado();
						String vStrIdEmpresa = String.valueOf(vIdEmpresa);
						addListTuplaEmpresaPerfil(vIsEmpresaDaCorporacao, vStrIdEmpresa);

						handlerClearComponents.sendEmptyMessage(0);
						return new ContainerCadastro(HelperDialog.DIALOG_CADASTRO_OK, true);

					} else {
						return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);

					}
				}
			} else {
				return new ContainerCadastro(FORM_ERROR_DIALOG_CAMPO_INVALIDO, false);
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			// corrigir o tipo de erro de retorno
			return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
		}
	}
}
