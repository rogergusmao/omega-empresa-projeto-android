package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.ResultSet;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresaRotina;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;

public class FormEmpresaRotinaActivityMobile extends OmegaCadastroActivity {

	//Constants
	private static final String TAG = "FormEmpresaRotinaActivityMobile";

	
//	Handler handlerView;
	private Button cancelarButton;
	Drawable originalDrawable;

	int ano;
	int mes;
	int diaMes;
	int diaSemana;
	String idPessoa ;
	String idUsuario;
	int semanaRotina;
	private EXTDAOPessoa objPessoa;
	private EXTDAOUsuario objUsuario;
	EXTDAOPessoaEmpresa objPessoaEmpresa;
	EXTDAOPessoaEmpresaRotina objPessoaEmpresaRotina;
	Database db=null;
	ContainerLayoutPessoaEmpresa containerPessoaEmpresa ;	
	LinearLayout llPessoasEmpresas;
	private Button btNovaPessoaEmpresa;
	ResultSet rsPessoaEmpresa = null;
	ResultSet rsPessoaEmpresaRotina = null;
	ArrayList<View> viewsPessoaEmpresa = new ArrayList<View>(); 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_empresa_rotina_activity_mobile_layout);
		//TODO corrigir disposicao do try catch
		try{
	
			Bundle vParameter = getIntent().getExtras();
			if(vParameter != null){
				
				idUsuario = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_USUARIO);
				db = new DatabasePontoEletronico(this);			
				objUsuario = new EXTDAOUsuario(db);
				objUsuario.select(idUsuario);
				ano = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_ANO);
				mes = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_MES);
				diaMes = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_DIA_MES);
				diaSemana= vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_DIA_SEMANA);
				semanaRotina = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_ID_SEMANA_CICLO);
				
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			
		}
		new CustomDataLoader(this).execute();
	}



	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------


	private class CancelarButtonListener implements OnClickListener
	{
		
		Activity activity;
		
		public CancelarButtonListener(Activity pActivity){
			activity = pActivity;
		}
		public void onClick(View v) {
			activity.finish();
			
		}
	}	

	@Override
	protected void beforeInitializeComponents() {
		

		//Attach search button to its listener
		this.cancelarButton = (Button) findViewById(R.id.cancelar_button);
		if(cancelarButton != null){
			this.cancelarButton.setOnClickListener(new CancelarButtonListener(this));	
		}
		btNovaPessoaEmpresa = (Button)findViewById(R.id.nova_pessoa_empresa_button);
		btNovaPessoaEmpresa.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(FormEmpresaRotinaActivityMobile.this, FormPessoaEmpresaActivityMobile.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA, objPessoa.getId());
				FormEmpresaRotinaActivityMobile.this.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
			}
		});
 		llPessoasEmpresas = (LinearLayout) findViewById(R.id.linearlayout_empresa_funcionario);
 		llPessoasEmpresas.removeAllViews();
		if(rsPessoaEmpresa != null){
			for(int i = 0 ; i < rsPessoaEmpresa.getTotalTupla(); i++){
				String[] valores = rsPessoaEmpresa.getTupla(i);
				String pessoa =valores[0];
				String empresa =valores[1];
				String profissao =valores[2];
				
				LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				
				View layoutView = layoutInflater.inflate(R.layout.checkbox_pessoa_empresa_layout, null);
				TextView tvEmpresa = (TextView) layoutView.findViewById(R.id.empresa_text_view);
				TextView tvPessoa = (TextView) layoutView.findViewById(R.id.pessoa_text_view);
				TextView tvProfissao = (TextView) layoutView.findViewById(R.id.profissao_text_view);
				
				
				tvEmpresa.setText(empresa);
				tvPessoa.setText(pessoa);
				tvProfissao.setText(profissao);
				llPessoasEmpresas.addView(layoutView);
				viewsPessoaEmpresa.add(layoutView);
				
			}
		}
		
		if(rsPessoaEmpresa != null && rsPessoaEmpresaRotina != null){
			//Habilita o checkbox das pessoa_empresa que possuem rotina no contexto
			for(int i = 0; i < rsPessoaEmpresa.getTotalTupla(); i++){
				String[] valores =  rsPessoaEmpresa.getTupla(i);
				String idPessoaEmpresa = valores[3];
				String token = rsPessoaEmpresaRotina.findRegistro(Table.ID_UNIVERSAL,  idPessoaEmpresa);
				//Se o registro foi encontrado
				if( token != null){
					CheckBox ck = (CheckBox)viewsPessoaEmpresa.get(i).findViewById(R.id.ck_selecionado);
					ck.setChecked(true);
				}
			}	
		}
	}

	@Override
	protected void loadEdit() {
		
		
		
	}

	@Override
	protected ContainerCadastro cadastrando() throws Exception {
		
		Table vObj = new EXTDAOPessoaEmpresaRotina(db);
		for(int i = 0 ; i < rsPessoaEmpresa.getTotalTupla(); i++){
			String[] valores = rsPessoaEmpresa.getTupla(i);
			View v = viewsPessoaEmpresa.get(i);
			CheckBox ckSelecionado = (CheckBox) v.findViewById(R.id.ck_selecionado);
			vObj.setId(null);
			
			vObj.setAttrValue(EXTDAOPessoaEmpresaRotina.PESSOA_EMPRESA_ID_INT, valores[3]);
			vObj.setAttrValue(EXTDAOPessoaEmpresaRotina.SEMANA_INT, String.valueOf( semanaRotina));
			vObj.setAttrValue(EXTDAOPessoaEmpresaRotina.DIA_SEMANA_INT, String.valueOf( diaSemana));
			vObj.setAttrValue(EXTDAOPessoaEmpresaRotina.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			Table registro =  vObj.getFirstTupla();
			if(ckSelecionado.isChecked()){
				if(registro == null){
					vObj.formatToSQLite();
					vObj.insert(true);	
				}	
			} else {
				
				if(registro != null){
					registro.remove(true);
				}
			}
		}
		
		return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);
		
	}



	@Override
	public boolean loadData() {
		try{
			db = new DatabasePontoEletronico(this);
			objUsuario = new EXTDAOUsuario(db);
			objUsuario.select(idUsuario);
			objPessoa = objUsuario.getObjPessoa();
			
			objPessoaEmpresa = new EXTDAOPessoaEmpresa(db);
			objPessoaEmpresa.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, objPessoa.getId());
			rsPessoaEmpresa = objPessoaEmpresa.getRegistrosDaPessoa(objPessoa.getId());
			
			objPessoaEmpresaRotina = new EXTDAOPessoaEmpresaRotina(db);
			
			rsPessoaEmpresaRotina = objPessoaEmpresaRotina.getRegistrosDaPessoa(
										objPessoa.getId(), 
										String.valueOf(semanaRotina), 
										String.valueOf(diaSemana));
			return true;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			return false;	
		}
	}	
}


