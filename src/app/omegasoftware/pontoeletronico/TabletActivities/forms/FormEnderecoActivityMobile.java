package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextView.TableDependentAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;

import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPais;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;

public class FormEnderecoActivityMobile extends OmegaRegularActivity {

	//Constants
	private static final String TAG = "FormEnderecoActivityMobile";

	public static String TABELAS_RELACIONADAS[] = {EXTDAOPais.NAME, EXTDAOUf.NAME, EXTDAOCidade.NAME};
	private final int FORM_ERROR_DIALOG_CADASTRO_OK = 3;
	private final int FORM_ERROR_DIALOG_CAMPO_INVALIDO = 4;
	private final int FORM_ERROR_DIALOG_MISSING_PAIS = 8;
	private final int FORM_ERROR_DIALOG_MISSING_ESTADO = 5;
	private final int FORM_ERROR_DIALOG_MISSING_CIDADE = 6;
	private final int ERROR_DIALOG_MISSING_CIDADE_E_LOGRADOURO = 7;


	//Search button
	private Button cadastrarButton;
	private Button cancelarButton;


	//Spinners
	private DatabasePontoEletronico db=null;
	private boolean isEdit = false;
	private Integer idLinearLayoutEnderecoRetorno = null;
	private EditText logradouroEditText;
	private EditText numeroEditText;


	String idEstadoInput = "";
	String idCidadeInput = "";


	String idEstadoTextView = "";
	String idCidadeTextView = "";

	String idPaisTextView = "";


	private AutoCompleteTextView cidadeAutoCompletEditText;
	private AutoCompleteTextView estadoAutoCompletEditText;
	private AutoCompleteTextView paisAutoCompletEditText;


	TableDependentAutoCompleteTextView cidadeContainerAutoComplete = null;
	TableDependentAutoCompleteTextView estadoContainerAutoComplete = null;
	TableAutoCompleteTextView paisContainerAutoComplete = null;

	OnFocusChangeListener paisOnFocusChangeListener;
	OnFocusChangeListener estadoOnFocusChangeListener;
	OnFocusChangeListener cidadeOnFocusChangeListener;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_endereco_activity_mobile_layout);

		Bundle vParameter = getIntent().getExtras();
		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT))
			idLinearLayoutEnderecoRetorno = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT);
		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_EDIT)){
			isEdit = vParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_EDIT);
		}

		

		new CustomDataLoader(this).execute();

	}

	private void loadEdit(){
		try{
		Bundle vParameter = getIntent().getExtras();
		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO)){
			logradouroEditText.setText(vParameter.getString(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO));	
		}

		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_NUMERO)){
			numeroEditText.setText(vParameter.getString(OmegaConfiguration.SEARCH_FIELD_NUMERO));	
		}



		String vNomeCidade = null;
		String vNomeEstado = null;
		String vNomePais = null;
		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE)){

			idCidadeInput = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE);
			if(idCidadeInput != null){
				EXTDAOCidade vObjCidade = new EXTDAOCidade(this.db);
				vObjCidade.setAttrValue(EXTDAOCidade.ID, idCidadeInput);
				if(vObjCidade.select()){
					vNomeCidade = vObjCidade.getStrValueOfAttribute(EXTDAOCidade.NOME);
					String vUfId= vObjCidade.getStrValueOfAttribute(EXTDAOCidade.UF_ID_INT);

					if(vUfId != null){
						EXTDAOUf vObjUF = new EXTDAOUf(db);
						vObjUF.setAttrValue(EXTDAOUf.ID, vUfId);
						if(vObjUF.select()){
							vNomeEstado = vObjUF.getStrValueOfAttribute(EXTDAOUf.NOME);
//							String vTokenEstadoEdit = estadoContainerAutoComplete.getTokenOfId(vUfId);
							
//							estadoOnFocusChangeListener.onFocusChange(null, false);
							String vPaisId = vObjUF.getStrValueOfAttribute(EXTDAOUf.PAIS_ID_INT);
							if(vPaisId != null){
								EXTDAOPais vObjPais = new EXTDAOPais(db);
								vObjPais.setAttrValue(EXTDAOPais.ID, vPaisId);
								
								if(vObjPais.select()){
									vNomePais = vObjPais.getStrValueOfAttribute(EXTDAOPais.NOME);
//									String vTokenPaisEdit = paisContainerAutoComplete.getTokenOfId(vPaisId);
									
//									paisOnFocusChangeListener.onFocusChange(null, false);
								}
							}
						}

					}
				}
			}
		}
		if(vNomePais != null){
			
			paisAutoCompletEditText.setText(vNomePais);
			paisOnFocusChangeListener.onFocusChange(null, false);
			paisAutoCompletEditText.clearFocus();
		}
		if(vNomeEstado != null){
			estadoAutoCompletEditText.setText(vNomeEstado);
			estadoOnFocusChangeListener.onFocusChange(null, false);
		}
		if(vNomeCidade!= null){
			cidadeAutoCompletEditText.setText(vNomeCidade);
			cidadeOnFocusChangeListener.onFocusChange(null, false);
		}

		cadastrarButton.setText(getResources().getString(R.string.atualizar));
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			
		}
	}

	@Override
	public boolean loadData() {
		return true;
	}

	@Override
	public void initializeComponents() throws OmegaDatabaseException {

		this.db = new DatabasePontoEletronico(this);

		this.logradouroEditText = (EditText) findViewById(R.id.logradouro_edittext);
		new MaskUpperCaseTextWatcher(this.logradouroEditText, 255);


		this.numeroEditText = (EditText) findViewById(R.id.numero_edittext);
		new MaskUpperCaseTextWatcher(this.numeroEditText, 30);


		//		EXTDAOPais vObjPais = new EXTDAOPais(this.db); 
		//		this.paisContainerAutoComplete = new TableAutoCompleteTextView(vObjPais); 
		//		ArrayAdapter<String> paisAdapter = new ArrayAdapter<String>(
		//				this,
		//				R.layout.spinner_item,
		//				paisContainerAutoComplete.getVetorStrId());
		//		this.paisAutoCompletEditText = (AutoCompleteTextView)
		//				findViewById(R.id.pais_autocompletetextview);
		//		paisAutoCompletEditText.setAdapter(paisAdapter);
		EXTDAOPais vObjPais = new EXTDAOPais(this.db);
		this.paisAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.pais_autocompletetextview);
		this.paisContainerAutoComplete = new TableAutoCompleteTextView(this, vObjPais, paisAutoCompletEditText);
		new MaskUpperCaseTextWatcher(paisAutoCompletEditText, 100);
		paisOnFocusChangeListener = new PaisOnItemSelectedListener();
		paisAutoCompletEditText.setOnFocusChangeListener(paisOnFocusChangeListener);

		EXTDAOUf vObjUf = new EXTDAOUf(this.db);
		this.estadoContainerAutoComplete = new TableDependentAutoCompleteTextView(vObjUf, EXTDAOUf.PAIS_ID_INT);
		this.estadoAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.estado_autocompletetextview);
		new MaskUpperCaseTextWatcher(estadoAutoCompletEditText, 100);
		estadoOnFocusChangeListener = new EstadoOnItemSelectedListener();
		estadoAutoCompletEditText.setOnFocusChangeListener(estadoOnFocusChangeListener);

		EXTDAOCidade vObjCidade = new EXTDAOCidade(this.db); 
		this.cidadeContainerAutoComplete = new TableDependentAutoCompleteTextView(vObjCidade, EXTDAOCidade.UF_ID_INT);
		this.cidadeAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.cidade_autocompletetextview);
		new MaskUpperCaseTextWatcher(cidadeAutoCompletEditText, 100);
		cidadeOnFocusChangeListener = new CidadeOnItemSelectedListener();
		cidadeAutoCompletEditText.setOnFocusChangeListener(cidadeOnFocusChangeListener);


		//Attach search button to its listener
		this.cadastrarButton = (Button) findViewById(R.id.cadastrar_dependente_button);
		this.cadastrarButton.setOnClickListener(new CadastroButtonListener(this));

		//Attach search button to its listener
		this.cancelarButton = (Button) findViewById(R.id.cancelar_button);
		this.cancelarButton.setOnClickListener(new CancelarButtonListener(this));

		if(isEdit)
			loadEdit();

		
	}

	private void showCitiesFromState(String pUfId)
	{
		ArrayAdapter<String> cidadeAdapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item,
				cidadeContainerAutoComplete.getVetorStrId(pUfId));
		cidadeAutoCompletEditText.setAdapter(cidadeAdapter);
	}

	private void showStateFromCountry(String pPaisId)
	{
		ArrayAdapter<String> estadoAdapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item,
				estadoContainerAutoComplete.getVetorStrId(pPaisId));
		estadoAutoCompletEditText.setAdapter(estadoAdapter);
	}

	private class CidadeOnItemSelectedListener implements OnFocusChangeListener {
		public void onFocusChange(View view, boolean hasFocus) {
			
			if(!hasFocus){
				String vCidade = cidadeAutoCompletEditText.getText().toString();
				if(vCidade != null){
					saveSelectedCidade(vCidade);
					String vIdCidade = cidadeContainerAutoComplete.getIdOfTokenIfExist(vCidade);
					if(vIdCidade != null){

						if(vIdCidade.compareTo(idCidadeTextView) != 0 ){

							idCidadeTextView = vIdCidade;
						}
					}
				}
			}
		}          
	}

	private class EstadoOnItemSelectedListener implements OnFocusChangeListener {
		public void onFocusChange(View view, boolean hasFocus) {
			
			if(!hasFocus){
				String vEstado = estadoAutoCompletEditText.getText().toString();
				if(vEstado != null){
					saveSelectedEstado(vEstado);
					String vIdEstado = estadoContainerAutoComplete.getIdOfTokenIfExist(vEstado);
					if(vIdEstado != null){
						boolean cidadeIsEmpty = cidadeContainerAutoComplete.isEmpty();
						//se a lista de estados possiveis estiver vazia ||
						//se o estado for igual ao antigo 
						//=> entao nao carrega novamente a lista de estado possiveis
						if(vIdEstado.compareTo(idEstadoTextView) != 0 || 
								cidadeIsEmpty){
							showCitiesFromState(vIdEstado);
							//se for iniciando edicao
							if(idCidadeInput != null){
								String vTokenCidade = cidadeContainerAutoComplete.getTokenOfId(idCidadeInput);
								if(vTokenCidade != null)
									cidadeAutoCompletEditText.setText(vTokenCidade);

								idCidadeInput = null;
							}
							idEstadoTextView = vIdEstado;
						}
					}
				}
			}
		}          
	}


	private class PaisOnItemSelectedListener implements OnFocusChangeListener {

		public void onFocusChange(View view, boolean hasFocus) {
			
			if(!hasFocus){
				String vPais = paisAutoCompletEditText.getText().toString();
				if(vPais != null){
					saveSelectedPais(vPais);
					String vIdPais = paisContainerAutoComplete.getIdOfTokenIfExist(vPais);
					if(vIdPais != null){
						boolean estadoIsEmpty = estadoContainerAutoComplete.isEmpty();
						//se a lista de estados possiveis estiver vazia ||
						//se o pais for igual ao antigo 
						//=> entao nao carrega novamente a lista de estado possiveis
						if(vIdPais.compareTo(idPaisTextView) != 0 || estadoIsEmpty){
							showStateFromCountry(vIdPais);
							//se for iniciando edicao
							if(idEstadoInput != null){
								//Tenta pegar o novo conteudo
								String vTokenEstado = estadoContainerAutoComplete.getTokenOfId(idEstadoInput);
								if(vTokenEstado != null){
									estadoAutoCompletEditText.setText(vTokenEstado);
									//forca o carregamento do bairro
									estadoOnFocusChangeListener.onFocusChange(null, false);
								}
								idEstadoInput = null;
							}
							idPaisTextView = vIdPais;
						}
					}
				}
			}
		}          
	}
	//Save Cidade ID in the preferences
	private void saveSelectedPais(String pPais)
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putString(OmegaConfiguration.FORM_PAIS, pPais);
		vEditor.commit();
	}
	//Save Cidade ID in the preferences
	private void saveSelectedEstado(String pEstado)
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putString(OmegaConfiguration.FORM_ESTADO, pEstado);
		vEditor.commit();
	}

	//Save Cidade ID in the preferences
	private void saveSelectedCidade(String pCidade)
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putString(OmegaConfiguration.FORM_CIDADE, pCidade);
		vEditor.commit();
	}
	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;

		switch (id) {
		default:
			vDialog = this.getErrorDialog(id);
			break;
		}
		return vDialog;
	}	

	@Override
	public void finish(){
		try{
			this.db.close();	
		}catch(Exception ex){
		}
		super.finish();
	}

	private Dialog getErrorDialog(int dialogType)
	{

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
		case FORM_ERROR_DIALOG_CADASTRO_OK:
			vTextView.setText(getResources().getString(R.string.cadastro_ok));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_CADASTRO_OK);
				}
			});
			break;
		case FORM_ERROR_DIALOG_CAMPO_INVALIDO:
			vTextView.setText(getResources().getString(R.string.error_invalid_field));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_CAMPO_INVALIDO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_ESTADO:
			vTextView.setText(getResources().getString(R.string.error_missing_estado));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_ESTADO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_PAIS:
			vTextView.setText(getResources().getString(R.string.error_missing_pais));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_PAIS);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_CIDADE:
			vTextView.setText(getResources().getString(R.string.error_missing_cidade));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_CIDADE);
				}
			});
			break;
		case ERROR_DIALOG_MISSING_CIDADE_E_LOGRADOURO:
			vTextView.setText(getResources().getString(R.string.cidade_ou_logradouro_obrigatorio));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(ERROR_DIALOG_MISSING_CIDADE_E_LOGRADOURO);
				}
			});
			break;

		default:
			return null;
		}

		return vDialog;
	}

	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onCadastroButtonClicked(Activity pActivity) {

//		controlerProgressDialog.createProgressDialog();


		boolean vValidade = true;
		String vIdPais =null;
		String vStrNomePais = this.paisAutoCompletEditText.getText().toString();
		if (vStrNomePais != null && vStrNomePais.length() > 0) {
			EXTDAOPais vObjPais = new EXTDAOPais(this.db);
			vObjPais.setAttrValue(EXTDAOPais.NOME, vStrNomePais);
			vIdPais = paisContainerAutoComplete.addTupleIfNecessay(vObjPais);
			if (vIdPais == null) vValidade = false;
		}

		String vIdEstado =null;
		String vStrNomeEstado = this.estadoAutoCompletEditText.getText().toString();
		if (vStrNomeEstado != null && vStrNomeEstado.length() > 0){
			EXTDAOUf vObjEstado = new EXTDAOUf(this.db);
			vObjEstado.setAttrValue(EXTDAOUf.NOME, vStrNomeEstado);
			vObjEstado.setAttrValue(EXTDAOUf.PAIS_ID_INT, vIdPais);
			vIdEstado = estadoContainerAutoComplete.addTupleIfNecessay(vObjEstado);
			if(vIdEstado == null) vValidade = false;
		}
		String selectedLogradouro = "";
		if(!this.logradouroEditText.getText().toString().equals(""))
		{
			selectedLogradouro = this.logradouroEditText.getText().toString();
		}

		String vStrNomeCidade = this.cidadeAutoCompletEditText.getText().toString();
		if(vStrNomeCidade.length() == 0 && selectedLogradouro.length() > 0 )
		{
			cidadeAutoCompletEditText.requestFocus();
			showDialog(ERROR_DIALOG_MISSING_CIDADE_E_LOGRADOURO);
			return;
		}
		String vIdCidade =null;
		if(vStrNomeCidade.length() > 0 ){
			EXTDAOCidade vObjCidade = new EXTDAOCidade(this.db);
			vObjCidade.setAttrValue(EXTDAOCidade.NOME, vStrNomeCidade);
			vObjCidade.setAttrValue(EXTDAOCidade.UF_ID_INT, vIdEstado);
			vIdCidade = cidadeContainerAutoComplete.addTupleIfNecessay(vObjCidade);
			if(vIdCidade == null) vValidade = false;
		}

		String selectedNumero = "";
		if(!this.numeroEditText.getText().toString().equals(""))
		{
			selectedNumero = this.numeroEditText.getText().toString();
		}

		if(vValidade){
			Intent intent = getIntent();
			if(vIdPais != null && vIdPais.length() > 0 )
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PAIS, vIdPais);
			if(vIdEstado != null && vIdEstado.length() > 0 )
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO, vIdEstado);
			if(vIdCidade != null && vIdCidade.length() > 0 )
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE, vIdCidade);
			if(vStrNomeCidade != null && vStrNomeCidade.length() > 0 )
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE, vStrNomeCidade);
			if(vStrNomeEstado != null && vStrNomeEstado.length() > 0 )
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO, vStrNomeEstado);
			if(vStrNomePais != null && vStrNomePais.length() > 0 )
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS, vStrNomePais);
			if(selectedLogradouro != null && selectedLogradouro.length() > 0 )
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO, selectedLogradouro);
			if(selectedNumero != null && selectedNumero.length() > 0 )
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO, selectedNumero );

			if(idLinearLayoutEnderecoRetorno != null)
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, idLinearLayoutEnderecoRetorno);
			setResult(RESULT_OK, intent);

			finish( );
		}
	}


	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------


	private class CancelarButtonListener implements OnClickListener
	{

		Activity activity;

		public CancelarButtonListener(Activity pActivity){
			activity = pActivity;
		}
		public void onClick(View v) {
			activity.finish();

		}
	}	

	private class CadastroButtonListener implements OnClickListener
	{
		Activity activity;
		public CadastroButtonListener(Activity pActivity){
			activity = pActivity;
		}

		public void onClick(View v) {
			onCadastroButtonClicked(activity);

		}

	}	

}


