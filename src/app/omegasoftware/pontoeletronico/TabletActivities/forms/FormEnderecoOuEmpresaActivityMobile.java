package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextView.TableDependentAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;


import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPais;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;

public class FormEnderecoOuEmpresaActivityMobile extends OmegaRegularActivity {

	//Constants
	private static final String TAG = "FormEnderecoOuEmpresaActivityMobile";
	//LinkedHashMap<String, String> hashIdentificador = null;

	private final int FORM_ERROR_DIALOG_CADASTRO_OK = 3;
	private final int FORM_ERROR_DIALOG_CAMPO_INVALIDO = 4;
	private final int DIALOG_CIDADE_OU_LOGRADOURO_OBRIGATORIO = 5;


	Spinner spTipoEndereco= null;
	CustomSpinnerAdapter tipoTipoEnderecoCustomSpinnerAdapter = null;
	//Spinner localOrigemSpinnerSpinner =null;
	AutoCompleteTextView localOrigemAutoComplete;


	//Search button
	private Button cadastrarButton;
	private Button cancelarButton;
	LoadEnderecoHandler handlerEndereco;
	LoadTipoEnderecoOrigemHandler handlerTipoEnderecoOrigem;
	//Spinners
	private DatabasePontoEletronico db=null;
	private boolean isEdit = false;
	private Integer idLinearLayoutEnderecoRetorno = null;
	private EditText logradouroEditText;
	private EditText numeroEditText;

	ImageButton ibTipoEndereco = null;
	String idEstadoInput = "";
	String idCidadeInput = "";
	Button btTipoEndereco = null;


			String idEstadoTextView = "";
	String idCidadeTextView = "";

	String idPaisTextView = "";
	String idIdentificadorTextView = "";


	private AutoCompleteTextView cidadeAutoCompletEditText;
	private AutoCompleteTextView estadoAutoCompletEditText;
	private AutoCompleteTextView paisAutoCompletEditText;


	TableDependentAutoCompleteTextView cidadeContainerAutoComplete = null;
	TableDependentAutoCompleteTextView estadoContainerAutoComplete = null;
	TableAutoCompleteTextView paisContainerAutoComplete = null;
	TableAutoCompleteTextView localOrigemContainerAutoComplete = null;

	View.OnFocusChangeListener paisOnFocusChangeListener;
	View.OnFocusChangeListener estadoOnFocusChangeListener;
	View.OnFocusChangeListener cidadeOnFocusChangeListener;




	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_endereco_pessoa_ou_empresa_activity_mobile_layout);

		Bundle vParameter = getIntent().getExtras();
		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT))
			idLinearLayoutEnderecoRetorno = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT);
		if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_EDIT)){
			isEdit = vParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_EDIT);
		}

		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}

	private void loadEdit(){
		try{
			Bundle vParameter = getIntent().getExtras();
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO)){
				logradouroEditText.setText(vParameter.getString(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO));
			}

			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_NUMERO)){
				numeroEditText.setText(vParameter.getString(OmegaConfiguration.SEARCH_FIELD_NUMERO));
			}


			String vNomeCidade = null;
			String vNomeEstado = null;
			String vNomePais = null;
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE)){

				idCidadeInput = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE);
				if(idCidadeInput != null){
					EXTDAOCidade vObjCidade = new EXTDAOCidade(this.db);
					vObjCidade.setAttrValue(EXTDAOCidade.ID, idCidadeInput);
					if(vObjCidade.select()){
						vNomeCidade = vObjCidade.getStrValueOfAttribute(EXTDAOCidade.NOME);
						String vUfId= vObjCidade.getStrValueOfAttribute(EXTDAOCidade.UF_ID_INT);

						if(vUfId != null){
							EXTDAOUf vObjUF = new EXTDAOUf(db);
							vObjUF.setAttrValue(EXTDAOUf.ID, vUfId);
							if(vObjUF.select()){
								vNomeEstado = vObjUF.getStrValueOfAttribute(EXTDAOUf.NOME);
//							String vTokenEstadoEdit = estadoContainerAutoComplete.getTokenOfId(vUfId);

//							estadoOnFocusChangeListener.onFocusChange(null, false);
								String vPaisId = vObjUF.getStrValueOfAttribute(EXTDAOUf.PAIS_ID_INT);
								if(vPaisId != null){
									EXTDAOPais vObjPais = new EXTDAOPais(db);
									vObjPais.setAttrValue(EXTDAOPais.ID, vPaisId);

									if(vObjPais.select()){
										vNomePais = vObjPais.getStrValueOfAttribute(EXTDAOPais.NOME);
//									String vTokenPaisEdit = paisContainerAutoComplete.getTokenOfId(vPaisId);

//									paisOnFocusChangeListener.onFocusChange(null, false);
									}
								}
							}

						}
					}
				}
			}
			if(vNomePais != null){

				paisAutoCompletEditText.setText(vNomePais);
				paisOnFocusChangeListener.onFocusChange(null, false);
				paisAutoCompletEditText.clearFocus();
			}
			if(vNomeEstado != null){
				estadoAutoCompletEditText.setText(vNomeEstado);
				estadoOnFocusChangeListener.onFocusChange(null, false);
			}
			if(vNomeCidade!= null){
				cidadeAutoCompletEditText.setText(vNomeCidade);
				cidadeOnFocusChangeListener.onFocusChange(null, false);
			}


			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_TIPO_ENDERECO)){
				int idTipoEndereco = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_ID_TIPO_ENDERECO);
				spTipoEndereco.setSelection(idTipoEndereco);
				if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_NOME)){
					String nome = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_NOME);
					localOrigemAutoComplete.setText(nome);
				}
			}

			cadastrarButton.setText(getResources().getString(R.string.atualizar));
			handlerTipoEnderecoOrigem.sendEmptyMessage(0);
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);

		}
	}

	@Override
	public boolean loadData() {
		return true;
	}

	@Override
	public void initializeComponents() throws OmegaDatabaseException {

		handlerEndereco=new LoadEnderecoHandler();
		handlerTipoEnderecoOrigem = new LoadTipoEnderecoOrigemHandler();

		this.db = new DatabasePontoEletronico(this);

		this.logradouroEditText = (EditText) findViewById(R.id.logradouro_edittext);

		this.numeroEditText = (EditText) findViewById(R.id.numero_edittext);
		new MaskUpperCaseTextWatcher(this.numeroEditText, 30);


		EXTDAOPais vObjPais = new EXTDAOPais(this.db);
		this.paisAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.pais_autocompletetextview);
		this.paisContainerAutoComplete = new TableAutoCompleteTextView(this, vObjPais, paisAutoCompletEditText);
		new MaskUpperCaseTextWatcher(paisAutoCompletEditText, 100);
		paisOnFocusChangeListener = new PaisOnItemSelectedListener();
		paisAutoCompletEditText.setOnFocusChangeListener(paisOnFocusChangeListener);

		EXTDAOUf vObjUf = new EXTDAOUf(this.db);
		this.estadoContainerAutoComplete = new TableDependentAutoCompleteTextView(vObjUf, EXTDAOUf.PAIS_ID_INT);
		this.estadoAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.estado_autocompletetextview);
		new MaskUpperCaseTextWatcher(estadoAutoCompletEditText, 100);
		estadoOnFocusChangeListener = new EstadoOnItemSelectedListener();
		estadoAutoCompletEditText.setOnFocusChangeListener(estadoOnFocusChangeListener);

		EXTDAOCidade vObjCidade = new EXTDAOCidade(this.db);
		this.cidadeContainerAutoComplete = new TableDependentAutoCompleteTextView(vObjCidade, EXTDAOCidade.UF_ID_INT);
		this.cidadeAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.cidade_autocompletetextview);
		new MaskUpperCaseTextWatcher(cidadeAutoCompletEditText, 100);
		cidadeOnFocusChangeListener = new CidadeOnItemSelectedListener();
		cidadeAutoCompletEditText.setOnFocusChangeListener(cidadeOnFocusChangeListener);


		//Attach search button to its listener
		this.cadastrarButton = (Button) findViewById(R.id.cadastrar_dependente_button);
		this.cadastrarButton.setOnClickListener(new CadastroButtonListener(this));

		//Attach search button to its listener
		this.cancelarButton = (Button) findViewById(R.id.cancelar_button);
		this.cancelarButton.setOnClickListener(new CancelarButtonListener(this));

		spTipoEndereco = (Spinner) findViewById(R.id.sp_tipo_endereco);
		tipoTipoEnderecoCustomSpinnerAdapter  = new CustomSpinnerAdapter(
				getApplicationContext(),
				EXTDAOTarefa.getAllTipoEndereco(this),
				R.layout.spinner_item,
				R.string.selecione);

		spTipoEndereco.setAdapter(tipoTipoEnderecoCustomSpinnerAdapter);
		((CustomSpinnerAdapter) spTipoEndereco.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
		spTipoEndereco.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				handlerTipoEnderecoOrigem.sendEmptyMessage(0);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
		btTipoEndereco = ((Button) findViewById(R.id.bt_tipo_endereco));
		btTipoEndereco.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				spTipoEndereco.performClick();

			}
		});


 		ibTipoEndereco = (ImageButton) findViewById(R.id.ib_tipo_endereco);
		ibTipoEndereco.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				spTipoEndereco.performClick();
			}
		});

		localOrigemAutoComplete = (AutoCompleteTextView) findViewById(R.id.local_origem_autocomplete);
		localOrigemAutoComplete.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				handlerEndereco.sendEmptyMessage(0);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				Toast.makeText(FormEnderecoOuEmpresaActivityMobile.this, R.string.local_nao_encontrado, Toast.LENGTH_SHORT);
			}
		});


		//localOrigemSpinnerSpinner = (Spinner) findViewById(R.id.local_origem_spinner);

//		localOrigemSpinnerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
//
//			@Override
//			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//				handlerEndereco.sendEmptyMessage(0);
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> parent) {
//
//			}
//		});

		if(isEdit)
			loadEdit();
	}

	public class LoadEnderecoHandler extends Handler{

		@Override
		public void handleMessage(Message msg)
		{
			try{

				String strTipo = String.valueOf(spTipoEndereco.getSelectedItemId());
				if(!strTipo.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
				{
					EXTDAOTarefa.TIPO_ENDERECO vTipoTarefa = EXTDAOTarefa.getTipoEnderecoById(Integer.valueOf(strTipo));
					//String destinoEndereco = String.valueOf(localOrigemSpinnerSpinner.getSelectedItemId());


					String local= localOrigemAutoComplete.getText().toString();

					String idIdentificador= localOrigemContainerAutoComplete.getIdOfTokenIfExist(local);

					if(idIdentificador != null){

						FormEnderecoOuEmpresaActivityMobile.this.idIdentificadorTextView = idIdentificador;
						String vIdCidade = null;
						String vIdEstado = null;
						String vIdPais = null;

						String vNomeCidade = null;
						String vNomeEstado = null;
						String vNomePais = null;
						String vLogradouro = null;

						String vNumero = null;
						Table vObj = null;
						switch (vTipoTarefa) {
							case EMPRESA:

								vObj = new EXTDAOEmpresa(db);
								vObj.setAttrValue(EXTDAOEmpresa.ID, idIdentificador);

								break;
							case PESSOA:
								vObj = new EXTDAOPessoa(db);
								vObj.setAttrValue(EXTDAOPessoa.ID, idIdentificador);
								break;
						}
						if(vObj != null && vObj.select()){

							vIdCidade = vObj.getStrValueOfAttribute(Table.CIDADE_ID_INT_UNIVERSAL);
							if(vIdCidade != null){
								String[] valores = EXTDAOCidade.getNomeCidadeEEstado(db, vIdCidade);
								if(valores != null && valores.length > 0){
									vNomeCidade = valores[0];
									vNomeEstado = valores[1];
									vNomePais = valores[2];
									vIdCidade = valores[3];
									vIdEstado = valores[4];
									vIdPais  = valores[5];
								}
							}
							vLogradouro = vObj.getStrValueOfAttribute(Table.LOGRADOURO_UNIVERSAL);

							vNumero = vObj.getStrValueOfAttribute(Table.NUMERO_UNIVERSAL);

							if((vLogradouro != null && vLogradouro.length() > 0)
									|| (vNomeEstado != null && vNomeEstado.length() > 0 )
									|| (vNomePais != null && vNomePais.length() > 0 )
									|| (vNumero != null && vNumero.length() > 0 )
									|| (vLogradouro != null && vLogradouro.length() > 0)
									){
								//logradouroEditText.setText(vLogradouro);
								vNomePais=vNomePais == null ? "" : vNomePais;
								paisAutoCompletEditText.setText(vNomePais);

								vNomeEstado =vNomeEstado == null ? "" : vNomeEstado ;
								idEstadoInput = vIdEstado;
								estadoAutoCompletEditText.setText(vNomeEstado);


								vNomeCidade=vNomeCidade == null ? "" : vNomeCidade;
								idCidadeInput = vIdCidade;
								cidadeAutoCompletEditText.setText(vNomeCidade);

								vLogradouro =vLogradouro == null ? "" : vLogradouro ;
								logradouroEditText.setText(vLogradouro);

								vNumero=vNumero== null ? "" : vNumero;
								numeroEditText.setText(vNumero);

								Toast.makeText(
										FormEnderecoOuEmpresaActivityMobile.this
										, R.string.local_setado
										, Toast.LENGTH_SHORT).show();
							} else {
								Toast.makeText(
										FormEnderecoOuEmpresaActivityMobile.this
										, R.string.local_nao_encontrado
										, Toast.LENGTH_SHORT).show();
							}

							//onActivityResult(OmegaConfiguration.ACTIVITY_FORM_ENDERECO, OmegaConfiguration.ACTIVITY_FORM_ADD_LAYOUT_ENDERECO, vIntent) ;

						}
						db.close();

					}

				} else{
					Toast.makeText(FormEnderecoOuEmpresaActivityMobile.this, R.string.local_nao_encontrado, Toast.LENGTH_SHORT);
				}
			}catch(Exception ex){
				SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);
			}
		}
	}


	private void showCitiesFromState(String pUfId)
	{
		ArrayAdapter<String> cidadeAdapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item,
				cidadeContainerAutoComplete.getVetorStrId(pUfId));
		cidadeAutoCompletEditText.setAdapter(cidadeAdapter);
	}

	private void showStateFromCountry(String pPaisId)
	{
		ArrayAdapter<String> estadoAdapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item,
				estadoContainerAutoComplete.getVetorStrId(pPaisId));
		estadoAutoCompletEditText.setAdapter(estadoAdapter);
	}

	private class CidadeOnItemSelectedListener implements View.OnFocusChangeListener {
		public void onFocusChange(View view, boolean hasFocus) {

			if(!hasFocus){
				String vCidade = cidadeAutoCompletEditText.getText().toString();
				if(vCidade != null){
					saveSelectedCidade(vCidade);
					String vIdCidade = cidadeContainerAutoComplete.getIdOfTokenIfExist(vCidade);
					if(vIdCidade != null){
						idCidadeTextView = vIdCidade;

					}
				}
			}
		}
	}

	private class EstadoOnItemSelectedListener implements View.OnFocusChangeListener {
		public void onFocusChange(View view, boolean hasFocus) {

			if(!hasFocus){
				String vEstado = estadoAutoCompletEditText.getText().toString();
				if(vEstado != null){
					saveSelectedEstado(vEstado);
					String vIdEstado = estadoContainerAutoComplete.getIdOfTokenIfExist(vEstado);
					if(vIdEstado != null){
						boolean cidadeIsEmpty = cidadeContainerAutoComplete.isEmpty();
						//se a lista de estados possiveis estiver vazia ||
						//se o estado for igual ao antigo
						//=> entao nao carrega novamente a lista de estado possiveis
						if(vIdEstado.compareTo(idEstadoTextView) != 0 ||
								cidadeIsEmpty){
							showCitiesFromState(vIdEstado);
							//se for iniciando edicao
							if(idCidadeInput != null){
								String vTokenCidade = cidadeContainerAutoComplete.getTokenOfId(idCidadeInput);
								if(vTokenCidade != null)
									cidadeAutoCompletEditText.setText(vTokenCidade);

								idCidadeInput = null;
							}
							idEstadoTextView = vIdEstado;
						}
					}
				}
			}
		}
	}


	private class PaisOnItemSelectedListener implements View.OnFocusChangeListener {

		public void onFocusChange(View view, boolean hasFocus) {

			if(!hasFocus){
				String vPais = paisAutoCompletEditText.getText().toString();
				if(vPais != null){
					saveSelectedPais(vPais);
					String vIdPais = paisContainerAutoComplete.getIdOfTokenIfExist(vPais);
					if(vIdPais != null){
						boolean estadoIsEmpty = estadoContainerAutoComplete.isEmpty();
						//se a lista de estados possiveis estiver vazia ||
						//se o pais for igual ao antigo
						//=> entao nao carrega novamente a lista de estado possiveis
						if(vIdPais.compareTo(idPaisTextView) != 0 || estadoIsEmpty){
							showStateFromCountry(vIdPais);
							//se for iniciando edicao
							if(idEstadoInput != null){
								//Tenta pegar o novo conteudo
								String vTokenEstado = estadoContainerAutoComplete.getTokenOfId(idEstadoInput);
								if(vTokenEstado != null){
									estadoAutoCompletEditText.setText(vTokenEstado);
									//forca o carregamento do
									estadoOnFocusChangeListener.onFocusChange(null, false);
								}
								idEstadoInput = null;
							}
							idPaisTextView = vIdPais;
						}
					}
				}
			}
		}
	}
	//Save Cidade ID in the preferences
	private void saveSelectedPais(String pPais)
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putString(OmegaConfiguration.FORM_PAIS, pPais);
		vEditor.commit();
	}

	private void saveSelectedIdentificador(String token)
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putString(OmegaConfiguration.FORM_IDENTIFICADOR, token);
		vEditor.commit();
	}
	//Save Cidade ID in the preferences
	private void saveSelectedEstado(String pEstado)
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putString(OmegaConfiguration.FORM_ESTADO, pEstado);
		vEditor.commit();
	}

	//Save Cidade ID in the preferences
	private void saveSelectedCidade(String pCidade)
	{
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putString(OmegaConfiguration.FORM_CIDADE, pCidade);
		vEditor.commit();
	}
	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;

		switch (id) {
			default:
				vDialog = this.getErrorDialog(id);
				break;
		}
		if(vDialog == null){
			return super.onCreateDialog(id);
		}
		else return vDialog;
	}

	private Dialog getErrorDialog(int dialogType)
	{
		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
			case FORM_ERROR_DIALOG_CADASTRO_OK:
				vTextView.setText(getResources().getString(R.string.cadastro_ok));
				vOkButton.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_CADASTRO_OK);
					}
				});
				break;
			case FORM_ERROR_DIALOG_CAMPO_INVALIDO:
				vTextView.setText(getResources().getString(R.string.error_invalid_field));
				vOkButton.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_CAMPO_INVALIDO);
					}
				});
				break;
			case DIALOG_CIDADE_OU_LOGRADOURO_OBRIGATORIO:
				vTextView.setText(getResources().getString(R.string.cidade_ou_logradouro_obrigatorio));
				vOkButton.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						dismissDialog(DIALOG_CIDADE_OU_LOGRADOURO_OBRIGATORIO);
					}
				});
				break;

			default:
				return null;
		}

		return vDialog;
	}

	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onCadastroButtonClicked(Activity pActivity)
	{

//		controlerProgressDialog.createProgressDialog();

		String vIdEstado =null;
		String vIdPais = null;
		String vIdCidade = null;
		boolean vValidade = true;

		String vStrNomePais = this.paisAutoCompletEditText.getText().toString();
		if(vStrNomePais.length() > 0){
			EXTDAOPais vObjPais = new EXTDAOPais(this.db);
			vObjPais.setAttrValue(EXTDAOPais.NOME, vStrNomePais);
			vIdPais = paisContainerAutoComplete.addTupleIfNecessay(vObjPais);
			if(vIdPais == null) vValidade = false;
		}


		String vStrNomeEstado = this.estadoAutoCompletEditText.getText().toString();
	if(vStrNomeEstado .length()>0){
		EXTDAOUf vObjEstado = new EXTDAOUf(this.db);
		vObjEstado.setAttrValue(EXTDAOUf.NOME, vStrNomeEstado);
		vObjEstado.setAttrValue(EXTDAOUf.PAIS_ID_INT, vIdPais);
		vIdEstado = estadoContainerAutoComplete.addTupleIfNecessay(vObjEstado);
		if(vIdEstado == null) vValidade = false;

	}

		String vStrNomeCidade = this.cidadeAutoCompletEditText.getText().toString();
		if(vStrNomeCidade.length() > 0 ){
			EXTDAOCidade vObjCidade = new EXTDAOCidade(this.db);
			vObjCidade.setAttrValue(EXTDAOCidade.NOME, vStrNomeCidade);
			vObjCidade.setAttrValue(EXTDAOCidade.UF_ID_INT, vIdEstado);
			vIdCidade = cidadeContainerAutoComplete.addTupleIfNecessay(vObjCidade);

		}



		String selectedLogradouro = "";
		if(!this.logradouroEditText.getText().toString().equals(""))
		{
			selectedLogradouro = this.logradouroEditText.getText().toString();
		}
		// ou a cidade ou o logradouro devem ser preenchidos, pois a tarefa por exemplo só tem FK para cidade, e o campo logradouro
		if(vStrNomeCidade.length()==0 && selectedLogradouro.length() == 0 ){
			showDialog(DIALOG_CIDADE_OU_LOGRADOURO_OBRIGATORIO);
			return;
		}

		String selectedNumero = "";
		if(!this.numeroEditText.getText().toString().equals(""))
		{
			selectedNumero = this.numeroEditText.getText().toString();
		}

		if(!vValidade){
			Toast.makeText(this, R.string.erro_fatal, Toast.LENGTH_SHORT);
			return;
		}

		Intent intent = getIntent();
		if(vIdPais != null && vIdPais.length() > 0 )
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PAIS, vIdPais);
		if(vIdEstado != null && vIdEstado.length() > 0 )
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO, vIdEstado);
		if(vIdCidade != null && vIdCidade.length() > 0 )
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE, vIdCidade);
		if(vStrNomeCidade != null && vStrNomeCidade.length() > 0 )
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE, vStrNomeCidade);
		if(vStrNomeEstado != null && vStrNomeEstado.length() > 0 )
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO, vStrNomeEstado);
		if(vStrNomePais != null && vStrNomePais.length() > 0 )
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS, vStrNomePais);
		if(selectedLogradouro != null && selectedLogradouro.length() > 0 )
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO, selectedLogradouro);
		if(selectedNumero != null && selectedNumero.length() > 0 )
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO, selectedNumero );

		if(idIdentificadorTextView != null && idIdentificadorTextView.length() > 0 ) {
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_TIPO_ENDERECO_TAREFA, getTipoEnderecoTarefaAtual().getId());
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, idIdentificadorTextView);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME, localOrigemContainerAutoComplete.getAutoCompleteEditText().getText().toString());
		}


		if(idLinearLayoutEnderecoRetorno != null)
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, idLinearLayoutEnderecoRetorno);
		setResult(RESULT_OK, intent);

		finish( );



	}


	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------


	private class CancelarButtonListener implements View.OnClickListener
	{

		Activity activity;

		public CancelarButtonListener(Activity pActivity){
			activity = pActivity;
		}
		public void onClick(View v) {
			activity.finish();

		}
	}

	private class CadastroButtonListener implements View.OnClickListener
	{
		Activity activity;
		public CadastroButtonListener(Activity pActivity){
			activity = pActivity;
		}

		public void onClick(View v) {
			onCadastroButtonClicked(activity);

		}

	}


	//	private class DeleteViewEnderecoButtonListener implements OnClickListener
	//	{
	//		View view= null;
	//
	//		public DeleteViewEnderecoButtonListener(View pView){
	//			view = pView;
	//		}
	//
	//		public void onClick(View v) {
	//			LinearLayout linearLayoutEmpresaFuncionario = (LinearLayout) findViewById(R.id.linearlayout_endereco);
	//			linearLayoutEmpresaFuncionario.removeView(view);
	//		}
	//	}
	public EXTDAOTarefa.TIPO_ENDERECO getTipoEnderecoTarefaAtual(){
		String strTipo = String.valueOf(spTipoEndereco.getSelectedItemId());
		if(!strTipo.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB)) {

			EXTDAOTarefa.TIPO_ENDERECO vTipoTarefa = EXTDAOTarefa.getTipoEnderecoById(Integer.valueOf(strTipo));
			return vTipoTarefa;
		} else return null;
	}
	public void refreshTipoEndereco(){
		EXTDAOTarefa.TIPO_ENDERECO vTipoTarefa = getTipoEnderecoTarefaAtual();
		if(vTipoTarefa != null)
		{
			Table objIdentificador = null;
			switch (vTipoTarefa) {
				case EMPRESA:
					ibTipoEndereco.setImageDrawable( getResources().getDrawable(
							R.drawable.map_company));
					objIdentificador = new EXTDAOEmpresa(db);
					//hashIdentificador = EXTDAOEmpresa.getAllEmpresa(db);
					break;
				case PESSOA:
					ibTipoEndereco.setImageDrawable( getResources().getDrawable(
							R.drawable.map_male));
					//hashIdentificador = EXTDAOPessoa.getAllPessoa(db);
					objIdentificador = new EXTDAOPessoa(db);
					break;
				default:
					//hashIdentificador = null;
					objIdentificador = null;
					ibTipoEndereco.setImageDrawable( getResources().getDrawable(
							R.drawable.map_text));
					break;
			}

			if(objIdentificador != null){


				if(localOrigemContainerAutoComplete == null) {

					localOrigemContainerAutoComplete = new TableAutoCompleteTextView(
							FormEnderecoOuEmpresaActivityMobile.this,
							objIdentificador,
							localOrigemAutoComplete
					);

					ArrayAdapter<String> identificadorAdapter = new ArrayAdapter<String>(
							FormEnderecoOuEmpresaActivityMobile.this,
							R.layout.spinner_item,
							localOrigemContainerAutoComplete.getVetorStrId());
					localOrigemAutoComplete.setAdapter(identificadorAdapter);


					localOrigemAutoComplete.setOnFocusChangeListener(new View.OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if(!hasFocus){

								String token = localOrigemAutoComplete.getText().toString();
								if(token != null){
									String idIdentificador = localOrigemContainerAutoComplete.getIdOfTokenIfExist(token);
									if(idIdentificador != null){
										handlerEndereco.sendEmptyMessage(0);
									}
								}
							}
						}
					});

					localOrigemAutoComplete.setOnTouchListener(new View.OnTouchListener() {

						@SuppressLint("ClickableViewAccessibility")
						@Override
						public boolean onTouch(View paramView, MotionEvent paramMotionEvent) {

							localOrigemAutoComplete.showDropDown();
							localOrigemAutoComplete.requestFocus();
							return false;
						}
					});


					new MaskUpperCaseTextWatcher(localOrigemAutoComplete, 255);
				}
				else {
					localOrigemContainerAutoComplete.refreshData(objIdentificador);

				}
				localOrigemAutoComplete.setVisibility(View.VISIBLE);
				//localOrigemSpinnerSpinner.setVisibility(View.VISIBLE);
				btTipoEndereco.setVisibility(View.GONE);

				//localOrigemAutoComplete.performClick();

			} else {
				localOrigemAutoComplete.setVisibility(View.GONE);
				btTipoEndereco.setVisibility(View.VISIBLE);
			}

		} else{
			localOrigemAutoComplete.setText("");

			localOrigemAutoComplete.setVisibility(View.GONE);
			btTipoEndereco.setVisibility(View.VISIBLE);
			ibTipoEndereco.setImageDrawable( getResources().getDrawable(
					R.drawable.map_text));
			localOrigemAutoComplete.clearListSelection();

			localOrigemAutoComplete.setAdapter(null);

		}
		db.close();
	}
	public class LoadTipoEnderecoOrigemHandler extends Handler {


		public LoadTipoEnderecoOrigemHandler(){

		}
		@Override
		public void handleMessage(Message msg)
		{
			refreshTipoEndereco();

		}
	}

}


