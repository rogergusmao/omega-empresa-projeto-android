package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.util.LinkedHashMap;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;

public class FormEntidadeDependenteActivity extends OmegaRegularActivity {

	//Constants
	private static final String TAG = "FormEntidadeDependenteActivity";

	
	private final int FORM_ERROR_DIALOG_CADASTRO_OK = 3;
	private final int FORM_ERROR_DIALOG_CAMPO_INVALIDO = 4;
	private final int FORM_ERROR_DIALOG_MISSING_SELECT = 5;
	

	//Search button
	private Button cadastrarButton;
	private Button cancelarButton;
	Drawable originalDrawable;

	//Spinners
	private DatabasePontoEletronico db=null;
	
	private Spinner entidadeSpinner;

	Table obj;

	String tabela;
	String atributoDescricao = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_entidade_dependente_layout);

		Intent intent =  getIntent();
		if(intent != null){
			tabela = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_TABELA);
			if(intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_ATRIBUTO_DESCRICAO)){
				atributoDescricao = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ATRIBUTO_DESCRICAO);
			}
			
		}
		new CustomDataLoader(this).execute();
	}
	LinkedHashMap<String, String> hash ;
	@Override
	public boolean loadData() throws OmegaDatabaseException {
		db = new DatabasePontoEletronico(this);
		obj = db.factoryTable(tabela);
		if(atributoDescricao != null)
			hash = obj.getHashMapIdByDefinition(false, atributoDescricao);
		else 
			hash = obj.getHashMapIdByDefinition(false);
		
		return true;
	}


	@Override
	public void initializeComponents() {

		
		this.entidadeSpinner = (Spinner) findViewById(R.id.entidade_spinner);
		
		this.entidadeSpinner.setAdapter(new CustomSpinnerAdapter(
				getApplicationContext(), 
				hash, 
				R.layout.spinner_item,
				R.string.selecione));
		
		((CustomSpinnerAdapter) this.entidadeSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		//Attach search button to its listener
		this.cadastrarButton = (Button) findViewById(R.id.cadastrar_dependente_button);
		this.cadastrarButton.setOnClickListener(new CadastroButtonListener());

		//Attach search button to its listener
		this.cancelarButton = (Button) findViewById(R.id.cancelar_button);
		this.cancelarButton.setOnClickListener(new CancelarButtonListener());
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;

		
		vDialog = this.getErrorDialog(id);
		if(vDialog == null){
			vDialog = super.onCreateDialog(id);
		}
		return vDialog;
	}	

	private Dialog getErrorDialog(int dialogType)
	{

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
		case FORM_ERROR_DIALOG_CADASTRO_OK:
			vTextView.setText(getResources().getString(R.string.cadastro_ok));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_CADASTRO_OK);
				}
			});
			break;
		case FORM_ERROR_DIALOG_CAMPO_INVALIDO:
			vTextView.setText(getResources().getString(R.string.error_invalid_field));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_CAMPO_INVALIDO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_SELECT:
			vTextView.setText(getResources().getString(R.string.error_missing_entidade));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_SELECT);
				}
			});
			break;
		default:
			return null;
		}

		return vDialog;
	}

	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onCadastroButtonClicked(Activity pActivity){
		
		boolean vValidade = true;
		
		//First we check the obligatory fields:
		//Plano, Especialidade, Cidade
		//Checking Cidade
		String selected = String.valueOf(this.entidadeSpinner.getSelectedItemId());
		String nome = (String) this.entidadeSpinner.getSelectedItem();
		if(selected.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
		{	
			showDialog(FORM_ERROR_DIALOG_MISSING_SELECT);
			return;
		} 

		
		if(vValidade){
			Intent intent = getIntent();
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, selected);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_CONTEUDO_DESCRITIVO, nome);

			setResult(OmegaConfiguration.ACTIVITY_FORM_EMPRESA_FUNCIONARIO, intent);
			
			pActivity.finish( );
		}	
		else{
			showDialog(FORM_ERROR_DIALOG_CAMPO_INVALIDO);
		}
	}


	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------


	private class CancelarButtonListener implements OnClickListener
	{
		
		
		public CancelarButtonListener(){
			
		}
		public void onClick(View v) {
			FormEntidadeDependenteActivity.this.finish();
			
		}
	}	
	
	private class CadastroButtonListener implements OnClickListener
	{
		
		public CadastroButtonListener(){
		
		}
		
		public void onClick(View v) {
			onCadastroButtonClicked(FormEntidadeDependenteActivity.this);
			
		}

	}	
}


