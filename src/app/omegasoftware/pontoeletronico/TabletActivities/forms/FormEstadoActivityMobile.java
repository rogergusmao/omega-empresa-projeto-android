package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextView.TableDependentAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPais;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;

public class FormEstadoActivityMobile extends OmegaCadastroActivity {

	//Constants
	public static final String TAG = "FormEstadoActivityMobile";

	private final int FORM_ERROR_DIALOG_MISSING_ESTADO = 5;
	private final int FORM_ERROR_DIALOG_ESTADO_DUPLICADO = 6;
	private final int FORM_ERROR_DIALOG_MISSING_PAIS = 7;
	private final int FORM_ERROR_DIALOG_PAIS_DUPLICADO = 8;
	private final int FORM_ERROR_DIALOG_INSERCAO = 9;
	
	
	//Spinners
	private DatabasePontoEletronico db=null;
	
	String idPaisInput = "";
	String idEstadoInput = "";

	String idPaisTextView = "";
	String idEstadoTextView = "";

	public static final String TABELAS_RELACIONADAS[] = new String[]{EXTDAOPais.NAME, EXTDAOUf.NAME};
	
	private AutoCompleteTextView estadoAutoCompletEditText;
	private AutoCompleteTextView paisAutoCompletEditText;

	TableDependentAutoCompleteTextView estadoContainerAutoComplete = null;
	TableAutoCompleteTextView paisContainerAutoComplete = null;

	OnFocusChangeListener paisOnFocusChangeListener;
	OnFocusChangeListener estadoOnFocusChangeListener;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_estado_activity_mobile_layout);
		
		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();
	}


	@Override
	public boolean loadData() {
		return true;
	}


	@Override
	public void beforeInitializeComponents() {

		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

				

		EXTDAOPais vObjPais = new EXTDAOPais(this.db);
		this.paisAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.pais_autocompletetextview);
		this.paisContainerAutoComplete = new TableAutoCompleteTextView(this, vObjPais, paisAutoCompletEditText);
		new MaskUpperCaseTextWatcher(paisAutoCompletEditText, 100);
		paisOnFocusChangeListener = new PaisOnItemSelectedListener();
		paisAutoCompletEditText.setOnFocusChangeListener(paisOnFocusChangeListener);

		EXTDAOUf vObjEstado = new EXTDAOUf(this.db); 
		this.estadoContainerAutoComplete = new TableDependentAutoCompleteTextView(vObjEstado, EXTDAOUf.PAIS_ID_INT);
		this.estadoAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.estado_autocompletetextview);
		new MaskUpperCaseTextWatcher(estadoAutoCompletEditText, 100);

		db.close();
	}


	private void showStatesFromCountry(String pPaisId)
	{
		ArrayAdapter<String> estadoAdapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item,
				estadoContainerAutoComplete.getVetorStrId(pPaisId));
		estadoAutoCompletEditText.setAdapter(estadoAdapter);
	}


	private class PaisOnItemSelectedListener implements OnFocusChangeListener {

		public void onFocusChange(View view, boolean hasFocus) {
			
			if(!hasFocus){
				String vPais = paisAutoCompletEditText.getText().toString();
				if(vPais != null){

					String vIdPais = paisContainerAutoComplete.getIdOfTokenIfExist(vPais);
					if(vIdPais != null){
						boolean estadoIsEmpty = estadoContainerAutoComplete.isEmpty();
						//se a lista de estados possiveis estiver vazia ||
						//se o pais for igual ao antigo 
						//=> entao nao carrega novamente a lista de estado possiveis
						if(vIdPais.compareTo(idPaisTextView) != 0 || estadoIsEmpty){
							showStatesFromCountry(vIdPais);
							//se for iniciando edicao
							if(idEstadoInput != null){
								//Tenta pegar o novo conteudo
								String vTokenEstado = estadoContainerAutoComplete.getTokenOfId(idEstadoInput);
								if(vTokenEstado != null){
									estadoAutoCompletEditText.setText(vTokenEstado);
									//forca o carregamento do bairro
									estadoOnFocusChangeListener.onFocusChange(null, false);
								}
								idEstadoInput = null;
							}
							idPaisTextView = vIdPais;
						}
					}
				}
			}
		}          
	}

	@Override
	protected Dialog onCreateDialog(int dialogType) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
		
		case FORM_ERROR_DIALOG_MISSING_PAIS:
			vTextView.setText(getResources().getString(R.string.error_missing_pais));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_PAIS);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_ESTADO:
			vTextView.setText(getResources().getString(R.string.error_missing_estado));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_ESTADO);
				}
			});
			break;

		case FORM_ERROR_DIALOG_PAIS_DUPLICADO:
			vTextView.setText(getResources().getString(R.string.error_pais_duplicado));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_PAIS_DUPLICADO);
				}
			});
			break;

		case FORM_ERROR_DIALOG_ESTADO_DUPLICADO:
			vTextView.setText(getResources().getString(R.string.error_estado_duplicado));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_ESTADO_DUPLICADO);
				}
			});
			break;

		case FORM_ERROR_DIALOG_INSERCAO:
			vTextView.setText(getResources().getString(R.string.error_insercao));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_INSERCAO);
				}
			});
			break;
		default:
			return super.getDialog(dialogType);
		}

		return vDialog;
	}	

	@Override
	protected void onDestroy(){
		try{
			this.db.close();	
		}catch(Exception ex){
		}
		super.onDestroy();
	}

	@Override
	public void finish(){
		try{
			this.db.close();	
		}catch(Exception ex){
		}
		super.finish();
	}
	
	@Override
	protected void loadEdit(){
		try{

		EXTDAOUf vObjEstado = new EXTDAOUf(db);
		vObjEstado.setAttrValue(EXTDAOUf.ID, id);
		vObjEstado.select();

		String vEstado = vObjEstado.getStrValueOfAttribute(EXTDAOUf.NOME);
		if(vEstado != null){
			estadoAutoCompletEditText.setText(vEstado);
		}

		String vPais = vObjEstado.getNomeDaChaveExtrangeira(EXTDAOUf.PAIS_ID_INT);
		if(vPais != null){
			paisAutoCompletEditText.setText(vPais);
		}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			
		}	
	}

	

	@Override
	protected ContainerCadastro cadastrando() {
		
		String vStrNomePais = paisAutoCompletEditText.getText().toString();
		if(vStrNomePais.length() == 0)
		{
			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_PAIS, false);
		}
		EXTDAOPais vObjPais = new EXTDAOPais(db);
		vObjPais.setAttrValue(EXTDAOPais.NOME, vStrNomePais);
		String vIdPais = paisContainerAutoComplete.addTupleIfNecessay(vObjPais);
		if(vIdPais == null){
			return new ContainerCadastro(FORM_ERROR_DIALOG_INSERCAO, false);

		}

		String vStrNomeEstado = estadoAutoCompletEditText.getText().toString();
		if(vStrNomeEstado.length() == 0)
		{
			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_ESTADO, false);
		}
		EXTDAOUf vObjEstado = new EXTDAOUf(db);
		vObjEstado.setAttrValue(EXTDAOUf.NOME, vStrNomeEstado);
		vObjEstado.setAttrValue(EXTDAOUf.PAIS_ID_INT, vIdPais);
		vObjEstado.setAttrValue(EXTDAOUf.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		if(id != null){
			vObjEstado.setAttrValue(EXTDAOUf.ID, id);
			vObjEstado.formatToSQLite();
			if(vObjEstado.update(true)){
				setBancoFoiModificado();

				return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);
			} else {
				return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
			}
		}  else{
			vObjEstado.formatToSQLite();
			Long vIdEstado = vObjEstado.insert(true); 
			if(vIdEstado != null){
//				clearComponents();
				handlerClearComponents.sendEmptyMessage(0);

				return new ContainerCadastro(DIALOG_CADASTRO_OK, true);
			}else{

				return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
			}
		}
	}	
}


