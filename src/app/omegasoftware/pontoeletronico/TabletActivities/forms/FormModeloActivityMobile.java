package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.util.LinkedHashMap;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskNumberTextWatcher;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOModelo;


public class FormModeloActivityMobile extends OmegaCadastroActivity {

	//Constants
	public static final String TAG = "FormModeloActivityMobile";
	private static final String TABELAS_RELACIONADAS[] = new String[]{EXTDAOModelo.NAME};

	private final static int FORM_ERROR_DIALOG_ANO_MODELO = 5;
	private final static int FORM_ERROR_DIALOG_NOME_MODELO  = 6;
	

	private DatabasePontoEletronico db=null;

	//EditText
	
	TableAutoCompleteTextView nomeModeloContainerAutoComplete = null;
	private AutoCompleteTextView nomeModeloAutoCompletEditText;
	TableAutoCompleteTextView containerAutoCompleteNomeModelo;
	private EditText anoModeloEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_modelo_activity_mobile_layout);
		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();
	}

	@Override
	public boolean loadData() {
		return true;
	}
	@Override
	public void finish(){
		try{
			this.db.close();
			//			Se for cadastro
			
			super.finish();			
		}catch(Exception ex){

		}
	}


	@Override
	protected void beforeInitializeComponents() {

		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		

		EXTDAOModelo vObjModelo = new EXTDAOModelo(this.db);
		this.nomeModeloAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.nome_modelo_autocompletetextview);
		this.nomeModeloContainerAutoComplete = new TableAutoCompleteTextView(this, vObjModelo, nomeModeloAutoCompletEditText);
		new MaskUpperCaseTextWatcher(nomeModeloAutoCompletEditText, 100);

		this.anoModeloEditText = (EditText) findViewById(R.id.ano_modelo_edittext);
        new MaskNumberTextWatcher(this.anoModeloEditText, 4);


		this.db.close();

	}
	
	@Override
	protected void loadEdit(){
		try{
		EXTDAOModelo vObj = new EXTDAOModelo(this.db);
		vObj.setAttrValue(EXTDAOModelo.ID, id);
		if(vObj.select()){
			String vAno = vObj.getStrValueOfAttribute(EXTDAOModelo.ANO_INT);
			String vModelo = vObj.getStrValueOfAttribute(EXTDAOModelo.NOME);
			
			nomeModeloAutoCompletEditText.setText(vModelo);
			anoModeloEditText.setText(vAno);
		}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			
		}	
	}

	protected LinkedHashMap<String, String> getAllModelo()
	{
		EXTDAOModelo vObj = new EXTDAOModelo(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}

	protected String[] getVetorAllModelo()
	{
		EXTDAOModelo vObj = new EXTDAOModelo(this.db);
		return vObj.getVetorStringDefinition(true);
	}

	protected String[] getVetorAllAnoModelo()
	{
		EXTDAOModelo vObj = new EXTDAOModelo(this.db);
		return vObj.getVetorStringDefinition(true);
	}


	@Override
	protected Dialog onCreateDialog(int dialogType) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
		case FORM_ERROR_DIALOG_ANO_MODELO:
			vTextView.setText(getResources().getString(R.string.error_missing_ano_modelo));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_ANO_MODELO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_NOME_MODELO:
			vTextView.setText(getResources().getString(R.string.error_missing_nome_modelo));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_NOME_MODELO);
				}
			});
			break;
		
		default:
			return super.getDialog(dialogType);
		}

		return vDialog;
	}	



	@Override
	protected ContainerCadastro cadastrando() {
		EXTDAOModelo vObj = new EXTDAOModelo(db);
		boolean vValidade = true;
		//First we check the obligatory fields:
		String vStrNomeModelo  = nomeModeloAutoCompletEditText.getText().toString();
		if(vStrNomeModelo.equals(""))
		{
			return new ContainerCadastro(FORM_ERROR_DIALOG_NOME_MODELO, false);
		}

		String vStrAnoModelo  = anoModeloEditText.getText().toString();
		if(vStrAnoModelo .equals(""))
		{
			return new ContainerCadastro(FORM_ERROR_DIALOG_ANO_MODELO, false);
		}
		
		
		if(vValidade){
			//							Se edicao
			if(id != null){
				vObj.setAttrValue(EXTDAOModelo.ID, id);
				vObj.formatToSQLite();
				if(vObj.update(true)){
					setBancoFoiModificado();
					return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);
				} else{
					return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
				}
			}else {
				vObj.formatToSQLite();
				if(vObj.insert(true) != null){
//						clearComponents();
					handlerClearComponents.sendEmptyMessage(0);
					return new ContainerCadastro(DIALOG_CADASTRO_OK, true);
				}else{
					return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
				}
			}
		}	
		else{
			return new ContainerCadastro(FORM_ERROR_DIALOG_CAMPO_INVALIDO, false);
		}
	}
	
}


