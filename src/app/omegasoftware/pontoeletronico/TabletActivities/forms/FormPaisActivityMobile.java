package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.os.Bundle;
import android.widget.AutoCompleteTextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPais;

public class FormPaisActivityMobile extends OmegaCadastroActivity {

	//Constants
	public static final String TAG = "FormPaisActivityMobile";
	public static final String TABELAS_RELACIONADAS[] = new String[]{EXTDAOPais.NAME};

	//Spinners
	private DatabasePontoEletronico db=null;

	String idPaisInput = "";
	String idPaisTextView = "";

	private AutoCompleteTextView paisAutoCompletEditText;

	TableAutoCompleteTextView paisContainerAutoComplete = null;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_pais_activity_mobile_layout);

		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();
	}
	
	@Override
	protected void loadEdit(){
try{
		EXTDAOPais vObjPais = new EXTDAOPais(db);
		vObjPais.setAttrValue(EXTDAOPais.ID, id);
		vObjPais.select();

		String vPais = vObjPais.getStrValueOfAttribute(EXTDAOPais.NOME);
		if(vPais != null){
			paisAutoCompletEditText.setText(vPais);
		}
}catch(Exception ex){
	SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
	
}	
	}


	@Override
	public boolean loadData() {
		return true;
	}

	@Override
	protected void beforeInitializeComponents() {
		
		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

				

		EXTDAOPais vObjPais = new EXTDAOPais(this.db);
		this.paisAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.pais_autocompletetextview);
		this.paisContainerAutoComplete = new TableAutoCompleteTextView(this, vObjPais, paisAutoCompletEditText);
		new MaskUpperCaseTextWatcher(paisAutoCompletEditText, 100);


		db.close();
	}

	@Override
	protected void onDestroy(){
		try{
			this.db.close();	
		}catch(Exception ex){
		}
		super.onDestroy();
	}

	@Override
	public void finish(){
		try{
			
			this.db.close();	
		}catch(Exception ex){
		}
		super.finish();
	}
	
	@Override
	protected ContainerCadastro cadastrando() {
		

		String strNomePais = paisAutoCompletEditText.getText().toString();
		if(strNomePais.length() == 0)
		{
			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_NOME, false);
			
		}else{
			EXTDAOPais objPais = new EXTDAOPais(db);

			objPais.setAttrValue(EXTDAOPais.NOME, strNomePais);
			objPais.setAttrValue(EXTDAOPais.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			if(id != null){
				objPais.setAttrValue(EXTDAOPais.ID, id);
				objPais.formatToSQLite();
				if(objPais.update(true)){
					setBancoFoiModificado();
					return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);
				} else {
					return new ContainerCadastro(FORM_ERROR_DIALOG_EDICAO , false);
				}
			} else{
				if(paisContainerAutoComplete.getIdOfTokenIfExist(strNomePais) != null){
					return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);

				} else{
					String vIdPais = paisContainerAutoComplete.addTupleIfNecessay(objPais);
					if(vIdPais == null){
						return new ContainerCadastro(FORM_ERROR_DIALOG_INSERCAO, false);
					} else{
						
						return new ContainerCadastro(DIALOG_CADASTRO_OK, true);
						
					}
				}
			}
		}	
	}


}


