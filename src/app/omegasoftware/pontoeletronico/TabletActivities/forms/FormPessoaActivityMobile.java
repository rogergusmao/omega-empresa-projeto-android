package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.FormPessoaAdapter;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaUsuario;


public class FormPessoaActivityMobile extends OmegaCadastroActivity {

	//Constants
	public static final String TAG = "FormPessoaActivityMobile";
	
	
	private EXTDAOPessoa objPessoa;
	private EXTDAOPessoaUsuario objPessoaUsuario;
	
	private DatabasePontoEletronico db=null;

	FormPessoaAdapter formPessoaAdapter ;
	private View viewFragmentFormPessoa;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, R.string.pessoa);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_pessoa_activity_mobile_layout);

		
		new CustomDataLoader(this).execute();
	}

	@Override
	protected void loadEdit() throws OmegaDatabaseException{
		if(formPessoaAdapter == null){
			formPessoaAdapter = new FormPessoaAdapter((OmegaCadastroActivity)this, 
					objPessoa.getId(), viewFragmentFormPessoa);
			formPessoaAdapter.inicializa();	
		}
		
		formPessoaAdapter.loadEdit();
	}
	@Override
	public boolean loadData() throws OmegaDatabaseException {
		this.db = new DatabasePontoEletronico(this);	

		
		objPessoaUsuario = new EXTDAOPessoaUsuario(db);
		objPessoa = new EXTDAOPessoa(db);
		
		
		return true;

	}

	@Override
	protected void beforeInitializeComponents() {
		try{
			this.db = new DatabasePontoEletronico(this);
	
			
			if(viewFragmentFormPessoa == null){
				viewFragmentFormPessoa = findViewById(R.id.fragment_form_pessoa);
			}
	
			if(id != null){
				objPessoa.select(id);
				
				objPessoaUsuario.setAttrValue(EXTDAOPessoaUsuario.PESSOA_ID_INT, objPessoa.getId());
				Table registroPessoaUsuario = objPessoaUsuario.getFirstTupla();
				if(registroPessoaUsuario != null){
					objPessoaUsuario = (EXTDAOPessoaUsuario)registroPessoaUsuario;
					
				} else {
					objPessoaUsuario = null;
				}
				if(formPessoaAdapter == null){
					formPessoaAdapter = new FormPessoaAdapter((OmegaCadastroActivity)this, 
							objPessoa.getId(), viewFragmentFormPessoa);
					formPessoaAdapter.inicializa();	
				}
				
				
			}else {
				
				
				formPessoaAdapter = new FormPessoaAdapter((OmegaCadastroActivity)this, null, viewFragmentFormPessoa);
				formPessoaAdapter.inicializa();
				
				setTituloCabecalho(getResources().getString(R.string.pessoa));
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			
		}	
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if(formPessoaAdapter.onActivityResult(requestCode, resultCode, intent))
			return;
		else super.onActivityResult(requestCode, resultCode, intent);
	}


	
	@Override
	public ContainerCadastro cadastrando(){
		return formPessoaAdapter.cadastrando(null);
	}
	@Override
	public void clearComponents(ViewGroup vg){

		formPessoaAdapter.clearComponents();
		super.clearComponents(vg);
		
	}
	
	
}


