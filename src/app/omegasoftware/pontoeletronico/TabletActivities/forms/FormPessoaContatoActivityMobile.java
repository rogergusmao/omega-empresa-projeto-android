package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;


public class FormPessoaContatoActivityMobile extends FormPessoaActivityMobile {

	//Constants
	public static final String TAG = "FormPessoaContatoActivityMobile";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO, false);
		super.onCreate(savedInstanceState);
	}
		
}


