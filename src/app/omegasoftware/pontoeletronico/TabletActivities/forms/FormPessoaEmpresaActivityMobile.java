package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.util.LinkedHashMap;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.dialog.HelperDialog;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;

public class FormPessoaEmpresaActivityMobile extends OmegaCadastroActivity {

	//Constants
	private static final String TAG = "FormPessoaEmpresaActivityMobile";

	
	EXTDAOProfissao objProfissao =null;
	private Button cancelarButton;
	Drawable originalDrawable;

	//Spinners
	private DatabasePontoEletronico db=null;
	
	private String idEmpresa;
	private String idPessoa;
	
	
	//EditText
	private AutoCompleteTextView profissaoAutoCompleteEditText;
	TableAutoCompleteTextView profissaoContainerAutoComplete = null;
	
	private Button novaPessoaButton;
	private Button novaEmpresaButton;
	private Spinner empresaSpinner;
	private Spinner pessoaSpinner;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_pessoa_empresa_activity_mobile_layout);

	
		Bundle vParameter = getIntent().getExtras();
		if(vParameter != null){
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA)){
				idEmpresa = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA);
			}
			
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA)){
				idPessoa = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA);
			}
			
		
			
		}
		new CustomDataLoader(this).execute();
	}

	private LinkedHashMap<String, String> getAllPessoa()
	{
		EXTDAOPessoa vObj = new EXTDAOPessoa(this.db);
		vObj.setAttrValue(EXTDAOPessoa.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		return vObj.getHashMapIdByDefinition(true);		
	}


	private LinkedHashMap<String, String> getAllEmpresa()
	{
		EXTDAOEmpresa vObj = new EXTDAOEmpresa(this.db);
		vObj.setAttrValue(EXTDAOEmpresa.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		return vObj.getHashMapIdByDefinition(true);		
	}

	protected LinkedHashMap<String, String> getAllProfissao()
	{
		EXTDAOProfissao vObj = new EXTDAOProfissao(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}


	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------


	private class CancelarButtonListener implements OnClickListener
	{
		
		Activity activity;
		
		public CancelarButtonListener(Activity pActivity){
			activity = pActivity;
		}
		public void onClick(View v) {
			activity.finish();
			
		}
	}	

	@Override
	protected void beforeInitializeComponents() throws OmegaDatabaseException {
		if(this.db == null)
		this.db = new DatabasePontoEletronico(this);
		if(novaPessoaButton == null)
			novaPessoaButton = (Button) findViewById(R.id.nova_pessoa_button);
		if(idPessoa == null){
			
			novaPessoaButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(FormPessoaEmpresaActivityMobile.this, FormPessoaActivityMobile.class);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_DESATIVAR_BOTAO_PROFISSOES, true);
					startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
				}
			});
		} else {
			novaPessoaButton.setVisibility(View.GONE);
		}
		
		
		if(novaEmpresaButton == null)
			novaEmpresaButton = (Button) findViewById(R.id.nova_empresa_button);
		if(idEmpresa == null){
			
			novaEmpresaButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(FormPessoaEmpresaActivityMobile.this, FormEmpresaActivityMobile.class);
					startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
				}
			});
		} else {
			novaEmpresaButton.setVisibility(View.GONE);
		}
		objProfissao = new EXTDAOProfissao(this.db);
		this.profissaoAutoCompleteEditText = (AutoCompleteTextView)
				findViewById(R.id.profissao_autocompletetextview);
		if(this.profissaoContainerAutoComplete == null){
			this.profissaoContainerAutoComplete = new TableAutoCompleteTextView(this, objProfissao, profissaoAutoCompleteEditText);
			new MaskUpperCaseTextWatcher(profissaoAutoCompleteEditText, 100);
		} else {
			profissaoContainerAutoComplete.updateAdapter();
		}
		this.empresaSpinner = (Spinner) findViewById(R.id.form_empresa_spinner);
		if(idEmpresa != null && idEmpresa.length() > 0 ){
			this.empresaSpinner.setVisibility(View.GONE);
		} else {
			this.empresaSpinner.setAdapter(
					new CustomSpinnerAdapter(getApplicationContext(), 
						this.getAllEmpresa(), 
						R.layout.spinner_item,
						R.string.form_empresa_prompt));
			((CustomSpinnerAdapter) this.empresaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
		}
		
		this.pessoaSpinner = (Spinner) findViewById(R.id.form_pessoa_spinner);
		if(idPessoa != null && idPessoa.length() > 0){
			this.pessoaSpinner.setVisibility(View.GONE);
		}
		else{
			this.pessoaSpinner.setAdapter(
					new CustomSpinnerAdapter(getApplicationContext(), 
						this.getAllPessoa(), 
						R.layout.spinner_item,
						R.string.form_pessoa_prompt));
			((CustomSpinnerAdapter) this.pessoaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
		}
		

		//Attach search button to its listener
		this.cancelarButton = (Button) findViewById(R.id.cancelar_button);
		this.cancelarButton.setOnClickListener(new CancelarButtonListener(this));
		
	}

	@Override
	protected void loadEdit() {
		
		
	}

	@Override
	protected ContainerCadastro cadastrando() throws Exception {
		
		Table vObj = new EXTDAOPessoaEmpresa(db);
		boolean vValidade = true;
		//First we check the obligatory fields:
		//Plano, Especialidade, Cidade
		//Checking Cidade
		if(id != null){
			vObj.setAttrValue(EXTDAOEmpresa.ID, id);
			vObj.select();
		}
		if(idEmpresa == null){
			String selectedEmpresa = String.valueOf(this.empresaSpinner.getSelectedItemId());
			if(selectedEmpresa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
			{	
				showDialog(FORM_ERROR_DIALOG_MISSING_EMPRESA);
				vValidade = false;
			} else{
				vObj.setAttrValue(EXTDAOPessoaEmpresa.EMPRESA_ID_INT, selectedEmpresa);
			}
		}else {
			vObj.setAttrValue(EXTDAOPessoaEmpresa.EMPRESA_ID_INT, idEmpresa);
		}
		
		if(idPessoa == null){
			String selectedPessoa = String.valueOf(this.pessoaSpinner.getSelectedItemId());
			if(selectedPessoa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
			{	
				showDialog(FORM_ERROR_DIALOG_MISSING_PESSOA);
				vValidade = false;
			} else{
				vObj.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, selectedPessoa);
			}
		}else {
			vObj.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, idPessoa);
		}
		
		String nomeProfissao = this.profissaoAutoCompleteEditText.getText().toString();
		if( nomeProfissao.length()  == 0)
		{
			showDialog(FORM_ERROR_DIALOG_MISSING_PROFISSAO);
			vValidade = false;
		} else {
			objProfissao.clearData();
			objProfissao.setAttrValue(EXTDAOProfissao.NOME, nomeProfissao);
			Table registroProfissao = objProfissao.getFirstTupla();
			String strIdProfissao = null;
			if(registroProfissao == null){
				objProfissao.formatToSQLite();
				Long idProfissao = objProfissao.insert(true);
				if(idProfissao  == null ){
					HelperDialog.factoryDialogErroFatal(this);
//					showDialog(FORM_ERROR_DIALOG_INESPERADO);
					vValidade= false;
				} else {
					strIdProfissao = String.valueOf(idProfissao);
				}
			} else {
				strIdProfissao = registroProfissao.getId();
			}
			vObj.setAttrValue(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT, strIdProfissao);
		}

		if(vValidade){

			if(id != null){
				vObj.setAttrValue(EXTDAOPessoaEmpresa.ID, id);
				vObj.formatToSQLite();
				if(vObj.update(true)){
					setBancoFoiModificado();
					return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true, true, true);

				} else{
					return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
				}
			}else {
				vObj.formatToSQLite();
				Long idPessoaEmpresa = vObj.insert(true); 
				if(idPessoaEmpresa != null){
					//Finaliza a activty
					setBancoFoiModificado();
//					FormPessoaEmpresaActivityMobile.this.finish();
					handlerClearComponents.sendEmptyMessage(0);
					return new ContainerCadastro(DIALOG_CADASTRO_OK, true, true, true);
					
				}else{
					return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);

				}
			}			
		}	
		else{
			return new ContainerCadastro(FORM_ERROR_DIALOG_CAMPO_INVALIDO, false);
		}
	}



	@Override
	public boolean loadData() {
		
		return true;
	}	
}


