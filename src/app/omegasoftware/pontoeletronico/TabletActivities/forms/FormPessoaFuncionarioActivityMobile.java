package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;


public class FormPessoaFuncionarioActivityMobile extends FormPessoaActivityMobile {

	//Constants
	public static final String TAG = "FormPessoaFuncionarioActivityMobile";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO, true);
		super.onCreate(savedInstanceState);
	}
		
}


