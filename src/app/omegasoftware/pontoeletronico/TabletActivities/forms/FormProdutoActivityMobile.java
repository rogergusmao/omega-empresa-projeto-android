package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.content.Intent;
import android.os.Bundle;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.TextView.TableDependentAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaProduto;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;


//public class FormProdutoActivityMobile extends OmegaCadastroActivity {
//
//	//Constants
//	public static final String TAG = "FormProdutoActivityMobile";
//
//	private DatabasePontoEletronico db=null;
//	private EditText nomeEditText;
//	private EditText identificadorEditText;
//	private EditText descricaoEditText;
//	private Spinner empresaSpinner;
//	private CustomSpinnerAdapter empresaAdapter;
//	private CustomSpinnerAdapter unidadeMedidaAdapter;
//	private Spinner unidadeMedidaSpinner;
//
//	private AutoCompleteTextView tipoProdutoAutoCompletEditText;
//	TableDependentAutoCompleteTextView tipoProdutoContainerAutoComplete = null;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.form_pessoa_activity_mobile_layout);
//
//		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();
//
//	}
//
//	@Override
//	protected void loadEdit(){
//try{
//		EXTDAOEmpresaProduto vObjFuncionario = new EXTDAOEmpresaProduto(db);
//		vObjFuncionario.setAttrValue(EXTDAOEmpresaProduto.ID, id);
//		vObjFuncionario.select();
//
//
//
//		String vNomeFuncionario = vObjFuncionario.getStrValueOfAttribute(EXTDAOEmpresaProduto.NOME);
//		if(vNomeFuncionario != null){
//			nomeEditText.setText(vNomeFuncionario);
//		}
//}catch(Exception ex){
//	SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
//
//}
//	}
//	@Override
//	public boolean loadData() {
//
//		return true;
//
//	}
//
//
//	@Override
//	public void beforeInitializeComponents() {
//
//		try {
//			this.db = new DatabasePontoEletronico(this);
//		} catch (OmegaDatabaseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
////		identificadorEditText = ((EditText) findViewById(R.id.identificador_textview));
////		nomeEditText = ((EditText) findViewById(R.id.nome_textview));
////		descricaoEditText = ((EditText) findViewById(R.id.descricao_textview));
//
//		EXTDAOEmpresaProdutoTipo vObjTipoProduto = new EXTDAOEmpresaProdutoTipo(db);
//		this.tipoProdutoContainerAutoComplete = new TableDependentAutoCompleteTextView(vObjTipoProduto, EXTDAOUf.PAIS_ID_INT);
//		this.tipoProdutoAutoCompletEditText = (AutoCompleteTextView)
//				findViewById(R.id.tipo_produto_autocompletetextview);
//		new MaskUpperCaseTextWatcher(tipoProdutoAutoCompletEditText, 100);
//
//		EXTDAOEmpresaProdutoUnidadeMedida vObjUnidadeMedida = new EXTDAOEmpresaProdutoUnidadeMedida(db);
//
//		this.unidadeMedidaSpinner = (Spinner) findViewById(R.id.unidade_medida_spinner);
//		unidadeMedidaAdapter = new CustomSpinnerAdapter(
//				getApplicationContext(),
//				vObjUnidadeMedida.getHashMapIdByDefinition(
//						true,
//						EXTDAOEmpresaProdutoUnidadeMedida.ABREVIACAO),
//				R.layout.spinner_item,
//				R.string.selecione);
//		this.unidadeMedidaSpinner.setAdapter(unidadeMedidaAdapter);
//
//		EXTDAOEmpresa vObjEmpresa = new EXTDAOEmpresa(db);
//		this.empresaSpinner = (Spinner) findViewById(R.id.empresa_spinner);
//		empresaAdapter = new CustomSpinnerAdapter(
//				getApplicationContext(),
//				vObjEmpresa.getHashMapIdByDefinition(true),
//				R.layout.spinner_item,
//				R.string.selecione);
//		this.empresaSpinner.setAdapter(empresaAdapter);
//		((CustomSpinnerAdapter) this.empresaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
//	}
//	@Override
//	public void finish(){
//		try{
//			this.db.close();
//
//			super.finish();
//		}catch(Exception ex){
//
//		}
//	}
//
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//
//		if(requestCode == OmegaConfiguration.ACTIVITY_FORM_EMPRESA_FUNCIONARIO){
//
//			try{
////				String vIdEmpresa = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA);
////				String vIdProfissao = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_PROFISSAO);
//
////				String vNomeEmpresa = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_EMPRESA);
////				String vNomeProfissao = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PROFISSAO);
//
////				addLayoutEmpresaPessoa(vIdEmpresa, vNomeEmpresa, vNomeProfissao);
//
//			}catch(Exception ex){
//				//Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());
//			}
//		}
//	}
//
//
//	@Override
//	protected ContainerCadastro cadastrando() {
//		try{
//		Table vObj = new EXTDAOEmpresaProduto(this.db);
//		boolean vValidade = true;
//		//First we check the obligatory fields:
//		//Plano, Especialidade, Cidade
//		//Checking Cidade
//		if(id != null){
//			vObj.setAttrValue(EXTDAOEmpresaProduto.ID, id);
//			vObj.select();
//		}
//
//		if(this.nomeEditText.getText().toString().length() > 0)
//		{
//			vObj.setAttrValue(EXTDAOEmpresaProduto.NOME, this.nomeEditText.getText().toString());
//		} else {
//
//			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_NOME, false);
//		}
//
//		if(!this.nomeEditText.getText().toString().equals(""))
//		{
//			vObj.setAttrValue(EXTDAOEmpresaProduto.NOME, this.nomeEditText.getText().toString());
//		}
//
//		if(!this.identificadorEditText.getText().toString().equals(""))
//		{
//			vObj.setAttrValue(EXTDAOEmpresaProduto.IDENTIFICADOR, this.identificadorEditText.getText().toString());
//		}
//
//		if(!this.descricaoEditText.getText().toString().equals(""))
//		{
//			vObj.setAttrValue(EXTDAOEmpresaProduto.DESCRICAO, this.descricaoEditText.getText().toString());
//		}
//
//		String selectedUnidadeMedida = String.valueOf(this.unidadeMedidaSpinner.getSelectedItemId());
//		if(!selectedUnidadeMedida.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
//		{
//			vObj.setAttrValue(EXTDAOEmpresaProduto.EMPRESA_PRODUTO_UNIDADE_MEDIDA_ID_INT, selectedUnidadeMedida);
//		}
//
//		String selectedEmpresa = String.valueOf(this.empresaSpinner.getSelectedItemId());
//		if(!selectedEmpresa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
//		{
//			vObj.setAttrValue(EXTDAOEmpresaProduto.EMPRESA_ID_INT, selectedEmpresa);
//		}
//
//		if(vValidade){
//			String vStrNomeTipoProduto = this.tipoProdutoAutoCompletEditText.getText().toString();
//			if(vStrNomeTipoProduto.length() > 0)
//			{
//				EXTDAOEmpresaProdutoTipo vObjTipoProduto = new EXTDAOEmpresaProdutoTipo(this.db);
//				vObjTipoProduto.setAttrValue(EXTDAOEmpresaProdutoTipo.NOME, vStrNomeTipoProduto);
//				String vIdTipoProduto = tipoProdutoContainerAutoComplete.addTupleIfNecessay(vObjTipoProduto);
//				if(vIdTipoProduto == null){
//					//TODO ajsutar
//					//return new ContainerCadastro(FORM_ERROR_DIALOG_INESPERADO, false);
//					return null;
//				} else {
//					vObj.setAttrValue(EXTDAOEmpresaProduto.EMPRESA_PRODUTO_TIPO_ID_INT, vIdTipoProduto);
//				}
//			}
//			if(id != null){
//				vObj.setAttrValue(EXTDAOEmpresaProduto.ID, id);
//				vObj.formatToSQLite();
//				if(vObj.update(true)){
//					setBancoFoiModificado();
//					return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);
//				} else {
//					return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
//
//				}
//			}else {
//				vObj.formatToSQLite();
//				Long vIdPessoa =vObj.insert(true);
//				if(vIdPessoa != null){
////					containerEmpresaProfissao.addListTupleInInsert(db, String.valueOf( vIdPessoa));
//
//					return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);
//				} else
//					return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
//			}
//		}
//		return new ContainerCadastro(FORM_ERROR_DIALOG_CAMPO_INVALIDO, false);
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
//			return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
//		}
//	}
//
//}
//
//
