package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.os.Bundle;
import android.widget.AutoCompleteTextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;

public class FormProfissaoActivityMobile extends OmegaCadastroActivity {

	//Constants
	public static final String TAG = "FormProfissaoActivityMobile";
	public static final String TABELAS_RELACIONADAS[] = new String[]{EXTDAOProfissao.NAME};

	//Spinners
	private DatabasePontoEletronico db=null;
	String idProfissaoInput = "";
	String idProfissaoTextView = "";

	private AutoCompleteTextView profissaoAutoCompletEditText;

	TableAutoCompleteTextView profissaoContainerAutoComplete = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_profissao_activity_mobile_layout);

		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();
	}

	@Override
	protected void loadEdit(){
try{
		EXTDAOProfissao vObjProfissao = new EXTDAOProfissao(db);
		vObjProfissao.setAttrValue(EXTDAOProfissao.ID, id);
		vObjProfissao.select();

		String vProfissao = vObjProfissao.getStrValueOfAttribute(EXTDAOProfissao.NOME);
		if(vProfissao != null){
			profissaoAutoCompletEditText.setText(vProfissao);
		}
}catch(Exception ex){
	SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
}
	}


	@Override
	public boolean loadData() {
//		if(OmegaConfiguration.DEBUGGING)
//			EXTDAOProfissao.randomPopulate();
		return true;
	}


	@Override
	public void beforeInitializeComponents() {
		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

				

		EXTDAOProfissao vObjProfissao = new EXTDAOProfissao(this.db);
		this.profissaoAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.profissao_autocompletetextview);
		this.profissaoContainerAutoComplete = new TableAutoCompleteTextView(this, vObjProfissao, profissaoAutoCompletEditText);
		new MaskUpperCaseTextWatcher(profissaoAutoCompletEditText, 100);

	
		db.close();
	}

	
	@Override
	protected void onDestroy(){
		try{
			this.db.close();	
		}catch(Exception ex){
		}
		super.onDestroy();
	}

	@Override
	public void finish(){
		try{
			this.db.close();	
		}catch(Exception ex){
		}
		super.finish();
	}


	@Override
	protected ContainerCadastro cadastrando() {
		

		String vStrNomeProfissao = profissaoAutoCompletEditText.getText().toString();
		if(vStrNomeProfissao.length() == 0)
		{
			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_NOME, false);

		}else{
			EXTDAOProfissao vObjProfissao = new EXTDAOProfissao(db);

			vObjProfissao.setAttrValue(EXTDAOProfissao.NOME, vStrNomeProfissao);
			vObjProfissao.setAttrValue(EXTDAOProfissao.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			if(id != null){
				vObjProfissao.setAttrValue(EXTDAOProfissao.ID, id);
				vObjProfissao.formatToSQLite();
				if(vObjProfissao.update(true)){
					setBancoFoiModificado();
					
					return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);
				} else{
					return new ContainerCadastro(FORM_ERROR_DIALOG_EDICAO, false);
				}
			} else{
				if(profissaoContainerAutoComplete.getIdOfTokenIfExist(vStrNomeProfissao) != null){
					return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
				} else{
					String vIdProfissao = profissaoContainerAutoComplete.addTupleIfNecessay(vObjProfissao);
					if(vIdProfissao == null){
						return new ContainerCadastro(FORM_ERROR_DIALOG_INSERCAO, false);

					} else{
						return new ContainerCadastro(DIALOG_CADASTRO_OK, true);
					}
				}
			}
		}	
	}	

}


