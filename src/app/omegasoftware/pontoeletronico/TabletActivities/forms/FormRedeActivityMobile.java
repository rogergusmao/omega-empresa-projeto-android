package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.FormRedeEmpresaAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.RedeEmpresaItemList;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORede;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORedeEmpresa;

public class FormRedeActivityMobile extends OmegaCadastroActivity {
	
	public static final String TAG = "FormRedeActivityMobile";
	private Button adicionarEmpresaButton;
	
	//Spinners
	private DatabasePontoEletronico db=null;
	EXTDAORede objRede;
	String idRedeInput = "";
	String idRedeTextView = "";

	private AutoCompleteTextView redeAutoCompletEditText;
	ContainerLayoutRedeEmpresa containerRedeEmpresa = new ContainerLayoutRedeEmpresa(this);
	TableAutoCompleteTextView redeContainerAutoComplete = null;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_rede_activity_mobile_layout);

		new CustomDataLoader(this).execute();
	}
	
	@Override
	protected void loadEdit(){
		
		

		String vRede = objRede.getStrValueOfAttribute(EXTDAORede.NOME);
		if(vRede != null){
			redeAutoCompletEditText.setText(vRede);
			redeAutoCompletEditText.clearFocus();
		}
		

	}
	ArrayList<String> idsRedeEmpresa = null;
	ArrayList<Table> empresas;
	@Override
	public boolean loadData() {
		try{
		db = new DatabasePontoEletronico(this);
		objRede = new EXTDAORede(db);
		if(id != null){
			objRede.select(id);
			EXTDAORedeEmpresa objRedeEmpresa = new EXTDAORedeEmpresa(db);
			objRedeEmpresa.setAttrValue(EXTDAORedeEmpresa.REDE_ID_INT, id);
			empresas = new ArrayList<Table>();
			idsRedeEmpresa = new ArrayList<String>();
			ArrayList<Table> empresasRede = objRedeEmpresa.getListTable();
			if(empresasRede != null){
				
				for(int i = 0; i < empresasRede.size(); i++){
					Table empresaRede = empresasRede.get(i);
					idsRedeEmpresa.add(empresaRede.getId());
					Table regEmpresa = empresaRede.getObjDaChaveExtrangeira(EXTDAORedeEmpresa.EMPRESA_ID_INT);
					empresas.add(regEmpresa);
				}
			}	
		}
		
		
		return true;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			return false;
		}
	}


	@Override
	protected void beforeInitializeComponents() {
		

				

		EXTDAORede vObjRede = new EXTDAORede(this.db);
		this.redeAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.rede_autocompletetextview);
		this.redeContainerAutoComplete = new TableAutoCompleteTextView(this, vObjRede, redeAutoCompletEditText);
		new MaskUpperCaseTextWatcher(redeAutoCompletEditText, 100);

		adicionarEmpresaButton = (Button) findViewById(R.id.cadastro_rede_empresa_button);
		this.adicionarEmpresaButton.setOnClickListener(new CadastrarEmpresaRedeListener());
		if(empresas != null){

			for(int i = 0; i < empresas.size(); i++){
				Table regEmpresa = empresas.get(i);
				addLayoutRedeEmpresa(idsRedeEmpresa.get(i), regEmpresa.getId(), regEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.NOME));	
			}		
		}	

	}
	private class CadastrarEmpresaRedeListener implements OnClickListener
	{

		Intent intent = null;

		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.cadastro_rede_empresa_button:
				intent = new Intent(getApplicationContext(), FormRedeEmpresaActivityMobile.class);
				
				break;

			default:
				break;
			}

			if(intent != null)
			{				
				try{

					startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_REDE_EMPRESA);
				}catch(Exception ex){
					
					SingletonLog.insereErro(ex, TIPO.FORMULARIO);
				}
			}
		}
	}
	
	public void addLayoutRedeEmpresa(
			String idRedeEmpresa,
			String pIdEmpresa,
			String pNomeEmpresa){
		if(pIdEmpresa == null ){
			return;
		} else if(pIdEmpresa.length() == 0){
			return;	
		}

		if(! containerRedeEmpresa.containsTuple(pIdEmpresa)){

			

			RedeEmpresaItemList empresaProfissaoItemList = new RedeEmpresaItemList(
					getApplicationContext(), 
					containerRedeEmpresa.getLastIdEmpresa(), 
					pIdEmpresa,
					pNomeEmpresa);
			LinearLayout linearLayoutEmpresaFuncionario = (LinearLayout) findViewById(R.id.linearlayout_empresa_funcionario);

			ArrayList<RedeEmpresaItemList> places = new ArrayList<RedeEmpresaItemList>();
			places.add(empresaProfissaoItemList);

			FormRedeEmpresaAdapter adapter = new FormRedeEmpresaAdapter(
					getApplicationContext(),
					places, 
					containerRedeEmpresa);
			View vNewView = adapter.getView(0, null, null);
			containerRedeEmpresa.add(idRedeEmpresa, pIdEmpresa, vNewView);

			linearLayoutEmpresaFuncionario.addView(vNewView);
		}

	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		switch (requestCode) {
		case OmegaConfiguration.ACTIVITY_FORM_REDE_EMPRESA:
			try{
				String vIdEmpresa = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA);
				String vNomeEmpresa = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_EMPRESA);

				addLayoutRedeEmpresa(null, vIdEmpresa, vNomeEmpresa);
			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.FORMULARIO);
			}
			break;
		
		default:
			super.onActivityResult(requestCode, resultCode, intent);
			break;
		}
	}

	@Override
	protected ContainerCadastro cadastrando() {


		String nomeRede = redeAutoCompletEditText.getText().toString();
		if(nomeRede != null && nomeRede.length() == 0)
		{
			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_NOME, false);

		}else{
			EXTDAORede vObjRede = new EXTDAORede(db);

			vObjRede.setAttrValue(EXTDAORede.NOME, nomeRede);
			vObjRede.setAttrValue(EXTDAORede.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			if(id != null){
				vObjRede.setAttrValue(EXTDAORede.ID, id);
				vObjRede.formatToSQLite();
				if(vObjRede.update(true)){
					containerRedeEmpresa.addListTupleInUpdate(db, id);
					setBancoFoiModificado();
					return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);

				} else {
					return new ContainerCadastro(FORM_ERROR_DIALOG_EDICAO, false);
				}
			} else{
				if(redeContainerAutoComplete.getIdOfTokenIfExist(nomeRede) != null){

					return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
				} else{
					String vIdRede = redeContainerAutoComplete.addTuple(vObjRede);
					containerRedeEmpresa.addListTupleInInsert(db, vIdRede);
					if(vIdRede == null){
						return new ContainerCadastro(FORM_ERROR_DIALOG_INSERCAO, false);
						
					} else{
						return new ContainerCadastro(DIALOG_CADASTRO_OK, true);
					}
				}
			}
		}	
	}
}


