package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.util.LinkedHashMap;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOBairro;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;

public class FormRedeEmpresaActivityMobile extends OmegaRegularActivity {

	//Constants
	//private static final String TAG = "FormPessoaEmpresaActivityMobile";

	private static final String TABELAS_RELACIONADAS[] = new String[]{EXTDAOTipoEmpresa.NAME, EXTDAOEmpresa.NAME, EXTDAOPessoa.NAME, EXTDAOBairro.NAME, EXTDAOUf.NAME, EXTDAOCidade.NAME};
	private final int FORM_ERROR_DIALOG_CADASTRO_OK = 3;
	private final int FORM_ERROR_DIALOG_CAMPO_INVALIDO = 4;
	private final int FORM_ERROR_DIALOG_MISSING_EMPRESA = 5;
	//private final int FORM_ERROR_DIALOG_MISSING_PROFISSAO = 6;

	//Search button
	private Button cadastrarButton;
	private Button cancelarButton;
	Drawable originalDrawable;

	//Spinners
	private DatabasePontoEletronico db=null;
	
	private Spinner empresaSpinner;

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_rede_empresa_activity_mobile_layout);

		
		
		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.NO_SYNC).execute();
	}

	@Override
	public boolean loadData() {
		return true;
	}


	@Override
	public void initializeComponents() {

		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		

		
		
		this.empresaSpinner = (Spinner) findViewById(R.id.form_empresa_spinner);
		this.empresaSpinner.setAdapter(new CustomSpinnerAdapter(getApplicationContext(), this.getAllEmpresa(), R.layout.spinner_item,R.string.selecione));
		((CustomSpinnerAdapter) this.empresaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		//Attach search button to its listener
		this.cadastrarButton = (Button) findViewById(R.id.cadastrar_dependente_button);
		this.cadastrarButton.setOnClickListener(new CadastroButtonListener(this));

		//Attach search button to its listener
		this.cancelarButton = (Button) findViewById(R.id.cancelar_button);
		this.cancelarButton.setOnClickListener(new CancelarButtonListener(this));
		
		
	}

	private LinkedHashMap<String, String> getAllEmpresa()
	{
		 
		
		EXTDAOEmpresa vObj = new EXTDAOEmpresa(this.db);
		vObj.setAttrValue(EXTDAOEmpresa.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		return vObj.getHashMapIdByDefinition(true);
		
	}


	
	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;

		switch (id) {
		default:
			vDialog = this.getErrorDialog(id);
			break;
		}
		return vDialog;
	}	

	private Dialog getErrorDialog(int dialogType)
	{

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
		case FORM_ERROR_DIALOG_CADASTRO_OK:
			vTextView.setText(getResources().getString(R.string.cadastro_ok));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_CADASTRO_OK);
				}
			});
			break;
		case FORM_ERROR_DIALOG_CAMPO_INVALIDO:
			vTextView.setText(getResources().getString(R.string.error_invalid_field));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_CAMPO_INVALIDO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_EMPRESA:
			vTextView.setText(getResources().getString(R.string.error_missing_empresa));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_EMPRESA);
				}
			});
			break;
		default:
			return null;
		}

		return vDialog;
	}

	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onCadastroButtonClicked(Activity pActivity){
		
		boolean vValidade = true;
		
		//First we check the obligatory fields:
		//Plano, Especialidade, Cidade
		//Checking Cidade
		String selectedEmpresa = String.valueOf(this.empresaSpinner.getSelectedItemId());
		String nomeEmpresa = (String) this.empresaSpinner.getSelectedItem();
		if(selectedEmpresa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
		{	
			showDialog(FORM_ERROR_DIALOG_MISSING_EMPRESA);
			return;
		} 

		
		if(vValidade){
			Intent intent = getIntent();
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, selectedEmpresa);
//			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PROFISSAO, selectedProfissao);
			
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_EMPRESA, nomeEmpresa);
			
			
			setResult(OmegaConfiguration.ACTIVITY_FORM_EMPRESA_FUNCIONARIO, intent);
			
			pActivity.finish( );
		}	
		else{
			showDialog(FORM_ERROR_DIALOG_CAMPO_INVALIDO);
		}
	}


	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------


	private class CancelarButtonListener implements OnClickListener
	{
		
		Activity activity;
		
		public CancelarButtonListener(Activity pActivity){
			activity = pActivity;
		}
		public void onClick(View v) {
			activity.finish();
			
		}
	}	
	
	private class CadastroButtonListener implements OnClickListener
	{
		Activity activity;
		public CadastroButtonListener(Activity pActivity){
			activity = pActivity;
		}
		
		public void onClick(View v) {
			onCadastroButtonClicked(activity);
			
		}

	}	
}


