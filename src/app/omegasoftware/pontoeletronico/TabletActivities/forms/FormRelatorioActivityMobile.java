package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.audio.ActivityAudioRecorder;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.cam.CameraConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.FormRelatorioAnexoAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.FormRelatorioAudioAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.FormRelatorioFotoAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.FormRelatorioVideoAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.RelatorioAnexoItemList;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORede;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORelatorio;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.file.FileExplorer;
import app.omegasoftware.pontoeletronico.file.HelperFile;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;

public class FormRelatorioActivityMobile extends OmegaCadastroActivity {
	private static final String  TABELAS_RELACIONADAS[] = new String[]{EXTDAORede.NAME};
	public static final String TAG = "FormRelatorioActivityMobile";
	//Limite maximo do arquivo em MB
	public static int LIMITE_MAXIMO_DO_ARQUIVO = 10;

	//Spinners
	private DatabasePontoEletronico db=null;

	private EditText tituloEditText;
	private EditText descricaoEditText;
	private Spinner empresaSpinner;
	private Spinner pessoaSpinner;
	ContainerLayoutRelatorioFoto containerFoto = new ContainerLayoutRelatorioFoto(this);
	ContainerLayoutRelatorioVideo containerVideo = new ContainerLayoutRelatorioVideo(this);
	ContainerLayoutRelatorioAudio containerAudio = new ContainerLayoutRelatorioAudio(this);
	ContainerLayoutRelatorioAnexo containerAnexo = new ContainerLayoutRelatorioAnexo(this);

	Uri mCapturedImageURI;

	private Button cadastroVideoButton;
	private Button cadastroAudioButton;
	private Button cadastroFotoButton;
	private Button cadastroAnexoButton;


	private Button audioButton;
	private Button videoButton;
	private Button relatorioButton;
	private Button fotoButton;
	private Button anexoButton;

	private LinearLayout fotoLinearLayout;
	private LinearLayout videoLinearLayout;
	private LinearLayout audioLinearLayout;
	private LinearLayout anexoLinearLayout;

	private final int FORM_ERROR_DIALOG_TIRAR_FOTO= 1;
	private final int FORM_ERROR_DIALOG_VIDEO= 2;
	private final int FORM_ERROR_DIALOG_AUDIO= 3;
	private final int FORM_ERROR_DIALOG_ANEXO= 4;
	private final int FORM_ERROR_DIALOG_ANEXO_ALEM_DO_LIMITE= 5;
	private final int FORM_ERROR_DIALOG_ARQUIVO_VAZIO= 6;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_relatorio_activity_mobile_layout);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();
	}
	@Override
	protected Dialog onCreateDialog(int dialogType)
	{

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {

		case FORM_ERROR_DIALOG_ANEXO_ALEM_DO_LIMITE:
			vTextView.setText(getResources().getString(R.string.error_dialog_anexo_alem_do_limite) + String.valueOf(LIMITE_MAXIMO_DO_ARQUIVO) + " mb");
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_ANEXO_ALEM_DO_LIMITE);
				}
			});
			break;
		case FORM_ERROR_DIALOG_ARQUIVO_VAZIO:
			vTextView.setText(getResources().getString(R.string.error_dialog_anexo_vazio));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_ARQUIVO_VAZIO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_ANEXO:
			vTextView.setText(getResources().getString(R.string.error_dialog_gravar_anexo));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_ANEXO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_TIRAR_FOTO:
			vTextView.setText(getResources().getString(R.string.error_dialog_gravar_foto));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_TIRAR_FOTO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_AUDIO:
			vTextView.setText(getResources().getString(R.string.error_dialog_audio));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_AUDIO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_VIDEO:
			vTextView.setText(getResources().getString(R.string.error_dialog_video));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_VIDEO);
				}
			});
			break;

		default:
			return super.onCreateDialog(dialogType);
		}

		return vDialog;
	}

	@Override
	protected void loadEdit(){
try{
		EXTDAORelatorio vObjRelatorio = new EXTDAORelatorio(db);
		vObjRelatorio.setAttrValue(EXTDAORelatorio.ID, id);
		vObjRelatorio.select();

		String vTitulo = vObjRelatorio.getStrValueOfAttribute(EXTDAORelatorio.TITULO);
		if(vTitulo != null){
			tituloEditText.setText(vTitulo);
		}

		String vDesc = vObjRelatorio.getStrValueOfAttribute(EXTDAORelatorio.DESCRICAO);
		if(vDesc != null){
			descricaoEditText.setText(vTitulo);
		}
}catch(Exception ex){
	SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
}
	}


	@Override
	public boolean loadData() {
		return true;
	}


	@Override
	protected void beforeInitializeComponents() {

		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		

		this.tituloEditText = (EditText)
				findViewById(R.id.titulo_edittext);
		new MaskUpperCaseTextWatcher(tituloEditText, 100);

		this.descricaoEditText = (EditText)
				findViewById(R.id.descricao_edittext);
		new MaskUpperCaseTextWatcher(descricaoEditText, 512);

		cadastroAudioButton = (Button) findViewById(R.id.cadastro_audio_button);
		cadastroAudioButton.setVisibility(View.GONE);
		cadastroAudioButton.setOnClickListener(new CadastrarRelatorioAudioListener());

		cadastroFotoButton = (Button) findViewById(R.id.cadastro_foto_button);
		cadastroFotoButton.setVisibility(View.GONE);
		cadastroFotoButton.setOnClickListener(new CadastrarRelatorioFotoListener());

		cadastroVideoButton = (Button) findViewById(R.id.cadastro_video_button);
		cadastroVideoButton.setVisibility(View.GONE);
		cadastroVideoButton.setOnClickListener(new CadastrarRelatorioVideoListener());

		cadastroAnexoButton = (Button) findViewById(R.id.cadastro_anexo_button);
		cadastroAnexoButton.setVisibility(View.GONE);
		cadastroAnexoButton.setOnClickListener(new CadastrarRelatorioAnexoListener(this));

		audioButton = (Button) findViewById(R.id.audio_button);
		audioButton.setOnClickListener(new AudioListener());
		fotoButton = (Button) findViewById(R.id.foto_button);
		fotoButton.setOnClickListener(new FotoListener());
		videoButton = (Button) findViewById(R.id.video_button);
		videoButton.setOnClickListener(new VideoListener());
		anexoButton = (Button) findViewById(R.id.anexo_button);
		anexoButton.setOnClickListener(new AnexoListener());

		relatorioButton = (Button) findViewById(R.id.relatorio_button);
		relatorioButton.setOnClickListener(new RelatorioListener());

		audioLinearLayout = (LinearLayout) findViewById(R.id.audio_linearlayout);
		audioLinearLayout.setVisibility(View.GONE);

		videoLinearLayout = (LinearLayout) findViewById(R.id.video_linearlayout);
		videoLinearLayout.setVisibility(View.GONE);

		fotoLinearLayout = (LinearLayout) findViewById(R.id.foto_linearlayout);
		fotoLinearLayout.setVisibility(View.GONE);


		anexoLinearLayout = (LinearLayout) findViewById(R.id.anexo_linearlayout);
		anexoLinearLayout.setVisibility(View.GONE);

		this.pessoaSpinner = (Spinner) findViewById(R.id.pessoa_spinner);
		this.pessoaSpinner.setAdapter(new CustomSpinnerAdapter(getApplicationContext(), EXTDAOPessoa.getAllPessoa(db), R.layout.spinner_item,R.string.selecione));
		((CustomSpinnerAdapter) this.pessoaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		this.empresaSpinner = (Spinner) findViewById(R.id.empresa_spinner);
		this.empresaSpinner.setAdapter(new CustomSpinnerAdapter(getApplicationContext(), EXTDAOEmpresa.getAllEmpresa(db), R.layout.spinner_item,R.string.selecione));
		((CustomSpinnerAdapter) this.empresaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		db.close();
	}

	private void setAudioVisibility(int pVisibility){
		cadastroAudioButton.setVisibility(pVisibility);
		audioLinearLayout.setVisibility(pVisibility);
	}

	private void setVideoVisibility(int pVisibility){
		cadastroVideoButton.setVisibility(pVisibility);
		videoLinearLayout.setVisibility(pVisibility);
	}

	private void setFotoVisibility(int pVisibility){
		cadastroFotoButton.setVisibility(pVisibility);
		fotoLinearLayout.setVisibility(pVisibility);
	}

	private void setAnexoVisibility(int pVisibility){
		cadastroAnexoButton.setVisibility(pVisibility);
		anexoLinearLayout.setVisibility(pVisibility);
	}

	private void setRelatorioVisibility(int pVisibility){
		tituloEditText.setVisibility(pVisibility);
		descricaoEditText.setVisibility(pVisibility);
		empresaSpinner.setVisibility(pVisibility);
		pessoaSpinner.setVisibility(pVisibility);
		cadastrarButton.setVisibility(pVisibility);
		((TextView) findViewById(R.id.titulo_textview)).setVisibility(pVisibility);
		((TextView) findViewById(R.id.descricao_textview)).setVisibility(pVisibility);
		((TextView) findViewById(R.id.pessoa_textview)).setVisibility(pVisibility);
		((TextView) findViewById(R.id.empresa_textview)).setVisibility(pVisibility);
	}

	private class FotoListener implements OnClickListener
	{

//		Intent intent = null;

		public void onClick(View v) {
			setVideoVisibility(View.GONE);
			setAudioVisibility(View.GONE);
			setFotoVisibility(View.VISIBLE);
			setRelatorioVisibility(View.GONE);
			setAnexoVisibility(View.GONE);
		}
	}

	private class RelatorioListener implements OnClickListener
	{

//		Intent intent = null;

		public void onClick(View v) {
			setVideoVisibility(View.GONE);
			setAudioVisibility(View.GONE);
			setFotoVisibility(View.GONE);
			setRelatorioVisibility(View.VISIBLE);
			setAnexoVisibility(View.GONE);
		}
	}

	private class VideoListener implements OnClickListener
	{

//		Intent intent = null;

		public void onClick(View v) {
			setVideoVisibility(View.VISIBLE);
			setAudioVisibility(View.GONE);
			setFotoVisibility(View.GONE);
			setRelatorioVisibility(View.GONE);
			setAnexoVisibility(View.GONE);
		}
	}

	private class AnexoListener implements OnClickListener
	{



		public void onClick(View v) {
			setVideoVisibility(View.GONE);
			setAudioVisibility(View.GONE);
			setFotoVisibility(View.GONE);
			setRelatorioVisibility(View.GONE);
			setAnexoVisibility(View.VISIBLE);
		}
	}

	private class AudioListener implements OnClickListener
	{

//		Intent intent = null;

		public void onClick(View v) {
			setVideoVisibility(View.GONE);
			setAudioVisibility(View.VISIBLE);
			setFotoVisibility(View.GONE);
			setRelatorioVisibility(View.GONE);
			setAnexoVisibility(View.GONE);
		}
	}



	private class CadastrarRelatorioAnexoListener implements OnClickListener
	{

//		Intent intent = null;
		Context context;
		public CadastrarRelatorioAnexoListener(Context pContext){
			context = pContext;
		}

		private void dispatchTakeVideoIntent() {
			try{
				Intent intent = new Intent(context, FileExplorer.class);
				startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FILE_EXPLORER);

			} catch(Exception ex){
				Log.d(TAG, ex.toString());
			}

		}

		public void onClick(View v) {
			dispatchTakeVideoIntent();
		}
	}
	private class CadastrarRelatorioVideoListener implements OnClickListener
	{

//		Intent intent = null;

		private void dispatchTakeVideoIntent() {

			Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
			startActivityForResult(takeVideoIntent, OmegaConfiguration.ACTIVITY_CAMERA_VIDEO_RECORDER);
		}

		public void onClick(View v) {
			dispatchTakeVideoIntent();
		}
	}
	Uri mUriAudio = null;

	public class CadastrarRelatorioAudioListener implements OnClickListener
	{

		Intent intent = null;

		public void dispatchRecordAudioIntent(){
			OmegaFileConfiguration config = new OmegaFileConfiguration(); 
			String vPath = config.getPath(OmegaFileConfiguration.TIPO.AUDIO)+HelperFile.getFileWithUniqueName() + ".mp3";
			mUriAudio = MediaStore.Audio.Media.getContentUriForPath(vPath); 
			Intent intent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
			startActivityForResult(intent, OmegaConfiguration.ACTIVITY_AUDIO_RECORDER);
		}

		public void onClick(View v) {
			//dispatchRecordAudioIntent();
			intent = new Intent(getApplicationContext(), ActivityAudioRecorder.class);
			OmegaFileConfiguration config = new OmegaFileConfiguration(); 
			String vPath = config.getPath(OmegaFileConfiguration.TIPO.AUDIO) +HelperFile.getFileWithUniqueName() + ".mp4";
			intent.putExtra(CameraConfiguration.PATH_DIRECTORY_FIELD, vPath);

			if(intent != null)
			{				
				try{

					startActivityForResult(intent, OmegaConfiguration.ACTIVITY_AUDIO_RECORDER);
				}catch(Exception ex){
					//Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
				}
			}
		}
	}

	private class CadastrarRelatorioFotoListener implements OnClickListener
	{

//		Intent intent = null;

		private void dispatchTakeCameraIntent() {
			OmegaFileConfiguration vConfig = new OmegaFileConfiguration(); 
			String vPath = vConfig.getPath(OmegaFileConfiguration.TIPO.ANEXO);
			String fileName = vPath + HelperFile.getFileWithUniqueName() + ".jpg";  
			ContentValues values = new ContentValues();  
			values.put(MediaStore.Images.Media.TITLE, fileName);  
			mCapturedImageURI = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);  

			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
			startActivityForResult(intent, OmegaConfiguration.ACTIVITY_CAMERA_TAKE_PICTURE_WITH_CLICK);
		}

		public void onClick(View v) {
			dispatchTakeCameraIntent();

		}
	}

	public void addLayoutRelatorioFoto(
			String pNomeFoto){
		if(pNomeFoto == null ){
			return;
		} else if(pNomeFoto.length() == 0){
			return;	
		}

		

		RelatorioAnexoItemList empresaProfissaoItemList = new RelatorioAnexoItemList(
				getApplicationContext(), 
				containerFoto.getLastId(), 
				pNomeFoto);
		LinearLayout linearLayoutRelatorioFoto = (LinearLayout) findViewById(R.id.foto_linearlayout);

		ArrayList<RelatorioAnexoItemList> places = new ArrayList<RelatorioAnexoItemList>();
		places.add(empresaProfissaoItemList);

		FormRelatorioFotoAdapter adapter = new FormRelatorioFotoAdapter(
				getApplicationContext(),
				places, 
				containerFoto);
		View vNewView = adapter.getView(0, null, null);
		containerFoto.add(pNomeFoto, vNewView);

		linearLayoutRelatorioFoto.addView(vNewView);
	}

	public void addLayoutRelatorioVideo(
			String pNomeVideo){
		if(pNomeVideo == null ){
			return;
		} else if(pNomeVideo.length() == 0){
			return;	
		}

		

		RelatorioAnexoItemList itemList = new RelatorioAnexoItemList(
				getApplicationContext(), 
				containerVideo.getLastId(), 
				pNomeVideo);
		LinearLayout linearLayoutRelatorioVideo = (LinearLayout) findViewById(R.id.video_linearlayout);

		ArrayList<RelatorioAnexoItemList> places = new ArrayList<RelatorioAnexoItemList>();
		places.add(itemList);

		FormRelatorioVideoAdapter adapter = new FormRelatorioVideoAdapter(
				this,
				places, 
				containerVideo);
		View vNewView = adapter.getView(0, null, null);
		containerVideo.add(pNomeVideo, vNewView);

		linearLayoutRelatorioVideo.addView(vNewView);
	}

	public void addLayoutRelatorioAnexo(
			String pNomeAnexo){
		if(pNomeAnexo == null ){
			return;
		} else if(pNomeAnexo.length() == 0){
			return;	
		}

		

		RelatorioAnexoItemList itemList = new RelatorioAnexoItemList(
				getApplicationContext(), 
				containerAnexo.getLastId(), 
				pNomeAnexo);
		LinearLayout linearLayoutRelatorioAnexo = (LinearLayout) findViewById(R.id.anexo_linearlayout);

		ArrayList<RelatorioAnexoItemList> places = new ArrayList<RelatorioAnexoItemList>();
		places.add(itemList);

		FormRelatorioAnexoAdapter adapter = new FormRelatorioAnexoAdapter(
				this,
				places, 
				containerAnexo);
		
		View vNewView = adapter.getView(0, null, null);
		containerAnexo.add(pNomeAnexo, vNewView);

		linearLayoutRelatorioAnexo.addView(vNewView);
	}

	@Override
	protected void onDestroy(){
		try{
			this.db.close();	
		}catch(Exception ex){
		}
		super.onDestroy();
	}

	@Override
	public void finish(){
		try{

			this.db.close();	
		}catch(Exception ex){
		}
		super.finish();
	}
	


		 
	public void addLayoutRelatorioAudio(
			String pNomeAudio){
		if(pNomeAudio == null ){
			return;
		} else if(pNomeAudio.length() == 0){
			return;	
		}

		

		RelatorioAnexoItemList empresaProfissaoItemList = new RelatorioAnexoItemList(
				getApplicationContext(), 
				containerAudio.getLastId(), 
				pNomeAudio);
		LinearLayout linearLayoutRelatorioAudio = (LinearLayout) findViewById(R.id.audio_linearlayout);

		ArrayList<RelatorioAnexoItemList> places = new ArrayList<RelatorioAnexoItemList>();
		places.add(empresaProfissaoItemList);

		FormRelatorioAudioAdapter adapter = new FormRelatorioAudioAdapter(
				this,
				places, 
				containerAudio);
		View vNewView = adapter.getView(0, null, null);
		containerAudio.add(pNomeAudio, vNewView);

		linearLayoutRelatorioAudio.addView(vNewView);
	}

	public static boolean isIntentVideoAvailable(Context context, String action) {
		final PackageManager packageManager = context.getPackageManager();
		final Intent intent = new Intent(action);
		List<ResolveInfo> list =
				packageManager.queryIntentActivities(intent,
						PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}


	//	private void handleCameraVideo(Intent intent) {
	//	    Uri mVideoUri = intent.getData();
	//	    mVideoView.setVideoURI(mVideoUri);
	//	}
	public void saveFileFromUri(Uri pUri, String pPathFile){
		try {
			AssetFileDescriptor videoAsset = getContentResolver().openAssetFileDescriptor(pUri, "r");
			FileInputStream fis = videoAsset.createInputStream();
			File tmpFile = new File(pPathFile); 
			FileOutputStream fos = new FileOutputStream(tmpFile);

			byte[] buf = new byte[1024];
			int len;
			while ((len = fis.read(buf)) > 0) {
				fos.write(buf, 0, len);
			}       
			fis.close();
			fos.close();
		} catch (IOException io_e) {
			// TODO: handle error
		}
	}
	//	public void saveFileFromWeb(String pUrl){
	//		
	//		URL URL_PONTO_ELETRONICO = new URL (pUrl);
	//		InputStream input = URL_PONTO_ELETRONICO.openStream();
	//		try {
	//		    //The sdcard directory e.g. '/sdcard' can be used directly, or 
	//		    //more safely abstracted with getExternalStorageDirectory()
	//		    File storagePath = Environment.getExternalStorageDirectory();
	//		    OutputStream output = new FileOutputStream (new File(storagePath,"myImage.png"));
	//		    try {
	//		        byte[] buffer = new byte[1024];
	//		        int bytesRead = 0;
	//		        while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
	//		            output.write(buffer, 0, bytesRead);
	//		        }
	//		    } finally {6
	//		        output.close();
	//		    }
	//		} finally {
	//		    input.close();
	//		}
	//	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		switch (requestCode) {
		case OmegaConfiguration.ACTIVITY_CAMERA_TAKE_PICTURE_WITH_CLICK:
			try{
				if(mCapturedImageURI != null){
					String[] projection = { MediaStore.Images.Media.DATA}; 
					Cursor cursor = managedQuery(mCapturedImageURI, projection, null, null, null); 
					int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA); 
					cursor.moveToFirst(); 
					String capturedImageFilePath = cursor.getString(column_index_data);
					File vFile = new File(capturedImageFilePath);
					if(vFile.exists()){
						OmegaFileConfiguration config = new OmegaFileConfiguration();
						
						String vPath = config.getPath(OmegaFileConfiguration.TIPO.FOTO) + HelperFile.getFileWithUniqueName() + ".jpg";
						File vFileTemp = new File(vPath);
						vFile.renameTo(vFileTemp);
						addLayoutRelatorioFoto(vPath);
					}
					

					mCapturedImageURI = null;
				} 

			}catch(Exception ex){
				Log.d( TAG, "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
			}
			break;
		case OmegaConfiguration.ACTIVITY_CAMERA_VIDEO_RECORDER:
			try{
				if(intent != null){
					OmegaFileConfiguration vConfig = new OmegaFileConfiguration(); 
					String vFilePath = vConfig.getPath(OmegaFileConfiguration.TIPO.ANEXO) + HelperFile.getFileWithUniqueName() + ".mp4";
					saveFileFromUri(intent.getData(), vFilePath);
					addLayoutRelatorioVideo(vFilePath);
				}


			}catch(Exception ex){
				Log.d( TAG, "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
			}
			break;
		case OmegaConfiguration.ACTIVITY_AUDIO_RECORDER:
			try{

				if(intent != null && intent.hasExtra(CameraConfiguration.PARAMETER_PICTURE)){
					String vFileName = intent.getStringExtra(CameraConfiguration.PARAMETER_PICTURE);
					if(vFileName != null && vFileName.length() > 0)
						addLayoutRelatorioAudio(vFileName);
					else 
						showDialog(FORM_ERROR_DIALOG_AUDIO);
				}
			}catch(Exception ex){
				Log.d( TAG, "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
			}
			break;
		case OmegaConfiguration.ACTIVITY_FILE_EXPLORER:
			try{
				if(resultCode==RESULT_OK){
					if(intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_PATH)){
						String vFileName = intent.getExtras().getString(OmegaConfiguration.SEARCH_FIELD_PATH);
						File vFile = new File(vFileName);
						
							if(vFileName != null && vFileName.length() > 0)
								if(vFile.length() > LIMITE_MAXIMO_DO_ARQUIVO * 1024)
									showDialog(FORM_ERROR_DIALOG_ANEXO_ALEM_DO_LIMITE);
								else if(vFile.length() == 0)
									showDialog(FORM_ERROR_DIALOG_ARQUIVO_VAZIO);
								else
									addLayoutRelatorioAnexo(vFileName);
							else{
								showDialog(FORM_ERROR_DIALOG_ANEXO);
							}
						
							
					}    
				}
			}catch(Exception ex){
				Log.d( TAG, "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
			}
			break;
		default:
			super.onActivityResult(requestCode, resultCode, intent);
			break;
		}
	}
	@Override
	protected ContainerCadastro cadastrando() {
		

		String vStrTitulo = tituloEditText.getText().toString();
		if(vStrTitulo.length() == 0)
		{
			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_TITULO, false);
			
		}else{
			EXTDAORelatorio vObjRelatorio = new EXTDAORelatorio(db);

			vObjRelatorio.setAttrValue(EXTDAORelatorio.TITULO, vStrTitulo);

			String vStrDesc = descricaoEditText.getText().toString();
			if(vStrDesc != null && vStrDesc.length() > 0 )
				vObjRelatorio.setAttrValue(EXTDAORelatorio.DESCRICAO, vStrDesc);

//			String selectedEmpresa = String.valueOf(empresaSpinner.getSelectedItemId());
//			if(!selectedEmpresa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
//			{
//				vObjRelatorio.setAttrValue(EXTDAORelatorio.EMPRESA_ID_INT, selectedEmpresa);
//			}  else {
//				vObjRelatorio.setAttrValue(EXTDAORelatorio.EMPRESA_ID_INT, null);
//			}
//
//			String selectedPessoa = String.valueOf(pessoaSpinner.getSelectedItemId());
//			if(!selectedPessoa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
//			{
//				vObjRelatorio.setAttrValue(EXTDAORelatorio.PESSOA_ID_INT, selectedPessoa);
//			}  else {
//				vObjRelatorio.setAttrValue(EXTDAORelatorio.PESSOA_ID_INT, null);
//			}

			vObjRelatorio.setAttrValue(EXTDAORelatorio.USUARIO_ID_INT, OmegaSecurity.getIdUsuario());

			vObjRelatorio.setAttrValue(EXTDAORelatorio.DATA_SEC, String.valueOf( HelperDate.getTimestampSegundosUTC(this)));
			vObjRelatorio.setAttrValue(EXTDAORelatorio.DATA_OFFSEC, HelperDate.getTimeAtualFormatada(this));

			vObjRelatorio.setAttrValue(EXTDAORede.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			if(id != null){
				vObjRelatorio.setAttrValue(EXTDAORede.ID, id);
				vObjRelatorio.formatToSQLite();
				if(vObjRelatorio.update(true)){
					setBancoFoiModificado();
					containerFoto.addListTupleInUpdate(db, id);
					containerAudio.addListTupleInUpdate(db, id);
					containerVideo.addListTupleInUpdate(db, id);
					return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);

				} else {
					return new ContainerCadastro(FORM_ERROR_DIALOG_EDICAO, false);
				}
			} else{
				vObjRelatorio.formatToSQLite();
				Long vIdRelatorio = vObjRelatorio.insert(true);
				if(vIdRelatorio != null ){
					containerFoto.addListTupleInInsert(db, String.valueOf(vIdRelatorio));
					containerAudio.addListTupleInInsert(db, String.valueOf(vIdRelatorio));
					containerVideo.addListTupleInInsert(db, String.valueOf(vIdRelatorio));

					return new ContainerCadastro(DIALOG_CADASTRO_OK, true);


				} else{
					return new ContainerCadastro(FORM_ERROR_DIALOG_INSERCAO, false);
				}
			}
		}	
	}

	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------


}


