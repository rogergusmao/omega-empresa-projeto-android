package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.FormEnderecoAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.AppointmentPlaceItemList;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOBairro;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPais;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.date.ContainerClickListenerDate;
import app.omegasoftware.pontoeletronico.date.ContainerClickListenerHour;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.listener.FactoryDateClickListener;
import app.omegasoftware.pontoeletronico.listener.FactoryHourClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;


public class FormTarefaActivityMobile extends OmegaCadastroActivity {
    public static int ORIGEM = 0;
    public static int DESTINO = 1;
    //Constants
    public static final String TAG = "FormTarefaActivityMobile";


    private final int FORM_ERROR_DIALOG_ENDERECO_ORIGEM = 5;
    private final int FORM_ERROR_DIALOG_DATA_EXIBIR_MAIOR_QUE_DATA_PROGRAMADA = 6;

    private final int FORM_ERROR_DIALOG_USUARIO = 8;
    private final int FORM_ERROR_DIALOG_VEICULO_USUARIO = 9;
    private final int FORM_ERROR_DIALOG_VEICULO = 10;
    private final int FORM_ERROR_DIALOG_TIPO_TAREFA = 11;
    private final int FORM_ERROR_DIALOG_CATEGORIA_PERMISSAO = 12;

    private final int FORM_ERROR_DIALOG_TIPO_ENDERECO_PESSOA = 13;
    private final int FORM_ERROR_DIALOG_TIPO_ENDERECO_EMPRESA = 14;

    private FactoryDateClickListener factoryDateClickListener;
    private FactoryHourClickListener factoryHourClickListener;

    private DatabasePontoEletronico db=null;

    LoadEnderecoHandler handlerEndereco;
    LinearLayout llOrigem;
    LinearLayout llDestino;
    CustomSpinnerAdapter usuarioAdapter;

    private Button inicioDataProgramadaButton;
    private Button inicioHoraProgramadaButton;

    private Button inicioHoraPrazoButton;
    private Button inicioDataPrazoButton;

    private Button dataExibirButton;


    private ContainerClickListenerDate inicioDataProgramadaExibirClickListener;
    private ContainerClickListenerHour inicioHoraProgramadaExibirClickListener;

    private ContainerClickListenerDate inicioDataPrazoExibirClickListener;
    private ContainerClickListenerHour inicioHoraPrazoExibirClickListener;

    private ContainerClickListenerDate dataExibirClickListener;
    private EditText descricaoEditText;
    private EditText tituloEditText;

    public TextView origemIdentificadorTextView, destinoIdentificadorTextView;
    public ImageButton origemImageButton, destinoImageButton;
    public EXTDAOTarefa.TIPO_ENDERECO origemTipoEndereco;
    public String origemIdPessoaOuEmpresa;
    public EXTDAOTarefa.TIPO_ENDERECO destinoTipoEndereco;

    public String destinoId;

    Spinner usuarioSpinner;

    private Button origemButton, destinoButton;
    private ImageButton removerOrigemButton, removerDestinoButton;

    ContainerLayoutEndereco containerEnderecoOrigem = null;
    ContainerLayoutEndereco containerEnderecoDestino = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.form_tarefa_activity_mobile_layout);
        new CustomDataLoader(
                this
                , EXTDAOTarefa.TABELAS_RELACIONADAS
                , TYPE_SYNC.INITIAL_AND_END
        ).execute();
    }

    @Override
    public boolean loadData() {

        return true;
    }
    @Override
    protected void loadEdit(){
        try{
            EXTDAOTarefa objTarefa = new EXTDAOTarefa(db);

            if(objTarefa.select(id)){
                String idUsuario = objTarefa.getStrValueOfAttribute(EXTDAOTarefa.USUARIO_ID_INT);

                if(idUsuario != null){
                    Integer selected = usuarioAdapter.getPositionFromId(idUsuario);
                    if(selected!= null)
                        usuarioSpinner.setSelection(selected);
                }

                HelperDate dProgramada = objTarefa.getHelperDateValueOfAttribute(
                        EXTDAOTarefa.INICIO_HORA_PROGRAMADA_SEC, EXTDAOTarefa.INICIO_HORA_PROGRAMADA_OFFSEC);
                if(dProgramada != null){
                    inicioDataProgramadaExibirClickListener.setDateOfView(
                            this,
                            dProgramada.getYear(),
                            dProgramada.getMonth(),
                            dProgramada.getDay());

                    inicioHoraProgramadaExibirClickListener.setOfTimeView(
                            dProgramada.getHour(),
                            dProgramada.getMinute());
                }

                HelperDate dPrazo = objTarefa.getHelperDateValueOfAttribute(
                        EXTDAOTarefa.PRAZO_SEC, EXTDAOTarefa.PRAZO_OFFSEC);
                if(dPrazo != null){
                    inicioDataPrazoExibirClickListener.setDateOfView(
                            this,
                            dProgramada.getYear(),
                            dProgramada.getMonth(),
                            dProgramada.getDay());

                    inicioHoraPrazoExibirClickListener.setOfTimeView(
                            dProgramada.getHour(),
                            dProgramada.getMinute());
                }



                String vDescricao = objTarefa.getStrValueOfAttribute(EXTDAOTarefa.DESCRICAO);
                if(vDescricao != null)
                    descricaoEditText.setText(vDescricao);

                String vTitulo = objTarefa.getStrValueOfAttribute(EXTDAOTarefa.TITULO);
                if(vTitulo != null)
                    tituloEditText.setText(vTitulo);

                EXTDAOTarefa.DadosEndereco dadosOrigem = objTarefa.getDadosEnderecoOrigem();
                if(dadosOrigem != null) {
                    refreshOrigem(dadosOrigem);

                    initDrawableTipoEndereco(
                            (ImageButton) findViewById(R.id.ib_tipo_endereco_origem)
                            , dadosOrigem.tipoEndereco);
                }

                EXTDAOTarefa.DadosEndereco dadosDestino = objTarefa.getDadosEnderecoDestino();
                if(dadosDestino != null) {
                    refreshDestino(dadosDestino);
                    initDrawableTipoEndereco(
                            (ImageButton) findViewById(R.id.ib_tipo_endereco_origem)
                            , dadosOrigem.tipoEndereco);
                }


            }

        }catch(Exception ex){
            SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
        }

    }



    @Override
    protected void beforeInitializeComponents() {

        try {
            this.db = new DatabasePontoEletronico(this);

            setTituloCabecalho(R.string.cadastrar_tarefa);
            handlerEndereco  = new  LoadEnderecoHandler();
            this.usuarioSpinner = (Spinner) findViewById(R.id.sp_usuario);

            this.usuarioAdapter = new CustomSpinnerAdapter(
                    getApplicationContext(),
                    EXTDAOUsuario.getAllUsuario(db),
                    R.layout.spinner_item,
                    R.string.todo_usuario);

            this.usuarioSpinner.setAdapter(usuarioAdapter);
            ((CustomSpinnerAdapter) this.usuarioSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);


            factoryDateClickListener = new FactoryDateClickListener(this);
            factoryHourClickListener = new FactoryHourClickListener(this);

//		veiculoUsuarioSpinner = (Spinner) findViewById(R.id.veiculo_usuario_spinner);

//		this.veiculoUsuarioSpinner.setAdapter(veiculoUsuarioCustomSpinnerAdapter);
//		((CustomSpinnerAdapter) this.veiculoUsuarioSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);


            origemIdentificadorTextView= (TextView) findViewById(R.id.tv_identificador_origem);
            destinoIdentificadorTextView= (TextView) findViewById(R.id.tv_identificador_destino);

            this.inicioDataProgramadaButton = (Button) findViewById(R.id.inicio_data_programada_button);
            inicioDataProgramadaExibirClickListener = factoryDateClickListener.setDateClickListener(inicioDataProgramadaButton);

            this.inicioHoraProgramadaButton = (Button) findViewById(R.id.inicio_hora_programada_button);
            inicioHoraProgramadaExibirClickListener = factoryHourClickListener.setHourClickListener(inicioHoraProgramadaButton);

            this.inicioDataPrazoButton = (Button) findViewById(R.id.inicio_data_prazo_button);
            inicioDataPrazoExibirClickListener = factoryDateClickListener.setDateClickListener(inicioDataPrazoButton);

            this.inicioHoraPrazoButton = (Button) findViewById(R.id.inicio_hora_prazo_button);
            inicioHoraPrazoExibirClickListener = factoryHourClickListener.setHourClickListener(inicioHoraPrazoButton);



            this.dataExibirButton = (Button) findViewById(R.id.data_exibir_button);
            dataExibirClickListener = factoryDateClickListener.setDateClickListener(dataExibirButton);

            this.descricaoEditText = (EditText) findViewById(R.id.descricao_edittext);

            this.tituloEditText = (EditText) findViewById(R.id.titulo_edittext);

            this.origemButton = (Button) findViewById(R.id.origem_button);
            this.origemButton.setOnClickListener(new CadastrarEnderecoListener());

            this.destinoButton = (Button) findViewById(R.id.destino_button);
            this.destinoButton.setOnClickListener(new CadastrarEnderecoListener());

            removerOrigemButton= (ImageButton) findViewById(R.id.ib_remover_origem);
            this.removerOrigemButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    removerOrigem();
                }
            });

            removerDestinoButton= (ImageButton) findViewById(R.id.ib_remover_destino);
            this.removerDestinoButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    removerDestino();
                }
            });

            llOrigem = (LinearLayout) findViewById(R.id.ll_origem);
            llDestino= (LinearLayout) findViewById(R.id.ll_destino);

            origemImageButton = (ImageButton) findViewById(R.id.ib_tipo_endereco_origem);
            destinoImageButton= (ImageButton) findViewById(R.id.ib_tipo_endereco_destino);
        } catch (OmegaDatabaseException e) {
            SingletonLog.insereErro(e, TIPO.FORMULARIO);
        }finally{
            if(this.db != null )
                this.db.close();
        }

    }



    public ContainerLayoutEndereco addLayoutEndereco(
            String pIdBairro ,
            String pIdCidade ,
            String pIdEstado,
            String pIdPais,
            String pNomeBairro ,
            String pNomeCidade ,
            String pNomeEstado,
            String pNomePais,
            String pLogradouro ,
            String pNumero ,
            String pComplemento,
            LinearLayout pLinearLayoutEndereco,
            Button pAddButtonEndereco,
            EXTDAOTarefa.TIPO_ENDERECO tipoEndereco,
            String idTipoEndereco){
        //verifica se algum item do endereço está setado
        if(!(
                (pNomePais!= null && pNomePais.length() > 0)
                        || (pNomeEstado!= null && pNomeEstado.length() > 0)
                        || (pNomeCidade != null && pNomeCidade.length() > 0)
                        || (pNomeBairro != null && pNomeBairro.length() > 0)
                        || (pLogradouro!= null && pLogradouro.length() > 0)
        ))
            return null;

        AppointmentPlaceItemList appointmentPlaceItemList = new AppointmentPlaceItemList(
                getApplicationContext(),
                0,
                pLogradouro ,
                pNumero ,
                pComplemento,
                pNomeBairro,
                pNomeCidade,
                pNomeEstado,
                pNomePais,
                pIdBairro ,
                pIdCidade ,
                pIdEstado,
                pIdPais,
                null,
                null,
                null);
        appointmentPlaceItemList.tipoEndereco = tipoEndereco;
        appointmentPlaceItemList.idEntidade= idTipoEndereco;
        //		LinearLayout linearLayoutEndereco = (LinearLayout) findViewById(R.id.linearlayout_endereco);

        ContainerLayoutEndereco vContainerEndereco = new ContainerLayoutEndereco(
                this,
                appointmentPlaceItemList,
                pLinearLayoutEndereco);

        FormEnderecoAdapter adapter = new FormEnderecoAdapter(
                this,
                vContainerEndereco,
                pAddButtonEndereco);
        adapter.setClasseResultado(FormEnderecoOuEmpresaActivityMobile.class);
        View vNewView = adapter.getView(0, null, null);

        pLinearLayoutEndereco.addView(vNewView);

        vContainerEndereco.setAppointmentPlace(vNewView);

        return vContainerEndereco;
    }

    public ContainerLayoutEndereco addLayoutEndereco(
            EXTDAOTarefa.DadosEndereco dados,
            LinearLayout pLinearLayoutEndereco,
            Button pAddButtonEndereco){
        //verifica se algum item do endereço está setado
        if(!(
            (dados.idCidade != null && dados.idCidade.length() > 0)
                    || (dados.logradouro!= null && dados.logradouro.length() > 0)
        ))
            return null;


        AppointmentPlaceItemList appointmentPlaceItemList = AppointmentPlaceItemList.factory(
                db, dados.logradouro, dados.numero, dados.idCidade);


        appointmentPlaceItemList.setIdEntidade(
                dados.tipoEndereco
                , dados.id
                , dados.tipoEndereco != null
                        ? (
                        dados.tipoEndereco == EXTDAOTarefa.TIPO_ENDERECO.PESSOA
                                ? DetailPessoaActivityMobile.class
                                : DetailEmpresaActivityMobile.class
                        )
                        : null
                , dados.nome);

        ContainerLayoutEndereco vContainerEndereco = new ContainerLayoutEndereco(
                this,
                appointmentPlaceItemList,
                pLinearLayoutEndereco);

        FormEnderecoAdapter adapter = new FormEnderecoAdapter(
                this,
                vContainerEndereco,
                pAddButtonEndereco);

        adapter.setClasseResultado(FormEnderecoOuEmpresaActivityMobile.class);
        View vNewView = adapter.getView(0, null, null);

        pLinearLayoutEndereco.addView(vNewView);

        vContainerEndereco.setAppointmentPlace(vNewView);

        return vContainerEndereco;
    }

    public void initDrawableTipoEndereco(ImageButton ib, EXTDAOTarefa.TIPO_ENDERECO tipoEndereco){
        if(tipoEndereco == EXTDAOTarefa.TIPO_ENDERECO.EMPRESA) {

            ib.setImageResource(R.drawable.ico_company);
            ib.invalidate();
        } else if(tipoEndereco == EXTDAOTarefa.TIPO_ENDERECO.PESSOA){

            ib.setImageResource(R.drawable.ico_male);
            ib.invalidate();
        } else {
            ib.setVisibility(View.GONE);
        }

    }

    public void refreshOrigem( EXTDAOTarefa.DadosEndereco dados ){


        if(containerEnderecoOrigem != null)
            containerEnderecoOrigem.delete();

        containerEnderecoOrigem =addLayoutEndereco(
                dados,
                (LinearLayout) findViewById(R.id.endereco_origem_linearlayout),
                origemButton);

        if(containerEnderecoOrigem != null){

            origemIdentificadorTextView.setText(dados.nome);
            llOrigem.setVisibility(View.VISIBLE);
            origemButton.setVisibility(View.GONE);
            origemIdPessoaOuEmpresa = dados.id;
            origemTipoEndereco = dados.tipoEndereco;
            initDrawableTipoEndereco(origemImageButton, origemTipoEndereco);
        }
    }

    public void refreshDestino( EXTDAOTarefa.DadosEndereco dados ){
        destinoTipoEndereco = dados.tipoEndereco;

        if(containerEnderecoDestino != null)
            containerEnderecoDestino.delete();

        containerEnderecoDestino =addLayoutEndereco(
                dados,
                (LinearLayout) findViewById(R.id.endereco_destino_linearlayout),
                destinoButton);

        if(containerEnderecoDestino != null){

            destinoIdentificadorTextView.setText(dados.nome);
            llDestino.setVisibility(View.VISIBLE);
            destinoButton.setVisibility(View.GONE);
            destinoId = dados.id;
            initDrawableTipoEndereco(destinoImageButton, destinoTipoEndereco);
        }
    }

    private void removerDestino(){
        try{
            destinoTipoEndereco = null;
            destinoId=null;
            containerEnderecoDestino.delete();
            if(destinoButton != null) {
                destinoButton.setVisibility(View.VISIBLE);
                llDestino.setVisibility(View.GONE);
            }
        }catch(Exception ex){
            SingletonLog.openDialogError(this,ex,SingletonLog.TIPO.PAGINA);
        }

    }

    private void removerOrigem(){
        try{
            origemTipoEndereco = null;
            origemIdPessoaOuEmpresa = null;
            containerEnderecoOrigem.delete();
            if(origemButton != null) {
                origemButton.setVisibility(View.VISIBLE);
                llOrigem.setVisibility(View.GONE);
            }
        }catch(Exception ex){
            SingletonLog.openDialogError(this,ex,SingletonLog.TIPO.PAGINA);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {


            case OmegaConfiguration.ACTIVITY_FORM_ENDERECO:
                switch (resultCode) {
                    case OmegaConfiguration.ACTIVITY_FORM_DELETE_LAYOUT_ENDERECO:
                        int vIdLinearLayout = intent.getIntExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, -1);
                        if(vIdLinearLayout == R.id.endereco_destino_linearlayout && containerEnderecoDestino!= null){
                            removerDestino();
                        } else if(vIdLinearLayout == R.id.endereco_origem_linearlayout && containerEnderecoOrigem != null){
                           removerOrigem();
                        }
                        break;

                    default:
                        try{
                            String vIdCidade = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE);

                            String vIdEstado = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO);
                            String vIdPais = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_PAIS);
                            String vNomeCidade = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE);
                            String vNomeBairro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_BAIRRO);
                            String vNomeEstado = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO);
                            String vNomePais = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS);
                            String vLogradouro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO);
                            String vNumero = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO);

                            Integer intIdTipoTarefa = intent.getIntExtra(OmegaConfiguration.SEARCH_FIELD_ID_TIPO_ENDERECO_TAREFA, 0);
                            intIdTipoTarefa = intIdTipoTarefa==0 ? null : intIdTipoTarefa;
                            String idTipoTarefa =intIdTipoTarefa != null ? intIdTipoTarefa.toString() : null;
                            EXTDAOTarefa.TIPO_ENDERECO tipoEndereco = EXTDAOTarefa.TIPO_ENDERECO.getTipoEndereco(intIdTipoTarefa);

                            String idIdentificador = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID);
                            String nome = null;
                            if(intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_NOME))
                                nome = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME);


                            LinearLayout vLinearLayoutEndereco = (LinearLayout) findViewById(R.id.linearlayout_endereco);
                            Integer vEnderecoLayout = -1;

                            if(intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT))
                                vEnderecoLayout = intent.getIntExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, -1);

                            if(vEnderecoLayout != -1)
                                vLinearLayoutEndereco = (LinearLayout) findViewById(vEnderecoLayout);
                            else
                                vLinearLayoutEndereco = (LinearLayout) findViewById(R.id.linearlayout_endereco);

                            Button vButtonAddEndereco = null;

                            if(vEnderecoLayout == R.id.endereco_origem_linearlayout){
                                origemTipoEndereco = tipoEndereco;
                                vButtonAddEndereco = this.origemButton;
                                if(containerEnderecoOrigem != null)
                                    containerEnderecoOrigem.delete();
                                containerEnderecoOrigem = addLayoutEndereco(
                                        null,
                                        vIdCidade,
                                        vIdEstado,
                                        vIdPais,
                                        vNomeBairro,
                                        vNomeCidade,
                                        vNomeEstado,
                                        vNomePais,
                                        vLogradouro,
                                        vNumero,
                                        null,
                                        vLinearLayoutEndereco,
                                        vButtonAddEndereco,
                                        tipoEndereco,
                                        idTipoTarefa);
                                if(containerEnderecoOrigem != null){

                                    origemIdentificadorTextView.setText(nome);

                                    llOrigem.setVisibility(View.VISIBLE);
                                    origemButton.setVisibility(View.GONE);
                                    origemIdPessoaOuEmpresa = idIdentificador;
                                    origemTipoEndereco = tipoEndereco;
                                    initDrawableTipoEndereco(origemImageButton, origemTipoEndereco);
                                }
                            }
                            else if(vEnderecoLayout == R.id.endereco_destino_linearlayout){
                                destinoTipoEndereco = tipoEndereco;
                                vButtonAddEndereco = this.destinoButton;
                                if(containerEnderecoDestino != null)
                                    containerEnderecoDestino.delete();
                                containerEnderecoDestino = addLayoutEndereco(
                                        null,
                                        vIdCidade,
                                        vIdEstado,
                                        vIdPais,
                                        vNomeBairro,
                                        vNomeCidade,
                                        vNomeEstado,
                                        vNomePais,
                                        vLogradouro,
                                        vNumero,
                                        null,
                                        vLinearLayoutEndereco,
                                        vButtonAddEndereco,
                                        tipoEndereco,
                                        idTipoTarefa);

                                if(containerEnderecoDestino != null) {
                                    destinoIdentificadorTextView.setText(nome);

                                    destinoId = idIdentificador;
                                    destinoTipoEndereco = tipoEndereco;
                                    llDestino.setVisibility(View.VISIBLE);
                                    destinoButton.setVisibility(View.GONE);
                                    initDrawableTipoEndereco(destinoImageButton, destinoTipoEndereco);
                                }
                            }

                        }catch(Exception ex){
                            SingletonLog.insereErro(ex, TIPO.FORMULARIO);
                        }
                        break;
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, intent);
                break;
        }

    }


    private class CadastrarEnderecoListener implements OnClickListener
    {

        Intent intent = null;


        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.origem_button:

                    intent = new Intent(getApplicationContext(), FormEnderecoOuEmpresaActivityMobile.class);
                    intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.endereco_origem_linearlayout);
                    if (containerEnderecoOrigem != null) {
                        AppointmentPlaceItemList vAppPlaceItemList = containerEnderecoOrigem.getAppointmentPlaceItemList();
                        vAppPlaceItemList.putDataInIntent(intent);
                    }
                    break;

                case R.id.destino_button:

                    intent = new Intent(getApplicationContext(), FormEnderecoOuEmpresaActivityMobile.class);
                    intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.endereco_destino_linearlayout);
                    if (containerEnderecoDestino != null) {
                        AppointmentPlaceItemList vAppPlaceItemList = containerEnderecoDestino.getAppointmentPlaceItemList();
                        vAppPlaceItemList.putDataInIntent(intent);
                    }
                    break;


                default:
                    break;
            }

            if(intent != null)
            {
                try{

                    startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_ENDERECO);
                }catch(Exception ex){
                    SingletonLog.insereErro(ex, TIPO.PAGINA);
                }
            }
        }
    }


    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {

            case FactoryDateClickListener.DATE_DIALOG_ID:
                factoryDateClickListener.onPrepareDialog(id, dialog);
                break;
            case FactoryHourClickListener.TIME_DIALOG_ID:
                factoryHourClickListener.onPrepareDialog(id, dialog);
                break;
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        Dialog vDialog = null;

        switch (id) {
            case FactoryDateClickListener.DATE_DIALOG_ID:
                return factoryDateClickListener.onCreateDialog(id);
            case FactoryHourClickListener.TIME_DIALOG_ID:
                return factoryHourClickListener.onCreateDialog(id);

            default:
                vDialog = this.getDialog(id);
                break;

        }

        return vDialog;

    }

    public Dialog getDialog(int dialogType)
    {

        Dialog vDialog = new Dialog(this);
        vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        vDialog.setContentView(R.layout.dialog);

        TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
        Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

        switch (dialogType) {

            case FORM_ERROR_DIALOG_DATA_EXIBIR_MAIOR_QUE_DATA_PROGRAMADA:
                vTextView.setText(getResources().getString(R.string.error_data_exibir_maior_que_data_programada));
                vOkButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        dismissDialog(FORM_ERROR_DIALOG_DATA_EXIBIR_MAIOR_QUE_DATA_PROGRAMADA);
                    }
                });
                break;

            case FORM_ERROR_DIALOG_CATEGORIA_PERMISSAO:
                vTextView.setText(getResources().getString(R.string.form_tarefa_error_dialog_tarefa_spinner_categoria_permissao));
                vOkButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        dismissDialog(FORM_ERROR_DIALOG_CATEGORIA_PERMISSAO);
                    }
                });
                break;
            case FORM_ERROR_DIALOG_TIPO_ENDERECO_PESSOA:
                vTextView.setText(getResources().getString(R.string.form_tarefa_error_dialog_tipo_endereco_pessoa));
                vOkButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        dismissDialog(FORM_ERROR_DIALOG_TIPO_ENDERECO_PESSOA);
                    }
                });
                break;
            case FORM_ERROR_DIALOG_TIPO_ENDERECO_EMPRESA:
                vTextView.setText(getResources().getString(R.string.form_tarefa_error_dialog_tipo_endereco_empresa));
                vOkButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        dismissDialog(FORM_ERROR_DIALOG_TIPO_ENDERECO_EMPRESA);
                    }
                });
                break;
            case FORM_ERROR_DIALOG_USUARIO:
                vTextView.setText(getResources().getString(R.string.form_tarefa_error_dialog_tarefa_spinner_usuario));
                vOkButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        dismissDialog(FORM_ERROR_DIALOG_USUARIO);
                    }
                });
                break;
            case FORM_ERROR_DIALOG_VEICULO:
                vTextView.setText(getResources().getString(R.string.form_tarefa_error_dialog_tarefa_spinner_veiculo));
                vOkButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        dismissDialog(FORM_ERROR_DIALOG_VEICULO);
                    }
                });
                break;
            case FORM_ERROR_DIALOG_VEICULO_USUARIO:
                vTextView.setText(getResources().getString(R.string.form_tarefa_error_dialog_tarefa_spinner_veiculo_usuario));
                vOkButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        dismissDialog(FORM_ERROR_DIALOG_VEICULO_USUARIO);
                    }
                });
                break;
            case FORM_ERROR_DIALOG_TIPO_TAREFA:
                vTextView.setText(getResources().getString(R.string.form_tarefa_error_dialog_tipo_tarefa));
                vOkButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        dismissDialog(FORM_ERROR_DIALOG_TIPO_TAREFA);
                    }
                });
                break;
            case FORM_ERROR_DIALOG_ENDERECO_ORIGEM:
                vTextView.setText(getResources().getString(R.string.error_missing_endereco_origem));
                vOkButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        dismissDialog(FORM_ERROR_DIALOG_ENDERECO_ORIGEM);
                    }
                });
                break;
            default:
                return super.getDialog(dialogType);
        }

        return vDialog;
    }



    public class LoadEnderecoHandler extends Handler{



        @Override
        public void handleMessage(Message msg)
        {


            try{

                EXTDAOTarefa.TIPO_ENDERECO tipoEndereco = null;
                if(msg.what == DESTINO)
                    tipoEndereco= destinoTipoEndereco;
                else if(msg.what == ORIGEM)
                    tipoEndereco= origemTipoEndereco;
                if(tipoEndereco != null)
                {
                    String destinoEndereco = null;
                    if(msg.what == DESTINO)
                        destinoEndereco = destinoId;
                    else if(msg.what == ORIGEM)
                        destinoEndereco = origemIdPessoaOuEmpresa;
                    if(!destinoEndereco.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB)){

                        String vIdBairro = null;
                        String vIdCidade = null;
                        String vIdEstado = null;
                        String vIdPais = null;
                        String vNomeBairro = null;
                        String vNomeCidade = null;
                        String vNomeEstado = null;
                        String vNomePais = null;
                        String vLogradouro = null;
                        String vComplemento = null;
                        String vNumero = null;
                        Table vObj = null;
                        switch (tipoEndereco) {
                            case EMPRESA:

                                vObj = new EXTDAOEmpresa(db);
                                vObj.setAttrValue(EXTDAOEmpresa.ID, destinoEndereco);

                                break;
                            case PESSOA:
                                vObj = new EXTDAOPessoa(db);
                                vObj.setAttrValue(EXTDAOPessoa.ID, destinoEndereco);
                                break;
                        }
                        if(vObj != null && vObj.select()){
                            vIdBairro = vObj.getStrValueOfAttribute(Table.BAIRRO_ID_INT_UNIVERSAL);
                            if(vIdBairro != null){
                                EXTDAOBairro vObjBairro = new EXTDAOBairro(db);
                                vNomeBairro = vObjBairro.getStrValueOfAttribute(EXTDAOBairro.NOME);
                            }
                            vIdCidade = vObj.getStrValueOfAttribute(Table.CIDADE_ID_INT_UNIVERSAL);
                            if(vIdCidade != null){
                                EXTDAOCidade vObjCidade = new EXTDAOCidade(db);
                                vNomeCidade = vObjCidade.getStrValueOfAttribute(EXTDAOCidade.NOME);
                            }
                            vIdEstado = vObj.getStrValueOfAttribute(Table.ESTADO_ID_INT_UNIVERSAL);
                            if(vIdEstado != null){
                                EXTDAOUf vObjUf = new EXTDAOUf(db);
                                vNomeEstado = vObjUf.getStrValueOfAttribute(EXTDAOUf.NOME);
                            }
                            vIdPais = vObj.getStrValueOfAttribute(Table.PAIS_ID_INT_UNIVERSAL);
                            if(vIdPais != null){
                                EXTDAOPais vObjPais = new EXTDAOPais(db);
                                vNomePais = vObjPais.getStrValueOfAttribute(EXTDAOPais.NOME);
                            }
                            vLogradouro = vObj.getStrValueOfAttribute(Table.LOGRADOURO_UNIVERSAL);
                            vComplemento = vObj.getStrValueOfAttribute(Table.COMPLEMENTO_UNIVERSAL);
                            vNumero = vObj.getStrValueOfAttribute(Table.NUMERO_UNIVERSAL);



                            Intent vIntent = new Intent();
                            vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE, vIdCidade);
                            vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO, vIdBairro);
                            vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO, vIdEstado);
                            vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PAIS, vIdPais);
                            vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE, vNomeBairro);
                            vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_BAIRRO, vNomeCidade);
                            vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO, vNomeEstado);
                            vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS, vNomePais);
                            vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO, vLogradouro);
                            vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO, vNumero);
                            vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO, vComplemento);
                            if(msg.what == DESTINO)
                                vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.endereco_destino_linearlayout);
                            else if(msg.what == ORIGEM)
                                vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.endereco_origem_linearlayout);

                            onActivityResult(OmegaConfiguration.ACTIVITY_FORM_ENDERECO, OmegaConfiguration.ACTIVITY_FORM_ADD_LAYOUT_ENDERECO, vIntent) ;
                        }
                        db.close();
                    }

                } else{
                    Intent vIntent = new Intent();
                    if(msg.what == DESTINO)
                        vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.endereco_destino_linearlayout);
                    else if(msg.what == ORIGEM)
                        vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.endereco_origem_linearlayout);
                    onActivityResult(OmegaConfiguration.ACTIVITY_FORM_ENDERECO, OmegaConfiguration.ACTIVITY_FORM_DELETE_LAYOUT_ENDERECO, vIntent) ;
                }
            }catch(Exception ex){
                SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
            }
        }
    }


    @Override
    public void clearComponents(ViewGroup vg) {
        if(containerEnderecoDestino != null)
            containerEnderecoDestino.delete();

        if(containerEnderecoOrigem != null)
            containerEnderecoOrigem.delete();

        if(origemButton.getVisibility() == View.GONE)
        {
            origemButton.setVisibility(View.VISIBLE);
        }

        if(destinoButton.getVisibility() == View.GONE)
        {
            destinoButton.setVisibility(View.VISIBLE);
        }

        if(FactoryDateClickListener.isDateButtonSet(this, inicioDataProgramadaButton))
        {
            String vStrData = getResources().getString(R.string.form_selecionar_data);
            this.inicioDataProgramadaButton.setText(vStrData);

        }

        if(FactoryHourClickListener.isHourButtonSet(this, inicioHoraProgramadaButton))
        {
            String vStrHora = getResources().getString(R.string.form_selecionar_hora);
            this.inicioHoraProgramadaButton.setText(vStrHora);
        }


        if(FactoryDateClickListener.isDateButtonSet(this, inicioDataPrazoButton))
        {
            String vStrData = getResources().getString(R.string.form_selecionar_data);
            this.inicioDataPrazoButton.setText(vStrData);

        }

        if(FactoryHourClickListener.isHourButtonSet(this, inicioHoraPrazoButton))
        {
            String vStrHora = getResources().getString(R.string.form_selecionar_hora);
            this.inicioHoraPrazoButton.setText(vStrHora);
        }


        if(FactoryDateClickListener.isDateButtonSet(this, dataExibirButton))
        {
            String vStrData = getResources().getString(R.string.form_selecionar_data);

            this.dataExibirButton.setText(vStrData);
        }



        if(!this.descricaoEditText.getText().toString().equals(""))
        {
            this.descricaoEditText.setText("");

        }

        if(!this.tituloEditText.getText().toString().equals(""))
        {
            this.tituloEditText.setText("");

        }
    }



    @Override
    protected ContainerCadastro cadastrando() {

        try{

            Table vObj = new EXTDAOTarefa(db);
            boolean vValidade = true;
            if(id != null){
                vObj.setAttrValue(EXTDAOTarefa.ID, id);
                vObj.select();
            }
            HelperDate hd = new HelperDate();
            vObj.setAttrValue(EXTDAOTarefa.CRIADO_PELO_USUARIO_ID_INT, OmegaSecurity.getIdUsuario());
            vObj.setLongAttrValue(EXTDAOPessoa.CADASTRO_SEC, HelperDate.getTimestampSegundosUTC(this));
            vObj.setIntegerAttrValue(EXTDAOPessoa.CADASTRO_OFFSEC, HelperDate.getOffsetSegundosTimeZone());


            if(containerEnderecoOrigem != null)
            {
                AppointmentPlaceItemList appointmentPlaceItemList = containerEnderecoOrigem.getAppointmentPlaceItemList();
                if(appointmentPlaceItemList != null){

                    vObj.setAttrValue(EXTDAOTarefa.ORIGEM_NUMERO, appointmentPlaceItemList.getNumber());
                    vObj.setAttrValue(EXTDAOTarefa.ORIGEM_CIDADE_ID_INT, appointmentPlaceItemList.getIdCity());

                    vObj.setAttrValue(EXTDAOTarefa.ORIGEM_LOGRADOURO, appointmentPlaceItemList.getAddress());
                }
            }

            //Se o botao estiver invisivel eh pq o endereco foi escolhido
            if(containerEnderecoDestino != null)
            {
                AppointmentPlaceItemList appointmentPlaceItemList = containerEnderecoDestino.getAppointmentPlaceItemList();
                if(appointmentPlaceItemList != null){

                    vObj.setAttrValue(EXTDAOTarefa.DESTINO_NUMERO, appointmentPlaceItemList.getNumber());
                    String idCidade=  appointmentPlaceItemList.getIdCity();
                    vObj.setAttrValue(EXTDAOTarefa.DESTINO_CIDADE_ID_INT, idCidade);
                    vObj.setAttrValue(EXTDAOTarefa.DESTINO_LOGRADOURO, appointmentPlaceItemList.getAddress());
                }
            }

            if(FactoryDateClickListener.isDateButtonSet(FormTarefaActivityMobile.this, inicioDataProgramadaButton))
            {
                String data = inicioDataProgramadaButton.getText().toString();
                if(FactoryHourClickListener.isHourButtonSet(FormTarefaActivityMobile.this, inicioHoraProgramadaButton))
                {
                    String hora = HelperDate.getHoraFormatadaSQL( inicioHoraProgramadaButton.getText().toString());
                    vObj.setStrDatetimeAttrValue(
                            EXTDAOTarefa.INICIO_HORA_PROGRAMADA_SEC
                            , EXTDAOTarefa.INICIO_HORA_PROGRAMADA_OFFSEC
                            , data + " " +hora );
                } else{
                    vObj.setStrDateAttrValue(EXTDAOTarefa.INICIO_HORA_PROGRAMADA_SEC, EXTDAOTarefa.INICIO_HORA_PROGRAMADA_OFFSEC, data);
                }
            }

            if(FactoryDateClickListener.isDateButtonSet(FormTarefaActivityMobile.this, inicioDataPrazoButton))
            {
                String data = inicioDataPrazoButton.getText().toString();
                if(FactoryHourClickListener.isHourButtonSet(FormTarefaActivityMobile.this, inicioHoraPrazoButton))
                {
                    String hora = HelperDate.getHoraFormatadaSQL( inicioHoraPrazoButton.getText().toString());
                    vObj.setStrDatetimeAttrValue(
                            EXTDAOTarefa.INICIO_HORA_PROGRAMADA_SEC
                            , EXTDAOTarefa.INICIO_HORA_PROGRAMADA_OFFSEC
                            , data + " " +hora );
                } else{
                    vObj.setStrDateAttrValue(EXTDAOTarefa.PRAZO_SEC, EXTDAOTarefa.PRAZO_OFFSEC, data);
                }
            }


//
//			if(FactoryDateClickListener.isDateButtonSet(FormTarefaActivityMobile.this, dataExibirButton))
//			{
//				if(FactoryDateClickListener.isDateButtonSet(FormTarefaActivityMobile.this, inicioDataProgramadaButton)){
//					Date vDateInicioDataProgramada = inicioDataProgramadaExibirClickListener.getDate();
//					Date vDateExibir = dataExibirClickListener.getDate();
//					if(vDateExibir.compareTo(vDateInicioDataProgramada) > 0){
//						return new ContainerCadastro(FORM_ERROR_DIALOG_DATA_EXIBIR_MAIOR_QUE_DATA_PROGRAMADA, false);
//					}
//				}
////			String vData = dataExibirButton.getText().toString();
////			vObj.setAttrValue(EXTDAOTarefa.DATA_EXIBIR_DATE, vData);
//
//			}



            if(!tituloEditText.getText().toString().equals(""))
            {
                vObj.setAttrValue(EXTDAOTarefa.TITULO, tituloEditText.getText().toString());
            }

            if(!descricaoEditText.getText().toString().equals(""))
            {
                vObj.setAttrValue(EXTDAOTarefa.DESCRICAO, descricaoEditText.getText().toString());
            }


            if(origemTipoEndereco != null)
            {
                switch (origemTipoEndereco) {
                    case PESSOA:
                        vObj.setAttrValue(EXTDAOTarefa.ORIGEM_PESSOA_ID_INT, origemIdPessoaOuEmpresa);
                        break;

                    case EMPRESA:
                        vObj.setAttrValue(EXTDAOTarefa.ORIGEM_EMPRESA_ID_INT , origemIdPessoaOuEmpresa);
                        break;

                    default:
                        break;
                }
            }

            if(destinoTipoEndereco != null)
            {
                switch (destinoTipoEndereco) {
                    case PESSOA:
                        vObj.setAttrValue(EXTDAOTarefa.DESTINO_PESSOA_ID_INT, destinoId);
                        break;

                    case EMPRESA:
                        vObj.setAttrValue(EXTDAOTarefa.DESTINO_EMPRESA_ID_INT , destinoId);
                        break;

                    default:
                        break;
                }
            }

            String selectedUsuario= String.valueOf(usuarioSpinner.getSelectedItemId());
            if (!selectedUsuario.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB)) {
                vObj.setAttrValue(EXTDAOTarefa.USUARIO_ID_INT, selectedUsuario);
            }

            vObj.setAttrValue(EXTDAOTarefa.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
            if(vValidade){

                if(id != null){
                    vObj.setAttrValue(EXTDAOTarefa.ID, id);
                    vObj.formatToSQLite();
                    if(vObj.update(true)){
                        setBancoFoiModificado();
                        return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);
                    } else{

                        return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
                    }
                }else {
                    HelperDate vHelperDate = new HelperDate();
//				vObj.setAttrValue(EXTDAOTarefa.DATA_CADASTRO_DATE, vHelperDate.getDateDisplay());
//				vObj.setAttrValue(EXTDAOTarefa.HORA_CADASTRO_TIME, vHelperDate.getTimeDisplay());
                    vObj.formatToSQLite();
                    if(vObj.insert(true) != null){
//					clearComponents();
                        handlerClearComponents.sendEmptyMessage(0);
                        return new ContainerCadastro(DIALOG_CADASTRO_OK, true);
                    }else{

                        return new ContainerCadastro( FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
                    }
                }
            }
            else{
                return new ContainerCadastro( FORM_ERROR_DIALOG_CAMPO_INVALIDO, false);
            }
        }catch(Exception ex){
            SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);

            return new ContainerCadastro( new Mensagem(ex), false);
        }
    }

}

