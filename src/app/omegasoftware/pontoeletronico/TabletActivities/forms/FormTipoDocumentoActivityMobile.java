package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.os.Bundle;
import android.widget.AutoCompleteTextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoDocumento;

public class FormTipoDocumentoActivityMobile extends OmegaCadastroActivity {

	//Constants
	public static final String TAG = "FormTipoDocumentoActivityMobile";
	public static final String TABELAS_RELACIONADAS[] = new String[]{EXTDAOTipoDocumento.NAME};

	//Spinners
	private DatabasePontoEletronico db=null;

	String idTipoDocumentoInput = "";
	String idTipoDocumentoTextView = "";

	private AutoCompleteTextView tipoDocumentoAutoCompletEditText;

	TableAutoCompleteTextView tipoDocumentoContainerAutoComplete = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_tipo_documento_activity_mobile_layout);

		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();

	}
	
	@Override
	protected void loadEdit(){
try{
		EXTDAOTipoDocumento vObjTipoDocumento = new EXTDAOTipoDocumento(db);
		vObjTipoDocumento.setAttrValue(EXTDAOTipoDocumento.ID, id);
		vObjTipoDocumento.select();

		String vTipoDocumento = vObjTipoDocumento.getStrValueOfAttribute(EXTDAOTipoDocumento.NOME);
		if(vTipoDocumento != null){
			tipoDocumentoAutoCompletEditText.setText(vTipoDocumento);
		}
}catch(Exception ex){
	SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
}
	}


	@Override
	public boolean loadData() {
		return true;
	}

	@Override
	public void beforeInitializeComponents() {

		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

				

		EXTDAOTipoDocumento vObjTipoDocumento = new EXTDAOTipoDocumento(this.db);
		this.tipoDocumentoAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.tipo_documento_autocompletetextview);
		this.tipoDocumentoContainerAutoComplete = new TableAutoCompleteTextView(this, vObjTipoDocumento, tipoDocumentoAutoCompletEditText);
		new MaskUpperCaseTextWatcher(tipoDocumentoAutoCompletEditText, 100);
		

		db.close();
	}

	@Override
	protected void onDestroy(){
		try{
			this.db.close();	
		}catch(Exception ex){
		}
		super.onDestroy();
	}

	@Override
	public void finish(){
		try{
		
			this.db.close();	
		}catch(Exception ex){
		}
		super.finish();
	}



	@Override
	protected ContainerCadastro cadastrando() {
	
		

		String vStrNomeTipoDocumento = tipoDocumentoAutoCompletEditText.getText().toString();
		if(vStrNomeTipoDocumento.length() == 0)
		{
			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_TIPO_DOCUMENTO, false);
		}else{
			EXTDAOTipoDocumento vObjTipoDocumento = new EXTDAOTipoDocumento(db);

			vObjTipoDocumento.setAttrValue(EXTDAOTipoDocumento.NOME, vStrNomeTipoDocumento);
			vObjTipoDocumento.setAttrValue(EXTDAOTipoDocumento.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			if(id != null){
				vObjTipoDocumento.setAttrValue(EXTDAOTipoDocumento.ID, id);
				vObjTipoDocumento.formatToSQLite();
				if(vObjTipoDocumento.update(true)){
					setBancoFoiModificado();
					return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);
				} else{
					return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
				}
			} else{
				if(tipoDocumentoContainerAutoComplete.getIdOfTokenIfExist(vStrNomeTipoDocumento) != null){
					return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
				} else{
					String vIdTipoDocumento = tipoDocumentoContainerAutoComplete.addTupleIfNecessay(vObjTipoDocumento);
					if(vIdTipoDocumento == null){
						
						return new ContainerCadastro(FORM_ERROR_DIALOG_INSERCAO, false);
					} else{
						
						return new ContainerCadastro(DIALOG_CADASTRO_OK, true);
					}
				}
			}
		}	
			
	}


}


