package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.os.Bundle;
import android.widget.AutoCompleteTextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoEmpresa;

public class FormTipoEmpresaActivityMobile extends OmegaCadastroActivity {

	//Constants
	public static final String TAG = "FormTipoEmpresaActivityMobile";
	public static final String TABELAS_RELACIONADAS[] = new String[]{EXTDAOTipoEmpresa.NAME};

	//Spinners
	//private DatabasePontoEletronico db=null;

	String idTipoEmpresaInput = "";
	String idTipoEmpresaTextView = "";

	private AutoCompleteTextView tipoEmpresaAutoCompletEditText;

	TableAutoCompleteTextView tipoEmpresaContainerAutoComplete = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_tipo_empresa_activity_mobile_layout);
	
		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL_AND_END).execute();

	}

	@Override
	protected void loadEdit(){
//try{
//		EXTDAOTipoEmpresa vObjTipoEmpresa = new EXTDAOTipoEmpresa(db);
//		vObjTipoEmpresa.setAttrValue(EXTDAOTipoEmpresa.ID, id);
//		vObjTipoEmpresa.select();
//
//		String vTipoEmpresa = vObjTipoEmpresa.getStrValueOfAttribute(EXTDAOTipoEmpresa.NOME);
//		if(vTipoEmpresa != null){
//			tipoEmpresaAutoCompletEditText.setText(vTipoEmpresa);
//		}
//}catch(Exception ex){
//	SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
//}
	}


	@Override
	public boolean loadData() {
		return true;
	}



	@Override
	protected void beforeInitializeComponents() {

//		this.db = new DatabasePontoEletronico(this);
//
//				
//
//		EXTDAOTipoEmpresa vObjTipoEmpresa = new EXTDAOTipoEmpresa(this.db);
//		this.tipoEmpresaAutoCompletEditText = (AutoCompleteTextView)
//				findViewById(R.id.tipo_empresa_autocompletetextview);
//		this.tipoEmpresaContainerAutoComplete = new TableAutoCompleteTextView(this, vObjTipoEmpresa, tipoEmpresaAutoCompletEditText);
//		new MaskUpperCaseTextWatcher(tipoEmpresaAutoCompletEditText, 100);
//
//
//		db.close();
	}



	@Override
	protected ContainerCadastro cadastrando() {
	
		return null;

//		String vStrNomeTipoEmpresa = tipoEmpresaAutoCompletEditText.getText().toString();
//		if(vStrNomeTipoEmpresa.length() == 0)
//		{
//			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_NOME, false);
//		}else{
//			EXTDAOTipoEmpresa vObjTipoEmpresa = new EXTDAOTipoEmpresa(db);
//
//			vObjTipoEmpresa.setAttrValue(EXTDAOTipoEmpresa.NOME, vStrNomeTipoEmpresa);
//			vObjTipoEmpresa.setAttrValue(EXTDAOTipoEmpresa.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
//			if(id != null){
//				vObjTipoEmpresa.setAttrValue(EXTDAOTipoEmpresa.ID, id);
//				vObjTipoEmpresa.formatToSQLite();
//				if(vObjTipoEmpresa.update(true)){
//					setBancoFoiModificado();
//					return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);
//				} else {
//					return new ContainerCadastro(FORM_ERROR_DIALOG_EDICAO, false);
//				}
//			} else{
//				if(tipoEmpresaContainerAutoComplete.getIdOfTokenIfExist(vStrNomeTipoEmpresa) != null){
//					
//					return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
//				} else{
//					String vIdTipoEmpresa = tipoEmpresaContainerAutoComplete.addTupleIfNecessay(vObjTipoEmpresa);
//					if(vIdTipoEmpresa == null){
//						
//						return new ContainerCadastro(FORM_ERROR_DIALOG_INSERCAO, false);
//					} else{
//						return new ContainerCadastro(DIALOG_CADASTRO_OK, true);
//					}
//				}
//			}
//		}	
//		
		
	}
	

}


