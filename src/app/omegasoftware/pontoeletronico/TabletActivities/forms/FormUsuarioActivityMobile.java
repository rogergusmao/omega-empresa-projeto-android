package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.TrocarEmailUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.FormPessoaAdapter;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskLowerCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.common.dialog.HelperDialog;
import app.omegasoftware.pontoeletronico.database.HelperCheckField;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioCategoriaPermissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioCorporacao;
import app.omegasoftware.pontoeletronico.http.HelperHttp;
import app.omegasoftware.pontoeletronico.webservice.ServicosCobranca;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class FormUsuarioActivityMobile extends OmegaCadastroActivity {

	public static final String TAG = "FormUsuarioActivityMobile";

	private final int FORM_ERROR_DIALOG_SENHA_INCORRETA = 5;
	private final int FORM_ERROR_DIALOG_MISSING_SENHA = 6;
	private final int FORM_ERROR_DIALOG_MISSING_NOME_USUARIO = 13;

	private final int FORM_ERROR_DIALOG_MISSING_CATEGORIA_PERMISSAO_OR_IS_ADM = 20;
	private final int FORM_ERROR_DIALOG_MISSING_USUARIO_TIPO = 21;
	private final int FORM_ERROR_DIALOG_PESSOA_DUPLICADA = 22;

	private final int FORM_ERROR_DIALOG_MISSING_EMAIL = 7;
	private final int FORM_ERROR_DIALOG_MISSING_CORPORACAO = 8;
	private final int FORM_ERROR_DIALOG_CORPORACAO_DUPLICADA = 9;
	private final int FORM_ERROR_DIALOG_EMAIL_DUPLICADO = 12;

	private final int DIALOG_CADASTRO_USUARIO_REALIZADO_COM_SUCESSO = 654456;

	private DatabasePontoEletronico db=null;
	private EXTDAOUsuario objUsuario;
	private EXTDAOPessoa objPessoa;
	private EXTDAOPessoaUsuario objPessoaUsuario;
	// EditText
	private EditText nomeEditText;
	private EditText senhaEditText;
	private Button trocarEmailButton;
	private EditText emailEditText;
	private View viewFragmentFormPessoa;
	private EditText emailPessoaEditText;
	private EditText nomePessoaEditText;
	FormPessoaAdapter formPessoaAdapter;

	DialogInterface.OnClickListener dialogUsuarioExistenteMasNaoNaEmpresaClickListener;
	DialogInterface.OnClickListener dialogEmailUsuarioExistenteClickListener;
	DialogInterface.OnClickListener dialogClickListener;
	DialogInterface.OnClickListener dialogConcluirCadastroClickListener;
	CheckBox isConsumidorCheckbox;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_usuario_activity_mobile_layout);

		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() throws OmegaDatabaseException {
		this.db = new DatabasePontoEletronico(this);

		objUsuario = new EXTDAOUsuario(db);
		objPessoaUsuario = new EXTDAOPessoaUsuario(db);

		return true;
	}

	@Override
	public void beforeInitializeComponents() {
		try {
			if (!HelperHttp.isConnected(this)) {
				showDialog(FORM_ERROR_DIALOG_DESCONECTADO);
				return;
			}

			// Se cadastro
			if (this.emailEditText == null) {
				this.emailEditText = (EditText) findViewById(R.id.form_usuario_email_edittext);
				new MaskLowerCaseTextWatcher(this.emailEditText, 255);
			}

			if (this.nomeEditText == null) {
				this.nomeEditText = (EditText) findViewById(R.id.form_usuario_nome_edittext);
				new MaskUpperCaseTextWatcher(this.nomeEditText, 255);
			}

			if (this.senhaEditText == null)
				this.senhaEditText = (EditText) findViewById(R.id.form_usuario_senha_edittext);

			if (trocarEmailButton == null)
				trocarEmailButton = (Button) findViewById(R.id.trocar_email_button);
			
			
			if (viewFragmentFormPessoa == null) {
				viewFragmentFormPessoa = findViewById(R.id.fragment_form_pessoa);
				isConsumidorCheckbox = (CheckBox) viewFragmentFormPessoa.findViewById(R.id.is_consumidor_checkbox);
				nomePessoaEditText = (EditText) viewFragmentFormPessoa.findViewById(R.id.nome_edittext);
				emailPessoaEditText = (EditText) viewFragmentFormPessoa.findViewById(R.id.email_edittext);
			}

			isConsumidorCheckbox.setVisibility(View.GONE);
			nomePessoaEditText.setVisibility(View.GONE);
			emailPessoaEditText.setVisibility(View.GONE);
			// pessoaSpinner = (Spinner)
			// findViewById(R.id.pessoa_usuario_spinner);
			// pessoaAdapter = new CustomSpinnerAdapter(getApplicationContext(),
			// EXTDAOPessoaUsuario.getAllPessoaUsuario(db),R.layout.spinner_item,R.string.selecione);
			// this.pessoaSpinner.setAdapter(pessoaAdapter);
			// ((CustomSpinnerAdapter)
			// this.pessoaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

			if (id != null) {

				objUsuario.select(id);

				objPessoaUsuario.setAttrValue(EXTDAOPessoaUsuario.USUARIO_ID_INT, objUsuario.getId());
				Table registroPessoaUsuario = objPessoaUsuario.getFirstTupla();
				if (registroPessoaUsuario != null) {
					objPessoaUsuario = (EXTDAOPessoaUsuario) registroPessoaUsuario;
					objPessoa = (EXTDAOPessoa) objPessoaUsuario
							.getObjDaChaveExtrangeira(EXTDAOPessoaUsuario.PESSOA_ID_INT);
				} else {
					objPessoaUsuario = null;
				}
				if (formPessoaAdapter == null) {
					formPessoaAdapter = new FormPessoaAdapter((OmegaCadastroActivity) this, objPessoa.getId(),
							viewFragmentFormPessoa);
					formPessoaAdapter.inicializa();
				}

				// Se for o proprio usuario o email e nome podem ser alterados,
				// porem a senha deve ser alterada em Configuracao -> Alterar
				// Senha

				senhaEditText.setVisibility(View.GONE);
				senhaEditText.setVisibility(View.GONE);
				nomeEditText.setVisibility(View.VISIBLE);
				trocarEmailButton.setVisibility(View.VISIBLE);
				
				if( !this.id.equalsIgnoreCase(OmegaSecurity.getIdUsuario())){
					trocarEmailButton.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							try{
								FactoryAlertDialog.showDialog(FormUsuarioActivityMobile.this, FormUsuarioActivityMobile.this.getString(R.string.somente_o_usuario_pode_trocar_email));
							}catch(Exception ex){
								SingletonLog.insereErro(ex, TIPO.FORMULARIO);	
							}

						}
					});
					
				}else{
					trocarEmailButton.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							
							Intent intent = new Intent(FormUsuarioActivityMobile.this,
									TrocarEmailUsuarioActivityMobile.class);
							intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_USUARIO, id);
							FormUsuarioActivityMobile.this.setBancoFoiModificado();
							FormUsuarioActivityMobile.this.startActivityForResult(intent,
									OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);

						}
					});
	
				}
				
				emailEditText.setVisibility(View.GONE);
			} else {
				trocarEmailButton.setVisibility(View.GONE);
				emailEditText.setVisibility(View.VISIBLE);

				viewFragmentFormPessoa.setVisibility(View.GONE);
				formPessoaAdapter = new FormPessoaAdapter((OmegaCadastroActivity) this, null, viewFragmentFormPessoa);
				formPessoaAdapter.inicializa();

				setTituloCabecalho(getResources().getString(R.string.usuario));
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}
	}

	@Override
	protected void loadEdit() {
		try {
			objUsuario.select(id);

			objPessoaUsuario.setAttrValue(EXTDAOPessoaUsuario.USUARIO_ID_INT, objUsuario.getId());
			Table registroPessoaUsuario = objPessoaUsuario.getFirstTupla();
			if (registroPessoaUsuario != null) {
				objPessoaUsuario = (EXTDAOPessoaUsuario) registroPessoaUsuario;
				objPessoa = (EXTDAOPessoa) objPessoaUsuario.getObjDaChaveExtrangeira(EXTDAOPessoaUsuario.PESSOA_ID_INT);
			} else {
				objPessoaUsuario = null;
			}

			if (formPessoaAdapter == null) {
				formPessoaAdapter = new FormPessoaAdapter((OmegaCadastroActivity) this, objPessoa.getId(),
						viewFragmentFormPessoa);

				formPessoaAdapter.inicializa();
			}

			formPessoaAdapter.loadEdit();

			// Se for o proprio usuario o email e nome podem ser alterados,
			// porem a senha deve ser alterada em Configuracao -> Alterar Senha

			trocarEmailButton.setText(objUsuario.getStrValueOfAttribute(EXTDAOUsuario.EMAIL));

			String emailUsuario = objUsuario.getStrValueOfAttribute(EXTDAOUsuario.EMAIL);
			emailEditText.setText(emailUsuario);

			String nomeUsuario = objUsuario.getStrValueOfAttribute(EXTDAOUsuario.NOME);
			if (nomeUsuario != null && nomeUsuario.length() > 0)
				nomeEditText.setText(nomeUsuario);
			nomeEditText.clearFocus();

			setTituloCabecalho(nomeUsuario);
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}
	}

	@Override
	protected Dialog onCreateDialog(int dialogType) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {

		case DIALOG_CADASTRO_USUARIO_REALIZADO_COM_SUCESSO:
			vTextView.setText(getResources().getString(R.string.dialog_cadastro_usuario_realizado_com_sucesso));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(DIALOG_CADASTRO_USUARIO_REALIZADO_COM_SUCESSO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_SENHA_INCORRETA:
			vTextView.setText(getResources().getString(R.string.error_form_senha_repetida));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_SENHA_INCORRETA);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_USUARIO_TIPO:
			vTextView.setText(getResources().getString(R.string.error_missing_usuario_tipo));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_USUARIO_TIPO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_PESSOA_DUPLICADA:
			vTextView.setText(getResources().getString(R.string.error_pessoa_duplicada));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_PESSOA_DUPLICADA);
				}
			});
			break;

		case FORM_ERROR_DIALOG_MISSING_CATEGORIA_PERMISSAO_OR_IS_ADM:
			vTextView.setText(getResources().getString(R.string.error_missing_categoria_permissao_or_is_adm));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_CATEGORIA_PERMISSAO_OR_IS_ADM);
				}
			});
			break;

		case FORM_ERROR_DIALOG_MISSING_CORPORACAO:
			vTextView.setText(getResources().getString(R.string.error_missing_corporacao));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_CORPORACAO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_CORPORACAO_DUPLICADA:
			vTextView.setText(getResources().getString(R.string.error_corporacao_duplicada));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_CORPORACAO_DUPLICADA);
				}
			});
			break;
		case FORM_ERROR_DIALOG_EMAIL_DUPLICADO:
			vTextView.setText(getResources().getString(R.string.email_usuario_existente_no_sistema_e_na_corporacao));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_EMAIL_DUPLICADO);
				}
			});
			break;

		case FORM_ERROR_DIALOG_MISSING_EMAIL:
			vTextView.setText(getResources().getString(R.string.error_missing_email));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_EMAIL);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_SENHA:
			vTextView.setText(getResources().getString(R.string.error_missing_senha));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_SENHA);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_NOME_USUARIO:
			vTextView.setText(getResources().getString(R.string.error_missing_nome_usuario));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_NOME_USUARIO);
				}
			});
			break;

		default:
			return super.onCreateDialog(dialogType);

		}

		return vDialog;
	}

	@Override
	protected ContainerCadastro cadastrando() throws Exception{
		
		Table obj = new EXTDAOUsuario(db);
		String senha = "";
		String email  = "";
		String nome = "";
		if(id != null){
			obj.setAttrValue(EXTDAOUsuario.ID, id);
			obj.select();
		}		
		nome = nomeEditText.getText().toString();
		email = emailEditText.getText().toString();
		senha = senhaEditText.getText().toString();
		//		Se cadastro
		if(id == null){
 
			if(nomeEditText.getText().toString().equals(""))
			{	
				return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_NOME_USUARIO, false);
			}

			
			if(!senha.equals(""))
			{
				senha = senhaEditText.getText().toString();					
			}else{
				
				return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_SENHA, false);
			}

		} else if(id != null ){
//					Se for edicao
			
			if(nome.equals(""))
			{	
				return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_NOME_USUARIO, false);
			}

		}

//		String vSelectedPessoa = String.valueOf(pessoaSpinner.getSelectedItemId());
		
		
//		if(msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO){
//			showDialog(id);
//		}
		if(id == null){
			ServicosCobranca servicos = new ServicosCobranca(this );
			InterfaceMensagem msg = servicos.conecta();
			if(msg.ok()){
				JSONObject ret = servicos.cadastrarClienteNaCorporacao(
						nome, 
						email, 
						senha);
				//NOVO - pede para o usuurio aguardar a proxima sincronizacao
				// ou para deslogar e logar novamente
				int mCodRetorno = ret.getInt("mCodRetorno");
				if(ret == null){
					return new ContainerCadastro(FORM_ERROR_DIALOG_SERVER_OFFLINE, false);
				}
				else if(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.isEqual(mCodRetorno)){
					return new ContainerCadastro(DIALOG_CADASTRO_USUARIO_REALIZADO_COM_SUCESSO, true);
				}
				else if(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.isEqual(mCodRetorno)){
					return new ContainerCadastro(DIALOG_ERRO_FATAL, false);
				}else if(PROTOCOLO_SISTEMA.TIPO.NUMERO_USUARIOS_EXCEDEU.isEqual(mCodRetorno)){
					
					return new ContainerCadastro(HelperDialog.FORM_EXCEDEU_PACOTE, false, ret.getString("mMensagem")); 
				}
				else {
					return new ContainerCadastro(ret.isNull("mMensagem") ? null : ret.getString("mMensagem"));
				}
			}
			else return new ContainerCadastro(msg.mMensagem);
		}
		else{ //Se edicao 
				obj.setAttrValue(EXTDAOUsuario.NOME, nome);
				if(obj.update(true)){
					setBancoFoiModificado();
					
					ContainerCadastro container = this.formPessoaAdapter.cadastrando(email);
					if(!container.result){
						return container;
					} else {
						return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true); 
					}
				} else {
					throw new Exception("Erro durante a atualizacao do usuurio: " + id);
				}
		}
		
			
		}

	// @Override
	// protected void onPostExecute(Boolean result) {
	// super.onPostExecute(result);
	// controlerProgressDialog.dismissProgressDialog();
	// if(dialogId != null)
	// showDialog(dialogId);
	// else if(dialogConcluirCadastroClickListener != null){
	// FactoryAlertDialog.showAlertDialog(
	// FormUsuarioActivityMobile.this,
	// getString(R.string.concluir_cadastro) ,
	// dialogConcluirCadastroClickListener);
	// } else if(dialogUsuarioExistenteMasNaoNaEmpresaClickListener != null){
	// FactoryAlertDialog.showAlertDialog(
	// FormUsuarioActivityMobile.this,
	// getString(R.string.confirmar_edicao) ,
	// dialogUsuarioExistenteMasNaoNaEmpresaClickListener);
	// } else if(dialogEmailUsuarioExistenteClickListener != null){
	// FactoryAlertDialog.showAlertDialog(
	// FormUsuarioActivityMobile.this,
	// getString(R.string.email_usuario_existente_mas_nao_na_corporacao) ,
	// dialogEmailUsuarioExistenteClickListener);
	// }
	// }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		switch (requestCode) {
		case OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW:
			trocarEmailButton.setText(OmegaSecurity.getEmail());
			break;
		case OmegaConfiguration.STATE_FORM_CONFIRMACAO:
			switch (resultCode) {
			case RESULT_OK:
				Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO, false);
				if (vConfirmacao) {
					new ProcedureConfirmaCadastroLoader(this).execute();
				}
				break;

			default:
				break;
			}
			break;

		default:
			super.onActivityResult(requestCode, resultCode, intent);
			break;
		}

		// Se encontrar o requestCode
		if (formPessoaAdapter.onActivityResult(requestCode, resultCode, intent))
			return;
	}

	public class ProcedureConfirmaCadastroLoader extends AsyncTask<String, Integer, Boolean> {

		Integer dialogId = null;
		Activity activity;
Exception e=null;
		public ProcedureConfirmaCadastroLoader(Activity pActivity) {
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@SuppressWarnings("unused")
		@Override
		protected Boolean doInBackground(String... params) {
			try {
				String vOutNomeUsuario = "";
				String vOutSenha = "";
				String vOutEmail = "";
				String vStrsenha = "";
				String vPessoa = "";
				boolean vValidade = true;
				// Se cadastro
				if (id == null) {
					if (!nomeEditText.getText().toString().equals("")) {
						String vStrNome = nomeEditText.getText().toString();

						if (!(vStrNome.length() > 0)) {
							nomeEditText.setBackgroundColor(Color.RED);
							vValidade = false;
							// Se for cadastro
							if (id == null) {
								dialogId = FORM_ERROR_DIALOG_MISSING_NOME_USUARIO;
								return false;
							}
						} else {
							vOutNomeUsuario = nomeEditText.getText().toString();
						}
					}
					// First we check the obligatory fields:
					// Plano, Especialidade, Cidade
					if (!emailEditText.getText().toString().equals("")) {
						String vToken = emailEditText.getText().toString();
						if (!HelperCheckField.checkEmail(vToken)) {
							emailEditText.setBackgroundColor(Color.RED); // white
							vValidade = false;
							dialogId = FORM_ERROR_DIALOG_MISSING_EMAIL;
							return false;
						} else {
							vOutEmail = emailEditText.getText().toString();
						}
					}

					if (!senhaEditText.getText().toString().equals("")) {
						vStrsenha = senhaEditText.getText().toString();

						if (!(vStrsenha.length() > 0)) {
							senhaEditText.setBackgroundColor(Color.RED);
							vValidade = false;

							dialogId = FORM_ERROR_DIALOG_MISSING_SENHA;
							return false;
						} else {
							vOutSenha = senhaEditText.getText().toString();
						}
					}

				} else if (id != null && id.compareTo(OmegaSecurity.getIdUsuario()) == 0) {
					// Se for edicao do proprio usuario
					String vNome = nomeEditText.getText().toString();
					if (vNome == null || vNome.compareTo("") == 0) {
						dialogId = FORM_ERROR_DIALOG_MISSING_NOME_USUARIO;
						return false;
					} else
						vOutNomeUsuario = nomeEditText.getText().toString();
				}

				boolean vIsAdm = false;
				String vCategoriaPermissao = "";
				vIsAdm = true;

				// Se cadastro
				if (id == null) {

					dialogId = DIALOG_CADASTRO_OK;

					return true;

				}

				else {
					EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(db);
					vObjUsuario.setAttrValue(EXTDAOUsuario.ID, id);
					vObjUsuario.select();

					EXTDAOUsuarioCategoriaPermissao vUCP = vObjUsuario.getObjUsuarioCategoriaPermissao();
					String vIdUsuarioCategoriaPermissao = null;
					if (vUCP != null) {
						vIdUsuarioCategoriaPermissao = vUCP.getStrValueOfAttribute(EXTDAOUsuarioCategoriaPermissao.ID);
					}
					EXTDAOUsuarioCorporacao vObjUC = vObjUsuario.getObjUsuarioCorporacao();
					String vIdUsuarioCorporacao = null;
					if (vObjUC != null)
						vIdUsuarioCorporacao = vObjUC.getStrValueOfAttribute(EXTDAOUsuarioCorporacao.ID);

				}
			} catch (Exception e) {
				this.e=e;
			} finally {
				db.close();
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
				if(SingletonLog.openDialogError(FormUsuarioActivityMobile.this, e, SingletonLog.TIPO.PAGINA)){
					return;
				}
				if (dialogId != null)
					showDialog(dialogId);
			} catch (Exception ex) {
				SingletonLog.openDialogError(FormUsuarioActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}

	@Override
	public void clearComponents(ViewGroup vg) {

		formPessoaAdapter.clearComponents();
		super.clearComponents(vg);

	}

	protected class IsAdmButtonListener implements OnClickListener {
		public void onClick(View v) {
			// if(isAdmCheckBox.isChecked()){
			// categoriaPermissaoAndroidSpinner.setVisibility(View.GONE);
			// } else{
			// categoriaPermissaoAndroidSpinner.setVisibility(View.VISIBLE);
			// }
		}
	}

}
