package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.TextWatcher.GrupoTextWatcher;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskLowerCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.HelperCheckField;
import app.omegasoftware.pontoeletronico.http.HelperHttp;
import app.omegasoftware.pontoeletronico.webservice.ServicosCobranca;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class FormUsuarioECorporacaoActivityMobile extends OmegaCadastroActivity {

	// Constants
	private static final String TAG = "FormUsuarioECorporacaoActivityMobile";

	private final int DIALOG_CORPORACAO_E_USUARIO_CRIADOS_COM_SUCESSO = 3;
	private final int FORM_ERROR_DIALOG_CADASTRO = 10;

	private final int FORM_ERROR_DIALOG_DESCONECTADO = 4;
	private final int FORM_ERROR_DIALOG_SENHA_INCORRETA = 5;
	private final int FORM_ERROR_DIALOG_MISSING_SENHA = 6;
	private final int FORM_ERROR_DIALOG_MISSING_NOME_USUARIO = 13;
	private final int FORM_ERROR_DIALOG_MISSING_EMAIL = 7;

	private final int FORM_ERROR_DIALOG_MISSING_CORPORACAO = 8;
	private final int FORM_ERROR_DIALOG_CORPORACAO_DUPLICADA = 9;
	private final int FORM_ERROR_DIALOG_EMAIL_DUPLICADO = 12;
	private final int FORM_ERROR_DIALOG_DO_USUARIO_JA_CADASTRADO = 18;

	// EditText
	private EditText nomeEditText;
	private EditText senhaEditText;

	private EditText emailEditText;
	private EditText corporacaoEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_usuario_e_corporacao_activity_mobile_layout);

		new CustomDataLoader(this).execute();

	}

	@Override
	public boolean loadData() {

		return true;

	}

	@Override
	protected void beforeInitializeComponents() {

		if (!HelperHttp.isConnected(this)) {
			showDialog(FORM_ERROR_DIALOG_DESCONECTADO);
			return;
		}
		this.emailEditText = (EditText) findViewById(R.id.form_usuario_email_edittext);
		new MaskLowerCaseTextWatcher(this.emailEditText, 255);

		this.nomeEditText = (EditText) findViewById(R.id.form_usuario_nome_edittext);
		new MaskUpperCaseTextWatcher(this.nomeEditText, 255);

		this.senhaEditText = (EditText) findViewById(R.id.form_usuario_senha_edittext);

		this.corporacaoEditText = (EditText) findViewById(R.id.form_usuario_corporacao_edittext);
		new GrupoTextWatcher(this.corporacaoEditText);
		inibirBotaoSalvarToolbar();
	}

	@Override
	protected Dialog onCreateDialog(int dialogType) {
		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
		case DIALOG_CORPORACAO_E_USUARIO_CRIADOS_COM_SUCESSO:
			vTextView.setText(getResources().getString(R.string.cadastro_ok));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(DIALOG_CORPORACAO_E_USUARIO_CRIADOS_COM_SUCESSO);

					Intent intent = getIntent();

					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_EMAIL, emailEditText.getText().toString());
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_SENHA, senhaEditText.getText().toString());
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_CORPORACAO,
							corporacaoEditText.getText().toString());

					setResult(RESULT_OK, intent);
					finish();

				}
			});
			break;
		case FORM_ERROR_DIALOG_DO_USUARIO_JA_CADASTRADO:
			vTextView.setText(getResources().getString(R.string.senha_invalida_do_usuario_ja_cadastrado));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_DO_USUARIO_JA_CADASTRADO);
				}
			});
			break;

		case FORM_ERROR_DIALOG_DESCONECTADO:
			vTextView.setText(getResources().getString(R.string.error_form_desconectado));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_DESCONECTADO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_CADASTRO:
			vTextView.setText(getResources().getString(R.string.error_cadastro));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_CADASTRO);
				}
			});
			break;

		case FORM_ERROR_DIALOG_SENHA_INCORRETA:
			vTextView.setText(getResources().getString(R.string.error_form_senha_repetida));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_SENHA_INCORRETA);
				}
			});
			break;

		case FORM_ERROR_DIALOG_MISSING_CORPORACAO:
			vTextView.setText(getResources().getString(R.string.error_missing_corporacao));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_CORPORACAO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_CORPORACAO_DUPLICADA:
			vTextView.setText(getResources().getString(R.string.error_corporacao_duplicada));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_CORPORACAO_DUPLICADA);
				}
			});
			break;
		case FORM_ERROR_DIALOG_EMAIL_DUPLICADO:
			vTextView.setText(getResources().getString(R.string.error_email_duplicado));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_EMAIL_DUPLICADO);
				}
			});
			break;

		case FORM_ERROR_DIALOG_MISSING_EMAIL:
			vTextView.setText(getResources().getString(R.string.error_missing_email));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_EMAIL);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_SENHA:
			vTextView.setText(getResources().getString(R.string.error_missing_senha));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_SENHA);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_NOME_USUARIO:
			vTextView.setText(getResources().getString(R.string.error_missing_nome_usuario));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_NOME_USUARIO);
				}
			});
			break;

		default:
			return super.getDialog(dialogType);

		}

		return vDialog;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if (requestCode == OmegaConfiguration.STATE_FORM_CONFIRMACAO) {
			if (resultCode == RESULT_OK) {
				Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO, false);
				if (vConfirmacao) {
					new ProcedureCadastroConfirmacaoLoader(this).execute();

				}
			}
		} else
			super.onActivityResult(requestCode, resultCode, intent);
	}

	public class ProcedureCadastroConfirmacaoLoader extends AsyncTask<String, Integer, Boolean> {

		Integer dialogId = null;
		Activity activity;
		Mensagem msg = null;
		Exception e = null;

		public ProcedureCadastroConfirmacaoLoader(Activity pActivity) {
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {

				String vOutCorporacao = "";
				String vOutNomeUsuario = "";
				String vOutSenha = "";
				// String vOutEmail = "";

				boolean vValidade = true;

				if (!corporacaoEditText.getText().toString().equals("")) {
					vOutCorporacao = corporacaoEditText.getText().toString();
				} else {
					// corporacaoEditText.setBackgroundColor(Color.RED);
					vValidade = false;

					dialogId = FORM_ERROR_DIALOG_MISSING_CORPORACAO;
					return false;
				}

				// First we check the obligatory fields:
				// Plano, Especialidade, Cidade
				if (!emailEditText.getText().toString().equals("")) {
					String vToken = emailEditText.getText().toString();
					if (!HelperCheckField.checkEmail(vToken)) {
						dialogId = FORM_ERROR_DIALOG_MISSING_EMAIL;
						vValidade = false;
						return false;
					}
					// else {
					// vOutEmail = emailEditText.getText().toString();
					// }
				}

				if (!nomeEditText.getText().toString().equals("")) {
					String vStrNome = nomeEditText.getText().toString();

					if (!(vStrNome.length() > 0)) {

						vValidade = false;
						dialogId = FORM_ERROR_DIALOG_MISSING_NOME_USUARIO;
						return false;
					} else {
						vOutNomeUsuario = nomeEditText.getText().toString();
					}
				}

				String vStrsenha = "";
				if (!senhaEditText.getText().toString().equals("")) {
					vStrsenha = senhaEditText.getText().toString();

					if (!(vStrsenha.length() > 0)) {
						// senhaEditText.setBackgroundColor(Color.RED);
						vValidade = false;
						dialogId = FORM_ERROR_DIALOG_MISSING_SENHA;
						return false;
					} else {
						vOutSenha = senhaEditText.getText().toString();
					}
				}

				if (vValidade) {

					ServicosCobranca servicoCO = new ServicosCobranca(activity.getApplicationContext());

					JSONObject jsonObj = servicoCO.criarCorporacaoEUsuarioEmUmaAssinaturaGratuita(vOutCorporacao,
							vOutNomeUsuario, emailEditText.toString(), vOutSenha);
					if (jsonObj == null) {
						dialogId = FORM_ERROR_DIALOG_DESCONECTADO;
						return false;
					}
					Mensagem msg = Mensagem.factory(jsonObj);

					if (msg == null) {
						dialogId = FORM_ERROR_DIALOG_DESCONECTADO;
						return false;
					} else if (msg.ok()) {

						dialogId = DIALOG_CORPORACAO_E_USUARIO_CRIADOS_COM_SUCESSO;
						// clearComponents();
						handlerClearComponents.sendEmptyMessage(0);
						return true;
					} else {
						this.msg = msg;
						return false;
					}
				}
			} catch (Exception e) {

				this.e = e;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				if (SingletonLog.openDialogError(FormUsuarioECorporacaoActivityMobile.this, e,
						SingletonLog.TIPO.PAGINA)) {
					return;
				} 
				if (dialogId != null)
					showDialog(dialogId);
				else if (msg != null) {
					FactoryAlertDialog.showDialog(activity, msg);
				}
			} catch (Exception ex) {
				SingletonLog.openDialogError(FormUsuarioECorporacaoActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally {
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}
	// ---------------------------------------------------------------
	// ----------------- Methods related to search action ------------
	// ---------------------------------------------------------------
	// Handles search button click

	//
	// public class CadastroButtonLoader extends AsyncTask<String, Integer,
	// Boolean>
	// {
	//
	// Integer dialogId = null;
	DialogInterface.OnClickListener dialogCadastroClickListenner = null;

	@Override
	protected ContainerCadastro cadastrando() throws Exception {

		if (!HelperHttp.isConnected(FormUsuarioECorporacaoActivityMobile.this)) {

			return new ContainerCadastro(FORM_ERROR_DIALOG_DESCONECTADO, false);
		}
		String vOutCorporacao = "";
		String vOutNomeUsuario = "";
		String vOutSenha = "";
		String vOutEmail = "";

		if (corporacaoEditText.getText().length() > 0) {
			vOutCorporacao = corporacaoEditText.getText().toString();
		} else {
			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_CORPORACAO, false);
		}

		// First we check the obligatory fields:
		// Plano, Especialidade, Cidade
		if (emailEditText.getText().toString().length() > 0) {
			String vToken = emailEditText.getText().toString();
			if (!HelperCheckField.checkEmail(vToken)) {

				return new ContainerCadastro(FORM_ERROR_DIALOG_INVALIDO_EMAIL, false);
			} else {
				vOutEmail = emailEditText.getText().toString();
			}
		} else {
			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_EMAIL, false);
		}

		if (nomeEditText.getText().toString().length() > 0) {
			vOutNomeUsuario = nomeEditText.getText().toString();

		} else {
			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_NOME_USUARIO, false);
		}

		if (senhaEditText.getText().toString().length() > 0) {
			vOutSenha = senhaEditText.getText().toString();

		} else {
			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_SENHA, false);
		}

		ServicosCobranca servicos = new ServicosCobranca(this);

		JSONObject json = servicos.criarCorporacaoEUsuarioEmUmaAssinaturaGratuita(vOutCorporacao, vOutNomeUsuario,
				vOutEmail, vOutSenha);
		if (json == null) {

			return new ContainerCadastro(FORM_ERROR_DIALOG_DESCONECTADO, false);
		} else if (json.getInt("mCodRetorno") == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {
			return new ContainerCadastro(DIALOG_CORPORACAO_E_USUARIO_CRIADOS_COM_SUCESSO, false);
		} else {

			return new ContainerCadastro(json.isNull("mMensagem") ? null : json.getString("mMensagem"));
		}
	}

	@Override
	protected void loadEdit() {

	}

}
