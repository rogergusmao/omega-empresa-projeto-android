package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.FormVeiculoUsuarioAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.VeiculoUsuarioItemList;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskNumberTextWatcher;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOModelo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculoUsuario;


public class FormVeiculoActivityMobile extends OmegaCadastroActivity {

	//Constants
	public static final String TAG = "FormVeiculoActivityMobile";
	

	private final static int FORM_ERROR_DIALOG_ANO_MODELO = 5;
	private final static int FORM_ERROR_DIALOG_NOME_MODELO  = 6;
	private final static int FORM_ERROR_DIALOG_PLACA = 7;


	private DatabasePontoEletronico db=null;
	
	//EditText
	private EditText placaEditText;
	private EditText anoModeloEditText;
	private Button adicionarMotoristaButton;
	
	private AutoCompleteTextView nomeModeloAutoCompletEditText;
	
	TableAutoCompleteTextView containerAutoCompleteNomeModelo;
	ContainerLayoutVeiculoUsuario containerVeiculoUsuario = new ContainerLayoutVeiculoUsuario(this);
	ArrayList<String> idsVeiculoUsuario = null;
	ArrayList<Table> usuarios;
	EXTDAOVeiculoUsuario objVeiculoUsuario;
	EXTDAOVeiculo objVeiculo;
	LinearLayout llMotoristas;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, R.string.veiculo);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_veiculo_activity_mobile_layout);
	
		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {
		try{
			db = new DatabasePontoEletronico(this);
			objVeiculo = new EXTDAOVeiculo(db);
			if(id != null){
				objVeiculo.select(id);
				objVeiculoUsuario = new EXTDAOVeiculoUsuario(db);
				objVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.VEICULO_ID_INT, id);
				usuarios = new ArrayList<Table>();
				idsVeiculoUsuario = new ArrayList<String>();
				ArrayList<Table> veiculoUsuario = objVeiculoUsuario.getListTable();
				if(veiculoUsuario != null){
					for(int i = 0; i < veiculoUsuario.size(); i++){
						EXTDAOUsuario objUsuario = new EXTDAOUsuario(db);
						Table empresaRede = veiculoUsuario.get(i);
						idsVeiculoUsuario.add(empresaRede.getId());
						String strIdUsuario = empresaRede.getStrValueOfAttribute(EXTDAOVeiculoUsuario.USUARIO_ID_INT);
						
						objUsuario.select(strIdUsuario);
						usuarios.add(objUsuario);
					}
				}	
			}
			
			
			return true;

		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			return false;
		}

		
		
	}

	@Override
	protected void beforeInitializeComponents() {
		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		llMotoristas = (LinearLayout) findViewById(R.id.ll_motoristas);
		handlerClearComponents.addLayout(llMotoristas);
		this.placaEditText = (EditText) findViewById(R.id.placa_edittext);
	
		this.placaEditText = (EditText) findViewById(R.id.placa_edittext);
		new MaskUpperCaseTextWatcher(this.placaEditText, 20);
        
        EXTDAOModelo vObjModelo = new EXTDAOModelo(this.db);
		this.nomeModeloAutoCompletEditText = (AutoCompleteTextView)
				findViewById(R.id.nome_modelo_autocompletetextview);
		this.containerAutoCompleteNomeModelo = new TableAutoCompleteTextView(this, vObjModelo, nomeModeloAutoCompletEditText);
		new MaskUpperCaseTextWatcher(nomeModeloAutoCompletEditText, 100);
        
		this.anoModeloEditText = (EditText) findViewById(R.id.ano_modelo_edittext);
        new MaskNumberTextWatcher(this.anoModeloEditText, 4);
        

		adicionarMotoristaButton = (Button) findViewById(R.id.cadastro_motorista_button);
		this.adicionarMotoristaButton.setOnClickListener(new CadastrarEmpresaRedeListener());
		if(usuarios != null){
			for(int i = 0; i < usuarios.size(); i++){
				Table regUsuario = usuarios.get(i);
				addLayoutMotorista(idsVeiculoUsuario.get(i), regUsuario.getId(), 
						regUsuario.getStrValueOfAttribute(EXTDAOUsuario.EMAIL));	
			}	
		}
		
		
	}	

	
	private class CadastrarEmpresaRedeListener implements OnClickListener
	{

		Intent intent = null;

		public void onClick(View v) {


			try{
				intent = new Intent(getApplicationContext(), FormEntidadeDependenteActivity.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_TABELA, EXTDAOUsuario.NAME);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ATRIBUTO_DESCRICAO, EXTDAOUsuario.EMAIL);
				startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_VEICULO_USUARIO);
			}catch(Exception ex){
				
				SingletonLog.insereErro(ex, TIPO.FORMULARIO);
			}

		}
	}
	


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		switch (requestCode) {
		case OmegaConfiguration.ACTIVITY_FORM_VEICULO_USUARIO:
			try{
				String id = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID);
				String descritivo = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_CONTEUDO_DESCRITIVO);
				addLayoutMotorista(null, id, descritivo);
			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.FORMULARIO);
	
			}
			break;
		
		default:
			super.onActivityResult(requestCode, resultCode, intent);
			break;
		}
	}
	@Override
	protected void loadEdit(){
		try{
			String vIdModelo = objVeiculo.getStrValueOfAttribute(EXTDAOVeiculo.MODELO_ID_INT );
			String vPlaca = objVeiculo.getStrValueOfAttribute(EXTDAOVeiculo.PLACA);
			
			EXTDAOModelo vObjModelo = new EXTDAOModelo(this.db);
			vObjModelo.setAttrValue(EXTDAOModelo.ID, vIdModelo);
			vObjModelo.select();
			String vAno = vObjModelo.getStrValueOfAttribute(EXTDAOModelo.ANO_INT);
			String vModelo = vObjModelo.getStrValueOfAttribute(EXTDAOModelo.NOME);
			
	        placaEditText.setText(vPlaca);
	        nomeModeloAutoCompletEditText.setText(vModelo);
	        anoModeloEditText.setText(vAno);
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);

		}
	}
	
	protected LinkedHashMap<String, String> getAllModelo()
	{
		EXTDAOModelo vObj = new EXTDAOModelo(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}
	
	protected String[] getVetorAllModelo()
	{
		EXTDAOModelo vObj = new EXTDAOModelo(this.db);
		return vObj.getVetorStringDefinition(true);
	}
	
	protected String[] getVetorAllAnoModelo()
	{
		EXTDAOModelo vObj = new EXTDAOModelo(this.db);
		return vObj.getVetorStringDefinition(true);
	}


	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;

		
		vDialog = this.getDialog(id);
		
		return vDialog;

	}	

	public Dialog getDialog(int dialogType)
	{

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
		case FORM_ERROR_DIALOG_ANO_MODELO:
			vTextView.setText(getResources().getString(R.string.error_missing_ano_modelo));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_ANO_MODELO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_NOME_MODELO:
			vTextView.setText(getResources().getString(R.string.error_missing_nome_modelo));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_NOME_MODELO);
				}
			});
			break;
			
		case FORM_ERROR_DIALOG_PLACA:
			vTextView.setText(getResources().getString(R.string.error_missing_placa));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_PLACA);
				}
			});
			break;
		default:
			return super.getDialog(dialogType);
		}

		return vDialog;
	}


	
	public void addLayoutMotorista(
			String idVeiculoUsuario,
			String pIdUsuario,
			String pEmailUsuario){
		if(pIdUsuario == null || pIdUsuario.length() == 0 ){
			return;
		} 
		if(! containerVeiculoUsuario.containsTuple(pIdUsuario)){

			VeiculoUsuarioItemList itemList = new VeiculoUsuarioItemList(
					idVeiculoUsuario,
					pIdUsuario,
					null, 
					null, 
					null,
					pEmailUsuario);
			
			ArrayList<VeiculoUsuarioItemList> places = new ArrayList<VeiculoUsuarioItemList>();
			places.add(itemList);

			FormVeiculoUsuarioAdapter adapter = new FormVeiculoUsuarioAdapter(
					getApplicationContext(),
					places, 
					containerVeiculoUsuario);
			View newView = adapter.getView(0, null, null);
			containerVeiculoUsuario.add(idVeiculoUsuario, pIdUsuario, newView);

			llMotoristas.addView(newView);
		}

	}
	
		@Override
		protected ContainerCadastro cadastrando() {
			
			EXTDAOVeiculo vObj = new EXTDAOVeiculo(db);
			boolean vValidade = true;
			//First we check the obligatory fields:
			if(!placaEditText.getText().toString().equals(""))
			{
				vObj.setAttrValue(EXTDAOVeiculo.PLACA, placaEditText.getText().toString());
			}else{
				
				return new ContainerCadastro(FORM_ERROR_DIALOG_PLACA, false);
			}
			
			String vStrNomeModelo  = nomeModeloAutoCompletEditText.getText().toString();
			if(vStrNomeModelo == null || vStrNomeModelo.length() == 0)
			{
				return new ContainerCadastro(FORM_ERROR_DIALOG_NOME_MODELO, false);
			}
			
			String vStrAnoModelo  = anoModeloEditText.getText().toString();
			if(vStrAnoModelo == null || vStrAnoModelo.length() == 0)
			{
				
				return new ContainerCadastro(FORM_ERROR_DIALOG_ANO_MODELO, false);
			}

			EXTDAOModelo vObjModelo = new EXTDAOModelo(db);
			vObjModelo.setAttrValue(EXTDAOModelo.NOME, vStrNomeModelo);
			vObjModelo.setAttrValue(EXTDAOModelo.ANO_INT, vStrAnoModelo);
			vObjModelo.setAttrValue(EXTDAOModelo.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			
			if(vValidade){
				String idNomeModelo = containerAutoCompleteNomeModelo.addTupleIfNecessay(vObjModelo);
				
				if(idNomeModelo == null) {
					
					return new ContainerCadastro(FORM_ERROR_DIALOG_CAMPO_INVALIDO, false);
				}
				else{
					vObj.setAttrValue(EXTDAOVeiculo.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
					vObj.setAttrValue(EXTDAOVeiculo.MODELO_ID_INT, idNomeModelo);
//						Se edicao
					if(id != null){
						vObj.setAttrValue(EXTDAOVeiculo.ID, id);
						vObj.formatToSQLite();
						if(vObj.update(true)){
							
							containerVeiculoUsuario.addListTupleInUpdate(db, id);
							
							setBancoFoiModificado();
							return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);
						} else{
							
							return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
						}
					}else {
						vObj.formatToSQLite();
						Long idVeiculo = vObj.insert(true); 
						if(idVeiculo != null){
							containerVeiculoUsuario.addListTupleInInsert(db, String.valueOf(idVeiculo));
							
							handlerClearComponents.sendEmptyMessage(0);
							
							return new ContainerCadastro(DIALOG_CADASTRO_OK, true);
						}else{
							
							return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
						}
					}
				}
			}
			else{
				
				return new ContainerCadastro(FORM_ERROR_DIALOG_CAMPO_INVALIDO, false);
			}
			
		}

}


