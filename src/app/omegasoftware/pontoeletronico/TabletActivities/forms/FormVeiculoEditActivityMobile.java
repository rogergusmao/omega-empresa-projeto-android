package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import android.content.Intent;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;

public class FormVeiculoEditActivityMobile extends FormEditActivityMobile {
	
	public static final String TAG = "FormVeiculoEditActivityMobile";
	
	@Override
	protected void procedureAlertAfterDeleteTupla(String pId){
		sendBroadCastToReceiveFileOfServerActivity(pId);
		
	}
	

	private void sendBroadCastToReceiveFileOfServerActivity(String pId)
	{
		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SERVICE_VEICULO_GPS_LOCATION_LISTENER);
		updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, pId);
		sendBroadcast(updateUIIntent);
	}

}


