package app.omegasoftware.pontoeletronico.TabletActivities.forms;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculoUsuario;


public class FormVeiculoUsuarioActivityMobile extends OmegaCadastroActivity {

	//Constants
	public static final String TAG = "FormVeiculoUsuarioActivityMobile";
	

	private final int FORM_ERROR_DIALOG_MISSING_VEICULO = 5;
	private final int FORM_ERROR_DIALOG_MISSING_USUARIO = 6;

	CustomSpinnerAdapter usuarioCustomSpinnerAdapter ;
	CustomSpinnerAdapter veiculoCustomSpinnerAdapter;	


	private DatabasePontoEletronico db=null;

	private Spinner veiculoSpinner;
	private Spinner usuarioSpinner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_veiculo_usuario_activity_mobile_layout);

		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {
		return true;
	}

	
	@Override
	protected void beforeInitializeComponents() {
		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		usuarioCustomSpinnerAdapter = new CustomSpinnerAdapter(getApplicationContext(), this.getAllUsuario(), R.layout.spinner_item,R.string.selecione);
		this.usuarioSpinner = (Spinner) findViewById(R.id.form_usuario_spinner);
		this.usuarioSpinner.setAdapter(usuarioCustomSpinnerAdapter);
		((CustomSpinnerAdapter) this.usuarioSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		veiculoCustomSpinnerAdapter = new CustomSpinnerAdapter(getApplicationContext(), this.getAllVeiculo(), R.layout.spinner_item,R.string.selecione);
		this.veiculoSpinner = (Spinner) findViewById(R.id.form_veiculo_spinner);
		this.veiculoSpinner.setAdapter(veiculoCustomSpinnerAdapter);
		((CustomSpinnerAdapter) this.veiculoSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

	}	


	protected LinkedHashMap<String, String> getAllVeiculo()
	{
		EXTDAOVeiculo vObj = new EXTDAOVeiculo(this.db);
		LinkedHashMap<String, String> vHash = new LinkedHashMap<String, String>();
		ArrayList<Table> vetor = vObj.getListTable();
		if(vetor == null) return null;
		for (Table vTable : vetor ) {
			String vId = vTable.getStrValueOfAttribute(EXTDAOVeiculo.ID);
			String vPlaca = vTable.getStrValueOfAttribute(EXTDAOVeiculo.PLACA);
			vHash.put(vId, vPlaca);
		}
		return vHash;
	}

	protected LinkedHashMap<String, String> getAllUsuario()
	{
		EXTDAOUsuario vObj = new EXTDAOUsuario(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}


	@Override
	protected Dialog onCreateDialog(int dialogType) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
		
		case FORM_ERROR_DIALOG_MISSING_VEICULO:
			vTextView.setText(getResources().getString(R.string.error_missing_veiculo_usuario_veiculo));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_VEICULO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_USUARIO:
			vTextView.setText(getResources().getString(R.string.error_missing_veiculo_usuario_usuario));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_USUARIO);
				}
			});
			break;
		default:
			return super.getDialog(dialogType);
		}

		return vDialog;
	}
	
	@Override
	protected void loadEdit(){
		try{
			EXTDAOVeiculoUsuario vObjVeiculoUsuario = new EXTDAOVeiculoUsuario(db);
			vObjVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.ID, id);
			vObjVeiculoUsuario.select();
			String vVeiculo = vObjVeiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.VEICULO_ID_INT);
			if(vVeiculo != null){
				int vPosition = veiculoCustomSpinnerAdapter.getPosition(vVeiculo);
				veiculoSpinner.setSelection(vPosition);
			}
	
			String vUsuario = vObjVeiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.USUARIO_ID_INT);
			if(vUsuario != null){
				int vPosition = usuarioCustomSpinnerAdapter.getPosition(vUsuario);
				usuarioSpinner.setSelection(vPosition);
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		
		}
	}


	@Override
	protected ContainerCadastro cadastrando() {
	

		Table vObj = new EXTDAOVeiculoUsuario(db);
		boolean vValidade = true;
		//First we check the obligatory fields:
		String selectedVeiculo = String.valueOf(veiculoSpinner.getSelectedItemId());
		if(!selectedVeiculo.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
		{
			vObj.setAttrValue(EXTDAOVeiculoUsuario.VEICULO_ID_INT, selectedVeiculo);
		} else {
			
			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_VEICULO, false);
		}

		String selectedUsuario = String.valueOf(usuarioSpinner.getSelectedItemId());
		if(!selectedUsuario.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
		{
			vObj.setAttrValue(EXTDAOVeiculoUsuario.USUARIO_ID_INT, selectedUsuario);
		} else{
			
			return new ContainerCadastro(FORM_ERROR_DIALOG_MISSING_USUARIO, false);
		}


		if(vValidade){
			if(id != null){
				vObj.setAttrValue(EXTDAOVeiculoUsuario.ID, id);
				
				if(vObj.update(true)){
					
					
					setBancoFoiModificado();
					
					return new ContainerCadastro(FORM_DIALOG_ATUALIZACAO_OK, true);
				} else{
					
					return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
				}
			}else {
				vObj.setAttrValue(EXTDAOVeiculoUsuario.IS_ATIVO_BOOLEAN, "0");
				vObj.formatToSQLite();
				if(vObj.insert(true) != null){
//							clearComponents();
					handlerClearComponents.sendEmptyMessage(0);
					
					return new ContainerCadastro(DIALOG_CADASTRO_OK, true);
				}else{
					
					return new ContainerCadastro(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
				}
			}

		}	
		else{
			
			return new ContainerCadastro(FORM_ERROR_DIALOG_CAMPO_INVALIDO, false);
		}
	
		
	}

	
	
}


