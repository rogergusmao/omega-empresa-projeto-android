package app.omegasoftware.pontoeletronico.TabletActivities.lists;

import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.BairroAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListBairroActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListBairroActivityMobile";
	int idLayout;
	String idPais;
	String idEstado;
	String idCidade;
	String nomeBairro;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		
	}
		
	@Override
	protected void loadCommomParameters(Bundle pParameters)
	{	
		nomeBairro = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_BAIRRO);
		idEstado = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO);
		idCidade = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE);
		idPais = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID_PAIS);
	}
	
	@Override
	protected void buildAdapter (boolean pIsAsc)
	{
		try{					
			adapter = new BairroAdapter(
					this,
					nomeBairro,
					idCidade,
					idEstado,
					idPais,
					pIsAsc);
		}
		catch(Exception e)
		{
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
	}

}


