package app.omegasoftware.pontoeletronico.TabletActivities.lists;


import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.CidadeAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListCidadeActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListCidadeActivityMobile";
	String idPais;
	String idEstado;
	String nomeCidade;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
	}
	
	protected void loadCommomParameters(Bundle pParameters)
	{	
		
		nomeCidade = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE);
		idEstado = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO);
		idPais = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID_PAIS);
		
	}
	
	@Override
	protected void buildAdapter (boolean pIsAsc)
	{
		try{					
			adapter = new CidadeAdapter(
					this,
					nomeCidade,
					idEstado,
					idPais,
					pIsAsc);
		}
		catch(Exception e)
		{
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
	}

}


