package app.omegasoftware.pontoeletronico.TabletActivities.lists;


import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.InformationDialog;
import app.omegasoftware.pontoeletronico.common.Adapter.CorporacaoAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListCorporacaoActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListCorporacaoActivityMobile";
	String nomeCorporacao;
	boolean isMinhaCorporacao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void loadCommomParameters(Bundle pParameters)
	{	
		nomeCorporacao = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_CORPORACAO);
		isMinhaCorporacao = pParameters.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MINHA_CORPORACAO, false);
	}
	
	 
	@Override
	protected void buildAdapter (boolean pIsAsc)
	{
		try{					
			adapter = new CorporacaoAdapter(
					this,
					nomeCorporacao,
					isMinhaCorporacao,
					pIsAsc);
		}
		catch(Exception e)
		{
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		try{
			switch (id) {
			
			case OmegaConfiguration.FORM_ERROR_DIALOG_INFORMACAO_DIALOG:
				InformationDialog vInformation = new InformationDialog(this, vDialog, TAG + "_dialog_informacao", OmegaConfiguration.FORM_ERROR_DIALOG_INFORMACAO_DIALOG);
				return vInformation.getDialogInformation(new int[]{R.string.informacao_uso_list_detalhes});
				
			default:
				//			vDialog = this.getErrorDialog(id);
				return super.onCreateDialog(id);
			}
		} catch(Exception ex){
			return null;
		}

	}
	
}


