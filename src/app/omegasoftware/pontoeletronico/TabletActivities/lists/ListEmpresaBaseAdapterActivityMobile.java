package app.omegasoftware.pontoeletronico.TabletActivities.lists;

import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.EmpresaAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListEmpresaBaseAdapterActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListEmpresaActivityMobile";
	String numero;
	String logradouro;
	String complemento;
	String cidadeId;
	String bairroId;
	String estadoId;
	String nomeEmpresa;
	String tipoEmpresa;
	String email;
	
	Boolean isEmpresaDaCorporacao;
	Boolean isParceiro;
	Boolean isFornecedor;
	Boolean isCliente;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void loadCommomParameters(Bundle pParameters)
	{	
		
		logradouro = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO);
		numero = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NUMERO);
		complemento = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO);
		bairroId = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO);	
		cidadeId = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE);
		estadoId = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO);
		tipoEmpresa = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID_TIPO_EMPRESA);
		nomeEmpresa = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_EMPRESA);
		email = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_EMAIL);
		
		isEmpresaDaCorporacao = pParameters.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO);
		isCliente = pParameters.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_CLIENTE);
		isFornecedor = pParameters.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_FORNECEDOR);
		isParceiro = pParameters.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_PARCEIRO);
		
		
	}
	@Override
	protected void buildAdapter (boolean pIsAsc)
	{
		try{					
			adapter = new EmpresaAdapter(
					this,
					isEmpresaDaCorporacao,
					isParceiro,
					isFornecedor,
					isCliente,
					nomeEmpresa,
					logradouro,
					numero,
					complemento,
					bairroId, 
					cidadeId, 
					estadoId,
					tipoEmpresa,
					email ,
					true);
		}
		catch(Exception e)
		{
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
	}

}


