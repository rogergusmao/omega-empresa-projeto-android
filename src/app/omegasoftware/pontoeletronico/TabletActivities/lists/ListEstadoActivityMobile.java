package app.omegasoftware.pontoeletronico.TabletActivities.lists;


import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.EstadoAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListEstadoActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListEstadoActivityMobile";
	String idPais;
	String nomeEstado;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void loadCommomParameters(Bundle pParameters)
	{	
		
		nomeEstado = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO);
		idPais = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID_PAIS);
	}
	
	
	@Override
	protected void buildAdapter (boolean pIsAsc)
	{
		try{					
			adapter = new EstadoAdapter(
					this,
					nomeEstado,
					idPais,
					pIsAsc);
		}
		catch(Exception e)
		{
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
	}


}


