package app.omegasoftware.pontoeletronico.TabletActivities.lists;


import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.ModeloAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListModeloActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListModeloActivityMobile";
	String nomeModeloId;
	String anoModeloId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void loadCommomParameters(Bundle pParameters)
	{		
		nomeModeloId = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_MODELO);
		anoModeloId = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ANO_MODELO);
		
	}
	
	@Override
	protected void buildAdapter(boolean pIsAsc)
	{
		try{
			adapter = new ModeloAdapter(
					this, 
					nomeModeloId, 
					anoModeloId,
					pIsAsc);
		}
		catch(Exception e)
		{
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
	}


}


