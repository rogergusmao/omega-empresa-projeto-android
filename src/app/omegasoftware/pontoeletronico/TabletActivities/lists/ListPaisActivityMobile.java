package app.omegasoftware.pontoeletronico.TabletActivities.lists;


import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.PaisAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListPaisActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListPaisActivityMobile";
	String nomePais;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void loadCommomParameters(Bundle pParameters)
	{	
		
		nomePais = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS);
	}
	
	
	@Override
	protected void buildAdapter (boolean pIsAsc)
	{
		try{					
			adapter = new PaisAdapter(
					this,
					nomePais,
					pIsAsc);
		}
		catch(Exception e)
		{
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
	}


}


