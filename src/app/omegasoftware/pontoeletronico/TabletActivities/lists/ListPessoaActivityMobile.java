package app.omegasoftware.pontoeletronico.TabletActivities.lists;

import android.os.Bundle;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.PessoaAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListPessoaActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG = "ListPessoaActivityMobile";

	String pessoaProfissaoId;
	String pessoaSexId;
	String numero;
	String logradouro;
	String complemento;
	String cidadeId;
	String bairroId;
	String estadoId;
	String email;
	String nomePessoa;
	String nomeEmpresa;
	String tipoEmpresa;
	Boolean isEmpresaDaCorporacao;
	Boolean isParceiro;
	Boolean isFornecedor;
	Boolean isCliente;
	Boolean isClienteDireto;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, R.string.pessoa);
		
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void loadCommomParameters(Bundle pParameters) {
		
		logradouro = pParameters
				.getString(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO);
		numero = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NUMERO);
		complemento = pParameters
				.getString(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO);
		bairroId = pParameters
				.getString(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO);
		cidadeId = pParameters
				.getString(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE);
		estadoId = pParameters
				.getString(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO);
		if (pParameters
				.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO))
			isEmpresaDaCorporacao = pParameters
					.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO);

		if (pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_CLIENTE))
			isCliente = pParameters
					.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_CLIENTE);

		if (pParameters
				.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_FORNECEDOR))
			isFornecedor = pParameters
					.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_FORNECEDOR);

		if (pParameters
				.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_PARCEIRO))
			isParceiro = pParameters
					.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_PARCEIRO);

		if (pParameters
				.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_CONSUMIDOR))
			isClienteDireto = pParameters
					.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_CONSUMIDOR);

		pessoaProfissaoId = pParameters
				.getString(OmegaConfiguration.SEARCH_FIELD_ID_PROFISSAO);
		tipoEmpresa = pParameters
				.getString(OmegaConfiguration.SEARCH_FIELD_ID_TIPO_EMPRESA);
		nomePessoa = pParameters
				.getString(OmegaConfiguration.SEARCH_FIELD_NOME_FUNCIONARIO);
		email = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_EMAIL);
		pessoaSexId = pParameters
				.getString(OmegaConfiguration.SEARCH_FIELD_SEXO_FUNCIONARIO);
		nomeEmpresa = pParameters
				.getString(OmegaConfiguration.SEARCH_FIELD_NOME_EMPRESA);

	}

	@Override
	protected void buildAdapter(boolean pIsAsc) {
		try {
			adapter = new PessoaAdapter(this, isEmpresaDaCorporacao,
					isParceiro, isCliente, isFornecedor, isClienteDireto,
					logradouro, numero, complemento, bairroId, cidadeId,
					estadoId, pessoaProfissaoId, pessoaSexId, nomePessoa,
					nomeEmpresa, tipoEmpresa, email, pIsAsc);

		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.LISTA);
		}
	}

}
