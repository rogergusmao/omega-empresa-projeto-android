package app.omegasoftware.pontoeletronico.TabletActivities.lists;


import android.os.Bundle;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.PessoaEmpresaAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListPessoaEmpresaActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListPessoaEmpresaActivityMobile";
	
	String empresaId;	
	String nomePessoa;
	boolean selecionarId;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		getIntent().putExtra(OmegaConfiguration.DESABILITA_DISCADOR, true);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, R.string.funcionarios_da_empresa);
		
		selecionarId = getIntent().getBooleanExtra(OmegaConfiguration.PARAM_SELECIONAR_ID, false);
		
		super.onCreate(savedInstanceState);
		
		
	}
	
	@Override
	protected void loadCommomParameters(Bundle pParameters)
	{	
		if(pParameters != null){
			if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA))
				empresaId = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA);
			if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_NOME_PESSOA))
				nomePessoa = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_PESSOA);	
		}
		
			
	}
	
	@Override
	protected void buildAdapter (boolean pIsAsc)
	{
		try{					
			adapter = new PessoaEmpresaAdapter(
					this,
					empresaId, 
					nomePessoa,
					pIsAsc,
					selecionarId);
			
		}
		catch(Exception e)
		{
			SingletonLog.insereErro(e, TIPO.LISTA);
		}
	}



}


