package app.omegasoftware.pontoeletronico.TabletActivities.lists;


import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.PessoaEmpresaRotinaAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaUsuario;

public class ListPessoaEmpresaRotinaActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListPessoaEmpresaRotinaActivityMobile";
	
	String empresaId;	
	String nomePessoa;
	boolean isSync = false;
	boolean isFinished = false;
	private ShowDialogHandler showDialogHandler = new ShowDialogHandler();
	
	final int ERROR_DIALOG_USUARIO_SEM_PESSOA_LINKADA = 939242074;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
	}


	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		try{
			switch (id) {
			case ERROR_DIALOG_USUARIO_SEM_PESSOA_LINKADA:
				
				vTextView.setText(getResources().getString(R.string.error_dialog_pessoa_usuario_rotina));
				vOkButton.setOnClickListener(new OnClickListener() {
					
					public void onClick(View arg0) {
						
						onBackPressed();
					}
				});
				return vDialog;
			
				
			default:
				//			vDialog = this.getErrorDialog(id);
				return super.onCreateDialog(id);
			}
		} catch(Exception ex){
			return null;
		}
		

	}
	
	@Override
	protected void loadCommomParameters(Bundle pParameters)
	{	
//		if(!OmegaConfiguration.IS_SINCRONIZACAO_POR_TIMER)
//			if(!isSync){
//				SynchronizeFastReceiveDatabase.procedureRun(this, EXTDAOPessoaEmpresaRotina.TABELAS_RELACIONADAS, false);
//				isSync = true;
//			}
		
		Database db = null;
		try{
			db = new DatabasePontoEletronico(this);
			
			EXTDAOPessoaUsuario vObj = new EXTDAOPessoaUsuario(db);
			
			String vIdPessoa = vObj.getIdPessoaDoUsuarioLogado();
			
			if(vIdPessoa ==null){
				isFinished = true;
				showDialogHandler.sendEmptyMessage(0);
			}
			
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		} finally{
			db.close();
		}
		
	}
	
	@Override
	protected void buildAdapter (boolean pIsAsc)
	{
		try{					
			if(!isFinished)
			adapter = new PessoaEmpresaRotinaAdapter(
					this,
					empresaId, 
					nomePessoa,
					pIsAsc);
			
		}
		catch(Exception e)
		{
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
	}



	public class ShowDialogHandler extends Handler{
		
		@Override
        public void handleMessage(Message msg)
        {	
			
			showDialog(ERROR_DIALOG_USUARIO_SEM_PESSOA_LINKADA);
        }
	}
	

}


