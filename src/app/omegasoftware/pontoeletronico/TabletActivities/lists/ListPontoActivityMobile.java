package app.omegasoftware.pontoeletronico.TabletActivities.lists;


import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.PontoAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListPontoActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListPontoActivityMobile";
	public String empresa;
	public String dataInicial;
	public String dataFinal;
	public String pessoa;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_IS_ORDENACAO_DATA, true);
		
		super.onCreate(savedInstanceState);
		
	}
	
	@Override
	protected void loadCommomParameters(Bundle pParameters)
	{
		
		empresa = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA);
		dataInicial = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_DATA_INICIAL);
		dataFinal = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_DATA_FINAL);
		pessoa = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA);	
	}
	
	@Override
	protected void buildAdapter (boolean pIsAsc)
	{
		try{					
			adapter = new PontoAdapter(
					this, 
					empresa,
					dataInicial,
					dataFinal,
					pessoa,
					true);
			
		}
		catch(Exception e)
		{
			SingletonLog.openDialogError(this, e, TIPO.LISTA);
		}
	}



}


