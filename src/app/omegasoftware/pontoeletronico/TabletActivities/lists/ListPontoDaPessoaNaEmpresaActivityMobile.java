package app.omegasoftware.pontoeletronico.TabletActivities.lists;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.BaterPontoDaPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.RelogioDePontoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.PontoDaPessoaNaEmpresaAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCustomAdapterListActivity;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class ListPontoDaPessoaNaEmpresaActivityMobile extends OmegaCustomAdapterListActivity {
	public static final String TAG = "ListPontoDaPessoaNaEmpresaActivityMobile";

	String idPessoa;
	String idEmpresa;
	String idPessoaEmpresa;
	String idProfissao;
	String empresa;
	String profissao;
	String pessoa;
	TextView tvNome;
	TextView tvEmpresa;
	TextView tvProfissao;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, R.string.pessoa);
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		idPessoa = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA);
		idEmpresa = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA);
		idProfissao = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_PROFISSAO);
		idPessoaEmpresa = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA_EMPRESA);
		
		empresa = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_EMPRESA);
		profissao = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_PROFISSAO);
		pessoa = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_PESSOA);
		
		setContentView(R.layout.list_pontos_da_pessoa_na_empresa_activity_mobile_layout);
		
		
		new CustomDataLoader(this).execute();
	}
	@Override
	protected void beforePostExecute(){
		tvNome.setText(HelperString.substring(pessoa, 25));
		tvEmpresa.setText(HelperString.substring( empresa, 25));
		tvProfissao.setText(HelperString.substring(profissao, 25));
	}
	@Override
	protected void loadCommomParameters(Bundle pParameters) {
		
		tvNome= (TextView)findViewById(R.id.tv_nome);

		tvEmpresa= (TextView)findViewById(R.id.tv_empresa);
		
		tvProfissao = (TextView)findViewById(R.id.tv_profissao);
		
		View viewBaterPonto = findViewById(R.id.ll_bater_ponto);
		viewBaterPonto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try{
					Intent intent = new Intent(getApplicationContext(), BaterPontoDaPessoaActivityMobile.class);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA_EMPRESA, idPessoaEmpresa);
					startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
				} catch(Exception ex){
					SingletonLog.insereErro(ex, TIPO.PAGINA);
				}
			}
		});
		
		
	}

	@Override
	protected CustomAdapter buildAdapter() {
		try {
			PontoDaPessoaNaEmpresaAdapter adapter = new PontoDaPessoaNaEmpresaAdapter(this, idPessoa, idEmpresa, idProfissao);
			
			return adapter;
		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.LISTA);
			return null;
		}
	}

}
