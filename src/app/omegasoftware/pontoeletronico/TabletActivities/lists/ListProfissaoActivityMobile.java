package app.omegasoftware.pontoeletronico.TabletActivities.lists;


import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.ProfissaoAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListProfissaoActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListProfissaoActivityMobile";
	String nomeProfissao;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void loadCommomParameters(Bundle pParameters)
	{
		nomeProfissao = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_PROFISSAO);
	}
	
	
	@Override
	protected void buildAdapter (boolean pIsAsc)
	{
		try{					
			adapter = new ProfissaoAdapter(
					this,
					nomeProfissao,
					pIsAsc);
		}
		catch(Exception e)
		{
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
	}


}


