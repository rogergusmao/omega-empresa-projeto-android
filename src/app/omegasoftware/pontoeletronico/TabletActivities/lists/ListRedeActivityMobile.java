package app.omegasoftware.pontoeletronico.TabletActivities.lists;


import android.os.Bundle;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.RedeAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListRedeActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListRedeActivityMobile";
	String nomeRede;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, R.string.redes);
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void loadCommomParameters(Bundle pParameters)
	{	
		if(pParameters != null){
			if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_NOME_REDE)){
				nomeRede = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_REDE);		
			}
		}
		
	}
	
	
	@Override
	protected void buildAdapter (boolean pIsAsc)
	{
		try{					
			adapter = new RedeAdapter(
					this,
					nomeRede,
					pIsAsc);
		}
		catch(Exception e)
		{
			SingletonLog.insereErro(e, TIPO.LISTA);
		}
	}


}


