package app.omegasoftware.pontoeletronico.TabletActivities.lists;

import android.os.Bundle;

import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.TarefaAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class ListTarefaActivityMobile extends OmegaDatabaseAdapterListActivity {

	public static String TAG = "ListTarefaActivityMobile";

	public String estadoTarefa;

	public String tipoTarefa;
	public String identificadorTipoTarefa;
	public String tipoOrigemEndereco;
	public String tipoDestinoEndereco;
	public String identificadorTipoOrigemEndereco;
	public String identificadorTipoDestinoEndereco;
	public String criadoPeloUsuario;

	private String origemLogradouro;
	private String origemNumero;
	private String origemComplemento;
	private String origemIdCidade;
	private String origemIdBairro;

	private String destinoLogradouro;
	private String destinoNumero;
	private String destinoComplemento;
	private String destinoIdCidade;
	private String destinoIdBairro;

	private String inicioDataProgramada;
	private String inicioHoraProgramada;

	private String inicioData;
	private String inicioHora;
	private String fimData;
	private String fimHora;

	private String dataCadastro;
	private String horaCadastro;
	private String dataExibir;

	private String titulo;
	private String descricao;

	EXTDAOTarefa.TIPO_TAREFA typeTarefa;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, R.string.tarefa);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_INATIVA_FILTRO_TELEFONE, true);
		super.onCreate(savedInstanceState);

	}

	@Override
	protected void loadCommomParameters(Bundle pParameters)
	{
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_CRIADO_PELO_USUARIO))
			criadoPeloUsuario= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_CRIADO_PELO_USUARIO);

		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ESTADO_TAREFA))
			estadoTarefa= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ESTADO_TAREFA);


		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_ENDERECO_ORIGEM))
			identificadorTipoOrigemEndereco= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_ENDERECO_ORIGEM);

		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ENDERECO_ORIGEM))
			tipoOrigemEndereco= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ENDERECO_ORIGEM);

		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_ENDERECO_DESTINO))
			identificadorTipoDestinoEndereco= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_ENDERECO_DESTINO);

		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ENDERECO_DESTINO))
			tipoDestinoEndereco= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ENDERECO_DESTINO);

		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_TIPO_TAREFA))
			tipoTarefa= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_TIPO_TAREFA);

		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_TAREFA))
			identificadorTipoTarefa= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_TAREFA);

		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ORIGEM_LOGRADOURO))
			origemLogradouro = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ORIGEM_LOGRADOURO);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ORIGEM_NUMERO))
			origemNumero = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ORIGEM_NUMERO);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ORIGEM_COMPLEMENTO))
			origemComplemento = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ORIGEM_COMPLEMENTO);

		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ORIGEM_ID_CIDADE))
			origemIdCidade = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ORIGEM_ID_CIDADE);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ORIGEM_ID_BAIRRO))
			origemIdBairro = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ORIGEM_ID_BAIRRO);

		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_DESTINO_LOGRADOURO))
			destinoLogradouro = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_DESTINO_LOGRADOURO);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_DESTINO_NUMERO))
			destinoNumero = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_DESTINO_NUMERO);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_DESTINO_COMPLEMENTO))
			destinoComplemento = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_DESTINO_COMPLEMENTO);

		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_DESTINO_ID_CIDADE))
			destinoIdCidade = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_DESTINO_ID_CIDADE);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_DESTINO_ID_BAIRRO))
			destinoIdBairro = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_DESTINO_ID_BAIRRO);

		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_INICIO_DATA_PROGRAMADA))
			inicioDataProgramada= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_INICIO_DATA_PROGRAMADA);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_INICIO_HORA_PROGRAMADA))
			inicioHoraProgramada= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_INICIO_HORA_PROGRAMADA);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_INICIO_DATA))
			inicioData= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_INICIO_DATA);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_INICIO_HORA))
			inicioHora= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_INICIO_HORA);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_FIM_DATA))
			fimData= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_FIM_DATA);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_FIM_HORA))
			fimHora= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_FIM_HORA);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_DATA_CADASTRO))
			dataCadastro= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_DATA_CADASTRO);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_HORA_CADASTRO))
			horaCadastro= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_HORA_CADASTRO);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_DATA_EXIBIR))
			dataExibir= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_DATA_EXIBIR);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_DESCRICAO))
			descricao= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_DESCRICAO);
		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_TIPO_TAREFA))
			typeTarefa= (EXTDAOTarefa.TIPO_TAREFA) pParameters.get(OmegaConfiguration.SEARCH_FIELD_TIPO_TAREFA);
	}


	@Override
	protected void buildAdapter (boolean pIsAsc)
	{
		try{

			adapter = new TarefaAdapter(
					this,
					EXTDAOTarefa.getTipoTarefaById(HelperInteger.parserInteger(tipoTarefa)),
					EXTDAOTarefa.getEstadoTarefaById(HelperInteger.parserInteger(estadoTarefa)),
					identificadorTipoTarefa,
					EXTDAOTarefa.getTipoEnderecoById(HelperInteger.parserInteger(tipoOrigemEndereco)),
					EXTDAOTarefa.getTipoEnderecoById(HelperInteger.parserInteger(tipoDestinoEndereco)),
					identificadorTipoOrigemEndereco,
					identificadorTipoDestinoEndereco,
					criadoPeloUsuario,
					titulo,
					origemLogradouro,
					origemNumero,
					origemComplemento,
					origemIdCidade,
					origemIdBairro,
					destinoLogradouro,
					destinoNumero,
					destinoComplemento,
					destinoIdCidade,
					destinoIdBairro,
					inicioDataProgramada,
					inicioHoraProgramada,
					inicioData,
					inicioHora,
					fimData,
					fimHora,
					dataCadastro,
					horaCadastro,
					dataExibir,
					descricao,
					pIsAsc );
		}
		catch(Exception e)
		{
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
			//SingletonLog.factoryToast(ListTarefaActivityMobile.this, e, SingletonLog.TIPO.LISTA);
			SingletonLog.insereErro(e, SingletonLog.TIPO.LISTA);
		}
	}

}
