package app.omegasoftware.pontoeletronico.TabletActivities.lists;

import android.os.Bundle;

//import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
//import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
//import app.omegasoftware.pontoeletronico.common.Adapter.BkpTarefaMinhaAdapter;
//import app.omegasoftware.pontoeletronico.common.activity.TarefaAdapterListActivity;
//import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;
//import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
//
//public class ListTarefaMinhaActivityMobile extends TarefaAdapterListActivity {
//
//	public static String TAG = "ListTarefaActivityMobile";
//
//	public String estadoTarefa;
//
//	public String tipoTarefa;
//	public String identificadorTipoTarefa;
//	public String tipoOrigemEndereco;
//	public String tipoDestinoEndereco;
//	public String identificadorTipoOrigemEndereco;
//	public String identificadorTipoDestinoEndereco;
//	public String criadoPeloUsuario;
//
//	private String inicioDataAPartir;
//	private String inicioDataAte;
//
//	private String titulo;
//
//
//	//TYPE_TAREFA typeTarefa;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
//		super.onCreate(savedInstanceState);
//	}
//
//	@Override
//	protected void loadCommomParameters(Bundle pParameters)
//	{
//		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_CRIADO_PELO_USUARIO))
//			criadoPeloUsuario= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_CRIADO_PELO_USUARIO);
//
//		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ESTADO_TAREFA))
//			estadoTarefa= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ESTADO_TAREFA);
//
//
//		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_ENDERECO_ORIGEM))
//			identificadorTipoOrigemEndereco= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_ENDERECO_ORIGEM);
//
//		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ENDERECO_ORIGEM))
//			tipoOrigemEndereco= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ENDERECO_ORIGEM);
//
//		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_ENDERECO_DESTINO))
//			identificadorTipoDestinoEndereco= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_ENDERECO_DESTINO);
//
//		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_ENDERECO_DESTINO))
//			tipoDestinoEndereco= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ENDERECO_DESTINO);
//
//		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_TIPO_TAREFA))
//			tipoTarefa= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_TIPO_TAREFA);
//
//		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_TAREFA))
//			identificadorTipoTarefa= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_IDENTIFICADOR_TIPO_TAREFA);
//
//		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_INICIO_DATA_PROGRAMADA))
//			inicioDataAPartir= pParameters.getString(OmegaConfiguration.SEARCH_FIELD_INICIO_DATA_PROGRAMADA);
//
//		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_FIM_DATA_PROGRAMADA))
//			inicioDataAte = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_FIM_DATA_PROGRAMADA);
//
//
////		if(pParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_TYPE_TAREFA))
////			typeTarefa= (TYPE_TAREFA) pParameters.get(OmegaConfiguration.SEARCH_FIELD_TYPE_TAREFA);
//	}
//
//
//	@Override
//	protected void buildAdapter (boolean pIsAsc)
//	{
//		try{
//			adapter = new BkpTarefaMinhaAdapter(
//					this,
//					EXTDAOTarefa.getTipoTarefaById(HelperInteger.parserInteger(tipoTarefa)),
//					EXTDAOTarefa.getEstadoTarefaById(HelperInteger.parserInteger(estadoTarefa)),
//					identificadorTipoTarefa,
//					EXTDAOTarefa.getTipoEnderecoById(HelperInteger.parserInteger(tipoOrigemEndereco)),
//					EXTDAOTarefa.getTipoEnderecoById(HelperInteger.parserInteger(tipoDestinoEndereco)),
//					identificadorTipoOrigemEndereco,
//					identificadorTipoDestinoEndereco,
//					criadoPeloUsuario,
//					titulo,
//					inicioDataAPartir,
//					inicioDataAte,
//					pIsAsc );
//		}
//		catch(Exception e)
//		{
//            SingletonLog.insereErro(e, SingletonLog.TIPO.LISTA);
//        }
//	}
//
//}
