package app.omegasoftware.pontoeletronico.TabletActivities.lists;


import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.TipoEmpresaAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListTipoEmpresaActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListTipoEmpresaActivityMobile";
	String nomeTipoEmpresa;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void loadCommomParameters(Bundle pParameters)
	{
		nomeTipoEmpresa = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_TIPO_EMPRESA);
	}
	
	
	@Override
	protected void buildAdapter (boolean pIsAsc)
	{
		try{					
			adapter = new TipoEmpresaAdapter(
					this,
					nomeTipoEmpresa,
					pIsAsc);
		}
		catch(Exception e)
		{
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
	}


}


