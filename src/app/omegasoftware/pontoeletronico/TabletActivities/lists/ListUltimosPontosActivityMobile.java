package app.omegasoftware.pontoeletronico.TabletActivities.lists;

import android.os.Bundle;
import android.view.View;

import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.UltimosPontosAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCustomAdapterListActivity;

public class ListUltimosPontosActivityMobile extends
		OmegaCustomAdapterListActivity {
	public static final String TAG = "ListUltimosPontosActivityMobile";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO,
				R.string.pessoa);
		getIntent().putExtra(
				OmegaConfiguration.SEARCH_FIELD_FILTRO_DISCADOR_ATIVO, false);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.list_activity_mobile_layout);
		findViewById(R.id.ll_cadastrar).setVisibility(
				View.GONE);
		findViewById(R.id.buscar_topo_button).setVisibility(
				View.GONE);
		new CustomDataLoader(this).execute();
	}

	@Override
	protected void loadCommomParameters(Bundle pParameters) {

	}

	@Override
	protected CustomAdapter buildAdapter() {
		try {
			UltimosPontosAdapter adapter = new UltimosPontosAdapter(this);

			return adapter;
		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.LISTA);
			return null;
		}
	}

}
