package app.omegasoftware.pontoeletronico.TabletActivities.lists;


import android.os.Bundle;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.UsuarioAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListUsuarioActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListUsuarioActivityMobile";
	short searchMode;
	
	String categoriaPermissaoId;
//	Boolean status = null;
	String nome;
	String email;
//	Boolean isAdm = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, R.string.usuario);
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void loadCommomParameters(Bundle pParameters)	
	{
		categoriaPermissaoId = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_CATEGORIA_PERMISSAO);
		nome = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_USUARIO);
		email = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_EMAIL);
//		status = pParameters.getBoolean(OmegaConfiguration.SEARCH_FIELD_STATUS);
//		isAdm = pParameters.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_ADM);
	}
	
	@Override
	protected void buildAdapter (boolean pIsAsc)
	{
		try{
			adapter = new UsuarioAdapter(
					this, 
					nome, 
					email, 
					categoriaPermissaoId,   
					null,
					null,
					pIsAsc);
		}
		catch(Exception e)
		{
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
	}
}


