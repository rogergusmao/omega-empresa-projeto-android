package app.omegasoftware.pontoeletronico.TabletActivities.lists;


import android.os.Bundle;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.VeiculoAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListVeiculoActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListVeiculoActivityMobile";
	String nomeModeloId;
	String anoModeloId;
	String placa;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, R.string.veiculos);
		
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void loadCommomParameters(Bundle pParameters)
	{		
		nomeModeloId = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_MODELO);
		anoModeloId = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ANO_MODELO);
		placa = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_PLACA);
		
	}
	
	@Override
	protected void buildAdapter(boolean pIsAsc)
	{
		try{
			adapter = new VeiculoAdapter(
					this, 
					placa, 
					nomeModeloId, 
					anoModeloId,
					pIsAsc);
		}
		catch(Exception e)
		{
			SingletonLog.insereErro(e, TIPO.LISTA);
		}
	}


}


