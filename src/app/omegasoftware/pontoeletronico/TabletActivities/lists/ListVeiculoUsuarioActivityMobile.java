package app.omegasoftware.pontoeletronico.TabletActivities.lists;


import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.VeiculoUsuarioAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;

public class ListVeiculoUsuarioActivityMobile extends OmegaDatabaseAdapterListActivity {
	public static final String TAG= "ListVeiculoUsuarioActivityMobile";

	String nomeModeloId;
	String anoModeloId;
	String placa;
	String emailUsuarioId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void buildAdapter (boolean pIsAsc)
	{
		try{
			adapter = new VeiculoUsuarioAdapter(
					this,
					this.placa,
					this.nomeModeloId,
					this.anoModeloId,
					this.emailUsuarioId,
					pIsAsc);
		}
		catch(Exception e)
		{
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
	}

	@Override
	protected void loadCommomParameters(Bundle pParameters)
	{		
		nomeModeloId = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_NOME_MODELO);
		anoModeloId = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_ANO_MODELO);
		placa = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_PLACA);
		emailUsuarioId = pParameters.getString(OmegaConfiguration.SEARCH_FIELD_EMAIL);
		
		
	}
	
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//
//		if (requestCode == OmegaConfiguration.STATE_RASTREAR_CARRO) {
//
//			if(intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO)){
//				long veiculoUsuarioId = intent.getLongExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, -1);
//				
//				Intent intent = getIntent();
//				if(veiculoUsuarioId >= 0){
//					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, veiculoUsuarioId);
//				}
//				setResult(OmegaConfiguration.STATE_RASTREAR_CARRO, intent);
//				
//				finish();
//			} else finish();
//
//		}
//	}

}


