package app.omegasoftware.pontoeletronico.TabletActivities.select;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;

public abstract class OmegaSelectActivity extends OmegaRegularActivity {

	// Constants
	public static final String TAG = "OmegaSelectActivityMobile";

	// Search button
	private Button searchButton;

	public Button btNovo;
	DatabasePontoEletronico db;

	protected final static int SEARCH_ERROR_DIALOG_MISSING_REGISTRO = 5;

	RefreshSpinner handlerRefreshSpinner;
	// Spinners
	protected Spinner selectSpinner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

	}

	@Override
	public boolean loadData() {

		return true;

	}

	public interface ContainerFormulario {
		public abstract int getIdStringMensagemNaoEncontrou();

		public abstract Class<?> getClassFormulario();
	}

	public ContainerFormulario containerFormulario = null;

	public void setContainerFormulario(ContainerFormulario containerFormulario) {
		this.containerFormulario = containerFormulario;
	}

	public abstract int getIdStringMensagem();

	public abstract int getIdStringPromptSpinner();

	@Override
	public void initializeComponents() throws OmegaDatabaseException {

		db = new DatabasePontoEletronico(this);
		handlerRefreshSpinner = new RefreshSpinner();
		// Attach search button to its listener
		this.searchButton = (Button) findViewById(R.id.buscar_button);
		this.searchButton.setOnClickListener(new SearchButtonListener(this));
		btNovo = (Button) findViewById(R.id.novo_button);
		if (containerFormulario != null) {
			btNovo.setVisibility(View.VISIBLE);
			btNovo.setText(getResources().getString(containerFormulario.getIdStringMensagemNaoEncontrou()));
			btNovo.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(OmegaSelectActivity.this, containerFormulario.getClassFormulario());

					OmegaSelectActivity.this.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);

				}
			});
		} else {
			btNovo.setVisibility(View.GONE);
		}

		selectSpinner = (Spinner) findViewById(R.id.select_spinner);

		((TextView) findViewById(R.id.descricao_textview)).setText(getResources().getString(getIdStringMensagem()));
		((Spinner) findViewById(R.id.select_spinner)).setPrompt(getResources().getString(getIdStringPromptSpinner()));

		onInitializeComponents();

	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;

		switch (id) {
		default:
			vDialog = this.getErrorDialog(id);
			break;

		}

		return vDialog;

	}

	private Dialog getErrorDialog(int dialogType) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {

		case SEARCH_ERROR_DIALOG_MISSING_REGISTRO:
			vTextView.setText(getResources().getString(R.string.error_missing_registro));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(SEARCH_ERROR_DIALOG_MISSING_REGISTRO);
				}
			});
			break;

		default:
			return super.onCreateDialog(dialogType);
		}

		return vDialog;
	}

	public abstract void onInitializeComponents();

	public class RefreshSpinner extends Handler {

		@Override
		public void handleMessage(Message msg) {

			onInitializeComponents();

		}
	}

	public abstract void onSelect(String id) throws OmegaDatabaseException;

	// ---------------------------------------------------------------
	// ----------------- Methods related to search action ------------
	// ---------------------------------------------------------------
	// Handles search button click
	private Integer onSelectButtonClicked() {
		try {
			String id = String.valueOf(this.selectSpinner.getSelectedItemId());
			if (id.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB)) {

				return SEARCH_ERROR_DIALOG_MISSING_REGISTRO;
			}
			onSelect(id);
			return null;
		} catch (OmegaDatabaseException oex) {
			SingletonLog.factoryToast(this, oex, SingletonLog.TIPO.PAGINA);
			return DIALOG_ERRO_FATAL;
		} catch (Exception ex) {
			SingletonLog.openDialogError(OmegaSelectActivity.this, ex, SingletonLog.TIPO.PAGINA);
			return DIALOG_ERRO_FATAL;
		}
	}
	// ---------------------------------------------------------------
	// ------------------ Methods related to planos ------------------
	// ---------------------------------------------------------------

	private class SearchButtonListener implements OnClickListener {
		Activity activity;

		public SearchButtonListener(Activity pActivity) {
			activity = pActivity;

		}

		public void onClick(View v) {
			new SelectLoader(activity).execute();
		}

	}

	public class SelectLoader extends AsyncTask<String, Integer, Boolean> {

		Integer dialogId = null;
		Activity activity;
		Exception e = null;

		public SelectLoader(Activity pActivity) {
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				dialogId = onSelectButtonClicked();
			} catch (Exception e) {
				this.e = e;
			} finally {
				db.close();
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				if (SingletonLog.openDialogError(activity, e, SingletonLog.TIPO.PAGINA)) {
					return;
				} 
				if (dialogId != null)
					showDialog(dialogId);
			} catch (Exception ex) {
				SingletonLog.openDialogError(OmegaSelectActivity.this, ex, SingletonLog.TIPO.PAGINA);
			} finally {
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}

}
