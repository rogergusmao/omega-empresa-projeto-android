package app.omegasoftware.pontoeletronico.TabletActivities.select;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormCategoriaPermissaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCategoriaPermissao;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class SelectCategoriaPermissaoActivityMobile extends OmegaSelectActivity {

	//Constants
	public static final String TAG = "SelectCategoriaPermissaoActivityMobile";
	private Button removeButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.select_categoria_permissao_activity_mobile_layout);
		new CustomDataLoader(this).execute();
	}


	@Override
	public int getIdStringMensagem() {
		
		return R.string.filter_categoria_permissao_prompt;
	}

	@Override
	public int getIdStringPromptSpinner() {
		
		
		return R.string.filter_categoria_permissao_prompt;
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		switch (requestCode) {
		case OmegaConfiguration.STATE_FORM_CONFIRMACAO:
			switch (resultCode) {
			case RESULT_OK:
				Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO , false);
				if(vConfirmacao){
					new RemoveLoader(this).execute();
				}
				break;

			default:
				break;
			}
			break;

		default:
			super.onActivityResult(requestCode, resultCode, intent);
			break;
		}

	}
	private class RemoveButtonListener implements OnClickListener
	{
		
		public RemoveButtonListener(){
			

		}
		public void onClick(View v) {
//			Intent intent = new Intent( activity, FormConfirmacaoActivityMobile.class);
//			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_MESSAGE, getResources().getString(R.string.form_confirmacao_remocao));
//			startActivityForResult(intent, OmegaConfiguration.STATE_FORM_CONFIRMACAO );

			DialogInterface.OnClickListener dialogRemoverClickListener = new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog, int which) {
			        switch (which){
			        case DialogInterface.BUTTON_POSITIVE:
			        	new RemoveLoader(SelectCategoriaPermissaoActivityMobile.this).execute();
			            break;

			        case DialogInterface.BUTTON_NEGATIVE:
			        	
			            break;
			        }
			    }
			};
			
			FactoryAlertDialog.showAlertDialog(
					SelectCategoriaPermissaoActivityMobile.this, 
							getString(R.string.form_confirmacao_remocao),
							dialogRemoverClickListener);
			
		}

	}	
	
	public class RemoveLoader extends AsyncTask<String, Integer, Boolean>
	{

		Integer dialogId = null;
		Activity activity;
		public RemoveLoader(Activity pActivity){
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				onRemoveButtonClicked();
			}
			catch(Exception e)
			{
				SingletonLog.insereErro(e, TIPO.PAGINA);
			}finally{
				db.close();
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
				if(dialogId != null)
					showDialog(dialogId);
			} catch (Exception ex) {
				SingletonLog.openDialogError(SelectCategoriaPermissaoActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}
	
	
	private void onRemoveButtonClicked(){
		String id = null;
		String selected = String.valueOf(this.selectSpinner.getSelectedItemId());
		if(! selected.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
		{
			id = selected;

		} else{
			showDialog(SEARCH_ERROR_DIALOG_MISSING_REGISTRO);
			return;
		}
		removendo(id);
		handlerRefreshSpinner.sendEmptyMessage(0);
		return;

	}
	
	
	public InterfaceMensagem removendo(String id) {
		try{
		EXTDAOCategoriaPermissao vObjCategoriaPermissao = new EXTDAOCategoriaPermissao(this.db);
		vObjCategoriaPermissao.setAttrValue(EXTDAOCategoriaPermissao.ID, id);
		vObjCategoriaPermissao.select();
		
		vObjCategoriaPermissao.remove(true);
		
		return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
		}catch(Exception ex){
			return new Mensagem(ex);
		}
	}

	@Override
	public void onInitializeComponents() {
		
		
		this.selectSpinner.setAdapter(
				new CustomSpinnerAdapter(
						getApplicationContext(),
						EXTDAOCategoriaPermissao.getAllCategoriaPermissao(db) ,
						R.layout.spinner_item,
						R.string.selecione));
		((CustomSpinnerAdapter) this.selectSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);		

		this.removeButton = (Button) findViewById(R.id.remove_button);
		this.removeButton.setOnClickListener(new RemoveButtonListener());

	}
	


	@Override
	public void onSelect(String id) {
		Bundle vParams = new Bundle();
		vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID, id);
		//TODO verificar se nao existe o veiculo em si ja ativo
		try{
			Intent vIntent ;	
			vIntent = new Intent(this, FormCategoriaPermissaoActivityMobile.class);
			vIntent.putExtras(vParams);
			startActivityForResult(vIntent, OmegaConfiguration.ACTIVITY_FORM_CATEGORIA_PERMISSAO);

		} catch(Exception ex){

		}
	}
}


