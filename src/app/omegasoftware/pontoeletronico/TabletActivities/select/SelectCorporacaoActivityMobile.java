package app.omegasoftware.pontoeletronico.TabletActivities.select;

import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.common.thread.HelperThread;
import app.omegasoftware.pontoeletronico.database.Tuple;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class SelectCorporacaoActivityMobile extends OmegaSelectActivity {

	// Constants
	public static final String TAG = "SelectCorporacaoActivityMobile";
	LinkedHashMap<String, String> hash = null;
	Handler handlerSelectCorporacao;
	ResetarLoader resetarLoader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, R.string.selecionar_grupo);
		// setContainerFormulario(new ContainerFormulario() {
		//
		// @Override
		// public int getIdStringMensagemNaoEncontrou() {
		//
		// return R.string.nao_encontrou_corporacao;
		// }
		//
		// @Override
		// public Class<?> getClassFormulario() {
		// return FormUsuarioECorporacaoActivityMobile.class;
		// }
		// });
		super.onCreate(savedInstanceState);

		setContentView(R.layout.select_corporacao_activity_mobile_layout);
		new CustomDataLoader(this).execute();

	}

	@Override
	public void onInitializeComponents() {
		//
		try {

			String json = getIntent().getStringExtra(OmegaConfiguration.SEARCH_FIELD_JSON);
			JSONObject jsonObject = new JSONObject(json);

			hash = new LinkedHashMap<String, String>();
			Tuple<Integer, String> registro = SincronizadorEspelho.getCorporacaoSincronizada(this);
			JSONArray corporacoes = jsonObject.getJSONArray("mObj");
			int position = -1;
			for (int i = 0; i < corporacoes.length(); i++) {
				JSONObject obj = corporacoes.getJSONObject(i);
				String id = obj.getString("id");
				String nome = obj.getString("nome");
				if (registro != null && registro.x.intValue() == HelperInteger.parserInt(id))
					position = i;
				hash.put(id, nome);
			}

			this.selectSpinner.setAdapter(
					new CustomSpinnerAdapter(getApplicationContext(), hash, R.layout.spinner_item, R.string.selecione));

			((CustomSpinnerAdapter) this.selectSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

			if (position >= 0)
				this.selectSpinner.setSelection(position + 1);

			findViewById(R.id.ll_menu_inferior).setVisibility(View.GONE);
			handlerSelectCorporacao = new Handler() {
				@Override
				public void handleMessage(Message msg) {

					super.handleMessage(msg);
					AlertDialog.Builder b = (AlertDialog.Builder) msg.obj;
					b.show();
				}

			};
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
		}

	}

	public void procedimentoResetar(boolean resetarAForca, String idCorporacaoEscolhida, String corporacao) {
		try {
			if (resetarLoader == null
					|| (resetarLoader != null && resetarLoader.getStatus() == AsyncTask.Status.FINISHED)) {
				resetarLoader = new ResetarLoader(resetarAForca, idCorporacaoEscolhida, corporacao);
				resetarLoader.execute();
			}

		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}

	}

	@Override
	public void onSelect(final String id) {
		Integer intId = HelperInteger.parserInt(id);
		final Tuple<Integer, String> registro = SincronizadorEspelho.getCorporacaoSincronizada(this);

		if (registro != null && registro.x.intValue() != intId.intValue()) {

			final String descCorp = getString(R.string.corporacao_carregada_atualmente).replace(":CORPORACAO",
					registro.y);

			final AlertDialog.Builder b = FactoryAlertDialog.factoryAlertDialog(this, descCorp,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							switch (which) {
							case DialogInterface.BUTTON_POSITIVE:
								procedimentoResetar(false, id, registro.y);
								break;

							case DialogInterface.BUTTON_NEGATIVE:

								break;
							}
						}
					});
			Message m = new Message();
			m.obj = b;
			handlerSelectCorporacao.sendMessage(m);

		} else {
			onSelectCorporacao(id);
		}

	}

	public void onSelectCorporacao(String id) {
		Intent intent = getIntent();
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_CORPORACAO, hash.get(id));
		setResult(RESULT_OK, intent);
		SelectCorporacaoActivityMobile.this.finish();
	}

	@Override
	public int getIdStringMensagem() {
		return R.string.select_corporacao_rotina_prompt;
	}

	@Override
	public int getIdStringPromptSpinner() {
		return R.string.select_corporacao_rotina_prompt;
	}

	public class ResetarLoader extends AsyncTask<Bundle, Integer, InterfaceMensagem> {
		boolean resetarAForca = false;
		String idCorporacaoEscolhida = null;
		String corporacao = null;

		public ResetarLoader(boolean resetarAForca, String idCorporacaoEscolhida, String corporacao) throws Exception {

			this.resetarAForca = resetarAForca;
			this.idCorporacaoEscolhida = idCorporacaoEscolhida;
			this.corporacao = corporacao;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected InterfaceMensagem doInBackground(Bundle... params) {

			return RotinaSincronizador.procedimentoResetar(SelectCorporacaoActivityMobile.this, resetarAForca);
		}

		@Override
		protected void onPostExecute(InterfaceMensagem msg) {
			super.onPostExecute(msg);
			try {

				if (msg == null)
					msg = new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
							SelectCorporacaoActivityMobile.this.getString(R.string.erro_fatal));
				if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO_A_AREA_LOGADA.getId()) {
					String strLoginResetar = SelectCorporacaoActivityMobile.this.getString(R.string.login_para_resetar);
					strLoginResetar = strLoginResetar.replace(":GRUPO", corporacao);
					FactoryAlertDialog.showAlertDialog(SelectCorporacaoActivityMobile.this, strLoginResetar,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									switch (which) {
									case DialogInterface.BUTTON_POSITIVE:
										procedimentoResetar(true, idCorporacaoEscolhida, corporacao);
										break;

									case DialogInterface.BUTTON_NEGATIVE:

										break;
									}
								}
							});
				} else if (msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()
						&& !resetarAForca) {
					FactoryAlertDialog.showAlertDialog(SelectCorporacaoActivityMobile.this,
							msg.mMensagem + ". " + SelectCorporacaoActivityMobile.this.getString(R.string.resetar_erro),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									switch (which) {
									case DialogInterface.BUTTON_POSITIVE:
										procedimentoResetar(false, idCorporacaoEscolhida, corporacao);
										break;

									case DialogInterface.BUTTON_NEGATIVE:

										break;
									}
								}
							});

				} else if (msg.ok()) {
					HelperThread.sleep(3000);
					// RotinaSincronizador.logout(SelectCorporacaoActivityMobile.this);
					onSelectCorporacao(idCorporacaoEscolhida);
				} else {
					SingletonLog.insereErro(new Exception("Situacao nao planejada"), TIPO.UTIL_ANDROID);
				}
			} catch (Exception ex) {
				SingletonLog.openDialogError(SelectCorporacaoActivityMobile.this, ex, SingletonLog.TIPO.PAGINA);
			} finally {
				controlerProgressDialog.dismissProgressDialog();
			}
		}

	}

}
