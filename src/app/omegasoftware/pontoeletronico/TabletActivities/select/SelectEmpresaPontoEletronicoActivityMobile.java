package app.omegasoftware.pontoeletronico.TabletActivities.select;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.content.Intent;
import android.os.Bundle;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.RelogioDePontoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormConfirmacaoCadastroFiltroActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.OmegaSelectActivity.ContainerFormulario;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.cam.CameraConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;

public class SelectEmpresaPontoEletronicoActivityMobile extends OmegaSelectActivity {

	// Constants
	public static final String TAG = "SelectEmpresaPontoEletronicoActivityMobile";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, R.string.painel_ponto_eletronico);
		setContainerFormulario(new ContainerFormulario() {

			@Override
			public int getIdStringMensagemNaoEncontrou() {

				return R.string.nao_encontrou_empresa;
			}

			@Override
			public Class<?> getClassFormulario() {
				return FormEmpresaActivityMobile.class;
			}
		});
		super.onCreate(savedInstanceState);

		setContentView(R.layout.select_activity_mobile_layout);
		new CustomDataLoader(this).execute();

	}

	@Override
	public void onInitializeComponents() {
		//

		LinkedHashMap<String, String> hashEmpresaSpinner = EXTDAOEmpresa.getAllEmpresa(db);

		this.selectSpinner.setAdapter(new CustomSpinnerAdapter(getApplicationContext(), hashEmpresaSpinner,
				R.layout.spinner_item, R.string.selecione));
		((CustomSpinnerAdapter) this.selectSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		if (hashEmpresaSpinner == null || hashEmpresaSpinner.size() == 0) {
			checkFormEmpresa(db);
		}

	}

	@Override
	public void onSelect(String id) {
		try {
			Database db = new DatabasePontoEletronico(SelectEmpresaPontoEletronicoActivityMobile.this);
			EXTDAOPessoa vObjPessoa = new EXTDAOPessoa(db);
			ArrayList<Table> vListPessoaEmpresa = vObjPessoa.getListTable();
	
			if (vListPessoaEmpresa == null || vListPessoaEmpresa.size() == 0) {
				checkFormPessoa(db);
				return;
			}

			Intent intent = getIntent();
			
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, id);
			setResult(RESULT_OK, intent);
			finish();

		} catch (Exception ex) {
			SingletonLog.openDialogError(this, ex, TIPO.PAGINA);
			
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		try{
			switch (requestCode) {
			case OmegaConfiguration.STATE_FORM_CONFIRMACAO_CADASTRO_OU_FILTRO_EMPRESA:
				if (resultCode == RESULT_OK) {
					Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO, false);
					if (vConfirmacao) {
						handlerRefreshSpinner.sendEmptyMessage(0);
					}
				}
				break;

			default:
				super.onActivityResult(requestCode, resultCode, intent);
				break;
			}	
		}catch(Exception ex){
			SingletonLog.openDialogError(this, ex, TIPO.PAGINA);
		}	
	}

	private void checkFormEmpresa(Database db) {
		Bundle vBundle = FormConfirmacaoCadastroFiltroActivityMobile.getBundleParameterToCallActivity(
				getResources().getString(R.string.error_empty_empresa_ponto_eletronico), FormEmpresaActivityMobile.TAG,
				FormEmpresaActivityMobile.class, null, null, EXTDAOEmpresa.NAME, false);

		Intent vIntent = new Intent(this, FormConfirmacaoCadastroFiltroActivityMobile.class);

		vIntent.putExtras(vBundle);

		startActivityForResult(vIntent, OmegaConfiguration.ACTIVITY_FORM_CONFIRMACAO_CADASTRO_FILTRO);
	}

	private void checkFormPessoa(Database db) {
		Bundle vBundle = FormConfirmacaoCadastroFiltroActivityMobile.getBundleParameterToCallActivity(
				getResources().getString(R.string.error_empty_pessoa_ponto_eletronico), FormPessoaActivityMobile.TAG,
				FormPessoaActivityMobile.class, FilterPessoaActivityMobile.TAG, FilterPessoaActivityMobile.class,
				EXTDAOPessoa.NAME, false);

		Intent vIntent = new Intent(this, FormConfirmacaoCadastroFiltroActivityMobile.class);

		vIntent.putExtras(vBundle);

		startActivityForResult(vIntent, OmegaConfiguration.ACTIVITY_FORM_CONFIRMACAO_CADASTRO_FILTRO);
	}

	@Override
	public int getIdStringMensagem() {
		return R.string.select_empresa_ponto_eletronico_prompt;
	}

	@Override
	public int getIdStringPromptSpinner() {
		return R.string.select_empresa_ponto_eletronico_prompt;
	}

}
