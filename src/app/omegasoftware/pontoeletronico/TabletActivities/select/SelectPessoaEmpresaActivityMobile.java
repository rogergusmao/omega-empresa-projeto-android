package app.omegasoftware.pontoeletronico.TabletActivities.select;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaPerfil;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPerfil;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;

public class SelectPessoaEmpresaActivityMobile extends OmegaRegularActivity {

	//Constants
	private static final String TAG = "SelectPessoaEmpresaActivityMobile";

	
	private final int FORM_ERROR_DIALOG_CADASTRO_OK = 3;
	private final int FORM_ERROR_DIALOG_CAMPO_INVALIDO = 4;
	private final int FORM_ERROR_DIALOG_MISSING_EMPRESA = 5;
	private final int FORM_ERROR_DIALOG_MISSING_PROFISSAO = 6;

	//Search button
	private Button cadastrarButton;
	private Button cancelarButton;
	Drawable originalDrawable;
	String id= null;
	//Spinners
	private DatabasePontoEletronico db=null;
	Boolean isEmpresaDaCorporacao = null;
	
	
	//EditText
	private AutoCompleteTextView profissaoAutoCompleteEditText;
	TableAutoCompleteTextView profissaoContainerAutoComplete = null;
	
	private Spinner empresaSpinner;
	CustomSpinnerAdapter empresaCustomSpinnerAdapter;
	EXTDAOPessoaEmpresa objPessoaEmpresa;

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.select_pessoa_empresa_activity_mobile_layout);
		setTituloCabecalho(getString(R.string.profissao_da_pessoa));
	
		Bundle vParameter = getIntent().getExtras();
		if(vParameter != null){
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO)){
				isEmpresaDaCorporacao = vParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO, false);
			}
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID)){
				id = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID);
			}
		}
		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean loadData() {
		try{
		this.db = new DatabasePontoEletronico(this);
		objPessoaEmpresa = new EXTDAOPessoaEmpresa(db);
		if(id != null){
			objPessoaEmpresa.select(id);
		}
		return true;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
return false;
		}
	}



	@Override
	public void initializeComponents() {

			try{	
		EXTDAOProfissao vObjProfissao = new EXTDAOProfissao(this.db);
		this.profissaoAutoCompleteEditText = (AutoCompleteTextView)
				findViewById(R.id.profissao_autocompletetextview);
		this.profissaoContainerAutoComplete = new TableAutoCompleteTextView(this, vObjProfissao, profissaoAutoCompleteEditText);
		new MaskUpperCaseTextWatcher(profissaoAutoCompleteEditText, 100);
		
		this.empresaSpinner = (Spinner) findViewById(R.id.form_empresa_spinner);
		empresaCustomSpinnerAdapter =new CustomSpinnerAdapter(getApplicationContext(), 
				this.getAllEmpresa(), 
				R.layout.spinner_item,
				R.string.form_empresa_prompt);
		
		this.empresaSpinner.setAdapter(empresaCustomSpinnerAdapter);
		
		((CustomSpinnerAdapter) this.empresaSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
		findViewById(R.id.form_pessoa_spinner).setVisibility(View.GONE);
		//Attach search button to its listener
		this.cadastrarButton = (Button) findViewById(R.id.cadastrar_dependente_button);
		this.cadastrarButton.setOnClickListener(new CadastroButtonListener(this));

		//Attach search button to its listener
		this.cancelarButton = (Button) findViewById(R.id.cancelar_button);
		this.cancelarButton.setOnClickListener(new CancelarButtonListener(this));
		if(id != null){
			String profissao = objPessoaEmpresa.getValorDoAtributoDaChaveExtrangeira(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT, EXTDAOProfissao.NOME);
			profissaoAutoCompleteEditText.setText(profissao);
			profissaoAutoCompleteEditText.clearFocus();
			String idEmpresa = objPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.EMPRESA_ID_INT);
			int posicaoEmpresa = empresaCustomSpinnerAdapter.getPositionFromId(idEmpresa);
			empresaSpinner.setSelection(posicaoEmpresa);
			this.cadastrarButton.setText(getResources().getString(R.string.atualizar));
			
		}
			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);

			}
	}

	private LinkedHashMap<String, String> getAllEmpresa()
	{
		 try{
		if(isEmpresaDaCorporacao != null){
			LinkedHashMap<String, String > vHash = new LinkedHashMap<String, String >();
			EXTDAOEmpresaPerfil  vObjEmpresaPerfil = new EXTDAOEmpresaPerfil(db);
			ArrayList<Table> vListTuplaEmpresaPerfil = null;
			if(isEmpresaDaCorporacao){
				vObjEmpresaPerfil.setAttrValue(EXTDAOEmpresaPerfil.PERFIL_ID_INT, EXTDAOPerfil.ID_STR_EMPRESA_CORPORACAO);
				vObjEmpresaPerfil.setAttrValue(EXTDAOEmpresaPerfil.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
				vListTuplaEmpresaPerfil = vObjEmpresaPerfil.getListTable();
				
			}else{
				vObjEmpresaPerfil.setAttrValue(EXTDAOEmpresaPerfil.PERFIL_ID_INT, EXTDAOPerfil.ID_STR_EMPRESA_CORPORACAO);
				vObjEmpresaPerfil.setAttrValue(EXTDAOEmpresaPerfil.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
				vListTuplaEmpresaPerfil = vObjEmpresaPerfil.getListTuplaEmpresaSemSerDaMinhaCorporacao();
				
			}
			if(vListTuplaEmpresaPerfil != null){
				for (Table vTuplaEP : vListTuplaEmpresaPerfil) {
					String vIdStrEmpresa = vTuplaEP.getStrValueOfAttribute(EXTDAOEmpresaPerfil.EMPRESA_ID_INT);
					String vNomeEmpresa = vTuplaEP.getNomeDaChaveExtrangeira(EXTDAOEmpresaPerfil.EMPRESA_ID_INT);
					
					vHash.put(vIdStrEmpresa, vNomeEmpresa);
				}
			}
			return vHash;
		} else{
			EXTDAOEmpresa vObj = new EXTDAOEmpresa(this.db);
			vObj.setAttrValue(EXTDAOEmpresa.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			return vObj.getHashMapIdByDefinition(true);
		}
			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
return null;
			}
	}

	protected LinkedHashMap<String, String> getAllProfissao()
	{
		EXTDAOProfissao vObj = new EXTDAOProfissao(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}

	
	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;

		switch (id) {
		default:
			vDialog = this.getErrorDialog(id);
			break;
		}
		return vDialog;
	}	

	private Dialog getErrorDialog(int dialogType)
	{

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
		case FORM_ERROR_DIALOG_CADASTRO_OK:
			vTextView.setText(getResources().getString(R.string.cadastro_ok));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_CADASTRO_OK);
				}
			});
			break;
		case FORM_ERROR_DIALOG_CAMPO_INVALIDO:
			vTextView.setText(getResources().getString(R.string.error_invalid_field));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_CAMPO_INVALIDO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_EMPRESA:
			vTextView.setText(getResources().getString(R.string.error_missing_empresa));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_EMPRESA);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_PROFISSAO:
			vTextView.setText(getResources().getString(R.string.error_missing_nome_profissao));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_PROFISSAO);
				}
			});
			break;
		default:
			return null;
		}

		return vDialog;
	}

	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onCadastroButtonClicked(Activity pActivity){
		
		boolean vValidade = true;
		
		//First we check the obligatory fields:
		//Plano, Especialidade, Cidade
		//Checking Cidade
		String selectedEmpresa = String.valueOf(this.empresaSpinner.getSelectedItemId());
		String nomeEmpresa = (String) this.empresaSpinner.getSelectedItem();
		if(selectedEmpresa.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
		{	
			showDialog(FORM_ERROR_DIALOG_MISSING_EMPRESA);
			return;
		} 

		
		String nomeProfissao = this.profissaoAutoCompleteEditText.getText().toString();
		
		if( nomeProfissao.length()  == 0)
		{
			showDialog(FORM_ERROR_DIALOG_MISSING_PROFISSAO);
			return;
		} 

		if(vValidade){
			Intent intent = getIntent();
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, selectedEmpresa);
//			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PROFISSAO, selectedProfissao);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, id);
			
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_EMPRESA, nomeEmpresa);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PROFISSAO, nomeProfissao );
			
			setResult(OmegaConfiguration.ACTIVITY_FORM_EMPRESA_FUNCIONARIO, intent);
			
			pActivity.finish( );
		}	
		else{
			showDialog(FORM_ERROR_DIALOG_CAMPO_INVALIDO);
		}
	}


	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------


	private class CancelarButtonListener implements OnClickListener
	{
		
		Activity activity;
		
		public CancelarButtonListener(Activity pActivity){
			activity = pActivity;
		}
		public void onClick(View v) {
			activity.finish();
			
		}
	}	
	
	private class CadastroButtonListener implements OnClickListener
	{
		Activity activity;
		public CadastroButtonListener(Activity pActivity){
			activity = pActivity;
		}
		
		public void onClick(View v) {
			onCadastroButtonClicked(activity);
			
		}

	}	
}


