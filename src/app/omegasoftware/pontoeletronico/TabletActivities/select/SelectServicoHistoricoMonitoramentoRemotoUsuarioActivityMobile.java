package app.omegasoftware.pontoeletronico.TabletActivities.select;

import java.sql.Time;
import java.util.Date;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import app.omegasoftware.pontoeletronico.R;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.TabletActivities.MonitoramentoRemotoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOModelo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculoUsuario;
import app.omegasoftware.pontoeletronico.date.ContainerClickListenerDate;
import app.omegasoftware.pontoeletronico.date.ContainerClickListenerHour;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.listener.FactoryDateClickListener;
import app.omegasoftware.pontoeletronico.listener.FactoryHourClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class SelectServicoHistoricoMonitoramentoRemotoUsuarioActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "SelectServicoHistoricoMonitoramentoRemotoUsuarioActivityMobile";

	//Search button
	private Button searchButton;

	private FactoryDateClickListener factoryDateClickListener;
	private FactoryHourClickListener factoryHourClickListener;

	private ContainerClickListenerDate inicioDataClickListener;
	private ContainerClickListenerDate fimDataClickListener;
	
	private ContainerClickListenerHour inicioHoraClickListener;
	private ContainerClickListenerHour fimHoraClickListener;
	
	String TABELAS_RELACIONADAS[] = new String[]{EXTDAOVeiculoUsuario.NAME, EXTDAOVeiculo.NAME, EXTDAOModelo.NAME};
	String lastDatetimeUpdate;
	final public static int FORM_ERROR_DIALOG_DESCONECTADO = 0;
	final public static int FORM_ERROR_DIALOG_USUARIO = 1;
	final public static int FORM_ERROR_DIALOG_DATA_INICIO = 2;
	final public static int FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_DATA_FINAL = 3;
	final public static int FORM_ERROR_DIALOG_ULTIMA_DATETIME = 4;
	
	
	final public static int FORM_ERROR_DIALOG_RESULTADO_NAO_ENCONTRADO = 5;
	final public static int FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_ULTIMA_DATA_ATUALIZACAO = 6;
	//Spinners
	private Spinner usuarioSpinner;

	private Button inicioDataButton;
	private Button inicioHoraButton;
	private Button fimDataButton;
	private Button fimHoraButton;

	private DatabasePontoEletronico db=null;


	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_servico_historico_monitoramento_usuario_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");

		new CustomDataLoader(this, TABELAS_RELACIONADAS, TYPE_SYNC.INITIAL).execute();

	}

	@Override
	public boolean loadData() {
		return true;
	}

	public void formatarStyle()
	{

		//Search mode buttons
		//Cidade em que deseja ser atendido:
		((TextView) findViewById(R.id.usuario_historico_monitoramento_usuario_textview)).setTypeface(this.customTypeFace);
		((TextView) findViewById(R.id.inicio_data_historico_monitoramento_usuario_textview)).setTypeface(this.customTypeFace);
		((TextView) findViewById(R.id.inicio_hora_historico_monitoramento_usuario_textview)).setTypeface(this.customTypeFace);
		((TextView) findViewById(R.id.fim_hora_historico_monitoramento_usuario_textview)).setTypeface(this.customTypeFace);
		((TextView) findViewById(R.id.fim_data_historico_monitoramento_usuario_textview)).setTypeface(this.customTypeFace);

		//Botuo buscar:
		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);

	}

	@Override
	public void initializeComponents() {
		
		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		

		factoryDateClickListener = new FactoryDateClickListener(this);
		factoryHourClickListener = new FactoryHourClickListener(this);
		this.usuarioSpinner = (Spinner) findViewById(R.id.usuario_historico_monitoramento_usuario_spinner);
		this.usuarioSpinner.setAdapter(
				new CustomSpinnerAdapter(getApplicationContext(), EXTDAOUsuario.getAllUsuario(this.db), R.layout.spinner_item,R.string.selecione));
		((CustomSpinnerAdapter) this.usuarioSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);

		this.inicioDataButton = (Button) findViewById(R.id.inicio_data_historico_monitoramento_usuario_button);
		inicioDataClickListener = factoryDateClickListener.setDateClickListener(inicioDataButton);
		 
		this.inicioHoraButton = (Button) findViewById(R.id.inicio_hora_historico_monitoramento_usuario_button);
		inicioHoraClickListener = factoryHourClickListener.setHourClickListener(inicioHoraButton);

		this.fimDataButton = (Button) findViewById(R.id.fim_data_historico_monitoramento_usuario_button);
		fimDataClickListener = factoryDateClickListener.setDateClickListener(fimDataButton);

		this.fimHoraButton = (Button) findViewById(R.id.fim_hora_historico_monitoramento_usuario_button);
		fimHoraClickListener = factoryHourClickListener.setHourClickListener(fimHoraButton);

		this.searchButton = (Button) findViewById(R.id.buscar_button);
		this.searchButton.setOnClickListener(new SearchButtonListener());
		//	Attach search button to its listener

		this.formatarStyle();		//Attach search button to its listener

	}
	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		switch (id) {

		case FactoryDateClickListener.DATE_DIALOG_ID:
			factoryDateClickListener.onPrepareDialog(id, dialog);
			break;
		case FactoryHourClickListener.TIME_DIALOG_ID:
			factoryHourClickListener.onPrepareDialog(id, dialog);
			break;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;

		switch (id) {

		case FactoryDateClickListener.DATE_DIALOG_ID:
			return factoryDateClickListener.onCreateDialog(id);
		case FactoryHourClickListener.TIME_DIALOG_ID:
			return factoryHourClickListener.onCreateDialog(id);

		default:
			vDialog = this.getErrorDialog(id);
			break;

		}

		return vDialog;

	}


	private Dialog getErrorDialog(int dialogType)
	{

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
		case FORM_ERROR_DIALOG_DESCONECTADO:
			vTextView.setText(getResources().getString(R.string.error_form_desconectado));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_DESCONECTADO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_RESULTADO_NAO_ENCONTRADO:
			vTextView.setText(getResources().getString(R.string.error_missing_resultado));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_RESULTADO_NAO_ENCONTRADO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_ULTIMA_DATA_ATUALIZACAO:
			if(lastDatetimeUpdate != null)
				vTextView.setText(getResources().getString(R.string.error_missing_data_inicio_maior_que_ultima_data_atualizacao));
			else
				vTextView.setText(getResources().getString(R.string.error_missing_data_inicio_maior_que_ultima_data_atualizacao) + lastDatetimeUpdate);
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_ULTIMA_DATA_ATUALIZACAO);
				}
			});
			break;
			
		case FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_DATA_FINAL:
			vTextView.setText(getResources().getString(R.string.error_data_inicio_maior_que_data_final));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_DATA_FINAL);
				}
			});
			break;
		case FORM_ERROR_DIALOG_DATA_INICIO:
			vTextView.setText(getResources().getString(R.string.error_missing_data_inicio));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_DATA_INICIO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_USUARIO:
			vTextView.setText(getResources().getString(R.string.error_missing_usuario));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_USUARIO);
				}
			});
			break;
		default:
			break;
		}

		return vDialog;
	}


	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onSearchButtonClicked()
	{

		
		String vInicioData = null;
		String vInicioHora = null;
		String vFimData = null;
		String vFimHora = null;
		String vUsuario = null;
		//First we check the obligatory fields:
		//Plano, Especialidade, Cidade
		
		if(FactoryDateClickListener.isDateButtonSet(this, inicioDataButton))
		{
			vInicioData = HelperDate.getDataFormatadaSQL(this.inicioDataButton.getText().toString());	
		} else {
			showDialog(FORM_ERROR_DIALOG_DATA_INICIO);
		}
		
		if(FactoryHourClickListener.isHourButtonSet(this, inicioHoraButton))
		{
			vInicioHora = HelperDate.getHoraFormatadaSQL( this.inicioHoraButton.getText().toString());
		} 

		if(FactoryDateClickListener.isDateButtonSet(this, fimDataButton))
		{
			vFimData = HelperDate.getDataFormatadaSQL(this.fimDataButton.getText().toString());
			
			Date vDateInicio = inicioDataClickListener.getDate();
			Date vDateFim = fimDataClickListener.getDate();
			int compareDate = vDateInicio.compareTo(vDateFim);
			if(compareDate > 0){
				showDialog(FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_DATA_FINAL);

				return;
			} else if(compareDate == 0){
				Time vTimeInicio = inicioHoraClickListener.getTime();
				Time vTimeFim = fimHoraClickListener.getTime();
				int vCompareTime = vTimeInicio.compareTo(vTimeFim);
				if(vCompareTime > 0 ){
					showDialog(FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_DATA_FINAL);
					return;					
				}
			}
		} 
		
		if(FactoryHourClickListener.isHourButtonSet(this, fimHoraButton))
		{
			vFimHora = HelperDate.getHoraFormatadaSQL(this.fimHoraButton.getText().toString());	
		}

		vUsuario = String.valueOf(this.usuarioSpinner.getSelectedItemId());
		if(vUsuario.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
		{
			showDialog(FORM_ERROR_DIALOG_USUARIO);
			return;
		}
		String vResult =null;
//		String vResult = HelperHttpPost.getConteudoStringPost(
//				this, 
//				OmegaConfiguration.URL_REQUEST_HISTORICO_MONITORAMENTO_USUARIO(), 
//				new String[]{"usuario", "id_corporacao", "corporacao", "data_inicio", "hora_inicio", "data_fim", "hora_fim"}, 
//				new String[]{vUsuario, OmegaSecurity.getIdCorporacao(),
//						OmegaSecurity.getCorporacao(), vInicioData, vInicioHora, vFimData, vFimHora});
		
		if(vResult == null)
			showDialog(FORM_ERROR_DIALOG_RESULTADO_NAO_ENCONTRADO);
		else if (vResult.length() == 0 )
			showDialog(FORM_ERROR_DIALOG_RESULTADO_NAO_ENCONTRADO);
		else if(vResult.contains("ERROR")){
			if(vResult.contains("#")){
				String vVetorDatetime[] = HelperString.splitWithNoneEmptyToken(vResult, "#");
				if(vVetorDatetime.length == 2){
					lastDatetimeUpdate = vVetorDatetime[1]; 
					showDialog(FORM_ERROR_DIALOG_DATA_INICIO_MAIOR_ULTIMA_DATA_ATUALIZACAO);
				} else showDialog(FORM_ERROR_DIALOG_RESULTADO_NAO_ENCONTRADO); 
			}
			else
			showDialog(FORM_ERROR_DIALOG_RESULTADO_NAO_ENCONTRADO);
		}else{
			
			String vListTupla[] = vResult.split( OmegaConfiguration.DELIMITER_QUERY_UNIQUE_LINHA);
			String vListId[] = new String[vListTupla.length];
			String vListDataHora[] = new String[vListTupla.length];
			int j = 0 ;
			for(int i = 0; i < vListTupla.length; i++) {
				String vTupla = vListTupla[i];
				String vListCampoTupla[] = vTupla.split( OmegaConfiguration.DELIMITER_QUERY_UNIQUE_COLUNA);
				if(vListCampoTupla.length == 2){
					String vId = vListCampoTupla[0];
					String vDataHora = vListCampoTupla[1];
					vListDataHora[j] = vDataHora;
					vListId[j] = vId;
					j += 1;
				}
			}
			Intent vIntent = new Intent(this, MonitoramentoRemotoUsuarioActivityMobile.class);	
			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_VETOR_FOTO_USUARIO_ID, vListId);
			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_VETOR_FOTO_USUARIO_DATA_HORA, vListDataHora);
			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_CAMERA_INTERNA, true);
			
			startActivity(vIntent);
		}
	}

	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------
	private class SearchButtonListener implements OnClickListener
	{

		public void onClick(View v) {
			onSearchButtonClicked();
		}

	}	


}


