package app.omegasoftware.pontoeletronico.TabletActivities.select;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import app.omegasoftware.pontoeletronico.R;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.TabletActivities.MonitoramentoRemotoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.http.HelperHttp;

public class SelectServicoMonitoramentoRemotoUsuarioActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "SelectServicoMonitoramentoRemotoUsuarioActivityMobile";
	
	//Search button
	private Button searchButton;
	
	DatabasePontoEletronico db=null;
	
	public final static int SEARCH_ERROR_DIALOG_SERVIDOR_OFFLINE  = 3;
	public final static int SEARCH_ERROR_DIALOG_USUARIO = 4;
	

	
	//Spinners
	private Spinner usuarioSpinner;

	String idUsuario;

	private Typeface customTypeFace;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_servico_monitoramento_remoto_usuario_activity_mobile_layout);
		try {
			db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
		
		new CustomDataLoader(this).execute();

	}

	@Override
	public boolean loadData() {

		return true;

	}

	public void formatarStyle()
	{
		
		//Search mode buttons
				//Cidade em que deseja ser atendido:
		((TextView) findViewById(R.id.filter_servico_online_grupo_usuario_textview)).setTypeface(this.customTypeFace);
				
		//Botuo buscar:
		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);
		
	}
	
	@Override
	public void finish(){
		try{
			db.close();
			super.finish();
		}catch(Exception ex){
			
		}
	}
	
	
	
	@Override
	public void initializeComponents() {
		
		DatabasePontoEletronico db=null;
		try {
			db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.formatarStyle();

		this.usuarioSpinner = (Spinner) findViewById(R.id.filter_servico_online_grupo_usuario_spinner);
		this.usuarioSpinner.setAdapter(new CustomSpinnerAdapter(getApplicationContext(), EXTDAOUsuario.getAllUsuario(db),R.layout.spinner_item,R.string.selecione));
		((CustomSpinnerAdapter) this.usuarioSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);		
		
		//Attach search button to its listener
		this.searchButton = (Button) findViewById(R.id.buscar_button);
		this.searchButton.setOnClickListener(new SearchButtonListener());
		
		db.close();
	}
		
	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;

		switch (id) {
		default:
			vDialog = this.getErrorDialog(id);
			break;

		}

		return vDialog;

	}

	private Dialog getErrorDialog(int dialogType)
	{
		
		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);
		
		switch (dialogType) {
		
		case SEARCH_ERROR_DIALOG_SERVIDOR_OFFLINE:
			vTextView.setText(getResources().getString(R.string.error_servidor_offline));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(SEARCH_ERROR_DIALOG_SERVIDOR_OFFLINE);
				}
			});
			break;
		case SEARCH_ERROR_DIALOG_USUARIO:
			vTextView.setText(getResources().getString(R.string.error_missing_usuario));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(SEARCH_ERROR_DIALOG_USUARIO);
				}
			});
			break;
		default:
			break;
		}
		
		return vDialog;
	}


	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onSearchButtonClicked()
	{
		
//		if(!HelperHttp.isServerOnline(this)){
//			showDialog(SEARCH_ERROR_DIALOG_SERVIDOR_OFFLINE);
//			return;
//		}
		 

		Bundle vParams = new Bundle();

		//First we check the obligatory fields:
		//Plano, Especialidade, Cidade
		
		//Checking Cidade
		String selectedUsuario = String.valueOf(this.usuarioSpinner.getSelectedItemId());
		if(! selectedUsuario.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
		{
			vParams.putString(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, selectedUsuario);
			idUsuario = selectedUsuario;
		} else{
			showDialog(SEARCH_ERROR_DIALOG_USUARIO);
			return;
		}
		
		//TODO verificar se nao existe o veiculo em si ja ativo
//		caso nao exista, o atributo IS_ATIVO da tupla referente deve ser setado
		
		Intent intent = null;
		
		intent = new Intent(getApplicationContext(), MonitoramentoRemotoUsuarioActivityMobile.class);
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, idUsuario);
		
		startActivity(intent);
		
	}


	//---------------------------------------------------------------
	//------------------ Methods related to planos ------------------
	//---------------------------------------------------------------

	private class SearchButtonListener implements OnClickListener
	{

		public void onClick(View v) {
			onSearchButtonClicked();
		}

	}	
}


