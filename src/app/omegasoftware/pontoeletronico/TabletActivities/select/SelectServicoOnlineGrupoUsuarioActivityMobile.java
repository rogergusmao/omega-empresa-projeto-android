package app.omegasoftware.pontoeletronico.TabletActivities.select;

import android.content.Intent;
import android.os.Bundle;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.MenuServicoOnlineGrupoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;

public class SelectServicoOnlineGrupoUsuarioActivityMobile extends OmegaSelectActivity {

	//Constants
	public static final String TAG = "SelectServicoOnlineGrupoUsuarioActivityMobile";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.select_activity_mobile_layout);
		new CustomDataLoader(this).execute();
	}


//	MenuServicoOnlineGrupoActivityMobile.class
	

	@Override
	public int getIdStringMensagem() {
		
		return R.string.filter_servico_online_grupo_usuario;
	}

	@Override
	public int getIdStringPromptSpinner() {
		
		
		return R.string.filter_servico_online_grupo_usuario_prompt;
	}


	@Override
	public void onInitializeComponents() {
		
		
		this.selectSpinner.setAdapter(
				new CustomSpinnerAdapter(
						getApplicationContext(),EXTDAOUsuario.getAllUsuario(db),
						R.layout.spinner_item,
						R.string.selecione));
		((CustomSpinnerAdapter) this.selectSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);		

	}
	


	@Override
	public void onSelect(String id) {
		Bundle vParams = new Bundle();
		vParams.putString(OmegaConfiguration.SEARCH_FIELD_ID, id);
		//TODO verificar se nao existe o veiculo em si ja ativo

		Intent vIntent ;	
		vIntent = new Intent(this, MenuServicoOnlineGrupoActivityMobile.class);
		vIntent.putExtras(vParams);
		startActivityForResult(vIntent, OmegaConfiguration.ACTIVITY_FORM_CATEGORIA_PERMISSAO);

	}
	
}


