package app.omegasoftware.pontoeletronico.TabletActivities.select;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.content.Intent;
import android.os.Bundle;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormConfirmacaoCadastroFiltroActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;

public class SelectUsuarioActivityMobile extends OmegaSelectActivity {

	//Constants
	public static final String TAG = "SelectUsuarioActivityMobile";
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, R.string.seleciona_rotina);
		setContainerFormulario(new ContainerFormulario() {
			
			@Override
			public int getIdStringMensagemNaoEncontrou() {
				
				return R.string.nao_encontrou_usuario;
			}
			
			@Override
			public Class<?> getClassFormulario() {
				return FormUsuarioActivityMobile.class;
			}
		});
		super.onCreate(savedInstanceState);		

		setContentView(R.layout.select_activity_mobile_layout);
		new CustomDataLoader(this).execute();

	}

	
	@Override
	public void onInitializeComponents() {
		// 
		
		LinkedHashMap<String, String> hashEmpresaSpinner = EXTDAOUsuario.getAllUsuario(db);

		this.selectSpinner.setAdapter(
				new CustomSpinnerAdapter(
						getApplicationContext(),
						hashEmpresaSpinner,
						R.layout.spinner_item,
						R.string.selecione));
		((CustomSpinnerAdapter) this.selectSpinner.getAdapter()).setDropDownLayout(
				R.layout.spinner_item_selected);	
		
	}

	@Override
	public void onSelect(String id) throws OmegaDatabaseException {
		Database db=null;
		
		db = new DatabasePontoEletronico(SelectUsuarioActivityMobile.this);
		
		EXTDAOPessoa vObjPessoa = new EXTDAOPessoa(db);
		ArrayList<Table> vListPessoaEmpresa = vObjPessoa.getListTable();

		if(vListPessoaEmpresa == null || vListPessoaEmpresa.size() == 0 ){
			checkFormPessoa(db);
			return ;
		}
		
		Intent intent = getIntent();
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_USUARIO, id);
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, true);
		
		setResult(RESULT_OK, intent);
		SelectUsuarioActivityMobile.this.finish();
		
//		try{
//			Intent vIntent ;	
//			vIntent = new Intent(this, MenuPontoEletronicoSemFotoActivityMobile.class);
//			
//			vIntent.putExtras(vParams);
//			startActivityForResult(vIntent, OmegaConfiguration.ACTIVITY_FORM_CATEGORIA_PERMISSAO);
//
//		} catch(Exception ex){
//			SingletonLog.insereErro(ex, TIPO.PAGINA);
//		}
	}


	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		switch (requestCode) {
		case OmegaConfiguration.STATE_FORM_CONFIRMACAO_CADASTRO_OU_FILTRO_EMPRESA:
			if (resultCode == RESULT_OK) {
				Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO , false);
				if(vConfirmacao){
					handlerRefreshSpinner.sendEmptyMessage(0);
				}
			}
			break;


		default:
			break;
		}
	}


	private void checkFormPessoa(Database db){
		Bundle vBundle = FormConfirmacaoCadastroFiltroActivityMobile.getBundleParameterToCallActivity(
				getResources().getString(R.string.error_empty_pessoa_ponto_eletronico), 
				FormPessoaActivityMobile.TAG, 
				FormPessoaActivityMobile.class, 
				FilterPessoaActivityMobile.TAG, 
				FilterPessoaActivityMobile.class, 
				EXTDAOPessoa.NAME, 
				false);

		Intent vIntent = new Intent(this, FormConfirmacaoCadastroFiltroActivityMobile.class);

		vIntent.putExtras(vBundle);

		startActivityForResult(vIntent, OmegaConfiguration.ACTIVITY_FORM_CONFIRMACAO_CADASTRO_FILTRO);
	}


	@Override
	public int getIdStringMensagem() {
		return R.string.select_usuario_rotina_prompt;
	}


	@Override
	public int getIdStringPromptSpinner() {
		return R.string.select_usuario_spinner_prompt;
	}

}


