package app.omegasoftware.pontoeletronico.TabletActivities.select;

//public class SelectUsuarioPosicaoActivityMobile extends OmegaRegularActivity {
//
//	//Constants
//	public static final String TAG = "SelectUsuarioPosicaoActivityMobile";
//
//	//Search button
//	private Button searchButton;
//
//	private FactoryDateClickListener factoryDateClickListener;
//	private FactoryHourClickListener factoryHourClickListener;
//	
//	protected final int FORM_ERROR_DIALOG_DESCONECTADO = 75;	
//	
//	//Spinners
//	private Spinner veiculoSpinner;
//	private Spinner usuarioSpinner;
//	private EditText velocidadeMinimaEditText;
//	private CheckBox somenteComFotoCheckBox;
//	private Button inicioDataButton;
//	private Button inicioHoraButton;
//	private Button fimDataButton;
//	private Button fimHoraButton;
//
//	private DatabasePontoEletronico db=null;
//	EXTDAOUsuarioPosicaoRastreamento objUsuarioPosicaoRastreamento;
//
//	private Typeface customTypeFace;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.filter_usuario_posicao_activity_mobile_layout);
//		try {
//			db = new DatabasePontoEletronico(this);
//		} catch (OmegaDatabaseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		objUsuarioPosicaoRastreamento = new EXTDAOUsuarioPosicaoRastreamento(db);
//		try{
//			objUsuarioPosicaoRastreamento.removeAllTuplas(false);	
//		} catch(Exception ex){
//			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
//		}
//		
//		db.close();
//		
//		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");
//
//		new CustomDataLoader(this).execute();
//
//	}
//
//	@Override
//	public boolean loadData() {
//
//		return true;
//
//	}
//
//	public void formatarStyle()
//	{
//
//		//Search mode buttons
//		((TextView) findViewById(R.id.veiculo_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.usuario_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.inicio_data_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.inicio_hora_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.fim_data_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.fim_hora_textview)).setTypeface(this.customTypeFace);
//		((TextView) findViewById(R.id.velocidade_textview)).setTypeface(this.customTypeFace);
//		((CheckBox) findViewById(R.id.somente_com_foto_checkbox)).setTypeface(this.customTypeFace);
//		
//		//Botuo buscar:
//		((Button) findViewById(R.id.buscar_button)).setTypeface(this.customTypeFace);
//
//	}
//
//	@Override
//	public void initializeComponents() {
//
//		try {
//			this.db = new DatabasePontoEletronico(this);
//		} catch (OmegaDatabaseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}		
//
//		factoryDateClickListener = new FactoryDateClickListener(this);
//		factoryHourClickListener = new FactoryHourClickListener(this);
//		
//		this.usuarioSpinner = (Spinner) findViewById(R.id.usuario_spinner);
//		showAllUsuario();
//		
////		this.usuarioSpinner.setOnItemSelectedListener(new UsuarioSpinnerListener());
//
//		this.veiculoSpinner = (Spinner) findViewById(R.id.veiculo_spinner);
//		showAllVeiculo();
//		this.veiculoSpinner.setOnItemSelectedListener(new VeiculoSpinnerListener());
//
//		this.inicioDataButton = (Button) findViewById(R.id.inicio_data_button);
//		factoryDateClickListener.setDateClickListener(inicioDataButton);
//
//		this.inicioHoraButton = (Button) findViewById(R.id.inicio_hora_button);
//		factoryHourClickListener.setHourClickListener(inicioHoraButton);
//
//		this.fimDataButton = (Button) findViewById(R.id.fim_data_button);
//		factoryDateClickListener.setDateClickListener(fimDataButton);
//
//		somenteComFotoCheckBox = (CheckBox) findViewById(R.id.somente_com_foto_checkbox); 
//		somenteComFotoCheckBox.setChecked(false);
//		this.fimHoraButton = (Button) findViewById(R.id.fim_hora_button);
//		factoryHourClickListener.setHourClickListener(fimHoraButton);
//
//		velocidadeMinimaEditText = (EditText) findViewById(R.id.velocidade_edittext);
//		new MaskNumberTextWatcher(this.velocidadeMinimaEditText,-1);
//		this.searchButton = (Button) findViewById(R.id.buscar_button);
//		this.searchButton.setOnClickListener(new SearchButtonListener());
//		//	Attach search button to its listener
//
//		this.formatarStyle();		//Attach search button to its listener
//		db.close();
//	}
//
//	@Override
//	public void finish(){
//		try{
//			this.db.close();
//			super.finish();
//		}catch(Exception ex){
//
//		}
//	}
//
//	private class VeiculoSpinnerListener implements OnItemSelectedListener
//	{
//
//		public void onItemSelected(AdapterView<?> parent, View view, int position,long id) {
//			try{
//			String vId = String.valueOf(id);
//			if(vId.compareTo(OmegaConfiguration.UNEXISTENT_ID_IN_DB) == 0)
//				showAllUsuario();
//			else
//				showAllUsuarioDoVeiculo(vId);
//			}catch(Exception ex){
//				SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//			
//			}
//		}
//
//		public void onNothingSelected(AdapterView<?> parent) {
//			//Do nothing
//		}
//
//	}
//
//	public class UsuarioSpinnerListener implements OnItemSelectedListener
//	{
//
//		public void onItemSelected(AdapterView<?> parent, View view, int position,long id) {
//			String vId = String.valueOf(id);
//			if(vId.compareTo(OmegaConfiguration.UNEXISTENT_ID_IN_DB) == 0)
//				showAllVeiculo();
//			else
//				showAllVeiculoDoUsuario(vId);
//		}
//
//		public void onNothingSelected(AdapterView<?> parent) {
//			//Do nothing
//		}
//
//	}
//	private void showAllUsuario(){
//		try{
//	this.usuarioSpinner.setAdapter(
//			new CustomSpinnerAdapter(
//					getApplicationContext(), 
//					EXTDAOVeiculoUsuario.getAllUsuario(this.db), 
//					R.layout.spinner_item,R.string.selecione));
//	((CustomSpinnerAdapter) this.usuarioSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
//	}catch(Exception ex){
//		SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//	
//	}
//	}
//	
//	private void showAllVeiculo(){
//		try{
//		this.veiculoSpinner.setAdapter(
//				new CustomSpinnerAdapter(
//						getApplicationContext(), 
//						EXTDAOVeiculoUsuario.getAllVeiculo(this.db), 
//						R.layout.spinner_item,R.string.selecione
//						));
//		((CustomSpinnerAdapter) this.veiculoSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//		
//		}
//	}
//	
//	private void showAllVeiculoDoUsuario(String pUsuarioId)
//	{
//
//try{
//		LinkedHashMap<String, String > vHash = EXTDAOVeiculoUsuario.getAllVeiculoDoUsuario(this.db, pUsuarioId);
//		
//
//		this.veiculoSpinner.setAdapter(
//				new CustomSpinnerAdapter(
//						getApplicationContext(), 
//						vHash, 
//						R.layout.spinner_item,
//						R.string.form_all_string));
//		((CustomSpinnerAdapter) this.veiculoSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
//}catch(Exception ex){
//	SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//
//}
//		
//
//	}
//
//	private void showAllUsuarioDoVeiculo(String pIdVeiculo)
//	{
//try{
//
//		LinkedHashMap<String, String > vHash = EXTDAOVeiculoUsuario.getAllUsuarioDoVeiculo(this.db, pIdVeiculo);
//		
//
//		this.usuarioSpinner.setAdapter(
//				new CustomSpinnerAdapter(
//						getApplicationContext(), 
//						vHash, 
//						R.layout.spinner_item,
//						R.string.form_all_string));
//		((CustomSpinnerAdapter) this.usuarioSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
//}catch(Exception ex){
//	SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//
//}
//		
//
//	}
//
//	@Override
//	protected void onPrepareDialog(int id, Dialog dialog) {
//		switch (id) {
//
//		case FactoryDateClickListener.DATE_DIALOG_ID:
//			factoryDateClickListener.onPrepareDialog(id, dialog);
//			break;
//		case FactoryHourClickListener.TIME_DIALOG_ID:
//			factoryHourClickListener.onPrepareDialog(id, dialog);
//			break;
//		}
//	}
//
//	@Override
//	protected Dialog onCreateDialog(int id) {
//
//		Dialog vDialog = null;
//
//		switch (id) {
//		
//		case FactoryDateClickListener.DATE_DIALOG_ID:
//			return factoryDateClickListener.onCreateDialog(id);
//		case FactoryHourClickListener.TIME_DIALOG_ID:
//			return factoryHourClickListener.onCreateDialog(id);
//
//		default:
//			vDialog = this.getErrorDialog(id);
//			break;
//
//		}
//
//		return vDialog;
//
//	}
//
//	private final int FORM_ERROR_DIALOG_USUARIO_E_OU_VEICULO = 777;
//	
//	private final int FORM_ERROR_DIALOG_SEM_RESULTADO = 779;
//	
//	private Dialog getErrorDialog(int dialogType)
//	{
//
//		Dialog vDialog = new Dialog(this);
//		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		
//		vDialog.setContentView(R.layout.dialog);
//
//		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
//		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);
//
//		switch (dialogType) {
//		case FORM_ERROR_DIALOG_DESCONECTADO:
//			vTextView.setText(getResources().getString(R.string.error_form_desconectado));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_DESCONECTADO);
//				}
//			});
//			break;
//		case FORM_ERROR_DIALOG_SEM_RESULTADO:
//			vTextView.setText(getResources().getString(R.string.error_no_result));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_SEM_RESULTADO);
//				}
//			});
//			break;
//		case FORM_ERROR_DIALOG_USUARIO_E_OU_VEICULO:
//			vTextView.setText(getResources().getString(R.string.error_missing_usuario_e_ou_veiculo));
//			vOkButton.setOnClickListener(new OnClickListener() {
//				public void onClick(View v) {
//					dismissDialog(FORM_ERROR_DIALOG_USUARIO_E_OU_VEICULO);
//				}
//			});
//			break;
//		default:
//			break;
//		}
//
//		return vDialog;
//	}
//	
//	//---------------------------------------------------------------
//	//----------------- Methods related to search action ------------
//	//---------------------------------------------------------------
//	//Handles search button click
//	private void onSearchButtonClicked()
//	{
//
//		Intent vIntent = new Intent(this, RastrearVeiculoUsuarioPosicaoMapActivity.class);
//		
//
//		//First we check the obligatory fields:
//		//Plano, Especialidade, Cidade
//
//		String vFimData = null;
//		String vFimHora = null;
//		String vInicioData = null;
//		String vInicioHora = null;
//		String vIdUsuario = null;
//		String vIdVeiculo = null;
//		boolean vSomenteComFoto = false;
//		if(somenteComFotoCheckBox.isChecked()) vSomenteComFoto = true;
//		
//		if(FactoryDateClickListener.isDateButtonSet(this, inicioDataButton))
//		{
//			vInicioData = HelperDate.getDataFormatadaSQL(this.inicioDataButton.getText().toString());	
//		}
//		
//		if(FactoryHourClickListener.isHourButtonSet(this, inicioHoraButton))
//		{
//			vInicioHora = HelperDate.getHoraFormatadaSQL( this.inicioHoraButton.getText().toString());
//		} 
//		
//		if(FactoryDateClickListener.isDateButtonSet(this, fimDataButton))
//		{
//			vFimData = HelperDate.getDataFormatadaSQL(this.fimDataButton.getText().toString());	
//		}
//		
//		if(FactoryHourClickListener.isHourButtonSet(this, fimHoraButton))
//		{
//			vFimHora = HelperDate.getHoraFormatadaSQL( this.fimHoraButton.getText().toString());
//		} 
//		
//		String vVelocidadeMinima = "";
//		if(velocidadeMinimaEditText.getText().toString().length() > 0){
//			vVelocidadeMinima = velocidadeMinimaEditText.getText().toString();
//		}
//		
//		boolean vValidadeUsuarioVeiculo = false;
//		String selectedVeiculo = String.valueOf(this.veiculoSpinner.getSelectedItemId());
//		if(!selectedVeiculo.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
//		{
//			vIdVeiculo = selectedVeiculo;
//			vValidadeUsuarioVeiculo = true;
//			
//		}
//
//		String selectedUsuario = String.valueOf(this.usuarioSpinner.getSelectedItemId());
//		if(!selectedUsuario.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
//		{
//			vIdUsuario = selectedUsuario;
//			vValidadeUsuarioVeiculo = true;
//			
//		}
//		
//		if(somenteComFotoCheckBox.isChecked()) 
//			vSomenteComFoto = true;
//		
//		if(!vValidadeUsuarioVeiculo){
//			showDialog(FORM_ERROR_DIALOG_USUARIO_E_OU_VEICULO);
//			return;
//		}
//		
//		Integer vTotalTuplasBaixadas ;
//		try {
//			vTotalTuplasBaixadas = ContainerRastreamento.rastrearRotaUsuario(
//					this, 
//					objUsuarioPosicaoRastreamento,
//					0,
//					vIdUsuario,
//					vIdVeiculo,
//					vInicioData,
//					vInicioHora,
//					vFimData,
//					vFimHora,
//					vVelocidadeMinima,
//					vSomenteComFoto);
//			if(vTotalTuplasBaixadas != null){
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_USUARIO, vIdUsuario);
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO, vIdVeiculo);
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_INICIO_DATA, vInicioData);
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_INICIO_HORA, vInicioHora);
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_FIM_DATA, vFimData);
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_FIM_HORA, vFimHora);
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_VELOCIDADE_MINIMA, vVelocidadeMinima);
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_SOMENTE_COM_FOTO, vSomenteComFoto);
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_TOTAL_TUPLAS_BAIXADAS, vTotalTuplasBaixadas);
//				
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_LAYOUT, R.layout.rastrear_veiculo_usuario_map_layout_mobile);
//				startActivity(vIntent);
//			} else showDialog(FORM_ERROR_DIALOG_SEM_RESULTADO);
//		} catch (Exception e) {
//			
//			showDialog(FORM_ERROR_DIALOG_DESCONECTADO);
//		}
//		
//		db.close();
//	}
//	
//	//---------------------------------------------------------------
//	//------------------ Methods related to planos ------------------
//	//---------------------------------------------------------------
//	private class SearchButtonListener implements OnClickListener
//	{
//
//		public void onClick(View v) {
//			onSearchButtonClicked();
//		}
//
//	}	
//
//
//}
//
//
