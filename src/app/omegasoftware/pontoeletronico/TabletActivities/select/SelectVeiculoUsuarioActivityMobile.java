package app.omegasoftware.pontoeletronico.TabletActivities.select;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomSpinnerAdapter;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculoUsuario;
import app.omegasoftware.pontoeletronico.http.HelperHttp;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;

//DEPRECATED
public class SelectVeiculoUsuarioActivityMobile extends OmegaSelectActivity {

	//Constants
	public static final String TAG = "SelectServicoVeiculoUsuarioActivityMobile";

	//Search button
	

	public final static int SEARCH_ERROR_DIALOG_VEICULO_EM_USO  = 1;
	public final static int SEARCH_ERROR_DIALOG_FUNCIONARIO_ID  = 2;
	public final static int SEARCH_ERROR_DIALOG_SERVIDOR_OFFLINE  = 3;
	public final static int SEARCH_ERROR_DIALOG_FUNCIONARIO_VEICULO  = 4;
	
		
	String idVeiculo;
	String idVeiculoUsuario;

	

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.select_activity_mobile_layout);
		new CustomDataLoader(this).execute();
		
	}

	@Override
	public boolean loadData() {

		return true;

	}

	private LinkedHashMap<String, String> getAllFuncionarioVeiculo()
	{
try{
		EXTDAOVeiculoUsuario vObj = new EXTDAOVeiculoUsuario(db);

		String vId = OmegaSecurity.getIdUsuario();

		vObj.setAttrValue(EXTDAOVeiculoUsuario.USUARIO_ID_INT, vId) ;
		ArrayList<Table> vListEXTDAOFuncionarioVeiculo = vObj.getListTable();
		EXTDAOVeiculo vObjVeiculo = new EXTDAOVeiculo(db);
		LinkedHashMap<String, String> vHashIdVeiculoFuncionarioPorPlacaVeiculo = new LinkedHashMap<String, String>();
		for (Table table : vListEXTDAOFuncionarioVeiculo) {
			String vIdFuncionarioVeiculo = table.getStrValueOfAttribute(EXTDAOVeiculoUsuario.ID);
			String vIdVeiculo = table.getStrValueOfAttribute(EXTDAOVeiculoUsuario.VEICULO_ID_INT);
			vObjVeiculo.clearData();
			vObjVeiculo.setAttrValue(EXTDAOVeiculo.ID, vIdVeiculo);
			vObjVeiculo.select();
			String vStrPlaca = vObjVeiculo.getStrValueOfAttribute(EXTDAOVeiculo.PLACA);
			vHashIdVeiculoFuncionarioPorPlacaVeiculo.put(vIdFuncionarioVeiculo, vStrPlaca );
		}
		return vHashIdVeiculoFuncionarioPorPlacaVeiculo;
}catch(Exception ex){
	SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
return null;
}
	}


	@Override
	public void onInitializeComponents() {
		
		
		this.selectSpinner.setAdapter(
				new CustomSpinnerAdapter(
						getApplicationContext(),
						getAllFuncionarioVeiculo(),
						R.layout.spinner_item,
						R.string.selecione));
		((CustomSpinnerAdapter) this.selectSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);		

	}
	

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (id) {
		case SEARCH_ERROR_DIALOG_VEICULO_EM_USO:
			vTextView.setText(getResources().getString(R.string.error_veiculo_em_uso));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(SEARCH_ERROR_DIALOG_VEICULO_EM_USO);
				}
			});
			break;
	
		case SEARCH_ERROR_DIALOG_FUNCIONARIO_ID:
			vTextView.setText(getResources().getString(R.string.error_id_funcionario_invalido));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(SEARCH_ERROR_DIALOG_FUNCIONARIO_ID);
				}
			});
			break;
		case SEARCH_ERROR_DIALOG_SERVIDOR_OFFLINE:
			vTextView.setText(getResources().getString(R.string.error_servidor_offline));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(SEARCH_ERROR_DIALOG_SERVIDOR_OFFLINE);
				}
			});
			break;
		case SEARCH_ERROR_DIALOG_FUNCIONARIO_VEICULO:
			vTextView.setText(getResources().getString(R.string.error_missing_funcionario_veiculo));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(SEARCH_ERROR_DIALOG_FUNCIONARIO_VEICULO);
				}
			});
			break;
		default:
			return super.onCreateDialog(id);
		}

		return vDialog;

	}


	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	@Override
	public void onSelect(String id)
	{
		try{
		Bundle vParams = new Bundle();

	
		//TODO verificar se nao existe o veiculo em si ja ativo
		//		caso nao exista, o atributo IS_ATIVO da tupla referente deve ser setado

		//		Tenta atualizar o estatus ativo do veiculo pelo funcionario em questao
		String vIdUsuario = OmegaSecurity.getIdUsuario();
		if(vIdUsuario == null ){
			showDialog(SEARCH_ERROR_DIALOG_FUNCIONARIO_ID);
			return  ;
		} else if(vIdUsuario.length() == 0 ){
			showDialog(SEARCH_ERROR_DIALOG_FUNCIONARIO_ID);
			return ;
		}
		EXTDAOVeiculoUsuario vEXTDAOVeiculoUsuario = new EXTDAOVeiculoUsuario(db);

		vEXTDAOVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.ID, id);
		vEXTDAOVeiculoUsuario.select();

		idVeiculo = vEXTDAOVeiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.VEICULO_ID_INT);

		try{
			String vResultPost = null;
//				String vResultPost = HelperHttpPost.getConteudoStringPost(
//						this, 
//						OmegaConfiguration.URL_REQUEST_IS_VEICULO_DISPONIVEL(), 
//						new String[]{
//								"usuario", 
//								"id_corporacao",
//								"corporacao", 
//								"senha", 
//								"veiculo", 
//								"veiculo_usuario"
//						}, 
//						new String[]{
//								OmegaSecurity.getIdUsuario(),
//								OmegaSecurity.getIdCorporacao(),
//								OmegaSecurity.getCorporacao(),
//								OmegaSecurity.getSenha(),
//								idVeiculo, 
//								id});

				if(vResultPost == null){
					exibeDialogUtilizacaoVeiculoOffline();
				}
				else if(vResultPost.compareTo("TRUE") == 0 ){
					vEXTDAOVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.IS_ATIVO_BOOLEAN, "1");
					vEXTDAOVeiculoUsuario.update(false);
					Intent intent = getIntent();
					intent.putExtras(vParams);
					setResult(OmegaConfiguration.ACTIVITY_FILTER_VEICULO_USUARIO, intent);
					finish();
					
				}else if(vResultPost.contains("ocupado")){

					exibeDialogUtilizacaoVeiculoOffline();

					
				} else {
					
					
				}
			
		} catch(Exception ex){
			showDialog(SEARCH_ERROR_DIALOG_SERVIDOR_OFFLINE);
			
		}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);

		}
	}
	
	private void exibeDialogUtilizacaoVeiculoOffline(){
		DialogInterface.OnClickListener dialogConfirmaUtilizacaoDoVeiculo = new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int which) {
		        switch (which){
		        case DialogInterface.BUTTON_POSITIVE:
		        	disponibilizaVeiculoAForca();
		            break;

		        case DialogInterface.BUTTON_NEGATIVE:
		            break;
		        }
		    }
		};
		
		FactoryAlertDialog.showAlertDialog(
						this, 
						getString(R.string.mensagem_confirmacao_utilizacao_veiculo),
						dialogConfirmaUtilizacaoDoVeiculo);
		
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		
		if(requestCode == OmegaConfiguration.ACTIVITY_MAPA_MULTIPLE_MARKER){
			try{

//				HelperHttpPost.getConteudoStringPost(
//						this, 
//						OmegaConfiguration.URL_REQUEST_DISPONIBILIZA_VEICULO(), 
//						new String[]{ "usuario", "id_corporacao", "corporacao", "senha", "veiculo", "veiculo_usuario"}, 
//						new String[]{OmegaSecurity.getIdUsuario(), OmegaSecurity.getIdCorporacao(),
//								OmegaSecurity.getCorporacao(), OmegaSecurity.getSenha(), idVeiculo, idVeiculoUsuario});


			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
			}

		} else 	if (requestCode == OmegaConfiguration.STATE_FORM_CONFIRMACAO) {
			if (resultCode == RESULT_OK) {
				
				disponibilizaVeiculoAForca();				
			}
		}
		
	}

	private void disponibilizaVeiculoAForca(){
		try{
		EXTDAOVeiculoUsuario vEXTDAOVeiculoUsuario = new EXTDAOVeiculoUsuario(db);
		//Checking Cidade
		String selectedVeiculoUsuario = String.valueOf(this.selectSpinner.getSelectedItemId());
		if(! selectedVeiculoUsuario.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
		{	
			idVeiculoUsuario = selectedVeiculoUsuario;
		}
//		if(HelperHttp.isServerOnline(this)){
//			String vResultPost = HelperHttpPost.getConteudoStringPost(
//					this, 
//					OmegaConfiguration.URL_REQUEST_DISPONIBILIZA_VEICULO_A_FORCA(), 
//					new String[]{ "usuario", "corporacao", "senha", "veiculo"}, 
//					new String[]{OmegaSecurity.getIdUsuario(), OmegaSecurity.getIdCorporacao(), OmegaSecurity.getSenha(), idVeiculo});
		String vResultPost = null;
			if(vResultPost == null){
				setVeiculoUsuarioOcupadoInDatabase(selectedVeiculoUsuario);
			}
			else{

				vEXTDAOVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.ID, selectedVeiculoUsuario);
				vEXTDAOVeiculoUsuario.select();
				vEXTDAOVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.IS_ATIVO_BOOLEAN, "1");
				vEXTDAOVeiculoUsuario.update(true);
				finish();
			}
//		} else{
//			setVeiculoUsuarioOcupadoInDatabase(selectedVeiculoUsuario);
//		}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);

		}
	}
	
	private void setVeiculoUsuarioOcupadoInDatabase(String pIdVeiculoUsuario){
try{
		EXTDAOVeiculoUsuario vEXTDAOVeiculoUsuario = new EXTDAOVeiculoUsuario(db);
		vEXTDAOVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.ID, pIdVeiculoUsuario);
		vEXTDAOVeiculoUsuario.select();
		vEXTDAOVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.IS_ATIVO_BOOLEAN, "1");
		vEXTDAOVeiculoUsuario.update(false);
}catch(Exception ex){
	SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);

}
	}


	@Override
	public int getIdStringMensagem() {
		
		return R.string.filter_servico_online_grupo_usuario;
	}

	@Override
	public int getIdStringPromptSpinner() {
		
		
		return R.string.filter_servico_online_grupo_usuario_prompt;
	}



}


