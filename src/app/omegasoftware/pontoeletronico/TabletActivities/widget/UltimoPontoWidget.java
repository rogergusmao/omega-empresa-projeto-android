package app.omegasoftware.pontoeletronico.TabletActivities.widget;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.view.View;
import android.widget.RemoteViews;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.ConfirmarPontoESalvarNoBancoTimerActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.container.EntradaSaida;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCorporacao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador.STATE_SINCRONIZADOR;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoPonto;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.pontoeletronico.PrincipalActivity;

public class UltimoPontoWidget extends AppWidgetProvider {
	
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		
		context.startService(new Intent(context, UpdateService.class));
//		final int N = appWidgetIds.length;
//		// Perform this loop procedure for each App Widget that belongs to this
//		// provider
//		for (int i = 0; i < N; i++) {
//			int appWidgetId = appWidgetIds[i];
//
//			// Create an Intent to launch ExampleActivity
//			// Intent intent = new Intent(context, ExampleActivity.class);
//			// PendingIntent pendingIntent = PendingIntent.getActivity(context,
//			// 0, intent, 0);
//
//			// Get the layout for the App Widget and attach an on-click listener
//			// to the button
//
//			// views.setOnClickPendingIntent(R.id.button, pendingIntent);
//
//			// Tell the AppWidgetManager to perform an update on the current app
//			// widget
//			RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.ultimo_ponto_widget_layout);
//			if (views != null) {
//
//				updateInformacaoPonto(views, context);
//				appWidgetManager.updateAppWidget(appWidgetId, views);
//			}
//
//		}
	}

	public static class UpdateService extends Service {
		@Override
		public void onStart(Intent intent, int startId) {
			
			// Build the widget update for today
			

			// Build the widget update for today
			RemoteViews updateViews = updateInformacaoPonto(UpdateService.this.getBaseContext());
			

			// Push update for this widget to the home screen
			ComponentName thisWidget = new ComponentName(this, UltimoPontoWidget.class);
			AppWidgetManager manager = AppWidgetManager.getInstance(this);
			manager.updateAppWidget(thisWidget, updateViews);
			
		}

		@Override
		public IBinder onBind(Intent intent) {
			return null;
		}
		

		public RemoteViews updateInformacaoPonto(Context c) {
			RemoteViews views = new RemoteViews(c.getPackageName(), R.layout.ultimo_ponto_widget_layout);
			String email = OmegaSecurity.getSavedEmail(c);
			String corporacao = OmegaSecurity.getSavedCorporacao(c);
			Database db = null;
			boolean inicializado = false;
			try {
				
				if(email != null && email.length() > 0 
						&& corporacao != null && corporacao.length()> 0 ){

					db = new DatabasePontoEletronico(c);

					Long[] idUsuarioEPessoa = EXTDAOUsuario.getIdUsuarioeIdPessoaECorporacaoDoEmail(db, email, corporacao);
					
					
					if (idUsuarioEPessoa != null
						&& idUsuarioEPessoa[2] != null
						&& RotinaSincronizador.getStateSincronizador(c, idUsuarioEPessoa[2].toString()) ==STATE_SINCRONIZADOR.COMPLETO 
						) {
						
						
						EXTDAOPonto objPonto = new EXTDAOPonto(db);
						String idUsuario= idUsuarioEPessoa[0].toString();
						String idPessoa = idUsuarioEPessoa[1].toString();
						EntradaSaida es = objPonto.getUltimaEntradaSaidaDaPessoa(idPessoa, EXTDAOTipoPonto.TIPO_PONTO.TURNO);
						Long idCorporacao =  EXTDAOCorporacao.getIdCorporacao(db, corporacao);
						if(es == null && idCorporacao != null){
							es = EXTDAOPessoaEmpresa.getPessoaEmpresaParaBaterPontoDaPessoaWidget(
									db, idPessoa, idCorporacao.toString(), idUsuario);
						}
						if(es != null){
							views.setTextViewText(R.id.tv_corporacao, corporacao);
							views.setTextViewText(R.id.tv_nome, es.pessoa);
							views.setTextViewText(R.id.tv_empresa, es.empresa);
							if (es!= null && es.entrada != null)
								views.setTextViewText(R.id.tv_data_ponto_turno, es.entrada.data + " " + es.entrada.hora);
							else
								views.setTextViewText(R.id.tv_data_ponto_turno, "");
							
							if (es!= null && es.saida != null){
								if(es.entrada != null && es.entrada.data.compareTo(es.saida.data) == 0)
									views.setTextViewText(R.id.tv_data_ponto_turno_saida,  es.saida.hora);
								else 
								views.setTextViewText(R.id.tv_data_ponto_turno_saida, es.saida.data + " " + es.saida.hora);
							} else 
								views.setTextViewText(R.id.tv_data_ponto_turno_saida, "");

							Intent intent = getIntent(c, true, es, idUsuarioEPessoa[2].toString());
							
							PendingIntent pendingIntent = PendingIntent.getActivity(c, OmegaConfiguration.STATE_BATIDA_PONTO_ENTRADA_WIDGET, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
							views.setOnClickPendingIntent(R.id.bt_entrada, pendingIntent);

							Intent intent2 = getIntent(c, false, es, idUsuarioEPessoa[2].toString());
							PendingIntent pendingIntent2 = PendingIntent.getActivity(c, OmegaConfiguration.STATE_BATIDA_PONTO_SAIDA_WIDGET, intent2, Intent.FLAG_ACTIVITY_NEW_TASK);
							views.setOnClickPendingIntent(R.id.bt_saida, pendingIntent2);
							views.setViewVisibility(R.id.ll_nao_autenticado, View.GONE);
							views.setViewVisibility(R.id.ll_autenticado, View.VISIBLE);
							inicializado=true;
							return views;
						} else{
							SingletonLog.insereErro(new Exception ("EntradaSaida nuo definida para a pessoa: " + idPessoa + " na corporacao: " + corporacao), SingletonLog.TIPO.PAGINA);
						}
						
					} 
				}
				
				return initLoginViewIfNecessary(c, inicializado, views );
				
				
			}

			catch (OmegaDatabaseException e) {
				SingletonLog.insereErro(e, SingletonLog.TIPO.UTIL_ANDROID);
				return initLoginViewIfNecessary(c, inicializado, views );
			} catch (Exception e1) {
				SingletonLog.insereErro(e1, SingletonLog.TIPO.UTIL_ANDROID);
				return initLoginViewIfNecessary(c, inicializado, views );
			} finally {
				
				
				if (db != null)
					db.close();
			}

		}
		
		private RemoteViews initLoginViewIfNecessary(Context c, boolean inicializado, RemoteViews views ){
			if(!inicializado){
				try{
					views.setViewVisibility(R.id.ll_nao_autenticado, View.VISIBLE);
					views.setViewVisibility(R.id.ll_autenticado, View.GONE);
					Intent intent = new Intent(c, PrincipalActivity.class);
					intent.putExtra(OmegaConfiguration.PARAM_ORIGEM_WIDGET, true);
					
					PendingIntent pendingIntent = PendingIntent.getActivity(c, OmegaConfiguration.STATE_LOGIN_WIDGET, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
					views.setOnClickPendingIntent(R.id.bt_login, pendingIntent);
					return views;
				}catch(Exception ex){
					SingletonLog.insereErro(ex, SingletonLog.TIPO.UTIL_ANDROID);
				}
			}
			return null;
		}
		
		private Intent getIntent(Context c, boolean entrada, final EntradaSaida es, String idCorporacao) {
			String vTextConfirmacao = EXTDAOPonto.getTextoDescritivo(c, entrada, es.pessoa, es.empresa);

			Intent intent = new Intent(c, ConfirmarPontoESalvarNoBancoTimerActivityMobile.class);
			intent.putExtra(OmegaConfiguration.PARAM_ENTRADA, entrada);
			intent.putExtra(OmegaConfiguration.PARAM_DESCRICAO_PONTO, vTextConfirmacao);
			intent.putExtra(OmegaConfiguration.PARAM_ID_PESSOA, es.idPessoa);
			intent.putExtra(OmegaConfiguration.PARAM_ID_EMPRESA, es.idEmpresa);
			intent.putExtra(OmegaConfiguration.PARAM_ID_PROFISSAO, es.idProfissao);
			intent.putExtra(OmegaConfiguration.PARAM_ID_CORPORACAO, idCorporacao);
			
			intent.putExtra(OmegaConfiguration.PARAM_IS_ENTRADA, entrada);
			
			return intent;

		}



	}

}