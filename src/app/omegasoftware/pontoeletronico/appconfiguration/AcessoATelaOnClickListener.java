package app.omegasoftware.pontoeletronico.appconfiguration;



import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import app.omegasoftware.pontoeletronico.common.controler.LoaderRunnable;

public class AcessoATelaOnClickListener  implements OnClickListener {
		
		Activity activity; 
		Class<?> classe;
		String tag;
		public AcessoATelaOnClickListener(
				Activity pActivity, 
				Class<?> pclasse){
			classe = pclasse;
			activity = pActivity;
			 
		}	
		
		public void onClick(View v) {
			
			//new LoaderAsyncTask(activity, classe, tag).execute();
			new LoaderRunnable(activity, classe, tag).execute();
		}
		
	
}
