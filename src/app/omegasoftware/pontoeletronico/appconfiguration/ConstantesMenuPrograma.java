package app.omegasoftware.pontoeletronico.appconfiguration;

import android.content.Context;
import app.omegasoftware.pontoeletronico.R;

public class ConstantesMenuPrograma {
	public static final Integer ID_RAIZ = null;
	public enum TIPO_PERMISSAO{
		MENU_COM_ICONE_DOS_FILHOS_DIFERENCIADA(1),
		ACESSO_A_TELA(2),
		ACESSO_A_FUNCIONALIDADE_DO_SISTEMA(3),
		MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS(4);
		
		public final int id ;
		
		TIPO_PERMISSAO(int id){
			this.id = id;
		}
		
		
	}
	public enum TIPO_DIMENSAO_IMAGEM{
		PEQUENO(0),
		MEDIO(1),
		GRANDE(2);
		
		public final int id ;
		
		TIPO_DIMENSAO_IMAGEM(int id){
			this.id = id;
		}
		
		public int getStyleFonte(Context context){
			//"android:textAppearance="@style/H3TextMenu";
			if(id == 0){
				return R.style.H1Text;	
			}
			else if (id == 1)
				return R.style.H2Text;
			else if (id == 2)
				return R.style.H3Text;
			else return R.style.H1Text;
		}
	}
	
	
}
