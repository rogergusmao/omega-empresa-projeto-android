package app.omegasoftware.pontoeletronico.appconfiguration;

import java.util.ArrayList;

import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.AboutActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.ConfigurationActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.DirigirVeiculoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.MenuPontoEletronicoRotinaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.MenuServicoOnlineUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.PontoEletronicoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.RelogioDePontoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.SynchronizeReceiveActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.TutorialPontoEletronicoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterBairroActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterCidadeActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterCorporacaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterEstadoActivityMobile;
//import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterMeuPontoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterModeloActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterPaisActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterPessoaContatoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterPessoaEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterPessoaFuncionarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterPontoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterProfissaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterRedeActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterRelatorioActivityMobile;
//import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterTarefaActivityMobile;
//import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterTarefaMinhaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterTipoDocumentoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterTipoEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterVeiculoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterVeiculoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormBairroActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormCategoriaPermissaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormCidadeActivityMobile;
//import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormCorporacaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEstadoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormModeloActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPaisActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPessoaContatoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPessoaFuncionarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormProfissaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormRedeActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormRelatorioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormTarefaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormTipoDocumentoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormTipoEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormVeiculoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormVeiculoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListPessoaEmpresaRotinaActivityMobile;
//import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListTarefaMinhaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListTarefaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectCategoriaPermissaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectEmpresaPontoEletronicoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectServicoHistoricoMonitoramentoRemotoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectServicoMonitoramentoRemotoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectServicoOnlineGrupoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectVeiculoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.ServiceOperacaoSistemaMobile;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.ServiceSendLogErro;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.ServiceSuporte;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.AlterarSenhaUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.container.AbstractContainerClassItem;
import app.omegasoftware.pontoeletronico.common.container.ContainerClassItem;
import app.omegasoftware.pontoeletronico.common.container.ContainerClassMenuItem;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPermissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPermissaoCategoriaPermissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioCategoriaPermissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioCorporacao;
import app.omegasoftware.pontoeletronico.service.ServiceMyPosition;
import app.omegasoftware.pontoeletronico.service.ServiceSendFileToServer;
import app.omegasoftware.pontoeletronico.service.ServiceTiraFoto;
import app.omegasoftware.pontoeletronico.service.ServiceWifi;

public class ContainerPrograma {

	public static String PONTO_ELETRONICO = "ServicePontoEletronicoActivityMobile";
	public static String RASTREAMENTO = "ServiceRastrearActivityMobile";
	public static String CADASTROS = "ServiceCadastroActivityMobile";
	public static String CADASTRO_GERAL = "ServiceCadastroGeralActivityMobile";
	public static String CATALOGO_GERAL = "ServiceCatalogoGeralActivityMobile";
	public static String CATALOGOS = "ServiceCatalogoActivityMobile";
	public static String MONITORAMENTO_REMOTO = "ServiceMonitoramentoRemotoActivityMobile";

	public static String PERMISSOES = "ServicePermissaoActivityMobile";
	public static String CONFIGURACAO = "ConfigurationActivityMobile";
	public static String MONITORAR = "ServiceMonitoramentoRemotoActivityMobile";
	public static String SERVICO_ONLINE = "ServiceOnlineActivityMobile";
	public static String RELATORIO = "ServiceRelatorioActivityMobile";

	public enum TYPE_PROGRAM {
		OMEGA_EMPRESA, // 12 dolar
		OMEGA_RASTREADOR, // 3 dolar
		OMEGA_MONITORADOR, // 3 dolar
		OMEGA_RASTREADOR_E_MONITORADOR, // 6 dolar
		OMEGA_PONTO_ELETRONICO, // 3 dolar
		OMEGA_PONTO_ELETRONICO_E_MONITORADOR, // 6 dolar
		OMEGA_CATOLOGO_ONLINE, // 2 dolar
		OMEGA_CATALOGO_OFFLINE, // free
		OMEGA_CATALOGO_ONLINE_CLIENTE, OMEGA_FACTU, OMEGA_VET_CLINICAL
	}; // free

	public enum TYPE_SERVICE_ID {
		SERVICE_MY_POSITION, SERVICE_TIRA_FOTO_INTERNA, SERVICE_TIRA_FOTO_EXTERNA
	};

	private static AbstractContainerClassItem vetorContainerClassItemCatalogoOnline[] = {
			new ContainerClassMenuItem(R.string.permissao_cadastros, CADASTROS, R.drawable.menu_cadastro),
			new ContainerClassMenuItem(R.string.permissao_catalogos, CATALOGOS, R.drawable.menu_catalogo),
			new ContainerClassMenuItem(R.string.permissao_permissoes, PERMISSOES, R.drawable.menu_permissao),
			new ContainerClassMenuItem(R.string.permissao_configuracao, CONFIGURACAO, R.drawable.menu_configuracao,
					true),
			new ContainerClassMenuItem(R.string.permissao_cadastro_geral, CADASTRO_GERAL,
					R.drawable.menu_cadastro_geral),
			new ContainerClassMenuItem(R.string.permissao_catalogo_geral, CATALOGO_GERAL,
					R.drawable.menu_catalogo_geral),
			new ContainerClassItem(R.string.permissao_alterar_senha, AlterarSenhaUsuarioActivityMobile.class,
					AlterarSenhaUsuarioActivityMobile.TAG, R.drawable.menu_configuracao),
			new ContainerClassItem(R.string.permissao_configuracao, ConfigurationActivityMobile.class,
					ConfigurationActivityMobile.TAG, R.drawable.menu_configuracao),
			new ContainerClassItem(R.string.permissao_sobre, AboutActivityMobile.class, AboutActivityMobile.TAG,
					R.drawable.menu_sobre),
			new ContainerClassItem(R.string.permissao_cadastro_pessoa, FormPessoaActivityMobile.class,
					FormPessoaActivityMobile.TAG, R.drawable.menu_cadastro_pessoa),
			new ContainerClassItem(R.string.permissao_cadastro_empresa, FormEmpresaActivityMobile.class,
					FormEmpresaActivityMobile.TAG, R.drawable.menu_cadastro_empresa),
			new ContainerClassItem(R.string.permissao_cadastro_usuario, FormUsuarioActivityMobile.class,
					FormUsuarioActivityMobile.TAG, R.drawable.menu_cadastro_usuario),
			new ContainerClassItem(R.string.permissao_cadastro_pais, FormPaisActivityMobile.class,
					FormPaisActivityMobile.TAG, R.drawable.menu_cadastro_pais),
			new ContainerClassItem(R.string.permissao_cadastro_estado, FormEstadoActivityMobile.class,
					FormEstadoActivityMobile.TAG, R.drawable.menu_cadastro_estado),
			new ContainerClassItem(R.string.permissao_cadastro_cidade, FormCidadeActivityMobile.class,
					FormCidadeActivityMobile.TAG, R.drawable.menu_cadastro_cidade),
			new ContainerClassItem(R.string.permissao_cadastro_bairro, FormBairroActivityMobile.class,
					FormBairroActivityMobile.TAG, R.drawable.menu_cadastro_bairro),
			new ContainerClassItem(R.string.permissao_cadastro_profissao, FormProfissaoActivityMobile.class,
					FormProfissaoActivityMobile.TAG, R.drawable.menu_cadastro_profissao),
			new ContainerClassItem(R.string.permissao_cadastro_tipo_documento, FormTipoDocumentoActivityMobile.class,
					FormTipoDocumentoActivityMobile.TAG, R.drawable.menu_cadastro_tipo_documento),
			new ContainerClassItem(R.string.permissao_cadastro_tipo_empresa, FormTipoEmpresaActivityMobile.class,
					FormTipoEmpresaActivityMobile.TAG, R.drawable.menu_cadastro_tipo_empresa),
			new ContainerClassItem(R.string.permissao_cadastro_categoria_permissao,
					FormCategoriaPermissaoActivityMobile.class, FormCategoriaPermissaoActivityMobile.TAG,
					R.drawable.menu_permissao_cadastrar_categoria_permissao),
			new ContainerClassItem(R.string.permissao_catalogo_pessoa, FilterPessoaActivityMobile.class,
					FilterPessoaActivityMobile.TAG, R.drawable.menu_catalogo_pessoa),
			new ContainerClassItem(R.string.permissao_catalogo_empresa, FilterEmpresaActivityMobile.class,
					FilterEmpresaActivityMobile.TAG, R.drawable.menu_catalogo_empresa),
			new ContainerClassItem(R.string.permissao_catalogo_usuario, FilterUsuarioActivityMobile.class,
					FilterUsuarioActivityMobile.TAG, R.drawable.menu_catalogo_usuario),
			new ContainerClassItem(R.string.permissao_catalogo_pais, FilterPaisActivityMobile.class,
					FilterPaisActivityMobile.TAG, R.drawable.menu_catalogo_pais),
			new ContainerClassItem(R.string.permissao_catalogo_estado, FilterEstadoActivityMobile.class,
					FilterEstadoActivityMobile.TAG, R.drawable.menu_catalogo_estado),
			new ContainerClassItem(R.string.permissao_catalogo_cidade, FilterCidadeActivityMobile.class,
					FilterCidadeActivityMobile.TAG, R.drawable.menu_catalogo_cidade),
			new ContainerClassItem(R.string.permissao_catalogo_bairro, FilterBairroActivityMobile.class,
					FilterBairroActivityMobile.TAG, R.drawable.menu_catalogo_bairro),
			new ContainerClassItem(R.string.permissao_catalogo_profissao, FilterProfissaoActivityMobile.class,
					FilterProfissaoActivityMobile.TAG, R.drawable.menu_catalogo_profissao),
			new ContainerClassItem(R.string.permissao_catalogo_tipo_documento, FilterTipoDocumentoActivityMobile.class,
					FilterTipoDocumentoActivityMobile.TAG, R.drawable.menu_catalogo_tipo_documento),
			new ContainerClassItem(R.string.permissao_catalogo_tipo_empresa, FilterTipoEmpresaActivityMobile.class,
					FilterTipoEmpresaActivityMobile.TAG, R.drawable.menu_catalogo_tipo_empresa),
			new ContainerClassItem(R.string.permissao_gerenciar_categoria_permissao,
					SelectCategoriaPermissaoActivityMobile.class, SelectCategoriaPermissaoActivityMobile.TAG,
					R.drawable.menu_permissao),
			// new ContainerClassItem(R.string.permissao_cadastro_corporacao,
			// FormCorporacaoActivityMobile.class,
			// FormCorporacaoActivityMobile.TAG,R.drawable.menu_cadastro_corporacao,
			// true),
			new ContainerClassItem(R.string.permissao_catalogo_corporacao, FilterCorporacaoActivityMobile.class,
					FilterCorporacaoActivityMobile.TAG, R.drawable.menu_catalogo_corporacao, true),
			new ContainerClassItem(R.string.permissao_cadastro_corporacao, FormRedeActivityMobile.class,
					FormRedeActivityMobile.TAG, R.drawable.menu_cadastro_corporacao, false),
			new ContainerClassItem(R.string.permissao_catalogo_corporacao, FilterRedeActivityMobile.class,
					FilterRedeActivityMobile.TAG, R.drawable.menu_catalogo_corporacao, false) };

	private static AbstractContainerClassItem vetorContainerClassItemOmegaPontoEletronico[] = {
			new ContainerClassMenuItem(R.string.permissao_ponto_eletronico, PONTO_ELETRONICO,
					R.drawable.menu_ponto_eletronico),
			new ContainerClassMenuItem(R.string.permissao_cadastros, CADASTROS, R.drawable.menu_cadastro),
			new ContainerClassMenuItem(R.string.permissao_catalogos, CATALOGOS, R.drawable.menu_catalogo),
			new ContainerClassMenuItem(R.string.permissao_permissoes, PERMISSOES, R.drawable.menu_permissao),
			new ContainerClassMenuItem(R.string.permissao_configuracao, CONFIGURACAO, R.drawable.menu_configuracao,
					true),
			new ContainerClassMenuItem(R.string.permissao_cadastro_geral, CADASTRO_GERAL,
					R.drawable.menu_cadastro_geral),
			new ContainerClassMenuItem(R.string.permissao_catalogo_geral, CATALOGO_GERAL,
					R.drawable.menu_catalogo_geral),
			new ContainerClassItem(R.string.permissao_sincronizar_dados, SynchronizeReceiveActivityMobile.class,
					SynchronizeReceiveActivityMobile.TAG, R.drawable.menu_sincronizador_upload, true),
			new ContainerClassItem(R.string.permissao_alterar_senha, AlterarSenhaUsuarioActivityMobile.class,
					AlterarSenhaUsuarioActivityMobile.TAG, R.drawable.menu_configuracao),
			new ContainerClassItem(R.string.permissao_configuracao, ConfigurationActivityMobile.class,
					ConfigurationActivityMobile.TAG, R.drawable.menu_configuracao),
			new ContainerClassItem(R.string.permissao_sobre, AboutActivityMobile.class, AboutActivityMobile.TAG,
					R.drawable.menu_sobre),
			new ContainerClassItem(R.string.permissao_ponto_eletronico,
					SelectEmpresaPontoEletronicoActivityMobile.class, SelectEmpresaPontoEletronicoActivityMobile.TAG,
					R.drawable.menu_ponto_eletronico),
			new ContainerClassItem(R.string.permissao_cadastro_tarefa, FormTarefaActivityMobile.class,
					FormTarefaActivityMobile.TAG, R.drawable.menu_cadastro_tarefa),
			new ContainerClassItem(R.string.permissao_cadastro_pessoa, FormPessoaActivityMobile.class,
					FormPessoaActivityMobile.TAG, R.drawable.menu_cadastro_pessoa),
			new ContainerClassItem(R.string.permissao_cadastro_funcionario, FormPessoaFuncionarioActivityMobile.class,
					FormPessoaFuncionarioActivityMobile.TAG, R.drawable.menu_cadastro_funcionario),
			new ContainerClassItem(R.string.permissao_cadastro_contato, FormPessoaContatoActivityMobile.class,
					FormPessoaContatoActivityMobile.TAG, R.drawable.menu_cadastro_contato),
			new ContainerClassItem(R.string.permissao_cadastro_empresa, FormEmpresaActivityMobile.class,
					FormEmpresaActivityMobile.TAG, R.drawable.menu_cadastro_empresa),
			new ContainerClassItem(R.string.permissao_cadastro_veiculo, FormVeiculoActivityMobile.class,
					FormVeiculoActivityMobile.TAG, R.drawable.menu_cadastro_veiculo),
			new ContainerClassItem(R.string.permissao_cadastro_usuario_veiculo, FormVeiculoUsuarioActivityMobile.class,
					FormVeiculoUsuarioActivityMobile.TAG, R.drawable.menu_cadastro_veiculo_usuario),
			new ContainerClassItem(R.string.permissao_cadastro_modelo, FormModeloActivityMobile.class,
					FormModeloActivityMobile.TAG, R.drawable.menu_cadastro_modelo),
			new ContainerClassItem(R.string.permissao_cadastro_usuario, FormUsuarioActivityMobile.class,
					FormUsuarioActivityMobile.TAG, R.drawable.menu_cadastro_usuario),
			new ContainerClassItem(R.string.permissao_cadastro_pais, FormPaisActivityMobile.class,
					FormPaisActivityMobile.TAG, R.drawable.menu_cadastro_pais),
			new ContainerClassItem(R.string.permissao_cadastro_estado, FormEstadoActivityMobile.class,
					FormEstadoActivityMobile.TAG, R.drawable.menu_cadastro_estado),
			new ContainerClassItem(R.string.permissao_cadastro_cidade, FormCidadeActivityMobile.class,
					FormCidadeActivityMobile.TAG, R.drawable.menu_cadastro_cidade),
			new ContainerClassItem(R.string.permissao_cadastro_bairro, FormBairroActivityMobile.class,
					FormBairroActivityMobile.TAG, R.drawable.menu_cadastro_bairro),
			new ContainerClassItem(R.string.permissao_catalogo_pessoa_empresa, FilterPessoaEmpresaActivityMobile.class,
					FilterPessoaEmpresaActivityMobile.TAG, R.drawable.menu_catalogo_geral),
			// new ContainerClassItem(R.string.permissao_sincronizacao_ponto,
			// NAOUTILIZADASincronizacaoPontoActivityMobile.class,
			// NAOUTILIZADASincronizacaoPontoActivityMobile.TAG,
			// R.drawable.menu_sincronizar_dados),
			new ContainerClassItem(R.string.permissao_tutorial_ponto_eletronico,
					TutorialPontoEletronicoActivityMobile.class, TutorialPontoEletronicoActivityMobile.TAG,
					R.drawable.help),
			// new ContainerClassItem(R.string.permissao_catalogo_tarefa,
			// FilterTarefaActivityMobile.class, FilterTarefaActivityMobile.TAG,
			// R.drawable.menu_catalogo_tarefa),
			new ContainerClassItem(R.string.permissao_catalogo_pessoa, FilterPessoaActivityMobile.class,
					FilterPessoaActivityMobile.TAG, R.drawable.menu_catalogo_pessoa),
			new ContainerClassItem(R.string.permissao_catalogo_contato, FilterPessoaContatoActivityMobile.class,
					FilterPessoaContatoActivityMobile.TAG, R.drawable.menu_catalogo_contato),
			new ContainerClassItem(R.string.permissao_catalogo_funcionario, FilterPessoaFuncionarioActivityMobile.class,
					FilterPessoaFuncionarioActivityMobile.TAG, R.drawable.menu_catalogo_funcionario),
			new ContainerClassItem(R.string.permissao_catalogo_empresa, FilterEmpresaActivityMobile.class,
					FilterEmpresaActivityMobile.TAG, R.drawable.menu_catalogo_empresa),
			new ContainerClassItem(R.string.permissao_catalogo_veiculo, FilterVeiculoActivityMobile.class,
					FilterVeiculoActivityMobile.TAG, R.drawable.menu_catalogo_veiculo),
			new ContainerClassItem(R.string.permissao_catalogo_modelo, FilterModeloActivityMobile.class,
					FilterModeloActivityMobile.TAG, R.drawable.menu_catalogo_modelo),
			new ContainerClassItem(R.string.permissao_catalogo_usuario_veiculo,
					FilterVeiculoUsuarioActivityMobile.class, FilterVeiculoUsuarioActivityMobile.TAG,
					R.drawable.menu_catalogo_veiculo_usuario),
			new ContainerClassItem(R.string.permissao_catalogo_usuario, FilterUsuarioActivityMobile.class,
					FilterUsuarioActivityMobile.TAG, R.drawable.menu_catalogo_usuario),
			new ContainerClassItem(R.string.permissao_catalogo_pais, FilterPaisActivityMobile.class,
					FilterPaisActivityMobile.TAG, R.drawable.menu_catalogo_pais),
			new ContainerClassItem(R.string.permissao_catalogo_estado, FilterEstadoActivityMobile.class,
					FilterEstadoActivityMobile.TAG, R.drawable.menu_catalogo_estado),
			new ContainerClassItem(R.string.permissao_catalogo_cidade, FilterCidadeActivityMobile.class,
					FilterCidadeActivityMobile.TAG, R.drawable.menu_catalogo_cidade),
			new ContainerClassItem(R.string.permissao_catalogo_bairro, FilterBairroActivityMobile.class,
					FilterBairroActivityMobile.TAG, R.drawable.menu_catalogo_bairro),
			new ContainerClassItem(R.string.permissao_catalogo_profissao, FilterProfissaoActivityMobile.class,
					FilterProfissaoActivityMobile.TAG, R.drawable.menu_catalogo_profissao),
			new ContainerClassItem(R.string.permissao_catalogo_tipo_documento, FilterTipoDocumentoActivityMobile.class,
					FilterTipoDocumentoActivityMobile.TAG, R.drawable.menu_catalogo_tipo_documento),
			new ContainerClassItem(R.string.permissao_catalogo_tipo_empresa, FilterTipoEmpresaActivityMobile.class,
					FilterTipoEmpresaActivityMobile.TAG, R.drawable.menu_catalogo_tipo_empresa),
			new ContainerClassItem(R.string.permissao_cadastro_profissao, FormProfissaoActivityMobile.class,
					FormProfissaoActivityMobile.TAG, R.drawable.menu_cadastro_profissao),
			new ContainerClassItem(R.string.permissao_cadastro_tipo_documento, FormTipoDocumentoActivityMobile.class,
					FormTipoDocumentoActivityMobile.TAG, R.drawable.menu_cadastro_tipo_documento),
			new ContainerClassItem(R.string.permissao_cadastro_tipo_empresa, FormTipoEmpresaActivityMobile.class,
					FormTipoEmpresaActivityMobile.TAG, R.drawable.menu_cadastro_tipo_empresa),
			new ContainerClassItem(R.string.permissao_cadastro_categoria_permissao,
					FormCategoriaPermissaoActivityMobile.class, FormCategoriaPermissaoActivityMobile.TAG,
					R.drawable.menu_permissao_cadastrar_categoria_permissao),
			new ContainerClassItem(R.string.permissao_gerenciar_categoria_permissao,
					SelectCategoriaPermissaoActivityMobile.class, SelectCategoriaPermissaoActivityMobile.TAG,
					R.drawable.menu_permissao),
			// new ContainerClassItem(R.string.permissao_cadastro_corporacao,
			// FormCorporacaoActivityMobile.class,
			// FormCorporacaoActivityMobile.TAG,R.drawable.menu_cadastro_corporacao,
			// true),
			new ContainerClassItem(R.string.permissao_catalogo_corporacao, FilterCorporacaoActivityMobile.class,
					FilterCorporacaoActivityMobile.TAG, R.drawable.menu_catalogo_corporacao, true) };

	private static AbstractContainerClassItem vetorContainerClassItemOmegaFactu[] = {
			new ContainerClassMenuItem(R.string.permissao_ponto_eletronico, PONTO_ELETRONICO,
					R.drawable.menu_ponto_eletronico),
			new ContainerClassMenuItem(R.string.permissao_cadastros, CADASTROS, R.drawable.menu_cadastro),
			new ContainerClassMenuItem(R.string.permissao_catalogos, CATALOGOS, R.drawable.menu_catalogo),
			new ContainerClassMenuItem(R.string.permissao_servicos_online, SERVICO_ONLINE,
					R.drawable.menu_servico_online),
			new ContainerClassItem(R.string.permissao_servico_online_usuario,
					MenuServicoOnlineUsuarioActivityMobile.class, MenuServicoOnlineUsuarioActivityMobile.TAG,
					R.drawable.menu_servico_online_usuario),
			new ContainerClassItem(R.string.permissao_servico_online_grupo,
					SelectServicoOnlineGrupoUsuarioActivityMobile.class,
					SelectServicoOnlineGrupoUsuarioActivityMobile.TAG, R.drawable.menu_servico_online_grupo),
			new ContainerClassMenuItem(R.string.permissao_permissoes, PERMISSOES, R.drawable.menu_permissao),
			new ContainerClassMenuItem(R.string.permissao_configuracao, CONFIGURACAO, R.drawable.menu_configuracao,
					true),
			new ContainerClassMenuItem(R.string.permissao_cadastro_geral, CADASTRO_GERAL,
					R.drawable.menu_cadastro_geral),
			new ContainerClassMenuItem(R.string.permissao_catalogo_geral, CATALOGO_GERAL,
					R.drawable.menu_catalogo_geral),
			new ContainerClassMenuItem(R.string.permissao_relatorio, RELATORIO, R.drawable.menu_relatorio),
			new ContainerClassItem(R.string.permissao_sincronizar_dados, SynchronizeReceiveActivityMobile.class,
					SynchronizeReceiveActivityMobile.TAG, R.drawable.menu_sincronizador_upload, true),
			new ContainerClassItem(R.string.permissao_alterar_senha, AlterarSenhaUsuarioActivityMobile.class,
					AlterarSenhaUsuarioActivityMobile.TAG, R.drawable.menu_configuracao),
			new ContainerClassItem(R.string.permissao_configuracao, ConfigurationActivityMobile.class,
					ConfigurationActivityMobile.TAG, R.drawable.menu_configuracao),
			new ContainerClassItem(R.string.permissao_ponto_eletronico,
					SelectEmpresaPontoEletronicoActivityMobile.class, SelectEmpresaPontoEletronicoActivityMobile.TAG,
					R.drawable.menu_ponto_eletronico),
			new ContainerClassItem(R.string.permissao_cadastro_tarefa, FormTarefaActivityMobile.class,
					FormTarefaActivityMobile.TAG, R.drawable.menu_cadastro_tarefa),
			new ContainerClassItem(R.string.permissao_cadastro_pessoa, FormPessoaActivityMobile.class,
					FormPessoaActivityMobile.TAG, R.drawable.menu_cadastro_pessoa),
			new ContainerClassItem(R.string.permissao_cadastro_funcionario, FormPessoaFuncionarioActivityMobile.class,
					FormPessoaFuncionarioActivityMobile.TAG, R.drawable.menu_cadastro_funcionario),
			new ContainerClassItem(R.string.permissao_cadastro_contato, FormPessoaContatoActivityMobile.class,
					FormPessoaContatoActivityMobile.TAG, R.drawable.menu_cadastro_contato),
			new ContainerClassItem(R.string.permissao_cadastro_empresa, FormEmpresaActivityMobile.class,
					FormEmpresaActivityMobile.TAG, R.drawable.menu_cadastro_empresa),
			new ContainerClassItem(R.string.permissao_cadastro_veiculo, FormVeiculoActivityMobile.class,
					FormVeiculoActivityMobile.TAG, R.drawable.menu_cadastro_veiculo),
			new ContainerClassItem(R.string.permissao_cadastro_usuario_veiculo, FormVeiculoUsuarioActivityMobile.class,
					FormVeiculoUsuarioActivityMobile.TAG, R.drawable.menu_cadastro_veiculo_usuario),
			new ContainerClassItem(R.string.permissao_cadastro_modelo, FormModeloActivityMobile.class,
					FormModeloActivityMobile.TAG, R.drawable.menu_cadastro_modelo),
			new ContainerClassItem(R.string.permissao_cadastro_usuario, FormUsuarioActivityMobile.class,
					FormUsuarioActivityMobile.TAG, R.drawable.menu_cadastro_usuario),
			new ContainerClassItem(R.string.permissao_cadastro_pais, FormPaisActivityMobile.class,
					FormPaisActivityMobile.TAG, R.drawable.menu_cadastro_pais),
			new ContainerClassItem(R.string.permissao_cadastro_estado, FormEstadoActivityMobile.class,
					FormEstadoActivityMobile.TAG, R.drawable.menu_cadastro_estado),
			new ContainerClassItem(R.string.permissao_cadastro_cidade, FormCidadeActivityMobile.class,
					FormCidadeActivityMobile.TAG, R.drawable.menu_cadastro_cidade),
			new ContainerClassItem(R.string.permissao_cadastro_bairro, FormBairroActivityMobile.class,
					FormBairroActivityMobile.TAG, R.drawable.menu_cadastro_bairro),
			new ContainerClassItem(R.string.permissao_catalogo_pessoa_empresa, FilterPessoaEmpresaActivityMobile.class,
					FilterPessoaEmpresaActivityMobile.TAG, R.drawable.menu_catalogo_geral),
			// new ContainerClassItem(R.string.permissao_sincronizacao_ponto,
			// NAOUTILIZADASincronizacaoPontoActivityMobile.class,
			// NAOUTILIZADASincronizacaoPontoActivityMobile.TAG,
			// R.drawable.menu_sincronizar_dados),
			new ContainerClassItem(R.string.permissao_tutorial_ponto_eletronico,
					TutorialPontoEletronicoActivityMobile.class, TutorialPontoEletronicoActivityMobile.TAG,
					R.drawable.help),
			new ContainerClassItem(R.string.permissao_catalogo_pessoa, FilterPessoaActivityMobile.class,
					FilterPessoaActivityMobile.TAG, R.drawable.menu_catalogo_pessoa),
			new ContainerClassItem(R.string.permissao_catalogo_contato, FilterPessoaContatoActivityMobile.class,
					FilterPessoaContatoActivityMobile.TAG, R.drawable.menu_catalogo_contato),
			new ContainerClassItem(R.string.permissao_catalogo_funcionario, FilterPessoaFuncionarioActivityMobile.class,
					FilterPessoaFuncionarioActivityMobile.TAG, R.drawable.menu_catalogo_funcionario),
			new ContainerClassItem(R.string.permissao_catalogo_empresa, FilterEmpresaActivityMobile.class,
					FilterEmpresaActivityMobile.TAG, R.drawable.menu_catalogo_empresa),
			new ContainerClassItem(R.string.permissao_catalogo_veiculo, FilterVeiculoActivityMobile.class,
					FilterVeiculoActivityMobile.TAG, R.drawable.menu_catalogo_veiculo),
			new ContainerClassItem(R.string.permissao_catalogo_modelo, FilterModeloActivityMobile.class,
					FilterModeloActivityMobile.TAG, R.drawable.menu_catalogo_modelo),
			new ContainerClassItem(R.string.permissao_catalogo_usuario_veiculo,
					FilterVeiculoUsuarioActivityMobile.class, FilterVeiculoUsuarioActivityMobile.TAG,
					R.drawable.menu_catalogo_veiculo_usuario),
			new ContainerClassItem(R.string.permissao_catalogo_usuario, FilterUsuarioActivityMobile.class,
					FilterUsuarioActivityMobile.TAG, R.drawable.menu_catalogo_usuario),
			new ContainerClassItem(R.string.permissao_catalogo_pais, FilterPaisActivityMobile.class,
					FilterPaisActivityMobile.TAG, R.drawable.menu_catalogo_pais),
			new ContainerClassItem(R.string.permissao_catalogo_estado, FilterEstadoActivityMobile.class,
					FilterEstadoActivityMobile.TAG, R.drawable.menu_catalogo_estado),
			new ContainerClassItem(R.string.permissao_catalogo_cidade, FilterCidadeActivityMobile.class,
					FilterCidadeActivityMobile.TAG, R.drawable.menu_catalogo_cidade),
			new ContainerClassItem(R.string.permissao_catalogo_bairro, FilterBairroActivityMobile.class,
					FilterBairroActivityMobile.TAG, R.drawable.menu_catalogo_bairro),
			new ContainerClassItem(R.string.permissao_catalogo_profissao, FilterProfissaoActivityMobile.class,
					FilterProfissaoActivityMobile.TAG, R.drawable.menu_catalogo_profissao),
			new ContainerClassItem(R.string.permissao_catalogo_tipo_documento, FilterTipoDocumentoActivityMobile.class,
					FilterTipoDocumentoActivityMobile.TAG, R.drawable.menu_catalogo_tipo_documento),
			new ContainerClassItem(R.string.permissao_catalogo_tipo_empresa, FilterTipoEmpresaActivityMobile.class,
					FilterTipoEmpresaActivityMobile.TAG, R.drawable.menu_catalogo_tipo_empresa),
			new ContainerClassItem(R.string.permissao_cadastro_profissao, FormProfissaoActivityMobile.class,
					FormProfissaoActivityMobile.TAG, R.drawable.menu_cadastro_profissao),
			new ContainerClassItem(R.string.permissao_cadastro_tipo_documento, FormTipoDocumentoActivityMobile.class,
					FormTipoDocumentoActivityMobile.TAG, R.drawable.menu_cadastro_tipo_documento),
			new ContainerClassItem(R.string.permissao_cadastro_tipo_empresa, FormTipoEmpresaActivityMobile.class,
					FormTipoEmpresaActivityMobile.TAG, R.drawable.menu_cadastro_tipo_empresa),
			new ContainerClassItem(R.string.permissao_cadastro_categoria_permissao,
					FormCategoriaPermissaoActivityMobile.class, FormCategoriaPermissaoActivityMobile.TAG,
					R.drawable.menu_permissao_cadastrar_categoria_permissao),
			new ContainerClassItem(R.string.permissao_gerenciar_categoria_permissao,
					SelectCategoriaPermissaoActivityMobile.class, SelectCategoriaPermissaoActivityMobile.TAG,
					R.drawable.menu_permissao),
			new ContainerClassItem(R.string.permissao_cadastro_rede, FormRedeActivityMobile.class,
					FormRedeActivityMobile.TAG, R.drawable.menu_cadastro_rede, false),
			new ContainerClassItem(R.string.permissao_catalogo_rede, FilterRedeActivityMobile.class,
					FilterRedeActivityMobile.TAG, R.drawable.menu_catalogo_rede, false),
			new ContainerClassItem(R.string.permissao_minha_rotina, ListPessoaEmpresaRotinaActivityMobile.class,
					ListPessoaEmpresaRotinaActivityMobile.TAG, R.drawable.menu_minha_rotina),
			new ContainerClassItem(R.string.permissao_catalogo_ponto, FilterPontoActivityMobile.class,
					FilterPontoActivityMobile.TAG, R.drawable.menu_catalogo_ponto),
			// new ContainerClassItem(R.string.permissao_catalogo_meu_ponto,
			// FilterMeuPontoActivityMobile.class,
			// FilterMeuPontoActivityMobile.TAG,
			// R.drawable.menu_catalogo_meu_ponto),
			new ContainerClassItem(R.string.permissao_catalogo_relatorio, FilterRelatorioActivityMobile.class,
					FilterRelatorioActivityMobile.TAG, R.drawable.menu_catalogo_relatorio),
			new ContainerClassItem(R.string.permissao_cadastro_relatorio, FormRelatorioActivityMobile.class,
					FormRelatorioActivityMobile.TAG, R.drawable.menu_cadastro_relatorio), };

	private static AbstractContainerClassItem vetorContainerClassItemOmegaEmpresa[] = {
			// new ContainerClassMenuItem(R.string.permissao_ponto_eletronico,
			// PONTO_ELETRONICO, R.drawable.menu_ponto_eletronico),
			new ContainerClassItem(R.string.permissao_ponto_eletronico, RelogioDePontoActivityMobile.class,
					PONTO_ELETRONICO, R.drawable.menu_ponto_eletronico),
			new ContainerClassMenuItem(R.string.permissao_rastreamento, RASTREAMENTO,
					R.drawable.menu_rastrear_monitorar),
			new ContainerClassMenuItem(R.string.permissao_cadastros, CADASTROS, R.drawable.menu_cadastro),
			new ContainerClassMenuItem(R.string.permissao_catalogos, CATALOGOS, R.drawable.menu_catalogo),
			new ContainerClassMenuItem(R.string.permissao_monitoramento_remoto, MONITORAMENTO_REMOTO,
					R.drawable.menu_monitor),
			new ContainerClassMenuItem(R.string.permissao_permissoes, PERMISSOES, R.drawable.menu_permissao),
			new ContainerClassMenuItem(R.string.permissao_configuracao, CONFIGURACAO, R.drawable.menu_configuracao,
					true),
			new ContainerClassMenuItem(R.string.permissao_servicos_online, SERVICO_ONLINE,
					R.drawable.menu_servico_online),
			new ContainerClassMenuItem(R.string.permissao_cadastro_geral, CADASTRO_GERAL,
					R.drawable.menu_cadastro_geral),
			new ContainerClassMenuItem(R.string.permissao_catalogo_geral, CATALOGO_GERAL,
					R.drawable.menu_catalogo_geral),
			new ContainerClassItem(R.string.permissao_sincronizar_dados, SynchronizeReceiveActivityMobile.class,
					SynchronizeReceiveActivityMobile.TAG, R.drawable.menu_sincronizador_upload, true),
			new ContainerClassItem(R.string.permissao_alterar_senha, AlterarSenhaUsuarioActivityMobile.class,
					AlterarSenhaUsuarioActivityMobile.TAG, R.drawable.menu_configuracao),
			new ContainerClassItem(R.string.permissao_servico_online_usuario,
					MenuServicoOnlineUsuarioActivityMobile.class, MenuServicoOnlineUsuarioActivityMobile.TAG,
					R.drawable.menu_servico_online_usuario),
			new ContainerClassItem(R.string.permissao_servico_online_grupo,
					SelectServicoOnlineGrupoUsuarioActivityMobile.class,
					SelectServicoOnlineGrupoUsuarioActivityMobile.TAG, R.drawable.menu_servico_online_grupo),
			new ContainerClassItem(R.string.permissao_configuracao, ConfigurationActivityMobile.class,
					ConfigurationActivityMobile.TAG, R.drawable.menu_configuracao),
			// new ContainerClassItem(R.string.permissao_ponto_eletronico,
			// SelectEmpresaPontoEletronicoActivityMobile.class,
			// SelectEmpresaPontoEletronicoActivityMobile.TAG,
			// R.drawable.menu_ponto_eletronico),
			new ContainerClassItem(R.string.permissao_leitor_qr_code, PontoEletronicoActivityMobile.class,
					PontoEletronicoActivityMobile.TAG, R.drawable.menu_ponto_eletronico_automatico),
			new ContainerClassItem(R.string.permissao_catalogo_pessoa_empresa, FilterPessoaEmpresaActivityMobile.class,
					FilterPessoaEmpresaActivityMobile.TAG, R.drawable.menu_catalogo_geral),
			// new ContainerClassItem(R.string.permissao_sincronizacao_ponto,
			// NAOUTILIZADASincronizacaoPontoActivityMobile.class,
			// NAOUTILIZADASincronizacaoPontoActivityMobile.TAG,
			// R.drawable.menu_sincronizar_dados),
			new ContainerClassItem(R.string.permissao_tutorial_ponto_eletronico,
					TutorialPontoEletronicoActivityMobile.class, TutorialPontoEletronicoActivityMobile.TAG,
					R.drawable.help),
			new ContainerClassItem(R.string.permissao_monitorar, SelectVeiculoUsuarioActivityMobile.class,
					SelectVeiculoUsuarioActivityMobile.TAG, R.drawable.menu_rastrear_monitorar),
			// new ContainerClassItem(R.string.permissao_historico_rota,
			// SelectUsuarioPosicaoActivityMobile.class,
			// SelectUsuarioPosicaoActivityMobile.TAG,
			// R.drawable.menu_rastrear_historico_rota),
			new ContainerClassItem(R.string.permissao_dirigir_veiculo, DirigirVeiculoActivityMobile.class,
					DirigirVeiculoActivityMobile.TAG, R.drawable.menu_rastrear_dirigir_veiculo),
			new ContainerClassItem(R.string.permissao_monitoramento_remoto, DirigirVeiculoActivityMobile.class,
					DirigirVeiculoActivityMobile.TAG, R.drawable.menu_monitoramento_remoto_rastrear_usuario),
			new ContainerClassItem(R.string.permissao_cadastro_pessoa, FormPessoaActivityMobile.class,
					FormPessoaActivityMobile.TAG, R.drawable.menu_cadastro_pessoa),
			new ContainerClassItem(R.string.permissao_cadastro_funcionario, FormPessoaFuncionarioActivityMobile.class,
					FormPessoaFuncionarioActivityMobile.TAG, R.drawable.menu_cadastro_funcionario),
			new ContainerClassItem(R.string.permissao_cadastro_contato, FormPessoaContatoActivityMobile.class,
					FormPessoaContatoActivityMobile.TAG, R.drawable.menu_cadastro_contato),
			new ContainerClassItem(R.string.permissao_cadastro_empresa, FormEmpresaActivityMobile.class,
					FormEmpresaActivityMobile.TAG, R.drawable.menu_cadastro_empresa),
			new ContainerClassItem(R.string.permissao_cadastro_veiculo, FormVeiculoActivityMobile.class,
					FormVeiculoActivityMobile.TAG, R.drawable.menu_cadastro_veiculo),
			new ContainerClassItem(R.string.permissao_cadastro_usuario_veiculo, FormVeiculoUsuarioActivityMobile.class,
					FormVeiculoUsuarioActivityMobile.TAG, R.drawable.menu_cadastro_veiculo_usuario),
			new ContainerClassItem(R.string.permissao_cadastro_modelo, FormModeloActivityMobile.class,
					FormModeloActivityMobile.TAG, R.drawable.menu_cadastro_modelo),
			new ContainerClassItem(R.string.permissao_cadastro_usuario, FormUsuarioActivityMobile.class,
					FormUsuarioActivityMobile.TAG, R.drawable.menu_cadastro_usuario),
			new ContainerClassItem(R.string.permissao_cadastro_pais, FormPaisActivityMobile.class,
					FormPaisActivityMobile.TAG, R.drawable.menu_cadastro_pais),
			new ContainerClassItem(R.string.permissao_cadastro_estado, FormEstadoActivityMobile.class,
					FormEstadoActivityMobile.TAG, R.drawable.menu_cadastro_estado),
			new ContainerClassItem(R.string.permissao_cadastro_cidade, FormCidadeActivityMobile.class,
					FormCidadeActivityMobile.TAG, R.drawable.menu_cadastro_cidade),
			new ContainerClassItem(R.string.permissao_cadastro_bairro, FormBairroActivityMobile.class,
					FormBairroActivityMobile.TAG, R.drawable.menu_cadastro_bairro),
			// new ContainerClassItem(R.string.permissao_catalogo_tarefa,
			// FilterTarefaActivityMobile.class, FilterTarefaActivityMobile.TAG,
			// R.drawable.menu_catalogo_tarefa),
			new ContainerClassItem(R.string.permissao_catalogo_pessoa, FilterPessoaActivityMobile.class,
					FilterPessoaActivityMobile.TAG, R.drawable.menu_catalogo_pessoa),
			new ContainerClassItem(R.string.permissao_catalogo_contato, FilterPessoaContatoActivityMobile.class,
					FilterPessoaContatoActivityMobile.TAG, R.drawable.menu_catalogo_contato),
			new ContainerClassItem(R.string.permissao_catalogo_funcionario, FilterPessoaFuncionarioActivityMobile.class,
					FilterPessoaFuncionarioActivityMobile.TAG, R.drawable.menu_catalogo_funcionario),
			new ContainerClassItem(R.string.permissao_catalogo_empresa, FilterEmpresaActivityMobile.class,
					FilterEmpresaActivityMobile.TAG, R.drawable.menu_catalogo_empresa),
			new ContainerClassItem(R.string.permissao_catalogo_veiculo, FilterVeiculoActivityMobile.class,
					FilterVeiculoActivityMobile.TAG, R.drawable.menu_catalogo_veiculo),
			new ContainerClassItem(R.string.permissao_catalogo_modelo, FilterModeloActivityMobile.class,
					FilterModeloActivityMobile.TAG, R.drawable.menu_catalogo_modelo),
			new ContainerClassItem(R.string.permissao_catalogo_usuario_veiculo,
					FilterVeiculoUsuarioActivityMobile.class, FilterVeiculoUsuarioActivityMobile.TAG,
					R.drawable.menu_catalogo_veiculo_usuario),
			new ContainerClassItem(R.string.permissao_catalogo_usuario, FilterUsuarioActivityMobile.class,
					FilterUsuarioActivityMobile.TAG, R.drawable.menu_catalogo_usuario),
			new ContainerClassItem(R.string.permissao_catalogo_pais, FilterPaisActivityMobile.class,
					FilterPaisActivityMobile.TAG, R.drawable.menu_catalogo_pais),
			new ContainerClassItem(R.string.permissao_catalogo_estado, FilterEstadoActivityMobile.class,
					FilterEstadoActivityMobile.TAG, R.drawable.menu_catalogo_estado),
			new ContainerClassItem(R.string.permissao_catalogo_cidade, FilterCidadeActivityMobile.class,
					FilterCidadeActivityMobile.TAG, R.drawable.menu_catalogo_cidade),
			new ContainerClassItem(R.string.permissao_catalogo_bairro, FilterBairroActivityMobile.class,
					FilterBairroActivityMobile.TAG, R.drawable.menu_catalogo_bairro),
			new ContainerClassItem(R.string.permissao_catalogo_profissao, FilterProfissaoActivityMobile.class,
					FilterProfissaoActivityMobile.TAG, R.drawable.menu_catalogo_profissao),
			new ContainerClassItem(R.string.permissao_catalogo_tipo_documento, FilterTipoDocumentoActivityMobile.class,
					FilterTipoDocumentoActivityMobile.TAG, R.drawable.menu_catalogo_tipo_documento),
			new ContainerClassItem(R.string.permissao_catalogo_tipo_empresa, FilterTipoEmpresaActivityMobile.class,
					FilterTipoEmpresaActivityMobile.TAG, R.drawable.menu_catalogo_tipo_empresa),
			new ContainerClassItem(R.string.permissao_cadastro_profissao, FormProfissaoActivityMobile.class,
					FormProfissaoActivityMobile.TAG, R.drawable.menu_cadastro_profissao),
			new ContainerClassItem(R.string.permissao_cadastro_tipo_documento, FormTipoDocumentoActivityMobile.class,
					FormTipoDocumentoActivityMobile.TAG, R.drawable.menu_cadastro_tipo_documento),
			new ContainerClassItem(R.string.permissao_cadastro_tipo_empresa, FormTipoEmpresaActivityMobile.class,
					FormTipoEmpresaActivityMobile.TAG, R.drawable.menu_cadastro_tipo_empresa),
			new ContainerClassItem(R.string.permissao_monitoramento_remoto,
					SelectServicoMonitoramentoRemotoUsuarioActivityMobile.class,
					SelectServicoMonitoramentoRemotoUsuarioActivityMobile.TAG, R.drawable.menu_rastrear_monitorar),
			new ContainerClassItem(R.string.permissao_historico_monitoramento_usuario,
					SelectServicoHistoricoMonitoramentoRemotoUsuarioActivityMobile.class,
					SelectServicoHistoricoMonitoramentoRemotoUsuarioActivityMobile.TAG,
					R.drawable.menu_rastrear_historico_rota),
			new ContainerClassItem(R.string.permissao_cadastro_categoria_permissao,
					FormCategoriaPermissaoActivityMobile.class, FormCategoriaPermissaoActivityMobile.TAG,
					R.drawable.menu_permissao_cadastrar_categoria_permissao),
			new ContainerClassItem(R.string.permissao_gerenciar_categoria_permissao,
					SelectCategoriaPermissaoActivityMobile.class, SelectCategoriaPermissaoActivityMobile.TAG,
					R.drawable.menu_permissao),
			// new ContainerClassItem(R.string.permissao_cadastro_corporacao,
			// FormCorporacaoActivityMobile.class,
			// FormCorporacaoActivityMobile.TAG,R.drawable.menu_cadastro_corporacao,
			// true),
			new ContainerClassItem(
					R.string.permissao_catalogo_corporacao,
					FilterCorporacaoActivityMobile.class,
					FilterCorporacaoActivityMobile.TAG,
					R.drawable.menu_catalogo_corporacao,
					true),
			 new ContainerClassItem(
			 		R.string.permissao_minhas_tarefas,
			 		ListTarefaActivityMobile.class,
					 ListTarefaActivityMobile.TAG,
			 		R.drawable.menu_catalogo_tarefa),
			new ContainerClassItem(R.string.permissao_minha_rotina, MenuPontoEletronicoRotinaActivityMobile.class,
					MenuPontoEletronicoRotinaActivityMobile.TAG, R.drawable.menu_minha_rotina) };

	public static boolean isEmptyServicoPermitido() {
		int vVetorIdServico[] = getVetorIdServicoPermissaoOnline();
		if (vVetorIdServico == null)
			return true;
		else if (vVetorIdServico.length == 0)
			return true;
		else
			return false;
	}

	public static boolean isServicoPermitido(int pId) {
		int[] vVetorIdServicoInterno = getVetorIdServicoInterno();
		int[] vVetorIdServicoPermitido = getVetorIdServicoPermissaoOnline();
		int[] vVetorIdServicoBibliotecaNuvem = getVetorIdServicoBibliotecaNuvem();

		int[][] matrizIdServico = { vVetorIdServicoInterno, vVetorIdServicoPermitido, vVetorIdServicoBibliotecaNuvem };
		for (int i = 0; i < matrizIdServico.length; i++) {
			int[] vVetor = matrizIdServico[i];
			if (vVetor != null)
				for (int id : vVetor) {
					if (id == pId)
						return true;
				}
		}

		return false;

	}

	public static boolean isCategoriaPermissaoPermitidaParaUsuarioLogado(Database db, String pTagClassPermissao) {
		try {
			if (OmegaConfiguration.DEBUGGING)
				return true;
			else if (pTagClassPermissao == null || pTagClassPermissao.length() == 0)
				return false;

			EXTDAOUsuarioCorporacao vObjUsuarioCorporacao = new EXTDAOUsuarioCorporacao(db);
			vObjUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.USUARIO_ID_INT, OmegaSecurity.getIdUsuario());
			vObjUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.CORPORACAO_ID_INT,
					OmegaSecurity.getIdCorporacao());
			ArrayList<Table> vListUsuarioCorporacao = vObjUsuarioCorporacao.getListTable();
			if (vListUsuarioCorporacao != null && vListUsuarioCorporacao.size() > 0) {
				EXTDAOUsuarioCorporacao vTuplaUsuarioCorporacao = (EXTDAOUsuarioCorporacao) vListUsuarioCorporacao
						.get(0);
				String vIsAdm = vTuplaUsuarioCorporacao.getStrValueOfAttribute(EXTDAOUsuarioCorporacao.IS_ADM_BOOLEAN);
				if (vIsAdm != null && vIsAdm.length() > 0) {
					if (vIsAdm.compareTo("1") == 0)
						return true;
				}
			}
			EXTDAOUsuarioCategoriaPermissao vObjUsuarioCategoriaPermissao = new EXTDAOUsuarioCategoriaPermissao(db);
			vObjUsuarioCategoriaPermissao.setAttrValue(EXTDAOUsuarioCategoriaPermissao.USUARIO_ID_INT,
					OmegaSecurity.getIdUsuario());
			ArrayList<Table> vListUCP = vObjUsuarioCategoriaPermissao.getListTable();

			if (vListUCP != null)
				if (vListUCP.size() == 1) {
					// Id da categoria do usuario
					Table vTuplaUCP = vListUCP.get(0);
					String vIdCategoriaIdPermissao = vTuplaUCP
							.getStrValueOfAttribute(EXTDAOUsuarioCategoriaPermissao.CATEGORIA_PERMISSAO_ID_INT);

					EXTDAOPermissaoCategoriaPermissao vObjPCP = new EXTDAOPermissaoCategoriaPermissao(db);
					vObjPCP.setAttrValue(EXTDAOPermissaoCategoriaPermissao.CATEGORIA_PERMISSAO_ID_INT,
							vIdCategoriaIdPermissao);

					ArrayList<Table> vList = vObjPCP.getListTable();
					for (Table tuplaPCP : vList) {
						String vIdPermissao = tuplaPCP
								.getStrValueOfAttribute(EXTDAOPermissaoCategoriaPermissao.PERMISSAO_ID_INT);
						EXTDAOPermissao vObjPermissao = new EXTDAOPermissao(db);
						vObjPermissao.setAttrValue(EXTDAOPermissao.ID, vIdPermissao);
						if (vObjPermissao.select()) {
							String vTag = vObjPermissao.getStrValueOfAttribute(EXTDAOPermissao.TAG);
							if (vTag != null && vTag.length() > 0 && vTag.compareTo(pTagClassPermissao) == 0) {
								return true;
							}
						}
					}
				}
			return false;
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			return false;
		}
	}

	public static AbstractContainerClassItem[] getVetorContainerClassItem() {
		switch (OmegaConfiguration.PROGRAM) {
		case OMEGA_FACTU:
			return vetorContainerClassItemOmegaFactu;
		case OMEGA_VET_CLINICAL:
			return vetorContainerClassItemOmegaEmpresa;
		case OMEGA_CATALOGO_OFFLINE:
			return null;
		case OMEGA_EMPRESA:
			return vetorContainerClassItemOmegaEmpresa;
		case OMEGA_CATOLOGO_ONLINE:
			return vetorContainerClassItemCatalogoOnline;
		case OMEGA_MONITORADOR:
			return null;
		case OMEGA_PONTO_ELETRONICO:
			return vetorContainerClassItemOmegaPontoEletronico;
		case OMEGA_RASTREADOR:
			return null;
		case OMEGA_PONTO_ELETRONICO_E_MONITORADOR:
			return null;
		case OMEGA_RASTREADOR_E_MONITORADOR:
			return null;
		case OMEGA_CATALOGO_ONLINE_CLIENTE:
			return null;
		default:
			return null;
		}
	}

	public static String getIdPrograma() {
		switch (OmegaConfiguration.PROGRAM) {
		case OMEGA_CATALOGO_OFFLINE:
			return "1";
		case OMEGA_EMPRESA:
			return "2";
		case OMEGA_CATOLOGO_ONLINE:
			return "3";
		case OMEGA_MONITORADOR:
			return "4";
		case OMEGA_PONTO_ELETRONICO:
			return "5";
		case OMEGA_RASTREADOR:
			return "6";
		case OMEGA_PONTO_ELETRONICO_E_MONITORADOR:
			return "7";
		case OMEGA_RASTREADOR_E_MONITORADOR:
			return "8";
		case OMEGA_CATALOGO_ONLINE_CLIENTE:
			return "9";
		case OMEGA_FACTU:
			return "10";
		case OMEGA_VET_CLINICAL:
			return "11";
		default:
			return null;
		}
	}

	public static String getNomeGrupoDaEmpresa() {
		switch (OmegaConfiguration.PROGRAM) {
		case OMEGA_FACTU:
			return "FACTU";
		case OMEGA_VET_CLINICAL:
			return "OMEGA_VET_CLINICAL";
		default:
			return null;
		}
	}

	public static boolean isProgramaDaEmpresa() {
		switch (OmegaConfiguration.PROGRAM) {
		case OMEGA_FACTU:
			return true;
		case OMEGA_VET_CLINICAL:
			return true;
		default:
			return false;
		}
	}

	// Servicos que devem ser realizada a checagem se existe permissao online
	public static int[] getVetorIdServicoBibliotecaNuvem() {
		switch (OmegaConfiguration.PROGRAM) {
		case OMEGA_FACTU:
			return new int[] { ServiceOperacaoSistemaMobile.ID, ServiceSuporte.ID, ServiceSendLogErro.ID };
		case OMEGA_VET_CLINICAL:
			return new int[] { ServiceOperacaoSistemaMobile.ID, ServiceSuporte.ID, ServiceSendLogErro.ID };
		case OMEGA_EMPRESA:
			// return new int[] {ServiceSendLogErro.ID };
			// return new int[] {ServiceOperacaoSistemaMobile.ID,
			// ServiceSuporte.ID, ServiceSendLogErro.ID };
			return null;
		default:
			return null;
		}
	}

	// Servicos que devem ser realizada a checagem se existe permissao online
	public static int[] getVetorIdServicoPermissaoOnline() {
		switch (OmegaConfiguration.PROGRAM) {
		case OMEGA_FACTU:
			return new int[] { ServiceMyPosition.ID };
		case OMEGA_VET_CLINICAL:
			return new int[] { ServiceMyPosition.ID };
		case OMEGA_CATALOGO_OFFLINE:
			return null;
		case OMEGA_EMPRESA:
			// return new int[] {ServiceTiraFoto.ID_INTERNO,
			// ServiceTiraFoto.ID_EXTERNO, ServiceMyPosition.ID};
			return new int[] { ServiceMyPosition.ID , ServiceWifi.ID};

		case OMEGA_CATOLOGO_ONLINE:
			return null;
		case OMEGA_MONITORADOR:
			return new int[] { ServiceTiraFoto.ID_INTERNO, ServiceTiraFoto.ID_EXTERNO };
		case OMEGA_PONTO_ELETRONICO:
			return null;
		case OMEGA_RASTREADOR:
			return new int[] { ServiceMyPosition.ID };
		case OMEGA_PONTO_ELETRONICO_E_MONITORADOR:
			return null;
		case OMEGA_RASTREADOR_E_MONITORADOR:
			return new int[] { ServiceTiraFoto.ID_INTERNO, ServiceTiraFoto.ID_EXTERNO, ServiceMyPosition.ID };
		case OMEGA_CATALOGO_ONLINE_CLIENTE:
			return null;
		default:
			return null;
		}
	}

	// Servicos que ocorrem internamente para o funcionamento do app, sem a
	// checagem da existencia de permissao ou nao
	public static int[] getVetorIdServicoInterno() {
		switch (OmegaConfiguration.PROGRAM) {
		case OMEGA_FACTU:
			return new int[] { ServiceSendFileToServer.ID };
		case OMEGA_VET_CLINICAL:
			return new int[] { ServiceSendFileToServer.ID };
		case OMEGA_CATALOGO_OFFLINE:
			return null;
		case OMEGA_EMPRESA:
			return null;
		// return new int[] {ServiceSendFileToServer.ID};
		// return new int[] {ServiceSendTarefa.ID};
		case OMEGA_CATOLOGO_ONLINE:
			return null;
		case OMEGA_MONITORADOR:
			return null;
		case OMEGA_PONTO_ELETRONICO:
			return null;
		case OMEGA_RASTREADOR:
			return null;
		case OMEGA_PONTO_ELETRONICO_E_MONITORADOR:
			return null;
		case OMEGA_RASTREADOR_E_MONITORADOR:
			return null;
		case OMEGA_CATALOGO_ONLINE_CLIENTE:
			return null;
		default:
			return null;
		}
	}

}
