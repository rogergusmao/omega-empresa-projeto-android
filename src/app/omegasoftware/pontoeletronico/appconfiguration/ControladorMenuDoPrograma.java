package app.omegasoftware.pontoeletronico.appconfiguration;

import java.util.ArrayList;

import android.content.Context;
import app.omegasoftware.pontoeletronico.appconfiguration.MenuDoProgramaFactu.PERMISSAO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;

public class ControladorMenuDoPrograma {

	
	Context context;
	
	MenuDoProgramaFactu menu = new MenuDoProgramaFactu();
	ArrayList<Integer> path = new ArrayList<Integer>();
	ArrayList<Integer> pathAba = new ArrayList<Integer>();
	
	static ControladorMenuDoPrograma singletonControlador;
	
	public static ControladorMenuDoPrograma inicializaControlador(Context context){
		if(singletonControlador  == null) 
			 singletonControlador = new ControladorMenuDoPrograma(context);
		return singletonControlador;
	}
	
	public static void logout(){
	
		singletonControlador.clearPath();
	}
	
	public void clearPath(){
		if(path != null) path.clear();
		if(pathAba != null) pathAba.clear();
	}
	
	public static ControladorMenuDoPrograma getControladorMenuPrograma(Context context){
		 
		
		 return singletonControlador;
	}
	
	public String getTituloDaTelaAtual(){
		PERMISSAO[] permissoes = MenuDoProgramaFactu.PERMISSAO.values();
		Integer idPermissao = getIdPermissaoTelaAtual();
		if(permissoes != null && permissoes.length > 0 ){
			for(int i = 0 ; i < permissoes.length; i++){
				if(permissoes[i].item.id == idPermissao){
					return permissoes[i].item.nome;
				}
			}
		}
		return null;
	}
	
	private ControladorMenuDoPrograma(Context context){
		this.context = context;
		//getHashItens();
		menu.getArvoreMenu();
		
		path.add(ConstantesMenuPrograma.ID_RAIZ);
		pathAba.add(0);
	}
	
	
	
	public ItemMenu[] voltarParaAPaginaAncestral(){
		if(path.size() > 1){
			pathAba.remove(path.size() - 1);
			//retira o ultimo
			path.remove(path.size() - 1);
			return getVetorItensMenu(path.get(path.size() - 1), false);
		}
		else if(path.size() == 1){
			
			ItemMenu[] vetor = getVetorItensMenu(path.get(0), false);
			
			return vetor;
		} else {
			//Ja esta na raiz
			return null;
		}
	}

	public ItemMenu[] getVetorItensAtual(){
		ItemMenu[] vetor = getVetorItensMenu(path.get(path.size() - 1), false);
		return vetor;
	}
	
	
	
	public Integer getIdPermissaoTelaAtual(){
		return path.get(path.size() - 1);
	}
	
	public Integer getIdAbaAtual(){
		return pathAba.get(pathAba.size() - 1);
	}
	
	
	public ItemMenu[] getVetorItensRaiz(){
		ItemMenu[] vetor = getVetorItensMenu(path.get(0), false);
		return vetor;
	}


	
	public ItemMenu[] avancaParaAPagina(Integer idPermissaoSelecionada){
		return getVetorItensMenu(idPermissaoSelecionada, true);
	}
	
	public void atualizaValorDaAbaAtual(int aba){
		pathAba.remove(pathAba.size() - 1);
		pathAba.add(aba);
	}
	
	public Class<?> getClasseAjudaDaTelaAtual(){
		Integer idPermissao = getIdPermissaoTelaAtual();
		if(idPermissao != null){
			PERMISSAO p = getPermissao(idPermissao);
			if(p != null){
				return p.item.classeAjuda;
			}	
		}
		 
		return null;
	}
	
	public PERMISSAO getPermissao(int idPermissao){
		
		PERMISSAO[] vetor = PERMISSAO.values();
		for(int i = 0 ; i < vetor.length; i++){
			if(vetor[i].item.id == idPermissao)
				return vetor[i];
		}
		return null;
	}
	
	private ItemMenu[] getVetorItensMenu(Integer idPermissaoSelecionada, boolean adicionarAoPath){
		PERMISSAO[] permissoes = menu.getArvoreMenu().get(idPermissaoSelecionada);
		ArrayList<ItemMenu> itens = new ArrayList<ItemMenu>(); 
		
		
		if(adicionarAoPath){
			path.add(idPermissaoSelecionada);
			pathAba.add(0);
		}
		
		for(int i = 0 ; i < permissoes.length; i++){
			if(OmegaSecurity.usuarioPossuiPermissao(permissoes[i].item.id))
			itens.add(permissoes[i].item);
		}
			
		
		ItemMenu[] retItens = new ItemMenu[itens.size()];
		itens.toArray(retItens);
		return retItens;
	}
	
	
	
	
}
