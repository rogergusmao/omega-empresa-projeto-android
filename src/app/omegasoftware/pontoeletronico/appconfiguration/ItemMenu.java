package app.omegasoftware.pontoeletronico.appconfiguration;

import app.omegasoftware.pontoeletronico.appconfiguration.ConstantesMenuPrograma.TIPO_PERMISSAO;

public class ItemMenu {
	 final public int id;
     final public int drawable[];
     final public String nome;
     final public Class<?> classe;
     final public String tag;
     final public Integer rIdStringNome;
     final public TIPO_PERMISSAO tipoPermissao;
     
     public ConstantesMenuPrograma.TIPO_DIMENSAO_IMAGEM tipoDimensaoImagem;
     public int drawableMenu;
     
     public Class<?> classeAjuda;
	public ItemMenu(
			ConstantesMenuPrograma.TIPO_PERMISSAO tipoPermissao,
			int id,
			int drawable[],
			String nome,
			Class<?> classe,
			String tag,
			Integer rIdStringNome) {
		this.id = id;
		this.drawable = drawable;
		this.nome = nome;
		this.classe = classe;
		this.tag = tag;
		this.rIdStringNome =rIdStringNome; 

		this.tipoPermissao = tipoPermissao;
	}
	
	public Integer getIdDrawable(){
		
		if(drawable == null || drawable.length == 0 ) return null;
		else if (tipoDimensaoImagem == null) return null;
		if(tipoDimensaoImagem.id > drawable.length) return drawable[drawable.length - 1];
		else return drawable[tipoDimensaoImagem.id];
	}
}
