

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  MenuDoPrograma
    * DATA DE GERACAO: 20.10.2013
    * ARQUIVO:         MenuDoPrograma.java
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.appconfiguration;

    import java.util.HashMap;

import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.AboutActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.DirigirVeiculoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.HistoricoSincronizadorActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.MenuServicoOnlineUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.PontoEletronicoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.RastrearUsuariosActivity;
import app.omegasoftware.pontoeletronico.TabletActivities.RelogioDePontoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.SynchronizeReceiveActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.TutorialPontoEletronicoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailRotinaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterBairroActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterCidadeActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterCorporacaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterEstadoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterModeloActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterPaisActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterProfissaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterRelatorioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterTipoDocumentoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterTipoEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterVeiculoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormBairroActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormCategoriaPermissaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormCidadeActivityMobile;
//import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormCorporacaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEstadoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormModeloActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPaisActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormProfissaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormRedeActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormRelatorioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormRotinaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormTarefaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormTipoDocumentoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormTipoEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormVeiculoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormVeiculoUsuarioActivityMobile;
    import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListPessoaEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListRedeActivityMobile;
    //import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListTarefaMinhaActivityMobile;
    import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListUltimosPontosActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListVeiculoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectCategoriaPermissaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectServicoHistoricoMonitoramentoRemotoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectServicoMonitoramentoRemotoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectServicoOnlineGrupoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectVeiculoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.common.authenticator.AlterarSenhaUsuarioActivityMobile;
    import app.omegasoftware.pontoeletronico.task.ExpList3;
    import app.omegasoftware.pontoeletronico.task.TaskListActivity;


    // **********************
    // DECLARACAO DA CLASSE
    // **********************

	
    
    public class MenuDoProgramaFactu {

    	
    	
    	
    
	public enum PERMISSAO {
                
            ServicePontoEletronicoActivityMobile  (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS,
                    1,
                    new int [] {R.drawable.ponto_eletronico_peq, R.drawable.ponto_eletronico_med, R.drawable.ponto_eletronico_grd},
                    "Relogio de Ponto",
                    null,
                    "ServicePontoEletronicoActivityMobile",
                    null,
                    TutorialPontoEletronicoActivityMobile.class), 
                         
//            FilterMinhasTarefasActivityMobile (
//                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
//                    104,
//                    null,
//                    "Minhas Tarefas",
//                    FilterMinhasTarefasActivityMobile.class,
//                    "FilterMinhasTarefasActivityMobile",
//                    null ), 
                         
//            ServiceRelatorioActivityMobile (
//                    ConstantesMenuPrograma.TIPO_PERMISSAO.MENU_COM_ICONE_DOS_FILHOS_DIFERENCIADA,
//                    110,
//                    new int[] {R.drawable.tarefa_peq, R.drawable.tarefa_med, R.drawable.tarefa_grd},
//                    "Relatorio",
//                    null,
//                    "ServiceRelatorioActivityMobile",
//                    null ), 
                ServiceRelatorioActivityMobile (
                		ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                        10,
                        null,
                        "Ultimas noticias",
                        HistoricoSincronizadorActivityMobile.class,
                        "HistoricoSincronizadorActivityMobile",
                        null ), 
            AboutActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    10,
                    null,
                    "Sobre",
                    AboutActivityMobile.class,
                    "AboutActivityMobile",
                    null ), 
                        
            ConfigurationActivityMobile  (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS,
                    9,
                    new int [] {R.drawable.configuracoes_peq, R.drawable.configuracoes_med, R.drawable.configuracoes_grd},
                    "Configuracao",
                    null,
                    "ConfigurationActivityMobile",
                    null ), 
                        
            ServicePermissaoActivityMobile  (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS,
                    8,
                    new int [] {R.drawable.permissoes_peq, R.drawable.permissoes_med, R.drawable.permissoes_grd},
                    "Permissoes",
                    null,
                    "ServicePermissaoActivityMobile",
                    null ), 
                        
            ServiceOnlineActivityMobile  (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS,
                    7,
                    new int [] {R.drawable.servicos_online_peq, R.drawable.servicos_online_med, R.drawable.servicos_online_grd},
                    "Servicos",
                    null,
                    "ServiceOnlineActivityMobile",
                    null ), 
                        
//            ServiceRastrearActivityMobile  (
//                    ConstantesMenuPrograma.TIPO_PERMISSAO.MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS,
//                    2,
//                    null,
//                    "Rastreamento",
//                    null,
//                    "ServiceRastrearActivityMobile",
//                    null ),
            ServiceRastrearActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    2,
                    new int[] {R.drawable.menu_map, R.drawable.menu_map, R.drawable.menu_map},
                    "Rastreamento",
                    RastrearUsuariosActivity.class,
                    "FilterPontoActivityMobile",
                    null ), 
                         
                        
            ServiceCadastroActivityMobile  (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS,
                    3,
                    new int [] {R.drawable.cadastros_peq, R.drawable.cadastros_med, R.drawable.cadastros_grd},
                    "Cadastros",
                    null,
                    "ServiceCadastroActivityMobile",
                    null ), 
                        
            ServiceCatalogoActivityMobile  (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS,
                    4,
                    new int [] {R.drawable.catalogos_peq, R.drawable.catalogos_med, R.drawable.catalogos_grd},
                    "Catalogos",
                    null,
                    "ServiceCatalogoActivityMobile",
                    null ), 
                        
            ServiceMonitoramentoRemotoActivityMobile  (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS,
                    5,
                    null,
                    "Monitoramento Remoto",
                    null,
                    "ServiceMonitoramentoRemotoActivityMobile",
                    null ), 
                         
//            ListMeuPontoActivityMobile (
//                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
//                    108,
//                    null,
//                    "Meus Pontos Batidos",
//                    ListMeuPontoActivityMobile.class,
//                    "ListMeuPontoActivityMobile",
//                    null ), 
                         
            FilterPontoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    109,
                    new int[] {R.drawable.catalogos_peq, R.drawable.catalogos_med, R.drawable.catalogos_grd},
                    "Pontos marcados nesse aparelho",
                    ListUltimosPontosActivityMobile.class,
                    "FilterPontoActivityMobile",
                    null ), 
                         
            TutorialPontoEletronicoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    103,
                    new int [] {R.drawable.sobre_peq, R.drawable.sobre_med, R.drawable.sobre_grd},
                    "Tutorial",
                    TutorialPontoEletronicoActivityMobile.class,
                    "TutorialPontoEletronicoActivityMobile",
                    null ), 
                         
//            SincronizacaoPontoActivityMobile (
//                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
//                    102,
//                    new int [] {R.drawable.sincronizacao_peq, R.drawable.sincronizacao_med, R.drawable.sincronizacao_grd},
//                    "Sinc. Ponto",
//                    NAOUTILIZADASincronizacaoPontoActivityMobile.class,
//                    "SincronizacaoPontoActivityMobile",
//                    null ), 
                         
            FilterPessoaEmpresaActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    101,
                    new int [] {R.drawable.catalogos_peq, R.drawable.catalogos_med, R.drawable.catalogos_grd},
                     "Catalogo de Pessoas e Usuarios",
                    //FilterPessoaEmpresaActivityMobile.class,
                     ListPessoaEmpresaActivityMobile.class,
                    "FilterPessoaEmpresaActivityMobile",
                    null ), 
            
		DetailRotinaActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    113,
                    new int [] {R.drawable.minha_rotina_peq, R.drawable.minha_rotina_med, R.drawable.minha_rotina_grd},
                    "Minha Rotina",
                    DetailRotinaActivityMobile.class,
                    //ListPessoaEmpresaRotinaActivityMobile.class,
                    "DetailRotinaActivityMobile",
                    null ), 
                    
            ListPessoaEmpresaRotinaActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    107,
                    new int [] {R.drawable.minha_rotina_peq, R.drawable.minha_rotina_med, R.drawable.minha_rotina_grd},
                    "Rotina dos usuarios",
                    FormRotinaActivityMobile.class,
                    //ListPessoaEmpresaRotinaActivityMobile.class,
                    "ListPessoaEmpresaRotinaActivityMobile",
                    null ), 
                         
            SelectEmpresaPontoEletronicoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    11,
                    new int[] {R.drawable.teclado_peq, R.drawable.teclado_med, R.drawable.teclado_grd},
                    "Relogio de Ponto",
                    RelogioDePontoActivityMobile.class,
                    "SelectEmpresaPontoEletronicoActivityMobile",
                    null ), 
                         
            PontoEletronicoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    12,
                    new int[] {R.drawable.leitor_qr_code_peq, R.drawable.leitor_qr_code_med, R.drawable.leitor_qr_code_grd},
                    "Leitor QR Code",
                    PontoEletronicoActivityMobile.class,
                    "PontoEletronicoActivityMobile",
                    null ), 
                         
            SelectUsuarioPosicaoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    13,
                    null,
                    "Historico de Rota",
                    null, //SelectUsuarioPosicaoActivityMobile.class,
                    "SelectUsuarioPosicaoActivityMobile",
                    null ), 
                         
            SelectVeiculoUsuarioActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    14,
                    null,
                    "Monitorar",
                    SelectVeiculoUsuarioActivityMobile.class,
                    "SelectVeiculoUsuarioActivityMobile",
                    null ), 
                         
            DirigirVeiculoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    80,
                    null,
                    "Dirigir Veiculo",
                    DirigirVeiculoActivityMobile.class,
                    "DirigirVeiculoActivityMobile",
                    null ), 
                         
            FormVeiculoUsuarioActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    19,
                    new int [] {R.drawable.usuario_veiculo_peq, R.drawable.usuario_veiculo_med, R.drawable.usuario_veiculo_grd},
                    "Usuario do Veiculo",
                    FormVeiculoUsuarioActivityMobile.class,
                    "FormVeiculoUsuarioActivityMobile",
                    null ), 
                         
            FormEmpresaActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    15,
                    new int [] {R.drawable.empresa_peq, R.drawable.empresa_med, R.drawable.empresa_grd},
                    "Empresa",
                    FormEmpresaActivityMobile.class,
                    "FormEmpresaActivityMobile",
                    null ), 
                         
            FormPessoaActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    16,
                    new int [] {R.drawable.pessoa_peq, R.drawable.pessoa_med, R.drawable.pessoa_grd},
                    "Pessoa",
                    FormPessoaActivityMobile.class,
                    "FormPessoaActivityMobile",
                    null ), 
                         
            FormTarefaActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    17,
                    new int[]{R.drawable.tarefa_peq, R.drawable.tarefa_med, R.drawable.tarefa_grd},
                    "Tarefa",
                    FormTarefaActivityMobile.class,
                    "FormTarefaActivityMobile",
                    null ), 
                         
            FormRedeActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    106,
                    new int[]{R.drawable.rede_peq, R.drawable.rede_med, R.drawable.rede_grd},
                    "Rede",
                    FormRedeActivityMobile.class,
                    "FormRedeActivityMobile",
                    null ), 
                        
            ServiceCadastroGeralActivityMobile  (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS,
                    74,
                    new int[]{R.drawable.cadastros_peq, R.drawable.cadastros_med, R.drawable.cadastros_grd},
                    "Geral",
                    null,
                    "ServiceCadastroGeralActivityMobile",
                    null ), 
                         
            FormUsuarioActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    20,
                    new int[]{R.drawable.usuario_peq, R.drawable.usuario_med, R.drawable.usuario_grd},
                    "Usuario",
                    FormUsuarioActivityMobile.class,
                    "FormUsuarioActivityMobile",
                    null ), 
                         
            FormVeiculoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    18,
                    new int[]{R.drawable.carro_peq, R.drawable.carro_med, R.drawable.carro_grd},
                    "Veiculo",
                    FormVeiculoActivityMobile.class,
                    "FormVeiculoActivityMobile",
                    null ), 
                        
            ServiceCatalogoGeralActivityMobile  (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.MENU_COM_ICONE_DO_PAI_REPETIDA_NOS_FILHOS,
                    75,
                    new int[]{R.drawable.catalogos_peq, R.drawable.catalogos_med, R.drawable.catalogos_grd},
                    "Geral",
                    null,
                    "ServiceCatalogoGeralActivityMobile",
                    null ), 
                         
            FilterRedeActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    105,
                    new int[]{R.drawable.rede_peq, R.drawable.rede_med, R.drawable.rede_grd},
                    "Rede",
                    ListRedeActivityMobile.class,
                    "FilterRedeActivityMobile",
                    null ), 
                         
            FilterVeiculoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    24,
                    new int[]{R.drawable.carro_peq, R.drawable.carro_med, R.drawable.carro_grd},
                    "Veiculo",
                    ListVeiculoActivityMobile.class,
                    "FilterVeiculoActivityMobile",
                    null ), 
                         
            FilterVeiculoUsuarioActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    25,
                    new int[]{R.drawable.usuario_veiculo_peq, R.drawable.usuario_veiculo_med, R.drawable.usuario_veiculo_grd},
                    "Usuario do Veiculo",
                    FilterVeiculoUsuarioActivityMobile.class,
                    "FilterVeiculoUsuarioActivityMobile",
                    null ), 
                         
            FilterUsuarioActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    26,
                    new int[]{R.drawable.usuario_peq, R.drawable.usuario_med, R.drawable.usuario_grd},
                    "Usuario",
                    ListUsuarioActivityMobile.class,
                    "FilterUsuarioActivityMobile",
                    null ), 
                         
            FilterPessoaActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    22,
                    new int[] {R.drawable.pessoa_peq, R.drawable.pessoa_med, R.drawable.pessoa_grd},
                    "Pessoa",
                    ListPessoaActivityMobile.class,
                    "FilterPessoaActivityMobile",
                    null ), 
                         
            FilterTarefaActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    23,
                    new int[] {R.drawable.tarefa_peq, R.drawable.tarefa_med, R.drawable.tarefa_grd},
                    "Tarefa",
                    TaskListActivity.class,
                    "FilterTarefaActivityMobile",
                    null ), 
                         
            FilterEmpresaActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    21,
                    new int[] {R.drawable.empresa_peq, R.drawable.empresa_med, R.drawable.empresa_grd},
                    "Empresa",
                    ListEmpresaActivityMobile.class,
                    "FilterEmpresaActivityMobile",
                    null ), 
                         
            SelectServicoMonitoramentoRemotoUsuarioActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    27,
                    null,
                    "Monitorar Usuario",
                    SelectServicoMonitoramentoRemotoUsuarioActivityMobile.class,
                    "SelectServicoMonitoramentoRemotoUsuarioActivityMobile",
                    null ), 
                         
            SelectServicoHistoricoMonitoramentoRemotoUsuarioActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    69,
                    null,
                    "Historico Monitoramento Usuario",
                    SelectServicoHistoricoMonitoramentoRemotoUsuarioActivityMobile.class,
                    "SelectServicoHistoricoMonitoramentoRemotoUsuarioActivityMobile",
                    null ), 
                         
            MenuServicoOnlineUsuarioActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    78,
                    new int[] {R.drawable.usuario_peq, R.drawable.usuario_med, R.drawable.usuario_grd},
                    "Meus Servicos",
                    MenuServicoOnlineUsuarioActivityMobile.class,
                    "MenuServicoOnlineUsuarioActivityMobile",
                    null ), 
                         
            SelectServicoOnlineGrupoUsuarioActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    79,
                    new int[] {R.drawable.grupo_peq, R.drawable.grupo_med, R.drawable.grupo_grd},
                    "Servico Online Usuario",
                    SelectServicoOnlineGrupoUsuarioActivityMobile.class,
                    "SelectServicoOnlineGrupoUsuarioActivityMobile",
                    null ), 
                         
            FormCategoriaPermissaoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    29,
                    new int[] {R.drawable.cadastros_peq, R.drawable.cadastros_med, R.drawable.cadastros_grd},
                    "Cadastrar Categoria Permissao",
                    FormCategoriaPermissaoActivityMobile.class,
                    "FormCategoriaPermissaoActivityMobile",
                    null ), 
                         
            SelectCategoriaPermissaoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    30,
                    new int[] {R.drawable.catalogos_peq, R.drawable.catalogos_med, R.drawable.catalogos_grd},
                    "Gerenciar Categoria Permissao",
                    SelectCategoriaPermissaoActivityMobile.class,
                    "SelectCategoriaPermissaoActivityMobile",
                    null ), 
                         
            FilterCorporacaoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    77,
                    new int[] {R.drawable.catalogos_peq, R.drawable.catalogos_med, R.drawable.catalogos_grd},
                    "Corporacao",
                    FilterCorporacaoActivityMobile.class,
                    "FilterCorporacaoActivityMobile",
                    null ), 
                         
//            FormCorporacaoActivityMobile (
//                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
//                    76,
//                    new int[] {R.drawable.cadastros_peq, R.drawable.cadastros_med, R.drawable.cadastros_grd},
//                    "Corporacao",
//                    FormCorporacaoActivityMobile.class,
//                    "FormCorporacaoActivityMobile",
//                    null ), 
                         
            AlterarSenhaUsuarioActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    37,
                    new int[] {R.drawable.alterar_senha_peq, R.drawable.alterar_senha_med, R.drawable.alterar_senha_grd},
                    "Alterar Senha",
                    AlterarSenhaUsuarioActivityMobile.class,
                    "AlterarSenhaUsuarioActivityMobile",
                    null ), 
                         
            SynchronizeReceiveActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    68,
                    new int [] {R.drawable.sincronizacao_peq, R.drawable.sincronizacao_med, R.drawable.sincronizacao_grd},
                    "Sincronizar Dados",
                    SynchronizeReceiveActivityMobile.class,
                    "SynchronizeReceiveActivityMobile",
                    null ), 
                         
            RemoveEmpresa (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    54,
                    null,
                    "Remover Empresa",
                    null,
                    "RemoveEmpresa",
                    null ), 
                         
            EditEmpresa (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    45,
                    null,
                    "Editar Empresa",
                    null,
                    "EditEmpresa",
                    null ), 
                         
            RemoveTarefa (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    56,
                    null,
                    "Remover Tarefa",
                    null,
                    "RemoveTarefa",
                    null ), 
                         
            EditTarefa (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    47,
                    null,
                    "Editar Tarefa",
                    null,
                    "EditTarefa",
                    null ), 
                         
            RemoveVeiculo (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    57,
                    null,
                    "Remover Veiculo",
                    null,
                    "RemoveVeiculo",
                    null ), 
                         
            EditVeiculo (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    48,
                    null,
                    "Editar Veiculo",
                    null,
                    "EditVeiculo",
                    null ), 
                         
            RemoveVeiculoUsuario (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    58,
                    null,
                    "Remover Usuario do Veiculo",
                    null,
                    "RemoveVeiculoUsuario",
                    null ), 
                         
            EditVeiculoUsuario (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    49,
                    null,
                    "Editar Usuario do Veiculo",
                    null,
                    "EditVeiculoUsuario",
                    null ), 
                         
            EditUsuario (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    50,
                    null,
                    "Editar Usuario",
                    null,
                    "EditUsuario",
                    null ), 
                         
            RemoveUsuario (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    59,
                    null,
                    "Remover Usuario",
                    null,
                    "RemoveUsuario",
                    null ), 
                         
            EditUf (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    51,
                    null,
                    "Editar Estado",
                    null,
                    "EditUf",
                    null ), 
                         
            RemoveUf (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    60,
                    null,
                    "Remover Estado",
                    null,
                    "RemoveUf",
                    null ), 
                         
            EditCidade (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    52,
                    null,
                    "Editar Cidade",
                    null,
                    "EditCidade",
                    null ), 
                         
            RemoveCidade (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    61,
                    null,
                    "Remover Cidade",
                    null,
                    "RemoveCidade",
                    null ), 
                         
            EditBairro (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    53,
                    null,
                    "Editar Bairro",
                    null,
                    "EditBairro",
                    null ), 
                         
            RemoveBairro (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    62,
                    null,
                    "Remover Bairro",
                    null,
                    "RemoveBairro",
                    null ), 
                         
            RemovePais (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    73,
                    null,
                    "Remover Pais",
                    null,
                    "RemovePais",
                    null ), 
                         
            EditPais (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    72,
                    null,
                    "Editar Pais",
                    null,
                    "EditPais",
                    null ), 
                         
            FormModeloActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    99,
                    null,
                    "Modelo",
                    FormModeloActivityMobile.class,
                    "FormModeloActivityMobile",
                    null ), 
                         
            FormCidadeActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    33,
                    null,
                    "Cidade",
                    FormCidadeActivityMobile.class,
                    "FormCidadeActivityMobile",
                    null ), 
                         
            FormTipoEmpresaActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    94,
                    null,
                    "Tipo Empresa",
                    FormTipoEmpresaActivityMobile.class,
                    "FormTipoEmpresaActivityMobile",
                    null ), 
                         
            FormEstadoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    31,
                    null,
                    "Estado",
                    FormEstadoActivityMobile.class,
                    "FormEstadoActivityMobile",
                    null ), 
                         
            FormTipoDocumentoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    86,
                    null,
                    "Tipo Documento",
                    FormTipoDocumentoActivityMobile.class,
                    "FormTipoDocumentoActivityMobile",
                    null ), 
                         
            FormPaisActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    71,
                    null,
                    "Pais",
                    FormPaisActivityMobile.class,
                    "FormPaisActivityMobile",
                    null ), 
                         
            FormProfissaoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    85,
                    null,
                    "Profissao",
                    FormProfissaoActivityMobile.class,
                    "FormProfissaoActivityMobile",
                    null ), 
                         
            FormBairroActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    35,
                    null,
                    "Bairro",
                    FormBairroActivityMobile.class,
                    "FormBairroActivityMobile",
                    null ), 
                         
            FilterTipoDocumentoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    88,
                    null,
                    "Tipo Documento",
                    FilterTipoDocumentoActivityMobile.class,
                    "FilterTipoDocumentoActivityMobile",
                    null ), 
                         
            FilterBairroActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    36,
                    null,
                    "Bairro",
                    FilterBairroActivityMobile.class,
                    "FilterBairroActivityMobile",
                    null ), 
                         
            FilterModeloActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    100,
                    null,
                    "Modelo",
                    FilterModeloActivityMobile.class,
                    "FilterModeloActivityMobile",
                    null ), 
                         
            FilterCidadeActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    34,
                    null,
                    "Cidade",
                    FilterCidadeActivityMobile.class,
                    "FilterCidadeActivityMobile",
                    null ), 
                         
            FilterProfissaoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    87,
                    null,
                    "Profissao",
                    FilterProfissaoActivityMobile.class,
                    "FilterProfissaoActivityMobile",
                    null ), 
                         
            FilterTipoEmpresaActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    93,
                    null,
                    "Tipo Empresa",
                    FilterTipoEmpresaActivityMobile.class,
                    "FilterTipoEmpresaActivityMobile",
                    null ), 
                         
            FilterEstadoActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    32,
                    null,
                    "Estado",
                    FilterEstadoActivityMobile.class,
                    "FilterEstadoActivityMobile",
                    null ), 
                         
            FilterPaisActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    70,
                    null,
                    "Pais",
                    FilterPaisActivityMobile.class,
                    "FilterPaisActivityMobile",
                    null ), 
                         
            EditProfissao (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    91,
                    null,
                    "Editar Profissao",
                    null,
                    "EditProfissao",
                    null ), 
                         
            RemoveProfissao (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    89,
                    null,
                    "Remover Profissao",
                    null,
                    "RemoveProfissao",
                    null ), 
                         
            EditTipoDocumento (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    92,
                    null,
                    "Editar Tipo Documento",
                    null,
                    "EditTipoDocumento",
                    null ), 
                         
            RemoveTipoDocumento (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    90,
                    null,
                    "Remover Tipo Documento",
                    null,
                    "RemoveTipoDocumento",
                    null ), 
                         
            EditTipoEmpresa (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    96,
                    null,
                    "Editar Tipo Empresa",
                    null,
                    "EditTipoEmpresa",
                    null ), 
                         
            RemoveTipoEmpresa (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_FUNCIONALIDADE_DO_SISTEMA,
                    95,
                    null,
                    "Remover Tipo Empresa",
                    null,
                    "RemoveTipoEmpresa",
                    null ), 
                         
            FilterRelatorioActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    111,
                    new int[] {R.drawable.catalogos_peq, R.drawable.catalogos_med, R.drawable.catalogos_grd},
                    "Catalogo",
                    FilterRelatorioActivityMobile.class,
                    "FilterRelatorioActivityMobile",
                    null ), 
                         
            FormRelatorioActivityMobile (
                    ConstantesMenuPrograma.TIPO_PERMISSAO.ACESSO_A_TELA,
                    112,
                    new int[] {R.drawable.cadastros_peq, R.drawable.cadastros_med, R.drawable.cadastros_grd},
                    "Cadastro",
                    FormRelatorioActivityMobile.class,
                    "FormRelatorioActivityMobile",
                    null )
                         ;
                
                final public ItemMenu item;
		private PERMISSAO(
				ConstantesMenuPrograma.TIPO_PERMISSAO tipoPermissao,
				int id,
				int drawable[],
				String nome,
				Class<?> classe,
				String tag,
				Integer rIdStringNome) {
			item = new ItemMenu(
					tipoPermissao,
					id,
					drawable,
					nome,
					classe,
					tag,
					rIdStringNome);
			
		}
		
		private PERMISSAO(
				ConstantesMenuPrograma.TIPO_PERMISSAO tipoPermissao,
				int id,
				int drawable[],
				String nome,
				Class<?> classe,
				String tag,
				Integer rIdStringNome,
				Class<?> classeAjuda) {
			item = new ItemMenu(
					tipoPermissao,
					id,
					drawable,
					nome,
					classe,
					tag,
					rIdStringNome);
			item.classeAjuda = classeAjuda;
		}
	 
	}    
    // *************************
    // DECLARACAO DE ATRIBUTOS
    // *************************
    
        
	//Armazena a arvore de permissoes
	static HashMap<Integer, PERMISSAO[]> singletonArvoreIdPermissoes ;

        

	public HashMap<Integer, PERMISSAO[]> getArvoreMenu(){
        if(singletonArvoreIdPermissoes != null) return singletonArvoreIdPermissoes;
        singletonArvoreIdPermissoes = new HashMap<Integer, PERMISSAO[]>();
                
                singletonArvoreIdPermissoes.put(ConstantesMenuPrograma.ID_RAIZ, new PERMISSAO [] {
                		//PERMISSAO.ServicePontoEletronicoActivityMobile, 
                		PERMISSAO.SelectEmpresaPontoEletronicoActivityMobile,
                		PERMISSAO.FilterPessoaActivityMobile,
                		PERMISSAO.FilterUsuarioActivityMobile,
                		PERMISSAO.FilterEmpresaActivityMobile,
                		PERMISSAO.ServiceRastrearActivityMobile,
                        PERMISSAO.FilterTarefaActivityMobile
                		//PERMISSAO.FilterRedeActivityMobile,
                		//PERMISSAO.FilterVeiculoActivityMobile,
//                    PERMISSAO.FilterMinhasTarefasActivityMobile, 
//                    PERMISSAO.AboutActivityMobile, 
//                    PERMISSAO.ConfigurationActivityMobile, 
//                    PERMISSAO.ServicePermissaoActivityMobile, 
//                    PERMISSAO.ServiceOnlineActivityMobile, 
//                    PERMISSAO.ServiceCatalogoActivityMobile, 
//                    PERMISSAO.ServiceCadastroActivityMobile,
//                    PERMISSAO.ServiceRelatorioActivityMobile
                    });
                    
                singletonArvoreIdPermissoes.put(1, new PERMISSAO []{ 
//                		PERMISSAO.ListMeuPontoActivityMobile, 
//                    PERMISSAO.FilterPontoActivityMobile, 
//                    PERMISSAO.SincronizacaoPontoActivityMobile,
//                		PERMISSAO.DetailRotinaActivityMobile,
                    PERMISSAO.FilterPessoaEmpresaActivityMobile, 
//                    PERMISSAO.ListPessoaEmpresaRotinaActivityMobile, 
//                    PERMISSAO.PontoEletronicoActivityMobile, 
                    PERMISSAO.SelectEmpresaPontoEletronicoActivityMobile,
//                    PERMISSAO.TutorialPontoEletronicoActivityMobile
                    });
                    
                singletonArvoreIdPermissoes.put(2, new PERMISSAO [] {PERMISSAO.DirigirVeiculoActivityMobile, 
                    PERMISSAO.SelectVeiculoUsuarioActivityMobile, 
                    PERMISSAO.SelectUsuarioPosicaoActivityMobile});
                    
                singletonArvoreIdPermissoes.put(3, new PERMISSAO [] { 
                    PERMISSAO.FormRedeActivityMobile, 
                    PERMISSAO.FormUsuarioActivityMobile, 
                     
                    //PERMISSAO.FormVeiculoActivityMobile, 
                    //PERMISSAO.FormTarefaActivityMobile, 
                    PERMISSAO.FormPessoaActivityMobile, 
                    PERMISSAO.FormEmpresaActivityMobile,
                    PERMISSAO.ServiceCadastroGeralActivityMobile});
                    
                singletonArvoreIdPermissoes.put(4, new PERMISSAO [] {
                	PERMISSAO.FilterRedeActivityMobile,
                    PERMISSAO.FilterUsuarioActivityMobile,
                    PERMISSAO.FilterPessoaActivityMobile,
                    PERMISSAO.FilterEmpresaActivityMobile, 
                    //PERMISSAO.FilterVeiculoUsuarioActivityMobile, 
                    //PERMISSAO.FilterVeiculoActivityMobile, 
                    //PERMISSAO.FilterTarefaActivityMobile,
                    PERMISSAO.ServiceCatalogoGeralActivityMobile});
                    
                singletonArvoreIdPermissoes.put(5, new PERMISSAO [] {PERMISSAO.SelectServicoMonitoramentoRemotoUsuarioActivityMobile, 
                    PERMISSAO.SelectServicoHistoricoMonitoramentoRemotoUsuarioActivityMobile});
                    
                singletonArvoreIdPermissoes.put(7, new PERMISSAO [] {PERMISSAO.SelectServicoOnlineGrupoUsuarioActivityMobile, 
                    PERMISSAO.MenuServicoOnlineUsuarioActivityMobile});
                    
                singletonArvoreIdPermissoes.put(8, new PERMISSAO [] {PERMISSAO.SelectCategoriaPermissaoActivityMobile, 
                    PERMISSAO.FormCategoriaPermissaoActivityMobile});
                    
                singletonArvoreIdPermissoes.put(9, new PERMISSAO [] {PERMISSAO.AlterarSenhaUsuarioActivityMobile, 
                    PERMISSAO.SynchronizeReceiveActivityMobile, 
                    //PERMISSAO.FormCorporacaoActivityMobile, 
                    PERMISSAO.FilterCorporacaoActivityMobile});
                    
                singletonArvoreIdPermissoes.put(74, new PERMISSAO [] {
                		//PERMISSAO.FormModeloActivityMobile, 
                    PERMISSAO.FormTipoEmpresaActivityMobile, 
                    PERMISSAO.FormCidadeActivityMobile, 
                    PERMISSAO.FormBairroActivityMobile, 
                    PERMISSAO.FormPaisActivityMobile, 
                    PERMISSAO.FormTipoDocumentoActivityMobile, 
                    PERMISSAO.FormProfissaoActivityMobile, 
                    PERMISSAO.FormEstadoActivityMobile});
                    
                singletonArvoreIdPermissoes.put(75, new PERMISSAO [] {PERMISSAO.FilterProfissaoActivityMobile, 
                    //PERMISSAO.FilterModeloActivityMobile, 
                    PERMISSAO.FilterTipoEmpresaActivityMobile, 
                    PERMISSAO.FilterTipoDocumentoActivityMobile, 
                    PERMISSAO.FilterCidadeActivityMobile, 
                    PERMISSAO.FilterEstadoActivityMobile, 
                    PERMISSAO.FilterPaisActivityMobile, 
                    PERMISSAO.FilterBairroActivityMobile});
                
                singletonArvoreIdPermissoes.put(110, new PERMISSAO [] {PERMISSAO.FilterRelatorioActivityMobile, 
                        PERMISSAO.FormRelatorioActivityMobile});
                
                    
		return singletonArvoreIdPermissoes;
	}
	
	
	public HashMap<Integer, PERMISSAO[]> bkpGetArvoreMenu(){
        if(singletonArvoreIdPermissoes != null) return singletonArvoreIdPermissoes;
        singletonArvoreIdPermissoes = new HashMap<Integer, PERMISSAO[]>();
                
                singletonArvoreIdPermissoes.put(ConstantesMenuPrograma.ID_RAIZ, new PERMISSAO [] {
//                		PERMISSAO.ServicePontoEletronicoActivityMobile,
                		PERMISSAO.SelectEmpresaPontoEletronicoActivityMobile,
//                    PERMISSAO.FilterMinhasTarefasActivityMobile, 
//                    PERMISSAO.AboutActivityMobile, 
                    PERMISSAO.ConfigurationActivityMobile, 
                    PERMISSAO.ServicePermissaoActivityMobile, 
                    PERMISSAO.ServiceOnlineActivityMobile, 
                    PERMISSAO.ServiceCatalogoActivityMobile, 
                    PERMISSAO.ServiceCadastroActivityMobile,
                    PERMISSAO.ServiceRelatorioActivityMobile});
                    
                singletonArvoreIdPermissoes.put(1, new PERMISSAO []{ 
//                		PERMISSAO.ListMeuPontoActivityMobile, 
                    PERMISSAO.FilterPontoActivityMobile, 
//                    PERMISSAO.SincronizacaoPontoActivityMobile, 
                    PERMISSAO.FilterPessoaEmpresaActivityMobile, 
                    PERMISSAO.ListPessoaEmpresaRotinaActivityMobile, 
                    //PERMISSAO.PontoEletronicoActivityMobile, 
                    PERMISSAO.SelectEmpresaPontoEletronicoActivityMobile,
                    PERMISSAO.TutorialPontoEletronicoActivityMobile});
                    
                singletonArvoreIdPermissoes.put(2, new PERMISSAO [] {PERMISSAO.DirigirVeiculoActivityMobile, 
                    PERMISSAO.SelectVeiculoUsuarioActivityMobile, 
                    PERMISSAO.SelectUsuarioPosicaoActivityMobile});
                    
                singletonArvoreIdPermissoes.put(3, new PERMISSAO [] { 
                    PERMISSAO.FormRedeActivityMobile, 
                    PERMISSAO.FormUsuarioActivityMobile, 
                    //PERMISSAO.FormVeiculoUsuarioActivityMobile, 
                    //PERMISSAO.FormVeiculoActivityMobile, 
                    //PERMISSAO.FormTarefaActivityMobile, 
                    PERMISSAO.FormPessoaActivityMobile, 
                    PERMISSAO.FormEmpresaActivityMobile,
                    PERMISSAO.ServiceCadastroGeralActivityMobile});
                    
                singletonArvoreIdPermissoes.put(4, new PERMISSAO [] {
                		PERMISSAO.FilterRedeActivityMobile,
                    PERMISSAO.FilterUsuarioActivityMobile,
                    PERMISSAO.FilterPessoaActivityMobile,
                    PERMISSAO.FilterEmpresaActivityMobile, 
                    //PERMISSAO.FilterVeiculoUsuarioActivityMobile, 
                    //PERMISSAO.FilterVeiculoActivityMobile, 
                    //PERMISSAO.FilterTarefaActivityMobile,
                    PERMISSAO.ServiceCatalogoGeralActivityMobile});
                    
                singletonArvoreIdPermissoes.put(5, new PERMISSAO [] {PERMISSAO.SelectServicoMonitoramentoRemotoUsuarioActivityMobile, 
                    PERMISSAO.SelectServicoHistoricoMonitoramentoRemotoUsuarioActivityMobile});
                    
                singletonArvoreIdPermissoes.put(7, new PERMISSAO [] {PERMISSAO.SelectServicoOnlineGrupoUsuarioActivityMobile, 
                    PERMISSAO.MenuServicoOnlineUsuarioActivityMobile});
                    
                singletonArvoreIdPermissoes.put(8, new PERMISSAO [] {PERMISSAO.SelectCategoriaPermissaoActivityMobile, 
                    PERMISSAO.FormCategoriaPermissaoActivityMobile});
                    
                singletonArvoreIdPermissoes.put(9, new PERMISSAO [] {PERMISSAO.AlterarSenhaUsuarioActivityMobile, 
                    PERMISSAO.SynchronizeReceiveActivityMobile, 
                    //PERMISSAO.FormCorporacaoActivityMobile, 
                    PERMISSAO.FilterCorporacaoActivityMobile});
                    
                singletonArvoreIdPermissoes.put(74, new PERMISSAO [] {
                		//PERMISSAO.FormModeloActivityMobile, 
                    PERMISSAO.FormTipoEmpresaActivityMobile, 
                    PERMISSAO.FormCidadeActivityMobile, 
                    PERMISSAO.FormBairroActivityMobile, 
                    PERMISSAO.FormPaisActivityMobile, 
                    PERMISSAO.FormTipoDocumentoActivityMobile, 
                    PERMISSAO.FormProfissaoActivityMobile, 
                    PERMISSAO.FormEstadoActivityMobile});
                    
                singletonArvoreIdPermissoes.put(75, new PERMISSAO [] {PERMISSAO.FilterProfissaoActivityMobile, 
                    //PERMISSAO.FilterModeloActivityMobile, 
                    PERMISSAO.FilterTipoEmpresaActivityMobile, 
                    PERMISSAO.FilterTipoDocumentoActivityMobile, 
                    PERMISSAO.FilterCidadeActivityMobile, 
                    PERMISSAO.FilterEstadoActivityMobile, 
                    PERMISSAO.FilterPaisActivityMobile, 
                    PERMISSAO.FilterBairroActivityMobile});
                
                singletonArvoreIdPermissoes.put(110, new PERMISSAO [] {PERMISSAO.FilterRelatorioActivityMobile, 
                        PERMISSAO.FormRelatorioActivityMobile});
                
                    
		return singletonArvoreIdPermissoes;
	}
    

    } // classe: fim
    

    