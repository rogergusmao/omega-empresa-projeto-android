package app.omegasoftware.pontoeletronico.appconfiguration;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.http.HelperHttp;
import app.omegasoftware.pontoeletronico.service.ServiceMyPosition;
import app.omegasoftware.pontoeletronico.service.ServiceTiraFoto;

public class OmegaConfiguration {

	public static final boolean DEBUGGING = false;

	//TODO PRODUCAO
	public static String URL_SINCRONIZADOR = "http://sync.workoffline.com.br";


	//public static String URL_SINCRONIZADOR = "http://192.168.1.102/SincronizadorWeb/SW10002Corporacao";

	public static String URL_COBRANCA = "http://my.workoffline.com.br/";
	//TODO PRODUCAO
	public  static String URL_PONTO_ELETRONICO = "http://beta.workoffline.com.br";


	//public static String URL_PONTO_ELETRONICO = "http://192.168.1.102/pontoeletroniconovo/PE10003Corporacao/modelo";

	public static String URL_BIBLIOTECA_NUVEM = "http://cloud.workoffline.com.br";
	//public static String URL_BIBLIOTECA_NUVEM = "http://192.168.1.102/bibliotecanuvem/BN10003Corporacao";


	// public enum TYPE_PROGRAM {
	// OMEGA_RASTREADOR, //3 dolar
	// OMEGA_MONITORADOR, //3 dolar
	// OMEGA_RASTREADOR_E_MONITORADOR, //6 dolar
	// OMEGA_PONTO_ELETRONICO, //3 dolar
	// OMEGA_PONTO_ELETRONICO_E_MONITORADOR, //6 dolar
	// OMEGA_CATOLOGO_ONLINE, //2 dolar
	// OMEGA_CATALOGO_OFFLINE, //free
	// OMEGA_CATALOGO_ONLINE_CLIENTE}; //free
	//
	public static final long TEMPO_MINIMO_PARA_CHAMADA_HTTP_COM_ERRO_CONSECUTIVA_MIN = 2;


	public static final boolean DEBUGGING_SINCRONIZACAO_VERBOSO = false;
	public static final boolean DEBUGGING_SINCRONIZACAO = true;
	public static final boolean DEBUGGING_DATABASE_SQLITE= true;
	public static final boolean DEBUGGING_HTTP = true;
	public static final boolean DEBUGGING_ERROR = true;

	public static final boolean DEBUGGING_WARNING = true;
	public static final boolean DEBUGGING_LOGCAT = false;
	public static final boolean DEBUGGING_TODOREMOVER = false;
	//Deseja apagar o log periodicamente?
	public static final boolean CLEAR_LOG = !OmegaConfiguration.DEBUGGING;
	
	public static final boolean CRIPT_LOG_DATA = false;

	public static final boolean GRAVAR_LOG_MODO_PRIVATE = !OmegaConfiguration.DEBUGGING;

	public static final boolean GRAVAR_LOG_SSD = OmegaConfiguration.DEBUGGING;
	
	//Operacoes de Sistemas vindas da biblioteca nuvem
	public static final boolean DEBUGGING_BIBLIOTECA_NUVEM = false;
	public static boolean DEBUG_RESETAR_BANCO_QUANDO_OCORRER_ERROR_DE_SINC = false;

	public static String DEBUGGING_EMAIL_USUARIO = "";
	public static String DEBUGGING_CORPORACAO = "";

	// public static String DEBUGGING_EMAIL_USUARIO = "rogerfsg6@gmail.com";
	// public static String DEBUGGING_CORPORACAO = "TESTE6";
	public static String DEBUGGING_SENHA = "";

	public static boolean DEBUGGING_GPS_OBRIGATORIO_PARA_PONTO = false;
	// public static Class<?> DEBUGGING_ACTIVITY = MenuActivityMobile.class;
	public static Class<?> DEBUGGING_ACTIVITY = null;
	public static Class<?> DEBUGGING_SERVICE = null;
	public static boolean TODAS_AS_TABELAS_POSSUI_ID_COMO_CHAVE = true;
	
	public static int TIMOUT_HTTP_MILISGUNDOS = 5000;
	public static int MAX_TENTIVAS_HTTP = 3;
	public static int MAX_TEMPO_ESPERA_CHAMADA_CONSECUTIVA_MILISEGUNDOS = 5000;
	public static int HTTP_TIMEOUT_MILISEGUNDOS = 30000;
	public static ContainerPrograma.TYPE_PROGRAM PROGRAM = ContainerPrograma.TYPE_PROGRAM.OMEGA_EMPRESA;
	/**
	 * Authtoken type string.
	 */
	
	String base64EncodedPublicKey = "PUT YOUR PUBLIC KEY HERE";


	public static String SEARCH_LOG_CATEGORY = "SEARCH_LOG_CATEGORY";
	public static final int STATE_INITIALIZE_DATABASE = 8;
	public static final int STATE_AUTHENTICATION = 9;
	public static final int STATE_INITIALIZE_PONTO_ELETRONICO = 10;
	public static final int STATE_ATUALIZAR_VERSAO = -1234;
	
	
	public static final int STATE_TROCAR_CORPORACAO = 1112;
	public static final int STATE_LEMBRAR_SENHA = 11165;
	public static final int STATE_RESETAR = 113;
	public static final int STATE_TESTE = 114;
	public static final int STATE_SELECIONAR_CORPORACAO = 115;
	public static final int STATE_OFFLINE = 116;
	
	public static final int STATE_BATIDA_PONTO_ENTRADA_WIDGET = 117;
	public static final int STATE_BATIDA_PONTO_SAIDA_WIDGET = 118;
	public static final int STATE_LOGIN_WIDGET = 119;

	
	//private static final String TAG = "OmegaConfiguration";

	public static final String STR_VETOR_SERVICO_LIBERADO = ServiceTiraFoto.ID_EXTERNO + ","
			+ ServiceTiraFoto.ID_EXTERNO + "," + ServiceMyPosition.ID;




	public static String getLocalUrl() {
		return getLocalUrl(null);
	}

//	public static void clearUrl() {
//		URL_PONTO_ELETRONICO = null;
//	}

	private static synchronized String calculateUrl(Context c) {
		
		if (URL_PONTO_ELETRONICO != null)
			return URL_PONTO_ELETRONICO;
		String token = "";
		for (int i = 100; i < 105; i++) {
			// token = "http://192.168.1."+String.valueOf(i)+":8080";
			token = "http://192.168.1." + String.valueOf(i);
			if (HelperHttp.isUrlAtiva(c, token)) {
				URL_PONTO_ELETRONICO = token;
				break;
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}

		}
		return URL_PONTO_ELETRONICO;
	}

	public static String getLocalUrl(Context c) {
		// return "http://192.168.1.100:8080";
		if (URL_PONTO_ELETRONICO == null) {
			URL_PONTO_ELETRONICO = calculateUrl(c);
		}
		return URL_PONTO_ELETRONICO;

		// if(URL_PONTO_ELETRONICO == null){
		// InetAddress[] ipAddress;
		// try {
		// ipAddress = InetAddress.getAllByName("home-PC");
		// if(ipAddress != null){
		// for(int i = 0 ; i < ipAddress.length; i++){
		// String teste = ipAddress.toString();
		// URL_PONTO_ELETRONICO = "http://" + teste + ":8080";
		// int k =0;
		// k++;
		// }
		// }
		// } catch (UnknownHostException e) {
		//
		// e.printStackTrace();
		// }
		//
		// }
		// return URL_PONTO_ELETRONICO;
	}

//	public static final String BASE_URL_PONTO_ELETRONICO_FIRST_SYNC() {
//		//return getLocalUrl() + "/PontoEletronicoNovo/PE10003Corporacao/modelo/";
//		return "http://workoffline.com.br/";
//	}

	// public static final String BASE_URL_PONTO_ELETRONICO_FIRST_SYNC =
	// "http://192.168.43.37:8080/PontoEletronico/10001/HospedagemCompartilhada/";
	// public static final String BASE_URL_PONTO_ELETRONICO_FIRST_SYNC =
	// "http://pontoeletronico.empresa.omegasoftware.com.br/";
	public static final String BASE_URL_PONTO_ELETRONICO() {
		//return getLocalUrl() + "/PontoEletronicoNovo/PE10003Corporacao/modelo/";
		return OmegaConfiguration.URL_PONTO_ELETRONICO + "/";
	}
	// public static final String BASE_URL_PONTO_ELETRONICO =
	// "http://192.168.43.37:8080/PontoEletronico/10001/HospedagemCompartilhada/";
	// public static final String BASE_URL_PONTO_ELETRONICO =
	// "http://pontoeletronico.empresa.omegasoftware.com.br/";

	// public static final String BASE_URL_OMEGA_SOFTWARE =
	// "http://www.omegasoftware.com.br/site/";
	public static final String BASE_URL_OMEGA_SOFTWARE() {

		return OmegaConfiguration.URL_COBRANCA;

	}


	// public static final String BASE_U RL_SINCRONIZADOR_EMPRESA =
	// "http://sincronizador.empresa.omegasoftware.com.br/adm/webservice.php?class=Servicos_web&action=";
	public static final String BASE_URL_SINCRONIZADOR_EMPRESA() {
		//return getLocalUrl() + "/SincronizadorWeb/SW10002Corporacao/adm/webservice.php?class=Servicos_web&action=";
		return URL_SINCRONIZADOR + "/adm/webservice.php?class=Servicos_web&action=";
	}

	public static final String BASE_URL_SINCRONIZADOR_EMPRESA_DOWNLOAD() {
		//return getLocalUrl() + "/SincronizadorWeb/SW10002Corporacao/adm/download.php?class=Servicos_web&action=";
		return URL_SINCRONIZADOR + "/adm/download.php?class=Servicos_web&action=";
	}

	public static String URL_BASE_WEBSERVICE_PONTO_ELETRONICO() {
		return BASE_URL_PONTO_ELETRONICO() + "adm/webservice.php";
	}

	public static String URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO() {
		return BASE_URL_PONTO_ELETRONICO() + "adm/webservice_actions.php";
	}

	public static String URL_BASE_DOWNLOAD_PONTO_ELETRONICO() {
		return BASE_URL_PONTO_ELETRONICO() + "adm/webservice_actions.php";
	}

	public static String URL_REQUEST_CHECK_SERVER() {
		return BASE_URL_PONTO_ELETRONICO() + "adm/online.php";
	}

	public static String URL_REQUEST_GET_BANCO_ZIPADO() {
		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO() + "?class=DatabaseSincronizador&action=downloadBanco";
	}

	public static String URL_REQUEST_LOGIN_MOBILE() {
		return URL_BASE_WEBSERVICE_PONTO_ELETRONICO() + "?class=Servicos_web_ponto_eletronico&action=loginMobile";
	}
	
	public static String URL_RASTREAMENTO() {
		return URL_BASE_WEBSERVICE_PONTO_ELETRONICO() + "?class=Servicos_web_ponto_eletronico&action=rastreamento";
	}

//	public static String URL_REQUEST_EMAIL_MELHORIA() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=sendMelhoriaToEmail";
//	}

//	public static String URL_REQUEST_EMAIL_ERROR() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO() + "?class=DatabaseSincronizador&action=sendErroToEmail";
//	}

//	public static String URL_REQUEST_CHECK_SET_DATA_FIM_TAREFA_OFFLINE() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=setDataDeFimDeExcecucaoDaTarefaOffline";
//	}

//	public static String URL_REQUEST_CHECK_SET_DATA_INICIO_TAREFA_OFFLINE() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=setDataDeInicioDeExcecucaoDaTarefaOffline";
//	}

//	public static String URL_REQUEST_CHECK_SET_DATA_FIM_TAREFA() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=setDataDeFimDeExcecucaoDaTarefa";
//	}

//	public static String URL_REQUEST_CHECK_SET_DATA_INICIO_TAREFA() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=setDataDeInicioDeExcecucaoDaTarefa";
//	}

//	public static String URL_REQUEST_CHECK_VEICULO_USUARIO_TAREFA() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=checkVeiculoUsuarioDaTarefa";
//	}

//	public static String URL_REQUEST_ULTIMA_IMAGEM_USUARIO_FOTO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=exportarUltimaImagemDoUsuario";
//	}

//	public static String URL_REQUEST_QUERY_INSERT() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveQueryAndReturnIdDatabase";
//	}

//	public static String URL_REQUEST_HISTORICO_MONITORAMENTO_USUARIO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=getListIdUsuarioFotoAndroidDatabase";
//	}

//	public static String URL_REQUEST_CADASTRO_USUARIO_E_CORPORACAO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveCadastroUsuarioECorporacao";
//	}

//	public static String URL_REQUEST_CADASTRO_USUARIO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveCadastroUsuario";
//	}

//	public static String URL_REQUEST_LEMBRAR_SENHA() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO() + "?class=DatabaseSincronizador&action=sendSenhaToEmail";
//	}

//	public static String URL_REQUEST_ALTERAR_SENHA() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveAlterarSenha";
//	}

//	public static String URL_REQUEST_REMOVE_USUARIO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveRemocaoUsuario";
//	}

//	public static String URL_REQUEST_EDICAO_USUARIO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveEdicaoUsuario";
//	}

//	public static String URL_REQUEST_IS_EMAIL_DO_USUARIO_EXISTENTE() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=isEmailDoUsuarioExistente";
//	}
//
//	public static String URL_REQUEST_EXISTE_REALACIONAMENTO_COM_A_PESSOA() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=existeRelacionamentoComAPessoa";
//	}
//
//	public static String URL_REQUEST_IS_CORPORACAO_EXISTENTE() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=isCorporacaoExistente";
//	}
//
//	public static String URL_REQUEST_CADASTRO_CORPORACAO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveCadastroCorporacao";
//	}
//
//	public static String URL_REQUEST_REMOVE_CORPORACAO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO() + "?class=DatabaseSincronizador&action=removeCorporacao";
//	}
//
//	public static String URL_REQUEST_IS_USUARIO_ADMINISTRADOR_DA_CORPORACAO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=isAdmnistradorDaCorporacao";
//	}
//
//	public static String URL_REQUEST_REMOVE_ALL_CATEGORIA_USUARIO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=removeAllPermissaoCategoriaPermissao";
//	}
//
//	public static String URL_REQUEST_LISTA_INFORMACAO_SINCRONIZADOR() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=sendListaInformacaoSincronizador";
//	}
//
//	public static String URL_REQUEST_RECEIVE_LIST_INSERT_AND_RETURN_ID_DATABASE() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveListInsertAndReturnIdDatabase";
//	}
//
//	public static String URL_REQUEST_RECEIVE_LIST_UPDATE() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveListUpdateDatabase";
//	}
//
//	public static String URL_REQUEST_SEND_LIST_QUERY_OF_LIST_ID_SINCRONIZADOR() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=sendListQueryOfListIdSincronizador";
//	}
//
//	public static String URL_REQUEST_RECEIVE_LISTA_TUPLA_USUARIO_POSICAO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveListaTuplaUsuarioPosicao";
//	}
//
//	public static String URL_REQUEST_RECEIVE_LISTA_TUPLA_USUARIO_FOTO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveListaTuplaUsuarioFoto";
//	}
//
//	public static String URL_REQUEST_SEND_ULTIMO_ID_SINCRONIZADOR_ANDROID() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=sendUltimoIdSincronizadorAndroid";
//	}
//
//	public static String URL_REQUEST_INFORMACAO_DO_USUARIO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveInformacaoUsuairo";
//	}
//
//	public static String URL_REQUEST_GET_LISTA_SERVICO_ATIVO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveListaIdServicoAtivo";
//	}
//
//	public static String URL_REQUEST_CHECA_INTEGRIDADE_DE_SERVICOS_DO_USUARIO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=checaIntegridadeDeServicosDoUsuario";
//	}
//
//	public static String URL_REQUEST_GET_LISTA_POSICAO_VEICULO_DISPONIVEL() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveTabelaVeiculoPosicaoAndroidDatabase";
//	}
//
//	public static String URL_REQUEST_TOTAL_TUPLAS_RASTREAR_ROTA_USUARIO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=getTotalTuplasRastrearRotaUsuarioAndroidDatabase";
//	}
//
//	public static String URL_REQUEST_RASTREAR_ROTA_USUARIO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=rastrearRotaUsuarioAndroidDatabase";
//	}
//
//	public static String URL_REQUEST_GET_LISTA_TAREFA() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveTabelaTarefaAndroidDatabase";
//	}
//
//	public static String URL_REQUEST_GET_LISTA_MENSAGEM_SMS_PESSOA() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveListaMensagemSMSPessoa";
//	}
//
//	public static String URL_REQUEST_SEND_LISTA_MENSAGEM_SMS_PESSOA_COM_FALHA_DE_ENVIO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveListaMensagemSMSPessoaComFalhaDeEnvio";
//	}
//
//	public static String URL_REQUEST_GET_LISTA_MENSAGEM_SMS() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveListaPessoaMensagemSMS";
//	}
//
//	public static String URL_REQUEST_IS_VEICULO_DISPONIVEL() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=isVeiculoDisponivel";
//	}
//
//	public static String URL_REQUEST_DISPONIBILIZA_VEICULO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=disponibilizaVeiculo";
//	}
//
//	public static String URL_REQUEST_DISPONIBILIZA_TODO_VEICULO_OCUPADO_PELO_USUARIO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=disponibilizaTodoVeiculoOcupadoPeloUsuario";
//	}
//
//	public static String URL_REQUEST_DISPONIBILIZA_VEICULO_A_FORCA() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=disponibilizaVeiculoPorOutroUsuario";
//	}
//
//	public static String URL_REQUEST_VETOR_QUERY() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveVetanorQueryTabelaAndroidDatabase";
//	}
//
//	public static String URL_REQUEST_VETOR_QUERY_INSERT_AND_GET_LIST_ID() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveVetorQueryInsertAndReturnListIdTabelaDatabase";
//	}
//
//	public static String URL_REQUEST_VETOR_QUERY_SELECT_AND_GET_LIST_ID() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveVetorQuerySelectAndReturnListIdTabelaDatabase";
//	}
//
//	public static String URL_REQUEST_RECEIVE_LIST_REMOVE_DATABASE() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveListRemoveDatabase";
//	}
//
//	public static String URL_REQUEST_UPDATE_OPERADORA_PESSOA() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=updateOperadoraPessoa";
//	}
//
//	public static String URL_REQUEST_UPDATE_OPERADORA_EMPRESA() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=updateOperadoraEmpresa";
//	}
//
//	public static String URL_REQUEST_LAST_TUPLA_USUARIO_FOTO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveLastTuplaUsuarioFoto";
//	}
//
//	public static String URL_REQUEST_QUERY() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveTabelaAndroidDatabase";
//	}
//
//	public static String URL_REQUEST_FILE_ZIPPER_VETOR_QUERY() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=getListIdOfReceiveZipFileQueryInsert";
//	}
//
//	public static String URL_REQUEST_FILE_ZIPPER_QUERY() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=receiveZipFileQuery";
//	}

//	public static String URL_REQUEST_SEND_FILE_PHOTO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO() + "?class=DatabaseSincronizador&action=receiveFoto";
//	}
//
//	public static String URL_REQUEST_SEND_ZIP_FILE_PHOTO() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO() + "?class=DatabaseSincronizador&action=receiveZipFoto";
//	}
//
//	public static String URL_REQUEST_RECEIVE_FILE() {
//		return URL_BASE_DOWNLOAD_PONTO_ELETRONICO() + "?class=DatabaseSincronizador&action=exportarImagemUsuarioFoto";
//	}

//	public static String URL_REQUEST_DOWNLOAD_BANCO() {
//		return BASE_URL_PONTO_ELETRONICO_FIRST_SYNC() + "recursos/sync/first_sync.php";
//	}
//
//	public static String URL_REQUEST_DOWNLOAD_BANCO_CORPORACAO() {
//		return BASE_URL_PONTO_ELETRONICO_FIRST_SYNC() + "recursos/sync/first_sync_corporacao.php";
//	}

//	public static String URL_REQUEST_SINCRONIZA_TABELA_ANDROID_DATABASE() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=sincronizaTabelaAndroidDatabase";
//	}
//
//	public static String URL_REQUEST_SINCRONIZA_TABELA_USUARIO_ANDROID_DATABASE() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=getStrQueryInsertUsuarioMultipleAndroidDatabase";
//	}
//
//	public static String URL_REQUEST_SINCRONIZA_TABELA_CORPORACAO_ANDROID_DATABASE() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=sincronizaTabelaCorporacaoAndroidDatabase";
//	}
//
//	public static String URL_REQUEST_VERIFICA_ESTRUTURA_TABELA_ANDROID() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=verificaEstruturaTabelaAndroid";
//	}
//
//	public static String URL_REQUEST_VERIFICA_ESTRUTURA_BANCO_ANDROID() {
//		return URL_BASE_WEBSERVICE_ACTIONS_PONTO_ELETRONICO()
//				+ "?class=DatabaseSincronizador&action=verificaEstruturaBancoAndroid";
//	}

	public static int NUMERO_TENTATIVAS = 5;

	// public static String LISTA_TABELA_SINCRONIZACAO_DOWNLOAD_SISTEMA[] = {
	// EXTDAOTipoPonto.NAME,
	// EXTDAOOperadora.NAME,
	// EXTDAOApp.NAME,
	// EXTDAOEstadoCivil.NAME,
	// EXTDAOPermissao.NAME,
	// EXTDAOSistemaTipoOperacaoBanco.NAME,
	// EXTDAOSistemaTabela.NAME,
	// EXTDAOPerfil.NAME,
	// EXTDAOServico.NAME,
	// EXTDAOSexo.NAME,
	// EXTDAOTipoAnexo.NAME,
	// EXTDAOSistemaTipoUsuarioMensagem.NAME};

	private static String mListaTabelaSincronizacaoDownloadUnico[] = null;

	public static String[] LISTA_TABELA_SINCRONIZACAO_DOWNLOAD_UNICO(Database db) {
		if (mListaTabelaSincronizacaoDownloadUnico != null) {
			return mListaTabelaSincronizacaoDownloadUnico;
		} else {

			mListaTabelaSincronizacaoDownloadUnico = EXTDAOSistemaTabela.getVetorDownloadUnico(db);

			return mListaTabelaSincronizacaoDownloadUnico;
		}
	}

	private static String mListaTabelaSincronizacaoUploadContinuo[] = null;

	public static String[] LISTA_TABELA_SINCRONIZACAO_UPLOAD_CONTINUO(Database db) {
		if (mListaTabelaSincronizacaoUploadContinuo != null && mListaTabelaSincronizacaoUploadContinuo.length > 0) {
			return mListaTabelaSincronizacaoUploadContinuo;
		} else {

			mListaTabelaSincronizacaoUploadContinuo = EXTDAOSistemaTabela.getVetorUploadContinuo(db);
			mListaTabelaSincronizacaoUploadContinuo = db
					.getVetorNameHierarquicalOrderOfVetorTable(mListaTabelaSincronizacaoUploadContinuo);

			return mListaTabelaSincronizacaoUploadContinuo;
		}
	}

	private static String mListaTabelaSincronizacaoDownloadContinuo[] = null;

	public static String[] LISTA_TABELA_SINCRONIZACAO_DOWNLOAD_CONTINUO(Database db) {
		if (mListaTabelaSincronizacaoDownloadContinuo != null && mListaTabelaSincronizacaoDownloadContinuo.length > 0) {
			return mListaTabelaSincronizacaoDownloadContinuo;
		} else {

			mListaTabelaSincronizacaoDownloadContinuo = EXTDAOSistemaTabela.getVetorDownloadContinuo(db);
			mListaTabelaSincronizacaoDownloadContinuo = db
					.getVetorNameHierarquicalOrderOfVetorTable(mListaTabelaSincronizacaoDownloadContinuo);

			return mListaTabelaSincronizacaoDownloadContinuo;
		}
	}

	// public static final String DELIMITER_QUERY_UNIQUE_LINHA = "[+!+]";
	// public static final String DELIMITER_QUERY_UNIQUE_COLUNA = "[%%$%%]";
	public static final String DELIMITER_QUERY_UNIQUE_LINHA = "[+!+]+";
	public static final String DELIMITER_QUERY_UNIQUE_COLUNA = "[%%$%%]+";
	public static final String DELIMITER_QUERY_LINHA = "+!+";
	public static final String DELIMITER_QUERY_COLUNA = "%%$%%";

	public static final String CODIGO_DUPLICADA = "-2";
	public static final String CODIGO_INEXISTENTE = "-3";
	public static final String NULL = "null";

	// Some DB constants
	public static final int UNEXISTENT_ID_IN_DB_INT = 0;
	public static final String UNEXISTENT_ID_IN_DB = "0";
	public static final String MASCULIN_CODE_IN_DB = "1";
	public static final String FEMININ_CODE_IN_DB = "2";
	public static final String COD_TIPO_PRESTADOR_MEDIC = "6";

	public static final int ON_LOAD_DIALOG = -3;
	public static final int SEARCH_NO_RESULTS_DIALOG = -1;
	public static final int FORM_ERROR_DIALOG_INFORMACAO_DIALOG = -4;
	public static final int FORM_DIALOG_INFORMACAO_DIALOG_ENDERECO = -5;
	public static final int INFORMACAO_PESSOA_DUPLICIDADE = -254;
	public static final int FORM_ERROR_DIALOG_TELEFONE_OFFLINE = -6;
	public static final int FORM_ERROR_DIALOG_SERVER_OFFLINE = -7;
	public static final int FORM_DIALOG_AUTENTICACAO_INVALIDA = -987;

	// Bar title TypeFace
	public static final String OMEGA_BAR_TITLE_FF = "UnimedDemiBold.ttf";

	// Intent extras

	public static final String MEDIC_ID = "MEDIC_ID";
	public static final String FUNCIONARIO_ID = "FUNCIONARIO_ID";

	// FuncionarioTakePictureActivity
	public static final String STATE_ID = "FuncionarioTakePictureActivity_STATE_ID";

	public static final int STATE_READ_QR_CODE = 1;

	public static final int STATE_FUNCIONARIO_TAKE_PICTURE = 3;
	public static final int ACTIVITY_CAMERA_TAKE_PICTURE_WITH_CLICK = 4;
	public static final int STATE_PONTO_ELETRONICO_MANUAL = 5;

	public static final int ACTIVITY_FORM_CONFIRMACAO_CADASTRO_FILTRO = 73;
	public static final int ACTIVITY_FORM_CATEGORIA_PERMISSAO = 74;
	public static final int ACTIVITY_MAPA_MULTIPLE_MARKER = 75;
	public static final int ACTIVITY_LIST_VEICULO_USUARIO_ATIVO = 77;
	public static final int ACTIVITY_FORM_PONTO = 78;

	public static final int ACTIVITY_CAMERA_VIDEO_RECORDER = 79;
	public static final int ACTIVITY_AUDIO_RECORDER = 80;
	public static final int ACTIVITY_FILE_EXPLORER = 81;

	public static final int ACTIVITY_FACTORY_MENU = 82;

	public static final int ACTIVITY_WIFI = 83;

	public static final int FLAG_ON_BACK_PRESSED = 5000;

	// Service Connection Information Service
	public static final String REFRESH_NOTIFICACAO_APP = "REFRESH_NOTIFICACAO_APP";
	public static final String REFRESH_SERVICE_PAINEL_USUARIO = "REFRESH_SERVICE_PAINEL_USUARIO";
	public static final String REFRESH_SERVICE_STATUS_SERVER = "REFRESH_SERVICE_STATUS_SERVER";
	public static final String REFRESH_LOCALIZACAO = "REFRESH_LOCALIZACAO";

	public static final String REFRESH_UI_INTENT = "REFRESH_UI_INTENT";
	public static final String REFRESH_SERVICE_RECEIVE_FILE_OF_SERVER = "REFRESH_SERVICE_RECEIVE_FILE_OF_SERVER";
	public static final String REFRESH_ACTIVITY_MONITORAMENTO_REMOTO_USUARIO = "REFRESH_ACTIVITY_MONITORAMENTO_REMOTO_USUARIO";
	public static final String REFRESH_ESTADO_SINCRONIZADOR = "REFRESH_ICONE_SINCRONIZADOR";
	public static final String REFRESH_ESTADO_RASTREADOR = "REFRESH_ESTADO_RASTREADOR";
	// SERVICOS SMS
	public static final String REFRESH_ACTIVITY_SEND_ERROR_MENSAGEM_PESSOA_SERVIDOR = "REFRESH_ACTIVITY_SEND_ERROR_MENSAGEM_PESSOA_SERVIDOR";
	public static final String REFRESH_ACTIVITY_RECEIVE_MENSAGEM_SMS_SERVIDOR = "REFRESH_ACTIVITY_RECEIVE_MENSAGEM_SMS_SERVIDOR";
	public static final String REFRESH_ACTIVITY_SEND_SMS = "REFRESH_ACTIVITY_SEND_SMS";
	public static final String REFRESH_ACTIVITY_RECEIVE_SMS = "REFRESH_ACTIVITY_RECEIVE_SMS";
	public static final int STATE_RECEIVE_SMS = 301;

	public static final String REFRESH_SERVICE_LAST_USUARIO_FOTO = "REFRESH_SERVICE_LAST_TUPLA_USUARIO_FOTO";
	// VeiculoGPSLocationListener
	public static final String REFRESH_SERVICE_VEICULO_GPS_LOCATION_LISTENER = "REFRESH_SERVICE_VEICULO_GPS_LOCATION_LISTENER";

	public static final String REFRESH_SERVICE_RECEIVE_USUARIO_RASTREAMENTO_POSICAO = "REFRESH_SERVICE_RECEIVE_USUARIO_RASTREAMENTO_POSICAO";

	public static final String REFRESH_SINCRONIZACAO_A_SER_REALIZADA = "REFRESH_SINCRONIZACAO_A_SER_REALIZADA";
	public static final String REFRESH_SERVICE_STATUS_SERVICO = "REFRESH_SINCRONIZACAO_A_SER_REALIZADA";
	public static final String REFRESH_SERVICE_RECEIVE_SERVICE_STATUS_ATIVO = "REFRESH_SERVICE_RECEIVE_SERVICE_STATUS_ATIVO";
	public static final String REFRESH_SERVICE_RECEIVE_SERVICE_STATUS_INATIVO = "REFRESH_SERVICE_RECEIVE_SERVICE_STATUS_inATIVO";

	// Veiculo Map Activity Map
	public static final String REFRESH_LOG_ERRO = "REFRESH_LOG_ERRO";
	public static final String REFRESH_PONTO_MARCADO = "REFRESH_PONTO_MARCADO";
	public static final String REFRESH_TAREFA_ATUALIZADA = "REFRESH_TAREFA_ATUALIZADA";
	
	public static final String REFRESH_AUTENTICACAO_INVALIDA = "REFRESH_AUTENTICACAO_INVALIDA";
	public static final String REFRESH_SINCRONIZADOR_STATUS = "REFRESH_SINCRONIZADOR_STATUS";

	public static final String REFRESH_SERVICE_LIST_POSITION_OTHER_CAR = "REFRESH_SERVICE_LIST_POSITION_OTHER_CAR";
	public static final String REFRESH_SERVICE_LIST_POSITION_TAREFA = "REFRESH_SERVICE_LIST_POSITION_TAREFA";
	public static final String SEARCH_FIELD_ON_ACTIVITY_RESULT = "SEARCH_FIELD_ON_ACTIVITY_RESULT";
	public static final String SEARCH_FIELD_ESTADO_SINCRONIZADOR = "SEARCH_FIELD_ESTADO_SINCRONIZADOR";
	public static final String SEARCH_FIELD_ESTADO_RASTREADOR = "SEARCH_FIELD_ESTADO_RASTREADOR";
	public static final String SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO = "SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO";
	public static final String SEARCH_FIELD_NOME_TIPO_DOCUMENTO = "SEARCH_FIELD_NOME_TIPO_DOCUMENTO";
	public static final String SEARCH_FIELD_NOME_TIPO_EMPRESA = "SEARCH_FIELD_NOME_TIPO_EMPRESA";
	public static final String SEARCH_FIELD_ID_OPERACAO_SISTEMA_MOBILE = "SEARCH_FIELD_ID_OPERACAO_SISTEMA_MOBILE";
	public static final String SEARCH_FIELD_ID_USUARIO = "SEARCH_FIELD_ID_USUARIO";
	public static final String SEARCH_FIELD_ID_SEMANA_CICLO = "SEARCH_FIELD_ID_SEMANA_CICLO";

	public static final String REFRESH_SERVICE_MY_POSITION_OTHER_CAR = "REFRESH_SERVICE_MY_POSITION_OTHER_CAR";
	public static final String SEARCH_FIELD_IS_ATIVO = "SEARCH_FIELD_IS_ATIVO";
	public static final String SEARCH_FIELD_TOTAL_TUPLAS_BAIXADAS = "SEARCH_FIELD_TOTAL_TUPLAS_BAIXADAS";
	public static final String SEARCH_LIST_LATITUDE = "SEARCH_LIST_LATITUDE";
	public static final String SEARCH_LIST_LONGITUDE = "SEARCH_LIST_LONGITUDE";
	public static final int STATE_RASTREAR_CARRO = 6;
	public static final int STATE_RASTREAR_TAREFA = 66;
	public static final int STATE_TRACAR_ROTA = 68;
	public static final int STATE_RECARREGAR_ADAPTER = 69;
	public static final int ACTIVITY_FILTER_VEICULO_USUARIO = 667;

	public static final int ACTIVITY_FILTER = 668;

	public static boolean IS_SINCRONIZACAO_POR_TIMER = true;

	public static final String SEARCH_FIELD_IS_FLOAT = "SEARCH_FIELD_IS_FLOAT";
	public static final String SEARCH_FIELD_SERVICO = "SEARCH_FIELD_SERVICO";

	// Timing Configuration
	public static final long SERVICE_TIRA_FOTO_DELAY = 60000;
	public static final long SERVICE_TIRA_FOTO_INITIAL_DELAY = 60000;

	public static final long SERVICE_DELETE_FILE_TIMER_INTERVAL = 2 * 60000;
	public static final long SERVICE_DELETE_FILE_INITIAL_DELAY = 2 * 60000;

	public static final long INITIAL_INFORMATION_PONTO_ELETRONICO = 60000;
	public static final long INFORMATION_PONTO_ELETRONICO_TIMER = 60000;

	public static final long SERVICE_RECEIVE_USUARIO_POSICAO_TIMER_INTERVAL = 2000;
	public static final long SERVICE_RECEIVE_USUARIO_POSICAO_INITIAL_DELAY = 60000;
	// 30 seg
	public static final long SERVICE_TIRA_FOTO_TIMER_INTERVAL = 30000;

	public static final long LOAD_LIST_TUPLA_INITIAL_DELAY = 5000;
	public static final long LOAD_LIST_TUPLA_TIMER_INTERVAL = 10000;

	public static final long LOAD_IMAGE_VIEW_OFFLINE_INITIAL_DELAY = LOAD_LIST_TUPLA_TIMER_INTERVAL;
	public static final long LOAD_IMAGE_VIEW_OFFLINE_TIMER_INTERVAL = LOAD_LIST_TUPLA_TIMER_INTERVAL;

	public static final long LOAD_RETIRADA_MOBILE_DA_FILA_ESPERA_INTERVAL_SEGUNDOS = 1 * 60;

	public static final long LOAD_TELA_MONITORAMENTO_INITIAL_DELAY = 3000;
	public static final long LOAD_TELA_MONITORAMENTO_TIMER_INTERVAL = 30000;

	public static final long LOAD_IMAGE_VIEW_ONLINE_INITIAL_DELAY = 50000;
	public static final long LOAD_IMAGE_VIEW_ONLINE_TIMER_INTERVAL = 30000;

	public static final long SERVICE_POSITION_TAREFA_INITIAL_DELAY = 1000;
	public static final long SERVICE_POSITION_TAREFA_TIMER_INTERVAL = 60000;

	public static final long SERVICE_POSITION_OTHER_CAR_INITIAL_DELAY = 10000;

	public static final long SERVICE_SEND_PONTO_MARCADO_TIMER_INTERVAL = 5 * 60000;
	public static final long SERVICE_SEND_PONTO_MARCADO_INITIAL_DELAY = 20000;

	public static final long SERVICE_SEND_LOG_ERRO_TIMER_INTERVAL = 60000;
	public static final long SERVICE_SEND_LOG_ERRO_INITIAL_DELAY = 10000;

	public static final long SERVICE_SEND_TAREFA_TIMER_INTERVAL = 5 * 60000;
	public static final long SERVICE_SEND_TAREFA_INITIAL_DELAY = 45000;

	public static final long SERVICE_SYNCHRONIZE_FAST_DATABASE_TIMER_INTERVAL = 15 * 60000;
	public static final long SERVICE_SYNCHRONIZE_FAST_DATABASE_INITIAL_DELAY = 40000;
	// 5 seg
	public static final long SERVICE_POSITION_OTHER_CAR_TIMER_INTERVAL = 60000;

	public static final long SERVICE_SEND_FILE_TO_SERVER_INITIAL_DELAY = 100000;
	public static final long SERVICE_SEND_FILE_TO_SERVER_TIMER_INTERVAL = 15 * 60000;

	public static final long SERVICE_SEND_MENSAGE_USUARIO_INITIAL_DELAY = 50000;
	public static final long SERVICE_SEND_MENSAGE_USUARIO_TIMER_INTERVAL = 15 * 60000;

	public static final long SERVICE_POSITION_MY_CAR_INITIAL_DELAY = 5000;
	public static final long SERVICE_POSITION_MY_CAR_TIMER_INTERVAL = 60000;

	public static final long SERVICE_NOTIFICACAO_INITIAL_DELAY = 5 * 60000;
	public static final long SERVICE_NOTIFICACAO_TIMER_INTERVAL = 5 * 60000;

	public static final long SERVICE_SYNCH_INITIAL_DELAY = (long) (1000);
	// TODO retornar data antiga
	public static final long SERVICE_SYNCH_TIMER_INTERVAL = 1 * 60000;
	//public static final long SERVICE_SYNCH_TIMER_INTERVAL = 10000;
	public static final long SERVICE_SYNCH_TIMER_INTERVAL_SUB_ROUTINE = 2000;
	public static final long SERVICE_SYNCH_TIMER_INTERVAL_ERRO_DOWNLOAD = 10000;

	public static final long SERVICE_CHECK_LIST_ATIVE_SERVICE_OF_USER_INITIAL_DELAY = 3000;
	public static final long SERVICE_CHECK_LIST_ATIVE_SERVICE_OF_USER_INTERVAL = 5 * 80000;

	public static final long SERVICE_CHECK_SERVER_ONLINE = 60000;
	// 10 seg - Checa se esta online
	public static final long SERVICE_CHECK_SERVER_ONLINE_INTERVAL = 2 * 60000;

	// SMS SERVICES
	public static final long SEND_ERROR_SMS_MENSAGEM_PESSOA_SERVIDOR_INITIAL_DELAY = 5 * 60000;
	public static final long SEND_ERROR_SMS_MENSAGEM_PESSOA_SERVIDOR_TIMER_INTERVAL = 15 * 60000;

	public static final long RECEIVE_MENSAGEM_SMS_SERVIDOR_INITIAL_DELAY = 6 * 60000;
	public static final long RECEIVE_MENSAGEM_SMS_SERVIDOR_TIMER_INTERVAL = 15 * 60000;

	public static final long SERVICE_WIFI_INITIAL_DELAY = 5000;
	public static final long SERVICE_WIFI_TIMER_INTERVAL = 600000;

	// Form empresa
	public static final int ACTIVITY_FORM_EMPRESA_FUNCIONARIO = 4;
	public static final int ACTIVITY_FORM_FUNCIONARIO = 5;
	public static final int ACTIVITY_FORM_ENDERECO = 6;
	public static final int ACTIVITY_FORM_REDE_EMPRESA = 7;
	public static final int ACTIVITY_FORM_VEICULO_USUARIO = 7321354;
	public static final int ACTIVITY_FORM_TECLADO_TELEFONE = 306;
	public static final int ACTIVITY_FORM_TECLADO_CELULAR = 307;
	public static final int ACTIVITY_FORM_TECLADO_CELULAR_SMS = 308;
	public static final int ACTIVITY_FORM_TECLADO_TELEFONE_1 = 309;
	public static final int ACTIVITY_FORM_TECLADO_TELEFONE_2 = 310;
	public static final int ACTIVITY_FORM_TECLADO_FAX = 311;

	public static final String SEARCH_FIELD_TECLADO_TELEFONE_ORIGEM_CHAMADO = "SEARCH_FIELD_TECLADO_TELEFONE_ORIGEM_CHAMADO";

	public static final int ACTIVITY_FORM_DELETE_LAYOUT_ENDERECO = 7;
	public static final int ACTIVITY_FORM_ADD_LAYOUT_ENDERECO = 16;
	public static final int ACTIVITY_FORM_EDIT = 201;
	public static final int ACTIVITY_DETAIL = 205;
	public static final int ACTIVITY_ATUALIZA_VIEW = 443;
	public static final int ACTIVITY_ALERT = 444;
	public static final int ACTIVITY_NOTIFICATION = 445;
	public static final int ACTIVITY_FILTER_PESSOA_EMPRESA = 56546546;
	public static final int ACTIVITY_CONFIRMAR_PONTO = 56234546;
	public static final int ACTIVITY_FORM_PESSOA_EMPRESA = 56546547;
	public static final int ACTIVITY_ALERT_OPERACAO_REALIZADA_COM_SUCESSO = 445;

	public static final int ACTIVITY_FORM_EDIT_DELETE = 210;
	public static final int ACTIVITY_FORM_EDIT_EDIT = 211;
	public static final int ACTIVITY_SETTING_SAIR = 202;
	public static final int ACTIVITY_SETTING_AJUDA = 203;
	
	public static final int ACTIVITY_DESBLOQUEAR_RELOGIO_DE_PONTO = 2033;
	
	
	
	// Visibilities
	public static final int VISIBILITY_HIDDEN = 8;
	public static final int VISIBILITY_VISIBLE = 0;
	public static final int VISIBILITY_INVISIBLE = 4;

	// Order mode
	public static final short ORDER_BY_MODE_CRESCENTE = 1;
	public static final short ORDER_BY_MODE_DECRESCENTE = 2;

	public static final short ORDER_BY_MODE_AGRUPADO_ON = 3;
	public static final short ORDER_BY_MODE_AGRUPADO_OFF = 3;
	public static final short DATA_ORDER_BY_MODE = ORDER_BY_MODE_CRESCENTE;
	public static final short AGRUPADO_ORDER_BY_MODE = ORDER_BY_MODE_AGRUPADO_OFF;

	// Search modes
	public static final short SEARCH_MODE_EMPRESA = 1;
	public static final short SEARCH_MODE_USUARIO = 7;
	public static final short SEARCH_MODE_PESSOA = 3;

	public static final short SEARCH_MODE_VEICULO = 4;
	public static final short SEARCH_MODE_VEICULO_USUARIO = 5;
	public static final short SEARCH_MODE_VEICULO_USUARIO_ATIVO = 6;
	public static final short SEARCH_MODE_ESTADO = 10;

	public static final short SEARCH_MODE_REDE = 22;
	public static final short SEARCH_MODE_PAIS = 13;
	public static final short SEARCH_MODE_CIDADE = 11;
	public static final short SEARCH_MODE_BAIRRO = 12;
	public static final short SEARCH_MODE_CORPORACAO = 17;
	public static final short SEARCH_MODE_PROFISSAO = 18;
	public static final short SEARCH_MODE_TIPO_DOCUMENTO = 19;
	public static final short SEARCH_MODE_TIPO_EMPRESA = 20;
	public static final short SEARCH_MODE_MODELO = 21;
	public static final short SEARCH_MODE_PESSOA_EMPRESA_ROTINA = 23;
	public static final short SEARCH_MODE_RELATORIO = 24;
	public static final short SEARCH_MODE_MAPA_PONTO = 25;

	public static final short SEARCH_MODE_TAREFA= 31;

	public static final short DEFAULT_SEARCH_MODE = SEARCH_MODE_PESSOA;

	// Tipo local values
	public static final short TIPO_LOCAL_CLINICAS = 1;
	public static final short TIPO_LOCAL_HOSPITAIS = 2;

	// Seu plano options
	public static final int UNIFACIL_PLANO_ID_IN_DB = 0;

	// Cadastro modes
	public static final short FORM_MODE_EMPRESA = 4;
	public static final short FORM_MODE_FUNCIONARIO = 5;
	public static final short DEFAULT_FORM_MODE = FORM_MODE_FUNCIONARIO;

	// Search field names

	public static final String SEARCH_FIELD_FILTRO_DISCADOR_ATIVO = "SEARCH_FIELD_FILTRO_DISCADOR_ATIVO";
	public static final String SEARCH_FIELD_LAYOUT = "SEARCH_FIELD_LAYOUT";

	public static final String SEARCH_FIELD_NOME_PROFISSAO = "SEARCH_FIELD_NOME_PROFISSAO";
	public static final String SEARCH_FIELD_IS_MINHA_PROFISSAO = "SEARCH_FIELD_IS_MINHA_PROFISSAO";

	public static final String SEARCH_FIELD_IDENTIFICADOR = "SEARCH_FIELD_IDENTIFICADOR";
	public static final String SEARCH_FIELD_NOME = "SEARCH_FIELD_PLANO";
	public static final String SEARCH_FIELD_CPF = "SEARCH_FIELD_CIDADE";
	public static final String SEARCH_FIELD_RG = "SEARCH_FIELD_BAIRRO";
	public static final String SEARCH_FIELD_DESATIVAR_BOTAO_PROFISSOES = "SEARCH_FIELD_DESATIVAR_BOTAO_PROFISSOES";

	public static final String SEARCH_FIELD_SEXO = "SEARCH_FIELD_SEXO_FUNCIONARIO";
	public static final String SEARCH_FIELD_TELEFONE = "SEARCH_FIELD_TELEFONE";
	public static final String SEARCH_FIELD_NUMERO_TELEFONE = "SEARCH_FIELD_NUMERO_TELEFONE";
	public static final String SEARCH_FIELD_CELULAR = "SEARCH_FIELD_CELULAR";
	public static final String SEARCH_FIELD_TABELA = "SEARCH_FIELD_TABELA";
	public static final String SEARCH_FIELD_ID_TIPO_ENDERECO_TAREFA = "SEARCH_FIELD_ID_TIPO_ENDERECO_TAREFA";
	public static final String SEARCH_FIELD_ATRIBUTO_DESCRICAO = "SEARCH_FIELD_ATRIBUTO_DESCRICAO";

	public static final String SEARCH_FIELD_TIPO_DIMENSAO_IMAGEM = "SEARCH_FIELD_TIPO_DIMENSAO_IMAGEM";

	public static final int LIMITE_TENTATIVA_DOWNLOAD_SINCRONIZACAO = 3;
	public static final int LIMIT_TUPLA_RECEIVE_FROM_SERVER_PHP = 10;

	public static final String SEARCH_FIELD_ID_ESTADO = "SEARCH_FIELD_ID_ESTADO";
	public static final String SEARCH_FIELD_ID_PAIS = "SEARCH_FIELD_ID_PAIS";

	/**
	 * 
	 */
	public static final String SEARCH_FIELD_NOME_CIDADE = "SEARCH_FIELD_NOME_CIDADE";
	public static final String SEARCH_FIELD_NOME_BAIRRO = "SEARCH_FIELD_NOME_BAIRRO";
	public static final String SEARCH_FIELD_NOME_ESTADO = "SEARCH_FIELD_NOME_ESTADO";
	public static final String SEARCH_FIELD_NOME_PAIS = "SEARCH_FIELD_NOME_PAIS";

	public static final String SEARCH_FIELD_NOME_REDE = "SEARCH_FIELD_NOME_REDE";

	// Detail Tarefa
	public static final String SEARCH_FIELD_TITULO_TAREFA = "SEARCH_FIELD_TITULO_TAREFA";

	public static final String SEARCH_FIELD_LATITUDE_ORIGEM = "SEARCH_FIELD_LATITUDE_ORIGEM";
	public static final String SEARCH_FIELD_LONGITUDE_ORIGEM = "SEARCH_FIELD_LONGITUDE_ORIGEM";

	public static final String SEARCH_FIELD_LATITUDE_DESTINO = "SEARCH_FIELD_LATITUDE_DESTINO";
	public static final String SEARCH_FIELD_LONGITUDE_DESTINO = "SEARCH_FIELD_LONGITUDE_DESTINO";

	public static final String SEARCH_FIELD_ENDERECO_ORIGEM = "SEARCH_FIELD_ENDERECO_ORIGEM";
	public static final String SEARCH_FIELD_ENDERECO_DESTINO = "SEARCH_FIELD_ENDERECO_DESTINO";

	public static final String SEARCH_FIELD_DATETIME_CADASTRO = "SEARCH_FIELD_DATETIME_CADASTRO";
	public static final String SEARCH_FIELD_DATE_ABERTURA = "SEARCH_FIELD_DATE_ABERTURA";

	public static final String SEARCH_FIELD_DATETIME_INICIO_PROGRAMADO_TAREFA = "SEARCH_FIELD_DATETIME_INICIO_PROGRAMADO_TAREFA";

	public static final String SEARCH_FIELD_DATA_INICIAL = "SEARCH_FIELD_DATA_INICIAL";

	public static final String SEARCH_FIELD_DATA_FINAL = "SEARCH_FIELD_DATA_FINAL";

	public static final String SEARCH_FIELD_PONTO = "SEARCH_FIELD_PONTO";

	public static final String SEARCH_FIELD_EMPRESA = "SEARCH_FIELD_EMPRESA";
	public static final String SEARCH_FIELD_PROFISSAO = "SEARCH_FIELD_PROFISSAO";
	public static final String SEARCH_FIELD_PESSOA = "SEARCH_FIELD_PESSOA";
	public static final String SEARCH_FIELD_DATA_INICIO_TAREFA = "SEARCH_FIELD_DATA_INICIO_TAREFA";
	public static final String SEARCH_FIELD_HORA_INICIO_TAREFA = "SEARCH_FIELD_HORA_INICIO_TAREFA";
	public static final String SEARCH_FIELD_DATA_FIM_TAREFA = "SEARCH_FIELD_DATA_FIM_TAREFA";
	public static final String SEARCH_FIELD_HORA_FIM_TAREFA = "SEARCH_FIELD_HORA_FIM_TAREFA";

	public static final String SEARCH_FIELD_ENDERECO_LAYOUT = "SEARCH_FIELD_ENDERECO_LAYOUT";
	public static final String SEARCH_FIELD_DATETIME_INICIO_TAREFA = "SEARCH_FIELD_DATETIME_INICIO_TAREFA";
	public static final String SEARCH_FIELD_DATETIME_FIM_TAREFA = "SEARCH_FIELD_DATETIME_FIM_TAREFA";

	public static final String SEARCH_FIELD_LATITUDE_PROPRIA = "SEARCH_FIELD_LATITUDE_PROPRIA";
	public static final String SEARCH_FIELD_LONGITUDE_PROPRIA = "SEARCH_FIELD_LONGITUDE_PROPRIA";

	// Funcionario Take Picture
	public static final String SEARCH_FUNCIONARIO_ID = "SEARCH_FUNCIONARIO_ID";
	public static final String SEARCH_FIELD_IS_ENTRADA = "SEARCH_FIELD_IS_ENTRADA";
	public static final String SEARCH_NEXT_ACTIVITY = "SEARCH_NEXT_ACTIVITY";
	public static final String SEARCH_FIELD_TIPO_PONTO = "SEARCH_FIELD_TIPO_PONTO";

	public static final String SEARCH_LIST_MAP_ICONE = "SEARCH_LIST_MAP_ICONE";

	// Map fied layout
	public static final String MAP_FIELD_ID = "MAP_FIELD_ID";
	public static final String MAP_ADDRESS = "MAP_ADDRESS";
	public static final String MAP_GEOPOINT_LATITUDE = "MAP_GEOPOINT_LATITUDE";
	public static final String MAP_GEOPOINT_LONGITUDE = "MAP_GEOPOINT_LONGITUDE";

	public static final String MAP_TYPE_LOCALIZATION = "MAP_TYPE_LOCALIZATION";

	// Search field names

	public static final String SEARCH_FIELD_GRAVA_SENHA = "SEARCH_FIELD_GRAVA_SENHA";
	public static final String SEARCH_FIELD_ON_BACK_PRESSED = "SEARCH_FIELD_ON_BACK_PRESSED";

	public static final String SEARCH_FIELD_ID = "SEARCH_FIELD_ID";
	public static final String SEARCH_FIELD_CONTEUDO_DESCRITIVO = "SEARCH_FIELD_CONTEUDO_DESCRITIVO";

	public static final String SEARCH_FIELD_NOT_LOAD_CUSTOM_EXECUTE = "SEARCH_FIELD_NOT_LOAD_CUSTOM_EXECUTE";
	public static final String SEARCH_FIELD_TAG = "SEARCH_FIELD_TAG";
	public static final String SEARCH_FIELD_ATIVAR_BOTAO_SAIDA = "SEARCH_FIELD_ATIVAR_BOTAO_SAIDA";

	public static final String DESABILITA_DISCADOR = "DESABILITA_DISCADOR";
	public static final String SEARCH_FIELD_IS_ORDENACAO_DATA = "SEARCH_FIELD_IS_ORDENACAO_DATA";

	public static final String SEARCH_FIELD_CLASS = "SEARCH_FIELD_CLASS";
	public static final String SEARCH_FIELD_CLASS_REMOCAO = "SEARCH_FIELD_CLASS_REMOCAO";
	public static final String SEARCH_FIELD_IS_REMOVE_PERMITED = "SEARCH_FIELD_IS_REMOVE_PERMITED";
	public static final String SEARCH_FIELD_IS_EDIT_PERMITED = "SEARCH_FIELD_IS_EDIT_PERMITED";

	public static final String SEARCH_FIELD_CLASS_CADASTRO = "SEARCH_FIELD_CLASS_CADASTRO";
	public static final String SEARCH_FIELD_TAG_CADASTRO = "SEARCH_FIELD_TAG_CADASTRO";
	public static final String SEARCH_FIELD_CLASS_FILTRO = "SEARCH_FIELD_CLASS_FILTRO";
	public static final String SEARCH_FIELD_TAG_FILTRO = "SEARCH_FIELD_TAG_FILTRO";

	public static final String SEARCH_FIELD_MESSAGE = "SEARCH_FIELD_MESSAGE";
	public static final String SEARCH_FIELD_ENTIDADE = "SEARCH_FIELD_ENTIDADE";
	public static final String SEARCH_FIELD_IS_MASCULINO = "SEARCH_FIELD_IS_MASCULINO";
	public static final int STATE_FORM_CONFIRMACAO_SAIR_CADASTRO = 222;
	public static final int STATE_FORM_CONFIRMACAO_DELETAR_REGISTRO = 232;
	public static final int STATE_FORM_CONFIRMACAO = 214;
	public static final int STATE_FORM_CONFIRMACAO_LOGOUT = 242;
	public static final int STATE_FORM_CONFIRMACAO_LEMBRAR_SENHA = 220;

	public static final int STATE_FORM_CONFIRMACAO_RESETAR = 221;
	public static final int STATE_FORM_CONFIRMACAO_PRIMEIRO_LOGIN = 339;
	public static final int STATE_FORM_CONFIRMACAO_PONTO = 340;
	public static final int STATE_FORM_MARCA_PONTO = 341;

	public static final int STATE_FORM_CONFIRMACAO_CADASTRO_GENERICO = 340;
	public static final int STATE_FORM_CONFIRMACAO_FILTRO_GENERICO = 341;

	public static final int STATE_FORM_CONFIRMACAO_CADASTRO_OU_FILTRO_EMPRESA = 342;
	public static final int STATE_FORM_CADASTRA_EMPRESA = 343;
	public static final int STATE_SEND_FOTO = 238;
	public static final int STATE_FORM_CONFIRMACAO_REMOVER_CORPORACAO = 215;
	public static final int STATE_FORM_CONFIRMACAO_LIMPAR_DADOS = 216;
	public static final int STATE_FORM_CADASTRO_USUARIO_E_CORPORACAO = 400;
	
	public static final int STATE_SELECT_EMPRESA = 401;
	public static final String SEARCH_FIELD_IS_CONFIRMADO = "SEARCH_FIELD_IS_CONFIRMADO";

	public static final String SEARCH_FIELD_TYPE_REMOVE = "SEARCH_FIELD_TYPE_REMOVE";
	public static final String SEARCH_FIELD_IS_EDIT = "SEARCH_FIELD_IS_EDIT";
	public static final String SEARCH_FIELD_IS_MODIFICATED = "SEARCH_FIELD_IS_MODIFICATED";
	public static final String SEARCH_FIELD_EDIT_INDEX = "SEARCH_FIELD_EDIT_INDEX";

	public static final String SEARCH_FIELD_VETOR_ID = "SEARCH_FIELD_VETOR_ID";
	public static final String SEARCH_FIELD_VETOR_FOTO_USUARIO_ID = "SEARCH_FIELD_VETOR_FOTO_USUARIO_ID";
	public static final String SEARCH_FIELD_VETOR_FOTO_USUARIO_DATA_HORA = "SEARCH_FIELD_VETOR_FOTO_USUARIO_DATA_HORA";
	public static final String SEARCH_FIELD_IS_CAMERA_INTERNA = "SEARCH_FIELD_IS_CAMERA_INTERNA";
	public static final String SEARCH_FIELD_PLANO = "SEARCH_FIELD_PLANO";

	public static final String SEARCH_FIELD_ID_PROFISSAO = "SEARCH_FIELD_ID_PROFISSAO";
	public static final String SEARCH_FIELD_NOME_FUNCIONARIO = "SEARCH_FIELD_NOME_FUNCIONARIO";
	public static final String SEARCH_FIELD_SEXO_FUNCIONARIO = "SEARCH_FIELD_SEXO_FUNCIONARIO";
	public static final String SEARCH_FIELD_IS_CONSUMIDOR = "SEARCH_FIELD_IS_CONSUMIDOR";
	public static final String SEARCH_FIELD_SETOR = "SEARCH_FIELD_SETOR";
	public static final String SEARCH_FIELD_ID_TIPO_EMPRESA = "SEARCH_FIELD_ID_TIPO_EMPRESA";
	public static final String SEARCH_FIELD_NOME_EMPRESA = "SEARCH_FIELD_NOME_EMPRESA";
	public static final String SEARCH_FIELD_LIST_USUARIO_VEICULO = "SEARCH_FIELD_LIST_USUARIO_VEICULO";
	public static final String VERSION_CODE = "VersionCode";

	public static final String SEARCH_FIELD_VELOCIDADE = "SEARCH_FIELD_VELOCIDADE";
	public static final String SEARCH_FIELD_DATE_TIME = "SEARCH_FIELD_DATE_TIME";
	public static final String SEARCH_FIELD_LONGITUDE = "SEARCH_FIELD_LONGITUDE";
	public static final String SEARCH_FIELD_LATITUDE = "SEARCH_FIELD_LATITUDE";
	public static final String SEARCH_FIELD_FILE = "SEARCH_FIELD_FILE";

	public static final String SEARCH_FIELD_IS_EMPTY = "SEARCH_FIELD_IS_EMPTY";
	public static final String SEARCH_FIELD_IS_SMS_ENVIADO_COM_SUCESSO = "SEARCH_FIELD_IS_SMS_ENVIADO_COM_SUCESSO";

	public static final String SEARCH_FIELD_LIST_LONGITUDE = "SEARCH_FIELD_LIST_LONGITUDE";
	public static final String SEARCH_FIELD_LIST_LATITUDE = "SEARCH_FIELD_LIST_LATITUDE";
	public static final String SEARCH_FIELD_LIST_MARKER = "SEARCH_FIELD_LIST_MARKER";
	public static final String SEARCH_FIELD_LIST_LONGITUDE_ROTA = "SEARCH_FIELD_LIST_LONGITUDE_ROTA";
	public static final String SEARCH_FIELD_LIST_LATITUDE_ROTA = "SEARCH_FIELD_LIST_LATITUDE_ROTA";
	public static final String SEARCH_FIELD_LIST_COLOR_ROTA = "SEARCH_FIELD_LIST_COLOR_ROTA";
	public static final String SEARCH_FIELD_INDEX_FOCO = "SEARCH_FIELD_INDEX_FOCO";

	public static final String SEARCH_FIELD_NOME_CORPORACAO = "SEARCH_FIELD_NOME_CORPORACAO";
	public static final String SEARCH_FIELD_IS_MINHA_CORPORACAO = "SEARCH_FIELD_IS_MINHA_CORPORACAO";

	public static final String SEARCH_FIELD_STATUS_SERVER = "SEARCH_FIELD_STATUS_SERVER";

	public static final String SEARCH_FIELD_DIRIGIR_VEICULO = "SEARCH_FIELD_DIRIGIR_VEICULO";

	public static final String SEARCH_MODE = "SEARCH_MODE";
	public static final String FORM_MODE = "FORM_MODE";
	public static final String ORDER_BY_MODE = "ORDER_BY_MODE";
	public static final String SEARCH_FIELD_PRESTADOR_ENDERECO = "SEARCH_FIELD_PRESTADOR_ENDERECO";

	public static final String SEARCH_FIELD_USUARIO = "SEARCH_FIELD_USUARIO";
	public static final String SEARCH_FIELD_VEICULO = "SEARCH_FIELD_VEICULO";

	public static final String SEARCH_FIELD_TIPO_TAREFA = "SEARCH_FIELD_TIPO_TAREFA";

	public static final String SEARCH_FIELD_IDENTIFICADOR_TIPO_TAREFA = "SEARCH_FIELD_IDENTIFICADOR_TIPO_TAREFA";

	public static final String SEARCH_FIELD_TIPO_ENDERECO_DESTINO = "SEARCH_FIELD_TIPO_ENDERECO_DESTINO";

	public static final String SEARCH_FIELD_IDENTIFICADOR_TIPO_ENDERECO_DESTINO = "SEARCH_FIELD_IDENTIFICADOR_TIPO_ENDERECO_DESTINO";

	public static final String SEARCH_FIELD_TIPO_ENDERECO_ORIGEM = "SEARCH_FIELD_TIPO_ENDERECO_ORIGEM";

	public static final String SEARCH_FIELD_IDENTIFICADOR_TIPO_ENDERECO_ORIGEM = "SEARCH_FIELD_IDENTIFICADOR_TIPO_ENDERECO_ORIGEM";

	public static final String SEARCH_FIELD_CRIADO_PELO_USUARIO = "SEARCH_FIELD_CRIADO_PELO_USUARIO";
	public static final String SEARCH_FIELD_ESTADO_TAREFA = "SEARCH_FIELD_ESTADO_TAREFA";

	public static final String SEARCH_FIELD_TITULO = "SEARCH_FIELD_TITULO";
	public static final String SEARCH_FIELD_INATIVA_FILTRO_TELEFONE ="SEARCH_FIELD_INATIVA_FILTRO_TELEFONE";

	public static final String SEARCH_FIELD_VEICULO_USUARIO = "SEARCH_FIELD_FUNCIONARIO_VEICULO";
	public static final String SEARCH_FIELD_IS_DISPLAY_ROTA = "SEARCH_FIELD_IS_DISPLAY_ROTA";
	public static final String SEARCH_FIELD_IS_TO_FOCO = "SEARCH_FIELD_IS_TO_FOCO";
	public static final String SEARCH_FIELD_TYPE_MONITORAMENTO = "SEARCH_FIELD_TYPE_MONITORAMENTO";
	public static final String SEARCH_FIELD_TAREFA = "SEARCH_FIELD_TAREFA";

	public static final String SEARCH_FIELD_TYPE_TAREFA = "SEARCH_FIELD_TYPE_TAREFA_ONLINE";

	public static final String SEARCH_FIELD_STATUS = "SEARCH_FIELD_STATUS";
	public static final String SEARCH_FIELD_IS_ADM = "SEARCH_FIELD_IS_ADM";

	public static final String SEARCH_FIELD_JSON = "SEARCH_FIELD_JSON";
	public static final String SEARCH_FIELD_CORPORACAO = "SEARCH_FIELD_CORPORACAO";
	public static final String SEARCH_FIELD_SENHA = "SEARCH_FIELD_SENHA";
	public static final String SEARCH_FIELD_EMAIL = "SEARCH_FIELD_EMAIL";
	public static final String SEARCH_FIELD_CATEGORIA_PERMISSAO = "SEARCH_FIELD_CATEGORIA_PERMISSAO";
	public static final String SEARCH_FIELD_NOME_USUARIO = "SEARCH_FIELD_NOME_USUARIO";
	public static final String SEARCH_FIELD_NOME_MODELO = "SEARCH_FIELD_NOME_MODELO";
	public static final String SEARCH_FIELD_ANO_MODELO = "SEARCH_FIELD_ANO_MODELO";
	public static final String SEARCH_FIELD_MES_SELECIONADO = "SEARCH_FIELD_MES_SELECIONADO";

	public static final String SEARCH_FIELD_MES = "SEARCH_FIELD_MES";
	public static final String SEARCH_FIELD_ANO = "SEARCH_FIELD_ANO";
	public static final String SEARCH_FIELD_DIA_MES = "SEARCH_FIELD_DIA_MES";
	public static final String SEARCH_FIELD_DIA_SEMANA = "SEARCH_FIELD_DIA_SEMANA";

	public static final String SEARCH_FIELD_ANO_SELECIONADO = "SEARCH_FIELD_ANO_SELECIONADO";
	public static final String SEARCH_FIELD_PATH = "SEARCH_FIELD_PATH";

	public static final String SEARCH_FIELD_PLACA = "SEARCH_FIELD_PLACA";

	public static final String SEARCH_ENDERECO_ORIGEM = "SEARCH_ENDERECO_ORIGEM";
	public static final String SEARCH_DATA_CADASTRO_TAREFA = "SEARCH_DATA_CADASTRO_TAREFA";
	public static final String SEARCH_NOME_TAREFA = "SEARCH_NOME_TAREFA";

	// Filter Tarefa

	public static final String SEARCH_FIELD_DESTINO_LOGRADOURO = "SEARCH_FIELD_DESTINO_LOGRADOURO";
	public static final String SEARCH_FIELD_DESTINO_ID_BAIRRO = "SEARCH_FIELD_DESTINO_ID_BAIRRO";
	public static final String SEARCH_FIELD_DESTINO_ID_CIDADE = "SEARCH_FIELD_DESTINO_ID_CIDADE";
	public static final String SEARCH_FIELD_DESTINO_NUMERO = "SEARCH_FIELD_DESTINO_NUMERO";
	public static final String SEARCH_FIELD_DESTINO_COMPLEMENTO = "SEARCH_FIELD_DESTINO_COMPLEMENTO";

	public static final String SEARCH_FIELD_ORIGEM_LOGRADOURO = "SEARCH_FIELD_ORIGEM_LOGRADOURO";
	public static final String SEARCH_FIELD_ORIGEM_ID_BAIRRO = "SEARCH_FIELD_ORIGEM_ID_BAIRRO";
	public static final String SEARCH_FIELD_ORIGEM_ID_CIDADE = "SEARCH_FIELD_ORIGEM_ID_CIDADE";
	public static final String SEARCH_FIELD_ORIGEM_NUMERO = "SEARCH_FIELD_ORIGEM_NUMERO";
	public static final String SEARCH_FIELD_ORIGEM_COMPLEMENTO = "SEARCH_FIELD_ORIGEM_COMPLEMENTO";

	public static final String SEARCH_FIELD_IS_PARCEIRO = "SEARCH_FIELD_IS_PARCEIRO";
	public static final String SEARCH_FIELD_IS_CLIENTE = "SEARCH_FIELD_IS_CLIENTE";
	public static final String SEARCH_FIELD_IS_FORNECEDOR = "SEARCH_FIELD_IS_FORNECEDOR";

	public static final String SEARCH_FIELD_ID_EMPRESA = "SEARCH_FIELD_ID_EMPRESA";
	public static final String SEARCH_FIELD_ID_PESSOA_EMPRESA = "SEARCH_FIELD_ID_PESSOA_EMPRESA";
	public static final String SEARCH_FIELD_NOME_PESSOA = "SEARCH_FIELD_NOME_PESSOA";
	public static final String SEARCH_FIELD_ID_PESSOA = "SEARCH_FIELD_ID_PESSOA";

	public static final String SEARCH_FIELD_LOGRADOURO = "SEARCH_FIELD_LOGRADOURO";
	public static final String SEARCH_FIELD_ID_BAIRRO = "SEARCH_FIELD_ID_BAIRRO";
	public static final String SEARCH_FIELD_ID_TIPO_ENDERECO = "SEARCH_FIELD_ID_TIPO_ENDERECO";
	public static final String SEARCH_FIELD_ID_ENTIDADE = "SEARCH_FIELD_ID_ENTIDADE";
	public static final String SEARCH_FIELD_ID_CIDADE = "SEARCH_FIELD_ID_CIDADE";
	public static final String SEARCH_FIELD_NUMERO = "SEARCH_FIELD_NUMERO";
	public static final String SEARCH_FIELD_COMPLEMENTO = "SEARCH_FIELD_COMPLEMENTO";

	public static final String SEARCH_FIELD_ORIGEM_ENDERECO = "SEARCH_FIELD_ORIGEM_ENDERECO";
	public static final String SEARCH_FIELD_DESTINO_ENDERECO = "SEARCH_FIELD_DESTINO_ENDERECO";
	public static final String SEARCH_FIELD_INICIO_DATA_PROGRAMADA = "SEARCH_FIELD_INICIO_DATA_PROGRAMADA";
	public static final String SEARCH_FIELD_INICIO_HORA_PROGRAMADA = "SEARCH_FIELD_INICIO_HORA_PROGRAMADA";
	public static final String SEARCH_FIELD_FIM_DATA_PROGRAMADA = "SEARCH_FIELD_FIM_DATA_PROGRAMADA";
	public static final String SEARCH_FIELD_DATA_EXIBIR = "SEARCH_FIELD_DATA_EXIBIR";
	public static final String SEARCH_FIELD_INICIO_DATA = "SEARCH_FIELD_INICIO_DATA";
	public static final String SEARCH_FIELD_INICIO_HORA = "SEARCH_FIELD_INICIO_HORA";
	public static final String SEARCH_FIELD_FIM_DATA = "SEARCH_FIELD_FIM_DATA";
	public static final String SEARCH_FIELD_FIM_HORA = "SEARCH_FIELD_FIM_HORA";
	public static final String SEARCH_FIELD_VELOCIDADE_MINIMA = "SEARCH_FIELD_VELOCIDADE_MINIMA";
	public static final String SEARCH_FIELD_SOMENTE_COM_FOTO = "SEARCH_FIELD_SOMENTE_COM_FOTO";
	public static final String SEARCH_FIELD_DATA_CADASTRO = "SEARCH_FIELD_DATA_CADASTRO";
	public static final String SEARCH_FIELD_HORA_CADASTRO = "SEARCH_FIELD_HORA_CADASTRO";
	public static final String SEARCH_FIELD_DESCRICAO = "SEARCH_FIELD_DESCRICAO";

	// Camera configuration
	public static final Bitmap.CompressFormat PICTURE_COMPRESS_FORMAT = Bitmap.CompressFormat.JPEG;
	public static final int PICTURE_COMPRESS_QUALITY = 20;

	// File storage configuration
	public static final String BASE_DIRECTORY_PATH = "";
	public static final String LOG_FILES_EXTENSION = ".txt";
	public static final String ZIP_FILES_EXTENSION = ".zip";
	public static final String PICTURE_FILES_EXTENSION = ".jpeg";
	// Saved Preferences
	public static final String PREFERENCES_NAME = "PontoEletronicoPreferences";

	public static final String SAVED_SINCRONIZACAO_AUTOMATICA = "SAVED_SINCRONIZACAO_AUTOMATICA";

	public static final String SAVED_PAIS = "SAVED_PAIS";
	public static final String SAVED_ESTADO = "SAVED_ESTADO";
	public static final String SAVED_CIDADE = "SAVED_CIDADE";
	public static final String SAVED_NOME_MODELO = "SAVED_MODELO";

	public static final String FORM_IDENTIFICADOR = "FORM_IDENTIFICADOR";
	public static final String FORM_PAIS = "FORM_PAIS";
	public static final String FORM_ESTADO = "FORM_ESTADO";
	public static final String FORM_CIDADE = "FORM_CIDADE";

//	public static final String SAVED_EMAIL = "SAVED_EMAIL";
//	public static final String SAVED_PASSWORD = "SAVED_PASSWORD";
//	public static final String SAVED_CORPORACAO = "SAVED_CORPORACAO";
//	public static final String SAVED_SALVAR_SENHA = "SAVED_SALVAR_SENHA";
	public static final String SAVED_CHAVE_SEGURANCA = "SAVED_CHAVE_SEGURANCA";
//	public static final String SAVED_IS_ADM = "SAVED_IS_ADM";
	public static final String DATABASE_INITIALIZED = "DATABASE_INITIALIZED";

	public static final String DATABASE_CREATED = "DATABASE_CREATED";
	public static final int TYPE_CLASS_TEXT = 0x00000001;
	public static final int TYPE_TEXT_VARIATION_PASSWORD = 0x00000080;
	public static final int TYPE_TEXT_VARIATION_VISIBLE_PASSWORD = 0x00000090;

	public static boolean isTablet(Context p_context) {
		try {
			// Compute screen size
			DisplayMetrics dm = p_context.getResources().getDisplayMetrics();
			float screenWidth = dm.widthPixels / dm.xdpi;
			float screenHeight = dm.heightPixels / dm.ydpi;
			double size = Math.sqrt(Math.pow(screenWidth, 2) + Math.pow(screenHeight, 2));
			// If screen size > 6", device is a tablet
			return size >= 6;
		} catch (Exception e) {
			// Log.e(TAG, "Failed to compute screen size", e);
			return false;
		}
	}
	
	public static String PARAM_EXIT = "PARAM_EXIT";
	public static String PARAM_AUTO_CONNECT_IF_SAVED="PARAM_AUTO_CONNECT_IF_SAVED";
	public static String PARAM_CANCELAR	="PARAM_CANCELAR";
	public static String PARAM_ENTRADA	="PARAM_ENTRADA";
	public static String PARAM_DESCRICAO_PONTO	="PARAM_DESCRICAO_PONTO";
	public static String PARAM_SELECIONAR_ID="PARAM_SELECIONAR_ID";
	public static String PARAM_CORPORACAO="PARAM_CORPORACAO";
	public static String PARAM_BLOQUEADO="PARAM_BLOQUEADO";
	public static String PARAM_ID_EMPRESA="PARAM_ID_EMPRESA";
	public static String PARAM_ID_PESSOA="PARAM_ID_PESSOA";
	public static String PARAM_ID_PROFISSAO="PARAM_ID_PROFISSAO";
	public static String PARAM_IS_ENTRADA="PARAM_IS_ENTRADA";
	public static String PARAM_ID_CORPORACAO="PARAM_ID_CORPORACAO";
	public static String PARAM_ORIGEM_WIDGET="PARAM_ORIGEM_WIDGET";
	

	
}
