package app.omegasoftware.pontoeletronico.audio;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Toast;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.cam.CameraConfiguration;

public class ActivityAudioRecorder extends Activity {
	private static final String TAG = "ActivityAudioRecorder";
    private static String mFileName = null;

    Chronometer mChronometer ;
    private MediaRecorder mRecorder = null;
    private Button recordButton = null;
    private Button stopButton = null;
    
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.audio_recorder_activity_mobile_layout);
        
        mChronometer = ((Chronometer) findViewById(R.id.cronometro));
        mChronometer.showContextMenu();
        
        
        recordButton = (Button) findViewById(R.id.record_button);
        recordButton.setOnClickListener(new RecordOnClickListener());
        stopButton = (Button) findViewById(R.id.stop_button);
        stopButton.setOnClickListener(new StopOnClickListener(this));
        stopButton.setVisibility(View.GONE);
        Bundle vParameters = getIntent().getExtras();
        mFileName = vParameters.getString(CameraConfiguration.PATH_DIRECTORY_FIELD);
	}

    
    private void onRecord(boolean start) {
        if (start) {
            startRecording();
        } else {
            stopRecording();
        }
    }

    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        
        try {
            mRecorder.prepare();
            mChronometer.start();
            recordButton.setVisibility(View.GONE);
            stopButton.setVisibility(View.VISIBLE);
        } catch (IOException e) {
            Log.e(TAG, "prepare() failed");
        }

        mRecorder.start();
    }
    

    protected void showElapsedTime() {
	    long elapsedMillis = SystemClock.elapsedRealtime() - mChronometer.getBase();            
	    Toast.makeText(ActivityAudioRecorder.this, "Elapsed milliseconds: " + elapsedMillis, 
	            Toast.LENGTH_SHORT).show();
	}

    @Override
	public void onBackPressed(){
		
		if(stopButton.getVisibility() == View.VISIBLE){
			Intent intent = getIntent();
			intent.putExtra(CameraConfiguration.PARAMETER_PICTURE, mFileName);
			setResult(OmegaConfiguration.ACTIVITY_AUDIO_RECORDER, intent);	
		}
		
		super.onBackPressed();
	}
	
	
	
    private void stopRecording() {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        mChronometer.stop();
        Intent intent = getIntent();
		intent.putExtra(CameraConfiguration.PARAMETER_PICTURE, mFileName);
		setResult(OmegaConfiguration.ACTIVITY_AUDIO_RECORDER, intent);
		finish();
        //showElapsedTime();
    }

    public class RecordOnClickListener implements OnClickListener{
    	 boolean mStartRecording = true;
    	 public void onClick(View v) {
             onRecord(mStartRecording);
             mStartRecording = !mStartRecording;
         }
    }
    

    public class StopOnClickListener implements OnClickListener{
    	 boolean mPauseRecording = false;
    	 Activity activity;
    	 
    	 public StopOnClickListener(Activity pActivity){
    		 activity = pActivity;
    	 }
    	 
    	 public void onClick(View v) {
    		 onRecord(false);
    		 
         }
    }
    
    @Override
    public void onPause() {
        super.onPause();
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }

    }
}
