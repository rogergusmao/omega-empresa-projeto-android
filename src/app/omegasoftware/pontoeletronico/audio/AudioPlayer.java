package app.omegasoftware.pontoeletronico.audio;

import android.media.MediaPlayer;

public class AudioPlayer {
	String mFilePath;
	MediaPlayer mp ;
	public AudioPlayer(String pFilePath){
		mFilePath = pFilePath;
		initialize();
		
	}
	public void initialize(){
		try {
			mp = new MediaPlayer();
	        mp.setDataSource(mFilePath);
	        mp.prepare();
		} catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	 public void play(){
	    //set up MediaPlayer    
	    try {
	    	
	        mp.start();
	        
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	 
	 public void stop(){
	    //set up MediaPlayer    
	    try {
	        if(mp.isPlaying())
	        	mp.stop();
	        
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	 
	 public void pause(){
	    //set up MediaPlayer    
	    try {
	        mp.pause();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}
