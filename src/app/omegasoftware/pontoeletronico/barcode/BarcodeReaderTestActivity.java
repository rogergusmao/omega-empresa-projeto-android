/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package app.omegasoftware.pontoeletronico.barcode;


import android.app.AlertDialog;
import android.content.Intent;
import app.omegasoftware.pontoeletronico.R;
import android.os.Bundle;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;

public final class BarcodeReaderTestActivity extends OmegaRegularActivity {
	public static int TEST_QR_READER = 1;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.splash_mobile);

		Intent intent = new Intent("com.google.zxing.client.android.SCAN");
		intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
		startActivityForResult(intent, 0);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		try{
			

		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				String contents = intent.getStringExtra("SCAN_RESULT");
				String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
				showDialog(R.string.result_succeeded, "Formato: " + format
						+ "\nConteudo: " + contents);
				boolean validade = false;
				if (contents != null) {
					String[] vetor = contents.split(":");
					if (vetor.length == 2) {
						@SuppressWarnings("unused")
						boolean isEntrada = false;
						if (vetor[0] != null && vetor[1] != null) {
							if (vetor[0].compareTo("e") == 0) {
								isEntrada = true;
							} else if (vetor[0].compareTo("s") == 0) {
								isEntrada = false;
							} else {
								showDialog(R.string.result_failed,
										getString(R.string.result_failed_why));
							}
							if (vetor[1].length() > 0) {
								DatabasePontoEletronico database = new DatabasePontoEletronico(
										this);

								EXTDAOPessoa obj = new EXTDAOPessoa(database);
								obj.setAttrValue(EXTDAOPessoa.ID,vetor[1]);
								// Se encontrar o funcionario em questao
								if (obj.select()) {
									Intent intent2 = new Intent(
											this,
											DetailPessoaActivityMobile.class);
									
									intent2.putExtra(
											OmegaConfiguration.FUNCIONARIO_ID,
											vetor[1]);
									
									validade = true;
									startActivityForResult(intent2, 0);
								}
							}
						}
					}
				}
				if (!validade)
					showDialog(R.string.result_failed,
							getString(R.string.result_failed_why));
			} else if (resultCode == RESULT_CANCELED) {
				showDialog(R.string.result_failed,
						getString(R.string.result_failed_why));
			}

		}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);

		}
	}

	private void showDialog(int title, CharSequence message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton("OK", null);
		builder.show();
	}

	@Override
	public boolean loadData() {
		
		return false;
	}

	@Override
	public void initializeComponents() {
		

	}

}