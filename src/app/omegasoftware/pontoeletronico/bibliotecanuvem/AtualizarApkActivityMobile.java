package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import java.io.File;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.ContainerPrograma;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.ControladorAtualizadorVersao.ESTADO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.parametroglobal.ItemXMLParametroGlobal;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.parametroglobal.ParserXMLParametroGlobal;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;

public class AtualizarApkActivityMobile extends OmegaRegularActivity {

	// Constants
	public static final String TAG = "AtualizarApkActivityMobile";
	public static final int FORM_DIALOG_HABILITAR_PERMISSAO_INSTALACAO_APLICACAO_SEM_SER_DO_MARKET = 1;
	public static final int FORM_DIALOG_INSTRUCAO_INSTALACAO_APK = 2;
	public static final int FORM_DIALOG_FALHA_ATUALIZACAO = 3;
	public static final int FORM_DIALOG_VERSAO_ATUALIZADA_COM_SUCESSO = 4;

	ControladorAtualizadorVersao mControlador;

	Context context;
	private Typeface customTypeFace;

	public static final int STATE_ATUALIZA_VERSAO = 112;
	public static final int STATE_CONFIRMA_INSTALACAO_APLICATIVO_SEM_SER_DO_MARKET = 113;

	private enum ESTADO_INSTALACAO_SEM_SER_DO_MARKET {
		NAO_VERIFICADO, NAO_HABILITADO, HABILITADO
	}

	private ESTADO_INSTALACAO_SEM_SER_DO_MARKET estado = ESTADO_INSTALACAO_SEM_SER_DO_MARKET.NAO_VERIFICADO;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.atualizar_apk_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(this.getAssets(),
				"trebucbd.ttf");

		new CustomDataLoader(this).execute();
	}

	public boolean checaPermissaoInstalacaoAplicativoSemSerDoMarket() {
		int result = Settings.Secure.getInt(getContentResolver(),
				Settings.Secure.INSTALL_NON_MARKET_APPS, 0);
		if (result == 0)
			return false;
		else
			return true;
	}

	@Override
	public boolean loadData() {

		switch (estado) {
		case NAO_VERIFICADO:
			if (checaPermissaoInstalacaoAplicativoSemSerDoMarket())
				estado = ESTADO_INSTALACAO_SEM_SER_DO_MARKET.HABILITADO;
			else
				estado = ESTADO_INSTALACAO_SEM_SER_DO_MARKET.NAO_HABILITADO;
			break;
		case HABILITADO:

			break;
		case NAO_HABILITADO:

			if (checaPermissaoInstalacaoAplicativoSemSerDoMarket())
				estado = ESTADO_INSTALACAO_SEM_SER_DO_MARKET.HABILITADO;
			else
				estado = ESTADO_INSTALACAO_SEM_SER_DO_MARKET.NAO_HABILITADO;
			break;

		default:
			estado = ESTADO_INSTALACAO_SEM_SER_DO_MARKET.NAO_HABILITADO;
			break;
		}
		return true;
	}

	public void formatarStyle() {

		((TextView) findViewById(R.id.atualizar_textview))
				.setTypeface(this.customTypeFace);
	}

	@Override
	public void initializeComponents() {

		//

		formatarStyle();
		mControlador = new ControladorAtualizadorVersao(this);
		if (estado == ESTADO_INSTALACAO_SEM_SER_DO_MARKET.NAO_HABILITADO
				&& ContainerPrograma.isProgramaDaEmpresa()) {
			showDialog(FORM_DIALOG_HABILITAR_PERMISSAO_INSTALACAO_APLICACAO_SEM_SER_DO_MARKET);
		} else {
			new ControladorLoader(this).execute();
		}
		return;
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (id) {
		case FORM_DIALOG_INSTRUCAO_INSTALACAO_APK:

			vTextView.setText(getResources().getString(
					R.string.instrucao_instalacao_apk));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_DIALOG_INSTRUCAO_INSTALACAO_APK);
					instalaVersao();
				}
			});
			break;
		case FORM_DIALOG_HABILITAR_PERMISSAO_INSTALACAO_APLICACAO_SEM_SER_DO_MARKET:
			vTextView
					.setText(getResources()
							.getString(
									R.string.habilitar_permissao_instalacao_aplicacao_sem_ser_do_maket));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					procedimentoConfirmaInstalacaoAplicativoSemSerDoMarket();
					dismissDialog(FORM_DIALOG_HABILITAR_PERMISSAO_INSTALACAO_APLICACAO_SEM_SER_DO_MARKET);
				}
			});
			break;
		case FORM_DIALOG_FALHA_ATUALIZACAO:
			vTextView
					.setText(getResources()
							.getString(
									R.string.falha_ao_sincronizar_os_dados_durante_sincronizacao));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_DIALOG_FALHA_ATUALIZACAO);
					Intent i = new Intent();
					i.putExtra(
							OmegaConfiguration.SEARCH_FIELD_ON_ACTIVITY_RESULT,
							OmegaConfiguration.STATE_ATUALIZAR_VERSAO);
					setResult(RESULT_CANCELED, i);
					finish();
				}
			});
			break;
		case FORM_DIALOG_VERSAO_ATUALIZADA_COM_SUCESSO:
			vTextView.setText(getResources().getString(
					R.string.versao_atualizada_com_sucesso));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_DIALOG_VERSAO_ATUALIZADA_COM_SUCESSO);
					Intent i = new Intent();
					i.putExtra(
							OmegaConfiguration.SEARCH_FIELD_ON_ACTIVITY_RESULT,
							OmegaConfiguration.STATE_ATUALIZAR_VERSAO);
					setResult(RESULT_OK, i);
					finish();
				}
			});
			break;

		default:
			vDialog = super.onCreateDialog(id);
			break;
		}
		return vDialog;
	}

	private void procedimentoConfirmaInstalacaoAplicativoSemSerDoMarket() {
		Intent intent = new Intent();
		intent.setAction(Settings.ACTION_APPLICATION_SETTINGS);
		startActivityForResult(intent,
				STATE_CONFIRMA_INSTALACAO_APLICATIVO_SEM_SER_DO_MARKET);
	}

	@Override
	public void onBackPressed() {
		if (mControlador == null) {
			return;
		}
		ControladorAtualizadorVersao.ESTADO estadoAtual = mControlador
				.getEstado();
		switch (estadoAtual) {
		case FINALIZADO:
		case APK_INSTALADA:
		case TRATAMENTO_EXCECAO_DE_ATUALIZACAO_DE_BANCO:
			super.onBackPressed();
			break;
		case ATUALIZANDO_VERSAO_BANCO:
		case DOWNLOAD_APK:
		case INSTALANDO_APK:
		case NAO_INICIALIZADO:
			DialogInterface.OnClickListener dialogConfirmarSaida = new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						AtualizarApkActivityMobile.this.finish();
						break;

					case DialogInterface.BUTTON_NEGATIVE:

						break;
					}
				}
			};

			FactoryAlertDialog.showAlertDialog(this,
					getString(R.string.dialog_sair_atualizacao_versao),
					dialogConfirmarSaida);

			break;

		default:
			break;
		}

	}

	@Override
	public void finish() {

		super.finish();
	}

	public boolean instalaVersao() {
		Integer pId = mControlador.getIdSPVSPMNovaVersaoDaApk();
		BibliotecaNuvemArvoreArquivo vObj = new BibliotecaNuvemArvoreArquivo();
		String pathXmlConfiguracaoNovaApk = vObj.getPathApk(pId.toString())
				+ BibliotecaNuvemConfiguracao.XML_CONFIGURACAO;

		File fileXMLPublicacao = new File(pathXmlConfiguracaoNovaApk);
		if (fileXMLPublicacao.isFile()) {
			ItemXMLParametroGlobal xmlPublicacao = ParserXMLParametroGlobal
					.getItemXMLParametroGlobal(this, fileXMLPublicacao);

			String fileName = vObj.getPathApk(pId.toString())
					+ xmlPublicacao.programa_arquivo;
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(new File(fileName)),
					"application/vnd.android.package-archive");

			mControlador.atualizaEstadoParaApkInstalada();
			startActivityForResult(intent,
					OmegaConfiguration.STATE_ATUALIZAR_VERSAO);
			return true;
		} else {
			SingletonLog.insereErro(new Exception(
					"Pacote apk nuo possui o arquivo de configurauuo:"
							+ BibliotecaNuvemConfiguracao.XML_CONFIGURACAO
							+ ". Path Atual: " + pathXmlConfiguracaoNovaApk),
					TIPO.BIBLIOTECA_NUVEM);
			return false;
		}
	}

	public void procedimentoStateConfirmaInstalacaoAplicativoSemSerDoMarket(
			int resultCode) {

		if (!checaPermissaoInstalacaoAplicativoSemSerDoMarket()) {
			estado = ESTADO_INSTALACAO_SEM_SER_DO_MARKET.NAO_HABILITADO;
			showDialog(FORM_DIALOG_HABILITAR_PERMISSAO_INSTALACAO_APLICACAO_SEM_SER_DO_MARKET);
		} else {
			estado = ESTADO_INSTALACAO_SEM_SER_DO_MARKET.HABILITADO;
			new ControladorLoader(this).execute();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (mControlador == null)
			mControlador = new ControladorAtualizadorVersao(this);
		switch (requestCode) {
		case STATE_ATUALIZA_VERSAO:

			switch (resultCode) {
			case RESULT_CANCELED:

				if (mControlador.atualizaEstadoParaInstalandoApkSeNecessario() == ESTADO.APK_INSTALADA)
					mControlador.processa();
				else
					showDialog(FORM_DIALOG_INSTRUCAO_INSTALACAO_APK);

				break;

			default:
				// Continua o processo do controlador
				new ControladorLoader(this).execute();
				break;
			}
			break;
		case STATE_CONFIRMA_INSTALACAO_APLICATIVO_SEM_SER_DO_MARKET:
			procedimentoStateConfirmaInstalacaoAplicativoSemSerDoMarket(resultCode);
			break;
		default:
			break;
		}

	}

	public class ControladorLoader extends AsyncTask<String, Integer, Boolean> {
		Activity activity;
		boolean executaActivity = false;
		ESTADO est = null;
		double porcentagem = 0;

		public ControladorLoader(Activity pActivity) {
			activity = pActivity;

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			if (mControlador.getEstado() != ESTADO.DOWNLOAD_APK)
				mControlador.atualizaEstadoParaInstalandoApkSeNecessario();

			if (mControlador.getEstado() == ESTADO.INSTALANDO_APK) {

				instalaVersao();
			} else {
				est = mControlador.processa();
				if (mControlador.getEstado() == ESTADO.INSTALANDO_APK) {
					instalaVersao();

				}

			}
			return true;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);

			controlerProgressDialog.dismissProgressDialog();

			try {
				switch (est) {
				case FINALIZADO:
					mControlador.persistirEstado(ESTADO.NAO_INICIALIZADO);
					showDialog(FORM_DIALOG_VERSAO_ATUALIZADA_COM_SUCESSO);
					break;
				case TRATAMENTO_EXCECAO_DE_ATUALIZACAO_DE_BANCO:
					mControlador.persistirEstado(ESTADO.NAO_INICIALIZADO);
					showDialog(FORM_DIALOG_FALHA_ATUALIZACAO);
					break;
				case INSTALANDO_APK:
					break;
				default:
					SingletonLog.insereErro(new Exception(
							"Estado nao identificado" + String.valueOf(est)),
							TIPO.BIBLIOTECA_NUVEM);
					break;
				}
			} catch (Exception ex) {
				SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
			}

		}

	}
}
