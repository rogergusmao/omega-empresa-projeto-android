package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import android.graphics.Bitmap.CompressFormat;
import android.os.Environment;
import app.omegasoftware.pontoeletronico.file.HelperFile;

public class BibliotecaNuvemArvoreArquivo {

	//File storage configuration
	public static final String BASE_DIRECTORY_PATH = "";
	public static final String LOG_FILES_EXTENSION = ".txt";
	public static final String ZIP_FILES_EXTENSION = ".zip";
	public static final String PICTURE_FILES_EXTENSION = ".jpeg";
	public static final int PERCENT_QUALITY = 50;
	public static final CompressFormat COMPRESS_FORMAT = CompressFormat.JPEG;
	
	
	static String rootPathUpload;
	static String rootPathUploadArquivoZipado;
	static String rootPathDownload;
	static String rootPathTemporario;
	
	boolean isCheckPathUploadArquivoZipado= false;
	boolean isCheckPathUpload= false;
	boolean isCheckDownload = false;
	boolean isCheckTemporario = false;
	
	
	public BibliotecaNuvemArvoreArquivo(){
		String vRootPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BibliotecaNuvem/" + BibliotecaNuvemConfiguracao.NOME_DO_PROJETO ;
//		rootPath = pContext.getApplicationContext().getFilesDir().getAbsolutePath();
		rootPathUpload = vRootPath + "/Upload";
		rootPathUploadArquivoZipado = vRootPath + "/UploadArquivoZipado";
		rootPathDownload = vRootPath + "/Download";
		rootPathTemporario = vRootPath + "/Temporario";
		
//		HelperFile.createDirectory(rootPathSendPhotoFile);
	}
	
	public String getPathApk(String pIdVersao){
		String vPathDownload = getPathDownload();
		String vPathApk = vPathDownload + "apk/" + pIdVersao;
		HelperFile.createDirectory(vPathApk);
		return vPathApk + "/";
	}
	

	public String getPathScript(String pIdVersao){
		String vPathDownload = getPathUpload();
		String vPathApk = vPathDownload + "operacao_sistema_mobile/" +pIdVersao + "/script";
		HelperFile.createDirectory(vPathApk);
		return vPathApk + "/";
	}

	public String getPathUpload(){
		if(! isCheckPathUpload){
			
			HelperFile.createDirectory(rootPathUpload);
			isCheckPathUpload = true;
		}
		return rootPathUpload + "/";
	}
	
	public String getPathUploadArquivoZipado(){
		if(! isCheckPathUploadArquivoZipado){
		HelperFile.createDirectory(rootPathUpload);
		isCheckPathUploadArquivoZipado = true;
		}
		return rootPathUpload + "/";
	}
	
	public String getPathDownload(){
		if(! isCheckDownload){
		HelperFile.createDirectory(rootPathDownload);
		isCheckDownload = true;
		}
		return rootPathDownload + "/";
	}
	
	public String getPathTemporario(){
		if(! isCheckTemporario){
			HelperFile.createDirectory(rootPathTemporario);
			isCheckTemporario = true;
		}
		return rootPathTemporario + "/";
	}
	
	
	public String getFileUniqueName(String pExtension){
		return HelperFile.getFileWithUniqueName() + pExtension;
	}

}
