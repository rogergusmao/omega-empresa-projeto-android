package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;

public class BibliotecaNuvemConfiguracao {
	public static String NOME_DO_PROJETO = "Ponto Eletronico";
	public static String PACOTE_APLICACAO = "app.omegasoftware.pontoeletronico";

	public static String XML_INSTRUCOES_ATUALIZACAO = "instrucoes_atualizacao.xml";

	public static String XML_CONFIGURACAO = "publicacao_android.xml";


	//"/BibliotecaNuvem/BN10003Corporacao/";
	
	public static final String BASE_URL(){
		if(OmegaConfiguration.URL_BIBLIOTECA_NUVEM == null)
			OmegaConfiguration.URL_BIBLIOTECA_NUVEM = OmegaConfiguration.getLocalUrl();
		if(OmegaConfiguration.URL_BIBLIOTECA_NUVEM == null)
			return null;
		return OmegaConfiguration.URL_BIBLIOTECA_NUVEM + "/public_html/adm/webservice.php?class=Servico_mobile&action=";
	}

	public static final String BASE_URL_DOWNLOAD(){
		if(OmegaConfiguration.URL_BIBLIOTECA_NUVEM == null)
			OmegaConfiguration.URL_BIBLIOTECA_NUVEM = OmegaConfiguration.getLocalUrl();
		if(OmegaConfiguration.URL_BIBLIOTECA_NUVEM == null)
			return null;
		return OmegaConfiguration.URL_BIBLIOTECA_NUVEM + "/public_html/adm/download.php?class=Servico_mobile&action=";
	}
	

	// public static final String BASE_URL =

	// public static final String BASE_URL =


	public enum TIPO_REQUEST {
		logErroProdutoMobile, //DEPRECATED
		
		isServidorOnline, downloadApkAndroid, downloadBanco,
		conectaTelefone, modoSuporte, registraUsuarioMensagem, 
		consultaListaIdSistemaProjetosVersaoSistemaProdutoMobile, 
		consultaUltimoIdSistemaProjetosVersaoSistemaProdutoMobile, 
		consultaOperacaoSistemaMobile, atualizaEstadoOperacaoSistemaMobile, 
		registraLogErroMobile, registraLogErroAtualizacaoBancoMobile, 
		atualizaConexao, desconectaTelefone, transfereArquivoMobileParaWeb, 
		transfereArquivoZipadoMobileParaWeb, transfereScriptBancoAndroidParaWeb, 
		transfereArquivoWebParaMobile, transferePrintScreenMobileParaWeb, 
		falhaPrintScreenMobileParaWeb, consultaMonitoraTelaWebParaMobile,
		criaOperacaoSistemaMobileDownloadBancoMobileAvulso,
		criaOperacaoSistemaMobileDownloadBancoSQLiteMobileAvulso,
		logArquivoErroProdutoMobile
	}

//	public static boolean checaSeOSistemaEstaNoAr(Context context) {
//		return HelperHttp.isUrlAtiva(context,
//				getUrlWebService(TIPO_REQUEST.isServidorOnline));
//	}

	public static final long SERVICE_OPERACAO_SISTEMA_MOBILE_INITIAL_DELAY = 3000;
	// TODO voltar para 50 seg (ta 10 seg agora pq to depurando)
	public static final long SERVICE_OPERACAO_SISTEMA_MOBILE_INTERVAL = 30000;

	public static final long SERVICE_SUPORTE_OPERACAO_SISTEMA_MOBILE_INITIAL_DELAY = 10000;
	public static final long SERVICE_SUPORTE_OPERACAO_SISTEMA_MOBILE_INTERVAL = 50000;

	public static final long SERVICE_SUPORTE_INITIAL_DELAY = 5000;
	public static final long SERVICE_SUPORTE_INTERVAL = 100000;

	public static final long SERVICE_MONITORA_TELA_MOBILE_PARA_WEB_INITIAL_DELAY = 2000;
	public static final long SERVICE_MONITORA_TELA_MOBILE_PARA_WEB_INTERVAL = 10000;

	public static final long SERVICE_MONITORA_TELA_WEB_PARA_MOBILE_INITIAL_DELAY = 5000;
	public static final long SERVICE_MONITORA_TELA_WEB_PARA_MOBILE_INTERVAL = 10000;
	//
	public static final long SERVICE_MONITORA_TELA_SESSAO_INITIAL_DELAY = 5000;
	public static final long SERVICE_MONITORA_TELA_SESSAO_INTERVAL = 5 * 60000;

	public static int MAXIMO_LOG_ERRO_ENVIADO = 10;

	public enum PARAM {
		IS_SUPORTE_ATIVO, OPERACAO_SISTEMA_MOBILE_FINALIZA_CICLO, ESTADO_OPERACAO_SISTEMA_MOBILE
	}

	public enum REFRESH_SERVICE {
		SUPORTE_MOBILE, OPERACAO_SISTEMA_MOBILE,

	}

	public enum SERVICE_OPERACAO_SISTEMA_MOBILE {
		FINALIZA_CICLO(1), ATIVA_SUPORTE(2);
		int id;

		SERVICE_OPERACAO_SISTEMA_MOBILE(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}

		public SERVICE_OPERACAO_SISTEMA_MOBILE getTipo(int id) {
			SERVICE_OPERACAO_SISTEMA_MOBILE[] vetor = SERVICE_OPERACAO_SISTEMA_MOBILE
					.values();
			for (int i = 0; i < vetor.length; i++) {
				if (vetor[i].getId() == id)
					return vetor[i];
			}
			return null;
		}
	}

	public static String getUrlWebService(TIPO_REQUEST pTipoRequest) {
		String urlBase = BASE_URL();
		if(urlBase == null ) {
			SingletonLog.insereWarning(new Exception("Url base nula: " + pTipoRequest.toString()), TIPO.HTTP);
			return null;
		}
		String vUrl =  urlBase + pTipoRequest.toString();
		return vUrl;
	}
	
	public static String getUrlDownload(TIPO_REQUEST pTipoRequest) {
		String vUrl = BASE_URL_DOWNLOAD() + pTipoRequest.toString();
		return vUrl;
	}
	

	public static String NOME_ARQUIVO_ZIPADO_APK = "conjunto_apk.zip";
}
