package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import android.content.Context;
import app.omegasoftware.pontoeletronico.common.HelperSharedPreference;

public class BibliotecaNuvemSharedPreference {
	
	public static final String PREFERENCES_NAME = "BibliotecaNuvemSharedPreference";
	
	public enum TIPO_STRING{
		SISTEMA_PROJETOS_VERSAO_PRODUTO_ID,
		SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID,
		SISTEMA_PROJETOS_VERSAO_ID,
		SISTEMA_ID,
		SISTEMA_PRODUTO_ID,
		SISTEMA_PRODUTO_MOBILE_ID,
		PROJETOS_ID,
		PROGRAMA_ARQUIVO,
		MOBILE_IDENTIFICADOR,
		
		
	}
	
	public enum TIPO_BOOLEAN{
		XML_CONFIGURACAO_CARREGADO,
		RESETAR_VERSAO
	}
	
	public enum TIPO_VETOR_INT{
		VETOR_NOVA_VERSAO_SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID
	}
	
	public enum TIPO_INT{
		PONTEIRO_ATUALIZADOR_BANCO,
		ID_SPVSPM_VERSAO_A_SER_INSTALADA,
		ESTADO_CONTROLADOR_ATUALIZADOR_VERSAO,
		VERSAO_ATUAL_DO_SISTEMA
		
	}
	
	public static void saveValor(Context pContext, TIPO_VETOR_INT pTipo, Integer[] pValor){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		h.saveValorArrayInteger(pContext, pTipo.toString(), pValor);
	}
	
	public static Integer[] getValor(Context pContext, TIPO_VETOR_INT pTipo){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		return h.getValorArrayInteger(pContext, pTipo.toString());
	}

	public static void saveValor(Context pContext, TIPO_BOOLEAN pTipo, boolean pValor){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		h.saveValorBoolean(pContext, pTipo.toString(), pValor);
		
	}
	
	public static boolean getValor(Context pContext, TIPO_BOOLEAN pTipo){
		//saveValor(pContext, pTipo, true);
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		return h.getValorBoolean(pContext, pTipo.toString());
	}
	
	public static void saveValor(Context pContext, TIPO_STRING pTipo, String pValor){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		h.saveValorString(pContext, pTipo.toString(), pValor);
	}
	
	public static String getValor(Context pContext, TIPO_STRING pTipo){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		return h.getValorString(pContext, pTipo.toString());
	}
	
	public static void saveValor(Context pContext, TIPO_INT pTipo, int pValor){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		h.saveValorInt(pContext, pTipo.toString(), pValor);					
	}
	
	public static int getValor(Context pContext, TIPO_INT pTipo, int defaultValue){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		return h.getValorInt(pContext, pTipo.toString(), defaultValue);	
	}
	
}
