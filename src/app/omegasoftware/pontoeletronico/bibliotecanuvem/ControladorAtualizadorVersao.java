package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import java.util.ArrayList;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemSharedPreference.TIPO_INT;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.InterfaceItemXML.TIPO_ITEM;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.instrucaoatualizacao.ItemScriptVersao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.scriptbanco.ItemScripComandoBanco;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.ItemXMLHandler;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.ParserXMLScript;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOSistemaProjetosVersaoSistemaProdutoMobile;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Database.TIPO_COMANDO_BANCO;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class ControladorAtualizadorVersao extends InterfaceControlador {

	static boolean estadoSetado = false;	
	Integer mIdSPVSPMNovaVersaoDaApk = null;
	ESTADO estado = null;
	Integer vetorIdVersao[];
	
	
	public static enum ESTADO {
		NAO_INICIALIZADO(0), DOWNLOAD_APK(1), INSTALANDO_APK(2), APK_INSTALADA(3), ATUALIZANDO_VERSAO_BANCO(4), FINALIZADO(5), TRATAMENTO_EXCECAO_DE_ATUALIZACAO_DE_BANCO(6);

		int id;

		ESTADO(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}
		public static ESTADO getEstadoDoId(int id) {
			ESTADO[] vetor = ESTADO.values();
			for (int i = 0; i < vetor.length; i++) {
				ESTADO e = vetor[i];
				if (e.getId() == id)
					return e;
			}
			return null;
		}
	}

	public ControladorAtualizadorVersao(Context pContext) {
		super(pContext);
		carrega();
	}
		
	public Integer getIdSPVSPMNovaVersaoDaApk() {
		return mIdSPVSPMNovaVersaoDaApk;
	}
//
//	public boolean persistir(Integer[] pVetorVersao) {
//		if (pVetorVersao == null || pVetorVersao.length == 0){
//			persistirEstado(ESTADO.NAO_INICIALIZADO);
//			return false;
//		}
//		else if (pVetorVersao.length > 0) {
//			persistirEstado(ESTADO.DOWNLOAD_APK);
//			
//			BibliotecaNuvemSharedPreference
//				.saveValor(
//					context,
//					BibliotecaNuvemSharedPreference.TIPO_INT.ESTADO_CONTROLADOR_ATUALIZADOR_VERSAO,
//					estado.getId());
//			
//			BibliotecaNuvemSharedPreference
//				.saveValor(
//						context,
//						BibliotecaNuvemSharedPreference.TIPO_VETOR_INT.VETOR_NOVA_VERSAO_SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID,
//						pVetorVersao);
//
//			BibliotecaNuvemSharedPreference.saveValor(context,
//					TIPO_INT.PONTEIRO_ATUALIZADOR_BANCO, 0);
//			
//			
//			BibliotecaNuvemSharedPreference.saveValor(context,
//					TIPO_INT.ID_SPVSPM_VERSAO_A_SER_INSTALADA,  pVetorVersao[pVetorVersao.length - 1 ]);
//			return true;
//		} else
//			return false;
//
//	}
	public boolean persistir(Integer[] pVetorVersao) {
		if (pVetorVersao == null || pVetorVersao.length == 0){
			persistirEstado(ESTADO.NAO_INICIALIZADO);
			return false;
		}
		else if (pVetorVersao.length > 0) {
			persistirEstado(ESTADO.DOWNLOAD_APK);
			
			BibliotecaNuvemSharedPreference
				.saveValor(
					context,
					BibliotecaNuvemSharedPreference.TIPO_INT.ESTADO_CONTROLADOR_ATUALIZADOR_VERSAO,
					estado.getId());
			
			BibliotecaNuvemSharedPreference
				.saveValor(
						context,
						BibliotecaNuvemSharedPreference.TIPO_VETOR_INT.VETOR_NOVA_VERSAO_SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID,
						pVetorVersao);

			BibliotecaNuvemSharedPreference.saveValor(context,
					TIPO_INT.PONTEIRO_ATUALIZADOR_BANCO, 0);
			
			
			BibliotecaNuvemSharedPreference.saveValor(context,
					TIPO_INT.ID_SPVSPM_VERSAO_A_SER_INSTALADA,  pVetorVersao[pVetorVersao.length - 1 ]);
			return true;
		} else
			return false;

	}
	public void apagarPersistencia(){
		BibliotecaNuvemSharedPreference
		.saveValor(
				context,
				BibliotecaNuvemSharedPreference.TIPO_INT.ESTADO_CONTROLADOR_ATUALIZADOR_VERSAO,
				-1);
		
		BibliotecaNuvemSharedPreference
				.saveValor(
						context,
						BibliotecaNuvemSharedPreference.TIPO_VETOR_INT.VETOR_NOVA_VERSAO_SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID,
						null);

		BibliotecaNuvemSharedPreference.saveValor(context,
				TIPO_INT.PONTEIRO_ATUALIZADOR_BANCO, -1);
		
		BibliotecaNuvemSharedPreference.saveValor(context,
				TIPO_INT.ID_SPVSPM_VERSAO_A_SER_INSTALADA, -1 );
		
		BibliotecaNuvemSharedPreference.saveValor(
				context, 
				BibliotecaNuvemSharedPreference.TIPO_BOOLEAN.RESETAR_VERSAO,
				false);
		
	}
	
	public void persistirEstado(ESTADO estado){
		this.estado = estado;
		
		BibliotecaNuvemSharedPreference.saveValor(context,
				TIPO_INT.ESTADO_CONTROLADOR_ATUALIZADOR_VERSAO,
				this.estado.getId());
	}
	
	public boolean existeAtualizacaoEmAndamento(){
		int estadoId = BibliotecaNuvemSharedPreference
				.getValor(
						context,
						BibliotecaNuvemSharedPreference.TIPO_INT.ESTADO_CONTROLADOR_ATUALIZADOR_VERSAO,
						-1);
		if(estadoId == -1) return false;
		else if(estado == ESTADO.NAO_INICIALIZADO) return false;
		else return true;
			
	}
	
	private ESTADO carrega(){
		vetorIdVersao = BibliotecaNuvemSharedPreference
				.getValor(
						context,
						BibliotecaNuvemSharedPreference.TIPO_VETOR_INT.VETOR_NOVA_VERSAO_SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID);
		if(vetorIdVersao == null || vetorIdVersao.length == 0){
			persistirEstado(ESTADO.NAO_INICIALIZADO);
		} else {
			
			int estadoId = BibliotecaNuvemSharedPreference
					.getValor(
							context,
							BibliotecaNuvemSharedPreference.TIPO_INT.ESTADO_CONTROLADOR_ATUALIZADOR_VERSAO,
							-1);
			estado = ESTADO.getEstadoDoId(estadoId);
			if(estado == null)
				persistirEstado(ESTADO.TRATAMENTO_EXCECAO_DE_ATUALIZACAO_DE_BANCO);
			else{
				
				mIdSPVSPMNovaVersaoDaApk = BibliotecaNuvemSharedPreference
						.getValor(
								context,
								BibliotecaNuvemSharedPreference.TIPO_INT.ID_SPVSPM_VERSAO_A_SER_INSTALADA,
								-1);
				if (mIdSPVSPMNovaVersaoDaApk == -1){
					mIdSPVSPMNovaVersaoDaApk = null;
					persistirEstado(ESTADO.TRATAMENTO_EXCECAO_DE_ATUALIZACAO_DE_BANCO);
					
				} 
			}
		}
		
		return estado;
	}
	
	
	
	public ESTADO processa() {
		
		
		switch (estado) {	
		case DOWNLOAD_APK:

				String vPathArquivo = BOSistemaProjetosVersaoSistemaProdutoMobile.transfereArquivoWebParaAndroidDoPrograma(
						context, 
						BibliotecaNuvemConfiguracao.NOME_ARQUIVO_ZIPADO_APK, 
						String.valueOf( vetorIdVersao[vetorIdVersao.length - 1])
					);
					
				if(vPathArquivo == null){
					persistirEstado(ESTADO.FINALIZADO);
				} else{
					persistirEstado(ESTADO.INSTALANDO_APK);
				}	
				
		case INSTALANDO_APK:
			
			//TODO original, retirar o trecho abaixo, substituir pelo comentario
//			if(estado == ESTADO.INSTALANDO_APK)
//				persistirEstado(ESTADO.ATUALIZANDO_VERSAO_BANCO);
			
			//persistirEstado(ESTADO.APK_INSTALADA);
			break;
		case APK_INSTALADA:
			//carega xml instrucoes
			if(estado == ESTADO.APK_INSTALADA)
				persistirEstado(ESTADO.ATUALIZANDO_VERSAO_BANCO);
			
		case ATUALIZANDO_VERSAO_BANCO:
			if(estado == ESTADO.ATUALIZANDO_VERSAO_BANCO){
				FilaVersao mFilaVersao = null;
				mFilaVersao = new FilaVersao( vetorIdVersao, context);
				if (!mFilaVersao.eof()) {
					mFilaVersao.apagaPersistencia();
					persistirEstado(ESTADO.FINALIZADO);
				} else{
					if(executaProcessoDeAtualizacaoDoBanco(mFilaVersao)){
						persistirEstado(ESTADO.FINALIZADO);	
					} else{
						persistirEstado(ESTADO.TRATAMENTO_EXCECAO_DE_ATUALIZACAO_DE_BANCO);
					}
				}
			}
			//Nao para, o processo eh continuo ate o final
		case FINALIZADO:
			if(estado == ESTADO.FINALIZADO){
				FilaVersao mFilaVersao2 = null;
				mFilaVersao2 = new FilaVersao( vetorIdVersao, context);
				apagarPersistencia();
				mFilaVersao2.apagaPersistencia();
				
				ControladorBibliotecaNuvem.carregaXMLConfiguracao(context, true);
				
				break;
			}
			
		case TRATAMENTO_EXCECAO_DE_ATUALIZACAO_DE_BANCO:
			if(estado == ESTADO.TRATAMENTO_EXCECAO_DE_ATUALIZACAO_DE_BANCO){
				//reseta o banco de dados
				//TODO rever processo
//				RotinaSincronizador.resetar(context, true);
//				ControladorBibliotecaNuvem.carregaXMLConfiguracao(context, true);
				
			}			
			break;
		default:
			SingletonLog.insereErro(new Exception("Tipo nuo identificado: " + estado.toString()), TIPO.BIBLIOTECA_NUVEM);
			break;
		}
		return estado;
	}

	

	

	public ESTADO getEstado() {
		return estado;

	}
	public void atualizaEstadoParaApkInstalada(){
		estado = ESTADO.APK_INSTALADA;
		setaIndicadorResentado();
	}
	
	public ESTADO atualizaEstadoParaInstalandoApkSeNecessario(){
		ControladorBibliotecaNuvem.carregaXMLConfiguracao(context, false);
		Integer idSPVSPMASerInstalada = getIdSPVSPMNovaVersaoDaApk();
		String idSPVSPMInstalada = BibliotecaNuvemSharedPreference.getValor(
				context, 
				BibliotecaNuvemSharedPreference.TIPO_STRING.SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID) ;
		Boolean resetando = BibliotecaNuvemSharedPreference.getValor(
				context, 
				BibliotecaNuvemSharedPreference.TIPO_BOOLEAN.RESETAR_VERSAO);
		resetando = resetando == null ? false : resetando;
		
		//so verifica uma vez na vida a questao do reset
		if(!resetando){
			if( idSPVSPMASerInstalada != null && idSPVSPMInstalada != null){
				if(idSPVSPMASerInstalada.toString().compareTo(idSPVSPMInstalada) ==0){
					estado = ESTADO.APK_INSTALADA;
					return estado;
				}
					
			}
			estado = ESTADO.INSTALANDO_APK;
		} else{
			BibliotecaNuvemSharedPreference.saveValor(
					context, 
					BibliotecaNuvemSharedPreference.TIPO_BOOLEAN.RESETAR_VERSAO,
					false);
			estado = ESTADO.INSTALANDO_APK;
		}
		
		
		return estado;
		
	}
	
	public void setaIndicadorResentado(){
		BibliotecaNuvemSharedPreference.saveValor(
				context, 
				BibliotecaNuvemSharedPreference.TIPO_BOOLEAN.RESETAR_VERSAO,
				false);
	}
	
	private boolean executaProcessoDeAtualizacaoDoBanco(FilaVersao pFilaVersao) {
		InterfaceItemXML itemXMLSCB = null;
		Exception erro = null;
		Database db = null;
		try{
			//BibliotecaNuvemArvoreArquivo vObj = new BibliotecaNuvemArvoreArquivo();
			
			ItemXMLHandler item = ParserXMLScript.parse(context, BibliotecaNuvemConfiguracao.XML_INSTRUCOES_ATUALIZACAO);
			if(item == null) return true;
			ArrayList<InterfaceItemXML> listVersao = item.getList(TIPO_ITEM.versao);
			ItemScriptVersao itemAux = new ItemScriptVersao();
			int idVersaoAtual = BibliotecaNuvemSharedPreference.getValor(context, TIPO_INT.VERSAO_ATUAL_DO_SISTEMA, -1);
			if(idVersaoAtual != -1){
				pFilaVersao.atualizaPonteiro(idVersaoAtual);
			} 
			while (pFilaVersao.getVersaoDoPonteiro() != null ) {
				
				// atualizando estrutura do banco
				Integer vIdVersao = pFilaVersao.getVersaoDoPonteiro();
				if (vIdVersao != null) {
					InterfaceItemXML itemXML= itemAux.searchItem(listVersao, ItemScriptVersao.ATRIBUTO.sistema_projetos_versao_sistema_produto_mobile, vIdVersao.toString());
					ItemScriptVersao iterador  = (ItemScriptVersao) itemXML;
					String arquivo = iterador.getAtributo(ItemScriptVersao.ATRIBUTO.arquivo_atualizacao_estrutura);
					if(arquivo != null && arquivo.length() > 0){ 
						
						ItemXMLHandler itemScriptBanco = ParserXMLScript.parse(context, arquivo);
						ArrayList<InterfaceItemXML> listScriptComandoBanco = itemScriptBanco.getList(TIPO_ITEM.script_comando_banco);
						//ArrayList<InterfaceItemXML> listScriptProjetosVersaoBancoBanco = itemScriptBanco.getList(TIPO_ITEM.script_projetos_versao_banco_banco);
						
						try{
							db = new DatabasePontoEletronico(context);
							for(int i =0; i< listScriptComandoBanco.size(); i++){
								itemXMLSCB = listScriptComandoBanco.get(i);
								String consulta = itemXMLSCB.getAtributo(ItemScripComandoBanco.ATRIBUTO.consulta);
								String idTipo = itemXMLSCB.getAtributo(ItemScripComandoBanco.ATRIBUTO.tipo_comando_banco_id_INT);
								
								TIPO_COMANDO_BANCO tipo = Database.getTipoComandoBanco(HelperInteger.parserInt(idTipo));
								if(tipo == null){
									erro = new Exception("Id tipo de comando de banco nuo identificado.");
									return false;
								}
								db.queryUpdateStructure(tipo, consulta);
							}
						} catch(Exception ex){
							
							erro = ex;
							return false;
						} 
					}
				}

				if (pFilaVersao.existeProximaVersao()){
					pFilaVersao.proxima();
					Integer versaoAtual = pFilaVersao.getVersaoDoPonteiro();
					BibliotecaNuvemSharedPreference.saveValor(context, TIPO_INT.VERSAO_ATUAL_DO_SISTEMA, versaoAtual);
				} else {
					pFilaVersao.finaliza(); 
					break;
				}
				
			}
			return true;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
			return false;
		}finally{
			
			
			if(itemXMLSCB != null && erro != null){
				//registra o erro da chamada relacionando ao scriptComando
								
				SingletonLog.insereErro(erro, TIPO.BIBLIOTECA_NUVEM);
				
			} else if(erro != null){
				SingletonLog.insereErro(erro, TIPO.BIBLIOTECA_NUVEM);
			}
			if(pFilaVersao != null)
			pFilaVersao.apagaPersistencia();
			
			if(db != null)
				 db.close();
		}
		

	}



}
