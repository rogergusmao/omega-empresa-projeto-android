package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.parametroglobal.ItemXMLParametroGlobal;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.parametroglobal.ParserXMLParametroGlobal;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.Servicos;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemInteger;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemVetorInteger;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;


public class ControladorBibliotecaNuvem extends InterfaceControlador{
	
	public enum ESTADO{
		VERSAO_ANTIGA,
		OK,
		ERRO_DURANTE_A_CHECAGEM_DE_VERSAO,
		NAO_VERIFICADO,
		VERSAO_SEM_VERSIONAMENTO
	}
	
	public enum ACAO{
		ATUALIZAR_VERSAO,
		NAO_FAZER_NADA
	}
	
	ESTADO mEstado = ESTADO.NAO_VERIFICADO;
	
	
	public static String TAG = "ControladorBibliotecaNuvem";
	
	public ControladorBibliotecaNuvem(Context pContext){
		super(pContext);
		
	}
	
	
	public ContainerAcao procedimentoVerificandoExistenciaNovaVersaoDoSistema(Context pContext){
		try{
//			if(!BibliotecaNuvemConfiguracao.checaSeOSistemaEstaNoAr(pContext))
//				return new ContainerAcao(ACAO.NAO_FAZER_NADA);
			
			ControladorAtualizadorVersao controladorAtualizadorVersao = new ControladorAtualizadorVersao(pContext);
			
			
			if(controladorAtualizadorVersao.existeAtualizacaoEmAndamento()){
				return new ContainerAcao(ACAO.ATUALIZAR_VERSAO);
			} else{
				ContainerEstado vContainerEstado = checaVersaoDoSistema(pContext);
				
				switch (vContainerEstado.mEstado) {
				case VERSAO_ANTIGA:
					controladorAtualizadorVersao.persistir( vContainerEstado.mVetorNovaVersaoApk);
					return new ContainerAcao(ACAO.ATUALIZAR_VERSAO);
				case OK:
					break;
				case VERSAO_SEM_VERSIONAMENTO:
				case ERRO_DURANTE_A_CHECAGEM_DE_VERSAO:
					break;
				default:
					break;
				}
			}
			
			
			return new ContainerAcao(ACAO.NAO_FAZER_NADA);
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
			return new ContainerAcao(ACAO.NAO_FAZER_NADA);
		}
		
	}
	
	
	
	private ContainerEstado checaVersaoDoSistema(Context pContext){
		String vIdSistemaProdutoMobile =  null;
		String vIdSistemaProjetosVersaoSistemaProdutoMobile = null;
		
		carregaXMLConfiguracao(pContext, false);
		try{
			vIdSistemaProjetosVersaoSistemaProdutoMobile = BibliotecaNuvemSharedPreference.getValor(
					pContext, 
					BibliotecaNuvemSharedPreference.TIPO_STRING.SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID) ;
			vIdSistemaProdutoMobile = BibliotecaNuvemSharedPreference.getValor(
					pContext, 
					BibliotecaNuvemSharedPreference.TIPO_STRING.SISTEMA_PRODUTO_MOBILE_ID) ;
		}catch(Exception ex){
			//A versao eh antiga, e nao possuia a biblioteca nuvem antes
			vIdSistemaProjetosVersaoSistemaProdutoMobile = null;
		}
		
		Integer vIdVetorVersaoSistema[] = null;
		
//		if(!BibliotecaNuvemConfiguracao.checaSeOSistemaEstaNoAr(pContext)) 
//			return new ContainerEstado(ESTADO.ERRO_DURANTE_A_CHECAGEM_DE_VERSAO);
		
		Sessao.getSessao(pContext, OmegaSecurity.getIdCorporacao(), OmegaSecurity.getIdUsuario(), OmegaSecurity.getCorporacao());
		
		InterfaceMensagem msg = Servicos.consultaListaVersao(
				pContext, 
				vIdSistemaProdutoMobile,
				vIdSistemaProjetosVersaoSistemaProdutoMobile);
		if(msg == null )
			return new ContainerEstado(ESTADO.ERRO_DURANTE_A_CHECAGEM_DE_VERSAO);
		else if(msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REINSTALAR_VERSAO.getId()){
			BibliotecaNuvemSharedPreference.saveValor(
					context, 
					BibliotecaNuvemSharedPreference.TIPO_BOOLEAN.RESETAR_VERSAO,
					true);
			MensagemInteger msgInteger = (MensagemInteger)msg;
			return new ContainerEstado(ESTADO.VERSAO_ANTIGA, null, new Integer[]{msgInteger.mToken});
		}
		else if(msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()){
			MensagemVetorInteger msgVetor = (MensagemVetorInteger)msg;
			vIdVetorVersaoSistema = msgVetor.mVetorToken;
			if(vIdVetorVersaoSistema == null)
				return new ContainerEstado(ESTADO.ERRO_DURANTE_A_CHECAGEM_DE_VERSAO);
			else if(vIdVetorVersaoSistema.length == 0)
				return new ContainerEstado(ESTADO.OK);
			else{
				
				
				String vStrIdAtualVersao = BibliotecaNuvemSharedPreference.getValor(
						pContext, 
						BibliotecaNuvemSharedPreference.TIPO_STRING.SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID) ;
				Integer vIdAtualVersaoSistema =  HelperInteger.parserInteger(vStrIdAtualVersao);
				
				if(vIdAtualVersaoSistema == null)
					return new ContainerEstado(ESTADO.VERSAO_ANTIGA, null, vIdVetorVersaoSistema);
				else if(vIdVetorVersaoSistema.length > 0)
					return new ContainerEstado(ESTADO.VERSAO_ANTIGA, vIdAtualVersaoSistema, vIdVetorVersaoSistema);
				else return new ContainerEstado(ESTADO.OK);
				
			}	
		} else return new ContainerEstado(ESTADO.ERRO_DURANTE_A_CHECAGEM_DE_VERSAO);
		
		
	}

	
	static boolean XMLCarregado = false;
	public static void carregaXMLConfiguracao(Context pContext, boolean sobrescreve){
//		boolean vXMLCarregado = BibliotecaNuvemSharedPreference.getValor(pContext, BibliotecaNuvemSharedPreference.TIPO_BOOLEAN.XML_CONFIGURACAO_CARREGADO);
		if(!XMLCarregado || sobrescreve){
			ItemXMLParametroGlobal vItem = ParserXMLParametroGlobal.getItemXMLParametroGlobalFromAssets(pContext);
			BibliotecaNuvemSharedPreference.saveValor(
					pContext, 
					BibliotecaNuvemSharedPreference.TIPO_STRING.PROJETOS_ID, 
					vItem.getProjetosId()) ;
			
			BibliotecaNuvemSharedPreference.saveValor(
					pContext, 
					BibliotecaNuvemSharedPreference.TIPO_STRING.SISTEMA_PROJETOS_VERSAO_ID, 
					vItem.getSistemaProjetosVersaoId()) ;
			
			BibliotecaNuvemSharedPreference.saveValor(
					pContext, 
					BibliotecaNuvemSharedPreference.TIPO_STRING.SISTEMA_PROJETOS_VERSAO_PRODUTO_ID, 
					vItem.getSistemaProjetosVersaoProdutoId()) ;
			
			BibliotecaNuvemSharedPreference.saveValor(
					pContext, 
					BibliotecaNuvemSharedPreference.TIPO_STRING.PROGRAMA_ARQUIVO, 
					vItem.programa_arquivo) ;
			
			BibliotecaNuvemSharedPreference.saveValor(
					pContext, 
					BibliotecaNuvemSharedPreference.TIPO_STRING.SISTEMA_ID, 
					vItem.getSistemaId()) ;
			
			BibliotecaNuvemSharedPreference.saveValor(
					pContext, 
					BibliotecaNuvemSharedPreference.TIPO_STRING.SISTEMA_PRODUTO_ID, 
					vItem.getSistemaProdutoId());
			
			
			BibliotecaNuvemSharedPreference.saveValor(
					pContext, 
					BibliotecaNuvemSharedPreference.TIPO_STRING.SISTEMA_PRODUTO_MOBILE_ID, 
					vItem.getSistemProdutoMobileId());
			
			BibliotecaNuvemSharedPreference.saveValor(
					pContext, 
					BibliotecaNuvemSharedPreference.TIPO_STRING.SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID, 
					vItem.getSistemaProjetosVersaoSistemaProdutoMobileId());

			BibliotecaNuvemSharedPreference.saveValor(
					pContext, 
					BibliotecaNuvemSharedPreference.TIPO_BOOLEAN.XML_CONFIGURACAO_CARREGADO, 
					true);
			
			XMLCarregado = true;
		}
	}

	private class ContainerEstado{
		public ESTADO mEstado;
		public Integer[] mVetorNovaVersaoApk;
		//public Integer mVersaoAntigaApk;
		
		public ContainerEstado(
				ESTADO pEstado, 
				Integer pVersaoAntigaApk,
				Integer[] pVetorNovaVersaoApk){
			mEstado = pEstado;
			//mVersaoAntigaApk = pVersaoAntigaApk;
			mVetorNovaVersaoApk = pVetorNovaVersaoApk;
		}
		
		
		public ContainerEstado(
				ESTADO pEstado){
			mEstado = pEstado;
		}
		
	}
	
	public class ContainerAcao{
		public ACAO mAcao;

		public ContainerAcao(
				ACAO pAcao){
			mAcao = pAcao;
		}
	}
}
