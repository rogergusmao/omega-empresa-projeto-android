package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOControladorMobile;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOOperacaoSistemaMobile;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.entidade.ProtocoloOperacao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador.STATE_SINCRONIZADOR;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;

public class ControladorOperacaoSistema extends InterfaceControlador {

	public static String TAG = "ControladorOperacaoSistema";

	public enum ESTADO {

		VERIFICANDO_OPERACOES_ABERTAS_NO_BANCO_LOCAL, VERIFICANDO_NOVAS_OPERACOES_DE_SISTEMA_NO_BANCO_WEB, PROCESSANDO_OPERACAO_DE_SISTEMA, FINALIZANDO, 
		CICLO
	}

	ESTADO mEstado = ESTADO.VERIFICANDO_OPERACOES_ABERTAS_NO_BANCO_LOCAL;

	String mIdUsuario;
	String mIdCorporacao;
	BOControladorMobile mControladorMobile;
	FilaOperacaoSistema mFilaOS;
	String idOSMCiclo = null;
	static ControladorOperacaoSistema singleton;
	static Intent serviceMonitoraView = null;

	public void destructSingleton() {
		mFilaOS.mVetorId.clear();

		paraServicoMonitoraView();
	}

	public void paraServicoMonitoraView() {
		if (serviceMonitoraView != null) {
			context.stopService(serviceMonitoraView);
			serviceMonitoraView = null;
		}
	}

	private ControladorOperacaoSistema(Context pContext) {
		super(pContext);

		mFilaOS = new FilaOperacaoSistema(new ArrayList<String>());

	}

	public boolean inicializaServicoMonitoraView(String idOSM) {
		if (serviceMonitoraView == null) {
			serviceMonitoraView = new Intent(context, ServiceMonitoraView.class);

			serviceMonitoraView.putExtra(
					OmegaConfiguration.SEARCH_FIELD_ID_OPERACAO_SISTEMA_MOBILE,
					idOSM);
			context.startService(serviceMonitoraView);
			return true;
		} else
			return false;
	}

	public static ControladorOperacaoSistema getSingleton() {
		return singleton;
	}

	public static ControladorOperacaoSistema factory(Context pContext) {
		if (singleton != null)
			return singleton;
		else {
			singleton = new ControladorOperacaoSistema(pContext);
		}
		return singleton;
	}

	private class FilaOperacaoSistema {

		public ArrayList<String> mVetorId;

		public FilaOperacaoSistema(ArrayList<String> pVetorId) {

			mVetorId = pVetorId;
			if (mVetorId == null)
				mVetorId = new ArrayList<String>();
		}

		@SuppressWarnings("unused")
		protected int getTamanhoBuffer() {
			if (mVetorId == null)
				return 0;
			else if (mVetorId.size() == 0)
				return 0;
			else
				return mVetorId.size();
		}

		@SuppressWarnings("unused")
		public void adiciona(String pId) {
			mVetorId.add(pId);
		}

		public String proximo() {
			if (mVetorId == null)
				return null;
			else if (mVetorId.size() == 0)
				return null;

			String vValor = mVetorId.get(0);
			mVetorId.remove(0);

			return vValor;

		}

		public String proximoSemRetirar() {
			if (mVetorId == null)
				return null;
			else if (mVetorId.size() == 0)
				return null;

			String vValor = mVetorId.get(0);

			return vValor;

		}

		public boolean existeProximo() {
			if (mVetorId == null)
				return false;
			else if (mVetorId.size() == 0)
				return false;
			else
				return true;
		}

	}

	public void finaliza() {
		mEstado = ESTADO.FINALIZANDO;
	}

	static ReentrantLock reentrantLock = new ReentrantLock();

	public void executa(String pIdCorporacao, String pIdUsuario, String pCorporacao) {
		if (reentrantLock.tryLock()){

			
			Database db = null;
			try {
				STATE_SINCRONIZADOR state= RotinaSincronizador.getStateSincronizador(context, null);
				if(state == STATE_SINCRONIZADOR.RESETAR || state == STATE_SINCRONIZADOR.BANCO_AINDA_NAO_CRIADO)
					return;
				
				ProtocoloConectaTelefone protocolo = Sessao.getSessao(
						context,
						pIdCorporacao, 
						pIdUsuario,
						pCorporacao);
				if (protocolo == null) {
					return;
				}
				db = new DatabasePontoEletronico(context);
				if(!db.isDatabaseCreated())
					return;
				switch (mEstado) {
				
				case CICLO:
					break;
				case VERIFICANDO_OPERACOES_ABERTAS_NO_BANCO_LOCAL:
					int vTotalOSAbertas = procedimentoVerificaOperacoesAbertasNoBancoLocal(db);
					if (vTotalOSAbertas == 0)
						mEstado = ESTADO.VERIFICANDO_NOVAS_OPERACOES_DE_SISTEMA_NO_BANCO_WEB;
					else
						mEstado = ESTADO.PROCESSANDO_OPERACAO_DE_SISTEMA;
					break;

				case VERIFICANDO_NOVAS_OPERACOES_DE_SISTEMA_NO_BANCO_WEB:
					int vTotalOSAbertas2 = procedimentoVerificandoNovasOperacoesDeSistemaNoBancoWeb(
							db, protocolo);
					if (vTotalOSAbertas2 == 0){
						mEstado = ESTADO.VERIFICANDO_NOVAS_OPERACOES_DE_SISTEMA_NO_BANCO_WEB;
						break;
					}
					else
						mEstado = ESTADO.PROCESSANDO_OPERACAO_DE_SISTEMA;
					
				case PROCESSANDO_OPERACAO_DE_SISTEMA:
					ESTADO_OPERACAO_SISTEMA_MOBILE estadoOSM = procedimentoProcessandoOperacaoDeSistema(
							db, protocolo);
					if (estadoOSM == null)
						estadoOSM = ESTADO_OPERACAO_SISTEMA_MOBILE.CANCELADA;
					switch (estadoOSM) {
					case PROCESSANDO:
						mEstado = ESTADO.CICLO;
						break;
					case AGUARDANDO:
					case CANCELADA:
					case CONCLUIDA:
					case ERRO_EXECUCAO:

						if (mFilaOS == null || !mFilaOS.existeProximo())
							mEstado = ESTADO.VERIFICANDO_NOVAS_OPERACOES_DE_SISTEMA_NO_BANCO_WEB;
						else
							mEstado = ESTADO.PROCESSANDO_OPERACAO_DE_SISTEMA;
						break;

					default:
						break;
					}

					break;


				case FINALIZANDO:

					break;

				default:
					break;
				}
			}
			catch(SQLiteException sqlEx){
//				if(sqlEx.getMessage().contains("code 17")){
//					if(db == null)
//						try {
//							db = new DatabasePontoEletronico(context);
//							db.close(true);
//						} catch (OmegaDatabaseException e) {
//							SingletonLog.insereErro(e, TIPO.BIBLIOTECA_NUVEM);
//						}
//				}
				
				SingletonLog.insereErro(sqlEx, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			}
			catch (Exception ex) {
				SingletonLog.insereErro(ex, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			} finally {
				
				if (db != null)
					db.close();
				
				reentrantLock.unlock();
			}			
		}
			

	}

	public void finalizaCiclo(ProtocoloConectaTelefone protocolo,
			String idEstadoOperacaoSistemaMobile) {

		if (mFilaOS == null || !mFilaOS.existeProximo())
			mEstado = ESTADO.VERIFICANDO_NOVAS_OPERACOES_DE_SISTEMA_NO_BANCO_WEB;
		else
			mEstado = ESTADO.PROCESSANDO_OPERACAO_DE_SISTEMA;

		if (protocolo != null) {
			BOOperacaoSistemaMobile
					.atualizaEstado(
							context,
							protocolo
									.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador),
							protocolo
									.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileConectado),
							idOSMCiclo, idEstadoOperacaoSistemaMobile);

		}
		idOSMCiclo = null;

	}

	private ESTADO_OPERACAO_SISTEMA_MOBILE procedimentoProcessandoOperacaoDeSistema(
			Database db, ProtocoloConectaTelefone protocolo) {

		try {
			if (protocolo == null)
				return null;
			else if (mFilaOS == null)
				return null;
			else if (!mFilaOS.existeProximo())
				return null;
			String vId = null;
			vId = mFilaOS.proximoSemRetirar();
			try {
				if (vId == null) {
					throw new Exception("Id deveria estar preenchido");
				}
				EXTDAOSistemaOperacaoSistemaMobile obj = new EXTDAOSistemaOperacaoSistemaMobile(
						db);
				obj.select(vId);
				BOOperacaoSistemaMobile boOSM = obj.getObjBO();
				if(boOSM == null){
					obj.remove(false);
				}
				String idEOSMEstadoProcessando = EXTDAOSistemaOperacaoSistemaMobile
						.getIdEstadoOperacaoSistemaMobile(EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE.PROCESSANDO);
				BOOperacaoSistemaMobile
						.atualizaEstado(
								context,
								protocolo
										.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador),
								protocolo
										.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileConectado),
								vId, idEOSMEstadoProcessando);

				EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE novoEstado = boOSM
						.processa();

				if (novoEstado == null) {
					return null;
				} else {
					if (novoEstado == ESTADO_OPERACAO_SISTEMA_MOBILE.PROCESSANDO) {
						idOSMCiclo = vId;
						EXTDAOSistemaOperacaoSistemaMobile vObjOSM = new EXTDAOSistemaOperacaoSistemaMobile(
								db);
						vObjOSM.select(vId);
						vObjOSM.formatToSQLite();
						
						vObjOSM.remove(false);
						return novoEstado;
					} else {
						EXTDAOSistemaOperacaoSistemaMobile vObjOSM = new EXTDAOSistemaOperacaoSistemaMobile(
								db);
						vObjOSM.select(vId);
						String vIdEstado = EXTDAOSistemaOperacaoSistemaMobile
								.getIdEstadoOperacaoSistemaMobile(novoEstado);
						if (vIdEstado == null)
							throw new Exception("Estado inexistente");
						String strNovoEstado = EXTDAOSistemaOperacaoSistemaMobile
								.getIdEstadoOperacaoSistemaMobile(novoEstado);
						if (strNovoEstado == null)
							throw new Exception(
									"Id do estado nuo identificado.");
						vObjOSM.setAttrValue(
								EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE_ID_INT,
								strNovoEstado);
						vObjOSM.formatToSQLite();
						vObjOSM.update(false);
						BOOperacaoSistemaMobile
						.atualizaEstado(
								context,
								protocolo
										.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador),
								protocolo
										.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileConectado),
										vId, strNovoEstado);
						mFilaOS.proximo();
						return novoEstado;
					}

				}
			} catch (Exception ex) {
				if (vId != null) {
					EXTDAOSistemaOperacaoSistemaMobile obj = new EXTDAOSistemaOperacaoSistemaMobile(
							db);
					obj.select(vId);
					obj.remove(false);
					String idEOSMErroExecucao = EXTDAOSistemaOperacaoSistemaMobile
							.getIdEstadoOperacaoSistemaMobile(EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE.ERRO_EXECUCAO);
					BOOperacaoSistemaMobile
							.atualizaEstado(
									context,
									protocolo
											.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador),
									protocolo
											.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileConectado),
									vId, idEOSMErroExecucao);

					SingletonLog.insereErro(ex,
							SingletonLog.TIPO.BIBLIOTECA_NUVEM);

				}

				// Retira a OSM da fila de espera
				mFilaOS.proximo();
			}
		} catch (Exception ex) {

			SingletonLog.insereErro(ex, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	private int procedimentoVerificandoNovasOperacoesDeSistemaNoBancoWeb(
			Database db, ProtocoloConectaTelefone protocolo) {
		if (protocolo == null)
			return 0;

		ArrayList<ProtocoloOperacao> vetor = BOOperacaoSistemaMobile
				.consultaVetorOperacaoSistemaMobile(
						context,
						protocolo
								.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador),
						protocolo
								.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileConectado));

		if (vetor == null)
			return 0;
		else {
			ArrayList<String> listaIdSOSM = new ArrayList<String>();
			for (int i = 0; i < vetor.size(); i++) {
				ProtocoloOperacao protocoloOSM = vetor.get(i);
				protocoloOSM.persiste(db, false);
				listaIdSOSM.add(protocoloOSM.getIdOSM());

			}
			mFilaOS = new FilaOperacaoSistema(listaIdSOSM);
			return listaIdSOSM.size();
		}

	}

	private int procedimentoVerificaOperacoesAbertasNoBancoLocal(Database db) {
		
		EXTDAOSistemaOperacaoSistemaMobile vObj = new EXTDAOSistemaOperacaoSistemaMobile(
				db);
		ArrayList<String> vListaIdOS = vObj
				.getListaIdNoEstado(new String[] { EXTDAOSistemaOperacaoSistemaMobile
						.getIdEstadoOperacaoSistemaMobile(EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE.AGUARDANDO) });
		if (vListaIdOS == null)
			return 0;
		else {
			mFilaOS = new FilaOperacaoSistema(vListaIdOS);
			return vListaIdOS.size();
		}
	}

}
