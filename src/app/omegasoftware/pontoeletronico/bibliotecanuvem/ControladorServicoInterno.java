package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import android.app.Activity;
import android.content.Intent;
import app.omegasoftware.pontoeletronico.appconfiguration.ContainerPrograma;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;

public class ControladorServicoInterno extends InterfaceControlador{
	static Intent serviceOperacaoSistema = null;
	static Intent serviceSendLogErro = null;
	
	static ControladorServicoInterno controle = null;
	
	public static ControladorServicoInterno constroi(Activity pActivity){
		
		if(controle == null)
		controle = new ControladorServicoInterno(pActivity);
		return controle;
	}
	
	public static ControladorServicoInterno getSingleton(){
		
		return controle;
	}
	
	private ControladorServicoInterno(Activity pActivity){
		super(pActivity);
		context = pActivity;
	}
	
	public static enum  TYPE_SERVICO {
		OPERACAO_SISTEMA,
		SEND_LOG_ERRO
	};

	public void stopService(TYPE_SERVICO pTypeService){
		switch (pTypeService) {
		
			case OPERACAO_SISTEMA:
				//Starts the TruckTrackerService
				if(serviceOperacaoSistema != null){
					context.stopService( serviceOperacaoSistema);
					serviceOperacaoSistema = null;
				}
			break;
			case SEND_LOG_ERRO:
				if(serviceSendLogErro != null){
					context.stopService( serviceSendLogErro);
					serviceSendLogErro = null;
				}		
				break;
			default: break;
		}
	}
	
	public void startService(TYPE_SERVICO pTypeService){
	
		switch (pTypeService) {
		
		case OPERACAO_SISTEMA:
			//Starts the TruckTrackerService
			if(serviceOperacaoSistema == null 
				&& ContainerPrograma.isServicoPermitido(ServiceOperacaoSistemaMobile.ID)
				&& OmegaConfiguration.DEBUGGING_BIBLIOTECA_NUVEM ){
				serviceOperacaoSistema = new Intent(context, ServiceOperacaoSistemaMobile.class);
				context.startService(serviceOperacaoSistema);
			}
		break;
		
		case SEND_LOG_ERRO:
			
			if(serviceSendLogErro == null &&
				ContainerPrograma.isServicoPermitido(ServiceSendLogErro.ID) ){
				serviceSendLogErro = new Intent(context, ServiceSendLogErro.class);
				context.startService(serviceSendLogErro);
			}
			break;
		
		default: 
			break;
		}
			
	}
		

	public void startAllServices(){
		TYPE_SERVICO vVetorServico[] = TYPE_SERVICO.values();
		for (TYPE_SERVICO vTypeServico : vVetorServico) {
			startService(vTypeServico);
		}
	}
	
	public void stopAllServices(){
		TYPE_SERVICO vVetorServico[] = TYPE_SERVICO.values();
		for (TYPE_SERVICO vTypeServico : vVetorServico) {
			stopService(vTypeServico);
		}
	}

}
