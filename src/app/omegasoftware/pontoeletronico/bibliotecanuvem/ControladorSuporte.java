package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemSharedPreference.TIPO_STRING;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOControladorMobile;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloSuporte;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;



public class ControladorSuporte extends InterfaceControlador{
	
	public static String TAG = "ControladorSuporte";
	public enum ESTADO {
		INICIALIZANDO,
		VERIFICANDO_MODO,
		INCIALIZANDO_AMBIENTE_SUPORTE,
		AMBIENTE_SUPORTE,
		FINALIZANDO_AMBIENTE_SUPORTE
	}

	ESTADO mEstado = ESTADO.INICIALIZANDO;
	ProtocoloConectaTelefone mProtocolo;
	ProtocoloSuporte mProtocoloModoSuporte;
	String mIdUsuario;
	String mIdCorporacao;
	
	
	String mIdVersao;
	static boolean mEstruturaBancoCriada = false;
	public ControladorSuporte(Context pContext){
		super(pContext);
		
		mIdVersao = BibliotecaNuvemSharedPreference.getValor(pContext, TIPO_STRING.SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID);
		

	}
	
	private boolean inicializa(String idCorporacao, String idUsuario, String pNomeCorporacao) {
		
		mProtocolo = BOControladorMobile.conectaTelefone(context, idCorporacao, idUsuario, pNomeCorporacao);
		
		if(mProtocolo == null) return false;
		else return true;
	}
	

	public boolean isAtivo(){
		if(mEstado == ESTADO.INCIALIZANDO_AMBIENTE_SUPORTE || mEstado== ESTADO.AMBIENTE_SUPORTE){
			return true;
		} else return false;
	}
	
	
	
	private boolean modoAtivo(){
		mProtocoloModoSuporte = BOControladorMobile.modoSuporte(context, mProtocolo);
		if(mProtocoloModoSuporte == null) return false;
		else return true;
	}

	public void executa(String pIdCorporacao, String pIdUsuario, String pNomeCorporacao){
		Database db = null;
		
		try{
			db = new DatabasePontoEletronico(context);
			switch (mEstado) {
			case INICIALIZANDO:
				if(inicializa(pIdCorporacao, pIdUsuario, pNomeCorporacao)){
					mEstado = ESTADO.VERIFICANDO_MODO;	
				}
				
				break;
			case VERIFICANDO_MODO:
				if(modoAtivo()){
					mEstado = ESTADO.INCIALIZANDO_AMBIENTE_SUPORTE;
					
				}
				break;
			case INCIALIZANDO_AMBIENTE_SUPORTE:
				
				break;
			case AMBIENTE_SUPORTE:
				
				break;
			case FINALIZANDO_AMBIENTE_SUPORTE:

				break;

			default:
				break;
			}
		}catch(Exception ex){
			Log.e("executa", HelperExcecao.getDescricao(ex));
			SingletonLog.insereErro(ex, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
		}finally{
			if(db != null)
				db.close();
		}
	}


}
