package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import java.util.ArrayList;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemSharedPreference.TIPO_INT;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.instrucaoatualizacao.ItemScriptVersao;

public class FilaVersao {
	public int mPonteiro;
	public Integer[] mVetorIdVersao;
	public ArrayList<ItemScriptVersao> mVetorScriptVersao;
	Context context;
	public FilaVersao(Integer[] pVetorIdVersao, Context context) {
		mPonteiro = BibliotecaNuvemSharedPreference.getValor(context,
				TIPO_INT.PONTEIRO_ATUALIZADOR_BANCO, -1);
		if(mPonteiro <= -1 && pVetorIdVersao != null && pVetorIdVersao.length == 1){
			mPonteiro = 0;
		}
		mVetorIdVersao = pVetorIdVersao;
		this.context = context;

	}
	
	public void atualizaPonteiro(int idVersaoAtual){
		
		for(int i = 0 ; i < mVetorIdVersao.length; i++){
			if(mVetorIdVersao[i] == idVersaoAtual){
				mPonteiro = i;
				proxima();
				return ;
			}
		}
		 
	}
	
	public Integer getIdUltimaVersao(){
		if(mVetorIdVersao == null || mVetorIdVersao.length == 0) return null;
		else return mVetorIdVersao[mVetorIdVersao.length - 1];
	}
	public void apagaPersistencia(){
		// finaliza os dados


		BibliotecaNuvemSharedPreference.saveValor(context,
				TIPO_INT.PONTEIRO_ATUALIZADOR_BANCO, -1);
				
	}
	public void persiste() {

		
		// Atualiza o ponteiro
		BibliotecaNuvemSharedPreference.saveValor(context,
				TIPO_INT.PONTEIRO_ATUALIZADOR_BANCO, mPonteiro);
		
	}
	public void finaliza(){
		mPonteiro = mVetorIdVersao.length;
	}
	
	public boolean proxima() {
		if (mVetorIdVersao == null)
			return false;
		else if (mVetorIdVersao.length == 0)
			return false;
		else {

			if (mPonteiro < mVetorIdVersao.length) {
				mPonteiro += 1;
				persiste();
				return true;
			} else
				return false;
		}
		
	}

	public Integer getTamanhoBuffer() {
		if (mVetorIdVersao == null)
			return null;
		else if (mVetorIdVersao.length == 0)
			return null;
		else
			return mVetorIdVersao.length;
	}

	public Integer getVersaoDoPonteiro() {
		if (mVetorIdVersao == null)
			return null;
		else if (mVetorIdVersao.length == 0)
			return null;
		else if (mPonteiro < 0 || mPonteiro >= mVetorIdVersao.length)
			return null;
		else
			return mVetorIdVersao[mPonteiro];
	}
	public boolean eof(){
		if (mVetorIdVersao == null)
			return false;
		else if (mVetorIdVersao.length == 0)
			return false;
		else if (mPonteiro >= mVetorIdVersao.length)
			return false;
		else
			return true;
	}
	public boolean existeProximaVersao() {
		if (mVetorIdVersao == null)
			return false;
		else if (mVetorIdVersao.length == 0)
			return false;
		else if (mPonteiro + 1 >= mVetorIdVersao.length)
			return false;
		else
			return true;
	}

}