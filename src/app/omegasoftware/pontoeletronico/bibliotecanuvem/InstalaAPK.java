package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;

public class InstalaAPK {
	public void atualizaVersao(Activity pAcitivity, String pPathArquivoApk){
//		OmegaFileConfiguration vOFC = new OmegaFileConfiguration();
//		String vPath = vOFC.getPathFilesTemp();
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(new File(pPathArquivoApk)), "application/" + BibliotecaNuvemConfiguracao.PACOTE_APLICACAO);
		pAcitivity.startActivity(intent);
	}
	
	public void janelaDePermissaoDeInstalacaoDeAppForaDoAndroidMarket(Activity pAcitivty){
		int result = Settings.Secure.getInt(pAcitivty.getContentResolver(), Settings.Secure.INSTALL_NON_MARKET_APPS, 0);
		if (result == 0) {
		    // show some dialog here
		    // ...
		    // and may be show application settings dialog manually
		    Intent intent = new Intent();
		    intent.setAction(Settings.ACTION_APPLICATION_SETTINGS);
		    pAcitivty.startActivity(intent);
		}
	}
}
