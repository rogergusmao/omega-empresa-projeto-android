package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import android.content.Context;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoCrudAleatorio;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoDownloadBancoMobile;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoExecutaScript;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile;


public class InterfaceControlador {
	protected Context context;
	protected  static String TAG = "InterfaceControlador";
	private static Table[] vetorTabela = new Table[]{
//			new EXTDAOSistemaProdutoLogErro(null),
			new EXTDAOSistemaOperacaoSistemaMobile(null),
			new EXTDAOSistemaOperacaoDownloadBancoMobile(null),
			new EXTDAOSistemaOperacaoCrudAleatorio(null),
			new EXTDAOSistemaOperacaoExecutaScript(null)};
	
	public InterfaceControlador(Context pContext){
		context = pContext;
		//OBSOLETO, agora criamos a estrutura completa do banco
//		verificaEstruturaDoBanco();
	}
	
	public boolean verificaEstruturaDoBanco(){
		Database db = null;
		try{
			db = new DatabasePontoEletronico(context);
			for(int i = 0 ; i  < vetorTabela.length; i++){
				Table table = vetorTabela[i];
				if(!db.isTableCreated(table.getName())){
					db.createTableStructure(table.getName());
				}
			}
			
			return true;
		}catch(Exception ex){
			SingletonLog.insereErro(
					ex, 
					SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			return false;
		} finally{
			if(db != null)db.close();
		}
		
		
	}
	
	

	
}
