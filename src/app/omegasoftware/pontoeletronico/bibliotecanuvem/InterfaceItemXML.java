package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.instrucaoatualizacao.ItemScriptVersao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.scriptbanco.ItemScripComandoBanco;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.scriptbanco.ItemScriptProjetosVersaoBancoBanco;


 
public abstract class InterfaceItemXML {

	public enum TIPO_ITEM {
		script_projetos_versao_banco_banco(
				new ItemScriptProjetosVersaoBancoBanco()
		), 
		script_comando_banco(
				new ItemScripComandoBanco()
		), 
		versao(
				new ItemScriptVersao()
		), 
		;
		
		
		private InterfaceItemXML item;
		private TIPO_ITEM(InterfaceItemXML item) {
			this.item = item;
		}
	 
		
		public InterfaceItemXML factory(){
			return item.factory();
		}
	 
	}
	
	public InterfaceItemXML searchItem(ArrayList<InterfaceItemXML> list, Enum<?> chave, String valorProcurado){
		if(list == null || list.size() == 0) return null;
		else{
			for(int i = 0 ; i < list.size(); i++){
				InterfaceItemXML item = list.get(i);
				String attr = item.getAtributo(chave);
				if(attr != null && attr.compareTo(valorProcurado) == 0) 
					return item;
			}
			return null;
		}
	}
	
	public HashMap<Enum<?>, String> hashNomePorValor = new HashMap<Enum<?>, String>();
	public TIPO_ITEM tipo = null;
	
	public <E extends Enum<E>> InterfaceItemXML(TIPO_ITEM tipo, Class<E> atributos){
		this.tipo = tipo;
		for (Enum<E> enumVal: atributos.getEnumConstants()) {  
            
            hashNomePorValor.put(enumVal, null);
        }  
	}
	
	
	public void setAtributo(Enum<?> key, String valor){
		if(hashNomePorValor.containsKey(key)){
			hashNomePorValor.put(key, valor);
		}
	}
	
	public Enum<?> possuiItem(String pNome){
		Iterator<Enum<?>> it = hashNomePorValor.keySet().iterator();
		Enum<?> key;
		while(it.hasNext()){
			key = it.next();
			if(key.toString().compareTo(pNome) == 0) return key;
			
		}	
		return null;
	}
	
	public Set<Enum<?>> getEnum(){
		return hashNomePorValor.keySet();
	}
	
	public String getAtributo(Enum<?> key){
		
		if(hashNomePorValor.containsKey(key)){
			return hashNomePorValor.get(key);
		} else return null;
	}
	
	public abstract InterfaceItemXML factory();
}


