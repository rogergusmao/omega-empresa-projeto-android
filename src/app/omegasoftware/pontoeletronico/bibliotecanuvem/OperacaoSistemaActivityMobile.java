package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;

public class OperacaoSistemaActivityMobile  extends OmegaRegularActivity {
	
	//Constants
	public static final String TAG = "OperacaoSistemaActivityMobile";
	public static final int FORM_DIALOG_HABILITAR_PERMISSAO_INSTALACAO_APLICACAO_SEM_SER_DO_MARKET = 1;
	private TextView carregandoTextView;
	private ProgressBar progressBar;
	
	
//	ControladorAtualizadorVersao mControlador;
	
	Context context ;
	private Typeface customTypeFace;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.atualizar_apk_activity_mobile_layout);
		
//		mControlador = new ControladorAtualizadorVersao(this);
		this.customTypeFace = Typeface.createFromAsset(this.getAssets(),"trebucbd.ttf");

		new CustomDataLoader(this).execute();
	}

	public boolean checaPermissaoInstalacaoAplicativoSemSerDoMarket(){
		int result = Settings.Secure.getInt(getContentResolver(), Settings.Secure.INSTALL_NON_MARKET_APPS, 0);
		if(result == 0) return false;
		else return true;
	}
	
	@Override
	public boolean loadData() {
		
		return true;
	}

	public void formatarStyle(){
		((Button) findViewById(R.id.resetar_button)).setTypeface(this.customTypeFace);
		
		((TextView) findViewById(R.id.resetar_textview)).setTypeface(this.customTypeFace);
	}
	
	@Override
	public void initializeComponents() {
		
//	
		
		progressBar = (ProgressBar) findViewById(R.id.progress_bar);
		carregandoTextView = (TextView) findViewById(R.id.carregando_textview);
		carregandoTextView.setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
		progressBar.setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
	
		formatarStyle();
//		mControlador = new ControladorAtualizadorVersao(this);
//		if(estado == ESTADO_INSTALACAO_SEM_SER_DO_MARKET.NAO_HABILITADO && 
//				ContainerPrograma.isProgramaDaEmpresa()){
//			showDialog(FORM_DIALOG_HABILITAR_PERMISSAO_INSTALACAO_APLICACAO_SEM_SER_DO_MARKET);
//		} else{
//			new ControladorLoader(this).execute();
//		}
		return ;					
	}

	
	
	
	
	@Override
	public void onBackPressed(){
		Intent intent = getIntent();
		
		setResult(RESULT_OK, intent);
		finish();
	}
	
	@Override
	public void finish(){
		
		super.finish();
	}
	
	

	public void procedimentoStateAtualizaVersao(int resultCode){
		switch (resultCode) {
		case RESULT_CANCELED:
			
			break;
		case RESULT_FIRST_USER:
			
			break;
		case RESULT_OK:
			
			break;

		default:
			break;
		}
	}
	
	


}
