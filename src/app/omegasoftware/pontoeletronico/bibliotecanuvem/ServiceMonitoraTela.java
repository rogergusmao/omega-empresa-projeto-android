package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources.NotFoundException;
import android.os.IBinder;
import android.os.RemoteException;
import android.widget.Toast;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoDownloadArquivo;
import app.omegasoftware.pontoeletronico.file.FileHttpPost;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.IScreenshotProvider;

public class ServiceMonitoraTela extends Service {

	// Constants
	public static final int ID = 19;

	public static final String TAG = "ServiceMonitoraTela";

	private Timer webserviceTimer = new Timer();
	ScreenshotServiceProvider ssp;
	// Download button

	Context context;
	FileHttpPost filesHttpPost;
	String path;
	int seq = 0;

	static int numeroTentativas = 0;

	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}

	String idOSM;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		int vReturn = super.onStartCommand(intent, flags, startId);
		idOSM = intent
				.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_OPERACAO_SISTEMA_MOBILE);

		return vReturn;
	}

	@Override
	public void onDestroy() {
		unbindService(aslServiceConn);
		this.webserviceTimer.cancel();
		super.onDestroy();
	}

	@Override
	public void onCreate() {

		super.onCreate();
		Intent intent = new Intent();
		intent.setClass(this, ScreenshotServiceProvider.class);
		// intent.addCategory(Intent.ACTION_DEFAULT);
		bindService(intent, aslServiceConn, Context.BIND_AUTO_CREATE);

		setTimer();

	}

	private ServiceConnection aslServiceConn = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			

		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			aslProvider = IScreenshotProvider.Stub.asInterface(service);
		}
	};
	private IScreenshotProvider aslProvider = null;

	private File printScreen() {
		File screen = null;
		try {
			if (aslProvider == null) {
				Toast.makeText(ServiceMonitoraTela.this, "n_a",
						Toast.LENGTH_SHORT).show();

			} else if (!aslProvider.isAvailable()) {
				Toast.makeText(ServiceMonitoraTela.this, "native_na",
						Toast.LENGTH_SHORT).show();

			} else {
				String file = aslProvider.takeScreenshot();
				if (file == null) {
					Toast.makeText(ServiceMonitoraTela.this,
							"screenshot_error", Toast.LENGTH_SHORT).show();

				} else {
					Toast.makeText(ServiceMonitoraTela.this, "screenshot_ok",
							Toast.LENGTH_SHORT).show();
					// screen = BitmapFactory.decodeFile(file);
					screen = new File(file);

				}
			}
		} catch (NotFoundException e) {
			
			e.printStackTrace();
			SingletonLog.insereWarning(e, TIPO.BIBLIOTECA_NUVEM);

		} catch (RemoteException e) {
			SingletonLog.insereWarning(e, TIPO.BIBLIOTECA_NUVEM);

		} catch (Exception e) {
			SingletonLog.insereWarning(e, TIPO.BIBLIOTECA_NUVEM);

		}
		if (screen == null)
			numeroTentativas++;
		else if (screen != null)
			numeroTentativas = -1;
		return screen;
	}

	static boolean IS_EXECUTING = false;

	public void operacao() {
		if (IS_EXECUTING)
			return;
		IS_EXECUTING = true;
		File screenFile = null;
		try {

			screenFile = printScreen();
			if (screenFile != null) {
				if (!enviaPrintScreen(screenFile)) {
					enviaErroDaOperacao("Nao foi possivel enviar o screen da tela.");
				}
				seq++;
			} else {

				enviaErroDaOperacao("Nuo foi possuvel tirar o printscreen.");
			}

		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			enviaErroDaOperacao("Exceuuo ocorrida.");
		} finally {

			try {
				if (screenFile != null && screenFile.exists())
					screenFile.delete();
			} catch (Exception ex) {

			}

		}
		IS_EXECUTING = false;
	}

	//

	private void setTimer() {
		// Starts reporter timer
		this.webserviceTimer
				.scheduleAtFixedRate(
						new TimerTask() {
							@Override
							public void run() {
								operacao();
								// sendBroadCastToVeiculoMapActivity();
							}
						},
						BibliotecaNuvemConfiguracao.SERVICE_MONITORA_TELA_WEB_PARA_MOBILE_INITIAL_DELAY,
						BibliotecaNuvemConfiguracao.SERVICE_MONITORA_TELA_WEB_PARA_MOBILE_INTERVAL);
	}

	public boolean enviaPrintScreen(File file) {
		try {
			String strSeq = String.valueOf(seq);
			String mobileIdentificador = Sessao
					.getItem(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador);
			String vStrJson = filesHttpPost
					.uploadAndRemoveFile(
							file,
							new String[] { "id_na_tabela", "nome_tabela",
									"id_sistema_tipo_download_arquivo",
									"id_operacao_sistema_mobile",
									"id_mobile_identificador", "seq",
									"programa_aberto", "teclado_ativo" },
							new String[] {
									idOSM,
									"monitora_tela_mobile_para_web",
									EXTDAOSistemaTipoDownloadArquivo
											.getId(EXTDAOSistemaTipoDownloadArquivo.TIPO.DEFAULT),
									idOSM, mobileIdentificador, strSeq, "1",
									"0" });
			JSONObject vObj;
			try {
				if (vStrJson == null)
					return false;
				vObj = new JSONObject(vStrJson);
				int vCodRetorno = vObj.getInt("mCodRetorno");

				if (vCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
						.getId()) {
					return true;
				} else {
					return false;
				}
			} catch (JSONException e) {
				
				SingletonLog.insereErro(e, TIPO.BIBLIOTECA_NUVEM);
			}

		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
		}
		return false;
	}

	public boolean enviaErroDaOperacao(String motivoErro) {
		try {
			String strSeq = String.valueOf(seq);
			String mobileIdentificador = Sessao
					.getItem(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador);
			String vStrJson = HelperHttpPost
					.getConteudoStringPost(
							this,
							BibliotecaNuvemConfiguracao
									.getUrlWebService(BibliotecaNuvemConfiguracao.TIPO_REQUEST.falhaPrintScreenMobileParaWeb),
							new String[] { "id_operacao_sistema_mobile",
									"id_mobile_identificador", "seq",
									"programa_aberto", "teclado_ativo",
									"motivo_erro" }, new String[] { idOSM,
									mobileIdentificador, strSeq, "1", "0",
									motivoErro });

			JSONObject vObj;
			try {
				if (vStrJson == null || vStrJson.length() == 0)
					return false;
				vObj = new JSONObject(vStrJson);
				int vCodRetorno = vObj.getInt("mCodRetorno");

				if (vCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
						.getId()) {
					return true;
				} else {
					return false;
				}
			} catch (JSONException e) {
				
				SingletonLog.insereErro(e, TIPO.BIBLIOTECA_NUVEM);
			}

		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
		}
		return false;
	}
}
