package app.omegasoftware.pontoeletronico.bibliotecanuvem;


import java.io.File;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import android.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.IBinder;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOOperacaoMonitoraView;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOOperacaoSistemaMobile;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOOperacaoMonitoraView.TIPO_OPERACAO_MONITORA;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.entidade.MONITORA_TELA_WEB_PARA_MOBILE;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloMonitoraTelaWebParaMobile;
import app.omegasoftware.pontoeletronico.common.activity.HelperActivity;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.common.view.DrawCanvasCirculo;
import app.omegasoftware.pontoeletronico.common.view.DrawCanvasQuadrado;
import app.omegasoftware.pontoeletronico.common.view.DrawCanvasReta;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoDownloadArquivo;
import app.omegasoftware.pontoeletronico.file.FileHttpPost;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.phone.HelperScreen;
import app.omegasoftware.pontoeletronico.phone.HelperScreen.ConfiguracaoTela;
import app.omegasoftware.pontoeletronico.pontoeletronico.OmegaApplication;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceProtocoloEnum;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemVetorProtocolo;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class ServiceMonitoraView extends Service  {

	//Constants
	public static final int ID = 19;

	public static final String TAG = "ServiceMonitoraView";

	private Timer printscreenTimer = new Timer();
	private Timer webParaMobileTimer = new Timer();
	private Timer sessaoTimer = new Timer();
	static boolean IS_EXECUTING_WEB_PARA_MOBILE = false;
	//Download button

	Context context ;
	FileHttpPost filesHttpPost ;
	String path;
	int seq= 0;



	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}
	String idOSM;
	ConfiguracaoTela conf ;
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		int vReturn = super.onStartCommand(intent, flags, startId);
		idOSM = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_OPERACAO_SISTEMA_MOBILE);

		HelperScreen helperScreen = new HelperScreen();
		conf = helperScreen.getConfiguracaoTela(this);

		return vReturn;
	}


	@Override
	public void onDestroy(){

		finalizaServico();
		super.onDestroy();
	}

	public void finalizaServico(){
		this.printscreenTimer.cancel();
		this.webParaMobileTimer.cancel();
		this.sessaoTimer.cancel();
		BOOperacaoSistemaMobile.sendBroadcastFinalizaCicloDaOperacao(
				ServiceMonitoraView.this,
				ESTADO_OPERACAO_SISTEMA_MOBILE.CONCLUIDA,
				idOSM);

	}

	@Override
	public void onCreate() {

		super.onCreate();
		OmegaFileConfiguration conf = new OmegaFileConfiguration();
		path = conf.getPath(OmegaFileConfiguration.TIPO.SCREENSHOT);

		filesHttpPost = new FileHttpPost(
				this,
				BibliotecaNuvemConfiguracao
						.getUrlWebService(BibliotecaNuvemConfiguracao.TIPO_REQUEST.transferePrintScreenMobileParaWeb),
				path);

		setTimer();

	}


	static boolean IS_EXECUTING_PRINTSCREEN = false;
	public void procedimentoPrintScreen(){

		File screenFile =  null;
		try{
			if(IS_EXECUTING_PRINTSCREEN) return;
			IS_EXECUTING_PRINTSCREEN = true;
			Activity a =  HelperActivity.getTopActivity(OmegaApplication.getSingleton());
			if(a != null){
				//getWindow().getDecorView().findViewById(android.R.id.content)
				View view = a.findViewById(android.R.id.content).getRootView();
				if(view != null){

					screenFile = HelperScreen.printScreen(view, path, String.valueOf(seq) + ".jpg");
					if(screenFile != null){
						if(!enviaPrintScreen(screenFile )){
							enviaErroDaOperacao("Nao foi possivel enviar o screen da tela.");
						}
						seq ++;
					} else enviaErroDaOperacao("Nuo foi possuvel tirar o printscreen.");
				} else enviaErroDaOperacao("Nuo encontrou a view da atividade topo.");
			} else enviaErroDaOperacao("Nuo encontrou a atividade topo.");
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			enviaErroDaOperacao("Exceuuo ocorrida.");
		}finally{

			try{
				if(screenFile != null && screenFile.exists())
					screenFile.delete();
			}catch(Exception ex){

			}
			IS_EXECUTING_PRINTSCREEN = false;
		}

	}


	public enum FORMA_GEOMETRICA{
		RETANGULO,
		CIRCULO,
		RETA
	}


	public void desenhaFormaGeometricaNaView(FORMA_GEOMETRICA fg, ProtocoloMonitoraTelaWebParaMobile p){

		File screenFile =  null;
		try{
			Activity a =  HelperActivity.getTopActivity(OmegaApplication.getSingleton());
			if(a != null){
				//getWindow().getDecorView().findViewById(android.R.id.content)
				View view = a.findViewById(android.R.id.content).getRootView();
				String erro = null;
				if(view != null){
					switch (fg) {
						case CIRCULO:
							erro = desenhaCirculo(a, view, p);
							break;
						case RETA:
							erro = desenhaReta(a, view, p);
							break;
						case RETANGULO:
							erro = desenhaRetangulo(a, view, p);
							break;
						default:
							break;
					}

					if(erro != null &&
							erro.length() > 0)
						enviaErroDaOperacao(erro);
				} else enviaErroDaOperacao("Nuo encontrou a view da atividade topo.");
			} else enviaErroDaOperacao("Nuo encontrou a atividade topo.");

		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			enviaErroDaOperacao("Exceuuo ocorrida.");
		}finally{

			try{
				if(screenFile != null && screenFile.exists())
					screenFile.delete();
			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
			}

		}

	}

//	

	private void setTimer(){
		//Starts reporter timer
		this.printscreenTimer.scheduleAtFixedRate(new TimerTask() {
													  @Override
													  public void run() {
														  procedimentoPrintScreen();
														  //ProcedimentoWebParaMobile();
														  //sendBroadCastToVeiculoMapActivity();
													  }
												  },
				BibliotecaNuvemConfiguracao.SERVICE_MONITORA_TELA_MOBILE_PARA_WEB_INITIAL_DELAY,
				BibliotecaNuvemConfiguracao.SERVICE_MONITORA_TELA_MOBILE_PARA_WEB_INTERVAL);

		this.webParaMobileTimer.scheduleAtFixedRate(new TimerTask() {
														@Override
														public void run() {
															//procedimentoPrintScreen();
															ProcedimentoWebParaMobile();
															//sendBroadCastToVeiculoMapActivity();
														}
													},
				BibliotecaNuvemConfiguracao.SERVICE_MONITORA_TELA_WEB_PARA_MOBILE_INITIAL_DELAY,
				BibliotecaNuvemConfiguracao.SERVICE_MONITORA_TELA_WEB_PARA_MOBILE_INTERVAL);

		this.sessaoTimer.scheduleAtFixedRate(new TimerTask() {
												 @Override
												 public void run() {
													 //procedimentoPrintScreen();
													 Date now = new Date();
													 if(ultimaRequisicaoWebParaMobile != null){
														 if(ultimaRequisicaoWebParaMobile.getTime() <= now.getTime() - TEMPO_TOTAL_SESSAO_SEGUNDOS){

															 stopSelf();
														 }
													 }


													 //sendBroadCastToVeiculoMapActivity();
												 }
											 },
				BibliotecaNuvemConfiguracao.SERVICE_MONITORA_TELA_SESSAO_INITIAL_DELAY,
				BibliotecaNuvemConfiguracao.SERVICE_MONITORA_TELA_SESSAO_INTERVAL);
	}
	static int TEMPO_TOTAL_SESSAO_SEGUNDOS = 5 * 60000;
	Date ultimaRequisicaoWebParaMobile = new Date();



	public InterfaceMensagem ProcedimentoWebParaMobile(){

		try{
			if(IS_EXECUTING_WEB_PARA_MOBILE) return null;
			IS_EXECUTING_WEB_PARA_MOBILE = true;

			MensagemVetorProtocolo msg = BOOperacaoMonitoraView.consultaMonitoraTelaWebParaMobile(context, idOSM);
			if(msg != null){
				InterfaceProtocoloEnum[] vetor = msg.mVetorObj;
				if(vetor != null){
					if(vetor.length > 0 )
						ultimaRequisicaoWebParaMobile = new Date();

					for(int i = 0 ; i < vetor.length; i++){
						try{
							ProtocoloMonitoraTelaWebParaMobile protocolo = (ProtocoloMonitoraTelaWebParaMobile)vetor[i];
							String strTipoOMT = protocolo.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.tipo_operacao_monitora_tela_id_INT);
							TIPO_OPERACAO_MONITORA tipo =BOOperacaoMonitoraView.TIPO_OPERACAO_MONITORA.getTipo(HelperInteger.parserInt(strTipoOMT));
							switch (tipo) {
								case acaoDeCliqueWebParaMobile:

									break;
								case desenhaCirculoNaTelaDoMobile:
									desenhaFormaGeometricaNaView(FORMA_GEOMETRICA.CIRCULO, protocolo);
									break;
								case desenhaRetanguloNaTelaDoMobile:
									desenhaFormaGeometricaNaView(FORMA_GEOMETRICA.RETANGULO, protocolo);
									break;
								case desenhaRetaNaTelaDoMobile:
									desenhaFormaGeometricaNaView(FORMA_GEOMETRICA.RETA, protocolo);
									break;
								case dialogMessage:
									mensagemDialog(protocolo);
									break;
								case fecharMonitoramento:
									String idEOSMEstadoConcluida = EXTDAOSistemaOperacaoSistemaMobile.getIdEstadoOperacaoSistemaMobile(
											EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE.CONCLUIDA);
									BOOperacaoSistemaMobile.atualizaEstado(
											context,
											protocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador),
											protocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileConectado),
											idOSM,
											idEOSMEstadoConcluida);

									stopSelf();
									break;
								case mensagemToast:
//								mensagemToast(protocolo);
									mensagemDialog(protocolo);
									break;
								default:
									break;
							}


						}catch(Exception ex){
							SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
						}
					}
				}
			}





		} catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
		} finally{
			IS_EXECUTING_WEB_PARA_MOBILE = false;
		}
		return null;
	}

	public boolean enviaPrintScreen(File file){
		try{
			String strSeq = String.valueOf(seq);
			String mobileIdentificador = Sessao.getItem(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador);
			String vStrJson = filesHttpPost.uploadAndRemoveFile(file,
					new String[]{
							"id_na_tabela",
							"nome_tabela",
							"id_sistema_tipo_download_arquivo",
							"id_operacao_sistema_mobile",
							"id_mobile_identificador",
							"seq",
							"programa_aberto",
							"teclado_ativo"
					},
					new String[]{
							idOSM,
							"operacao_sistema_mobile",
							EXTDAOSistemaTipoDownloadArquivo.getId(EXTDAOSistemaTipoDownloadArquivo.TIPO.DEFAULT),
							idOSM,
							mobileIdentificador,
							strSeq,
							"1",
							"0"});
			JSONObject vObj;
			try {
				if(vStrJson == null) return false;
				vObj = new JSONObject(vStrJson);
				int vCodRetorno = vObj.getInt("mCodRetorno");

				if(vCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId() ){
					return true;
				} else{
					return false;
				}
			} catch (JSONException e) {

				SingletonLog.insereErro(e, TIPO.BIBLIOTECA_NUVEM);
			}

		} catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
		}
		return false;
	}


	public boolean enviaErroDaOperacao(String motivoErro){
		try{
			String strSeq = String.valueOf(seq);
			String mobileIdentificador = Sessao.getItem(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador);
			String vStrJson = HelperHttpPost.getConteudoStringPost(
					this,
					BibliotecaNuvemConfiguracao.getUrlWebService(
							BibliotecaNuvemConfiguracao.TIPO_REQUEST.falhaPrintScreenMobileParaWeb
					),
					new String[]{
							"id_operacao_sistema_mobile",
							"id_mobile_identificador",
							"seq",
							"programa_aberto",
							"teclado_ativo",
							"motivo_erro"
					},
					new String[]{
							idOSM,
							mobileIdentificador,
							strSeq,
							"1",
							"0",
							motivoErro});


			JSONObject vObj;
			try {
				if(vStrJson == null || vStrJson.length() == 0) return false;
				vObj = new JSONObject(vStrJson);
				int vCodRetorno = vObj.getInt("mCodRetorno");

				if(vCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId() ){
					return true;
				} else{
					return false;
				}
			} catch (JSONException e) {

				SingletonLog.insereErro(e, TIPO.BIBLIOTECA_NUVEM);
			}

		} catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
		}
		return false;
	}


	//Retorna o motivo do erro
	// nulo caso nenhum erro ocorrer
	public String desenhaCirculo(Activity a,View v, ProtocoloMonitoraTelaWebParaMobile p){
		try{
			if(v instanceof ViewGroup){
				//ViewGroup ll = (ViewGroup) v;
				DrawCanvasCirculo pcc = new DrawCanvasCirculo(
						a,
						HelperInteger.parserInt( p.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.raio_circulo_INT)),
						HelperInteger.parserInt(p.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.cor_INT)),
						HelperInteger.parserInt(p.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.coordenada_x_INT)),
						HelperInteger.parserInt(p.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.coordenada_y_INT)));
				a.runOnUiThread(new RunnableDraw(a, pcc));
				return null;
			} else {
				return "View nuo u instancia da viewgroup: "+v.getClass().toString();
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
			return HelperExcecao.getDescricao(ex);
		}
	}

	//Retorna o motivo do erro
	// nulo caso nenhum erro ocorrer
	public String mensagemToast(ProtocoloMonitoraTelaWebParaMobile p){
		try{


			Activity a =  HelperActivity.getTopActivity(OmegaApplication.getSingleton());
			a.runOnUiThread( new RunnableToast(a, p));
			return null;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
			return HelperExcecao.getDescricao(ex);
		}
	}

	//Retorna o motivo do erro
	// nulo caso nenhum erro ocorrer
	public String mensagemDialog(ProtocoloMonitoraTelaWebParaMobile p){
		try{


			Activity a =  HelperActivity.getTopActivity(OmegaApplication.getSingleton());
			a.runOnUiThread( new RunnableDialog(a, p));
			return null;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
			return HelperExcecao.getDescricao(ex);
		}
	}


	//Retorna o motivo do erro
	// nulo caso nenhum erro ocorrer
	public String desenhaReta(Activity a,View v, ProtocoloMonitoraTelaWebParaMobile p){
		try{
			if(v instanceof ViewGroup){
				//ViewGroup ll = (ViewGroup) v;
				DrawCanvasReta pcc = new DrawCanvasReta(
						a,
						HelperInteger.parserInt( p.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.largura_quadrado_INT)),
						HelperInteger.parserInt( p.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.altura_quadrado_INT)),
						HelperInteger.parserInt(p.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.cor_INT)),
						HelperInteger.parserInt(p.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.coordenada_x_INT)),
						HelperInteger.parserInt(p.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.coordenada_y_INT)));
				a.runOnUiThread(new RunnableDraw(a, pcc));
				return null;
			} else {
				return "View nuo u instancia da viewgroup: "+v.getClass().toString();
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
			return HelperExcecao.getDescricao(ex);
		}
	}

	//Retorna o motivo do erro
	// nulo caso nenhum erro ocorrer
	public String desenhaRetangulo(Activity a,View v, ProtocoloMonitoraTelaWebParaMobile p){
		try{
			if(v instanceof ViewGroup){
				//ViewGroup ll = (ViewGroup) v;
//							 int largura, int altura, int cor, int x, int y
				DrawCanvasQuadrado pcc = new DrawCanvasQuadrado(
						a,
						HelperInteger.parserInt( p.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.largura_quadrado_INT)),
						HelperInteger.parserInt( p.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.altura_quadrado_INT)),
						HelperInteger.parserInt(p.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.cor_INT)),
						HelperInteger.parserInt(p.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.coordenada_x_INT)),
						HelperInteger.parserInt(p.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.coordenada_y_INT)));
				a.runOnUiThread(new RunnableDraw(a, pcc));
				return null;
			} else {
				return "View nuo u instancia da viewgroup: "+v.getClass().toString();
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
			return HelperExcecao.getDescricao(ex);
		}
	}

	public class RunnableDraw implements Runnable{
		View pcc;
		Activity a;
		public RunnableDraw(Activity a, View pcc){
			this.pcc = pcc;
			this.a = a;
		}
		@Override
		public void run() {
			try{
				if(!a.isFinishing()){
					LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT , LayoutParams.WRAP_CONTENT );
					a.addContentView(pcc, lp);
				}

			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM );
			}

		}

	}

	public class RunnableDialog implements Runnable{
		ProtocoloMonitoraTelaWebParaMobile protocolo;
		Activity a;
		public RunnableDialog(Activity a, ProtocoloMonitoraTelaWebParaMobile protocolo){
			this.protocolo = protocolo;
			this.a = a;
		}
		@Override
		public void run() {
			try{
				if(!a.isFinishing()){

					AlertDialog.Builder builder1 = new AlertDialog.Builder(a);
					builder1.setMessage(protocolo.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.mensagem));
					builder1.setCancelable(true);
					builder1.setPositiveButton(a.getResources().getString(R.string.yes),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
								}
							});
					builder1.setNegativeButton(a.getResources().getString(R.string.no),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
								}
							});

					AlertDialog alert11 = builder1.create();
					alert11.show();

				}


			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM );
			}

		}

	}

	public class RunnableToast implements Runnable{
		ProtocoloMonitoraTelaWebParaMobile protocolo;
		Activity a;
		public RunnableToast(Activity a, ProtocoloMonitoraTelaWebParaMobile protocolo){
			this.protocolo = protocolo;
			this.a = a;
		}
		@Override
		public void run() {
			try{
				if(!a.isFinishing()){
					CharSequence text = protocolo.getAtributo(MONITORA_TELA_WEB_PARA_MOBILE.mensagem);
					int duration = Toast.LENGTH_LONG;

					Toast toast = Toast.makeText(a, text, duration);
					toast.show();
				}


			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM );
			}

		}

	}

}
