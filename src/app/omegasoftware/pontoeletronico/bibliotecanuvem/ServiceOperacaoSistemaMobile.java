package app.omegasoftware.pontoeletronico.bibliotecanuvem;


import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;

public class ServiceOperacaoSistemaMobile extends Service  {

	//Constants
	public static final int ID = 18;
	
	public static final String TAG = "ServiceOperacaoSistemaMobile";
	public static final String LIMIT_TUPLA_TO_SEND = "100";
	private Timer webserviceTimer = null;
	
	//Download button
	
	Context context ;
	
	ControladorOperacaoSistema mControlador;
	

	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}
	
	private static MyIntentsReceiver myIntentReceiver = null;
	private void registerIntentFilter(){
		if(myIntentReceiver == null)
			myIntentReceiver = new MyIntentsReceiver();
		//Starts the TruckTrackerService
		IntentFilter serviceListCarPositionIntent = new IntentFilter(BibliotecaNuvemConfiguracao.REFRESH_SERVICE.OPERACAO_SISTEMA_MOBILE.toString());
		registerReceiver(myIntentReceiver, serviceListCarPositionIntent);
	}
	
	public void unregisterIntentFilter(){
		unregisterReceiver(myIntentReceiver);
	}
	
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		int vReturn = super.onStartCommand(intent, flags, startId);
		
		mControlador = ControladorOperacaoSistema.factory (this);
		
		registerIntentFilter();
		return vReturn; 
	}
	
	
	@Override
	public void onDestroy(){
		unregisterIntentFilter();
		mControlador.destructSingleton();
		stopTimer();
		super.onDestroy();
	}
	private void stopTimer(){
		try{
			if(this.webserviceTimer != null){
				this.webserviceTimer.cancel();
				this.webserviceTimer = null;	
			}	
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SERVICO_ANDROID);
		}
	}
	
	@Override
	public void onCreate() {
		
		super.onCreate();
		
		
		setTimer();

	}

	public void operacao(){
		OmegaConfiguration.getLocalUrl(this);
		//if(!HelperHttp.isServerOnline(this)) return;
		
		mControlador.executa(
				OmegaSecurity.getIdCorporacao(), 
				OmegaSecurity.getIdUsuario(), 
				OmegaSecurity.getCorporacao());
		
	}
//	

	private void setTimer(){
		if(this.webserviceTimer == null){
			this.webserviceTimer = new Timer();
		//Starts reporter timer
		this.webserviceTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				operacao();
				//sendBroadCastToVeiculoMapActivity();
			}
		},
		BibliotecaNuvemConfiguracao.SERVICE_OPERACAO_SISTEMA_MOBILE_INITIAL_DELAY,
		BibliotecaNuvemConfiguracao.SERVICE_OPERACAO_SISTEMA_MOBILE_INTERVAL);
		}
	}
	

	private void setTimerModoSuporte(){
		
		if(this.webserviceTimer == null){
			this.webserviceTimer = new Timer();
			//Starts reporter timer
			this.webserviceTimer.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					operacao();
					//sendBroadCastToVeiculoMapActivity();
				}
			},
			BibliotecaNuvemConfiguracao.SERVICE_SUPORTE_OPERACAO_SISTEMA_MOBILE_INITIAL_DELAY,
			BibliotecaNuvemConfiguracao.SERVICE_SUPORTE_OPERACAO_SISTEMA_MOBILE_INTERVAL);
		}
	}


	private class MyIntentsReceiver extends BroadcastReceiver {

		public static final String TAG = "MyIntentsReceiver";

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent != null){
				
				if(intent.hasExtra(BibliotecaNuvemConfiguracao.SERVICE_OPERACAO_SISTEMA_MOBILE.ATIVA_SUPORTE.toString())){
					if(intent.getBooleanExtra(BibliotecaNuvemConfiguracao.PARAM.IS_SUPORTE_ATIVO.toString(), false))
						setTimer();
					else setTimerModoSuporte();	
				} else if(intent.hasExtra(BibliotecaNuvemConfiguracao.SERVICE_OPERACAO_SISTEMA_MOBILE.FINALIZA_CICLO.toString())){
					ProtocoloConectaTelefone protocolo= Sessao.getSessao(
							context, 
							OmegaSecurity.getIdCorporacao(), 
							OmegaSecurity.getIdUsuario(),
							OmegaSecurity.getCorporacao());
					
					String idOSM = intent.getStringExtra(BibliotecaNuvemConfiguracao.PARAM.OPERACAO_SISTEMA_MOBILE_FINALIZA_CICLO.toString());
					String idEstado = intent.getStringExtra(BibliotecaNuvemConfiguracao.PARAM.ESTADO_OPERACAO_SISTEMA_MOBILE.toString());
					mControlador.finalizaCiclo(protocolo, idEstado);
				}
					
			}
			
		}
	}
}
