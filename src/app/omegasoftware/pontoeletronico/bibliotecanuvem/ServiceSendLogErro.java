package app.omegasoftware.pontoeletronico.bibliotecanuvem;


import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.ReentrantLock;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;

public class ServiceSendLogErro extends Service  {

	//Constants
	private static final String TAG = "ServiceSendLogErro";
	public static final int ID = 13; 
	private Timer sendLogErroTimer = new Timer();
	
	//Download button

	Context context ;


	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		int vReturn = super.onStartCommand(intent, flags, startId);
		
		return vReturn; 
	}
	
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {

		super.onCreate();
		
		setTimer();
	}

	
	@Override
	public void onDestroy(){
		
		try {
			if(sendLogErroTimer != null)
			sendLogErroTimer.cancel();
			
		} catch (Throwable e) {
			
			e.printStackTrace();
		}
		super.onDestroy();
	}


	private void setTimer()
	{

		//Starts reporter timer
		this.sendLogErroTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				launchUploaderTask();
			}
		},
		OmegaConfiguration.SERVICE_SEND_LOG_ERRO_INITIAL_DELAY,
		OmegaConfiguration.SERVICE_SEND_LOG_ERRO_TIMER_INTERVAL);
	}


	public void launchUploaderTask()
	{
		
		if(loadData(this, false))
			sendBroadCastPontoMarcado();
	
	}
	
	
	
	
	public boolean loadData( Context context, boolean pIsManual) {
		try{
//			if(!BibliotecaNuvemConfiguracao.checaSeOSistemaEstaNoAr(context)) return true;
			SingletonLog.writeLog();
			
//			Database db = null;
//			try{
//			db  =new DatabasePontoEletronico(context); 
//				
//			ProtocoloConectaTelefone procotolo = Sessao.getSessao(context, OmegaSecurity.getIdCorporacao(), OmegaSecurity.getIdUsuario());
//			if(procotolo != null){
//				BOSistemaLogErroMobile.enviaLogErro(
//						context, 
//						procotolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador), 
//						procotolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileConectado), 
//						BibliotecaNuvemConfiguracao.MAXIMO_LOG_ERRO_ENVIADO);
//				
//			}
			return true;
		} catch(Exception ex){
			Log.e(TAG, ex.toString());
			return false;
		} finally{
			
		}
	}

	private void sendBroadCastPontoMarcado()
	{

		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_LOG_ERRO);
		sendBroadcast(updateUIIntent);
	}

}
