package app.omegasoftware.pontoeletronico.bibliotecanuvem;


import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemConfiguracao.SERVICE_OPERACAO_SISTEMA_MOBILE;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.http.HelperHttp;

public class ServiceSuporte extends Service  {

	//Constants
	public static final int ID = 19;
	
	public static final String TAG = "ServiceSuporteMobile";
	
	private Timer webserviceTimer = new Timer();
	public boolean isActivityAberta = false;
	//Download button
	
	Context context ;
	
	
	
	ControladorSuporte mControlador;
	

	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		int vReturn = super.onStartCommand(intent, flags, startId);
		
		mControlador = new ControladorSuporte(this);
		
		
		return vReturn; 
	}
	
	
	@Override
	public void onDestroy(){
		
		this.webserviceTimer.cancel();
		super.onDestroy();
	}
	
	
	@Override
	public void onCreate() {
		
		super.onCreate();
		 
		
		
		setTimer();

	}
	
	public void inicializaActivitySuporte(){
		Intent i = new Intent();
		i.setClass(this, SuporteActivityMobile.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
	}

	
	public void operacao(){
		
//		if(!HelperHttp.isServerOnline(this)) return;
		mControlador.executa(
				OmegaSecurity.getIdCorporacao(), 
				OmegaSecurity.getIdUsuario(), 
				OmegaSecurity.getCorporacao()	);
		if(mControlador.isAtivo()){
			if(!isActivityAberta){
				inicializaActivitySuporte();
				isActivityAberta = true;
			}
		}
	}
//	

	private void setTimer(){
		//Starts reporter timer
		this.webserviceTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				operacao();
				//sendBroadCastToVeiculoMapActivity();
			}
		},
		BibliotecaNuvemConfiguracao.SERVICE_SUPORTE_INITIAL_DELAY,
		BibliotecaNuvemConfiguracao.SERVICE_SUPORTE_INTERVAL);
	}


	protected void sendBroadCastToOperacaoSistemaMobile()
	{
		Intent updateUIIntent = new Intent(BibliotecaNuvemConfiguracao.REFRESH_SERVICE.OPERACAO_SISTEMA_MOBILE.toString());
		updateUIIntent.putExtra(SERVICE_OPERACAO_SISTEMA_MOBILE.ATIVA_SUPORTE.toString(), true);
		updateUIIntent.putExtra(BibliotecaNuvemConfiguracao.PARAM.IS_SUPORTE_ATIVO.toString() , mControlador.isAtivo());
		sendBroadcast(updateUIIntent);
	
	}
	

}
