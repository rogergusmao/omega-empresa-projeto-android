package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemSharedPreference.TIPO_STRING;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOControladorMobile;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone;



public class Sessao {
	
	public static String TAG = "SingletonSessao";
	
	static ProtocoloConectaTelefone mProtocolo = null;
	
	private Sessao() {
		
	}
	

	public static boolean sessaoExpirada(){
		if(mProtocolo == null) return true;
		else return false;
	}

	public static String getItem(ProtocoloConectaTelefone.ATRIBUTO item){
 		if(mProtocolo == null) return null;
 		else {
 			if(mProtocolo.possuiItem(item.toString()) != null)
 			return mProtocolo.getAtributo(item);
 			else return null;
 		}
	}
	
	public static ProtocoloConectaTelefone getProtocolo()
	{
		return mProtocolo;
				
	}
	
	public static ProtocoloConectaTelefone getSessao(
			Context context, 
			String pIdCorporacao, 
			String pIdUsuario, 
			String pNomeCorporacao) {
		return getSessao(
				context, 
				pIdCorporacao, 
				pIdUsuario, 
				 pNomeCorporacao,
				true);
	}
	
	public static ProtocoloConectaTelefone getSessao(
			Context context, 
			String pIdCorporacao, 
			String pIdUsuario, 
			String pNomeCorporacao,
			boolean getSessaoOnline) {
		if(pIdCorporacao != null)
			if(pIdCorporacao.length() == 0)
				pIdCorporacao = null;
		if(pIdUsuario != null )
			if(pIdUsuario.length() == 0)
				pIdUsuario = null;
		if(mProtocolo != null){
			String corporacaoProtocolo = mProtocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.corporacao);
			String usuarioProtocolo = mProtocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.usuario);
			if(corporacaoProtocolo != null && pIdCorporacao == null)
				mProtocolo = null;
			else if(corporacaoProtocolo == null && pIdCorporacao != null)
				mProtocolo = null;
			else if(corporacaoProtocolo != null && pIdCorporacao != null){
				if(corporacaoProtocolo.compareTo(pIdCorporacao) != 0)
					mProtocolo = null;
			}else if(usuarioProtocolo != null && pIdUsuario == null)
				mProtocolo = null;
			else if(usuarioProtocolo == null && pIdUsuario != null)
				mProtocolo = null;
			else if(usuarioProtocolo != null && pIdUsuario != null){
				if(usuarioProtocolo.compareTo(pIdUsuario) != 0)
					mProtocolo = null;
			}
		}
		if(mProtocolo == null && getSessaoOnline){
			
			mProtocolo = BOControladorMobile.conectaTelefone(context, pIdCorporacao, pIdUsuario, pNomeCorporacao);
			
			if(mProtocolo != null)
			BibliotecaNuvemSharedPreference.saveValor(
					context, 
					TIPO_STRING.MOBILE_IDENTIFICADOR, 
					mProtocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador)
			);
			
		}
		
		return mProtocolo;
	}

}
