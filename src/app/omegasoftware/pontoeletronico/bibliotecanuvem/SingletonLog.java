package app.omegasoftware.pontoeletronico.bibliotecanuvem;

import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.ReentrantLock;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemSharedPreference.TIPO_STRING;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.dialog.HelperDialog;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class SingletonLog {
	final MODO modo = MODO.ARQUIVO;


	static ConcurrentLinkedQueue<ItemLog> filaLogs = new ConcurrentLinkedQueue<ItemLog>();
	static final int LIMITE_POR_ITERACAO = 40;
	static final int TAMANHO_MAXIMO_FILA = 200;
	static ReentrantLock reentrantLock = new ReentrantLock();

	public static void writeLog(){
		if(reentrantLock.tryLock()){
			try{
				SingletonLog.escreveLogArquivo();
			}finally{
				reentrantLock.unlock();
			}
		}
	}

	static void escreveLogArquivo(){

		if(!OmegaConfiguration.DEBUGGING_ERROR && !OmegaConfiguration.DEBUGGING_WARNING)
			return ;

		int tamanhoFila = filaLogs.size();
		if(tamanhoFila == 0){
			return;
		}
		int limite = filaLogs.size() > LIMITE_POR_ITERACAO ? LIMITE_POR_ITERACAO : filaLogs.size();

		String identificador = String.format("Identificador: [Versao: %s. Id usuario: %s. Id corporacao: %s]. ",
				BibliotecaNuvemSharedPreference.getValor(obj.context,TIPO_STRING.SISTEMA_PROJETOS_VERSAO_ID),
				OmegaSecurity.getIdUsuario(),
				OmegaSecurity.getIdCorporacao());
		String nomeArquivo = "log_error_warning_" + HelperDate.getNomeArquivoGeradoACadaXMinutos() + ".log";
//		String nomeArquivoWarning = "log_warning_" + HelperDate.getNomeArquivoGeradoACadaXMinutos() + ".log";

		OmegaLog omegaLog = null;
//		OmegaLog omegaLogWarning = null;
		try{

			omegaLog = new OmegaLog(
					nomeArquivo,
					OmegaFileConfiguration.TIPO.LOG,
					OmegaConfiguration.DEBUGGING_ERROR
							|| OmegaConfiguration.DEBUGGING_WARNING,
					obj.context);
//			omegaLogWarning= new OmegaLog(nomeArquivoWarning, OmegaFileConfiguration.TIPO.LOG,OmegaConfiguration.DEBUGGING_WARNING);
			for(int i = 0; i < limite; i ++){
				ItemLog itemLog = filaLogs.poll();
				omegaLog.escreveLog(
						itemLog.msg + identificador,
						itemLog.dataOcorrida,
						itemLog.warning ? Log.WARN : Log.ERROR);
//				if(itemLog.warning)
//					omegaLog.escreveLog(
//							itemLog.msg + identificador, 
//							itemLog.dataOcorrida, 
//							Log.ERROR);
//				else 
//					omegaLogWarning.escreveLog(
//							itemLog.msg + identificador, 
//							itemLog.dataOcorrida, 
//							Log.WARN);

			}

		}finally{
			omegaLog.close();
//			omegaLogWarning.close();
		}
	}

	public enum MODO{
		ARQUIVO(1), BANCO(2);

		private int id;

		private MODO(int id) {
			this.id = id;
		}

		public String getId() {
			return String.valueOf(id);
		}
	}

	// EXTDAOSistemaTipoProdutoLogErro
	public enum TIPO {
		SINCRONIZADOR(1), FORMULARIO(2), FILTRO(3), LISTA(4), ADAPTADOR(5), PAGINA(
				6), BANCO(7), BIBLIOTECA_NUVEM(8), WEBSERVICE_PONTO_ELETRONICO(9), WEBSERVICE_SINCRONIZADOR(10), SERVICO_ANDROID(11),

		UTIL_ANDROID(12),
		HTTP(13);

		private int id;

		private TIPO(int id) {
			this.id = id;
		}

		public String getId() {
			return String.valueOf(id);
		}

	}

	public static String TAG = "SingletonLog";
	private static SingletonLog obj;

	private String sufixo = "";
	Context context;

	private SingletonLog(Context context) {

		// app.omegasoftware.pontoeletronico.pontoeletronico.PrincipalActivity@413d7ea0
		String teste = context.toString();

		String vetor[] = HelperString.splitWithNoneEmptyToken(teste, "[.]");
		if (vetor != null && vetor.length > 0) {
			this.sufixo = "";
			for (int i = 0; i < 2; i++) {
				if (i > 0)
					this.sufixo += ".";
				this.sufixo += vetor[i];
			}
		}
		this.context = context;

	}

	public static SingletonLog constroi() {
		if (obj != null)
			return obj;
		else {

			Log.e(TAG, "Singleton nao foi inicializado.");
			return null;
		}
	}

	public static SingletonLog constroi(Context context) {
		if (obj != null)
			return obj;
		else {
			obj = new SingletonLog(context);
			return obj;
		}

	}

	private static String getStackTrace(Exception ex) {
		if (ex == null)
			return null;
		else {
			StackTraceElement vetor[] = ex.getStackTrace();
			String stack = "";
			if (vetor != null)
				for (StackTraceElement stackTraceElement : vetor) {
					String identificadorErro = stackTraceElement.toString();
					if (!stackTraceElement.isNativeMethod()) {
						if (identificadorErro.startsWith(obj.sufixo)) {
							stack += ". " + identificadorErro;
						}
					}
				}
			return stack;
		}
	}
	private static String getStackTrace(Throwable ex) {
		if (ex == null)
			return null;
		else {
			StackTraceElement vetor[] = ex.getStackTrace();
			String stack = "";
			if (vetor != null)
				for (StackTraceElement stackTraceElement : vetor) {
					String identificadorErro = stackTraceElement.toString();
					if (!stackTraceElement.isNativeMethod()) {
						if (identificadorErro.startsWith(obj.sufixo)) {
							stack += ". " + identificadorErro;
						}
					}
				}
			return stack;
		}
	}

	public static void insereErro(Exception ex, InterfaceItemXML itemXMLSCB,
								  TIPO pTipoLogErro) {
		insereErro(ex, pTipoLogErro, null,  false);
	}

	public static void insereErroSeExistente(InterfaceMensagem msg, TIPO pTipoLogErro ){
		if(msg != null && msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId()){
			insereErro(new Exception( msg.mMensagem), pTipoLogErro,  null, false);
		}
	}

	public static boolean insereErroSeExistente(JSONObject msg, TIPO pTipoLogErro ){
		try{
			if(msg != null && msg.getInt("mCodRetorno") == PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId()){
				insereErro(new Exception( msg.getString("mMensagem")), pTipoLogErro,  null, false);
				return true;
			}	 else return false;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
			return true;
		}
	}

	public static void factoryToast(Context c, OmegaDatabaseException ex, TIPO tipo){
		SingletonLog.insereWarning(ex, tipo);

		Toast.makeText(c, c.getString(R.string.erro_banco_omega), Toast.LENGTH_SHORT).show();

	}

	public static void factoryToast(Context c, Exception ex, TIPO tipo){
		SingletonLog.insereErro(ex, tipo);
		Toast.makeText(c, c.getString(R.string.erro_banco_omega), Toast.LENGTH_SHORT).show();
	}

	public static void insereWarning(Exception ex, TIPO pTipoLogErro) {
		insereErro(ex, pTipoLogErro,  null, true);
	}

	public static void insereWarning(Throwable ex, TIPO pTipoLogErro) {
		insereErro(ex, pTipoLogErro,  null, true);
	}

	public static long insereErro(Exception ex, TIPO pTipoLogErro) {
		return insereErro(ex, pTipoLogErro,  null, false);
	}

	public static long insereErro(Exception ex, TIPO pTipoLogErro, String obs) {
		return insereErro(ex, pTipoLogErro,  obs, false);
	}

	public static String formatarDescricao(
			Exception ex,
			TIPO pTipoLogErro,
			String obs,
			boolean warning){
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" " +pTipoLogErro.toString());
			StackTraceElement stack = null;
			if(warning){
				sb.append("[WARNING]");
			}
			if (obs != null) {
				sb.append(". Obs.: "+obs );
			}
			if(ex != null){
				StackTraceElement vetor[] = ex.getStackTrace();
				String identificadorErro = "";

				if (vetor != null && vetor.length > 0) {
					// android.database.sqlite.SQLiteCompiledSql.<init>(SQLiteCompiledSql.java:68)
					for (int i = 0; i < vetor.length; i++) {
						stack = vetor[i];

						if (stack != null && !stack.isNativeMethod()) {

							identificadorErro = stack.toString();
							if (identificadorErro != null && identificadorErro.startsWith(obj.sufixo)) {

								sb.append(
										String.format(". [%s::%s(%d) - %s]: Err: %s",
												stack.getClassName(),
												stack.getMethodName(),
												stack.getLineNumber(),
												stack.getFileName(),
												identificadorErro));
								break;
							}
						}
					}
				}

				sb.append(". Msg: "+ex.getMessage());
				String stacktrace = getStackTrace(ex);
				sb.append(". Stack: " + stacktrace);
			} else
			{
				if (obs != null) {
					sb.append(". Obs.: "+obs );
				}
			}
			return sb.toString();
		} catch (Exception e) {
			if(OmegaConfiguration.DEBUGGING_LOGCAT)
				Log.e(TAG, HelperExcecao.getDescricao(ex));
			return null;
		}
	}
	public static long insereErro(
			Exception ex,
			TIPO pTipoLogErro,
			String obs,
			boolean warning) {

		try {
			if(!OmegaConfiguration.DEBUGGING_ERROR && !warning)
				return -1;
			else if(!OmegaConfiguration.DEBUGGING_WARNING && warning)
				return -1;
			if(obj == null || filaLogs.size() > TAMANHO_MAXIMO_FILA)
				return -1;
			StringBuilder sb = new StringBuilder();
			sb.append(" " +pTipoLogErro.toString());
			StackTraceElement stack = null;
			if(warning){
				sb.append("[WARNING]");
			}
			if (obs != null) {
				sb.append(". Obs.: "+obs );
			}
			if(ex != null){
				StackTraceElement vetor[] = ex.getStackTrace();
				String identificadorErro = "";

				if (vetor != null && vetor.length > 0) {
					// android.database.sqlite.SQLiteCompiledSql.<init>(SQLiteCompiledSql.java:68)
					for (int i = 0; i < vetor.length; i++) {
						stack = vetor[i];

						if (stack != null && !stack.isNativeMethod()) {

							identificadorErro = stack.toString();
							if (identificadorErro != null && identificadorErro.startsWith(obj.sufixo)) {

								sb.append(
										String.format(". [%s::%s(%d) - %s]: Err: %s",
												stack.getClassName(),
												stack.getMethodName(),
												stack.getLineNumber(),
												stack.getFileName(),
												identificadorErro));
								break;
							}
						}
					}
				}

				sb.append(". Msg: "+ex.getMessage());
				String stacktrace = getStackTrace(ex);
				sb.append(". Stack: " + stacktrace);
			} else
			{
				if (obs != null) {
					sb.append(". Obs.: "+obs );
				}
			}
			ItemLog itemLog = new ItemLog();
			itemLog.msg = sb.toString();
			itemLog.dataOcorrida = new Date();
			itemLog.warning = warning;
			boolean validade = filaLogs.add(itemLog);

			logCatIfNecessary(warning, stack, ex);

			writeLog();

			return validade ? 1 : 0;
		} catch (Exception e) {
			if(OmegaConfiguration.DEBUGGING_LOGCAT)
				Log.e(TAG, HelperExcecao.getDescricao(ex));
			return -1;
		}
	}

	public static long insereErro(Throwable ex, TIPO pTipoLogErro) {
		return insereErro(
				ex,
				pTipoLogErro,
				null,
				false);
	}

	public static long insereErro(Throwable ex, TIPO pTipoLogErro,
								  String obs, boolean warning) {

		try {
			if(!OmegaConfiguration.DEBUGGING_ERROR)
				return -1;
			if(obj == null || filaLogs.size() > TAMANHO_MAXIMO_FILA)
				return -1;
			StringBuilder sb = new StringBuilder();
			sb.append(" " +pTipoLogErro.toString());
			StackTraceElement stack = null;
			if(warning){
				sb.append("[WARNING]");
			}
			if (obs != null) {
				sb.append(". Obs.: "+obs );
			}
			if(ex != null){
				StackTraceElement vetor[] = ex.getStackTrace();
				String identificadorErro = "";

				if (vetor != null && vetor.length > 0) {
					// android.database.sqlite.SQLiteCompiledSql.<init>(SQLiteCompiledSql.java:68)
					for (int i = 0; i < vetor.length; i++) {
						stack = vetor[i];

						if (stack != null && !stack.isNativeMethod()) {

							identificadorErro = stack.toString();
							if (identificadorErro != null && identificadorErro.startsWith(obj.sufixo)) {

								sb.append(
										String.format(". [%s::%s(%d) - %s]: Err: %s",
												stack.getClassName(),
												stack.getMethodName(),
												stack.getLineNumber(),
												stack.getFileName(),
												identificadorErro));
								break;
							}
						}
					}
				}

				sb.append(". Msg: "+ex.getMessage());
				String stacktrace = getStackTrace(ex);
				sb.append(". Stack: " + stacktrace);
			} else
			{
				if (obs != null) {
					sb.append(". Obs.: "+obs );
				}
			}

			logIfNecessary(warning, stack, ex);

			ItemLog itemLog = new ItemLog();
			itemLog.msg = sb.toString();
			itemLog.dataOcorrida = new Date();
			boolean validade = filaLogs.add(itemLog);

			return validade ? 1 : 0;
		} catch (Exception e) {
			if(OmegaConfiguration.DEBUGGING_LOGCAT)
				Log.e(TAG, HelperExcecao.getDescricao(ex));
			return -1;
		}
	}

	private static void logCatIfNecessary(boolean warning, StackTraceElement stack, Exception ex){
		if(OmegaConfiguration.DEBUGGING_LOGCAT){
			if(warning){
				if(OmegaConfiguration.DEBUGGING_WARNING){
					if (stack != null)
						Log.w(stack.getClassName() + stack.getMethodName(),
								HelperExcecao.getDescricao(ex));
					else
						Log.w("-",
								HelperExcecao.getDescricao(ex));
				}

			}else if(!warning ){
				if(OmegaConfiguration.DEBUGGING){
					if (stack != null)
						Log.e(stack.getClassName() + stack.getMethodName(),
								HelperExcecao.getDescricao(ex));
					else
						Log.e("-",
								HelperExcecao.getDescricao(ex));
				}
			}
		}

	}

	private static void logIfNecessary(boolean warning, StackTraceElement stack, Throwable ex){
		if(OmegaConfiguration.DEBUGGING_LOGCAT){
			if(warning && OmegaConfiguration.DEBUGGING_WARNING){
				if (stack != null)
					Log.w(stack.getClassName() + stack.getMethodName(),
							HelperExcecao.getDescricao(ex));
				else
					Log.w("-",
							HelperExcecao.getDescricao(ex));
			}else if(!warning && OmegaConfiguration.DEBUGGING){
				if (stack != null)
					Log.e(stack.getClassName() + stack.getMethodName(),
							HelperExcecao.getDescricao(ex));
				else
					Log.e("-",
							HelperExcecao.getDescricao(ex));
			}
		}

	}

	public static long insereErro(String sufixo, Exception ex, TIPO pTipoLogErro) {

		return insereErro(ex, pTipoLogErro,  sufixo, false);
	}

	public static void insereErro(String pClasse, String pFuncao,
								  String pDescricao, TIPO pTipoLogErro) {
		insereErro(null, pTipoLogErro,  String.format("%s::$s - %s", pClasse, pFuncao, pDescricao), false);

	}

	//Retorna true em caso de erro fatal
	//retorna false em caso de erro aceitavel para o fluxo
	public static boolean openDialogError(Activity a, Exception ex, TIPO tipo){
		try{
			if(ex!=null){
				SingletonLog.insereErro(ex, tipo);

				if(ex instanceof OmegaDatabaseException){
					factoryToast(a, ex, TIPO.UTIL_ANDROID);
					return false;
				}else {
					HelperDialog.factoryDialogErroFatal(a);
					return true;
				}
			}

		}catch(Exception ex2){
			Log.e("Erro no log", ex2.toString());
		}
		return false;
	}

}
