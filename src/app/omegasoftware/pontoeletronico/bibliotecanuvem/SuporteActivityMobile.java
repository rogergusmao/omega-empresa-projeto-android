package app.omegasoftware.pontoeletronico.bibliotecanuvem;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;

public class SuporteActivityMobile extends OmegaRegularActivity {
	public static String TAG = "SuporteActivityMobile";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.suporte_activity_mobile_layout);
		
		new CustomDataLoader(this).execute();
	}
	
	@Override
	public boolean loadData() {
		//
		
		return true;
	}

	@Override
	public void initializeComponents() {
		
		String mobileConectado = Sessao.getItem(ProtocoloConectaTelefone.ATRIBUTO.mobileConectado);
		String mobile = Sessao.getItem(ProtocoloConectaTelefone.ATRIBUTO.mobile);
		
		String mobileIdentificador = Sessao.getItem(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador);
		
		TextView mi = (TextView) findViewById(R.id.mobile_identificador_conteudo);
		mi.setText(mobileIdentificador == null ? "-" : mobileIdentificador);
		
		TextView mc = (TextView) findViewById(R.id.mobile_conectado_conteudo);
		mc.setText(mobileConectado == null ? "-" : mobileConectado);
		
		TextView m = (TextView) findViewById(R.id.mobile_conteudo);
		m.setText(mobile == null ? "-" : mobile);
		String corporacao = OmegaSecurity.getCorporacao();
		String usuario = OmegaSecurity.getEmail();
		TextView c = (TextView) findViewById(R.id.corporacao_conteudo);
		c.setText(corporacao == null ? "-" : corporacao);
		
		TextView u = (TextView) findViewById(R.id.usuario_conteudo);
		u.setText(usuario == null ? "-" : usuario);
		
		Button chatButton = (Button) findViewById(R.id.chat_button);
		chatButton.setOnClickListener(new ChatClickListener(R.id.chat_button));
		
		
		
		
		
	}


public class ChatClickListener implements OnClickListener {

private Integer buttonId = null;
	
	public ChatClickListener(int pButtonId){
		buttonId = pButtonId;
	}
	public void onClick(View v) {
		
	
		Button view = (Button) v.findViewById(buttonId);
		if(view == null)
		{
			view = (Button) v;
		}
		
		String emailAddress = (String) view.getText();
		
		Intent intent = new Intent(android.content.Intent.ACTION_SEND);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ emailAddress });
		intent.setType("text/plain");
		v.getContext().startActivity(intent);
		
	}
	
	
}

	
}
