package app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.instrucaoatualizacao;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.InterfaceItemXML;


 
public class ItemScriptVersao extends InterfaceItemXML{
	public static enum ATRIBUTO{
		arquivo_atualizacao_estrutura,
		sistema_projetos_versao_produto,
		sistema_projetos_versao_sistema_produto_mobile,
		resetar_banco_no_fim_da_atualizacao_BOOLEAN,
		enviar_dados_locais_do_sincronizador_BOOLEAN
	}

	
	public ItemScriptVersao() {
		super(
				TIPO_ITEM.versao, 
				ItemScriptVersao.ATRIBUTO.class);
		

	}


	@Override
	public InterfaceItemXML factory() {

		return new ItemScriptVersao();
	}

	
}


