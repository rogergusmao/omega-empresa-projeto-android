package app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.parametroglobal;


 
public class ItemXMLParametroGlobal {
 
	
	public String sistema_id_INT;
	public String sistema_produto_id_INT;
	public String sistema_produto_mobile_id_INT;
	public String projetos_id_INT;
	public String sistema_projetos_versao_sistema_produto_mobile_id_INT;
	public String sistema_projetos_versao_id_INT;
	public String sistema_projetos_versao_produto_id_INT;
	public String data_homologacao_sistema_projetos_versao;
	public String data_producao_sistema_projetos_versao;
	public String data_homologacao_sistema_projetos_versao_produto;
	public String data_producao_sistema_projetos_versao_produto;
	public String sistema_tipo_mobile;
	public String programa_arquivo;
	public void setSistemaId(String pSistemaId){
		sistema_id_INT = pSistemaId;
	}
	
	public void setSistemaProdutoId(String pSistemaProdutoId){
		sistema_produto_id_INT = pSistemaProdutoId;
	}
	
	public void setSistemaProdutoMobileId(String pSistemaProdutoMobileId){
		sistema_produto_mobile_id_INT = pSistemaProdutoMobileId;
	}
	
	public void setProjetosId(String pProjetosId){
		projetos_id_INT = pProjetosId;
	}

	public void setSistemaProjetosVersaoId(String pId){
		sistema_projetos_versao_id_INT = pId;
	}
	
	public void setSistemaProjetosVersaoProdutoId(String pId){
		sistema_projetos_versao_produto_id_INT = pId;
	}
	
	public void setSistemaProjetosVersaoSistemaProdutoMobileId(String pId){
		sistema_projetos_versao_sistema_produto_mobile_id_INT = pId;
	}
	
	public String getProjetosId(){
		return projetos_id_INT;
	}
	
	public String getSistemProdutoMobileId(){
		return sistema_produto_mobile_id_INT;
	}
//	sistema_projetos_versao_sistema_produto_mobile
	public String getSistemaProjetosVersaoSistemaProdutoMobileId(){
		return sistema_projetos_versao_sistema_produto_mobile_id_INT;
	}
	
	public String getSistemaProdutoId(){
		return sistema_produto_id_INT;
	}
	
	public String getSistemaId(){
		return sistema_id_INT;
	}
	
	
	public String getSistemaProjetosVersaoId(){
		return sistema_projetos_versao_id_INT ;
	}
	
	public String getSistemaProjetosVersaoProdutoId(){
		return sistema_projetos_versao_produto_id_INT;
	}
	
	
}


