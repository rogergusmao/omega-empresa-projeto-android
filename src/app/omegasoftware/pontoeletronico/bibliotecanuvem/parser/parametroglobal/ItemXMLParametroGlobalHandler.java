package app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.parametroglobal;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ItemXMLParametroGlobalHandler extends DefaultHandler {

	Boolean currentElement = false;
	String currentValue = "";

	private ItemXMLParametroGlobal item = new ItemXMLParametroGlobal();

	public ItemXMLParametroGlobal getItem() {
		return item;
	}

	// Called when tag starts 
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		
		currentElement = true;
		currentValue = "";
		if (localName.equals("parametro_global")) {
			item = new ItemXMLParametroGlobal();
		} 

	}

	// Called when tag closing 
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		currentElement = false;

		/** set value */
		
		if (localName.equalsIgnoreCase("data_homologacao_sistema_projetos_versao"))
			item.data_homologacao_sistema_projetos_versao = currentValue;
		
		if (localName.equalsIgnoreCase("data_producao_sistema_projetos_versao"))
			item.data_producao_sistema_projetos_versao = currentValue;
		
		if (localName.equalsIgnoreCase("data_homologacao_sistema_projetos_versao_produto"))
			item.data_homologacao_sistema_projetos_versao_produto = currentValue;
		
		if (localName.equalsIgnoreCase("data_producao_sistema_projetos_versao_produto"))
			item.data_producao_sistema_projetos_versao_produto = currentValue;
		
		if (localName.equalsIgnoreCase("sistema_tipo_mobile"))
			item.sistema_tipo_mobile = currentValue;
		
		if (localName.equalsIgnoreCase("programa_arquivo"))
			item.programa_arquivo = currentValue;
		
		if (localName.equalsIgnoreCase("sistema"))
			item.setSistemaId(currentValue);
		
		else if (localName.equalsIgnoreCase("sistema_produto"))
			item.setSistemaProdutoId(currentValue);
		
		else if (localName.equalsIgnoreCase("sistema_produto_mobile"))
			item.setSistemaProdutoMobileId(currentValue);
		
		else if (localName.equalsIgnoreCase("projetos"))
			item.setProjetosId(currentValue);
		
		else if (localName.equalsIgnoreCase("sistema_projetos_versao"))
			item.setSistemaProjetosVersaoId(currentValue);
		
		else if (localName.equalsIgnoreCase("sistema_projetos_versao_sistema_produto_mobile"))
			item.setSistemaProjetosVersaoSistemaProdutoMobileId(currentValue);
		
		else if (localName.equalsIgnoreCase("sistema_projetos_versao_produto"))
			item.setSistemaProjetosVersaoProdutoId(currentValue);
		
		
	}

	// Called to get tag characters 
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {

		if (currentElement) {
			currentValue = currentValue +  new String(ch, start, length);
		}

	}

}

