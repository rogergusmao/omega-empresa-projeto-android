package app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.parametroglobal;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemConfiguracao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;

public class ParserXMLParametroGlobal {
	
	
	
	public static ItemXMLParametroGlobal getItemXMLParametroGlobalFromAssets(Context pContext) {

		try {
			/** Handling XML */
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();
			
			ItemXMLParametroGlobalHandler myXMLHandler = new ItemXMLParametroGlobalHandler();
			xr.setContentHandler(myXMLHandler);
			
			InputSource inStream = new InputSource();
			InputStream inputFile = pContext.getAssets().open( BibliotecaNuvemConfiguracao.XML_CONFIGURACAO);
			inStream.setByteStream(inputFile);
			
			xr.parse(inStream);
			ItemXMLParametroGlobal vItem = myXMLHandler.getItem();
			inputFile.close();
			return vItem;
		}
		catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.UTIL_ANDROID);
		}
		return null;
	}
	
	public static ItemXMLParametroGlobal getItemXMLParametroGlobal(Context pContext, File pFile) {

		try {
			/** Handling XML */
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();
			
			ItemXMLParametroGlobalHandler myXMLHandler = new ItemXMLParametroGlobalHandler();
			xr.setContentHandler(myXMLHandler);
			
			InputSource inStream = new InputSource();
			
			InputStream inputFile = new FileInputStream(pFile);
			inStream.setByteStream(inputFile);
			
			xr.parse(inStream);
			ItemXMLParametroGlobal vItem = myXMLHandler.getItem();
			inputFile.close();
			return vItem;
		}
		catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.UTIL_ANDROID);
		}
		return null;
	}
}
