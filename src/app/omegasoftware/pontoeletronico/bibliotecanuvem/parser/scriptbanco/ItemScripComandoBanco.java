package app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.scriptbanco;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.InterfaceItemXML;


 
public class ItemScripComandoBanco extends InterfaceItemXML{
	public static enum ATRIBUTO{
		id,
		consulta,
		seq_INT,
		script_tabela_tabela_id_INT,
		tipo_comando_banco_id_INT,
		atributo_atributo_id_INT
		
	}
	public ItemScripComandoBanco() {
		super(TIPO_ITEM.script_comando_banco, ATRIBUTO.class);

		
	}
	@Override
	public InterfaceItemXML factory() {

		return new ItemScripComandoBanco();
	}

	
}


