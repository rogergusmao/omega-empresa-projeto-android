package app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.scriptbanco;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.InterfaceItemXML;


 
public class ItemScriptProjetosVersaoBancoBanco extends InterfaceItemXML{
	public static enum ATRIBUTO{
		id,
		is_atualizacao_BOOLEAN,
		projetos_versao_banco_banco_id_INT,
		data_criacao_DATETIME,
		data_atualizacao_DATETIME
	}

	
//	public enum UserStatus {
//		PENDING("P"), ACTIVE("A"), INACTIVE("I"), DELETED("D");
//	 
//		private String statusCode;
//	 
//		private UserStatus(String s) {
//			statusCode = s;
//			
//		}
//	 
//		public String getStatusCode() {
//			return statusCode;
//		}
//	 
//	}
//	
	public ItemScriptProjetosVersaoBancoBanco() {
		super(
				TIPO_ITEM.script_projetos_versao_banco_banco, 
				ItemScriptProjetosVersaoBancoBanco.ATRIBUTO.class);
		
		
	}


	@Override
	public InterfaceItemXML factory() {
		
		return new ItemScriptProjetosVersaoBancoBanco();
	}

	
}


