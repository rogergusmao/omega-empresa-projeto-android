package app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.scriptbanco;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.InterfaceItemXML;

public class ItemXMLScriptBancoHandler extends DefaultHandler {

	Boolean currentElement = false;
	String currentValue = "";

	private InterfaceItemXML item = null;

	
	public InterfaceItemXML getItem() {
		return item;
	}

	// Called when tag starts 
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		
		currentElement = true;
		currentValue = "";
		InterfaceItemXML.TIPO_ITEM tipoItem = Enum.valueOf(InterfaceItemXML.TIPO_ITEM.class,localName);
		if(tipoItem != null)
			item = tipoItem.factory();
		

	}

	// Called when tag closing 
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		currentElement = false;

		/** set value */
		Enum<?> key = item.possuiItem(localName);
		if(key != null){
			item.setAtributo(key, currentValue);
		}
	}

	// Called to get tag characters 
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {

		if (currentElement) {
			currentValue = currentValue +  new String(ch, start, length);
		}

	}

}

