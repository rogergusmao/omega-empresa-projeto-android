package app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.scriptbanco;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;

public class ParserXMLScriptBanco {
	
	
	
	public static boolean parse(Context pContext, String path) {
		InputSource inStream = null;
		InputStream inputFile = null;
		try {
			/** Handling XML */
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();

			ItemXMLScriptBancoHandler myXMLHandler = new ItemXMLScriptBancoHandler();
			xr.setContentHandler(myXMLHandler);
			inStream = new InputSource();
			inputFile = pContext.getAssets().open( path);
			inStream.setByteStream(inputFile);

			xr.parse(inStream);
			
			inputFile.close();
			return true;
		}
		catch (Exception e) {
			if(inputFile != null){
				try {
					inputFile.close();
				} catch (IOException e1) {
					
					e1.printStackTrace();
				}
			}
			SingletonLog.insereErro(e, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			return false;
		}
		
	}
	
}
