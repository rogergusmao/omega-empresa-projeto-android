package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemConfiguracao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemSharedPreference;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemSharedPreference.TIPO_STRING;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.ParamGet;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloSuporte;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.phone.HelperPhone;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemJSONObject;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class BOControladorMobile {
	public static ProtocoloConectaTelefone conectaTelefone(Context pContext, String pIdCorporacao, String pIdUsuario, String pNomeCorporacao) {
		
		String pSistemaProjetosVersaoSistemaProdutoMobile = BibliotecaNuvemSharedPreference.getValor(pContext, TIPO_STRING.SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID);
//		pSistemaProjetosVersaoSistemaProdutoMobile = "1";
		pIdCorporacao= (pIdCorporacao== null || pIdCorporacao.length() == 0 ) ? null: pIdCorporacao; 
		pIdUsuario = (pIdUsuario == null || pIdUsuario.length() == 0 ) ? null: pIdUsuario ;
		HelperPhone helper = new HelperPhone();
		HelperPhone.Container container = helper.getInformacoesTelefone();
		String imei;
		try {
			imei = HelperPhone.getIMEI(pContext);
		} catch (Exception e1) {
			SingletonLog.insereErro(e1, TIPO.UTIL_ANDROID);
			return null;
		}
		String identificador = container.marca + " " + container.modelo + "/" + imei; 
		String vStrJson = HelperHttpPost.getConteudoStringPost(
				pContext, 
				BibliotecaNuvemConfiguracao.getUrlWebService(
					BibliotecaNuvemConfiguracao.TIPO_REQUEST.conectaTelefone
				),
				new String[] {ParamGet.IDENTIFICADOR, ParamGet.IMEI, ParamGet.USUARIO, ParamGet.CORPORACAO, 
					ParamGet.SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE , ParamGet.MARCA, ParamGet.MODELO, ParamGet.CPU, ParamGet.NOME_CORPORACAO}, 
				new String[] {identificador, imei, pIdUsuario, pIdCorporacao, 
					pSistemaProjetosVersaoSistemaProdutoMobile, container.marca, container.modelo, container.cpu, pNomeCorporacao });
		JSONObject vObj;
		try {

			if(vStrJson == null) return null;
			//{"mObj":{"mobile":"19","mobileConectado":"7","mobileIdentificador":"77"},"mCodRetorno":0,"mMensagem":null}
			
			vObj = new JSONObject(vStrJson);
			MensagemJSONObject vProtocolo = new MensagemJSONObject(pContext, vObj);
			
			if(vProtocolo.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId() ){
				
				ProtocoloConectaTelefone vProtocoloCT = new ProtocoloConectaTelefone(pContext, vProtocolo.mObj);
				vProtocoloCT.setAtributo(ProtocoloConectaTelefone.ATRIBUTO.corporacao, pIdCorporacao);
				vProtocoloCT.setAtributo(ProtocoloConectaTelefone.ATRIBUTO.usuario, pIdUsuario);
				
				return vProtocoloCT;
			} else{
				SingletonLog.insereErro(new Exception(vObj.isNull("mMensagem") ? null : vObj.getString("mMensagem")), 
						SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			}
		} catch (JSONException e) {
			 
			Log.e("consultaUltimaVersao", HelperExcecao.getDescricao(e));
		}
		//return null;
		return null;
	}
	
	public static ProtocoloSuporte modoSuporte(Context pContext, ProtocoloConectaTelefone protocolo){
		
		String vStrJson = HelperHttpPost.getConteudoStringPost(
				pContext, 
				BibliotecaNuvemConfiguracao.getUrlWebService(
					BibliotecaNuvemConfiguracao.TIPO_REQUEST.modoSuporte
				),
				new String[] {ParamGet.ID_MOBILE_IDENTIFICADOR, ParamGet.ID_USUARIO, ParamGet.ID_CORPORACAO}, 
				new String[] {
					protocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador), 
					protocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.usuario),
					protocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.corporacao),
					});
		JSONObject vObj;
		try {
			if(vStrJson == null) return null;
			//{"mObj":{"mobile":"19","mobileConectado":"7","mobileIdentificador":"77"},"mCodRetorno":0,"mMensagem":null}
			vObj = new JSONObject(vStrJson);
			MensagemJSONObject vProtocolo = new MensagemJSONObject(pContext, vObj);
			
			
			if(vProtocolo.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId() ){
				
				ProtocoloSuporte vProtocoloCT = new ProtocoloSuporte(pContext, vProtocolo.mObj);
				return vProtocoloCT;
			} else{
//				String vMensagem = vObj.getString("mMensagem");
				
			}
		} catch (JSONException e) {
			 
			Log.e("consultaUltimaVersao", HelperExcecao.getDescricao(e));
		}
		//return null;
		return null;
	}
}
