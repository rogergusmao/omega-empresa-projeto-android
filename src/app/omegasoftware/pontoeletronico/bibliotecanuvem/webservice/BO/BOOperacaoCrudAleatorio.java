package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO;

import java.util.Random;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.PopulateTable;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoCrudAleatorio;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.primitivetype.HelperDouble;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class BOOperacaoCrudAleatorio extends BOOperacaoSistemaMobile {
	public static String TAG = "BOOperacaoCrudAleatorio";
	public static String[] BLACK_LIST = new String[]{
            "usuario", 
            "categoria_permissao",
            "permissao_categoria_permissao",
            "usuario_categoria_permissao",
            "usuario_corporacao",
            "usuario_servico",
            "tipo_ponto",
            "usuario_tipo_corporacao",
            "pessoa_usuario",
            "usuario_tipo",
            "operadora",
            "corporacao"
	};
	public BOOperacaoCrudAleatorio(Context mContext, Database db, String vId) {
		super(mContext, db, vId, new EXTDAOSistemaOperacaoCrudAleatorio(db));

	}

	@Override
	public EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE processa() {
		try {
			if (!OmegaSecurity.isAutenticacaoRealizada())
				throw new Exception("Usuario offline");
			else if(!Database.isDatabaseCreated(db.getContext())){
				throw new Exception("Database not created yet!");
			}else if(!Database.isDatabaseInitialized(db.getContext())){
				throw new Exception("Database not initialized yet!");
			} else if(OmegaSecurity.getIdCorporacao() == null 
					|| OmegaSecurity.getIdCorporacao().length() == 0)
				throw new Exception("Corporacao indefinida");
			
//			BibliotecaNuvemArvoreArquivo vArvore = new BibliotecaNuvemArvoreArquivo();
//			String vPathScript = vArvore.getPathScript(idOSM);

//			GeradorScriptBancoSQL vGerador = new GeradorScriptBancoSQL(
//					mContext, db);

			EXTDAOSistemaOperacaoCrudAleatorio objEXTDAO = new EXTDAOSistemaOperacaoCrudAleatorio(
					db);
			String id = getId();
			objEXTDAO.select(id);
			Double porcentagemEdicao = HelperDouble
					.parserDouble(objEXTDAO
							.getStrValueOfAttribute(EXTDAOSistemaOperacaoCrudAleatorio.PORCENTAGEM_EDICAO_FLOAT));
			Double porcentagemRemocao = HelperDouble
					.parserDouble(objEXTDAO
							.getStrValueOfAttribute(EXTDAOSistemaOperacaoCrudAleatorio.PORCENTAGEM_REMOCAO_FLOAT));
			int totalInsercao = HelperInteger
					.parserInt(objEXTDAO
							.getStrValueOfAttribute(EXTDAOSistemaOperacaoCrudAleatorio.TOTAL_INSERCAO_INT));
//			String strSincronizar = objEXTDAO
//					.getStrValueOfAttribute(EXTDAOSistemaOperacaoCrudAleatorio.SINCRONIZAR_BOOLEAN);
//			Boolean sincronizar = HelperBoolean
//					.valueOfStringSQL(strSincronizar);

			PopulateTable objPopulateTable = new PopulateTable(db,
					totalInsercao, porcentagemRemocao, porcentagemEdicao,
					true);

			String[] vetorUploadContinuo = EXTDAOSistemaTabela
					.getVetorUploadContinuo(db);
			Table[] vetorTabela = db.getVetorTable();

			// temporario, remover -- INICIO
//			Table[] vetorTabelaTemp = new Table[3];
//			int indexAdicao = 0;
//			for (int i = 0; i < vetorTabela.length; i++) {
//
//				if (vetorTabela[i].name.equals("empresa")
//						|| vetorTabela[i].name.equals("empresa_servico_tipo")
//						|| vetorTabela[i].name.equals("empresa_servico_unidade_medida")
//						|| vetorTabela[i].name.equals("uf")) {
//					vetorTabelaTemp[indexAdicao] = vetorTabela[i];
//					indexAdicao++;
//				}
//
//			}
//			vetorTabela = vetorTabelaTemp;
			// temporario, remover -- FIM
			Random r = new Random();
			for (int i = 0; i < vetorTabela.length; i++) {
//				if(r.nextDouble() > 0.8)
//					continue;
				Table tableAux = vetorTabela[i];
				int index = HelperString.getIndexTokenInVector(
						vetorUploadContinuo, tableAux.getName());
				if (index < 0)
					continue;
				
				Table table = tableAux.factory();

				// corrigir informacoes de campos com FK
				//table.fixFKAttributesProperties();

				index = HelperString.getIndexTokenInVector(
						BLACK_LIST, tableAux.getName());
				if (index >=0)
					continue;
				
				table.setDatabase(db);
				String nomeTabela = table.getName();

				if (nomeTabela.toLowerCase().compareTo("android_metadata") == 0)
					continue;

				Boolean tabelaDoSistema = EXTDAOSistemaTabela
						.isTabelaDoSistema(db, nomeTabela);

				// se nuo existir registro na sistema_tabela, retorna null e
				// registra no log
				if (tabelaDoSistema == null) {

					SingletonLog.insereErro(new Exception(
							"Tabela nuo mapeada no sistema_tabela, cujo nome u: "
									+ nomeTabela), TIPO.BIBLIOTECA_NUVEM);
					continue;

				}
				// se for tablea do sistema, nuo faz o CRUD
				else if (tabelaDoSistema)
					continue;
				// se for tabela normal, faz o CRUD
				else {
					try{
						if(r.nextDouble() > 0.35)
						// insert
						objPopulateTable.populate(table);						
					}catch(Exception ex2){
						SingletonLog.insereErro(ex2, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
					}

					try{
						if(r.nextDouble() > 0.5)
						// update
						objPopulateTable.edit(table);
					}catch(Exception ex3){
						SingletonLog.insereErro(ex3, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
					}
					try{
						if(r.nextDouble() > 0.5)
					// delete
						objPopulateTable.remove(table);
					}catch(Exception ex4){
						SingletonLog.insereErro(ex4, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
					}
				}
			}
			return EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE.CONCLUIDA;

		} catch (Exception ex) {

			SingletonLog.insereErro(ex, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			return EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE.ERRO_EXECUCAO;

		}

	}

}
