package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.PopulateTableBaseadoWeb;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoCrudAleatorio;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoCrudMobileBaseadoWeb;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile;
import app.omegasoftware.pontoeletronico.primitivetype.HelperBoolean;

public class BOOperacaoCrudMobileBaseadoWeb extends BOOperacaoSistemaMobile {
	public static String TAG = "BOOperacaoCrudMobileBaseadoWeb";

	public BOOperacaoCrudMobileBaseadoWeb(Context mContext, Database db, String vId) {
		super(mContext, db, vId, new EXTDAOSistemaOperacaoCrudAleatorio(db));

	}

	@Override
	public EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE processa() {
		try {
			if (!OmegaSecurity.isAutenticacaoRealizada())
				throw new Exception("Usuario offline");
			
			EXTDAOSistemaOperacaoCrudMobileBaseadoWeb objEXTDAO = new EXTDAOSistemaOperacaoCrudMobileBaseadoWeb(
					db);
			String id = getId();
			objEXTDAO.select(id);
			
			String urlDownloadJsonWeb = objEXTDAO
							.getStrValueOfAttribute(EXTDAOSistemaOperacaoCrudMobileBaseadoWeb.URL_JSON_OPERACOES_WEB);
			
			String strSincronizar = objEXTDAO
					.getStrValueOfAttribute(EXTDAOSistemaOperacaoCrudMobileBaseadoWeb.SINCRONIZAR_BOOLEAN);
			
			Boolean sincronizar = HelperBoolean
					.valueOfStringSQL(strSincronizar);

			PopulateTableBaseadoWeb objPopulateTable = new PopulateTableBaseadoWeb(db,
					urlDownloadJsonWeb, mContext, sincronizar == null ? false : sincronizar);

			if(objPopulateTable.downloadJson()){
				
				objPopulateTable.parseJson();
				
			}
			
			return EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE.CONCLUIDA;

		} catch (Exception ex) {

			SingletonLog.insereErro(ex, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			return EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE.ERRO_EXECUCAO;

		}

	}

}
