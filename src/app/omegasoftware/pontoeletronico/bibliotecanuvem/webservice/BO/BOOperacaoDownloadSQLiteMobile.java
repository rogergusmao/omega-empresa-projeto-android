package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO;

import java.io.File;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemArvoreArquivo;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemConfiguracao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.Sessao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.Servicos;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloOperacaoAvulsa;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloTransfereScriptBancoAndroidParaWeb;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone.ATRIBUTO;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoDownloadBancoMobile;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.file.FileHttpPost;
import app.omegasoftware.pontoeletronico.file.HelperFile;
import app.omegasoftware.pontoeletronico.file.HelperZip;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemJSONObject;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class BOOperacaoDownloadSQLiteMobile extends BOOperacaoSistemaMobile {
	public static String TAG = "EXTDAOSistemaOperacaoSistemaMobile";

	public BOOperacaoDownloadSQLiteMobile(Context mContext, Database db,
			String vId) {
		super(mContext, db, vId, new EXTDAOSistemaOperacaoDownloadBancoMobile(
				db));

	}

	@Override
	public EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE processa() {
		try {

			BibliotecaNuvemArvoreArquivo vArvore = new BibliotecaNuvemArvoreArquivo();
			String vPathScript = vArvore.getPathScript(idOSM);
			Database db = new DatabasePontoEletronico(mContext);
			String databaseName = db.getName();
			String shortNameDb = db.getShortName();
			String nomeCopiaDatabase = HelperString.getRandomName(shortNameDb,
					shortNameDb.length() + 5) + ".db";
			String pathCopiaDatabase = vPathScript + nomeCopiaDatabase;

			HelperDatabase.saveDatabaseNoArquivo(mContext, vPathScript,
					nomeCopiaDatabase, databaseName);

			String nomeZip = nomeCopiaDatabase.replace(".", "_");
			File zipFile = new File(vPathScript, nomeZip + ".zip");

			File vFile = new File(pathCopiaDatabase);
			HelperZip.zipVetorFile(new File[] { vFile }, zipFile);
			String vStrJson = "";
			FileHttpPost filesHttpPost = new FileHttpPost(
					mContext,
					BibliotecaNuvemConfiguracao
							.getUrlWebService(BibliotecaNuvemConfiguracao.TIPO_REQUEST.transfereScriptBancoAndroidParaWeb),
					vPathScript);
			// Se nao adicionar o arquivo, entao
			vStrJson = filesHttpPost.uploadAndRemoveFile(zipFile,
					new String[] { "operacao_sistema_mobile" },
					new String[] { idOSM });

			if (vStrJson != null) {
				try{
					zipFile.delete();	
				}catch(Exception ex){SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);}
				
				JSONObject vObj;
				try {

					// {"mObj":{"mobile":"19","mobileConectado":"7","mobileIdentificador":"77"},"mCodRetorno":0,"mMensagem":null}
					vObj = new JSONObject(vStrJson);

					MensagemJSONObject vProtocolo = new MensagemJSONObject(
							mContext, vObj);

					if (vProtocolo.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
							.getId()) {
						
						HelperFile.removeDirectoryAndAllFiles(vPathScript);;
						
						ProtocoloTransfereScriptBancoAndroidParaWeb vProtocoloEspecifico = new ProtocoloTransfereScriptBancoAndroidParaWeb(
								mContext, vProtocolo.mObj);

						return vProtocoloEspecifico.mEstado;

					} else {
						return null;
					}
				} catch (JSONException e) {
					
					SingletonLog.insereErro(e,
							SingletonLog.TIPO.BIBLIOTECA_NUVEM);

				}

			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
		}
		return null;
	}
	


	public static EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE processaAvulso(Context context, String nomeArquivo){
	
		try {
			String idMI = Sessao.getItem(ATRIBUTO.mobileIdentificador);
			ProtocoloOperacaoAvulsa protocolo = Servicos.criaOperacaoSistemaMobileDownloadBancoSQLiteMobileAvulso( context, idMI, nomeArquivo);
			if(protocolo == null) return null;
			
			Database db = new DatabasePontoEletronico(context);
			
			EXTDAOSistemaOperacaoSistemaMobile objSOSM = new EXTDAOSistemaOperacaoSistemaMobile(db);
			objSOSM.setId(protocolo.getAtributo(ProtocoloOperacaoAvulsa.ATRIBUTO.idOSM));
			objSOSM.setAttrValue(EXTDAOSistemaOperacaoSistemaMobile.TIPO_OPERACAO_SISTEMA_ID_INT, 
					String.valueOf(EXTDAOSistemaOperacaoSistemaMobile.TIPO_OPERACAO_SISTEMA.DOWNLOAD_BANCO_SQLITE_MOBILE.getId()));
			objSOSM.setAttrValue(EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE_ID_INT, 
					EXTDAOSistemaOperacaoSistemaMobile.getIdEstadoOperacaoSistemaMobile(ESTADO_OPERACAO_SISTEMA_MOBILE.AVULSA));
			objSOSM.setAttrValue(EXTDAOSistemaOperacaoSistemaMobile.MOBILE_IDENTIFICADOR_ID_INT, idMI);
			objSOSM.setAttrValue(EXTDAOSistemaOperacaoSistemaMobile.DATA_ABERTURA_DATETIME, HelperDate.getDateAtualFormatadaParaSQL());
			objSOSM.formatToSQLite();
			objSOSM.insert(false);
			
			EXTDAOSistemaOperacaoDownloadBancoMobile objEXTDAO = new EXTDAOSistemaOperacaoDownloadBancoMobile(db);
			objEXTDAO.setId(protocolo.getAtributo(ProtocoloOperacaoAvulsa.ATRIBUTO.idODBM));
			objEXTDAO.setAttrValue(EXTDAOSistemaOperacaoDownloadBancoMobile.PATH_SCRIPT_SQL_BANCO, nomeArquivo);
			objEXTDAO.setAttrValue(EXTDAOSistemaOperacaoDownloadBancoMobile.OPERACAO_SISTEMA_MOBILE_ID_INT, protocolo.getAtributo(ProtocoloOperacaoAvulsa.ATRIBUTO.idOSM));
			objEXTDAO.setAttrValue(EXTDAOSistemaOperacaoDownloadBancoMobile.DROP_TABELA_BOOLEAN, "1");
			objEXTDAO.setAttrValue(EXTDAOSistemaOperacaoDownloadBancoMobile.POPULAR_TABELA_BOOLEAN, "1");
			objEXTDAO.formatToSQLite();
			objEXTDAO.insert(false);
			
			BOOperacaoDownloadSQLiteMobile bo = new BOOperacaoDownloadSQLiteMobile(
					context, 
					db, 
					protocolo.getAtributo(ProtocoloOperacaoAvulsa.ATRIBUTO.idOSM));
			
			return bo.processa();
			
		} catch (Exception ex) {
			SingletonLog.insereErro(
					ex,
					SingletonLog.TIPO.BIBLIOTECA_NUVEM);
		}
		return null;

	}

}
