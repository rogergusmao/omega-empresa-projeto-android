package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO;

import java.io.File;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemArvoreArquivo;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemConfiguracao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.parser.scriptbanco.ParserXMLScriptBanco;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.ParamGet;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloTransfereScriptBancoAndroidParaWeb;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.GeradorScriptBancoSQL;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoDownloadBancoMobile;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoExecutaScript;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoDownloadArquivo;
import app.omegasoftware.pontoeletronico.file.FileHttpPost;
import app.omegasoftware.pontoeletronico.file.HelperZip;
import app.omegasoftware.pontoeletronico.http.HelperHttpPostReceiveFile;
import app.omegasoftware.pontoeletronico.primitivetype.HelperBoolean;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemJSONObject;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class BOOperacaoExecutaScript extends BOOperacaoSistemaMobile{
	public static String TAG = "BOOperacaoExecutaScript";
	public BOOperacaoExecutaScript(Context mContext, Database db, String vId) {
		super(mContext, db, vId, new EXTDAOSistemaOperacaoDownloadBancoMobile(db));
		
		
	}
	
	
	@Override
	public EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE processa() {
		try {
			
			BibliotecaNuvemArvoreArquivo vArvore = new BibliotecaNuvemArvoreArquivo();
			String vPathScript = vArvore.getPathScript(idOSM);

			GeradorScriptBancoSQL vGerador = new GeradorScriptBancoSQL(
					mContext, db);

			EXTDAOSistemaOperacaoExecutaScript vObjEXTDAO = new EXTDAOSistemaOperacaoExecutaScript(
					db);
			
			vObjEXTDAO.select(getId());

			Boolean dropTabela = HelperBoolean
					.valueOfStringSQL(vObjEXTDAO
							.getStrValueOfAttribute(EXTDAOSistemaOperacaoDownloadBancoMobile.DROP_TABELA_BOOLEAN));
			dropTabela = (dropTabela == null) ? false : dropTabela;
			Boolean popularTabela = HelperBoolean
					.valueOfStringSQL(vObjEXTDAO
							.getStrValueOfAttribute(EXTDAOSistemaOperacaoDownloadBancoMobile.POPULAR_TABELA_BOOLEAN));
			popularTabela = (popularTabela == null) ? false : popularTabela;
			GeradorScriptBancoSQL vGeradorTabela = new GeradorScriptBancoSQL(
					mContext, db);
			
			String nomeArquivo = vObjEXTDAO.getStrValueOfAttribute(EXTDAOSistemaOperacaoDownloadBancoMobile.PATH_SCRIPT_SQL_BANCO);
			
			
			String vPathFile = vGerador.getScriptEstrutura(vPathScript,
					nomeArquivo,
					vGeradorTabela.constroi(dropTabela, popularTabela));

			FileHttpPost filesHttpPost = new FileHttpPost(
					mContext,
					BibliotecaNuvemConfiguracao
							.getUrlWebService(BibliotecaNuvemConfiguracao.TIPO_REQUEST.transfereScriptBancoAndroidParaWeb),
					vPathScript);
			String nomeZip = nomeArquivo.replace(".", "_");
			File zipFile = new File(vPathScript,  nomeZip + ".zip");
			
			File vFile = new File(vPathFile);
			HelperZip.zipVetorFile(new File[] { vFile }, zipFile);
			String vStrJson = "";
			
				// Se nao adicionar o arquivo, entao
				vStrJson = filesHttpPost.uploadFile(zipFile,
						new String[] { "operacao_sistema_mobile" },
						new String[] { idOSM });
			
			if (vStrJson != null) {
				JSONObject vObj;
				try {

					// {"mObj":{"mobile":"19","mobileConectado":"7","mobileIdentificador":"77"},"mCodRetorno":0,"mMensagem":null}
					vObj = new JSONObject(vStrJson);

					MensagemJSONObject vProtocolo = new MensagemJSONObject(mContext, vObj);
					
					if (vProtocolo.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {

						ProtocoloTransfereScriptBancoAndroidParaWeb vProtocoloEspecifico = new ProtocoloTransfereScriptBancoAndroidParaWeb(
								mContext, vProtocolo.mObj);
						
						return vProtocoloEspecifico.mEstado;

					} else {
						return null;
					}
				} catch (JSONException e) {
					
					Log.e("procedimentoProcessandoOperacaoDeSistema",
							HelperExcecao.getDescricao(e));
					SingletonLog.insereErro(
							e,
							SingletonLog.TIPO.BIBLIOTECA_NUVEM);
					
				}

			}
		} catch (Exception ex) {
			SingletonLog.insereErro(
					ex,
					SingletonLog.TIPO.BIBLIOTECA_NUVEM);
		}
		return null;
	}


	public void executaXmlScript(){
		try{
			String pathScript = downloadXmlScript();
			ParserXMLScriptBanco.parse(mContext, pathScript);
			
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
		}
	}
	
	public String downloadXmlScript(){
		try {
			
			BibliotecaNuvemArvoreArquivo vArvore = new BibliotecaNuvemArvoreArquivo();
			String vPathScript = vArvore.getPathScript(idOSM);
			
			EXTDAOSistemaOperacaoExecutaScript vObjEXTDAO = new EXTDAOSistemaOperacaoExecutaScript(
					db);
			
			vObjEXTDAO.select(getId());
			
			String nomeArquivo = vObjEXTDAO.getStrValueOfAttribute(EXTDAOSistemaOperacaoExecutaScript.XML_SCRIPT_SQL_BANCO_ARQUIVO);
			
			Mensagem mensagem = HelperHttpPostReceiveFile.DownloadFileFromHttpPost(
					mContext,
					BibliotecaNuvemConfiguracao
					.getUrlDownload(BibliotecaNuvemConfiguracao.TIPO_REQUEST.transfereArquivoWebParaMobile),
					nomeArquivo,
					new String[]{
						ParamGet.ID_NA_TABELA, 
						ParamGet.NOME_TABELA, 
						ParamGet.ID_SISTEMA_TIPO_DOWNLOAD_ARQUIVO, 
						ParamGet.NOME_ARQUIVO
					},
					new String[]{
						vObjEXTDAO.getStrValueOfAttribute(EXTDAOSistemaOperacaoExecutaScript.ID),
						vObjEXTDAO.getName(),
						EXTDAOSistemaTipoDownloadArquivo.getId(  EXTDAOSistemaTipoDownloadArquivo.TIPO.DEFAULT),
						nomeArquivo
					});
		
			if(mensagem.ok())
				return vPathScript + nomeArquivo;
			else return null;
		
			
		} catch (Exception ex) {
			SingletonLog.insereErro(
					ex,
					SingletonLog.TIPO.BIBLIOTECA_NUVEM);
		}
		return null;
	}


}
