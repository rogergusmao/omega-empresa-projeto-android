package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO;

import java.util.concurrent.locks.ReentrantLock;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemConfiguracao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.ControladorOperacaoSistema;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.Sessao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone.ATRIBUTO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloMonitoraTelaWebParaMobile;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemVetorProtocolo;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class BOOperacaoMonitoraView extends BOOperacaoSistemaMobile{
	public static String TAG = "BOOperacaoMonitoraView";
	public enum TIPO_OPERACAO_MONITORA{
		printScreenMobileParaWeb(1),
		acaoDeCliqueWebParaMobile(2),
		desenhaCirculoNaTelaDoMobile(3),
		desenhaRetanguloNaTelaDoMobile(4),
		mensagemToast(5),
		dialogMessage(6),
		fecharMonitoramento(7),
		respostaMobileWebDaRequisicao(8),
		dimensaoDaTela(9),
		desenhaRetaNaTelaDoMobile(11),
		naoDesconectaContinuaConectado(10);
		
		int id;
		TIPO_OPERACAO_MONITORA(int id){
			this.id = id;
		}
		public int getId(){
			return id;
		}
		public static TIPO_OPERACAO_MONITORA getTipo(int id){
			TIPO_OPERACAO_MONITORA vetor[] = TIPO_OPERACAO_MONITORA.values();
			for(int i = 0 ; i < vetor.length; i++){
				if(vetor[i].getId() == id) return vetor[i];
			}
			return null;
		}
	}
	
	
	public BOOperacaoMonitoraView(Context mContext, Database db, String vId) {
		super(mContext, db, vId, null);
		
		
	}
	
	public static MensagemVetorProtocolo consultaMonitoraTelaWebParaMobile(Context context, String idOSM){

		try{
				
			String idMI = Sessao.getItem(ATRIBUTO.mobileIdentificador);
			String vStrJson = HelperHttpPost.getConteudoStringPost(
					context, 
					BibliotecaNuvemConfiguracao.getUrlWebService(
						BibliotecaNuvemConfiguracao.TIPO_REQUEST.consultaMonitoraTelaWebParaMobile
					),
					new String[]{"id_mobile_identificador", "id_operacao_sistema_mobile"}, 
					new String[]{idMI, idOSM});
			
			JSONObject vObj;
			try {
				if(vStrJson == null) return null;
				vObj = new JSONObject(vStrJson);
				Mensagem msg = new Mensagem(context, vObj);
				
				if(msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId() ){
					MensagemVetorProtocolo msgProtocolo = new MensagemVetorProtocolo(
							context, 
							new ProtocoloMonitoraTelaWebParaMobile(), 
							vObj);
					 
					return msgProtocolo;
				} else{
					//SingletonLog.insereErro(new Exception(vObj.getString("mMensagem")), SingletonLog.TIPO.BIBLIOTECA_NUVEM);
				}
				return null;
			} catch (JSONException e) {
 
				SingletonLog.insereErro(e, TIPO.BIBLIOTECA_NUVEM);
				return null;
			}
			
		} catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
			return null;
		} 
	}
	
	public static String ID_OPERACAO_SISTEMA_MOBILE = null;
	
	public static boolean IS_RUNNING(){
		if(ID_OPERACAO_SISTEMA_MOBILE != null) return true;
		else return false;
	}
	
	public static void PARA_OPERACAO(){
		ID_OPERACAO_SISTEMA_MOBILE = null;
		
	}
	static ReentrantLock reentrantLock = new ReentrantLock();
	@Override
	public EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE processa() {
		
			if(reentrantLock.tryLock()){
				try {
					
					ControladorOperacaoSistema cos = ControladorOperacaoSistema.getSingleton();
					if(cos != null){
						if(!cos.inicializaServicoMonitoraView(idOSM)){
							throw new Exception("O serviuo ju estu sendo executado.");
						}
					}
				
	
					
					return EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE.PROCESSANDO;
				} catch (Exception ex) {
					SingletonLog.insereErro(
							ex,
							SingletonLog.TIPO.BIBLIOTECA_NUVEM);
					return EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE.ERRO_EXECUCAO;
				} finally{
					reentrantLock.unlock();
				}
			}else
				return EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE .CANCELADA;
			
		
	}
	


}
