package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile;

public class BOOperacaoSincroniza extends BOOperacaoSistemaMobile{
	public static String TAG = "BOOperacaoSincroniza";
	public BOOperacaoSincroniza(Context mContext, Database db, String vId) {
		super(mContext, db, vId, null);
		
		
	}
	
	
	@Override
	public EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE processa() {
		try {
//			SynchronizeFastReceiveDatabase objSync = SynchronizeFastReceiveDatabase.getInstance();
//			objSync.run(false, false);
			
			
			return EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE.CONCLUIDA;
		} catch (Exception ex) {
			SingletonLog.insereErro(
					ex,
					SingletonLog.TIPO.BIBLIOTECA_NUVEM);
		}
		return null;
	}



}
