package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemConfiguracao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemConfiguracao.SERVICE_OPERACAO_SISTEMA_MOBILE;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.ParamGet;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.entidade.FactoryProtocoloOperacao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.entidade.ProtocoloOperacao;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public abstract class BOOperacaoSistemaMobile {
	
	public abstract  EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE processa() ;

	public static String TAG = "BOOperacaoSistemaMobile";
	
	protected Context mContext;
	protected Database db=null;
	protected String idOSM;
	protected Table objOSM;
	
	public BOOperacaoSistemaMobile(Context context, Database db, String idOSM, Table objOSM) {
		mContext = context;
		this.db = db;
		this.idOSM = idOSM;
		this.objOSM = objOSM;
	}

	public BOOperacaoSistemaMobile(Context context, Database db, String idOSM) {
		mContext = context;
		this.db = db;
		this.idOSM = idOSM;
	}
	
	
	public static void sendBroadcastFinalizaCicloDaOperacao(Context context, ESTADO_OPERACAO_SISTEMA_MOBILE estado, String idOSM){
	
		Intent updateUIIntent = new Intent(BibliotecaNuvemConfiguracao.REFRESH_SERVICE.OPERACAO_SISTEMA_MOBILE.toString());
		updateUIIntent.putExtra(SERVICE_OPERACAO_SISTEMA_MOBILE.FINALIZA_CICLO.toString(), true);
		updateUIIntent.putExtra(BibliotecaNuvemConfiguracao.PARAM.OPERACAO_SISTEMA_MOBILE_FINALIZA_CICLO.toString() , idOSM);
		updateUIIntent.putExtra(BibliotecaNuvemConfiguracao.PARAM.ESTADO_OPERACAO_SISTEMA_MOBILE.toString() , EXTDAOSistemaOperacaoSistemaMobile.getIdEstadoOperacaoSistemaMobile( estado));
		context.sendBroadcast(updateUIIntent);
		
		
	}
	
	public static boolean atualizaEstado(Context pContext, String pMobileIdentificador, String pMobileConectado, String pIdOSM, String idEOSM){
		if(pIdOSM == null) 
			return false;
		String vStrJson = HelperHttpPost.getConteudoStringPost(
				pContext, 
				BibliotecaNuvemConfiguracao.getUrlWebService(
					BibliotecaNuvemConfiguracao.TIPO_REQUEST.atualizaEstadoOperacaoSistemaMobile
				),
				new String[] {ParamGet.ID_MOBILE_IDENTIFICADOR, ParamGet.ID_MOBILE_CONECTADO, ParamGet.ID_OPERACAO_SISTEMA_MOBILE, ParamGet.ID_ESTADO_OPERACAO_SISTEMA_MOBILE}, 
				new String[] {pMobileIdentificador, pMobileConectado, pIdOSM, idEOSM});
		JSONObject vObj;
		try {
			if(vStrJson == null)
				return false;
			
			vObj = new JSONObject(vStrJson);
			int vCodRetorno = vObj.getInt("mCodRetorno");
			
			if(vCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId() ){
				return true;
			} else{
//				String vMensagem = vObj.getString("mMensagem");
				return false;
			}
		} catch (JSONException e) {


			SingletonLog.insereErro(
					e,
					SingletonLog.TIPO.BIBLIOTECA_NUVEM,
					vStrJson);
		}
		return false;
	}
	@SuppressWarnings("rawtypes")
	public static ArrayList<ProtocoloOperacao> consultaVetorOperacaoSistemaMobile(
			Context pContext, 
			String pMobileIdentificador, 
			String pMobileConectado){
		
		String vStrJson = HelperHttpPost.getConteudoStringPost(
				pContext, 
				BibliotecaNuvemConfiguracao.getUrlWebService(
					BibliotecaNuvemConfiguracao.TIPO_REQUEST.consultaOperacaoSistemaMobile
				),
				new String[] {ParamGet.ID_MOBILE_IDENTIFICADOR, ParamGet.ID_MOBILE_CONECTADO}, 
				new String[] {pMobileIdentificador, pMobileConectado});
		JSONObject vObj;
		try {
			if(vStrJson == null) return null;
			vObj = new JSONObject(vStrJson);
			int vCodRetorno = vObj.getInt("mCodRetorno");
			
			if (vCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId() ){
				JSONArray vJsonArrayVetor = vObj.getJSONArray("mVetorObj");
				int capacity = vJsonArrayVetor.length();
				
				ArrayList<ProtocoloOperacao> list = new ArrayList<ProtocoloOperacao>(capacity);
				
				for(int i = 0 ; i < vJsonArrayVetor.length(); i++){
					
					
					JSONObject jsonObj = vJsonArrayVetor.getJSONObject(i);
					
					FactoryProtocoloOperacao factory = new FactoryProtocoloOperacao(jsonObj);
					ProtocoloOperacao objProtocolo = factory.factoryProtocoloOperacao();
					
					list.add(i, objProtocolo);
				}
				
				return list;
			} else{
				
				//TODO tratamento
				
			}
		} catch (JSONException e) {
			
			SingletonLog.insereErro(
					e, 
					SingletonLog.TIPO.BIBLIOTECA_NUVEM,
					vStrJson);
		} catch (Exception e){
			SingletonLog.insereErro(
					e, 
					SingletonLog.TIPO.BIBLIOTECA_NUVEM,
					vStrJson);
		}
		return null;
	}
	
	 public String getId(){
		 if(objOSM == null) return null;
		 try{
			 String q = "SELECT " + Table.ID_UNIVERSAL + 
					 " FROM " + objOSM.getName() + 
					 " WHERE " + EXTDAOSistemaOperacaoSistemaMobile.OPERACAO_SISTEMA_MOBILE_ID_INT + " = ?";
			 String[] vVetor = db.getResultSetDoPrimeiroObjeto(q, new String[]{idOSM}, 1);
			 if(vVetor.length > 0) return vVetor[0];
			 else return null;
		 }catch(Exception e){
			 SingletonLog.insereErro(
						e, 
						SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			 return null;
		 }
		 
	 }
	
	
}
