package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO;

import java.io.File;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemConfiguracao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.ParamGet;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.file.FileHttpPost;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemVetorEXTDAO;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class BOSistemaLogErroMobile {
//	public static String TAG = "BOSistemaLogErroMobile";
//
//
//	final static int TAMANHO_LIMITE_ARQUIVO_KB = 1024;
//
//	public static String logArquivoErroProdutoMobile(
//			Context context,
//			String path,
//			String idMobileIdentificador){
//
//		FileHttpPost fileHttp = new FileHttpPost(
//				context,
//				BibliotecaNuvemConfiguracao.getUrlWebService(
//						BibliotecaNuvemConfiguracao.TIPO_REQUEST.logArquivoErroProdutoMobile
//					),
//				path);
//		File f=  new File(path);
//		if(f.exists() && f.length() > 0){
//			String strJson = fileHttp.performUpload(f, true,
//					new String[] { "id_mobile_identificador"},
//					new String[] { idMobileIdentificador });
//			return strJson;
//		} else return null;
//
//
//	}
//
//	public static boolean enviaLogErro(Context context, String pMobileIdentificador, String pMobileConectado, int maximoProtocolo){
//		Database db = null;
//		try{
//			db = new DatabasePontoEletronico(context);
//			if(!db.isTableCreated(EXTDAOSistemaProdutoLogErro.NAME)) return false;
//			EXTDAOSistemaProdutoLogErro obj = new EXTDAOSistemaProdutoLogErro(db);
//			obj.setAttrValue(EXTDAOSistemaProdutoLogErro.SINCRONIZADO_BOOLEAN, "0");
//			Table list[] = obj.getVetorTable(null, " 0, " + String.valueOf( maximoProtocolo ), true);
//			if(list == null || list.length == 0 ) return true;
//			String[] idsEnviados = new String[list.length];
//			for(int i = 0 ; i < list.length; i++)
//				idsEnviados[i] = list[i].getStrValueOfAttribute(Table.ID_UNIVERSAL);
//
//			MensagemVetorEXTDAO msg = new MensagemVetorEXTDAO(list);
//			String strMsg = msg.toJson();
//			String vStrJson = HelperHttpPost.getConteudoStringPost(
//					context,
//					BibliotecaNuvemConfiguracao.getUrlWebService(
//						BibliotecaNuvemConfiguracao.TIPO_REQUEST.logErroProdutoMobile
//					),
//					new String[] {ParamGet.ID_MOBILE_IDENTIFICADOR, ParamGet.ID_MOBILE_CONECTADO, ParamGet.ID_OBJ_JSON},
//					new String[] {pMobileIdentificador, pMobileConectado, strMsg});
//
//
//			JSONObject vObj;
//			try {
//				if(vStrJson == null) return false;
//				vObj = new JSONObject(vStrJson);
//				int vCodRetorno = vObj.getInt("mCodRetorno");
//
//				if(vCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId() ){
//
//					try{
//
//
//						for(int i = 0 ; i < list.length; i++){
//							obj.setAttrValue(Table.ID_UNIVERSAL, idsEnviados[i]);
////							Seta como sincronizado
//							obj.setAttrValue(EXTDAOSistemaProdutoLogErro.SINCRONIZADO_BOOLEAN, "1");
//							obj.formatToSQLite();
//							obj.remove(false);
//						}
//
//					}catch(Exception ex){
//						SingletonLog.insereErro(ex, TIPO.BANCO);
//					}
//
//					return true;
//				} else{
//					//String vMensagem = vObj.getString("mMensagem");
//
//				}
//			} catch (JSONException e) {
//
//				if(OmegaConfiguration.DEBUGGING)
//					Log.e("enviaLogErro", HelperExcecao.getDescricao(e));
//
//				try{
//
//
//					for(int i = 0 ; i < list.length; i++)
//						list[i].remove(false);
//
//
//				}catch(Exception ex){
//					Log.e("enviaLogErro", HelperExcecao.getDescricao(ex));
//				}
//
//			}
//			return false;
//
//		} catch(Exception ex){
//			Log.e("enviaLogErro", HelperExcecao.getDescricao(ex));
//			//Nao posso inserir na tabela log de erro pra nao travar o sistema de tanta mensagem de erro
//			//Uma vez que se der excecao, significa que a comunicacao com o envioo de erro, esta com problemas
//			return false;
//		}	finally{
//			if(db != null)
//				db.close();
//		}
//
//	}
}
