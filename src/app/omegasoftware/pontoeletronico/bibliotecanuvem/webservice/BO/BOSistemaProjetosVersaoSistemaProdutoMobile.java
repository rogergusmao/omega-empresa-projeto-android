package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO;

import java.io.File;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemArvoreArquivo;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemConfiguracao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemConfiguracao.TIPO_REQUEST;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.file.HelperZip;
import app.omegasoftware.pontoeletronico.http.HelperHttp;
import app.omegasoftware.pontoeletronico.http.HelperHttpPostReceiveFile;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class BOSistemaProjetosVersaoSistemaProdutoMobile {
	
	public static String transfereArquivoWebParaAndroidDoPrograma(
			Context pContext, 
			String pNomeArquivo, 
			String pIdSPVSPMAtual){
		try{
			
			Boolean validade = null;
			BibliotecaNuvemArvoreArquivo vObjArquivo = new BibliotecaNuvemArvoreArquivo();
			String pathDownload = vObjArquivo.getPathApk(pIdSPVSPMAtual);
			
			File file = new File(pathDownload, pNomeArquivo);
			file.delete();
			if(!file.exists()){
				
				Mensagem msg = HelperHttpPostReceiveFile.DownloadFileFromHttpPost(
						pContext, 
						BibliotecaNuvemConfiguracao.getUrlDownload(TIPO_REQUEST.downloadApkAndroid), 
						pathDownload + pNomeArquivo, 
						new String[]{"sistema_projetos_versao_sistema_produto_mobile"}, 
						new String[]{pIdSPVSPMAtual});
				
				
				validade = InterfaceMensagem.validaChamadaWebservice(msg);
				if(validade)
					HelperZip.unzipToDirectory(file, new File(pathDownload));
				
			} else validade = true;
			
			return validade ? pathDownload : null;

		} catch(Exception ex){
			
			SingletonLog.insereErro(ex, TIPO.HTTP);
			return null;
		}	
	}
}
