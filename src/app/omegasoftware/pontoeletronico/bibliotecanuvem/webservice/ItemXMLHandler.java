package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice;

import java.util.ArrayList;
import java.util.HashMap;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.InterfaceItemXML;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.InterfaceItemXML.TIPO_ITEM;

public class ItemXMLHandler extends DefaultHandler {

	Boolean currentElement = false;
	String currentValue = "";
	
	private InterfaceItemXML item = null;

	
	HashMap<String, ArrayList<InterfaceItemXML>> hash = new HashMap<String, ArrayList<InterfaceItemXML>>();
	
	public InterfaceItemXML getItem() {
		return item;
	}
	public ArrayList<InterfaceItemXML> getList(TIPO_ITEM tipo){
		if(hash.containsKey(tipo.toString())){
			return hash.get(tipo.toString());
		} return null;
	}
	// Called when tag starts 
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		
		currentElement = true;
		currentValue = "";
		InterfaceItemXML.TIPO_ITEM tipoItem = null;
		try{
			tipoItem = Enum.valueOf(InterfaceItemXML.TIPO_ITEM.class,localName);	
		} catch(IllegalArgumentException ex){
			
		}
		
		if(tipoItem != null){
			item = tipoItem.factory();
			
			ArrayList<InterfaceItemXML> list = null;
			if(!hash.containsKey(tipoItem.toString())){
				hash.put(tipoItem.toString(), new ArrayList<InterfaceItemXML>());
			}
			
			list = hash.get(tipoItem.toString());
			list.add(item);	
		}
	
		

	}

	// Called when tag closing 
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		currentElement = false;

		/** set value */
		Enum<?> key = item.possuiItem(localName);
		if(key != null){
			item.setAtributo(key, currentValue);
		}
	}

	// Called to get tag characters 
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {

		if (currentElement) {
			currentValue = currentValue +  new String(ch, start, length);
		}

	}

}

