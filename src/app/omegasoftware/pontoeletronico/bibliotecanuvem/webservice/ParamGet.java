package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice;

public class ParamGet {
	public static String ID_MOBILE_IDENTIFICADOR = "id_mobile_identificador";
	public static String ID_MOBILE_CONECTADO = "id_mobile_conectado";
	public static String ID_USUARIO = "id_usuario";
	public static String ID_CORPORACAO = "id_corporacao";
	public static String ID_OPERACAO_SISTEMA_MOBILE= "id_operacao_sistema_mobile";
	public static String ID_ESTADO_OPERACAO_SISTEMA_MOBILE= "id_estado_operacao_sistema_mobile";
	public static String SENHA = "senha";
	public static String ID_OBJ_JSON = "id_obj_json";
	public static String MARCA = "marca";
	public static String MODELO = "modelo";
	public static String  CPU = "cpu";
	
	public static String  IDENTIFICADOR = "identificador"; 
	public static String  IMEI = "imei"; 
	public static String  USUARIO = "usuario"; 
	public static String  CORPORACAO = "corporacao"; 
	public static String  NOME_CORPORACAO = "nome_corporacao";
	public static String  SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE = "sistema_projetos_versao_sistema_produto_mobile";
	
	public static String  ID_NA_TABELA = "id_na_tabela";
    public static String NOME_TABELA = "nome_tabela";
    public static String ID_SISTEMA_TIPO_DOWNLOAD_ARQUIVO = "id_sistema_tipo_download_arquivo";
    public static String NOME_ARQUIVO = "nome_arquivo";
}
