package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;

public class ParserXMLScript {
	
	
//	
//	public static ItemXMLHandler parse(Context pContext, String path) {
//		InputSource inStream = null;
//		InputStream inputFile = null;
//		try {
//			/** Handling XML */
//			SAXParserFactory spf = SAXParserFactory.newInstance();
//			SAXParser sp = spf.newSAXParser();
//			XMLReader xr = sp.getXMLReader();
//
//			ItemXMLHandler myXMLHandler = new ItemXMLHandler();
//			xr.setContentHandler(myXMLHandler);
//			inStream = new InputSource();
//			inputFile = pContext.getAssets().open( path);
//			inStream.setByteStream(inputFile);
//
//			xr.parse(inStream);
//			
//			inputFile.close();
//			return myXMLHandler;
//		}
//		catch (Exception e) {
//			if(inputFile != null){
//				try {
//					inputFile.close();
//				} catch (IOException e1) {
//					
//					e1.printStackTrace();
//				}
//			}
//			SingletonLog.insereErro(e, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
//			return null;
//		}
//		
//	}
//	

	public static ItemXMLHandler parse(Context context, String fileAssets) {
		
		InputStream inputFile = null;
		try {
			inputFile = context.getAssets().open(fileAssets);
			if(inputFile == null) return null;
			/** Handling XML */
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();

			ItemXMLHandler myXMLHandler = new ItemXMLHandler();
			xr.setContentHandler(myXMLHandler);
			InputSource inStream = new InputSource();
			inStream.setByteStream(inputFile);

			xr.parse(inStream);
			
			return myXMLHandler;
		}
		catch (Exception e) {
			
			SingletonLog.insereErro(e, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			return null;
		}finally{
			if(inputFile != null)
				try {
					inputFile.close();
				} catch (IOException e1) {
					
					e1.printStackTrace();
				}
		}
		
	}
	
}
