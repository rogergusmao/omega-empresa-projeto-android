package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemConfiguracao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.Sessao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloOperacaoAvulsa;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemInteger;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemJSONObject;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemVetorInteger;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class Servicos {


	public static ProtocoloOperacaoAvulsa criaOperacaoSistemaMobileDownloadBancoMobileAvulso(
			Context pContext, 
			String idMI, 
			String nomeArquivo){
		
		
		String vStrJson = HelperHttpPost.getConteudoStringPost(
				pContext, 
				BibliotecaNuvemConfiguracao.getUrlWebService(
					BibliotecaNuvemConfiguracao.TIPO_REQUEST.criaOperacaoSistemaMobileDownloadBancoMobileAvulso
				),
				new String[] {ParamGet.ID_MOBILE_IDENTIFICADOR, ParamGet.NOME_ARQUIVO}, 
				new String[] {idMI, nomeArquivo});
		
		try {
			if(vStrJson == null) return null;

			MensagemJSONObject vProtocolo = new MensagemJSONObject(
					pContext, new JSONObject(vStrJson));

			if (vProtocolo.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
					.getId()) {

				ProtocoloOperacaoAvulsa protocolo = new ProtocoloOperacaoAvulsa(pContext, vProtocolo.mObj);

				return protocolo;

			} else {
				return null;
			}
			
		} catch (JSONException e) {

			if(OmegaConfiguration.DEBUGGING)
				Log.e("enviaLogErro", HelperExcecao.getDescricao(e));
			return null;
			
		}
	}
	
	public static ProtocoloOperacaoAvulsa criaOperacaoSistemaMobileDownloadBancoSQLiteMobileAvulso(
			Context pContext, 
			String idMI, 
			String nomeArquivo){
		 
		
		String vStrJson = HelperHttpPost.getConteudoStringPost(
				pContext, 
				BibliotecaNuvemConfiguracao.getUrlWebService(
					BibliotecaNuvemConfiguracao.TIPO_REQUEST.criaOperacaoSistemaMobileDownloadBancoSQLiteMobileAvulso
				),
				new String[] {ParamGet.ID_MOBILE_IDENTIFICADOR, ParamGet.NOME_ARQUIVO}, 
				new String[] {idMI, nomeArquivo});
		
		try {
			if(vStrJson == null) return null;

			MensagemJSONObject vProtocolo = new MensagemJSONObject(
					pContext, new JSONObject(vStrJson));

			if (vProtocolo.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
					.getId()) {

				ProtocoloOperacaoAvulsa protocolo = new ProtocoloOperacaoAvulsa(pContext, vProtocolo.mObj);

				return protocolo;

			} else {
				return null;
			}
			
		} catch (JSONException e) {

			if(OmegaConfiguration.DEBUGGING)
				Log.e("enviaLogErro", HelperExcecao.getDescricao(e));
			return null;
			
		}
	}
	
	
	/*
	 * {"mValor":"1","mCodRetorno":3,"mMensagem":null}
	 */
	public static InterfaceMensagem consultaListaVersao(
			Context pContext,
			String pSistemaProdutoMobileId,
			String pSistemaProjetosVersaoSistemaProdutoMobile) {
		// TODO retirar o null da passagem de parametro do POST
		
		try {
			JSONObject vObj = null;
			String mi = Sessao.getItem(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador);
			mi = (mi == null || mi.length() == 0 ) ? "" : mi;
			String vStrJson = HelperHttpPost
				.getConteudoStringPost(
						pContext,
						BibliotecaNuvemConfiguracao
								.getUrlWebService(BibliotecaNuvemConfiguracao.TIPO_REQUEST.consultaListaIdSistemaProjetosVersaoSistemaProdutoMobile),
						new String[] { "sistema_produto_mobile",
								"sistema_projetos_versao_sistema_produto_mobile",
								"mobile_identificador"},
						new String[] { pSistemaProdutoMobileId,
								pSistemaProjetosVersaoSistemaProdutoMobile,
								mi});
		
		
			if (vStrJson == null)
				return null;
			vObj = new JSONObject(vStrJson);
			int vCodRetorno = vObj.getInt("mCodRetorno");
			if(vCodRetorno == PROTOCOLO_SISTEMA.TIPO.REINSTALAR_VERSAO.getId()){
				Integer versaoASerInstalada = vObj.getInt("mVetor");
				return new MensagemInteger(PROTOCOLO_SISTEMA.TIPO.REINSTALAR_VERSAO, versaoASerInstalada);
			}
			else if (vCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {
				JSONArray vJsonArrayVetor = vObj.getJSONArray("mVetor");
				Integer[] vVetor = new Integer[vJsonArrayVetor.length()];

				for (int i = 0; i < vJsonArrayVetor.length(); i++)
					vVetor[i] = vJsonArrayVetor.getInt(i);
				MensagemVetorInteger msg = new MensagemVetorInteger(vVetor);
				return msg;
			} else {
				
				String vMensagem = vObj.getString("mMensagem");
				return new Mensagem(PROTOCOLO_SISTEMA.getTipo(vCodRetorno), vMensagem);
				
			}
		} catch (JSONException e) {
			
			Log.e("consultaUltimaVersao", HelperExcecao.getDescricao(e));
		}
		return null;
	}
//	
//	public static Integer consultaUltimaVersao(
//			Context pContext,
//			String pSistemaProdutoMobileId,
//			String pSistemaProjetosVersaoSistemaProdutoMobile) {
//		// TODO retirar o null da passagem de parametro do POST
//		
//		try {
//			JSONObject vObj = null;
//			String vStrJson = HelperHttpPost
//				.getConteudoStringPost(
//						pContext,
//						BibliotecaNuvemConfiguracao
//								.getUrlWebService(BibliotecaNuvemConfiguracao.TIPO_REQUEST.consultaUltimoIdSistemaProjetosVersaoSistemaProdutoMobile),
//						new String[] { "sistema_produto_mobile",
//								"sistema_projetos_versao_sistema_produto_mobile" },
//						new String[] { pSistemaProdutoMobileId,
//								pSistemaProjetosVersaoSistemaProdutoMobile },
//						5);
//		
//		
//			if (vStrJson == null)
//				return null;
//			vObj = new JSONObject(vStrJson);
//			int vCodRetorno = vObj.getInt("mCodRetorno");
//
//			if (vCodRetorno == 0) {
//				JSONArray vJsonArrayVetor = vObj.getJSONArray("mVetor");
//				int[] vVetor = new int[vJsonArrayVetor.length()];
//				
//				for (int i = 0; i < vJsonArrayVetor.length(); i++)
//					vVetor[i] = vJsonArrayVetor.getInt(i);
//
//				return vVetor[0];
//			} else {
//				String vMensagem = vObj.getString("mMensagem");
//
//			}
//		} catch (JSONException e) {
//			
//			Log.e("consultaUltimaVersao", HelperExcecao.getDescricao(e));
//		}
//		return null;
//	}

	//
	// public static int[] consultaVetorOperacaoSistemaMobile(Context pContext,
	// String pSistemaProdutoMobileId,
	// String pSistemaProjetosVersaoSistemaProdutoMobile){
	//
	//
	// BOOperacaoSistemaMobile bo = new BOOperacaoSistemaMobile();
	// bo.consultaVetorOperacaoSistemaMobile(
	// pContext,
	// pSistemaProjetosVersaoSistemaProdutoMobile);
	//
	// return null;
	// }
//	public static void registraUsuarioMensagem() {
//
//	}
//
//	public static void registraLogErroMobile() {
//
//	}
//
//	public static void registraLogErroAtualizacaoBancoMobile() {
//
//	}
//
//	public static void consultaOperacaoDeSistemaMobile() {
//
//	}
//
//	public static void atualizaConexao() {
//
//	}
}
