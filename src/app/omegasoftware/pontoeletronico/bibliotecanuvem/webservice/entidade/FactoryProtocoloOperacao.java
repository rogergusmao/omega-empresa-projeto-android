package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.entidade;

import org.json.JSONException;
import org.json.JSONObject;

import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaOperacaoCrudAleatorio.ATRIBUTOS_SISTEMA_OPERACAO_CRUD_ALEATORIO;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaOperacaoCrudMobileBaseadoWeb.ATRIBUTOS_SISTEMA_OPERACAO_CRUD_MOBILE_BASEADO_WEB;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaOperacaoDownloadBancoMobile.ATRIBUTOS_SISTEMA_OPERACAO_DOWNLOAD_BANCO_MOBILE;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaOperacaoExecutaScript.ATRIBUTOS_SISTEMA_OPERACAO_EXECUTA_SCRIPT;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoCrudAleatorio;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoDownloadBancoMobile;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoExecutaScript;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile;


public class FactoryProtocoloOperacao {
	public static String TAG = "FactoryProtocoloOperacao";
	//	{"mVetorObj":[{"id":"1","tipo_operacao_sistema_id_INT":"1",
	//    "estado_operacao_sistema_mobile_id_INT":"1","mobile_identificador_id_INT":"137",
	//    "data_abertura_DATETIME":"2013-03-26 13:26:01","data_processamento_DATETIME":null,
	//    "data_conclusao_DATETIME":null,"objOperacao":{"id":"1","tipo_operacao_sistema_id_INT":null,
	//    	"estado_operacao_sistema_mobile_id_INT":null,"mobile_identificador_id_INT":null,
	//    	"data_abertura_DATETIME":null,"data_processamento_DATETIME":null,
	//    	"data_conclusao_DATETIME":null,"

	ProtocoloEntidade<OPERACAO_SISTEMA_MOBILE> protocolo;
	JSONObject jsonObjectOperacaoFilha;
	public FactoryProtocoloOperacao(JSONObject pJsonObject) throws JSONException, Exception{
//		super(pJsonObject.getJSONObject("objOperacao"), objEXTDAO, atributos);
		if(! pJsonObject.isNull("objOperacao"))
			jsonObjectOperacaoFilha = pJsonObject.getJSONObject("objOperacao");
		protocolo = new ProtocoloEntidade<OPERACAO_SISTEMA_MOBILE>(
				pJsonObject, 
				new EXTDAOSistemaOperacaoSistemaMobile(null), 
				OPERACAO_SISTEMA_MOBILE.class);
		
	}
	
	@SuppressWarnings("rawtypes")
	public ProtocoloOperacao factoryProtocoloOperacao() throws Exception{
		String tipo = protocolo.getAtributo(OPERACAO_SISTEMA_MOBILE.tipo_operacao_sistema_id_INT);
		
		
		switch (EXTDAOSistemaOperacaoSistemaMobile.getTipoOperacaoSistema(tipo)) {
		
		case DOWNLOAD_BANCO_MOBILE:
			return new ProtocoloOperacao<ATRIBUTOS_SISTEMA_OPERACAO_DOWNLOAD_BANCO_MOBILE>(
					protocolo,
					jsonObjectOperacaoFilha, 
					new EXTDAOSistemaOperacaoDownloadBancoMobile(null), 
					ATRIBUTOS_SISTEMA_OPERACAO_DOWNLOAD_BANCO_MOBILE.class);
			
		case EXECUTA_SCRIPT:
			return new ProtocoloOperacao<ATRIBUTOS_SISTEMA_OPERACAO_EXECUTA_SCRIPT>(
					protocolo,
					jsonObjectOperacaoFilha, 
					new EXTDAOSistemaOperacaoExecutaScript(null), 
					ATRIBUTOS_SISTEMA_OPERACAO_EXECUTA_SCRIPT.class);
			
		case CRUD_ALEATORIO:
			return new ProtocoloOperacao<ATRIBUTOS_SISTEMA_OPERACAO_CRUD_ALEATORIO>(
					protocolo,
					jsonObjectOperacaoFilha, 
					new EXTDAOSistemaOperacaoCrudAleatorio(null), 
					ATRIBUTOS_SISTEMA_OPERACAO_CRUD_ALEATORIO.class);
			
		case CRUD_MOBILE_BASEADO_NA_WEB:
			return new ProtocoloOperacao<ATRIBUTOS_SISTEMA_OPERACAO_CRUD_MOBILE_BASEADO_WEB>(
					protocolo,
					jsonObjectOperacaoFilha, 
					new EXTDAOSistemaOperacaoSistemaMobile(null), 
					ATRIBUTOS_SISTEMA_OPERACAO_CRUD_MOBILE_BASEADO_WEB.class);
			

		default:
			//nenhum protocolo adicional u necessario, uma vez que toda informacao necessaria
			//ja esta contida no registro 'operacao_sistema_mobile'
			return new ProtocoloOperacao<OPERACAO_VAZIA>(
					protocolo,
					null, 
					null, 
					OPERACAO_VAZIA.class);
			
		}
		
	}
//	
//	public ProtocoloEntidade<OPERACAO_SISTEMA_MOBILE> getProtocoloOperacaoSistemaMobile(){
//		return protocolo;
//	}
//	
//	public String getIdOSM(){
//		return protocolo.getAtributo(OPERACAO_SISTEMA_MOBILE.id);
//	}
//
//	@Override
//	public String persiste(Database db, boolean isToSync){
//		String idOSM = protocolo.persiste(db, isToSync);
//		
//		objEXTDAO.setAttrStrValue( "operacao_sistema_mobile_id_INT", idOSM);
//		
//		return super.persiste(db, isToSync);
//	}

}
