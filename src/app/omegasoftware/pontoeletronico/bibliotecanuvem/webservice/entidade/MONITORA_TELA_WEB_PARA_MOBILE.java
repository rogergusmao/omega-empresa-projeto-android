package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.entidade;
public enum MONITORA_TELA_WEB_PARA_MOBILE{
	 id      ,                           //| int(11)      | NO   | PRI | NULL    | auto_increment |
	 mobile_identificador_id_INT,       // | int(11)      | YES  |     | NULL    |                |
	 operacao_sistema_mobile_id_INT   , // | int(11)      | NO   |     | NULL    |                |
	 coordenada_x_INT                  , //| int(11)      | YES  |     | NULL    |                |
	 coordenada_y_INT                  , //| int(11)      | YES  |     | NULL    |                |
	 tipo_operacao_monitora_tela_id_INT, //| int(11)      | YES  |     | NULL    |                |
	 sequencia_operacao_INT            , //| int(11)      | YES  |     | NULL    |                |
	 data_ocorrencia_DATETIME          , //| datetime     | YES  |     | NULL    |                |
	 raio_circulo_INT                  , //| int(11)      | YES  |     | NULL    |                |
	 largura_quadrado_INT              , //| int(11)      | YES  |     | NULL    |                |
	 altura_quadrado_INT               , //| int(11)      | YES  |     | NULL    |                |
	 cor_INT,                            //| int(11)      | YES  |     | NULL    |                |
	 mensagem        ,
				
	}