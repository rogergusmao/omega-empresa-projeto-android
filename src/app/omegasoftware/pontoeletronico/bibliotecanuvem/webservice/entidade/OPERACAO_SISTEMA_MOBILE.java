package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.entidade;
public enum OPERACAO_SISTEMA_MOBILE{
		id,
		tipo_operacao_sistema_id_INT,
		estado_operacao_sistema_mobile_id_INT,
		mobile_identificador_id_INT,
		data_abertura_DATETIME,
		data_processamento_DATETIME,
		data_conclusao_DATETIME
				
	}