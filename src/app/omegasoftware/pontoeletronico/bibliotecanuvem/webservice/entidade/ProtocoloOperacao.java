package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.entidade;

import org.json.JSONException;
import org.json.JSONObject;

import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
import app.omegasoftware.pontoeletronico.database.Table;

@SuppressWarnings("rawtypes")
public class ProtocoloOperacao <E extends Enum<E>>  extends ProtocoloEntidade {
	public static String TAG = "InterfaceoProtocoloOperacao";
	//	{"mVetorObj":[{"id":"1","tipo_operacao_sistema_id_INT":"1",
	//    "estado_operacao_sistema_mobile_id_INT":"1","mobile_identificador_id_INT":"137",
	//    "data_abertura_DATETIME":"2013-03-26 13:26:01","data_processamento_DATETIME":null,
	//    "data_conclusao_DATETIME":null,"objOperacao":{"id":"1","tipo_operacao_sistema_id_INT":null,
	//    	"estado_operacao_sistema_mobile_id_INT":null,"mobile_identificador_id_INT":null,
	//    	"data_abertura_DATETIME":null,"data_processamento_DATETIME":null,
	//    	"data_conclusao_DATETIME":null,"

	ProtocoloEntidade<OPERACAO_SISTEMA_MOBILE> protocolo;
	@SuppressWarnings("unchecked")
	public ProtocoloOperacao(
			ProtocoloEntidade<OPERACAO_SISTEMA_MOBILE> protocolo,
			JSONObject pJsonObject, 
			Table objEXTDAO, 
			Class<E> atributos) throws JSONException, Exception{
		super(pJsonObject, objEXTDAO, atributos);
		this.protocolo = protocolo;
		
	}
	
	public ProtocoloEntidade<OPERACAO_SISTEMA_MOBILE> getProtocoloOperacaoSistemaMobile(){
		return protocolo;
	}
	
	public String getIdOSM(){
		return protocolo.getAtributo(OPERACAO_SISTEMA_MOBILE.id);
	}

	@Override
	public String persiste(Database db, boolean isToSync){
		String idOSM = protocolo.persiste(db, isToSync);
		if(objEXTDAO != null)
			objEXTDAO.setAttrValue( "operacao_sistema_mobile_id_INT", idOSM);
		
		return super.persiste(db, isToSync);
	}

}
