package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.entidade;
public enum SISTEMA_PRODUTO_LOG_ERRO
{id,
classe,
funcao,
linha_INT,
nome_arquivo,
identificador_erro,
descricao,
stacktrace,
data_visualizacao_DATETIME,
data_ocorrida_DATETIME,
sistema_tipo_log_erro_id_INT,
sistema_projetos_versao_produto_id_INT,
sistema_projetos_versao_id_INT,
id_usuario_INT,
id_corporacao_INT}