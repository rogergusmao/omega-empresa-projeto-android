package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo;

import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceProtocoloEnum;


public class ProtocoloArquivo extends InterfaceProtocoloEnum {
	
	
    public enum ATRIBUTO {
    	nomeArquivo,
    	pathArquivo
    }
	public static String TAG = "ProtocoloArquivo";
	public ProtocoloArquivo(Context pContext,
			JSONObject pJsonObject) {
		
		super(pJsonObject, ATRIBUTO.class);
		
	}
	@Override
	public InterfaceProtocoloEnum factory(Context pContext,
			JSONObject pJsonObject) {
		
		return new ProtocoloArquivo(pContext, pJsonObject);
	}
	
}
