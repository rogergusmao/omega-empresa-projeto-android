package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo;

import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceProtocoloEnum;


public class ProtocoloConectaTelefone extends InterfaceProtocoloEnum {
	
    public enum ATRIBUTO{
    	mobile,
        mobileConectado,
        mobileIdentificador,
        corporacao,
        usuario	
    }
	public static String TAG = "ProtocoloConectaTelefone";
	public ProtocoloConectaTelefone(Context pContext,
			JSONObject pJsonObject) {
		super(pJsonObject, ATRIBUTO.class);
		
		
	}
	@Override
	public InterfaceProtocoloEnum factory(Context pContext,
			JSONObject pJsonObject) {
 
		return new ProtocoloConectaTelefone(pContext, pJsonObject);
	}
}
