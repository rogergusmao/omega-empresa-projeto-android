package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo;

import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.entidade.MONITORA_TELA_WEB_PARA_MOBILE;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceProtocoloEnum;


public class ProtocoloMonitoraTelaWebParaMobile extends InterfaceProtocoloEnum {
	
	public static String TAG = "ProtocoloMonitoraTelaWebParaMobile";
	public ProtocoloMonitoraTelaWebParaMobile(Context pContext,
			JSONObject pJsonObject) {
		super(pJsonObject, MONITORA_TELA_WEB_PARA_MOBILE.class);
		
		
	}
	public ProtocoloMonitoraTelaWebParaMobile() {
		
	}
	
	@Override
	public InterfaceProtocoloEnum factory(Context pContext,
			JSONObject pJsonObject) {
		
		return new ProtocoloMonitoraTelaWebParaMobile(pContext, pJsonObject);
	}
}
