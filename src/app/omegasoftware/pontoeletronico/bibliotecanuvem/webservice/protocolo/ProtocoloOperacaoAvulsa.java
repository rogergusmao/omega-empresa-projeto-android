package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo;

import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceProtocoloEnum;


public class ProtocoloOperacaoAvulsa extends InterfaceProtocoloEnum {
	
	
    public enum ATRIBUTO {
    	idOSM,
    	idODBM
    }
	public static String TAG = "ProtocoloOperacaoAvulsa";
	public ProtocoloOperacaoAvulsa(Context pContext,
			JSONObject pJsonObject) {
		
		super(pJsonObject, ATRIBUTO.class);
		
	}
	@Override
	public InterfaceProtocoloEnum factory(Context pContext,
			JSONObject pJsonObject) {
		
		return new ProtocoloOperacaoAvulsa(pContext, pJsonObject);
	}
	
}
