package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo;

import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceProtocoloEnum;

public class ProtocoloProcessoEmAberto extends InterfaceProtocoloEnum {

    public enum ATRIBUTO{
    	idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
        idSincronizacaoMobile,
        enviouDadosSincronizacaoMobile
    }
	public static String TAG = "ProtocoloProcessoEmAberto";
	public ProtocoloProcessoEmAberto(Context pContext,
			JSONObject pJsonObject) {
		super(pJsonObject, ATRIBUTO.class);
		
		
	}
	public ProtocoloProcessoEmAberto() {
	
	}
	@Override
	public InterfaceProtocoloEnum factory(Context pContext,
			JSONObject pJsonObject) {

		return new ProtocoloProcessoEmAberto(pContext, pJsonObject);
	}
}
