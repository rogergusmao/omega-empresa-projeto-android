package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.database.InterfaceProtocolo;


public class ProtocoloSuporte extends InterfaceProtocolo{
	public boolean isSuporteRequisitado = false;
	public Long totSegundosTempoReqSuporte = null;
	public String usuario = null;
	
    
	public static String TAG = "ProtocoloSuporte";
	public ProtocoloSuporte(Context pContext,
			JSONObject pJsonObject) {
		
		try {
			this.isSuporteRequisitado = pJsonObject.getBoolean("isSuporteRequisitado");
			this.totSegundosTempoReqSuporte = pJsonObject.getLong("totSegundosTempoReqSuporte");
			this.usuario = pJsonObject.getString("usuarioRequisicao");
		} catch (JSONException e) {
			SingletonLog.insereErro( e, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
		}
		
		
	}
	
    
}
