package app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.database.InterfaceProtocolo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile;


public class ProtocoloTransfereScriptBancoAndroidParaWeb extends InterfaceProtocolo {
	
	public ProtocoloArquivo protocoloArquivo;
	public EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE mEstado;
	public static String TAG = "ProtocoloTransfereScriptBancoAndroidParaWeb";
	public ProtocoloTransfereScriptBancoAndroidParaWeb(Context pContext,
			JSONObject pJsonObject) {
		
		
		try {
			if(pJsonObject != null && !pJsonObject.isNull("protocoloArquivo"))
				this.protocoloArquivo = new ProtocoloArquivo(pContext, pJsonObject.getJSONObject("protocoloArquivo"));
			this.mEstado = EXTDAOSistemaOperacaoSistemaMobile.getEstadoOperacaoSistemaMobile(pJsonObject.getInt("idEstado"));
		} catch (JSONException e) {
			SingletonLog.insereErro( e, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
		}
		
		
	}
	
    
}
