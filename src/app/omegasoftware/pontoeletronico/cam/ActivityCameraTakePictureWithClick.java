package app.omegasoftware.pontoeletronico.cam;


//import android.app.Activity;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.hardware.Camera;
//import android.hardware.Camera.PictureCallback;
//import android.hardware.Camera.ShutterCallback;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.Button;
//import android.widget.FrameLayout;
//import app.omegasoftware.pontoeletronico.R;
//import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
//import app.omegasoftware.pontoeletronico.file.HelperFile;
//import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
//
//public class ActivityCameraTakePictureWithClick extends Activity {
//	private static final String TAG = "ActivityCameraTakePictureWithClick";
//	Camera camera;
//	Preview preview;
//	Button buttonClick;
//	String pathDirectory;
//	/** Called when the activity is first created. */
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.camera_demo);
//
//		Bundle vParameters = getIntent().getExtras();
//		this.pathDirectory = vParameters.getString(CameraConfiguration.PATH_DIRECTORY_FIELD);
//
//		preview = new Preview(this);
//		((FrameLayout) findViewById(R.id.preview)).addView(preview);
//
//		buttonClick = (Button) findViewById(R.id.buttonClick);
//		buttonClick.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				preview.camera.takePicture(shutterCallback, rawCallback,
//						jpegCallback);
//			}
//		});
//
//		Log.d(TAG, "onCreate'd");
//	}
//
//	ShutterCallback shutterCallback = new ShutterCallback() {
//		public void onShutter() {
//			Log.d(TAG, "onShutter'd");
//		}
//	};
//
//	/** Handles data for raw picture */
//	PictureCallback rawCallback = new PictureCallback() {
//		public void onPictureTaken(byte[] data, Camera camera) {
//			Log.d(TAG, "onPictureTaken - raw");
//		}
//	};
//
//	/** Handles data for jpeg picture */
//	PictureCallback jpegCallback = new PictureCallback() {
//		public void onPictureTaken(byte[] data, Camera camera) {
//			//Decode the data obtained by the camera into a Bitmap
//			//			Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);
//			//
//			//			Intent intent = getIntent();
//			//
//			//			intent.putExtra(CameraConfiguration.PARAMETER_PICTURE, picture);
//			//			setResult(1, intent);
//			//			finish();
//
//			//FileOutputStream outStream = null;
//			try {
//				// write to local sandbox file system
//				// outStream =
//				// CameraDemo.this.openFileOutput(String.format("%d.jpg",
//				// System.currentTimeMillis()), 0);
//				// Or write to sdcard
//
//				String vFileName = HelperFile.getFileWithUniqueName() + ".jpg";
//
//				Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);
//				HelperCamera.savePhotoInFile(
//						pathDirectory,
//						vFileName,
//						picture,
//						OmegaFileConfiguration.PERCENT_QUALITY,
//						OmegaFileConfiguration.COMPRESS_FORMAT);
////				String vNamePicture = HelperFile.getFileWithUniqueName() + ".jpg";
////				String path =String.format("/sdcard/%s", vNamePicture);
////				outStream = new FileOutputStream(path);
////				outStream.write(data);
////				outStream.close();
//				Intent intent = getIntent();
//				intent.putExtra(CameraConfiguration.PARAMETER_PICTURE, vFileName);
//				setResult(OmegaConfiguration.ACTIVITY_CAMERA_TAKE_PICTURE_WITH_CLICK, intent);
//				finish();
//				Log.d(TAG, "onPictureTaken - wrote bytes: " + data.length);
//			} catch (Exception e) {
//				e.printStackTrace();
//			} finally {
//			}
//			Log.d(TAG, "onPictureTaken - jpeg");
//		}
//	};
//
//}