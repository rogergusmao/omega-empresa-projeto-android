
package app.omegasoftware.pontoeletronico.cam;


import java.io.FileOutputStream;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.FrameLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.file.HelperFile;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;

//public class ActivityCameraTakePictureWithTimer extends Activity {
//	private static final String TAG = "ActivityCameraTakePictureWithTimer";
//
//	Preview preview;
//	Button buttonClick;
//	String pathDirectory;
//	private Timer uiUpdaterTimer = new Timer();
//	/** Called when the activity is first created. */
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.camera_take_picture_with_timer);
//
//		Bundle vParameters = getIntent().getExtras();
//
//		//Gets funcionario id from the Intent used to create this activity
//		//TODO: Test connecting between ListResultActivity and FuncionarioDetailsActivity
//		//this.funcionarioId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
//		this.pathDirectory = vParameters.getString(CameraConfiguration.PATH_DIRECTORY_FIELD);
//
//		preview = new Preview(this);
//		((FrameLayout) findViewById(R.id.preview)).addView(preview);
//		setTimer();
//		Log.d(TAG, "onCreate'd");
//	}
//
//	class TakePictureTimer extends TimerTask{
//		Activity activity;
//		public TakePictureTimer(Activity pActivity){
//			activity = pActivity;
//		}
//
//		@Override
//		public void run() {
//			if(shutterCallback != null &&
//					rawCallback != null &&
//					jpegCallback != null){
//			preview.camera.takePicture(shutterCallback, rawCallback,
//					jpegCallback);
//				this.cancel();
//			}
//
//		}
//	}
//
//	private void setTimer(){
//
//		//Starts ui updater timer
//		this.uiUpdaterTimer.scheduleAtFixedRate(
//			new TakePictureTimer(this),
//			CameraConfiguration.INITIAL_TAKE_PICTURE_TIMER,
//			CameraConfiguration.TAKE_PICTURE_TIMER);
//	}
//
//	ShutterCallback shutterCallback = new ShutterCallback() {
//		public void onShutter() {
//			Log.d(TAG, "onShutter'd");
//		}
//	};
//
//	/** Handles data for raw picture */
//	PictureCallback rawCallback = new PictureCallback() {
//		public void onPictureTaken(byte[] data, Camera camera) {
//			Log.d(TAG, "onPictureTaken - raw");
//		}
//	};
//
//	/** Handles data for jpeg picture */
//	PictureCallback jpegCallback = new PictureCallback() {
//		public void onPictureTaken(byte[] data, Camera camera) {
//			//Decode the data obtained by the camera into a Bitmap
//			//			Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);
//			//
//			//			Intent intent = getIntent();
//			//
//			//			intent.putExtra(CameraConfiguration.PARAMETER_PICTURE, picture);
//			//			setResult(1, intent);
//			//			finish();
//
//
//			try {
//
//				String vFileName = HelperFile.getFileWithUniqueName() + ".jpg";
//
//				Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);
//				HelperCamera.savePhotoInFile(
//						pathDirectory,
//						vFileName,
//						picture,
//						OmegaFileConfiguration.PERCENT_QUALITY,
//						OmegaFileConfiguration.COMPRESS_FORMAT);
//				//				outStream.close();
//				Intent intent = getIntent();
//
//				intent.putExtra(CameraConfiguration.PARAMETER_PICTURE, vFileName);
//				setResult(OmegaConfiguration.ACTIVITY_CAMERA_TAKE_PICTURE_WITH_CLICK, intent);
//				finish();
//				Log.d(TAG, "onPictureTaken - wrote bytes: " + data.length);
//			} catch (Exception e) {
//				SingletonLog.insereErro(e, TIPO.UTIL_ANDROID);
//			} finally {
//			}
//			Log.d(TAG, "onPictureTaken - jpeg");
//		}
//	};
//
//}