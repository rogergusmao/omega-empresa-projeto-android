package app.omegasoftware.pontoeletronico.cam;


import android.app.Activity;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;

public class ActivityCameraVideoRecorder extends Activity {
	private static final String TAG = "ActivityCameraVideoRecorder";
	MediaRecorder mediaRecorder;
	VideoRecorderPreview preview;
	Button recorderButton;
	Button stopButton;
	String pathFile;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.camera_video_recorder_activity_mobile_layout);

		Bundle vParameters = getIntent().getExtras();

		//Gets funcionario id from the Intent used to create this activity
		//TODO: Test connecting between ListResultActivity and FuncionarioDetailsActivity
		//this.funcionarioId = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		this.pathFile = vParameters.getString(CameraConfiguration.PATH_DIRECTORY_FIELD);

		
		mediaRecorder = new MediaRecorder();
		mediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
		mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		
		mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		//mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
		
		mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);
		mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		
		preview = new VideoRecorderPreview(this, mediaRecorder, pathFile);
		
        
        //setContentView(preview);
		((FrameLayout) findViewById(R.id.preview)).addView(preview);
		
		recorderButton = (Button) findViewById(R.id.record_button);
		recorderButton.setOnClickListener(new VideoRecorderOnClickListenner(this));
		
		stopButton = (Button) findViewById(R.id.stop_button);
		stopButton.setOnClickListener(new StopOnClickListenner(this));
		stopButton.setVisibility(View.GONE);
		
		Log.d(TAG, "onCreate'd");
	}
	@Override
	public void onBackPressed(){
		if(stopButton.getVisibility() == View.VISIBLE){
			stopButton.setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
			//mediaRecorder.prepare();
			 mediaRecorder.stop();
			 // mediaRecorder.release();
			 Intent intent = getIntent();
			 intent.putExtra(CameraConfiguration.PARAMETER_PICTURE, pathFile);
			
			intent.putExtra(CameraConfiguration.PARAMETER_PICTURE, pathFile);
			setResult(OmegaConfiguration.ACTIVITY_CAMERA_VIDEO_RECORDER, intent);	
		}
		
		super.onBackPressed();
	}
	
	public class VideoRecorderOnClickListenner implements OnClickListener{
		Activity activity;
		public VideoRecorderOnClickListenner(Activity pActivity){
			activity = pActivity;
		}

		public void onClick(View arg0) {
			
			
			try {
				mediaRecorder.setOutputFile(pathFile);
				mediaRecorder.prepare();
				mediaRecorder.start();
				
				recorderButton.setVisibility(View.GONE);
				stopButton.setVisibility(View.VISIBLE);
			} catch (Exception e) {
				
				e.printStackTrace();
				mediaRecorder.release();
	            mediaRecorder = null;
			}
	       
		}
		
	}
	

	
	public class StopOnClickListenner implements OnClickListener{
		Activity activity;
		public StopOnClickListenner(Activity pActivity){
			activity = pActivity;
		}

		public void onClick(View arg0) {
			
			
			try {
				
				stopButton.setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
				
				mediaRecorder.stop();
				mediaRecorder.release();
				// mediaRecorder.release();
				Intent intent = activity.getIntent();
				intent.putExtra(CameraConfiguration.PARAMETER_PICTURE, pathFile);
				activity.setResult(OmegaConfiguration.ACTIVITY_CAMERA_VIDEO_RECORDER, intent);
				activity.finish();
			} catch (Exception e) {
				
				e.printStackTrace();
			}
	       
		}
		
	}

}