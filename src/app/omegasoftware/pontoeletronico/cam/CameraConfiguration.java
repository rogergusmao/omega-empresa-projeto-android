package app.omegasoftware.pontoeletronico.cam;

public class CameraConfiguration {
	public static final long INITIALIZE_TAKE_PICTURE_DELAY = 1000;
	public static final long TAKE_PICTURE_DELAY = 6000;
	public static final long FINALIZE_TAKE_PICTURE_DELAY = 1000;
	public static final String TAKE_PICTURE_INTENT = "android.omegasoftware.cam.TAKE_PICTURE_INTENT";
	
	public static final String PATH_DIRECTORY_FIELD = "PATH_DIRECTORY_FIELD";
	
	
	//Timing configurations
		public static final long INITIAL_TAKE_PICTURE_TIMER = 3000;
		public static final long TAKE_PICTURE_TIMER = 1000;
		public static final long INITIAL_INFORMATION_PONTO_ELETRONICO= 5000;
		public static final long INFORMATION_PONTO_ELETRONICO_TIMER = 1000;
	
	public static final String PARAMETER_PICTURE = "parameter_acitivity";
	//CameraPreview
	static final int TAKING_PICTURE = 0;
}
