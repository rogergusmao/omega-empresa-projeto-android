/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package app.omegasoftware.pontoeletronico.cam;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;


// ----------------------------------------------------------------------

public class CameraPreview extends Activity {    
	private Preview mPreview;
	protected static String TAG = "CameraPreview";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Hide the window title.
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Create our Preview view and set it as the content of our activity.
		mPreview = new Preview(this);
		setContentView(mPreview);

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case CameraConfiguration.TAKING_PICTURE:
			ProgressDialog mProgressDialog;
			mProgressDialog = new ProgressDialog(this);
			mProgressDialog.setMessage("Preparing picture ...");
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
			return mProgressDialog;
		default:
			return null;
		}
	}

boolean isFirstTime = true;


	// ----------------------------------------------------------------------

	class Preview extends SurfaceView implements SurfaceHolder.Callback {
		private Timer uiUpdaterTimer = new Timer();
		SurfaceHolder mHolder;
		Camera mCamera;
//		PictureTakerTask pictureTakerTask;
		Activity activity;
		boolean mPreviewRunning = false;
		Bitmap picture = null;
		
		Preview(Activity p_activity) {
			super(p_activity);
			
			activity = p_activity;
			// Install a SurfaceHolder.Callback so we get notified when the
			// underlying surface is created and destroyed.
			mHolder = getHolder();
			mHolder.addCallback(this);
			mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
			
//			setTimer();
//			pictureTakerTask = new PictureTakerTask(this, mCamera);
//			
//			
//			pictureTakerTask.execute();
		}

		public Activity getActivity(){
			return activity;
		}

		public void surfaceCreated(SurfaceHolder holder) {
			// The Surface has been created, acquire the camera and tell it where
			// to draw.
			if(mCamera != null){
				mCamera.release();	
			}
			try {
				mCamera = Camera.open();
				isFirstTime = false;
				mCamera.setPreviewDisplay(holder);
				
			} catch (Exception exception) {
//				if(mCamera != null){
//					mCamera.release();	
//				}
				// TODO: add more exception handling logic here
			}
		}

		public void surfaceDestroyed(SurfaceHolder holder) {
			// Surface will be destroyed when we return, so stop the preview.
			// Because the CameraDevice object is not a shared resource, it's very
			// important to release it when the activity is paused.
			try {
			if(mPreviewRunning){
				mCamera.stopPreview();
				mPreviewRunning = false;
				mCamera.release();	
				
			}
			}catch (Exception exception) {
				if(mCamera != null){
					mCamera.release();	
				}
				
				
				// TODO: add more exception handling logic here
			}
		}

		public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
			// Now that the size is known, set up the camera parameters and begin
			// the preview.
			try {
				Camera.Parameters parameters = mCamera.getParameters();
				parameters.setPreviewSize(w, h);
				if(mCamera != null){
					mCamera.setParameters(parameters);
					mCamera.setPreviewDisplay(holder);
					mCamera.startPreview();	
					mPreviewRunning = true;
				}	
			} catch (Exception exception) {
				if(mCamera != null){
					mCamera.release();	
				}// TODO: add more exception handling logic here
			}
		}
		
		public void finishActivity(){
			if(mCamera != null){
				mCamera.stopPreview();
				mCamera.release();
				mCamera = null;
			}
			
			activity.finish();
		}
		public void setTimer(){
			
			//Starts ui updater timer
			this.uiUpdaterTimer.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					try {
////						showDialog(CameraConfiguration.TAKING_PICTURE);
//						if(mCamera != null && mPreviewRunning){
//							mCamera.takePicture(null, mPictureCallback, mPictureCallback);
//							try {
//								Thread.sleep(CameraConfiguration.FINALIZE_TAKE_PICTURE_DELAY);
//							} catch (InterruptedException e) {
//								Log.e(TAG,"doInBackground(): " + e.getMessage());
//							}
//							//preview.finishActivity();
//							finishActivity();
//						}
						//			takePictureCallBack.finish();
					} catch(Exception ex){
						Log.w("onPostExecute", "Take picture");
					} finally{
//						dismissDialog(CameraConfiguration.TAKING_PICTURE);
					}
	
				}
			},
			CameraConfiguration.INITIALIZE_TAKE_PICTURE_DELAY,
			CameraConfiguration.TAKE_PICTURE_DELAY);
		}
//		
//		Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {
//			public void onPictureTaken(byte[] data, Camera camera) {
//
//				if (data != null) {
//
//					
//
//					//Decode the data obtained by the camera into a Bitmap
//					picture = BitmapFactory.decodeByteArray(data, 0, data.length);
//					
//					Intent intent = activity.getIntent();
//
//					intent.putExtra(CameraConfiguration.PARAMETER_PICTURE, picture);
//					activity.setResult(1, intent);
//
//				}
//			}
//		};

//
//		class PictureTakerTask extends AsyncTask<String, Integer, Boolean>
//		{
//
//
//			Camera camera;
////			TakePictureCallBack takePictureCallBack;
//			Preview preview;
//			public PictureTakerTask(Preview preview, Camera p_cam) {
//				this.preview = preview;
////				takePictureCallBack = new TakePictureCallBack(preview.getActivity());
//				this.camera = p_cam;
//			}
//
//			@Override
//			protected void onPreExecute(){
//				super.onPreExecute();
//				showDialog(CameraConfiguration.TAKING_PICTURE);
//			}
//
//			@Override
//			protected Boolean doInBackground(String... arg0) {
//
//				try {
//					Thread.sleep(CameraConfiguration.TAKE_PICTURE_DELAY);
//				} catch (InterruptedException e) {
//					Log.e(TAG,"doInBackground(): " + e.getMessage());
//				}
//
//				return true;
//
//			}
//
//
//
//			@Override
//			protected void onPostExecute(Boolean result) {
//				try {
//					if(camera != null){
//						camera.takePicture(null, mPictureCallback, mPictureCallback);
//						try {
//							Thread.sleep(CameraConfiguration.FINALIZE_TAKE_PICTURE_DELAY);
//						} catch (InterruptedException e) {
//							Log.e(TAG,"doInBackground(): " + e.getMessage());
//						}
//						//preview.finishActivity();
//					}
//					dismissDialog(CameraConfiguration.TAKING_PICTURE);
//					
//					//			takePictureCallBack.finish();
//				} catch(Exception ex){
//					Log.w("onPostExecute", "Take picture");
//				}
//
//			}
//			Bitmap picture = null;
//			Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {
//				public void onPictureTaken(byte[] data, Camera camera) {
//
//					if (data != null) {
//
//						
//
//						//Decode the data obtained by the camera into a Bitmap
//						picture = BitmapFactory.decodeByteArray(data, 0, data.length);
//						
//						Intent intent = activity.getIntent();
//
//						intent.putExtra(CameraConfiguration.PARAMETER_PICTURE, picture);
//						activity.setResult(1, intent);
//
//					}
//				}
//			};
//
//		}
		
	}
//
//	class TakePictureCallBack implements Camera.PictureCallback  {
//
//		Activity activity;
//		Bitmap picture = null;
//		
//		public TakePictureCallBack(Activity p_activity){
//
//			this.activity = p_activity;
//		}
//
//		public Bitmap getPicture(){
//			return picture;
//		}
//
//		public void onPictureTaken(byte[] data, Camera cam) {
//			
//
//			//Decode the data obtained by the camera into a Bitmap
//			picture = BitmapFactory.decodeByteArray(data, 0, data.length);
//			
//			Intent intent = activity.getIntent();
//
//			intent.putExtra(CameraConfiguration.PARAMETER_PICTURE, picture);
//			activity.setResult(1, intent);
//			//this.finish();
//		}
//
//	}
}


