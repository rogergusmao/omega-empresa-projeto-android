package app.omegasoftware.pontoeletronico.cam;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import app.omegasoftware.pontoeletronico.R;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;

public class CameraTakePictureActivity extends Activity implements SurfaceHolder.Callback,Camera.PictureCallback  {

	private static final String TAG = "CameraTakePictureActivity";
	
	private Camera camera;
	private Parameters cameraParameters;
	
	private SurfaceView cameraSurfaceView;
	private SurfaceHolder cameraSurfaceHolder;
	
	
	
	//List of dialogs
	static final int TAKING_PICTURE = 0;
	
	/** Called when the activity is first created. */  
	@Override  
	public void onCreate(Bundle savedInstanceState)  
	{  
		super.onCreate(savedInstanceState);  
		
		
//		Bundle vParameters = getIntent().getExtras();
		
		//Integer idView = vParameters.getInt(CameraConfiguration.VIEW_CAMERA_TAKE_PICTURE,);
		setContentView(R.layout.camera_take_picture_activity_layout);

		this.cameraSurfaceView = (SurfaceView) findViewById(R.id.camera_preview_surface);
		this.cameraSurfaceHolder = this.cameraSurfaceView.getHolder();
		this.cameraSurfaceHolder.addCallback(this);
		this.cameraSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		new PictureTaker(this).execute();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
//		this.camera.release();
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		
		this.cameraParameters = this.camera.getParameters();
		this.camera.setParameters(this.cameraParameters);
		this.camera.startPreview();
		
	}

	public void surfaceCreated(SurfaceHolder holder) {
	
		try
		{
			this.camera = Camera.open();	
			this.cameraParameters = this.camera.getParameters();
			this.cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_INFINITY);
			this.cameraParameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
			this.camera.setPreviewDisplay(this.cameraSurfaceHolder);
			
//			this.camera.setDisplayOrientation(90);
			
		}
		catch(Exception e)
		{
			//TODO Handle this exception
			this.camera.release();
			this.camera = null;
		}
		
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		

		try
		{
			this.camera.stopPreview();
			this.camera.release();
			this.camera = null;	
		}
		catch(Exception e)
		{
		}
		
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
			case TAKING_PICTURE:
				ProgressDialog mProgressDialog;
				mProgressDialog = new ProgressDialog(this);
				mProgressDialog.setMessage("Preparing picture ...");
				mProgressDialog.setCancelable(false);
				mProgressDialog.show();
				return mProgressDialog;
			default:
				return null;
		}
	}
	
	private class PictureTaker extends AsyncTask<String, Integer, Boolean>
	{

		CameraTakePictureActivity parent;
		
		public PictureTaker(CameraTakePictureActivity parent) {
			this.parent = parent;
		}
		
		@Override
    	protected void onPreExecute(){
    		super.onPreExecute();
    		showDialog(TAKING_PICTURE);
    		
    	}
		
		@Override
		protected Boolean doInBackground(String... arg0) {

			try {
				Thread.sleep(CameraConfiguration.TAKE_PICTURE_DELAY);
			} catch (InterruptedException e) {
				Log.e(TAG,"doInBackground(): " + e.getMessage());
			}
			try {
	    		if(camera != null){
					camera.takePicture(null, null, this.parent);	
	    		}
			}
	    		catch(Exception ex){
					Log.w("onPostExecute", "Take picture");
				}
	    		
			return true;
			
		}
		
		@Override
		 protected void onPostExecute(Boolean result) {
			try {
				
					parent.finish();
				
					
			} catch(Exception ex){
				Log.w("onPostExecute", "Take picture");
			}
			try {
			dismissDialog(TAKING_PICTURE);
			} catch(Exception ex){
				Log.w("onPostExecute", "Take picture");
			}
		}
		
	}

	public void onPictureTaken(byte[] data, Camera camera) {
		
		//Decode the data obtained by the camera into a Bitmap
		Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);
		
		Intent intent = getIntent();
		intent.putExtra(CameraConfiguration.PARAMETER_PICTURE, picture);
		//this.finish();

	}
	
	public void storePictureTakenInFile(byte[] data, Camera camera){
//		Decode the data obtained by the camera into a Bitmap
		Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);
//		TODO confirir se o comando abaixo tem como parametro o path correto
		File file = new File(getApplicationContext().getFilesDir(), this.generatePictureFileName());
		
		try {
			FileOutputStream fos = new FileOutputStream(file);
			picture.compress(OmegaConfiguration.PICTURE_COMPRESS_FORMAT, OmegaConfiguration.PICTURE_COMPRESS_QUALITY, fos);
			fos.close();
		} catch (FileNotFoundException e) {
			Log.e(TAG,"onPictureTaken(): " + e.getMessage());
		} catch (IOException e) {
			Log.e(TAG,"onPictureTaken(): " + e.getMessage());
		}
		 
		finish();
				
	}
	
	private String generatePictureFileName()
	{
		String timestamp = String.valueOf(Calendar.getInstance().getTime().getTime());
		return timestamp + OmegaConfiguration.PICTURE_FILES_EXTENSION;
	}

}

