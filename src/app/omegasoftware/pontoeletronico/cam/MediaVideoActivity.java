package app.omegasoftware.pontoeletronico.cam;

import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;

public class MediaVideoActivity extends OmegaRegularActivity {
	public static String TAG = "MediaVideoActivity";
    VideoView myVideoView ;
    String mFilePath;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.media_video_activity_mobile_layout);
		myVideoView = (VideoView)findViewById(R.id.myvideoview1);
		Bundle vParameters = getIntent().getExtras();

		this.mFilePath = vParameters.getString(CameraConfiguration.PATH_DIRECTORY_FIELD);
		
		play();

	}
	
	@Override
	public boolean loadData() {
		
		return true;
	}

	@Override
	public void initializeComponents() {
		
		
	}

	public void play(){
		
       myVideoView.setVideoURI(Uri.parse(mFilePath));
       myVideoView.setMediaController(new MediaController(this));
       myVideoView.requestFocus();
       Runnable r = new Runnable() {
         public void run() {
             try {
            	 myVideoView.start();
                 
             } catch (Exception e) {
                 Log.e(TAG, e.getMessage(), e);
             }
         }
     };
     new Thread(r).start();
       
	}
	

	
	public void videoPlayer(String path, String fileName, boolean autoplay){
	    //get current window information, and set format, set it up differently, if you need some special effects
	    getWindow().setFormat(PixelFormat.TRANSLUCENT);
	    //the VideoView will hold the video
	    VideoView videoHolder = new VideoView(this);
	    //MediaController is the ui control howering above the video (just like in the default youtube player).
	    videoHolder.setMediaController(new MediaController(this));
	    //assing a video file to the video holder
	    videoHolder.setVideoURI(Uri.parse(path+"/"+fileName));
	    //get focus, before playing the video.
	    videoHolder.requestFocus();
	    if(autoplay){
	        videoHolder.start();
	    }
	 
	 }
}
