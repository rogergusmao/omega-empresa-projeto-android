package app.omegasoftware.pontoeletronico.cam;
import java.util.List;

import android.content.Context;
import android.hardware.Camera;

import android.hardware.Camera.Size;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;

//public class Preview extends SurfaceView implements SurfaceHolder.Callback {
//	protected static final String TAG = "Preview";
//
//	SurfaceHolder holder;
//	public Camera camera;
//	public FrameLayout frameLayout;
//	public LinearLayout llControleCamera;
//	int tipoCamera;
//	public boolean horizontal;
//	public Preview(Context context, FrameLayout frameLayout, int tipoCamera, boolean horizontal) {
//		super(context);
//		this.horizontal = horizontal;
////		CameraInfo.CAMERA_FACING_FRONT
//		this.tipoCamera = tipoCamera;
//		this.frameLayout = frameLayout;
//		this.llControleCamera = (LinearLayout) frameLayout.findViewById(R.id.ll_controle_camera);
//
//		// Install a SurfaceHolder.Callback so we get notified when the
//		// underlying surface is created and destroyed.
//		holder = getHolder();
//		holder.addCallback(this);
//		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
//
//	}
//
//	public Preview(Context context) {
//		super(context);
//
//		// Install a SurfaceHolder.Callback so we get notified when the
//		// underlying surface is created and destroyed.
//		holder = getHolder();
//		holder.addCallback(this);
//		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
//	}
//
//	public boolean possuiOutraCamera(){
//		try{
//			Camera.Parameters parameters = camera.getParameters();
//			List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
//			if(previewSizes.size() > 0) return true;
//			else return false;
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, TIPO.PAGINA);
//			return false;
//		}
//
//	}
//	public void surfaceCreated(SurfaceHolder holder) {
//		// The Surface has been created, acquire the camera and tell it where
//		// to draw.
//		try{
//			if(camera == null)
//				camera = Camera.open(tipoCamera);
//			this.holder = holder;
//
//			startCamera();
//		}catch (Exception e) {
//			SingletonLog.insereErro(e, TIPO.PAGINA);
//		}
//
//	}
//
//	public void release(){
//		try{
//			camera.release();
//			holder.removeCallback(this);
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, TIPO.FORMULARIO);
//		}
//
//	}
//
//	public void surfaceDestroyed(SurfaceHolder holder) {
//		// Surface will be destroyed when we return, so stop the preview.
//		// Because the CameraDevice object is not a shared resource, it's very
//		// important to release it when the activity is paused.
//		try{
//
//			if(camera != null){
//				camera.stopPreview();
//				camera.setPreviewCallback(null);
//				camera.release();
//			}
//			camera = null;
//		} catch(Exception ex){
//			SingletonLog.insereErro(ex, TIPO.PAGINA);
//		}
//	}
//
//	public boolean possuiCamera(){
//		Camera.Parameters parameters = camera.getParameters();
//
//		List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
//		if(previewSizes.size() > 0 ){
//			return true;
//		} else return false;
//	}
//
//	public Size getPreviewSizeDoTipoDeCamera(int idTipoCamera){
//		Camera.Parameters parameters = camera.getParameters();
//		List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
//		int idMenorResolucao = -1;
//		int menorWidth = -1;
//		int maiorWidth = -1;
//		int idMaiorResolucao = -1;
//		for(int i = 0 ; i < previewSizes.size(); i++){
//			Size s = previewSizes.get(i);
////			if(horizontal){
////				if(s.width < s.height)
////					continue;
////			} else if(!horizontal){
////				if(s.width > s.height)
////					continue;
////			}
//			int largura = s.width;
//			if(menorWidth > largura
//					|| menorWidth == -1){
//				idMenorResolucao = i;
//				menorWidth = largura;
//			}
//			if(maiorWidth < largura
//					|| maiorWidth == -1){
//				idMaiorResolucao = i;
//				maiorWidth = largura;
//			}
//		}
//
//		if(idTipoCamera == CameraInfo.CAMERA_FACING_FRONT && previewSizes.size() == 1)
//			return null;
//		else if(idTipoCamera == CameraInfo.CAMERA_FACING_FRONT && previewSizes.size() > 1){
//			return previewSizes.get(idMenorResolucao);
//		}else if(idTipoCamera == CameraInfo.CAMERA_FACING_BACK ){
//			return previewSizes.get(idMaiorResolucao);
//		}  else return null;
//
//	}
//
////	public void flipcamera(int tipoCamera) {
////		this.tipoCamera = tipoCamera;
////        if (camera != null)
////        {
////	         release();
////        }
////        holder = getHolder();
////		holder.addCallback(this);
////		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
////
////        camera = Camera.open(tipoCamera);
////
////        startCamera();
////    }
//
//	private void startCamera(){
//		if(camera != null){
//			try {
//				camera.setPreviewDisplay(holder);
//				Camera.Parameters parameters = camera.getParameters();
//				Camera.Size previewSize = getPreviewSizeDoTipoDeCamera(tipoCamera);
//				if(previewSize != null){
//
//					parameters.setPreviewSize(previewSize.width, previewSize.height);
//					camera.setDisplayOrientation(90);
//					camera.setParameters(parameters);
//
//					camera.startPreview();
//					llControleCamera.bringToFront();
//				}
//			} catch (Exception e) {
//				SingletonLog.insereErro(e, TIPO.PAGINA);
//			}
//		}
//	}
//	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
//		try{
//			this.holder = holder;
//
//            startCamera();
//
//            if(llControleCamera != null)
//            llControleCamera.bringToFront();
//		}
//		catch(Exception ex){
//			SingletonLog.insereErro(ex, TIPO.PAGINA);
//		}
//	}
//
//}