package app.omegasoftware.pontoeletronico.cam;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import app.omegasoftware.pontoeletronico.R;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;

public class TiraFotoActivityMobileActivity extends Activity implements SurfaceHolder.Callback,Camera.PictureCallback  {

	private static final String TAG = "TiraFotoActivityMobileActivity";
	
	private Camera camera;
	private Parameters cameraParameters;
	
	private SurfaceView cameraSurfaceView;
	private SurfaceHolder cameraSurfaceHolder;
	
	//List of dialogs
	static final int TAKING_PICTURE = 0;
	
	/** Called when the activity is first created. */  
	@Override  
	public void onCreate(Bundle savedInstanceState)  
	{  
		super.onCreate(savedInstanceState);  
		setContentView(R.layout.tira_foto_activity_mobile_activity);
		
		this.cameraSurfaceView = (SurfaceView) findViewById(R.id.camera_preview_surface);
		this.cameraSurfaceHolder = this.cameraSurfaceView.getHolder();
		this.cameraSurfaceHolder.addCallback(this);
		this.cameraSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		new PictureTaker(this).execute();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		this.camera.release();
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		
		this.cameraParameters = this.camera.getParameters();
		this.camera.setParameters(this.cameraParameters);
		this.camera.startPreview();
		
	}

	public void surfaceCreated(SurfaceHolder holder) {
	
		try
		{
			this.camera = Camera.open();	
			this.cameraParameters = this.camera.getParameters();
			this.cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_INFINITY);
			this.cameraParameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
			this.camera.setPreviewDisplay(this.cameraSurfaceHolder);
//			this.camera.setDisplayOrientation(90);
			
		}
		catch(Exception e)
		{
			//TODO Handle this exception
			this.camera.release();
			this.camera = null;
		}
		
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		

		try
		{
			this.camera.stopPreview();
			this.camera.release();
			this.camera = null;	
		}
		catch(Exception e)
		{
		}
		
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
			case TAKING_PICTURE:
				ProgressDialog mProgressDialog;
				mProgressDialog = new ProgressDialog(this);
				mProgressDialog.setMessage("Preparing picture ...");
				mProgressDialog.setCancelable(false);
				mProgressDialog.show();
				return mProgressDialog;
			default:
				return null;
		}
	}
	
	private class PictureTaker extends AsyncTask<String, Integer, Boolean>
	{

		TiraFotoActivityMobileActivity parent;
		
		public PictureTaker(TiraFotoActivityMobileActivity parent) {
			this.parent = parent;
		}
		
		@Override
    	protected void onPreExecute(){
    		super.onPreExecute();
    		showDialog(TAKING_PICTURE);
    	}
		
		@Override
		protected Boolean doInBackground(String... arg0) {

			try {
				Thread.sleep(OmegaConfiguration.SERVICE_TIRA_FOTO_DELAY);
			} catch (InterruptedException e) {
				Log.e(TAG,"doInBackground(): " + e.getMessage());
			}
			
			return true;
			
		}
		
		@Override
		 protected void onPostExecute(Boolean result) {
			
		  dismissDialog(TAKING_PICTURE);
		  camera.takePicture(null, null, this.parent);
		  
		}
		
	}
	
	
	public void onPictureTaken(byte[] data, Camera camera) {
		
		//Decode the data obtained by the camera into a Bitmap
		Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);
		OmegaFileConfiguration config = new OmegaFileConfiguration(); 

		
		String path = config.getPath(OmegaFileConfiguration.TIPO.FOTO);
		File file = new File(
				getApplicationContext().getFilesDir(), 
				path);
		
		try {
			FileOutputStream fos = new FileOutputStream(file);
			picture.compress(
					OmegaConfiguration.PICTURE_COMPRESS_FORMAT, 
					OmegaConfiguration.PICTURE_COMPRESS_QUALITY, 
					fos);
			fos.close();
		} catch (FileNotFoundException e) {
			Log.e(TAG,"onPictureTaken(): " + e.getMessage());
		} catch (IOException e) {
			Log.e(TAG,"onPictureTaken(): " + e.getMessage());
		}
		 
		finish();
		
	}
	
	protected String generatePictureFileName()
	{
		String timestamp = String.valueOf(Calendar.getInstance().getTime().getTime());
		return timestamp + OmegaConfiguration.PICTURE_FILES_EXTENSION;
	}

}

