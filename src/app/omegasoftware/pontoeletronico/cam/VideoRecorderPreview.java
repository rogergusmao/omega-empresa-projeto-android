package app.omegasoftware.pontoeletronico.cam;

import android.content.Context;
import android.media.MediaRecorder;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

class VideoRecorderPreview extends SurfaceView implements SurfaceHolder.Callback
{
    SurfaceHolder mHolder;
    MediaRecorder tempRecorder;
    String mFilePath;
    
    public VideoRecorderPreview(Context context,MediaRecorder recorder, String pFilePath) {
        super(context);
        mFilePath = pFilePath;
        tempRecorder = recorder;
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        
    }

    public Surface getSurface()
    {
        return mHolder.getSurface();
    }

    public void surfaceCreated(SurfaceHolder holder){
        tempRecorder.setPreviewDisplay(mHolder.getSurface());
    }

    public void surfaceDestroyed(SurfaceHolder holder) 
    {

    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) 
    {

    }
}   