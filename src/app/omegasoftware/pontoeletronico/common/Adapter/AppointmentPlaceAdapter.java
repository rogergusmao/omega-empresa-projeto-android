package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.AppointmentPlaceItemList;
import app.omegasoftware.pontoeletronico.listener.MapButtonClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class AppointmentPlaceAdapter extends BaseAdapter {

	private ArrayList<AppointmentPlaceItemList> appointmentPlaces;
	private Context context;
	public boolean emailAtivo = true;
	public boolean telefoneAtivo = true;
	public boolean enderecoAtivo = true;

	public AppointmentPlaceAdapter(Context context)
	{
		this(context,new ArrayList<AppointmentPlaceItemList>());
		
	}

	public AppointmentPlaceAdapter(Context context, ArrayList<AppointmentPlaceItemList> appointmentPlaces)
	{
		this.appointmentPlaces = appointmentPlaces;
		this.context = context;
		
	}
	public AppointmentPlaceAdapter(Context context, AppointmentPlaceItemList appointmentPlace)
	{
		this.appointmentPlaces = new ArrayList<AppointmentPlaceItemList>();
		this.appointmentPlaces.add(appointmentPlace);
		this.context = context;

	}

	public int getCount() {
		return this.appointmentPlaces.size();
	}

	public AppointmentPlaceItemList getItem(int position) {
		if(position >= this.appointmentPlaces.size()) return null;
		return this.appointmentPlaces.get(position);
	}

	public long getItemId(int position) {
		return this.appointmentPlaces.get(position).getId();
	}

	private void refreshTitulo(View v, AppointmentPlaceItemList a ){

		if(a != null && a.getTitulo() != null){
			TextView tvTitulo = (TextView) v.findViewById(R.id.tv_titulo);
			tvTitulo.setText(a.getTitulo());
			tvTitulo.setVisibility(View.VISIBLE);
		}
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		AppointmentPlaceItemList appointmentPlace = this.getItem(position);
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View layoutView = layoutInflater.inflate(R.layout.appointment_place_item_list_layout, null);

		ImageButton buttonShowOnMap = (ImageButton) layoutView.findViewById(R.id.appointment_place_showmap_button);
		buttonShowOnMap.setOnClickListener(new MapButtonClickListener(this.getItemId(position), appointmentPlace));

		//		TextView titleTextView = (TextView) layoutView.findViewById(R.id.appointment_place_title);
		//		titleTextView.setText(String.format(context.getResources().getString(R.string.appointent_place_title), position + 1));
		boolean validade = false;

		TextView cityTextView = (TextView) layoutView.findViewById(R.id.appointment_place_city);
		TextView ufTextView = (TextView) layoutView.findViewById(R.id.appointment_place_uf);
		TextView countryTextView = (TextView) layoutView.findViewById(R.id.appointment_place_country);
		TextView cepTextView = (TextView) layoutView.findViewById(R.id.appointment_place_cep);
		TextView complementoTextView = (TextView) (layoutView.findViewById(R.id.appointment_place_complemento));

		refreshTitulo(layoutView, appointmentPlace);

		refreshEntidade(layoutView, appointmentPlace);

		boolean validadeEndereco = false;
		if(enderecoAtivo){
			String address = appointmentPlace.getStrAddressSimplificado(null);
			TextView addressTextView = (TextView) layoutView.findViewById(R.id.appointment_place_address);
			if(address != null && address.length() > 0){
				validade = true;
				addressTextView.setText(HelperString.ucFirstForEachToken(address));
				validadeEndereco = true;
			} else {
				addressTextView.setVisibility(View.GONE);
			}

			String complemento = appointmentPlace.getComplemento();
			if(complemento != null && complemento.length() > 0){
				validade = true;
				complementoTextView.setText(HelperString.ucFirstForEachToken(complemento));
				validadeEndereco = true;
			} else {
				complementoTextView.setVisibility(View.GONE);
			}
			
			
			String vCity = appointmentPlace.getCity();
			if(vCity != null && vCity.length() > 0){
				validade = true;
				cityTextView.setText(HelperString.ucFirstForEachToken(vCity));
				
				validadeEndereco = true;
			} else cityTextView.setVisibility(View.GONE);
			
			
			String vUf = appointmentPlace.getUf();
			if(vUf != null && vUf.length() > 0){
				validade = true;
				validadeEndereco = true;
				ufTextView.setText(HelperString.ucFirstForEachToken(vUf));
				
			} else ufTextView.setVisibility(View.GONE);

			
			String country = appointmentPlace.getCountry();
			if(country != null && country.length() > 0){
				validade = true;
				validadeEndereco = true;
				countryTextView.setText(HelperString.ucFirstForEachToken(country));
				
			} else countryTextView.setVisibility(View.GONE);

			String vCep = appointmentPlace.getCEPWithStartingString();
			if(vCep != null && vCep.length() > 0){
				validade = true;
				cepTextView.setText(HelperString.ucFirstForEachToken(vCep));
				
			} else cepTextView.setVisibility(View.GONE);
			
			if(!validadeEndereco){
				buttonShowOnMap.setVisibility(View.GONE);
				((RelativeLayout)layoutView.findViewById(R.id.rlEndereco)).setVisibility(View.GONE);
			} else{
				((RelativeLayout)layoutView.findViewById(R.id.rlEndereco)).setVisibility(View.VISIBLE);
			}
			
			
		} else {
			((RelativeLayout)layoutView.findViewById(R.id.rlEndereco)).setVisibility(View.GONE);
		}
		
		LinearLayout emailAndPhoneHolder = (LinearLayout) layoutView.findViewById(R.id.email_and_phone_holder_linearlayout);

		PhoneEmailAdapter phoneAdapter = null;
		if(telefoneAtivo)
			phoneAdapter = appointmentPlace.getPhoneAdapter();
		PhoneEmailAdapter emailAdapter = null;
		if(emailAtivo)
			emailAdapter = appointmentPlace.getEmailAdapter();
		
		if(phoneAdapter != null || emailAdapter != null){
		
			LinearLayout phonesLayout = null;
			
			if(phoneAdapter != null){
				LinearLayout.LayoutParams parametrosPai = new LinearLayout.LayoutParams(
						LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
				
				parametrosPai.setMargins(10, 5, 5, 0);
				phonesLayout = new LinearLayout(this.context);
				phonesLayout.setLayoutParams(parametrosPai);
				phonesLayout.setOrientation(LinearLayout.HORIZONTAL);
				
				for(int i=0;i<phoneAdapter.getCount();i++)
				{
					validade = true;
					LinearLayout.LayoutParams parametros = new LinearLayout.LayoutParams(
							LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					if(i > 0)
						parametros.setMargins(0,5,5,5);
					LinearLayout auxLayout = new LinearLayout(this.context);
					auxLayout.setLayoutParams(parametros);
					auxLayout.setOrientation(LinearLayout.HORIZONTAL);
					
					auxLayout.addView(phoneAdapter.getView(i, null, null));
					phonesLayout.addView(auxLayout);	
				}
				
				emailAndPhoneHolder.addView(phonesLayout);
			}
	
			if(emailAdapter != null){
				for(int i=0;i<emailAdapter.getCount();i++)
				{
					validade = true;
					emailAndPhoneHolder.addView(emailAdapter.getView(i, null, null));
				}
			}
		} else {
			emailAndPhoneHolder.setVisibility(View.GONE);
		}
		if(! validade) return null;
		else return layoutView;

	}


	public void refreshEntidade( View layoutView, AppointmentPlaceItemList item){

		ImageButton  btEntidade = (ImageButton)layoutView.findViewById(R.id.bt_entidade);
		TextView tvEntidade = (TextView)layoutView.findViewById(R.id.tv_entidade);
		Integer idDrawable= null;
		if(item.tipoEndereco != null && item.idEntidade != null && item.classEntidade != null){
			switch (item.tipoEndereco){
				case EMPRESA:
					idDrawable = R.drawable.ico_company;
					break;
				case PESSOA:
					idDrawable = R.drawable.ico_male;
				default:
					break;
			}
			if(idDrawable != null){
				final Intent intent = new Intent(
						this.context
						, item.classEntidade);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, item.idEntidade);
				btEntidade.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						context.startActivity(intent);
					}
				});


				tvEntidade.setText(item.descricaoEntidade);

				btEntidade.setVisibility(View.VISIBLE);
				tvEntidade.setVisibility(View.VISIBLE);

				btEntidade.setImageResource(idDrawable);
				btEntidade.invalidate();
			}
		}
	}


}
