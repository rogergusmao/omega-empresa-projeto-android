package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormBairroActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.ItemList.BairroItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOBairro;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class BairroAdapter extends DatabaseCustomAdapter{

//	private static final int ID_TEXT_VIEW_BAIRRO = R.id.bairro_textview;
//	private static final int ID_TEXT_VIEW_CIDADE = R.id.cidade_textview;
//	private static final int ID_TEXT_VIEW_ESTADO = R.id.estado_textview;
//	private static final int ID_TEXT_VIEW_PAIS = R.id.pais_textview;
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;

	String estadoId;
	public String paisId;
	public String cidadeId;
	public String bairroNome;
	
	public BairroAdapter(
			Activity pActivity,
			String pBairroNome,
			String pCidadeId,
			String pEstadoId,
			String pPaisId,
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, p_isAsc, EXTDAOBairro.NAME, BairroItemList.BAIRRO, false);
		paisId =pPaisId;
		estadoId =pEstadoId;
		cidadeId =pCidadeId; 
		bairroNome = pBairroNome;
		
		
		super.initalizeListCustomItemList();
	}


	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			BairroItemList vItemList = (BairroItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.item_list_layout, null);
			
			PersonalizaItemView(vLayoutView, pPosition);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_1, 
					vItemList.getConteudoContainerItem(BairroItemList.BAIRRO), 
					vLayoutView);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_2, 
					vItemList.getConteudoContainerItem(BairroItemList.CIDADE), 
					vLayoutView);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_3, 
					vItemList.getConteudoContainerItem(BairroItemList.ESTADO), 
					vLayoutView);
			
			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_4, 
					vItemList.getConteudoContainerItem(BairroItemList.PAIS), 
					vLayoutView);


			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
						activity, 
						EXTDAOBairro.NAME,
						pParent, 
						vItemList.getId(),
						FormEditActivityMobile.class, 
						FormBairroActivityMobile.class,
						isEditPermitido(),
						isRemovePermitido(), vItemList);
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_BAIRRO, 
						vClickListener);
			}
			else {
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_BAIRRO);
			}
			return vLayoutView;
		} catch (Exception exc) {
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
					"EmpresaAdapter: getView()");
		}
		return null;
	}



	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {

		
		DatabasePontoEletronico database=null;
		try {
			database = new DatabasePontoEletronico(this.activity);
		} catch (OmegaDatabaseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {

			String querySelectIdPrestadores = "SELECT DISTINCT b."
					+ EXTDAOBairro.ID + " ";

			String queryFromTables =  "FROM " + EXTDAOBairro.NAME + " b ";

			String queryWhereClausule = ""; 
			ArrayList<String> args = new ArrayList<String>();

			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " b." + EXTDAOBairro.CORPORACAO_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND b." + EXTDAOBairro.CORPORACAO_ID_INT + " = ? " ;
			
			args.add(OmegaSecurity.getIdCorporacao());
			
			if(this.bairroNome != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " b." + Attribute.getNomeAtributoNormalizado( EXTDAOBairro.NOME) + " LIKE ? " ;
				else 
					queryWhereClausule += " AND b." + Attribute.getNomeAtributoNormalizado(EXTDAOBairro.NOME) + " LIKE ? " ;

				args.add("%" + HelperString.getWordWithoutAccent( this.bairroNome) + "%");
			}
			//Se o bairro tiver sido setado entao nao ha a necessidade de setar a cidade
			if( this.cidadeId != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " b." + EXTDAOBairro.CIDADE_ID_INT + " = ? " ;
				else 
					queryWhereClausule += " AND b." + EXTDAOBairro.CIDADE_ID_INT + " = ? " ;

				args.add(this.cidadeId);
			}
			
			boolean vIsCidadeInFromClause = false;
			//Se o cidade tiver sido setado entao nao ha a necessidade de setar o estado
			if( this.estadoId != null && this.cidadeId == null ){
				if(!vIsCidadeInFromClause){
					queryFromTables +=  ", " + EXTDAOCidade.NAME + " c " ;
					
					if(queryWhereClausule.length() ==  0)
						queryWhereClausule += " b." + EXTDAOBairro.CIDADE_ID_INT + " = c." + EXTDAOCidade.ID ;
					else 
						queryWhereClausule += " AND b." + EXTDAOBairro.CIDADE_ID_INT + " = c." + EXTDAOCidade.ID ;
					
					vIsCidadeInFromClause = true;
				}
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " c." + EXTDAOCidade.UF_ID_INT + " = ? " ;
				else 
					queryWhereClausule += " AND c." + EXTDAOCidade.UF_ID_INT + " = ? " ;

				args.add(this.estadoId);
				vIsCidadeInFromClause = true;
			}

			boolean vIsEstadoInFromClause = false;
			if( this.paisId != null && this.cidadeId == null && this.estadoId == null){
				if(!vIsCidadeInFromClause){
					queryFromTables +=  ", " + EXTDAOCidade.NAME + " c " ;
					
					if(queryWhereClausule.length() ==  0)
						queryWhereClausule += " b." + EXTDAOBairro.CIDADE_ID_INT + " = c." + EXTDAOCidade.ID ;
					else 
						queryWhereClausule += " AND b." + EXTDAOBairro.CIDADE_ID_INT + " = c." + EXTDAOCidade.ID ;
					
					vIsCidadeInFromClause = true;
				}
				if(!vIsEstadoInFromClause){
					queryFromTables +=  ", " + EXTDAOUf.NAME + " uf " ;
					
					if(queryWhereClausule.length() ==  0)
						queryWhereClausule += " c." + EXTDAOCidade.UF_ID_INT + " = uf." + EXTDAOUf.ID;
					else 
						queryWhereClausule += " AND c." + EXTDAOCidade.UF_ID_INT + " = uf." + EXTDAOUf.ID;
					
					vIsEstadoInFromClause = true;
				}
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " uf." + EXTDAOUf.PAIS_ID_INT + " = ? " ;
				else 
					queryWhereClausule += " AND uf." + EXTDAOUf.PAIS_ID_INT + " = ? " ;

				args.add(this.paisId);
			}
			
			
			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY b." + Attribute.getNomeAtributoNormalizado(EXTDAOBairro.NOME)
						+ " DESC";
			} else {
				orderBy += " ORDER BY  b." + Attribute.getNomeAtributoNormalizado(EXTDAOBairro.NOME)
						+ " ASC";
			}

			String query = querySelectIdPrestadores + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

			if(orderBy.length() > 0 ){
				query += orderBy;  
			} 

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			
			Cursor cursor = null;
			try{
				cursor = database.rawQuery(query, vetorArg);
				// Cursor cursor = oh.query(true, EXTDAOBairro.NAME,
				// new String[]{EXTDAOBairro.ID_PRESTADOR}, "", new String[]{},
				// null, "", "", "");
				String[] vetorChave = new String[] { EXTDAOBairro.ID };

				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}
		} catch (Exception e) {
			
			e.printStackTrace();
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
		finally
		{
			database.close();
		}
		return null;


	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {
try{
		EXTDAOBairro vEXTDAOBairro = (EXTDAOBairro) pObj;
		
		EXTDAOCidade vObjCidade = new EXTDAOCidade(pObj.getDatabase());
		String vIdCidade = pObj.getStrValueOfAttribute(EXTDAOBairro.CIDADE_ID_INT);
		String vNomeEstado = null;
		String vNomeCidade = null;
		String vNomePais = null;
		if(vIdCidade != null){
			vObjCidade.setAttrValue(EXTDAOCidade.ID, vIdCidade);
			vObjCidade.select();
			vNomeCidade = vObjCidade.getStrValueOfAttribute(EXTDAOCidade.NOME);
			String vIdEstado = vObjCidade.getStrValueOfAttribute(EXTDAOCidade.UF_ID_INT);
			if(vIdEstado != null){
				EXTDAOUf vObjUf = new EXTDAOUf(pObj.getDatabase());
				vObjUf.setAttrValue(EXTDAOUf.ID, vIdEstado);
				vObjUf.select();
				vNomeEstado = vObjUf.getStrValueOfAttribute(EXTDAOUf.NOME);
				vNomePais= vObjUf.getNomeDaChaveExtrangeira(EXTDAOUf.PAIS_ID_INT);
			}
		}
		BairroItemList vItemList = new BairroItemList(  
				vEXTDAOBairro.getAttribute(EXTDAOBairro.ID).getStrValue(),
				vEXTDAOBairro.getAttribute(EXTDAOBairro.NOME).getStrValue(),
				vNomeCidade,
				vNomeEstado,
				vNomePais);

		return vItemList;
}catch(Exception ex){
	SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
return null;
}
	}
}

