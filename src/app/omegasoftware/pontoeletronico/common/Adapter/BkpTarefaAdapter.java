package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormTarefaActivityMobile;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.TarefaItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa.ContainerResponsavel;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.listener.GestureOnTouchListener;
import app.omegasoftware.pontoeletronico.listener.TarefaButtonClickListener;
import app.omegasoftware.pontoeletronico.listener.TarefaSlideSimpleOnGestureListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

//public class BkpTarefaAdapter extends DatabaseCustomAdapter{
//
//	private static final int ID_TEXT_VIEW_NOME_TAREFA = R.id.nome_tarefa;
//	private static final int ID_TEXT_VIEW_USUARIO_TAREFA = R.id.usuario_tarefa;
//	private static final int ID_TEXT_VIEW_ORIGEM_TAREFA = R.id.endereco_origem_tarefa;
//	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;
//	Activity activity;
//
//
//	private EXTDAOTarefa.TIPO_TAREFA  tipoTarefa;
//	private String identificadorTipoTarefa;
//	public EXTDAOTarefa.TIPO_ENDERECO tipoOrigemEndereco;
//	public EXTDAOTarefa.TIPO_ENDERECO tipoDestinoEndereco;
//	public String identificadorTipoOrigemEndereco;
//	public String identificadorTipoDestinoEndereco;
//	public String criadoPeloUsuario;
//
//	public EXTDAOTarefa.ESTADO_TAREFA vetorEstadoTarefa[];
//
//
//
//	String titulo;
//
//	String origemPessoa;
//	String origemEmpresa;
//
//	String destinoPessoa;
//	String destinoEmpresa;
//
//	String origemLogradouro;
//	String origemNumero;
//	String origemComplemento;
//	String origemIdCidade;
//	String origemIdBairro;
//
//	String destinoLogradouro;
//	String destinoNumero;
//	String destinoComplemento;
//	String destinoCidadeId;
//	String destinoBairroId;
//
//	private String inicioDataProgramada;
//	private String inicioHoraProgramada;
//
//	private String inicioData;
//	private String inicioHora;
//	private String fimData;
//	private String fimHora;
//
//	private String dataCadastro;
//	private String horaCadastro;
//	private String dataExibir;
//	private String descricao;
//
//	public BkpTarefaAdapter(
//			Activity pActivity,
//			EXTDAOTarefa.TIPO_TAREFA pTipoTarefa,
//			EXTDAOTarefa.ESTADO_TAREFA pEstadoTarefa,
//			String pIdentificadorTipoTarefa,
//			EXTDAOTarefa.TIPO_ENDERECO pTipoOrigemEndereco,
//			EXTDAOTarefa.TIPO_ENDERECO pTipoDestinoEndereco,
//			String pIdentificadorTipoOrigemEndereco,
//			String pIdentificadorTipoDestinoEndereco,
//			String pCriadoPeloUsuario,
//			String pTitulo,
//			String pOrigemLogradouro,
//			String pOrigemNumero,
//			String pOrigemComplemento,
//			 String pOrigemIdCidade,
//			 String pOrigemIdBairro,
//			 String pDestinoLogradouro,
//				String pDestinoNumero,
//				String pDestinoComplemento,
//			 String pDestinoCidade,
//			 String pDestinoBairro,
//			 String inicioDataProgramada,
//			 String inicioHoraProgramada,
//			 String inicioData,
//			 String inicioHora,
//			 String fimData,
//			 String fimHora,
//			 String dataCadastro,
//			 String horaCadastro,
//			 String dataExibir,
//			 String descricao,
//			boolean p_isAsc)
//	{
//		super(pActivity, p_isAsc, EXTDAOTarefa.NAME, TarefaItemList.DATETIME_CADASTRO_TAREFA, false);
//		tipoTarefa = pTipoTarefa;
//		identificadorTipoTarefa = pIdentificadorTipoTarefa;
//		tipoOrigemEndereco = pTipoOrigemEndereco;
//		tipoDestinoEndereco = pTipoDestinoEndereco;
//		identificadorTipoOrigemEndereco = pIdentificadorTipoOrigemEndereco;
//		identificadorTipoDestinoEndereco = pIdentificadorTipoDestinoEndereco;
//
//		criadoPeloUsuario = pCriadoPeloUsuario;
//		titulo = pTitulo;
//
//		vetorEstadoTarefa = new EXTDAOTarefa.ESTADO_TAREFA[1];
//		vetorEstadoTarefa[0] =pEstadoTarefa;
//
//		this.origemIdCidade = pOrigemIdCidade;
//		this.origemIdBairro = pOrigemIdBairro;
//		this.destinoCidadeId = pDestinoCidade;
//		this.destinoBairroId = pDestinoBairro;
//		origemLogradouro = pOrigemLogradouro;
//		origemNumero= pOrigemNumero;
//		origemComplemento= pOrigemComplemento;
//		destinoLogradouro = pDestinoLogradouro;
//		destinoNumero= pDestinoNumero;
//		destinoComplemento= pDestinoComplemento;
//		 this.inicioDataProgramada = inicioDataProgramada;
//		 this.inicioHoraProgramada = inicioHoraProgramada;
//		 this.inicioData = inicioData;
//		 this.inicioHora = inicioHora;
//		 this.fimData = fimData;
//		 this.fimHora = fimHora;
//		 this.dataCadastro = dataCadastro;
//		 this.horaCadastro = horaCadastro;
//		 this.dataExibir = dataExibir;
//		 this.descricao = descricao;
//		 activity = pActivity;
//		initalizeListCustomItemList();
//	}
//
//	public BkpTarefaAdapter(
//			Activity pActivity,
//			EXTDAOTarefa.TIPO_TAREFA pTipoTarefa,
//			EXTDAOTarefa.ESTADO_TAREFA pVetorEstadoTarefa[],
//			String pIdentificadorTipoTarefa,
//			EXTDAOTarefa.TIPO_ENDERECO pTipoOrigemEndereco,
//			EXTDAOTarefa.TIPO_ENDERECO pTipoDestinoEndereco,
//			String pIdentificadorTipoOrigemEndereco,
//			String pIdentificadorTipoDestinoEndereco,
//			String pCriadoPeloUsuario,
//			String pTitulo,
//			String pOrigemLogradouro,
//			String pOrigemNumero,
//			String pOrigemComplemento,
//			 String pOrigemIdCidade,
//			 String pOrigemIdBairro,
//			 String pDestinoLogradouro,
//				String pDestinoNumero,
//				String pDestinoComplemento,
//			 String pDestinoCidade,
//			 String pDestinoBairro,
//			 String inicioDataProgramada,
//			 String inicioHoraProgramada,
//			 String inicioData,
//			 String inicioHora,
//			 String fimData,
//			 String fimHora,
//			 String dataCadastro,
//			 String horaCadastro,
//			 String dataExibir,
//			 String descricao,
//			 Boolean pIsMinhaTarefa,
//			boolean p_isAsc)
//	{
//		super(pActivity, p_isAsc, EXTDAOTarefa.NAME, TarefaItemList.DATETIME_CADASTRO_TAREFA, false);
//		tipoTarefa = pTipoTarefa;
//		identificadorTipoTarefa = pIdentificadorTipoTarefa;
//		tipoOrigemEndereco = pTipoOrigemEndereco;
//		tipoDestinoEndereco = pTipoDestinoEndereco;
//		identificadorTipoOrigemEndereco = pIdentificadorTipoOrigemEndereco;
//		identificadorTipoDestinoEndereco = pIdentificadorTipoDestinoEndereco;
//
//		criadoPeloUsuario = pCriadoPeloUsuario;
//		titulo = pTitulo;
//
//		vetorEstadoTarefa = new EXTDAOTarefa.ESTADO_TAREFA[1];
//		vetorEstadoTarefa =pVetorEstadoTarefa;
//
//		this.origemIdCidade = pOrigemIdCidade;
//		this.origemIdBairro = pOrigemIdBairro;
//		this.destinoCidadeId = pDestinoCidade;
//		this.destinoBairroId = pDestinoBairro;
//		origemLogradouro = pOrigemLogradouro;
//		origemNumero= pOrigemNumero;
//		origemComplemento= pOrigemComplemento;
//		destinoLogradouro = pDestinoLogradouro;
//		destinoNumero= pDestinoNumero;
//		destinoComplemento= pDestinoComplemento;
//		 this.inicioDataProgramada = inicioDataProgramada;
//		 this.inicioHoraProgramada = inicioHoraProgramada;
//		 this.inicioData = inicioData;
//		 this.inicioHora = inicioHora;
//		 this.fimData = fimData;
//		 this.fimHora = fimHora;
//		 this.dataCadastro = dataCadastro;
//		 this.horaCadastro = horaCadastro;
//		 this.dataExibir = dataExibir;
//		 this.descricao = descricao;
//		 activity = pActivity;
//		initalizeListCustomItemList();
//	}
//
//	public View getView(int pPosition, View pView, ViewGroup pParent) {
//		try {
//			TarefaItemList vItemList = (TarefaItemList) this.getItem(pPosition);
//			LayoutInflater vLayoutInflater = (LayoutInflater) activity
//					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//			View vLayoutView = vLayoutInflater.inflate(
//					R.layout.tarefa_minha_item_list_layout, null);
//
//			EXTDAOTarefa.ESTADO_TAREFA vEstado = vItemList.getEstadoTarefa();
//
//			switch (vEstado) {
//			case ABERTA:
//
//				if((pPosition % 2) == 0)
//					((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.drawable.menu_light_gray_button);
//				else
//					((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.drawable.menu_gray_button);
//
//
//				break;
//			case AGUARDANDO_INICIALIZACAO:
//				if((pPosition % 2) == 0)
//					((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.drawable.menu_light_green_button);
//				else
//					((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.drawable.menu_green_button);
//
//				break;
//			case EM_EXECUCAO:
//				if((pPosition % 2) == 0)
//					((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.drawable.menu_light_yellow_button);
//				else
//					((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.drawable.menu_yellow_button);
//
//				break;
//			case FINALIZADA:
//				if((pPosition % 2) == 0)
//					((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.drawable.menu_light_red_button);
//				else
//					((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.drawable.menu_red_button);
//
//				break;
//			default:
//				break;
//			}
//
//
//
//
//			String vDataInicioMarcado = vItemList.getConteudoContainerItem(TarefaItemList.DATETIME_INICIO_MARCADO_DA_TAREFA);
//			if(vDataInicioMarcado == null || vDataInicioMarcado.length() == 0 ){
//				vDataInicioMarcado = activity.getString(R.string.sem_hora_marcada_para_inicio_da_tarefa);
//			}
//
//			CreateTextView(
//					ID_TEXT_VIEW_NOME_TAREFA,
//					vDataInicioMarcado,
//					vLayoutView);
//
//			ContainerResponsavel vContainer = vItemList.getContainerResponsavel();
//			String vNomeResponsavel = null;
//			if(vContainer != null)
//				vNomeResponsavel = vContainer.getNomeResponsavel();
//			CreateTextView(
//					ID_TEXT_VIEW_USUARIO_TAREFA,
//					vNomeResponsavel,
//					vLayoutView);
//
//
//			CreateTextView(
//					ID_TEXT_VIEW_ORIGEM_TAREFA,
//					vItemList.getConteudoContainerItem(TarefaItemList.NOME_TAREFA),
//					vLayoutView);
//
//
//			if(isEditPermitido() || isRemovePermitido()){
//				OnLongClickListener vOnLongClickListener = new EditCustomOnLongClickListener(
//						activity,
//						EXTDAOTarefa.NAME,
//						pParent,
//						vItemList.getId(),
//						FormEditActivityMobile.class,
//						FormTarefaActivityMobile.class,
//						isEditPermitido(),
//						isRemovePermitido(), vItemList);
//
//				CreateTarefaDetailButton(vLayoutView,
//						ID_CUSTOM_BUTTON_DETAIL,
//						vItemList.getId(),
//						vItemList,
//						vOnLongClickListener);
//			}
//			else {
//				CreateTarefaDetailButton(vLayoutView,
//						ID_CUSTOM_BUTTON_DETAIL,
//						vItemList.getId(),
//						vItemList,
//						null);
//			}
//
//
//
//			if(isEditPermitido() || isRemovePermitido()){
//			OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
//					activity,
//					EXTDAOTarefa.NAME,
//					pParent,
//					vItemList.getId(),
//					FormEditActivityMobile.class,
//					FormTarefaActivityMobile.class,
//					isEditPermitido(),
//					isRemovePermitido(), vItemList);
//			LinearLayout vLinearLayout1 = (LinearLayout) vLayoutView.findViewById(R.id.linearLayoutfuncionariodetails1);
//
//			vLinearLayout1.setOnLongClickListener(vClickListener);
//			}
//			return vLayoutView;
//		} catch (Exception exc) {
//			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
////					"EmpresaAdapter: getView()");
//		}
//		return null;
//	}
//
//
//	protected void CreateTarefaDetailButton(
//			View pView,
//			int pRLayoutId,
//			String pIdCustomItemList,
//			TarefaItemList pTarefaItemList,
//			OnLongClickListener pOnLongClickListener)
//	{
//		Button vButton = (Button) pView.findViewById(pRLayoutId);
//		vButton.setClickable(true);
//		vButton.setFocusable(true);
//
//		vButton.setOnClickListener(new TarefaButtonClickListener(
//				activity,
//				pIdCustomItemList,
//				pTarefaItemList));
//		OnClickListener vOnClickListener = (OnClickListener) new TarefaButtonClickListener(
//				activity,
//				pIdCustomItemList,
//				pTarefaItemList);
//		TarefaSlideSimpleOnGestureListener vSlideListener = new TarefaSlideSimpleOnGestureListener(
//				pView,
//				pIdCustomItemList,
//				vOnClickListener,
//				pOnLongClickListener);
//		GestureDetector vGestor = new  GestureDetector(activity, vSlideListener);
//		GestureOnTouchListener vListener = new GestureOnTouchListener(vGestor);
//		listSlideGestureDetector.add(vGestor);
//		pView.setOnTouchListener(vListener);
//
//	}
//
//	@Override
//	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {
//
//
//		DatabasePontoEletronico database=null;
//		try {
//			database = new DatabasePontoEletronico(this.activity);
//		} catch (OmegaDatabaseException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//
//		try {
//
////			String querySelectIdPrestadores = "SELECT DISTINCT t."
////					+ EXTDAOTarefa.ID + ", " +  EXTDAOTarefa.DATA_CADASTRO_DATE + " ";
//
//			String querySelectIdPrestadores = "SELECT DISTINCT t."
//					+ EXTDAOTarefa.ID ;
//
//			String queryFromTables =  "FROM " + EXTDAOTarefa.NAME + " t ";
//
//			String queryWhereClausule = "";
//			ArrayList<String> args = new ArrayList<String>();
//
//			if(queryWhereClausule.length() ==  0)
//				queryWhereClausule += " t." + EXTDAOTarefa.CORPORACAO_ID_INT + " = ? " ;
//			else
//				queryWhereClausule += " AND t." + EXTDAOTarefa.CORPORACAO_ID_INT + " = ? " ;
//
//			args.add(OmegaSecurity.getIdCorporacao());
//
//			if( criadoPeloUsuario != null){
//				queryWhereClausule += " t." + EXTDAOTarefa.CRIADO_PELO_USUARIO_ID_INT + " LIKE ? ";
//				args.add(criadoPeloUsuario);
//			}
//			if(vetorEstadoTarefa != null && vetorEstadoTarefa.length > 0){
//				for(int i = 0; i < vetorEstadoTarefa.length; i++){
//					EXTDAOTarefa.ESTADO_TAREFA estadoTarefa = vetorEstadoTarefa[i];
//					if(estadoTarefa != null ){
//
//						switch(estadoTarefa){
//							case ABERTA:
//								queryWhereClausule += " t." + EXTDAOTarefa.USUARIO_ID_INT + " IS NULL ";
//								break;
////							case AGUARDANDO_INICIALIZACAO:
////								queryWhereClausule += " t." + EXTDAOTarefa.INICIO_DATA_DATE + " IS NULL ";
////								break;
////							case EM_EXECUCAO:
////								queryWhereClausule += " t." + EXTDAOTarefa.INICIO_DATA_DATE + " NOT IS NULL ";
////								break;
////							case FINALIZADA:
////								queryWhereClausule += " t." + EXTDAOTarefa.FIM_DATA_DATE + " NOT IS NULL ";
////								break;
//							default:
//								break;
//						}
//					}
//				}
//			}
//
//
//			if( identificadorTipoTarefa != null && tipoTarefa != null){
//				boolean vPossuiRestricao = false;
//				switch (tipoTarefa) {
//				case ABERTA:
//					queryWhereClausule += " t." + EXTDAOTarefa.USUARIO_ID_INT + " IS NULL ";
//					break;
//				case USUARIO:
//					queryWhereClausule += " t." + EXTDAOTarefa.USUARIO_ID_INT + " = ? ";
//					vPossuiRestricao = true;
//					break;
//				case CATEGORIA_PERMISSAO:
//					queryWhereClausule += " t." + EXTDAOTarefa.CATEGORIA_PERMISSAO_ID_INT + " = ? ";
//					vPossuiRestricao = true;
//					break;
//				case VEICULO:
//					queryWhereClausule += " t." + EXTDAOTarefa.VEICULO_ID_INT + " = ? ";
//					vPossuiRestricao = true;
//					break;
////				case VEICULO_USUARIO:
////					queryWhereClausule += " t." + EXTDAOTarefa.VEICULO_USUARIO_ID_INT + " = ? ";
////					vPossuiRestricao = true;
////					break;
//				default:
//					break;
//				}
//
//				if(vPossuiRestricao)
//					args.add(identificadorTipoTarefa);
//			}
//
//
//			if( tipoOrigemEndereco != null && identificadorTipoOrigemEndereco != null){
//				boolean vPossuiRestricao = false;
//				switch (tipoOrigemEndereco) {
//				case EMPRESA:
//					queryWhereClausule += " t." + EXTDAOTarefa.ORIGEM_EMPRESA_ID_INT + " = ? ";
//					vPossuiRestricao = true;
//					break;
//				case PESSOA:
//					queryWhereClausule += " t." + EXTDAOTarefa.ORIGEM_PESSOA_ID_INT + " = ? ";
//					vPossuiRestricao = true;
//					break;
//				default:
//					break;
//				}
//
//				if(vPossuiRestricao)
//					args.add(identificadorTipoOrigemEndereco);
//			}
//
//			if( tipoDestinoEndereco != null && identificadorTipoDestinoEndereco != null){
//				boolean vPossuiRestricao = false;
//				switch (tipoDestinoEndereco) {
//				case EMPRESA:
//					queryWhereClausule += " t." + EXTDAOTarefa.DESTINO_EMPRESA_ID_INT + " = ? ";
//					vPossuiRestricao = true;
//					break;
//				case PESSOA:
//					queryWhereClausule += " t." + EXTDAOTarefa.DESTINO_PESSOA_ID_INT + " = ? ";
//					vPossuiRestricao = true;
//					break;
//				default:
//					break;
//				}
//
//				if(vPossuiRestricao)
//					args.add(identificadorTipoDestinoEndereco);
//			}
//
//			if( this.origemLogradouro != null){
//				if(queryWhereClausule.length() ==  0)
//				queryWhereClausule += " t." + Attribute.getNomeAtributoNormalizado(EXTDAOTarefa.ORIGEM_LOGRADOURO) + " LIKE ? ";
//				else
//					queryWhereClausule += " AND t." + Attribute.getNomeAtributoNormalizado(EXTDAOTarefa.ORIGEM_LOGRADOURO) + " LIKE ? ";
//				args.add("%" + HelperString.getWordWithoutAccent(this.origemLogradouro) + "%");
//			}
//
//			if( this.origemNumero != null){
//				if(queryWhereClausule.length() ==  0)
//				queryWhereClausule += " t." + EXTDAOTarefa.ORIGEM_NUMERO + " LIKE ? ";
//				else
//					queryWhereClausule += " AND t." + EXTDAOTarefa.ORIGEM_NUMERO + " LIKE ? ";
//				args.add("%" + this.origemNumero + "%");
//			}
//
////			if( this.origemComplemento != null){
////				if(queryWhereClausule.length() ==  0)
////				queryWhereClausule += " t." + Attribute.getNomeAtributoNormalizado(EXTDAOTarefa.ORIGEM_COMPLEMENTO) + " LIKE ? ";
////				else
////					queryWhereClausule += " AND t." + Attribute.getNomeAtributoNormalizado(EXTDAOTarefa.ORIGEM_COMPLEMENTO) + " LIKE ? ";
////				args.add("%" + HelperString.getWordWithoutAccent(this.origemComplemento) + "%");
////			}
//
//			if( this.origemIdCidade != null){
//				if(queryWhereClausule.length() ==  0)
//				queryWhereClausule += " t." + EXTDAOTarefa.ORIGEM_CIDADE_ID_INT + " = ? ";
//				else
//					queryWhereClausule += " AND t." + EXTDAOTarefa.ORIGEM_CIDADE_ID_INT + " = ? ";
//				args.add(this.origemIdCidade);
//			}
//
////			if(this.origemIdBairro != null ){
////				if(queryWhereClausule.length() ==  0)
////					queryWhereClausule += " t." + EXTDAOTarefa.ORIGEM_BAIRRO_ID_INT + " = ? " ;
////				else
////					queryWhereClausule += " AND t." + EXTDAOTarefa.ORIGEM_BAIRRO_ID_INT + " = ? " ;
////
////				args.add(this.origemIdBairro);
////			}
//
//			if( this.destinoLogradouro != null){
//				if(queryWhereClausule.length() ==  0)
//				queryWhereClausule += " t." + Attribute.getNomeAtributoNormalizado(EXTDAOTarefa.DESTINO_LOGRADOURO) + " LIKE ? ";
//				else
//					queryWhereClausule += " AND t." + Attribute.getNomeAtributoNormalizado(EXTDAOTarefa.DESTINO_LOGRADOURO) + " LIKE ? ";
//				args.add("%" + HelperString.getWordWithoutAccent(this.destinoLogradouro) + "%");
//			}
//
//			if( this.destinoNumero != null){
//				if(queryWhereClausule.length() ==  0)
//				queryWhereClausule += " t." + EXTDAOTarefa.DESTINO_NUMERO + " LIKE ? ";
//				else
//					queryWhereClausule += " AND t." + EXTDAOTarefa.DESTINO_NUMERO + " LIKE ? ";
//				args.add("%" + this.destinoNumero + "%");
//			}
//
////			if( this.destinoComplemento != null){
////				if(queryWhereClausule.length() ==  0)
////				queryWhereClausule += " t." + EXTDAOTarefa.DESTINO_COMPLEMENTO + " LIKE ? ";
////				else
////					queryWhereClausule += " AND t." + EXTDAOTarefa.DESTINO_COMPLEMENTO + " LIKE ? ";
////				args.add("%" + this.destinoComplemento + "%");
////			}
//
//			if( this.destinoCidadeId != null){
//				queryWhereClausule += " t." + EXTDAOTarefa.DESTINO_CIDADE_ID_INT + " = ? ";
//				args.add(this.destinoCidadeId);
//			}
//
////			if(this.destinoBairroId != null ){
////				if(queryWhereClausule.length() ==  0)
////					queryWhereClausule += " t." + EXTDAOTarefa.DESTINO_BAIRRO_ID_INT + " = ? " ;
////				else
////					queryWhereClausule += " AND t." + EXTDAOTarefa.DESTINO_BAIRRO_ID_INT + " = ? " ;
////
////				args.add(this.destinoBairroId);
////			}
//
////			if(this.dataCadastro != null ){
////				if(queryWhereClausule.length() ==  0)
////					queryWhereClausule += " t." + EXTDAOTarefa.DATA_CADASTRO_DATE + " = ?" ;
////				else
////					queryWhereClausule += " AND t." + EXTDAOTarefa.DATA_CADASTRO_DATE + " = ?" ;
////
////				args.add( this.dataCadastro );
////			}
////
////			if(this.horaCadastro != null ){
////				if(queryWhereClausule.length() ==  0)
////					queryWhereClausule += " t." + EXTDAOTarefa.HORA_CADASTRO_TIME + " = ?" ;
////				else
////					queryWhereClausule += " AND t." + EXTDAOTarefa.HORA_CADASTRO_TIME + " = ?" ;
////
////				args.add( this.horaCadastro );
////			}
//
////			if(this.dataExibir != null ){
////				if(queryWhereClausule.length() ==  0)
////					queryWhereClausule += " t." + EXTDAOTarefa.DATA_EXIBIR_DATE + " = ?" ;
////				else
////					queryWhereClausule += " AND t." + EXTDAOTarefa.DATA_EXIBIR_DATE + " = ?" ;
////
////				args.add( this.dataExibir );
////			} else {
////				HelperDate vDateAtual = new HelperDate();
////				queryWhereClausule += " (t." + EXTDAOTarefa.DATA_EXIBIR_DATE + " > " + vDateAtual.getDateDisplay() + " OR t."+ EXTDAOTarefa.DATA_EXIBIR_DATE + " IS NULL) ";
////			}
//
////			if(this.inicioDataProgramada  != null ){
////				if(queryWhereClausule.length() ==  0)
////					queryWhereClausule += " t." + EXTDAOTarefa.INICIO_DATA_PROGRAMADA_DATE  + " = ?" ;
////				else
////					queryWhereClausule += " AND t." + EXTDAOTarefa.INICIO_DATA_PROGRAMADA_DATE + " = ?" ;
//
////				args.add( this.inicioDataProgramada );
////			}
//
//
////			if(this.inicioHoraProgramada != null ){
////				if(queryWhereClausule.length() ==  0)
////					queryWhereClausule += " t." + EXTDAOTarefa.INICIO_HORA_PROGRAMADA_TIME + " = ?" ;
////				else
////					queryWhereClausule += " AND t." + EXTDAOTarefa.INICIO_HORA_PROGRAMADA_TIME + " = ?" ;
////
////				args.add( this.inicioHoraProgramada );
////			}
//
//
////			if(this.inicioData != null ){
////				if(queryWhereClausule.length() ==  0)
////					queryWhereClausule += " t." + EXTDAOTarefa.INICIO_DATA_DATE + " = ?" ;
////				else
////					queryWhereClausule += " AND t." + EXTDAOTarefa.INICIO_DATA_DATE + " = ?" ;
////
////				args.add( this.inicioData );
////			}
////
////			if(this.inicioHora != null ){
////				if(queryWhereClausule.length() ==  0)
////					queryWhereClausule += " t." + EXTDAOTarefa.INICIO_HORA_TIME + " = ?" ;
////				else
////					queryWhereClausule += " AND t." + EXTDAOTarefa.INICIO_HORA_TIME + " = ?" ;
////
////				args.add( this.inicioHora );
////			}
////
////			if(this.fimData != null ){
////				if(queryWhereClausule.length() ==  0)
////					queryWhereClausule += " t." + EXTDAOTarefa.FIM_DATA_DATE + " = ?" ;
////				else
////					queryWhereClausule += " AND t." + EXTDAOTarefa.FIM_DATA_DATE + " = ?" ;
////
////				args.add( this.fimData );
////			}
////
////
////			if(this.fimHora != null ){
////				if(queryWhereClausule.length() ==  0)
////					queryWhereClausule += " t." + EXTDAOTarefa.FIM_HORA_TIME + " = ?" ;
////				else
////					queryWhereClausule += " AND t." + EXTDAOTarefa.FIM_HORA_TIME + " = ?" ;
////
////				args.add( this.fimHora );
////			}
//
//			if(this.descricao != null ){
//				if(queryWhereClausule.length() ==  0)
//					queryWhereClausule += " t." + Attribute.getNomeAtributoNormalizado(EXTDAOTarefa.DESCRICAO) + " = ?" ;
//				else
//					queryWhereClausule += " AND t." + Attribute.getNomeAtributoNormalizado(EXTDAOTarefa.DESCRICAO) + " LIKE ?" ;
//
//				args.add('%' + HelperString.getWordWithoutAccent(descricao) + '%' );
//			}
//
//			String orderBy = "";
////			if (!pIsAsc) {
////				orderBy += " ORDER BY t." + EXTDAOTarefa.DATA_CADASTRO_DATE
////						+ " DESC";
////			} else {
////				orderBy += " ORDER BY  t." + EXTDAOTarefa.DATA_CADASTRO_DATE
////						+ " ASC";
////			}
//
//			String query = querySelectIdPrestadores + queryFromTables;
//			if(queryWhereClausule.length() > 0 )
//				query 	+= " WHERE " + queryWhereClausule;
//
//			if(orderBy.length() > 0 ){
//				query += orderBy;
//			}
//
//			String[] vetorArg = new String [args.size()];
//			args.toArray(vetorArg);
//
//			Cursor cursor = null;
//			try{
//				cursor = database.rawQuery(query, vetorArg);
//				String[] vetorChave = new String[] { EXTDAOTarefa.ID };
//
//				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
//			}finally{
//				if(cursor != null && ! cursor.isClosed()){
//					cursor.close();
//				}
//			}
//
//		} catch (Exception e) {
//
//			//e.printStackTrace();
//			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
//		}
//		finally
//		{
//			database.close();
//		}
//		return null;
//	}
//
//	@Override
//	public CustomItemList getCustomItemListOfTable(Table pObj) {
//		EXTDAOTarefa vEXTDAOTarefa = (EXTDAOTarefa) pObj;
//		Attribute vAttrId = pObj.getAttribute(EXTDAOTarefa.ID);
//		String vId = null;
//
//		if(vAttrId != null){
//			vId = vAttrId.getStrValue();
//		}
//		if(vId == null){
//			return null;
//		}
//
////		TarefaItemList vItemList = new TarefaItemList(
////				vId,
////				vEXTDAOTarefa.getContainerResponsavel(activity),
////				vEXTDAOTarefa.getEstadoTarefa(),
////				vEXTDAOTarefa.getTipoTarefa(),
////				vEXTDAOTarefa.getIdentificadorTipoTarefa(),
////				vEXTDAOTarefa.getNomeTarefa(),
////				HelperString.getStrSeparateByDelimiterColumn(
////						new String[] {pObj.getStrValueOfAttribute(EXTDAOTarefa.DATA_CADASTRO_DATE),
////						pObj.getStrValueOfAttribute(EXTDAOTarefa.HORA_CADASTRO_TIME)} ,
////						" "),
////				pObj.getStrValueOfAttribute(EXTDAOTarefa.DATA_EXIBIR_DATE),
////				HelperString.getStrSeparateByDelimiterColumn(
////						new String[] {pObj.getStrValueOfAttribute(EXTDAOTarefa.INICIO_DATA_PROGRAMADA_DATE),
////						pObj.getStrValueOfAttribute(EXTDAOTarefa.INICIO_HORA_PROGRAMADA_TIME)} ,
////						" "),
////				HelperString.getStrSeparateByDelimiterColumn(
////						new String[] {pObj.getStrValueOfAttribute(EXTDAOTarefa.INICIO_DATA_DATE),
////						pObj.getStrValueOfAttribute(EXTDAOTarefa.INICIO_HORA_TIME)} ,
////						" "),
////				HelperString.getStrSeparateByDelimiterColumn(
////						new String[] {pObj.getStrValueOfAttribute(EXTDAOTarefa.FIM_DATA_DATE),
////						pObj.getStrValueOfAttribute(EXTDAOTarefa.FIM_HORA_TIME)} ,
////						" "));
//
//
//
////		return vItemList;
//		return null;
//	}
//
//
//
//
//}
