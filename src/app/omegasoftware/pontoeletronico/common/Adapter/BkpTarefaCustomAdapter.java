
package app.omegasoftware.pontoeletronico.common.Adapter;



import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.app.Activity;
import android.view.GestureDetector;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.NameCustomItemListComparator;
import app.omegasoftware.pontoeletronico.common.TarefaCustomItemListComparator;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPermissaoCategoriaPermissao;
import app.omegasoftware.pontoeletronico.listener.DetailButtonClickListener;
import app.omegasoftware.pontoeletronico.listener.GestureOnTouchListener;
import app.omegasoftware.pontoeletronico.listener.SlideSimpleOnGestureListener;

public abstract class BkpTarefaCustomAdapter extends BaseAdapter {

	protected ArrayList<CustomItemList> list = new ArrayList<CustomItemList>();
	protected ArrayList<GestureDetector> listSlideGestureDetector = new ArrayList<GestureDetector>();
	protected Activity activity;
	boolean isAsc;

	String nomeTabela;
	String nomeAtributoItemListOrdenacao;
	boolean isEditPermitido = false;
	boolean isRemovePermitido = false;
	
	TarefaCustomItemListComparator comparator;
	
	
	protected BkpTarefaCustomAdapter(Activity pActivity, boolean pIsAsc, String nomeTabela, String pNomeAtributoItemListOrdenacao)
	{		
		this.activity = pActivity;
		this.isAsc = pIsAsc;
		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoItemListOrdenacao;
		comparator = new TarefaCustomItemListComparator(true, true); 
		initializePermissao();
		this.list = loadData(pIsAsc);
	}
	
	private void initializePermissao(){
		DatabasePontoEletronico db=null;
		try {
			db = new DatabasePontoEletronico(activity);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		EXTDAOPermissaoCategoriaPermissao vObj = new EXTDAOPermissaoCategoriaPermissao(db);
		isEditPermitido = vObj.hasPermissaoToEdit(nomeTabela);
		isRemovePermitido = vObj.hasPermissaoToRemove(nomeTabela);
		db.close();
	}
	
	public boolean isEditPermitido(){
		return isEditPermitido;
	}
	
	public boolean isRemovePermitido(){
		return isRemovePermitido;
	}
	
	protected BkpTarefaCustomAdapter(
			Activity pActivity, 
			boolean pIsAsc, 
			String nomeTabela, 
			boolean loadCustomItemList, 
			String pNomeAtributoItemListOrdenacao)
	{
		this.activity = pActivity;
		this.isAsc = pIsAsc;
		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoItemListOrdenacao;
		comparator = new TarefaCustomItemListComparator(true, true);
		initializePermissao();
		if(loadCustomItemList)
			this.list = loadData(pIsAsc);
		
	}
	
	protected void initalizeListCustomItemList(){
		this.list = loadData(this.isAsc);
	}
	
	protected BkpTarefaCustomAdapter(Activity pActivit, boolean pIsAsc, String nomeTabela, String pNomeAtributoOrdenacao, boolean loadCustomItemList)
	{
		this.activity = pActivit;
		this.isAsc = pIsAsc;
		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoOrdenacao;
		comparator = new TarefaCustomItemListComparator(true, true);
		initializePermissao();
		if(loadCustomItemList)
			this.list = loadData(pIsAsc);
	}

	public String getNomeTabela(){
		return nomeTabela;
	}
	
	public boolean isAsc(){
		return isAsc;
	}

	protected void CreateTextView(int pRLayoutId, String pText, View pView)
	{
		CreateTextView(pRLayoutId, pText, pView, false);
	}

	protected void CreateLinearLayout(int pRLayoutId, View pView, OnClickListener pOnClickListener)
	{
		LinearLayout vLinearLayout = (LinearLayout)pView.findViewById(pRLayoutId);
		vLinearLayout.setClickable(true);
		vLinearLayout.setFocusable(true);
		if(pOnClickListener != null)
			vLinearLayout.setOnClickListener(pOnClickListener);
	}

	protected void setVisibilityHidden(int pRLayoutId, View pView)
	{
		TextView vTextView = (TextView) pView.findViewById(pRLayoutId);
		vTextView.setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
	}
	
	protected TextView CreateTextView(int pRLayoutId, String pText, View pView, Boolean pAppend)
	{
		TextView vTextView = (TextView) pView.findViewById(pRLayoutId);
		if(pText == null || pText.length() == 0){
			setVisibilityHidden(pRLayoutId, pView);
		}else{
			if(pAppend)
				vTextView.setText(new StringBuilder(vTextView.getText() + pText));
			else
				vTextView.setText(pText);
		}
		return vTextView;
	}
	protected void CreateDetailButton(View pView, int pRLayoutId, String pIdCustomItemList, short p_searchMode){
		CreateDetailButton(pView, pRLayoutId, pIdCustomItemList, p_searchMode, null);
	}
	
	protected void CreateDetailButton(View pView, int pRLayoutId, String pIdCustomItemList, short p_searchMode, OnLongClickListener pOnLongClickListener)
	{
		Button vButton = (Button) pView.findViewById(pRLayoutId);
		vButton.setClickable(true);
		vButton.setFocusable(true);	   

		vButton.setOnClickListener(new DetailButtonClickListener(activity,pIdCustomItemList, p_searchMode));
		 
		SlideSimpleOnGestureListener vSlideListener = new SlideSimpleOnGestureListener( pView, pIdCustomItemList, p_searchMode, pOnLongClickListener);
		GestureDetector vGestor = new  GestureDetector(activity, vSlideListener);
		GestureOnTouchListener vListener = new GestureOnTouchListener(vGestor);
		listSlideGestureDetector.add(vGestor);
		pView.setOnTouchListener(vListener);
	}

	protected void CreateDetailButton(String pIdCustomItemList, View pView, short p_searchMode) {
		CreateDetailButton(pView, R.id.custom_button_detail, pIdCustomItemList, p_searchMode);		
	}

	public int getCount() {	
		if(this.list == null)
			return 0;
		else return this.list.size();
	}

	public CustomItemList getItem(int pPosition) {
		return this.list.get(pPosition);
	}

	public ArrayList<CustomItemList> loadData(boolean p_isAsc )
	{

		try {

			ArrayList<Long> vListId = getListIdBySearchParameters(p_isAsc);

			ArrayList<CustomItemList> vRetorno = getListCustomItemList( vListId);
			
			Collections.sort(vRetorno, new NameCustomItemListComparator(p_isAsc, nomeAtributoItemListOrdenacao));

			return vRetorno;

		} catch (Exception e) {
			
			e.printStackTrace();
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}

		return null;
	}


	public ArrayList<CustomItemList> getListCustomItemList(
			ArrayList<Long> pArrayListLong) throws Exception {

		ArrayList<CustomItemList> vArrayFuncionarioItemList = new ArrayList<CustomItemList>();
		DatabasePontoEletronico database = new DatabasePontoEletronico(this.activity);
		Table vObjTable = database.factoryTable(this.nomeTabela);
		try{
			for (Long iLong : pArrayListLong) {

				vObjTable.setAttrValue(
						Table.ID_UNIVERSAL,
						iLong.toString());
				vObjTable.select();
				//				CustomItemList item = pObj.createFuncionarioItemList();
				CustomItemList item = getCustomItemListOfTable(vObjTable);
				if(item != null)
					vArrayFuncionarioItemList.add(item);
				
				vObjTable.clearData();
			}
			return vArrayFuncionarioItemList;	
		}catch (Exception e) {
			
			e.printStackTrace();
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
		finally
		{
			if(database != null){
				database.close();	
			}

		}
		return null;
	}

	public long getItemId(int pPosition) {		
		CustomItemList vCustom = this.list.get(pPosition);
		String id = vCustom.getId();
		try{
			if(id != null)
				return Long.parseLong(id);
			else return -1;
		} catch(Exception ex){
			return -1;
		}

	}

	protected abstract ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc);

	public abstract CustomItemList getCustomItemListOfTable(Table pObj);

	public void SortList(boolean pAscData, boolean pIsAgrupado) {
		comparator.setOrdenacao(pAscData, pIsAgrupado);
		Collections.sort(list, (Comparator<Object>) comparator);
		this.notifyDataSetChanged();
	}

	public  abstract View getView(int position, View convertView, ViewGroup parent) ;

}
