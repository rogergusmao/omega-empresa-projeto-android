package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormTarefaActivityMobile;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.TarefaItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa.ContainerResponsavel;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.listener.GestureOnTouchListener;
import app.omegasoftware.pontoeletronico.listener.TarefaButtonClickListener;
import app.omegasoftware.pontoeletronico.listener.TarefaSlideSimpleOnGestureListener;

public class BkpTarefaMinhaAdapter extends BkpTarefaCustomAdapter {

	private static final int ID_TEXT_VIEW_NOME_TAREFA = R.id.nome_tarefa;
	private static final int ID_TEXT_VIEW_USUARIO_TAREFA = R.id.usuario_tarefa;
	private static final int ID_TEXT_VIEW_ORIGEM_TAREFA = R.id.endereco_origem_tarefa;
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;
	Activity activity;
	
	public EXTDAOTarefa.TIPO_ENDERECO tipoOrigemEndereco;
	public EXTDAOTarefa.TIPO_ENDERECO tipoDestinoEndereco;
	public String identificadorTipoOrigemEndereco;
	public String identificadorTipoDestinoEndereco;
	public String criadoPeloUsuario;
	
	public EXTDAOTarefa.ESTADO_TAREFA vetorEstadoTarefa[];
	
	String titulo;
	
	String origemPessoa;
	String origemEmpresa;
	
	String destinoPessoa;
	String destinoEmpresa;
	
	private String inicioDataAPartirProgramada;
	private String inicioDataAteProgramada;
	
	
	public BkpTarefaMinhaAdapter(
			Activity pActivity,
			EXTDAOTarefa.TIPO_TAREFA pTipoTarefa,
			EXTDAOTarefa.ESTADO_TAREFA pEstadoTarefa,
			String pIdentificadorTipoTarefa,
			EXTDAOTarefa.TIPO_ENDERECO pTipoOrigemEndereco,
			EXTDAOTarefa.TIPO_ENDERECO pTipoDestinoEndereco,
			String pIdentificadorTipoOrigemEndereco,
			String pIdentificadorTipoDestinoEndereco,
			String pCriadoPeloUsuario,	
			String pTitulo,
			String inicioDataAPartirProgramada,
			String inicioDataAteProgramada,
			boolean p_isAsc)
	{
		super(pActivity, p_isAsc, EXTDAOTarefa.NAME, TarefaItemList.DATETIME_CADASTRO_TAREFA, false);
		tipoOrigemEndereco = pTipoOrigemEndereco;
		tipoDestinoEndereco = pTipoDestinoEndereco;
		identificadorTipoOrigemEndereco = pIdentificadorTipoOrigemEndereco;
		identificadorTipoDestinoEndereco = pIdentificadorTipoDestinoEndereco;
		
		criadoPeloUsuario = pCriadoPeloUsuario;
		titulo = pTitulo;
		
		vetorEstadoTarefa = new EXTDAOTarefa.ESTADO_TAREFA[1];
		vetorEstadoTarefa[0] =pEstadoTarefa;
		
		this.inicioDataAPartirProgramada = inicioDataAPartirProgramada;
		this.inicioDataAteProgramada = inicioDataAPartirProgramada;
		 
		 activity = pActivity;
		 
		initalizeListCustomItemList();
	}
	

	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			TarefaItemList vItemList = (TarefaItemList) this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.tarefa_minha_item_list_layout, null);
			
			EXTDAOTarefa.ESTADO_TAREFA vEstado = vItemList.getEstadoTarefa();
			
			switch (vEstado) {

			case AGUARDANDO_INICIALIZACAO:
				if((pPosition % 2) == 0)
					((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.drawable.menu_light_green_button);
				else
					((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.drawable.menu_green_button);
					
				break;
			case EM_EXECUCAO:
				if((pPosition % 2) == 0)
					((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.drawable.menu_light_yellow_button);
				else
					((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.drawable.menu_yellow_button);
					
				break;
			case FINALIZADA:
				if((pPosition % 2) == 0)
					((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.drawable.menu_light_red_button);
				else
					((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.drawable.menu_red_button);
					
				break;
			default:
				break;
			}
			
			

			
			String vDataInicioMarcado = vItemList.getConteudoContainerItem(TarefaItemList.DATETIME_INICIO_MARCADO_DA_TAREFA);
			if(vDataInicioMarcado == null || vDataInicioMarcado.length() == 0 ){
				vDataInicioMarcado = activity.getString(R.string.sem_hora_marcada_para_inicio_da_tarefa);
			}
			
			CreateTextView(
					ID_TEXT_VIEW_NOME_TAREFA, 
					vDataInicioMarcado, 
					vLayoutView);
			
			ContainerResponsavel vContainer = vItemList.getContainerResponsavel();
			String vNomeResponsavel = null;
			if(vContainer != null)
				vNomeResponsavel = vContainer.getNomeResponsavel();
			CreateTextView(
					ID_TEXT_VIEW_USUARIO_TAREFA, 
					vNomeResponsavel, 
					vLayoutView);
			
			
			CreateTextView(
					ID_TEXT_VIEW_ORIGEM_TAREFA, 
					vItemList.getConteudoContainerItem(TarefaItemList.NOME_TAREFA), 
					vLayoutView);

			
			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vOnLongClickListener = new EditCustomOnLongClickListener(
						activity, 
						EXTDAOTarefa.NAME,
						pParent, 
						vItemList.getId(),
						FormEditActivityMobile.class, 
						FormTarefaActivityMobile.class,
						isEditPermitido(),
						isRemovePermitido(), vItemList);
				
				CreateTarefaDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL,
						vItemList.getId(),
						vItemList,
						vOnLongClickListener);
			}
			else {
				CreateTarefaDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL,
						vItemList.getId(),
						vItemList,
						null);
			}
			
			
			
			if(isEditPermitido() || isRemovePermitido()){
			OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
					activity, 
					EXTDAOTarefa.NAME,
					pParent, 
					vItemList.getId(),  
					FormEditActivityMobile.class, 
					FormTarefaActivityMobile.class,
					isEditPermitido(),
					isRemovePermitido(), vItemList);
			LinearLayout vLinearLayout1 = (LinearLayout) vLayoutView.findViewById(R.id.linearLayoutfuncionariodetails1);
			
			vLinearLayout1.setOnLongClickListener(vClickListener);
			}
			return vLayoutView;
		} catch (Exception exc) {
			SingletonLog.factoryToast(activity, exc, SingletonLog.TIPO.ADAPTADOR);
		}
		return null;
	}
	
	
	protected void CreateTarefaDetailButton(
			View pView, 
			int pRLayoutId, 
			String pIdCustomItemList, 
			TarefaItemList pTarefaItemList,
			OnLongClickListener pOnLongClickListener)
	{
		Button vButton = (Button) pView.findViewById(pRLayoutId);
		vButton.setClickable(true);
		vButton.setFocusable(true);	   

		vButton.setOnClickListener(new TarefaButtonClickListener(
				activity,
				pIdCustomItemList, 
				pTarefaItemList));
		OnClickListener vOnClickListener = (OnClickListener) new TarefaButtonClickListener(
				activity,
				pIdCustomItemList, 
				pTarefaItemList);
		TarefaSlideSimpleOnGestureListener vSlideListener = new TarefaSlideSimpleOnGestureListener( 
				pView, 
				pIdCustomItemList, 
				vOnClickListener,
				pOnLongClickListener);
		GestureDetector vGestor = new  GestureDetector(activity, vSlideListener);
		GestureOnTouchListener vListener = new GestureOnTouchListener(vGestor);
		listSlideGestureDetector.add(vGestor);
		pView.setOnTouchListener(vListener); 

	}

	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {

		
		DatabasePontoEletronico database=null;
		try {
			database = new DatabasePontoEletronico(this.activity);
		} catch (OmegaDatabaseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			ArrayList<String> args = new ArrayList<String>();
//			String querySelectIdPrestadores = "SELECT DISTINCT t."
//					+ EXTDAOTarefa.ID + ", t." +  EXTDAOTarefa.INICIO_DATA_DATE + " ";
			String querySelectIdPrestadores = "SELECT DISTINCT t."
					+ EXTDAOTarefa.ID + " ";
			String queryFromTables =  " FROM tarefa t LEFT JOIN veiculo_usuario vu ON " + 
                    " (t.veiculo_usuario_id_INT = vu.id AND vu.usuario_id_INT = ?) " + 
           " OR " + 
           " (t.veiculo_id_INT = vu.veiculo_id_INT AND vu.usuario_id_INT = ? ) " + 
           " LEFT JOIN usuario_categoria_permissao ucp ON ucp.usuario_id_INT = ? AND " +
           " ucp.categoria_permissao_id_INT = t.categoria_permissao_id_INT ";

			args.add(OmegaSecurity.getIdUsuario());
			args.add(OmegaSecurity.getIdUsuario());
			args.add(OmegaSecurity.getIdUsuario());
			
			String queryWhereClausule = " (t.usuario_id_INT IS NULL OR t.usuario_id_INT = ?) "; 
			args.add(OmegaSecurity.getIdUsuario());
			

			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " t." + EXTDAOTarefa.CORPORACAO_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND t." + EXTDAOTarefa.CORPORACAO_ID_INT + " = ? " ;
			
			args.add(OmegaSecurity.getIdCorporacao());
			
			if(this.inicioDataAPartirProgramada  != null ){
//				if(queryWhereClausule.length() ==  0)
//					queryWhereClausule += " t." + EXTDAOTarefa.INICIO_DATA_PROGRAMADA_DATE  + " >= ? " ;
//				else 
//					queryWhereClausule += " AND t." + EXTDAOTarefa.INICIO_DATA_PROGRAMADA_DATE + " >= ? " ;
//
//				args.add( this.inicioDataAPartirProgramada );
			}
			
			if(this.inicioDataAteProgramada  != null ){
//				if(queryWhereClausule.length() ==  0)
//					queryWhereClausule += " t." + EXTDAOTarefa.INICIO_DATA_PROGRAMADA_DATE  + " >= ? " ;
//				else 
//					queryWhereClausule += " AND t." + EXTDAOTarefa.INICIO_DATA_PROGRAMADA_DATE + " >= ? " ;
//
//				args.add( this.inicioDataAteProgramada );
			}
			
			
//			String orderBy = "";
//			if (!pIsAsc) {
//				orderBy += " ORDER BY t." + EXTDAOTarefa.INICIO_DATA_DATE
//						+ " DESC";
//			} else {
//				orderBy += " ORDER BY  t." + EXTDAOTarefa.INICIO_DATA_DATE
//						+ " ASC";
//			}

			String query = querySelectIdPrestadores + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

//			if(orderBy.length() > 0 ){
//				query += orderBy;  
//			} 

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			
			Cursor cursor = null;
			try{
				cursor = database.rawQuery(query, vetorArg);
				String[] vetorChave = new String[] { EXTDAOTarefa.ID };
	
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}

		} catch (Exception e) {

			//SingletonLog.insereErro(e, SingletonLog.TIPO.ADAPTADOR);
			SingletonLog.openDialogError(activity, e, SingletonLog.TIPO.ADAPTADOR);
		}
		finally
		{
			database.close();
		}
		return null;
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {
		EXTDAOTarefa vEXTDAOTarefa = (EXTDAOTarefa) pObj;
		Attribute vAttrId = pObj.getAttribute(EXTDAOTarefa.ID);
		String vId = null;

		if(vAttrId != null){
			vId = vAttrId.getStrValue();
		}
		if(vId == null){
			return null;
		}
		TarefaItemList vItemList =null;
//		TarefaItemList vItemList = new TarefaItemList(
//				vId,
//				vEXTDAOTarefa.getContainerResponsavel(activity),
//				vEXTDAOTarefa.getEstadoTarefa(),
//				vEXTDAOTarefa.getTipoTarefa(),
//				vEXTDAOTarefa.getIdentificadorTipoTarefa(),
//				vEXTDAOTarefa.getNomeTarefa(),
//null,
//				null,
//				null,
//				null,
//				null);

//				HelperString.getStrSeparateByDelimiterColumn(
//						new String[] {pObj.getStrValueOfAttribute(EXTDAOTarefa.CADASTRO_SEC),
//						pObj.getStrValueOfAttribute(EXTDAOTarefa.CADASTRO_TIME)} ,
//						" "),
//				pObj.getStrValueOfAttribute(EXTDAOTarefa.DATA_EXIBIR_DATE),
//				HelperString.getStrSeparateByDelimiterColumn(
//						new String[] {pObj.getStrValueOfAttribute(EXTDAOTarefa.INICIO_DATA_PROGRAMADA_DATE),
//						pObj.getStrValueOfAttribute(EXTDAOTarefa.INICIO_HORA_PROGRAMADA_TIME)} ,
//						" "),
//				HelperString.getStrSeparateByDelimiterColumn(
//						new String[] {pObj.getStrValueOfAttribute(EXTDAOTarefa.INICIO_DATA_DATE),
//						pObj.getStrValueOfAttribute(EXTDAOTarefa.INICIO_HORA_TIME)} ,
//						" "),
//				HelperString.getStrSeparateByDelimiterColumn(
//						new String[] {pObj.getStrValueOfAttribute(EXTDAOTarefa.FIM_DATA_DATE),
//						pObj.getStrValueOfAttribute(EXTDAOTarefa.FIM_HORA_TIME)} ,
//						" "));
		
		

		return vItemList;
		

	}




}
