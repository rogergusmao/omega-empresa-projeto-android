package app.omegasoftware.pontoeletronico.common.Adapter;

import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.TarefaOnlineItemList;

//public class TarefaOnlineAdapter extends DatabaseCustomAdapter{
//
//	private static final int ID_TEXT_VIEW_NOME_TAREFA = R.id.nome_tarefa;
//	private static final int ID_TEXT_VIEW_DATA_CADASTRO_TAREFA = R.id.data_tarefa;
//	private static final int ID_TEXT_VIEW_ORIGEM_TAREFA = R.id.endereco_origem_tarefa;
//
//	Activity activity;
//	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;
//	Integer veiculoUsuario;
//
//	Integer latitudeProprio;
//	Integer longitudeProprio;
//	String strLatitudeProprio;
//	String strLongitudeProprio;
//
//    @Override
//    protected ArrayList<CustomItemList> inicializaItens() {
//        return null;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        return null;
//    }
//
//
//	TYPE_TAREFA typeTarefa = TYPE_TAREFA.ABERTA;
//	public TarefaOnlineAdapter(
//			Activity pActivity,
//			Integer pVeiculoUsuario,
//			boolean p_isAsc,
//			TYPE_TAREFA pTypeTarefa,
//			Integer pLatitudeProprio,
//			Integer pLongitudeProprio)
//	{
//		super(pActivity, p_isAsc,false, TarefaOnlineItemList.NOME_TAREFA);
//		veiculoUsuario = pVeiculoUsuario;
//		activity = pActivity;
//		typeTarefa = pTypeTarefa;
//		if(pLatitudeProprio != null)
//			latitudeProprio = pLatitudeProprio;
//		if(pLongitudeProprio != null) longitudeProprio = pLongitudeProprio;
//
//		if(latitudeProprio != null)
//			strLatitudeProprio = String.valueOf(latitudeProprio);
//		if(longitudeProprio != null)
//			strLongitudeProprio = String.valueOf(longitudeProprio);
//		this.list = loadData(p_isAsc);
//	}
//
//
//
//	public View getView(int pPosition, View pView, ViewGroup pParent) {
//		try {
//			TarefaOnlineItemList vTarefaAtivaItemList = (TarefaOnlineItemList) this.getItem(pPosition);
//			LayoutInflater vLayoutInflater = (LayoutInflater) activity
//					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//			View vLayoutView = vLayoutInflater.inflate(
//					R.layout.tarefa_ativa_item_list_layout, null);
//			if((pPosition % 2) == 0)
//			{
//				((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.color.gray_189);
//			}
//			else
//			{
//				((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.color.white);
//			}
//			DatabasePontoEletronico database = new DatabasePontoEletronico(this.activity);
//			CreateTextView(
//					ID_TEXT_VIEW_NOME_TAREFA,
//					vTarefaAtivaItemList.getConteudoContainerItem(TarefaItemList.NOME_TAREFA),
//					vLayoutView);
//
//			CreateTextView(
//					ID_TEXT_VIEW_DATA_CADASTRO_TAREFA,
//					vTarefaAtivaItemList.getConteudoContainerItem(TarefaItemList.DATETIME_CADASTRO_TAREFA),
//					vLayoutView);
//
//			CreateTextView(
//					ID_TEXT_VIEW_ORIGEM_TAREFA,
//					vTarefaAtivaItemList.getConteudoContainerItem(TarefaOnlineItemList.ORIGEM_ENDERECO),
//					vLayoutView);
//
//			CreateTarefaDetailButton(vLayoutView,
//					ID_CUSTOM_BUTTON_DETAIL,
//					vTarefaAtivaItemList.getId(),
//					vTarefaAtivaItemList);
//			database.close();
//			return vLayoutView;
//		} catch (Exception exc) {
//			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
//				//	"EmpresaAdapter: getView()");
//		}
//		return null;
//	}
//
//
//	protected void CreateTarefaDetailButton(
//			View pView,
//			int pRLayoutId,
//			String pIdCustomItemList,
//			TarefaOnlineItemList pTarefaItemList)
//	{
//		Button vButton = (Button) pView.findViewById(pRLayoutId);
//		vButton.setClickable(true);
//		vButton.setFocusable(true);
//		//		if(DoutorLaserActivity.isTablet){
//		//
//		//			vDetailButtonClickListenerTablet =  new DetailButtonClickListenerTablet(this.activity, pIdCustomItemList, p_searchMode);
//		//			vButton.setOnClickListener(vDetailButtonClickListenerTablet);
//		//			CreateLinearLayout(R.id.linearLayoutfuncionariodetails1, pView, vDetailButtonClickListenerTablet);
//		//		} else {
//		vButton.setOnClickListener(
//				new TarefaOnlineButtonClickListener(
//						activity,
//						pIdCustomItemList,
//						pTarefaItemList,
//						typeTarefa,
//						latitudeProprio,
//						longitudeProprio));
//
//		CreateLinearLayout(
//				R.id.linearLayoutfuncionariodetails1,
//				pView,
//				new TarefaOnlineButtonClickListener(
//						activity,
//						pIdCustomItemList,
//						pTarefaItemList,
//						typeTarefa,
//						latitudeProprio,
//						longitudeProprio
//						));
//	}
//
//
//
//
//	@Override
//	protected ArrayList<Integer> getListIdBySearchParameters(boolean pIsAsc) {
//
//		try {
//			ArrayList<Integer> vArrayListInteger = new ArrayList<Integer>();
//			ArrayList<TarefaCustomOverlayItem> vListTarefa = null;
//			switch (typeTarefa) {
//			case ABERTA:
//				vListTarefa = MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAberta;
//				break;
//			case PROPRIA_ABERTA:
//				vListTarefa = MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAbertaPropria;
//				break;
//			case PROPRIA_EXECUCAO:
//				vListTarefa = MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemPropriaExecucao;
//				break;
//			case PROPRIA_CONCLUIDA:
//				vListTarefa = MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemPropriaConcluida;
//				break;
//
//			default:
//				break;
//			}
//
//			for (TarefaCustomOverlayItem vCustom : vListTarefa) {
//				vArrayListInteger.add((Integer)vCustom.getIdTarefa());
//			}
//
//			return vArrayListInteger;
//		} catch (Exception e) {
//
//			//e.printStackTrace();
//			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
//		}
//		return null;
//	}
//
//	@Override
//	public CustomItemList getCustomItemList(Integer pId) {
//
//
//		TarefaCustomOverlayItem vOverlayItem = null;
//		switch (typeTarefa) {
//		case ABERTA:
//			vOverlayItem = MapaListMarkerActivity.getCustomOverlayItemDaTarefaAberta(pId);
//			break;
//		case PROPRIA_EXECUCAO:
//			vOverlayItem = MapaListMarkerActivity.getCustomOverlayItemDaTarefaPropriaExecucao(pId);
//			break;
//		case PROPRIA_ABERTA:
//			vOverlayItem = MapaListMarkerActivity.getCustomOverlayItemDaTarefaPropria(pId);
//			break;
//		case PROPRIA_CONCLUIDA:
//			vOverlayItem = MapaListMarkerActivity.getCustomOverlayItemDaTarefaPropriaConcluida(pId);
//			break;
//
//		default:
//			break;
//		}
//
//
//		TarefaOnlineItemList vItemList = new TarefaOnlineItemList(
//				String.valueOf(vOverlayItem.getIdTarefa()),
//				this.veiculoUsuario ,
//				"Tarefa " + String.valueOf(vOverlayItem.getIdTarefa()),
//				vOverlayItem.getDatetimeCadastro(),
//				vOverlayItem.getDateAberturaTarefa(),
//				vOverlayItem.getDatetimeInicioMarcadoDaTarefa(),
//				vOverlayItem.getDatetimeInicioTarefa(),
//				vOverlayItem.getDatetimeFimTarefa(),
//				vOverlayItem.getEnderecoOrigem(),
//				vOverlayItem.getOrigemComplemento(),
//				vOverlayItem.getEnderecoDestino(),
//				vOverlayItem.getDestinoComplemento(),
//				strLatitudeProprio,
//				strLongitudeProprio);
//
//		return vItemList;
//	}
//}
