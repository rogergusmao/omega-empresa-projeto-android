package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.common.activity.HelperFonte;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORotina;
import app.omegasoftware.pontoeletronico.date.HelperDate;

public class CalendarioAdapter {

	
	Activity activity;
	ViewMes viewMes;
	
	public CalendarioAdapter(
			Activity activity, 
			int ano, 
			int mes){
		
		this.activity =activity;
		viewMes = new ViewMes(ano, mes);
	}
	public void formatarView(){
		viewMes.formatarView();
	}
	public interface OnFormataViewSemana{
		public abstract void formataView(ViewSemana view);
	}
	public interface OnFormataViewDia{
		public abstract void formataView(ViewDia view);
	}
	public void setOnFormataViewDia(OnFormataViewDia eventoFormataViewDia){
		viewMes.setOnFormataViewDia(eventoFormataViewDia);
		 
	}
	
	public void setOnFormataViewSemana(OnFormataViewSemana evento){
		viewMes.setOnFormataViewSemana(evento);
		 
	}
	
	public class ViewDia{
		RelativeLayout rlDia;
		TextView tvDia;
		TextView tvDescricaoDia;
		OnFormataViewDia eventoFormataViewDia;
		int diaSemana;
		int diaMes;
		int mes;
		int ano;
		int semanaCiclo;
		ViewMes viewMes;
		boolean diaAtual;
		public int getDiaSemana(){
			return diaSemana;
		}
		public int getSemanaCiclo(){
			return semanaCiclo;
		}
		public int getDiaMes(){
			return diaMes;
		}
		public int getMes(){
			return mes;
		}
		public int getAno(){
			return ano;
		}
		public HelperDate getHelperDate(){
			return new HelperDate(ano, mes, diaMes);
		}
		public Date getDate(){
			return new Date(ano, mes, diaMes);
		}
		
		public void setDescricao(String desc ){
			tvDescricaoDia.setText(desc);
		}
		
		public boolean isMesAtual(){

			if(viewMes.getMes()== mes && ano == viewMes.getAno())
				return true;
			else
				return false;
		}
		
		public void formatarView(){
			
			tvDia.setText(String.valueOf(diaMes));
			if(diaAtual){
				if(viewMes.getMes()== mes && ano == viewMes.getAno()){
					rlDia.setBackgroundResource(R.drawable.ui_button_calendario_dia_atual_ativo);
				}
				else{
					rlDia.setBackgroundResource(R.drawable.ui_button_calendario_dia_atual);
				}
			} else {
				if(viewMes.getMes()== mes && ano == viewMes.getAno()){
					rlDia.setBackgroundResource(R.drawable.ui_button_calendario_ativo);
				}
				else{
					rlDia.setBackgroundResource(R.drawable.ui_button_calendario);
				}	
			}
			
			
			if(eventoFormataViewDia != null)
				this.eventoFormataViewDia.formataView(this);
		}
		public void setOnFormataViewDia(OnFormataViewDia eventoFormataViewDia){
			this.eventoFormataViewDia= eventoFormataViewDia; 
		}
		public RelativeLayout getRelativeLayout(){
			return this.rlDia;
		}
		
		public ViewDia(ViewMes viewMes, View viewSemana, int ano, int mes, int diaSemana, int diaMes, int semanaCiclo, boolean diaAtual){
			this.viewMes = viewMes;
			this.ano = ano;
			this.diaSemana = diaSemana;
			this.diaMes = diaMes;
			this.mes= mes;
			this.semanaCiclo = semanaCiclo;
			this.diaAtual = diaAtual;
			
			switch (diaSemana) {
			case Calendar.SUNDAY:
				tvDia = (TextView) viewSemana.findViewById(R.id.tv_domingo);
				rlDia = (RelativeLayout)viewSemana.findViewById(R.id.rl_domingo);
				tvDescricaoDia= (TextView) viewSemana.findViewById(R.id.tv_descricao_domingo);
				break;
			case Calendar.MONDAY:
				tvDia = (TextView) viewSemana.findViewById(R.id.tv_segunda);
				rlDia = (RelativeLayout)viewSemana.findViewById(R.id.rl_segunda);
				tvDescricaoDia= (TextView) viewSemana.findViewById(R.id.tv_descricao_segunda);
				break;
			case Calendar.TUESDAY:
				tvDia = (TextView) viewSemana.findViewById(R.id.tv_terca);
				rlDia = (RelativeLayout)viewSemana.findViewById(R.id.rl_terca);
				tvDescricaoDia= (TextView) viewSemana.findViewById(R.id.tv_descricao_terca);
				break;
			case Calendar.WEDNESDAY:
				tvDia = (TextView) viewSemana.findViewById(R.id.tv_quarta);
				rlDia = (RelativeLayout)viewSemana.findViewById(R.id.rl_quarta);
				tvDescricaoDia= (TextView) viewSemana.findViewById(R.id.tv_descricao_quarta);
				break;
			case Calendar.THURSDAY:
				tvDia = (TextView) viewSemana.findViewById(R.id.tv_quinta);
				rlDia = (RelativeLayout)viewSemana.findViewById(R.id.rl_quinta);
				tvDescricaoDia= (TextView) viewSemana.findViewById(R.id.tv_descricao_quinta);
				break;
			case Calendar.FRIDAY:
				tvDia = (TextView) viewSemana.findViewById(R.id.tv_sexta);
				rlDia = (RelativeLayout)viewSemana.findViewById(R.id.rl_sexta);
				tvDescricaoDia= (TextView) viewSemana.findViewById(R.id.tv_descricao_sexta);
				break;
			case Calendar.SATURDAY:
				tvDia = (TextView) viewSemana.findViewById(R.id.tv_sabado);
				rlDia = (RelativeLayout)viewSemana.findViewById(R.id.rl_sabado);
				tvDescricaoDia= (TextView) viewSemana.findViewById(R.id.tv_descricao_sabado);
				break;
			default:
				break;
			}
			
			
		}
	}
	

	public class ViewMes{
		
		LinearLayout llMes;
		
		
		ViewSemana viewsSemana[] = new ViewSemana[5];
		TextView tvsSemana[] = new TextView[5];
		
		int primeiroDiaSemana = -1;
		int ultimoDiaMes = -1; 
		int ano = -1;
		int mes = -1;
		
		HelperDate helperDate = null;
		Calendar helperCalendar = null;
		
		public ViewMes(
				int ano, 
				int mes){
			this.ano = ano;
			this.mes = mes;
			
			
			this.llMes = (LinearLayout) activity.findViewById(R.id.ll_mes);
			
			GregorianCalendar c = new GregorianCalendar(ano, mes, 1);
		
				
			primeiroDiaSemana = c.get(Calendar.DAY_OF_WEEK);
			ultimoDiaMes= c.getMaximum(Calendar.DAY_OF_MONTH);
			
			
			int iMes = 0;
			int iDiaMes = 0;
			int iAnos= 0;
			int iViewsSemana = 0;
			//retorna a data atu o dia 1
			if(primeiroDiaSemana != Calendar.SUNDAY){
				while(c.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){					
					c.add(Calendar.DAY_OF_MONTH, -1);
				}
			}
			GregorianCalendar dataAtual = new GregorianCalendar();
			while(iViewsSemana < viewsSemana.length){
				iAnos = 0;
				iMes = 0;
				iDiaMes = 0;
				int diasMes[] = new int[7];
				int meses[] = new int[7];
				int anos[] = new int[7];
				for(int i = Calendar.SUNDAY; 
						i <= Calendar.SATURDAY; 
						i++){
					anos[iAnos++] = c.get(Calendar.YEAR);
					diasMes[iDiaMes++] = c.get(Calendar.DAY_OF_MONTH);
					meses[iMes++] = c.get(Calendar.MONTH);
					c.add(Calendar.DAY_OF_MONTH, 1);
					
				}
				int idViewSemana = getIdViewSemana(iViewsSemana);
				View ultimaViewSemana = activity.findViewById(idViewSemana);
				int idTextViewSemana = getIdTextViewSemana(iViewsSemana);
				TextView tvSemana = (TextView)activity.findViewById(idTextViewSemana);
				
				LinearLayout llTituloSemana = (LinearLayout)activity.findViewById(getViewTituloSemana(iViewsSemana));
				viewsSemana[iViewsSemana++] = new ViewSemana(
						this,
						ultimaViewSemana,
						tvSemana,
						llTituloSemana,
						anos,
						meses,
						diasMes,
						dataAtual);
			}
			
		}
		public int getMes(){
			return mes;
		}
		public int getAno(){
			return ano;
		}
		
		
		
		
		public void formatarView(){
			for(int i = 0 ; i < viewsSemana.length; i++)
				viewsSemana[i].formatarView();
			
//			GregorianCalendar cal = new GregorianCalendar(ano, mes, 1); 
//			String strMes = new SimpleDateFormat("MMMM").format(cal.getTime());
//			btSelecionarMes.setText(strMes);
			HelperFonte.setFontAllView(llMes);

		}
		public int getIdTextViewSemana(int iSemana){
			switch (iSemana) {
			case 0:
				return R.id.tv_semana_1;
			case 1:
				return R.id.tv_semana_2;
			case 2:
				return R.id.tv_semana_3;
			case 3:
				return R.id.tv_semana_4;
			case 4:
				return R.id.tv_semana_5;
			default:
				return -1;
			}
		}
		
		public int getViewTituloSemana(int iSemana){
			switch (iSemana) {
			case 0:
				return R.id.ll_titulo_semana_1;
			case 1:
				return R.id.ll_titulo_semana_2;
			case 2:
				return R.id.ll_titulo_semana_3;
			case 3:
				return R.id.ll_titulo_semana_4;
			case 4:
				return R.id.ll_titulo_semana_5;
			default:
				return -1;
			}
		}
		
		public int getIdViewSemana(int iSemana){
			switch (iSemana) {
			case 0:
				return R.id.in_semana1;
			case 1:
				return R.id.in_semana2;
			case 2:
				return R.id.in_semana3;
			case 3:
				return R.id.in_semana4;
			case 4:
				return R.id.in_semana5;
			default:
				return -1;
			}
		}
		
		public void setOnFormataViewDia(OnFormataViewDia eventoFormataViewDia){
			if(viewsSemana != null){
				for(int i = 0 ; i < viewsSemana.length; i++){
					if(viewsSemana[i] != null)
						viewsSemana[i].setOnFormataViewDia(eventoFormataViewDia);
				}
			}
		}
		

		public void setOnFormataViewSemana(OnFormataViewSemana eventoFormataView){
			if(viewsSemana != null){
				for(int i = 0 ; i < viewsSemana.length; i++){
					if(viewsSemana[i] != null)
						viewsSemana[i].setOnFormataViewSemana(eventoFormataView);
				}
			}
		}
	}
	
	public class ViewSemana{
		OnFormataViewSemana eventoFormataViewSemana= null;
		View viewSemana;
		int anos[];
		int meses[];
		int diasMes[];
		
		TextView tvSemana;
		LinearLayout llTituloSemana;
		
		public GregorianCalendar getDataDaPrimeiraViewDia(){
			GregorianCalendar gc = new GregorianCalendar(anos[0], meses[0], diasMes[0]);
			return gc;
		}
		public int getSemanaCiclo(){
			GregorianCalendar data = getDataDaPrimeiraViewDia();
			int semana = EXTDAORotina.parseSemana(data);
			return semana;
		}
		public void setTituloSemana(String conteudo){
			tvSemana.setText(conteudo);
		}
		
		public LinearLayout getLinearLayoutTitulo(){
			return llTituloSemana;
		}
		
		
		ViewDia[] viewsDia = new ViewDia[7] ;
		protected ViewSemana(
				ViewMes viewMes, 
				View viewSemana,
				TextView tvSemana,
				LinearLayout llTituloSemana,
				int anos[], int meses[] , int diasMes[],
				GregorianCalendar dataAtual) {
			this.viewSemana = viewSemana;
			this.tvSemana = tvSemana;
			this.llTituloSemana = llTituloSemana;
			this.anos = anos;
			this.meses = meses;
			this.diasMes = diasMes;
			int semanaCiclo = getSemanaCiclo();
			for(int i = Calendar.SUNDAY ; i <= Calendar.SATURDAY; i++){
				viewsDia[i - 1] = new ViewDia(
						viewMes, 
						viewSemana, 
						anos[i - 1], 
						meses[i - 1], 
						i, 
						diasMes[i - 1], 
						semanaCiclo,
						dataAtual.get(Calendar.YEAR) == anos[i - 1] 
								&& dataAtual.get(Calendar.MONTH) == meses[i - 1]
								&& dataAtual.get(Calendar.DAY_OF_MONTH) == diasMes[i - 1]);
			}
		}
		
		public void setOnFormataViewSemana(OnFormataViewSemana evento){
			this.eventoFormataViewSemana = evento;
		}
		public void setOnFormataViewDia(OnFormataViewDia eventoFormataViewDia){
			if(viewsDia != null){
				for(int i = 0 ; i < viewsDia.length; i++){
					if(viewsDia[i] != null)
						viewsDia[i].setOnFormataViewDia(eventoFormataViewDia);
				}
			}
			if(eventoFormataViewSemana != null)
				eventoFormataViewSemana.formataView(this);
		}
		
		public void formatarView(){
			for(int i = 0 ; i < viewsDia.length; i++)
				viewsDia[i].formatarView();
		}
		
		
		
	}
}
