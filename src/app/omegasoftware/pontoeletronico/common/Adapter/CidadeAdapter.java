package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormCidadeActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.ItemList.CidadeItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class CidadeAdapter extends DatabaseCustomAdapter{

//	private static final int ID_TEXT_VIEW_CIDADE = R.id.cidade_textview;
//	private static final int ID_TEXT_VIEW_ESTADO = R.id.estado_textview;
//	private static final int ID_TEXT_VIEW_PAIS = R.id.pais_textview;
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;
	
	public String ufId;
	public String paisId;
	public String nomeCidade;
	

	public CidadeAdapter(
			Activity pActivity,
			String pNomeCidade,
			String pEstadoId,
			String pPaisId,
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, p_isAsc, EXTDAOCidade.NAME, CidadeItemList.CIDADE, false);
		ufId = pEstadoId;
		paisId = pPaisId;
		nomeCidade =pNomeCidade;
		super.initalizeListCustomItemList();
	}


	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			CidadeItemList vItemList = (CidadeItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.item_list_layout, null);
			
			PersonalizaItemView(vLayoutView, pPosition);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_1, 
					vItemList.getConteudoContainerItem(CidadeItemList.CIDADE), 
					vLayoutView);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_2, 
					vItemList.getConteudoContainerItem(CidadeItemList.ESTADO), 
					vLayoutView);
			
			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_3, 
					vItemList.getConteudoContainerItem(CidadeItemList.PAIS), 
					vLayoutView);

			CreateDetailButton(vLayoutView, 
					ID_CUSTOM_BUTTON_DETAIL, 
					vItemList.getId(), 
					OmegaConfiguration.SEARCH_MODE_CIDADE);
			
			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
						activity, 
						EXTDAOCidade.NAME,
						pParent, 
						vItemList.getId(),
						FormEditActivityMobile.class, 
						FormCidadeActivityMobile.class,
						isEditPermitido(),
						isRemovePermitido(), vItemList);
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_CIDADE, 
						vClickListener);
			}
			else {
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_BAIRRO);
			}			return vLayoutView;
		} catch (Exception exc) {
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
					"EmpresaAdapter: getView()");
		}
		return null;
	}



	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {

		
		DatabasePontoEletronico database = null;

		try {
			database = new DatabasePontoEletronico(this.activity);
			String querySelectIdPrestadores = "SELECT DISTINCT c."
					+ EXTDAOCidade.ID + " ";

			String queryFromTables =  " FROM " + EXTDAOCidade.NAME + " c ";

			String queryWhereClausule = ""; 
			ArrayList<String> args = new ArrayList<String>();

			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " c." + EXTDAOCidade.CORPORACAO_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND c." + EXTDAOCidade.CORPORACAO_ID_INT + " = ? " ;
			
			args.add(OmegaSecurity.getIdCorporacao());
			
			//Se o bairro tiver sido setado entao nao ha a necessidade de setar a cidade
			if( this.nomeCidade != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " c." + Attribute.getNomeAtributoNormalizado(EXTDAOCidade.NOME) + " LIKE ? " ;
				else 
					queryWhereClausule += " AND c." + Attribute.getNomeAtributoNormalizado(EXTDAOCidade.NOME) + " LIKE ? " ;

				args.add("%" + HelperString.getWordWithoutAccent(this.nomeCidade) + "%");
			}

			if( this.ufId != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " c." + EXTDAOCidade.UF_ID_INT + " = ? " ;
				else 
					queryWhereClausule += " AND c." + EXTDAOCidade.UF_ID_INT + " = ? " ;

				args.add(this.ufId);
			}
			
			if( this.paisId != null && this.ufId == null ){
				queryFromTables += ", "+ EXTDAOUf.NAME + " uf ";
				
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " uf."+ EXTDAOUf.ID + " = c." + EXTDAOCidade.UF_ID_INT +" AND " + 
							" uf." + EXTDAOUf.PAIS_ID_INT + " = ? " ;
				else 
					queryWhereClausule += " AND uf."+ EXTDAOUf.ID + " = c." + EXTDAOCidade.UF_ID_INT +" AND " + 
							" uf." + EXTDAOUf.PAIS_ID_INT + " = ? " ;

				args.add(this.paisId);
			}

			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY c." + Attribute.getNomeAtributoNormalizado(EXTDAOCidade.NOME)
						+ " DESC";
			} else {
				orderBy += " ORDER BY  c." + Attribute.getNomeAtributoNormalizado(EXTDAOCidade.NOME)
						+ " ASC";
			}

			String query = querySelectIdPrestadores + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

			if(orderBy.length() > 0 ){
				query += orderBy;  
			} 

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			
			Cursor cursor = null;
			try{
				cursor = database.rawQuery(query, vetorArg);
				// Cursor cursor = oh.query(true, EXTDAOEmpresa.NAME,
				// new String[]{EXTDAOEmpresa.ID_PRESTADOR}, "", new String[]{},
				// null, "", "", "");
				String[] vetorChave = new String[] { EXTDAOCidade.ID };
	
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}
		} catch (Exception e) {
			SingletonLog.openDialogError(activity, e, SingletonLog.TIPO.ADAPTADOR);
		}
		finally
		{
			if(database != null )database.close();
		}
		return null;
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {
try{
		EXTDAOCidade vEXTDAOCidade = (EXTDAOCidade) pObj;

		String vIdCidade = pObj.getStrValueOfAttribute(EXTDAOCidade.ID);
		String vNomeEstado = null;
		String vNomeCidade = vEXTDAOCidade.getStrValueOfAttribute(EXTDAOCidade.NOME);
		String vNomePais = null;
		if(vIdCidade != null){
			String vIdEstado = vEXTDAOCidade.getStrValueOfAttribute(EXTDAOCidade.UF_ID_INT);
			if(vIdEstado != null){
				EXTDAOUf vObjUf = new EXTDAOUf(pObj.getDatabase());
				vObjUf.setAttrValue(EXTDAOUf.ID, vIdEstado);
				vObjUf.select();
				vNomeEstado = vObjUf.getStrValueOfAttribute(EXTDAOUf.NOME);
				vNomePais= vObjUf.getNomeDaChaveExtrangeira(EXTDAOUf.PAIS_ID_INT);
			}
		}
		CidadeItemList vItemList = new CidadeItemList(  
				vEXTDAOCidade.getAttribute(EXTDAOCidade.ID).getStrValue(),
				vNomePais,
				vNomeEstado,
				vNomeCidade);

		return vItemList;
}catch(Exception ex){
	SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
return null;
}
	}
}

