package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.CorporacaoItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCorporacao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioCorporacao;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class CorporacaoAdapter extends DatabaseCustomAdapter{
	
	private static final int ID_TEXT_VIEW_CORPORACAO = R.id.corporacao_textview;
	
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;


	public String nomeCorporacao;
	boolean isMinhaCorporacao;
	
	public CorporacaoAdapter(
			Activity pActivity,
			String nomeCorporacao, 
			boolean pIsMinhaCorporacao,
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, p_isAsc, EXTDAOCorporacao.NAME, CorporacaoItemList.CORPORACAO, false);
		this.nomeCorporacao = nomeCorporacao; 
		
		super.initalizeListCustomItemList();
	}

	
	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			CorporacaoItemList vCorporacaoItemList = (CorporacaoItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.corporacao_item_list_layout, null);
			
			PersonalizaItemView(vLayoutView, pPosition);

			CreateTextView(
					ID_TEXT_VIEW_CORPORACAO, 
					vCorporacaoItemList.getConteudoContainerItem(CorporacaoItemList.CORPORACAO), 
					vLayoutView);


			CreateDetailButton(vLayoutView, 
					ID_CUSTOM_BUTTON_DETAIL, 
					vCorporacaoItemList.getId(), 
					OmegaConfiguration.SEARCH_MODE_CORPORACAO);
			
			return vLayoutView;
		} catch (Exception exc) {
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
					"CorporacaoAdapter: getView()");
		}
		return null;
	}



	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {

		
		DatabasePontoEletronico database=null;
		try {
			database = new DatabasePontoEletronico(this.activity);
		} catch (OmegaDatabaseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			
			String querySelect = "SELECT DISTINCT corporacao."
					+ EXTDAOCorporacao.ID + " ";

			String queryFromTables =  " FROM " + EXTDAOCorporacao.NAME + " corporacao ";

			String queryWhereClausule = ""; 
			ArrayList<String> args = new ArrayList<String>();


			if(this.nomeCorporacao != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " corporacao." + Attribute.getNomeAtributoNormalizado(EXTDAOCorporacao.NOME) + " LIKE ?" ;
				else 
					queryWhereClausule += " AND corporacao." + Attribute.getNomeAtributoNormalizado(EXTDAOCorporacao.NOME) + " LIKE ?" ;

				args.add('%' + HelperString.getWordWithoutAccent(this.nomeCorporacao) + '%' );
			}
			
			if(isMinhaCorporacao){
				queryFromTables += ", " + EXTDAOUsuarioCorporacao.NAME + " uc ";
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " corporacao." + EXTDAOCorporacao.ID + " = uc." + EXTDAOUsuarioCorporacao.CORPORACAO_ID_INT + " AND " +
											" uc." + EXTDAOUsuarioCorporacao.USUARIO_ID_INT + " = ? ";
				else 
					queryWhereClausule += " AND corporacao." + EXTDAOCorporacao.ID + " = uc." + EXTDAOUsuarioCorporacao.CORPORACAO_ID_INT + " AND " +
							" uc." + EXTDAOUsuarioCorporacao.USUARIO_ID_INT + " = ? ";
				args.add(OmegaSecurity.getIdUsuario());
			}

			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY corporacao." + Attribute.getNomeAtributoNormalizado(EXTDAOCorporacao.NOME)
						+ " DESC";
			} else {
				orderBy += " ORDER BY  corporacao." + Attribute.getNomeAtributoNormalizado(EXTDAOCorporacao.NOME)
						+ " ASC";
			}

			String query = querySelect + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

			if(orderBy.length() > 0 ){
				query += orderBy;  
			} 

			String[] vetorArgs = new String [args.size()];
			args.toArray(vetorArgs);
			
			Cursor cursor = null;
			try{
				cursor = database.rawQuery(query, vetorArgs);
				String[] vetorChave = new String[] { EXTDAOCorporacao.ID };
	
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}

		} catch (Exception e) {
			
			e.printStackTrace();
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
		finally
		{
			database.close();
		}
		return null;
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {

		EXTDAOCorporacao vEXTDAOCorporacao = (EXTDAOCorporacao) pObj;
		
	
		CorporacaoItemList vItemList = new CorporacaoItemList(  
				vEXTDAOCorporacao.getAttribute(EXTDAOCorporacao.ID).getStrValue(),
				vEXTDAOCorporacao.getAttribute(EXTDAOCorporacao.NOME).getStrValue(), 
				isMinhaCorporacao);

		return vItemList;
	}
}

