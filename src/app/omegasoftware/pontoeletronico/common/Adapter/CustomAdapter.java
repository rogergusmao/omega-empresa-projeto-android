
package app.omegasoftware.pontoeletronico.common.Adapter;



import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCustomAdapterListActivity;


public abstract class CustomAdapter extends BaseAdapter  implements Filterable {
	protected ArrayList<CustomItemList> listaCompleta = new ArrayList<CustomItemList>();
	protected ArrayList<CustomItemList> listaFiltrada = new ArrayList<CustomItemList>();
	
	Activity activity;
//	protected ArrayList<GestureDetector> listSlideGestureDetector = new ArrayList<GestureDetector>();
	OmegaCustomAdapterListActivity.HandlerFiltroRapido handlerFiltro;
	OmegaCustomAdapterListActivity.HandlerDiscador handlerDiscador;
	Class<?> filtroClass;
	protected String ultimaBuscaMinusculo= "";
	protected String ultimaBuscaMaiusculo= "";
	
	public void setFiltroClass(Class<?> classe){
		filtroClass = classe;
	}

	public void setHandler(
			OmegaCustomAdapterListActivity.HandlerFiltroRapido handlerFiltro,
			OmegaCustomAdapterListActivity.HandlerDiscador handlerDiscador){
		this.handlerDiscador = handlerDiscador;
		this.handlerFiltro = handlerFiltro;
	}
	

	
	public Activity getActivity(){
		return activity;
	}
	
	protected CustomAdapter(Activity a)
	{
		this.activity = a;
		
	}

	public Class<?> getClasseFiltro(){
		return filtroClass;
	}
	public long getSize(){
		if(listaCompleta == null) return 0;
		return listaCompleta.size();
	}

	public String insereTagHtml(String token, String search){
		int indice = token.indexOf(search);
		if(indice >= 0 ){
			String formatado = token.substring(0, indice) 
					+ "<b>" 
					+ token.substring(indice, search.length()) 
					+ "</b>"
					+ token.substring(indice + search.length(), token.length() - (indice + ultimaBuscaMinusculo.length()));
			return formatado;
		} else return null;
	}
	
	public String formatarTextoDoFiltro(String token){
		String formatado = insereTagHtml(token, ultimaBuscaMinusculo);
		if(formatado == null)
			formatado = insereTagHtml(token, ultimaBuscaMaiusculo);
		
		return formatado;
	}

	public Context getContext(){
		return activity;
	}


	public int getCount() {		
		if(this.listaFiltrada == null)
			return 0;
		else return this.listaFiltrada.size();
//		if(this.list == null) return 0;
//		else return this.list.size();
	}

	public CustomItemList getItem(int pPosition) {
//		return this.list.get(pPosition);
		return this.listaFiltrada.get(pPosition);
	}


	public ArrayList<CustomItemList> loadData(boolean isAsc )
	{
		try {
			this.listaCompleta = inicializaItens();
			this.listaFiltrada =this.listaCompleta; 
			return listaCompleta;
		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.ADAPTADOR);
		}
		return null;
	}


//	public long getItemId(int pPosition) {		
//		CustomItemList vCustom = this.list.get(pPosition);
//		String id = vCustom.getId();
//		try{
//			if(id != null)
//				return Long.parseLong(id);
//			else return -1;
//		} catch(Exception ex){
//			return -1;
//		}
//
//	}

	public long getItemId(int pPosition) {		
		CustomItemList vCustom = this.listaFiltrada.get(pPosition);
		String id = vCustom.getId();
		try{
			if(id != null)
				return Long.parseLong(id);
			else return -1;
		} catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.ADAPTADOR);
			return -1;
		}

	}
	@Override
    public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
            	listaFiltrada = (ArrayList<CustomItemList>) results.values;
        		notifyDataSetChanged();	
    
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
            	FilterResults results = new FilterResults();
            	//Se nenhum dos dois(discador e filtro rupido) filtros estiver ativo
            	if((handlerFiltro == null || (handlerFiltro != null && ! handlerFiltro.ativo))
            		&& (handlerDiscador == null || (handlerDiscador != null && ! handlerDiscador.ativo))	){
            		results.count = listaCompleta.size();
                    results.values = listaCompleta;	
                    return results; 
            	}
            	constraint = constraint.toString().trim();
            	ultimaBuscaMaiusculo = constraint.toString().toLowerCase();
            	ultimaBuscaMinusculo = constraint.toString().toUpperCase();
            	
            	
            	if(constraint == null || constraint.length() == 0){
            		results.count = listaCompleta.size();
                    results.values = listaCompleta;
                    return results;
            	}
            	
                ArrayList<CustomItemList> listaFiltrada = filtraToken(constraint.toString());
                
                
                
                if(listaFiltrada == null ){
                	results.count = 0;
                    results.values = null;	
                } else {
                	results.count = listaFiltrada.size();
                    results.values = listaFiltrada;	
                }
                
                return results;
            }
        };

        return filter;
    }

	//Funcao a ser sobreescrita para o filtro rupido
	public ArrayList<CustomItemList> filtraToken(CharSequence cs){
		return null;
	}
	
	protected abstract ArrayList<CustomItemList> inicializaItens();
	
	public  abstract View getView(int position, View convertView, ViewGroup parent) ;

}

