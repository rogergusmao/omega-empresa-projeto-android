package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.activity.HelperFonte;

public class CustomSpinnerAdapter extends ArrayAdapter<String> {

//	Typeface customTypeFace;
	Context context;
	ArrayList<String> values;
	ArrayList<String> ids;
	int layout;
	int dropDownLayout;
	int count;
	boolean isToUpperCase = true;
	public CustomSpinnerAdapter(
			Context context,
			LinkedHashMap<String, String> data,
			int layoutResource,
			int firstEntryResource,
			boolean pIsToUpperCase)
	{
		this(context, data, layoutResource, firstEntryResource);
		isToUpperCase = pIsToUpperCase;
	}
	public CustomSpinnerAdapter(
			Context context,
			LinkedHashMap<String, String> data,
			int layoutResource,
			int firstEntryResource)
	{
		super(context,layoutResource);
		
		//Salvando o context para utilizar no mutodo getView()
		this.context = context;
		
		//Criando o customTypeFace (define o tipo de letra)
//		this.customTypeFace = Typeface.createFromAsset(this.context.getAssets(),"trebucbd.ttf");
				
		//Salvando o layoutResource para utilizar no mutodo getView()
		this.layout = layoutResource;
		this.dropDownLayout = layoutResource;
		
		this.values = new ArrayList<String>();
		this.ids = new ArrayList<String>();
		
		this.values.add(this.context.getResources().getString(firstEntryResource));
		this.ids.add(OmegaConfiguration.UNEXISTENT_ID_IN_DB);
		
		//As opuues suo definidas no parumetro dataList
		for(String key : data.keySet())
		{
			if(key == null) continue;
			this.ids.add((String) key);
			if(isToUpperCase)
				this.values.add(data.get(((String) key).toUpperCase()));
			else
				this.values.add(data.get(((String) key).toLowerCase()));
		}

	}
	
	
	public CustomSpinnerAdapter(Context context,
			LinkedHashMap<String, String> data,int layoutResource)
	{
		super(context,layoutResource);
		
		//Salvando o context para utilizar no mutodo getView()
		this.context = context;
		
		//Criando o customTypeFace (define o tipo de letra)
//		this.customTypeFace = Typeface.createFromAsset(this.context.getAssets(),"trebucbd.ttf");
				
		//Salvando o layoutResource para utilizar no mutodo getView()
		this.layout = layoutResource;
		this.dropDownLayout = layoutResource;
		
		this.values = new ArrayList<String>();
		this.ids = new ArrayList<String>();		

		//As opuues suo definidas no parumetro dataList
		for(String key : data.keySet())
		{
			this.ids.add((String) key);
			if(isToUpperCase)
				this.values.add(data.get((String) key).toUpperCase());
			else
				this.values.add(data.get(((String) key).toLowerCase())); 
				
		}

	}
	
	public CustomSpinnerAdapter(Context context,int dataResource,int layoutResource) {
		super(context,layoutResource);
				
		//Salvando o context para utilizar no mutodo getView()
		this.context = context;

		//Criando o customTypeFace (define o tipo de letra)
//		this.customTypeFace = Typeface.createFromAsset(this.context.getAssets(),"trebucbd.ttf");
		
		//Salvando o layoutResource para utilizar no mutodo getView()
		this.layout = layoutResource;
		this.dropDownLayout = layoutResource;
		
		this.values = new ArrayList<String>();
		this.ids = new ArrayList<String>();
		
		//As opuues suo definidas no arquivo de resource strings.xml
		
		for(int i=0;i<context.getResources().getStringArray(dataResource).length;i++)
		{
			if(isToUpperCase)
				this.values.add(context.getResources().getStringArray(dataResource)[i].toUpperCase());
			else
				this.values.add(context.getResources().getStringArray(dataResource)[i].toLowerCase());
				
			this.ids.add(String.valueOf(i));
		}
		
	}
	
	public CustomSpinnerAdapter(Context context,int dataResource,int layoutResource,int firstEntryResource) {
		super(context,layoutResource);
				
		//Salvando o context para utilizar no mutodo getView()
		this.context = context;

		//Criando o customTypeFace (define o tipo de letra)
//		this.customTypeFace = Typeface.createFromAsset(this.context.getAssets(),"trebucbd.ttf");
		
		//Salvando o layoutResource para utilizar no mutodo getView()
		this.layout = layoutResource;
		this.dropDownLayout = layoutResource;
		
		this.values = new ArrayList<String>();
		this.ids = new ArrayList<String>();
		
		this.values.add(this.context.getResources().getString(firstEntryResource));
		this.ids.add(OmegaConfiguration.UNEXISTENT_ID_IN_DB);
		
		//As opuues suo definidas no arquivo de resource strings.xml
		
		for(int i=0;i<context.getResources().getStringArray(dataResource).length;i++)
		{
			if(isToUpperCase)
			this.values.add(context.getResources().getStringArray(dataResource)[i].toUpperCase());
			else
				this.values.add(context.getResources().getStringArray(dataResource)[i].toLowerCase());
			this.ids.add(String.valueOf(i));
		}		
	}
	
	public int getCount() {
		return this.values.size();
	}

	public String getItem(int position) {	
		return this.values.get(position);
	}

	public long getItemId(int position) {
		return Long.valueOf(this.ids.get(position));
	}

	public int getPositionFromId(String id)
	{
		//Search for id in the idList, returns the position
		for(int i=0;i<this.ids.size();i++)
		{
			if(this.ids.get(i).equals(id))
			{
				return i;
			}
		}
		
		//Could not find, return first position
		return 0;
	}
	
	public View getView(int position, View convertView, ViewGroup parent)
	{
		
		TextView opcao = (TextView)LayoutInflater.from(this.context).inflate(this.dropDownLayout, parent,false);
		opcao.setText(this.values.get(position));
		
//		opcao.setTypeface(this.customTypeFace);
		HelperFonte.setFontView(opcao);
		return opcao;
	}
	
	public View getDropDownView(int position, View convertView, ViewGroup parent)
	{
		
		TextView opcao = (TextView)LayoutInflater.from(this.context).inflate(this.layout, parent,false);
		opcao.setText(this.values.get(position));
//		opcao.setTypeface(this.customTypeFace);
		
//		ViewGroup vg = (ViewGroup) findViewById(R.id.view_pai);
		HelperFonte.setFontView(opcao);
		return opcao;
	}
	
	public void setDropDownLayout(int dropDownLayout)
	{
		this.dropDownLayout = dropDownLayout;
	}

}
