
package app.omegasoftware.pontoeletronico.common.Adapter;



import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.app.Activity;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.CustomItemListComparator;
import app.omegasoftware.pontoeletronico.common.NameCustomItemListComparator;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.activity.HelperFonte;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity.HandlerDiscador;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity.HandlerFiltroRapido;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPermissaoCategoriaPermissao;
import app.omegasoftware.pontoeletronico.listener.DetailButtonClickListener;

public abstract class DatabaseCustomAdapter extends BaseAdapter implements Filterable {
	protected static final int ID_TEXT_VIEW_DESCRICAO_1 = R.id.tv_descricao_1;
	protected static final int ID_TEXT_VIEW_DESCRICAO_2 = R.id.tv_descricao_2;
	protected static final int ID_TEXT_VIEW_DESCRICAO_3 = R.id.tv_descricao_3;
	protected static final int ID_TEXT_VIEW_DESCRICAO_4 = R.id.tv_descricao_4;
	protected static final int ID_TEXT_VIEW_DESCRICAO_5 = R.id.tv_descricao_5;
	protected static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;
	
	protected ArrayList<CustomItemList> listaFiltrada = new ArrayList<CustomItemList>();
	protected ArrayList<CustomItemList> listaCompleta = new ArrayList<CustomItemList>();

	protected Activity activity;
	boolean isAsc;
	
	String nomeTabela;
	String nomeAtributoItemListOrdenacao;
	boolean isEditPermitido = false;
	boolean isRemovePermitido = false;
	Class<?> formularioClass;
	Class<?> filtroClass;
	CustomItemListComparator comparator;
	HandlerFiltroRapido handlerFiltro;
	HandlerDiscador handlerDiscador;
	protected String ultimaBuscaMinusculo= "";
	protected String ultimaBuscaMaiusculo= "";
	
	protected DatabaseCustomAdapter(
			Activity pActivity, 
			boolean pIsAsc, 
			String nomeTabela, 
			String pNomeAtributoItemListOrdenacao, 
			CustomItemListComparator pComparator) throws OmegaDatabaseException
	{		
		this.activity = pActivity;
		this.isAsc = pIsAsc;
		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoItemListOrdenacao;
		comparator = pComparator;
		initializePermissao();
		initalizeListCustomItemList();
		
	}
	
	public void setHandler(
			HandlerFiltroRapido handlerFiltro,
			HandlerDiscador handlerDiscador){
		this.handlerDiscador = handlerDiscador;
		this.handlerFiltro = handlerFiltro;
	}
	
	protected DatabaseCustomAdapter(Activity pActivity, boolean pIsAsc, 
			String nomeTabela, String pNomeAtributoItemListOrdenacao) throws OmegaDatabaseException
	{		
		this.activity = pActivity;
		this.isAsc = pIsAsc;
		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoItemListOrdenacao;
		comparator = new NameCustomItemListComparator(true, nomeAtributoItemListOrdenacao); 
		initializePermissao();
		initalizeListCustomItemList();
	}
	
	private void initializePermissao() throws OmegaDatabaseException{
		DatabasePontoEletronico db = new DatabasePontoEletronico(activity);
		EXTDAOPermissaoCategoriaPermissao vObj = new EXTDAOPermissaoCategoriaPermissao(db);
		isEditPermitido = vObj.hasPermissaoToEdit(nomeTabela);
		isRemovePermitido = vObj.hasPermissaoToRemove(nomeTabela);
		db.close();
	}
	
	
	public boolean isEditPermitido(){
		return isEditPermitido;
	}
	
	public boolean isRemovePermitido(){
		return isRemovePermitido;
	}
	
	public Class<?> getClasseFiltro(){
		return filtroClass;
	}
	
	public Class<?> getClasseFormulario(){
		return formularioClass;
	}
	protected DatabaseCustomAdapter(
			Activity pActivity, 
			boolean pIsAsc, 
			String nomeTabela, 
			boolean loadCustomItemList, 
			String pNomeAtributoItemListOrdenacao,
			Class<?> formularioClass,
			Class<?> filterClass) throws OmegaDatabaseException
	{
		this.formularioClass = formularioClass;
		this.activity = pActivity;
		this.isAsc = pIsAsc;
		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoItemListOrdenacao;
		this.comparator = new NameCustomItemListComparator(
				true, pNomeAtributoItemListOrdenacao);
		this.filtroClass = filterClass;
		initializePermissao();
		if(loadCustomItemList)
			initalizeListCustomItemList();
		
	}

	protected DatabaseCustomAdapter(
			Activity pActivity,
			String nomeTabela,
			boolean loadCustomItemList,
			String pNomeAtributoItemListOrdenacao,
			Class<?> formularioClass,
			Class<?> filterClass) throws OmegaDatabaseException
	{
		this.formularioClass = formularioClass;
		this.activity = pActivity;

		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoItemListOrdenacao;
		this.comparator = new NameCustomItemListComparator(
				true, pNomeAtributoItemListOrdenacao);
		this.filtroClass = filterClass;
		initializePermissao();
		if(loadCustomItemList)
			initalizeListCustomItemList();

	}
	
	protected DatabaseCustomAdapter(
			Activity pActivity, 
			boolean pIsAsc, 
			String nomeTabela, 
			boolean loadCustomItemList, 
			String pNomeAtributoItemListOrdenacao) throws OmegaDatabaseException
	{
		this.activity = pActivity;
		this.isAsc = pIsAsc;
		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoItemListOrdenacao;
		comparator = new NameCustomItemListComparator(true, nomeAtributoItemListOrdenacao);
		initializePermissao();
		if(loadCustomItemList)
			initalizeListCustomItemList();
		
	}
	
	protected void initalizeListCustomItemList(){
		this.listaCompleta = loadData(this.isAsc);
		this.listaFiltrada =this.listaCompleta; 
	}
	
	protected DatabaseCustomAdapter(Activity pActivit, boolean pIsAsc, String nomeTabela, String pNomeAtributoOrdenacao, boolean loadCustomItemList) throws OmegaDatabaseException
	{
		this.activity = pActivit;
		this.isAsc = pIsAsc;
		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoOrdenacao;
		comparator = new NameCustomItemListComparator(true, nomeAtributoItemListOrdenacao);
		initializePermissao();
		if(loadCustomItemList){
			initalizeListCustomItemList();
		}
	}

	public String getNomeTabela(){
		return nomeTabela;
	}
	
	public boolean isAsc(){
		return isAsc;
	}

	protected void PersonalizaItemView(View vLayoutView, int position){
		if((position % 2) == 0)
		{
			vLayoutView.setBackgroundResource(R.drawable.ui_button_blue);
		}
		else
		{
			vLayoutView.setBackgroundResource(R.drawable.ui_button_white);
			
		}
		HelperFonte.setFontAllView(vLayoutView);
	}
	
	protected void CreateTextViewHtml(int pRLayoutId, String pText, View pView)
	{
		CreateTextViewHtml(pRLayoutId, pText, pView, false);
	}
	protected void CreateTextView(int pRLayoutId, String pText, View pView)
	{
		CreateTextView(pRLayoutId, pText, pView, false);
	}

	protected void CreateLinearLayout(int pRLayoutId, View pView, OnClickListener pOnClickListener)
	{
		LinearLayout vLinearLayout = (LinearLayout)pView.findViewById(pRLayoutId);
		vLinearLayout.setClickable(true);
		vLinearLayout.setFocusable(true);
		if(pOnClickListener != null)
			vLinearLayout.setOnClickListener(pOnClickListener);
	}

	protected void setVisibilityHidden(int pRLayoutId, View pView)
	{
		TextView vTextView = (TextView) pView.findViewById(pRLayoutId);
		vTextView.setVisibility(View.INVISIBLE);
	}
	protected TextView CreateTextViewHtml(int pRLayoutId, String pText, View pView, Boolean pAppend)
	{
		
		
		TextView vTextView = (TextView) pView.findViewById(pRLayoutId);
		if(pText == null || pText.length() == 0){
			setVisibilityHidden(pRLayoutId, pView);
		}else{
			vTextView.setText(Html.fromHtml(pText));
//			vTextView.setText(Html.fromHtml("<b>" + pText + "</b>" +  "<br />" + 
//		            "<small>" + pText + "</small>" + "<br />" + 
//		            "<small>" + pText + "</small>"));
			
		
		}
		return vTextView;
	}
	protected TextView CreateTextView(int pRLayoutId, String pText, View pView, Boolean pAppend)
	{
		TextView vTextView = (TextView) pView.findViewById(pRLayoutId);
		if(pText == null || pText.length() == 0){
			setVisibilityHidden(pRLayoutId, pView);
		}else{
			if(pAppend)
				vTextView.setText(new StringBuilder(vTextView.getText() + pText));
			else
				vTextView.setText(pText);
		}
		return vTextView;
	}
	protected void CreateDetailButton(View pView, int pRLayoutId, String pIdCustomItemList, short p_searchMode){
		CreateDetailButton(pView, pRLayoutId, pIdCustomItemList, p_searchMode, null, null);
	}
	protected void CreateDetailButton(View pView, int pRLayoutId, String pIdCustomItemList, short p_searchMode, OnLongClickListener pOnLongClickListener){
		CreateDetailButton(pView, pRLayoutId, pIdCustomItemList, p_searchMode, pOnLongClickListener, null);
	}
	protected void CreateDetailButton(View pView, int pRLayoutId, String pIdCustomItemList, short p_searchMode, OnLongClickListener pOnLongClickListener, String email)
	{
		View viewDetalhes = pView.findViewById(pRLayoutId);
		
		if(viewDetalhes != null ){
			String className = viewDetalhes.getClass().getName();
			if(className.equals("android.widget.Button")){
				Button b = (Button) viewDetalhes;
				b.setClickable(true);
				b.setFocusable(true);
			} else if (className.equals("android.widget.ImageView")){
				ImageView iv = (ImageView) viewDetalhes;
				iv.setClickable(true);
				iv.setFocusable(true);
			}	
		}
		if(viewDetalhes != null){
			viewDetalhes.setOnClickListener(new DetailButtonClickListener(activity, pIdCustomItemList, p_searchMode));
		}
		pView.setOnClickListener(new DetailButtonClickListener(
				activity, pIdCustomItemList, p_searchMode, email));
		if(pOnLongClickListener != null)
			pView.setOnLongClickListener(pOnLongClickListener);

	}
	
	protected void CreateDetailView(View viewDetalhes, String pIdCustomItemList, short p_searchMode, OnLongClickListener pOnLongClickListener)
	{
		
		
		if(viewDetalhes != null ){
			//String className = viewDetalhes.getClass().getName();
			viewDetalhes.setClickable(true);
			viewDetalhes.setFocusable(true);
				
		}
		
		viewDetalhes.setOnClickListener(new DetailButtonClickListener(activity, pIdCustomItemList, p_searchMode));
		if(pOnLongClickListener != null)
			viewDetalhes.setOnLongClickListener(pOnLongClickListener);

	}

	public Activity getActivity(){
		return activity;
	}
	
	protected void CreateDetailButton(String pIdCustomItemList, View pView, short p_searchMode) {
		CreateDetailButton(pView, R.id.custom_button_detail, pIdCustomItemList, p_searchMode);		
	}

	public int getCount() {	
		if(this.listaFiltrada == null)
			return 0;
		else return this.listaFiltrada.size();
	}

	public CustomItemList getItem(int pPosition) {
		return this.listaFiltrada.get(pPosition);
	}

	public ArrayList<CustomItemList> loadData(boolean p_isAsc )
	{

		try {

			ArrayList<Long> vListId = getListIdBySearchParameters(p_isAsc);

			ArrayList<CustomItemList> vRetorno = getListCustomItemList( vListId);
			try{
				Collections.sort(vRetorno, new NameCustomItemListComparator(p_isAsc, nomeAtributoItemListOrdenacao));
			} catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.FILTRO);
			}
			return vRetorno;

		}catch(OmegaDatabaseException oe){
			SingletonLog.factoryToast(activity, oe, SingletonLog.TIPO.ADAPTADOR);
		}
		catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.FILTRO);
		}

		return null;
	}


	public ArrayList<CustomItemList> getListCustomItemList(
			ArrayList<Long> ids) throws Exception {
		DatabasePontoEletronico database = null;
		ArrayList<CustomItemList> customs = new ArrayList<CustomItemList>();
		try{
			if(ids == null) return null;

			database = new DatabasePontoEletronico(this.activity);
			
			Table objTable = null;
			objTable = database.factoryTable(this.nomeTabela);

			for (Long iLong : ids) {
				if(objTable.select(iLong.toString())){
					//				CustomItemList item = pObj.createFuncionarioItemList();
					CustomItemList item = getCustomItemListOfTable(objTable);
					if(item != null)
						customs.add(item);
					objTable.clearData();
				}
			}

			return customs;
		}catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.FILTRO);
		}
		finally
		{
			if(database != null){
				database.close();	
			}

		}
		return customs;
	}

	public long getItemId(int pPosition) {		
		CustomItemList vCustom = this.listaFiltrada.get(pPosition);
		String id = vCustom.getId();
		try{
			if(id != null)
				return Long.parseLong(id);
			else return -1;
		} catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.ADAPTADOR);
			return -1;
		}

	}

	protected abstract ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) throws OmegaDatabaseException;

	public abstract CustomItemList getCustomItemListOfTable(Table pObj);

	public void SortList(boolean pAsc) {
		comparator.setAsc(pAsc);
		Collections.sort(listaFiltrada, (Comparator<Object>) comparator);
		this.notifyDataSetChanged();
	}

	public  abstract View getView(int position, View convertView, ViewGroup parent) ;

	//Funcao a ser sobreescrita para o filtro rupido
	public ArrayList<CustomItemList> filtraToken(CharSequence cs){
		return null;
	}
	
	
	public String insereTagHtml(String token, String search){
		int indice = token.indexOf(search);
		if(indice >= 0 ){
			String formatado = token.substring(0, indice) 
					+ "<b>" 
					+ token.substring(indice, search.length()) 
					+ "</b>"
					+ token.substring(indice + search.length(), token.length() - (indice + ultimaBuscaMinusculo.length()));
			return formatado;
		} else return null;
	}
	
	
    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
            	listaFiltrada = (ArrayList<CustomItemList>) results.values;
        		notifyDataSetChanged();	
    
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
            	FilterResults results = new FilterResults();
            	//Se nenhum dos dois(discador e filtro rupido) filtros estiver ativo
            	if((handlerFiltro == null || (handlerFiltro != null && ! handlerFiltro.ativo))
            		&& (handlerDiscador == null || (handlerDiscador != null && ! handlerDiscador.ativo))	){
            		results.count = listaCompleta.size();
                    results.values = listaCompleta;	
                    return results; 
            	}
            	constraint = constraint.toString().trim();
            	ultimaBuscaMaiusculo = constraint.toString().toLowerCase();
            	ultimaBuscaMinusculo = constraint.toString().toUpperCase();
            	
            	
            	if(constraint == null || constraint.length() == 0){
            		results.count = listaCompleta.size();
                    results.values = listaCompleta;
                    return results;
            	}
            	
                ArrayList<CustomItemList> listaFiltrada = filtraToken(constraint.toString());
                
                
                
                if(listaFiltrada == null ){
                	results.count = 0;
                    results.values = null;	
                } else {
                	results.count = listaFiltrada.size();
                    results.values = listaFiltrada;	
                }
                
                return results;
            }
        };

        return filter;
    }
}
