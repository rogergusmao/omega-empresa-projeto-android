
package app.omegasoftware.pontoeletronico.common.Adapter;



import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.app.Activity;
import android.content.Context;
import android.view.GestureDetector;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.CustomItemListComparator;
import app.omegasoftware.pontoeletronico.common.NameCustomItemListComparator;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPermissaoCategoriaPermissao;
import app.omegasoftware.pontoeletronico.listener.DetailButtonClickListener;

public abstract class DatabaseCustomArrayAdapter extends ArrayAdapter<CustomItemList> {

	protected static final int ID_TEXT_VIEW_DESCRICAO_1 = R.id.tv_descricao_1;
	protected static final int ID_TEXT_VIEW_DESCRICAO_2 = R.id.tv_descricao_2;
	protected static final int ID_TEXT_VIEW_DESCRICAO_3 = R.id.tv_descricao_3;
	protected static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;
	
	protected ArrayList<CustomItemList> list = new ArrayList<CustomItemList>();
	protected ArrayList<GestureDetector> listSlideGestureDetector = new ArrayList<GestureDetector>();
	protected Activity activity;
	boolean isAsc;

	String nomeTabela;
	String nomeAtributoItemListOrdenacao;
	boolean isEditPermitido = false;
	boolean isRemovePermitido = false;
	
	
	public abstract Class<?> getClassFormEdit();
	short searchMode;
	
	CustomItemListComparator comparator;
	public DatabaseCustomArrayAdapter(Context context, int layoutId)
	{
		super(context, layoutId);
	}
	protected DatabaseCustomArrayAdapter(Activity pActivity, short p_searchMode, int layoutId, boolean pIsAsc, String nomeTabela, String pNomeAtributoItemListOrdenacao, CustomItemListComparator pComparator) throws OmegaDatabaseException
	{		
		super(pActivity.getApplicationContext(), layoutId);
		this.activity = pActivity;
		this.isAsc = pIsAsc;
		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoItemListOrdenacao;
		comparator = pComparator;
		initializePermissao();
		this.list = loadData(pIsAsc);
		searchMode = p_searchMode;
		
	}
	
	protected DatabaseCustomArrayAdapter(Activity pActivity, short p_searchMode, int layoutId, boolean pIsAsc, String nomeTabela, String pNomeAtributoItemListOrdenacao) throws OmegaDatabaseException
	{		
		super(pActivity.getApplicationContext(), layoutId);
		this.activity = pActivity;
		this.isAsc = pIsAsc;
		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoItemListOrdenacao;
		comparator = new NameCustomItemListComparator(true, nomeAtributoItemListOrdenacao); 
		initializePermissao();
		this.list = loadData(pIsAsc);
		searchMode = p_searchMode;
	}
	
	private void initializePermissao() throws OmegaDatabaseException{
		DatabasePontoEletronico db = new DatabasePontoEletronico(activity);
		EXTDAOPermissaoCategoriaPermissao vObj = new EXTDAOPermissaoCategoriaPermissao(db);
		isEditPermitido = vObj.hasPermissaoToEdit(nomeTabela);
		isRemovePermitido = vObj.hasPermissaoToRemove(nomeTabela);
		db.close();
	}
	
	public boolean isEditPermitido(){
		return isEditPermitido;
	}
	
	public boolean isRemovePermitido(){
		return isRemovePermitido;
	}
	
	protected DatabaseCustomArrayAdapter(
			Activity pActivity,
			 short p_searchMode,
			int layoutId, 
			boolean pIsAsc, 
			String nomeTabela, 
			boolean loadCustomItemList, 
			String pNomeAtributoItemListOrdenacao) throws OmegaDatabaseException
	{
		super(pActivity.getApplicationContext(), layoutId);
		this.activity = pActivity;
		this.isAsc = pIsAsc;
		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoItemListOrdenacao;
		comparator = new NameCustomItemListComparator(true, nomeAtributoItemListOrdenacao);
		initializePermissao();
		if(loadCustomItemList)
			this.list = loadData(pIsAsc);
		searchMode = p_searchMode;
	}	
	
	public short getSearchMode(){
		return searchMode;
	}

	protected void initalizeListCustomItemList(){
		this.list = loadData(this.isAsc);
	}
	
	protected DatabaseCustomArrayAdapter(
			Activity pActivity, 
			int layoutId, 
			boolean pIsAsc, 
			String nomeTabela, 
			String pNomeAtributoOrdenacao, 
			boolean loadCustomItemList) throws OmegaDatabaseException
	{
		super(pActivity.getApplicationContext(), layoutId);
		this.activity = pActivity;
		this.isAsc = pIsAsc;
		this.nomeTabela = nomeTabela;
		this.nomeAtributoItemListOrdenacao = pNomeAtributoOrdenacao;
		comparator = new NameCustomItemListComparator(true, nomeAtributoItemListOrdenacao);
		initializePermissao();
		if(loadCustomItemList)
			this.list = loadData(pIsAsc);
	}

	public String getNomeTabela(){
		return nomeTabela;
	}
	
	public boolean isAsc(){
		return isAsc;
	}

	protected void CreateTextView(int pRLayoutId, String pText, View pView)
	{
		CreateTextView(pRLayoutId, pText, pView, false);
	}

	protected void PersonalizaItemView(View vLayoutView, int position){
		if((position % 2) == 0)
		{
			vLayoutView.setBackgroundResource(R.drawable.ui_button_blue);
		}
		else
		{
			vLayoutView.setBackgroundResource(R.drawable.ui_border_gray);
			
		}
	}
	protected void CreateLinearLayout(int pRLayoutId, View pView, OnClickListener pOnClickListener)
	{
		LinearLayout vLinearLayout = (LinearLayout)pView.findViewById(pRLayoutId);
		vLinearLayout.setClickable(true);
		vLinearLayout.setFocusable(true);
		if(pOnClickListener != null)
			vLinearLayout.setOnClickListener(pOnClickListener);
	}

	protected void setVisibilityHidden(int pRLayoutId, View pView)
	{
		TextView vTextView = (TextView) pView.findViewById(pRLayoutId);
		vTextView.setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
	}
	
	protected TextView CreateTextView(int pRLayoutId, String pText, View pView, Boolean pAppend)
	{
		TextView vTextView = (TextView) pView.findViewById(pRLayoutId);
		if(pText == null || pText.length() == 0){
			setVisibilityHidden(pRLayoutId, pView);
		}else{
			if(pAppend)
				vTextView.setText(new StringBuilder(vTextView.getText() + pText));
			else
				vTextView.setText(pText);
		}
		return vTextView;
	}
	protected void CreateDetailButton(View pView, int pRLayoutId, String pIdCustomItemList, short p_searchMode){
		CreateDetailButton(pView, pRLayoutId, pIdCustomItemList, p_searchMode, null);
	}
	
	protected void CreateDetailButton(View pView, int pRLayoutId, String pIdCustomItemList, short p_searchMode, OnLongClickListener pOnLongClickListener)
	{
		View viewDetalhes = pView.findViewById(pRLayoutId);
		
		if(viewDetalhes != null ){
			String className = viewDetalhes.getClass().getName();
			if(className.equals("android.widget.Button")){
				Button b = (Button) viewDetalhes;
				b.setClickable(true);
				b.setFocusable(true);
			} else if (className.equals("android.widget.ImageView")){
				ImageView iv = (ImageView) viewDetalhes;
				iv.setClickable(true);
				iv.setFocusable(true);
			}	
		}
		
		pView.setOnClickListener(new DetailButtonClickListener(activity,pIdCustomItemList, p_searchMode));
		if(pOnLongClickListener != null)
		pView.setOnLongClickListener(pOnLongClickListener);
	}

	public Activity getActivity(){
		return activity;
	}
	
	protected void CreateDetailButton(String pIdCustomItemList, View pView, short p_searchMode) {
//		CreateDetailButton(pView, R.id.custom_button_detail, pIdCustomItemList, p_searchMode);		
	}

	public int getCount() {	
		if(this.list == null)
			return 0;
		else return this.list.size();
	}

	public CustomItemList getItem(int pPosition) {
		return this.list.get(pPosition);
	}

	public ArrayList<CustomItemList> loadData(boolean p_isAsc )
	{

		try {

			ArrayList<Long> vListId = getListIdBySearchParameters(p_isAsc);

			ArrayList<CustomItemList> vRetorno = getListCustomItemList( vListId);
			try{
			Collections.sort(vRetorno, new NameCustomItemListComparator(p_isAsc, nomeAtributoItemListOrdenacao));
			} catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
			}
			return vRetorno;

		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.ADAPTADOR);
		}

		return null;
	}


	public ArrayList<CustomItemList> getListCustomItemList(
			ArrayList<Long> pArrayListLong) throws Exception {
		if(pArrayListLong == null) return null;
		ArrayList<CustomItemList> vArrayFuncionarioItemList = new ArrayList<CustomItemList>();
		DatabasePontoEletronico database = new DatabasePontoEletronico(this.activity);
		Table vObjTable = database.factoryTable(this.nomeTabela);
		try{
			
			for (Long iLong : pArrayListLong) {

				vObjTable.setAttrValue(
						Table.ID_UNIVERSAL,
						iLong.toString());
				vObjTable.select();
				//				CustomItemList item = pObj.createFuncionarioItemList();
				CustomItemList item = getCustomItemListOfTable(vObjTable);
				if(item != null)
					vArrayFuncionarioItemList.add(item);
				
				vObjTable.clearData();
			}
			while(vArrayFuncionarioItemList.size() > 0 &&
					vArrayFuncionarioItemList.get(vArrayFuncionarioItemList.size() - 1) == null)
					vArrayFuncionarioItemList.remove(vArrayFuncionarioItemList.size() - 1);
			return vArrayFuncionarioItemList;	
		}catch (Exception e) {
			
			e.printStackTrace();
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
		finally
		{
			if(database != null){
				database.close();	
			}

		}
		return null;
	}

	public long getItemId(int pPosition) {		
		CustomItemList vCustom = this.list.get(pPosition);
		String id = vCustom.getId();
		try{
			if(id != null)
				return Long.parseLong(id);
			else return -1;
		} catch(Exception ex){
			return -1;
		}

	}
	
	public CustomItemList findId(String id){
		for(int i = 0 ; i < list.size(); i++){
			CustomItemList item = list.get(i);
			if(item.getId().compareTo(id) == 0) return item;
		}
		return null;
	}
	

	public int findPosition(String id){
		for(int i = 0 ; i < list.size(); i++){
			CustomItemList item = list.get(i);
			if(item.getId().compareTo(id) == 0) return i;
		}
		return -1;
	}


	protected abstract ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc);

	public abstract CustomItemList getCustomItemListOfTable(Table pObj);

	public void delete(CustomItemList item){
		list.remove(item);
	}

	
	public void SortList(boolean pAsc) {
		comparator.setAsc(pAsc);
		Collections.sort(list, (Comparator<Object>) comparator);
		this.notifyDataSetChanged();
	}

	public  abstract View getView(int position, View convertView, ViewGroup parent) ;
	
	public Table getObjTable() throws OmegaDatabaseException{
		Database db = new DatabasePontoEletronico(activity);
		Table obj = db.factoryTable(getNomeTabela());
		return obj;
		
		
	}

}
