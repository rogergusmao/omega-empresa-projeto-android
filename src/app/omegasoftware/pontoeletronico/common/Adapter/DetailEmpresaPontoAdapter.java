package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.EmpresaPontoItemList;
import app.omegasoftware.pontoeletronico.common.activity.HelperFonte;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class DetailEmpresaPontoAdapter extends BaseAdapter {

	private ArrayList<EmpresaPontoItemList> itens;
	private Activity context;
	
	Database db=null;
	EXTDAOEmpresa objEmpresa;
	String idEmpresa;
	
	public DetailEmpresaPontoAdapter(
			Activity context,
			ArrayList<EmpresaPontoItemList> itens) throws OmegaDatabaseException
	{
		this.itens = itens;
		this.context = context;
		
		db = new DatabasePontoEletronico(context);
		objEmpresa = new EXTDAOEmpresa(db);
		
	}
	
	public int getCount() {
		return this.itens.size();
	}

	public EmpresaPontoItemList getItem(int position) {
		return this.itens.get(position);
	}

	public long getItemId(int position) {
		return Long.valueOf(this.itens.get(position).getId());
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		
		final EmpresaPontoItemList empresaPontoItemList = this.getItem(position);
		
		 
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View layoutView = layoutInflater.inflate(R.layout.item_list_empresa_ponto_layout, null);
		TextView tvEmpresa = (TextView) layoutView.findViewById(R.id.empresa_text_view);
		TextView tvTotalFuncionarios = (TextView)layoutView.findViewById(R.id.tv_total_funcionarios);
		TextView tvNoTurno =(TextView)layoutView.findViewById(R.id.tv_no_turno);
		TextView tvNoIntervalo =(TextView)layoutView.findViewById(R.id.tv_no_intervalo);
		TextView tvNaoCompareceram =(TextView)layoutView.findViewById(R.id.tv_nao_compareceram);
		
		tvEmpresa.setText(
				HelperString.ucFirstForEachToken( 
						empresaPontoItemList.getConteudoContainerItem(EmpresaPontoItemList.ATRIBUTOS.EMPRESA.toString(),"-")));
		tvNoTurno.setText( empresaPontoItemList.getConteudoContainerItem(EmpresaPontoItemList.ATRIBUTOS.TOTAL_FUNCIONARIO_NO_TURNO.toString(), "-"));
		tvTotalFuncionarios.setText(empresaPontoItemList.getConteudoContainerItem(EmpresaPontoItemList.ATRIBUTOS.TOTAL_FUNCIONARIOS.toString(), "-"));
		tvNoIntervalo.setText( empresaPontoItemList.getConteudoContainerItem(EmpresaPontoItemList.ATRIBUTOS.TOTAL_FUNCIONARIOS_INTERVALO.toString(), "-"));
		tvNaoCompareceram.setText( empresaPontoItemList.getConteudoContainerItem(EmpresaPontoItemList.ATRIBUTOS.TOTAL_FUNCIONARIO_NAO_COMPARECERAM.toString(), "-"));
				
		ImageButton buttonVer = (ImageButton) layoutView.findViewById(R.id.ver_button);
	
		final Class<?> classeDetalhes = DetailEmpresaActivityMobile.class;
		final String idDetalhes = String.valueOf(empresaPontoItemList.getId()) ;
		buttonVer.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent intent = new Intent(
						context.getApplicationContext(), 
						classeDetalhes);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, idDetalhes);
				context.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
				
			}
		});
		HelperFonte.setFontAllView(layoutView);
		
		
		return layoutView;
		
	}
	
}

