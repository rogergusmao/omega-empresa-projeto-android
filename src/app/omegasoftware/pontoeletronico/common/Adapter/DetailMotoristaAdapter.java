package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.MotoristaItemList;
import app.omegasoftware.pontoeletronico.common.activity.HelperFonte;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class DetailMotoristaAdapter extends BaseAdapter {

	private ArrayList<MotoristaItemList> itens;
	private Activity context;
	
	Database db=null;
	EXTDAOUsuario objUsuario;
	String idEmpresa;
	
	public DetailMotoristaAdapter(
			Activity context,
			ArrayList<MotoristaItemList> itens)
	{
		this.itens = itens;
		this.context = context;
		
		try {
			db = new DatabasePontoEletronico(context);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		objUsuario = new EXTDAOUsuario(db);
		
	}
	
	public int getCount() {
		return this.itens.size();
	}

	public MotoristaItemList getItem(int position) {
		return this.itens.get(position);
	}

	public long getItemId(int position) {
		return Long.valueOf(this.itens.get(position).getId());
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		
		final MotoristaItemList itemList = this.getItem(position);
		
		 
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View layoutView = layoutInflater.inflate(R.layout.item_list_motorista_layout, null);
		TextView tvEmail = (TextView) layoutView.findViewById(R.id.email_textview);
		TextView tvNome = (TextView)layoutView.findViewById(R.id.nome_textview);
		
		tvEmail.setText(itemList.getConteudoContainerItem(MotoristaItemList.ATRIBUTOS.EMAIL.toString(),"-", true));
		tvNome.setText( 
				HelperString.ucFirstForEachToken(
						itemList.getConteudoContainerItem(MotoristaItemList.ATRIBUTOS.NOME.toString(), "-")));
				
		ImageButton buttonVer = (ImageButton) layoutView.findViewById(R.id.ver_button);
	
		final Class<?> classeDetalhes = DetailUsuarioActivityMobile.class;
		final String idDetalhes = itemList.getConteudoContainerItem(MotoristaItemList.ATRIBUTOS.ID_USUARIO.toString());
		buttonVer.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent intent = new Intent(
						context.getApplicationContext(), 
						classeDetalhes);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, idDetalhes);
				context.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
				
			}
		});
		HelperFonte.setFontAllView(layoutView);
		
		
		return layoutView;
		
	}
	
}

