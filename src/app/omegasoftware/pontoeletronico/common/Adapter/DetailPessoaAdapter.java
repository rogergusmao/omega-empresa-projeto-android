package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutPessoaEmpresa;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPessoaEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.ItemList.EmpresaProfissaoItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.EmpresaProfissaoItemList.ACAO_DETALHES;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOOperadora;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class DetailPessoaAdapter {

	// private static final String TAG = "DetaiPessoaAdapter";

	private String PREFIXO_NAO_SINCRONIZADO;
	Activity activity;
	AppointmentPlaceAdapter emailTelefoneEEndereco;
	EXTDAOPessoa objPessoa;
	String idUsuario;
	Database db = null;
	ContainerLayoutPessoaEmpresa containerPessoaEmpresa;
	View viewDetailUsuario;

	public DetailPessoaAdapter(Activity activity, EXTDAOPessoa objPessoa, View viewDetailUsuario) throws Exception {
		this.activity = activity;
		this.objPessoa = objPessoa;
		PREFIXO_NAO_SINCRONIZADO = activity.getString(R.string.prefixoNaoSincronizado);
		idUsuario = objPessoa.getIdUsuario();
		db = objPessoa.getDatabase();
		containerPessoaEmpresa = new ContainerLayoutPessoaEmpresa(activity);
		emailTelefoneEEndereco = objPessoa.getAppointmentPlacesAdapter(activity.getApplicationContext());
		this.viewDetailUsuario = viewDetailUsuario;

	}

	public void formatarView() {
		try {
			refreshIdentificador();
			refreshNovaProfissao();
			refreshNome();
			refreshPessoaOperadora();
			refreshTipoDocumento();
			refreshPessoaEmpresa();
			refreshSexo();
			refreshEmailEnderecoETelefone(true);
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(activity.getApplicationContext(), ex, null);
		}

	}

	public void refreshNovaProfissao() {
		try {
			LinearLayout ll = (LinearLayout) viewDetailUsuario.findViewById(R.id.ll_cadastrar);
			if (ll != null) {
				ll.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent(activity, FormPessoaEmpresaActivityMobile.class);
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA, objPessoa.getId());
						activity.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);

					}
				});
			}
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(activity.getApplicationContext(), ex, null);
		}
	}

	/**
	 * Updates the TextView which holds the medic name
	 */
	public void refreshNome() {
		try {
			String nomeFuncionario = objPessoa.getAttribute(EXTDAOPessoa.NOME).getStrValue();
			if (nomeFuncionario != null && nomeFuncionario.length() > 0) {
				nomeFuncionario = nomeFuncionario.toUpperCase();
				((TextView) viewDetailUsuario.findViewById(R.id.tv_title)).setText(nomeFuncionario);

			}
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(activity.getApplicationContext(), ex, null);
		}
	}

	public void refreshIdentificador() {
		try {
			String identificador = objPessoa.getId();
			if (HelperInteger.parserInt(identificador) < 0) {
				identificador = identificador.replace("-", PREFIXO_NAO_SINCRONIZADO);
				((TextView) viewDetailUsuario.findViewById(R.id.tv_identificador)).setText(identificador);

				View v = viewDetailUsuario.findViewById(R.id.tv_nao_sincronizado);
				if (v != null)
					((TextView) v).setVisibility(View.VISIBLE);
			} else {
				((TextView) viewDetailUsuario.findViewById(R.id.tv_identificador)).setText(identificador);
				View v = viewDetailUsuario.findViewById(R.id.tv_nao_sincronizado);
				if (v != null)
					((TextView) v).setVisibility(View.GONE);
			}
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(activity.getApplicationContext(), ex, null);
		}
	}

	public void refreshPessoaOperadora() {
		try {
			String operadora = objPessoa.getAttribute(EXTDAOPessoa.OPERADORA_ID_INT).getStrValue();
			boolean vValidade = false;
			if (operadora != null && operadora.length() > 0) {
				EXTDAOOperadora vObjOperadora = new EXTDAOOperadora(db);
				vObjOperadora.setAttrValue(EXTDAOOperadora.ID, operadora);
				if (vObjOperadora.select()) {
					vValidade = true;
					String vNomeOperadora = vObjOperadora.getAttribute(EXTDAOOperadora.NOME).getStrValue();
					vNomeOperadora = vNomeOperadora.toUpperCase();

					((TextView) viewDetailUsuario.findViewById(R.id.conteudo_operadora)).setText(vNomeOperadora);

				}
			}

			if (!vValidade)
				((LinearLayout) viewDetailUsuario.findViewById(R.id.linearLayout_operadora))
						.setVisibility(View.GONE);
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(activity.getApplicationContext(), ex, null);
		}
	}

	public void refreshTipoDocumento() {
		try {
			String nomeTipoDocumento = objPessoa.getNomeDaChaveExtrangeira(EXTDAOPessoa.TIPO_DOCUMENTO_ID_INT);
			String numeroDocumento = objPessoa.getStrValueOfAttribute(EXTDAOPessoa.NUMERO_DOCUMENTO);
			if (nomeTipoDocumento != null && numeroDocumento != null && nomeTipoDocumento.length() > 0
					&& numeroDocumento.length() > 0) {
				((TextView) viewDetailUsuario.findViewById(R.id.funcionario_details_tipo_documento))
						.setText(nomeTipoDocumento);
				((TextView) viewDetailUsuario.findViewById(R.id.funcionario_details_numero_documento))
						.setText(numeroDocumento);

			} else {
				((TextView) viewDetailUsuario.findViewById(R.id.funcionario_details_tipo_documento))
						.setVisibility(View.GONE);
				((TextView) viewDetailUsuario.findViewById(R.id.funcionario_details_numero_documento))
						.setVisibility(View.GONE);
				((LinearLayout) viewDetailUsuario.findViewById(R.id.linearLayout_tipo_documento))
						.setVisibility(View.GONE);
			}
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(activity.getApplicationContext(), ex, null);
		}
	}

	public void addLayoutEmpresaPessoa(String idPessoaEmpresa, String idEmpresa, String idProfissao, String nomeEmpresa,
			String nomeProfissao, String idUsuario) throws OmegaDatabaseException {
		if (idEmpresa == null || idProfissao == null) {
			return;
		} else if (idEmpresa.length() == 0 || idProfissao.length() == 0) {
			return;
		}

		if (!containerPessoaEmpresa.containsTuple(idEmpresa, idProfissao)) {

			// String descPessoa = objPessoa.getDescricao();
			EmpresaProfissaoItemList empresaProfissaoItemList = new EmpresaProfissaoItemList(
					containerPessoaEmpresa.getUltimoIdEmpresa(), idUsuario, objPessoa.getId(), idEmpresa, idProfissao,
					String.valueOf(idPessoaEmpresa), nomeEmpresa, nomeProfissao, null, ACAO_DETALHES.EMPRESA);
			
			LinearLayout linearLayoutEmpresaFuncionario = (LinearLayout) viewDetailUsuario
					.findViewById(R.id.linearlayout_profissoes_da_pessoa);

			ArrayList<EmpresaProfissaoItemList> places = new ArrayList<EmpresaProfissaoItemList>();
			places.add(empresaProfissaoItemList);
			
			DetailPessoaEmpresaAdapter adapter = new DetailPessoaEmpresaAdapter(activity, places, containerPessoaEmpresa);

			View vNewView = adapter.getView(0, null, null);
			linearLayoutEmpresaFuncionario.addView(vNewView);
			containerPessoaEmpresa.add(idPessoaEmpresa, idEmpresa, nomeProfissao, vNewView);
		}
	}

	/**
	 * Updates the TextView which holds the funcionario profissao
	 */
	public boolean refreshPessoaEmpresa() {
		try {
			EXTDAOPessoaEmpresa vFuncionarioEmpresa = new EXTDAOPessoaEmpresa(this.db);

			vFuncionarioEmpresa.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, objPessoa.getId());

			ArrayList<Table> vList = vFuncionarioEmpresa.getListTable();
			boolean vValidade = false;
			if (vList != null && vList.size() > 0) {
				LinearLayout llPessoaEmpresa = ((LinearLayout) viewDetailUsuario
						.findViewById(R.id.linearlayout_profissoes_da_pessoa));
				(viewDetailUsuario.findViewById(R.id.tv_profissoes_da_pessoa))
						.setVisibility(View.VISIBLE);
				llPessoaEmpresa.setVisibility(View.VISIBLE);
				llPessoaEmpresa.removeAllViews();
				containerPessoaEmpresa.clear();
				vValidade = true;
				EXTDAOProfissao objProfissao = new EXTDAOProfissao(this.db);
				EXTDAOEmpresa objEmpresa = new EXTDAOEmpresa(this.db);
				for (Table table : vList) {
					EXTDAOPessoaEmpresa objPessoaEmpresa = (EXTDAOPessoaEmpresa) table;
					String idProfissao = objPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT);
					String idEmpresa = objPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.EMPRESA_ID_INT);

					objProfissao.clearData();
					objProfissao.setAttrValue(EXTDAOProfissao.ID, idProfissao);
					objProfissao.select();

					objEmpresa.clearData();
					objEmpresa.setAttrValue(EXTDAOEmpresa.ID, idEmpresa);
					objEmpresa.select();

					addLayoutEmpresaPessoa(objPessoaEmpresa.getId(), idEmpresa, idProfissao, objEmpresa.getDescricao(),
							objProfissao.getStrValueOfAttribute(EXTDAOProfissao.NOME), idUsuario);
				}
			}
			if (!vValidade) {
				(viewDetailUsuario.findViewById(R.id.tv_profissoes_da_pessoa))
						.setVisibility(View.GONE);

				(viewDetailUsuario.findViewById(R.id.linearlayout_profissoes_da_pessoa))
						.setVisibility(View.GONE);
			}

			return vValidade;
		} catch (OmegaDatabaseException e) {
			SingletonLog.factoryToast(activity, e, SingletonLog.TIPO.ADAPTADOR);
			return false;
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(activity.getApplicationContext(), ex, null);
			return false;
		}
	}

	/**
	 * Updates the TextView which holds the funcionario sex and registry
	 */
	public void refreshSexo() {
		try {
			String sexo = objPessoa.getFormattedSex(activity.getApplicationContext());
			if (sexo != null && sexo.length() > 0) {
				((TextView) viewDetailUsuario.findViewById(R.id.tv_sexo)).setText(sexo);
				((TextView) viewDetailUsuario.findViewById(R.id.tv_sexo))
						.setVisibility(View.VISIBLE);
			} else
				((TextView) viewDetailUsuario.findViewById(R.id.tv_sexo))
						.setVisibility(View.GONE);
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(activity.getApplicationContext(), ex, null);
		}
	}

	/**
	 * Updates the LinearLayout with the medic appointment places
	 */
	public void refreshEmailEnderecoETelefone(boolean emailAtivo) {
		try {
			emailTelefoneEEndereco.emailAtivo = emailAtivo;
			LinearLayout emailEnderecoTelelefoneLinearLayout = (LinearLayout) viewDetailUsuario
					.findViewById(R.id.ll_email_telefone_endereco);
			if (this.emailTelefoneEEndereco != null && this.emailTelefoneEEndereco.getCount() > 0) {
				boolean validade = false;
				emailEnderecoTelelefoneLinearLayout.removeAllViews();
				for (int i = 0; i < this.emailTelefoneEEndereco.getCount(); i++) {
					View vView = this.emailTelefoneEEndereco.getView(i, null, null);

					if (vView != null) {
						emailEnderecoTelelefoneLinearLayout.addView(vView);
						validade = true;
					}
				}
				if (!validade)
					emailEnderecoTelelefoneLinearLayout.setVisibility(View.GONE);
				else {
					emailEnderecoTelelefoneLinearLayout.setVisibility(View.VISIBLE);
				}
			} else {
				emailEnderecoTelelefoneLinearLayout.setVisibility(View.GONE);
			}
		} catch (Exception ex) {
			FactoryAlertDialog.makeToast(activity.getApplicationContext(), ex, null);
		}

	}

}
