package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.BaterPontoDaPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.RelogioDePontoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutPessoaEmpresa;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListPontoDaPessoaNaEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.EmpresaProfissaoItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.EmpresaProfissaoItemList.ACAO_DETALHES;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
import junit.runner.BaseTestRunner;

public class DetailPessoaEmpresaAdapter extends BaseAdapter {

	private ArrayList<EmpresaProfissaoItemList> listEmpresaProfissaoItemList;
	private Activity context;
	ContainerLayoutPessoaEmpresa container;
	Database db=null;
	EXTDAOPonto objPonto;
	
	public DetailPessoaEmpresaAdapter(Activity context,
			ArrayList<EmpresaProfissaoItemList> pListEmpresaProfissaoItemList,
			ContainerLayoutPessoaEmpresa pContainer) throws OmegaDatabaseException
	{
		this.listEmpresaProfissaoItemList = pListEmpresaProfissaoItemList;
		this.context = context;
		container = pContainer;
		db = new DatabasePontoEletronico(context);
		objPonto = new EXTDAOPonto(db);
		
	}
	
	public int getCount() {
		return this.listEmpresaProfissaoItemList.size();
	}

	public EmpresaProfissaoItemList getItem(int position) {
		return this.listEmpresaProfissaoItemList.get(position);
	}

	public long getItemId(int position) {
		return this.listEmpresaProfissaoItemList.get(position).getId();
	}
	final int LENGTH_STRING_TITULO = 17;
	public View getView(int position, View convertView, ViewGroup parent) {
		
		final EmpresaProfissaoItemList pessoaEmpresaItemList = this.getItem(position);
		final String idPessoaEmpresa = pessoaEmpresaItemList.getIdPessoaEmpresa();
		final String idEmpresa = pessoaEmpresaItemList.getIdEmpresa();
		final String idPessoa = pessoaEmpresaItemList.getIdPessoa();
		final String idProfissao = pessoaEmpresaItemList.getIdProfissao();
		final EmpresaProfissaoItemList.ACAO_DETALHES acaoDetalhes =pessoaEmpresaItemList.getAcaoDetalhes(); 
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View layoutView = layoutInflater.inflate(R.layout.item_list_pessoa_empresa_layout, null);
		
		View itemListPontoPessoa = layoutView.findViewById(R.id.item_list_ponto_pessoa);
		
		EXTDAOPonto.PontoPessoa pontoPessoa = objPonto.getUltimoPontoDaPessoaNaEmpresa(idPessoaEmpresa, idPessoa, idEmpresa, idProfissao);
		if(pontoPessoa != null){
			PessoaPontoAdapter ppa = new PessoaPontoAdapter(context, pontoPessoa);
			ppa.formatarView(itemListPontoPessoa);
		} else {
			itemListPontoPessoa.setVisibility(View.GONE);
		}
		itemListPontoPessoa.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				

				Intent intent = new Intent(
						context.getApplicationContext(), 
						ListPontoDaPessoaNaEmpresaActivityMobile.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA_EMPRESA, idPessoaEmpresa);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA, idPessoa);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, idEmpresa);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PROFISSAO, idProfissao);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_EMPRESA, pessoaEmpresaItemList.getEmpresa());
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_PROFISSAO, pessoaEmpresaItemList.getProfissao());
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_PESSOA, pessoaEmpresaItemList.getPessoa());
				
				context.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
				
			}
		});
		
		
		TextView profissaoTextView = (TextView) layoutView.findViewById(R.id.titulo_textview);
		profissaoTextView.setText(HelperString.substring(HelperString.ucFirst( pessoaEmpresaItemList.getProfissao()), LENGTH_STRING_TITULO));
		
		
		Button baterPontoBt = (Button) layoutView.findViewById(R.id.bater_ponto_button);
		baterPontoBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent intent = new Intent(
						context.getApplicationContext(), 
						BaterPontoDaPessoaActivityMobile.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA_EMPRESA, idPessoaEmpresa);
				context.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
				
			}
		});
		
		((Button) layoutView.findViewById(R.id.bt_ultimos_pontos)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				

				Intent intent = new Intent(
						context.getApplicationContext(), 
						ListPontoDaPessoaNaEmpresaActivityMobile.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA_EMPRESA, idPessoaEmpresa);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA, idPessoa);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, idEmpresa);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PROFISSAO, idProfissao);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_EMPRESA, pessoaEmpresaItemList.getEmpresa());
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_PROFISSAO, pessoaEmpresaItemList.getProfissao());
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_PESSOA, pessoaEmpresaItemList.getPessoa());
				
				context.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
				
			}
		});

		
		
		
		
		
		Button buttonVer = (Button )layoutView.findViewById(R.id.ver_button);
		String desc= ""; 
		switch (acaoDetalhes) {
		
		case USUARIO:
		case PESSOA:
			desc = HelperString.substring(HelperString.ucFirst(pessoaEmpresaItemList.getPessoa()), LENGTH_STRING_TITULO);
			break;
		case EMPRESA:
			desc = HelperString.substring(HelperString.ucFirst(pessoaEmpresaItemList.getEmpresa()), LENGTH_STRING_TITULO);
			break;
		default:
			buttonVer.setVisibility(View.GONE);
			break;
		}
		
		
		if(acaoDetalhes == ACAO_DETALHES.EMPRESA
				|| acaoDetalhes == ACAO_DETALHES.PESSOA
				|| acaoDetalhes == ACAO_DETALHES.USUARIO){
			if(desc == null || desc.length() == 0 )
				desc = context.getResources().getString(R.string.anonimo);
			buttonVer.setText(desc);
			final Class<?> classeDetalhes = pessoaEmpresaItemList.getClasseDetalhes();
			final String idDetalhes = pessoaEmpresaItemList.getIdDaAcaoDetalhes();
			if(classeDetalhes != null && idDetalhes != null){
				buttonVer.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						
						Intent intent = new Intent(
								context.getApplicationContext(), 
								classeDetalhes);
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, idDetalhes);
						context.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
						
					}
				});
			} else 
				buttonVer.setVisibility(View.GONE);
		}
		
		return layoutView;
	}
	
}

