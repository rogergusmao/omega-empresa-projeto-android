package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.EmpresaItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaPerfil;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPerfil;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoEmpresa;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class EmpresaAdapter extends DatabaseCustomAdapter{

	public String nomeEmpresa; 
	String complemento;
	String logradouro;
	String numero;
	String estadoId;
	public String cidadeId;
	public String bairroId;
	public String tipoEmpresaId;
	String email;
	public String setorId; 
	Boolean isEmpresaDaCorporacao;
	Boolean isParceiro;
	Boolean isFornecedor;
	Boolean isCliente;

	public EmpresaAdapter(
			Activity pActivity,
			Boolean pIsEmpresaDaCorporacao,
			Boolean pIsParceiro,
			Boolean pIsFornecedor,
			Boolean pIsCliente,
			String name, 
			String pLogradouro,
			String pNumero,
			String pComplemento,
			String pBairroId,
			String pCidadeId,
			String pEstadoId,
			String tipoEmpresaId,
			String pEmail,
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(
				pActivity, 
				p_isAsc, 
				EXTDAOEmpresa.NAME, 
				false, 
				EmpresaItemList.NOME ,
				FormEmpresaActivityMobile.class,
				FilterEmpresaActivityMobile.class);
		this.nomeEmpresa = name; 
		email = pEmail;
		isEmpresaDaCorporacao = pIsEmpresaDaCorporacao;
		isParceiro = pIsParceiro;
		isFornecedor = pIsFornecedor;
		isCliente = pIsCliente;
		
		numero =pNumero;
		logradouro =pLogradouro;
		complemento = pComplemento;
		estadoId =pEstadoId;
		cidadeId =pCidadeId; 
		bairroId = pBairroId;
		this.tipoEmpresaId = tipoEmpresaId;
		super.initalizeListCustomItemList();
	}

	@Override
	public ArrayList<CustomItemList> filtraToken(CharSequence cs){
		if(listaCompleta == null) return null;
		ArrayList<CustomItemList> resultado = new ArrayList<CustomItemList>();
		String csMaiusculo = cs.toString().toUpperCase();
		String csMinusculo= cs.toString().toLowerCase();
		for(int i = 0 ; i < listaCompleta.size(); i++){
			EmpresaItemList itemList = (EmpresaItemList)listaCompleta.get(i);
			
			String nome = itemList.getConteudoContainerItem(EmpresaItemList.NOME);
			if(nome != null && nome.contains(csMaiusculo)){
				resultado.add(itemList);
				continue;
			}
			
			String email = itemList.getConteudoContainerItem(EmpresaItemList.EMAIL);
			if(email != null && email.contains(csMinusculo)){
				resultado.add(itemList);
				continue;
			}
			
			String telefone = itemList.getConteudoContainerItem(EmpresaItemList.TELEFONE);
			if(telefone != null && telefone.contains(csMaiusculo)){
				resultado.add(itemList);
				continue;
			}
			
			String celular = itemList.getConteudoContainerItem(EmpresaItemList.CELULAR);
			if(celular != null && celular.contains(csMaiusculo)){
				resultado.add(itemList);
				continue;
			}
		}
		
		return resultado;
	}

	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			EmpresaItemList vItemList = (EmpresaItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.item_list_layout, null);

			PersonalizaItemView(vLayoutView, pPosition);


			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_1, 
					HelperString.substring(vItemList.getConteudoContainerItem(EmpresaItemList.NOME), 25), 
					vLayoutView);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_2, 
					HelperString.substring(vItemList.getConteudoContainerItem(EmpresaItemList.EMAIL), 25), 
					vLayoutView);
			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_5, 
					HelperString.substring(vItemList.getConteudoContainerItem(EmpresaItemList.TIPO_EMPRESA), 25), 
					vLayoutView);
			
			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_3, 
					vItemList.getConteudoContainerItem(EmpresaItemList.TELEFONE), 
					vLayoutView);
			
			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_4, 
					vItemList.getConteudoContainerItem(EmpresaItemList.CELULAR), 
					vLayoutView);
			
			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
						activity, 
						EXTDAOEmpresa.NAME,
						pParent, 
						vItemList.getId(),  
						FormEditActivityMobile.class, 
						FormEmpresaActivityMobile.class, 
						isEditPermitido(), 
						isRemovePermitido(), vItemList);
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_EMPRESA, 
						vClickListener);
			}
			else {
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_EMPRESA);
			}
			return vLayoutView;
		} catch (Exception exc) {
			SingletonLog.insereErro(exc, TIPO.ADAPTADOR);
		}
		return null;
	}



	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {

		
		DatabasePontoEletronico database = null;

		try {
			database = new DatabasePontoEletronico(this.activity);
			String querySelectIdPrestadores = "SELECT DISTINCT e."
					+ EXTDAOEmpresa.ID + " ";

			String queryFromTables =  "FROM " + EXTDAOEmpresa.NAME + " e ";

			String queryWhereClausule = ""; 
			ArrayList<String> args = new ArrayList<String>();

			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " e." + EXTDAOEmpresa.CORPORACAO_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND e." + EXTDAOEmpresa.CORPORACAO_ID_INT + " = ? " ;

			args.add(OmegaSecurity.getIdCorporacao());
			
			if(this.logradouro != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " e." + Attribute.getNomeAtributoNormalizado(EXTDAOEmpresa.LOGRADOURO) + " LIKE ? " ;
				else 
					queryWhereClausule += " AND e." + Attribute.getNomeAtributoNormalizado(EXTDAOEmpresa.LOGRADOURO) + " LIKE ? " ;

				args.add("%" + HelperString.getWordWithoutAccent(this.logradouro) + "%");
			}

			if(this.numero != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " e." + EXTDAOEmpresa.NUMERO + " LIKE ? " ;
				else 
					queryWhereClausule += " AND e." + EXTDAOEmpresa.NUMERO + " LIKE ? " ;

				args.add("%" + this.numero + "%");
			}

			if(this.complemento != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " e." + Attribute.getNomeAtributoNormalizado(EXTDAOEmpresa.COMPLEMENTO) + " LIKE ? " ;
				else 
					queryWhereClausule += " AND e." +  Attribute.getNomeAtributoNormalizado(EXTDAOEmpresa.COMPLEMENTO)  + " LIKE ? " ;

				args.add("%" +HelperString.getWordWithoutAccent( this.complemento) + "%");
			}

			if(this.bairroId != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " e." + EXTDAOEmpresa.BAIRRO_ID_INT + " = ? " ;
				else 
					queryWhereClausule += " AND e." + EXTDAOEmpresa.BAIRRO_ID_INT + " = ? " ;

				args.add(this.bairroId);
			}
			//Se o bairro tiver sido setado entao nao ha a necessidade de setar a cidade
			if( this.cidadeId != null && this.bairroId == null){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " e." + EXTDAOEmpresa.CIDADE_ID_INT + " = ? " ;
				else 
					queryWhereClausule += " AND e." + EXTDAOEmpresa.CIDADE_ID_INT + " = ? " ;

				args.add(this.cidadeId);
			}


			if(this.nomeEmpresa != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " e." + Attribute.getNomeAtributoNormalizado(EXTDAOEmpresa.NOME) + " LIKE ?" ;
				else 
					queryWhereClausule += " AND e." + Attribute.getNomeAtributoNormalizado(EXTDAOEmpresa.NOME) + " LIKE ?" ;

				args.add('%' + HelperString.getWordWithoutAccent(this.nomeEmpresa) + '%' );
			}
			
			if(this.email != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " e." + EXTDAOEmpresa.EMAIL + " LIKE ? " ;
				else 
					queryWhereClausule += " AND e." + EXTDAOEmpresa.EMAIL + " LIKE ? " ;

				args.add('%' + this.email + '%' );
			}

			if(tipoEmpresaId != null){
				queryFromTables +=  ", " + EXTDAOEmpresa.NAME + " fe ";
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " e." + EXTDAOEmpresa.ID + "= ?";
				else 
					queryWhereClausule += " AND e." + EXTDAOEmpresa.ID + "= ?";
				if(queryWhereClausule.length() >  0)
					queryWhereClausule += " AND ";
				queryWhereClausule += " fe." + EXTDAOEmpresa.TIPO_EMPRESA_ID_INT + " = ?" ;
				args.add(tipoEmpresaId);
			}

			
			boolean vIsEmpresaPerfilInFromClause = false;
			if(isEmpresaDaCorporacao || isParceiro || isCliente || isFornecedor){
				
				if(!vIsEmpresaPerfilInFromClause){
					queryFromTables +=  ", " + EXTDAOEmpresaPerfil.NAME + " ep ";
					
					if(queryWhereClausule.length() ==  0)
						queryWhereClausule += " ep." + EXTDAOEmpresaPerfil.EMPRESA_ID_INT + " = e." + EXTDAOEmpresa.ID;
					else 
						queryWhereClausule += " AND ep." + EXTDAOEmpresaPerfil.EMPRESA_ID_INT + " = e." + EXTDAOEmpresa.ID;
					
					vIsEmpresaPerfilInFromClause = true;
				}
				
				String queryWherePerfil = "";
				Boolean vVetorIsPerfilSelected[] = new Boolean[]{
						isEmpresaDaCorporacao,
						isParceiro, 
						isCliente, 
						isFornecedor};
				String vVetorIdPerfil[] = new String[]{
						EXTDAOPerfil.ID_STR_EMPRESA_CORPORACAO,
						EXTDAOPerfil.ID_STR_PARCEIRO, 
						EXTDAOPerfil.ID_STR_CLIENTE, 
						EXTDAOPerfil.ID_STR_FORNECEDOR};
				for(int i = 0 ; i < vVetorIsPerfilSelected.length; i++){
					Boolean vIsPerfilSelected = vVetorIsPerfilSelected[i];
					if(vIsPerfilSelected != null && vIsPerfilSelected){
						String vIdPerfil = vVetorIdPerfil[i];
						if(queryWherePerfil.length() > 0 )
							queryWherePerfil += " OR ep." + EXTDAOEmpresaPerfil.PERFIL_ID_INT + " = ?" ;
						else queryWherePerfil += " ep." + EXTDAOEmpresaPerfil.PERFIL_ID_INT + " = ?" ;
						
						args.add(vIdPerfil);
					}
				}
				if(queryWhereClausule.length() >  0)
					queryWhereClausule += " AND ";
				queryWhereClausule += " (" + queryWherePerfil + " ) " ;
			}
			
			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY e." + Attribute.getNomeAtributoNormalizado(EXTDAOEmpresa.NOME)
						+ " DESC";
			} else {
				orderBy += " ORDER BY  e." + Attribute.getNomeAtributoNormalizado(EXTDAOEmpresa.NOME)
						+ " ASC";
			}

			String query = querySelectIdPrestadores + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

			if(orderBy.length() > 0 ){
				query += orderBy;  
			} 

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			
			Cursor cursor = null;
			try{
				cursor = database.rawQuery(query, vetorArg);
				// Cursor cursor = oh.query(true, EXTDAOEmpresa.NAME,
				// new String[]{EXTDAOEmpresa.ID_PRESTADOR}, "", new String[]{},
				// null, "", "", "");
				String[] vetorChave = new String[] { EXTDAOEmpresa.ID };
	
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}

		}catch (OmegaDatabaseException e) {
			SingletonLog.factoryToast(activity, e, SingletonLog.TIPO.ADAPTADOR);
		}
		catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.ADAPTADOR);
		}
		finally
		{
			if(database != null)
				database.close();
		}
		return null;
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {
try{
		EXTDAOEmpresa vEXTDAOEmpresa = (EXTDAOEmpresa) pObj;

		Attribute vAttrId = pObj.getAttribute(EXTDAOEmpresa.ID);
		String vId = null;

		if(vAttrId != null){
			vId = vAttrId.getStrValue();
		}
		if(vId == null){
			return null;
		}
		String vIdTipoEmpresa = pObj.getAttribute(EXTDAOEmpresa.TIPO_EMPRESA_ID_INT).getStrValue();
		String vNomeTipoEmpresa = null;
		if(vIdTipoEmpresa != null)
			if(vIdTipoEmpresa.length() > 0 ){
				EXTDAOTipoEmpresa vObjTE = new EXTDAOTipoEmpresa(vEXTDAOEmpresa.getDatabase());
				vObjTE.setAttrValue(EXTDAOTipoEmpresa.ID, vIdTipoEmpresa);
				vObjTE.select();
				vNomeTipoEmpresa = vObjTE.getStrValueOfAttribute(EXTDAOTipoEmpresa.NOME);
			}

		EmpresaItemList vItemList = new EmpresaItemList(  
				vEXTDAOEmpresa.getAttribute(EXTDAOEmpresa.ID).getStrValue(),
				vEXTDAOEmpresa.getAttribute(EXTDAOEmpresa.NOME).getStrValue(),
				vNomeTipoEmpresa,
				vEXTDAOEmpresa.getAttribute(EXTDAOEmpresa.EMAIL).getStrValue(),
				vEXTDAOEmpresa.getAttribute(EXTDAOEmpresa.TELEFONE1).getStrValue(),
				vEXTDAOEmpresa.getAttribute(EXTDAOEmpresa.CELULAR).getStrValue());

		return vItemList;
}catch(Exception ex){
	SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
return null;
}
	}
	
}

