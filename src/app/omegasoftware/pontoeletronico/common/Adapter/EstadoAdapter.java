package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEstadoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.EstadoItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class EstadoAdapter extends DatabaseCustomAdapter{
	
//	private static final int ID_TEXT_VIEW_ESTADO = R.id.estado_textview;
//	private static final int ID_TEXT_VIEW_PAIS = R.id.pais_textview;
//	private static final int ID_TEXT_VIEW_SIGLA = R.id.sigla_textview;
	
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;
	public static String TAG = "EstadoAdapter";

	public String nomeEstado;
	public String nomePais;

	public EstadoAdapter(
			Activity pActivity,
			String nomeEstado,
			String nomePais,
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, p_isAsc, EXTDAOUf.NAME, EstadoItemList.ESTADO, false);
		this.nomeEstado = nomeEstado; 
		this.nomePais = nomePais; 
		super.initalizeListCustomItemList();
	}

	
	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			EstadoItemList vItemList = (EstadoItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.item_list_layout, null);
			
			
			PersonalizaItemView(vLayoutView, pPosition);
			

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_1, 
					vItemList.getConteudoContainerItem(EstadoItemList.ESTADO), 
					vLayoutView);
			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_2, 
					vItemList.getConteudoContainerItem(EstadoItemList.PAIS), 
					vLayoutView);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_3, 
					vItemList.getConteudoContainerItem(EstadoItemList.SIGLA), 
					vLayoutView);

			CreateDetailButton(vLayoutView, 
					ID_CUSTOM_BUTTON_DETAIL, 
					vItemList.getId(), 
					OmegaConfiguration.SEARCH_MODE_ESTADO);
			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
						activity, 
						EXTDAOUf.NAME,
						pParent, 
						vItemList.getId(),
						FormEditActivityMobile.class, 
						FormEstadoActivityMobile.class,
						isEditPermitido(),
						isRemovePermitido(), vItemList);
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_ESTADO, 
						vClickListener);
			}
			else {
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_BAIRRO);
			}
			return vLayoutView;
		} catch (Exception exc) {
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
					"EstadoAdapter: getView()");
		}
		return null;
	}



	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {

		
		DatabasePontoEletronico database=null;
		try {
			database = new DatabasePontoEletronico(this.activity);
		} catch (OmegaDatabaseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			
			String querySelectIdPrestadores = "SELECT DISTINCT uf."
					+ EXTDAOUf.ID + " ";

			String queryFromTables =  "FROM " + EXTDAOUf.NAME + " uf ";

			String queryWhereClausule = ""; 
			ArrayList<String> args = new ArrayList<String>();

			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " uf." + EXTDAOUf.CORPORACAO_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND uf." + EXTDAOUf.CORPORACAO_ID_INT + " = ? " ;
			
			args.add(OmegaSecurity.getIdCorporacao());

			if(this.nomeEstado != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " uf." + Attribute.getNomeAtributoNormalizado(EXTDAOUf.NOME) + " LIKE ?" ;
				else 
					queryWhereClausule += " AND uf." + Attribute.getNomeAtributoNormalizado(EXTDAOUf.NOME) + " LIKE ?" ;

				args.add('%' + HelperString.getWordWithoutAccent(this.nomeEstado) + '%' );
			}

			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY uf." + Attribute.getNomeAtributoNormalizado(EXTDAOUf.NOME)
						+ " DESC";
			} else {
				orderBy += " ORDER BY  uf." + Attribute.getNomeAtributoNormalizado(EXTDAOUf.NOME)
						+ " ASC";
			}

			String query = querySelectIdPrestadores + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

			if(orderBy.length() > 0 ){
				query += orderBy;  
			} 

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			
			Cursor cursor = null;
			try{
				cursor = database.rawQuery(query, vetorArg);
				// Cursor cursor = oh.query(true, EXTDAOEmpresa.NAME,
				// new String[]{EXTDAOEmpresa.ID_PRESTADOR}, "", new String[]{},
				// null, "", "", "");
				String[] vetorChave = new String[] { EXTDAOUf.ID };
	
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}

		} catch (Exception e) {
			
			e.printStackTrace();
			Log.e(TAG, e.getMessage());
		}
		finally
		{
			database.close();
		}
		return null;
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {
try{
		EXTDAOUf vEXTDAOUf = (EXTDAOUf) pObj;
		
	
		EstadoItemList vItemList = new EstadoItemList(  
				vEXTDAOUf.getAttribute(EXTDAOUf.ID).getStrValue(),
				vEXTDAOUf.getNomeDaChaveExtrangeira(EXTDAOUf.PAIS_ID_INT),
				vEXTDAOUf.getAttribute(EXTDAOUf.NOME).getStrValue(), 
				vEXTDAOUf.getAttribute(EXTDAOUf.SIGLA).getStrValue());

		return vItemList;
}catch(Exception ex){
	SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
return null;
}
	}
}

