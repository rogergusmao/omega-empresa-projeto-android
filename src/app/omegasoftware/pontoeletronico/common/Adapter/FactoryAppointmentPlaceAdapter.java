package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.common.ItemList.AppointmentPlaceItemList;

public class FactoryAppointmentPlaceAdapter extends BaseAdapter {

	private ArrayList<AppointmentPlaceItemList> appointmentPlaces = new  ArrayList<AppointmentPlaceItemList>(); 
	private Activity activity;
	private Typeface customTypeFace;
	Integer typeFactoryClickListener = null;
	
	OnClickListener clickListener = null;

	public FactoryAppointmentPlaceAdapter(Activity pActivity, Integer pTypeFactoryClickListener)
	{
		this(pActivity,new ArrayList<AppointmentPlaceItemList>(), pTypeFactoryClickListener);
	}
	
	

	public FactoryAppointmentPlaceAdapter(
			Activity pActivity, 
			AppointmentPlaceItemList appointmentPlaces, 
			int pTypeFactoryClickListener)
	{
		this.appointmentPlaces.add(appointmentPlaces);
		this.activity = pActivity;
		this.customTypeFace = Typeface.createFromAsset(this.activity.getAssets(),"trebucbd.ttf");
		typeFactoryClickListener = pTypeFactoryClickListener;
	}

	public FactoryAppointmentPlaceAdapter(
			Activity pActivity, 
			AppointmentPlaceItemList appointmentPlaces,
			OnClickListener pClickListener)
	{
		this.appointmentPlaces.add(appointmentPlaces);
		this.activity = pActivity;
		this.customTypeFace = Typeface.createFromAsset(this.activity.getAssets(),"trebucbd.ttf");
		clickListener = pClickListener;
	}
	
	public FactoryAppointmentPlaceAdapter(
			Activity pActivity, 
			ArrayList<AppointmentPlaceItemList> appointmentPlaces,
			Integer pTypeFactoryClickListener)
	{
		this.appointmentPlaces = appointmentPlaces;
		this.activity = pActivity;
		this.customTypeFace = Typeface.createFromAsset(this.activity.getAssets(),"trebucbd.ttf");
		typeFactoryClickListener = pTypeFactoryClickListener;
	}

	public int getCount() {
		return this.appointmentPlaces.size();
	}

	public AppointmentPlaceItemList getItem(int position) {
		return this.appointmentPlaces.get(position);
	}

	public long getItemId(int position) {
		return this.appointmentPlaces.get(position).getId();
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		AppointmentPlaceItemList appointmentPlace = this.getItem(position);
		LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View layoutView = layoutInflater.inflate(R.layout.appointment_place_item_list_layout, null);

		Button buttonShowOnMap = (Button) layoutView.findViewById(R.id.appointment_place_showmap_button);
//		if(typeFactoryClickListener != null)
//			buttonShowOnMap.setOnClickListener(FactoryMapClickListener.factory(typeFactoryClickListener, this.getItemId(position), activity) );
//		else 
			buttonShowOnMap.setOnClickListener(clickListener);
		//		TextView titleTextView = (TextView) layoutView.findViewById(R.id.appointment_place_title);
		//		titleTextView.setText(String.format(context.getResources().getString(R.string.appointent_place_title), position + 1));
		//		
		TextView addressTextView = (TextView) layoutView.findViewById(R.id.appointment_place_address);
		addressTextView.setText(appointmentPlace.getAddress());
		addressTextView.setTypeface(this.customTypeFace);

		LinearLayout emailAndPhoneHolder = (LinearLayout) layoutView.findViewById(R.id.email_and_phone_holder_linearlayout);
		LinearLayout phonesLayout = new LinearLayout(this.activity);

		if(appointmentPlace.hasPhone()){

			PhoneEmailAdapter phoneAdapter = appointmentPlace.getPhoneAdapter();


			phonesLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			phonesLayout.setOrientation(LinearLayout.HORIZONTAL);

			for(int i=0;i<phoneAdapter.getCount();i++)
			{
				if((i % 2 == 0) && i != 0)
				{
					emailAndPhoneHolder.addView(phonesLayout);
					phonesLayout = new LinearLayout(this.activity);
					phonesLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
					phonesLayout.setOrientation(LinearLayout.HORIZONTAL);
				}

				phonesLayout.addView(phoneAdapter.getView(i, null, null));

			}
		}

		if(appointmentPlace.hasEmail()){
			PhoneEmailAdapter emailAdapter = appointmentPlace.getEmailAdapter();

			emailAndPhoneHolder.addView(phonesLayout);

			for(int i=0;i<emailAdapter.getCount();i++)
			{
				emailAndPhoneHolder.addView(emailAdapter.getView(i, null, null));
			}
		}
		return layoutView;

	}
}
