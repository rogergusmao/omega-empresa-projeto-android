package app.omegasoftware.pontoeletronico.common.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterEditEnderecoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutEndereco;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.ItemList.AppointmentPlaceItemList;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPais;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;
import app.omegasoftware.pontoeletronico.listener.MapButtonClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class FilterEnderecoAdapter extends BaseAdapter {

	
	private Activity activity;
	ContainerLayoutEndereco container = null;
	Button buttonAddEndereco = null;
	
	public FilterEnderecoAdapter(Activity pActivity, 
			ContainerLayoutEndereco pContainer)
	{
		
		this.activity = pActivity;
		this.container = pContainer;
	}
	
	public FilterEnderecoAdapter(Activity pActivity, 
			ContainerLayoutEndereco pContainer,
			Button pButtonEndereco)
	{
		
		this.activity = pActivity;
		this.container = pContainer;
		this.buttonAddEndereco = pButtonEndereco;
	}
	
	public int getCount() {
		AppointmentPlaceItemList vAppointment= container.getAppointmentPlaceItemList();
		if(vAppointment != null ) return 1;
		else return 0;
	}

	public AppointmentPlaceItemList getItem(int position) {
		AppointmentPlaceItemList vAppointment= container.getAppointmentPlaceItemList();
		return vAppointment;
	}

	public long getItemId(int position) {
		AppointmentPlaceItemList vAppointment= container.getAppointmentPlaceItemList();
		return vAppointment.getId();
	}

	public class EditEnderecoOnLongClickListener implements OnLongClickListener{
		Activity activity;	
		protected int idLinearLayout;
		private String logradouro;
		private String numero;
		private String complemento;
		private String bairroId;
		private String cidadeId;
		
		public EditEnderecoOnLongClickListener(
				Activity pActivity, 
				int pIdLinearLayout,
				String pLogradouro, 
				String pNumero,
				String pComplemento,
				String pBairroId,
				String pCidadeId){
			logradouro = pLogradouro;
			numero = pNumero;
			complemento = pComplemento;
			bairroId = pBairroId;
			cidadeId = pCidadeId;
			
			activity = pActivity;
			idLinearLayout = pIdLinearLayout;
		}
		
		
		public boolean onLongClick(View arg0) {
			Intent intent;
			
			intent = new Intent(activity, FilterEditEnderecoActivityMobile.class);

			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, idLinearLayout);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO, logradouro);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO, numero);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO, complemento);
			
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO, bairroId);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE, cidadeId);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_EDIT, true);
			
			activity.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_ENDERECO);
//			context.startActivity(intent);
			
			return false;
		}
	}


	public View getView(int position, View convertView, ViewGroup parent) {
		try{
		DatabasePontoEletronico db = new DatabasePontoEletronico(activity);
		
		AppointmentPlaceItemList appointmentPlace = this.getItem(position);
		LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View layoutView = layoutInflater.inflate(R.layout.item_list_endereco_layout, null);
						
		Button buttonShowOnMap = (Button) layoutView.findViewById(R.id.appointment_place_showmap_button);
		buttonShowOnMap.setOnClickListener(new MapButtonClickListener(0, appointmentPlace));
		
		TextView addressTextView = (TextView) layoutView.findViewById(R.id.appointment_place_address);
		
		String vAddress = appointmentPlace.getStrAddressSimplificado(db);
		if(vAddress != null && vAddress.length() > 0 ){
			addressTextView.setText(HelperString.ucFirstForEachToken(vAddress));
		} else addressTextView.setVisibility(View.GONE);
		
		TextView complementoTextView = (TextView) layoutView.findViewById(R.id.appointment_place_complemento);
		String vComplemento = appointmentPlace.getComplemento();
		if(vComplemento != null && vComplemento.length() > 0 ){
			complementoTextView.setText(HelperString.ucFirstForEachToken(vComplemento));
		} else complementoTextView.setVisibility(View.GONE);
		
		
		String vNomeCidade = appointmentPlace.getCity();
		TextView cityTextView = (TextView) layoutView.findViewById(R.id.appointment_place_city);
		if(vNomeCidade != null && vNomeCidade.length() > 0 ){			
			cityTextView.setText(HelperString.ucFirstForEachToken(vNomeCidade));
		} else{
			String vIdCidade = appointmentPlace.getIdCity();
			
			if(vIdCidade != null){
				EXTDAOCidade vObjCidade = new EXTDAOCidade(db);
				
				vObjCidade.setAttrValue(EXTDAOCidade.ID, vIdCidade);
				vObjCidade.select();
				vNomeCidade = vObjCidade.getStrValueOfAttribute(EXTDAOCidade.NOME);
				if(vNomeCidade != null && vNomeCidade.length() > 0 )
					cityTextView.setText(HelperString.ucFirstForEachToken(vNomeCidade));
				else
					cityTextView.setVisibility(View.GONE);
			}else
				cityTextView.setVisibility(View.GONE);
		}
		
		String vNomeEstado = appointmentPlace.getUf();
		TextView stateTextView = (TextView) layoutView.findViewById(R.id.appointment_place_state);
		
		if(vNomeEstado != null && vNomeEstado.length() > 0 ){			
			stateTextView.setText(HelperString.ucFirstForEachToken(vNomeEstado));
		} else{
			String vIdUf = appointmentPlace.getIdUf();
			
			if(vIdUf != null){
				EXTDAOUf vObjUf = new EXTDAOUf(db);
				
				vObjUf.setAttrValue(EXTDAOUf.ID, vIdUf);
				vObjUf.select();
				vNomeEstado = vObjUf.getStrValueOfAttribute(EXTDAOUf.NOME);
				if(vNomeEstado != null && vNomeEstado.length() > 0 )
					stateTextView.setText(HelperString.ucFirstForEachToken(vNomeEstado));
				else
					stateTextView.setVisibility(View.GONE);
			}else
			stateTextView.setVisibility(View.GONE);
		}
		
		String vNomePais = appointmentPlace.getCountry();
		TextView countryTextView = (TextView) layoutView.findViewById(R.id.appointment_place_country);
		if(vNomePais != null && vNomePais.length() > 0 ){			
			countryTextView.setText(HelperString.ucFirstForEachToken(vNomePais));
		} else{
			String vIdPais = appointmentPlace.getIdCountry();
			
			if(vIdPais != null){
				EXTDAOPais vObjPais = new EXTDAOPais(db);
				
				vObjPais.setAttrValue(EXTDAOPais.ID, vIdPais);
				vObjPais.select();
				vNomePais = vObjPais.getStrValueOfAttribute(EXTDAOPais.NOME);
				if(vNomePais != null && vNomePais.length() > 0 )
					countryTextView.setText(HelperString.ucFirstForEachToken(vNomePais));
				else
					countryTextView.setVisibility(View.GONE);
			}else
				countryTextView.setVisibility(View.GONE);
		}
		TextView cepTextView = (TextView) layoutView.findViewById(R.id.appointment_place_cep);
		cepTextView.setText(appointmentPlace.getCEPWithStartingString());

//		Button closeButton = (Button) layoutView.findViewById(R.id.close_button);
		
		layoutView.setOnLongClickListener(
			new EditEnderecoOnLongClickListener(
				activity, 
				container.getIdLinearLayout(), 
				appointmentPlace.getAddress(),
				appointmentPlace.getNumber(),
				appointmentPlace.getComplemento(),
				appointmentPlace.getIdNeighborhood(),
				appointmentPlace.getIdCity()));
		
		if(buttonAddEndereco != null)
			buttonAddEndereco.setVisibility(View.GONE);
		
		db.close();
		return layoutView;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
return null;
		}
		
	}



	public class DeleteButtonListener implements OnClickListener
	{	
		ContainerLayoutEndereco container = null;
		Button buttonAddEndereco = null;
		public DeleteButtonListener(
				ContainerLayoutEndereco pContainer){
			
			container = pContainer;
		}
		
		public DeleteButtonListener(
				ContainerLayoutEndereco pContainer, 
				Button pButtonAddEndereco){
			
			container = pContainer;
			this.buttonAddEndereco = pButtonAddEndereco;
		}
		
		public void onClick(View v) {
			container.delete();
			if(this.buttonAddEndereco != null)
				this.buttonAddEndereco.setVisibility(View.VISIBLE);
		}
	}	
	
}
