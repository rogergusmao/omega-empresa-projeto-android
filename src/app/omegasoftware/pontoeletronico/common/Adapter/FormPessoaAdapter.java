package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutEndereco;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutPessoaEmpresa;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEnderecoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectPessoaEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.ItemList.AppointmentPlaceItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.EmpresaProfissaoItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.EmpresaProfissaoItemList.ACAO_DETALHES;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskLowerCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskUpperCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.activity.ContainerCadastro;
import app.omegasoftware.pontoeletronico.common.activity.HelperFonte;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.dialog.HelperDialog;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOBairro;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSexo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoDocumento;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.listener.MenuCelularOnClickListener;
import app.omegasoftware.pontoeletronico.listener.MenuTelefoneOnClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class FormPessoaAdapter {
	//Constants
	public static final String TAG = "FormPessoaActivityMobile";
	
	

	OnFocusChangeListener celularOnFocusChangeListener;
	//Search button

	private Button enderecoButton;
	private Button adicionarEmpresaButton;

	private CheckBox isClienteDiretoCheckBox;

	private DatabasePontoEletronico db=null;
	private AutoCompleteTextView tipoDocumentoAutoCompletEditText;
	private Button documentoButton;
	private MenuTelefoneOnClickListener telefoneOnClickListener;
	private MenuCelularOnClickListener celularOnClickListener;


	TableAutoCompleteTextView tipoDocumentoContainerAutoComplete = null;

	private EditText numeroDocumentoEditText;
	//EditText
	CustomSpinnerAdapter sexoCustomSpinnerAdapter ;

	public EditText nomeEditText;

	private Spinner sexoSpinner;

	private Button telefoneButton;
	private Button celularButton;

	public EditText emailEditText;

	Boolean isEmpresaDaCorporacao = null;
	ContainerLayoutPessoaEmpresa containerEmpresaProfissao ;
	String id;
	String idUsuario;
	ContainerLayoutEndereco containerEndereco = null;
	
	//ViewFocusHandler emailHandler;
	DocumentoHandler documentoHandler;
	private boolean desativaBotaoProfissoes = false;
	OmegaCadastroActivity activity;
	View view;
	public  FormPessoaAdapter(
			OmegaCadastroActivity activity,
			String id,
			View view) {
		
		Bundle vBundle = activity.getIntent().getExtras();
		if(vBundle != null){
			if(vBundle.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO)){
				isEmpresaDaCorporacao = vBundle.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO);
			}
			if(vBundle.containsKey(OmegaConfiguration.SEARCH_FIELD_DESATIVAR_BOTAO_PROFISSOES)){
				desativaBotaoProfissoes = vBundle.getBoolean(OmegaConfiguration.SEARCH_FIELD_DESATIVAR_BOTAO_PROFISSOES, false);
			}
		}
		this.view = view;
		this.activity = activity;
		containerEmpresaProfissao = new ContainerLayoutPessoaEmpresa(activity);
		this.id = id;
	}
	
	public void clearComponents(){

		//First we check the obligatory fields:
		if(containerEndereco != null){
			containerEndereco.delete();
			enderecoButton.setVisibility(View.VISIBLE);
		}
		
		documentoHandler.sendEmptyMessage(DocumentoHandler.CLEAR);
		
		telefoneOnClickListener.clear();
		
		celularOnClickListener.clear();
		
		containerEmpresaProfissao.clear();
		
		

	}
	public void loadEdit(){
		try{
			EXTDAOPessoa vObjPessoa = new EXTDAOPessoa(db);
			vObjPessoa.select(id);
			idUsuario = vObjPessoa.getIdUsuario();
			String vNomeFuncionario = vObjPessoa.getStrValueOfAttribute(EXTDAOPessoa.NOME);
			if(vNomeFuncionario != null){
				nomeEditText.setText(vNomeFuncionario);
				nomeEditText.clearFocus();
			}
			String vTelefone = vObjPessoa.getStrValueOfAttribute(EXTDAOPessoa.TELEFONE);
			if(vTelefone != null && vTelefone.length() > 0 ){
				telefoneButton.setText(vTelefone);
	//			telefoneButton.setTextSize(20);
			}
			String vCelular = vObjPessoa.getStrValueOfAttribute(EXTDAOPessoa.CELULAR);
			if(vCelular != null && vCelular.length() > 0){
				celularButton.setText(vCelular);
	//			celularButton.setTextSize(20);
			}
	
	
			String vEmail = vObjPessoa.getStrValueOfAttribute(EXTDAOPessoa.EMAIL);
			if(vEmail != null && vEmail.length() > 0){
				emailEditText.setText(vEmail);
				emailEditText.clearFocus();
			}
	
			String vSexo = vObjPessoa.getStrValueOfAttribute(EXTDAOPessoa.SEXO_ID_INT);
			if(vSexo != null){
				int vPosition = sexoCustomSpinnerAdapter.getPosition(vSexo);
				sexoSpinner.setSelection(vPosition);
			}
	
	
			String vTipoDocumento = vObjPessoa.getNomeDaChaveExtrangeira(EXTDAOPessoa.TIPO_DOCUMENTO_ID_INT);
	
			if(vTipoDocumento != null && vTipoDocumento.length() > 0 ){
				tipoDocumentoAutoCompletEditText.setText(vTipoDocumento);
				String vNumeroDocumento = vObjPessoa.getStrValueOfAttribute(EXTDAOPessoa.NUMERO_DOCUMENTO);
				if(vNumeroDocumento != null && vNumeroDocumento.length() > 0){
					numeroDocumentoEditText.setText(vNumeroDocumento);
					numeroDocumentoEditText.clearFocus();
				}
			}
	
			String idCidade = vObjPessoa.getStrValueOfAttribute(EXTDAOPessoa.CIDADE_ID_INT);
			String idBairro = vObjPessoa.getStrValueOfAttribute(EXTDAOPessoa.BAIRRO_ID_INT);
	
			EXTDAOCidade objCidade = new EXTDAOCidade(db);
			objCidade.select(idCidade);
			
			String nomeCidade = objCidade.getStrValueOfAttribute(EXTDAOCidade.NOME);
			String idEstado = objCidade.getStrValueOfAttribute(EXTDAOCidade.UF_ID_INT);
			String nomeEstado = objCidade.getNomeEstado(idCidade);
	
			String momePais = null;
			String idPais = null;
			if(idEstado != null){
				EXTDAOUf vUf = new EXTDAOUf(db);
				vUf.setAttrValue(EXTDAOUf.ID, idEstado);
				vUf.select();
				momePais = vUf.getNomeDaChaveExtrangeira(EXTDAOUf.PAIS_ID_INT);
				idPais = vUf.getStrValueOfAttribute(EXTDAOUf.PAIS_ID_INT);
			}
			String nomeBairro = null;
			if(idBairro != null){
				EXTDAOBairro vObjBairro = new EXTDAOBairro(db);
				vObjBairro.select(idBairro);
				nomeBairro = vObjBairro.getStrValueOfAttribute(EXTDAOBairro.NOME);	
			}
			
	
			String vLogradouro = vObjPessoa.getStrValueOfAttribute(EXTDAOPessoa.LOGRADOURO);
			String vNumero = vObjPessoa.getStrValueOfAttribute(EXTDAOPessoa.NUMERO);
			String vComplemento = vObjPessoa.getStrValueOfAttribute(EXTDAOPessoa.COMPLEMENTO);
			LinearLayout vLinearLayoutEndereco = (LinearLayout) view. findViewById(R.id.linearlayout_endereco);
			if(idBairro != null || idCidade != null || idEstado != null || idPais != null){
				
				containerEndereco = addLayoutEndereco(
						idBairro, 
						idCidade, 
						idEstado,
						idPais,
						nomeBairro, 
						nomeCidade, 
						nomeEstado,
						momePais, 
						vLogradouro, 
						vNumero, 
						vComplemento, 
						vLinearLayoutEndereco, 
						this.enderecoButton);
	
				activity.showDialog(OmegaConfiguration.FORM_DIALOG_INFORMACAO_DIALOG_ENDERECO);
			}
	
			EXTDAOPessoaEmpresa objPessoaEmpresa = new EXTDAOPessoaEmpresa(db);
			objPessoaEmpresa.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, id );
			objPessoaEmpresa.setAttrValue(EXTDAOPessoaEmpresa.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			ArrayList<Table> list = objPessoaEmpresa.getListTable();
			if(list != null && list.size() > 0){
				for (Table obj : list) {
	
					String vIdEmpresa = obj.getStrValueOfAttribute(EXTDAOPessoaEmpresa.EMPRESA_ID_INT);
					EXTDAOEmpresa vObjEmpresa = new EXTDAOEmpresa(db);
					vObjEmpresa.setAttrValue(EXTDAOEmpresa.ID, vIdEmpresa);
					if(vObjEmpresa.select()){
						String vNomeEmpresa = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.NOME);
						String vIdProfissao = obj.getStrValueOfAttribute(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT);
	
						EXTDAOProfissao vObjProfissao = new EXTDAOProfissao(db);
						vObjProfissao.setAttrValue(EXTDAOProfissao.ID, vIdProfissao);
						if(vObjProfissao.select()){
							String vNomeProfissao = vObjProfissao.getStrValueOfAttribute(EXTDAOEmpresa.NOME);
	
							addOrUpdateLayoutEmpresaPessoa(obj.getId(), vIdEmpresa, vNomeEmpresa, vNomeProfissao);
						}
					}
				}
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
		
		}
	}

	public void inicializa() throws OmegaDatabaseException {

		this.db = new DatabasePontoEletronico(activity);
//		this.identificadorFuncionarioEditText = (EditText) findViewById(R.id.identificador_funcionario_edittext);
//		new MaskUpperCaseTextWatcher(this.identificadorFuncionarioEditText, 30);

		EXTDAOTipoDocumento vObjTipoDocumento = new EXTDAOTipoDocumento(this.db);

		this.tipoDocumentoAutoCompletEditText = (AutoCompleteTextView)
				view.findViewById(R.id.tipo_documento_autocompletetextview);
		this.tipoDocumentoContainerAutoComplete = new TableAutoCompleteTextView(activity, vObjTipoDocumento, tipoDocumentoAutoCompletEditText);

		new MaskUpperCaseTextWatcher(tipoDocumentoAutoCompletEditText, 100);
		isClienteDiretoCheckBox = (CheckBox) view.findViewById(R.id.is_consumidor_checkbox);
		isClienteDiretoCheckBox.setVisibility(View.GONE);
		numeroDocumentoEditText = (EditText) view.findViewById(R.id.numero_documento_edittext);
		new MaskUpperCaseTextWatcher(this.numeroDocumentoEditText, 30);

		this.numeroDocumentoEditText.setOnFocusChangeListener( new NumeroDocumentoOnFocusChangeListener());
		this.numeroDocumentoEditText.setOnClickListener(new NumeroDocumentoOnClickListener());
		this.tipoDocumentoAutoCompletEditText.setOnFocusChangeListener(new TipoDocumentoOnFocusChangeListener());
		this.tipoDocumentoAutoCompletEditText.setVisibility(View.GONE);
		this.numeroDocumentoEditText.setVisibility(View.GONE);
		this.documentoButton = (Button) view.findViewById(R.id.documento_button);
		this.documentoButton.setVisibility(View.VISIBLE);
		this.documentoHandler = new DocumentoHandler();
		this.documentoButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				documentoHandler.sendEmptyMessage(DocumentoHandler.VISIBLE);
				
			}
		});
		this.nomeEditText = (EditText) view.findViewById(R.id.nome_edittext);
		new MaskUpperCaseTextWatcher(this.nomeEditText, 255);
		this.nomeEditText.clearFocus();
		this.sexoSpinner = (Spinner) view.findViewById(R.id.sexo_spinner);
		this.sexoSpinner.setVisibility(View.GONE);
		sexoCustomSpinnerAdapter = new CustomSpinnerAdapter(activity, this.getAllSexo(), R.layout.spinner_item,R.string.sexo);
		this.sexoSpinner.setAdapter(sexoCustomSpinnerAdapter);
		((CustomSpinnerAdapter) this.sexoSpinner.getAdapter()).setDropDownLayout(R.layout.spinner_item_selected);
		this.sexoSpinner.setVisibility(View.GONE);
		
		this.telefoneButton = (Button) view.findViewById(R.id.telefone_button);
		telefoneOnClickListener = new MenuTelefoneOnClickListener(activity, telefoneButton, OmegaConfiguration.ACTIVITY_FORM_TECLADO_TELEFONE);
		telefoneButton.setOnClickListener(telefoneOnClickListener);

//		this.celularSMSButton = (Button) findViewById(R.id.celular_sms_button);
//		celularSMSOnClickListener = new MenuTelefoneOnClickListener(this, celularSMSButton, OmegaConfiguration.ACTIVITY_FORM_TECLADO_CELULAR_SMS);
//		celularSMSButton.setOnClickListener(celularSMSOnClickListener);

		this.celularButton = (Button) view.findViewById(R.id.celular_button);
		celularOnClickListener = new MenuCelularOnClickListener(activity, celularButton, OmegaConfiguration.ACTIVITY_FORM_TECLADO_CELULAR, null);
		celularButton.setOnClickListener(celularOnClickListener);  


		this.emailEditText = (EditText) view.findViewById(R.id.email_edittext);
		new MaskLowerCaseTextWatcher(this.emailEditText, 255);
//		emailHandler = new ViewFocusHandler(activity, emailEditText);

		
		this.enderecoButton = (Button) view.findViewById(R.id.endereco_button);
		this.enderecoButton.setOnClickListener(new CadastrarEnderecoListener());
		
		adicionarEmpresaButton = (Button) view.findViewById(R.id.cadastro_funcionario_empresa_button);
		if(desativaBotaoProfissoes){
			this.adicionarEmpresaButton.setVisibility(View.GONE);
		} else {
			this.adicionarEmpresaButton.setOnClickListener(new CadastrarFuncionarioEmpresaListener());	
		}
		
	}

	public ContainerLayoutEndereco addLayoutEndereco(
			String pIdBairro ,
			String pIdCidade ,
			String pIdEstado,
			String pIdPais,
			String pNomeBairro ,
			String pNomeCidade ,
			String pNomeEstado,
			String pNomePais,
			String pLogradouro ,
			String pNumero ,
			String pComplemento, 
			LinearLayout pLinearLayoutEndereco, 
			Button pAddButtonEndereco){

		

		AppointmentPlaceItemList appointmentPlaceItemList = new AppointmentPlaceItemList(
				activity.getApplicationContext(), 
				0,
				pLogradouro ,
				pNumero ,
				pComplemento,
				pNomeBairro, 
				pNomeCidade,
				pNomeEstado,
				pNomePais,
				pIdBairro ,
				pIdCidade ,
				pIdEstado,
				pIdPais,
				null,
				null,
				null);
		//		LinearLayout linearLayoutEndereco = (LinearLayout) findViewById(R.id.linearlayout_endereco);

		ContainerLayoutEndereco vContainerEndereco = new ContainerLayoutEndereco(
				activity, 
				appointmentPlaceItemList,
				pLinearLayoutEndereco);

		FormEnderecoAdapter adapter = new FormEnderecoAdapter(
				activity, 
				vContainerEndereco, 
				pAddButtonEndereco);

		View vNewView = adapter.getView(0, null, null);
		
		HelperFonte.setFontAllView(vNewView);
		
		
		pLinearLayoutEndereco.setVisibility(View.VISIBLE);
		pLinearLayoutEndereco.addView(vNewView);

		vContainerEndereco.setAppointmentPlace(vNewView);

		return vContainerEndereco;
	}

	public void addOrUpdateLayoutEmpresaPessoa(
			final String pIdPessoaEmpresa,
			final String pIdEmpresa,
			final String pNomeEmpresa, 
			final String pNomeProfissao){
		if(pIdEmpresa == null || pNomeProfissao == null){
			return;
		} else if(pIdEmpresa.length() == 0 || pNomeProfissao.length() == 0){
			return;	
		}

		if(pIdPessoaEmpresa != null && containerEmpresaProfissao.containsRegistro(pIdPessoaEmpresa)){
			View view = containerEmpresaProfissao.getViewDoIdPessoaEmpresa(pIdPessoaEmpresa);
			TextView tvEmpresa = (TextView)view.findViewById(R.id.empresa_text_view);
			TextView tvProfissao =(TextView) view.findViewById(R.id.profissao_text_view);
			
			tvEmpresa.setText(pNomeEmpresa);
			tvProfissao.setText(pNomeProfissao);
			containerEmpresaProfissao.updateRegistro(pIdPessoaEmpresa, pIdEmpresa, pNomeProfissao);
		}else if(! containerEmpresaProfissao.containsTuple(pIdEmpresa, pNomeProfissao)){

			

			EmpresaProfissaoItemList empresaProfissaoItemList = new EmpresaProfissaoItemList(
					containerEmpresaProfissao.getUltimoIdEmpresa(), 
					idUsuario,
					id,
					pIdEmpresa,
					null,
					pIdPessoaEmpresa,
					pNomeEmpresa,
					pNomeProfissao,
					null,
					ACAO_DETALHES.SEM_ACAO);
			LinearLayout linearLayoutEmpresaFuncionario = (LinearLayout) view.findViewById(R.id.linearlayout_empresa_funcionario);

			ArrayList<EmpresaProfissaoItemList> itemLists = new ArrayList<EmpresaProfissaoItemList>();
			itemLists.add(empresaProfissaoItemList);

			FormPessoaEmpresaAdapter adapter = new FormPessoaEmpresaAdapter(
					activity,
					itemLists, 
					containerEmpresaProfissao);
			View vNewView = adapter.getView(0, null, null);
			HelperFonte.setFontAllView(vNewView);
			
			
			containerEmpresaProfissao.add(pIdPessoaEmpresa, pIdEmpresa, pNomeProfissao, vNewView);
			
			vNewView.setOnLongClickListener(new OnLongClickListener() {
				
				@Override
				public boolean onLongClick(View v) {
					Intent intent = new Intent(FormPessoaAdapter.this.activity, SelectPessoaEmpresaActivityMobile.class);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, pIdPessoaEmpresa);
					FormPessoaAdapter.this.activity.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_EMPRESA_FUNCIONARIO);
					return true;
				}
			});
			linearLayoutEmpresaFuncionario.addView(vNewView);
			linearLayoutEmpresaFuncionario.setVisibility(View.VISIBLE);
		}
	}
	
	public boolean onActivityResult(int requestCode, int resultCode, Intent intent) {
		
		switch (requestCode) {
		case OmegaConfiguration.ACTIVITY_FORM_EMPRESA_FUNCIONARIO:
			try{
				String idPessoaEmpresa = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID);
				String idEmpresa = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA);
				String empresa = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_EMPRESA);
				String profissao = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PROFISSAO);

				addOrUpdateLayoutEmpresaPessoa(idPessoaEmpresa, idEmpresa, empresa, profissao);

			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.FORMULARIO);
			}
			return true;
			
		case OmegaConfiguration.ACTIVITY_FORM_TECLADO_TELEFONE:
			telefoneOnClickListener.onActivityResult(intent);
			return true;
		case OmegaConfiguration.ACTIVITY_FORM_TECLADO_CELULAR:
			celularOnClickListener.onActivityResult(intent);
			return true;
		case OmegaConfiguration.ACTIVITY_FORM_TECLADO_CELULAR_SMS:
//			celularSMSOnClickListener.onActivityResult(intent);
			return true;
		case OmegaConfiguration.ACTIVITY_FORM_ENDERECO:
			switch (resultCode) {
			case OmegaConfiguration.ACTIVITY_FORM_DELETE_LAYOUT_ENDERECO:
				int vIdLinearLayout = intent.getIntExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, -1);
				if(vIdLinearLayout == R.id.linearlayout_endereco){
					containerEndereco.delete();
					if(enderecoButton != null)
						enderecoButton.setVisibility(View.VISIBLE);
				} 
				return true;

			default:
				
				try{
					if(intent != null && intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE)){
						String vIdCidade = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE);
						String vIdBairro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO);
						String vIdEstado = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_ESTADO);
						String vIdPais = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID_PAIS);
						String vNomeCidade = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CIDADE);
						String vNomeBairro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_BAIRRO);
						String vNomeEstado = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_ESTADO);
						String vNomePais = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_PAIS);
						String vLogradouro = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO);
						String vNumero = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO);
						String vComplemento = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO);
						LinearLayout vLinearLayoutEndereco = (LinearLayout) view.findViewById(R.id.linearlayout_endereco);
						if(containerEndereco != null)
							containerEndereco.delete();
						containerEndereco = addLayoutEndereco(
								vIdBairro, 
								vIdCidade, 
								vIdEstado,
								vIdPais,
								vNomeBairro, 
								vNomeCidade, 
								vNomeEstado,
								vNomePais, 
								vLogradouro, 
								vNumero, 
								vComplemento,
								vLinearLayoutEndereco, 
								enderecoButton);
						activity.showDialog(OmegaConfiguration.FORM_DIALOG_INFORMACAO_DIALOG_ENDERECO);
					}
				}catch(Exception ex){
					SingletonLog.insereErro(ex, TIPO.FORMULARIO);
				}
			
			}
			return true;
		default:
			return false;
			
		}
	}


	private class CadastrarEnderecoListener implements OnClickListener
	{

		Intent intent = null;

		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.endereco_button:
				intent = new Intent(activity.getApplicationContext(), FormEnderecoActivityMobile.class);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_LAYOUT, R.id.linearlayout_endereco);
				break;

			default:
				break;
			}

			if(intent != null)
			{				
				try{

					activity.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_ENDERECO);
				}catch(Exception ex){
					//Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
				}
			}
		}
	}

	private class CadastrarFuncionarioEmpresaListener implements OnClickListener
	{

		Intent intent = null;

		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.cadastro_funcionario_empresa_button:
				intent = new Intent(activity.getApplicationContext(), SelectPessoaEmpresaActivityMobile.class);
				if(isEmpresaDaCorporacao != null)
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_EMPRESA_DA_CORPORACAO, isEmpresaDaCorporacao);
				break;

			default:
				break;
			}

			if(intent != null)
			{				
				try{

					activity.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_EMPRESA_FUNCIONARIO);
				}catch(Exception ex){
					SingletonLog.insereErro(ex, TIPO.ADAPTADOR);
					//Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
				}
			}
		}
	}

	protected LinkedHashMap<String, String> getAllSexo()
	{
		EXTDAOSexo vObj = new EXTDAOSexo(this.db);
		return vObj.getHashMapIdByDefinition(true);
	}




	public class DocumentoHandler extends Handler{
		public static final int VISIBLE = 1;
		public static final int CLEAR = 2;
		public DocumentoHandler(){
						
		}
		
		@Override
	    public void handleMessage(Message msg)
	    {
			switch(msg.what){
				
				case CLEAR:
					documentoButton.setVisibility(View.VISIBLE);
					tipoDocumentoAutoCompletEditText.setVisibility(View.GONE);
					tipoDocumentoAutoCompletEditText.setText("");
					numeroDocumentoEditText.setVisibility(View.GONE);
					numeroDocumentoEditText.setText("");
					break;
				default:
				case VISIBLE:
					documentoButton.setVisibility(View.GONE);
					tipoDocumentoAutoCompletEditText.setVisibility(View.VISIBLE);
					numeroDocumentoEditText.setVisibility(View.VISIBLE);
					tipoDocumentoAutoCompletEditText.requestFocus();
					break;
			}
			
	    }
	}
	
	
	
	public ContainerCadastro cadastrando(String inputEmail){
		try{	
			//				controlerProgressDialog.createProgressDialog();
			
			Table vObj = new EXTDAOPessoa(db);
			boolean vValidade = true;
			//First we check the obligatory fields:
			//Plano, Especialidade, Cidade
			//Checking Cidade
			if(id != null){
				
				vObj.select(id);
			}

			vObj.setAttrValue(EXTDAOPessoa.IS_CONSUMIDOR_BOOLEAN, HelperString.valueOfBooleanSQL(isClienteDiretoCheckBox.isChecked()));


			if(!nomeEditText.getText().toString().equals(""))
			{
				vObj.setAttrValue(EXTDAOPessoa.NOME, nomeEditText.getText().toString());
			} else {
				
				vObj.setAttrValue(EXTDAOPessoa.NOME, HelperString.formatarNomeDoEmail(emailEditText.getText().toString()));
			}
			
			if(inputEmail == null)
				inputEmail = emailEditText.getText().toString();
			
			if(!inputEmail.equals(""))
			{
				String token = inputEmail;
//				if(! HelperCheckField.checkEmail(token)){
//					if(validarEmail )
//						return activity.factoryContainerCadastro(HelperDialog.ERROR_DIALOG_INVALID_EMAIL, false);	
//				}
			
				if(id != null ){
					String emailAntigo = vObj.getStrValueOfAttribute(EXTDAOPessoa.EMAIL);
					if(emailAntigo != null && !emailAntigo.equalsIgnoreCase(token)){
						//se existir pessoa com esse email
						EXTDAOPessoa objPessoaAux = new EXTDAOPessoa(db);
						objPessoaAux.setAttrValue(EXTDAOPessoa.EMAIL, token);
						Table registro = objPessoaAux.getFirstTupla();
						if(registro != null){
							//essa pessoa ju existente seru associada
							EXTDAOPessoaUsuario objPessoaUsuario = new EXTDAOPessoaUsuario(db);
							objPessoaUsuario.setAttrValue(EXTDAOPessoaUsuario.PESSOA_ID_INT, registro.getStrValueOfAttribute(EXTDAOPessoaUsuario.PESSOA_ID_INT));
							
							Table registroUsuario = objPessoaUsuario.getFirstTupla();
							
							if(registroUsuario != null){
								return activity.factoryContainerCadastro(activity.ERROR_DIALOG_USUARIO_DUPLICADO, false);				
							}
						}	
					}	
				}
				
				vObj.setAttrValue(Table.EMAIL_UNIVERSAL, token);	
				
			} else {
//				emailHandler.sendEmptyMessage(0);
				return activity.factoryContainerCadastro(activity.ERROR_DIALOG_MISSING_EMAIL, false);
			}
			
			String vStrNomeTipoDocumento = tipoDocumentoAutoCompletEditText.getText().toString();
			if(vStrNomeTipoDocumento.length() > 0)
			{			

				String vStrNumeroDocumento = numeroDocumentoEditText.getText().toString();
				if(vStrNumeroDocumento.length() == 0)
				{
					return activity.factoryContainerCadastro(activity.FORM_ERROR_DIALOG_MISSING_NUMERO_DOCUMENTO, false);
				} else {
					vObj.setAttrValue(EXTDAOPessoa.NUMERO_DOCUMENTO, vStrNumeroDocumento);
				}
			} else {
				vObj.setAttrValue(EXTDAOPessoa.TIPO_DOCUMENTO_ID_INT, null);
				vObj.setAttrValue(EXTDAOPessoa.NUMERO_DOCUMENTO, null);
			}

			String selectedSex = String.valueOf(sexoSpinner.getSelectedItemId());
			if(!selectedSex.equals(OmegaConfiguration.UNEXISTENT_ID_IN_DB))
			{
				vObj.setAttrValue(Table.SEXO_ID_INT_UNIVERSAL, selectedSex);
			}  else {
				vObj.setAttrValue(EXTDAOPessoa.SEXO_ID_INT_UNIVERSAL, null);
			}

			if(telefoneOnClickListener.isNumeroSet())
				vObj.setAttrValue(Table.TELEFONE_UNIVERSAL, telefoneButton.getText().toString());
			else vObj.setAttrValue(EXTDAOPessoa.TELEFONE_UNIVERSAL, null);

//			if(celularSMSOnClickListener.isNumeroSet())
//				vObj.setAttrValue(Table.CELULAR_SMS_UNIVERSAL, celularSMSButton.getText().toString());
//			else vObj.setAttrValue(EXTDAOPessoa.CELULAR_SMS_UNIVERSAL, null);

			if(celularOnClickListener.isNumeroSet()){
				vObj.setAttrValue(Table.CELULAR_UNIVERSAL, celularButton.getText().toString());
			}
			else vObj.setAttrValue(EXTDAOPessoa.CELULAR_UNIVERSAL, null);

			

			if(containerEndereco != null && containerEndereco.getAppointmentPlaceItemList() != null){
				AppointmentPlaceItemList appointmentPlaceItemList = containerEndereco.getAppointmentPlaceItemList();

				if(appointmentPlaceItemList.getComplemento() != null && appointmentPlaceItemList.getComplemento().length() > 0 )
					vObj.setAttrValue(Table.COMPLEMENTO_UNIVERSAL, appointmentPlaceItemList.getComplemento());
				else vObj.setAttrValue(EXTDAOPessoa.COMPLEMENTO_UNIVERSAL, null);

				if(appointmentPlaceItemList.getNumber() != null && appointmentPlaceItemList.getNumber().length() > 0 )
					vObj.setAttrValue(Table.NUMERO_UNIVERSAL, appointmentPlaceItemList.getNumber());
				else vObj.setAttrValue(Table.NUMERO_UNIVERSAL, null);

				if(appointmentPlaceItemList.getIdCity() != null && appointmentPlaceItemList.getIdCity().length() > 0 )
					vObj.setAttrValue(Table.CIDADE_ID_INT_UNIVERSAL, appointmentPlaceItemList.getIdCity());
				else vObj.setAttrValue(Table.CIDADE_ID_INT_UNIVERSAL, null);

				if(appointmentPlaceItemList.getIdNeighborhood() != null && appointmentPlaceItemList.getIdNeighborhood().length() > 0 )
					vObj.setAttrValue(Table.BAIRRO_ID_INT_UNIVERSAL, appointmentPlaceItemList.getIdNeighborhood());
				else vObj.setAttrValue(Table.BAIRRO_ID_INT_UNIVERSAL, null);

				if (appointmentPlaceItemList.getAddress() != null && appointmentPlaceItemList.getAddress().length() > 0 )
					vObj.setAttrValue(Table.LOGRADOURO_UNIVERSAL, appointmentPlaceItemList.getAddress());
				else vObj.setAttrValue(Table.LOGRADOURO_UNIVERSAL, null);

			}else {
				vObj.setAttrValue(EXTDAOPessoa.COMPLEMENTO_UNIVERSAL, null);
				vObj.setAttrValue(EXTDAOPessoa.NUMERO_UNIVERSAL, null);
				vObj.setAttrValue(EXTDAOPessoa.CIDADE_ID_INT_UNIVERSAL, null);
				vObj.setAttrValue(EXTDAOPessoa.BAIRRO_ID_INT_UNIVERSAL, null);
				vObj.setAttrValue(EXTDAOPessoa.LOGRADOURO_UNIVERSAL, null);
			}

			vObj.setAttrValue(EXTDAOPessoa.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			
			
			if(vValidade){
				if(vStrNomeTipoDocumento.length() > 0 ){
					EXTDAOTipoDocumento vObjTipoDocumento = new EXTDAOTipoDocumento(db);
					vObjTipoDocumento.setAttrValue(EXTDAOTipoDocumento.NOME, vStrNomeTipoDocumento);
					vObjTipoDocumento.setAttrValue(EXTDAOTipoDocumento.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
					String vIdTipoDocumento = tipoDocumentoContainerAutoComplete.addTupleIfNecessay(vObjTipoDocumento);
					if(vIdTipoDocumento == null){
						vObj.setAttrValue(EXTDAOPessoa.TIPO_DOCUMENTO_ID_INT, null);
						vObj.setAttrValue(EXTDAOPessoa.NUMERO_DOCUMENTO, null);
						
						return new ContainerCadastro(new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR), false);
					} else {
						vObj.setAttrValue(EXTDAOPessoa.TIPO_DOCUMENTO_ID_INT, vIdTipoDocumento);
					}
				}
				if(id != null){
					vObj.setAttrValue(EXTDAOPessoa.ID, id);
					vObj.formatToSQLite();
					if(vObj.update(true)){
						activity.setBancoFoiModificado();
						containerEmpresaProfissao.addListTupleInUpdate(db, id);
						
						return activity.factoryContainerCadastro(activity.FORM_DIALOG_ATUALIZACAO_OK, true);
					} else{
						return activity.factoryContainerCadastro(activity.FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
						
					}
				}else {
					
					vObj.setLongAttrValue(EXTDAOPessoa.CADASTRO_SEC, HelperDate.getTimestampSegundosUTC(activity.getApplicationContext()));
					vObj.setIntegerAttrValue(EXTDAOPessoa.CADASTRO_OFFSEC, HelperDate.getOffsetSegundosTimeZone());
					
					vObj.formatToSQLite();
					Long vIdPessoa =vObj.insert(true); 
					if(vIdPessoa != null){
						containerEmpresaProfissao.addListTupleInInsert(db, String.valueOf( vIdPessoa));
						//							clearComponents();
						activity.handlerClearComponents.sendEmptyMessage(0);
						
						return activity.factoryContainerCadastro(HelperDialog.DIALOG_CADASTRO_OK, true);
					} else{
						
						return activity.factoryContainerCadastro(activity.FORM_ERROR_DIALOG_REGISTRO_DUPLICADO, false);
					}
				}
			}	
			else{
				return activity.factoryContainerCadastro(activity.FORM_ERROR_DIALOG_CAMPO_INVALIDO, false);
				
			}
		}
		catch(Exception e)
		{
			SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
			return new ContainerCadastro(new Mensagem(e), false);
		}
		
	}
	
	protected class NumeroDocumentoOnFocusChangeListener implements OnFocusChangeListener {
		//
		public void onFocusChange(View view, boolean hasFocus) {
			if(hasFocus){
				String vTipoDocumento = tipoDocumentoAutoCompletEditText.getText().toString();
				if(vTipoDocumento.length() == 0 ){
					activity.showDialog(activity.FORM_ERROR_DIALOG_MISSING_TIPO_DOCUMENTO);
					tipoDocumentoAutoCompletEditText.requestFocus();
					tipoDocumentoAutoCompletEditText.setSelection(0);

				} 
			}
		}
	}

	protected class NumeroDocumentoOnClickListener implements OnClickListener{

		public void onClick(View arg0) {

			String vTipoDocumento = tipoDocumentoAutoCompletEditText.getText().toString();

			if(vTipoDocumento.length() == 0 ){
				activity.showDialog(activity.FORM_ERROR_DIALOG_MISSING_TIPO_DOCUMENTO);
				tipoDocumentoAutoCompletEditText.requestFocus();
				tipoDocumentoAutoCompletEditText.setSelection(0);

			} 

		}

	}
	protected class TipoDocumentoOnFocusChangeListener implements OnFocusChangeListener {

		public void onFocusChange(View view, boolean hasFocus) {

			if(!hasFocus){
				String vTipoDocumento = tipoDocumentoAutoCompletEditText.getText().toString();

				if(vTipoDocumento.length() == 0 ){
					numeroDocumentoEditText.setText("");
				} 
			}
		}
	}

}
