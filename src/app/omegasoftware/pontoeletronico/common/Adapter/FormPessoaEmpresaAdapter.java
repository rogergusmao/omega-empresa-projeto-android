package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutPessoaEmpresa;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectPessoaEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.EmpresaProfissaoItemList;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;

public class FormPessoaEmpresaAdapter extends BaseAdapter {

	private ArrayList<EmpresaProfissaoItemList> listEmpresaProfissaoItemList;
	private Activity activity;
	ContainerLayoutPessoaEmpresa container;
	
	
	public FormPessoaEmpresaAdapter(Activity context,
			ArrayList<EmpresaProfissaoItemList> pListEmpresaProfissaoItemList,
			ContainerLayoutPessoaEmpresa pContainer)
	{
		this.listEmpresaProfissaoItemList = pListEmpresaProfissaoItemList;
		this.activity = context;
		container = pContainer;
	}
	
	public int getCount() {
		return this.listEmpresaProfissaoItemList.size();
	}

	public EmpresaProfissaoItemList getItem(int position) {
		return this.listEmpresaProfissaoItemList.get(position);
	}

	public long getItemId(int position) {
		return this.listEmpresaProfissaoItemList.get(position).getId();
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		
		EmpresaProfissaoItemList empresaProfissaoItemList = this.getItem(position);
		LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View layoutView = layoutInflater.inflate(R.layout.form_pessoa_empresa_item_list_layout, null);
		
		TextView empresaTextView = (TextView) layoutView.findViewById(R.id.empresa_text_view);
		empresaTextView.setText(empresaProfissaoItemList.getEmpresa());
		
		TextView profissaoTextView = (TextView) layoutView.findViewById(R.id.profissao_text_view);
		profissaoTextView.setText(empresaProfissaoItemList.getProfissao());
		
		Button deleteButton = (Button) layoutView.findViewById(R.id.delete_button);
		Button editButton = (Button) layoutView.findViewById(R.id.edit_button);
		
		
		if(empresaProfissaoItemList.getIdPessoaEmpresa() != null){
			editButton.setOnClickListener(
				new EditButtonListener(
						empresaProfissaoItemList.getIdPessoaEmpresa()
				));
			editButton.setVisibility(View.VISIBLE);
		} else {
			editButton.setVisibility(OmegaConfiguration.VISIBILITY_INVISIBLE);
		}
	
		
		deleteButton.setOnClickListener(
				new DeleteButtonListener(
						empresaProfissaoItemList.getIdEmpresa(), 
						empresaProfissaoItemList.getProfissao(),
						empresaProfissaoItemList.getIdPessoaEmpresa(),
						container
				));
		
		return layoutView;
		
	}

	private class EditButtonListener implements OnClickListener
	{
		
		String idPessoaEmpresa = null;
		
		
		
		public EditButtonListener(
				String idPessoaEmpresa){
			this.idPessoaEmpresa = idPessoaEmpresa;
		}
		
		public void onClick(View v) {
			Intent intent = new Intent(activity, SelectPessoaEmpresaActivityMobile.class);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, idPessoaEmpresa);
			activity.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_EMPRESA_FUNCIONARIO);
		}
	}	

	private class DeleteButtonListener implements OnClickListener
	{
		
		String idEmpresa = null;
		String profissao = null;
		String idPessoaEmpresa = null;
		ContainerLayoutPessoaEmpresa container = null;
		
		public DeleteButtonListener(
				String pIdEmpresa, 
				String pNomeProfissao, 
				String idPessoaEmpresa,
				ContainerLayoutPessoaEmpresa pContainer){
			idEmpresa = pIdEmpresa;
			profissao = pNomeProfissao;
			container = pContainer;
			this.idPessoaEmpresa = idPessoaEmpresa;
		}
		
		public void onClick(View v) {
			FactoryAlertDialog.showAlertDialog(
					activity, 
					activity.getResources().getString(R.string.deseja_remover_profissao),
					new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							//SIM
							if(which == -1){
								if(idPessoaEmpresa != null)
									container.deletarRegistro(idPessoaEmpresa);
								else 
									container.deletarRegistro(idEmpresa, profissao );	
							}
							
						}
					});
		}
	}	
}

