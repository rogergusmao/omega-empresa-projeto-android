package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutRedeEmpresa;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.RedeEmpresaItemList;

public class FormRedeEmpresaAdapter extends BaseAdapter {

	private ArrayList<RedeEmpresaItemList> listEmpresaProfissaoItemList;
	private Context context;
	ContainerLayoutRedeEmpresa container;
	
	
	public FormRedeEmpresaAdapter(Context context,
			ArrayList<RedeEmpresaItemList> pListEmpresaProfissaoItemList,
			ContainerLayoutRedeEmpresa pContainer)
	{
		this.listEmpresaProfissaoItemList = pListEmpresaProfissaoItemList;
		this.context = context;
		container = pContainer;
	}
	
	public int getCount() {
		return this.listEmpresaProfissaoItemList.size();
	}

	public RedeEmpresaItemList getItem(int position) {
		return this.listEmpresaProfissaoItemList.get(position);
	}

	public long getItemId(int position) {
		return this.listEmpresaProfissaoItemList.get(position).getId();
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		
		RedeEmpresaItemList empresaProfissaoItemList = this.getItem(position);
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View layoutView = layoutInflater.inflate(R.layout.form_pessoa_empresa_item_list_layout, null);
		
		TextView empresaTextView = (TextView) layoutView.findViewById(R.id.empresa_text_view);
		empresaTextView.setText(empresaProfissaoItemList.getNomeEmpresa());
		
		Button closeButton = (Button) layoutView.findViewById(R.id.delete_button);
		
		closeButton.setOnClickListener(
				new DeleteButtonListener(
						empresaProfissaoItemList.getEmpresa(),
						container
				));
		 layoutView.findViewById(R.id.edit_button).setVisibility(View.GONE);
		return layoutView;
		
	}
	

	private class DeleteButtonListener implements OnClickListener
	{
		
		String idEmpresa = null;
		
		ContainerLayoutRedeEmpresa container = null;
		
		public DeleteButtonListener(
				String pIdEmpresa,  
				ContainerLayoutRedeEmpresa pContainer){
			idEmpresa = pIdEmpresa;
			container = pContainer;
		}
		
		public void onClick(View v) {
			container.delete(idEmpresa);
		}
	}	
}

