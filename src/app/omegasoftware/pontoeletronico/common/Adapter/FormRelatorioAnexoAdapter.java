package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutRelatorioAnexo;
import app.omegasoftware.pontoeletronico.common.ItemList.RelatorioAnexoItemList;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;

@SuppressLint("InflateParams")
public class FormRelatorioAnexoAdapter extends BaseAdapter {
     
	private ArrayList<RelatorioAnexoItemList> listRelatorioAnexoItemList;
	private Context activity;
	ContainerLayoutRelatorioAnexo container;
	
	private LinearLayout linearLayoutAnexo;
	OmegaFileConfiguration config = new OmegaFileConfiguration();
	
	public FormRelatorioAnexoAdapter(Activity pActivity,
			ArrayList<RelatorioAnexoItemList> pListItemList,
			ContainerLayoutRelatorioAnexo pContainer)
	{
		this.listRelatorioAnexoItemList = pListItemList;
		this.activity = pActivity;
		container = pContainer;
		
	}
	
	public int getCount() {
		return this.listRelatorioAnexoItemList.size();
	}

	public RelatorioAnexoItemList getItem(int position) {
		return this.listRelatorioAnexoItemList.get(position);
	}

	public long getItemId(int position) {
		return this.listRelatorioAnexoItemList.get(position).getId();
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		
		RelatorioAnexoItemList itemList = this.getItem(position);
		LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View layoutView = layoutInflater.inflate(R.layout.item_list_relatorio_anexo_layout, null);
		linearLayoutAnexo = (LinearLayout) layoutView.findViewById(R.id.anexo_linearlayout);
		
		TextView pathTextView = (TextView) layoutView.findViewById(R.id.path_textview);
		pathTextView.setText(itemList.getAnexo());
		
		//LayoutParams vParam = new LayoutParams(50,50);
		
		
		
		Button closeButton = (Button) layoutView.findViewById(R.id.close_button);
		
		closeButton.setOnClickListener(
				new DeleteButtonListener(
						itemList.getAnexo(),
						container
				));
		
		return layoutView;
		
	}

	private class DeleteButtonListener implements OnClickListener
	{
		
		String anexo = null;
		
		ContainerLayoutRelatorioAnexo container = null;
		
		public DeleteButtonListener(
				String pAnexo,  
				ContainerLayoutRelatorioAnexo pContainer){
			anexo = pAnexo;
			container = pContainer;
		}
		
		public void onClick(View v) {
			container.delete(anexo);
		}
	}
}

