package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutRelatorioAudio;
import app.omegasoftware.pontoeletronico.audio.MediaAudioActivity;
import app.omegasoftware.pontoeletronico.cam.CameraConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.RelatorioAnexoItemList;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;

@SuppressLint("InflateParams")
public class FormRelatorioAudioAdapter extends BaseAdapter {

	private ArrayList<RelatorioAnexoItemList> listRelatorioAudioItemList;
	private Context activity;
	ContainerLayoutRelatorioAudio container;
	
//	private LinearLayout linearLayoutImage;
	OmegaFileConfiguration config = new OmegaFileConfiguration();
	
	public FormRelatorioAudioAdapter(Activity pActivity,
			ArrayList<RelatorioAnexoItemList> pListItemList,
			ContainerLayoutRelatorioAudio pContainer)
	{
		this.listRelatorioAudioItemList = pListItemList;
		this.activity = pActivity;
		container = pContainer;
		
	}
	
	public int getCount() {
		return this.listRelatorioAudioItemList.size();
	}

	public RelatorioAnexoItemList getItem(int position) {
		return this.listRelatorioAudioItemList.get(position);
	}

	public long getItemId(int position) {
		return this.listRelatorioAudioItemList.get(position).getId();
	}

	@SuppressLint("ViewHolder")
	public View getView(int position, View convertView, ViewGroup parent) {
		
		RelatorioAnexoItemList itemList = this.getItem(position);
		LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View layoutView = layoutInflater.inflate(R.layout.item_list_relatorio_audio_layout, null);
//		linearLayoutImage = (LinearLayout) layoutView.findViewById(R.id.audio_linearlayout);
		Button audioButton = (Button) layoutView.findViewById(R.id.audio_button);
		audioButton.setOnClickListener( new PlayAudio(itemList.getAnexo()));
//		LayoutParams vParam = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		//LayoutParams vParam = new LayoutParams(50,50);
		
		
		
		Button closeButton = (Button) layoutView.findViewById(R.id.close_button);
		
		closeButton.setOnClickListener(
				new DeleteButtonListener(
						itemList.getAnexo(),
						container
				));
		
		return layoutView;
		
	}

	private class PlayAudio implements OnClickListener{
		String pathFile;
		public PlayAudio(String pArquivo){
			pathFile = pArquivo;
		}
		
		public void onClick(View v) {
			
			Intent intent = new Intent(activity, MediaAudioActivity.class);
			intent.putExtra(CameraConfiguration.PATH_DIRECTORY_FIELD, pathFile);

			if(intent != null)
			{				
				try{

					activity.startActivity(intent);
				}catch(Exception ex){
					//Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
				}
			}
		}
		
	}

	private class DeleteButtonListener implements OnClickListener
	{
		
		String audio = null;
		
		ContainerLayoutRelatorioAudio container = null;
		
		public DeleteButtonListener(
				String pAudio,  
				ContainerLayoutRelatorioAudio pContainer){
			audio = pAudio;
			container = pContainer;
		}
		
		public void onClick(View v) {
			container.delete(audio);
		}
	}
}

