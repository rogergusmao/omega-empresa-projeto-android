package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutRelatorioFoto;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.OmegaImageView;
import app.omegasoftware.pontoeletronico.common.ItemList.RelatorioAnexoItemList;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;

public class FormRelatorioFotoAdapter extends BaseAdapter {

	private ArrayList<RelatorioAnexoItemList> listRelatorioFotoItemList;
	private Context context;
	ContainerLayoutRelatorioFoto container;
	private OmegaImageView imageView;
	private LinearLayout linearLayoutImage;
	OmegaFileConfiguration config = new OmegaFileConfiguration();
	
	public FormRelatorioFotoAdapter(Context context,
			ArrayList<RelatorioAnexoItemList> pListItemList,
			ContainerLayoutRelatorioFoto pContainer)
	{
		this.listRelatorioFotoItemList = pListItemList;
		this.context = context;
		container = pContainer;
		
	}
	
	public int getCount() {
		return this.listRelatorioFotoItemList.size();
	}

	public RelatorioAnexoItemList getItem(int position) {
		return this.listRelatorioFotoItemList.get(position);
	}

	public long getItemId(int position) {
		return this.listRelatorioFotoItemList.get(position).getId();
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		
		RelatorioAnexoItemList itemList = this.getItem(position);
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View layoutView = layoutInflater.inflate(R.layout.item_list_relatorio_foto_layout, null);
		linearLayoutImage = (LinearLayout) layoutView.findViewById(R.id.foto_linearlayout);
		
		imageView = new OmegaImageView(context);
		LayoutParams vParam = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		//LayoutParams vParam = new LayoutParams(50,50);
		imageView.setLayoutParams(vParam);
		linearLayoutImage.addView(imageView);
		
		
		
		Bitmap bMap = BitmapFactory.decodeFile(itemList.getAnexo());
		imageView.setImageBitmap(bMap);
		imageView.setVisibility(View.VISIBLE);
		
		Button closeButton = (Button) layoutView.findViewById(R.id.close_button);
		
		closeButton.setOnClickListener(
				new DeleteButtonListener(
						itemList.getAnexo(),
						container
				));
		
		return layoutView;
		
	}
	

	private class DeleteButtonListener implements OnClickListener
	{
		
		String foto = null;
		
		ContainerLayoutRelatorioFoto container = null;
		
		public DeleteButtonListener(
				String pFoto,  
				ContainerLayoutRelatorioFoto pContainer){
			foto = pFoto;
			container = pContainer;
		}
		
		public void onClick(View v) {
			container.delete(foto);
		}
	}
}

