package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutRelatorioVideo;
import app.omegasoftware.pontoeletronico.cam.CameraConfiguration;
import app.omegasoftware.pontoeletronico.cam.MediaVideoActivity;
import app.omegasoftware.pontoeletronico.common.ItemList.RelatorioAnexoItemList;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;

@SuppressLint("InflateParams")
public class FormRelatorioVideoAdapter extends BaseAdapter {

	private ArrayList<RelatorioAnexoItemList> listRelatorioVideoItemList;
	private Activity activity;
	ContainerLayoutRelatorioVideo container;

//	private LinearLayout linearLayoutImage;

	OmegaFileConfiguration config = new OmegaFileConfiguration();
	
	public FormRelatorioVideoAdapter(Activity pActivity,
			ArrayList<RelatorioAnexoItemList> pListItemList,
			ContainerLayoutRelatorioVideo pContainer)
	{
		this.listRelatorioVideoItemList = pListItemList;
		activity = pActivity;
		container = pContainer;
		
	}
	
	public int getCount() {
		return this.listRelatorioVideoItemList.size();
	}

	public RelatorioAnexoItemList getItem(int position) {
		return this.listRelatorioVideoItemList.get(position);
	}

	public long getItemId(int position) {
		return this.listRelatorioVideoItemList.get(position).getId();
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		
		RelatorioAnexoItemList itemList = this.getItem(position);
		LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View layoutView = layoutInflater.inflate(R.layout.item_list_relatorio_video_layout, null);
		
		Button videoButton = (Button) layoutView.findViewById(R.id.video_button);
		
		videoButton.setOnClickListener( new PlayVideo(itemList.getAnexo()));
		//LayoutParams vParam = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		//LayoutParams vParam = new LayoutParams(50,50);
		
		Button closeButton = (Button) layoutView.findViewById(R.id.close_button);
		
		closeButton.setOnClickListener(
				new DeleteButtonListener(
						itemList.getAnexo(),
						container
				));
		
		return layoutView;
		
	}
	
	protected class DownloadVideo implements OnClickListener{
		
		public DownloadVideo(){
			
		}
		
		public void onClick(View arg0) {
			
			boolean vValidade = false;
			for(int i = 0 ; i < 3; i++){
//				vValidade = HelperHttpPostReceiveFile.DownloadFileFromHttpPost(
//						this, 
//						OmegaConfiguration.URL_REQUEST_RECEIVE_FILE, 
//						".jpg", 
//						new String[]{"usuario_foto"}, 
//						new String[]{"mudar para o identificador do video"});
				if(vValidade  ) break;
				
			}
		}
		
	}

	
	private class PlayVideo implements OnClickListener{
		String pathFile;
		public PlayVideo(String pArquivo){
			pathFile = pArquivo;
		}
		
		public void onClick(View v) {
			
			Intent intent = new Intent(activity, MediaVideoActivity.class);
			intent.putExtra(CameraConfiguration.PATH_DIRECTORY_FIELD, pathFile);

			if(intent != null)
			{				
				try{

					activity.startActivity(intent);
				}catch(Exception ex){
					//Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
				}
			}
		}
		
	}
	private class DeleteButtonListener implements OnClickListener
	{
		
		String video = null;
		
		ContainerLayoutRelatorioVideo container = null;
		
		public DeleteButtonListener(
				String pVideo,  
				ContainerLayoutRelatorioVideo pContainer){
			video = pVideo;
			container = pContainer;
		}
		
		public void onClick(View v) {
			container.delete(video);
		}
	}
}

