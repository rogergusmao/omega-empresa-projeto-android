package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.ContainerLayoutVeiculoUsuario;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.VeiculoUsuarioItemList;
import app.omegasoftware.pontoeletronico.common.activity.HelperFonte;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class FormVeiculoUsuarioAdapter extends BaseAdapter {

	private ArrayList<VeiculoUsuarioItemList> veiculosUsuarioItemList;
	private Context context;
	ContainerLayoutVeiculoUsuario container;
	
	
	public FormVeiculoUsuarioAdapter(Context context,
			ArrayList<VeiculoUsuarioItemList> veiculosUsuarioItemList,
			ContainerLayoutVeiculoUsuario pContainer)
	{
		this.veiculosUsuarioItemList = veiculosUsuarioItemList;
		this.context = context;
		container = pContainer;
	}
	
	public int getCount() {
		return this.veiculosUsuarioItemList.size();
	}

	public VeiculoUsuarioItemList getItem(int position) {
		return this.veiculosUsuarioItemList.get(position);
	}

	public long getItemId(int position) {
		return HelperInteger.parserLong( this.veiculosUsuarioItemList.get(position).getId());
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		
		VeiculoUsuarioItemList empresaProfissaoItemList = this.getItem(position);
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View layoutView = layoutInflater.inflate(R.layout.form_pessoa_empresa_item_list_layout, null);
		
		TextView empresaTextView = (TextView) layoutView.findViewById(R.id.empresa_text_view);
		empresaTextView.setText(empresaProfissaoItemList.getConteudoContainerItem(VeiculoUsuarioItemList.EMAIL_USUARIO));
		
		Button closeButton = (Button) layoutView.findViewById(R.id.delete_button);
		
		closeButton.setOnClickListener(
				new DeleteButtonListener(
						empresaProfissaoItemList.getConteudoContainerItem(VeiculoUsuarioItemList.ID_USUARIO)
				));
		layoutView.findViewById(R.id.edit_button).setVisibility(View.GONE);
		HelperFonte.setFontAllView(layoutView);
		return layoutView;
		
	}
	

	private class DeleteButtonListener implements OnClickListener
	{
		
		String idUsuario = null;
		
		public DeleteButtonListener(
				String pIdEmpresa){
			idUsuario = pIdEmpresa;
		}
		
		public void onClick(View v) {
			container.delete(idUsuario);
		}
	}	
}

