package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.HashSet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Notificacao;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaHistoricoSincronizador;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class HistoricoSincronizadorAdapter {

//	private static final String TAG = "PessoaPontoAdapter";

	TextView tvDescricao;
	TextView tvData;
	TextView tvTitulo;

	Database db=null;
	Activity activity;
	HashSet<Integer> idsSistemaTabelasSemAvisoDeNotificacao;

	String strNovo = "";
	String strNova = "";
	String strEditado = "";
	String strEditada = "";
	String strRemovido = "";
	String strRemovida = "";

	public HistoricoSincronizadorAdapter(Database db, Activity activity) {
		this.db = db;

		this.activity = activity;
		PontoEletronicoSharedPreference sp = new PontoEletronicoSharedPreference();
		Integer[] idsSistemaTabela = PontoEletronicoSharedPreference
				.getValor(
						activity,
						PontoEletronicoSharedPreference.TIPO_VETOR_INT.IDS_SISTEMA_TABELA);
		if (idsSistemaTabela != null) {
			for (int i = 0; i < idsSistemaTabela.length; i++) {
				idsSistemaTabelasSemAvisoDeNotificacao.add(idsSistemaTabela[i]);
			}
		}

	}

	//
	// public void adicionarAcao(EXTDAOSistemaHistoricoSincronizador registro,
	// View view){
	// String idSistemaTabela =
	// registro.getStrValueOfAttribute(EXTDAOSistemaHistoricoSincronizador.SISTEMA_TABELA_ID_INT);
	// final String tabela = EXTDAOSistemaTabela.getNomeSistemaTabela(db,
	// idSistemaTabela);
	//
	//
	// view.setOnClickListener(new View.OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// Intent intent = null;
	// if(tabela.equalsIgnoreCase(EXTDAOUsuario.NAME)){
	// intent = new Intent(activity, ListUsuarioActivityMobile.class);
	//
	// } else if(tabela.equalsIgnoreCase(EXTDAOPessoa.NAME)){
	// intent = new Intent(activity, ListPessoaActivityMobile.class);
	//
	// } else if(tabela.equalsIgnoreCase(EXTDAOEmpresa.NAME)){
	// intent = new Intent(activity, ListEmpresaActivityMobile.class);
	//
	// } else if(tabela.equalsIgnoreCase(EXTDAOPonto.NAME)){
	// intent = new Intent(activity, ListPontoActivityMobile.class);
	//
	// } else if(tabela.equalsIgnoreCase(EXTDAOPessoaEmpresa.NAME)){
	// intent = new Intent(activity, ListPessoaEmpresaActivityMobile.class);
	//
	// } else if(tabela.equalsIgnoreCase(EXTDAOVeiculo.NAME)){
	// intent = new Intent(activity, ListVeiculoActivityMobile.class);
	//
	// } else if(tabela.equalsIgnoreCase(EXTDAOVeiculoUsuario.NAME)){
	// intent = new Intent(activity, DetailVeiculoActivityMobile.class);
	// }
	// //Nova categoria do usuurio
	// else if(tabela.equalsIgnoreCase(EXTDAOUsuarioCategoriaPermissao.NAME)){
	// intent = new Intent(activity, DetailUsuarioActivityMobile.class);
	//
	// }else if(tabela.equalsIgnoreCase(EXTDAOUsuarioCategoriaPermissao.NAME)){
	// intent = new Intent(activity, ListPessoaEmpresaActivityMobile.class);
	//
	// } else if(tabela.equalsIgnoreCase(EXTDAOPessoaEmpresaRotina.NAME)){
	// intent = new Intent(activity, ListPessoaEmpresaActivityMobile.class);
	//
	// } else if(tabela.equalsIgnoreCase(EXTDAOUsuarioServico.NAME)){
	// intent = new Intent(activity, ListPessoaEmpresaActivityMobile.class);
	// }
	// activity.startActivityForResult(intent,
	// OmegaConfiguration.ACTIVITY_ALERT);
	//
	// }
	// });
	// }
	// //
	// public void formatarAcao(EXTDAOSistemaHistoricoSincronizador registro,
	// View view){
	// String strTipoOperacao =
	// registro.getStrValueOfAttribute(EXTDAOSistemaHistoricoSincronizador.SISTEMA_TIPO_OPERACAO_ID_INT);
	// int tipoOperacao = HelperInteger.parserInt(strTipoOperacao);
	// if(tipoOperacao == EXTDAOSistemaTipoOperacaoBanco.TIPO.REMOVE.getId()){
	// // nao faz nada
	// } else if(tipoOperacao ==
	// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT.getId()){
	// adicionarAcao(registro, view);
	// } else if(tipoOperacao ==
	// EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT.getId()){
	// adicionarAcao(registro, view);
	// }
	// }

	public View getView(EXTDAOSistemaHistoricoSincronizador registro) {
		try {
			String idSistemaTabela = registro
					.getStrValueOfAttribute(EXTDAOSistemaHistoricoSincronizador.SISTEMA_TABELA_ID_INT);
			String tabela = EXTDAOSistemaTabela.getNomeSistemaTabela(db,
					idSistemaTabela);
			Table obj = this.db.factoryTable(tabela);
			if (!obj.select(registro
					.getStrValueOfAttribute(EXTDAOSistemaHistoricoSincronizador.ID_TABELA_INT)))
				return null;

			final Notificacao notificacao = obj.getNotificacao(activity);
			if (notificacao == null)
				return null;
			String titulo = notificacao
					.getTitulo(registro
							.getIntValueOfAttribuite(EXTDAOSistemaHistoricoSincronizador.SISTEMA_TIPO_OPERACAO_ID_INT));
			String descricao = notificacao.getSubtitulo();

			String data = registro
					.getStrValueOfAttribute(EXTDAOSistemaHistoricoSincronizador.DATA_DATETIME);

			HelperDate helper = new HelperDate(data);
			data = helper.getDateAgo(activity);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			View viewItem = vLayoutInflater.inflate(
					R.layout.item_list_historico_sincronizador, null);

			TextView tvTitulo = (TextView) viewItem
					.findViewById(R.id.tv_titulo);
			tvTitulo.setText(titulo);
			
			TextView tvData = (TextView) viewItem.findViewById(R.id.tv_data);
			tvData.setText(data);

			TextView tvDescricao = (TextView) viewItem
					.findViewById(R.id.tv_descricao);
			tvDescricao.setText(HelperString.ucFirst(descricao));

			// String data =
			// reg.getStrValueOfAttribute(EXTDAOSistemaHistoricoSincronizador.DATA_DATETIME);
			// String idTipoOperacao =
			// reg.getStrValueOfAttribute(EXTDAOSistemaHistoricoSincronizador.SISTEMA_TIPO_OPERACAO_ID_INT);
			// String idSistemaTabela
			// =reg.getStrValueOfAttribute(EXTDAOSistemaHistoricoSincronizador.SISTEMA_TABELA_ID_INT);
			// String id =
			// reg.getStrValueOfAttribute(EXTDAOSistemaHistoricoSincronizador.ID);
			//

			viewItem.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = null;
					intent = new Intent(activity, notificacao.getClasseAcao());
					notificacao.adicionaParametrosAcao(intent);
					activity.startActivityForResult(intent,
							OmegaConfiguration.ACTIVITY_ALERT);
				}
			});

			return viewItem;
		} catch (Exception exc) {
			SingletonLog.insereErro(exc, TIPO.FORMULARIO);
			return null;
		}

	}

}
