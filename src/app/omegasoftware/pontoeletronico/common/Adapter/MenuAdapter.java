package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.FactoryMenuActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.IntegerListComparator;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.MenuItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.container.AbstractContainerClassItem;
import app.omegasoftware.pontoeletronico.common.container.ContainerMenuOption;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPermissao;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class MenuAdapter extends BkpCustomAdapter{

	private static final int ID_TEXT_VIEW_IMAGEVIEW = R.id.menu_imageview;
	private static final int ID_TEXT_VIEW_TEXTVIEW = R.id.menu_textview;
	DatabasePontoEletronico db = null;
	
	public String nomeTagAtual;
	public String idPermissaoAtual;
	
	FactoryMenuActivityMobile activity;
	
	public static HashMap<Integer, ArrayList<Integer>> hashMenu = new HashMap<Integer, ArrayList<Integer>>();
	public static HashMap<Integer, CustomItemList> hashMenuItemList = new HashMap<Integer, CustomItemList>();
	public static void clear(){
		if(hashMenu != null && hashMenu.size() > 0)
			hashMenu.clear();
		if(hashMenuItemList != null && hashMenuItemList.size() > 0)
			hashMenuItemList.clear();
	}
	public MenuAdapter(
			FactoryMenuActivityMobile pActivity,
			String pNomeTagAtual) throws OmegaDatabaseException
	{
		super(pActivity, false, false, false, null);
		activity = pActivity;
		nomeTagAtual =pNomeTagAtual;
		db = new DatabasePontoEletronico(this.activity);
		if(nomeTagAtual != null){
			EXTDAOPermissao vObjPermissao = new EXTDAOPermissao(db);
			vObjPermissao.setAttrValue(EXTDAOPermissao.TAG, nomeTagAtual);

			ArrayList<Table> vListTupla = vObjPermissao.getListTable();
			
			if(vListTupla != null)
				if(vListTupla.size() == 1){
					EXTDAOPermissao vTuplaPermissao = (EXTDAOPermissao) vListTupla.get(0);
					String vIdPermissao = vTuplaPermissao.getStrValueOfAttribute(EXTDAOPermissao.ID);
					if(vIdPermissao != null)
						if(HelperInteger.parserInteger(vIdPermissao) != null)
							idPermissaoAtual = vIdPermissao;
				}

		}
		super.initalizeListCustomItemList();
		db.close();
	}
	@Override
	public void initalizeListCustomItemList(){
		this.list = loadData(isAsc);
		//Adicionar os itens comuns desejados aqui no menu Raiz
			
			
	}
	
	@Override
	public void finalize(){
		try{

			if(hashMenu != null)
				hashMenu.clear();
			
			db.close();
			try {
				super.finalize();
			} catch (Throwable e) {
				
				e.printStackTrace();
			}
		}catch(Exception ex){

		}
	}

	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			MenuItemList vMenuItemList = (MenuItemList)this.getItem(pPosition);
			if(vMenuItemList == null) return null;
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.menu_item_list_layout, null);
			
			if((pPosition % 2) == 0)
			{
				((LinearLayout) vLayoutView.findViewById(R.id.menu_lineLayout)).setBackgroundResource(R.drawable.item_menu_light_gray_button);
			}
			else
			{
				((LinearLayout) vLayoutView.findViewById(R.id.menu_lineLayout)).setBackgroundResource(R.drawable.item_menu_white_button);
			}

			CreateTextView(
					ID_TEXT_VIEW_TEXTVIEW, 
					vMenuItemList.getConteudoContainerItem(MenuItemList.TEXTVIEW), 
					vLayoutView,
					false);

			Integer vIdDrawable = HelperInteger.parserInt(vMenuItemList.getConteudoContainerItem(MenuItemList.DRAWABLE));
			if(vIdDrawable == null) return null;
			ImageView vImageView = (ImageView) vLayoutView.findViewById(ID_TEXT_VIEW_IMAGEVIEW);
			Drawable vDrawable = activity.getResources().getDrawable(vIdDrawable);
			vImageView.setImageDrawable(vDrawable);

			OnClickListener vOnClick = vMenuItemList.getClickListener();

			if(vOnClick != null){
				vLayoutView.setOnClickListener(vOnClick);
				vImageView.setOnClickListener(vOnClick);
			}
			return vLayoutView;
		} catch (Exception exc) {
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
//					"MenuAdapter: getView()");
		}
		return null;
	}

	@Override
	protected ArrayList<Integer> getListIdBySearchParameters(boolean pIsAsc) {



		ArrayList<Integer> vListRetorno = null;
		Integer intPermissaoAtual = HelperInteger.parserInt( idPermissaoAtual);
		
		if(hashMenu.containsKey(intPermissaoAtual)){
			return hashMenu.get(intPermissaoAtual);
		}
		try {
			vListRetorno = OmegaSecurity.getListaIdPermissaoDaTela(db, intPermissaoAtual == -1 ? null : intPermissaoAtual);
			
			hashMenu.put(intPermissaoAtual, vListRetorno);
		} catch (Exception e) {
			
			e.printStackTrace();
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
		finally
		{
			db.close();
		}
		if(vListRetorno != null)
			if(vListRetorno.size() > 1 )
				Collections.sort(vListRetorno, new IntegerListComparator(true));
		
		return vListRetorno;
	}


	@Override
	public CustomItemList getCustomItemList(Integer pId) {
		if(hashMenuItemList.containsKey(pId)){
			return hashMenuItemList.get(pId);
		}else{
			
			String vStrId = String.valueOf(pId);
			if(vStrId == null) return null;
			EXTDAOPermissao vEXTDAOPermissao = OmegaSecurity.getObjPermissao(db, pId);
			if(vEXTDAOPermissao != null){
				String vTagPermissao = vEXTDAOPermissao.getStrValueOfAttribute(EXTDAOPermissao.TAG);
				
		//		String vNome = vEXTDAOPermissao.getStrValueOfAttribute(EXTDAOPermissao.NOME);
				
				AbstractContainerClassItem vContainer = ContainerMenuOption.getContainerItem(vTagPermissao);
				if(vContainer == null) return null;
				String vNome = vContainer.getNome(activity);
				
				Integer vIdDrawable = null;
				vIdDrawable = vContainer.getDrawableId();
				if(vIdDrawable == null) return null;
				String vStrDrawable = String.valueOf(vIdDrawable);
				MenuItemList vItemList = new MenuItemList(
						vStrId, 
						vStrDrawable, 
						vNome,
						vContainer.getOnClickListener(activity));
				hashMenuItemList.put(pId, vItemList);
		
				return vItemList;
			} else return null;
		}
		
	}
}

