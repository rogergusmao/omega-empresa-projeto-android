package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormModeloActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.ModeloItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOModelo;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class ModeloAdapter extends DatabaseCustomAdapter{

	
	//private static final int ID_TEXT_VIEW_NOME_MODELO = R.id.modelo_name;
		private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;


	public String nomeModelo;
	public String anoModelo;


	public ModeloAdapter(
			Activity pActivity,
			String nomeModeloId,
			String anoModeloId,
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, p_isAsc, EXTDAOModelo.NAME, ModeloItemList.NOME_MODELO, false);
		this.nomeModelo = nomeModeloId;
		this.anoModelo = anoModeloId;
		initalizeListCustomItemList();
	}
	
	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			ModeloItemList vItemList = (ModeloItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.item_list_layout, null);
			
			PersonalizaItemView(vLayoutView, pPosition);
	
			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_1, 
					vItemList.getConteudoContainerItem(ModeloItemList.NOME_MODELO), 
					vLayoutView);

	
			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
						activity, 
						EXTDAOModelo.NAME,
						pParent, 
						vItemList.getId(),
						FormEditActivityMobile.class, 
						FormModeloActivityMobile.class,
						isEditPermitido(),
						isRemovePermitido(), vItemList);
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_MODELO, 
						vClickListener);
			}
			else {
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_MODELO);
			}
			return vLayoutView;
		} catch (Exception exc) {
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
					"ModeloAdapter: getView()");
		}
		return null;
	}




//	@Override
//	public void SortList(boolean pAsc) {
//		Collections.sort(list, new NameCustomItemListComparator(pAsc));
//		this.notifyDataSetChanged();
//	}

	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {

		
		DatabasePontoEletronico database=null;
		try {
			database = new DatabasePontoEletronico(this.activity);
		} catch (OmegaDatabaseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			
			String querySelectIdPrestadores = "SELECT DISTINCT v."
					+ EXTDAOModelo.ID + ", v." + EXTDAOModelo.NOME;

			String queryFromTables =  " FROM " + EXTDAOModelo.NAME + " v" ;

			String queryWhereClausule = ""; 
			ArrayList<String> args = new ArrayList<String>();
			
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " v." + EXTDAOModelo.CORPORACAO_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND v." + EXTDAOModelo.CORPORACAO_ID_INT + " = ? " ;
			
			args.add(OmegaSecurity.getIdCorporacao());
			
			if( nomeModelo != null){
			
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " m." + Attribute.getNomeAtributoNormalizado(EXTDAOModelo.NOME) + " LIKE ? ";
				else 
					queryWhereClausule += "AND m." + Attribute.getNomeAtributoNormalizado(EXTDAOModelo.NOME) + " LIKE ? ";
				
				args.add( HelperString.getWordWithoutAccent(nomeModelo));
			}

			if(anoModelo != null ){
				
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " m." + EXTDAOModelo.ANO_INT + " = ? " ;
				else 
					queryWhereClausule += " AND m." + EXTDAOModelo.ANO_INT + " = ? " ;

				args.add(anoModelo);
			}

			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY v." + EXTDAOModelo.NOME
						+ " DESC";
			} else {
				orderBy += " ORDER BY  v." + EXTDAOModelo.NOME
						+ " ASC";
			}

			String query = querySelectIdPrestadores + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

			if(orderBy.length() > 0 ){
				query += orderBy;  
			} 

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			
			Cursor cursor = null;
			try{
				

				cursor = database.rawQuery(query, vetorArg);
				// Cursor cursor = oh.query(true, EXTDAOModelo.NAME,
				// new String[]{EXTDAOModelo.ID_PRESTADOR}, "", new String[]{},
				// null, "", "", "");
				String[] vetorChave = new String[] { EXTDAOModelo.ID };
	
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}

		} catch (Exception e) {
			
			e.printStackTrace();
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
		finally
		{
			database.close();
		}
		return null;
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {

		EXTDAOModelo vEXTDAOModelo = (EXTDAOModelo) pObj; 

		ModeloItemList vItemList = new ModeloItemList(  
				vEXTDAOModelo.getAttribute(EXTDAOModelo.ID).getStrValue(),
				vEXTDAOModelo.getAttribute(EXTDAOModelo.NOME).getStrValue());

		return vItemList;
	}




}
