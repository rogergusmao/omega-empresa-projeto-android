package app.omegasoftware.pontoeletronico.common.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;

public class NoResultsAdapterAdapter {
	
	//private static final String TAG = "PessoaPontoAdapter";
	
	Activity activity;
	String descricao;
	Class<?> classeDetalhe;
	String idTable;
	public NoResultsAdapterAdapter(
			Activity pActivity,
			int idString,
			Class<?> classeDetalhe,
			String idTable)
	{
		this.activity = pActivity;
		descricao =activity.getResources().getString(idString);
		this.classeDetalhe = classeDetalhe;
		this.idTable = idTable;
		
	}

	
	
	public void formatarView(View viewNoResults)  {
		try {			
			
			View vDescricao=viewNoResults.findViewById(R.id.tv_descricao);
			if(vDescricao != null ){
				TextView tvDescricao = (TextView)vDescricao;
				if(descricao != null 
						&& descricao.length() > 0){
					tvDescricao.setText(descricao);
					tvDescricao.setVisibility(View.VISIBLE);
				} else {
					tvDescricao.setVisibility(View.GONE);
				}	
			}
			
			
			viewNoResults.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(activity, classeDetalhe);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, idTable);
					activity.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW);
					
				}
			});
		} catch (Exception exc) {
			SingletonLog.insereErro(exc, TIPO.FORMULARIO);
			
		}
		
	}



}
