package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPaisActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.PaisItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPais;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class PaisAdapter extends DatabaseCustomAdapter{
	
	//private static final int ID_TEXT_VIEW_PAIS = R.id.pais_textview;
	
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;


	public String nomePais;

	public PaisAdapter(
			Activity pActivity,
			String nomePais, 
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, p_isAsc, EXTDAOPais.NAME, PaisItemList.PAIS, false);
		this.nomePais = nomePais; 
		
		super.initalizeListCustomItemList();
	}

	
	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			PaisItemList vItemList = (PaisItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.item_list_layout, null);

			PersonalizaItemView(vLayoutView, pPosition);
			

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_1, 
					vItemList.getConteudoContainerItem(PaisItemList.PAIS), 
					vLayoutView);

			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
						activity, 
						EXTDAOPais.NAME,
						pParent, 
						vItemList.getId(),
						FormEditActivityMobile.class, 
						FormPaisActivityMobile.class,
						isEditPermitido(),
						isRemovePermitido(), vItemList);
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_PAIS, 
						vClickListener);
			}
			else {
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_PAIS);
			}
			return vLayoutView;
		} catch (Exception exc) {
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
					"PaisAdapter: getView()");
		}
		return null;
	}



	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {

		
		DatabasePontoEletronico database=null;
		try {
			database = new DatabasePontoEletronico(this.activity);
		} catch (OmegaDatabaseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			
			String querySelect = "SELECT DISTINCT pais."
					+ EXTDAOPais.ID + " ";

			String queryFromTables =  "FROM " + EXTDAOPais.NAME + " pais ";

			String queryWhereClausule = ""; 
			ArrayList<String> args = new ArrayList<String>();

			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " pais." + EXTDAOPais.CORPORACAO_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND pais." + EXTDAOPais.CORPORACAO_ID_INT + " = ? " ;
			
			args.add(OmegaSecurity.getIdCorporacao());
			
			if(this.nomePais != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " pais." + Attribute.getNomeAtributoNormalizado(EXTDAOPais.NOME) + " LIKE ?" ;
				else 
					queryWhereClausule += " AND pais." + Attribute.getNomeAtributoNormalizado(EXTDAOPais.NOME) + " LIKE ?" ;

				args.add('%' + HelperString.getWordWithoutAccent(this.nomePais) + '%' );
			}

			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY pais." + Attribute.getNomeAtributoNormalizado(EXTDAOPais.NOME)
						+ " DESC";
			} else {
				orderBy += " ORDER BY  pais." + Attribute.getNomeAtributoNormalizado(EXTDAOPais.NOME)
						+ " ASC";
			}

			String query = querySelect + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

			if(orderBy.length() > 0 ){
				query += orderBy;  
			} 

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			
			Cursor cursor = null;
			try{
				cursor = database.rawQuery(query, vetorArg);
				String[] vetorChave = new String[] { EXTDAOPais.ID };
	
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}

		} catch (Exception e) {
			
			e.printStackTrace();
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
		finally
		{
			database.close();
		}
		return null;
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {

		EXTDAOPais vEXTDAOPais = (EXTDAOPais) pObj;
		
	
		PaisItemList vItemList = new PaisItemList(  
				vEXTDAOPais.getAttribute(EXTDAOPais.ID).getStrValue(),
				vEXTDAOPais.getAttribute(EXTDAOPais.NOME).getStrValue());

		return vItemList;
	}
}

