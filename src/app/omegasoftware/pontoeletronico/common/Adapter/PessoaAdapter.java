package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.PessoaItemList;
import app.omegasoftware.pontoeletronico.common.activity.HelperFonte;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.container.DadosPessoaUsuario;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.Tuple;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaPerfil;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPerfil;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPermissaoCategoriaPermissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class PessoaAdapter extends DatabaseCustomAdapter{
	
	private static final String TAG = "PessoaAdapter";
	
	public String complemento;
	public String logradouro;
	public String numero;
	public String estadoId;
	public String cidadeId;
	public String bairroId;
	public String pessoaProfissaoId; 
	public String funcionarioSexId; 
	public String nomePessoa;
	public String email;
	public String nomeEmpresa;
	public String tipoEmpresa;
	public Boolean isPessoaDaCorporacao;
	public Boolean isParceiro;
	public Boolean isCliente;
	public Boolean isFornecedor;
	public Boolean isClienteDireto;
	public HashMap<Long, DadosPessoaUsuario> idsPessoasQuePossuemUsuario;
	
	public boolean isUsuarioEditPermitido;
	public boolean isUsuarioRemovePermitido;
	
	private void initPermissaoUsuario() throws OmegaDatabaseException{
		DatabasePontoEletronico db = new DatabasePontoEletronico(activity);
		EXTDAOPermissaoCategoriaPermissao vObj = new EXTDAOPermissaoCategoriaPermissao(db);
		isUsuarioEditPermitido = vObj.hasPermissaoToEdit(EXTDAOUsuario.NAME);
		isUsuarioRemovePermitido = vObj.hasPermissaoToRemove(EXTDAOUsuario.NAME);
		db.close();
	
	}
	
	public PessoaAdapter(
			Activity pActivity,
			Boolean pIsPessoaDaCorporacao,
			Boolean pIsParceiro,
			Boolean pIsCliente,
			Boolean pIsFornecedor,
			Boolean pIsClienteDireto,
			String pLogradouro,
			String pNumero,
			String pComplemento,
			String pBairroId,
			String pCidadeId,
			String pEstadoId,
			String pFuncionarioProfissaoId, 
			String pFuncionarioSexId, 
			String pNomeFuncionario,
			String pNomeEmpresa,
			String pTipoEmpresa,
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, 
			p_isAsc, 
			EXTDAOPessoa.NAME, 
			false, 
			PessoaItemList.NOME, 
			FormPessoaActivityMobile.class,
			FilterPessoaActivityMobile.class);
		Database db = new DatabasePontoEletronico(pActivity);
		idsPessoasQuePossuemUsuario = EXTDAOPessoaUsuario.getAllIdPessoaQuePossuiUsuario(db);
		
		numero =pNumero;
		isClienteDireto = pIsClienteDireto;
		isPessoaDaCorporacao = pIsPessoaDaCorporacao;
		isParceiro = pIsParceiro;
		isFornecedor = pIsFornecedor;
		isCliente = pIsCliente;

		logradouro =pLogradouro;
		complemento = pComplemento;
		estadoId =pEstadoId;
		cidadeId =pCidadeId; 
		bairroId = pBairroId;
		pessoaProfissaoId = pFuncionarioProfissaoId; 
		funcionarioSexId = pFuncionarioSexId; 
		nomePessoa = pNomeFuncionario;
		nomeEmpresa = pNomeEmpresa;
		formularioClass = PessoaAdapter.class;
		tipoEmpresa = pTipoEmpresa;
		initPermissaoUsuario();
		super.initalizeListCustomItemList();
	}
	public boolean existePessoaUsuario(Long idPessoa){
		return idsPessoasQuePossuemUsuario.containsKey(idPessoa); 
	}
	
	public DadosPessoaUsuario getIdUsario(Long idPessoa){
		if(idsPessoasQuePossuemUsuario.containsKey(idPessoa))
			return idsPessoasQuePossuemUsuario.get(idPessoa);
		else
			return null;
	}
	@Override
	public ArrayList<CustomItemList> filtraToken(CharSequence cs){
		if(listaCompleta == null) return null;
		ArrayList<CustomItemList> resultado = new ArrayList<CustomItemList>();
		String csMaiusculo = cs.toString().toUpperCase();
		String csMinusculo= cs.toString().toLowerCase();
		for(int i = 0 ; i < listaCompleta.size(); i++){
			PessoaItemList itemList = (PessoaItemList)listaCompleta.get(i);
			
			String nome = itemList.getConteudoContainerItem(PessoaItemList.NOME);
			if(nome != null && nome.contains(csMaiusculo)){
				resultado.add(itemList);
				continue;
			}
			
			String email = itemList.getConteudoContainerItem(PessoaItemList.EMAIL);
			if(email != null && email.contains(csMinusculo)){
				resultado.add(itemList);
				continue;
			}
			
			String telefone = itemList.getConteudoContainerItem(PessoaItemList.TELEFONE);
			if(telefone != null && telefone.contains(csMaiusculo)){
				resultado.add(itemList);
				continue;
			}
			
			String celular = itemList.getConteudoContainerItem(PessoaItemList.CELULAR);
			if(celular != null && celular.contains(csMaiusculo)){
				resultado.add(itemList);
				continue;
			}
		}
		
		return resultado;
	}
	
	public PessoaAdapter(
			Activity pActivity,
			Boolean pIsPessoaDaCorporacao,
			Boolean pIsParceiro,
			Boolean pIsCliente,
			Boolean pIsFornecedor,
			Boolean pIsClienteDireto,
			String pLogradouro,
			String pNumero,
			String pComplemento,
			String pBairroId,
			String pCidadeId,
			String pEstadoId,
			String pFuncionarioProfissaoId, 
			String pFuncionarioSexId, 
			String pNomeFuncionario,
			String pNomeEmpresa,
			String pTipoEmpresa,
			String pEmail,
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, 
			p_isAsc, 
			EXTDAOPessoa.NAME, 
			false, 
			PessoaItemList.NOME, 
			FormPessoaActivityMobile.class, 
			FilterPessoaActivityMobile.class);
		Database db = new DatabasePontoEletronico(pActivity);
		idsPessoasQuePossuemUsuario = EXTDAOPessoaUsuario.getAllIdPessoaQuePossuiUsuario(db);
		numero =pNumero;
		isClienteDireto = pIsClienteDireto;
		isPessoaDaCorporacao = pIsPessoaDaCorporacao;
		isParceiro = pIsParceiro;
		isFornecedor = pIsFornecedor;
		isCliente = pIsCliente;

		logradouro =pLogradouro;
		complemento = pComplemento;
		estadoId =pEstadoId;
		cidadeId =pCidadeId; 
		bairroId = pBairroId;
		pessoaProfissaoId = pFuncionarioProfissaoId; 
		funcionarioSexId = pFuncionarioSexId; 
		nomePessoa = pNomeFuncionario;
		nomeEmpresa = pNomeEmpresa;
		tipoEmpresa = pTipoEmpresa;
		email = pEmail;
		super.initalizeListCustomItemList();
	}

	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			PessoaItemList itemList = (PessoaItemList)this.getItem(pPosition);
			boolean existeUsuario= existePessoaUsuario(Long.parseLong( itemList.getId()));
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layoutView = vLayoutInflater.inflate(
					R.layout.item_list_layout, null);
//			"@drawable/menu_windows_phone_blue_button"
//			Drawable d = activity.getResources().getDrawable(R.drawable.menu_windows_phone_blue_button);
//			vLayoutView.setBackgroundDrawable(d);

			PersonalizaItemView(layoutView, pPosition);
			
			
				
			if(idsPessoasQuePossuemUsuario.containsKey(Long.parseLong( itemList.getId()) )){
				String strIdPessoa = itemList.getId();
				DadosPessoaUsuario dados = idsPessoasQuePossuemUsuario.get(Long.parseLong(strIdPessoa));
				
				//se o usuario cor da corporacao corrente
				if(dados != null){
					String strIdUsuario = String.valueOf( dados.idUsuario);
					
					
					itemList.setContainerItem(PessoaItemList.NOME, dados.emailUsuario);
					itemList.setContainerItem(PessoaItemList.EMAIL, dados.nomeUsuario);
					
					if(OmegaSecurity.getIdUsuario().equalsIgnoreCase( strIdUsuario)){
						CreateDetailButton(layoutView, 
								ID_CUSTOM_BUTTON_DETAIL, 
								strIdUsuario, 
								OmegaConfiguration.SEARCH_MODE_USUARIO);
					} else{
						if( isUsuarioEditPermitido || isUsuarioRemovePermitido){
							OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
									activity, 
									EXTDAOUsuario.NAME,
									pParent, 
									strIdUsuario,
									FormEditActivityMobile.class, 
									FormUsuarioActivityMobile.class,
									isUsuarioEditPermitido,
									isUsuarioRemovePermitido, 
									itemList);
							CreateDetailButton(layoutView, 
									ID_CUSTOM_BUTTON_DETAIL, 
									strIdUsuario, 
									OmegaConfiguration.SEARCH_MODE_USUARIO, 
									vClickListener);
						}
						else {
							CreateDetailButton(layoutView, 
									ID_CUSTOM_BUTTON_DETAIL, 
									strIdUsuario, 
									OmegaConfiguration.SEARCH_MODE_USUARIO);
						}
						
					}
				}
				
			} else {
				if(isEditPermitido() || isRemovePermitido()){
					OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
							activity, 
							EXTDAOPessoa.NAME,
							pParent, 
							itemList.getId(),
							FormEditActivityMobile.class, 
							FormPessoaActivityMobile.class,
							isEditPermitido(),
							isRemovePermitido(), itemList);
					CreateDetailButton(layoutView, 
							ID_CUSTOM_BUTTON_DETAIL, 
							itemList.getId(), 
							OmegaConfiguration.SEARCH_MODE_PESSOA, 
							vClickListener,
							itemList.getConteudoContainerItem(PessoaItemList.EMAIL));
				}
				else {
					CreateDetailButton(layoutView, 
							ID_CUSTOM_BUTTON_DETAIL, 
							itemList.getId(), 
							OmegaConfiguration.SEARCH_MODE_PESSOA);
				}
			}
			
			
			String nomeFuncionario = itemList.getConteudoContainerItem(PessoaItemList.NOME);
			String email = itemList.getConteudoContainerItem(PessoaItemList.EMAIL);
			if(nomeFuncionario != null){
				
				CreateTextViewHtml(ID_TEXT_VIEW_DESCRICAO_1, 
						HelperString.substring(nomeFuncionario, 18), 
						layoutView);
				CreateTextView(ID_TEXT_VIEW_DESCRICAO_2, 
						HelperString.substring(email, 21), 
						layoutView, 
						true);
			
			} else {
				CreateTextViewHtml(ID_TEXT_VIEW_DESCRICAO_1, 
						itemList.getConteudoContainerItem(PessoaItemList.EMAIL), 
						layoutView);
				
			}			
			
			
			CreateTextView(ID_TEXT_VIEW_DESCRICAO_3,  
					itemList.getConteudoContainerItem(PessoaItemList.CELULAR),
					layoutView, true);
			CreateTextView(ID_TEXT_VIEW_DESCRICAO_4,  
					itemList.getConteudoContainerItem(PessoaItemList.TELEFONE), 
					layoutView, true);
			
			
			HelperFonte.setFontAllView(layoutView);
			return layoutView;
		} catch (Exception exc) {
			SingletonLog.insereErro(exc, TIPO.FORMULARIO);
			
		}
		return null;
	}


	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {
		
		boolean vIsEmpresaInFromClause = false;
		boolean vIsPessoaEmpresaInFromClause = false;
		boolean vIsEmpresaPerfilInFromClause = false;
		boolean vIsEstadoInFromClause = false;

		String querySelectIdPrestadores = "SELECT DISTINCT p."
				+ EXTDAOPessoa.ID + " ";

		String queryFromTables =  " FROM " + EXTDAOPessoa.NAME + " p ";

		String queryWhereClausule = ""; 
		ArrayList<String> args = new ArrayList<String>();

		if(queryWhereClausule.length() ==  0)
			queryWhereClausule += " p." + EXTDAOPessoa.CORPORACAO_ID_INT + " = ? " ;
		else 
			queryWhereClausule += " AND p." + EXTDAOPessoa.CORPORACAO_ID_INT + " = ? " ;

		
		
		args.add(OmegaSecurity.getIdCorporacao());
		
		if(this.logradouro != null ){
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " p." + Attribute.getNomeAtributoNormalizado(EXTDAOPessoa.LOGRADOURO) + " LIKE ? " ;
			else 
				queryWhereClausule += " AND p." + Attribute.getNomeAtributoNormalizado(EXTDAOPessoa.LOGRADOURO) + " LIKE ? " ;

			args.add("%" + HelperString.getWordWithoutAccent(this.logradouro) + "%");
		}
		
		if(this.numero != null ){
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " p." + EXTDAOPessoa.NUMERO + " LIKE ? " ;
			else 
				queryWhereClausule += " AND p." + EXTDAOPessoa.NUMERO + " LIKE ? " ;

			args.add("%" + this.numero + "%");
		}

		if(this.complemento != null ){
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " p." + Attribute.getNomeAtributoNormalizado(EXTDAOPessoa.COMPLEMENTO) + " LIKE ? " ;
			else 
				queryWhereClausule += " AND p." +  Attribute.getNomeAtributoNormalizado(EXTDAOPessoa.COMPLEMENTO)  + " LIKE ? " ;

			args.add("%" +  HelperString.getWordWithoutAccent(this.complemento) + "%");
		}

		if(this.bairroId != null ){
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " p." + EXTDAOPessoa.BAIRRO_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND p." + EXTDAOPessoa.BAIRRO_ID_INT + " = ? " ;

			args.add(this.bairroId);
		}

		if( this.cidadeId != null ){
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " p." + EXTDAOPessoa.CIDADE_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND p." + EXTDAOPessoa.CIDADE_ID_INT + " = ? " ;

			args.add(this.cidadeId);
		}

		if(isClienteDireto != null && isClienteDireto){
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " p." + EXTDAOPessoa.IS_CONSUMIDOR_BOOLEAN + " = ? " ;
			else 
				queryWhereClausule += " AND p." + EXTDAOPessoa.IS_CONSUMIDOR_BOOLEAN + " = ? " ;

			args.add(HelperString.valueOfBooleanSQL( this.isClienteDireto));			
		}

		if(this.funcionarioSexId != null ){
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " p." + EXTDAOPessoa.SEXO_ID_INT + " = ?" ;
			else 
				queryWhereClausule += " AND p." + EXTDAOPessoa.SEXO_ID_INT + " = ?" ;

			args.add(this.funcionarioSexId);
		}

		if(this.email != null ){
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " p." + EXTDAOPessoa.EMAIL + " LIKE ? " ;
			else 
				queryWhereClausule += " AND p." + EXTDAOPessoa.EMAIL + " LIKE ? " ;

			args.add('%' + this.email + '%' );
		}
		
		if(this.nomePessoa != null ){
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " p." + Attribute.getNomeAtributoNormalizado(EXTDAOPessoa.NOME) + " LIKE ? " ;
			else 
				queryWhereClausule += " AND p." + Attribute.getNomeAtributoNormalizado(EXTDAOPessoa.NOME) + " LIKE ? " ;

			args.add('%' + HelperString.getWordWithoutAccent(this.nomePessoa) + '%' );
		}

		if(this.funcionarioSexId != null ){
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " p." + EXTDAOPessoa.SEXO_ID_INT + " = ?" ;
			else 
				queryWhereClausule += " AND p." + EXTDAOPessoa.SEXO_ID_INT + " = ?" ;

			args.add(this.funcionarioSexId);
		}		

//		Se for consulta para funcionarios
		if((this.isPessoaDaCorporacao != null && isPessoaDaCorporacao ) &&
				isParceiro == null &&
				isFornecedor == null &&
				isCliente == null){
			if(!vIsPessoaEmpresaInFromClause){
				queryFromTables +=  " JOIN " + EXTDAOPessoaEmpresa.NAME + " pe ON pe." + EXTDAOPessoaEmpresa.PESSOA_ID_INT + " = p." + EXTDAOPessoa.ID + " ";
				vIsPessoaEmpresaInFromClause = true;
			}
			if(!vIsEmpresaInFromClause){				
				queryFromTables +=  " JOIN " + EXTDAOEmpresa.NAME + " e ON pe." + EXTDAOPessoaEmpresa.EMPRESA_ID_INT + " = e." + EXTDAOEmpresa.ID + " ";
				vIsEmpresaInFromClause = true;
			}
			if(!vIsEmpresaPerfilInFromClause){
				queryFromTables +=  " JOIN " + EXTDAOEmpresaPerfil.NAME + " ep ON ep." + EXTDAOEmpresaPerfil.EMPRESA_ID_INT + " = e." + EXTDAOEmpresa.ID + " ";
				vIsEmpresaPerfilInFromClause = true;
			}

			if(queryWhereClausule.length() >  0)
				queryWhereClausule += " AND ";
			if(isPessoaDaCorporacao)
				queryWhereClausule += " ep." + EXTDAOEmpresaPerfil.PERFIL_ID_INT + " = ?" ;
			else
				queryWhereClausule += " ep." + EXTDAOEmpresaPerfil.PERFIL_ID_INT + " != ? OR ep." + EXTDAOEmpresaPerfil.PERFIL_ID_INT + " IS NULL" ;
			args.add(EXTDAOPerfil.ID_STR_EMPRESA_CORPORACAO );
		} else{
//			LEFT JOIN pois todo contato inclusicve os que sao consumidores devem ser exibidos na consulta
			if(isPessoaDaCorporacao == null) isPessoaDaCorporacao = false;
			if(isParceiro == null) isParceiro = false;
			if(isCliente == null) isCliente = false;
			if(isFornecedor == null) isFornecedor = false;

			if(isPessoaDaCorporacao || isParceiro || isCliente || isFornecedor){
				if(!vIsPessoaEmpresaInFromClause){
					queryFromTables +=  " LEFT JOIN " + EXTDAOPessoaEmpresa.NAME + " pe ON pe." + EXTDAOPessoaEmpresa.PESSOA_ID_INT + " = p." + EXTDAOPessoa.ID + " ";
					vIsPessoaEmpresaInFromClause = true;
				}
				if(!vIsEmpresaInFromClause){				
					queryFromTables +=  " LEFT JOIN " + EXTDAOEmpresa.NAME + " e ON pe." + EXTDAOPessoaEmpresa.EMPRESA_ID_INT + " = e." + EXTDAOEmpresa.ID + " ";
					vIsEmpresaInFromClause = true;
				}
				if(!vIsEmpresaPerfilInFromClause){
					queryFromTables +=  " LEFT JOIN " + EXTDAOEmpresaPerfil.NAME + " ep ON ep." + EXTDAOEmpresaPerfil.EMPRESA_ID_INT + " = e." + EXTDAOEmpresa.ID + " ";
					vIsEmpresaPerfilInFromClause = true;
				}

				String queryWherePerfil = "";
				Boolean vVetorIsPerfilSelected[] = new Boolean[]{
						isPessoaDaCorporacao, 
						isParceiro, 
						isCliente, 
						isFornecedor};
				String vVetorIdPerfil[] = new String[]{
						EXTDAOPerfil.ID_STR_EMPRESA_CORPORACAO,
						EXTDAOPerfil.ID_STR_PARCEIRO, 
						EXTDAOPerfil.ID_STR_CLIENTE, 
						EXTDAOPerfil.ID_STR_FORNECEDOR};
				for(int i = 0 ; i < vVetorIsPerfilSelected.length; i++){
					Boolean vIsPerfilSelected = vVetorIsPerfilSelected[i];
					if(vIsPerfilSelected != null && vIsPerfilSelected){
						String vIdPerfil = vVetorIdPerfil[i];
						if(queryWherePerfil.length() > 0 )
							queryWherePerfil += " OR ep." + EXTDAOEmpresaPerfil.PERFIL_ID_INT + " = ?" ;
						else queryWherePerfil += " ep." + EXTDAOEmpresaPerfil.PERFIL_ID_INT + " = ?" ;

						args.add(vIdPerfil);
					}
				}
				if(queryWhereClausule.length() >  0)
					queryWhereClausule += " AND ";
				if(queryWherePerfil.length() > 0 )
					queryWhereClausule += " (" + queryWherePerfil + " ) " ;
			}
		}

		//Se o cidade tiver sido setado entao nao ha a necessidade de setar o estado
		if( this.estadoId != null && this.cidadeId == null && this.bairroId == null){
			if(!vIsEstadoInFromClause){
				queryFromTables +=  " JOIN " + EXTDAOCidade.NAME + " c ON c." + EXTDAOPessoa.CIDADE_ID_INT + " = c." + EXTDAOCidade.ID + " " +
						" JOIN " + EXTDAOUf.NAME + " uf ON c." + EXTDAOCidade.UF_ID_INT + " = uf." + EXTDAOUf.ID + " " ;
				vIsEstadoInFromClause = true;
			}
			
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " uf." + EXTDAOUf.ID + " = ? " ;
			else 
				queryWhereClausule += " AND uf." + EXTDAOUf.ID + " = ? " ;

			args.add(this.estadoId);
			vIsEstadoInFromClause = true;
		}

		if(this.nomeEmpresa != null){
			if(!vIsPessoaEmpresaInFromClause){
				queryFromTables +=  " JOIN " + EXTDAOPessoaEmpresa.NAME + " pe ON pe." + EXTDAOPessoaEmpresa.PESSOA_ID_INT + " = p." + EXTDAOPessoa.ID + " ";
				vIsPessoaEmpresaInFromClause = true;
			}
			if(!vIsEmpresaInFromClause){				
				queryFromTables +=  " JOIN " + EXTDAOEmpresa.NAME + " e ON pe." + EXTDAOPessoaEmpresa.EMPRESA_ID_INT + " = e." + EXTDAOEmpresa.ID + " ";
				vIsEmpresaInFromClause = true;
			}
			if(queryWhereClausule.length() >  0)
				queryWhereClausule += " AND "; 
			queryWhereClausule += " e." + Attribute.getNomeAtributoNormalizado(EXTDAOEmpresa.NOME) + " LIKE ?" ;
			args.add("%" + HelperString.getWordWithoutAccent(this.nomeEmpresa) + "%");
		}

		if(this.tipoEmpresa != null){
			if(!vIsPessoaEmpresaInFromClause){
				queryFromTables +=  " JOIN " + EXTDAOPessoaEmpresa.NAME + " pe ON pe." + EXTDAOPessoaEmpresa.PESSOA_ID_INT + " = p." + EXTDAOPessoa.ID + " ";
				vIsPessoaEmpresaInFromClause = true;
			}
			if(!vIsEmpresaInFromClause){				
				queryFromTables +=  " JOIN " + EXTDAOEmpresa.NAME + " e ON pe." + EXTDAOPessoaEmpresa.EMPRESA_ID_INT + " = e." + EXTDAOEmpresa.ID + " ";
				vIsEmpresaInFromClause = true;
			}
			if(queryWhereClausule.length() >  0)
				queryWhereClausule += " AND ";
			queryWhereClausule += " e." + EXTDAOEmpresa.TIPO_EMPRESA_ID_INT + " = ? " ;
			args.add(this.tipoEmpresa );
		}
		
		if(this.pessoaProfissaoId != null){
			if(!vIsPessoaEmpresaInFromClause){
				queryFromTables +=  " JOIN " + EXTDAOPessoaEmpresa.NAME + " pe ON pe." + EXTDAOPessoaEmpresa.PESSOA_ID_INT + " = p." + EXTDAOPessoa.ID + " ";
				vIsPessoaEmpresaInFromClause = true;
			}
			if(queryWhereClausule.length() >  0)
				queryWhereClausule += " AND ";
			queryWhereClausule += " pe." + EXTDAOPessoaEmpresa.PROFISSAO_ID_INT + " = ?" ;
			args.add(this.pessoaProfissaoId);
		}
		

		
		String orderBy = "";
		if (!pIsAsc) {
			orderBy += " ORDER BY p." + Attribute.getNomeAtributoNormalizado(EXTDAOPessoa.NOME)
					+ " DESC";
		} else {
			orderBy += " ORDER BY  p." + Attribute.getNomeAtributoNormalizado(EXTDAOPessoa.NOME)
					+ " ASC";
		}

		String query = querySelectIdPrestadores + queryFromTables;
		if(queryWhereClausule.length() > 0 )
			query 	+= " WHERE " + queryWhereClausule;
		if(orderBy.length() > 0 ){
			query 	+= orderBy;
		}
		String[] vetorArg = new String [args.size()];
		args.toArray(vetorArg);
		
		DatabasePontoEletronico database = null;
		Cursor cursor = null;
		try{
			database = new DatabasePontoEletronico(this.activity);
			
			
			
			
			cursor = database.rawQuery(query, vetorArg);
	
			String[] chaves = new String[] { EXTDAOPessoa.ID };
	
			ArrayList<Long> idsResultado = HelperDatabase.convertCursorToArrayListId(cursor, chaves);
			if(idsResultado != null && idsResultado.size() > 0){
				Iterator<Long> it = idsResultado.iterator();
				while(it.hasNext()){
					Long idPessoa = it.next();
					if(idsPessoasQuePossuemUsuario.containsKey(idPessoa) ){
						DadosPessoaUsuario dados = idsPessoasQuePossuemUsuario.get(idPessoa);
						//o hashmap ids contem nulo no caso de usuarios que estao no cadastro mas nuo fazem parte
						//do grupo logado
						if(dados == null){
							it.remove();
						}
					}	
				}
			}
			return idsResultado;
		} catch(Exception ex){
			if(ex != null)
				Log.e(TAG, ex.getMessage());
			return null;
		} finally{
			if(cursor != null && ! cursor.isClosed()){
				cursor.close();
			}
			if(database != null)
				database.close();
		}
	
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {

		EXTDAOPessoa vEXTDAOPessoa = (EXTDAOPessoa) pObj; 
		

		Attribute vAttrId = pObj.getAttribute(EXTDAOPessoa.ID);
		String vId = null;

		if(vAttrId != null){
			vId = vAttrId.getStrValue();
		}
		if(vId == null){
			return null;
		}

		PessoaItemList vItemList = new PessoaItemList(  
				vEXTDAOPessoa.getAttribute(EXTDAOPessoa.ID).getStrValue(),
				vEXTDAOPessoa.getAttribute(EXTDAOPessoa.IDENTIFICADOR).getStrValue(),
				vEXTDAOPessoa.getAttribute(EXTDAOPessoa.NOME).getStrValue(),
				vEXTDAOPessoa.getAttribute(EXTDAOPessoa.SEXO_ID_INT).getStrValue(),   
				null,
				null, 
				null,
				vEXTDAOPessoa.getAttribute(EXTDAOPessoa.EMAIL).getStrValue(),
				vEXTDAOPessoa.getAttribute(EXTDAOPessoa.TELEFONE).getStrValue(),
				vEXTDAOPessoa.getAttribute(EXTDAOPessoa.CELULAR).getStrValue(),
				null);

		return vItemList;
	}


}
