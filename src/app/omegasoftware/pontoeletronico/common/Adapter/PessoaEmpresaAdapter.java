package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormPessoaEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.PessoaItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.ResultSet;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaUsuario;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class PessoaEmpresaAdapter extends DatabaseCustomAdapter{
	
	//private static final String TAG = "PessoaEmpresaAdapter";
	private static final int ID_TEXT_VIEW_PESSOA_ID = R.id.id_textview;
	private static final int ID_TEXT_VIEW_NAO_SINCRONIZADO = R.id.texto_nao_sincronizado_textview;
	private static final int ID_TEXT_VIEW_FUNCIONARIO_NAME = R.id.funcionario_name;

	private static final int ID_TEXT_VIEW_FUNCIONARIO_IDENTIFICADOR = R.id.funcionario_identificador;
	private static final int ID_TEXT_VIEW_EMAIL = R.id.email_textview;
	private String PREFIXO_NAO_SINCRONIZADO = null;
//	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;
	final int LENGTH_TEXTO = 17;
	final int LENGTH_EMAIL = 25;
	String empresaId; 
	String nomePessoa;
	ResultSet rs;
	boolean selecionarId = false;
	public PessoaEmpresaAdapter(
			Activity pActivity,
			String pEmpresaId, 
			String pNomePessoa,
			boolean p_isAsc,
			boolean selecionarId) throws OmegaDatabaseException
	{
		super(
				pActivity , 
				p_isAsc, 
				EXTDAOPessoa.NAME, 
				false, 
				PessoaItemList.NOME,
				FormPessoaActivityMobile.class,
				null);
		PREFIXO_NAO_SINCRONIZADO = pActivity.getString(R.string.prefixoNaoSincronizado);
		empresaId = pEmpresaId;
		nomePessoa = pNomePessoa;
		this.selecionarId=selecionarId;
		super.initalizeListCustomItemList();
	}
	
	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			final PessoaItemList vItemList = (PessoaItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.pessoa_empresa_item_list_layout, null);

			PersonalizaItemView(vLayoutView, pPosition);
			
			String nomeFuncionario = vItemList.getConteudoContainerItem(PessoaItemList.NOME);
			 
			
			if(vItemList.getId().startsWith("-")){
//				TextView vTextViewIdPessoa = CreateTextView(ID_TEXT_VIEW_PESSOA_ID,
//						vItemList.getId(), 
//						vLayoutView,
//						true);
//				vTextViewIdPessoa.setTextColor(R.color.red);
				String idFormatado = vItemList.getId().replace("-", PREFIXO_NAO_SINCRONIZADO);
				CreateTextView(ID_TEXT_VIEW_PESSOA_ID, idFormatado, vLayoutView, true);
			}
			else {
				((TextView) vLayoutView.findViewById(ID_TEXT_VIEW_NAO_SINCRONIZADO)).setVisibility(View.GONE);
				
				CreateTextView(ID_TEXT_VIEW_PESSOA_ID, vItemList.getId(), vLayoutView, true);
			}
			
			if(nomeFuncionario != null)
				CreateTextView(ID_TEXT_VIEW_FUNCIONARIO_NAME, 
						HelperString.substring( nomeFuncionario, LENGTH_TEXTO), 
						vLayoutView);
			
			
			CreateTextView(
					ID_TEXT_VIEW_FUNCIONARIO_IDENTIFICADOR,
					HelperString.substring( vItemList.getConteudoContainerItem(PessoaItemList.IDENTIFICADOR), LENGTH_EMAIL), 
					vLayoutView, 
					true);

			CreateTextView(ID_TEXT_VIEW_EMAIL,  HelperString.substring( vItemList.getConteudoContainerItem(PessoaItemList.EMAIL), LENGTH_EMAIL), vLayoutView, true);
			String idAction = vItemList.getId();
			String idUsuario = vItemList.getConteudoContainerItem(PessoaItemList.USUARIO);
			short action = OmegaConfiguration.SEARCH_MODE_PESSOA;
			if(idUsuario != null && idUsuario != null && idUsuario.length() >0){
				action = OmegaConfiguration.SEARCH_MODE_USUARIO;
				idAction = idUsuario;
			}
			
			if(this.selecionarId){
				
				final Activity a= this.activity;
				OnClickListener onClick1 = new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						Intent intent = a.getIntent();
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA, vItemList.getId());
						a.setResult(Activity.RESULT_OK, intent);
						a.finish();
						
					}
				};
				vLayoutView.setOnClickListener(onClick1);
			} else {
				if(isEditPermitido() || isRemovePermitido()){
					OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
							activity, 
							EXTDAOPessoa.NAME,
							pParent, 
							vItemList.getId(),
							FormEditActivityMobile.class, 
							FormPessoaActivityMobile.class,
							isEditPermitido(),
							isRemovePermitido(), vItemList);
					CreateDetailView(vLayoutView,
							idAction, 
							action, 
							vClickListener);
				}
				else {
					CreateDetailView(vLayoutView, 
							idAction, 
							action,
							null);
				}
			}
			
			
			return vLayoutView;
		} catch (Exception exc) {
			SingletonLog.insereErro(exc, TIPO.ADAPTADOR);
		}
		return null;
	}


	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {
		
		boolean vIsPessoaEmpresaInFromClause = false;

		String querySelectIdPrestadores = "SELECT DISTINCT " 
				+ " p."+ EXTDAOPessoa.ID + ", " 
				+ " pu." + EXTDAOPessoaUsuario.USUARIO_ID_INT ;

		String queryFromTables =  " FROM " + EXTDAOPessoa.NAME + " p ";

		String queryWhereClausule = ""; 
		ArrayList<String> args = new ArrayList<String>();

		if(queryWhereClausule.length() ==  0)
			queryWhereClausule += " p." + EXTDAOPessoa.CORPORACAO_ID_INT + " = ? " ;
		else 
			queryWhereClausule += " AND p." + EXTDAOPessoa.CORPORACAO_ID_INT + " = ? " ;

		args.add(OmegaSecurity.getIdCorporacao());
			
		if(this.nomePessoa != null ){
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " p." + Attribute.getNomeAtributoNormalizado(EXTDAOPessoa.NOME) + " LIKE ? " ;
			else 
				queryWhereClausule += " AND p." + Attribute.getNomeAtributoNormalizado(EXTDAOPessoa.NOME) + " LIKE ? " ;

			args.add('%' + HelperString.getWordWithoutAccent(this.nomePessoa) + '%' );
		}

		if(this.empresaId != null){
			if(!vIsPessoaEmpresaInFromClause){
				queryFromTables +=  " JOIN " + EXTDAOPessoaEmpresa.NAME + " pe " +
						" ON pe." + EXTDAOPessoaEmpresa.PESSOA_ID_INT + " = p." + EXTDAOPessoa.ID + " ";
				vIsPessoaEmpresaInFromClause = true;
			}
			if(queryWhereClausule.length() >  0)
				queryWhereClausule += " AND ";
			queryWhereClausule += " pe." + EXTDAOPessoaEmpresa.EMPRESA_ID_INT + " = ?" ;
			args.add(this.empresaId);
		}
		
		queryFromTables +=  " LEFT JOIN " + EXTDAOPessoaUsuario.NAME + " pu " +
				" ON pu." + EXTDAOPessoaUsuario.PESSOA_ID_INT + " = p." + EXTDAOPessoa.ID + " ";
		
		String orderBy = "";
		if (!pIsAsc) {
			orderBy += " ORDER BY p." + Attribute.getNomeAtributoNormalizado(EXTDAOPessoa.NOME)
					+ " DESC";
		} else {
			orderBy += " ORDER BY  p." + Attribute.getNomeAtributoNormalizado(EXTDAOPessoa.NOME)
					+ " ASC";
		}

		String query = querySelectIdPrestadores + queryFromTables;
		if(queryWhereClausule.length() > 0 )
			query 	+= " WHERE " + queryWhereClausule;
		if(orderBy.length() > 0 ){
			query 	+= orderBy;
		}
		String[] vetorArg = new String [args.size()];
		args.toArray(vetorArg);
		
		DatabasePontoEletronico database = null;
		try{
			database = new DatabasePontoEletronico(this.activity);
	
			try{
				rs = database.getResultSet(query, vetorArg);
				ArrayList<Long> ids = rs.getValoresLongDoAtributo(EXTDAOPessoa.ID);
				
				return ids;
				
			}catch(Exception ex){
				SingletonLog.insereErro(
						ex, 
						SingletonLog.TIPO.BANCO);
				return null;
			}
			
			
		} catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.ADAPTADOR);
			return null;
		} finally{
			if(database != null)
				database.close();
		}

	}
	
	@Override
	public ArrayList<CustomItemList> filtraToken(CharSequence cs){
		if(listaCompleta == null) return null;
		ArrayList<CustomItemList> resultado = new ArrayList<CustomItemList>();
		String csMaiusculo = cs.toString().toUpperCase();
		String csMinusculo= cs.toString().toLowerCase();
		for(int i = 0 ; i < listaCompleta.size(); i++){
			PessoaItemList itemList = (PessoaItemList)listaCompleta.get(i);
			
			String nome = itemList.getConteudoContainerItem(PessoaItemList.NOME);
			if(nome != null && nome.contains(csMaiusculo)){
				resultado.add(itemList);
				continue;
			}
			
			String email = itemList.getConteudoContainerItem(PessoaItemList.EMAIL);
			if(email != null && email.contains(csMinusculo)){
				resultado.add(itemList);
				continue;
			}
			
			
			String id = itemList.getId();
			if(id != null && id.contains(csMaiusculo)){
				resultado.add(itemList);
				continue;
			}
		}
		
		return resultado;
	}
	

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {

		EXTDAOPessoa objPessoa = (EXTDAOPessoa) pObj;
		
		String idUsuario = rs.findRegistro(EXTDAOPessoa.ID, pObj.getId(), EXTDAOPessoaUsuario.USUARIO_ID_INT);

		PessoaItemList vItemList = new PessoaItemList(  
				objPessoa.getAttribute(EXTDAOPessoa.ID).getStrValue(),
				objPessoa.getAttribute(EXTDAOPessoa.IDENTIFICADOR).getStrValue(),
				objPessoa.getAttribute(EXTDAOPessoa.NOME).getStrValue(),
				objPessoa.getAttribute(EXTDAOPessoa.SEXO_ID_INT).getStrValue(),   
				null,
				null, 
				null,
				objPessoa.getAttribute(EXTDAOPessoa.EMAIL).getStrValue(),
				objPessoa.getAttribute(EXTDAOPessoa.TELEFONE).getStrValue(),
				objPessoa.getAttribute(EXTDAOPessoa.CELULAR).getStrValue(),
				idUsuario);

		return vItemList;
	}


}
