package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.PessoaEmpresaRotinaItemList;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresaRotina;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;

public class PessoaEmpresaRotinaAdapter extends DatabaseCustomAdapter{
	
	private static final String TAG = "PessoaEmpresaRotinaAdapter";
	private static final int ID_TEXT_VIEW_ENDERECO_EMPRESA = R.id.endereco_empresa_textview;
	private static final int ID_TEXT_VIEW_EMPRESA = R.id.empresa_textview;
	private static final int ID_TEXT_VIEW_PROFISSAO = R.id.profissao_textview;
	
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;

	String empresaId; 
	String nomePessoa;
	
	public PessoaEmpresaRotinaAdapter(
			Activity pActivity,
			String pEmpresaId, 
			String pNomePessoa,
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, p_isAsc, EXTDAOPessoaEmpresaRotina.NAME, false, PessoaEmpresaRotinaItemList.EMPRESA);
		empresaId = pEmpresaId;
		nomePessoa = pNomePessoa;
		
		super.initalizeListCustomItemList();
	}
	
	@SuppressLint("ViewHolder")
	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			PessoaEmpresaRotinaItemList vItemList = (PessoaEmpresaRotinaItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.pessoa_empresa_rotina_item_list_layout, null);
			if((pPosition % 2) == 0)
			{
				((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.color.gray_189);
			}
			else
			{
				((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.color.white);
			}
			String vNomePessoa = vItemList.getConteudoContainerItem(PessoaEmpresaRotinaItemList.ENDERECO_EMPRESA);
			
			CreateTextView(ID_TEXT_VIEW_ENDERECO_EMPRESA, 
					vNomePessoa, 
					vLayoutView);
			
			String vNomeEmpresa = vItemList.getConteudoContainerItem(PessoaEmpresaRotinaItemList.EMPRESA);
			
			CreateTextView(ID_TEXT_VIEW_EMPRESA, 
					vNomeEmpresa, 
					vLayoutView);
			
			String vNomeProfissao = vItemList.getConteudoContainerItem(PessoaEmpresaRotinaItemList.PROFISSAO);
			
			CreateTextView(ID_TEXT_VIEW_PROFISSAO, 
					vNomeProfissao, 
					vLayoutView);

			CreateDetailButton(vLayoutView, 
					ID_CUSTOM_BUTTON_DETAIL, 
					vItemList.getId(), 
					OmegaConfiguration.SEARCH_MODE_PESSOA_EMPRESA_ROTINA);
			
			return vLayoutView;
		} catch (Exception exc) {
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
					"FuncionarioAdapter: getView()");
		}
		return null;
	}


	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {
				
		DatabasePontoEletronico database = null;
		try{
			database = new DatabasePontoEletronico(activity);
			
			EXTDAOPessoaEmpresaRotina vObjPER = new EXTDAOPessoaEmpresaRotina(database);
			return vObjPER.getListaIdTuplaDoDiaDoCicloAtual();
		} catch(Exception ex){
			if(ex.getMessage() == EXTDAOPessoaEmpresaRotina.ERROR_SEU_USUARIO_NAO_ESTA_LINKADO_A_NENHUMA_PESSOA_DO_BANCO)
				Log.e(TAG, "Seu usuurios nuo possui pessoa definida no banco de dados");
			else if(ex != null)
				Log.e(TAG, ex.getMessage());
			return null;
		} finally{
			if(database != null)
				database.close();
		}

	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {
try{
		EXTDAOPessoaEmpresaRotina vObj = (EXTDAOPessoaEmpresaRotina) pObj; 
		
		vObj.getNomeDaChaveExtrangeira(EXTDAOPessoaEmpresa.EMPRESA_ID_INT);
		String vIdProfissao = vObj.getValorDoAtributoDaChaveExtrangeira(EXTDAOPessoaEmpresaRotina.PESSOA_EMPRESA_ID_INT, EXTDAOPessoaEmpresa.PROFISSAO_ID_INT);
		String vIdEmpresa = vObj.getValorDoAtributoDaChaveExtrangeira(EXTDAOPessoaEmpresaRotina.PESSOA_EMPRESA_ID_INT, EXTDAOPessoaEmpresa.EMPRESA_ID_INT);
//		String vIdPessoa = vObj.getValorDoAtributoDaChaveExtrangeira(EXTDAOPessoaEmpresaRotina.PESSOA_EMPRESA_ID_INT, EXTDAOPessoaEmpresa.PESSOA_ID_INT);
		
		EXTDAOProfissao vObjProfissao = new EXTDAOProfissao(pObj.getDatabase());
		String vNomeProfissao = null;
		if(vObjProfissao.select(vIdProfissao)){
			vNomeProfissao = vObjProfissao.getStrValueOfAttribute(EXTDAOProfissao.NOME); 
		}
		
		EXTDAOEmpresa vObjEmpresa = new EXTDAOEmpresa(pObj.getDatabase());
		String vNomeEmpresa = null;
		if(vObjEmpresa.select(vIdEmpresa)){
			vNomeEmpresa = vObjEmpresa.getStrValueOfAttribute(EXTDAOEmpresa.NOME); 
		}
		
		
	

		PessoaEmpresaRotinaItemList vItemList = new PessoaEmpresaRotinaItemList(
				pObj.getStrValueOfAttribute(EXTDAOPessoaEmpresaRotina.ID),
				vObjEmpresa.getEndereco(activity),
				vNomeEmpresa,
				vNomeProfissao);

		return vItemList;
}catch(Exception ex){
	SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
return null;
}
	}


}
