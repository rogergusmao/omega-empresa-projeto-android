package app.omegasoftware.pontoeletronico.common.Adapter;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class PessoaPontoAdapter {
	
//	private static final String TAG = "PessoaPontoAdapter";
	
	Activity activity;
	EXTDAOPonto.PontoPessoa pontoPessoa;
	public PessoaPontoAdapter(
			Activity pActivity,
			EXTDAOPonto.PontoPessoa pontoPessoa)
	{
		this.activity = pActivity;
		this.pontoPessoa = pontoPessoa;
	}

	final int LIMIT_CHARS = 17;
	
	public void formatarView(View viewUsuario)  {
		try {			
			
			View vPessoa=viewUsuario.findViewById(R.id.tv_pessoa);
			if(vPessoa != null ){
				TextView tvPessoa = (TextView)vPessoa;
				if(pontoPessoa.pessoa != null 
						&& pontoPessoa.pessoa.length() > 0){
					tvPessoa.setText(HelperString.substring(pontoPessoa.pessoa, LIMIT_CHARS));
					tvPessoa.setVisibility(View.VISIBLE);
				} else {
					tvPessoa.setVisibility(View.GONE);
				}	
			}
			
			View vEmpresa=viewUsuario.findViewById(R.id.tv_empresa);
			if(vEmpresa != null ){
				TextView tvEmpresa= (TextView)vEmpresa;
				
				if(pontoPessoa.empresa != null 
						&& pontoPessoa.empresa.length() > 0){
					tvEmpresa.setText(HelperString.substring( pontoPessoa.empresa, LIMIT_CHARS));
					tvEmpresa.setVisibility(View.VISIBLE);
				} else {
					tvEmpresa.setVisibility(View.GONE);
				}	
			}
			
			View vProfissao = viewUsuario.findViewById(R.id.tv_profissao);
			if(vProfissao != null ){
				TextView tvProfissao= (TextView)vProfissao;
				
				if(pontoPessoa.profissao != null 
						&& pontoPessoa.profissao.length() > 0){
					tvProfissao.setText(HelperString.substring(pontoPessoa.profissao, LIMIT_CHARS));
					tvProfissao.setVisibility(View.VISIBLE);
				} else {
					tvProfissao.setVisibility(View.GONE);
				}	
			}
			
			if(pontoPessoa.intervalo != null){
				
				if(pontoPessoa.intervalo.entrada != null){
					((TextView) viewUsuario.findViewById(R.id.tv_data_ponto_intervalo_entrada)).setText(
							pontoPessoa.intervalo.entrada.data + " " + pontoPessoa.intervalo.entrada.hora);	
				} else {
					((TextView) viewUsuario.findViewById(R.id.tv_data_ponto_intervalo_entrada)).setText("-");
				}
				if(pontoPessoa.intervalo.saida != null){
					((TextView) viewUsuario.findViewById(R.id.tv_data_ponto_intervalo_saida)).setText(
							pontoPessoa.intervalo.saida.data + " " + pontoPessoa.intervalo.saida.hora);	
				} else {
					((TextView) viewUsuario.findViewById(R.id.tv_data_ponto_intervalo_saida)).setText("-");
				}
			}
			
			if(pontoPessoa.turno != null){
				
				if(pontoPessoa.turno.entrada != null){
					((TextView) viewUsuario.findViewById(R.id.tv_data_ponto_turno_entrada)).setText(
							pontoPessoa.turno.entrada.data + " " + pontoPessoa.turno.entrada.hora);	
				} else {
					((TextView) viewUsuario.findViewById(R.id.tv_data_ponto_turno_entrada)).setText("-");
				}
				if(pontoPessoa.turno.saida != null){
					((TextView) viewUsuario.findViewById(R.id.tv_data_ponto_turno_saida)).setText(
							pontoPessoa.turno.saida.data + " " + pontoPessoa.turno.saida.hora);	
				} else {
					((TextView) viewUsuario.findViewById(R.id.tv_data_ponto_turno_saida)).setText("-");
				}
			}
			
		} catch (Exception exc) {
			SingletonLog.insereErro(exc, TIPO.FORMULARIO);
			
		}
		
	}



}
