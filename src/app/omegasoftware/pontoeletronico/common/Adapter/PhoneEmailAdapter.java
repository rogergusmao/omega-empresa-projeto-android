package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.listener.EmailClickListener;
import app.omegasoftware.pontoeletronico.listener.PhoneClickListener;

public class PhoneEmailAdapter extends BaseAdapter {

	public static final int EMAIL_TYPE = 0;
	public static final int PHONE_TYPE = 1;
	
	Context context;
	ArrayList<String> data;
	int type;
	
	public PhoneEmailAdapter(Context context, ArrayList<String> data, int type)
	{
		this.context = context;
		this.data = data;
		this.type = type;
		
		
		
	}
	public PhoneEmailAdapter(Context context, String data, int type)
	{
		this.context = context;
		this.data = new ArrayList<String>();
		this.data.add(data);
		this.type = type;
		
		
		
	}

	public int getCount() {
		return this.data.size();
	}

	public String getItem(int position) {
		return this.data.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		
		String value = this.getItem(position);
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout.LayoutParams parametrosPai = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		
		parametrosPai.setMargins(5, 5, 5, 5);
		View layoutView = null;
		switch(this.type)
		{
			case PhoneEmailAdapter.EMAIL_TYPE:
				layoutView = layoutInflater.inflate(R.layout.email_button, null);
				break;
			case PhoneEmailAdapter.PHONE_TYPE:
				layoutView = layoutInflater.inflate(R.layout.phone_button, null);
				
				break;
		}
		
		layoutView.setLayoutParams(parametrosPai);
		TextView valueButton = (TextView) layoutView.findViewById(R.id.tv_identificador_email_ou_telefone);
		
		valueButton.setText(value);
		
		
		switch(this.type)
		{
		case PhoneEmailAdapter.EMAIL_TYPE:
			layoutView.setOnClickListener(new EmailClickListener(R.id.tv_identificador_email_ou_telefone));
			valueButton.setOnClickListener(new EmailClickListener(R.id.tv_identificador_email_ou_telefone));
			break;
		case PhoneEmailAdapter.PHONE_TYPE:
			layoutView.setOnClickListener(new PhoneClickListener(R.id.tv_identificador_email_ou_telefone));
			valueButton.setOnClickListener(new PhoneClickListener(R.id.tv_identificador_email_ou_telefone));
			break;
		}
		
		return layoutView;
	}
	
}