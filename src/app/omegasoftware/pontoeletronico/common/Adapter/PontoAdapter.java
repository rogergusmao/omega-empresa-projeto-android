package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.PontoItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorAndroidParaWeb;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoPonto;
import app.omegasoftware.pontoeletronico.primitivetype.HelperBoolean;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class PontoAdapter extends DatabaseCustomAdapter {

	private static final int ID_TEXT_VIEW_EMPRESA = R.id.empresa_textview;
	private static final int ID_TEXT_VIEW_TIPO_PONTO = R.id.tipo_ponto_textview;
	private static final int ID_TEXT_VIEW_DATA = R.id.data_textview;
	private static final int ID_TEXT_VIEW_PESSOA = R.id.pessoa_textview;
	private static final int ID_TEXT_VIEW_PROFISSAO = R.id.profissao_textview;
	private static final int ID_TEXT_VIEW_ENTRADA_OU_SAIDA = R.id.entrada_saida_textview;
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.appointment_place_showmap_button;

	public String empresa;
	public String dataInicial;
	public String dataFinal;
	public String pessoa;

	public PontoAdapter(Activity pActivity, String empresa, String dataInicial, String dataFinal, String pessoa,
			boolean p_isAsc) throws OmegaDatabaseException {
		super(pActivity, p_isAsc, EXTDAOPonto.NAME, PontoItemList.DATA, false);

		this.empresa = empresa;
		this.dataInicial = dataInicial;
		this.dataFinal = dataFinal;
		this.pessoa = pessoa;

		super.initalizeListCustomItemList();
	}

	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {
			PontoItemList vItemList = (PontoItemList) this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(R.layout.item_list_ponto_layout, null);

			PersonalizaItemView(vLayoutView, pPosition);

			CreateTextView(ID_TEXT_VIEW_EMPRESA,
					HelperString.ucFirstForEachToken(vItemList.getConteudoContainerItem(PontoItemList.EMPRESA)),
					vLayoutView);

			CreateTextView(ID_TEXT_VIEW_PROFISSAO,
					HelperString.ucFirstForEachToken(vItemList.getConteudoContainerItem(PontoItemList.PROFISSAO)),
					vLayoutView);

			CreateTextView(ID_TEXT_VIEW_PESSOA,
					HelperString.ucFirstForEachToken(vItemList.getConteudoContainerItem(PontoItemList.PESSOA)),
					vLayoutView);

			CreateTextView(ID_TEXT_VIEW_TIPO_PONTO, vItemList.getConteudoContainerItem(PontoItemList.TIPO_PONTO),
					vLayoutView);

			CreateTextView(ID_TEXT_VIEW_DATA, vItemList.getConteudoContainerItem(PontoItemList.DATA), vLayoutView);

			CreateTextView(ID_TEXT_VIEW_ENTRADA_OU_SAIDA, vItemList.getStrIsEntrada(activity), vLayoutView);

			CreateDetailButton(vLayoutView, ID_CUSTOM_BUTTON_DETAIL, vItemList.getId(),
					OmegaConfiguration.SEARCH_MODE_MAPA_PONTO);

			return vLayoutView;
		} catch (Exception exc) {
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, "PontoAdapter: getView()");
		}
		return null;
	}

	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) throws OmegaDatabaseException {

		DatabasePontoEletronico db = null;

		try {
			db = new DatabasePontoEletronico(this.activity);
			String querySelect = "SELECT DISTINCT p." + EXTDAOPonto.ID + " ";

			String queryFromTables = "FROM " + EXTDAOPonto.NAME + " p ";

			String queryWhereClausule = "";
			ArrayList<String> args = new ArrayList<String>();

			if (queryWhereClausule.length() == 0)
				queryWhereClausule += " p." + EXTDAOPonto.CORPORACAO_ID_INT + " = ? ";
			else
				queryWhereClausule += " AND p." + EXTDAOPonto.CORPORACAO_ID_INT + " = ? ";
			args.add(OmegaSecurity.getIdCorporacao());

			if (pessoa != null && pessoa.length() > 0) {
				if (queryWhereClausule.length() == 0)
					queryWhereClausule += " p." + EXTDAOPonto.PESSOA_ID_INT + " = ? ";
				else
					queryWhereClausule += " AND p." + EXTDAOPonto.PESSOA_ID_INT + " = ? ";
				args.add(pessoa);
			}

			if (dataInicial != null && dataInicial.length() > 0) {
				if (queryWhereClausule.length() == 0)
					queryWhereClausule += " p." + EXTDAOPonto.DATA_SEC + " >= ? ";
				else
					queryWhereClausule += " AND p." + EXTDAOPonto.USUARIO_ID_INT + " >= ? ";
				args.add(dataInicial);
			}

			if (dataFinal != null && dataFinal.length() > 0) {
				if (queryWhereClausule.length() == 0)
					queryWhereClausule += " p." + EXTDAOPonto.DATA_SEC + " <= ? ";
				else
					queryWhereClausule += " AND p." + EXTDAOPonto.USUARIO_ID_INT + " <= ? ";
				args.add(dataFinal);
			}

			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY p." + EXTDAOPonto.DATA_SEC + " DESC";
			} else {
				orderBy += " ORDER BY  p." + EXTDAOPonto.DATA_SEC + " ASC";
			}

			String query = querySelect + queryFromTables;
			if (queryWhereClausule.length() > 0)
				query += " WHERE " + queryWhereClausule;

			if (orderBy.length() > 0) {
				query += orderBy;
			}

			String[] vetorArg = new String[args.size()];
			args.toArray(vetorArg);

			Cursor cursor = null;
			try {
				db.rawQuery(query, vetorArg);
				String[] vetorChave = new String[] { EXTDAOPonto.ID };
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			} finally {
				if (cursor != null && !cursor.isClosed()) {
					cursor.close();
				}
			}

		} finally {
			if (db != null)
				db.close();
		}
		
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {
		try {
			EXTDAOPonto objPonto = (EXTDAOPonto) pObj;

			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb vObjSincronizador = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(
					pObj.getDatabase());
			String vId = objPonto.getAttribute(EXTDAOPonto.ID).getStrValue();

			PontoItemList vItemList = new PontoItemList(activity, vId,
					vObjSincronizador.isTuplaSincronizadaNoServidor(EXTDAOPonto.NAME, vId),
					objPonto.getValorDoAtributoDaChaveExtrangeira(EXTDAOPonto.TIPO_PONTO_ID_INT, EXTDAOTipoPonto.NOME),
					objPonto.getValorDoAtributoDaChaveExtrangeira(EXTDAOPonto.EMPRESA_ID_INT, EXTDAOEmpresa.NOME),
					objPonto.getValorDoAtributoDaChaveExtrangeira(EXTDAOPonto.PROFISSAO_ID_INT, EXTDAOProfissao.NOME),
					objPonto.getValorDoAtributoDaChaveExtrangeira(EXTDAOPonto.PESSOA_ID_INT, EXTDAOPessoa.NOME),
					objPonto.getAttribute(EXTDAOPonto.DATA_SEC).getLongValue(),
					HelperBoolean.valueOfStringSQL(objPonto.getAttribute(EXTDAOPonto.IS_ENTRADA_BOOLEAN).getStrValue()),
					objPonto.getAttribute(EXTDAOPonto.DATA_OFFSEC).getIntegerValue());

			return vItemList;
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
			return null;
		}
	}
}
