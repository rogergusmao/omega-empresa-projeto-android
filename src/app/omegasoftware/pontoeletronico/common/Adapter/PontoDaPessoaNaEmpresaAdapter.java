package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.PontoItemList;
import app.omegasoftware.pontoeletronico.common.activity.HelperFonte;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.ResultSet;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoPonto;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoPonto.TIPO_PONTO;
import app.omegasoftware.pontoeletronico.primitivetype.HelperBoolean;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperLong;

public class PontoDaPessoaNaEmpresaAdapter extends CustomAdapter{
	
//	private static final String TAG = "PontoDaPessoaNaEmpresaAdapter";

	Database db=null;
	String idPessoa;
	String idEmpresa;
	String idProfissao;
	Boolean entrada;
	
	EXTDAOPessoaEmpresa objPessoaEmpresa;
	public PontoDaPessoaNaEmpresaAdapter(Activity activity, 
			String idPessoa, 
			String idEmpresa, 
			String idProfissao) throws OmegaDatabaseException{
		
		super(activity);
		
		db = new DatabasePontoEletronico(activity);
		this.idPessoa = idPessoa;
		this.idEmpresa = idEmpresa;
		this.idProfissao = idProfissao;
		objPessoaEmpresa = new EXTDAOPessoaEmpresa(db);
	}

	@Override
	protected ArrayList<CustomItemList> inicializaItens() {
		String q = "SELECT DISTINCT	" 
					+ " p." + EXTDAOPonto.ID + ", "  
					+ " p." + EXTDAOPonto.TIPO_PONTO_ID_INT + ", "
					+ " p." + EXTDAOPonto.IS_ENTRADA_BOOLEAN + ", "
					+ " p." + EXTDAOPonto.DATA_SEC + ", " 
					+ " p." + EXTDAOPonto.DATA_OFFSEC
				+ " FROM " + EXTDAOPonto.NAME + " p "		
				+ " WHERE p." + EXTDAOPonto.PESSOA_ID_INT + " = ? "	
					+ "	AND p." + EXTDAOPonto.PROFISSAO_ID_INT + " = ? "
					+ "	AND p." + EXTDAOPonto.EMPRESA_ID_INT + " = ? "
					//+ "	AND " + EXTDAOPonto.IS_ENTRADA_BOOLEAN + " = ? "
				+ "	ORDER BY p." + EXTDAOPonto.DATA_SEC + " DESC, "
				 	+ " p." + EXTDAOPonto.DATA_OFFSEC + " DESC ";
	
		
		ResultSet rs = db.getResultSet(q, new String[] {idPessoa, idProfissao, idEmpresa});
		ArrayList<CustomItemList> ret = new ArrayList<CustomItemList>();
		if(rs == null) return ret;
		for(int i = 0 ; i < rs.getTotalTupla(); i++){
			String[] valores = rs.getTupla(i);
			//item_list_ponto_da_pessoa_na_empresa_layout
			PontoItemList ponto = new PontoItemList(
					activity,
					valores[0],
					false,
					valores[1],
					null,
					null, 
					null,
					HelperLong.parserLong( valores[3] ),
					HelperBoolean.valueOfStringSQL(valores[2]),
					HelperInteger.parserInteger( valores[4]));
			ret.add(ponto);
		}
		
		return ret;
	}
	@SuppressLint({ "ViewHolder", "InflateParams" })
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		PontoItemList item = (PontoItemList)listaCompleta.get(position);
		
		LayoutInflater vLayoutInflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View layoutView = vLayoutInflater.inflate(
				R.layout.item_list_ponto_da_pessoa_na_empresa_layout, null);
		String desc = "";
		TextView tvEntradaOuSaida  = ((TextView)layoutView.findViewById(R.id.tv_entrada_ou_saida));
		TextView tvHora = ((TextView)layoutView.findViewById(R.id.tv_hora));
		TextView tvData = ((TextView)layoutView.findViewById(R.id.tv_data));
		TextView tvTipoPonto = ((TextView)layoutView.findViewById(R.id.tv_tipo_ponto));
		int colorTexto = 0;
		int colorBackground = 0;
		if(position % 2 == 0){
			colorTexto = activity.getResources().getColor(R.color.gray_35);
			colorBackground = activity.getResources().getColor(R.color.white);
		}
		else{
			colorTexto = activity.getResources().getColor(R.color.white);
			colorBackground = activity.getResources().getColor(R.color.gray_35);	
		}
		
		if(item.isEntrada()){
			tvEntradaOuSaida.setTextColor(activity.getResources().getColor(R.color.green));
//			layoutView.setBackgroundColor(activity.getResources().getColor( R.color.green));
			desc = activity.getResources().getString(R.string.entrada);
			
			
		} else {
			tvEntradaOuSaida.setTextColor(activity.getResources().getColor( R.color.red));
//			layoutView.setBackgroundColor(activity.getResources().getColor(R.color.yellow));
			desc = activity.getResources().getString(R.string.saida);
			
		}
		tvHora.setTextColor(colorTexto);
		tvData.setTextColor(colorTexto);
		tvTipoPonto.setTextColor(colorTexto);
		layoutView.setBackgroundColor(colorBackground);
		
		tvEntradaOuSaida.setText(desc);
		
		String strIdTipoPonto =item.getConteudoContainerItem(PontoItemList.TIPO_PONTO);
		
		
		Integer idTipoPonto = HelperInteger.parserInteger(strIdTipoPonto);
		if(idTipoPonto != null){
			TIPO_PONTO tipo = EXTDAOTipoPonto.TIPO_PONTO.getTipoPonto(idTipoPonto);
			if(tipo != null){
				String descTipoPonto = tipo.getDescricao(activity);
				tvTipoPonto.setText(descTipoPonto);		
			}			
		}
		String data = item.getConteudoContainerItem(PontoItemList.DATA);
		tvData.setText(data);
		String hora = item.getConteudoContainerItem(PontoItemList.HORA);
		tvHora.setText(hora);
		ViewGroup vg = (ViewGroup) layoutView;
		HelperFonte.setFontAllView(vg);
		return layoutView ;
	}

//	@Override
//	protected ArrayList<Integer> getListIdBySearchParameters(boolean pIsAsc) {
//    	

//	}
//
//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {

//	}


}
