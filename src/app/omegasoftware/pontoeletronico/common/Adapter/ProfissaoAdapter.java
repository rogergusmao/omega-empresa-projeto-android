package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormProfissaoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.ProfissaoItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class ProfissaoAdapter extends DatabaseCustomAdapter{
	
//	private static final int ID_TEXT_VIEW_PROFISSAO = R.id.profissao_textview;
	
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;


	public String nomeProfissao;

	public ProfissaoAdapter(
			Activity pActivity,
			String nomeProfissao, 
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, p_isAsc, EXTDAOProfissao.NAME, ProfissaoItemList.PROFISSAO, false);
		this.nomeProfissao = nomeProfissao; 
		
		super.initalizeListCustomItemList();
	}

	
	@SuppressLint("InflateParams")
	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			ProfissaoItemList vItemList = (ProfissaoItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.item_list_layout, null);

			PersonalizaItemView(vLayoutView, pPosition);
			

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_1, 
					vItemList.getConteudoContainerItem(ProfissaoItemList.PROFISSAO), 
					vLayoutView);


			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
						activity, 
						EXTDAOProfissao.NAME,
						pParent, 
						vItemList.getId(),
						FormEditActivityMobile.class, 
						FormProfissaoActivityMobile.class,
						isEditPermitido(),
						isRemovePermitido(), vItemList);
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_PROFISSAO, 
						vClickListener);
			}
			else {
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_PROFISSAO);
			}
			return vLayoutView;
		} catch (Exception exc) {
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
					"ProfissaoAdapter: getView()");
		}
		return null;
	}



	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {

		
		DatabasePontoEletronico database=null;
		try {
			database = new DatabasePontoEletronico(this.activity);
		} catch (OmegaDatabaseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			
			String querySelect = "SELECT DISTINCT profissao."
					+ EXTDAOProfissao.ID + " ";

			String queryFromTables =  "FROM " + EXTDAOProfissao.NAME + " profissao ";

			String queryWhereClausule = ""; 
			ArrayList<String> args = new ArrayList<String>();

			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " profissao." + EXTDAOProfissao.CORPORACAO_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND profissao." + EXTDAOProfissao.CORPORACAO_ID_INT + " = ? " ;
			
			args.add(OmegaSecurity.getIdCorporacao());
			
			if(this.nomeProfissao != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " profissao." + Attribute.getNomeAtributoNormalizado(EXTDAOProfissao.NOME) + " LIKE ?" ;
				else 
					queryWhereClausule += " AND profissao." + Attribute.getNomeAtributoNormalizado(EXTDAOProfissao.NOME) + " LIKE ?" ;

				args.add('%' + HelperString.getWordWithoutAccent(this.nomeProfissao) + '%' );
			}

			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY profissao." + Attribute.getNomeAtributoNormalizado(EXTDAOProfissao.NOME)
						+ " DESC";
			} else {
				orderBy += " ORDER BY  profissao." + Attribute.getNomeAtributoNormalizado(EXTDAOProfissao.NOME)
						+ " ASC";
			}

			String query = querySelect + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

			if(orderBy.length() > 0 ){
				query += orderBy;  
			} 

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			
			Cursor cursor = null;
			try{
				cursor = database.rawQuery(query, vetorArg);
				String[] vetorChave = new String[] { EXTDAOProfissao.ID };
	
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}

		} catch (Exception e) {
			
			e.printStackTrace();
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
		finally
		{
			database.close();
		}
		return null;
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {

		EXTDAOProfissao vEXTDAOProfissao = (EXTDAOProfissao) pObj;
		
	
		ProfissaoItemList vItemList = new ProfissaoItemList(  
				vEXTDAOProfissao.getAttribute(EXTDAOProfissao.ID).getStrValue(),
				vEXTDAOProfissao.getAttribute(EXTDAOProfissao.NOME).getStrValue());

		return vItemList;
	}
}

