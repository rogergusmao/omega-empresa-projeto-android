package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormRedeActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.RedeItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORede;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class RedeAdapter extends DatabaseCustomAdapter{
	
//	private static final int ID_TEXT_VIEW_REDE = R.id.rede_textview;
	
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;


	public String nomeRede;

	public RedeAdapter(
			Activity pActivity,
			String nomeRede, 
			boolean isAsc) throws OmegaDatabaseException
	{
		super(pActivity, 
				isAsc, 
				EXTDAORede.NAME,
				false ,
				RedeItemList.REDE, 
				FormRedeActivityMobile.class, 
				null);
		this.nomeRede = nomeRede; 
		
		super.initalizeListCustomItemList();
	}

	
	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			RedeItemList vItemList = (RedeItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.item_list_layout, null);

			PersonalizaItemView(vLayoutView, pPosition);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_1, 
					vItemList.getConteudoContainerItem(RedeItemList.REDE), 
					vLayoutView);

			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
						activity, 
						EXTDAORede.NAME,
						pParent, 
						vItemList.getId(),
						FormEditActivityMobile.class, 
						FormRedeActivityMobile.class,
						isEditPermitido(),
						isRemovePermitido(), vItemList);
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_REDE, 
						vClickListener);
			}
			else {
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_REDE);
			}
			return vLayoutView;
		} catch (Exception exc) {
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
					"RedeAdapter: getView()");
		}
		return null;
	}



	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {

		
		DatabasePontoEletronico database=null;
		try {
			database = new DatabasePontoEletronico(this.activity);
		} catch (OmegaDatabaseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			
			String querySelect = "SELECT DISTINCT r."
					+ EXTDAORede.ID + " ";

			String queryFromTables =  "FROM " + EXTDAORede.NAME + " r ";

			String queryWhereClausule = ""; 
			ArrayList<String> args = new ArrayList<String>();

			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " r." + EXTDAORede.CORPORACAO_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND r." + EXTDAORede.CORPORACAO_ID_INT + " = ? " ;
			
			args.add(OmegaSecurity.getIdCorporacao());
			
			if(this.nomeRede != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " r." + Attribute.getNomeAtributoNormalizado(EXTDAORede.NOME) + " LIKE ?" ;
				else 
					queryWhereClausule += " AND r." + Attribute.getNomeAtributoNormalizado(EXTDAORede.NOME) + " LIKE ?" ;

				args.add('%' + HelperString.getWordWithoutAccent(this.nomeRede) + '%' );
			}

			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY r." + Attribute.getNomeAtributoNormalizado(EXTDAORede.NOME)
						+ " DESC";
			} else {
				orderBy += " ORDER BY  r." + Attribute.getNomeAtributoNormalizado(EXTDAORede.NOME)
						+ " ASC";
			}

			String query = querySelect + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

			if(orderBy.length() > 0 ){
				query += orderBy;  
			} 

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			
			Cursor cursor = null;
			try{
				cursor = database.rawQuery(query, vetorArg);
				String[] vetorChave = new String[] { EXTDAORede.ID };
	
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}

		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.ADAPTADOR);
		}
		finally
		{
			
		}
		return null;
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {

		EXTDAORede vEXTDAORede = (EXTDAORede) pObj;
		
	
		RedeItemList vItemList = new RedeItemList(  
				vEXTDAORede.getAttribute(EXTDAORede.ID).getStrValue(),
				vEXTDAORede.getAttribute(EXTDAORede.NOME).getStrValue());

		return vItemList;
	}
}

