package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormRelatorioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.RelatorioItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORelatorio;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class RelatorioAdapter extends DatabaseCustomAdapter{
	
	private static final int ID_TEXT_VIEW_DATA = R.id.data_textview;
	private static final int ID_TEXT_VIEW_PESSOA = R.id.pessoa_textview;
	private static final int ID_TEXT_VIEW_EMPRESA = R.id.empresa_textview;
	private static final int ID_TEXT_VIEW_HORA = R.id.hora_textview;
	private static final int ID_TEXT_VIEW_USUARIO = R.id.usuario_textview;
	private static final int ID_TEXT_VIEW_TITULO = R.id.titulo_textview;
	
	private static final int ID_BUTTON_VIDEO = R.id.video_button;
	private static final int ID_BUTTON_FOTO = R.id.foto_button;
	private static final int ID_BUTTON_ANEXO= R.id.anexo_button;
	private static final int ID_BUTTON_AUDIO= R.id.audio_button;
	
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;

	public String dataInicial;
	public String horaInicial;
	
	public String dataFinal;
	public String horaFinal;
	
	public String pessoa;
	public String empresa;
	public String usuario;
	public String titulo;
	public String descricao;

	public RelatorioAdapter(
			Activity pActivity,
			String dataInicio,
			String horaInicio,
			String dataFim,
			String horaFim,
			String pessoa,
			String empresa,
			String usuario,
			String titulo, 
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, p_isAsc, EXTDAORelatorio.NAME, RelatorioItemList.TITULO, false);
		this.dataInicial = dataInicio;
		this.horaInicial = horaInicio;
		this.dataFinal = dataFim;
		this.horaFinal = horaFim;
		this.pessoa = pessoa;
		this.empresa = empresa;
		this.usuario = usuario;
		this.titulo = titulo;
		
		super.initalizeListCustomItemList();
	}

	
	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			RelatorioItemList vItemList = (RelatorioItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.profissao_item_list_layout, null);
			if((pPosition % 2) == 0)
			{
				((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.color.gray_189);
			}
			else
			{
				((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.color.white);
			}
			

			CreateTextView(
					ID_TEXT_VIEW_DATA, 
					vItemList.getConteudoContainerItem(RelatorioItemList.DATA), 
					vLayoutView);

			CreateTextView(
					ID_TEXT_VIEW_PESSOA, 
					vItemList.getConteudoContainerItem(RelatorioItemList.PESSOA), 
					vLayoutView);
			
			CreateTextView(
					ID_TEXT_VIEW_EMPRESA, 
					vItemList.getConteudoContainerItem(RelatorioItemList.EMPRESA), 
					vLayoutView);
			
			CreateTextView(
					ID_TEXT_VIEW_USUARIO, 
					vItemList.getConteudoContainerItem(RelatorioItemList.USUARIO), 
					vLayoutView);
			CreateTextView(
					ID_TEXT_VIEW_HORA, 
					vItemList.getConteudoContainerItem(RelatorioItemList.HORA), 
					vLayoutView);
			CreateTextView(
					ID_TEXT_VIEW_TITULO, 
					vItemList.getConteudoContainerItem(RelatorioItemList.TITULO), 
					vLayoutView);
			EXTDAORelatorio.ContainerContem vContainer = vItemList.getContainerRelatorio();
			if(!vContainer.contemVideo)
				vLayoutView.findViewById(ID_BUTTON_VIDEO).setVisibility(View.GONE);
			
			if(!vContainer.contemAudio)
				vLayoutView.findViewById(ID_BUTTON_AUDIO).setVisibility(View.GONE);
			
			if(!vContainer.contemAnexo)
				vLayoutView.findViewById(ID_BUTTON_ANEXO).setVisibility(View.GONE);
			
			if(!vContainer.contemFoto)
				vLayoutView.findViewById(ID_BUTTON_FOTO).setVisibility(View.GONE);
			
			
			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
						activity, 
						EXTDAORelatorio.NAME,
						pParent, 
						vItemList.getId(),
						FormEditActivityMobile.class, 
						FormRelatorioActivityMobile.class,
						isEditPermitido(),
						isRemovePermitido(),
						vItemList);
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_RELATORIO, 
						vClickListener);
			}
			else {
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_RELATORIO);
			}
			return vLayoutView;
		} catch (Exception exc) {
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
					"RelatorioAdapter: getView()");
		}
		return null;
	}



	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {

		
		DatabasePontoEletronico database=null;
		try {
			database = new DatabasePontoEletronico(this.activity);
		} catch (OmegaDatabaseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			
			String querySelect = "SELECT DISTINCT r."
					+ EXTDAORelatorio.ID + " ";

			String queryFromTables =  "FROM " + EXTDAORelatorio.NAME + " r ";

			String queryWhereClausule = ""; 
			ArrayList<String> args = new ArrayList<String>();

			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " r." + EXTDAORelatorio.CORPORACAO_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND r." + EXTDAORelatorio.CORPORACAO_ID_INT + " = ? " ;
			
			args.add(OmegaSecurity.getIdCorporacao());
			
			if(this.titulo != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " r." + Attribute.getNomeAtributoNormalizado(EXTDAORelatorio.TITULO) + " LIKE ?" ;
				else 
					queryWhereClausule += " AND r." + Attribute.getNomeAtributoNormalizado(EXTDAORelatorio.TITULO) + " LIKE ?" ;

				args.add('%' + HelperString.getWordWithoutAccent(this.titulo) + '%' );
			}
			
			if(this.descricao != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " r." + Attribute.getNomeAtributoNormalizado(EXTDAORelatorio.DESCRICAO) + " LIKE ?" ;
				else 
					queryWhereClausule += " AND r." + Attribute.getNomeAtributoNormalizado(EXTDAORelatorio.DESCRICAO) + " LIKE ?" ;

				args.add('%' + HelperString.getWordWithoutAccent(this.descricao) + '%' );
			}
			

			if(dataInicial != null && dataInicial.length() > 0 ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " r." + EXTDAORelatorio.DATA_SEC + " >= ? " ;
				else 
					queryWhereClausule += " AND r." + EXTDAORelatorio.USUARIO_ID_INT + " >= ? " ;
				args.add(dataInicial);	
			}
			
			if(dataFinal != null && dataFinal.length() > 0 ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " r." + EXTDAORelatorio.DATA_SEC + " <= ? " ;
				else 
					queryWhereClausule += " AND r." + EXTDAORelatorio.USUARIO_ID_INT + " <= ? " ;
				args.add(dataFinal);	
			}
			
			
//			if(this.pessoa != null ){
//				if(queryWhereClausule.length() ==  0)
//					queryWhereClausule += " r." + EXTDAORelatorio.PESSOA_ID_INT + " = ?" ;
//				else
//					queryWhereClausule += " AND r." + EXTDAORelatorio.PESSOA_ID_INT + " = ?" ;
//				args.add(this.pessoa);
//			}
//
//			if(this.empresa != null ){
//				if(queryWhereClausule.length() ==  0)
//					queryWhereClausule += " r." + EXTDAORelatorio.EMPRESA_ID_INT + " = ?" ;
//				else
//					queryWhereClausule += " AND r." + EXTDAORelatorio.EMPRESA_ID_INT + " = ?" ;
//				args.add(this.empresa);
//			}

			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY r." + EXTDAORelatorio.ID
						+ " DESC";
			} else {
				orderBy += " ORDER BY  r." + EXTDAORelatorio.ID
						+ " ASC";
			}

			String query = querySelect + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

			if(orderBy.length() > 0 ){
				query += orderBy;  
			} 

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			
			Cursor cursor = null;
			try{
				cursor = database.rawQuery(query, vetorArg);
				String[] vetorChave = new String[] { EXTDAORelatorio.ID };
	
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}

		} catch (Exception e) {
			
			e.printStackTrace();
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
		finally
		{
			database.close();
		}
		return null;
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {

		try{
		
		EXTDAORelatorio vEXTDAORelatorio = (EXTDAORelatorio) pObj;
		
		String vIdRelatorio = vEXTDAORelatorio.getAttribute(EXTDAORelatorio.ID).getStrValue();
		RelatorioItemList vItemList = new RelatorioItemList(  
				vIdRelatorio,
				null,
				null,
				vEXTDAORelatorio.getAttribute(EXTDAORelatorio.TITULO).getStrValue(),
				vEXTDAORelatorio.getAttribute(EXTDAORelatorio.DATA_SEC).getStrValue(),
				vEXTDAORelatorio.getAttribute(EXTDAORelatorio.DATA_OFFSEC).getStrValue(),
				vEXTDAORelatorio.getValorDoAtributoDaChaveExtrangeira(EXTDAORelatorio.USUARIO_ID_INT, EXTDAOUsuario.NOME),
				vEXTDAORelatorio.getContainerContem());

		return vItemList;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
return null;
		}
	}
}

