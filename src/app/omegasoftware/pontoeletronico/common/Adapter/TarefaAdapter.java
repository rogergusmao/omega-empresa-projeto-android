package app.omegasoftware.pontoeletronico.common.Adapter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import app.omegasoftware.pontoeletronico.R;

import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;

import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormTarefaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;

import app.omegasoftware.pontoeletronico.common.ItemList.TarefaItemList;
import app.omegasoftware.pontoeletronico.common.activity.HelperFonte;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;

import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaPerfil;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPerfil;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPermissaoCategoriaPermissao;

import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class TarefaAdapter extends DatabaseCustomAdapter{

	private static final String TAG = "TarefaAdapter";

	String titulo;

	String origemPessoa;
	String origemEmpresa;

	String destinoPessoa;
	String destinoEmpresa;

	String origemLogradouro;
	String origemNumero;
	String origemComplemento;
	String origemIdCidade;
	String origemIdBairro;

	String destinoLogradouro;
	String destinoNumero;
	String destinoComplemento;
	String destinoCidadeId;
	String destinoBairroId;

	private String inicioDataProgramada;
	private String inicioHoraProgramada;
	private String inicioData;
	private String inicioHora;
	private String fimData;
	private String fimHora;
	private String dataCadastro;
	private String horaCadastro;
	private String dataExibir;
	private String descricao;

	private EXTDAOTarefa.TIPO_TAREFA  tipoTarefa;
	private String identificadorTipoTarefa;
	public EXTDAOTarefa.TIPO_ENDERECO tipoOrigemEndereco;
	public EXTDAOTarefa.TIPO_ENDERECO tipoDestinoEndereco;
	public String identificadorTipoOrigemEndereco;
	public String identificadorTipoDestinoEndereco;
	public String criadoPeloUsuario;



	public boolean isUsuarioEditPermitido;
	public boolean isUsuarioRemovePermitido;

	SimpleDateFormat sdf;


	private void initPermissao() throws OmegaDatabaseException{
		DatabasePontoEletronico db = new DatabasePontoEletronico(activity);
		EXTDAOPermissaoCategoriaPermissao vObj = new EXTDAOPermissaoCategoriaPermissao(db);
		isUsuarioEditPermitido = vObj.hasPermissaoToEdit(EXTDAOTarefa.NAME);
		isUsuarioRemovePermitido = vObj.hasPermissaoToRemove(EXTDAOTarefa.NAME);
		db.close();

	}

	public TarefaAdapter(
			Activity pActivity,
			EXTDAOTarefa.TIPO_TAREFA pTipoTarefa,
			EXTDAOTarefa.ESTADO_TAREFA pEstadoTarefa,
			String pIdentificadorTipoTarefa,
			EXTDAOTarefa.TIPO_ENDERECO pTipoOrigemEndereco,
			EXTDAOTarefa.TIPO_ENDERECO pTipoDestinoEndereco,
			String pIdentificadorTipoOrigemEndereco,
			String pIdentificadorTipoDestinoEndereco,
			String pCriadoPeloUsuario,
			String pTitulo,
			String pOrigemLogradouro,
			String pOrigemNumero,
			String pOrigemComplemento,
			String pOrigemIdCidade,
			String pOrigemIdBairro,
			String pDestinoLogradouro,
			String pDestinoNumero,
			String pDestinoComplemento,
			String pDestinoCidade,
			String pDestinoBairro,
			String inicioDataProgramada,
			String inicioHoraProgramada,
			String inicioData,
			String inicioHora,
			String fimData,
			String fimHora,
			String dataCadastro,
			String horaCadastro,
			String dataExibir,
			String descricao,
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity,
			p_isAsc,
			EXTDAOTarefa.NAME,
			false,
			TarefaItemList.NOME_TAREFA,
			FormTarefaActivityMobile.class,
			null);



		tipoTarefa = pTipoTarefa;
		identificadorTipoTarefa = pIdentificadorTipoTarefa;
		tipoOrigemEndereco = pTipoOrigemEndereco;
		tipoDestinoEndereco = pTipoDestinoEndereco;
		identificadorTipoOrigemEndereco = pIdentificadorTipoOrigemEndereco;
		identificadorTipoDestinoEndereco = pIdentificadorTipoDestinoEndereco;

		criadoPeloUsuario = pCriadoPeloUsuario;
		titulo = pTitulo;


		this.origemIdCidade = pOrigemIdCidade;
		this.origemIdBairro = pOrigemIdBairro;
		this.destinoCidadeId = pDestinoCidade;
		this.destinoBairroId = pDestinoBairro;
		origemLogradouro = pOrigemLogradouro;
		origemNumero= pOrigemNumero;
		origemComplemento= pOrigemComplemento;
		destinoLogradouro = pDestinoLogradouro;
		destinoNumero= pDestinoNumero;
		destinoComplemento= pDestinoComplemento;
		this.inicioDataProgramada = inicioDataProgramada;
		this.inicioHoraProgramada = inicioHoraProgramada;
		this.inicioData = inicioData;
		this.inicioHora = inicioHora;
		this.fimData = fimData;
		this.fimHora = fimHora;
		this.dataCadastro = dataCadastro;
		this.horaCadastro = horaCadastro;
		this.dataExibir = dataExibir;
		this.descricao = descricao;
		activity = pActivity;
		formularioClass = FormTarefaActivityMobile.class;

		initPermissao();
		super.initalizeListCustomItemList();
	}

	@Override
	public ArrayList<CustomItemList> filtraToken(CharSequence cs){
		if(listaCompleta == null) return null;
		ArrayList<CustomItemList> resultado = new ArrayList<CustomItemList>();
		String csMaiusculo = cs.toString().toUpperCase();
		String csMinusculo= cs.toString().toLowerCase();
		for(int i = 0 ; i < listaCompleta.size(); i++){
			TarefaItemList itemList = (TarefaItemList)listaCompleta.get(i);

			String nome = itemList.getConteudoContainerItem(TarefaItemList.NOME_TAREFA);
			if(nome != null && nome.contains(csMaiusculo)){
				resultado.add(itemList);
				continue;
			}
		}

		return resultado;
	}


	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			TarefaItemList itemList = (TarefaItemList)this.getItem(pPosition);

			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layoutView = vLayoutInflater.inflate(
					R.layout.item_list_layout, null);
//			"@drawable/menu_windows_phone_blue_button"
//			Drawable d = activity.getResources().getDrawable(R.drawable.menu_windows_phone_blue_button);
//			vLayoutView.setBackgroundDrawable(d);
			PersonalizaItemView(layoutView, pPosition);


			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
						activity,
						EXTDAOTarefa.NAME,
						pParent,
						itemList.getId(),
						FormEditActivityMobile.class,
						FormTarefaActivityMobile.class,
						isEditPermitido(),
						isRemovePermitido(), itemList);
				CreateDetailButton(layoutView,
						ID_CUSTOM_BUTTON_DETAIL,
						itemList.getId(),
						OmegaConfiguration.SEARCH_MODE_TAREFA,
						vClickListener,
						itemList.getConteudoContainerItem(TarefaItemList.NOME_TAREFA));
			}
			else {
				CreateDetailButton(
						layoutView,
						ID_CUSTOM_BUTTON_DETAIL,
						itemList.getId(),
						OmegaConfiguration.SEARCH_MODE_TAREFA);
			}

			
			String titulo = itemList.getConteudoContainerItem(TarefaItemList.NOME_TAREFA);



			CreateTextViewHtml(ID_TEXT_VIEW_DESCRICAO_1,
					titulo,
					layoutView);

			CreateTextView(ID_TEXT_VIEW_DESCRICAO_2,
					itemList.desc1,
					layoutView, true);
			CreateTextView(ID_TEXT_VIEW_DESCRICAO_3,
					itemList.desc2,
					layoutView, true);

			
			
			HelperFonte.setFontAllView(layoutView);
			return layoutView;
		} catch (Exception exc) {
			SingletonLog.insereErro(exc, TIPO.FORMULARIO);
			
		}
		return null;
	}


	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {


		String querySelectId = "SELECT DISTINCT t.id FROM tarefa t ";

		String queryWhereClausule = "";
		ArrayList<String> args = new ArrayList<String>();

		if(queryWhereClausule.length() ==  0)
			queryWhereClausule += " t.corporacao_id_INT = ? " ;
		else 
			queryWhereClausule += " AND t.corporacao_id_INT = ? " ;

		args.add(OmegaSecurity.getIdCorporacao());


		String query = querySelectId ;
		if(queryWhereClausule.length() > 0 )
			query 	+= " WHERE " + queryWhereClausule;

		if (!pIsAsc) {
			query += " ORDER BY t.id DESC";
		} else {
			query += " ORDER BY  t.id ASC";
		}

		String[] vetorArg = new String [args.size()];
		args.toArray(vetorArg);
		
		DatabasePontoEletronico database = null;
		Cursor cursor = null;
		try{
			database = new DatabasePontoEletronico(this.activity);

			cursor = database.rawQuery(query, vetorArg);
	
			String[] chaves = new String[] { EXTDAOTarefa.ID };
	
			ArrayList<Long> idsResultado = HelperDatabase.convertCursorToArrayListId(cursor, chaves);

			return idsResultado;
		} catch(Exception ex){

			SingletonLog.openDialogError(
					activity
					, ex
					, TIPO.ADAPTADOR);
			return null;
		} finally{
			if(cursor != null && ! cursor.isClosed()){
				cursor.close();
			}
			if(database != null)
				database.close();
		}
	
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {

		EXTDAOTarefa vEXTDAOTarefa = (EXTDAOTarefa) pObj;

		Attribute vAttrId = pObj.getAttribute(EXTDAOTarefa.ID);
		String vId = null;

		if(vAttrId != null){
			vId = vAttrId.getStrValue();
		}
		if(vId == null){
			return null;
		}

		TarefaItemList vItemList = new TarefaItemList(
				vId,
				vEXTDAOTarefa.getContainerResponsavel(activity),
				vEXTDAOTarefa.getEstadoTarefa(),
				vEXTDAOTarefa.getTipoTarefa(),
				vEXTDAOTarefa.getIdentificadorTipoTarefa(),
				vEXTDAOTarefa.getNomeTarefa(),
				vEXTDAOTarefa.getDataCadastroFormatada(),
				null,
				null,
				null,
				null,
				vEXTDAOTarefa.getDesc1Tarefa(),
				vEXTDAOTarefa.getDesc2Tarefa());



		return vItemList;
	}


}
