package app.omegasoftware.pontoeletronico.common.Adapter;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.HelperFonte;

public class TecladoAdapter {
	
//	public static final String TAG = "TecladoAdapter";
	DatabaseCustomAdapter adapter;
	CustomAdapter customAdapter;
	Activity activity;
	
	public TecladoAdapter(DatabaseCustomAdapter adapter)
	{
		this.activity = adapter.getActivity();
		this.adapter= adapter;
		
	}
	
	public TecladoAdapter(CustomAdapter adapter)
	{
		this.activity = adapter.getActivity();
		this.customAdapter = adapter;
		
	}

	public void formatarView(View viewTeclado)  {
		try {			
			EditText txtResult = (EditText)viewTeclado.findViewById(R.id.et_discador);
			txtResult.addTextChangedListener(new TextWatcher() {

		    @Override
		    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
		    	if(adapter != null)
		        adapter.getFilter().filter(cs);
		    	else if(customAdapter != null)
		    		customAdapter.getFilter().filter(cs);
		    }

		    @Override
		    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }
	
			    @Override
			    public void afterTextChanged(Editable arg0) {}
		});
			HelperFonte.setFontAllView(viewTeclado);
			
		} catch (Exception exc) {
			SingletonLog.insereErro(exc, TIPO.ADAPTADOR);
			
		}
		
	}



}
