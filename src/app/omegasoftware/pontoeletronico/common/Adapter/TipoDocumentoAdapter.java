package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormTipoDocumentoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.TipoDocumentoItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoDocumento;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class TipoDocumentoAdapter extends DatabaseCustomAdapter{
	
//	public static final int ID_TEXT_VIEW_TIPO_DOCUMENTO = R.id.tipo_documento_textview;
	
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;


	public String nomeTipoDocumento;

	public TipoDocumentoAdapter(
			Activity pActivity,
			String nomeTipoDocumento, 
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, p_isAsc, EXTDAOTipoDocumento.NAME, TipoDocumentoItemList.TIPO_DOCUMENTO, false);
		this.nomeTipoDocumento = nomeTipoDocumento; 
		
		super.initalizeListCustomItemList();
	}

	
	@SuppressLint("ViewHolder")
	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			TipoDocumentoItemList vItemList = (TipoDocumentoItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.item_list_layout, null);

			PersonalizaItemView(vLayoutView, pPosition);
			

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_1, 
					vItemList.getConteudoContainerItem(TipoDocumentoItemList.TIPO_DOCUMENTO), 
					vLayoutView);


			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
						activity, 
						EXTDAOTipoDocumento.NAME,
						pParent, 
						vItemList.getId(),
						FormEditActivityMobile.class, 
						FormTipoDocumentoActivityMobile.class,
						isEditPermitido(),
						isRemovePermitido(), vItemList);
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_TIPO_DOCUMENTO, 
						vClickListener);
			}
			else {
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_TIPO_DOCUMENTO);
			}
			return vLayoutView;
		} catch (Exception exc) {
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
			//		"TipoDocumentoAdapter: getView()");
		}
		return null;
	}



	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {

		
		DatabasePontoEletronico database=null;
		try {
			database = new DatabasePontoEletronico(this.activity);
		} catch (OmegaDatabaseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			
			String querySelect = "SELECT DISTINCT tipoDocumento."
					+ EXTDAOTipoDocumento.ID + " ";

			String queryFromTables =  "FROM " + EXTDAOTipoDocumento.NAME + " tipoDocumento ";

			String queryWhereClausule = ""; 
			ArrayList<String> args = new ArrayList<String>();

			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " tipoDocumento." + EXTDAOTipoDocumento.CORPORACAO_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND tipoDocumento." + EXTDAOTipoDocumento.CORPORACAO_ID_INT + " = ? " ;
			
			args.add(OmegaSecurity.getIdCorporacao());
			
			if(this.nomeTipoDocumento != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " tipoDocumento." + Attribute.getNomeAtributoNormalizado(EXTDAOTipoDocumento.NOME) + " LIKE ?" ;
				else 
					queryWhereClausule += " AND tipoDocumento." + Attribute.getNomeAtributoNormalizado(EXTDAOTipoDocumento.NOME) + " LIKE ?" ;

				args.add('%' + HelperString.getWordWithoutAccent(this.nomeTipoDocumento) + '%' );
			}

			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY tipoDocumento." + EXTDAOTipoDocumento.NOME
						+ " DESC";
			} else {
				orderBy += " ORDER BY  tipoDocumento." + EXTDAOTipoDocumento.NOME
						+ " ASC";
			}

			String query = querySelect + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

			if(orderBy.length() > 0 ){
				query += orderBy;  
			} 

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			
			Cursor cursor = null;
			try{
				cursor = database.rawQuery(query, vetorArg);
				String[] vetorChave = new String[] { EXTDAOTipoDocumento.ID };
	
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}

		} catch (Exception e) {
			
			//e.printStackTrace();
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
		finally
		{
			database.close();
		}
		return null;
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {

		EXTDAOTipoDocumento vEXTDAOTipoDocumento = (EXTDAOTipoDocumento) pObj;
		
	
		TipoDocumentoItemList vItemList = new TipoDocumentoItemList(  
				vEXTDAOTipoDocumento.getAttribute(EXTDAOTipoDocumento.ID).getStrValue(),
				vEXTDAOTipoDocumento.getAttribute(EXTDAOTipoDocumento.NOME).getStrValue());

		return vItemList;
	}
}

