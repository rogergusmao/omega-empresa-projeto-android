package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormTipoEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.TipoEmpresaItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoEmpresa;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class TipoEmpresaAdapter extends DatabaseCustomAdapter{
	
//	private static final int ID_TEXT_VIEW_TIPO_EMPRESA = R.id.tipo_empresa_textview;
	
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;


	public String nomeTipoEmpresa;

	public TipoEmpresaAdapter(
			Activity pActivity,
			String nomeTipoEmpresa, 
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, p_isAsc, EXTDAOTipoEmpresa.NAME, TipoEmpresaItemList.TIPO_EMPRESA, false);
		this.nomeTipoEmpresa = nomeTipoEmpresa; 
		
		super.initalizeListCustomItemList();
	}

	
	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			TipoEmpresaItemList vItemList = (TipoEmpresaItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.item_list_layout, null);
			
			PersonalizaItemView(vLayoutView, pPosition);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_1, 
					vItemList.getConteudoContainerItem(TipoEmpresaItemList.TIPO_EMPRESA), 
					vLayoutView);

			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
						activity, 
						EXTDAOTipoEmpresa.NAME,
						pParent, 
						vItemList.getId(),
						FormEditActivityMobile.class, 
						FormTipoEmpresaActivityMobile.class,
						isEditPermitido(),
						isRemovePermitido(), vItemList);
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_TIPO_EMPRESA, 
						vClickListener);
			}
			else {
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_TIPO_EMPRESA);
			}
			return vLayoutView;
		} catch (Exception exc) {
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
			//		"TipoEmpresaAdapter: getView()");
		}
		return null;
	}



	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {

		
		DatabasePontoEletronico database=null;
		try {
			database = new DatabasePontoEletronico(this.activity);
		} catch (OmegaDatabaseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			
			String querySelect = "SELECT DISTINCT tipoEmpresa."
					+ EXTDAOTipoEmpresa.ID + " ";

			String queryFromTables =  "FROM " + EXTDAOTipoEmpresa.NAME + " tipoEmpresa ";

			String queryWhereClausule = ""; 
			ArrayList<String> args = new ArrayList<String>();

			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " tipoEmpresa." + EXTDAOTipoEmpresa.CORPORACAO_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND tipoEmpresa." + EXTDAOTipoEmpresa.CORPORACAO_ID_INT + " = ? " ;
			
			args.add(OmegaSecurity.getIdCorporacao());
			
			if(this.nomeTipoEmpresa != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " tipoEmpresa." + Attribute.getNomeAtributoNormalizado(EXTDAOTipoEmpresa.NOME) + " LIKE ?" ;
				else 
					queryWhereClausule += " AND tipoEmpresa." + Attribute.getNomeAtributoNormalizado(EXTDAOTipoEmpresa.NOME) + " LIKE ?" ;

				args.add('%' + HelperString.getWordWithoutAccent(this.nomeTipoEmpresa) + '%' );
			}

			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY tipoEmpresa." + EXTDAOTipoEmpresa.NOME
						+ " DESC";
			} else {
				orderBy += " ORDER BY  tipoEmpresa." + EXTDAOTipoEmpresa.NOME
						+ " ASC";
			}

			String query = querySelect + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

			if(orderBy.length() > 0 ){
				query += orderBy;  
			} 

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			
			Cursor cursor = null;
			try{
				cursor = database.rawQuery(query, vetorArg);
				String[] vetorChave = new String[] { EXTDAOTipoEmpresa.ID };
	
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}

		} catch (Exception e) {
			
			e.printStackTrace();
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
		finally
		{
			database.close();
		}
		return null;
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {

		EXTDAOTipoEmpresa vEXTDAOTipoEmpresa = (EXTDAOTipoEmpresa) pObj;
		
	
		TipoEmpresaItemList vItemList = new TipoEmpresaItemList(  
				vEXTDAOTipoEmpresa.getAttribute(EXTDAOTipoEmpresa.ID).getStrValue(),
				vEXTDAOTipoEmpresa.getAttribute(EXTDAOTipoEmpresa.NOME).getStrValue());

		return vItemList;
	}
}

