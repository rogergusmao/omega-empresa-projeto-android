package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListPontoDaPessoaNaEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto;

public class UltimosPontosAdapter extends CustomAdapter{
	
//	private static final String TAG = "UltimosPontosAdapter";

	Database db=null;
	EXTDAOPonto objPonto;
	EXTDAOPessoaEmpresa objPessoaEmpresa;
	public UltimosPontosAdapter(Activity activity) throws OmegaDatabaseException{
		super(activity);
		
		db = new DatabasePontoEletronico(activity);
		objPonto = new EXTDAOPonto(db);
		objPessoaEmpresa = new EXTDAOPessoaEmpresa(db);
	}
//	@Override
//	public CustomItemList getCustomItemList(Integer pId) {
//		String strId = String.valueOf(pId);
//		objPessoaEmpresa.select();
//		String idPessoa = objPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.PESSOA_ID_INT);
//		String idEmpresa = objPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.EMPRESA_ID_INT);
//		String idProfissao = objPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT);
//		EXTDAOPonto.PontoPessoa pontoPessoa = objPonto.getUltimoPontoDaPessoaNaEmpresa(strId, idPessoa, idEmpresa, idProfissao);
//		return pontoPessoa;
//	}
	@Override
	protected ArrayList<CustomItemList> inicializaItens() {
		try{
		String q = "SELECT DISTINCT	pe." + EXTDAOPessoaEmpresa.ID 
				+ " FROM " + EXTDAOPessoaEmpresa.NAME + " pe "
				+ "		JOIN "+ EXTDAOPonto.NAME + " p " 
				+ "			ON p." + EXTDAOPonto.PESSOA_ID_INT + " = pe." + EXTDAOPessoaEmpresa.PESSOA_ID_INT
				+ "				AND p." + EXTDAOPonto.EMPRESA_ID_INT + " = pe." + EXTDAOPessoaEmpresa.EMPRESA_ID_INT
				+ "		JOIN " + EXTDAOPessoa.NAME + " pes " 
				+ "			ON pe." + EXTDAOPessoaEmpresa.PESSOA_ID_INT + " = pes." + EXTDAOPessoa.ID
				+ "		JOIN " + EXTDAOEmpresa.NAME + " e " 
				+ "			ON pe." + EXTDAOPessoaEmpresa.EMPRESA_ID_INT + " = e." + EXTDAOEmpresa.ID;		
				
//				+ "	ORDER BY p." + EXTDAOPonto.DIA_DATE + " DESC, "
//				 	+ " p." + EXTDAOPonto.HORA_TIME + " DESC ";
	
		
		String[] ids = db.getResultSetComoVetorDoUmUnicoObjeto(q, null);
		ArrayList<CustomItemList> ret = new ArrayList<CustomItemList>();
		for(int i = 0 ; i < ids.length; i++){
			
			objPessoaEmpresa.select(ids[i]);
			String idPessoa = objPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.PESSOA_ID_INT);
			String idEmpresa = objPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.EMPRESA_ID_INT);
			String idProfissao = objPessoaEmpresa.getStrValueOfAttribute(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT);
			
			String empresa = objPessoaEmpresa.getNomeDaChaveExtrangeira(EXTDAOPessoaEmpresa.EMPRESA_ID_INT);
			String pessoa = objPessoaEmpresa.getNomeDaChaveExtrangeira(EXTDAOPessoaEmpresa.PESSOA_ID_INT);
			String profissao = objPessoaEmpresa.getNomeDaChaveExtrangeira(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT);
			
			EXTDAOPonto.PontoPessoa pontoPessoa = objPonto.getUltimoPontoDaPessoaNaEmpresa(ids[i], idPessoa, idEmpresa, idProfissao);
			pontoPessoa.pessoa = pessoa;
			pontoPessoa.empresa = empresa;
			pontoPessoa.profissao = profissao;
			
			pontoPessoa.idPessoaEmpresa = String.valueOf(ids[i]);
			pontoPessoa.idEmpresa = idEmpresa;
			pontoPessoa.idProfissao = idProfissao;
			
			ret.add(pontoPessoa);	
		}
		return ret;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
return null;
		}
	}
	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		CustomItemList c = listaCompleta.get(position);
		final EXTDAOPonto.PontoPessoa pp = (EXTDAOPonto.PontoPessoa) c;
		PessoaPontoAdapter ppa = new PessoaPontoAdapter(this.activity, (EXTDAOPonto.PontoPessoa)c);
		LayoutInflater vLayoutInflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View vLayoutView = vLayoutInflater.inflate(
				R.layout.item_list_ponto_pessoa_layout, null);
		vLayoutView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent intent = new Intent( activity, ListPontoDaPessoaNaEmpresaActivityMobile.class);
				intent.putExtra(
						OmegaConfiguration.SEARCH_FIELD_ID_PESSOA_EMPRESA, 
						pp.idPessoaEmpresa);
				intent.putExtra(
						OmegaConfiguration.SEARCH_FIELD_ID_PESSOA, 
						pp.idPessoa);
				intent.putExtra(
						OmegaConfiguration.SEARCH_FIELD_ID_EMPRESA, 
						pp.idEmpresa);
				intent.putExtra(
						OmegaConfiguration.SEARCH_FIELD_ID_PROFISSAO, 
						pp.idProfissao);
				intent.putExtra(
						OmegaConfiguration.SEARCH_FIELD_PESSOA, 
						pp.pessoa);
				intent.putExtra(
						OmegaConfiguration.SEARCH_FIELD_EMPRESA, 
						pp.empresa);
				intent.putExtra(
						OmegaConfiguration.SEARCH_FIELD_PROFISSAO, 
						pp.profissao);
				
				activity.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW );
				
			}
		});
		ppa.formatarView(vLayoutView);
		return vLayoutView ;
	}

	@Override
	public ArrayList<CustomItemList> filtraToken(CharSequence cs){
		if(listaCompleta == null) return null;
		ArrayList<CustomItemList> resultado = new ArrayList<CustomItemList>();
		String csMaiusculo = cs.toString().toUpperCase();
//		String csMinusculo= cs.toString().toLowerCase();
		for(int i = 0 ; i < listaCompleta.size(); i++){
			EXTDAOPonto.PontoPessoa itemList = (EXTDAOPonto.PontoPessoa)listaCompleta.get(i);
			
			String nome = itemList.pessoa;
			if(nome != null && nome.contains(csMaiusculo)){
				resultado.add(itemList);
				continue;
			}
			
			nome = itemList.empresa;
			if(nome != null && nome.contains(csMaiusculo)){
				resultado.add(itemList);
				continue;
			}
			
			nome = itemList.profissao;
			if(nome != null && nome.contains(csMaiusculo)){
				resultado.add(itemList);
				continue;
			}
			
			
		}
		
		return resultado;
	}
//	@Override
//	protected ArrayList<Integer> getListIdBySearchParameters(boolean pIsAsc) {
//    	

//	}
//
//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {

//	}


}
