package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.filter.FilterUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.UsuarioItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioCategoriaPermissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioCorporacao;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class UsuarioAdapter extends DatabaseCustomAdapter{
	
//	private static final int ID_TEXT_VIEW_NOME = R.id.usuario_nome;
	
//	private static final int ID_TEXT_VIEW_CATEGORIA_PERMISSAO = R.id.usuario_categoria_permissao;
	
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;


	public String name; 
	public String categoriaPermissaoId;
	public String email;
	
	public Boolean statusBoolean; 
	public Boolean isAdmBoolean;

	public UsuarioAdapter(
			Activity pActivity,
			String name, 
			String email,
			String categoriaPermissaoId,
			Boolean statusBoolean,
			Boolean isAdmBoolean,
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, 
			p_isAsc, 
			EXTDAOUsuario.NAME, 
			false, 
			UsuarioItemList.NOME,
			FormUsuarioActivityMobile.class,
			FilterUsuarioActivityMobile.class);
		this.name = name; 
		this.categoriaPermissaoId = categoriaPermissaoId;
		this.email = email;
		this.statusBoolean = statusBoolean;
		this.isAdmBoolean = isAdmBoolean;
		super.initalizeListCustomItemList();
	}

	@SuppressLint("ViewHolder")
	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			UsuarioItemList vItemList = (UsuarioItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.item_list_layout, null);
			
			PersonalizaItemView(vLayoutView, pPosition);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_1, 
					vItemList.getConteudoContainerItem(UsuarioItemList.NOME), 
					vLayoutView);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_2, 
					vItemList.getConteudoContainerItem(UsuarioItemList.EMAIL), 
					vLayoutView);

			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
						activity, 
						EXTDAOUsuario.NAME,
						pParent, 
						vItemList.getId(),
						FormEditActivityMobile.class, 
						FormUsuarioActivityMobile.class,
						isEditPermitido(),
						isRemovePermitido(), vItemList);
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_USUARIO, 
						vClickListener);
			}
			else {
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_USUARIO);
			}
			
			return vLayoutView;
		} catch (Exception exc) {
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
			//		"UsuarioAdapter: getView()");
		}
		return null;
	}



	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) throws OmegaDatabaseException {

		
		DatabasePontoEletronico database = new DatabasePontoEletronico(this.activity);
		
		try {
			
			String querySelectIdPrestadores = "SELECT DISTINCT t."
					+ EXTDAOUsuario.ID + " ";

			String queryFromTables =  "FROM " + EXTDAOUsuario.NAME + " t, usuario_corporacao uc ";

			String queryWhereClausule = ""; 
			ArrayList<String> args = new ArrayList<String>();

			
			queryWhereClausule += " uc." + EXTDAOUsuarioCorporacao.USUARIO_ID_INT + " = t." + EXTDAOUsuario.ID + " AND " +
						" uc." + EXTDAOUsuarioCorporacao.CORPORACAO_ID_INT + " = ? " ;
			
			args.add(OmegaSecurity.getIdCorporacao());
			
			if(this.email != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " t." + EXTDAOUsuario.NOME + " LIKE ?" ;
				else 
					queryWhereClausule += " AND t." + EXTDAOUsuario.NOME + " LIKE ?" ;

				args.add('%' + this.email + '%' );
			}

			if(this.name != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " t." + Attribute.getNomeAtributoNormalizado(EXTDAOUsuario.NOME) + " LIKE ?" ;
				else 
					queryWhereClausule += " AND t." + Attribute.getNomeAtributoNormalizado(EXTDAOUsuario.NOME) + " LIKE ?" ;

				args.add('%' + HelperString.getWordWithoutAccent(this.name) + '%' );
			}


			if(categoriaPermissaoId != null){
				queryFromTables +=  ", " + EXTDAOUsuarioCategoriaPermissao.NAME + " fe ";
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " fe." + EXTDAOUsuarioCategoriaPermissao.USUARIO_ID_INT + " = t." + EXTDAOUsuario.ID;
				else 
					queryWhereClausule += " AND fe." + EXTDAOUsuarioCategoriaPermissao.ID + " = t." + EXTDAOUsuario.ID;

				queryWhereClausule += " AND fe." + EXTDAOUsuarioCategoriaPermissao.CATEGORIA_PERMISSAO_ID_INT + " = ?" ;
				args.add(categoriaPermissaoId);
			}
//			boolean vIsTabelaUsuarioCorporacaoInFrom = false;
//			if(isAdmBoolean != null && isAdmBoolean == true){
//				
//				queryFromTables +=  ", " + EXTDAOUsuarioCorporacao.NAME + " uc ";
//				vIsTabelaUsuarioCorporacaoInFrom = true;
//				if(queryWhereClausule.length() ==  0)
//					queryWhereClausule += " uc." + EXTDAOUsuarioCorporacao.USUARIO_ID_INT + " = t." + EXTDAOUsuario.ID;
//				else 
//					queryWhereClausule += " AND uc." + EXTDAOUsuarioCorporacao.ID + " = t." + EXTDAOUsuario.ID;
//
//				queryWhereClausule += " AND uc." + EXTDAOUsuarioCorporacao.IS_ADM_BOOLEAN + " = ?" ;
//				args.add(HelperString.valueOfBooleanSQL(isAdmBoolean));
//			}
//			
//			if(this.statusBoolean != null  && statusBoolean){
//				if(!vIsTabelaUsuarioCorporacaoInFrom){
//					queryFromTables +=  ", " + EXTDAOUsuarioCorporacao.NAME + " uc ";
//					vIsTabelaUsuarioCorporacaoInFrom = true;
//					if(queryWhereClausule.length() ==  0)
//						queryWhereClausule += " uc." + EXTDAOUsuarioCorporacao.USUARIO_ID_INT + " = t." + EXTDAOUsuario.ID;
//					else 
//						queryWhereClausule += " AND uc." + EXTDAOUsuarioCorporacao.ID + " = t." + EXTDAOUsuario.ID;
//				}
//				String vStrStatus = "";
//				if(statusBoolean) vStrStatus = "1";
//				else vStrStatus = "0";
//				
//				if(queryWhereClausule.length() ==  0)
//					queryWhereClausule += " uc." + EXTDAOUsuarioCorporacao.STATUS_BOOLEAN + " = ?" ;
//				else 
//					queryWhereClausule += " AND uc." + EXTDAOUsuarioCorporacao.STATUS_BOOLEAN + " = ?" ;
//
//				args.add(vStrStatus);
//			}

			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY t." + Attribute.getNomeAtributoNormalizado(EXTDAOUsuario.NOME)
						+ " DESC";
			} else {
				orderBy += " ORDER BY  t." + Attribute.getNomeAtributoNormalizado(EXTDAOUsuario.NOME)
						+ " ASC";
			}

			String query = querySelectIdPrestadores + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

			if(orderBy.length() > 0 ){
				query += orderBy;  
			} 

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			
			Cursor cursor = null;
			try{
				cursor = database.rawQuery(query, vetorArg);
				// Cursor cursor = oh.query(true, EXTDAOUsuario.NAME,
				// new String[]{EXTDAOUsuario.ID_PRESTADOR}, "", new String[]{},
				// null, "", "", "");
				String[] vetorChave = new String[] { EXTDAOUsuario.ID };
	
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}

		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.ADAPTADOR);
		}
		finally
		{

		}
		return null;
	}
	
	@Override
	public ArrayList<CustomItemList> filtraToken(CharSequence cs){
		if(listaCompleta == null) return null;
		ArrayList<CustomItemList> resultado = new ArrayList<CustomItemList>();
		String csMaiusculo = cs.toString().toUpperCase();
		String csMinusculo= cs.toString().toLowerCase();
		for(int i = 0 ; i < listaCompleta.size(); i++){
			UsuarioItemList itemList = (UsuarioItemList)listaCompleta.get(i);
			
			String nome = itemList.getConteudoContainerItem(UsuarioItemList.NOME);
			if(nome != null && nome.contains(csMaiusculo)){
				resultado.add(itemList);
				continue;
			}
			
			String email = itemList.getConteudoContainerItem(UsuarioItemList.EMAIL);
			if(email != null && email.contains(csMinusculo)){
				resultado.add(itemList);
				continue;
			}
			
			String telefone = itemList.getConteudoContainerItem(UsuarioItemList.TELEFONE);
			if(telefone != null && telefone.contains(csMaiusculo)){
				resultado.add(itemList);
				continue;
			}
			
			String celular = itemList.getConteudoContainerItem(UsuarioItemList.CELULAR);
			if(celular != null && celular.contains(csMaiusculo)){
				resultado.add(itemList);
				continue;
			}
		}
		
		return resultado;
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {
try{
		EXTDAOUsuario objUsuario = (EXTDAOUsuario) pObj; 
//		DatabasePontoEletronico database= (DatabasePontoEletronico) vEXTDAOUsuario.getDatabase();

		EXTDAOPessoa objPessoa = objUsuario.getObjPessoa();
		
		Attribute vAttrId = pObj.getAttribute(EXTDAOUsuario.ID);
		String vId = null;

		if(vAttrId != null){
			vId = vAttrId.getStrValue();
		}
		if(vId == null){
			return null;
		}
//			
//
//		EXTDAOUsuarioCategoriaPermissao vObjUsuarioCategoriaPermissao = new EXTDAOUsuarioCategoriaPermissao(database);
//		vObjUsuarioCategoriaPermissao.setAttrStrValue(
//				EXTDAOUsuarioCategoriaPermissao.USUARIO_ID_INT, 
//				vEXTDAOUsuario.getAttribute(EXTDAOUsuario.ID).getStrValue());
//		
//		vObjUsuarioCategoriaPermissao.setAttrStrValue(
//				EXTDAOUsuarioCategoriaPermissao.CATEGORIA_PERMISSAO_ID_INT, 
//				vEXTDAOUsuario.getAttribute(EXTDAOCategoriaPermissao.ID).getStrValue());
//		
//
//		String vIdCorporacao = OmegaSecurity.getIdCorporacao();
//		String[] vetorArg = {vId, vIdCorporacao};
//		String queryCategoriaPermissao = "SELECT cp.id " +
//				"FROM " +
//				"usuario_categoria_permissao ucp, " +
//				"categoria_permissao cp " +
//				"WHERE " +
//				"ucp.usuario_id_INT = ? AND " +
//				"ucp.categoria_permissao_id_INT = cp.id AND " +
//				"cp.corporacao_id_INT = ? " +
//				"ORDER BY cp.nome " ;
//		int vNumeroArg = 1;
//		String[] vetorCategoriaPermissaoId =  database.getResultSetDoPrimeiroObjeto(queryCategoriaPermissao, vetorArg, vNumeroArg);
//		String vStrCategoriaPermissao = null;
//		if(vetorCategoriaPermissaoId != null)
//			if(vetorCategoriaPermissaoId.length == 1){
//				vStrCategoriaPermissao = vetorCategoriaPermissaoId[0];
//				EXTDAOCategoriaPermissao vObjCP = new EXTDAOCategoriaPermissao(pObj.getDatabase());
//				vObjCP.setAttrStrValue(EXTDAOCategoriaPermissao.ID, vStrCategoriaPermissao);
//				vObjCP.select();
//				vStrCategoriaPermissao = vObjCP.getStrValueOfAttribute(EXTDAOCategoriaPermissao.NOME);
//			}
 
		UsuarioItemList vItemList = new UsuarioItemList(  
				objUsuario.getAttribute(EXTDAOUsuario.ID).getStrValue(),
				objUsuario.getAttribute(EXTDAOUsuario.NOME).getStrValue(),
				objUsuario.getAttribute(EXTDAOUsuario.EMAIL).getStrValue(),
				objPessoa != null ? objPessoa.getStrValueOfAttribute(EXTDAOPessoa.TELEFONE) : null,
				objPessoa != null ? objPessoa.getStrValueOfAttribute(EXTDAOPessoa.CELULAR) : null	
		);

		return vItemList;
}catch(Exception ex){
	SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
return null;
}
	}




}
