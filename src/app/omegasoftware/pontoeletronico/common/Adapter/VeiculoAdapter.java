package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormVeiculoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.VeiculoItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOModelo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculo;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class VeiculoAdapter extends DatabaseCustomAdapter{

//	private static final int ID_TEXT_VIEW_PLACA = R.id.veiculo_name;
//	private static final int ID_TEXT_VIEW_NOME_MODELO = R.id.veiculo_nome_modelo;
//	private static final int ID_TEXT_VIEW_ANO_MODELO = R.id.veiculo_ano_modelo;
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;


	public String placa; 
	public String nomeModelo;
	public String anoModelo;


	public VeiculoAdapter(
			Activity pActivity,
			String placa, 
			String nomeModeloId,
			String anoModeloId,
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, 
				p_isAsc, 
				EXTDAOVeiculo.NAME, 
				false,
				VeiculoItemList.PLACA, 
				FormVeiculoActivityMobile.class,
				null);
		this.placa = placa; 
		this.nomeModelo = nomeModeloId;
		this.anoModelo = anoModeloId;
		initalizeListCustomItemList();
	}
	
	@SuppressLint("ViewHolder")
	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			VeiculoItemList vItemList = (VeiculoItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.item_list_layout, null);

			PersonalizaItemView(vLayoutView, pPosition);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_1, 
					vItemList.getConteudoContainerItem(VeiculoItemList.PLACA), 
					vLayoutView);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_2, 
					vItemList.getConteudoContainerItem(VeiculoItemList.NOME_MODELO), 
					vLayoutView);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_3, 
					vItemList.getConteudoContainerItem(VeiculoItemList.ANO_MODELO), 
					vLayoutView);

			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
						activity, 
						EXTDAOVeiculo.NAME,
						pParent, 
						vItemList.getId(),
						FormEditActivityMobile.class, 
						FormVeiculoActivityMobile.class,
						isEditPermitido(),
						isRemovePermitido(), vItemList);
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_VEICULO, 
						vClickListener);
			}
			else {
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_VEICULO);
			}
			return vLayoutView;
		} catch (Exception exc) {
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
			//		"VeiculoAdapter: getView()");
		}
		return null;
	}




//	@Override
//	public void SortList(boolean pAsc) {
//		Collections.sort(list, new NameCustomItemListComparator(pAsc));
//		this.notifyDataSetChanged();
//	}

	@Override
	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {

		
		DatabasePontoEletronico database=null;
		try {
			database = new DatabasePontoEletronico(this.activity);
		} catch (OmegaDatabaseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			
			String querySelectIdPrestadores = "SELECT DISTINCT v."
					+ EXTDAOVeiculo.ID + ", v." + EXTDAOVeiculo.PLACA;

			String queryFromTables =  " FROM " + EXTDAOVeiculo.NAME + " v" ;

			String queryWhereClausule = ""; 
			ArrayList<String> args = new ArrayList<String>();
			boolean vIsModeloInWhere = false;
			
			
			queryWhereClausule += " v." + EXTDAOVeiculo.CORPORACAO_ID_INT + " = ? " ;
			
			args.add(OmegaSecurity.getIdCorporacao());
			
			if( nomeModelo != null){
				if(!vIsModeloInWhere){
					queryFromTables += ", " + EXTDAOModelo.NAME + " m ";
					if(queryWhereClausule.length() ==  0)
						queryWhereClausule += " m." + EXTDAOModelo.ID + "= v." + EXTDAOVeiculo.MODELO_ID_INT;
					else 
						queryWhereClausule += "AND m." + EXTDAOModelo.ID + "= v." + EXTDAOVeiculo.MODELO_ID_INT;
					vIsModeloInWhere = true;
				}
			
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " m." + Attribute.getNomeAtributoNormalizado(EXTDAOModelo.NOME) + " LIKE ? ";
				else 
					queryWhereClausule += "AND m." + Attribute.getNomeAtributoNormalizado(EXTDAOModelo.NOME) + " LIKE ? ";
				
				args.add( HelperString.getWordWithoutAccent(nomeModelo));
			}

			if(anoModelo != null ){
				if(!vIsModeloInWhere){
					queryFromTables += ", " + EXTDAOModelo.NAME + " m ";
					if(queryWhereClausule.length() ==  0)
						queryWhereClausule += " m." + EXTDAOModelo.ID + "= v." + EXTDAOVeiculo.MODELO_ID_INT;
					else 
						queryWhereClausule += "AND m." + EXTDAOModelo.ID + "= v." + EXTDAOVeiculo.MODELO_ID_INT;
					vIsModeloInWhere = true;
				}
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " m." + EXTDAOModelo.ANO_INT + " = ? " ;
				else 
					queryWhereClausule += " AND m." + EXTDAOModelo.ANO_INT + " = ? " ;

				args.add(anoModelo);
			}


			if(this.placa != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " v." + EXTDAOVeiculo.PLACA + " LIKE ?" ;
				else 
					queryWhereClausule += " AND v." + EXTDAOVeiculo.PLACA + " LIKE ?" ;

				args.add('%' + this.placa + '%' );
			}

			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY v." + EXTDAOVeiculo.PLACA
						+ " DESC";
			} else {
				orderBy += " ORDER BY  v." + EXTDAOVeiculo.PLACA
						+ " ASC";
			}

			String query = querySelectIdPrestadores + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

			if(orderBy.length() > 0 ){
				query += orderBy;  
			} 

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			
			Cursor cursor = null;
			try{
				cursor = database.rawQuery(query, vetorArg);
				// Cursor cursor = oh.query(true, EXTDAOVeiculo.NAME,
				// new String[]{EXTDAOVeiculo.ID_PRESTADOR}, "", new String[]{},
				// null, "", "", "");
				String[] vetorChave = new String[] { EXTDAOVeiculo.ID };
	
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}

		} catch (Exception e) {
			
			//e.printStackTrace();
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
		finally
		{
			database.close();
		}
		return null;
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {

		EXTDAOVeiculo vEXTDAOVeiculo = (EXTDAOVeiculo) pObj; 
		DatabasePontoEletronico database= (DatabasePontoEletronico) vEXTDAOVeiculo.getDatabase();

		Attribute vAttrId = pObj.getAttribute(EXTDAOPessoa.ID);
		String vId = null;

		if(vAttrId != null){
			vId = vAttrId.getStrValue();
		}
		if(vId == null){
			return null;
		}
		String[] vetorArg = {vId};

		String querySetor = "SELECT m.nome, m.ano_INT " +
				"FROM " +
				"veiculo v, " +
				"modelo m " +
				"WHERE " +
				"v.id = ? AND " +
				"m.id = v.modelo_id_INT " + 
				"ORDER BY m.nome, m.ano_INT" ;
		int vNumeroArg = 2;
		String[] vetorModelo =  database.getResultSetDoPrimeiroObjeto(querySetor, vetorArg, vNumeroArg);
		String vNomeModelo = null;
		String vAnoModelo = null;
		if(vetorModelo != null) 
			if(vetorModelo.length == vNumeroArg) {
				vNomeModelo = vetorModelo[0];
				vAnoModelo = vetorModelo[1];
			}
		
		VeiculoItemList vItemList = new VeiculoItemList(  
				vEXTDAOVeiculo.getAttribute(EXTDAOVeiculo.ID).getStrValue(),
				vEXTDAOVeiculo.getAttribute(EXTDAOVeiculo.PLACA).getStrValue(),
				vNomeModelo,
				vAnoModelo);

		return vItemList;
	}




}
