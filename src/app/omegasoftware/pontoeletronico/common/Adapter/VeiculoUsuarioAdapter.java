package app.omegasoftware.pontoeletronico.common.Adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormEditActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormVeiculoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.VeiculoUsuarioItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOModelo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculoUsuario;
import app.omegasoftware.pontoeletronico.listener.EditCustomOnLongClickListener;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class VeiculoUsuarioAdapter extends DatabaseCustomAdapter{

//	private static final int ID_TEXT_VIEW_PLACA = R.id.veiculo_placa;
//	private static final int ID_TEXT_VIEW_USUARIO = R.id.nome_usuario;
//	private static final int ID_TEXT_VIEW_VEICULO_MODELO = R.id.nome_veiculo_modelo;

	Activity activity;
	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;

	public String placa; 
	public String nomeModelo;
	public String anoModelo;
	public String idUsuario;

	public VeiculoUsuarioAdapter(
			Activity pActivity,
			String placa, 
			String nomeModelo,
			String anoModeloId,
			String emailUsuario,
			boolean p_isAsc) throws OmegaDatabaseException
	{
		super(pActivity, p_isAsc, EXTDAOVeiculoUsuario.NAME, VeiculoUsuarioItemList.EMAIL_USUARIO, false);
		activity = pActivity; 
		this.placa = placa; 
		this.nomeModelo = nomeModelo;
		this.anoModelo = anoModeloId;
		this.idUsuario = emailUsuario;
		initalizeListCustomItemList();
	}

	public View getView(int pPosition, View pView, ViewGroup pParent) {
		try {			
			VeiculoUsuarioItemList vItemList = (VeiculoUsuarioItemList)this.getItem(pPosition);
			LayoutInflater vLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View vLayoutView = vLayoutInflater.inflate(
					R.layout.item_list_layout, null);

			PersonalizaItemView(vLayoutView, pPosition);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_1, 
					vItemList.getConteudoContainerItem(VeiculoUsuarioItemList.PLACA), 
					vLayoutView);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_2, 
					vItemList.getConteudoContainerItem(VeiculoUsuarioItemList.NOME_MODELO), 
					vLayoutView);

			CreateTextView(
					ID_TEXT_VIEW_DESCRICAO_3, 
					vItemList.getConteudoContainerItem(VeiculoUsuarioItemList.EMAIL_USUARIO), 
					vLayoutView);


			if(isEditPermitido() || isRemovePermitido()){
				OnLongClickListener vClickListener = new EditCustomOnLongClickListener(
						activity, 
						EXTDAOVeiculoUsuario.NAME,
						pParent, 
						vItemList.getId(),
						FormEditActivityMobile.class, 
						FormVeiculoUsuarioActivityMobile.class,
						isEditPermitido(),
						isRemovePermitido(), vItemList);
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_VEICULO_USUARIO, 
						vClickListener);
			}
			else {
				CreateDetailButton(vLayoutView, 
						ID_CUSTOM_BUTTON_DETAIL, 
						vItemList.getId(), 
						OmegaConfiguration.SEARCH_MODE_VEICULO_USUARIO);
			}
			return vLayoutView;
		} catch (Exception exc) {
			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
			//		"EmpresaAdapter: getView()");
		}
		return null;
	}



	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {

		
		DatabasePontoEletronico database=null;
		try {
			database = new DatabasePontoEletronico(this.activity);
		} catch (OmegaDatabaseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {

			String querySelectIdPrestadores = "SELECT vu.id, v.placa, u.email";

			String queryFromTables =  " FROM " + EXTDAOVeiculoUsuario.NAME + " vu, "
					+ EXTDAOUsuario.NAME + " u, "
					+ EXTDAOVeiculo.NAME + " v ";

			String queryWhereClausule = ""; 
			ArrayList<String> args = new ArrayList<String>();

			boolean vIsTableModeloInClause = false;

			queryWhereClausule += " vu." + EXTDAOVeiculoUsuario.CORPORACAO_ID_INT + " = ? " ;
			args.add(OmegaSecurity.getIdCorporacao());
			
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " u." + EXTDAOUsuario.ID + " = vu." + EXTDAOVeiculoUsuario.USUARIO_ID_INT ;
			else 
				queryWhereClausule += " AND u." + EXTDAOUsuario.ID + " = vu." + EXTDAOVeiculoUsuario.USUARIO_ID_INT ;

			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " vu." + EXTDAOVeiculoUsuario.VEICULO_ID_INT + " = v." + EXTDAOVeiculo.ID ;
			else
				queryWhereClausule += " AND vu." + EXTDAOVeiculoUsuario.VEICULO_ID_INT + " =  v." + EXTDAOVeiculo.ID;

			if(this.placa != null ){
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " vu." + EXTDAOVeiculo.PLACA + " LIKE ?" ;
				else 
					queryWhereClausule += " AND vu." + EXTDAOVeiculo.PLACA + " LIKE ?" ;

				args.add('%' + this.placa + '%' );
			}

			if( nomeModelo != null){
				if(!vIsTableModeloInClause){
					queryFromTables +=  ", " + EXTDAOModelo.NAME + " m ";
					if(queryWhereClausule.length() ==  0)
						queryWhereClausule += " m." + EXTDAOModelo.ID + " = v." + EXTDAOVeiculo.MODELO_ID_INT ;
					else 
						queryWhereClausule += " AND m." + EXTDAOModelo.ID + " = v." + EXTDAOVeiculo.MODELO_ID_INT ;

					vIsTableModeloInClause = true;
				}

				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " m." + Attribute.getNomeAtributoNormalizado(EXTDAOModelo.NOME) + " LIKE ? " ;
				else
					queryWhereClausule += " AND m." + Attribute.getNomeAtributoNormalizado(EXTDAOModelo.NOME) + " LIKE ? " ;

				args.add("%" + HelperString.getWordWithoutAccent(nomeModelo) + "%");
			}

			if(anoModelo != null ){
				if(!vIsTableModeloInClause){
					queryFromTables +=  ", " + EXTDAOModelo.NAME + " m ";

					if(queryWhereClausule.length() ==  0)
						queryWhereClausule += " m." + EXTDAOModelo.ID + " = v." + EXTDAOVeiculo.MODELO_ID_INT ;
					else 
						queryWhereClausule += " AND m." + EXTDAOModelo.ID + " = v." + EXTDAOVeiculo.MODELO_ID_INT ;

					vIsTableModeloInClause = true;
				}
				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " m." + EXTDAOModelo.ANO_INT + " = ? " ;
				else
					queryWhereClausule += " AND m." + EXTDAOModelo.ANO_INT + " = ? " ;
				args.add(anoModelo);
			}

			if(this.idUsuario != null ){

				if(queryWhereClausule.length() ==  0)
					queryWhereClausule += " u." + EXTDAOUsuario.ID + " = ? ";
				else 
					queryWhereClausule += " AND u." + EXTDAOUsuario.ID + " = ? ";

				args.add(this.idUsuario );
			}

			String orderBy = "";
			if (!pIsAsc) {
				orderBy += " ORDER BY v." +EXTDAOVeiculo.PLACA + ", u." + EXTDAOUsuario.EMAIL
						+ " DESC";
			} else {
				orderBy += " ORDER BY  v." +EXTDAOVeiculo.PLACA + ", u." + EXTDAOUsuario.EMAIL
						+ " ASC";
			}

			String query = querySelectIdPrestadores + queryFromTables;
			if(queryWhereClausule.length() > 0 )
				query 	+= " WHERE " + queryWhereClausule;

			if(orderBy.length() > 0 ){
				query += orderBy;  
			} 

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			
			Cursor cursor = null;
			try{
				cursor = database.rawQuery(query, vetorArg);
				// Cursor cursor = oh.query(true, EXTDAOVeiculo.NAME,
				// new String[]{EXTDAOVeiculo.ID_PRESTADOR}, "", new String[]{},
				// null, "", "", "");
				String[] vetorChave = new String[] { EXTDAOVeiculoUsuario.ID };
	
				return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}

		} catch (Exception e) {
			
			e.printStackTrace();
			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
		}
		finally
		{
			database.close();
		}
		return null;
	}

	@Override
	public CustomItemList getCustomItemListOfTable(Table pObj) {
try{
		EXTDAOVeiculoUsuario vEXTDAOVeiculoUsuario = (EXTDAOVeiculoUsuario) pObj; 
		DatabasePontoEletronico database= (DatabasePontoEletronico) vEXTDAOVeiculoUsuario.getDatabase();
		String vVeiculoId = vEXTDAOVeiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.VEICULO_ID_INT);

		String vUsuarioId = vEXTDAOVeiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.USUARIO_ID_INT);
		EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(pObj.getDatabase());
		vObjUsuario.setAttrValue(EXTDAOUsuario.ID, vUsuarioId);
		vObjUsuario.select();
		String vEmailUsuario = vObjUsuario.getStrValueOfAttribute(EXTDAOUsuario.EMAIL); 

		EXTDAOVeiculo vObjVeiculo = new EXTDAOVeiculo(pObj.getDatabase());
		vObjVeiculo.setAttrValue(EXTDAOVeiculo.ID, vVeiculoId);
		vObjVeiculo.select();

		String vPlaca = vObjVeiculo.getStrValueOfAttribute(EXTDAOVeiculo.PLACA);

		Attribute vAttrId = pObj.getAttribute(EXTDAOPessoa.ID);
		String vId = null;

		if(vAttrId != null){
			vId = vAttrId.getStrValue();
		}
		if(vId == null){
			return null;
		}
		String[] vetorArg = {vId};

		String querySetor = "SELECT m.nome, m.ano_INT " +
				"FROM " +
				"veiculo v, " +
				"modelo m " +
				"WHERE " +
				"v.id = ? AND " +
				"m.id = v.modelo_id_INT " + 
				"ORDER BY m.nome, m.ano_INT" ;
		int vNumeroArg = 2;
		String[] vetorSetor =  database.getResultSetDoPrimeiroObjeto(querySetor, vetorArg, vNumeroArg);
		String vNomeModelo = null;
		String vAnoModelo = null;

		if(vetorSetor != null) 
			if(vetorSetor.length == vNumeroArg){
				vNomeModelo = vetorSetor[0];
				vAnoModelo = vetorSetor[1];
			}

		VeiculoUsuarioItemList vItemList = new VeiculoUsuarioItemList(  
				vVeiculoId,
				vPlaca,
				vNomeModelo,
				vAnoModelo, 
				vEmailUsuario,
				vUsuarioId);

		return vItemList;
}catch(Exception ex){
	SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
return null;
}
	}

}
