package app.omegasoftware.pontoeletronico.common.Adapter;

//public class VeiculoUsuarioAtivoAdapter extends DatabaseCustomAdapter{
//
//	private static final int ID_TEXT_VIEW_PLACA = R.id.veiculo_placa;
//	private static final int ID_TEXT_VIEW_USUARIO = R.id.nome_usuario;
//	private static final int ID_TEXT_VIEW_VEICULO_MODELO = R.id.nome_veiculo_modelo;
//	
//	Activity activity;
//	private static final int ID_CUSTOM_BUTTON_DETAIL = R.id.custom_button_detail;
//	
//	public VeiculoUsuarioAtivoAdapter(
//			Activity pActivity,
//			boolean p_isAsc) throws OmegaDatabaseException
//	{
//		super(pActivity, p_isAsc, EXTDAOVeiculoUsuario.NAME, VeiculoUsuarioAtivoItemList.PLACA, false);
//		activity = pActivity; 
//		initalizeListCustomItemList();
//	}
//
//	
//	
//	public View getView(int pPosition, View pView, ViewGroup pParent) {
//		try {			
//			VeiculoUsuarioAtivoItemList vVeiculoUsuarioItemList = (VeiculoUsuarioAtivoItemList)this.getItem(pPosition);
//			LayoutInflater vLayoutInflater = (LayoutInflater) activity
//					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//			View vLayoutView = vLayoutInflater.inflate(
//					R.layout.veiculo_usuario_item_list_layout, null);
//			if((pPosition % 2) == 0)
//			{
//				((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.color.gray_189);
//			}
//			else
//			{
//				((LinearLayout) vLayoutView.findViewById(R.id.search_result_main_layout)).setBackgroundResource(R.color.white);
//			}
//
//			CreateTextView(
//					ID_TEXT_VIEW_PLACA, 
//					vVeiculoUsuarioItemList.getConteudoContainerItem(VeiculoUsuarioAtivoItemList.PLACA), 
//					vLayoutView);
//			
//			CreateTextView(
//					ID_TEXT_VIEW_VEICULO_MODELO, 
//					vVeiculoUsuarioItemList.getConteudoContainerItem(VeiculoUsuarioAtivoItemList.NOME_VEICULO_MODELO), 
//					vLayoutView);
//
//			CreateTextView(
//					ID_TEXT_VIEW_USUARIO, 
//					vVeiculoUsuarioItemList.getConteudoContainerItem(VeiculoUsuarioAtivoItemList.NOME_USUARIO), 
//					vLayoutView);
//
//			CreateVeiculoDetailButton(vLayoutView, 
//					ID_CUSTOM_BUTTON_DETAIL, 
//					vVeiculoUsuarioItemList.getId(), 
//					vVeiculoUsuarioItemList);
//
//			return vLayoutView;
//		} catch (Exception exc) {
//			//Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY,
//			//		"EmpresaAdapter: getView()");
//		}
//		return null;
//	}
//
//	
//	protected void CreateVeiculoDetailButton(
//			View pView, 
//			int pRLayoutId, 
//			String pIdCustomItemList, 
//			VeiculoUsuarioAtivoItemList pVeiculoItemList)
//	{
//		Button vButton = (Button) pView.findViewById(pRLayoutId);
//		vButton.setClickable(true);
//		vButton.setFocusable(true);	   
//		//		if(DoutorLaserActivity.isTablet){
//		//			
//		//			vDetailButtonClickListenerTablet =  new DetailButtonClickListenerTablet(this.activity, pIdCustomItemList, p_searchMode);
//		//			vButton.setOnClickListener(vDetailButtonClickListenerTablet);
//		//			CreateLinearLayout(R.id.linearLayoutfuncionariodetails1, pView, vDetailButtonClickListenerTablet);
//		//		} else {
//		vButton.setOnClickListener(
//				new VeiculoUsuarioAtivoButtonClickListener(
//						activity,
//						pIdCustomItemList, 
//						pVeiculoItemList));
//		
//		CreateLinearLayout(
//				R.id.linearLayoutfuncionariodetails1, 
//				pView, 
//				new VeiculoUsuarioAtivoButtonClickListener(
//						activity,
//						pIdCustomItemList, 
//						pVeiculoItemList
//				));
//		//		}
//
//	}
//	
//	@Override
//	public void SortList(boolean pAsc) {
//		Collections.sort(listaFiltrada, new NameCustomItemListComparator(pAsc, VeiculoUsuarioAtivoItemList.PLACA));
//		this.notifyDataSetChanged();
//	}
//
//	@Override
//	protected ArrayList<Long> getListIdBySearchParameters(boolean pIsAsc) {
//
//		try {
//			ArrayList<Long> vArrayListLong = new ArrayList<Long>();
//			for (VeiculoCustomOverlayItem vCustom : MapaListMarkerActivity.listContainerMarcadorMapaCarros) {
//				vArrayListLong.add((long)vCustom.getIdVeiculoUsuario());
//			}
//			
//			return vArrayListLong;
//		} catch (Exception e) {
//			
//			e.printStackTrace();
//			Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
//		}
//		return null;
//	}
//	
//	@Override
//	public CustomItemList getCustomItemListOfTable(Table pObj) {
//try{
//		EXTDAOVeiculoUsuario vEXTDAOVeiculoUsuario = (EXTDAOVeiculoUsuario) pObj; 
//		DatabasePontoEletronico database= (DatabasePontoEletronico) vEXTDAOVeiculoUsuario.getDatabase();
//
//		Attribute vAttrId = pObj.getAttribute(EXTDAOVeiculoUsuario.ID);
//		String vId = null;
//
//		if(vAttrId != null){
//			vId = vAttrId.getStrValue();
//		}
//		if(vId == null){
//			return null;
//		}
//		
//		VeiculoCustomOverlayItem vOverlayItem = MapaListMarkerActivity.getCustomOverlayItemDoVeiculoUsuario(HelperInteger.parserInteger(vId)); 
//		
//		String vIdVeiculo = vEXTDAOVeiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.VEICULO_ID_INT);
//		String vIdUsuario = vEXTDAOVeiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.USUARIO_ID_INT);
//		if(vOverlayItem == null) return null;
//		else if(vIdVeiculo == null || vIdUsuario == null) return null;
//		
//		EXTDAOVeiculo vEXTDAOVeiculo = new EXTDAOVeiculo(database);
//		vEXTDAOVeiculo.setAttrValue(EXTDAOVeiculo.ID, vIdVeiculo);
//		vEXTDAOVeiculo.select();
//		
//		EXTDAOUsuario vEXTDAOUsuario = new EXTDAOUsuario(database);
//		vEXTDAOUsuario.setAttrValue(EXTDAOUsuario.ID, vIdUsuario);
//		vEXTDAOUsuario.select();
//		
//		String vIdModelo = vEXTDAOVeiculo.getStrValueOfAttribute(EXTDAOVeiculo.MODELO_ID_INT);
//		EXTDAOModelo vEXTDAOModelo = new EXTDAOModelo(database);
//		vEXTDAOModelo.setAttrValue(EXTDAOModelo.ID, vIdModelo);
//		vEXTDAOModelo.select();
//		
//		String vPlaca = vEXTDAOVeiculo.getStrValueOfAttribute(EXTDAOVeiculo.PLACA);
//		String vNomeModelo = vEXTDAOModelo.getStrValueOfAttribute(EXTDAOModelo.NOME);
//		String vNomeUsuario = vEXTDAOUsuario.getStrValueOfAttribute(EXTDAOUsuario.NOME);
//		if(vPlaca != null){
//			if(vPlaca.length() > 0 ){
//				VeiculoUsuarioAtivoItemList vItemList = new VeiculoUsuarioAtivoItemList(
//					vId,
//					vPlaca,
//					vNomeUsuario,
//					vNomeModelo, 
//					vOverlayItem.getPoint(), 
//					vOverlayItem.getDatetime());
//				
//				return vItemList;
//			} else return null;
//		} else return null;
//}catch(Exception ex){
//	SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//return null;
//}
//	}
//}
