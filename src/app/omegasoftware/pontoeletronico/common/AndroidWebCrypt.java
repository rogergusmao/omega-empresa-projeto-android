package app.omegasoftware.pontoeletronico.common;

import java.net.URLEncoder;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.content.Context;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.http.HelperHttp;
import android.util.Base64;
public class AndroidWebCrypt {
	 private static String CIPHER_NAME = "AES/CBC/PKCS5PADDING";
	    private static int CIPHER_KEY_LEN = 16; //128 bits
	    private static String CIPHER_KEY = "asdfawefqawefves"; //128 bits
	    private static String IV = "testetesteteste1";
	    
	    /**
	     * Encrypt data using AES Cipher (CBC) with 128 bit key
	     * 
	     * 
	     * @param key  - key to use should be 16 bytes long (128 bits)
	     * @param iv - initialization vector
	     * @param data - data to encrypt
	     * @return encryptedData data in base64 encoding with iv attached at end after a :
	     */
	    public static String encrypt(  String data) {
	    	//
	    	String key = CIPHER_KEY;
	        try {
	            if (key.length() < CIPHER_KEY_LEN) {
	                int numPad = CIPHER_KEY_LEN - key.length();

	                for(int i = 0; i < numPad; i++){
	                    key += "0"; //0 pad to len 16 bytes
	                }

	            } else if (key.length() > CIPHER_KEY_LEN) {
	                key = key.substring(0, CIPHER_KEY_LEN); //truncate to 16 bytes
	            }

	            String iv = IV;
	            IvParameterSpec initVector = new IvParameterSpec(iv.getBytes("UTF-8"));
	            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

	            Cipher cipher = Cipher.getInstance(CIPHER_NAME);
	            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, initVector);

	            byte[] encryptedData = cipher.doFinal((data.getBytes()));
	            
	            String base64_EncryptedData = Base64.encodeToString(encryptedData, Base64.NO_PADDING);
	            base64_EncryptedData = base64_EncryptedData.replace('\n', '\0').trim();
	            
	            
	            String base64_IV = Base64.encodeToString(iv.getBytes("UTF-8"), Base64.NO_PADDING);
	            //base64_IV = base64_IV.replace('\n', '\0').trim();
	            
	            String res = base64_EncryptedData + ":" + base64_IV;
	            
	             
	            return res;

	        } catch (Exception ex) {
	            SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
	        }

	        return null;
	    }
	    
	    /**
	     * Decrypt data using AES Cipher (CBC) with 128 bit key
	     * 
	     * @param key - key to use should be 16 bytes long (128 bits)
	     * @param data - encrypted data with iv at the end separate by :
	     * @return decrypted data string
	     */

	    public static String decrypt( String data) {
	        try {
	        	String key = CIPHER_KEY;
	            String[] parts = data.split(":");
	            
	            IvParameterSpec iv = new IvParameterSpec(Base64.decode(parts[1], Base64.NO_PADDING));
	            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

	            Cipher cipher = Cipher.getInstance(CIPHER_NAME);
	            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
	            
	            byte[] decodedEncryptedData = Base64.decode(parts[0], Base64.NO_PADDING);

	            byte[] original = cipher.doFinal(decodedEncryptedData);

	            return new String(original);
	        } catch (Exception ex) {
	        	SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
	        }

	        return null;
	    }
	    
	    public static String encryptUrl(
	    		Context c, 
	    		String p_url, 
	    		String[] parametros, 
	    		String[] valores){
	    	
			String strGet  = HelperHttp.getStrGet(
					parametros, 
					valores, 
					new String[] {"data" }, 
					new String[] {String.valueOf( HelperDate.getTimestampSegundosUTC(c))});

			String strCrypt = AndroidWebCrypt.encrypt(strGet);
			if(strCrypt == null) return null;
			String url = null;
			if(p_url.contains("?"))
				url = p_url+ "&chave=" + URLEncoder.encode( strCrypt);
			else 
				url = p_url+ "?chave=" + URLEncoder.encode( strCrypt);
			return url;

	    }

}