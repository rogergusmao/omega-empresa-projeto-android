package app.omegasoftware.pontoeletronico.common;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;

import app.omegasoftware.pontoeletronico.date.HelperDate;

/**
 * Created by W10 on 18/02/2018.
 */

public class CacheSharedPreference {
Context c;

    final String PREFIXO_PRAZO = "CACHE_PP_";
    final String PREFIXO_ULTIMA_ATUALIZACAO_SEC = "CACHE_UAS_";
    final String PREFIXO_ULTIMA_ATUALIZACAO_OFFSEC = "CACHE_UAO_";
    final String CACHE_NAME = "CACHE";
    SimpleDateFormat sdf = new SimpleDateFormat();
    HelperSharedPreference hsp = new HelperSharedPreference(CACHE_NAME);

    public CacheSharedPreference(Context c){
        this.c=c;
    }
    public boolean expirou(String chave){
        return expirou(chave, null);
    }
    public boolean expirou(String chave, Integer prazoMin){
        if(prazoMin==null)
            prazoMin = hsp.getValorInt(c,  PREFIXO_PRAZO + chave, -1);

        if(prazoMin == null || prazoMin < 0)
            return true;

        long sec = hsp.getValorLong(c,  PREFIXO_ULTIMA_ATUALIZACAO_SEC + chave, -1);
        int offsec = hsp.getValorInt(c,  PREFIXO_ULTIMA_ATUALIZACAO_OFFSEC+ chave, -1);
        Date d= HelperDate.getDateFromSecOfssec(sec, offsec, sdf);
        if(d == null) return true;

        Date now = new Date();
        HelperDate hd = new HelperDate();
        if(hd.getAbsDiferencaEmMinutos(now) > prazoMin)
            return true;
        else return false;
    }

    public void update(String chave, int prazoMin){

        hsp.saveValorInt(c,  PREFIXO_PRAZO + chave, prazoMin);
        hsp.saveValorLong(c, PREFIXO_ULTIMA_ATUALIZACAO_SEC + chave, HelperDate.getSecOffsetSegundosTimeZone(c));
        hsp.saveValorInt(c, PREFIXO_ULTIMA_ATUALIZACAO_OFFSEC + chave, HelperDate.getOffsetSegundosTimeZone());
    }

}
