package app.omegasoftware.pontoeletronico.common;

import java.util.Comparator;

import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;



public abstract class CustomItemListComparator implements Comparator<Object> {
	protected boolean isAsc;
	protected String nameField;
	public CustomItemListComparator(boolean p_isAsc, String pNameField)
	{
		this.isAsc = p_isAsc;
		this.nameField = pNameField;
	}
	
	public void setAsc(boolean pIsAsc){
		isAsc = pIsAsc;
	}
    public int compare(Object o1, Object o2)
    {
        return compare((CustomItemList)o1, (CustomItemList)o2);
    }
    
    public abstract int compare(CustomItemList o1, CustomItemList o2);
}
