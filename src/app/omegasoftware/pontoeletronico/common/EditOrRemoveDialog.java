package app.omegasoftware.pontoeletronico.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.widget.Button;
import android.widget.Toast;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDetailActivity;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity.CustomDataLoader;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;

public class EditOrRemoveDialog {
	Activity activity;
	//Constants
	public static final String TAG = "EditOrRemoveDialog";

//	private final int FORM_ERROR_DIALOG_REMOCAO_OK = 3;

	protected final int FORM_ERROR_DIALOG_POST = 76;
	//	Se online: remocao atomica
	//	Se offline: nao permite a remocao
//	public final int TYPE_REMOVE_ATOMIC = 1;

	//	Se online ou offline: Remove com o sincronizador
	public final int TYPE_REMOVE_WITH_SYNC = 2;

	//	se online: remocao atomica  
	//	se offline: remocao com o sincronizador
//	public final int TYPE_REMOVE_WITH_OR_WITHOUT_SYNC = 3;

	CustomItemList item;

	//EditText
	protected Button deleteButton;
	protected Button editButton;

	protected String tabela;
	protected String id;
	protected Class<?> classe;
	protected int typeRemove = TYPE_REMOVE_WITH_SYNC;
	protected Typeface customTypeFace;
	boolean isRemovePermited;
	boolean isEditPermited;
	CustomDataLoader dataLoader = null;
	public EditOrRemoveDialog(
			Activity activity,
			String id, 
			String tabela, 
			Class<?> classe, 
			boolean enableRemove, 
			boolean enableEdit,
			CustomItemList item){
		this.activity= activity;
		this.id = id;
		this.tabela = tabela;
		//class to edit of field
		this.classe = classe;
		this.isRemovePermited = enableRemove;
		this.isEditPermited = enableEdit;
		this.item = item;

	}
	
	public void setTypeRemove(int pTypeRemove){
		typeRemove = pTypeRemove;
	}

	public void showAlertDialog(){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
		alertDialogBuilder.setTitle(activity.getResources().getString(R.string.gerenciar));
		String edit = activity.getResources().getString(R.string.form_edit);
		String delete = activity.getResources().getString(R.string.form_delete);
		// set title
		
		// set dialog message
		alertDialogBuilder
				.setMessage("")
				.setCancelable(true);
		if(isEditPermited)
			alertDialogBuilder.setPositiveButton(edit,
			new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,
						int id) {
					// if this button is clicked, just close
					// the dialog box and do nothing
					dialog.cancel();
					activity.runOnUiThread(new Runnable() {
	  				      public void run() {
	  				    	  procedureEditLoader();
	  				      }
	  				});
				}
			});
		final String tempId = id;
		final CustomItemList tempItem = item;
		final String tempTabela = tabela;
		final Activity tempActivity = activity;
		if(isRemovePermited)
		alertDialogBuilder.setNegativeButton(delete,
				new DialogInterface.OnClickListener() {
			public void onClick(
					DialogInterface dialog,
					int id) {
				dialog.cancel();
//				activity.runOnUiThread(new Runnable() {
//				      public void run() {
    	  String confirmacaoDelecao= activity.getResources().getString(R.string.form_confirmacao_remocao);
    	  DialogInterface.OnClickListener removeOnClickListener = new DialogInterface.OnClickListener() {
	  			public void onClick(DialogInterface dialog,
	  					int id) {
	  				new DeleteLoader(tempActivity, tempTabela, tempId, tempItem).execute();
	  			}
  			};
    	  getAlertConfirmacao(confirmacaoDelecao, removeOnClickListener);
				       }
//				 });
//			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
		
		
	}

	

	public AlertDialog getAlertConfirmacao(String titulo, DialogInterface.OnClickListener okListener){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
		alertDialogBuilder.setTitle(titulo);
		String ok = activity.getResources().getString(R.string.form_ok);
		String cancelar = activity.getResources().getString(R.string.form_cancelar);
		// set title
		

		// set dialog message
		alertDialogBuilder
				.setMessage("")
				.setCancelable(false)
				.setPositiveButton(ok, okListener)
				.setNegativeButton(cancelar,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
		return alertDialog;
	}
	
	

	public void procedureEditLoader(){
		final ProgressDialog dialog = new ProgressDialog(activity);
		dialog.show();
	    new Thread(new Runnable() {

	        public void run() {
	        	Intent intent = new Intent(activity, classe);
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, id);
				activity.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_EDIT);
	            dialog.dismiss();
	        }
	    }).start();
	}


	protected void procedureAlertAfterDeleteTupla(String pId){

	}
	


	public class DeleteLoader extends AsyncTask<String, Integer, Boolean> {
		
		Exception ex2 = null;
		Mensagem msg =null;
		Activity activity;
		String tabela;
		String id;
		CustomItemList item;
		ProgressDialog dialogProgresss =null;
		public DeleteLoader(Activity activity, String tabela, String id, CustomItemList item) {
			this.activity=activity;
			this.tabela=tabela;
			this.id=id ;
			this.item=item;
			this.dialogProgresss= new ProgressDialog(activity);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			dialogProgresss.show();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				msg = Table.procedimentoDelete(activity, tabela, id, item);
			}catch(Exception ex){
				this.ex2=ex;	
			}
			
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
				if(this.ex2!= null)
					FactoryAlertDialog.makeToast(activity, ex2, null);
				else if(msg != null)
					Toast.makeText(activity, msg.mMensagem, Toast.LENGTH_SHORT).show();
			}finally{
				dialogProgresss.dismiss();
			}
		}

	}

}