package app.omegasoftware.pontoeletronico.common;

import android.content.Context;
import app.omegasoftware.pontoeletronico.common.HelperSharedPreference.TIPO;

public class HashSharedPreference {

	
	String nome;
	HelperSharedPreference h;
	Context c;
	
	public HashSharedPreference(Context c, String nome){
		this.h = new HelperSharedPreference(nome);
		this.c = c;
		this.nome = nome;
	}
	
	public void apagarHash(){
		
		h.removerTodasAsChavesComOPrefixo(this.c, TIPO.INT, nome );
	}
	public void put(Integer id, Integer valor){
		
		String chave = getChave(id);
		h.saveValorInt(c, chave, valor);
	}
	public boolean hasChave(Integer id){
		String chave = nome + "_" + id.toString();
		if(h.hasChave(c, chave)){
			return true;
		}
		return false;
	}
	private String getChave(Integer id){
		String chave = nome + "_" + id.toString();
		return chave;
	}
	public Integer get(Integer id){
		String chave = getChave(id);
		int v = h.getValorInt(c, chave, -10000);
		
		if(v == -10000) return null;
		else return v;
	}
}
