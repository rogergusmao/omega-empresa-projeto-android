package app.omegasoftware.pontoeletronico.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import android.content.Context;
import android.content.SharedPreferences;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class HelperSharedPreference {
	
	String preferenceName;
	public HelperSharedPreference(String preferenceName){
		this.preferenceName=  preferenceName;
	}
	public enum TIPO{
		INT,
		BOOLEAN,
		VETOR_INTEGER,
		VETOR_STRING,
		STRING,
		LONG,
		DATETIME
	}
	private String getPreferenceNameDoTipo( TIPO tipo){
		return preferenceName + "_" + tipo.toString();
	}
	public  void saveValorArrayInteger( Context context, String chave, Integer[] pValor){
		
		SharedPreferences.Editor vEditor = getSharedPreferenceEditor(context, TIPO.VETOR_STRING);
		String vToken = "";
		if(pValor != null && pValor.length > 0 ){
			for(int i = 0 ; i < pValor.length; i++){
				String vStrValor = String.valueOf(pValor[i]);
				if(i == 0) vToken +=vStrValor;
				else vToken += "," + vStrValor;
			}
		
			vEditor.putString(chave, vToken);
		} else vEditor.putString(chave, null);
		vEditor.commit();
			
	}
	
	public  void saveValorArrayString( Context context, String chave, String[] pValor){
		
		SharedPreferences.Editor vEditor = getSharedPreferenceEditor(context, TIPO.VETOR_STRING);
		String vToken = "";
		if(pValor != null && pValor.length > 0 ){
			for(int i = 0 ; i < pValor.length; i++){
				String vStrValor = pValor[i];
				if(i == 0) vToken +=vStrValor;
				else vToken += "," + vStrValor;
			}
		
			vEditor.putString(chave, vToken);
		} else vEditor.putString(chave, null);
		vEditor.commit();
			
	}
	
	

	public  String[] getValorArrayString(Context context,  String chave){
		//saveValor(pContext, pTipo, true);
		SharedPreferences vSavedPreferences = getSharedPreference(context, TIPO.VETOR_STRING);
		
		
		String vValor = vSavedPreferences.getString(chave, null);
		if(vValor == null) return null;
		String[] vVetorStr = HelperString.splitWithNoneEmptyToken(vValor, ",");
		
		
		return vVetorStr;
	}
	public  Integer[] getValorArrayInteger(Context context,  String chave){
		//saveValor(pContext, pTipo, true);
		SharedPreferences vSavedPreferences = getSharedPreference(context, TIPO.VETOR_INTEGER);
		Integer[] vVetor;
		
		String vValor = vSavedPreferences.getString(chave, null);
		if(vValor == null) return null;
		String[] vVetorStr = HelperString.splitWithNoneEmptyToken(vValor, ",");
		
		vVetor = new Integer[vVetorStr.length];
		for (int i = 0; i < vVetor.length; i ++) {
			if(vVetorStr[i].compareTo("null") == 0)
				vVetor[i] = null;
			else vVetor[i] = HelperInteger.parserInteger(vVetorStr[i]);
		}
		
		return vVetor;
	}
	
	private SharedPreferences getSharedPreference(Context context, TIPO tipo){
		SharedPreferences s = context.getSharedPreferences(getPreferenceNameDoTipo(tipo), 0);
		return s;
	}

	private SharedPreferences.Editor getSharedPreferenceEditor(Context context, TIPO tipo){
		SharedPreferences s = getSharedPreference(context, tipo);
		
		return s.edit();
	}
	
	public void removerTodasAsChavesComOPrefixo(Context context, TIPO tipo, String prefixo){
		SharedPreferences s= getSharedPreference(context, tipo);
		//ATENCAO: u proibido alterar o hash
		HashMap<String, ?> hash = (HashMap<String, ?>) s.getAll();
		Set<String> chaves = hash.keySet();
		Iterator<String> it = chaves.iterator();
		ArrayList<String> removidas = new ArrayList<String>();
		while(it.hasNext()){
			String chave = it.next();
			if(chave.startsWith(prefixo)){
				removidas.add(chave);
			}
		}
		if(removidas.size() > 0){
			SharedPreferences.Editor e = getSharedPreferenceEditor(context, tipo);
			for(int i = 0 ; i < removidas.size(); i++){
				e.remove(removidas.get(i));
			}	
		}
	}
	
	public  void saveValorBoolean(Context context,  String chave, boolean pValor){
		
		
		SharedPreferences.Editor vEditor = getSharedPreferenceEditor(context, TIPO.BOOLEAN);
		vEditor.putBoolean(chave, pValor);
		vEditor.commit();
			
	}
	

	
	public  boolean hasChave(Context context,  String chave){
		
		SharedPreferences s = getSharedPreference(context, TIPO.BOOLEAN);
		return s.contains(chave);
		
	}
	
	public  boolean getValorBoolean(Context context,  String chave){
		
		SharedPreferences s = getSharedPreference(context, TIPO.BOOLEAN);
		boolean valor = s.getBoolean(chave, false);
		return valor;
	}
	
	public  void saveValorString(Context context,  String chave, String valor){
		
		SharedPreferences.Editor vEditor = getSharedPreferenceEditor(context, TIPO.STRING);
		vEditor.putString(chave, valor);
		vEditor.commit();
	}
	
	public boolean possuiChave(Context c, String chave, TIPO tipo){
		SharedPreferences s = getSharedPreference(c, tipo);
		return s.contains(chave);
	}
	
	public  String getValorString(Context context,  String chave){
		SharedPreferences s = getSharedPreference(context, TIPO.STRING);
		String valor = s.getString(chave, null);
		return valor;
	}
	
	
	public  HelperDate getValorDateTime(Context context,  String chave){
		SharedPreferences s = getSharedPreference(context, TIPO.DATETIME);
		String valor = s.getString(chave, null);
		if(valor == null)return null;
		HelperDate hd = new HelperDate(valor);
		return hd;
	}
	
	public void saveValorDateTime(Context context, String chave, HelperDate hd){
		String strD= hd != null ? hd.getDatetimeSQL() : null;
		SharedPreferences.Editor vEditor = getSharedPreferenceEditor(context, TIPO.DATETIME);
		vEditor.putString(chave, strD);
		vEditor.commit();
	}
	
	public  void saveValorInt(Context context,  String chave, int valor){
		
		SharedPreferences.Editor e = getSharedPreferenceEditor(context, TIPO.INT);
		
		e.putInt(chave, valor);
		e.commit();
	
	}
	
	public  void saveValorLong(Context context,  String chave, long valor){
		
		SharedPreferences.Editor e = getSharedPreferenceEditor(context, TIPO.LONG);
		
		e.putLong(chave, valor);
		e.commit();
	
	}
	
	public  int getValorInt(Context context,  String chave, int defaultValue){
		SharedPreferences s = getSharedPreference(context, TIPO.INT);
		int valor = s.getInt(chave, defaultValue);
		return valor;
			
	}
	
	public  long getValorLong(Context context,  String chave, long defaultValue){
		SharedPreferences s = getSharedPreference(context, TIPO.LONG);
		long valor = s.getLong(chave, defaultValue);
		return valor;
			
	}
}
