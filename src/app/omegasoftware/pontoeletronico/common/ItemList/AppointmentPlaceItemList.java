package app.omegasoftware.pontoeletronico.common.ItemList;

import java.util.ArrayList;
import java.util.Iterator;

import android.app.LauncherActivity.ListItem;
import android.content.Context;
import android.content.Intent;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.PhoneEmailAdapter;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOBairro;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class AppointmentPlaceItemList extends ListItem {

	private int id;
	
	private String address;
	private String neighborhood;
	private String city;
	private String uf;
	private String country;
	private String idNeighborhood;
	private String idCity;
	private String idUf;
	private String idCountry;
	private String cep;
	private ArrayList<String> phones;
	private ArrayList<String> emails;
	private Context context;
	private String number;
	private String complemento;
	private String titulo;


	public String idEntidade;
	public EXTDAOTarefa.TIPO_ENDERECO tipoEndereco;

	public Class<?> classEntidade;
	//contem o nome da pessoa ou da empresa
	public String descricaoEntidade;
	public void setIdEntidade(
			EXTDAOTarefa.TIPO_ENDERECO tipoEndereco
			, String idEntidade
			, Class<?> classEntidade
			, String descricaoEntidade){
		this.idEntidade=idEntidade;
		this.tipoEndereco=tipoEndereco;
		this.classEntidade=classEntidade;
		this.descricaoEntidade = descricaoEntidade;
	}


	public AppointmentPlaceItemList(){
		
	}

	public static AppointmentPlaceItemList factory(
			Database db,
			String address,
			String number,
			String idCity)
	{
		String[] valores = EXTDAOCidade.getNomeCidadeEEstado(db, idCity);

			return new AppointmentPlaceItemList(
			db.getContext(),
			0,
			address,
			number,
			null,
			null,
			valores[0],
			valores[1],
			valores[2],
				null,
					valores[3],
					valores[4],
					valores[5],
				null,
					null,
					null);
	}


	public void putDataInIntent(Intent intent){
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_LOGRADOURO, this.getAddress());
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO, this.getNumber());
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_COMPLEMENTO, this.getComplemento());
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_BAIRRO, this.getIdNeighborhood());
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_CIDADE, this.getIdCity());
	}
	
	public AppointmentPlaceItemList(
			Context context, 
			int id, 
			String address)
	{
		
		this(context, 
				id, 
				address, 
				null, 
				null, 
				null,
				null, 
				null, 
				null, 
				null, 
				null, 
				null); 
	}
	
	public AppointmentPlaceItemList(
			Context context, 
			int id, 
			String address,
			ArrayList<String> phones, 
			String email)
	{
		
		this(context, 
				id, 
				address, 
				null, 
				null, 
				null, 
				null,
				null, 
				null, 
				null, 
				phones, 
				email); 
	}
	
	
	public AppointmentPlaceItemList(
			Context context, 
			int id, 
			String address, 
			String number, 
			String complemento, 
			String neighborhood,
			String city, 
			String uf, 
			String country,
			String pIdNeighborhood,
			String idCity, 
			String idUf, 
			String idCountry,
			String cep, 
			ArrayList<String> phones, 
			String email)
	{
		
		this(context, 
				id, 
				address, 
				number, 
				complemento, 
				neighborhood, 
				city, 
				uf,
				country,
				cep, 
				phones, 
				email);
		
		this.idNeighborhood = pIdNeighborhood;
		this.idCity = idCity;
		this.idUf = idUf; 
		this.idCountry = idCountry;
	}
	

	public AppointmentPlaceItemList(
			Context context, 
			int id, 
			String address, 
			String number, 
			String complemento, 
			String neighborhood, 
			String city, 
			String uf, 
			String country,
			String cep, 
			ArrayList<String> phones, 
			String email)
	{
		this.context = context;
		this.id = id;
		
		if(neighborhood != null && neighborhood.length() > 0 )
				this.neighborhood = neighborhood;

		if(address != null && address.length() > 0 )
			this.address = address;

		if(city  != null && city.length() > 0 )
			this.city = city;

		if(uf != null && uf.length() > 0 )
			this.uf = uf;
		
		if(country  != null && country.length() > 0 )
			this.country = country;
		
		if(cep != null && cep.length() > 0 )
			this.cep = cep;
		
		if(phones != null && phones.size() > 0 ){
			Iterator<String> it = phones.iterator();
			while(it.hasNext()){
				String phone = it.next();
				if(phone == null || phone.length() == 0 )
					it.remove();
			}
			this.phones = phones;
		}
		
		if(email != null && email.length() > 0 ){
			this.emails =  new ArrayList<String>();
			this.emails.add(email);
		}

		if(number != null && number.length() > 0 )
				this.number = number;

		if(complemento != null && complemento.length() > 0 )
				this.complemento = complemento;
	}

	public void setTitulo(String titulo ) { this.titulo = titulo;}
	public String getTitulo(){return titulo;}

	public String getNumber(){
		return this.number;
	}

	public String getComplemento(){
		return this.complemento;
	}

	public String getStrAddress(){
		String result = "";
		if(this.getAddress() != null){
			result += this.getAddress();
		}
		if(this.getNumber() != null){
			if(result.length() > 0 )
				result += ", " + this.getNumber();
			else result += this.getNumber();
		}
		if(this.getNeighborhood() != null){
			if(result.length() > 0 )
				result += ", " + this.getNeighborhood() ;
			else result += this.getNeighborhood() ;
		}

		if(this.getCity() != null){
			if(result.length() > 0 )
				result += " - " + this.getCity();
			else result += this.getCity();
		}
		if(this.getUf() != null){
			if(result.length() > 0 )
				result += " / " + this.getUf();
			else result += this.getUf();
		}
		
		if(this.getCountry() != null){
			if(result.length() > 0 )
				result += " / " + this.getCountry();
			else result += this.getCountry();
		}		
		if(result.length() > 0 ){
			return result ;	
		} else return null;
	}
	
	public String getStrAddressSimplificado(Database db){
		try{
		String result = "";
		if(this.getAddress() != null){
			result += HelperString.ucFirstForEachToken(this.getAddress());
		}
		if(this.getNumber() != null){
			if(result.length() > 0 )
				result += ", " + this.getNumber();
			else result += this.getNumber();
		}
		if(this.getNeighborhood() != null){
			if(result.length() > 0 )
				result += ", " + HelperString.ucFirstForEachToken(this.getNeighborhood()) ;
			else result += HelperString.ucFirstForEachToken(this.getNeighborhood());
		}else if(db != null){
			String vIdBairro = getIdNeighborhood();
			if(vIdBairro != null){
				EXTDAOBairro vObjBairro = new EXTDAOBairro(db);
				
				vObjBairro.setAttrValue(EXTDAOBairro.ID, vIdBairro);
				vObjBairro.select();
				String vNomeBairro = vObjBairro.getStrValueOfAttribute(EXTDAOBairro.NOME);
				if(vNomeBairro != null && vNomeBairro.length() > 0 ){
					if(result.length() > 0 )
						result += ", " + HelperString.ucFirstForEachToken(this.getNeighborhood());
					else result += HelperString.ucFirstForEachToken(this.getNeighborhood()) ;
				}
				
			}
		}
		
		if(result.length() > 0 ){
			return result ;	
		} else return null;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
return null;
		}
	}

	
	public void clear(){
		this.neighborhood = null;
		this.address = null;
		this.city = null;
		
		this.cep = null;
		this.idCity = null;
		this.idCountry = null;
		this.idNeighborhood = null;
		this.idUf = null;
		this.uf = null;
		this.cep = null;
		
		if(this.phones != null)
			this.phones.clear();
		
		if(emails != null)
			this.emails.clear();

		this.number = null;
		this.complemento = null;
	}

	public int getId()
	{
		return id;
	}

	public String getAddress()
	{
		return this.address;
	}
	public String getNeighborhood(){
		return neighborhood;
	}

	public String getIdNeighborhood(){
		return idNeighborhood;
	}
	
	public String getIdCity()
	{
		return this.idCity;
	}
	
	public String getIdCountry()
	{
		return this.idCountry;
	}
	
	public String getIdUf()
	{
		return this.idUf;
	}
	
	public String getUf()
	{
		return this.uf;
	}
	
	public String getCountry()
	{
		return this.country;
	}
	
	public String getCity()
	{
		return this.city;
	}
	public String getCEP()
	{
		if(this.cep == null){
			return null;
		} else if(cep.length() == 0){
			return null;
		}
		if(this.cep.contains("-"))
		{
			return this.cep;
		}

		String cep = "";
		try
		{
			cep = this.cep.substring(0, 5) + "-" + this.cep.substring(5,8);
		}
		catch(Exception e)
		{
			cep = this.cep;
		}

		return cep;
	}
	public String getCEPWithStartingString()
	{
		String cep = this.getCEP();
		if(cep == null)
			return null; 
		else
			return "CEP: " + cep;
	}

	public boolean hasEmail(){
		if(this.emails == null) return false; 
		else if(this.emails.size() > 0 ) return true;
		else return false;
	}
	
	public boolean hasPhone(){
		if(this.phones == null) return false;
		else if(this.phones.size() > 0 ) return true;
		else return false;
	}
	
	public PhoneEmailAdapter getEmailAdapter()
	{
		if(this.emails == null || this.emails.size() == 0) return null;
		else return new PhoneEmailAdapter(this.context, this.emails, PhoneEmailAdapter.EMAIL_TYPE);
	}
	public PhoneEmailAdapter getPhoneAdapter()
	{
		if(this.phones == null || this.phones.size() == 0) return null;
		else return new PhoneEmailAdapter(this.context, this.phones, PhoneEmailAdapter.PHONE_TYPE);
	}	
}

