package app.omegasoftware.pontoeletronico.common.ItemList;

public class BairroItemList extends CustomItemList {
	
	public static final String PAIS = "pais";
	public static final String ESTADO = "estado";
	public static final String CIDADE = "cidade";
	public static final String BAIRRO = "bairro";
	
	public BairroItemList(
			String pId,
			String pBairro,
			String pCidade,
			String pEstado,
			String pPais) {
		super(pId, 
			new ContainerItem[]{
				new ContainerItem(PAIS, pPais),	
			new ContainerItem(ESTADO, pEstado),
			new ContainerItem(CIDADE, pCidade),
			new ContainerItem(BAIRRO, pBairro)});
		
	}
	
}
