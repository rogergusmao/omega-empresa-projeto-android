package app.omegasoftware.pontoeletronico.common.ItemList;

public class CidadeItemList extends CustomItemList {
	
	public static final String PAIS = "pais";
	public static final String ESTADO = "estado";
	public static final String CIDADE = "cidade";
	
	
	
	public CidadeItemList(
			String pId,
			String pNomePais, 
			String pNomeEstado, 
			String pNomeCidade) {
		super(pId, 
			new ContainerItem[]{
				new ContainerItem(PAIS, pNomePais),
			new ContainerItem(ESTADO, pNomeEstado),
			new ContainerItem(CIDADE, pNomeCidade)});
		
	}
	
}
