package app.omegasoftware.pontoeletronico.common.ItemList;

public class CorporacaoItemList extends CustomItemList {
	
	public static final String CORPORACAO = "corporacao";
	boolean isMinhaCorporacao = false;
	public CorporacaoItemList(
			String pId,
			String pCorporacao, 
			boolean pIsMinhasCoporacao) {
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(CORPORACAO, pCorporacao)});
		isMinhaCorporacao = pIsMinhasCoporacao;
		
	}
	public boolean IsMinhaCorporacao(){
		return isMinhaCorporacao;
	}
}
