package app.omegasoftware.pontoeletronico.common.ItemList;

import android.app.LauncherActivity.ListItem;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperLong;

public abstract class CustomItemList extends ListItem {
	
	
	private ContainerItem[] vetorContainerItem;
	private String id;
	public CustomItemList(String pId, ContainerItem pContainerItem){
		vetorContainerItem = new ContainerItem[1];
		vetorContainerItem[0] = pContainerItem;
		id = pId;
	}
	
	
	
	public CustomItemList(String pId, ContainerItem[] pVetorContainerItem){
		vetorContainerItem = pVetorContainerItem;
		id = pId;
	}
	
	public CustomItemList(String pId){
		id = pId;
	}

	public String getId(){
		return id;
	}

	public long getLongId(){
		return HelperLong.parserLong( id);
	}
	
	
	public ContainerItem[] getVetorContainerItem(){
		return vetorContainerItem;
	}
	public void setContainerItem(String nome, String valor){
		ContainerItem c = getContainerItem(nome);
		if(c != null){
			c.vetorConteudo = new String[]{valor};
		}
	}
	public ContainerItem getContainerItem(String p_nome){
		for (ContainerItem containerItem : vetorContainerItem) {
			if(containerItem.isEqual(p_nome)){
				return containerItem;
			}
		}
		return null;
	}
	
	public String getConteudoContainerItem(String p_nome){
		for (ContainerItem containerItem : vetorContainerItem) {
			if(containerItem.isEqual(p_nome)){
				return containerItem.getConteudo();
			}
		}
		return null;
	}
	
	public Long getConteudoContainerItemLong(String p_nome){
		for (ContainerItem containerItem : vetorContainerItem) {
			if(containerItem.isEqual(p_nome)){
				return HelperInteger.parserLong( containerItem.getConteudo());
			}
		}
		return null;
	}
	public String getConteudoContainerItem(String p_nome, String defaultValue){
		for (ContainerItem containerItem : vetorContainerItem) {
			if(containerItem.isEqual(p_nome)){
				return containerItem.getConteudo();
			}
		}
		return defaultValue;
	}
	
	public String getConteudoContainerItem(String p_nome, String defaultValue, boolean lowerCase){
		for (ContainerItem containerItem : vetorContainerItem) {
			if(containerItem.isEqual(p_nome)){
				
				String ret=  containerItem.getConteudo();
				if(ret != null){
					if(lowerCase)
						ret = ret.toLowerCase();
					else ret = ret.toUpperCase();
					return ret;
				}
			}
		}
		return defaultValue;
	}
		
}
