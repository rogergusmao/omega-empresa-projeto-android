package app.omegasoftware.pontoeletronico.common.ItemList;

public class EmpresaItemList extends CustomItemList {
	
	
	public static final String NOME = "nome";
	public static final String TIPO_EMPRESA = "tipo_empresa";
	public static final String EMAIL = "email";
	public static final String TELEFONE = "telefone";
	public static final String CELULAR = "celular";
	
	public EmpresaItemList(
			String pId, 
			String pName, 
			String pTipoEmpresa,
			String pEmail,
			String telefone,
			String celular) {
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(NOME, pName),
			new ContainerItem(TIPO_EMPRESA, pTipoEmpresa),
			new ContainerItem(EMAIL, pEmail),
			new ContainerItem(TELEFONE, telefone),
			new ContainerItem(CELULAR, celular)});
		
	}
	
}
