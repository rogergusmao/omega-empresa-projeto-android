package app.omegasoftware.pontoeletronico.common.ItemList;

import app.omegasoftware.pontoeletronico.date.HelperDate;

public class EmpresaPontoItemList extends CustomItemList {
	
	public enum ATRIBUTOS{
		EMPRESA,
		TOTAL_FUNCIONARIOS_INTERVALO,
		TOTAL_FUNCIONARIO_NO_TURNO,
		TOTAL_FUNCIONARIOS,
		TOTAL_FUNCIONARIO_NAO_COMPARECERAM,
	}
	
	public HelperDate data;
	public EmpresaPontoItemList(
			String pId,
			String empresa,
			HelperDate agora,
			Long totalFuncionariosNoIntervalo,
			Long totalFuncionariosNoTurno,
			Long totalFuncionarios,
			Long totalFuncionarioQueNaoCompareceram) {
		super(pId, 
			new ContainerItem[]{
				new ContainerItem(
						EmpresaPontoItemList.ATRIBUTOS.EMPRESA.toString(), 
						empresa),
			new ContainerItem(
					EmpresaPontoItemList.ATRIBUTOS.TOTAL_FUNCIONARIOS_INTERVALO.toString(), 
					String.valueOf(totalFuncionariosNoIntervalo)),
					
			new ContainerItem(
					EmpresaPontoItemList.ATRIBUTOS.TOTAL_FUNCIONARIO_NO_TURNO.toString(), 
					String.valueOf(totalFuncionariosNoTurno)),
					
			new ContainerItem(
					EmpresaPontoItemList.ATRIBUTOS.TOTAL_FUNCIONARIO_NAO_COMPARECERAM.toString(), 
					String.valueOf(totalFuncionarioQueNaoCompareceram)),
			
			new ContainerItem(
					EmpresaPontoItemList.ATRIBUTOS.TOTAL_FUNCIONARIOS.toString(), 
					String.valueOf(totalFuncionarios)),
					
		});
		this.data = agora;
	}
	
}
