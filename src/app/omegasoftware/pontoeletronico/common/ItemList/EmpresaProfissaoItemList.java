package app.omegasoftware.pontoeletronico.common.ItemList;

import android.app.LauncherActivity.ListItem;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailUsuarioActivityMobile;

public class EmpresaProfissaoItemList extends ListItem {

	private int id;
	private String idUsuario;
	private String idEmpresa;
	private String idProfissao;
	private String idPessoa;
	private String empresa;
	private String profissao;
	private String idPessoaEmpresa;
	private String pessoa;
	public enum ACAO_DETALHES{
		EMPRESA,
		PESSOA,
		USUARIO,
		SEM_ACAO
		
	}
	private ACAO_DETALHES acaoDetalhes; 
	
	
	
	public EmpresaProfissaoItemList(
			int id, 
			String idUsuario,
			String idPessoa,
			String idEmpresa, 
			String idProfissao,
			String idPessoaEmpresa,
			String empresa,
			String profissao,
			String pessoa,
			ACAO_DETALHES acao)
	{
		this.idUsuario = idUsuario;
		this.acaoDetalhes = acao;
		this.id = id;
		this.idPessoa = idPessoa;
		this.idProfissao = idProfissao;
		this.idEmpresa = idEmpresa;
		
		this.empresa = empresa;
		this.profissao = profissao;
		this.idPessoaEmpresa = idPessoaEmpresa;
		this.pessoa = pessoa;
	}
	public String getPessoa(){
		return pessoa;
	}
	public String getProfissao(){
		return this.profissao;
	}
	
	public String getEmpresa(){
		return this.empresa;
	}
	
	public String getIdPessoaEmpresa(){
		return idPessoaEmpresa;
	}
	public String getIdEmpresa(){
		return idEmpresa;
	}
	public String getIdProfissao(){
		return idProfissao;
	}
	public String getIdPessoa(){
		return idPessoa;
	}
	public ACAO_DETALHES getAcaoDetalhes(){
		return acaoDetalhes;
	}
	public int getId(){
		return id;
	}
	public String getIdDaAcaoDetalhes(){
		switch (acaoDetalhes) {
		case USUARIO:
			return idUsuario;
		case EMPRESA:
			return idEmpresa;
			
		case PESSOA:
			return idPessoa;
		case SEM_ACAO:
		default:
			return null;
		}
	}
	public Class<?> getClasseDetalhes(){
		switch (acaoDetalhes) {
		case USUARIO:
			return DetailUsuarioActivityMobile.class;
		case EMPRESA:
			return DetailEmpresaActivityMobile.class;
			
		case PESSOA:
			return DetailPessoaActivityMobile.class;
		case SEM_ACAO:
		default:
			return null;
		}
	}
	
		
}

