package app.omegasoftware.pontoeletronico.common.ItemList;

public class EstadoItemList extends CustomItemList {
	
	public static final String PAIS = "pais";
	public static final String ESTADO = "estado";
	public static final String SIGLA = "sigla";
	
	
	
	public EstadoItemList(
			String pId,
			String pPais, 
			String pEstado, 
			String pSigla) {
		super(pId, 
			new ContainerItem[]{
				new ContainerItem(PAIS, pPais),
			new ContainerItem(ESTADO, pEstado),
			new ContainerItem(SIGLA, pSigla)});
		
	}
	
}
