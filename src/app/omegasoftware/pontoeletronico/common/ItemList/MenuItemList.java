package app.omegasoftware.pontoeletronico.common.ItemList;

import android.view.View.OnClickListener;

public class MenuItemList extends CustomItemList {
	
	
	public static final String DRAWABLE = "drawable";
	public static final String TEXTVIEW = "textview";
	
	OnClickListener onClickListener;
	
	public MenuItemList(
			String pId,
			String pDrawable, 
			String pTextView,
			OnClickListener pOnClickListener) {
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(DRAWABLE, pDrawable),
			new ContainerItem(TEXTVIEW, pTextView)});
		onClickListener = pOnClickListener;
		
	}
	public OnClickListener getClickListener(){
		return onClickListener;
	}
}
