package app.omegasoftware.pontoeletronico.common.ItemList;

public class ModeloItemList extends CustomItemList {
	
	
	public static final String NOME_MODELO = "nome_modelo";
	
	public ModeloItemList(
			String pId,
			String pNomeModelo) {
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(NOME_MODELO, pNomeModelo)});
		
	}
	
}
