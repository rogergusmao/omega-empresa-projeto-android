package app.omegasoftware.pontoeletronico.common.ItemList;

public class MotoristaItemList extends CustomItemList {
	
	
	public enum ATRIBUTOS{
		EMAIL,
		NOME,
		ID_USUARIO
	}
	public MotoristaItemList(
			String pIdVeiculoUsuario,
			String pNome,
			String pEmail,
			String idUsuario) {
		super(pIdVeiculoUsuario, 
			new ContainerItem[]{
			new ContainerItem(ATRIBUTOS.NOME.toString(), pNome),
			new ContainerItem(ATRIBUTOS.EMAIL.toString(), pEmail),
			new ContainerItem(ATRIBUTOS.ID_USUARIO.toString(), idUsuario),
			});
		
	}
	
}
