package app.omegasoftware.pontoeletronico.common.ItemList;

public class PaisItemList extends CustomItemList {
	
	public static final String PAIS = "pais";
	
	public PaisItemList(
			String pId,
			String pPais) {
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(PAIS, pPais)});
		
	}
	
}
