package app.omegasoftware.pontoeletronico.common.ItemList;


public class PessoaEmpresaRotinaItemList extends CustomItemList {

	
	public static final String IDENTIFICADOR = "identificador";
	public static final String ENDERECO_EMPRESA = "endereco_empresa";
	public static final String EMPRESA = "empresa";
	public static final String PROFISSAO = "profissao";
	
	
	public PessoaEmpresaRotinaItemList(
			String pId,
			String pPessoa,
			String pEmpresa,
			String pProfissao){
		super(pId, 
				new ContainerItem[]{
				new ContainerItem(IDENTIFICADOR, pId),
				new ContainerItem(ENDERECO_EMPRESA, pPessoa),
				new ContainerItem(EMPRESA, pEmpresa),
				new ContainerItem(PROFISSAO, pProfissao)});
		
	}
	
}
