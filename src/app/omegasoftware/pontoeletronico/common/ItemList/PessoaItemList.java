package app.omegasoftware.pontoeletronico.common.ItemList;


public class PessoaItemList extends CustomItemList {

	
	public static final String IDENTIFICADOR = "identificador";
	public static final String NOME = "nome";
	public static final String SEXO = "sexo";
	public static final String VETOR_PROFISSAO = "vetor_profissao";
	public static final String VETOR_EMPRESA = "vetor_empresa";
	public static final String TIPO_EMPRESA = "tipo_empresa";
	public static final String EMAIL = "email";
	public static final String TELEFONE = "telefone";
	public static final String CELULAR = "celular";
	public static final String USUARIO = "usuario";
	
	public PessoaItemList(
			String pId,
			String pIdentificador,
			String pNome,
			String pSex,
			String[] pVetorIdProfissao,
			String[] pVetorEmpresa,
			String pTipoEmpresa, 
			String pEmail,
			String pTelefone,
			String pCelular,
			String idUsuario) {
		super(pId, 
				new ContainerItem[]{
				new ContainerItem(IDENTIFICADOR, pIdentificador),
				new ContainerItem(NOME, pNome),
				new ContainerItem(SEXO, pSex),
				new ContainerItem(VETOR_PROFISSAO, pVetorIdProfissao),
				new ContainerItem(VETOR_EMPRESA, pVetorEmpresa),
				new ContainerItem(TIPO_EMPRESA, pTipoEmpresa),
				new ContainerItem(EMAIL, pEmail),
				new ContainerItem(TELEFONE, pTelefone),
				new ContainerItem(CELULAR, pCelular),
				new ContainerItem(USUARIO, idUsuario),
				});
		
	}
	
}
