package app.omegasoftware.pontoeletronico.common.ItemList;

import android.content.Context;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.date.HelperDate;

public class PontoItemList extends CustomItemList {
	
	
	public static final String EMPRESA = "empresa";
	public static final String PROFISSAO = "profissao";
	public static final String PESSOA = "pessoa";
	public static final String DATA = "data";
	public static final String HORA = "hora";
	public static final String TIPO_PONTO = "tipo_ponto";
	
	boolean isSincronizada = false;
	Boolean isEntrada = false;
	
	public PontoItemList(
			Context c,
			String pId,
			boolean pIsSincronizada,
			String pTipoPonto,
			String pEmpresa,
			String pProfissao, 
			String pPessoa,
			Long sec,
			Boolean pIsEntrada,
			Integer offsec) {
		
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(TIPO_PONTO, pTipoPonto),
			new ContainerItem(EMPRESA, pEmpresa),
			new ContainerItem(DATA, HelperDate.getStringDateFromUtcTimestamp(c, sec)),
			new ContainerItem(PROFISSAO, pProfissao),
			new ContainerItem(PESSOA, pPessoa),
			new ContainerItem(HORA, HelperDate.getStringTimeFromUtcTimestamp(c,sec))});
		isEntrada = pIsEntrada;
		isSincronizada = pIsSincronizada;
	}
	
	public boolean isEntrada(){
		return isEntrada;
	}
	
	public String getStrIsEntrada(Context pContext){
		if(isEntrada == null) 
			return null;
		else if (isEntrada)
			return pContext.getString(R.string.entrada);
		else 
			return pContext.getString(R.string.saida);
	}
	
	public boolean isSincronizada(){
		return isSincronizada;
	}
	
}
