package app.omegasoftware.pontoeletronico.common.ItemList;

public class ProfissaoItemList extends CustomItemList {
	
	public static final String PROFISSAO = "profissao";
	
	public ProfissaoItemList(
			String pId,
			String pProfissao) {
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(PROFISSAO, pProfissao)});
		
	}
	
}
