package app.omegasoftware.pontoeletronico.common.ItemList;

import android.app.LauncherActivity.ListItem;
import android.content.Context;

public class RedeEmpresaItemList extends ListItem {

	private int id;
	private Context context;
	private String empresa;
	
	private String nomeEmpresa;
	
	public RedeEmpresaItemList(
			Context context, 
			int id, 
			String pIdEmpresa, 
			String pNomeEmpresa)
	{
		this.context = context;
		this.id = id;
		
		this.empresa = pIdEmpresa;
		
		this.nomeEmpresa = pNomeEmpresa;
		
	}
	
	
	public String getNomeEmpresa(){
		return this.nomeEmpresa;
	}
	public String getEmpresa(){
		return empresa;
	}
	
	public int getId(){
		return id;
	}
		
}

