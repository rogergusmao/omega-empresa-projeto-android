package app.omegasoftware.pontoeletronico.common.ItemList;

public class RedeItemList extends CustomItemList {
	
	public static String REDE = "rede";
	
	public RedeItemList(
			String pId,
			String pRede) {
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(REDE, pRede)});
		
	}
	
}
