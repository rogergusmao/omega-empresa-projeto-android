package app.omegasoftware.pontoeletronico.common.ItemList;

import android.app.LauncherActivity.ListItem;
import android.content.Context;

public class RelatorioAnexoItemList extends ListItem {

	private int id;
	
	private String arquivo;
	
	public RelatorioAnexoItemList(
			Context context, 
			int id, 
			String pFoto)
	{
	
		this.id = id;
		this.arquivo = pFoto;
		
	}
	
	
	public String getAnexo(){
		return this.arquivo;
	}
	
	public int getId(){
		return id;
	}
		
}

