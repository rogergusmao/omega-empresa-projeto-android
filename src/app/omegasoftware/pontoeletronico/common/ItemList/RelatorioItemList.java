package app.omegasoftware.pontoeletronico.common.ItemList;

import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORelatorio;

public class RelatorioItemList extends CustomItemList {
	
	public static final String PESSOA = "pessoa";
	public static final String EMPRESA = "empresa";
	public static final String TITULO = "titulo";
	public static final String DATA = "data";
	public static final String HORA = "hora";
	public static final String USUARIO = "usuario";
	public EXTDAORelatorio.ContainerContem container;
	public RelatorioItemList(
			String pId,
			String pPessoa,
			String pEmpresa,
			String pTitulo,
			String pData,
			String pHora,
			String pUsuario,
			EXTDAORelatorio.ContainerContem pContainer) {
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(PESSOA, pPessoa),
			new ContainerItem(EMPRESA, pEmpresa),
			new ContainerItem(TITULO, pTitulo),
			new ContainerItem(DATA, pData),
			new ContainerItem(HORA, pHora),
			new ContainerItem(USUARIO, pUsuario),});
		container = pContainer;
		
	}
	
	public EXTDAORelatorio.ContainerContem getContainerRelatorio(){
		return container;
	}
	
}
