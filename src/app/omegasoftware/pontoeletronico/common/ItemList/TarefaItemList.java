package app.omegasoftware.pontoeletronico.common.ItemList;


import java.util.Date;

import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;
import app.omegasoftware.pontoeletronico.date.HelperDate;


public class TarefaItemList extends CustomItemList {
	
	public static final String NOME_TAREFA = "nome_tarefa";
	public static final String DATA_ABERTURA_TAREFA = "DATA_ABERTURA_TAREFA";
	public static final String DATETIME_INICIO_MARCADO_DA_TAREFA = "DATETIME_INICIO_MARCADO_DA_TAREFA";
	public static final String DATETIME_INICIO_TAREFA = "DATETIME_INICIO_TAREFA";
	public static final String DATETIME_FIM_TAREFA = "DATETIME_FIM_TAREFA";
	
	public static final String DATETIME_CADASTRO_TAREFA = "data_cadastro_tarefa";
//	public static final String ORIGEM_CIDADE = "origem_cidade";
//	public static final String ORIGEM_BAIRRO = "origem_bairro";
//	public static final String ORIGEM_LOGRADOURO = "origem_logradouro";
//	public static final String ORIGEM_NUMERO = "origem_numero";
//	public static final String ORIGEM_COMPLEMENTO = "origem_complemento";
//	public static final String ORIGEM_CIDADE_ID = "origem_cidade_id";
//	public static final String ORIGEM_BAIRRO_ID = "origem_bairro_id";
//	
//	public static final String DESTINO_CIDADE = "destino_cidade";
//	public static final String DESTINO_BAIRRO = "destino_bairro";
//	public static final String DESTINO_LOGRADOURO = "destino_logradouro";
//	public static final String DESTINO_NUMERO = "destino_numero";
//	public static final String DESTINO_COMPLEMENTO = "destino_complemento";
//	
//	public static final String DESTINO_CIDADE_ID = "destino_cidade_id";
//	public static final String DESTINO_BAIRRO_ID = "destino_bairro_id";
	
	
	private Date datetimePrograma;
	private EXTDAOTarefa.TIPO_TAREFA tipoTarefa;
	private String identificadorTipoTarefa;
	private EXTDAOTarefa.ESTADO_TAREFA estadoTarefa;
	private EXTDAOTarefa.ContainerResponsavel containerResponsavel;
	public String desc1;
	public String desc2;
	
	public TarefaItemList(
			String pId,
			EXTDAOTarefa.ContainerResponsavel pContainerResponsavel,
			EXTDAOTarefa.ESTADO_TAREFA pEstadoTarefa,
			EXTDAOTarefa.TIPO_TAREFA pTipoTarefa,
			String pIdentificadorTipoTarefa,
			String pNomeTarefa, 
			String pDataCadastroTarefa,
			String pDateAberturaTarefa ,
			String pDatetimeInicioMarcadoTarefa,
			String pDatetimeInicioTarefa,
			String pDatetimeFimTarefa,
			String desc1,
			String desc2) {
		
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(NOME_TAREFA, pNomeTarefa),
			new ContainerItem(DATETIME_CADASTRO_TAREFA, pDataCadastroTarefa),
			new ContainerItem(DATA_ABERTURA_TAREFA, pDateAberturaTarefa),
			new ContainerItem(DATETIME_INICIO_MARCADO_DA_TAREFA, pDatetimeInicioMarcadoTarefa),
			new ContainerItem(DATETIME_INICIO_TAREFA, pDatetimeInicioTarefa),
			new ContainerItem(DATETIME_FIM_TAREFA, pDatetimeFimTarefa)});
		
		HelperDate vHelper = new HelperDate(pDatetimeInicioMarcadoTarefa);
		datetimePrograma = vHelper.getDate() ;
		
		containerResponsavel = pContainerResponsavel;
		tipoTarefa = pTipoTarefa;
		estadoTarefa = pEstadoTarefa;
		this.desc1=desc1;
		this.desc2=desc2;
		
	}
	
	public EXTDAOTarefa.ContainerResponsavel getContainerResponsavel(){
		return containerResponsavel;
	}
	
	
	public EXTDAOTarefa.TIPO_TAREFA getTipoTarefa(){
		return tipoTarefa;
	}
	
	public String getIdentificadorTipoTarefa(){
		return identificadorTipoTarefa;
	}
	
	
	public Date getDatetime(){
		return datetimePrograma;
	}

	
	
	public EXTDAOTarefa.ESTADO_TAREFA getEstadoTarefa(){
		return estadoTarefa;
	}


}
