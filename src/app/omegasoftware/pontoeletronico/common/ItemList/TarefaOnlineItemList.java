package app.omegasoftware.pontoeletronico.common.ItemList;




public class TarefaOnlineItemList extends CustomItemList {
	
	public static final String NOME_TAREFA = "nome_tarefa";
	public static final String DATA_ABERTURA_TAREFA = "DATA_ABERTURA_TAREFA";
	public static final String DATETIME_INICIO_MARCADO_DA_TAREFA = "DATETIME_INICIO_MARCADO_DA_TAREFA";
	public static final String DATETIME_INICIO_TAREFA = "DATETIME_INICIO_TAREFA";
	public static final String DATETIME_FIM_TAREFA = "DATETIME_FIM_TAREFA";
	
	public static final String DATETIME_CADASTRO_TAREFA = "data_cadastro_tarefa";
	
	public static final String ORIGEM_ENDERECO = "origem_endereco";
	public static final String ORIGEM_COMPLEMENTO = "origem_complemento";
	
	public static final String DESTINO_ENDERECO = "destino_endereco";
	public static final String DESTINO_COMPLEMENTO = "destino_complemento";
	
	public static final String LATITUDE_PROPRIO = "latitude_proprio";
	public static final String LONGITUDE_PROPRIO = "longitude_proprio";
	
	private Integer veiculoUsuarioId;
	
	
	public TarefaOnlineItemList(
			String pId,
			Integer pVeiculoUsuarioId,
			String pNomeTarefa, 
			String pDataCadastroTarefa,
			String pDateAberturaTarefa ,
			String pDatetimeInicioMarcadoTarefa,
			String pDatetimeInicioTarefa,
			String pDatetimeFimTarefa,
			String pOrigemEndereco,
			String pOrigemComplemento,
			String pDestinoEndereco,
			String pDestinoComplemento,
			String pLatitudeProprio,
			String pLongitudeProprio) {
		
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(NOME_TAREFA, pNomeTarefa),
			new ContainerItem(DATETIME_CADASTRO_TAREFA, pDataCadastroTarefa),
			new ContainerItem(DATA_ABERTURA_TAREFA, pDateAberturaTarefa),
			new ContainerItem(DATETIME_INICIO_MARCADO_DA_TAREFA, pDatetimeInicioMarcadoTarefa),
			new ContainerItem(DATETIME_INICIO_TAREFA, pDatetimeInicioTarefa),
			new ContainerItem(DATETIME_FIM_TAREFA, pDatetimeFimTarefa),
			new ContainerItem(ORIGEM_ENDERECO, pOrigemEndereco),
			new ContainerItem(ORIGEM_COMPLEMENTO, pOrigemComplemento),
			new ContainerItem(DESTINO_ENDERECO, pDestinoEndereco),
			new ContainerItem(DESTINO_COMPLEMENTO, pDestinoComplemento),
			new ContainerItem(LATITUDE_PROPRIO, pLatitudeProprio),
			new ContainerItem(LONGITUDE_PROPRIO, pLongitudeProprio)});
		
		
		veiculoUsuarioId = pVeiculoUsuarioId;
		
	}
	
	
	
	public Integer getVeiculoUsuairo(){
		return veiculoUsuarioId;
	}
	
}
