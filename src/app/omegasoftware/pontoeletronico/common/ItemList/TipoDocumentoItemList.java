package app.omegasoftware.pontoeletronico.common.ItemList;

public class TipoDocumentoItemList extends CustomItemList {
	
	public static final String TIPO_DOCUMENTO = "tipo_documento";
	
	public TipoDocumentoItemList(
			String pId,
			String pTipoDocumento) {
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(TIPO_DOCUMENTO, pTipoDocumento)});
		
	}
	
}
