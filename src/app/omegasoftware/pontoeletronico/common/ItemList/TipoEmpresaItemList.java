package app.omegasoftware.pontoeletronico.common.ItemList;

public class TipoEmpresaItemList extends CustomItemList {
	
	public static final String TIPO_EMPRESA = "tipo_empresa";
	
	public TipoEmpresaItemList(
			String pId,
			String pTipoEmpresa) {
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(TIPO_EMPRESA, pTipoEmpresa)});
		
	}
	
}
