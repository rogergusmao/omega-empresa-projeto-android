package app.omegasoftware.pontoeletronico.common.ItemList;


public class UsuarioItemList extends CustomItemList {
	
	
	public static final String NOME = "nome";
	public static final String EMAIL = "email";
	public static final String TELEFONE = "telefone";
	public static final String CELULAR = "celular";
	
	
	public UsuarioItemList(
			String pId,
			String pNome,
			String pEmail,
			String telefone,
			String celular) {
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(NOME, pNome),
			new ContainerItem(EMAIL, pEmail),
			new ContainerItem(TELEFONE, telefone),
			new ContainerItem(CELULAR, celular),
			});
		
	}
	
}
