package app.omegasoftware.pontoeletronico.common.ItemList;

public class VeiculoItemList extends CustomItemList {
	
	
	public static final String PLACA = "placa";
	public static final String NOME_MODELO = "nome_modelo";
	public static final String ANO_MODELO = "ano_modelo";
	
	
	public VeiculoItemList(
			String pId,
			String pPlaca, 
			String pNomeModelo, 
			String pAnoModelo) {
		super(pId, 
			new ContainerItem[]{
			new ContainerItem(PLACA, pPlaca),
			new ContainerItem(NOME_MODELO, pNomeModelo),
			new ContainerItem(ANO_MODELO, pAnoModelo)});
		
	}
	
}
