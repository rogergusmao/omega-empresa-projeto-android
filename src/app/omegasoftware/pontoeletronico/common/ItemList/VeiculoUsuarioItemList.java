package app.omegasoftware.pontoeletronico.common.ItemList;

public class VeiculoUsuarioItemList extends CustomItemList {
	
	
	public static final String PLACA = "placa";
	public static final String NOME_MODELO = "nome_modelo";
	public static final String ANO_MODELO = "ano_modelo";
	public static final String EMAIL_USUARIO = "email_usuario";
	public static final String ID_USUARIO = "id_usuario";
	
	
	public VeiculoUsuarioItemList(
			String pId,
			String pIdUsuario,
			String pPlaca, 
			String pNomeModelo, 
			String pAnoModelo,
			String pEmailUsuario) {
		super(pId, 
			new ContainerItem[]{
				new ContainerItem(ID_USUARIO, pIdUsuario),
			new ContainerItem(PLACA, pPlaca),
			new ContainerItem(NOME_MODELO, pNomeModelo),
			new ContainerItem(ANO_MODELO, pAnoModelo),
			new ContainerItem(EMAIL_USUARIO, pEmailUsuario)});
		
	}
	
}
