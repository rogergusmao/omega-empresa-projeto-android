package app.omegasoftware.pontoeletronico.common;

import java.util.Comparator;



public class LongListComparator implements Comparator<Object> {
	private boolean isAsc;
	
	public LongListComparator(boolean p_isAsc)
	{
		this.isAsc = p_isAsc;
	}
	
    public int compare(Object o1, Object o2)
    {
        return compare((Long)o1, (Long)o2);
    }
    
    public int compare(Long o1, Long o2)
    {
    	
    	
    	if(o1 != null && o2 != null){
    		
			if(isAsc)
            	if(o1 > o2 ) return 1;
            	else if(o1 == o2) return 0;
            	else return -1;
            else
            	if(o1 < o2 ) return 1;
            	else if(o1 == o2) return 0;
            	else return -1;
    		
    	} 
    	
    	return 0;
    }

}
