package app.omegasoftware.pontoeletronico.common;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.http.HelperHttp;

public class MCrypt {

        static char[] HEX_CHARS = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

        private String iv = "fedcba9876543210";//Dummy iv (CHANGE IT!)
        private IvParameterSpec ivspec;
        private SecretKeySpec keyspec;
        private Cipher cipher;

        private String SecretKey = "0123456789abcdef";//Dummy secretKey (CHANGE IT!)

        public MCrypt()
        {
                ivspec = new IvParameterSpec(iv.getBytes());

                keyspec = new SecretKeySpec(SecretKey.getBytes(), "AES");

                try {
                        cipher = Cipher.getInstance("AES/CBC/NoPadding");
                } catch (NoSuchAlgorithmException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                } catch (NoSuchPaddingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }
        }

        public String encrypt(String text) throws Exception
        {
                if(text == null || text.length() == 0)
                        throw new Exception("Empty string");

                byte[] encrypted = null;

                try {
                        cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);

                        encrypted = cipher.doFinal(padString(text).getBytes());
                } catch (Exception e)
                {                       
                        throw new Exception("[encrypt] " + e.getMessage());
                }
                if(encrypted==null)return null;
                return bytesToHex(encrypted);
        }

        public byte[] decrypt(String code) throws Exception
        {
                if(code == null || code.length() == 0)
                        throw new Exception("Empty string");

                byte[] decrypted = null;

                try {
                        cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

                        decrypted = cipher.doFinal(hexToBytes(code));
                        //Remove trailing zeroes
                        if( decrypted.length > 0)
                        {
                            int trim = 0;
                            for( int i = decrypted.length - 1; i >= 0; i-- ) if( decrypted[i] == 0 ) trim++;

                            if( trim > 0 )
                            {
                                byte[] newArray = new byte[decrypted.length - trim];
                                System.arraycopy(decrypted, 0, newArray, 0, decrypted.length - trim);
                                decrypted = newArray;
                            }
                        }
                } catch (Exception e)
                {
                        throw new Exception("[decrypt] " + e.getMessage());
                }
                return decrypted;
        }      


        public static String bytesToHex(byte[] buf)
        {
            char[] chars = new char[2 * buf.length];
            for (int i = 0; i < buf.length; ++i)
            {
                chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
                chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
            }
            return new String(chars);
        }


        public static byte[] hexToBytes(String str) {
                if (str==null) {
                        return null;
                } else if (str.length() < 2) {
                        return null;
                } else {
                        int len = str.length() / 2;
                        byte[] buffer = new byte[len];
                        for (int i=0; i<len; i++) {
                                buffer[i] = (byte) Integer.parseInt(str.substring(i*2,i*2+2),16);
                        }
                        return buffer;
                }
        }



        private static String padString(String source)
        {
          char paddingChar = 0;
          int size = 16;
          int x = source.length() % size;
          int padLength = size - x;

          for (int i = 0; i < padLength; i++)
          {
                  source += paddingChar;
          }

          return source;
        }
        
        public static String encryptUrl(
	    		Context c, 
	    		String p_url, 
	    		String[] parametros, 
	    		String[] valores){
	    	
        	try{
        		String strGet  = HelperHttp.getStrGet(
    					parametros, 
    					valores, 
    					new String[] {"data" }, 
    					new String[] {String.valueOf( HelperDate.getTimestampSegundosUTC(c))});
    			MCrypt m = new MCrypt();
    			String strCrypt = m.encrypt(strGet);
    			if(strCrypt == null) return null;
    			String url = null;
    			if(p_url.contains("?"))
    				url = p_url+ "&chave=" + URLEncoder.encode( strCrypt);
    			else 
    				url = p_url+ "?chave=" + URLEncoder.encode( strCrypt);
    			return url;
        	}catch(Exception ex){
        		SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
        		return null;
        	}
	    }
        
        public static String getChaveCript(
	    		Context c,
	    		String[] parametros, 
	    		String[] valores){
	    	
        	try{
        		String strGet  = HelperHttp.getStrGet(
    					parametros, 
    					valores, 
    					new String[] {"data" }, 
    					new String[] {String.valueOf( HelperDate.getTimestampSegundosUTC(c))});
    			MCrypt m = new MCrypt();
    			String strCrypt = m.encrypt(strGet);
    			if(strCrypt == null) return null;
    			
    			return URLEncoder.encode( strCrypt);
    			
        	}catch(Exception ex){
        		SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
        		return null;
        	}
	    }
        
        
        final int SIZE_BYTES_BLOCK_CRIPT = 1024;
        public void encryptFile(String text, String pathFileOut) throws Exception
        {
	        if(text == null || text.length() == 0)
	                throw new Exception("Empty string");
	        try {
	        	cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
	        	
	        	ByteArrayInputStream byteStream = null;
	            CipherInputStream in =null;
	            FileOutputStream fos = null;
	            try{
	            	text = padString(text);
	            	byteStream = new ByteArrayInputStream(text.getBytes());
	                in = new CipherInputStream(byteStream, cipher);
	                fos =new FileOutputStream(pathFileOut);
	                
	                
	                int read;
	                
	                while ((read = in.read()) != -1) {
	                	
	                    fos.write((char)read);
	                    fos.flush();
	                }
	            }finally{
	            	if(byteStream != null) byteStream.close();
	            	if(in != null) in.close();
	            	if(fos != null) fos.close();
	            }
	        } catch (Exception e)
	        {                       
	        	throw new Exception("[encrypt] " + e.getMessage());
	        }
	        
        }
        

        public void decryptFile(String pathFile, String pathFileOut) throws Exception
        {

	        try {
	                cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
	                FileInputStream fin = null;
	                
	    	        CipherOutputStream out = null;
	    	        FileOutputStream fos =null;
	    	        int cont=1;
	    	        try{
	    	        	fin = new FileInputStream(pathFile);
	    	        	
	    	        	//new ByteArrayInputStream(text.getBytes())
	    	        	fos=new FileOutputStream( pathFileOut);
	    	            out = new CipherOutputStream(fos, cipher);
		                
		                int read;
		                
		                while ((read = fin.read()) != -1) {
		                	
		                    out.write((char)read);
		                    out.flush();
		                }
	    	            
	    	        }finally{
	    	        	if(out!=null)out.close();
	    	        	
	    	        	if(fin!=null)fin.close();
	    	        	
	    	        }
//	                encrypted = cipher.doFinal(padString(text).getBytes());
	        } catch (Exception e)
	        {                       
	                throw new Exception("[encrypt] " + e.getMessage());
	        }
            
        }
//        public void encryptFile(String text, String pathFileOut) throws Exception
//        {
//	        if(text == null || text.length() == 0)
//	                throw new Exception("Empty string");
//	        try {
//	        	cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
//	        	
//	        	ByteArrayInputStream byteStream = null;
//	            CipherInputStream in =null;
//	            FileOutputStream fos = null;
//	            try{
//	            	text = padString(text);
//	            	byteStream = new ByteArrayInputStream(text.getBytes());
//	                in = new CipherInputStream(byteStream, cipher);
//	                fos =new FileOutputStream(pathFileOut);
//	                
//	                byte[] b = new byte[SIZE_BYTES_BLOCK_CRIPT];
//	                int numberOfBytedRead;
//	                
//	                while ((numberOfBytedRead = in.read(b)) >= 0) {
//	                	
//	                    fos.write(b, 0, numberOfBytedRead);
//	                    fos.flush();
//	                }
//	            }finally{
//	            	if(byteStream != null) byteStream.close();
//	            	if(in != null) in.close();
//	            	if(fos != null) fos.close();
//	            }
//	        } catch (Exception e)
//	        {                       
//	        	throw new Exception("[encrypt] " + e.getMessage());
//	        }
//	        
//        }
//        
//
//        public void decryptFile(String pathFile, String pathFileOut) throws Exception
//        {
//
//	        try {
//	                cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
//	                FileInputStream fin = null;
//	                
//	    	        CipherOutputStream out = null;
//	    	        FileOutputStream fos =null;
//	    	        int cont=1;
//	    	        try{
//	    	        	fin = new FileInputStream(pathFile);
//	    	        	
//	    	        	//new ByteArrayInputStream(text.getBytes())
//	    	        	fos=new FileOutputStream( pathFileOut);
//	    	            out = new CipherOutputStream(fos, cipher);
//		                
//		                byte[] b = new byte[SIZE_BYTES_BLOCK_CRIPT];
//		                int numberOfBytedRead;
//		                while ((numberOfBytedRead = fin.read(b)) >= 0) {
//		                	
//		                    out.write(b, 0, numberOfBytedRead);
//		                    out.flush();
//		                }
//	    	            
//	    	        }finally{
//	    	        	if(out!=null)out.close();
//	    	        	
//	    	        	if(fin!=null)fin.close();
//	    	        	
//	    	        }
////	                encrypted = cipher.doFinal(padString(text).getBytes());
//	        } catch (Exception e)
//	        {                       
//	                throw new Exception("[encrypt] " + e.getMessage());
//	        }
//            
//        }
        public void teste(){
        	try{
        		OmegaFileConfiguration o = new OmegaFileConfiguration();
        		String pathFile= o.getPath(OmegaFileConfiguration.TIPO.LOG) + "TesteCript.txt";
            	String pathFileOut= o.getPath(OmegaFileConfiguration.TIPO.LOG) + "TesteDecript.txt";
            	
            	//FileInputStream fis = new FileInputStream(new File("D:/Shashank/inputVideo.avi"));
            	String text ="a";
            	ByteArrayInputStream fis = new ByteArrayInputStream(text.getBytes());
                File outfile = new File(pathFile);
                int read;
                if(!outfile.exists())
                    outfile.createNewFile();
                File decfile = new File(pathFileOut);
                if(!decfile.exists())
                    decfile.createNewFile();
                FileOutputStream fos = new FileOutputStream(outfile);
                FileInputStream encfis = new FileInputStream(outfile);
                FileOutputStream decfos = new FileOutputStream(decfile);
                Cipher encipher = Cipher.getInstance("AES");
                Cipher decipher = Cipher.getInstance("AES");
                KeyGenerator kgen = KeyGenerator.getInstance("AES");
                //byte key[] = {0x00,0x32,0x22,0x11,0x00,0x00,0x00,0x00,0x00,0x23,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
                SecretKey skey = kgen.generateKey();
                //Lgo
                encipher.init(Cipher.ENCRYPT_MODE, skey);
                CipherInputStream cis = new CipherInputStream(fis, encipher);
                decipher.init(Cipher.DECRYPT_MODE, skey);
                CipherOutputStream cos = new CipherOutputStream(decfos,decipher);
                while((read = cis.read())!=-1)
                        {
                            fos.write((char)read);
                            fos.flush();
                        } 
                cis.close();
                fos.close();
                while((read=encfis.read())!=-1)
                {
                    cos.write(read);
                    cos.flush();
                }
            cos.close(); 
            encfis.close();
        	}catch(Exception ex){
        		SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
        	}
        	
        }
        
        public void bkpteste(){
        	//https://stackoverflow.com/questions/9496447/encryption-of-video-files
        	        	try {
        	            	OmegaFileConfiguration o = new OmegaFileConfiguration();
        	            	String pathFile= o.getPath(OmegaFileConfiguration.TIPO.LOG) + "TesteCript.txt";
        	            	String pathFileOut= o.getPath(OmegaFileConfiguration.TIPO.LOG) + "TesteDecript.txt";
        	            	String codA = encrypt("a");
        					encryptFile("a", pathFile);
        					decryptFile(pathFile, pathFileOut);
        					
        				} catch (Exception e) {
        					SingletonLog.insereErro(e, SingletonLog.TIPO.PAGINA);
        				}
        	        }
}