package app.omegasoftware.pontoeletronico.common;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.http.HelperHttp;

public class MEncryptFile extends FileWriter {

     	String path;
     	private String SecretKey = "0123456789abcdef";//Dummy secretKey (CHANGE IT!)
     	SecretKey skey ;
        public MEncryptFile(String path, boolean append) throws IOException
        {
        	super(path, append);
        	this.path=path;
//        	OmegaFileConfiguration o = new OmegaFileConfiguration();
//    		String pathFile= o.getPath(OmegaFileConfiguration.TIPO.LOG) + "TesteCript.txt";
//        	String pathFileOut= o.getPath(OmegaFileConfiguration.TIPO.LOG) + "TesteDecript.txt";
//        	KeyGenerator kgen;
//			try {
//				kgen = KeyGenerator.getInstance("AES");
//				//byte key[] = {0x00,0x32,0x22,0x11,0x00,0x00,0x00,0x00,0x00,0x23,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
//	            skey = kgen.generateKey();
//			} catch (NoSuchAlgorithmException e) {

//				e.printStackTrace();
//			}
        	skey = new SecretKeySpec(SecretKey.getBytes(), "AES");
        	open();
        }
        Cipher encipher;
        FileInputStream encfis;
        //FileOutputStream fos;
        public boolean open(){
        	try{
        		File outfile = new File(path);
                if(!outfile.exists())
                    outfile.createNewFile();
                
                encfis = new FileInputStream(outfile);
                if(encipher == null){
                	encipher = Cipher.getInstance("AES");
                    //Lgo
                    encipher.init(Cipher.ENCRYPT_MODE, skey);
                }
                
                return true;
        	}catch(Exception ex){
        		SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
        		return false;
        	}
    		    	
        }
        
        public void close(){
        	try{
        		
        		super.close();
                encfis.close();
        	}catch(Exception ex){
        		SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
        	}   	
        }

        public void write(String text) throws IOException{
        	if(encipher == null) return;
        	int read;
        	ByteArrayInputStream fis = new ByteArrayInputStream(text.getBytes());
        	CipherInputStream cis = null;
        	try{
        		cis = new CipherInputStream(fis, encipher);
            	while((read = cis.read())!=-1)
                {
                    super.write((char)read);
                    super.flush();
                }	
        	}finally{
        		if(cis!=null)cis.close();
        	}
        }
        
        
        
        public void teste(){
        	try{
        		OmegaFileConfiguration o = new OmegaFileConfiguration();
        		String pathFile= o.getPath(OmegaFileConfiguration.TIPO.LOG) + "TesteCript.txt";
            	String pathFileOut= o.getPath(OmegaFileConfiguration.TIPO.LOG) + "TesteDecript.txt";
            	
            	//FileInputStream fis = new FileInputStream(new File("D:/Shashank/inputVideo.avi"));
            	String text ="a";
            	ByteArrayInputStream fis = new ByteArrayInputStream(text.getBytes());
                File outfile = new File(pathFile);
                int read;
                if(!outfile.exists())
                    outfile.createNewFile();
                File decfile = new File(pathFileOut);
                if(!decfile.exists())
                    decfile.createNewFile();
                FileOutputStream fos = new FileOutputStream(outfile);
                FileInputStream encfis = new FileInputStream(outfile);
                FileOutputStream decfos = new FileOutputStream(decfile);
                Cipher encipher = Cipher.getInstance("AES");
                Cipher decipher = Cipher.getInstance("AES");
                KeyGenerator kgen = KeyGenerator.getInstance("AES");
                //byte key[] = {0x00,0x32,0x22,0x11,0x00,0x00,0x00,0x00,0x00,0x23,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
                SecretKey skey = kgen.generateKey();
                //Lgo
                encipher.init(Cipher.ENCRYPT_MODE, skey);
                CipherInputStream cis = new CipherInputStream(fis, encipher);
                decipher.init(Cipher.DECRYPT_MODE, skey);
                CipherOutputStream cos = new CipherOutputStream(decfos,decipher);
                while((read = cis.read())!=-1)
                        {
                            fos.write((char)read);
                            fos.flush();
                        } 
                cis.close();
                fos.close();
                while((read=encfis.read())!=-1)
                {
                    cos.write(read);
                    cos.flush();
                }
            cos.close(); 
            encfis.close();
        	}catch(Exception ex){
        		SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
        	}
        	
        }
}