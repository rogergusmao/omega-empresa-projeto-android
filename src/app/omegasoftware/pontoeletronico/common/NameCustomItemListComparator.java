package app.omegasoftware.pontoeletronico.common;

import android.util.Log;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;



public class NameCustomItemListComparator extends CustomItemListComparator {
	public static String TAG= "NameCustomItemListComparator";
	private String nameField;
	public NameCustomItemListComparator(boolean p_isAsc, String pNameField)
	{
		super(p_isAsc, pNameField);
		this.isAsc = p_isAsc;
		this.nameField = pNameField;
	}
	
    @Override
    public int compare(CustomItemList o1, CustomItemList o2)
    {
    	try{
    		if(o1 != null && o2 != null){
		    	CustomItemList custom1 =  ((CustomItemList)o1);
		    	CustomItemList custom2 =  ((CustomItemList)o2);
	    	
		    	if(custom1 != null && custom2 != null){
		    		
		    		String vNomeUm = custom1.getConteudoContainerItem(this.nameField);
		    		String vNomeDois = custom2.getConteudoContainerItem(this.nameField);
		    		
		    		if(vNomeUm != null && vNomeDois != null){
		    			
			    		if(isAsc)
			            	return vNomeUm.compareTo(vNomeDois);
			            else
			            	return -(vNomeUm.compareTo(vNomeDois));
		    		}
		    	}
    		} 
    	}catch (Exception ex){
        	Log.e(TAG, ex.getMessage());
    	}
    	return 0;
    }

}
