package app.omegasoftware.pontoeletronico.common;

import android.content.Context;
import android.content.Intent;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.Table.SEXO;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoOperacaoBanco;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public abstract class Notificacao {

	public static String[] tabelasNotificacao = new String[]{EXTDAOEmpresa.NAME};
	
	protected Context context;

	protected class Cache {

		public String strNovo = "";
		public String strNova = "";
		public String strEditado = "";
		public String strEditada = "";
		public String strRemovido = "";
		public String strRemovida = "";

		protected Cache(Context context) {
			strNovo = context.getResources().getString(R.string.novo);
			strNova = context.getResources().getString(R.string.nova);
			strEditado = context.getResources().getString(R.string.editado);
			strEditada = context.getResources().getString(R.string.editada);
			strRemovido = context.getResources().getString(R.string.removido);
			strRemovida = context.getResources().getString(R.string.removida);
		}
	}

	protected static Cache cache;

	public Notificacao(Context c) {
		this.context = c;
		if (cache == null)
			cache = new Cache(c);
	}

	public abstract void adicionaParametrosAcao(Intent intent);

	public abstract Class<?> getClasseAcao();

	public abstract Table.SEXO getSexo();

	protected String getSufixoTitulo() {
		return context.getResources().getString(getIdStringSufixoTitulo());
	}

	protected abstract int getIdStringSufixoTitulo();

	public String getTitulo(Integer tipoOperacao) {
		StringBuilder titulo = new StringBuilder();
		Table.SEXO sexo = getSexo();

		if (tipoOperacao != null) {
			if (tipoOperacao == EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT
					.getId()) {
				if (sexo == SEXO.FEMININO) {
					titulo.append(cache.strNova);
				} else
					titulo.append(cache.strNovo);

			} else if (tipoOperacao == EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT
					.getId()) {
				if (sexo == SEXO.FEMININO) {
					titulo.append(cache.strEditada);
				} else
					titulo.append(cache.strEditado);
			} else if (tipoOperacao == EXTDAOSistemaTipoOperacaoBanco.TIPO.REMOVE
					.getId()) {
				if (sexo == SEXO.FEMININO) {
					titulo.append(cache.strRemovida);
				} else
					titulo.append(cache.strRemovido);
			}

		}
		
		String sufixo = getSufixoTitulo();
		if (sufixo != null)
			titulo.append(" " + sufixo);

		String retorno = titulo.toString();
		return HelperString.ucFirst(retorno);
	}

	// public abstract String getSufixoTitulo();

	public abstract String getSubtitulo();

}