package app.omegasoftware.pontoeletronico.common;

public class OmegaException extends Exception {
    /**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	
	public OmegaException(String message) {
        super(message);
        
    }
	
	
}

