package app.omegasoftware.pontoeletronico.common;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.phone.HelperPhone;

public class OmegaLog {
	
	
	
	String nomeArquivo;
	HelperDate helperDate ;
	long counter = 0;
	boolean isLogAtivo;
	//EXTERNAL SD CARD
	FileWriter fwExternalSdcard;
	String path;
	boolean cript;
	
	//PRIVATE
	FileOutputStream fosPrivate = null;
	File dirPrivate;
	Context c;
	
	OmegaFileConfiguration.TIPO tipo;
	String strTipo;
	
	//Se o log nao estiver ativo isLogAtivo
	public OmegaLog( 
			String nomeArquivo, 
			OmegaFileConfiguration.TIPO tipo,
			boolean isLogAtivo,
			Context c
	){
		OmegaFileConfiguration ofc = new OmegaFileConfiguration();
		this.path = ofc.getPath(tipo);
		this.tipo=tipo;
		this.strTipo =tipo.toString();
		//this.dirPrivate = ofc.getDirPrivate(tipo, c);
		
		this.nomeArquivo=nomeArquivo;
		helperDate  = new HelperDate();
		this.isLogAtivo=isLogAtivo;
		this.cript=OmegaConfiguration.CRIPT_LOG_DATA;
		this.c=c;
	}

	public String getNomeArquivo(){
		
		return nomeArquivo;
	}
	public void escreveLog(String classe, String funcao, String msg){
		escreveLog(classe + "::" + funcao + " - " + msg) ;
	}
	public void escreveLog(String classe, String funcao){
		escreveLog(classe + "::" + funcao);
	}
	private void openIfNecessary() throws Exception{
		if(fwExternalSdcard == null && fosPrivate == null){
			
			if(OmegaConfiguration.GRAVAR_LOG_MODO_PRIVATE){
				
				fosPrivate = c.openFileOutput( strTipo+ "_" + nomeArquivo , Context.MODE_PRIVATE);
			} 
			if(OmegaConfiguration.GRAVAR_LOG_SSD){
				String pathArquivo= getPathArquivo();
				File f = new File(pathArquivo);
				
				if(!f.exists()) {
					if(!f.createNewFile())
						throw new Exception("Falha ao criar um novo arquivo");
				}
				if(cript)
					fwExternalSdcard = new MEncryptFile(pathArquivo , true);
				else
					fwExternalSdcard = new FileWriter(this.getPathArquivo(), true);	
			}
			
		}
	}
	
	public void escreveLog(String prefixo, Exception ex){
		
		escreveLog(prefixo + " " + HelperExcecao.getDescricao(ex));
	}
	
	public void escreveLog(String msg, int idtipoLog){
		escreveLog(msg, new Date(), idtipoLog);
	}	
	public void escreveLog(String msg){
		escreveLog(msg, new Date(), Log.WARN);
	}
	public void escreveLog(String msg, Date data, int idTipoLog){
		try {
			if(!isLogAtivo || HelperPhone.isFullDisk()){
				return;
			}
			openIfNecessary();
			
			String strData = helperDate.formataDatetimeComMilisegundosSQL(data);
			String strMsg = "[" + strData + "] : " + msg + "\n";
			if(OmegaConfiguration.DEBUGGING_LOGCAT){
				switch(idTipoLog){
					case Log.INFO:
						Log.i("Log", strMsg);
						break;
					case Log.WARN:
						Log.w("Log", strMsg);
						break;
					case Log.ERROR:
						Log.e("Log", strMsg);
						break;
					default:
						Log.v("Log", strMsg);
						break;
				}
			}
			if(fwExternalSdcard != null)
				fwExternalSdcard.write(strMsg);
			
			if(fosPrivate != null)
				fosPrivate.write(strMsg.getBytes());
			
			//O cript ja da o flush para cada char da string
			if(!cript && counter % 10 == 0){
				if(fwExternalSdcard != null)
					fwExternalSdcard.flush();
				if(fosPrivate != null)
					fosPrivate.flush();
					
			}
		} catch(IOException io){
			if(io.getMessage().contains("full disk")){
				HelperPhone.setFullDisk(true);
			}
		}
		catch (Exception e) {
			SingletonLog.insereErro(e, SingletonLog.TIPO.SINCRONIZADOR);
		} finally{
			counter ++;
		}
			
	}
	public String getPathArquivo(){
		return path + nomeArquivo;
	}
	public void delete(){
		this.close();
		File f = new File(getPathArquivo());
		if(f.exists()) f.delete();
	}
	public void close(){
		if(fwExternalSdcard != null ){
			try {
				fwExternalSdcard.close();
				
			} catch (IOException e) {
				SingletonLog.insereErro(e, SingletonLog.TIPO.SINCRONIZADOR);
			}finally {
				fwExternalSdcard = null;
			}
		}
		if(fosPrivate != null ){
			try {
				fosPrivate.close();
				
			} catch (IOException e) {
				SingletonLog.insereErro(e, SingletonLog.TIPO.SINCRONIZADOR);
			}finally {
				fosPrivate = null;
			}
		}
	}
}
