package app.omegasoftware.pontoeletronico.common;

import java.util.Comparator;
import java.util.Date;

import app.omegasoftware.pontoeletronico.common.ItemList.TarefaItemList;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;



public class TarefaCustomItemListComparator implements Comparator<Object>  {
	private boolean isAscData;
	private boolean isAgrupado;
	
	protected String nameField;
	public TarefaCustomItemListComparator(boolean pIsAscData, boolean pIsAgrupado)
	{
		
		this.isAscData = pIsAscData;
		this.isAgrupado = pIsAgrupado;
		this.nameField = TarefaItemList.DATETIME_INICIO_MARCADO_DA_TAREFA;
	}
	
	public void setOrdenacao(boolean pIsAscData, boolean pIsAgrupado){
		isAgrupado = pIsAgrupado;
		isAscData = pIsAscData;
	}

    public int compare(TarefaItemList custom1, TarefaItemList custom2)
    {
    	    	
    	if(custom1 != null && custom2 != null){
    		if(isAgrupado){
    			custom1.getEstadoTarefa();
    			EXTDAOTarefa.ESTADO_TAREFA vEstado1 = custom1.getEstadoTarefa();
    			EXTDAOTarefa.ESTADO_TAREFA vEstado2 = custom2.getEstadoTarefa();
    			if(vEstado1 == vEstado2){
    				Date vDate1 = custom1.getDatetime();
    	    		Date vDate2 = custom2.getDatetime();
    	    		
    	    		if(vDate1 != null && vDate2 != null)	{
    		    		if(isAscData)
    		            	return vDate1.compareTo(vDate2);
    		            else
    		            	return -(vDate1.compareTo(vDate2));
    	    		}
    			} else{
    				int vIdEstado1 = EXTDAOTarefa.getIdEstadoTarefa(vEstado1);
    				int vIdEstado2 = EXTDAOTarefa.getIdEstadoTarefa(vEstado2);
    				
    				if(vIdEstado1 == vIdEstado2) return 0;
    				else if(vIdEstado1 > vIdEstado2) return 1;
    				else if(vIdEstado1 < vIdEstado2) return -1;
    			}
    		}
    		
    	} 
    	
    	return 0;
    }
    public int compare(Object o1, Object o2)
    {
        return compare((TarefaItemList)o1, (TarefaItemList)o2);
    }


}
