package app.omegasoftware.pontoeletronico.common.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;


import android.app.Activity;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import android.os.Handler;
import android.os.Message;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import app.omegasoftware.pontoeletronico.database.Table;

public class TableAutoCompleteTextView {

	LinkedHashMap<String, String> hashIdByDefinition;

	String attributeName = Table.NOME_UNIVERSAL;
	boolean isToUpperCase = true;
	AutoCompleteTextView autoCompleteEditText;
	ArrayAdapter<String> arrayAdapter;
	Table table;
	Activity activity;
	AutoCompleteTextViewHandler handler;
	public void setTable(Table ptable){
		this.table = ptable;
	}
	public TableAutoCompleteTextView(
			Activity pActivity,
			Table pTable,
			AutoCompleteTextView pAutoCompleteEditText){
		activity = pActivity;
		table = pTable;

		autoCompleteEditText = pAutoCompleteEditText;
		updateAdapter(true);
		if(pAutoCompleteEditText != null)
			handler = new AutoCompleteTextViewHandler();
	}
	public TableAutoCompleteTextView(
			Activity pActivity,
			Table pTable,
			AutoCompleteTextView pAutoCompleteEditText,
			boolean updateAdapter){
		activity = pActivity;
		table = pTable;
		
		autoCompleteEditText = pAutoCompleteEditText;
		if(updateAdapter)
			updateAdapter(true);
		if(pAutoCompleteEditText != null)
			handler = new AutoCompleteTextViewHandler();
	}



	public TableAutoCompleteTextView(
			Table pTable, 
			String pStrAttributeName, 
			boolean pIsToUpperCase){
		isToUpperCase = pIsToUpperCase;
		hashIdByDefinition = pTable.getHashMapIdByAttributeValue(pIsToUpperCase, pStrAttributeName);
		attributeName = pStrAttributeName;
		handler = new AutoCompleteTextViewHandler();
	}

	public TableAutoCompleteTextView(
			Table pTable, 
			String pStrAttributeName, 
			boolean pIsToUpperCase,
			boolean inicializaHash){
		isToUpperCase = pIsToUpperCase;
		if(inicializaHash)
		hashIdByDefinition = pTable.getHashMapIdByAttributeValue(pIsToUpperCase, pStrAttributeName);
		attributeName = pStrAttributeName;
		handler = new AutoCompleteTextViewHandler();
	}
	
	public void updateAdapter(){
		updateAdapter(false);
	}

	public AutoCompleteTextView getAutoCompleteEditText(){
		return autoCompleteEditText;
	}
	
	private void updateAdapter(boolean pIsFirstTime){
		hashIdByDefinition = table.getHashMapIdByDefinition(true);
		if(autoCompleteEditText != null){
			arrayAdapter = new ArrayAdapter<String>(
					activity,
					R.layout.spinner_item,
					getVetorStrId());

			autoCompleteEditText.setAdapter(arrayAdapter);
			if(!pIsFirstTime)
				autoCompleteEditText.postInvalidate();
		}
	}


	public String[] getVetorStrId(){
		if(hashIdByDefinition == null) return new String[]{};
		Set<String> vListKey = hashIdByDefinition.keySet();
		ArrayList<String> vListId = new ArrayList<String>(); 
		for (String vKey : vListKey) {
			String vContent = hashIdByDefinition.get(vKey);
			vListId.add(vContent);
		}
		String vVetorId[] = new String[vListId.size()];
		vListId.toArray(vVetorId);
		return vVetorId;
	}



	public String getIdOfTokenIfExist(String pToken){
		if(hashIdByDefinition == null) return null;
		Set<String> vListKey = hashIdByDefinition.keySet();
		for (String vKey : vListKey) {
			String vContent = hashIdByDefinition.get(vKey);
			if(pToken.compareTo(vContent) == 0 ) return vKey;

		}
		return null;
	}



	public String getTokenOfId(String pId){
		if(hashIdByDefinition == null) return null;
		if(hashIdByDefinition.containsKey(pId))
			return hashIdByDefinition.get(pId);
		else return null;
	}


	//Retorna o id correspondente a tupla da tabela
	public String addTupleIfNecessay(Table pTable, String pTokenDefinition){
		try{
		String vId = getIdOfTokenIfExist(pTokenDefinition);
		if(vId == null){
			pTable.clearData();
			pTable.setAttrValue(attributeName, pTokenDefinition);
			Long pId = pTable.insert(true);
			return pId.toString();
		} else{
			pTable.clearData();
			pTable.setAttrValue(Table.ID_UNIVERSAL, vId);
			pTable.select();
			return vId;
		}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
return null;
		}
	}

	//Envia o objeto Table com todos os atributos que compoe a unicidade da tupla
	public String addTupleIfNecessay(Table pTable){
		String vValueAttribute = pTable.getStrValueOfAttribute(attributeName);
		String vId = getIdOfTokenIfExist(vValueAttribute);
		if(vId != null) return vId;
		else {

			pTable.formatToSQLite();
			Long vNewId = pTable.insert(true);

			if(vNewId != null ){
				hashIdByDefinition.put(vNewId.toString(), vValueAttribute);
//				updateAdapter(false);
				if(handler != null)
					handler.sendEmptyMessage(0);
				return vNewId.toString(); 
			}
			else return null;
		} 

	}

	public void refreshData(Table obj){
		table = obj;
		hashIdByDefinition = obj.getHashMapIdByAttributeValue(isToUpperCase, attributeName);

		if(handler != null)
			handler.sendEmptyMessage(0);
	}


	//Nao adiciona a forca a tupla, adiciona se nao existir
		public String addTuple(Table pTable){
			String vValueAttribute = pTable.getStrValueOfAttribute(attributeName);
			String vId = getIdOfTokenIfExist(vValueAttribute);
			
			if(vId != null) return null;
			else {
				pTable.formatToSQLite();
				Long vNewId = pTable.insert(true);
				if(vNewId != null ){
					if(handler != null)
						handler.sendEmptyMessage(0);
					return vNewId.toString(); 
				}
				else return null;
			} 

		}
	
	public class AutoCompleteTextViewHandler extends Handler{
		
		public AutoCompleteTextViewHandler(){
			
		}
		
		@Override
	    public void handleMessage(Message msg)
	    {
			updateAdapter();
	    }
	}
}
