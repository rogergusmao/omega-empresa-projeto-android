package app.omegasoftware.pontoeletronico.common.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;

import app.omegasoftware.pontoeletronico.database.Table;

public class TableDependentAutoCompleteTextView {
	
	LinkedHashMap<String, String> hashIdByDefinition;
	
	String attributeName = Table.NOME_UNIVERSAL;
	String attributeDependentName ;
	
	Table table;
	
	public TableDependentAutoCompleteTextView(
			Table pTable, 
			String pStrAttributeNameDependent){
		
		attributeDependentName = pStrAttributeNameDependent;
		table = pTable;
		
	}
	
	public boolean isEmpty(){
		if(hashIdByDefinition == null) return true;
		else if(hashIdByDefinition.size() == 0 ) return true;
		else return false;
	}
	
	public LinkedHashMap<String, String> getHashIdByDefinition(String pValueAttributeDependent){
		table.setAttrValue(attributeDependentName, pValueAttributeDependent);
		return table.getHashMapIdByAttributeValue(true, attributeName);
	}
	
	public String[] getVetorStrId(String pValueAttributeDependent){
		
		hashIdByDefinition = getHashIdByDefinition(pValueAttributeDependent);
		if(hashIdByDefinition == null) return new String[]{};
		Set<String> vListKey = hashIdByDefinition.keySet();
		ArrayList<String> vListId = new ArrayList<String>(); 
		for (String vKey : vListKey) {
			String vContent = hashIdByDefinition.get(vKey);
			vListId.add(vContent);
		}
		String vVetorId[] = new String[vListId.size()];
		vListId.toArray(vVetorId);
		return vVetorId;
	}
	

	public String getIdOfTokenIfExist(String pToken){
		if(hashIdByDefinition == null){
			
			return null;
		}
		Set<String> vListKey = hashIdByDefinition.keySet();
		for (String vKey : vListKey) {
			String vContent = hashIdByDefinition.get(vKey);
			if(pToken.compareTo(vContent) == 0 ) return vKey;
			
		}
		return null;
	}

	
	public String getTokenOfId(String pId){
		if(hashIdByDefinition == null) return null;
		if(hashIdByDefinition.containsKey(pId))
			return hashIdByDefinition.get(pId);
		else return null;
	}
	
	
	//Envia o objeto Table com todos os atributos que compoe a unicidade da tupla
		public String addTupleIfNecessay(Table pTable){
			String vValueAttribute = pTable.getStrValueOfAttribute(attributeName);
			
			String vId = getIdOfTokenIfExist(vValueAttribute);
			if(vId != null) return vId;
			else {
				pTable.formatToSQLite();
				Long vNewId = pTable.insert(true);
				if(vNewId != null ) return vNewId.toString(); 
				else{
					pTable.setAttrValue(Table.ID_UNIVERSAL, null);
					Table vTupla = pTable.getFirstTupla();
					if(vTupla != null) return vTupla.getStrValueOfAttribute(Table.ID_UNIVERSAL);
					else return null;
				}
			}
		}
		
}
