package app.omegasoftware.pontoeletronico.common.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.database.Table;



public class TableDependentFKAutoCompleteTextView {
	
	LinkedHashMap<String, String> hashIdByDefinition;
	
	String attributeFK = Table.NOME_UNIVERSAL;
	String attributeDependent ;
	String attributeValueOfFK  = Table.NOME_UNIVERSAL;
	Table table;
	
//	Table :: EXTDAOUsuarioCorporacao
//	attributeDependentName :: uc.USUARIO_ID_INT
//	Table desejada :: EXTDAOCorporacao 
//	attributeName :: c.nome
//	id da hash :: pTable.id
//	pTable = usuario_corporacao uc
//	attributeDependent = USUARIO_ID_INT
//	attributeFK = uc.CORPORACAO_ID_INT
//	attributeValueOfFK = c.NOME

	public TableDependentFKAutoCompleteTextView(
			Table pTable,
			String pAttributeDependent,
			String pAttributeFK, 
			String pAttributeValueOfFK){
		table = pTable;
		attributeDependent = pAttributeDependent;
		attributeFK = pAttributeFK;
		attributeValueOfFK = pAttributeValueOfFK;
	}
	
	
	public boolean isEmpty(){
		if(hashIdByDefinition == null) return true;
		else if(hashIdByDefinition.size() == 0 ) return true;
		else return false;
	}
	
	

	
//	Table :: EXTDAOUsuarioCorporacao
//	attributeDependentName :: uc.USUARIO_ID_INT
//	Table desejada :: EXTDAOCorporacao 
//	attributeName :: c.nome
//	id da hash :: pTable.id
//	=========================
//	RETORNO
//	c.id, c.nome
	public LinkedHashMap<String, String> getHashIdByDefinition(String pValueAttributeDependent){
		try{
//		attributeDependent = USUARIO_ID_INT
//		attributeFK = uc.CORPORACAO_ID_INT
//		attributeValueOfFK = c.NOME
		table.clearData();
		table.setAttrValue(attributeDependent, pValueAttributeDependent);
		LinkedHashMap<String, String> vHash = new LinkedHashMap<String, String>(); 
//		List of EXTDAOUsuarioCorporacao
		ArrayList<Table> vList = table.getListTable();
		for (Table tupla : vList) {

			String vName = tupla.getValorDoAtributoDaChaveExtrangeira(attributeFK, attributeValueOfFK);
			String vId = tupla.getStrValueOfAttribute(attributeFK);
			vHash.put(vId, vName);
		}
		return vHash;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
return null;
		}
	}
	
//	Table :: EXTDAOUsuarioCorporacao
//	attributeDependentName :: uc.USUARIO_ID_INT
//	Table desejada :: EXTDAOCorporacao 
//	attributeName :: c.nome
//	id da hash :: pTable.id
	public String[] getVetorStrId(String pValueAttributeDependent){
		
		hashIdByDefinition = getHashIdByDefinition(pValueAttributeDependent);
		if(hashIdByDefinition == null) return new String[]{};
		Set<String> vListKey = hashIdByDefinition.keySet();
		ArrayList<String> vListId = new ArrayList<String>(); 
		for (String vKey : vListKey) {
			String vContent = hashIdByDefinition.get(vKey);
			vListId.add(vContent);
		}
		String vVetorId[] = new String[vListId.size()];
		vListId.toArray(vVetorId);
		return vVetorId;
	}
	

	public String getIdOfTokenIfExist(String pToken){
		if(hashIdByDefinition == null) return null;
		Set<String> vListKey = hashIdByDefinition.keySet();
		for (String vKey : vListKey) {
			String vContent = hashIdByDefinition.get(vKey);
			if(pToken.compareTo(vContent) == 0 ) return vKey;
			
		}
		return null;
	}

	
	public String getTokenOfId(String pId){
		if(hashIdByDefinition == null) return null;
		if(hashIdByDefinition.containsKey(pId))
			return hashIdByDefinition.get(pId);
		else return null;
	}
	
//	//Retorna o id correspondente a tupla da tabela
//	public String addTupleIfNecessay(Table pTable, String pTokenDefinition){
//		String vId = getIdOfTokenIfExist(pTokenDefinition);
//		if(vId == null){
//			pTable.clearData();
//			pTable.setAttrStrValue(attributeName, pTokenDefinition);
//			Long pId = pTable.insert();
//			return pId.toString();
//		} else{
//			pTable.clearData();
//			pTable.setAttrStrValue(Table.ID_UNIVERSAL, vId);
//			pTable.select();
//			return vId;
//		}
//	}
	
	//Envia o objeto Table com todos os atributos que compoe a unicidade da tupla
//	attributeDependent = USUARIO_ID_INT
//	attributeFK = uc.CORPORACAO_ID_INT
//	attributeValueOfFK = c.NOME
//	pTableFK = EXTDAOCorporacao
		public String addTupleIfNecessay(Table pTableFK){
			String vValueAttribute = pTableFK.getStrValueOfAttribute(attributeFK);
			
			String vId = getIdOfTokenIfExist(vValueAttribute);
			if(vId != null) return vId;
			else {
				pTableFK.formatToSQLite();
				Long vNewId = pTableFK.insert(true);
				if(vNewId!= null) return vNewId.toString(); 
				else{
					pTableFK.setAttrValue(Table.ID_UNIVERSAL, null);
					Table vTupla = pTableFK.getFirstTupla();
					if(vTupla != null) return vTupla.getStrValueOfAttribute(Table.ID_UNIVERSAL);
					else return null;
					
				}
			}
		}
}
