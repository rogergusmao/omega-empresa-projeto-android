package app.omegasoftware.pontoeletronico.common.TextWatcher;

import android.widget.EditText;



public class GrupoTextWatcher extends InterfaceMaskTextWatcher{
	//	(31) 3333 - 3333
	//123456789A
	//  0,
	
		public GrupoTextWatcher(EditText pEditText){
			super(pEditText, false, 100);
		}
		public String getValidToken(String pToken){
			
			String vNewToken = "";
			if(pToken != null){
				vNewToken = pToken.toUpperCase().trim();
				vNewToken = vNewToken.replaceAll("[^A-Z0-9]","");
			}
			
			return vNewToken;
		}

}