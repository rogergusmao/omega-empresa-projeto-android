package app.omegasoftware.pontoeletronico.common.TextWatcher;


import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public abstract class InterfaceMaskTextWatcher implements TextWatcher{
	//	(31) 3333 - 3333
	//123456789A
	//  0,

	private enum TYPE  {
		INICIO, 
		FORMATANDO, 
		NOVO_CONTEUDO};

		//posicao considerando q o token vai ser colocado, logo se for no 0, ele sera
		//colocado antes do primeiro digito
		protected int vetorPosicaoInicioToken[] ;
		protected String vetorString[] ;
		String conteudoBeforeModicacao;
		int indexSelection;
		protected TYPE type = TYPE.INICIO ;
		protected String novoConteudo;
		protected int maxSize ;
		protected boolean isJustNumber = true;
		private EditText editText;
		public InterfaceMaskTextWatcher(
				EditText pEditText,
				int pVetorPosicaoInicioToken[],
				String pVetorString[], 
				int pMaxSize, 
				boolean pIsJustNumber){
			vetorPosicaoInicioToken = pVetorPosicaoInicioToken;
			vetorString = pVetorString;
			maxSize = pMaxSize;
			isJustNumber = pIsJustNumber;
			editText = pEditText;
			editText.addTextChangedListener(this);
		}

		public InterfaceMaskTextWatcher(EditText pEditText, boolean pIsJustNumber){
			vetorPosicaoInicioToken = null;
			vetorString = null;
			maxSize = -1;
			isJustNumber = pIsJustNumber;
			editText = pEditText;
			editText.addTextChangedListener(this);
		}
		
		public InterfaceMaskTextWatcher(EditText pEditText, boolean pIsJustNumber, int pMaxSize){
			vetorPosicaoInicioToken = null;
			vetorString = null;
			maxSize = pMaxSize;
			isJustNumber = pIsJustNumber;
			editText = pEditText;
			editText.addTextChangedListener(this);
		}

		public String getTokenDaPosicao(int pPosicaoDesejada){
			int i = 0 ;
			if(this.vetorPosicaoInicioToken == null) return null;
			for (int vPosicao : this.vetorPosicaoInicioToken) {
				String vTokenDemarcada = vetorString[i];

				int vPosicaoFinal = vPosicao + vTokenDemarcada.length();

				if(pPosicaoDesejada >= vPosicao &&  
						pPosicaoDesejada <= vPosicaoFinal) return vTokenDemarcada;

				i += 1;
			}
			return null;

		}


		public abstract String getValidToken(String pToken);

		public String appendMask(String pToken){
			if(this.vetorPosicaoInicioToken == null && !isJustNumber) return pToken;
			int i = -1 ;
			String vNewToken = "";
			for (char vLetra : pToken.toCharArray()) {
				i += 1;
				String vToken = this.getTokenDaPosicao(i);
				//se a posicao eh correspondente a uma mascara,
				//entao eh inserida no conteudo
				if(vToken != null){
					vNewToken += vToken;
					i += vToken.length();
					vNewToken += vLetra;
				} else if(isNumero(vLetra) && isJustNumber){
					vNewToken += vLetra;
				} else vNewToken += vLetra; 
			}
			return vNewToken;
		}

		public boolean isNumero(char vUltimaLetra){
			if(vUltimaLetra >= 48 && 
					vUltimaLetra <= 57 ){
				return true;
			} else return false;
		}

		public boolean isLetraMaiuscula(char vUltimaLetra){
			if(vUltimaLetra >= 65 && 
					vUltimaLetra <= 90 ){
				return true;
			} else return false;
		}

		public boolean isLetraMinuscula(char vUltimaLetra){
			if(vUltimaLetra >= 97 && 
					vUltimaLetra <= 122 ){
				return true;
			} else return false;
		}

		public boolean isLetra(char vUltimaLetra){
			if(vUltimaLetra >= 65 && 
					vUltimaLetra <= 90 ){
				return true;
			} else if(vUltimaLetra >= 97 && 
					vUltimaLetra <= 122 ){
				return true;
			} else return false;
		}
		

		public void afterTextChanged(Editable s) {
			
			while(true){
				if(type == TYPE.INICIO ){
					conteudoBeforeModicacao = s.toString();
					String vConteudoValido = getValidToken(conteudoBeforeModicacao);
					String vConteudoComMascara = appendMask(vConteudoValido);
					novoConteudo = vConteudoComMascara;
					type = TYPE.FORMATANDO;
					
					s.clear();
					break;

				}  else if(type == TYPE.FORMATANDO ){
					type = TYPE.NOVO_CONTEUDO;
					String vBackupNovoConteudo = novoConteudo ;
					novoConteudo = "";
					
					if(vBackupNovoConteudo == null || vBackupNovoConteudo.length() == 0){ 
						type = TYPE.INICIO;
						continue;
					}
					else {
						s.append(vBackupNovoConteudo);
						break;
					}
					
					
				} else if(type == TYPE.NOVO_CONTEUDO){
					String vBackupNovoConteudo = s.toString();
					if(conteudoBeforeModicacao.length() > vBackupNovoConteudo.length()){
						indexSelection -= conteudoBeforeModicacao.length() - vBackupNovoConteudo.length();
					} else if(conteudoBeforeModicacao.length() < vBackupNovoConteudo.length()){
						indexSelection += conteudoBeforeModicacao.length() - vBackupNovoConteudo.length();
					}
					conteudoBeforeModicacao = "";
					editText.setSelection(indexSelection);
					type = TYPE.INICIO;
					break;
				}
			}
		}
		
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			
		}

		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if(type == TYPE.INICIO ){
				indexSelection = start;
				if (count > 0 ){
					indexSelection += count;
				}
					
			} else if(type == TYPE.FORMATANDO && conteudoBeforeModicacao.length() == 0 ){
				indexSelection = start;
				if (count > 0 ){
					indexSelection += count;
				}
			}
		}


}