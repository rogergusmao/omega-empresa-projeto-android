package app.omegasoftware.pontoeletronico.common;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

public class ViewFocusHandler extends Handler{
	
	
	View v;
	Activity a;
	public ViewFocusHandler(Activity a, View v){
		final Activity x = a;
		this.a = a;
		this.v = v;
		this.v.setOnFocusChangeListener(new View.OnFocusChangeListener() {
		    @Override
		    public void onFocusChange(View v, boolean hasFocus) {
		        if (hasFocus) {
		            x.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		        }
		    }
		});			
	}
	
	@Override
    public void handleMessage(Message msg)
    {
		this.v.requestFocus();
		
		InputMethodManager imm = (InputMethodManager) this.a.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
    }
}