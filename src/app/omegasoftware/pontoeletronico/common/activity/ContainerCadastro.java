package app.omegasoftware.pontoeletronico.common.activity;

import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity.TIPO_CONTAINER_CADASTRO;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;

public class ContainerCadastro{
		
		
		public TIPO_CONTAINER_CADASTRO tipo;
		public Integer dialogId;
		public Boolean result;
		public boolean flutuante = false;
		public boolean finalizar = false;
		public String msg;
		public Mensagem mensagem;
		public ContainerCadastro(boolean result){
			this.result = result;
		}
		public ContainerCadastro(Mensagem mensagem, boolean result){
			this.mensagem=mensagem;
		}
		public ContainerCadastro(Integer dialogId, boolean result, String mensagem){
			this.result = result;
			this.dialogId = dialogId;
			this.tipo = TIPO_CONTAINER_CADASTRO.DIALOG;
			this.msg=mensagem;
		}
		public ContainerCadastro(Integer dialogId, boolean result){
			this.result = result;
			this.dialogId = dialogId;
			this.tipo = TIPO_CONTAINER_CADASTRO.DIALOG;
		}
		public ContainerCadastro(Integer dialogId, boolean result, boolean flutuante, boolean finalizar){
			this.result = result;
			this.dialogId = dialogId;
			this.tipo = TIPO_CONTAINER_CADASTRO.DIALOG;
			this.flutuante = flutuante;
			this.finalizar = finalizar;
		}
		public ContainerCadastro(String msg){
			this.msg = msg;
			this.tipo = TIPO_CONTAINER_CADASTRO.ACTIVITY_DIALOG;
		}
		public ContainerCadastro(){
			
		}
		public void setMensagem(Mensagem mensagem){
			this.mensagem = mensagem;
		}
		public Mensagem getMensagem(){
			return mensagem;
		}
	}