package app.omegasoftware.pontoeletronico.common.activity;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOOperacaoMonitoraView;

public class HelperActivity {
	public void getListaAtividadesExecutadas(Context c){
		ActivityManager am = (ActivityManager) c.getSystemService(Activity.ACTIVITY_SERVICE);
	  List<RunningTaskInfo> taskInfo = am.getRunningTasks(5);
	  ComponentName componentInfo = taskInfo.get(0).topActivity;
	  String valor = taskInfo.get(0).topActivity.getClassName()+"   Package Name :  "+componentInfo.getPackageName();
	  Log.d(BOOperacaoMonitoraView.TAG, "CURRENT Activity ::" + valor);
	}
	
	public static RunningTaskInfo getTopActivityRunningTaskInfo(Context c, String packageName){
		try{
			ActivityManager am = (ActivityManager) c.getSystemService(Activity.ACTIVITY_SERVICE);
			  List<RunningTaskInfo> taskInfo = am.getRunningTasks(5);
			  
			  for(int i = 0 ; i < taskInfo.size(); i++){
				  ComponentName componentInfo = taskInfo.get(i).topActivity;
				  if(componentInfo.getPackageName().compareTo(packageName) == 0){
					  return taskInfo.get(i);
				  }
			  }	  
			  return null;	
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
			return null;
		}
		
	}
	
	public static Activity getTopActivity(Application application){
		try {
		RunningTaskInfo taskTop = getTopActivityRunningTaskInfo(application.getApplicationContext(), application.getPackageName());
		
		if(taskTop == null) return null;
		ComponentName cn = taskTop.topActivity;
		String idTopActivity = cn.getClassName();
		  Object obj = null;
		  Field f;

		  
		   f = Application.class.getDeclaredField("mLoadedApk");
		   f.setAccessible(true);
		   obj = f.get(application); // obj => LoadedApk
		   f = obj.getClass().getDeclaredField("mActivityThread");
		   f.setAccessible(true);
		   obj= f.get(obj); // obj => ActivityThread
		   f = obj.getClass().getDeclaredField("mActivities");
		   f.setAccessible(true);
		   @SuppressWarnings("rawtypes")
		HashMap map= (HashMap) f.get(obj); //  obj => HashMap=<IBinder, ActivityClientRecord>
		   if (map.values().size() == 0) {
		    return null;
		   }
		   Object[] vetor = map.values().toArray();
		   for(int i = 0 ; i < vetor.length; i++){
			   obj = vetor[i];
			   f = obj.getClass().getDeclaredField("activity");
			   f.setAccessible(true);
			   
			   obj= f.get(obj); // obj => Activity
			   if (obj instanceof Activity) {
				   Activity temp = (Activity) obj;
			     String idTemp = temp.getClass().getName();
			     if(idTopActivity.compareTo(idTemp) == 0){
			    	 return temp;
			     }
			   }
		   }
		   return null;
		  }
		  catch (SecurityException e) {
			  SingletonLog.insereWarning(e, TIPO.BIBLIOTECA_NUVEM);
		   
		  }
		  catch (NoSuchFieldException e) {
			  SingletonLog.insereWarning(e, TIPO.BIBLIOTECA_NUVEM);
		   
		  }
		  catch (IllegalArgumentException e) {
			  SingletonLog.insereWarning(e, TIPO.BIBLIOTECA_NUVEM);
		  }
		  catch (IllegalAccessException e) {
			  SingletonLog.insereWarning(e, TIPO.BIBLIOTECA_NUVEM);
		  } catch(Exception e){
			  SingletonLog.insereWarning(e, TIPO.BIBLIOTECA_NUVEM);
		  }
		  return null;
	}
}
