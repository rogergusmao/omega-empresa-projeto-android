package app.omegasoftware.pontoeletronico.common.activity;

import java.io.InputStream;
import java.io.OutputStream;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;

public class HelperFonte {
	public static final String BOLD_FONT_PATH = "fonts/Montserrat-Bold.ttf";
	public static final String REGULAR_FONT_PATH = "fonts/Montserrat-Regular.ttf";
	public static final String ITALIC_FONT_PATH = "fonts/DroidSerif-Italic.ttf";
	public static Typeface regularFont, boldFont, italicFont;

	public static void loadFonts(Context c) {
		if(regularFont==null){
			regularFont = Typeface.createFromAsset(c.getAssets(),
					HelperFonte.REGULAR_FONT_PATH);
			boldFont = Typeface.createFromAsset(c.getAssets(),
					HelperFonte.BOLD_FONT_PATH);
			italicFont = Typeface.createFromAsset(c.getAssets(),
					HelperFonte.ITALIC_FONT_PATH);	
		}
		
	}

	public static void setFontAllView(View v){

			
		ViewGroup vg = (ViewGroup) v;
		HelperFonte.setFontAllView(vg);
	
	}
	
	@SuppressLint("DefaultLocale")
	public static void setFontAllView(ViewGroup vg) {
		if(vg == null) return;
		for (int i = 0; i < vg.getChildCount(); ++i) {
			View child = vg.getChildAt(i);

			if (child instanceof ViewGroup) {

				setFontAllView((ViewGroup) child);

			} else if (child != null) {
				setFontView(child);
			}
			
		}
	}
	private static Typeface getTypeface(View child){
		if(child.getTag() != null){
			String tag = child.getTag().toString().toLowerCase();
			return getTypeface(child, tag);
		} else return regularFont;
	}
	private static Typeface getTypeface(View child, String tipo){

		Typeface face;
		if(tipo != null){
			String tag = tipo.toLowerCase();
			if(tag != null){
				if ( tag.equals("bold")) {
					face = boldFont;
				}else if (tag.equals("italic")) {
					face = italicFont;
				}  else {
					face = regularFont;
				}
			}	else face = regularFont;
		} else face = regularFont;
		return face;
		
	}
	
	public static void setFontView(View child){
		Typeface face = null;
		if (child instanceof TextView) {
			TextView textView = (TextView) child;
			int idStyle = -1000;
			if(textView.getTypeface()!=null){
				idStyle = textView.getTypeface().getStyle();
			}
			if(idStyle  == Typeface.BOLD){
				face = boldFont;
			} else if(idStyle == Typeface.ITALIC){
				face = italicFont;
			} else {
				face = regularFont;
			}  
		} else {
			face = getTypeface(child);
		}
	
		if (child instanceof TextView) {
			TextView textView = (TextView) child;
			
			textView.setTypeface(face);
		} else if (child instanceof EditText) {
			EditText editView = (EditText) child;
			editView.setTypeface(face);
		} else if (child instanceof RadioButton) {
			RadioButton radioView = (RadioButton) child;
			radioView.setTypeface(face);
		} else if (child instanceof CheckBox) {
			CheckBox checkboxView = (CheckBox) child;
			checkboxView.setTypeface(face);
		}

	

	}
	
	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
	}
	
	public static Bitmap GetImageFromAssets(Context context,String imagePath){
		Bitmap bmp = null;
		try {
	        InputStream bitmap= context.getAssets().open(imagePath);
	        bmp = BitmapFactory.decodeStream(bitmap);

	    } catch (Exception e1) {
	    	SingletonLog.insereErro(e1, TIPO.PAGINA);
	    }
		return bmp;
	}
}
