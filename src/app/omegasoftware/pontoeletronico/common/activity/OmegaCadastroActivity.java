package app.omegasoftware.pontoeletronico.common.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.AlertActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.InformationDialog;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.common.dialog.HelperDialog;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;

public abstract class OmegaCadastroActivity extends OmegaRegularActivity {

	private String TAG = "OmegaCadastroActivity";

	public final int FORM_ERROR_DIALOG_CAMPO_INVALIDO = 72;
	public final int FORM_ERROR_DIALOG_REGISTRO_DUPLICADO = 74;

	public final int FORM_ERROR_DIALOG_MISSING_NUMERO_DOCUMENTO = 880;
	public final int FORM_ERROR_DIALOG_MISSING_TIPO_DOCUMENTO = 881;
	public final int FORM_ERROR_DIALOG_MISSING_EMPRESA_DA_CORPORACAO = 882;
	public final int FORM_ERROR_DIALOG_MISSING_CHECK_BOX_NAO_EH_EMPRESA_DA_CORPORACAO = 883;
	public final int ERROR_DIALOG_MISSING_EMAIL = 884;

	public final int FORM_ERROR_DIALOG_INSERCAO = 885;
	public final int FORM_ERROR_DIALOG_EDICAO = 1002;
	public final int FORM_ERROR_DIALOG_MISSING_TITULO = 886;
	public final int FORM_ERROR_DIALOG_MISSING_EMPRESA = 102344;
	public final int FORM_ERROR_DIALOG_MISSING_PESSOA = 102345;

	public final int ERROR_DIALOG_PESSOA_DUPLICADA = 789;
	public final int ERROR_DIALOG_USUARIO_DUPLICADO = 78445;

	protected final int FORM_ERROR_DIALOG_MISSING_PROFISSAO = 88123453;

	private enum TIPO {
		EDICAO, CADASTRO
	}

	TIPO tipo;

	private boolean isTipoDocumentoDialogExibido = false;
	protected String id = null;

	// Search button
	protected Button cadastrarButton;
	protected Button removerButton;

	protected abstract void beforeInitializeComponents() throws OmegaDatabaseException;

	protected abstract void loadEdit() throws OmegaDatabaseException;

	@Override
	public void finish() {
		if (foiModificado) {
			Intent intent = getIntent();
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, true);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, id);
			setResult(RESULT_OK, intent);

		}

		super.finish();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle vParameter = getIntent().getExtras();
		if (vParameter != null) {

			if (vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID)) {
				id = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID);
				tipo = TIPO.EDICAO;
			} else
				tipo = TIPO.CADASTRO;
			if (vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_TAG)) {
				nomeTag = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_TAG);
			}

		}

	}

	public enum TIPO_CONTAINER_CADASTRO {
		DIALOG, ACTIVITY_DIALOG
	}

	public ContainerCadastro factoryContainerCadastro(Integer dialogId, boolean result) {
		return new ContainerCadastro(dialogId, result);
	}

	private class CadastroButtonListener implements OnClickListener {
		public void onClick(View v) {
			onCadastroButtonClicked();
		}
	}

	private void onCadastroButtonClicked() {
		new CadastroButtonLoader().execute();
	}

	public class CadastroButtonLoader extends AsyncTask<String, Integer, Boolean> {

		ContainerCadastro container = null;
Exception e=null;
		public CadastroButtonLoader() {
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				ContainerCadastro c = cadastrando();
				container = c;

			} catch (Exception ex) {
				this.e=ex;
				container = new ContainerCadastro(HelperDialog.DIALOG_ERRO_FATAL, false);
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			
			try{
				try {
					
					if(SingletonLog.openDialogError(OmegaCadastroActivity.this, e, SingletonLog.TIPO.PAGINA))
						return;
					
					if (container == null) {
						return;
					}
					if (container.getMensagem() != null) {

						FactoryAlertDialog.showDialog(OmegaCadastroActivity.this, container.getMensagem());
						return;
					}

					if (container.tipo == TIPO_CONTAINER_CADASTRO.ACTIVITY_DIALOG) {
						Intent intent = new Intent(OmegaCadastroActivity.this, AlertActivityMobile.class);
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_MESSAGE, container.msg);
						startActivity(intent);
					} // if(container.tipo == TIPO_CONTAINER_CADASTRO.DIALOG){
					else {

						if (container.dialogId == null) {

							// tenta inicializar o container para abertura do dialog
							// de final de acao de cadastro
							// com algum dialog default
							if (container.result == true && tipo == TIPO.EDICAO) {
								container = new ContainerCadastro(HelperDialog.FORM_DIALOG_ATUALIZACAO_OK, true);
							}
							if (container.result == true && tipo == TIPO.CADASTRO) {
								container = new ContainerCadastro(HelperDialog.DIALOG_CADASTRO_OK, true);
							} else if (container.dialogId == null && container.result == false) {
								container = new ContainerCadastro(HelperDialog.DIALOG_ERRO_FATAL, false);
							}
						}
						if (container.dialogId != null) {
							if (container.dialogId == HelperDialog.FORM_EXCEDEU_PACOTE) {
								InterfaceMensagem msg = container.getMensagem();
								String strMsg = "";
								if(msg != null){
									strMsg = msg.mMensagem;
								} else {
									strMsg = container.msg;
								}
								HelperDialog.factoryExcedeuPacote(OmegaCadastroActivity.this,
										strMsg);
								return;
							} else
								showDialog(container.dialogId);
						} else {
							SingletonLog.insereErro(new Exception("Condicional nao programada[AFWE]"),
									SingletonLog.TIPO.UTIL_ANDROID);
						}

					}

				} finally {
					if (container != null) {
						if (container.result != null && container.result == true)
							setBancoFoiModificado();

						if (container.finalizar)
							finish();
					}
				}	
			}catch(Exception ex){
				SingletonLog.openDialogError(OmegaCadastroActivity.this, ex, SingletonLog.TIPO.PAGINA);
			}finally{
				controlerProgressDialog.dismissProgressDialog();
			}
			

		}
	}

	protected abstract ContainerCadastro cadastrando() throws Exception;

	@Override
	public void initializeComponents() throws OmegaDatabaseException {
		beforeInitializeComponents();

		// Attach search button to its listener
		View viewCadastrarButton = findViewById(R.id.cadastrar_button);
		if (viewCadastrarButton != null) {
			this.cadastrarButton = (Button) viewCadastrarButton;
			this.cadastrarButton.setOnClickListener(new CadastroButtonListener());
		}

		View viewTopo = findViewById(R.id.header);

		if (viewTopo != null) {
			View viewBtTopo = viewTopo.findViewById(R.id.cadastrar_topo_button);
			if (viewBtTopo != null) {
				ImageButton cadastrarTopoButton = (ImageButton) viewBtTopo;
				cadastrarTopoButton.setOnClickListener(new CadastroButtonListener());
				// if(id != null){
				// cadastrarTopoButton.setImageResource(R.drawable.sa);
				// }
			}

		}

		if (id != null) {
			String vSave = getResources().getString(R.string.form_save);
			if (this.cadastrarButton != null)
				this.cadastrarButton.setText(vSave);
			loadEdit();
		}

	}

	public void inibirBotaoSalvarToolbar() {
		View viewTopo = findViewById(R.id.header);

		if (viewTopo != null) {
			View viewBtTopo = viewTopo.findViewById(R.id.cadastrar_topo_button);
			if (viewBtTopo != null) {
				ImageButton cadastrarTopoButton = (ImageButton) viewBtTopo;
				if (cadastrarTopoButton != null)
					cadastrarTopoButton.setVisibility(View.GONE);
				// if(id != null){
				// cadastrarTopoButton.setImageResource(R.drawable.sa);
				// }
			}
		}
	}

	@Override
	public Dialog getDialog(int dialogType) {

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);
		InformationDialog vInformation = null;
		switch (dialogType) {

		case OmegaConfiguration.INFORMACAO_PESSOA_DUPLICIDADE:
			vDialog = new Dialog(this);
			vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			vDialog.setContentView(R.layout.dialog);

			vInformation = new InformationDialog(this, vDialog, TAG + "_dialog_informacao_duplicidade",
					OmegaConfiguration.INFORMACAO_PESSOA_DUPLICIDADE);
			return vInformation.getDialogInformation(
					new int[] { R.string.informacao_uso_list_detalhes, R.string.informacao_email_cadastro_pessoa });

		case OmegaConfiguration.FORM_DIALOG_INFORMACAO_DIALOG_ENDERECO:

			if (id != null) {
				vInformation = new InformationDialog(this, vDialog, TAG + "_dialog_informacao_cadastro_endereco",
						OmegaConfiguration.FORM_DIALOG_INFORMACAO_DIALOG_ENDERECO);
				return vInformation
						.getDialogInformation(new int[] { R.string.informacao_uso_form_gerenciamento_endereco });
			} else {
				vInformation = new InformationDialog(this, vDialog, TAG + "_dialog_informacao_edicao_endereco",
						OmegaConfiguration.FORM_DIALOG_INFORMACAO_DIALOG_ENDERECO);
				return vInformation
						.getDialogInformation(new int[] { R.string.informacao_uso_form_gerenciamento_endereco });
			}

		case ERROR_DIALOG_USUARIO_DUPLICADO:
			vTextView.setText(getResources().getString(R.string.usuario_duplicado));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(ERROR_DIALOG_USUARIO_DUPLICADO);
				}
			});
			break;
		case ERROR_DIALOG_PESSOA_DUPLICADA:
			vTextView.setText(getResources().getString(R.string.pessoa_duplicada));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(ERROR_DIALOG_PESSOA_DUPLICADA);
				}
			});
			break;
		case FORM_ERROR_DIALOG_EDICAO:
			vTextView.setText(getResources().getString(R.string.error_edicao));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_EDICAO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_EMPRESA:
			vTextView.setText(getResources().getString(R.string.error_missing_empresa));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_EMPRESA);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_PROFISSAO:
			vTextView.setText(getResources().getString(R.string.error_missing_nome_profissao));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_PROFISSAO);
				}
			});
			break;

		case FORM_ERROR_DIALOG_INSERCAO:
			vTextView.setText(getResources().getString(R.string.error_insercao));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_INSERCAO);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_TIPO_DOCUMENTO:
			if (!isTipoDocumentoDialogExibido) {
				isTipoDocumentoDialogExibido = true;
				vTextView.setText(getResources().getString(R.string.error_missing_tipo_documento));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_MISSING_TIPO_DOCUMENTO);
					}
				});
			} else {
				// Se o usuario persistir no erro
				isTipoDocumentoDialogExibido = false;
				return null;
			}

			break;

		case FORM_ERROR_DIALOG_MISSING_TITULO:
			vTextView.setText(getResources().getString(R.string.error_dialog_missing_relatorio_foto_titulo));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_TITULO);
				}
			});
			break;
		case ERROR_DIALOG_MISSING_EMAIL:
			vTextView.setText(getResources().getString(R.string.error_missing_email));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(ERROR_DIALOG_MISSING_EMAIL);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_CHECK_BOX_NAO_EH_EMPRESA_DA_CORPORACAO:
			vTextView.setText(getResources().getString(R.string.error_dialog_missing_empresa_nao_eh_da_corporacao));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_CHECK_BOX_NAO_EH_EMPRESA_DA_CORPORACAO);
				}
			});

			break;
		case FORM_ERROR_DIALOG_MISSING_EMPRESA_DA_CORPORACAO:
			vTextView.setText(getResources().getString(R.string.error_dialog_missing_empresa_da_corporacao));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_EMPRESA_DA_CORPORACAO);
				}
			});

			break;

		case FORM_ERROR_DIALOG_MISSING_NUMERO_DOCUMENTO:
			vTextView.setText(getResources().getString(R.string.error_missing_numero_documento));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_NUMERO_DOCUMENTO);
				}
			});

			break;

		case FORM_ERROR_DIALOG_REGISTRO_DUPLICADO:
			vTextView.setText(getResources().getString(R.string.error_duplicado));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_REGISTRO_DUPLICADO);
				}
			});
			break;

		case FORM_ERROR_DIALOG_CAMPO_INVALIDO:
			vTextView.setText(getResources().getString(R.string.error_invalid_field));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_CAMPO_INVALIDO);
				}
			});
			break;
		default:
			return HelperDialog.getDialog(this, dialogType);

		}

		return vDialog;
	}

	@Override
	protected Dialog onCreateDialog(int dialogType) {
		Dialog d = null;
		d = getDialog(dialogType);
		return d;
	}

	@Override
	public void procedureBeforeLoadData() {
		// Se for cadastro tenta sincronizar, do contrario, na edicao so ocorre
		// sincronizacao no
		// FilterAcitivity
		// if(TABELAS_RELACIONADAS != null && TABELAS_RELACIONADAS.length > 0)
		// if((typeSync == TYPE_SYNC.INITIAL || typeSync ==
		// TYPE_SYNC.INITIAL_AND_END) && id ==null)
		// SynchronizeFastReceiveDatabase.procedureRun(this,
		// TABELAS_RELACIONADAS, false);
	}

	@Override
	public void onBackPressed() {
		// pede a confirmacao para continuar a volta
		// Intent intent = new Intent( this,
		// FormConfirmacaoActivityMobile.class);
		// intent.putExtra(OmegaConfiguration.SEARCH_FIELD_MESSAGE,
		// getResources().getString(R.string.confirmacao_sair_cadastro));
		// startActivityForResult(intent,
		// OmegaConfiguration.STATE_FORM_CONFIRMACAO_SAIR_CADASTRO );

		DialogInterface.OnClickListener dialogConfirmarSaida = new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:

					finish();
					break;

				case DialogInterface.BUTTON_NEGATIVE:

					break;
				}
			}
		};
		// if(id == null ){
		// FactoryAlertDialog.showAlertDialog(
		// this,
		// getString(R.string.confirmacao_sair_cadastro),
		// dialogConfirmarSaida);
		// } else {
		// FactoryAlertDialog.showAlertDialog(
		// this,
		// getString(R.string.confirmacao_sair_edicao),
		// dialogConfirmarSaida);
		// }

		finish();

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if (requestCode == OmegaConfiguration.STATE_FORM_CONFIRMACAO_SAIR_CADASTRO) {
			if (resultCode == RESULT_OK) {
				Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO, false);
				if (vConfirmacao) {
					if (foiModificado) {
						Intent intent2 = getIntent();
						intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, true);
						intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, id);
						setResult(RESULT_OK, intent2);
					}
					finish();
				}
			}
		} else if (requestCode == OmegaConfiguration.ACTIVITY_FORM_EDIT
				|| requestCode == OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW) {

			try {
				if (resultCode == RESULT_OK) {
					boolean vIsModificated = false;
					Bundle pParameter = intent.getExtras();

					if (pParameter != null) {
						if (pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED))
							vIsModificated = pParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED);
					}
					if (onAtualizaView != null) {
						onAtualizaView.onAtualizaView(pParameter);
					}
					if (vIsModificated) {
						foiModificado = true;
						new CustomDataLoader(OmegaCadastroActivity.this).execute();
					}
				}

			} catch (Exception ex) {
				SingletonLog.insereErro(ex, SingletonLog.TIPO.FORMULARIO);
			}
		} else {
			super.onActivityResult(requestCode, resultCode, intent);
		}
	}



}
