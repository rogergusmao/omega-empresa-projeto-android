package app.omegasoftware.pontoeletronico.common.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.CustomAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.TecladoAdapter;
import app.omegasoftware.pontoeletronico.common.controler.ControlerProgressDialog;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiverBroadcastServiceConnectionServerInformation;

public abstract class OmegaCustomAdapterListActivity extends ListActivity {

	protected String TAG = "OmegaCustomAdapterListActivity";
	ImageButton btnVoltarDesktop;
	//ll_titulo_secundario
	
	//tv_titulo_secundario
	TextView tvTituloSecundario;
	protected CustomAdapter adapter;
	ControlerProgressDialog controlerProgressDialog;
	String nomeTag = null;
	ReceiverBroadcastServiceConnectionServerInformation receiverConnectionServerInformation;
	
	private Button fecharMenuFiltroButton;
	private EditText filtrarEditText;
	private LinearLayout filtrarLinearLayout;
	private LinearLayout menuFiltroLinearLayout;
	protected HandlerFiltroRapido handlerFiltroRapido = new HandlerFiltroRapido();
	protected HandlerDiscador handlerDiscador = new HandlerDiscador();
	private LinearLayout menuInferiorLinearLayout;

	private LinearLayout menuDiscadorLinearLayout;

	private LinearLayout discadorLinearLayout;
	private EditText discadorEditText;
	private View tecladoView;
	
	boolean filtroDiscadorAtivo = true;
	View noResultView;
	ListView listView;
	protected abstract CustomAdapter buildAdapter();
	protected abstract void loadCommomParameters(Bundle pParameter);
	public void hideSoftKeyboard() {
	    if(getCurrentFocus()!=null) {
	        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
	        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
	    }
	}

	/**
	 * Shows the soft keyboard
	 */
	public void showSoftKeyboard(View view) {
	    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
	    view.requestFocus();
	    inputMethodManager.showSoftInput(view, 0);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);		
		controlerProgressDialog = new ControlerProgressDialog(this);
		Intent intent = getIntent();
		if(intent != null && intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_FILTRO_DISCADOR_ATIVO)){
			filtroDiscadorAtivo = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_FILTRO_DISCADOR_ATIVO, true);
		}
	}


	protected void loadParameters()	
	{
		Bundle vParameters = getIntent().getExtras();
		if(vParameters != null)
		{
			loadCommomParameters(vParameters);			
		}
		
	}

	public void setHandler(
			HandlerFiltroRapido handlerFiltro,
			HandlerDiscador handlerDiscador){
		this.handlerDiscador = handlerDiscador;
		this.handlerFiltroRapido = handlerFiltro;
	}
	
	protected void applyTypeFace(View v)
	{
		
		ViewGroup vg = (ViewGroup) v;
		HelperFonte.setFontAllView(vg);
	}
	
	public void setTag(String p_tag)
	{
		this.TAG = p_tag;
	}
	boolean foiModificado = false;
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if(requestCode == OmegaConfiguration.ACTIVITY_FORM_EDIT
				|| requestCode == OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW){

			try{
				if(resultCode == RESULT_OK){
					boolean vIsModificated = true;
					Bundle pParameter = intent.getExtras();

					if(pParameter != null)
						if(pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED))
							vIsModificated = pParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED);
					if(vIsModificated){
						foiModificado = true;
						new CustomDataLoader(OmegaCustomAdapterListActivity.this).execute();
					}
				}

			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.LISTA);	
			}
		} 
	}

	@Override
	public void onBackPressed(){

    	if(foiModificado){
			Intent intent = getIntent();
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, true);
			setResult(RESULT_OK, intent);
			finish();
		} else {
			Intent intent = getIntent();
			setResult(RESULT_CANCELED, intent);
			finish();
		}
	
		
	}
	
	
	protected Dialog CreateAlertDialog(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message)
		.setCancelable(false)
		.setNeutralButton(R.string.botao_voltar, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				finish();
			}
		});

		return builder.create();
	}

	@Override
	protected Dialog onCreateDialog(int id) {


		switch (id) {
		case OmegaConfiguration.ON_LOAD_DIALOG:
			return controlerProgressDialog.getDialog();
			
		case OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG:
			Dialog vDialog = new Dialog(this);
			vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			
			vDialog.setContentView(R.layout.dialog);

			TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
			Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

			vTextView.setText(getResources().getString(R.string.dialog_noresult_alert));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
					finish();
				}
			});
			return vDialog;
		default:
			return null;
		}

		

	}

	public boolean loadData() throws Exception {

		receiverConnectionServerInformation = new ReceiverBroadcastServiceConnectionServerInformation(
				OmegaCustomAdapterListActivity.this, 
				nomeTag);
		
		btnVoltarDesktop = (ImageButton) findViewById(R.id.btn_voltar);
		btnVoltarDesktop.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onBackPressed();
				
			}
		});
		tvTituloSecundario = (TextView) findViewById(R.id.tv_title);
		
		loadParameters();
		adapter = buildAdapter();
		if (adapter != null) {
			adapter.setHandler(handlerFiltroRapido, handlerDiscador);
		}
		adapter.loadData(false);
		
		
		filtrarEditText = (EditText) findViewById(R.id.et_filtrar);
		if(filtrarEditText != null)
			filtrarEditText.addTextChangedListener(new TextWatcher() {
	
				@Override
				public void onTextChanged(CharSequence cs, int arg1, int arg2,
						int arg3) {
	
					adapter.getFilter().filter(cs);
				}
	
				@Override
				public void beforeTextChanged(CharSequence arg0, int arg1,
						int arg2, int arg3) {
				}
	
				@Override
				public void afterTextChanged(Editable arg0) {
				}
			});
		
		
		if(adapter!= null && adapter.getSize() > 0){
			
			return true;
		} else return false;
			
	}

	@Override
	public void finish(){
		
		if(receiverConnectionServerInformation != null)
			receiverConnectionServerInformation.unregisterReceiverIfExist();
		super.finish();
		
	}
	protected void beforePostExecute(){
		
		
		
	}
	public void setTituloCabecalho(String nomeUsuario){
		View viewTitle = findViewById(R.id.tv_title);
		TextView tvTitle = (TextView)viewTitle;
		tvTitle.setText(nomeUsuario);	
	}
	protected void initializeComponents(){
		Intent intent = getIntent();
		if(intent != null && intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_TITULO)){
			int idStringTitulo = intent.getIntExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, -1);
			if(idStringTitulo != -1){
				String titulo = getResources().getString(idStringTitulo);
				setTituloCabecalho(titulo);	
			}
			
		}
		listView = getListView();
		noResultView = findViewById(R.id.ll_no_result_view);
		menuInferiorLinearLayout = (LinearLayout) findViewById(R.id.ll_menu_inferior);
		View filtrar = findViewById(R.id.ll_filtrar);
		if (filtrar != null) {
			filtrarLinearLayout = (LinearLayout) filtrar;
			filtrarLinearLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					handlerFiltroRapido.sendEmptyMessage(0);
				}
			});
			fecharMenuFiltroButton = (Button) findViewById(R.id.bt_fechar_menu_filtro);
			fecharMenuFiltroButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					handlerFiltroRapido.sendEmptyMessage(1);
				}
			});

			menuFiltroLinearLayout = (LinearLayout) findViewById(R.id.ll_menu_filtro);

		}
		
		

		View vDiscador = findViewById(R.id.ll_discador);
		if (vDiscador != null) {
			discadorLinearLayout = (LinearLayout) vDiscador;
			if(!filtroDiscadorAtivo)
				discadorLinearLayout.setVisibility(View.GONE);
			discadorEditText = (EditText) findViewById(R.id.et_discador);
			tecladoView = (View) findViewById(R.id.ll_menu_discador);
			TecladoAdapter tecladoAdapter = new TecladoAdapter(adapter);
			tecladoAdapter.formatarView(tecladoView);

			menuInferiorLinearLayout = (LinearLayout) findViewById(R.id.ll_menu_inferior);

			menuDiscadorLinearLayout = (LinearLayout) findViewById(R.id.ll_menu_discador);
			if (getIntent().hasExtra(OmegaConfiguration.DESABILITA_DISCADOR)) {
				discadorLinearLayout
						.setVisibility(View.GONE);
			} else {
				discadorLinearLayout.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						handlerDiscador.sendEmptyMessage(0);
					}
				});
			}

		}
	}

	public class HandlerDiscador extends Handler {
		public boolean ativo = false;

		public HandlerDiscador() {

		}

		@Override
		public void handleMessage(Message msg) {

			if (menuDiscadorLinearLayout != null) {
				
				// filtro inativo
				if (menuDiscadorLinearLayout.getVisibility() == View.GONE) {
					ativo = true;
					menuDiscadorLinearLayout
							.setVisibility(View.VISIBLE);
					menuInferiorLinearLayout
							.setVisibility(View.GONE);
					discadorEditText.requestFocus();
					OmegaCustomAdapterListActivity.this.showSoftKeyboard(discadorEditText);	
				} else {
					ativo = false;
					discadorEditText.setText("");
					discadorEditText.clearFocus();
					menuDiscadorLinearLayout
							.setVisibility(View.GONE);
					menuInferiorLinearLayout
							.setVisibility(View.VISIBLE);

				}
			}
		}
	}
	public class HandlerFiltroRapido extends Handler {
		public boolean ativo = false;

		public HandlerFiltroRapido() {

		}

		@Override
		public void handleMessage(Message msg) {
			if (menuFiltroLinearLayout != null) {
				// filtro inativo
				if (menuFiltroLinearLayout.getVisibility() == View.GONE) {
					ativo = true;
					menuInferiorLinearLayout
							.setVisibility(View.GONE);
					menuFiltroLinearLayout
							.setVisibility(View.VISIBLE);
					filtrarEditText.requestFocus();
					OmegaCustomAdapterListActivity.this.showSoftKeyboard(filtrarEditText);
				} else {

					ativo = false;
					menuFiltroLinearLayout
							.setVisibility(View.GONE);
					menuInferiorLinearLayout
							.setVisibility(View.VISIBLE);
					filtrarEditText.setText("");
					filtrarEditText.clearFocus();
				}
			}

		}
	}

	
	public class CustomDataLoader extends AsyncTask<String, Integer, Boolean>
	{

		Activity activity;
		Exception ex;
		 
		public CustomDataLoader(Activity pActivity){
			activity = pActivity;
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				return loadData();
			}
			catch(Exception e)
			{
				
				this.ex = e;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
				
				if(SingletonLog.openDialogError(activity, ex, SingletonLog.TIPO.PAGINA))
					return;
			
				initializeComponents();
				
				beforePostExecute();
				if(result)
				{
					setListAdapter(adapter);
				}
				if(adapter== null || adapter.getCount() == 0)
				{
					if(noResultView == null){
						showDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
					}
					else{
						noResultView.setVisibility(View.VISIBLE);
						listView.setVisibility(View.GONE);
					}
				}
				
			}catch(Exception ex){
				SingletonLog.openDialogError(activity, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}
}
