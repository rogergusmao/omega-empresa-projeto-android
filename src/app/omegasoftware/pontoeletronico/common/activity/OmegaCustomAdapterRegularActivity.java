package app.omegasoftware.pontoeletronico.common.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.AboutActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Adapter.BkpCustomAdapter;
import app.omegasoftware.pontoeletronico.common.controler.ControlerProgressDialog;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.pontoeletronico.PrincipalActivity;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiverBroadcastServiceConnectionServerInformation;

public abstract class OmegaCustomAdapterRegularActivity extends Activity {

	//private String TAG = "OmegaCustomAdapterRegularActivity";
	ControlerProgressDialog controlerProgressDialog;
	protected BkpCustomAdapter adapter;
	private GridView resultGridView;

	protected abstract void buildAdapter() throws OmegaDatabaseException;

	public abstract void initializeComponents();

	protected abstract void loadCommomParameters(Bundle pParameter);

	CustomDataLoader loader;
	protected String nomeTag = null;
	ReceiverBroadcastServiceConnectionServerInformation receiverConnectionServerInformation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_TAG))
			nomeTag = getIntent().getStringExtra(OmegaConfiguration.SEARCH_FIELD_TAG);
		setContentView(R.layout.factory_menu_activity_mobile_layout);

		resultGridView = (GridView) findViewById(R.id.search_result_tablet_gridview);
		controlerProgressDialog = new ControlerProgressDialog(this);
		controlerProgressDialog.createProgressDialog();

		inicializaAsync();
	}

	public void inicializaAsync() {
		if (loader == null) {
			loader = new CustomDataLoader(this);
			loader.execute();
		}
	}

	protected void loadParameters() {
		Bundle vParameters = getIntent().getExtras();
		if (vParameters != null) {
			loadCommomParameters(vParameters);
		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mobile_menu, menu);
		return true;

	}

	@Override
	protected Dialog onCreateDialog(int id) {

		
		try {
			switch (id) {
			case OmegaConfiguration.ON_LOAD_DIALOG:

				return controlerProgressDialog.getDialog();

			case OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG:
				Dialog vDialog = new Dialog(this);
				vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

				vDialog.setContentView(R.layout.dialog);

				TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
				Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

				vTextView.setText(getResources().getString(R.string.no_result_usuario_permissao_categoria_permissao));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
						finish();
					}
				});
				return vDialog;

			default:
				// vDialog = this.getErrorDialog(id);
				return null;
			}
		} catch (Exception ex) {
			return null;
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent intent = null;

		if (PrincipalActivity.isTablet) {
			switch (item.getItemId()) {

			// case R.id.menu_sincronizador_dado:
			// intent = new Intent(this,
			// SynchronizeReceiveActivityMobile.class);
			// break;
			case R.id.menu_sobre:
				intent = new Intent(this, AboutActivityMobile.class);
				break;

			default:
				return super.onOptionsItemSelected(item);

			}
		}
		if (intent != null) {
			startActivity(intent);
		}
		return true;

	}

	public boolean loadData() throws OmegaDatabaseException {

		loadParameters();
		buildAdapter();
		if (adapter.getCount() == 0)
			adapter = null;
		return (adapter != null ? true : false);

	}

	@Override
	protected void onDestroy() {

		if (receiverConnectionServerInformation != null) {

			receiverConnectionServerInformation.unregisterReceiverIfExist();
			receiverConnectionServerInformation = null;
		}
		super.onDestroy();
	}

	// @Override
	// protected void onResume() {
	//
	//
	// super.onResume();
	// }
	@Override
	public void finish() {

		if (receiverConnectionServerInformation != null) {
			receiverConnectionServerInformation.unregisterReceiverIfExist();
			receiverConnectionServerInformation = null;
		}
		super.finish();

	}

	public class CustomDataLoader extends AsyncTask<String, Integer, Boolean> {
		Activity activity;
		Context context;

		public CustomDataLoader(Activity pActivity) {
			activity = pActivity;
			context = pActivity.getApplicationContext();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		
		Exception __ex2 = null;

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				return loadData();
			}catch (Exception e) {
				__ex2 = e;
			}

			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				if (SingletonLog.openDialogError(activity, __ex2, SingletonLog.TIPO.PAGINA)) {
					return;
				}
				
				try {
					initializeComponents();

					if (receiverConnectionServerInformation == null)
						receiverConnectionServerInformation = new ReceiverBroadcastServiceConnectionServerInformation(
								activity, nomeTag);

					if (!(adapter == null || adapter.getCount() == 0)) {
						inicializaGridView();
					}

				} catch (Exception ex) {
					SingletonLog.openDialogError(activity, ex, SingletonLog.TIPO.PAGINA);
				}

				if (adapter == null || adapter.getCount() == 0) {

					showDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
				}
			} catch (Exception ex) {
				SingletonLog.openDialogError(activity, ex, SingletonLog.TIPO.PAGINA);
				
				
			} finally {
				controlerProgressDialog.dismissProgressDialog();
			}

		}

	}

	public void inicializaGridView() {

		try {
			if (adapter != null && adapter.getCount() > 0)
				resultGridView.setAdapter(adapter);

		} catch (Exception ex) {
			SingletonLog.openDialogError(this, ex, SingletonLog.TIPO.PAGINA);
		}
	}

}
