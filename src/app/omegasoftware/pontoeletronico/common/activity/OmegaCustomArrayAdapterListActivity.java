package app.omegasoftware.pontoeletronico.common.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.AboutActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Adapter.BkpCustomAdapter;
import app.omegasoftware.pontoeletronico.common.controler.ControlerProgressDialog;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.pontoeletronico.PrincipalActivity;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiverBroadcastServiceConnectionServerInformation;

public abstract class OmegaCustomArrayAdapterListActivity extends ListActivity {

	private String TAG = "OmegaCustomAdapterListActivity";

	protected BkpCustomAdapter adapter;
	ControlerProgressDialog controlerProgressDialog;
	String nomeTag = null;
	ReceiverBroadcastServiceConnectionServerInformation receiverConnectionServerInformation;

	protected abstract void buildAdapter(boolean pIsAsc);

	protected abstract void loadCommomParameters(Bundle pParameter);

	private Typeface customTypeFace;
	private ToggleButton azOrderByButton;
	private ToggleButton zaOrderByButton;
	public View listView;
	private short currentOrderByMode = OmegaConfiguration.DATA_ORDER_BY_MODE;
	View noResultView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.list_activity_mobile_layout);
		controlerProgressDialog = new ControlerProgressDialog(this);

		new CustomDataLoader(this).execute();

	}

	protected void loadParameters() {
		Bundle vParameters = getIntent().getExtras();
		if (vParameters != null) {
			loadCommomParameters(vParameters);
		}
	}

	public void setTag(String p_tag) {
		this.TAG = p_tag;
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mobile_menu, menu);
		return true;

	}

	public class AZOrderByTypeChangeListener implements OnCheckedChangeListener {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if (isChecked) {

				zaOrderByButton.setChecked(false);
				zaOrderByButton.setTextColor(R.color.gray_215);

				azOrderByButton.setTextColor(R.color.white);
				setOrderByMode(OmegaConfiguration.ORDER_BY_MODE_CRESCENTE);
				ListUpdate(true);
			}
		}

	}

	private void ListUpdate(boolean pAsc) {
		adapter.SortList(pAsc);
		adapter.notifyDataSetChanged();
	}

	public class ZAOrderByTypeChangeListener implements OnCheckedChangeListener {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if (isChecked) {
				azOrderByButton.setChecked(false);
				azOrderByButton.setTextColor(R.color.gray_215);

				zaOrderByButton.setTextColor(R.color.white);
				setOrderByMode(OmegaConfiguration.ORDER_BY_MODE_AGRUPADO_OFF);
				ListUpdate(false);

			}
		}
	}

	public void initializeComponents() {
		listView = getListView();
		noResultView = findViewById(R.id.ll_no_result_view);
		((ToggleButton) findViewById(R.id.toogle_a_z_button)).setTypeface(this.customTypeFace);
		((ToggleButton) findViewById(R.id.toogle_z_a_button)).setTypeface(this.customTypeFace);

		// Attach toogle_a_z_button to ZAOrderByTypeChangeListener()
		this.zaOrderByButton = (ToggleButton) findViewById(R.id.toogle_z_a_button);
		this.zaOrderByButton.setOnCheckedChangeListener(new ZAOrderByTypeChangeListener());

		// Attach toogle_z_a_button to AZOrderByTypeChangeListener()
		this.azOrderByButton = (ToggleButton) findViewById(R.id.toogle_a_z_button);
		this.azOrderByButton.setOnCheckedChangeListener(new AZOrderByTypeChangeListener());

		// Sets default search mode
		switch (this.currentOrderByMode) {
		case OmegaConfiguration.ORDER_BY_MODE_CRESCENTE:
			this.azOrderByButton.setChecked(true);
			this.zaOrderByButton.setChecked(false);
			break;
		case OmegaConfiguration.ORDER_BY_MODE_AGRUPADO_OFF:
			this.zaOrderByButton.setChecked(true);
			this.azOrderByButton.setChecked(false);
			break;
		}
		setListAdapter(adapter);
	}

	public void performExecute() {
		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent intent = null;

		if (PrincipalActivity.isTablet) {
			switch (item.getItemId()) {

			// case R.id.menu_sincronizador_dado:
			// intent = new Intent(this,
			// SynchronizeReceiveActivityMobile.class);
			// break;
			case R.id.menu_sobre:
				intent = new Intent(this, AboutActivityMobile.class);
				break;

			default:
				return super.onOptionsItemSelected(item);

			}
		}
		if (intent != null) {
			startActivity(intent);
		}
		return true;

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if (requestCode == OmegaConfiguration.ACTIVITY_FORM_EDIT) {

			try {
				boolean vIsModificated = true;
				Bundle pParameter = intent.getExtras();

				if (pParameter != null)
					if (pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED))
						vIsModificated = pParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED);
				if (vIsModificated)
					performExecute();

			} catch (Exception ex) {
				Log.w("onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());
			}
		}
	}

	private void setOrderByMode(short p_orderByMode) {
		this.currentOrderByMode = p_orderByMode;

	}

	protected Dialog CreateAlertDialog(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message).setCancelable(false).setNeutralButton(R.string.botao_voltar,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						finish();
					}
				});

		return builder.create();
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		switch (id) {
		case OmegaConfiguration.ON_LOAD_DIALOG:
			return controlerProgressDialog.getDialog();

		case OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG:
			Dialog vDialog = new Dialog(this);
			vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			vDialog.setContentView(R.layout.dialog);

			TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
			Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

			vTextView.setText(getResources().getString(R.string.dialog_noresult_alert));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
					finish();
				}
			});
			return vDialog;
		default:
			return null;
		}

	}

	public boolean loadData() throws Exception {
		loadParameters();
		buildAdapter(true);
		if (adapter.getSize() == 0)
			adapter = null;
		return (adapter != null ? true : false);
	}

	private void showDialogNoResultIfNecessary() {
		if (adapter == null && adapter.getCount() <= 0) {
			if (noResultView == null) {
				showDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
			} else {
				noResultView.setVisibility(View.VISIBLE);
				listView.setVisibility(View.GONE);
			}

		} else {
			noResultView.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void finish() {

		if (receiverConnectionServerInformation != null)
			receiverConnectionServerInformation.unregisterReceiverIfExist();
		super.finish();

	}

	public class CustomDataLoader extends AsyncTask<String, Integer, Boolean> {

		Activity activity;

		
		Exception __ex2 = null;

		public CustomDataLoader(Activity pActivity) {
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();

		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				return loadData();
			} catch (Exception e) {
				__ex2 = e;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				if(SingletonLog.openDialogError(activity, __ex2, SingletonLog.TIPO.PAGINA))
					return;
				
				if (result) {
					initializeComponents();
				} 
				showDialogNoResultIfNecessary();
			} catch (Exception ex) {
				SingletonLog.openDialogError(OmegaCustomArrayAdapterListActivity.this, ex, SingletonLog.TIPO.PAGINA);
			} finally {
				controlerProgressDialog.dismissProgressDialog();
			}
		}

	}

}
