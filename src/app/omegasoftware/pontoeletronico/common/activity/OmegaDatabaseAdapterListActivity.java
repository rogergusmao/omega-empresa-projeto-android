package app.omegasoftware.pontoeletronico.common.activity;

import java.util.ArrayList;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.InformationDialog;
import app.omegasoftware.pontoeletronico.common.Adapter.DatabaseCustomAdapter;
import app.omegasoftware.pontoeletronico.common.Adapter.TecladoAdapter;
import app.omegasoftware.pontoeletronico.common.controler.ControlerProgressDialog;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiverBroadcastServiceConnectionServerInformation;

public abstract class OmegaDatabaseAdapterListActivity extends ListActivity {

	protected String TAG = "OmegaDatabaseAdapterListActivity";

	private Button fecharMenuFiltroButton;
	private Button fecharMenuDiscadorButton;
	private LinearLayout filtrarLinearLayout;
	private LinearLayout menuFiltroLinearLayout;

	private LinearLayout menuInferiorLinearLayout;
	private LinearLayout menuDiscadorLinearLayout;

	private LinearLayout cadastrarLinearLayout;
	private ImageButton buscarButton;
	private LinearLayout discadorLinearLayout;
	private EditText discadorEditText;
	private View tecladoView;

	private EditText filtrarEditText;
	ControlerProgressDialog controlerProgressDialog;
	// Order by mode
	public short currentOrderByMode = OmegaConfiguration.DATA_ORDER_BY_MODE;

	protected DatabaseCustomAdapter adapter;

	String nomeTag = null;
	String titulo = null;
	ReceiverBroadcastServiceConnectionServerInformation receiverConnectionServerInformation;

	protected ArrayList<String> listEnderecosDialog;
	protected HandlerFiltroRapido handlerFiltroRapido = new HandlerFiltroRapido();
	protected HandlerDiscador handlerDiscador = new HandlerDiscador();
	View listView;
	View noResultView;


	public class HandlerFiltroRapido extends Handler {
		public boolean ativo = false;

		public HandlerFiltroRapido() {

		}

		@Override
		public void handleMessage(Message msg) {
			if (menuFiltroLinearLayout != null) {
				// filtro inativo
				if (menuFiltroLinearLayout.getVisibility() == View.GONE) {
					ativo = true;
					menuInferiorLinearLayout.setVisibility(View.GONE);
					menuFiltroLinearLayout.setVisibility(View.VISIBLE);
					filtrarEditText.requestFocus();
					OmegaDatabaseAdapterListActivity.this.showSoftKeyboard(filtrarEditText);
				} else {

					ativo = false;
					menuFiltroLinearLayout.setVisibility(View.GONE);
					menuInferiorLinearLayout.setVisibility(View.VISIBLE);
					filtrarEditText.setText("");
					filtrarEditText.clearFocus();
				}
			}

		}
	}

	public class HandlerDiscador extends Handler {
		public boolean ativo = false;

		public HandlerDiscador() {

		}

		@Override
		public void handleMessage(Message msg) {

			if (menuDiscadorLinearLayout != null) {

				// filtro inativo
				if (menuDiscadorLinearLayout.getVisibility() == View.GONE) {
					ativo = true;
					menuDiscadorLinearLayout.setVisibility(View.VISIBLE);
					menuInferiorLinearLayout.setVisibility(View.GONE);
					discadorEditText.requestFocus();

					OmegaDatabaseAdapterListActivity.this.showSoftKeyboard(discadorEditText);
				} else {
					ativo = false;
					discadorEditText.setText("");
					discadorEditText.clearFocus();
					menuDiscadorLinearLayout.setVisibility(View.GONE);
					menuInferiorLinearLayout.setVisibility(View.VISIBLE);

				}
			}
		}
	}

	public void setTituloCabecalho(String nomeUsuario) {
		View viewTitle = findViewById(R.id.tv_title);
		TextView tvTitle = (TextView) viewTitle;
		tvTitle.setText(nomeUsuario);
	}

	protected abstract void loadCommomParameters(Bundle pParameters);

	protected abstract void buildAdapter(boolean pIsAsc);

	@Override
	public void onBackPressed() {
		if (menuFiltroLinearLayout != null
				&& menuFiltroLinearLayout.getVisibility() != View.GONE) {
			handlerFiltroRapido.sendEmptyMessage(0);
		} else if (menuDiscadorLinearLayout != null
				&& menuDiscadorLinearLayout.getVisibility() != View.GONE) {
			handlerDiscador.sendEmptyMessage(0);
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_IS_ORDENACAO_DATA))
			setContentView(R.layout.list_data_activity_mobile_layout);
		else
			setContentView(R.layout.list_activity_mobile_layout);
		if (getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_TAG))
			nomeTag = getIntent().getStringExtra(OmegaConfiguration.SEARCH_FIELD_TAG);
		if (getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_TITULO)) {
			int idTitulo = getIntent().getIntExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, -999);
			if (idTitulo != -999) {
				titulo = HelperString.ucFirstForEachToken(getResources().getString(idTitulo));
			}
		}

		controlerProgressDialog = new ControlerProgressDialog(this);
		if (!getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_NOT_LOAD_CUSTOM_EXECUTE)) {

			new CustomDataLoader().execute();

		}
	}

	public void setTag(String p_tag) {
		this.TAG = p_tag;
	}

	public void addIdPrestadorEnderecoDialog(String p_idPrestadorEndereco) {
		listEnderecosDialog.add(p_idPrestadorEndereco);
	}

	public void clearPrestadorEnderecoDialog() {
		listEnderecosDialog.clear();
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mobile_menu, menu);
		return true;

	}

	protected void loadParameters() {

		Bundle vParameters = getIntent().getExtras();
		if (vParameters == null)
			vParameters = new Bundle();
		loadCommomParameters(vParameters);

	}

	public void performExecute() {
		new CustomDataLoader().execute();
	}

	@Override
	public void finish() {

		if (receiverConnectionServerInformation != null)
			receiverConnectionServerInformation.unregisterReceiverIfExist();
		super.finish();

	}

	protected Boolean getSavedDialogInformacao() {
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		if (vSavedPreferences.contains(nomeTag + "_dialog_informacao"))
			return vSavedPreferences.getBoolean(nomeTag + "_dialog_informacao", false);
		else
			return false;
	}

	// Save Cidade ID in the preferences
	protected void saveDialogInformacao() {

		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putBoolean(nomeTag + "_dialog_informacao", true);
		vEditor.commit();

	}

	boolean isInformacaoDialogShow = false;

	@Override
	protected Dialog onCreateDialog(int id) {

		switch (id) {
		case OmegaConfiguration.ON_LOAD_DIALOG:
			return controlerProgressDialog.getDialog();
		default:
			break;
		}

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		try {
			switch (id) {

			case OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG:
				vTextView.setText(getResources().getString(R.string.dialog_noresult_alert));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
						// if(contTentativasDeBusca < 1){
						// contTentativasDeBusca += 1;
						// OmegaDatabaseAdapterListActivity.this.setIntent(new
						// Intent());
						// OmegaDatabaseAdapterListActivity.this.performExecute();
						//
						// } else
						// contTentativasDeBusca = 0;

					}
				});
				break;
			case OmegaConfiguration.FORM_ERROR_DIALOG_INFORMACAO_DIALOG:
				if (isInformacaoDialogShow)
					return null;
				else
					isInformacaoDialogShow = true;
				InformationDialog vInformation = new InformationDialog(this, vDialog, nomeTag + "_dialog_informacao",
						OmegaConfiguration.FORM_ERROR_DIALOG_INFORMACAO_DIALOG);
				return vInformation.getDialogInformation(new int[] { R.string.informacao_uso_list_detalhes,
						R.string.informacao_uso_list_gerenciamento });

			default:
				// vDialog = this.getErrorDialog(id);
				return null;
			}
		} catch (Exception ex) {
			return null;
		}
		return vDialog;

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		try {

			switch (requestCode) {
			case OmegaConfiguration.ACTIVITY_FILTER:
				switch (resultCode) {
				case RESULT_OK:
					// contTentativasDeBusca = 0;
					setIntent(intent);
					performExecute();
					break;

				default:
					break;
				}
				break;

			case OmegaConfiguration.ACTIVITY_DETAIL:
			case OmegaConfiguration.ACTIVITY_FORM_EDIT:
				switch (resultCode) {
				default:

					boolean vIsModificated = true;
					if (intent != null) {
						Bundle pParameter = intent.getExtras();

						if (pParameter != null)
							if (pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED))
								vIsModificated = pParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED);
						if (vIsModificated)
							performExecute();

						break;
					}
				}

				break;

			default:
				break;
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.LISTA);

		}

	}

	public class CustomDataLoader extends AsyncTask<String, Integer, Boolean> {

		
		Exception __ex2 = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				return loadData();
			} catch (Exception e) {
				__ex2 = e;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				if(SingletonLog.openDialogError(OmegaDatabaseAdapterListActivity.this, this.__ex2, SingletonLog.TIPO.PAGINA)){
					return;
				}
				if (result) {
					initializeComponents();
				} 
				
				showDialogNoResultIfNecessary();
			} catch (Exception ex) {
				SingletonLog.openDialogError(OmegaDatabaseAdapterListActivity.this, ex, SingletonLog.TIPO.PAGINA);
			} finally {
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}

	private void showDialogNoResultIfNecessary() {
		if (adapter == null || adapter.getCount() == 0) {

			if (noResultView == null) {
				showDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
			} else {
				noResultView.setVisibility(View.VISIBLE);
				listView.setVisibility(View.GONE);
			}
		} else {
			noResultView.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
		}
	}

	public class AdapterChangeListener implements OnCheckedChangeListener {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

			buildAdapter(true);
			if (adapter != null) {
				adapter.setHandler(handlerFiltroRapido, handlerDiscador);
			}
		}
	}

	public void initializeComponents() {
		listView = getListView();
		noResultView = findViewById(R.id.ll_no_result_view);
		Intent intent = getIntent();
		if (intent != null && intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_TITULO)) {
			int idStringTitulo = intent.getIntExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, -1);
			if (idStringTitulo != -1) {
				String titulo = getResources().getString(idStringTitulo);
				setTituloCabecalho(titulo);
			}

		}
		receiverConnectionServerInformation = new ReceiverBroadcastServiceConnectionServerInformation(this, nomeTag);
		View vCadastrar = findViewById(R.id.ll_cadastrar);
		if (vCadastrar != null) {
			final Class<?> classeFormulario = adapter.getClasseFormulario();
			if (classeFormulario == null) {
				vCadastrar.setVisibility(View.GONE);
			} else {
				vCadastrar.setVisibility(View.VISIBLE);
				cadastrarLinearLayout = (LinearLayout) vCadastrar;
				cadastrarLinearLayout.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						if (classeFormulario != null) {
							Intent intent = new Intent(OmegaDatabaseAdapterListActivity.this, classeFormulario);
							startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_EDIT);
						}

					}
				});
			}

		}

		View filtrar = findViewById(R.id.ll_filtrar);
		if (filtrar != null) {
			filtrarLinearLayout = (LinearLayout) filtrar;
			filtrarLinearLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					handlerFiltroRapido.sendEmptyMessage(0);
				}
			});
			fecharMenuFiltroButton = (Button) findViewById(R.id.bt_fechar_menu_filtro);
			fecharMenuFiltroButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					handlerFiltroRapido.sendEmptyMessage(1);
				}
			});

			fecharMenuDiscadorButton = (Button) findViewById(R.id.bt_fechar_menu_discador);
			fecharMenuDiscadorButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					handlerDiscador.sendEmptyMessage(1);
				}
			});

			menuFiltroLinearLayout = (LinearLayout) findViewById(R.id.ll_menu_filtro);

		}

		View vDiscador = findViewById(R.id.ll_discador);
		if (vDiscador != null) {
			discadorLinearLayout = (LinearLayout) vDiscador;

			discadorEditText = (EditText) findViewById(R.id.et_discador);
			tecladoView = (View) findViewById(R.id.ll_menu_discador);
			TecladoAdapter tecladoAdapter = new TecladoAdapter(adapter);
			tecladoAdapter.formatarView(tecladoView);

			menuInferiorLinearLayout = (LinearLayout) findViewById(R.id.ll_menu_inferior);

			menuDiscadorLinearLayout = (LinearLayout) findViewById(R.id.ll_menu_discador);
			if (getIntent().hasExtra(OmegaConfiguration.DESABILITA_DISCADOR)) {
				discadorLinearLayout.setVisibility(View.GONE);
			} else {
				discadorLinearLayout.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						handlerDiscador.sendEmptyMessage(0);
					}
				});
			}

		}
		View vBuscar = findViewById(R.id.buscar_topo_button);
		if (vBuscar != null) {
			final Class<?> classeFiltro = adapter.getClasseFiltro();
			if (classeFiltro == null) {
				vBuscar.setVisibility(View.GONE);
			} else {
				buscarButton = (ImageButton) vBuscar;
				buscarButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						if (classeFiltro != null) {
							Intent intent = new Intent(OmegaDatabaseAdapterListActivity.this, classeFiltro);
							startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FILTER);

						}

					}
				});
				vBuscar.setVisibility(View.VISIBLE);
			}
		}

		if (titulo != null && titulo.length() > 0) {
			setTituloCabecalho(titulo);
			View v = findViewById(R.id.tv_title);
			if (v != null) {
				TextView tvTitulo = (TextView) v;
				tvTitulo.setText(titulo);
			}
		}

		if(getIntent().getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_INATIVA_FILTRO_TELEFONE, false)){
			inativaFiltroTelefone();
		}

		setListAdapter(adapter);
	}

	public boolean loadData() throws Exception{
		loadParameters();
		buildAdapter(true);
		if (adapter != null) {
			adapter.setHandler(handlerFiltroRapido, handlerDiscador);
		}
		filtrarEditText = (EditText) findViewById(R.id.et_filtrar);

		filtrarEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

				adapter.getFilter().filter(cs);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			}

			@Override
			public void afterTextChanged(Editable arg0) {
			}
		});

		return (adapter != null ? true : false);
	}

	public void showSoftKeyboard(View view) {
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		view.requestFocus();
		inputMethodManager.showSoftInput(view, 0);
	}

	public void inativaFiltroTelefone(){
		discadorLinearLayout.setVisibility(View.GONE);
	}
}
