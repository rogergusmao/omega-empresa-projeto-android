package app.omegasoftware.pontoeletronico.common.activity;

import java.util.ArrayList;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.AboutActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.SynchronizeReceiveActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.InformationDialog;
import app.omegasoftware.pontoeletronico.common.Adapter.DatabaseCustomArrayAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.controler.ControlerProgressDialog;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.pontoeletronico.PrincipalActivity;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiverBroadcastServiceConnectionServerInformation;

public abstract class OmegaDatabaseArrayAdapterListActivity extends ListActivity {

	protected String TAG = "OmegaDatabaseArrayAdapterListActivity";

	private ToggleButton azOrderByButton;
	private ToggleButton zaOrderByButton;
	ListView listView;
	View noResultView;

	ControlerProgressDialog controlerProgressDialog;
	// Order by mode
	private short currentOrderByMode = OmegaConfiguration.DATA_ORDER_BY_MODE;

	protected DatabaseCustomArrayAdapter adapter;

	String nomeTag = null;
	ReceiverBroadcastServiceConnectionServerInformation receiverConnectionServerInformation;

	protected ArrayList<String> listIdPrestadorEnderecoDialog = new ArrayList<String>();
	protected Typeface customTypeFace;

	protected abstract void loadCommomParameters(Bundle pParameters);

	protected abstract void buildAdapter(boolean pIsAsc);
	//
	//
	// @Override
	// protected void onListItemClick(ListView l, View v, int position, long id)
	// {
	//
	// CustomItemList item = adapter.findId(String.valueOf(id));
	// EditOrRemoveDialog dialog = new EditOrRemoveDialog(
	// OmegaDatabaseArrayAdapterListActivity.this,
	// item.getId(),
	// adapter.getNomeTabela(),
	// adapter.getClassFormEdit(),
	// adapter.isRemovePermitido(),
	// adapter.isEditPermitido(),
	// item);
	// dialog.showAlertDialog();
	// ContainerActionDetail.actionDetail(OmegaDatabaseArrayAdapterListActivity.this,
	// adapter.getSearchMode(), item.getId());
	// }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_IS_ORDENACAO_DATA))
			setContentView(R.layout.list_data_activity_mobile_layout);
		else
			setContentView(R.layout.list_activity_mobile_layout);
		if (getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_TAG))
			nomeTag = getIntent().getStringExtra(OmegaConfiguration.SEARCH_FIELD_TAG);

		controlerProgressDialog = new ControlerProgressDialog(this);
		if (!getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_NOT_LOAD_CUSTOM_EXECUTE)) {

			new CustomDataLoader().execute();

		}
	}

	public void setTag(String p_tag) {
		this.TAG = p_tag;
	}

	public void deleteItem(CustomItemList toRemove) {

		adapter.delete(toRemove);
		adapter.remove(toRemove);
		adapter.notifyDataSetChanged();

		// view.setAlpha(1);

	}

	public void editItem(CustomItemList oldItem, CustomItemList newItem, int index) {

		adapter.remove(oldItem);
		adapter.insert(newItem, index);

		adapter.notifyDataSetChanged();

		// view.setAlpha(1);

	}

	public void addIdPrestadorEnderecoDialog(String p_idPrestadorEndereco) {
		listIdPrestadorEnderecoDialog.add(p_idPrestadorEndereco);
	}

	public void clearPrestadorEnderecoDialog() {
		listIdPrestadorEnderecoDialog.clear();
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mobile_menu, menu);
		return true;

	}

	protected void loadParameters() {
		Bundle vParameters = getIntent().getExtras();
		if (vParameters != null) {
			loadCommomParameters(vParameters);
		}
	}

	public void performExecute() {
		new CustomDataLoader().execute();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent intent = null;

		if (PrincipalActivity.isTablet) {
			switch (item.getItemId()) {

			// case R.id.menu_sincronizador_dado:
			// intent = new Intent(this,
			// SynchronizeReceiveActivityMobile.class);
			// break;
			case R.id.menu_sobre:
				intent = new Intent(this, AboutActivityMobile.class);
				break;

			default:
				return super.onOptionsItemSelected(item);

			}
		}
		if (intent != null) {
			startActivity(intent);
		}
		return true;

	}

	@Override
	public void finish() {

		if (receiverConnectionServerInformation != null)
			receiverConnectionServerInformation.unregisterReceiverIfExist();
		super.finish();

	}

	protected Boolean getSavedDialogInformacao() {
		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		if (vSavedPreferences.contains(nomeTag + "_dialog_informacao"))
			return vSavedPreferences.getBoolean(nomeTag + "_dialog_informacao", false);
		else
			return false;
	}

	// Save Cidade ID in the preferences
	protected void saveDialogInformacao() {

		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putBoolean(nomeTag + "_dialog_informacao", true);
		vEditor.commit();

	}

	boolean isInformacaoDialogShow = false;

	@Override
	protected Dialog onCreateDialog(int id) {

		switch (id) {
		case OmegaConfiguration.ON_LOAD_DIALOG:
			return controlerProgressDialog.getDialog();
		default:
			break;
		}

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		try {
			switch (id) {

			case OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG:
				vTextView.setText(getResources().getString(R.string.dialog_noresult_alert));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
						Intent vIntent = getIntent();
						vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, true);
						finish();
					}
				});
				break;
			case OmegaConfiguration.FORM_ERROR_DIALOG_INFORMACAO_DIALOG:
				if (isInformacaoDialogShow)
					return null;
				else
					isInformacaoDialogShow = true;
				InformationDialog vInformation = new InformationDialog(this, vDialog, nomeTag + "_dialog_informacao",
						OmegaConfiguration.FORM_ERROR_DIALOG_INFORMACAO_DIALOG);
				return vInformation.getDialogInformation(new int[] { R.string.informacao_uso_list_detalhes,
						R.string.informacao_uso_list_gerenciamento });

			default:
				// vDialog = this.getErrorDialog(id);
				return null;
			}
		} catch (Exception ex) {
			return null;
		}
		return vDialog;

	}

	@SuppressWarnings("unused")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		try {
			switch (requestCode) {
			case OmegaConfiguration.ACTIVITY_FORM_EDIT:
				switch (resultCode) {

				default:

					boolean vIsModificated = true;
					if (intent != null) {
						Bundle pParameter = intent.getExtras();

						if (pParameter != null)
							if (pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED))
								vIsModificated = pParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED);
						// if(vIsModificated){
						// String id
						// =pParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID);
						// int position = adapter.findPosition(id);
						// CustomItemList oldItem = adapter.getItem(position);
						// Table tupla = adapter.getObjTable();
						// tupla.select(oldItem.getId());
						// CustomItemList newItem =
						// adapter.getCustomItemListOfTable(tupla);
						//// editItem(oldItem, newItem, position);
						// }

						break;
					}
				}

				break;

			default:
				break;
			}
		} catch (Exception ex) {
			Log.e("onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());
		}

	}

	public class CustomDataLoader extends AsyncTask<String, Integer, Boolean> {
		
		Exception __ex2 = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				return loadData();
			} catch (Exception e) {
				__ex2 = e;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				if(SingletonLog.openDialogError(
						OmegaDatabaseArrayAdapterListActivity.this, 
						__ex2,
						SingletonLog.TIPO.PAGINA))
					return;
				
				if (result) {
					initializeComponents();
				} 

				showDialogNoResultIfNecessary();
			} catch (Exception ex) {
				SingletonLog.openDialogError(
						OmegaDatabaseArrayAdapterListActivity.this, 
						ex,
						SingletonLog.TIPO.PAGINA);
			} finally {
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}

	private void showDialogNoResultIfNecessary() {
		if (adapter == null && adapter.getCount() <= 0) {
			showDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
		}
	}

	public class AZOrderByTypeChangeListener implements OnCheckedChangeListener {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if (isChecked) {

				zaOrderByButton.setChecked(false);
				zaOrderByButton.setTextColor(R.color.gray_215);

				azOrderByButton.setTextColor(R.color.white);
				setOrderByMode(OmegaConfiguration.ORDER_BY_MODE_CRESCENTE);
				ListUpdate(true);
			}
		}

	}

	public class AdapterChangeListener implements OnCheckedChangeListener {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			boolean vIsAsc = azOrderByButton.isChecked();
			buildAdapter(vIsAsc);
		}
	}

	private void setOrderByMode(short p_orderByMode) {
		this.currentOrderByMode = p_orderByMode;

	}

	public void initializeComponents() {

		this.customTypeFace = Typeface.createFromAsset(this.getAssets(), "trebucbd.ttf");
		if (nomeTag != null)
			receiverConnectionServerInformation = new ReceiverBroadcastServiceConnectionServerInformation(this,
					nomeTag);

		((ToggleButton) findViewById(R.id.toogle_a_z_button)).setTypeface(this.customTypeFace);
		((ToggleButton) findViewById(R.id.toogle_z_a_button)).setTypeface(this.customTypeFace);

		// Attach toogle_a_z_button to ZAOrderByTypeChangeListener()
		this.zaOrderByButton = (ToggleButton) findViewById(R.id.toogle_z_a_button);
		this.zaOrderByButton.setOnCheckedChangeListener(new ZAOrderByTypeChangeListener());

		// Attach toogle_z_a_button to AZOrderByTypeChangeListener()
		this.azOrderByButton = (ToggleButton) findViewById(R.id.toogle_a_z_button);
		this.azOrderByButton.setOnCheckedChangeListener(new AZOrderByTypeChangeListener());
		// noResultView = (View) findViewById(R.id.)
		// Sets default search mode
		switch (this.currentOrderByMode) {
		case OmegaConfiguration.ORDER_BY_MODE_CRESCENTE:
			this.azOrderByButton.setChecked(true);
			this.zaOrderByButton.setChecked(false);
			break;
		case OmegaConfiguration.ORDER_BY_MODE_AGRUPADO_OFF:
			this.zaOrderByButton.setChecked(true);
			this.azOrderByButton.setChecked(false);
			break;
		}

		setListAdapter(adapter);
		listView = getListView();
		noResultView = findViewById(R.id.ll_no_result_view);

		// listView.setLongClickable(true);
		// listView.setClickable(true);
		// listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		final Vibrator vibe = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
		//
		// listView.setOnItemLongClickListener(new
		// AdapterView.OnItemLongClickListener() {
		//
		// public boolean onItemLongClick(AdapterView<?> parent, View view,
		// int position, long id) {
		//// Toast.makeText(OmegaDatabaseArrayAdapterListActivity.this,
		//// "Item in position " + position + " clicked",
		//// Toast.LENGTH_LONG).show();
		// vibe.vibrate(80);
		//// CustomItemList item = (CustomItemList)adapter.getItem((int)id);
		// CustomItemList item = adapter.findId(String.valueOf(id));
		// EditOrRemoveDialog dialog = new EditOrRemoveDialog(
		// OmegaDatabaseArrayAdapterListActivity.this,
		// item.getId(),
		// adapter.getNomeTabela(),
		// adapter.getClassFormEdit(),
		// adapter.isRemovePermitido(),
		// adapter.isEditPermitido(),
		// item);
		// dialog.showAlertDialog();
		// return true;
		// }
		// });
		//

		// listView.setOnItemClickListener(listener);
		//
		adapter.setDropDownViewResource(R.layout.empresa_item_list_layout);
		adapter.notifyDataSetChanged();
	}

	// private AdapterView.OnItemClickListener listener = new
	// AdapterView.OnItemClickListener() {
	//
	// @Override
	// public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
	// long id) {
	//
	// CustomItemList item = adapter.findId(String.valueOf(id));
	// EditOrRemoveDialog dialog = new EditOrRemoveDialog(
	// OmegaDatabaseArrayAdapterListActivity.this,
	// item.getId(),
	// adapter.getNomeTabela(),
	// adapter.getClassFormEdit(),
	// adapter.isRemovePermitido(),
	// adapter.isEditPermitido(),
	// item);
	// dialog.showAlertDialog();
	// ContainerActionDetail.actionDetail(OmegaDatabaseArrayAdapterListActivity.this,
	// adapter.getSearchMode(), item.getId());
	// }
	// };
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		default:
			break;
		}
		return super.onContextItemSelected(item);
	}

	public boolean loadData() throws Exception {
		loadParameters();
		buildAdapter();

		return (adapter != null ? true : false);
	}

	private void buildAdapter() {
		buildAdapter(true);
	}

	public class ZAOrderByTypeChangeListener implements OnCheckedChangeListener {

		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if (isChecked) {
				azOrderByButton.setChecked(false);
				azOrderByButton.setTextColor(R.color.gray_215);

				zaOrderByButton.setTextColor(R.color.white);
				setOrderByMode(OmegaConfiguration.ORDER_BY_MODE_AGRUPADO_OFF);
				ListUpdate(false);

			}
		}
	}

	private void ListUpdate(boolean pAsc) {
		if (adapter.getCount() == 0) {
			if (noResultView == null) {
				showDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
			} else {
				noResultView.setVisibility(View.VISIBLE);
				listView.setVisibility(View.GONE);
			}

		} else {
			adapter.SortList(pAsc);
			adapter.notifyDataSetChanged();

			noResultView.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);

			showDialog(OmegaConfiguration.FORM_ERROR_DIALOG_INFORMACAO_DIALOG);
		}
	}

}
