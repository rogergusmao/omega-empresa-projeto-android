package app.omegasoftware.pontoeletronico.common.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.ControladorAtualizadorVersao.ESTADO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;


public abstract class OmegaDetailActivity extends OmegaRegularActivity {

	protected String TAG = "OmegaDetailActivity";

	protected String id = null;
	protected DatabasePontoEletronico db=null;
	
	//Search button
	protected ImageButton editarButton;
	protected ImageButton removerButton;
	boolean editado = false;
	Class<?> classeEdicao = null;

	@Override
	public void procedureBeforeLoadData() throws OmegaDatabaseException {
		this.db = new DatabasePontoEletronico(this);
		Bundle vParameter = getIntent().getExtras();
		if(vParameter != null){
			
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_ID)){
				id = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID);
			}
			if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_TAG)){
				nomeTag = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_TAG);
			}
		}
	}
	
	protected abstract void beforeInitializeComponents();

	public abstract Table getEXTDAO();
	
	@Override
	public void initializeComponents() {
		
		
		if(getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_ID)){
			id = getIntent().getStringExtra(OmegaConfiguration.SEARCH_FIELD_ID);
		}
		beforeInitializeComponents();
		
		if(getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_CLASS)){
			
			classeEdicao = (Class<?>)  getIntent().getExtras().get(OmegaConfiguration.SEARCH_FIELD_CLASS);	
		}
		
		
		if(classeEdicao != null){
			//Attach search button to its listener
			this.editarButton = (ImageButton) findViewById(R.id.editar_topo_button);
			this.editarButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {

					Intent vIntent = new Intent(
								OmegaDetailActivity.this, 
								classeEdicao);
					vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, id);
					startActivityForResult(vIntent, OmegaConfiguration.ACTIVITY_DETAIL);

					
				}
			});
		}
		View vRemover = findViewById(R.id.remover_topo_button);
		if(vRemover != null){
			this.removerButton = (ImageButton) vRemover;
			this.removerButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					DialogInterface.OnClickListener dialogConfirmarSaida = new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog, int which) {
					        switch (which){
					        case DialogInterface.BUTTON_POSITIVE:
					        	
				  				new DeleteLoader().execute();
				  				
					        	break;

					        case DialogInterface.BUTTON_NEGATIVE:
					        	
					            break;
					        }
					    }
					};
					FactoryAlertDialog.showAlertDialog(
							OmegaDetailActivity.this, 
							getString(R.string.form_confirmacao_remocao),
							dialogConfirmarSaida);
				}
				
			});
		}
	}
	
	public class DeleteLoader extends AsyncTask<String, Integer, Boolean> {
		
		Exception ex2 = null;
		Mensagem msg =null;
		public DeleteLoader() {
			
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try{
				Table obj = getEXTDAO();
		    	msg = Table.procedimentoDelete(OmegaDetailActivity.this, obj.getName(), id, null );
//		    	obj.select(id);
//		    	try{
//		    		obj.remove(true);
//		    	} catch(Exception ex){
//					SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
//				}

				
				Intent intent = getIntent();
				if(msg.ok()){
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, true);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, id);
					setResult(RESULT_OK, intent);	
				} else {
					setResult(RESULT_CANCELED, intent);
				}
				
				finish();
			}catch(Exception ex){
				this.ex2=ex;	
			}
			
			
			return true;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
				if(this.ex2!= null)
					FactoryAlertDialog.makeToast(OmegaDetailActivity.this, this.ex2, null);
				else if(msg != null)
					Toast.makeText(OmegaDetailActivity.this, msg.mMensagem, Toast.LENGTH_SHORT).show();
					
			}finally{
				controlerProgressDialog.dismissProgressDialog();
			}


		}

	}

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
	}
	
	@Override
	public void onBackPressed(){
    	if(editado){
    		
			Intent intent = getIntent();
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, true);
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, id);
			setResult(RESULT_OK, intent);
			finish();
			return;
		}
    	super.onBackPressed();
	}
	

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if (requestCode == OmegaConfiguration.ACTIVITY_FORM_EDIT 
				|| requestCode == OmegaConfiguration.ACTIVITY_DETAIL
				|| requestCode == OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW) {
			if (resultCode == RESULT_OK) {
				Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED , false);
				
				if(vConfirmacao){
					editado = true;
					new CustomDataLoader(this).execute();
				}
			}
		}
	}
}
