package app.omegasoftware.pontoeletronico.common.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import app.omegasoftware.pontoeletronico.R;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import app.omegasoftware.pontoeletronico.TabletActivities.AboutActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.SynchronizeReceiveActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.controler.ControlerProgressDialog;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.pontoeletronico.PrincipalActivity;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiverBroadcastServiceConnectionServerInformation;

public abstract class OmegaListActivity extends ListActivity {

	protected String TAG = "OmegaListActivity";

	String nomeTag = null;
	ReceiverBroadcastServiceConnectionServerInformation receiverConnectionServerInformation;
	ControlerProgressDialog controlerProgressDialog;

	public abstract boolean loadData() throws Exception;

	public abstract void initializeComponents();

	protected ArrayList<String> listIdPrestadorEnderecoDialog = new ArrayList<String>();

	// TODO lembrar que nao tinha o onCreate
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		controlerProgressDialog = new ControlerProgressDialog(this);
	}

	public void setTag(String p_tag) {
		this.TAG = p_tag;
	}

	public void addIdPrestadorEnderecoDialog(String p_idPrestadorEndereco) {
		listIdPrestadorEnderecoDialog.add(p_idPrestadorEndereco);
	}

	public void clearPrestadorEnderecoDialog() {
		listIdPrestadorEnderecoDialog.clear();
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mobile_menu, menu);
		return true;
	}

	@Override
	public void finish() {

		if (receiverConnectionServerInformation != null)
			receiverConnectionServerInformation.unregisterReceiverIfExist();
		super.finish();

	}

	/**
	 * Creates loading dialog
	 */
	@Override
	protected Dialog onCreateDialog(int id) {

		switch (id) {
		case OmegaConfiguration.ON_LOAD_DIALOG:
			return controlerProgressDialog.getDialog();
		default:
			return null;
		}

	}

	public void performExecute() {
		new CustomDataLoader(this).execute();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent intent = null;

		if (PrincipalActivity.isTablet) {
			switch (item.getItemId()) {

			// case R.id.menu_sincronizador_dado:
			// intent = new Intent(this,
			// SynchronizeReceiveActivityMobile.class);
			// break;
			case R.id.menu_sobre:
				intent = new Intent(this, AboutActivityMobile.class);
				break;

			default:
				return super.onOptionsItemSelected(item);

			}
		}
		if (intent != null) {
			startActivity(intent);
		}
		return true;

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if (requestCode == OmegaConfiguration.ACTIVITY_FORM_EDIT) {

			try {
				boolean vIsModificated = true;
				Bundle pParameter = intent.getExtras();

				if (pParameter != null)
					if (pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED))
						vIsModificated = pParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED);
				if (vIsModificated)
					performExecute();

			} catch (Exception ex) {
				Log.w("onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());
			}
		}
	}

	public class CustomDataLoader extends AsyncTask<String, Integer, Boolean> {
		Activity activity;
		
		Exception __ex2 = null;

		public CustomDataLoader(Activity pActivity) {
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				return loadData();
			}  catch (Exception e) {
				__ex2 = e;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				if (nomeTag != null)
					receiverConnectionServerInformation = new ReceiverBroadcastServiceConnectionServerInformation(
							activity, nomeTag);
				
				if (SingletonLog.openDialogError(
						OmegaListActivity.this, 
						this.__ex2,
						SingletonLog.TIPO.PAGINA)) {
					return;
				}
				if (result) {
					initializeComponents();
				} 
			} catch (Exception ex) {
				
				SingletonLog.openDialogError(OmegaListActivity.this, ex, SingletonLog.TIPO.LISTA);
			} finally {
				controlerProgressDialog.dismissProgressDialog();
			}
		}

	}

}
