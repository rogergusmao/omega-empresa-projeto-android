package app.omegasoftware.pontoeletronico.common.activity;

//public abstract class OmegaMapActivity extends MapActivity {
//
//	// private static String TAG = "OmegaMapActivity";
//	public MapView mapView;
//
//	public LocationTouchOverlay m_locationTouchOverlay;
//	public MapController mapController;
//	ControlerProgressDialog controlerProgressDialog;
//	public LinkedHashMap<Drawable, InfoItemizedOverlay> hashIconeKeyByInfoOverlay = new LinkedHashMap<Drawable, InfoItemizedOverlay>();
//	public LinkedHashMap<Drawable, MapaItemizedOverlay> hashIconeKeyByItemizedOverlay = new LinkedHashMap<Drawable, MapaItemizedOverlay>();
//	Drawable listIcon[] = null;
//
//	String nomeTag = null;
//	ReceiverBroadcastServiceConnectionServerInformation receiverConnectionServerInformation;
//
//	protected ContainerMarcadorMapa principalContainerMarcadorMapa = null;
//	protected ArrayList<ContainerMarcadorMapa> listContainerMarcadorMapa = new ArrayList<ContainerMarcadorMapa>();
//
//	private int autoIncrement = 0;
//
//	public abstract boolean loadData() throws Exception;
//
//	public abstract void initializeComponents() throws Exception;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		Bundle vParameters = getIntent().getExtras();
//		try {
//			listIcon = (Drawable[]) vParameters.getParcelableArray(OmegaConfiguration.SEARCH_LIST_MAP_ICONE);
//		} catch (Exception ex) {
//			SingletonLog.insereErro(ex, TIPO.PAGINA);
//		}
//
//		controlerProgressDialog = new ControlerProgressDialog(this);
//
//	}
//
//	public boolean onCreateOptionsMenu(Menu menu) {
//		super.onCreateOptionsMenu(menu);
//
//		// MenuInflater inflater = getMenuInflater();
//		// inflater.inflate(R.menu.mobile_menu, menu);
//		return true;
//
//	}
//
//	/**
//	 * Creates loading dialog
//	 */
//	@Override
//	protected Dialog onCreateDialog(int id) {
//
//		switch (id) {
//		case OmegaConfiguration.ON_LOAD_DIALOG:
//			return controlerProgressDialog.getDialog();
//		default:
//			return null;
//		}
//
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//
//		Intent intent = null;
//
//		switch (item.getItemId()) {
//
//		// case R.id.menu_sincronizador_dado:
//		// intent = new Intent(this, SynchronizeReceiveActivityMobile.class);
//		// break;
//
//		case R.id.menu_sobre:
//			intent = new Intent(this, AboutActivityMobile.class);
//			break;
//
//		default:
//			return super.onOptionsItemSelected(item);
//
//		}
//
//		startActivity(intent);
//
//		return true;
//
//	}
//
//	public class CustomDataLoader extends AsyncTask<String, Integer, Boolean> {
//
//		Activity activity;
//		
//		Exception __ex2 = null;
//
//		public CustomDataLoader(Activity pActivity) {
//			activity = pActivity;
//		}
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			controlerProgressDialog.createProgressDialog();
//
//		}
//
//		@Override
//		protected Boolean doInBackground(String... params) {
//			try {
//				return loadData();
//			}  catch (Exception e) {
//				__ex2 = e;
//			}
//			return false;
//		}
//
//		@Override
//		protected void onPostExecute(Boolean result) {
//			super.onPostExecute(result);
//
//			try {
//				if (SingletonLog.openDialogError(activity, __ex2, SingletonLog.TIPO.PAGINA)) {
//					return;	
//				}
//				if (result) {
//					myInitializeComponents();
//					initializeComponents();
//				} 
//			}catch (Exception ex) {
//				SingletonLog.openDialogError(OmegaMapActivity.this, ex, SingletonLog.TIPO.PAGINA);
//			} finally {
//				controlerProgressDialog.dismissProgressDialog();
//			}
//		}
//
//	}
//
//	public void removeListMarcador(ArrayList<ContainerMarcadorMapa> pListContainerMarcadorMapa) {
//		if (pListContainerMarcadorMapa != null) {
//			if (pListContainerMarcadorMapa.size() > 0) {
//				for (ContainerMarcadorMapa containerMarcadorMapa : pListContainerMarcadorMapa) {
//					removeMarcador(containerMarcadorMapa);
//				}
//			}
//		}
//	}
//
//	public void removeMarcador(ContainerMarcadorMapa pContainerMarcadorMapa) {
//
//		Integer vIndex = pContainerMarcadorMapa.getIndex();
//		if (vIndex != null) {
//
//			if (this.hashIconeKeyByItemizedOverlay.containsKey(vIndex)) {
//				this.hashIconeKeyByItemizedOverlay.remove(vIndex);
//			}
//
//			if (this.hashIconeKeyByInfoOverlay.containsKey(vIndex)) {
//				this.hashIconeKeyByInfoOverlay.remove(vIndex);
//			}
//
//		}
//	}
//
//	private void initializeIfNecessaryInfoOverlayAndMapItemizedOverlay(Drawable pIcon) {
//		if (pIcon == null)
//			return;
//
//		if (!this.hashIconeKeyByInfoOverlay.containsKey(pIcon)) {
//
//			InfoItemizedOverlay infoOverlay = new InfoItemizedOverlay(pIcon, mapView);
//			this.hashIconeKeyByInfoOverlay.put(pIcon, infoOverlay);
//		}
//
//		if (!this.hashIconeKeyByItemizedOverlay.containsKey(pIcon)) {
//
//			MapaItemizedOverlay itemizedOverlay = new MapaItemizedOverlay(pIcon, mapView.getOverlays());
//			this.hashIconeKeyByItemizedOverlay.put(pIcon, itemizedOverlay);
//		}
//	}
//
//	private Integer adicionarMarcador(Drawable pIcon, OverlayItem overlayitem) {
//		int vIndex = autoIncrement;
//		InfoItemizedOverlay vInfoItemizedOverlay = null;
//		MapaItemizedOverlay vMapaItemizedOverlay = null;
//
//		initializeIfNecessaryInfoOverlayAndMapItemizedOverlay(pIcon);
//
//		if (this.hashIconeKeyByInfoOverlay.containsKey(pIcon)) {
//			vInfoItemizedOverlay = this.hashIconeKeyByInfoOverlay.get(pIcon);
//		}
//		if (this.hashIconeKeyByItemizedOverlay.containsKey(pIcon)) {
//			vMapaItemizedOverlay = this.hashIconeKeyByItemizedOverlay.get(pIcon);
//		}
//		if (vInfoItemizedOverlay != null && vMapaItemizedOverlay != null) {
//			overlayitem.setMarker(pIcon);
//			vInfoItemizedOverlay.addOverlay(overlayitem);
//			vMapaItemizedOverlay.addOverlay(overlayitem);
//			autoIncrement++;
//			return vIndex;
//		}
//		return null;
//	}
//
//	protected OverlayItem getOverlayItem(Location pLocation) {
//		OverlayItem overlayItem = new OverlayItem(
//				HelperMapa.getGeoPoint(pLocation.getLatitude(), pLocation.getLongitude()),
//				"Localizacao em " + HelperDate.getDatetimeAtualFormatadaParaExibicao(this),
//				"Localizacao: " + "Lat: " + (pLocation.getLatitude()) + " Lng: " + (pLocation.getLongitude()));
//		return overlayItem;
//	}
//
//	// public void acoesAoMudarDePosicao(Location location){
//	// GeoPoint vGeoPoint = HelperMapa.getGeoPoint(location);
//	// if(principalContainerMarcadorMapa != null){
//	// principalContainerMarcadorMapa.setGeoPoint(vGeoPoint);
//	// }
//	//
//	// GeoPoint pGeoPoint = HelperMapa.getGeoPoint(location);
//	// OverlayItem vOverlayItem = getOverlayItem(location);
//	//
//	// adicionarMarcador(principalContainerMarcadorMapa.icone, vOverlayItem);
//	//
//	// irParaPonto(pGeoPoint);
//	//
//	// }
//
//	public void clearListContainerMarcadorMapa() {
//		Set<Drawable> vListKey = hashIconeKeyByItemizedOverlay.keySet();
//		for (Drawable vKey : vListKey) {
//			MapaItemizedOverlay vItemizedOverlay = hashIconeKeyByItemizedOverlay.get(vKey);
//			vItemizedOverlay.limparOverlay();
//		}
//	}
//
//	public void myInitializeComponents() {
//		if (nomeTag != null)
//			receiverConnectionServerInformation = new ReceiverBroadcastServiceConnectionServerInformation(this,
//					nomeTag);
//
//		mapView = (MapView) findViewById(R.id.mapview);
//		mapView.setBuiltInZoomControls(true);
//
//		mapController = mapView.getController();
//		m_locationTouchOverlay = new LocationTouchOverlay(this);
//
//		boolean vValidade = false;
//		if (this.listIcon != null) {
//			if (this.listIcon.length > 0) {
//				vValidade = true;
//				for (Drawable vIcon : this.listIcon) {
//					initializeIfNecessaryInfoOverlayAndMapItemizedOverlay(vIcon);
//				}
//			}
//		}
//
//		if (!vValidade) {
//			Drawable vIcon = this.getResources().getDrawable(R.drawable.marcador);
//			initializeIfNecessaryInfoOverlayAndMapItemizedOverlay(vIcon);
//		}
//	}
//
//	public void loadPrincipalContainerMarcadorMapa(ContainerMarcadorMapa pContainerMarcadorMapa) {
//
//		OverlayItem vOverlayItem = new OverlayItem(pContainerMarcadorMapa.geoPoint, pContainerMarcadorMapa.tituloBalao,
//				pContainerMarcadorMapa.textoBalao);
//
//		Integer vIndex = adicionarMarcador(pContainerMarcadorMapa.icone, vOverlayItem);
//		if (vIndex != null)
//			pContainerMarcadorMapa.setIndex(vIndex);
//
//		irParaPonto(pContainerMarcadorMapa.geoPoint);
//	}
//
//	public void loadContainerMarcadorMapa(ContainerMarcadorMapa pContainerMarcadorMapa) {
//
//		OverlayItem vOverlayItem = new OverlayItem(pContainerMarcadorMapa.geoPoint, pContainerMarcadorMapa.tituloBalao,
//				pContainerMarcadorMapa.textoBalao);
//
//		Integer vIndex = adicionarMarcador(pContainerMarcadorMapa.icone, vOverlayItem);
//		if (vIndex != null)
//			pContainerMarcadorMapa.setIndex(vIndex);
//	}
//
//	public void irParaPonto(GeoPoint p) {
//		this.mapController.animateTo(p);
//		this.mapController.setZoom(16);
//		this.mapView.invalidate();
//	}
//
//	@Override
//	public void finish() {
//
//		if (receiverConnectionServerInformation != null)
//			receiverConnectionServerInformation.unregisterReceiverIfExist();
//		super.finish();
//
//	}
//
//}
