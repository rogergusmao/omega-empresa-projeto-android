package app.omegasoftware.pontoeletronico.common.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.AboutActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.controler.ControlerProgressDialog;
import app.omegasoftware.pontoeletronico.common.dialog.HelperDialog;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.pontoeletronico.PrincipalActivity;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiveBroadcastAutenticacaoInvalida;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiverBroadcastServiceConnectionServerInformation;

public abstract class OmegaRegularActivity extends Activity {

	protected String TAG = "OmegaRegularActivity";
	protected ControlerProgressDialog controlerProgressDialog;
	protected String TABELAS_RELACIONADAS[];

	protected enum TYPE_SYNC {
		INITIAL, END, INITIAL_AND_END, NO_SYNC
	};

	protected TYPE_SYNC typeSync = TYPE_SYNC.NO_SYNC;
	public static final int FORM_ERROR_DIALOG_POST = 76;
	public static final int FORM_ERROR_DIALOG_SERVER_OFFLINE = 887;
	public static final int FORM_ERROR_DIALOG_SERVER_OFFLINE_2 = -7;
	public static final int FORM_ERROR_DIALOG_DESCONECTADO = 75;
	public static final int DIALOG_ERRO_FATAL = 1001;
	public static final int ERROR_DIALOG_EMAIL_INVALIDO = 888;
	public static final int DIALOG_CADASTRO_OK = 771;
	public static final int FORM_DIALOG_ATUALIZACAO_OK = 73;
	public static final int FORM_ERROR_DIALOG_INVALIDO_EMAIL = 777;
	public static final int FORM_ERROR_DIALOG_MISSING_NOME = 889;
	public static final int FORM_ERROR_DIALOG_MISSING_EMAIL = 890;
	public static final int FORM_ERROR_DIALOG_MISSING_SENHA = 891;
//	public static final int FORM_ERROR_DIALOG_INESPERADO = 77;
	public static final int ON_LOAD_DIALOG = -3;
	public static final int SEARCH_NO_RESULTS_DIALOG = -1;
	public static final int FORM_ERROR_DIALOG_INFORMACAO_DIALOG = -4;
	public static final int FORM_DIALOG_INFORMACAO_DIALOG_ENDERECO = -5;
	public static final int INFORMACAO_PESSOA_DUPLICIDADE = -254;
	public static final int FORM_ERROR_DIALOG_TELEFONE_OFFLINE = -6;
	String titulo = null;
	String nomeTag = null;
	ReceiverBroadcastServiceConnectionServerInformation receiverConnectionServerInformation;
	ReceiveBroadcastAutenticacaoInvalida receiverAutenticacaoInvalida;
	public ClearComponentsHandler handlerClearComponents;

	public abstract boolean loadData() throws OmegaDatabaseException, Exception;

	public abstract void initializeComponents() throws OmegaDatabaseException;

	protected boolean foiModificado = false;
	protected OnAtualizaView onAtualizaView = null;

	protected void setOnAtualizaView(OnAtualizaView onAtualizaView) {
		this.onAtualizaView = onAtualizaView;
	}

	public interface OnAtualizaView {
		public abstract void onAtualizaView(Bundle bundle);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		controlerProgressDialog = new ControlerProgressDialog(this);
		handlerClearComponents = new ClearComponentsHandler();
		if (getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_TAG))
			nomeTag = getIntent().getStringExtra(OmegaConfiguration.SEARCH_FIELD_TAG);

		if (getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_TITULO)) {
			int idTitulo = getIntent().getIntExtra(OmegaConfiguration.SEARCH_FIELD_TITULO, -999);
			if (idTitulo != -999) {
				titulo = getResources().getString(idTitulo);
			}
		}

	}

	public void setBancoFoiModificado() {
		foiModificado = true;
	}

	public void setTituloCabecalho(int idString) {
		setTituloCabecalho(this.getString(idString));
	}
	
	public void setTituloCabecalho(String titulo) {
		View viewTitle = findViewById(R.id.tv_title);
		TextView tvTitle = (TextView) viewTitle;
		if (titulo != null && tvTitle != null)
			tvTitle.setText(HelperString.substring(titulo, 16));
	}

	public void inicializaComponentesPadroes() {

		receiverConnectionServerInformation = new ReceiverBroadcastServiceConnectionServerInformation(
				OmegaRegularActivity.this, nomeTag);

		receiverAutenticacaoInvalida = new ReceiveBroadcastAutenticacaoInvalida(OmegaRegularActivity.this);

		if (titulo != null && titulo.length() > 0) {
			View v = findViewById(R.id.tv_title);
			if (v != null) {
				TextView tvTitulo = (TextView) v;
				tvTitulo.setText(titulo);
			}
		}
		View voltar = findViewById(R.id.btn_voltar_desktop);
		if (voltar != null) {
			voltar.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					onBackPressed();

				}
			});
		}
	}

	public static void clearComponent(View child) {

		if (child instanceof EditText) {
			EditText editView = (EditText) child;
			editView.setText("");

		} else if (child instanceof CheckBox) {
			CheckBox checkboxView = (CheckBox) child;
			checkboxView.setChecked(false);
		}

	}


	protected void formatarStyle() {
		ViewGroup vg = (ViewGroup) findViewById(R.id.root);
		HelperFonte.setFontAllView(vg);
	}

	public class ClearComponentsHandler extends Handler {

		ArrayList<View> viewsToClear = new ArrayList<View>();

		public void addLayout(View v) {
			viewsToClear.add(v);
		}

		@Override
		public void handleMessage(Message msg) {
			
			ViewGroup vg = (ViewGroup) findViewById(R.id.root);
			clearComponents(vg);

			for (int i = 0; i < viewsToClear.size(); i++) {
				View v = viewsToClear.get(i);
				if (v != null) {
					if (v instanceof LinearLayout) {
						((LinearLayout) v).removeAllViews();
					} else {
						clearComponent(v);
					}
				}

			}
		}
	}

	public void clearComponents(ViewGroup vg) {

		for (int i = 0; i < vg.getChildCount(); ++i) {
			View child = vg.getChildAt(i);

			if (child instanceof ViewGroup) {

				recursiveClearComponents((ViewGroup) child);
				
			} else if (child != null) {
				clearComponent(child);
			}

		}
	}
	
	
	private void recursiveClearComponents(ViewGroup vg){
		for (int i = 0; i < vg.getChildCount(); ++i) {
			View child = vg.getChildAt(i);

			if (child instanceof ViewGroup) {

				recursiveClearComponents((ViewGroup) child);
				
			} else if (child != null) {
				clearComponent(child);
			}

		}
	}

	public void setTag(String p_tag) {
		this.TAG = p_tag;
	}

	private void setVetorTabelaRelacionada(String pVetorTabelaRelacionada[]) {
		TABELAS_RELACIONADAS = pVetorTabelaRelacionada;
	}

	 public boolean onCreateOptionsMenu(Menu menu) {
		 super.onCreateOptionsMenu(menu);
		
		 MenuInflater inflater = getMenuInflater();
		 inflater.inflate(R.menu.mobile_menu, menu);
		 return true;
	
	 }

	@Override
	public void finish() {
		if (foiModificado) {
			Intent intent = getIntent();
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, true);
			setResult(RESULT_OK, intent);
		}

		super.finish();
	}

	public Dialog getDialog(int id) {
		return null;
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog d = this.getDialog(id);
		if (d != null)
			return d;
		d = HelperDialog.getDialog(this, id);
		return d;
	}

	@Override
	protected void onDestroy() {

		if (receiverConnectionServerInformation != null) {
			receiverConnectionServerInformation.unregisterReceiverIfExist();
			receiverConnectionServerInformation = null;
		}

		if (receiverAutenticacaoInvalida != null) {
			unregisterReceiver(receiverAutenticacaoInvalida);
			receiverAutenticacaoInvalida = null;
		}

		super.onDestroy();
	}

	public void procedureBeforeLoadData() throws OmegaDatabaseException{

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		try {
			
			if (requestCode == OmegaConfiguration.ACTIVITY_ATUALIZA_VIEW) {

				try {
					if (resultCode == RESULT_OK) {
						boolean vIsModificated = true;
						Bundle pParameter = intent.getExtras();

						if (pParameter != null)
							if (pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED))
								vIsModificated = pParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED);
						if (onAtualizaView != null) {
							onAtualizaView.onAtualizaView(pParameter);
						}
						if (vIsModificated) {
							foiModificado = true;

							new CustomDataLoader(OmegaRegularActivity.this).execute();

						}
					}

				} catch (Exception ex) {
					SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
				}
			} else {
				super.onActivityResult(requestCode, resultCode, intent);
			}
		} catch (Exception ex) {
			SingletonLog.openDialogError(this, ex, TIPO.PAGINA);
		}

	}

	/**
	 * Hides the soft keyboard
	 */
	public void hideSoftKeyboard() {
		if (getCurrentFocus() != null) {
			InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
		}
	}

	/**
	 * Shows the soft keyboard
	 */
	public void showSoftKeyboard(View view) {
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		view.requestFocus();
		inputMethodManager.showSoftInput(view, 0);
	}

	public class CustomDataLoader extends AsyncTask<String, Integer, Boolean> {
		Activity activity;
		boolean mostrarCarregando = true;

		public void setTypeSync(TYPE_SYNC ts) {
			typeSync = ts;
		}

		public void setMostrarCarregando(boolean mostrar) {
			mostrarCarregando = mostrar;
		}

		public CustomDataLoader(Activity pActivity) {
			activity = pActivity;
			typeSync = TYPE_SYNC.NO_SYNC;
		}

		public CustomDataLoader(Activity pActivity, String[] pVetorTabelaRelacionada, TYPE_SYNC pTypeSync) {
			activity = pActivity;
			setVetorTabelaRelacionada(pVetorTabelaRelacionada);
			typeSync = pTypeSync;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (mostrarCarregando)
				controlerProgressDialog.createProgressDialog();
		}
		
		Exception e2 = null;
		@Override
		protected Boolean doInBackground(String... params) {
			try {
				procedureBeforeLoadData();
				return loadData();
			} catch(Exception ex){
				this.e2=ex;
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				if(SingletonLog.openDialogError(activity, this.e2, SingletonLog.TIPO.PAGINA)){
					return;
				}	
				if (result) {
					inicializaComponentesPadroes();
					initializeComponents();
					OmegaRegularActivity.this.hideSoftKeyboard();
				} 			
			}
			catch (Exception ex) {
				SingletonLog.openDialogError(activity, ex, TIPO.PAGINA);
			}finally{
				if (mostrarCarregando)
					controlerProgressDialog.dismissProgressDialog();
				
			}

		}

	}

	protected void setVisibilityBotoesToolbar(int visibilidadeEditar, int visibilidadeRemover) {
		try {
			View editButton = findViewById(R.id.editar_topo_button);
			if (editButton != null)
				editButton.setVisibility(visibilidadeEditar);
			;
			View removerButton = findViewById(R.id.remover_topo_button);
			if (removerButton != null)
				removerButton.setVisibility(visibilidadeEditar);
			;
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}

	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent intent = null;


		if(PrincipalActivity.isTablet)
		{
			switch (item.getItemId()) {
//				case R.id.menu_sincronizador_dado:
//					intent = new Intent(this, SynchronizeReceiveActivityMobile.class);
//					break;
				case R.id.menu_sobre:
					intent = new Intent(this, AboutActivityMobile.class);
					break;
				default:
					return super.onOptionsItemSelected(item);
			}
		}
		if(intent != null){
			startActivity(intent);	
		}
		return true;

	}


	protected void requestFocusToolbar(){
		View viewTopo = findViewById(R.id.header);
		
		if(viewTopo != null){
			viewTopo.requestFocus();
			
		}
	}	

	
	
}
