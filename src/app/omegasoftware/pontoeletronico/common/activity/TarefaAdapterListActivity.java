package app.omegasoftware.pontoeletronico.common.activity;

import java.util.ArrayList;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.AboutActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.Adapter.TarefaAdapter;
import app.omegasoftware.pontoeletronico.common.InformationDialog;
import app.omegasoftware.pontoeletronico.common.Adapter.BkpTarefaCustomAdapter;
import app.omegasoftware.pontoeletronico.common.controler.ControlerProgressDialog;
import app.omegasoftware.pontoeletronico.pontoeletronico.PrincipalActivity;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiverBroadcastServiceConnectionServerInformation;

//public abstract class TarefaAdapterListActivity extends OmegaDatabaseAdapterListActivity {

//	public String TAG = "TarefaAdapterListActivity";
//
//
//	private ToggleButton dataOrderByToggleButton;
//	private ToggleButton agruparOrderByToggleButton;
//	ControlerProgressDialog controlerProgressDialog ;
//
//	//Order by mode
//	protected short currentOrderByMode = OmegaConfiguration.DATA_ORDER_BY_MODE;
//
//
//	protected abstract void loadCommomParameters(Bundle pParameters);
//	protected abstract void buildAdapter (boolean pIsAsc);
//
//	protected TarefaAdapter adapter;
//
//	String nomeTag = null;
//	ReceiverBroadcastServiceConnectionServerInformation receiverConnectionServerInformation;
//
//	protected ArrayList<String> listIdPrestadorEnderecoDialog = new ArrayList<String>();
//	protected Typeface customTypeFace;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState){
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.list_tarefa_activity_mobile_layout);
//		if(getIntent().hasExtra(OmegaConfiguration.SEARCH_FIELD_TAG))
//			nomeTag = getIntent().getStringExtra(OmegaConfiguration.SEARCH_FIELD_TAG);
//
//		controlerProgressDialog = new ControlerProgressDialog(this);
//
//		new CustomDataLoader().execute();
//	}
//
//	public void setTag(String p_tag)
//	{
//		this.TAG = p_tag;
//	}
//
//	public void addIdPrestadorEnderecoDialog(String p_idPrestadorEndereco){
//		listIdPrestadorEnderecoDialog.add(p_idPrestadorEndereco);
//	}
//	public void clearPrestadorEnderecoDialog(){
//		listIdPrestadorEnderecoDialog.clear();
//	}
//	public boolean onCreateOptionsMenu(Menu menu) {
//		super.onCreateOptionsMenu(menu);
//
////		MenuInflater inflater = getMenuInflater();
////		inflater.inflate(R.menu.mobile_menu, menu);
//		return true;
//
//	}
//
//	protected void loadParameters()
//	{
//		Bundle vParameters = getIntent().getExtras();
//		if(vParameters != null)
//		{
//			loadCommomParameters(vParameters);
//		}
//	}
//	public void performExecute(){
//		new CustomDataLoader().execute();
//	}
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//
//		Intent intent = null;
//
//
//		if(PrincipalActivity.isTablet)
//		{
//			switch (item.getItemId()) {
//
//
////			case R.id.menu_sincronizador_dado:
////				intent = new Intent(this, SynchronizeReceiveActivityMobile.class);
////				break;
//			case R.id.menu_sobre:
//				intent = new Intent(this, AboutActivityMobile.class);
//				break;
//
//			default:
//				return super.onOptionsItemSelected(item);
//
//			}
//		}
//		if(intent != null){
//			startActivity(intent);
//		}
//		return true;
//
//	}
//
//	@Override
//	public void finish(){
//
//		if(receiverConnectionServerInformation != null)
//			receiverConnectionServerInformation.unregisterReceiverIfExist();
//		super.finish();
//
//	}
//	protected Boolean getSavedDialogInformacao()
//	{
//		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
//		if(vSavedPreferences.contains(nomeTag + "_dialog_informacao"))
//			return vSavedPreferences.getBoolean(nomeTag + "_dialog_informacao", false);
//		else return false;
//	}
//
//	//Save Cidade ID in the preferences
//	protected void saveDialogInformacao()
//	{
//
//		SharedPreferences vSavedPreferences = getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
//		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
//		vEditor.putBoolean(nomeTag + "_dialog_informacao", true);
//		vEditor.commit();
//
//	}
//
//	boolean isInformacaoDialogShow = false;
//	@Override
//	protected Dialog onCreateDialog(int id) {
//
//
//		switch (id) {
//		case OmegaConfiguration.ON_LOAD_DIALOG:
//			return controlerProgressDialog.getDialog();
//		default:
//			break;
//		}
//		Dialog vDialog = new Dialog(this);
//		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//
//		vDialog.setContentView(R.layout.dialog);
//		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
//		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);
//
//		try{
//
//			switch (id) {
//
//			case OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG:
//
//				vTextView.setText(getResources().getString(R.string.dialog_noresult_alert));
//				vOkButton.setOnClickListener(new OnClickListener() {
//					public void onClick(View v) {
//						dismissDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
//						Intent vIntent = getIntent();
//						vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED, true);
//						finish();
//					}
//				});
//				break;
//			case OmegaConfiguration.FORM_ERROR_DIALOG_INFORMACAO_DIALOG:
//				if(isInformacaoDialogShow) return null;
//				else isInformacaoDialogShow = true;
//				InformationDialog vInformation = new InformationDialog(this, vDialog, nomeTag + "_dialog_informacao", OmegaConfiguration.FORM_ERROR_DIALOG_INFORMACAO_DIALOG);
//				return vInformation.getDialogInformation(new int[]{R.string.informacao_uso_list_detalhes,
//						R.string.informacao_uso_list_gerenciamento});
//
//
//			default:
//
//				return null;
//			}
//		} catch(Exception ex){
//			return null;
//		}
//
//
//		return vDialog;
//
//	}
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//		try{
//			switch (requestCode) {
//			case OmegaConfiguration.ACTIVITY_FORM_EDIT:
//				switch (resultCode) {
//
//				default:
//
//					boolean vIsModificated = true;
//					if(intent != null){
//						Bundle pParameter = intent.getExtras();
//
//						if(pParameter != null)
//							if(pParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED))
//								vIsModificated = pParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_MODIFICATED);
//						if(vIsModificated)
//							performExecute();
//
//						break;
//					}
//				}
//
//				break;
//
//			default:
//				break;
//			}
//		}catch(Exception ex){
//			Log.e( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());
//		}
//
//	}
//
//
//
//	public class CustomDataLoader extends AsyncTask<String, Integer, Boolean>
//	{
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			controlerProgressDialog.createProgressDialog();
//		}
//
//		@Override
//		protected Boolean doInBackground(String... params) {
//			try{
//				return loadData();
//			}
//			catch(Exception e)
//			{
//				Log.e(OmegaConfiguration.SEARCH_LOG_CATEGORY, e.getMessage());
//			}
//			return false;
//		}
//
//		@Override
//		protected void onPostExecute(Boolean result) {
//			super.onPostExecute(result);
//
//			if(result)
//			{
//				initializeComponents();
//			}
//
//
//			controlerProgressDialog.dismissProgressDialog();
//
//			showDialogNoResultIfNecessary();
//
//		}
//	}
//
//	private void showDialogNoResultIfNecessary()
//	{
//		if(adapter!= null)
//			if(adapter.getCount() <= 0)
//			{
//
//					showDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
//
//			}
//	}
//
//
//	public class AdapterChangeListener implements OnCheckedChangeListener
//	{
//
//		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
//			boolean vIsAsc = dataOrderByToggleButton.isChecked();
//			buildAdapter(vIsAsc);
//		}
//	}
//
//	private void setOrderByMode(short p_orderByMode){
//		this.currentOrderByMode = p_orderByMode;
//	}
//
//	protected void setOrderByAgrupada(short p_orderByMode){
//		this.currentOrderByMode = p_orderByMode;
//	}
//
//	public void initializeComponents() {
//
//		this.customTypeFace = Typeface.createFromAsset(this.getAssets(),"trebucbd.ttf");
//		if(nomeTag != null)
//			receiverConnectionServerInformation = new ReceiverBroadcastServiceConnectionServerInformation(this, nomeTag);
//
//		dataOrderByToggleButton = (ToggleButton) findViewById(R.id.data_togglebutton);
//		dataOrderByToggleButton.setOnCheckedChangeListener(new DataListener());
//		dataOrderByToggleButton.setTypeface(this.customTypeFace);
//
//		agruparOrderByToggleButton = (ToggleButton) findViewById(R.id.agrupar_togglebutton);
//		agruparOrderByToggleButton.setOnCheckedChangeListener(new AgruparListener());
//		agruparOrderByToggleButton.setTypeface(this.customTypeFace);
//
//		this.dataOrderByToggleButton.setChecked(true);
////		//Sets default search mode
////		switch (this.currentOrderByMode) {
////		case OmegaConfiguration.ORDER_BY_MODE_CRESCENTE:
////
////
////			break;
////		case OmegaConfiguration.ORDER_BY_MODE_AGRUPADO_OFF:
////			this.dataOrderByToggleButton.setChecked(true);
////			this.dataOrderByToggleButton.setChecked(false);
////			break;
////		}
//
//		setListAdapter(adapter);
//	}
//
//
//
//	public boolean loadData() {
//		loadParameters();
//		buildAdapter();
//
//		return (adapter!=null?true:false);
//	}
//
//	private void buildAdapter ()
//	{
//		buildAdapter(true);
//	}
//
//
//	private class DataListener implements OnCheckedChangeListener
//	{
//
//		public DataListener(){
//
//
//		}
//
//		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
//			if(isChecked){
//				setOrderByMode(OmegaConfiguration.ORDER_BY_MODE_CRESCENTE);
//				ListUpdate(true);
//			} else{
//				setOrderByMode(OmegaConfiguration.ORDER_BY_MODE_DECRESCENTE);
//				ListUpdate(false);
//			}
//
//		}
//	}
//
//
//
//	private class AgruparListener implements OnCheckedChangeListener
//	{
////		Activity activity;
//		public AgruparListener(){
////			activity = pActivity;
//
//		}
//
//		public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
//			if(isChecked){
//				setOrderByMode(OmegaConfiguration.ORDER_BY_MODE_AGRUPADO_ON);
//				ListUpdate(false);
//			} else{
//				setOrderByMode(OmegaConfiguration.ORDER_BY_MODE_AGRUPADO_OFF);
//				ListUpdate(false);
//			}
//
//		}
//	}
//
//
//
//	private void ListUpdate(boolean pAsc)
//	{
//		if(adapter.getCount() == 0)
//		{
//
//
//				showDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
//
//
//		} else {
//
////			adapter.SortList(dataOrderByToggleButton.isChecked(), agruparOrderByToggleButton.isChecked());
//			adapter.notifyDataSetChanged();
//			showDialog(OmegaConfiguration.FORM_ERROR_DIALOG_INFORMACAO_DIALOG);
//		}
//	}
//
//}
