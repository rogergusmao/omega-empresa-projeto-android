package app.omegasoftware.pontoeletronico.common.authenticator;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.common.dialog.HelperDialog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.http.HelperHttp;
import app.omegasoftware.pontoeletronico.webservice.ServicosPontoEletronico;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;

public class AlterarSenhaUsuarioActivityMobile extends OmegaRegularActivity {

	// Constants
	public static final String TAG = "AlterarSenhaUsuarioActivityMobile";

	private final int FORM_ERROR_DIALOG_SENHA_REPETIDA_INCORRETA = 5;
	private final int FORM_ERROR_DIALOG_SENHA_INCORRETA = 775;
	protected final int FORM_ERROR_DIALOG_POST = 76;

	protected final int FORM_ERROR_DIALOG_CADASTRO_OK = 71;
	private final int FORM_ERROR_DIALOG_MISSING_SENHA = 6;
	private final int FORM_ERROR_DIALOG_MISSING_SENHA_ANTIGA = 15;
	private final int FORM_ERROR_DIALOG_MISSING_NOME_USUARIO = 13;
	private final int FORM_ERROR_DIALOG_MISSING_CATEGORIA_PERMISSAO_OR_IS_ADM = 20;
	private final int FORM_ERROR_DIALOG_MISSING_EMAIL = 7;
	protected final int FORM_ERROR_DIALOG_DESCONECTADO = 75;

	// Search button
	private Button cadastrarButton;

	// EditText
	private EditText senhaAntigaEditText;
	private EditText senhaEditText;
	private EditText repitaSenhaEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.alterar_senha_usuario_activity_mobile_layout);

		new CustomDataLoader(this).execute();

	}

	@Override
	public boolean loadData() {
		return true;
	}

	@Override
	public void initializeComponents() {

		if (!HelperHttp.isConnected(this)) {
			showDialog(FORM_ERROR_DIALOG_DESCONECTADO);
			return;
		}

		this.setTituloCabecalho(R.string.alterar_senha);
		this.senhaEditText = (EditText) findViewById(R.id.form_usuario_senha_edittext);
		this.repitaSenhaEditText = (EditText) findViewById(R.id.form_usuario_repita_senha_edittext);
		this.senhaAntigaEditText = (EditText) findViewById(R.id.form_usuario_senha_antiga_edittext);

		// Attach search button to its listener
		this.cadastrarButton = (Button) findViewById(R.id.cadastrar_button);
		this.cadastrarButton.setOnClickListener(new AlterarSenhaButtonListener());
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			Dialog dl = this.getErrorDialog(id);

			return dl;
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			HelperDialog.factoryDialogErroFatal(this);
			return null;
		}
	}

	public Dialog getErrorDialog(int dialogType) {

		Dialog vDialog = new Dialog(this);

		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {

		case FORM_ERROR_DIALOG_SENHA_INCORRETA:
			vTextView.setText(getResources().getString(R.string.error_form_senha_incorreta));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_SENHA_INCORRETA);
				}
			});
			break;
		case FORM_ERROR_DIALOG_SENHA_REPETIDA_INCORRETA:
			vTextView.setText(getResources().getString(R.string.error_form_senha_repetida));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_SENHA_REPETIDA_INCORRETA);
				}
			});
			break;
		case FORM_ERROR_DIALOG_CADASTRO_OK:
			vTextView.setText(getResources().getString(R.string.cadastro_ok));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_CADASTRO_OK);
				}
			});
			break;
		case FORM_ERROR_DIALOG_POST:
			vTextView.setText(getResources().getString(R.string.error_post));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_POST);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_CATEGORIA_PERMISSAO_OR_IS_ADM:
			vTextView.setText(getResources().getString(R.string.error_missing_categoria_permissao_or_is_adm));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_CATEGORIA_PERMISSAO_OR_IS_ADM);
				}
			});
			break;

		case FORM_ERROR_DIALOG_MISSING_SENHA_ANTIGA:
			vTextView.setText(getResources().getString(R.string.error_missing_senha_antiga));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_SENHA_ANTIGA);
				}
			});
			break;

		case FORM_ERROR_DIALOG_MISSING_EMAIL:
			vTextView.setText(getResources().getString(R.string.error_missing_email));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_EMAIL);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_SENHA:
			vTextView.setText(getResources().getString(R.string.error_missing_senha));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_SENHA);
				}
			});
			break;
		case FORM_ERROR_DIALOG_MISSING_NOME_USUARIO:
			vTextView.setText(getResources().getString(R.string.error_missing_nome_usuario));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_NOME_USUARIO);
				}
			});
			break;

		default:
			return HelperDialog.getDialog(this, dialogType);

		}

		return vDialog;
	}

	// ---------------------------------------------------------------
	// ----------------- Methods related to search action ------------
	// ---------------------------------------------------------------
	// Handles search button click
	private void onAlterarSenhaButtonClicked() {
		if (!HelperHttp.isConnected(this)) {
			showDialog(FORM_ERROR_DIALOG_DESCONECTADO);
			return;
		}
		@SuppressWarnings("unused")
		boolean vValidade = true;

		String vStrNovaSenha = "";
		@SuppressWarnings("unused")
		String vNovaSenha = "";
		String vSenhaAntiga = "";
		if (!this.senhaAntigaEditText.getText().toString().equals("")) {
			vSenhaAntiga = this.senhaAntigaEditText.getText().toString();
			if (vSenhaAntiga.compareTo(OmegaSecurity.getSenha()) != 0) {
				showDialog(FORM_ERROR_DIALOG_SENHA_INCORRETA);
				return;
			}
			if (!(vSenhaAntiga.length() > 0)) {

				vValidade = false;
				showDialog(FORM_ERROR_DIALOG_MISSING_SENHA_ANTIGA);
				return;
			} else {
				vSenhaAntiga = this.senhaAntigaEditText.getText().toString();
			}
		} else {
			showDialog(FORM_ERROR_DIALOG_MISSING_SENHA_ANTIGA);
			return;
		}
		if (!this.senhaEditText.getText().toString().equals("")) {
			vStrNovaSenha = this.senhaEditText.getText().toString();

			if (!(vStrNovaSenha.length() > 0)) {

				vValidade = false;
				showDialog(FORM_ERROR_DIALOG_MISSING_SENHA);
				return;
			} else {
				vNovaSenha = this.senhaEditText.getText().toString();
			}
		} else {
			showDialog(FORM_ERROR_DIALOG_MISSING_SENHA);
			return;
		}

		if (!this.repitaSenhaEditText.getText().toString().equals("")) {
			String vSenhaRepetida = this.repitaSenhaEditText.getText().toString();
			if (vStrNovaSenha.compareTo(vSenhaRepetida) != 0) {

				showDialog(FORM_ERROR_DIALOG_SENHA_REPETIDA_INCORRETA);
				vValidade = false;
				return;
			}
		} else {
			showDialog(FORM_ERROR_DIALOG_SENHA_REPETIDA_INCORRETA);
			return;
		}

		DialogInterface.OnClickListener dialogConfirmarSaida = new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					new AlterarSenhaTask(AlterarSenhaUsuarioActivityMobile.this).execute();
					break;

				case DialogInterface.BUTTON_NEGATIVE:

					break;
				}
			}
		};

		FactoryAlertDialog.showAlertDialog(this, getString(R.string.confirmar_alterar_senha), dialogConfirmarSaida);

		// Intent intent = new Intent( this,
		// FormConfirmacaoActivityMobile.class);
		// intent.putExtra(OmegaConfiguration.SEARCH_FIELD_MESSAGE,
		// getResources().getString(R.string.confirmar_alterar_senha));
		// startActivityForResult(intent,
		// OmegaConfiguration.STATE_FORM_CONFIRMACAO );
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		if (requestCode == OmegaConfiguration.STATE_FORM_CONFIRMACAO) {
			if (resultCode == RESULT_OK) {
				Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO, false);
				if (vConfirmacao) {
					new AlterarSenhaTask(this).execute();
				}
			}

		}
	}

	public class AlterarSenhaTask extends AsyncTask<String, Integer, Boolean> {
		Mensagem msg = null;
		Integer dialogId = null;
		Activity activity;
		
		Exception __ex2 = null;

		public AlterarSenhaTask(Activity pActivity) {
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			Database db = null;
			try {

				String vNovaSenhaRepetida = "";
				String vNovaSenha = "";
				String vSenhaAntiga = "";
				boolean vValidade = true;
				if (!senhaAntigaEditText.getText().toString().equals("")) {
					vSenhaAntiga = senhaAntigaEditText.getText().toString();

					if (!(vSenhaAntiga.length() > 0)) {

						vValidade = false;
						dialogId = FORM_ERROR_DIALOG_MISSING_SENHA_ANTIGA;
						return false;
					} else {
						vSenhaAntiga = senhaAntigaEditText.getText().toString();
					}
				}
				if (!senhaEditText.getText().toString().equals("")) {
					vNovaSenha = senhaEditText.getText().toString();

					if (!(vNovaSenha.length() > 0)) {

						vValidade = false;
						dialogId = FORM_ERROR_DIALOG_MISSING_SENHA;
						return false;
					} else {
						vNovaSenha = senhaEditText.getText().toString();
					}
				}

				if (!repitaSenhaEditText.getText().toString().equals("")) {
					vNovaSenhaRepetida = repitaSenhaEditText.getText().toString();
					if (vNovaSenhaRepetida.compareTo(vNovaSenhaRepetida) != 0) {

						dialogId = FORM_ERROR_DIALOG_SENHA_REPETIDA_INCORRETA;
						vValidade = false;
						return false;
					}
				}
				if (vValidade) {
					ServicosPontoEletronico servicoPE = new ServicosPontoEletronico(activity);
					Mensagem msg = servicoPE.alterarSenhaApp(vSenhaAntiga, vNovaSenha, vNovaSenhaRepetida);

					if (msg == null) {
						dialogId = FORM_ERROR_DIALOG_POST;
					} else if (msg.ok()) {
						OmegaSecurity.setSenha(vNovaSenha);
						this.msg = msg;
						this.dialogId = null;
						handlerClearComponents.sendEmptyMessage(0);

						db = new DatabasePontoEletronico(activity);
						EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(db);
						vObjUsuario.setAttrValue(EXTDAOUsuario.ID, OmegaSecurity.getIdUsuario());
						Table vTuplaUsuario = vObjUsuario.getFirstTupla();
						if (vTuplaUsuario != null) {
							vTuplaUsuario.setAttrValue(EXTDAOUsuario.SENHA, vNovaSenha);
							vTuplaUsuario.update(false);
						}

						OmegaSecurity.setSenha(vNovaSenha);

					} else {
						this.msg = msg;
						dialogId = null;
					}
				}
				return true;
			}catch (Exception e) {
				__ex2 = e;
			} finally {
				if (db != null)
					db.close();
			}

			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {

				if (SingletonLog.openDialogError(activity, this.__ex2, SingletonLog.TIPO.PAGINA)) {
					return;
				} else {
					if (dialogId != null)
						showDialog(dialogId);
					else if (this.msg != null) {
						FactoryAlertDialog.showDialog(activity, msg);
					}	
				}				
			} catch (Exception ex) {
				SingletonLog.openDialogError(activity, ex, SingletonLog.TIPO.PAGINA);
			} finally {
				controlerProgressDialog.dismissProgressDialog();
			}
		}
	}

	ClearComponentsHandler handlerClearComponents = new ClearComponentsHandler();

	public class ClearComponentsHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			clearComponents();
		}
	}

	private void clearComponents() {

		if (!this.repitaSenhaEditText.getText().toString().equals("")) {
			this.repitaSenhaEditText.setText("");
		}

		if (!this.senhaEditText.getText().toString().equals("")) {
			this.senhaEditText.setText("");
		}

		if (!this.senhaAntigaEditText.getText().toString().equals("")) {
			this.senhaAntigaEditText.setText("");
		}

	}

	private class AlterarSenhaButtonListener implements OnClickListener {
		public void onClick(View v) {
			onAlterarSenhaButtonClicked();
		}
	}
}
