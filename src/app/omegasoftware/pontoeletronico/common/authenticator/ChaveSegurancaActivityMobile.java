package app.omegasoftware.pontoeletronico.common.authenticator;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;


public class ChaveSegurancaActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "ChaveSegurancaActivityMobile";


	public static int NUMERO_TENTATIVA = 1;
	private final int FORM_ERROR_CHAVE_INVALIDA = 5;
	protected final int FORM_CHAVE_VALIDA = 6;
	protected final int FORM_ERROR_MISSING_CHAVE = 7;
	
	//Search button
	private Button cadastrarButton;

	//EditText
	private EditText chaveInvalidaEditText;
	
	private Typeface customTypeFace;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chave_seguranca_activity_mobile_layout);

		this.customTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(),"trebucbd.ttf");

		new CustomDataLoader(this).execute();

	}

	@Override
	public boolean loadData() {

		return true;

	}


	public void formatarStyle()
	{
		//		Se Cadastro	
		((TextView) findViewById(R.id.chave_seguranca_textview)).setTypeface(this.customTypeFace);
		
		//Botuo cadastrar:
		((Button) findViewById(R.id.cadastrar_button)).setTypeface(this.customTypeFace);

	}

	@Override
	public void initializeComponents() {


		((Button) findViewById(R.id.cadastrar_button)).setTypeface(this.customTypeFace);

		this.formatarStyle();

		this.chaveInvalidaEditText = (EditText) findViewById(R.id.chave_seguranca_edittext);
		
		//Attach search button to its listener
		this.cadastrarButton = (Button) findViewById(R.id.cadastrar_button);
		this.cadastrarButton.setOnClickListener(new ConfirmarButtonListener());

		this.formatarStyle();
	}	


	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;

		switch (id) {

		default:
			vDialog = this.getErrorDialog(id);
			break;
		}
		return vDialog;
	}	
	
	
	private String getStrTentativa(){
		String vToken = getResources().getString(R.string.chave_seguranca_incorreta);
		vToken += " " + String.valueOf(NUMERO_TENTATIVA) ;
		return vToken;
	}
	
	public static boolean checkIfChaveSegurancaIsSaved(Context pContext){
		if(OmegaConfiguration.DEBUGGING)
			return true;
		String vChave = getSavedChaveSeguranca(pContext);
		if(vChave != null && 
				vChave.length() > 0 && 
				vChave.compareTo(OmegaSecurity.OMEGA_KEY) == 0)
			return true;
		else return false;
	}
	
	public static boolean checkIfChaveSegurancaIsBlocked(Context pContext){
		if(OmegaConfiguration.DEBUGGING)
			return false;
		String vChave = getSavedChaveSeguranca(pContext);
		if(vChave != null && 
				vChave.length() > 0 && 
				vChave.compareTo(OmegaSecurity.OMEGA_KEY_BLOCKED) == 0)
			return true;
		else return false;
	}
	
	
	public Dialog getErrorDialog(int dialogType)
	{

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {

		
		case FORM_ERROR_CHAVE_INVALIDA:
			vTextView.setText(getStrTentativa());
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_CHAVE_INVALIDA);
				}
			});
			break;
		case FORM_CHAVE_VALIDA:
			vTextView.setText(getResources().getString(R.string.chave_seguranca_validada));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_CHAVE_VALIDA);
				}
			});
			break;
		case FORM_ERROR_MISSING_CHAVE:
			vTextView.setText(getResources().getString(R.string.chave_seguranca_vazia));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_MISSING_CHAVE);
				}
			});
			break;	
			
		
		default:
			return super.onCreateDialog(dialogType);

		}

		return vDialog;
	}


	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	private void onButtonClicked()
	{
		
		

		String vStrChave = "";
		
		if(!this.chaveInvalidaEditText.getText().toString().equals(""))
		{
			vStrChave = this.chaveInvalidaEditText.getText().toString();
			if(vStrChave.compareTo(OmegaSecurity.OMEGA_KEY) == 0){
				saveChaveSeguranca(this, vStrChave);
				Intent vNewintent = new Intent();
				setResult(RESULT_OK, vNewintent);
				finish();
			} else {
				
				if(NUMERO_TENTATIVA >= 3){
					saveChaveSeguranca(this, OmegaSecurity.OMEGA_KEY_BLOCKED);
					
					Intent vNewintent = new Intent();
					setResult(RESULT_CANCELED, vNewintent);
					finish();
				} else{
					showDialog(FORM_ERROR_CHAVE_INVALIDA);
					NUMERO_TENTATIVA ++;
				}
			}
		} else {
			showDialog(FORM_ERROR_MISSING_CHAVE);
			return;
		}


	}
	
	
	
	public static String getSavedChaveSeguranca(Context pContext)
	{
		SharedPreferences vSavedPreferences = pContext.getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		if(vSavedPreferences.contains(OmegaConfiguration.SAVED_CHAVE_SEGURANCA))
			return vSavedPreferences.getString(OmegaConfiguration.SAVED_CHAVE_SEGURANCA, OmegaConfiguration.UNEXISTENT_ID_IN_DB);
		else return "";
	}

	
	//Save Cidade ID in the preferences
	public static void saveChaveSeguranca(Context pContext, String pChaveSeguranca)
	{

		SharedPreferences vSavedPreferences = pContext.getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putString(OmegaConfiguration.SAVED_CHAVE_SEGURANCA, pChaveSeguranca);
		vEditor.commit();

	}

	private class ConfirmarButtonListener implements OnClickListener
	{
		public void onClick(View v) {
			onButtonClicked();
		}
	}
}


