package app.omegasoftware.pontoeletronico.common.authenticator;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;

public class DesbloquearRelogioDePonto extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "DesbloquearRelogioDePonto";

	
	private Button cadastrarButton;

	
	private EditText senhaEditText;	

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.desbloquear_relogio_de_ponto_mobile);
	
		new CustomDataLoader(this).execute();

	}


	@Override
	public boolean loadData() {
		return true;
	}

	@Override
	public void initializeComponents() {	
		try{
			String desc= getString(R.string.senha_usuario_logado);
			
			((TextView) findViewById(R.id.descricao_textview)).setText(desc);
			
			View vPassword = findViewById(R.id.password_edit);
			senhaEditText = (EditText)vPassword; 
			
			
			//Attach search button to its listener
			this.cadastrarButton = (Button) findViewById(R.id.enviar_button);
			this.cadastrarButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					
					String strSenha = senhaEditText.getText().toString();
					if(OmegaSecurity.getSenha().compareTo(strSenha) ==0){

						setResult(RESULT_OK);
						finish();
					} else {
						FactoryAlertDialog.showDialog(
								DesbloquearRelogioDePonto.this, 
								getString(R.string.senha_incorreta));
						
					}
					
					
				}
			});
	
			this.formatarStyle();
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}
	}
	
}


