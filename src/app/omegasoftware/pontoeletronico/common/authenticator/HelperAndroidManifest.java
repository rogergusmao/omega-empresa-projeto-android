package app.omegasoftware.pontoeletronico.common.authenticator;

import android.content.Context;
import android.content.pm.PackageManager;

public class HelperAndroidManifest {
	public static boolean checaPermissao(Context c, String permissao){
		
        int check = c.checkCallingOrSelfPermission(permissao);
        return (check == PackageManager.PERMISSION_GRANTED);
	    
	}
	
	public static boolean[] getPermissionsGranted(Context context, String[] permissions){
	    boolean[] permissionsGranted = new boolean[permissions.length];
	    for(int i=0;i<permissions.length;i++){
	        int check = context.checkCallingOrSelfPermission(permissions[i]);
	        permissionsGranted[i] = (check == PackageManager.PERMISSION_GRANTED);
	    }
	    return permissionsGranted;
	}
}
