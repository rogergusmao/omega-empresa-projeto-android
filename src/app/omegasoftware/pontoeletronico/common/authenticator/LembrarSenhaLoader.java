package app.omegasoftware.pontoeletronico.common.authenticator;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.AsyncTask;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.controler.ControlerProgressDialog;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.webservice.ServicosCobranca;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class LembrarSenhaLoader extends AsyncTask<String, Integer, Mensagem> {
	Activity activity;

	ControlerProgressDialog controlerProgressDialog = null;
	Exception e;
	public LembrarSenhaLoader(Activity pActivity, ControlerProgressDialog controlerProgressDialog) {
		activity = pActivity;
		this.controlerProgressDialog = controlerProgressDialog;

	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		controlerProgressDialog.createProgressDialog();
	}

	@Override
	protected Mensagem doInBackground(String... params) {
		try {
			String email = params[0];
			if (email == null || email.length() == 0) {

				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
						activity.getString(R.string.error_dialog_missing_email));

			} else {
				ServicosCobranca servicosCO = new ServicosCobranca(activity);
				return servicosCO.relembrarSenhaPorEmail(email);
			}

		} catch (Exception e) {
			this.e=e;
			return new Mensagem(e);
		}

	}

	@Override
	protected void onPostExecute(Mensagem msg) {
		super.onPostExecute(msg);
		try {
			if(SingletonLog.openDialogError(activity, e, SingletonLog.TIPO.PAGINA)){
				return;
			}
			
			if (msg == null)
				msg = Mensagem.factoryMsgSemInternet(activity);
			else if (msg != null && msg.erro())
				FactoryAlertDialog.showDialog(activity, msg);
			final boolean hasToFinish = msg != null && msg.ok();
			FactoryAlertDialog.showDialogOk(activity, msg.mMensagem, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (hasToFinish)
						activity.finish();

				}
			});
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.LISTA);
		} finally {
			controlerProgressDialog.dismissProgressDialog();
		}
	}

}
