/*
 * Copyright (C) 2010 The Android Open Source Project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package app.omegasoftware.pontoeletronico.common.authenticator;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.CacheSharedPreference;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaAuthenticatorActivity.ContainerAuthentication;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador.STATE_SINCRONIZADOR;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.http.ConfiguracaoHttpPost;
import app.omegasoftware.pontoeletronico.http.HelperHttp;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.http.RetornoJson;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

/**
 * Provides utility methods for communicating with the server.
 */
public class NetworkUtilities {
	//private static final String TAG = "NetworkUtilities";
	public static final String PARAM_USERNAME = "email";
	public static final String PARAM_PASSWORD = "senha";
	public static final String PARAM_CORPORACAO = "corporacao";

	public static final String PARAM_UPDATED = "timestamp";

	public static final String USER_AGENT = "AuthenticationService/1.0";
	public static final int REGISTRATION_TIMEOUT = 20 * 1000; // ms

	

	private static HttpClient mHttpClient;

	/**
	 * Configures the httpClient to connect to the URL provided.
	 */
	public static void maybeCreateHttpClient() {
		if (mHttpClient == null) {
			mHttpClient = new DefaultHttpClient();
			final HttpParams params = mHttpClient.getParams();
			HttpConnectionParams.setConnectionTimeout(params,
					REGISTRATION_TIMEOUT);
			HttpConnectionParams.setSoTimeout(params, REGISTRATION_TIMEOUT);
			ConnManagerParams.setTimeout(params, REGISTRATION_TIMEOUT);
		}
	}
	
	public static RetornoJson LAST_POST_LOGIN = null;
	
	/**
	 * Connects to the Voiper server, authenticates the provided username and
	 * password.
	 *
	 */
	public static ContainerAuthentication authenticate(
			Database db,
			String email, 
			String senha, 
			String corporacao,
			OmegaAuthenticatorActivity pActivity,
			boolean loginAutomatico) {
//		if(HelperHttp.isServerOnline(pActivity)){
			STATE_SINCRONIZADOR state = RotinaSincronizador.getStateSincronizador(pActivity, null);
			boolean carregarPermissoes =
					state!=STATE_SINCRONIZADOR.COMPLETO || 
					EXTDAOUsuario.existeNecessidadeDeCarregarAsPermissoes(db, email, corporacao);
			
			ConfiguracaoHttpPost conf = new ConfiguracaoHttpPost();
			conf.setCookie =true;

		CacheSharedPreference cache = new CacheSharedPreference(pActivity);

			RetornoJson vResult = null;
			if(!loginAutomatico ||
					//se for login automatico sera ralizado apenas uma vez por dia e no WIFI
					(
							cache.expirou("UltimoCheckLoginOnline", 24 * 60)
							&& HelperHttp.isConnectedWifi(pActivity)
					)) {
				vResult = HelperHttpPost.getRetornoJsonPost(
						pActivity,
						OmegaConfiguration.URL_REQUEST_LOGIN_MOBILE(),
						new String[]{PARAM_USERNAME, PARAM_PASSWORD, PARAM_CORPORACAO, "carregarPermissao", "carregarInformacoesUsuario"},
						new String[]{email, senha, corporacao, String.valueOf(carregarPermissoes), "true"},
						conf);
				cache.update("UltimoCheckLoginOnline", 24 * 60);
			}
			JSONObject obj;
			
			try {
				if(vResult == null) 
					return pActivity.onAuthenticationResult(
							db,
							null, 
							corporacao,
							email,
							senha,
							null);
				obj = vResult.json;
				int codRetorno = obj.getInt("mCodRetorno");
				
				if (codRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId() ){
					LAST_POST_LOGIN = vResult;
				} 

				return pActivity.onAuthenticationResult(
						db,
						vResult,
						corporacao,
						email,
						senha,
						null);
			} catch (Exception e) {
				return pActivity.onAuthenticationResult(
						db,
						null,
						corporacao,
						email,
						senha,
						e);
				
			} 
//		}
//		else{
//			return pActivity.onAuthenticationResult(db, null, false, false, false);
//			
		// }
	}
}
