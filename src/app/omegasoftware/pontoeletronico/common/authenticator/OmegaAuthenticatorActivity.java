package app.omegasoftware.pontoeletronico.common.authenticator;

import org.json.JSONArray;
import org.json.JSONObject;

import android.accounts.AccountAuthenticatorActivity;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.TrocarCorporacaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormUsuarioECorporacaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.select.SelectCorporacaoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.ContainerPrograma;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.InformationDialogSistemaWeb;
import app.omegasoftware.pontoeletronico.common.TextView.TableAutoCompleteTextView;
import app.omegasoftware.pontoeletronico.common.TextWatcher.GrupoTextWatcher;
import app.omegasoftware.pontoeletronico.common.TextWatcher.MaskLowerCaseTextWatcher;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity.STATE_LOAD_USER_HTTP_POST;
import app.omegasoftware.pontoeletronico.common.controler.ControlerProgressDialog;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.common.dialog.HelperDialog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Tuple;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCorporacao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador.STATE_SINCRONIZADOR;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho;
import app.omegasoftware.pontoeletronico.http.RetornoJson;
import app.omegasoftware.pontoeletronico.listener.BrowserOpenerListener;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference;
import app.omegasoftware.pontoeletronico.primitivetype.HelperBoolean;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiverBroadcastServiceConnectionServerInformation;
import app.omegasoftware.pontoeletronico.webservice.ServicosCobranca;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemToken;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class OmegaAuthenticatorActivity extends AccountAuthenticatorActivity {
	public static final String PARAM_CONFIRMCREDENTIALS = "confirmCredentials";
	public static final String PARAM_PASSWORD = "password";
	public static final String PARAM_USERNAME = "username";
	public static final String PARAM_AUTHTOKEN_TYPE = "authtokenType";

	private static final String TAG = "OmegaAuthenticatorActivity";
	public static final short SEARCH_NO_RESULTS_DIALOG = 1;
	public static final int DIALOG_INFORMACAO_SISTEMA_ANDROID = 38;
	public static final int ERROR_DIALOG_MISSING_CORPORACAO = 2;
	public static final int ERROR_DIALOG_MISSING_EMAIL = 3;
	public static final int ERROR_DIALOG_MISSING_SENHA = 4;
	public static final int ERROR_DIALOG_DESCONECTADO_PRIMEIRO_LOGIN = 98;
	public static final int ERROR_DIALOG_SERVER_OFFLINE_PRIMEIRO_LOGIN = 97;
	public static final int FORM_ERROR_DIALOG_FALHA_AO_ENVIAR_O_EMAIL = 23;
	public static final int FORM_ERROR_DIALOG_EMAIL_INEXISTENTE = 22;
	public static final int FORM_ERROR_DIALOG_CORPORACAO_INEXISTENTE = 25;

	public static final int FORM_DIALOG_EMAIL_ENVIADO = 21;
	public static final int ERROR_DIALOG_DESCONECTADO = 99;
	public static final int ERROR_DIALOG_SERVER_OFFLINE = 100;
	public static final int ERROR_DIALOG_ERROR_LOAD_USUARIO_FROM_SERVER = 101;
	public static final int ERROR_DIALOG_ERROR_LOAD_USUARIO_NOT_SAVED_AND_SERVER_OFFLINE = 103;
	public static final int ERROR_DIALOG_ERROR_LOAD_USUARIO_NOT_SAVED_AND_OFFLINE = 102;
	public static final int ERROR_DIALOG_ERROR_INESPERADO = 110;
	public static final int ERROR_DIALOG_SENHA_OU_USUARIO_INVALIDO = 112;
	public static final int FORM_ERROR_DIALOG_POST = 113;
	public static final int ERROR_DIALOG_USUARIO_SEM_CATEGORIA_PERMISSAO_ANDROID = 114;
	public static final int DIALOG_VERSAO_HOMOLOGACAO = 114548;
	public static final int FORM_ERROR_DIALOG_EM_MANUTENCAO = 24541;

	final static boolean DIALOG_PRIMEIRO_LOGIN_ATIVA = false;
	AuthenticatorButtonListener authenticatorButtonListener;
	OmegaSecurity.STATE_LOAD_USER_HTTP_POST statePost = null;

	String idUsuarioTextView = "";
	String idCorporacaoTextView = "";
	ControlerProgressDialog controlerProgressDialog;
	TableAutoCompleteTextView usuarioContainerAutoComplete = null;
	TableAutoCompleteTextView corporacaoContainerAutoComplete = null;
	TextView corporacaoTextView = null;
	private AutoCompleteTextView corporacaoAutoCompletEditText;
	private AutoCompleteTextView usuarioAutoCompletEditText;
	Button corporacaoJaCarregadaButton;

	private EditText senhaEditText;

	private Button mResetarButton;
	private Button mLoginButton;
	private Button mEnviarSenhaEmailButton;

	ReceiverBroadcastServiceConnectionServerInformation receiverConnectionServerInformation;
	private CheckBox salvarSenhaCheckbox;
	private MostarSenhaViewHandler mostrarSenhaHandler = new MostarSenhaViewHandler();
	EmptySenhaHandler emptySenhaHandler = new EmptySenhaHandler();

	CheckBox mostrarSenhaCheckbox;

	
	//O caso abaixo su ocorre para usuurios que possuem mais de um grupo
	
	//Quando o usuario possui mais de uma corporacao
	//abre o dialog para que ele selecione uma das, porem se ele errar a senha
	//ele retorna par aa tela de login novamente
	//e teria que selecionarrnovamente a corporacao
	String ultimaCorporacaoSelecionada = null;
	String ultimoEmailTentativaLogin = null;
	
	public class EmptySenhaHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			try {

				senhaEditText.setText("");
			} catch (Exception e) {

				SingletonLog.insereErro(e, TIPO.PAGINA);
			}
		}
	}

	public class MostarSenhaViewHandler extends Handler {

		public MostarSenhaViewHandler() {
		}

		@Override
		public void handleMessage(Message msg) {
			try {
				int intChecked = msg.what;
				boolean checked = HelperBoolean.parserInt(intChecked);
				if (!checked)
					senhaEditText.setInputType(OmegaConfiguration.TYPE_TEXT_VARIATION_PASSWORD);
				else {
					senhaEditText.setInputType(OmegaConfiguration.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
					String cript = senhaEditText.getText().toString();
					senhaEditText.setText("");
					senhaEditText.setText(cript);
				}

			} catch (Exception e) {

				SingletonLog.insereErro(e, TIPO.PAGINA);
			}
		}
	}

	private class MostrarSenhaListener implements OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			try {
				Message msg = new Message();
				msg.what = HelperInteger.parserBoolean(isChecked);
				mostrarSenhaHandler.sendMessage(msg);
			} catch (Exception ex) {
				SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
			}

		}

	}

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.omega_authentication_activity_mobile_layout);
		controlerProgressDialog = new ControlerProgressDialog(this);
		refreshEstado(true);
		new Loader().execute();
	}

	public class Loader extends AsyncTask<String, Integer, String[]> {
		Exception __ex;
		

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();

		}

		@Override
		protected String[] doInBackground(String... params) {
			Database db = null;
			try {
				db = new DatabasePontoEletronico(OmegaAuthenticatorActivity.this);

				String[] userLoaded = OmegaSecurity.loadComponentesSalvos(db);
				return userLoaded;
			} catch (Exception ex) {
				__ex = ex;
			} finally {
				if (db != null)
					db.close();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String[] userLoaded) {
			super.onPostExecute(userLoaded);

			try {
				
				if (SingletonLog.openDialogError(OmegaAuthenticatorActivity.this, __ex, SingletonLog.TIPO.PAGINA)) {

					return;
				} 
				
				initializeComponents();					

				if (getIntent().getBooleanExtra(OmegaConfiguration.PARAM_AUTO_CONNECT_IF_SAVED, true)
						&& userLoaded != null) {
					new CheckFieldLoginLoader(userLoaded[0], userLoaded[1], userLoaded[2], true).execute();
				}
			} catch (Exception ex) {
				SingletonLog.openDialogError(OmegaAuthenticatorActivity.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				if(userLoaded == null)
					refreshEstado(false);
				controlerProgressDialog.dismissProgressDialog();
			}

		}

	}

	@Override
	public void finish() {
		try {
			if (receiverConnectionServerInformation != null) {
				receiverConnectionServerInformation.unregisterReceiverIfExist();
				receiverConnectionServerInformation = null;
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
		}
		super.finish();
	}

	@Override
	public void onBackPressed() {
		try {
			Intent vNewintent = new Intent();
			vNewintent.putExtra(OmegaConfiguration.SEARCH_FIELD_ON_BACK_PRESSED, true);

			salvarCache();
			setResult(RESULT_CANCELED, vNewintent);
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
		}
		super.onBackPressed();
	}

	private void salvarCache() {
		PontoEletronicoSharedPreference.saveValor(this, PontoEletronicoSharedPreference.TIPO_STRING.CACHE_EMAIL,
				usuarioAutoCompletEditText.getText().toString());
		PontoEletronicoSharedPreference.saveValor(this, PontoEletronicoSharedPreference.TIPO_STRING.CACHE_SENHA,
				senhaEditText.getText().toString());
	}

	private void limparCache() {
		PontoEletronicoSharedPreference.saveValor(this, PontoEletronicoSharedPreference.TIPO_STRING.CACHE_EMAIL, null);
		PontoEletronicoSharedPreference.saveValor(this, PontoEletronicoSharedPreference.TIPO_STRING.CACHE_SENHA, null);

	}

	private void loadCache() {
		senhaEditText.setText(PontoEletronicoSharedPreference.getValor(this,
				PontoEletronicoSharedPreference.TIPO_STRING.CACHE_SENHA));
		usuarioAutoCompletEditText.setText(PontoEletronicoSharedPreference.getValor(this,
				PontoEletronicoSharedPreference.TIPO_STRING.CACHE_EMAIL));
	}

	static boolean formularioEditado = false;

	private void initializeComponents() throws OmegaDatabaseException {

		Database db = null;
		try {
			db = new DatabasePontoEletronico(this);
			receiverConnectionServerInformation = new ReceiverBroadcastServiceConnectionServerInformation(this, TAG);

			this.mLoginButton = (Button) findViewById(R.id.login_button);
			this.authenticatorButtonListener = new AuthenticatorButtonListener();
			this.mLoginButton.setOnClickListener(authenticatorButtonListener);

			CustomServiceListener listener = new CustomServiceListener();

			this.mResetarButton = (Button) findViewById(R.id.resetar_button);
			this.mResetarButton.setOnClickListener(listener);

			this.mEnviarSenhaEmailButton = (Button) findViewById(R.id.lembrar_senha_button);
			this.mEnviarSenhaEmailButton.setOnClickListener(listener);

			this.salvarSenhaCheckbox = (CheckBox) findViewById(R.id.continuar_conectado_checkbox);

			this.salvarSenhaCheckbox.setChecked(true);
			Button vCadastroUsuario = (Button) findViewById(R.id.cadastrar_button);
			vCadastroUsuario.setOnClickListener(listener);
			// Activity listener
			TextView tvTitle = (TextView) findViewById(R.id.tv_title);
			String appName = getResources().getString(R.string.app_name);
			tvTitle.setText(appName);

			EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(db);
			if (db.isTableCreated(EXTDAOUsuario.NAME)) {
				this.usuarioContainerAutoComplete = new TableAutoCompleteTextView(vObjUsuario, EXTDAOUsuario.EMAIL,
						false, true);
			} else {
				this.usuarioContainerAutoComplete = new TableAutoCompleteTextView(vObjUsuario, EXTDAOUsuario.EMAIL,
						false, false);
			}

			ArrayAdapter<String> usuarioAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item,
					usuarioContainerAutoComplete.getVetorStrId());
			this.usuarioAutoCompletEditText = (AutoCompleteTextView) findViewById(R.id.usuario_autocompletetextview);
			usuarioAutoCompletEditText.setAdapter(usuarioAdapter);
			new MaskLowerCaseTextWatcher(usuarioAutoCompletEditText);

			senhaEditText = (EditText) findViewById(R.id.password_edit);

			this.corporacaoAutoCompletEditText = (AutoCompleteTextView) findViewById(
					R.id.corporacao_autocompletetextview);
			this.corporacaoTextView = (TextView) findViewById(R.id.corporacao_textview);
			corporacaoJaCarregadaButton = (Button) findViewById(R.id.corporacao_carregada_button);

			EXTDAOCorporacao vObjCorporacao = new EXTDAOCorporacao(db);

			if (db.isTableCreated(EXTDAOCorporacao.NAME)) {
				this.corporacaoContainerAutoComplete = new TableAutoCompleteTextView(this, vObjCorporacao,
						corporacaoAutoCompletEditText, true);
			} else {
				this.corporacaoContainerAutoComplete = new TableAutoCompleteTextView(this, vObjCorporacao,
						corporacaoAutoCompletEditText, false);
			}

			new GrupoTextWatcher(corporacaoAutoCompletEditText);
			if (ContainerPrograma.isProgramaDaEmpresa()) {
				corporacaoAutoCompletEditText.setVisibility(View.GONE);
				corporacaoAutoCompletEditText.setText(ContainerPrograma.getNomeGrupoDaEmpresa());

			}

			STATE_SINCRONIZADOR state = RotinaSincronizador.getStateSincronizador(this, null);
			if (state != STATE_SINCRONIZADOR.COMPLETO) {
				this.corporacaoAutoCompletEditText.setVisibility(View.VISIBLE);
				this.corporacaoJaCarregadaButton.setVisibility(View.GONE);
			} else {
				this.corporacaoAutoCompletEditText.setVisibility(View.GONE);
				this.corporacaoJaCarregadaButton.setVisibility(View.VISIBLE);
				Tuple<Integer, String> regCorporacao = SincronizadorEspelho.getCorporacaoSincronizada(this);

				this.corporacaoJaCarregadaButton.setText(regCorporacao.y);
				this.corporacaoJaCarregadaButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent(OmegaAuthenticatorActivity.this,
								TrocarCorporacaoActivityMobile.class);
						String strNovaCorporacao = OmegaAuthenticatorActivity.this.corporacaoAutoCompletEditText
								.getText().toString();

						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CORPORACAO, strNovaCorporacao);
						startActivityForResult(intent, OmegaConfiguration.STATE_TROCAR_CORPORACAO);
					}
				});
			}

			mostrarSenhaCheckbox = (CheckBox) findViewById(R.id.mostrar_senha_checkbox);
			mostrarSenhaCheckbox.setOnCheckedChangeListener(new MostrarSenhaListener());

			Button vSisteSistemaAndroidBrowser = (Button) findViewById(R.id.website_sistema_android_button);
			vSisteSistemaAndroidBrowser.setOnClickListener(
					new BrowserOpenerListener(getResources().getString(R.string.sistema_cobranca_site_url)));

			loadCache();

			if (OmegaConfiguration.DEBUGGING && OmegaConfiguration.DEBUGGING_EMAIL_USUARIO != null
					&& OmegaConfiguration.DEBUGGING_EMAIL_USUARIO.length() > 0) {
				usuarioAutoCompletEditText.setText(OmegaConfiguration.DEBUGGING_EMAIL_USUARIO);
				senhaEditText.setText(OmegaConfiguration.DEBUGGING_SENHA);
				corporacaoAutoCompletEditText.setText(OmegaConfiguration.DEBUGGING_CORPORACAO);

			}
			if (OmegaConfiguration.BASE_URL_PONTO_ELETRONICO().contains("homologacao")) {
				showDialog(DIALOG_VERSAO_HOMOLOGACAO);
			}


		} catch (Exception ex) {
			SingletonLog.openDialogError(OmegaAuthenticatorActivity.this, ex, SingletonLog.TIPO.PAGINA);
		} finally {
			if (db != null)
				db.close();

		}

	}

	public  void refreshEstado(boolean carregando){
		if(carregando){
			findViewById(R.id.ll_carregando).setVisibility(View.VISIBLE);
			findViewById(R.id.sv_login).setVisibility(View.GONE);
		} else {
			findViewById(R.id.ll_carregando).setVisibility(View.GONE);
			findViewById(R.id.sv_login).setVisibility(View.VISIBLE);
		}

	}

	public class CheckFieldLoginLoader extends AsyncTask<String, Integer, Boolean> {

		ContainerAuthentication container;
		String corporacao = null;
		String email = null;
		String senha = null;
		Exception ex = null;
		boolean loginAutomatico;
		
		public CheckFieldLoginLoader(String corporacao, String email, String senha, boolean loginAutomatico) {
			this.corporacao = corporacao;
			this.email = email;
			this.senha = senha;
			this.loginAutomatico=loginAutomatico;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Database db = null;
			try {
				db = new DatabasePontoEletronico(OmegaAuthenticatorActivity.this);

				container = NetworkUtilities.authenticate(
						db
						, this.email
						, this.senha
						, this.corporacao
						,OmegaAuthenticatorActivity.this
						, loginAutomatico
				);
				return container.isAuthenticated();

			} catch (Exception e) {

				SingletonLog.insereErro(e, TIPO.PAGINA);
				this.ex=e;
			} finally {
				if (db != null)
					db.close();
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);

			
			try {
				if(SingletonLog.openDialogError(OmegaAuthenticatorActivity.this, ex, SingletonLog.TIPO.PAGINA)){
					refreshEstado(false);
					return;
				}
				if (result) {
					finishConfirmCredentials(true, corporacao, email, senha);
				} else {
					refreshEstado(false);
					if (container != null && container.hasDialog()) {
						int idDialog = container.getIdDialog();
						showDialog(idDialog);
					} else if (container.retornoJsonInvalido != null) {
						if (container.retornoJsonInvalido.json == null) {
							showDialog(HelperDialog.FORM_ERROR_DIALOG_DESCONECTADO);
						} else {
							FactoryAlertDialog.showDialog(OmegaAuthenticatorActivity.this,
									container.retornoJsonInvalido.json);
						}
					}
				}
			} catch (Exception ex) {
				SingletonLog.openDialogError(OmegaAuthenticatorActivity.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
		}

	}

	public class CorporacoesLoader extends AsyncTask<String, Integer, InterfaceMensagem> {

		String strJson = null;
		Exception ex = null;
		public CorporacoesLoader() {

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected InterfaceMensagem doInBackground(String... params) {
			strJson = null;
			Database db = null;
			try {
				db = new DatabasePontoEletronico(OmegaAuthenticatorActivity.this);
				InterfaceMensagem msg = checkObrigatoryField(db);
				if (msg == null || msg.ok()) {
					String email = usuarioAutoCompletEditText.getText().toString();
					String senha = senhaEditText.getText().toString();
					
					if(ultimoEmailTentativaLogin != null 
						&& ultimoEmailTentativaLogin.compareTo(email) == 0){
						
						new CheckFieldLoginLoader(ultimaCorporacaoSelecionada, email, senha, false).execute();
						
						return new MensagemToken(
								PROTOCOLO_SISTEMA.TIPO.CORPORACAO_SELECIONADA_AUTOMATICAMENTE,
								null, 
								ultimaCorporacaoSelecionada);
					}
					
					ServicosCobranca sc = new ServicosCobranca(OmegaAuthenticatorActivity.this);
					strJson = sc.getCorporacoesDoCliente(email, senha);
					if (strJson == null) {
						Tuple<Integer, String> registro = SincronizadorEspelho
								.getCorporacaoSincronizada(OmegaAuthenticatorActivity.this);
						if (registro != null) {
							// valida se o banco atualmente armazenado, se
							// contem o usuario
							if (OmegaSecurity.loadUserFromDatabase(email, registro.y, senha,
									OmegaAuthenticatorActivity.this)) {
								new CheckFieldLoginLoader(registro.y, email, senha, false).execute();
								return new MensagemToken(PROTOCOLO_SISTEMA.TIPO.CORPORACAO_SELECIONADA_AUTOMATICAMENTE,
										null, registro.y);
							}
						}

						return Mensagem.factoryMsgSemInternet(OmegaAuthenticatorActivity.this);
					}
					JSONObject jsonObj = new JSONObject(strJson);
					msg = Mensagem.factory(jsonObj);

					if (msg.ok()) {
						JSONArray corporacoes = jsonObj.getJSONArray("mObj");
						if (corporacoes.length() == 1) {
							JSONObject obj = corporacoes.getJSONObject(0);
							String id = obj.getString("id");
							String nome = obj.getString("nome");

							Tuple<Integer, String> registro = SincronizadorEspelho
									.getCorporacaoSincronizada(OmegaAuthenticatorActivity.this);
							// Se o registro atual for da mesma corporacao que
							// estu arquivada
							if (registro == null || registro.x == HelperInteger.parserInt(id)) {
								new CheckFieldLoginLoader(nome, email, senha, false).execute();
								return new MensagemToken(PROTOCOLO_SISTEMA.TIPO.CORPORACAO_SELECIONADA_AUTOMATICAMENTE,
										null, nome);
							} else {
								return new MensagemToken(PROTOCOLO_SISTEMA.TIPO.TROCAR_CORPORACAO, null, nome);
							}
						}
					}

					return msg;
				} else {
					return msg;
				}
			} catch (Exception e) {

				this.ex = e;
				return new Mensagem(e);
			} finally {
				if (db != null)
					db.close();
			}

		}

		@Override
		protected void onPostExecute(InterfaceMensagem msg) {
			super.onPostExecute(msg);
			boolean dismissDialog= true;
			try {

				if(SingletonLog.openDialogError(OmegaAuthenticatorActivity.this, ex, SingletonLog.TIPO.PAGINA)){
					return;
				}
				else if (msg != null) {
					if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.CORPORACAO_SELECIONADA_AUTOMATICAMENTE.getId()) {
						MensagemToken mt = (MensagemToken) msg;
						corporacaoAutoCompletEditText.setText(mt.mValor);
						dismissDialog= false;
					} else if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.TROCAR_CORPORACAO.getId()) {
						dismissDialog= false;
						Intent intent = new Intent(OmegaAuthenticatorActivity.this,
								TrocarCorporacaoActivityMobile.class);
						MensagemToken mt = (MensagemToken) msg;
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CORPORACAO, mt.mValor);

						startActivityForResult(intent, OmegaConfiguration.STATE_TROCAR_CORPORACAO);

					} else if (msg.ok()) {
						dismissDialog= false;
						Intent intent = new Intent(OmegaAuthenticatorActivity.this,
								SelectCorporacaoActivityMobile.class);
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_JSON, strJson);
						startActivityForResult(intent, OmegaConfiguration.STATE_SELECIONAR_CORPORACAO);
					} else {

						FactoryAlertDialog.showDialog(OmegaAuthenticatorActivity.this, msg);
					}
				}
			} catch (Exception ex) {
				SingletonLog.openDialogError(OmegaAuthenticatorActivity.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				if(dismissDialog)
					controlerProgressDialog.dismissProgressDialog();
			}
		}
	}

	private class AuthenticatorButtonListener implements OnClickListener {
		
		public AuthenticatorButtonListener() {
			
		}

		public void onClick(View view) {
			try{
				
				new CorporacoesLoader().execute();	
			}catch(Exception ex){
				SingletonLog.openDialogError(
					OmegaAuthenticatorActivity.this, 
					ex, 
					SingletonLog.TIPO.PAGINA);
			}
		}
	}

	public InterfaceMensagem checkObrigatoryField(Database db) {

		String vPassword = senhaEditText.getText().toString();
		if (vPassword == null || vPassword.length() == 0) {

			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_SEM_SER_EXCECAO,
					db.getContext().getString(R.string.error_dialog_missing_senha));

		}

		String vUserName = usuarioAutoCompletEditText.getText().toString();
		if (vUserName == null || vUserName.length() == 0) {
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_SEM_SER_EXCECAO,
					db.getContext().getString(R.string.error_dialog_missing_email));
		}
		return null;
	}

	/**
	 * Called when response is received from the server for confirm credentials
	 * request. See onAuthenticationResult(). Sets the
	 * AccountAuthenticatorResult which is sent back to the caller.
	 * 
	 * @param
	 *             .
	 * @throws OmegaDatabaseException
	 */

	@SuppressWarnings("unused")
	protected void finishConfirmCredentials(boolean result, String corporacao, String email, String senha)
			throws OmegaDatabaseException {
		if (result) {
			Database db = new DatabasePontoEletronico(this);
			persisteSenha(db, email, senha);
			if (salvarSenhaCheckbox.isChecked()) {
				OmegaSecurity.salvarLoginAutomatico(this, corporacao, email, senha);
			} else {
				OmegaSecurity.apagarLoginAutomatico(this);
			}

			limparCache();
		}

		if (statePost == STATE_LOAD_USER_HTTP_POST.RESETAR
				|| statePost == STATE_LOAD_USER_HTTP_POST.PRIMEIRO_LOGIN_DA_CORPORACAO) {

			if ((DIALOG_PRIMEIRO_LOGIN_ATIVA && statePost == STATE_LOAD_USER_HTTP_POST.PRIMEIRO_LOGIN_DA_CORPORACAO)
					|| statePost == STATE_LOAD_USER_HTTP_POST.RESETAR) {
				DialogInterface.OnClickListener dialogPrimeiroLoginGrupo = new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case DialogInterface.BUTTON_POSITIVE:
							Intent vNewintent = new Intent();
							if (salvarSenhaCheckbox.isChecked())
								vNewintent.putExtra(OmegaConfiguration.SEARCH_FIELD_GRAVA_SENHA, true);
							setAccountAuthenticatorResult(vNewintent.getExtras());
							setResult(RESULT_OK, vNewintent);
							OmegaAuthenticatorActivity.this.finish();

							break;

						case DialogInterface.BUTTON_NEGATIVE:

							break;
						}
					}
				};
				String titulo = "";
				if (statePost == STATE_LOAD_USER_HTTP_POST.RESETAR)
					titulo = getString(R.string.resetar_banco);
				else if (statePost == STATE_LOAD_USER_HTTP_POST.PRIMEIRO_LOGIN_DA_CORPORACAO)
					titulo = getString(R.string.primeiro_login_grupo);

				FactoryAlertDialog.showAlertDialog(this, titulo, dialogPrimeiroLoginGrupo);
			} else if (statePost == STATE_LOAD_USER_HTTP_POST.PRIMEIRO_LOGIN_DA_CORPORACAO) {
				myFinishActivity();
			}
		} else {
			myFinishActivity();
		}

	}

	private void myFinishActivity() {
		Intent vNewintent = new Intent();
		if (salvarSenhaCheckbox.isChecked())
			vNewintent.putExtra(OmegaConfiguration.SEARCH_FIELD_GRAVA_SENHA, true);
		setAccountAuthenticatorResult(vNewintent.getExtras());
		setResult(RESULT_OK, vNewintent);
		OmegaAuthenticatorActivity.this.finish();
	}

	public class ContainerAuthentication {

		private boolean isAuthenticated;
		private Integer idDialog;
		RetornoJson retornoJsonInvalido;

		public ContainerAuthentication(boolean pIsAuthenticated) {

			isAuthenticated = pIsAuthenticated;
		}

		public ContainerAuthentication(boolean pIsAuthenticated, Integer pIdDialog) {
			idDialog = pIdDialog;
			isAuthenticated = pIsAuthenticated;
		}

		public ContainerAuthentication(boolean pIsAuthenticated, RetornoJson retornoJsonInvalido) {
			this.retornoJsonInvalido = retornoJsonInvalido;
			isAuthenticated = pIsAuthenticated;
		}

		public Integer getIdDialog() {
			return idDialog;
		}

		public boolean hasDialog() {
			if (idDialog == null) {
				return false;
			} else
				return true;
		}

		public boolean isAuthenticated() {
			return isAuthenticated;
		}
	}

	class DadosLogin {
		String corporacao;
		String email;
		String senha;
	}

	private ContainerAuthentication loadUserOffline(Database db, String corporacao, String email, String senha,
			RetornoJson retornoJson) {

		if (OmegaSecurity.loadUserFromDatabase(email, corporacao, senha, this)) {

			OmegaSecurity.salvarLoginAutomatico(this, corporacao, email, senha);

			// controlerProgressDialog.dismissProgressDialog();
			return new ContainerAuthentication(true);
			// return true;
		} else {
			if (retornoJson != null)
				return new ContainerAuthentication(false, retornoJson);
			else
				return new ContainerAuthentication(false, ERROR_DIALOG_ERROR_LOAD_USUARIO_NOT_SAVED_AND_OFFLINE);
		}
	}

	/**
	 * Called when the authentication process completes (see attemptLogin()).
	 */
	public ContainerAuthentication onAuthenticationResult(Database db, RetornoJson retornoJson, String corporacao,
			String email, String senha, Exception e) {
		try {

			if (retornoJson == null) {
				return loadUserOffline(db, corporacao, email, senha, retornoJson);
			} else {
				JSONObject obj = retornoJson.json;
				int codRetorno = obj.getInt("mCodRetorno");

				if (codRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {
					statePost = OmegaSecurity.procedureLoadUserJson(db.getContext(), email, senha, retornoJson, false);
					if (statePost == OmegaSecurity.STATE_LOAD_USER_HTTP_POST.PRIMEIRO_LOGIN_DA_CORPORACAO
							|| statePost == OmegaSecurity.STATE_LOAD_USER_HTTP_POST.OK
							|| statePost == OmegaSecurity.STATE_LOAD_USER_HTTP_POST.RESETAR) {

						OmegaSecurity.salvarLoginAutomatico(this, corporacao, email, senha);

						return new ContainerAuthentication(true);
					} else if (statePost == OmegaSecurity.STATE_LOAD_USER_HTTP_POST.USUARIO_SEM_CATEGORIA_DE_PERMISSAO)
						return new ContainerAuthentication(false, ERROR_DIALOG_USUARIO_SEM_CATEGORIA_PERMISSAO_ANDROID);
					else {
						return new ContainerAuthentication(false, ERROR_DIALOG_ERROR_LOAD_USUARIO_FROM_SERVER);
					}
				} else if (codRetorno == PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO.getId()) {
					return new ContainerAuthentication(false, retornoJson);
				} else {
					return loadUserOffline(db, corporacao, email, senha, retornoJson);
				}
			}
		} catch (Exception e2) {
			SingletonLog.insereErro(e2, TIPO.PAGINA);
		}

		return new ContainerAuthentication(false, FORM_ERROR_DIALOG_POST);
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog dialog = null;
		try {
			dialog = new Dialog(this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			dialog.setContentView(R.layout.dialog);

			TextView vTextView = (TextView) dialog.findViewById(R.id.tv_dialog);
			Button vOkButton = (Button) dialog.findViewById(R.id.dialog_ok_button);

			switch (id) {

			case DIALOG_INFORMACAO_SISTEMA_ANDROID:

				InformationDialogSistemaWeb vInformation = new InformationDialogSistemaWeb(this, dialog,
						TAG + "_dialog_website_sistema_android", DIALOG_INFORMACAO_SISTEMA_ANDROID);
				return vInformation.getDialogInformation(new int[] { R.string.informacao_uso_web_site_android });
			case DIALOG_VERSAO_HOMOLOGACAO:
				vTextView.setText(getResources().getString(R.string.versao_de_homologacao));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(DIALOG_VERSAO_HOMOLOGACAO);
					}
				});
				break;
			case FORM_ERROR_DIALOG_EM_MANUTENCAO:
				vTextView.setText(getResources().getString(R.string.em_manutencao));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_EM_MANUTENCAO);
					}
				});
				break;
			case OmegaConfiguration.ON_LOAD_DIALOG:
				return controlerProgressDialog.getDialog();

			case FORM_ERROR_DIALOG_EMAIL_INEXISTENTE:
				vTextView.setText(getResources().getString(R.string.login_activity_email_inexistente));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_EMAIL_INEXISTENTE);
					}
				});
				break;
			case FORM_ERROR_DIALOG_CORPORACAO_INEXISTENTE:
				vTextView.setText(getResources().getString(R.string.corporacao_inexistente));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_CORPORACAO_INEXISTENTE);
					}
				});
				break;
			case FORM_ERROR_DIALOG_FALHA_AO_ENVIAR_O_EMAIL:
				vTextView.setText(getResources().getString(R.string.error_falha_envio_email));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_FALHA_AO_ENVIAR_O_EMAIL);
					}
				});
				break;
			case FORM_DIALOG_EMAIL_ENVIADO:
				vTextView.setText(getResources().getString(R.string.login_activity_email_enviado));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_DIALOG_EMAIL_ENVIADO);
					}
				});
				break;
			case ERROR_DIALOG_SENHA_OU_USUARIO_INVALIDO:
				vTextView.setText(getResources().getString(R.string.error_senha_ou_usuario_invalido));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(ERROR_DIALOG_SENHA_OU_USUARIO_INVALIDO);
					}
				});
				break;
			case ERROR_DIALOG_MISSING_CORPORACAO:
				vTextView.setText(getResources().getString(R.string.error_missing_corporacao));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(ERROR_DIALOG_MISSING_CORPORACAO);
					}
				});
				break;
			case FORM_ERROR_DIALOG_POST:
				vTextView.setText(getResources().getString(R.string.error_post));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(FORM_ERROR_DIALOG_POST);
					}
				});
				break;
			case ERROR_DIALOG_ERROR_INESPERADO:
				vTextView.setText(getResources().getString(R.string.error_inesperado));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(ERROR_DIALOG_ERROR_INESPERADO);
					}
				});
				break;
			case ERROR_DIALOG_ERROR_LOAD_USUARIO_NOT_SAVED_AND_OFFLINE:
				vTextView.setText(getResources().getString(R.string.error_offline_usuario_not_saved));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(ERROR_DIALOG_ERROR_LOAD_USUARIO_NOT_SAVED_AND_OFFLINE);
					}
				});
				break;
			case ERROR_DIALOG_ERROR_LOAD_USUARIO_NOT_SAVED_AND_SERVER_OFFLINE:
				vTextView.setText(getResources().getString(R.string.error_server_offline_usuario_not_saved));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(ERROR_DIALOG_ERROR_LOAD_USUARIO_NOT_SAVED_AND_SERVER_OFFLINE);
					}
				});
				break;
			case ERROR_DIALOG_DESCONECTADO_PRIMEIRO_LOGIN:
				vTextView.setText(getResources().getString(R.string.error_offline_first_login));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(ERROR_DIALOG_DESCONECTADO_PRIMEIRO_LOGIN);
					}
				});
				break;
			case ERROR_DIALOG_SERVER_OFFLINE_PRIMEIRO_LOGIN:
				vTextView.setText(getResources().getString(R.string.error_server_offline_first_login));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(ERROR_DIALOG_SERVER_OFFLINE_PRIMEIRO_LOGIN);
					}
				});
				break;
			case ERROR_DIALOG_SERVER_OFFLINE:
				vTextView.setText(getResources().getString(R.string.error_servidor_offline));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(ERROR_DIALOG_SERVER_OFFLINE);
					}
				});
				break;
			case ERROR_DIALOG_DESCONECTADO:
				vTextView.setText(getResources().getString(R.string.is_desconectado));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(ERROR_DIALOG_DESCONECTADO);
					}
				});
				break;

			case ERROR_DIALOG_MISSING_EMAIL:
				vTextView.setText(getResources().getString(R.string.error_missing_email));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(ERROR_DIALOG_MISSING_EMAIL);
					}
				});
				break;
			case ERROR_DIALOG_MISSING_SENHA:
				vTextView.setText(getResources().getString(R.string.error_missing_senha));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(ERROR_DIALOG_MISSING_SENHA);
					}
				});
				break;
			case ERROR_DIALOG_ERROR_LOAD_USUARIO_FROM_SERVER:
				vTextView.setText(getResources().getString(R.string.error_load_usuario_from_server));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(ERROR_DIALOG_ERROR_LOAD_USUARIO_FROM_SERVER);
					}
				});
				break;
			case ERROR_DIALOG_USUARIO_SEM_CATEGORIA_PERMISSAO_ANDROID:
				vTextView.setText(getResources().getString(R.string.usuario_sem_categoria_permissao_associado));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(ERROR_DIALOG_USUARIO_SEM_CATEGORIA_PERMISSAO_ANDROID);
					}
				});
				break;
			default:
				return HelperDialog.getDialog(this, id);
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			return null;
		}
		return dialog;

	}

	private class LembrarSenhaLoader extends AsyncTask<String, Integer, Boolean> {
		
		Activity activity;
		Integer dialogId = null;
		Mensagem msg = null;
		Exception ex = null;
		
		public LembrarSenhaLoader(Activity pActivity) {
			activity = pActivity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			controlerProgressDialog.createProgressDialog();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				String vUsuarioEmail = usuarioAutoCompletEditText.getText().toString();
				if (vUsuarioEmail.length() == 0) {
					dialogId = ERROR_DIALOG_MISSING_EMAIL;
					return false;
				} else {
					ServicosCobranca servicosCO = new ServicosCobranca(activity);
					Mensagem msg = servicosCO.relembrarSenhaPorEmail(vUsuarioEmail);

					if (msg == null) {
						dialogId = FORM_ERROR_DIALOG_POST;
						return false;
					} else if (msg.ok()) {
						dialogId = FORM_DIALOG_EMAIL_ENVIADO;
						return true;
					} else if (!msg.erro()) {

						this.msg = msg;
						dialogId = null;
						return false;
					} else {
						dialogId = ERROR_DIALOG_ERROR_INESPERADO;
						return false;
					}
				}

			} catch (Exception e) {
				ex=e;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try{
				if(SingletonLog.openDialogError(OmegaAuthenticatorActivity.this, ex, SingletonLog.TIPO.PAGINA)){
					return;
				}
				if (dialogId != null)
					showDialog(dialogId);
				else if (msg != null) {
					FactoryAlertDialog.showDialog(activity, msg);
				}
			} catch (Exception ex) {
				SingletonLog.openDialogError(OmegaAuthenticatorActivity.this, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}
		}

	}

	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		try {
			switch (requestCode) {
			case OmegaConfiguration.STATE_SELECIONAR_CORPORACAO:
				OmegaAuthenticatorActivity.this.controlerProgressDialog.dismissProgressDialog();
				if (resultCode == RESULT_OK) {
					String corporacao = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_CORPORACAO);
					String email = usuarioAutoCompletEditText.getText().toString();
					if (corporacao != null && corporacao.length() > 0) {
						corporacaoAutoCompletEditText.setText(corporacao);
						ultimaCorporacaoSelecionada = corporacao;
						ultimoEmailTentativaLogin = email;
						
						new CheckFieldLoginLoader(
								corporacao, 
								email,
								senhaEditText.getText().toString(),
								false
							).execute();
					}
				}
				break;
			case OmegaConfiguration.STATE_FORM_CONFIRMACAO_LEMBRAR_SENHA:
				if (resultCode == RESULT_OK) {
					Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO, false);
					if (vConfirmacao) {
						new LembrarSenhaLoader(this).execute();
					}
				}
				break;
			case OmegaConfiguration.STATE_FORM_CADASTRO_USUARIO_E_CORPORACAO:
				if (resultCode == RESULT_OK) {
					String email = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_EMAIL);
					String senha = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_SENHA);
					String corporacao = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_CORPORACAO);
					usuarioAutoCompletEditText.setText(email);
					senhaEditText.setText(senha);
					corporacaoAutoCompletEditText.setText(corporacao);
					authenticatorButtonListener.onClick(null);

				}
				break;
			case OmegaConfiguration.STATE_TROCAR_CORPORACAO:
				if (resultCode == RESULT_OK) {

					String corporacao = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_NOME_CORPORACAO);
					new CheckFieldLoginLoader(
						corporacao
						, usuarioAutoCompletEditText.getText().toString()
						, senhaEditText.getText().toString()
						, false
					).execute();
				}
				break;
			case OmegaConfiguration.STATE_FORM_CONFIRMACAO_PRIMEIRO_LOGIN:
				if (resultCode == RESULT_OK) {
					Boolean vConfirmacao = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_IS_CONFIRMADO, false);
					if (vConfirmacao) {
						Intent vNewintent = new Intent();
						setAccountAuthenticatorResult(vNewintent.getExtras());
						setResult(RESULT_OK, vNewintent);
						finish();
					}
				}

			default:
				break;
			}

		} catch (Exception ex) {
			SingletonLog.openDialogError(this, ex, SingletonLog.TIPO.PAGINA);
		}

	}

	private class CustomServiceListener implements OnClickListener {

		public CustomServiceListener() {

		}

		public void onClick(View v) {
			try {
				switch (v.getId()) {

				case R.id.lembrar_senha_button:

					Intent intent = new Intent(OmegaAuthenticatorActivity.this, RelembrarSenhaActivityMobile.class);
					intent.putExtra(OmegaConfiguration.SEARCH_FIELD_EMAIL,
							usuarioAutoCompletEditText.getText().toString());
					startActivityForResult(intent, OmegaConfiguration.STATE_LEMBRAR_SENHA);

					break;
				case R.id.cadastrar_button:
					Intent vintent = new Intent(getApplicationContext(), FormUsuarioECorporacaoActivityMobile.class);
					startActivityForResult(vintent, OmegaConfiguration.STATE_FORM_CADASTRO_USUARIO_E_CORPORACAO);

					break;

				default:
					break;
				}

			} catch (Exception ex) {
				SingletonLog.openDialogError(OmegaAuthenticatorActivity.this, ex, SingletonLog.TIPO.PAGINA);
			}

		}

	}

	private void persisteSenha(Database db, String pEmail, String pSenha) {
		if (db.isDatabaseCreated()) {
			EXTDAOUsuario objUsuario = new EXTDAOUsuario(db);
			objUsuario.setAttrValue(EXTDAOUsuario.EMAIL, pEmail);
			EXTDAOUsuario regUsuario = (EXTDAOUsuario) objUsuario.getFirstTupla();
			if (regUsuario != null) {
				// sempre teremos que salvar a senha para conseguir enviar os
				// dados durante a operauuo de reset.
				regUsuario.setAttrValue(EXTDAOUsuario.SENHA, pSenha);
				regUsuario.update(false);

				if (this.salvarSenhaCheckbox.isChecked()) {

				} else {
					emptySenhaHandler.sendEmptyMessage(0);
				}
			}
		}
	}
}
