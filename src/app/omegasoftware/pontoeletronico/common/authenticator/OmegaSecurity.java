package app.omegasoftware.pontoeletronico.common.authenticator;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.util.HelperJson;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.Tuple;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCorporacao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPermissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador.STATE_SINCRONIZADOR;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioCategoriaPermissao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioCorporacao;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho;
import app.omegasoftware.pontoeletronico.http.ConfiguracaoHttpPost;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.http.RetornoJson;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_BOOLEAN;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_STRING;
import app.omegasoftware.pontoeletronico.primitivetype.HelperBoolean;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class OmegaSecurity {

	private static boolean IS_ADM = false;
	
	private static String ID_USUARIO = "";
	private static String ID_VEICULO_USUARIO = "";
	private static String ID_CORPORACAO = "";
	private static String ID_CATEGORIA_PERMISSAO = "";

	private static HashSet<Integer> idPermissoes = new HashSet<Integer>();
	//private static boolean autenticado = false;
	private static String CORPORACAO = "";
	private static String EMAIL = "";

	public static String setCorporacaoSincronizadaTemporariamente(Context context){
		Tuple<Integer, String> x = SincronizadorEspelho.getCorporacaoSincronizada(context);
		if(x==null)
			return null;
		ID_CORPORACAO=x.x.toString();
		CORPORACAO=x.y;
		return CORPORACAO;
	}
	
	public static void clearCorporacaoSincronizadaTemporariamente(){
		
		ID_CORPORACAO=null;
		CORPORACAO=null;
		
	}
	
	private static String SENHA = "";

	public static String OMEGA_KEY = "123";
	public static String OMEGA_KEY_BLOCKED = "BLOCKED";
	
	static CookieStore cookieStore = null;
	static CookieStore cookieStoreLogin = null;
	static Object lockCookieSeguranca = new Object();
	
	public static final String CHAVE_SESSION = "SS_CHAVE";
	public static final String CHAVE_AUTO_LOGIN = "SS_AL";
     //30 dias
     static final int TEMPO_LIMITE_AUTOLOGIN_MINUTOS = 43200;
     //1 dia
     static final int TEMPO_LIMITE_SESSAO_MINUTOS = 1440;


	public static String formataDomain(String url){
		String prefixo = "http://";
		int index= url.indexOf(prefixo);
		String domain = null;
		if(index >= 0){

			int indexFinal = url.indexOf("/", index + prefixo.length());
			if(indexFinal >= 0){
				domain = url.substring(index + prefixo.length() , indexFinal );
			} else {
				domain = url.substring(index + prefixo.length() );
			}
		} else {
			int indexFinal = url.indexOf("/");
			if(indexFinal >= 0){
				domain = url.substring(0, indexFinal );
			} else {
				domain = url.substring(0);
			}
		}
		return domain;
	}
	public static void initCookie(Context pContext, String idSeguranca, String autoLogin){

		if(cookieStore != null){
			cookieStore.clear();
		}
		Date data = new Date();
		data.setYear(data.getYear() + 1);
		cookieStore = new BasicCookieStore();
		cookieStoreLogin= new BasicCookieStore();
		//cookie.setDomain("your domain");

		String[] dominios = new String[]{
				OmegaConfiguration.URL_SINCRONIZADOR,

				OmegaConfiguration.URL_PONTO_ELETRONICO

		};
		for(int i = 0 ; i < dominios.length; i++){
			String domain = formataDomain(dominios[i]);

			BasicClientCookie cookie= new BasicClientCookie(CHAVE_SESSION, idSeguranca);
			cookie.setPath("/");
			cookie.setExpiryDate(data);
			cookie.setDomain(domain);
			cookieStore.addCookie(cookie);
			cookieStoreLogin.addCookie(cookie);


			BasicClientCookie cookie2= new BasicClientCookie(CHAVE_AUTO_LOGIN, autoLogin);
			cookie2.setPath("/");
			cookie2.setExpiryDate(data);
			cookie2.setDomain(domain);
			cookieStore.addCookie(cookie2);
		}


		
		PontoEletronicoSharedPreference.saveValor(pContext, TIPO_STRING.ID_SEGURANCA, idSeguranca);
		PontoEletronicoSharedPreference.saveValor(pContext, TIPO_STRING.ID_LOGIN_AUTOMATICO, autoLogin);
		
	}
	public static void setCookie(DefaultHttpClient httpclient){
		if(cookieStoreLogin != null){
			httpclient.setCookieStore(cookieStoreLogin);
		}
	}
	public static CookieStore getCookieStoreComplete(){
		return cookieStore;
	}
	public static CookieStore getCookieStoreLogin(){
		return cookieStoreLogin;
	}
	public static boolean isAutenticacaoRealizada() {
		return cookieStore != null ? true : false;

	}
	
	

	public static boolean realizaLoginOfflineComOUsuarioSalvo(Context c) {

		Boolean carregarAutomatico = PontoEletronicoSharedPreference.getValor(c,
				PontoEletronicoSharedPreference.TIPO_BOOLEAN.LOGIN_AUTOMATICO_ATIVO);
		if (carregarAutomatico == null || !carregarAutomatico) {
			return false;

		} else {

			if (!Database.isDatabaseInitialized(c))
				return false;
			String corporacao = PontoEletronicoSharedPreference.getValor(c,
					PontoEletronicoSharedPreference.TIPO_STRING.LOGIN_AUTOMATICO_CORPORACAO);
			String senha = PontoEletronicoSharedPreference.getValor(c,
					PontoEletronicoSharedPreference.TIPO_STRING.LOGIN_AUTOMATICO_SENHA);
			String email = PontoEletronicoSharedPreference.getValor(c,
					PontoEletronicoSharedPreference.TIPO_STRING.LOGIN_AUTOMATICO_EMAIL);
			if (corporacao != null 
					&& email != null
					&& corporacao.length() > 0 
					&& email.length() > 0 
					&& OmegaSecurity.loadUserFromDatabase(email, corporacao, senha, c)) {
				return true;
			} else {
				return false;
			}

		}

	}
	public static void apagarLoginAutomatico(Context c){
		PontoEletronicoSharedPreference.saveValor(c,
				PontoEletronicoSharedPreference.TIPO_STRING.LOGIN_AUTOMATICO_CORPORACAO, null);
		PontoEletronicoSharedPreference.saveValor(c,
				PontoEletronicoSharedPreference.TIPO_STRING.LOGIN_AUTOMATICO_SENHA, null);
		PontoEletronicoSharedPreference.saveValor(c,
				PontoEletronicoSharedPreference.TIPO_STRING.LOGIN_AUTOMATICO_EMAIL, null);
		PontoEletronicoSharedPreference.saveValor(c,
				PontoEletronicoSharedPreference.TIPO_BOOLEAN.LOGIN_AUTOMATICO_ATIVO, false);
	}
	public static void salvarLoginAutomatico(
			Context c, 
			String corporacao,
			String email,
			String senha) {
	
		PontoEletronicoSharedPreference.saveValor(c,
				PontoEletronicoSharedPreference.TIPO_STRING.LOGIN_AUTOMATICO_CORPORACAO,
				corporacao);
		PontoEletronicoSharedPreference.saveValor(c,
				PontoEletronicoSharedPreference.TIPO_STRING.LOGIN_AUTOMATICO_SENHA, senha);
		PontoEletronicoSharedPreference.saveValor(c,
				PontoEletronicoSharedPreference.TIPO_STRING.LOGIN_AUTOMATICO_EMAIL, email);
		PontoEletronicoSharedPreference.saveValor(c,
				PontoEletronicoSharedPreference.TIPO_BOOLEAN.LOGIN_AUTOMATICO_ATIVO, true);
	}

	public static String getSavedEmail(Context c) {
		return PontoEletronicoSharedPreference.getValor(c,
				PontoEletronicoSharedPreference.TIPO_STRING.LOGIN_AUTOMATICO_EMAIL);
	}
	
	public static String getSavedCorporacao(Context c) {
		return PontoEletronicoSharedPreference.getValor(c,
				PontoEletronicoSharedPreference.TIPO_STRING.LOGIN_AUTOMATICO_CORPORACAO);
	}
	
	public static boolean isLoginAutomaticoAtivo(Context c) {
		return PontoEletronicoSharedPreference.getValor(c,
				PontoEletronicoSharedPreference.TIPO_BOOLEAN.LOGIN_AUTOMATICO_ATIVO);
	}
	public static String getSavedSenha(Database db, String pEmail) {
		STATE_SINCRONIZADOR state = RotinaSincronizador.getStateSincronizador(db.getContext(), null);
		if (state == STATE_SINCRONIZADOR.COMPLETO) {
			EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(db);
			vObjUsuario.setAttrValue(EXTDAOUsuario.EMAIL, pEmail);
			EXTDAOUsuario vTuplaUsuario = (EXTDAOUsuario) vObjUsuario.getFirstTupla();
			if (vTuplaUsuario != null) {

				String vSenha = vTuplaUsuario.getStrValueOfAttribute(EXTDAOUsuario.SENHA);

				return vSenha;
			}
		}

		return "";
	}

	public static String[] loadComponentesSalvos(Database db) {

		if(!isLoginAutomaticoAtivo(db.getContext()))
			return null;
		String vEmail = getSavedEmail(db.getContext());
		String vSenha = getSavedSenha(db, vEmail);
		String vCorporacao = getSavedCorporacao(db.getContext());
		
		if (vEmail != null && vEmail.length() > 0	
			&& vSenha != null && vSenha.length() > 0
			&& vCorporacao != null && vCorporacao.length()> 0)
			return new String[]{vCorporacao, vEmail, vSenha};
		return null;
	}
	public static boolean setIdUser(
			Context pContext,
			String pIdUser, 
			String pSenha, 
			String pIdCorporacao, 
			boolean pIsAdm,
			String corporacao, 
			String email, 
			String categoriaPermissao,
			String idSeguranca,
			String autoLogin) {
		
		if (pIdUser == null || pIdCorporacao == null || pIdUser.length() == 0 || pIdCorporacao.length() == 0)
			return false;
		else {
			ID_USUARIO = pIdUser;
			ID_CORPORACAO = pIdCorporacao;
			SENHA = pSenha;
			IS_ADM = pIsAdm;
			CORPORACAO = corporacao;
			EMAIL = email;
			ID_CATEGORIA_PERMISSAO = HelperString.checkIfIsNull(categoriaPermissao);
			
			initCookie(pContext, idSeguranca, autoLogin);
			return true;
		}
	}

	public static String getCorporacao() {
		return CORPORACAO;
	}

	public static String getIdCategoriaPermissao() {
		return ID_CATEGORIA_PERMISSAO;
	}

	public static void setIdCategoriaPermissao(Integer pIdCategoriaPermissao) {
		ID_CATEGORIA_PERMISSAO = pIdCategoriaPermissao.toString();
	}

	public static String getEmail() {
		return EMAIL;
	}

	public static void setSenha(String pSenha) {
		SENHA = pSenha;
	}

	public static void setIdVeiculoUsuario(String pIdVeiculoUsuario) {
		if (ID_VEICULO_USUARIO != null && ID_VEICULO_USUARIO.length() > 0)
			ID_VEICULO_USUARIO = pIdVeiculoUsuario;
		else
			ID_VEICULO_USUARIO = null;
	}

	public static String getIdVeiculoUsuario() {
		if (ID_VEICULO_USUARIO != null && ID_VEICULO_USUARIO.length() > 0)
			return ID_VEICULO_USUARIO;
		else
			return null;
	}

	public static void clearVeiculoUsuario() {
		ID_VEICULO_USUARIO = null;
	}

	public static void logout() {
		// Nao podemos mais zerar os dados de login, pois durante uma
		// sincronizacao os mesmos suo necessurios
		// ju ocorreu um erro desses dados serem zerados, a sincronizacao nuo
		// ter sido parada, e o atributo fk
		// corporacao_id_INT ter sido setado para nulo localmente e enviado ao
		// servidor!!!
		// Por isso foi criada a flag 'autenticado'
		//
		// ID_USUARIO = "";
		// ID_CORPORACAO = "";
		// ID_CATEGORIA_PERMISSAO = "";
		// ID_VEICULO_USUARIO = "";
		if (idPermissoes != null)
			idPermissoes.clear();

		if(cookieStore != null){
			cookieStore.clear();
			cookieStore = null;
		}
		
	}

	public static boolean loadUserFromDatabase(String email, String corporacao, String senha, Context pContext) {

		Database db = null;
		try {
			STATE_SINCRONIZADOR state = RotinaSincronizador.getStateSincronizador(pContext, null);
			if (state != STATE_SINCRONIZADOR.COMPLETO)
				return false;
			db = new DatabasePontoEletronico(pContext);
			EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(db);
			vObjUsuario.setAttrValue(EXTDAOUsuario.EMAIL, email);
			Table tuplaUsuario = vObjUsuario.getFirstTupla();

			EXTDAOCorporacao objCorporacao = new EXTDAOCorporacao(db);
			objCorporacao.setAttrValue(EXTDAOCorporacao.NOME, corporacao);
			Table tuplaCorporacao = objCorporacao.getFirstTupla();
			if (tuplaUsuario != null & tuplaCorporacao != null) {
				String idUser = tuplaUsuario.getStrValueOfAttribute(EXTDAOUsuario.ID);
				String idCorporacao = tuplaCorporacao.getStrValueOfAttribute(EXTDAOUsuario.ID);
				EXTDAOUsuarioCorporacao objUsuarioCorporacao = new EXTDAOUsuarioCorporacao(db);
				objUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.USUARIO_ID_INT, idUser);
				objUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.CORPORACAO_ID_INT, idCorporacao);
				EXTDAOUsuarioCorporacao tuplaUsuarioCorporacao = (EXTDAOUsuarioCorporacao) objUsuarioCorporacao
						.getFirstTupla(null, false);
				if (tuplaUsuarioCorporacao != null) {

					String strAdm = tuplaUsuarioCorporacao
							.getStrValueOfAttribute(EXTDAOUsuarioCorporacao.IS_ADM_BOOLEAN);
					Boolean isAdm = HelperBoolean.valueOfStringSQL(strAdm);
					if (isAdm == null)
						isAdm = false;
					// se o usuario nao for administrador
					String categoriaPermissao = null;
					if (!isAdm) {

						categoriaPermissao = inicializaPermissoesUsuarioDoBancoDeDados(db);

					} else {
						// inicializaPermissoesUsuarioAdministrador(db);
					}
					String status = tuplaUsuarioCorporacao
							.getStrValueOfAttribute(EXTDAOUsuarioCorporacao.STATUS_BOOLEAN);
					
					String idSeguranca = PontoEletronicoSharedPreference.getValor(pContext, TIPO_STRING.ID_SEGURANCA);
					String idLoginAutomatico = PontoEletronicoSharedPreference.getValor(pContext, TIPO_STRING.ID_LOGIN_AUTOMATICO);
					if (status != null && HelperBoolean.valueOfStringSQL(status)) {
						return setIdUser(
								pContext,
								idUser, 
								senha, 
								idCorporacao, 
								isAdm, 
								corporacao, 
								email,
								categoriaPermissao,
								idSeguranca,
								idLoginAutomatico);
					}
					return false;
				} else
					return false;
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);

		} finally {
			if (db != null)
				db.close();
		}
		return false;
	}

	public static String inicializaPermissoesUsuarioDoBancoDeDados(Database db) {
		String categoriaPermissao = EXTDAOUsuarioCategoriaPermissao.getIdCategoriaPermissaoDoUsuario(db,
				OmegaSecurity.getIdUsuario());

		idPermissoes = EXTDAOPermissao.getListaPermissao(db, categoriaPermissao);

		return categoriaPermissao;
	}

	public static boolean usuarioPossuiPermissao(int permissao) {
		if (isAdm())
			return true;
		else if (idPermissoes.contains(permissao))
			return true;
		else
			return false;
	}

	public static ArrayList<Integer> getListaIdPermissaoDaTela(Database db, Integer idPaiPermissao) {
		//
		// ArrayList<Integer> listRetorno = new ArrayList<Integer>();
		//
		// if(permissoes != null){
		// Iterator<Integer> it = permissoes.keySet().iterator();
		// Integer id;
		// while(it.hasNext()){
		// id = it.next();
		// EXTDAOPermissao objPermissao = permissoes.get(id);
		// if(!ContainerMenuOption.isContainerItemExistent(objPermissao.getStrValueOfAttribute(EXTDAOPermissao.TAG)))
		// continue;
		// Integer paiPermissao = HelperInteger.parserInteger(
		// objPermissao.getStrValueOfAttribute(EXTDAOPermissao.PAI_PERMISSAO_ID_INT));
		// if(id != null && (paiPermissao == idPaiPermissao || (paiPermissao ==
		// null && idPaiPermissao == null))){
		// listRetorno.add(id);
		// }
		// }
		// }
		// return listRetorno;
		return null;
	}

	public static EXTDAOPermissao getObjPermissao(Database db, Integer id) {
		// if(permissoes == null) permissoes=new HashMap<Integer,
		// EXTDAOPermissao>();
		// if(permissoes.containsKey(id)) return permissoes.get(id);
		// else{
		// EXTDAOPermissao obj = new EXTDAOPermissao(db);
		// if(obj.select(id.toString())){
		// permissoes.put(id, obj);
		// }
		// return obj;
		// }
		return null;
	}
	//
	// public static EXTDAOPermissao getObjPermissao(String tag){
	//
	//
	//
	// if(permissoes != null){
	// Iterator<Integer> it = permissoes.keySet().iterator();
	// Integer id;
	// while(it.hasNext()){
	// id = it.next();
	// EXTDAOPermissao objPermissao = permissoes.get(id);
	//
	// if(objPermissao.getStrValueOfAttribute(EXTDAOPermissao.TAG).compareTo(tag)
	// == 0){
	//
	// }
	// return objPermissao;
	//
	// }
	// }
	// return null;
	// }

	// public static void innativeUserInDatabase(String pStrEmail, String
	// pStrCorporacao, Context pContext){
	// Database db = new DatabasePontoEletronico(pContext);
	// try{
	// EXTDAOUsuario vUsr = new EXTDAOUsuario(db);
	// vUsr.setAttrStrValue(EXTDAOUsuario.EMAIL, pStrEmail);
	//
	// ArrayList<Table> vListTableUsuario = vUsr.getListTable();
	//
	// EXTDAOCorporacao vCorporacao = new EXTDAOCorporacao(db);
	// vCorporacao.setAttrStrValue(EXTDAOCorporacao.NOME, pStrCorporacao);
	//
	// ArrayList<Table> vListTableCorporacao = vCorporacao.getListTable();
	//
	// if(vListTableUsuario.size() == 1 && vListTableCorporacao.size() == 1){
	// EXTDAOUsuario vObjUsuario = (EXTDAOUsuario) vListTableUsuario.get(0);
	// String vIdUsuario = vObjUsuario.getStrValueOfAttribute(EXTDAOUsuario.ID);
	//
	// EXTDAOCorporacao vObjCorporacao = (EXTDAOCorporacao)
	// vListTableCorporacao.get(0);
	// String vIdCorporacao =
	// vObjCorporacao.getStrValueOfAttribute(EXTDAOCorporacao.ID);
	// if(vIdUsuario != null && vIdCorporacao != null){
	// if(vIdUsuario.length() > 0 && vIdCorporacao.length() > 0 ){
	// EXTDAOUsuarioCorporacao vUsuarioCorporacao = new
	// EXTDAOUsuarioCorporacao(db);
	// vUsuarioCorporacao.setAttrStrValue(EXTDAOUsuarioCorporacao.CORPORACAO_ID_INT,
	// vIdCorporacao);
	// vUsuarioCorporacao.setAttrStrValue(EXTDAOUsuarioCorporacao.USUARIO_ID_INT,
	// vIdUsuario);
	// ArrayList<Table> vListUsuarioCorporacao =
	// vUsuarioCorporacao.getListTable(null, false);
	// if(vListUsuarioCorporacao.size() == 1){
	// EXTDAOUsuarioCorporacao vObjEXTDAOCorporacao = (EXTDAOUsuarioCorporacao)
	// vListUsuarioCorporacao.get(0);
	// vObjEXTDAOCorporacao.setAttrStrValue(EXTDAOUsuarioCorporacao.STATUS_BOOLEAN,
	// "0");
	// vObjEXTDAOCorporacao.update();
	// }
	// }
	// }
	//
	// }
	// }catch(Exception ex){
	//
	// }finally{
	// db.close();
	// }
	//
	// }

	public enum STATE_LOAD_USER_HTTP_POST {
		ERROR, PRIMEIRO_LOGIN_DA_CORPORACAO, OK, USUARIO_SEM_CATEGORIA_DE_PERMISSAO, RESETAR
	};

	public static STATE_LOAD_USER_HTTP_POST loadUserHttpPost(String pStrEmail, String pSenha, String pStrCorporacao,
			Context pContext) {
		return loadUserHttpPost(pStrEmail, pSenha, pStrCorporacao, pContext, false);
	}

	public static STATE_LOAD_USER_HTTP_POST loadUserHttpPost(String pStrEmail, String pSenha, String pStrCorporacao,
			Context pContext, boolean force) {
		STATE_LOAD_USER_HTTP_POST estado = procedureLoadUserHttpPost(pStrEmail, pSenha, pStrCorporacao, pContext,
				force);
		if (estado != STATE_LOAD_USER_HTTP_POST.ERROR) {
			if (PontoEletronicoSharedPreference.getValor(pContext, TIPO_BOOLEAN.RESETAR_BANCO_DE_DADOS))
				return STATE_LOAD_USER_HTTP_POST.RESETAR;
			else
				return estado;
		} else {
			return estado;
		}
	}

	
	@SuppressWarnings("unused")
	private static STATE_LOAD_USER_HTTP_POST procedureLoadUserHttpPost(String pStrEmail, String pSenha,
			String pStrCorporacao, Context pContext, boolean force) {
		Database db = null;
		try {
			db = new DatabasePontoEletronico(pContext);
			STATE_SINCRONIZADOR state = RotinaSincronizador.getStateSincronizador(pContext, null);
			boolean carregarPermissoes =
					state!=STATE_SINCRONIZADOR.COMPLETO || 
					EXTDAOUsuario.existeNecessidadeDeCarregarAsPermissoes(db, pStrEmail, pStrCorporacao); 
			RetornoJson rj = null;
			if(NetworkUtilities.LAST_POST_LOGIN == null){
				ConfiguracaoHttpPost conf = new ConfiguracaoHttpPost();
				conf.setCookie = true;
				rj = HelperHttpPost.getRetornoJsonPost(
						pContext, 
						OmegaConfiguration.URL_REQUEST_LOGIN_MOBILE(), 
						new String[]{"email", "senha", "corporacao", "carregarPermissao", "carregarInformacoesUsuario"}, 
						new String[]{pStrEmail, pSenha, pStrCorporacao, String.valueOf( carregarPermissoes), "true"},
						conf);
				
				if (rj == null )
					return STATE_LOAD_USER_HTTP_POST.ERROR;
				
				JSONObject obj= rj.json;
				int vCodRetorno = obj.getInt("mCodRetorno");
				
				if (vCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId() ){
					return STATE_LOAD_USER_HTTP_POST.ERROR;
				}
				
			} else rj = NetworkUtilities.LAST_POST_LOGIN;
			
			
			STATE_LOAD_USER_HTTP_POST statePost = OmegaSecurity.procedureLoadUserJson(
					db.getContext(),
					pStrEmail, 
					pSenha, 
					rj, 
					force);
			if(statePost != STATE_LOAD_USER_HTTP_POST.ERROR)
				NetworkUtilities.LAST_POST_LOGIN = null;
			return statePost;
		
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.WEBSERVICE_PONTO_ELETRONICO);
		} finally {
			if(db!=null)db.close();
		}
		return STATE_LOAD_USER_HTTP_POST.ERROR;
	}

	public static boolean isAdm() {
		return IS_ADM;
	}

	public static String getIdUsuario() {
		return ID_USUARIO;
	}

	public static String getSenha() {
		return SENHA;
	}

	public static String getIdCorporacao() {
		return ID_CORPORACAO;
	}

	public static STATE_LOAD_USER_HTTP_POST procedureLoadUserJson(
			Context pContext, 
			String email, 
			String pSenha, 
			RetornoJson rj, 
			boolean force) throws OmegaDatabaseException {
		Database db = new DatabasePontoEletronico(pContext);
		try {
			boolean vIsPrimeiroLogin = false;
			JSONObject obj = rj.json.getJSONObject("mObj");
			String idUsuario = obj.getString("id");
			String usuario = obj.getString("nome");
			String idCorporacao = HelperJson.getIntegerAsString(obj,"id_corporacao");
			String corporacao= obj.getString("corporacao");
			Boolean statusUsuario = HelperJson.getBoolean(obj, "status");
			String idUsuarioCorporacao = HelperJson.getIntegerAsString(obj,"id_usuario_corporacao");
			Boolean statusUsuarioCorporacao = HelperJson.getBoolean(obj, "status_corporacao");
			Boolean isAdm = HelperJson.getBoolean(obj, "is_adm");
			String idCategoriaPermissao = HelperJson.getIntegerAsString(obj, "id_categoria_permissao");
			String idSistemaSihop = HelperJson.getIntegerAsString(obj, "id_sistema_sihop");
			if(idSistemaSihop != null)
				PontoEletronicoSharedPreference.saveValor(
						pContext, 
						PontoEletronicoSharedPreference.TIPO_STRING.ID_SISTEMA_SIHOP, 
						idSistemaSihop);
			STATE_SINCRONIZADOR state = RotinaSincronizador.getStateSincronizador(pContext, idCorporacao );
			if (state == STATE_SINCRONIZADOR.COMPLETO || force) {
				try {

					EXTDAOCorporacao vCorporacao = new EXTDAOCorporacao(db);
					vCorporacao.setAttrValue(EXTDAOCorporacao.ID, idCorporacao);
					if (!vCorporacao.select()) {
						vCorporacao.setAttrValue(EXTDAOCorporacao.NOME, corporacao );
						vCorporacao.setAttrValue(Attribute.getNomeAtributoNormalizado(EXTDAOCorporacao.NOME),
								corporacao );
						vIsPrimeiroLogin = true;
						vCorporacao.insert(false);
					}
					
					EXTDAOUsuario vUsr = new EXTDAOUsuario(db);
					vUsr.setAttrValue(EXTDAOUsuario.ID, idUsuario);
					if (vUsr.select()) {
						vUsr.setAttrValue(EXTDAOUsuario.ID, idUsuario);
						vUsr.setAttrValue(EXTDAOUsuario.NOME, usuario);
						vUsr.setAttrValue(EXTDAOUsuario.EMAIL, email);
						vUsr.setAttrValue(Attribute.getNomeAtributoNormalizado(EXTDAOUsuario.NOME), usuario);
						vUsr.setAttrValue(EXTDAOUsuario.STATUS_BOOLEAN, statusUsuario);

						if (!vUsr.update(false))
							throw new Exception("Falha ao atualizar o usuurio["+idUsuario+"]");
					} else {
						vUsr.setAttrValue(EXTDAOUsuario.ID, idUsuario);
						vUsr.setAttrValue(EXTDAOUsuario.NOME, usuario);
						vUsr.setAttrValue(Attribute.getNomeAtributoNormalizado(EXTDAOUsuario.NOME), usuario);
						vUsr.setAttrValue(EXTDAOUsuario.EMAIL, email);
						vUsr.setAttrValue(EXTDAOUsuario.STATUS_BOOLEAN, statusUsuario);
						if(vUsr.insert(false) <0) throw new Exception("Falha ao inserir o usuurio["+idUsuario+"]");
					}
					EXTDAOUsuarioCorporacao vUsuarioCorporacao = new EXTDAOUsuarioCorporacao(db);
					
					if (vUsuarioCorporacao.select(idUsuarioCorporacao)) {
						vUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.ID, idUsuarioCorporacao);
						vUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.STATUS_BOOLEAN, statusUsuarioCorporacao);
						vUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.IS_ADM_BOOLEAN, isAdm);
						vUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.USUARIO_ID_INT, idUsuario);
						vUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.CORPORACAO_ID_INT, idCorporacao);

						if (!vUsuarioCorporacao.update(false))
							throw new Exception("Falha ao atualizar o usuario_corporacao["+idUsuarioCorporacao+"]");
					} else {
						vUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.ID, idUsuarioCorporacao);
						vUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.STATUS_BOOLEAN, statusUsuarioCorporacao);
						vUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.IS_ADM_BOOLEAN, isAdm);
						vUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.USUARIO_ID_INT, idUsuario);
						vUsuarioCorporacao.setAttrValue(EXTDAOUsuarioCorporacao.CORPORACAO_ID_INT, idCorporacao);

						if( vUsuarioCorporacao.insert(false) < 0 )
							throw new Exception("Falha ao isnerir o usuario_corporacao["+idUsuarioCorporacao+"]");
					}
				} catch (Exception ex1) {
					SingletonLog.insereErro(ex1, SingletonLog.TIPO.WEBSERVICE_PONTO_ELETRONICO);

				}
			} else if (state != STATE_SINCRONIZADOR.COMPLETO)
				vIsPrimeiroLogin = true;
			
			String idChaveSeguranca = rj.getCookie(OmegaSecurity.CHAVE_SESSION);
			String idChaveAutoLogin =  rj.getCookie(OmegaSecurity.CHAVE_AUTO_LOGIN);
			if(idChaveAutoLogin == null || idChaveSeguranca == null)
				return STATE_LOAD_USER_HTTP_POST.ERROR;
			boolean vValidade = setIdUser(pContext, idUsuario, pSenha, idCorporacao, isAdm, corporacao, email,
					idCategoriaPermissao, idChaveSeguranca, idChaveAutoLogin );

			if (!vValidade)
				return STATE_LOAD_USER_HTTP_POST.ERROR;

			else {
				
				if (OmegaSecurity.isAdm()) {
					// inicializaPermissoesUsuarioAdministrador(db);
				} else {
					JSONArray permissoes = obj.isNull("permissoes") ? null : obj.getJSONArray("permissoes");
					if (permissoes != null && permissoes.length() > 0) {
						
						if (idPermissoes == null)
							idPermissoes = new HashSet<Integer>();
						else
							idPermissoes.clear();
						
						for(int i = 0 ; i < permissoes.length(); i++)
							idPermissoes.add(HelperInteger.parserInteger(permissoes.getString(i)));
						
						
					} else if (!isAdm &&  idCategoriaPermissao == null) {
						return STATE_LOAD_USER_HTTP_POST.USUARIO_SEM_CATEGORIA_DE_PERMISSAO;
					} else if (!vIsPrimeiroLogin)
						inicializaPermissoesUsuarioDoBancoDeDados(db);
				}
				if (vIsPrimeiroLogin)
					return STATE_LOAD_USER_HTTP_POST.PRIMEIRO_LOGIN_DA_CORPORACAO;
				else
					return STATE_LOAD_USER_HTTP_POST.OK;

			}
		
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.WEBSERVICE_PONTO_ELETRONICO);
		} finally {
			db.close();
		}
		return STATE_LOAD_USER_HTTP_POST.ERROR;
	}

//	public static void loginComAlgumUsuarioDaCorporacaoSincronizada(Database db, String idCorporacao){
//		//Realiza o login com algum usuario da base de dados
//		Tuple<Integer, String> registro = SincronizadorEspelho.getCorporacaoSincronizada(c);
//		if(registro == null) return STATE_SINCRONIZADOR.NAO_REALIZADO;
//		else if(pIntIdCorporacao != null){
//			if( (int)registro.x == (int)pIntIdCorporacao) 
//				return STATE_SINCRONIZADOR.COMPLETO;
//			else return STATE_SINCRONIZADOR.NAO_REALIZADO;
//		} else 	return STATE_SINCRONIZADOR.COMPLETO;
//		
//		String q = "SELECT u.id, u.nome"
//				+ " FROM usuario u join usuario_corporacao uc on u.id=uc.usuario_id_INT "
//				+ "		join corporacao c on c.id = uc.corporacao_id_INT"
//				+ " WHERE c.id=?";
//		
//	}
	
	
}
