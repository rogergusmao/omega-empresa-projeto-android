package app.omegasoftware.pontoeletronico.common.authenticator;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;

public class RelembrarSenhaActivityMobile extends OmegaRegularActivity {

	//Constants
	public static final String TAG = "EnviarErroOcorridoActivityMobile";

	
	private final int  FORM_ERROR_DIALOG_MISSING_MENSAGEM = 91;
	
	
	private Button cadastrarButton;

	String email = "";
	
	private EditText emailEditText;	

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getIntent().putExtra(OmegaConfiguration.SEARCH_FIELD_TAG, TAG);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.relembrar_senha_activity_mobile_layout);
		
	
		Bundle p = getIntent().getExtras();
		if(p != null){
			if(p.containsKey(OmegaConfiguration.SEARCH_FIELD_EMAIL)){
				email = p.getString(OmegaConfiguration.SEARCH_FIELD_EMAIL);
			}
		}
		//
		new CustomDataLoader(this).execute();

	}


	@Override
	public boolean loadData() {
		return true;
	}



	@Override
	public void initializeComponents() {	
		try{
			emailEditText = (EditText) findViewById(R.id.email_edittext);
			emailEditText.setText(email);
			
			//Attach search button to its listener
			this.cadastrarButton = (Button) findViewById(R.id.enviar_button);
			this.cadastrarButton.setOnClickListener(new LembrarSenhaListener());
	
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog vDialog = null;

		switch (id) {
		default:
			vDialog = this.getErrorDialog(id);
			break;
		}
		return vDialog;
	}
	
	public Dialog getErrorDialog(int dialogType)
	{

		Dialog vDialog = new Dialog(this);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		switch (dialogType) {
		
		
		case FORM_ERROR_DIALOG_MISSING_MENSAGEM:
			vTextView.setText(getResources().getString(R.string.error_dialog_missing_mensagem));
			vOkButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dismissDialog(FORM_ERROR_DIALOG_MISSING_MENSAGEM);
				}
			});
			break;
			
		default:
			return super.onCreateDialog(dialogType);
		
		}

		return vDialog;
	}

	//---------------------------------------------------------------
	//----------------- Methods related to search action ------------
	//---------------------------------------------------------------
	//Handles search button click
	
	

	

	private class LembrarSenhaListener implements OnClickListener {

		
		
		public void onClick(View v) {
			new LembrarSenhaLoader(
					RelembrarSenhaActivityMobile.this, 
					controlerProgressDialog
			).execute(emailEditText.getText().toString());
	
		}
	}	
}


