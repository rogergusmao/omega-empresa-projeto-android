package app.omegasoftware.pontoeletronico.common.container;

import android.app.Activity;
import android.content.Context;
import android.view.View.OnClickListener;

public abstract class AbstractContainerClassItem {
	Class<?> classe;
	String tag;
	int idStringNome;
	Integer drawableId;
	boolean isComumATodosUsuarios = false;
	String tagPai = null;
	public AbstractContainerClassItem(
			int pIdStringNome,
			Class<?> pClass,
			String pTag,
			Integer pDrawable){
		classe = pClass;
		tag = pTag;
		drawableId = pDrawable;
		idStringNome = pIdStringNome;
	}
	
	public AbstractContainerClassItem(
			int pIdStringNome,
			Class<?> pClass,
			String pTag,
			Integer pDrawable,
			boolean pIsComumATodosUsuario){
		classe = pClass;
		tag = pTag;
		drawableId = pDrawable;
		idStringNome = pIdStringNome;
		isComumATodosUsuarios = pIsComumATodosUsuario;
	}
	
	
	public String getTagPai(){
		return tagPai;
	}
	
	public boolean isComumATodosUsuarios(){
		return isComumATodosUsuarios;
	}
	
	
	
	public abstract OnClickListener getOnClickListener(
			Activity pActivity);
	
	public boolean isEqual(String pTagCompare){
		if(pTagCompare == null) return false;
		
		String vTagCompare1 = tag.toLowerCase();
		String vTagCompare2 = pTagCompare.toLowerCase();
		if(vTagCompare1.compareTo(vTagCompare2) ==0 ) return true;
		else return false;
	}
	
	public Integer getDrawableId(){
		return drawableId;
	}
	
	public String getTag(){
		return tag;
	}
	
	public Class<?> getClassTarget(){
		return classe;
	}
	public String getNome(Context pContext){
		return pContext.getResources().getString(idStringNome);
		
	}
}
