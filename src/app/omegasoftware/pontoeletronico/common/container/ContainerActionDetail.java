package app.omegasoftware.pontoeletronico.common.container;

import android.app.Activity;
import android.content.Intent;
import app.omegasoftware.pontoeletronico.TabletActivities.MenuPontoEletronicoRotinaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailBairroActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailCidadeActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailCorporacaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailEstadoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailModeloActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailPaisActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailPessoaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailProfissaoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailRedeActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailRelatorioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailTarefaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailTipoDocumentoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailTipoEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailVeiculoActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailVeiculoUsuarioActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailVeiculoUsuarioAtivoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;


public class ContainerActionDetail {
	public static void actionDetail(
			Activity pContext, 
			short pModeSearch, 
			String pId,
			String email){
				
		Intent vIntent = null; 
		switch(pModeSearch){
		
		case OmegaConfiguration.SEARCH_MODE_RELATORIO:
			vIntent = new Intent(pContext, DetailRelatorioActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_PESSOA_EMPRESA_ROTINA:
			vIntent = new Intent(pContext, MenuPontoEletronicoRotinaActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_MODELO:
			vIntent = new Intent(pContext, DetailModeloActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_PROFISSAO:
			vIntent = new Intent(pContext, DetailProfissaoActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_CORPORACAO:
			vIntent = new Intent(pContext, DetailCorporacaoActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_PESSOA:
			vIntent = new Intent(pContext, DetailPessoaActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_USUARIO:
			vIntent = new Intent(pContext, DetailUsuarioActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_EMPRESA:
			vIntent = new Intent(pContext, DetailEmpresaActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_TIPO_EMPRESA:
			vIntent = new Intent(pContext, DetailTipoEmpresaActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_TIPO_DOCUMENTO:
			vIntent = new Intent(pContext, DetailTipoDocumentoActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_VEICULO:
			vIntent = new Intent(pContext, DetailVeiculoActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_VEICULO_USUARIO_ATIVO:
			vIntent = new Intent(pContext, DetailVeiculoUsuarioAtivoActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_VEICULO_USUARIO:
			vIntent = new Intent(pContext, DetailVeiculoUsuarioActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_PAIS:
			vIntent = new Intent(pContext, DetailPaisActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_REDE:
			vIntent = new Intent(pContext, DetailRedeActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_ESTADO:
			vIntent = new Intent(pContext, DetailEstadoActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_MAPA_PONTO:
//			vIntent = new Intent(pContext, PontoMapActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_CIDADE:
			vIntent = new Intent(pContext, DetailCidadeActivityMobile.class);
			break;
		case OmegaConfiguration.SEARCH_MODE_BAIRRO:
			vIntent = new Intent(pContext, DetailBairroActivityMobile.class);
			break;
			case OmegaConfiguration.SEARCH_MODE_TAREFA:
				vIntent = new Intent(pContext, DetailTarefaActivityMobile.class);
				break;

		}
		if(vIntent != null){
			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, pId);
			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_EMAIL, email);
//			vIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
			pContext.startActivityForResult(vIntent, OmegaConfiguration.ACTIVITY_DETAIL);	
		}
	}
}
