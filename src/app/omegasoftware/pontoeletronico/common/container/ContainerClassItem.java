package app.omegasoftware.pontoeletronico.common.container;

import android.app.Activity;
import android.view.View.OnClickListener;
import app.omegasoftware.pontoeletronico.TabletActivities.FactoryMenuActivityMobile;
import app.omegasoftware.pontoeletronico.listener.MenuOnClickListener;

public class ContainerClassItem extends AbstractContainerClassItem {
	
	public static String TAG = "ContainerClassItem";
	
	public ContainerClassItem(
			int pIdStringNome,
			Class<?> pClass,
			String pTag,
			Integer pDrawable,
			boolean pIsComumATodosUsuarios){
		super(pIdStringNome, 
				pClass,
				pTag,
				pDrawable,
				pIsComumATodosUsuarios);
		
	}
	
	public ContainerClassItem(
			int pIdStringNome,
			Class<?> pClass,
			String pTag,
			Integer pDrawable){
		super(pIdStringNome,
				pClass,
				pTag,
				pDrawable);
		
	}
	
	public ContainerClassItem(
			int pIdStringNome,
			Class<?> pClass,
			String pTag){
		super(pIdStringNome,
				pClass,
				pTag,
				null);
		
	}

	@Override
	public OnClickListener getOnClickListener(Activity pActivity) {
		
		return new MenuOnClickListener(
				(FactoryMenuActivityMobile) pActivity,
				getClassTarget());
	}
	
}
