package app.omegasoftware.pontoeletronico.common.container;

import android.app.Activity;
import android.view.View.OnClickListener;
import app.omegasoftware.pontoeletronico.TabletActivities.FactoryMenuActivityMobile;
import app.omegasoftware.pontoeletronico.listener.MenuOnClickListener;

public class ContainerClassMenuItem extends AbstractContainerClassItem{
	public static String TAG = "ContainerClassMenuItem";
	public ContainerClassMenuItem(
			int pIdStringNome,
			String pTag,
			Integer pDrawable){
		super(pIdStringNome,
				FactoryMenuActivityMobile.class,
				pTag, 
				pDrawable);
	}
	
	public ContainerClassMenuItem(
			int pIdStringNome,
			String pTag,
			Integer pDrawable, 
			boolean pIsComumATodosUsuarios){
		super(pIdStringNome,
				FactoryMenuActivityMobile.class,
				pTag, 
				pDrawable,
				pIsComumATodosUsuarios);
	}

	@Override
	public OnClickListener getOnClickListener(Activity pActivity) {
		
		return new MenuOnClickListener(
				(FactoryMenuActivityMobile) pActivity,
				getClassTarget(),
				tag);
	}


}

