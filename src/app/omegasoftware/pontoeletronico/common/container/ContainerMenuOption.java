package app.omegasoftware.pontoeletronico.common.container;

import java.util.ArrayList;

import app.omegasoftware.pontoeletronico.appconfiguration.ContainerPrograma;




public class ContainerMenuOption{

	 
	
	public static ArrayList<AbstractContainerClassItem> getListContainerItemComumATodosUsuarios(){
		AbstractContainerClassItem vetorContainerClassItemComumATodoUsuario[] = ContainerPrograma.getVetorContainerClassItem();
		
		
		ArrayList<AbstractContainerClassItem> vList = new ArrayList<AbstractContainerClassItem>(); 
		for (AbstractContainerClassItem containerClassItem : vetorContainerClassItemComumATodoUsuario) {
			if(containerClassItem.isComumATodosUsuarios()){
				vList.add(containerClassItem);
			}
			
		}
		return vList;
	}
	
	public static boolean isContainerItemExistent(String pTag){
		AbstractContainerClassItem vContainer = getContainerItem(pTag);
		if(vContainer == null) return false;
		else return true;
	}
	
	public static AbstractContainerClassItem getContainerItem(String pTag){
		if(pTag == null) return null;
		
		AbstractContainerClassItem vVetorContainerClassItemOmegaEmpresa[] = ContainerPrograma.getVetorContainerClassItem();
		for (AbstractContainerClassItem containerClassItem : vVetorContainerClassItemOmegaEmpresa) {
			
			if(containerClassItem.isEqual(pTag)) return containerClassItem;
		}
		return null;
	}
	
	public static ArrayList<AbstractContainerClassItem> getListContainerItem(ArrayList<String> pListTag){
		ArrayList<AbstractContainerClassItem> vList = new ArrayList<AbstractContainerClassItem>();
		for (String vToken : pListTag) {
			AbstractContainerClassItem vContainer = getContainerItem(vToken);
			if(vContainer != null) vList.add(vContainer);
		}
		
		return vList;
	}
	
}
