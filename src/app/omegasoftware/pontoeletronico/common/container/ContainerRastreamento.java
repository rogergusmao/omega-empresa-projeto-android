package app.omegasoftware.pontoeletronico.common.container;

import android.content.Context;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioPosicaoRastreamento;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class ContainerRastreamento {

	DatabasePontoEletronico db=null;
	EXTDAOUsuarioPosicaoRastreamento objUsuarioPosicaoRastreamento;
	public final static int LIMIT_TUPLAS = 500;
	public static String LIMIT_TUPLAS_STR = "500";

	public int limiteInferior = LIMIT_TUPLAS;
	Context context;
	String idUsuarioBusca;
	String idVeiculoBusca;
	String dataInicioBusca;
	String horaInicioBusca;
	String dataFimBusca;
	String horaFimBusca;
	String velocidadeMinima;
	private boolean hasMoreResult = true;
	Boolean somenteComFoto;
	public ContainerRastreamento(
			Context pContext,
			int pLimitInferior, 
			String pIdUsuarioBusca,
			String pIdVeiculoBusca,
			String pDataInicioBusca,
			String pHoraInicioBusca,
			String pDataFimBusca,
			String pHoraFimBusca,
			String pVelocidadeMinima,
			Boolean pSomenteComFoto){
		//TODO resolver isso quando utilizar a classe, descomentar linha abaixo
//		db = new DatabasePontoEletronico(pContext);
		objUsuarioPosicaoRastreamento = new EXTDAOUsuarioPosicaoRastreamento(db);
		context = pContext;
		limiteInferior = pLimitInferior;
		idUsuarioBusca = pIdUsuarioBusca;
		idVeiculoBusca = pIdVeiculoBusca;
		dataInicioBusca = pDataInicioBusca;
		horaInicioBusca = pHoraInicioBusca;
		dataFimBusca = pDataFimBusca;
		horaFimBusca = pHoraFimBusca;
		velocidadeMinima = pVelocidadeMinima;
		somenteComFoto = pSomenteComFoto;

	}

	//Null if not has result
	// False if not is finished the search
	// True if is finished the search
	public Integer receiveRotaUsuario() throws Exception{
		

		Integer vNumeroTuplasBaixadas = rastrearRotaUsuario(
				context, 
				objUsuarioPosicaoRastreamento, 
				limiteInferior, 
				idUsuarioBusca, 
				idVeiculoBusca, 
				dataInicioBusca, 
				horaInicioBusca, 
				dataFimBusca, 
				horaFimBusca, 
				velocidadeMinima, 
				somenteComFoto);

		if(vNumeroTuplasBaixadas == LIMIT_TUPLAS){
			hasMoreResult = true;
			limiteInferior += LIMIT_TUPLAS;
		}
		return vNumeroTuplasBaixadas;
	}

	public int getLimitInferior(){
		return limiteInferior;
	}
	public static Integer getTotalTuplasRastrearRotaUsuario(
			Context pContext, 
			EXTDAOUsuarioPosicaoRastreamento pObj, 
			String pIdUsuario, 
			String pIdVeiculo, 
			String pDataInicio, 
			String pHoraInicio, 
			String pDataFim, 
			String pHoraFim, 
			String pVelocidadeMinima,
			boolean pSomenteComFoto) {

		//			Envio os dados da tabela Veiculo Posicao inseridos, no caso somentes as minhas posicoes

		String vResultPost = null;
//		String vResultPost = HelperHttpPost.getConteudoStringPost(
//				pContext, 
//				OmegaConfiguration.URL_REQUEST_TOTAL_TUPLAS_RASTREAR_ROTA_USUARIO(), 
//				new String[]{
//						"usuario", 
//						"id_corporacao",
//						"corporacao", 
//						"senha", 
//						"veiculo", 
//						"p_usuario", 
//						"data_inicio", 
//						"hora_inicio", 
//						"data_fim", 
//						"hora_fim", 
//						"velocidade_minima",
//				"somente_com_foto"}, 
//				new String[]{
//						OmegaSecurity.getIdUsuario(),
//						OmegaSecurity.getIdCorporacao(),
//						OmegaSecurity.getCorporacao(),
//						OmegaSecurity.getSenha(),
//						pIdVeiculo, 
//						pIdUsuario, 
//						HelperDate.getDataFormatadaSQL(pDataInicio), 
//						pHoraInicio, 
//						HelperDate.getDataFormatadaSQL(pDataFim), 
//						pHoraFim, 
//						pVelocidadeMinima,
//						HelperString.valueOfBooleanSQL(pSomenteComFoto)});
		if(vResultPost == null) return null;
		else if (vResultPost.length() == 0 ) return 0;
		else {
			Integer vTotal = HelperInteger.parserInt(vResultPost);
			return vTotal;
		}
	}
	//Null if not has result
	// False if not is finished the search
	// True if is finished the search
	public static Integer rastrearRotaUsuario(
			Context pContext, 
			EXTDAOUsuarioPosicaoRastreamento pObj, 
			int pLimiteInferior, 
			String pIdUsuario, 
			String pIdVeiculo, 
			String pDataInicio, 
			String pHoraInicio, 
			String pDataFim, 
			String pHoraFim, 
			String pVelocidadeMinima,
			boolean pSomenteComFoto) throws Exception {

		//			Envio os dados da tabela Veiculo Posicao inseridos, no caso somentes as minhas posicoes

		//			int numberRowsAffected = synchronizeReceiveDatabase.receiveData(EXTDAOVeiculoUsuarioPosicao.NAME);
		//			return numberRowsAffected;
//		String vResultPost = HelperHttpPost.getConteudoStringPost(
//				pContext, 
//				OmegaConfiguration.URL_REQUEST_RASTREAR_ROTA_USUARIO(), 
//				new String[]{
//						"usuario", 
//						"id_corporacao", "corporacao", 
//						"senha", 
//						"limite_inferior", 
//						"numero_tuplas", 
//						"veiculo", 
//						"p_usuario", 
//						"data_inicio", 
//						"hora_inicio", 
//						"data_fim", 
//						"hora_fim", 
//						"velocidade_minima",
//				"somente_com_foto"}, 
//				new String[]{
//						OmegaSecurity.getIdUsuario(),
//						OmegaSecurity.getIdCorporacao(), OmegaSecurity.getCorporacao(), 
//						OmegaSecurity.getSenha(),
//						String.valueOf(pLimiteInferior),
//						ContainerRastreamento.LIMIT_TUPLAS_STR,
//						pIdVeiculo, 
//						pIdUsuario, 
//						HelperDate.getDataFormatadaSQL(pDataInicio), 
//						pHoraInicio, 
//						HelperDate.getDataFormatadaSQL(pDataFim), 
//						pHoraFim, 
//						pVelocidadeMinima,
//						HelperString.valueOfBooleanSQL(pSomenteComFoto)});
		String vResultPost = null;
		if(vResultPost == null) throw new Exception("Error post");
		else if (vResultPost.length() == 0 ) return null;

		String vVetorVeiculoPosicao[] = vResultPost.split( OmegaConfiguration.DELIMITER_QUERY_UNIQUE_LINHA);

		receiveListOfPosition(vVetorVeiculoPosicao, pObj);
		if(vVetorVeiculoPosicao.length == LIMIT_TUPLAS)
			return vVetorVeiculoPosicao.length;
		else return vVetorVeiculoPosicao.length;
		//			receiveListOfCarAndInsertInDatabase(vVetorVeiculoPosicao);

	}
	
	

	public boolean hasMoreResult(){
		return hasMoreResult;
	}

	private static int receiveListOfPosition(String[] pVetorVeiculoPosicao, EXTDAOUsuarioPosicaoRastreamento pObj){
		try{
			if(pVetorVeiculoPosicao == null) return 0;
			else if(pVetorVeiculoPosicao.length == 0) return 0;
			else{
				int vNumberOfInsert = 0 ;

				for (String vToken : pVetorVeiculoPosicao) {
					String vVetorColumn[] = vToken.split(OmegaConfiguration.DELIMITER_QUERY_UNIQUE_COLUNA);
					if(vVetorColumn.length == 9){

						pObj.clearData();

						pObj.setAttrValue(EXTDAOUsuarioPosicaoRastreamento.ID, HelperString.checkIfIsNull(vVetorColumn[0]));
						String vDatetime  = HelperString.checkIfIsNull(vVetorColumn[1]);
						String vData = null;
						String vHora = null;
						if(vDatetime  != null){
							String vVetorDatetime[] = vToken.split( OmegaConfiguration.DELIMITER_QUERY_UNIQUE_COLUNA);
							if(vVetorDatetime.length == 2){
								vData = vVetorDatetime[0];
								vHora = vVetorDatetime[1];
							}
						}
						pObj.setAttrValue(EXTDAOUsuarioPosicaoRastreamento.DATA_DATE, vData);
						pObj.setAttrValue(EXTDAOUsuarioPosicaoRastreamento.HORA_TIME, vHora);
						pObj.setAttrValue(EXTDAOUsuarioPosicaoRastreamento.VEICULO_USUARIO_ID_INT, HelperString.checkIfIsNull(vVetorColumn[2]));
						pObj.setAttrValue(EXTDAOUsuarioPosicaoRastreamento.USUARIO_ID_INT, HelperString.checkIfIsNull(vVetorColumn[3]));
						pObj.setAttrValue(EXTDAOUsuarioPosicaoRastreamento.LATITUDE_INT, HelperString.checkIfIsNull(vVetorColumn[4]));
						pObj.setAttrValue(EXTDAOUsuarioPosicaoRastreamento.LONGITUDE_INT, HelperString.checkIfIsNull(vVetorColumn[5]));
						pObj.setAttrValue(EXTDAOUsuarioPosicaoRastreamento.VELOCIDADE_INT, HelperString.checkIfIsNull(vVetorColumn[6]));
						pObj.setAttrValue(EXTDAOUsuarioPosicaoRastreamento.FOTO_INTERNA, HelperString.checkIfIsNull(vVetorColumn[7]));
						pObj.setAttrValue(EXTDAOUsuarioPosicaoRastreamento.FOTO_EXTERNA, HelperString.checkIfIsNull(vVetorColumn[8]));
						pObj.insert(false);
						//					RastrearVeiculoUsuarioPosicaoMapActivity.listEXTDAOVeiculoUsuarioPosicao.add( vObj);

						vNumberOfInsert += 1;
					}

				}
				return vNumberOfInsert;
			}
		} catch(Exception ex){
			return 0;
		}
	}

}
