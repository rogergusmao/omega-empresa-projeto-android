package app.omegasoftware.pontoeletronico.common.container;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

public class ContainerRouteInformation{
	public Float distancia =null;
	public String unidadeDistancia=null;
	public Integer tempoHora=null;
	public Integer tempoMinuto=null;
	public Integer tempoDia=null;
	public ContainerRouteInformation(Float pDistancia,
			String pUnidadeDistancia, Integer pTempoDia, Integer pTempoHora, Integer pTempoMinuto){
		distancia = pDistancia;
		unidadeDistancia = pUnidadeDistancia;
		tempoDia = pTempoDia;
		tempoHora = pTempoHora;
		tempoMinuto = pTempoMinuto;
	}

	public ContainerRouteInformation(Float pDistancia,
			String pUnidadeDistancia, LinkedHashMap<String, Integer> pHashDate){
		distancia = pDistancia;
		unidadeDistancia = pUnidadeDistancia;
		
		
		Set<String> vListKeyDate =  pHashDate.keySet();
		Iterator<String> it = vListKeyDate.iterator();
		while(it.hasNext()) {
			String vToken  =it.next();
			if(vToken.contains("day")){
				tempoDia = pHashDate.get(vToken);
			} else if(vToken.contains("hour")){
				tempoHora = pHashDate.get(vToken);
			} else if(vToken.contains("min")){
				tempoMinuto = pHashDate.get(vToken);		
			}  
		}
	}

	public String printDistancia(){
		if(distancia != null && unidadeDistancia != null)
			return String.valueOf(distancia) + " " + unidadeDistancia;
		else return null;
	}

	public String printTempo(){
		return printTempo(tempoDia, tempoHora, tempoMinuto);
	}
	
	public static String printTempo(Integer dia, Integer hora, Integer minuto){
		String vStrTempo = "";
		boolean vValidade = false;
		if(dia != null ){
			if(dia > 0 ){
				if(dia == 1)
					vStrTempo += String.valueOf(dia) + " dia" ;
				else  if(dia > 1)
					vStrTempo += String.valueOf(dia) + " dias" ;
				vValidade = true;
			}
			
		}
		if(hora != null){
			if(hora > 0 ){
			if(vValidade){
				vStrTempo += " " ;
			}
			if(hora == 1)
				vStrTempo += String.valueOf(hora) + " hora" ;
			else if(hora > 1) 
				vStrTempo += String.valueOf(hora) + " horas" ;
			vValidade = true;
			}
		}

		if(minuto != null){
			if(minuto > 0){
				if(vValidade){
					vStrTempo += " " ;
				}
				if(minuto == 1)
					vStrTempo += String.valueOf(minuto) + " min" ;
				else if(minuto > 1)
					vStrTempo += String.valueOf(minuto) + " mins" ;
				vValidade = true;	
			}
			
		}
		return vStrTempo;
	}

	public Integer getTempoHora(){
		return tempoHora;
	}

	public Integer getTempoDia(){
		return tempoDia;
	}

	public Integer getTempoMinuto(){
		return tempoMinuto;
	}

	public String getUnidadeDistancia(){
		return unidadeDistancia;
	}

	public Float getDistancia(){
		return distancia;
	}
}

