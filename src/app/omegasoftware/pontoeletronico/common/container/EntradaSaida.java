package app.omegasoftware.pontoeletronico.common.container;

import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto.Ponto;

public class EntradaSaida{
	public Ponto entrada;
	public Ponto saida;
	public String idPessoa ;
	public String idEmpresa;
	public String idProfissao;
	
	public String pessoa;
	public String empresa;
	public String profissao;
	public boolean bateuSaida(){
		return saida == null ? false : true;
	}
	
	public boolean bateuEntrada(){
		return entrada == null ? false : true;
	}
}