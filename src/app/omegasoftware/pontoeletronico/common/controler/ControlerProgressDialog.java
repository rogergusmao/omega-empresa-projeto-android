package app.omegasoftware.pontoeletronico.common.controler;

import android.app.Activity;
import android.app.ProgressDialog;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;

public class ControlerProgressDialog {
	ProgressDialog progressDialog = null;
	Activity activity;
	public ControlerProgressDialog(Activity activity){
		this.activity = activity;
	}
	public void createProgressDialog(){
		try{
			if(progressDialog == null || 
					(progressDialog != null && !progressDialog.isShowing())){
				
				progressDialog = new ProgressDialog(activity);
				progressDialog.setCancelable(true);
				progressDialog.setMessage(activity.getResources().getString(R.string.string_loading));
				progressDialog.show();
//				activity.showDialog(OmegaConfiguration.ON_LOAD_DIALOG);
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.UTIL_ANDROID);
		}
		
	}
	public boolean isShowing(){
		if(progressDialog == null) return false;
		else return progressDialog.isShowing();
	}
	public ProgressDialog getDialog(){
		return progressDialog;
	}
	
	public void dismissProgressDialog(){
		try{
			if(progressDialog != null){
				if(progressDialog.isShowing())
					try{
						progressDialog.dismiss();		
					}catch(Exception ex){
						
					}
				
				
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.UTIL_ANDROID);
		}
	}
	
}
