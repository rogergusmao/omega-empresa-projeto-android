package app.omegasoftware.pontoeletronico.common.controler;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;

import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuarioServico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioServico;
import app.omegasoftware.pontoeletronico.primitivetype.HelperBoolean;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class ControlerUsuarioServico {




	public static  void saveServicoStatus(
			Context pContext, 
			Database pDatabase, 
			String pIdUsuario, 
			int pIdServico, 
			boolean pNewStatus, 
			boolean pIsToSync)
	{	
		boolean status =true;
		EXTDAOUsuarioServico vObj = EXTDAOUsuarioServico.getObjUsuarioServico(
				pDatabase, 
				pIdUsuario, 
				String.valueOf(pIdServico));
		if(vObj == null) 
			status = false;
		else {
			String vStatus = vObj.getStrValueOfAttribute(EXTDAOUsuarioServico.STATUS_BOOLEAN);
			status = HelperBoolean.valueOfStringSQL(vStatus);	
		}
		Long id = null;
		
		if(status != pNewStatus || vObj == null){
			
			if(vObj != null){
				vObj.setAttrValue(EXTDAOUsuarioServico.SERVICO_ID_INT, String.valueOf(pIdServico));
				vObj.setAttrValue(EXTDAOUsuarioServico.USUARIO_ID_INT, pIdUsuario);
				vObj.setAttrValue(EXTDAOUsuarioServico.STATUS_BOOLEAN, HelperString.valueOfBooleanSQL(pNewStatus));
				vObj.setAttrValue(EXTDAOUsuarioServico.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
				vObj.formatToSQLite();
				if(!vObj.update(pIsToSync)){
				
					SingletonLog.insereErro(new Exception("Nuo conseguiu atualizar o status do servico: " + id), TIPO.PAGINA);
				}
				id = HelperInteger.parserLong( vObj.getStrValueOfAttribute(EXTDAOUsuarioServico.ID));
				
			} else {

				vObj = new EXTDAOUsuarioServico(pDatabase);
				vObj.setAttrValue(EXTDAOUsuarioServico.SERVICO_ID_INT, String.valueOf(pIdServico));
				vObj.setAttrValue(EXTDAOUsuarioServico.USUARIO_ID_INT, pIdUsuario);
				vObj.setAttrValue(EXTDAOUsuarioServico.STATUS_BOOLEAN, HelperString.valueOfBooleanSQL(pNewStatus));
				vObj.setAttrValue(EXTDAOUsuarioServico.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
				id = vObj.insert(true);
				
				
				
				
			}
			
		}

	}
//	public static void clearVeiculoUsuario(
//			Context pContext,
//			Database pDatabase,
//			String pIdUsuario){
//
//		OmegaSecurity.clearVeiculoUsuario();
//		EXTDAOVeiculoUsuarioServico.removeObjVeiculoUsuarioServico(pDatabase, pIdUsuario);
//	}
//
//	public static void saveVeiculoUsuario(Context pContext, Database pDatabase, Integer pIdVeiculoUsuario)
//	{
//		if(pIdVeiculoUsuario == null){
//			return;
//		}
//		else {
//			EXTDAOVeiculoUsuarioServico vObj = EXTDAOVeiculoUsuarioServico.addIfNecessaryVeiculoUsuarioServico(pDatabase, String.valueOf(pIdVeiculoUsuario) );
//			if(vObj != null)
//				OmegaSecurity.setIdVeiculoUsuario(String.valueOf(pIdVeiculoUsuario));
//		}
//	}
//	//Return veiculo_usuario_id
//	public static Integer getSavedVeiculoUsuarioServico(Context pContext, Database pDatabase, String pIdUsuario)
//	{
//		EXTDAOVeiculoUsuarioServico vObjVeiculoUsuarioServico = EXTDAOVeiculoUsuarioServico.getObjVeiculoUsuarioServico(pDatabase, pIdUsuario);
//		if(vObjVeiculoUsuarioServico != null)
//			return HelperInteger.parserInteger(vObjVeiculoUsuarioServico.getStrValueOfAttribute(EXTDAOVeiculoUsuarioServico.ID));
//		else return null;
//	}

	public static boolean getSavedServico(Context pContext, Database pDatabase, String pIdUsuario, int pIdServico)
	{	
		String q = "SELECT " + DAOUsuarioServico.STATUS_BOOLEAN 
				+ " FROM " + DAOUsuarioServico.NAME 
				+ " WHERE " + DAOUsuarioServico.USUARIO_ID_INT+ " = ? "  
				+ " AND " + DAOUsuarioServico.SERVICO_ID_INT + " = ? "
				+ " LIMIT 0,1 " ;
		
		Cursor c = null;
		try {
			c = pDatabase.rawQuery(q, new String [] { pIdUsuario, String.valueOf( pIdServico)});
			if(c.moveToFirst()){
				
				Boolean status = HelperBoolean.valueOfStringSQL( c.getString(0));
				return status == null ? false : status;
				
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		} finally{
			if(c != null && !c.isClosed()) c.close();
		}
		return false;
//		
//		EXTDAOUsuarioServico vObj = EXTDAOUsuarioServico.getObjUsuarioServico(
//				pDatabase, 
//				pIdUsuario, 
//				String.valueOf(pIdServico));
//		if(vObj == null) return false;
//		String id = vObj.getStrValueOfAttribute(EXTDAOUsuarioServico.ID);
//		
//		String vStatus = vObj.getStrValueOfAttribute(EXTDAOUsuarioServico.STATUS_BOOLEAN);
//		return HelperBoolean.valueOfStringSQL(vStatus);
		
		
		 
	}
	
	

	public static Integer[] getListaIdServicoAtivoNoStatus(Context context, Database database, String pIdUsuario, boolean status)
	{	
		String q = "SELECT " + DAOUsuarioServico.SERVICO_ID_INT 
				+ " FROM " + DAOUsuarioServico.NAME 
				+ " WHERE " + DAOUsuarioServico.USUARIO_ID_INT+ " = ? "
				+ " AND " + DAOUsuarioServico.STATUS_BOOLEAN + " = ? "
				+ " LIMIT 0,1 " ;
		
		ArrayList<Integer> list = new ArrayList<Integer>();
		Cursor c = null;
		try {
			c = database.rawQuery(q, new String [] { pIdUsuario,  HelperString.valueOfBooleanSQL(status)});
			if(c.moveToFirst()){
				do{
					list.add(c.getInt(0));
				}while(c.moveToNext());
				
				
			}
			Integer[] ret = new Integer[list.size()];
			list.toArray(ret);
			return ret;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			return null;
		} finally{
			if(c != null) c.close();
		}
		
		 
	}
}
