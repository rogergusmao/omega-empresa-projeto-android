package app.omegasoftware.pontoeletronico.common.controler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import app.omegasoftware.pontoeletronico.TabletActivities.FactoryMenuActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaAuthenticatorActivity.ContainerAuthentication;

public class LoaderAsyncTask extends AsyncTask<String, Integer, Boolean>
{
	public static String TAG = "LoaderTask";
	
	Context context;
	ContainerAuthentication container;
	Class<?> classe;
	String tag;
	ControlerProgressDialog controlerProgressDialog;
	
	Activity activity;
	
	Exception __ex2 = null;
	public LoaderAsyncTask(FactoryMenuActivityMobile pActivity, Class<?> pclasse){
		activity = pActivity;
		context = activity.getApplicationContext();
		classe = pclasse;
		controlerProgressDialog = new ControlerProgressDialog(pActivity);
	}
	
	public LoaderAsyncTask(FactoryMenuActivityMobile pActivity, Class<?> pclasse, String pTag){
		activity = pActivity;
		classe = pclasse;
		context = activity.getApplicationContext();
		tag = pTag;
		controlerProgressDialog = new ControlerProgressDialog(pActivity);
	}
	
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		controlerProgressDialog.createProgressDialog();
	}
	Intent intent = null;
	@Override
	protected Boolean doInBackground(String... params) {
		try{
			intent = new Intent(
					activity.getApplicationContext(), 
					classe);
			
			if(tag != null)
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME, tag);
			
		
	} catch (Exception e) {
		__ex2 = e;
	}
		return false;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		try{
			
			if(this.__ex2!=null) {
				SingletonLog.openDialogError(activity, __ex2, SingletonLog.TIPO.UTIL_ANDROID);
			} else {
				activity.startActivity(intent);				
			}
		} catch (Exception ex) {
			SingletonLog.openDialogError(activity, ex, SingletonLog.TIPO.PAGINA);
		} finally{
			controlerProgressDialog.dismissProgressDialog();
		}
		
		
	}

}