package app.omegasoftware.pontoeletronico.common.controler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaAuthenticatorActivity.ContainerAuthentication;

public class LoaderRunnable 
{
	public static String TAG = "LoaderRunnable";
	Activity activity;
	Context context;
	ContainerAuthentication container;
	Class<?> classe;
	String tag;
	ControlerProgressDialog controlerProgressDialog;
	
	
	public LoaderRunnable(Activity pActivity, Class<?> pclasse, String pTag){
		activity = pActivity;
		classe = pclasse;
		context = activity.getApplicationContext();
		tag = pTag;
		controlerProgressDialog = new ControlerProgressDialog(pActivity);
	}
	boolean validadeDialog = true ;
	public void execute(){
		
		if(!activity.isFinishing()){
			validadeDialog = true ;
			
			final ProgressDialog dialog = new ProgressDialog(activity);
			try{
				dialog.show();	
			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.PAGINA);
				validadeDialog = false;
			}
			
			new Thread(new Runnable() {

		        public void run() {
		        	
		        	Intent intent = new Intent(
							activity.getApplicationContext(), 
							classe);
		        	if(tag != null)
						intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME, tag);
					activity.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_EDIT);
					try{
						if(dialog != null && validadeDialog)
							dialog.dismiss();	
					}catch(Exception ex){
						SingletonLog.insereErro(ex, TIPO.PAGINA);
					}
		            
		        }
		    }).start();

		} else{
			Intent intent = new Intent(
					activity.getApplicationContext(), 
					classe);
        	if(tag != null)
				intent.putExtra(OmegaConfiguration.SEARCH_FIELD_NOME, tag);
			activity.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_EDIT);
		}
			
		
	    	}
	

}