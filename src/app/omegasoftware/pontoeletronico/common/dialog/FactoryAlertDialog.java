package app.omegasoftware.pontoeletronico.common.dialog;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class FactoryAlertDialog {
	static final int TEMPO_DURACAO_TAOST_SEGUNDOS = 1;
	public static void makeToast(Context c, Exception ex, String msgUsuario){
		SingletonLog.insereErro(ex, TIPO.PAGINA);
		if(msgUsuario == null || msgUsuario.length() ==0){
			msgUsuario = c.getString(R.string.erro_fatal);
		}
		Toast.makeText(c, msgUsuario, TEMPO_DURACAO_TAOST_SEGUNDOS).show();
	}
	
	public static void showDialog(Activity a, Exception ex){
		SingletonLog.insereErro(ex, TIPO.PAGINA);
		a.showDialog(HelperDialog.DIALOG_ERRO_FATAL);
	}
	public static void showDialog(Activity a, JSONObject msg){
		if(SingletonLog.insereErroSeExistente(msg, TIPO.PAGINA)){
			
			a.showDialog(HelperDialog.DIALOG_ERRO_FATAL);
		} else {
			String strMsg;
			try {
				strMsg = msg.getString("mMensagem");
				AlertDialog.Builder builder = new AlertDialog.Builder(a);
				builder.setMessage(strMsg)
			    .setNeutralButton(a.getString(R.string.ok_button), new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int id) {
		                dialog.cancel();
		            }
		        })
			    .show();
			} catch (JSONException e) {
				SingletonLog.insereErro(e, TIPO.UTIL_ANDROID);
				a.showDialog(HelperDialog.DIALOG_ERRO_FATAL);
			}
		}
	}
	
	public static void showDialog(Activity a, InterfaceMensagem msg){
		if(msg != null && msg.erro()){
			SingletonLog.insereErroSeExistente(msg, TIPO.PAGINA);
			HelperDialog.factoryDialogErroFatal(a);
		} else {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(a);
			builder.setMessage(
					msg.mMensagem 
			)
		    .setNeutralButton(a.getString(R.string.ok_button), new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int id) {
	                dialog.cancel();
	            }
	        })
		    .show();
		}
	}

	public static AlertDialog.Builder factoryAlertDialog(
			Context context, 
			String pergunta, 
			DialogInterface.OnClickListener dialogClickListener){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(
			pergunta 
		)
			.setPositiveButton(context.getString(R.string.yes), dialogClickListener)
		    .setNegativeButton(context.getString(R.string.no), dialogClickListener
		);
		return builder; 
	}
	
	public static void showAlertDialog(
			Context context, 
			String pergunta, 
			DialogInterface.OnClickListener dialogClickListener){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(
			pergunta 
		)
			.setPositiveButton(context.getString(R.string.yes), dialogClickListener)
		    .setNegativeButton(context.getString(R.string.no), dialogClickListener)
		    .show();
	}
	
	
	public static void showDialog(
			Context context, 
			String mensagem){
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(
			mensagem 
		)	
		.setCancelable(true)
		.setPositiveButton(context.getString(R.string.ok_button), null)
		.show();

//		Dialog d=  new Dialog(context);
//		d.setTitle(mensagem);
//		d.setMessage(mensagem);
//		d.show();
	}
	
	public static void showDialogOk(
			Context context, 
			String pergunta, 
			DialogInterface.OnClickListener dialogClickListener){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(
			pergunta 
		)
		.setPositiveButton(context.getString(R.string.ok_button), dialogClickListener)
		.show();
	}
}
