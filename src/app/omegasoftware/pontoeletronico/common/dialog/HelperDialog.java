package app.omegasoftware.pontoeletronico.common.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaCadastroActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.controler.ControlerProgressDialog;
import app.omegasoftware.pontoeletronico.common.thread.HelperThread;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.listener.BrowserOpenerListener;
import app.omegasoftware.pontoeletronico.log.BOLogApp;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class HelperDialog {
	
	public static final int FORM_ERROR_DIALOG_SERVER_OFFLINE = 887;
	public static final int FORM_ERROR_DIALOG_SERVER_OFFLINE_2 = -7;
	public static final int FORM_ERROR_DIALOG_DESCONECTADO = 75;
	public static final int DIALOG_ERRO_FATAL = 1001;
	public static final int ERROR_DIALOG_EMAIL_INVALIDO = 888;
	public static final int DIALOG_CADASTRO_OK = 771;
	public static final int FORM_DIALOG_ATUALIZACAO_OK = 73;
	public static final int FORM_ERROR_DIALOG_INVALIDO_EMAIL = 777;
	public static final int FORM_ERROR_DIALOG_MISSING_NOME = 889;
	public static final int FORM_ERROR_DIALOG_MISSING_EMAIL = 890;
	public static final int FORM_ERROR_DIALOG_MISSING_SENHA = 891;
	
	public static final int ON_LOAD_DIALOG = -3;
	public static final int SEARCH_NO_RESULTS_DIALOG = -1;
	public static final int FORM_ERROR_DIALOG_INFORMACAO_DIALOG = -4;
	public static final int FORM_DIALOG_INFORMACAO_DIALOG_ENDERECO = -5;
	public static final int INFORMACAO_PESSOA_DUPLICIDADE = -254;
	public static final int FORM_ERROR_DIALOG_TELEFONE_OFFLINE = -6;

	public static final int FORM_DIALOG_AUTENTICACAO_INVALIDA = -987;

	public static final int FORM_DIALOG_AGRADECIMENTO = -966;
	public static final int ERROR_DIALOG_INVALID_EMAIL = 8844;
	public static final int FORM_EXCEDEU_PACOTE = 54687;

	
	public static class EnviaLogAsyncTask extends AsyncTask<Bundle, Integer, Boolean> {
		public String TAG = "EnviaLogAsyncTask";
		ControlerProgressDialog controlerProgressDialog;
		Activity a =null;
		public EnviaLogAsyncTask(Activity a) {
			this.a=a;
			controlerProgressDialog=new ControlerProgressDialog(a);
			controlerProgressDialog.createProgressDialog();
		}



		@Override
		protected Boolean doInBackground(Bundle... params) {
			try {
				HelperThread.sleep(1000);
				BOLogApp.enviaLogImediato(a);
			} catch (Exception e) {
				SingletonLog.insereErro(e, SingletonLog.TIPO.SINCRONIZADOR);
			}

			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				controlerProgressDialog.dismissProgressDialog();
				Toast.makeText(a, R.string.log_enviado, Toast.LENGTH_SHORT).show();

			} catch (Exception ex) {
				SingletonLog.openDialogError(this.a, ex, SingletonLog.TIPO.PAGINA);
			} finally{
				controlerProgressDialog.dismissProgressDialog();
			}

		}
		public static EnviaLogAsyncTask factory(Activity a)
		{
			return new EnviaLogAsyncTask(a);
		}
	}
	
	public static void factoryDialogErroFatal(final Activity a) {
		AlertDialog.Builder builder = new AlertDialog.Builder(a);
		final EnviaLogAsyncTask t = new EnviaLogAsyncTask(a);
		builder.setMessage(a.getResources().getString(R.string.erro_fatal)).setCancelable(true)
				.setNegativeButton(a.getString(R.string.fechar), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				})
				// oferece o botuo de acesso ao sistema de cobranua para
				// expansuo do pacote
				.setPositiveButton(a.getString(R.string.reportar_erro), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
							t.execute();
						
					}
				});
		builder.show();
	}

	public static void factoryExcedeuPacote(final Activity a, String msg){
		AlertDialog.Builder builder = new AlertDialog.Builder(a);
		builder.setMessage(
			msg
		)	
		.setCancelable(true)
		.setNegativeButton(a.getString(R.string.fechar), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        })
		//oferece o botuo de acesso ao sistema de cobranua para expansuo do pacote
		.setPositiveButton(
				a.getString(R.string.website_sistema_cobranca), 
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						BrowserOpenerListener.actionrBrowserCript(
								a, 
								OmegaConfiguration.BASE_URL_OMEGA_SOFTWARE() + "client_area/actions.php?class=Servicos_web&action=actionPacotes", 
								new String[] {"email", "corporacao", "senha"}, 
								new String[] {OmegaSecurity.getEmail(), OmegaSecurity.getCorporacao(), OmegaSecurity.getSenha()}, 
								a.getResources().getString(R.string.sistema_cobranca_site_url));
						
					}
				}
		);
		builder.show();
	}
	
	public static Dialog getDialog(final Activity a, int id) {

		if(id == DIALOG_ERRO_FATAL){
			factoryDialogErroFatal(a);
			return null;
		}

		Dialog vDialog = new Dialog(a);
		vDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		vDialog.setContentView(R.layout.dialog);

		TextView vTextView = (TextView) vDialog.findViewById(R.id.tv_dialog);
		Button vOkButton = (Button) vDialog.findViewById(R.id.dialog_ok_button);

		try {
			switch (id) {
			case ERROR_DIALOG_INVALID_EMAIL:
				vTextView.setText(a.getResources().getString(R.string.email_invalido));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						a.dismissDialog(ERROR_DIALOG_INVALID_EMAIL);
					}
				});
				break;
			case FORM_DIALOG_AGRADECIMENTO:
				vTextView.setText(a.getResources().getString(R.string.agradecimento));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						a.dismissDialog(FORM_DIALOG_AGRADECIMENTO);
					}
				});
				break;
			

			case FORM_ERROR_DIALOG_INVALIDO_EMAIL:
				vTextView.setText(a.getResources().getString(R.string.email_invalido));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						a.dismissDialog(FORM_ERROR_DIALOG_INVALIDO_EMAIL);
					}
				});
				break;
			case FORM_DIALOG_ATUALIZACAO_OK:
				vTextView.setText(a.getResources().getString(R.string.atualizacao_ok));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						a.dismissDialog(FORM_DIALOG_ATUALIZACAO_OK);
					}
				});
				break;

			case FORM_ERROR_DIALOG_MISSING_NOME:
				vTextView.setText(a.getResources().getString(R.string.error_dialog_missing_nome));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						a.dismissDialog(FORM_ERROR_DIALOG_MISSING_NOME);
					}
				});
				break;

			case FORM_ERROR_DIALOG_MISSING_SENHA:
				vTextView.setText(a.getResources().getString(R.string.error_dialog_missing_senha));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						a.dismissDialog(FORM_ERROR_DIALOG_MISSING_SENHA);
					}
				});
				break;

			case FORM_ERROR_DIALOG_MISSING_EMAIL:
				vTextView.setText(a.getResources().getString(R.string.error_dialog_missing_email));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						a.dismissDialog(FORM_ERROR_DIALOG_MISSING_EMAIL);
					}
				});
				break;

						case ERROR_DIALOG_EMAIL_INVALIDO:
				vTextView.setText(a.getResources().getString(R.string.form_email_invalido));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						a.dismissDialog(ERROR_DIALOG_EMAIL_INVALIDO);
					}
				});
				break;

			case SEARCH_NO_RESULTS_DIALOG:
				vTextView.setText(a.getResources().getString(R.string.no_result_usuario_permissao_categoria_permissao));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						a.dismissDialog(OmegaConfiguration.SEARCH_NO_RESULTS_DIALOG);
						// finish();
					}
				});
				break;
			case DIALOG_CADASTRO_OK:
				vTextView.setText(a.getResources().getString(R.string.cadastro_ok));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						a.dismissDialog(DIALOG_CADASTRO_OK);
					}
				});
				break;
			case FORM_ERROR_DIALOG_DESCONECTADO:
				vTextView.setText(a.getResources().getString(R.string.error_form_desconectado));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						a.dismissDialog(FORM_ERROR_DIALOG_DESCONECTADO);
					}
				});
				break;
			case FORM_ERROR_DIALOG_SERVER_OFFLINE_2:
			case FORM_ERROR_DIALOG_SERVER_OFFLINE:
				vTextView.setText(a.getResources().getString(R.string.error_servidor_offline));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						a.dismissDialog(OmegaConfiguration.FORM_ERROR_DIALOG_SERVER_OFFLINE);
					}
				});
				break;
			case FORM_ERROR_DIALOG_TELEFONE_OFFLINE:
				vTextView.setText(a.getResources().getString(R.string.is_desconectado));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						a.dismissDialog(OmegaConfiguration.FORM_ERROR_DIALOG_TELEFONE_OFFLINE);
					}
				});
				break;
			case OmegaConfiguration.FORM_DIALOG_AUTENTICACAO_INVALIDA:
				vTextView.setText(a.getResources().getString(R.string.autenticacao_invalida));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						RotinaSincronizador.actionLogout(v.getContext(), true);
					}
				});
				break;

			default:
				// vDialog = this.getErrorDialog(id);
				return null;
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			factoryDialogErroFatal(a);
			return null;
		}
		return vDialog;
	}
}
