package app.omegasoftware.pontoeletronico.common.fisica;

public class VelocidadeUniforme {
	
	
	public Integer distanciaMetro = null;
	public Integer tempoSegundo = null;
	
	public VelocidadeUniforme(Integer pDistanciaMetro, Integer pTempoSegundo){
		distanciaMetro = pDistanciaMetro;
		tempoSegundo = pTempoSegundo;
	}
}
