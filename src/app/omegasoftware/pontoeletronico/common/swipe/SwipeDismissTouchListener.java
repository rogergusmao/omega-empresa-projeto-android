package app.omegasoftware.pontoeletronico.common.swipe;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * A {@link View.OnTouchListener} that makes any {@link View} dismissable when the
 * user swipes (drags her finger) horizontally across the view.
 *
 * <p><em>For {@link ListView} list items that don't manage their own touch events
 * (i.e. you're using
 * {@link ListView#setOnItemClickListener(AdapterView.OnItemClickListener)}
 * or an equivalent listener on {@link ListActivity} or
 * {@link ListFragment}, use {@link SwipeDismissListViewTouchListener} instead.</em></p>
 *
 * <p>Example usage:</p>
 *
 * <pre>
 * view.setOnTouchListener(new SwipeDismissTouchListener(
 *         view,
 *         null, // Optional token/cookie object
 *         new SwipeDismissTouchListener.OnDismissCallback() {
 *             public void onDismiss(View view, Object token) {
 *                 parent.removeView(view);
 *             }
 *         }));
 * </pre>
 *
 * <p>This class Requires API level 12 or later due to use of {@link
 * android.view.ViewPropertyAnimator}.</p>
 *
 * @see SwipeDismissListViewTouchListener
 */
public class SwipeDismissTouchListener implements View.OnTouchListener {
    // Cached ViewConfiguration and system-wide constant values
    private int mSlop;
    private int mMinFlingVelocity;
    private int mMaxFlingVelocity;
    private long mAnimationTime;

    // Fixed properties
    private View mView;
    private DismissCallbacks mCallbacks;
    private int mViewWidth = 1; // 1 and not 0 to prevent dividing by zero

    // Transient properties
    private float mDownX;
    private boolean mSwiping;
    private Object mToken;
    private VelocityTracker mVelocityTracker;
    private float mTranslationX;

    /**
     * The callback interface used by {@link SwipeDismissTouchListener} to inform its client
     * about a successful dismissal of the view for which it was created.
     */
    public interface DismissCallbacks {
        /**
         * Called to determine whether the view can be dismissed.
         */
        boolean canDismiss(Object token);

        /**
         * Called when the user has indicated they she would like to dismiss the view.
         *
         * @param view  The originating {@link View} to be dismissed.
         * @param token The optional token passed to this object's constructor.
         */
        void onDismiss(View view, Object token);
    }

    /**
     * Constructs a new swipe-to-dismiss touch listener for the given view.
     *
     * @param view     The view to make dismissable.
     * @param token    An optional token/cookie object to be passed through to the callback.
     * @param callbacks The callback to trigger when the user has indicated that she would like to
     *                 dismiss this view.
     */
    public SwipeDismissTouchListener(View view, Object token, DismissCallbacks callbacks) {
        ViewConfiguration vc = ViewConfiguration.get(view.getContext());
        mSlop = vc.getScaledTouchSlop();
        mMinFlingVelocity = vc.getScaledMinimumFlingVelocity() * 16;
        mMaxFlingVelocity = vc.getScaledMaximumFlingVelocity();
        mAnimationTime = view.getContext().getResources().getInteger(
                android.R.integer.config_shortAnimTime);
        mView = view;
        mToken = token;
        mCallbacks = callbacks;
    }

    
    @SuppressLint("UseValueOf")
	public boolean onTouch(View view, MotionEvent motionEvent) {
        // offset because the view is translated during swipe
        motionEvent.offsetLocation(mTranslationX, 0);

        if (mViewWidth < 2) {
            mViewWidth = mView.getWidth();
        }

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                // TODO: ensure this is a finger, and set a flag
                mDownX = motionEvent.getRawX();
                if (mCallbacks.canDismiss(mToken)) {
                    mVelocityTracker = VelocityTracker.obtain();
                    mVelocityTracker.addMovement(motionEvent);
                }
                view.onTouchEvent(motionEvent);
                return false;
            }

            case MotionEvent.ACTION_UP: {
                if (mVelocityTracker == null) {
                    break;
                }

                float deltaX = motionEvent.getRawX() - mDownX;
                mVelocityTracker.addMovement(motionEvent);
                mVelocityTracker.computeCurrentVelocity(1000);
                float velocityX = mVelocityTracker.getXVelocity();
                float absVelocityX = Math.abs(velocityX);
                float absVelocityY = Math.abs(mVelocityTracker.getYVelocity());
                boolean dismiss = false;
                boolean dismissRight = false;
                if (Math.abs(deltaX) > mViewWidth / 2) {
                    dismiss = true;
                    dismissRight = deltaX > 0;
                } else if (mMinFlingVelocity <= absVelocityX && absVelocityX <= mMaxFlingVelocity
                        && absVelocityY < absVelocityX) {
                    // dismiss only if flinging in the same direction as dragging
                    dismiss = (velocityX < 0) == (deltaX < 0);
                    dismissRight = mVelocityTracker.getXVelocity() > 0;
                }
                if (dismiss) {
                    // dismiss
//                    mView.animate()
//                            .translationX(dismissRight ? mViewWidth : -mViewWidth)
//                            .alpha(0)
//                            .setDuration(mAnimationTime)
//                            .setListener(new AnimatorListenerAdapter() {
//                                @Override
//                                public void onAnimationEnd(Animator animation) {
//                                    performDismiss();
//                                }
//                            });
                	 AnimationSet aa = new AnimationSet(true);
                     aa.setDuration(mAnimationTime);
//                     aa.setAnimationListener(new AnimationListener() {
//						
//						public void onAnimationStart(Animation animation) {
//							
//							
//						}
//						
//						public void onAnimationRepeat(Animation animation) {
//							
//							
//						}
//						
//						public void onAnimationEnd(Animation animation) {
//							performDismiss();
//							
//						}
//					});
                     
                     AlphaAnimation alphaAa = new AlphaAnimation(0f, 0f);
                     aa.addAnimation(alphaAa);
                     
                     TranslateAnimation trans = new TranslateAnimation(0, dismissRight ? mViewWidth : -mViewWidth, view.getScrollY(), view.getScrollY());
                     aa.addAnimation(trans);
                     
                     mView.startAnimation(aa);
                } else {
                    // cancel
//                    mView.animate()
//                            .translationX(0)
//                            .alpha(1)
//                            .setDuration(mAnimationTime)
//                            .setListener(null);
                    AnimationSet aa = new AnimationSet(true);
                    aa.setDuration(mAnimationTime);
                    aa.setAnimationListener(null);
                    
                    AlphaAnimation alphaAa = new AlphaAnimation(1f, 1f);
                    aa.addAnimation(alphaAa);
                    
                    TranslateAnimation trans = new TranslateAnimation(mDownX, view.getScrollX(), view.getScrollY(), view.getScrollY());
                    aa.addAnimation(trans);
                    
                    mView.startAnimation(aa);
                }
                mVelocityTracker.recycle();
                mVelocityTracker = null;
                mTranslationX = 0;
                mDownX = 0;
                mSwiping = false;
                break;
            }

            case MotionEvent.ACTION_MOVE: {
                if (mVelocityTracker == null) {
                    break;
                }

                mVelocityTracker.addMovement(motionEvent);
                float deltaX = motionEvent.getRawX() - mDownX;
                if (Math.abs(deltaX) > mSlop) {
                    mSwiping = true;
                    mView.getParent().requestDisallowInterceptTouchEvent(true);

//                    // Cancel listview's touch
                    MotionEvent cancelEvent = MotionEvent.obtain(motionEvent);
                    cancelEvent.setAction(MotionEvent.ACTION_CANCEL |
                        (motionEvent.getAction() << MotionEvent.ACTION_POINTER_ID_SHIFT));
                    mView.onTouchEvent(cancelEvent);
                    cancelEvent.recycle();
                }

                if (mSwiping) {
                    mTranslationX = deltaX;
//                    mView.setTranslationX(deltaX);
                    //ViewHelper.setTranslationX(mView, 0, deltaX);
                    // TODO: use an ease-out interpolator or such
//                    ViewHelper.setAlpha(Math.max(0f, Math.min(1f, 1f - 2f * Math.abs(deltaX) / mViewWidth)));
                    
                    //ViewHelper.setAlpha(mView);
                    
                    AnimationSet aa = new AnimationSet(true);
                    aa.setDuration(mAnimationTime);
                    aa.setAnimationListener(null);
                    
//					float alpha = Math.max(1f, Math.min(1f, 1f - 2f * Math.abs(deltaX) / mViewWidth));
                    float fatorTransp = Math.max(new Float(0.79),1f - 2f * Math.abs(deltaX) / mViewWidth);
                    AlphaAnimation alphaAa = new AlphaAnimation(1f, fatorTransp);
                    aa.addAnimation(alphaAa);
                    
                    TranslateAnimation trans = new TranslateAnimation(0, deltaX, view.getScrollY(), view.getScrollY());
                    aa.addAnimation(trans);
                    
                    mView.startAnimation(aa);
                    return true;
                }
                break;
            }
        }
        return false;
    }

}