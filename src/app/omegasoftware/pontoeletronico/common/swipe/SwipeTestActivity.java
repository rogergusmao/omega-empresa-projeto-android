package app.omegasoftware.pontoeletronico.common.swipe;

import java.util.ArrayList;
import java.util.Arrays;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import app.omegasoftware.pontoeletronico.R;

public class SwipeTestActivity extends ListActivity {
    ArrayAdapter<String> mAdapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_swipe_list_view);

        // Set up ListView example
        String[] items = new String[20];
        for (int i = 0; i < items.length; i++) {
            items[i] = "Item " + (i + 1);
        }

        mAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                new ArrayList<String>(Arrays.asList(items)));
        setListAdapter(mAdapter);

        ListView listView = getListView();
//         Create a ListView-specific touch listener. ListViews are given special treatment because
//         by default they handle touches for their list items... i.e. they're in charge of drawing
//         the pressed state (the list selector), handling list item clicks, etc.
        SwipeDismissListViewTouchListener touchListener =
                new SwipeDismissListViewTouchListener(
                        listView,
                        new SwipeDismissListViewTouchListener.DismissCallbacks() {
                            
                            public boolean canDismiss(int position) {
                                return true;
                            }

                            
                            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                    mAdapter.remove(mAdapter.getItem(position));
                                }
                                mAdapter.notifyDataSetChanged();
                            }
                        });
        listView.setOnTouchListener(touchListener);
        mAdapter.notifyDataSetChanged();
        // Setting this scroll listener is required to ensure that during ListView scrolling,
        // we don't look for swipes.
        listView.setOnScrollListener(touchListener.makeScrollListener());
//
//        // Set up normal ViewGroup example
//        final ViewGroup dismissableContainer = (ViewGroup) findViewById(R.id.dismissable_container);
//        for (int i = 0; i < items.length; i++) {
//            final Button dismissableButton = new Button(this);
//            dismissableButton.setLayoutParams(new ViewGroup.LayoutParams(
//                    ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//            dismissableButton.setText("Button " + (i + 1));
//            dismissableButton.setOnClickListener(new View.OnClickListener() {
//                
//                public void onClick(View view) {
//                    Toast.makeText(SwipeTestActivity.this,
//                            "Clicked " + ((Button) view).getText(),
//                            Toast.LENGTH_SHORT).show();
//                }
//            });
//            // Create a generic swipe-to-dismiss touch listener.
//            dismissableButton.setOnTouchListener(new SwipeDismissTouchListener(
//                    dismissableButton,
//                    null,
//                    new SwipeDismissTouchListener.DismissCallbacks() {
//                        
//                        public boolean canDismiss(Object token) {
//                            return true;
//                        }
//
//                        
//                        public void onDismiss(View view, Object token) {
//                            dismissableContainer.removeView(dismissableButton);
//                        }
//                    }));
//            dismissableContainer.addView(dismissableButton);
//        }
    }

    @Override
    protected void onListItemClick(ListView listView, View view, int position, long id) {
        Toast.makeText(this,
                "Clicked " + getListAdapter().getItem(position).toString(),
                Toast.LENGTH_SHORT).show();
    }
}
