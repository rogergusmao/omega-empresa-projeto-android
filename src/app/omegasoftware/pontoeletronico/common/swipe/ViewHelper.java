package app.omegasoftware.pontoeletronico.common.swipe;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.TranslateAnimation;

public final class ViewHelper {
	public static void setAlpha(View v){

	    AlphaAnimation aa = new AlphaAnimation(0f,1f);
	    aa.setDuration(5000);
	    v.startAnimation(aa);
	}

	public static void setTranslationX(View v, float fromXDelta, float toXDelta){
		TranslateAnimation aa = new TranslateAnimation(fromXDelta, toXDelta, 0, 0);
	    
	    aa.setDuration(5000);
	    v.startAnimation(aa);
	}
}
