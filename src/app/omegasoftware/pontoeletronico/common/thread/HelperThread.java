package app.omegasoftware.pontoeletronico.common.thread;

public class HelperThread {
	public static Thread performOnBackgroundThread(final Runnable runnable) {
		final Thread t = new Thread() {
			@Override
			public void run() {
				try {
					runnable.run();
				} catch(Exception ex){
				}
			}
		};
		t.start();
		return t;
	}
	
	public static void sleep(long mil){
		try {
			Thread.sleep(mil);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}

	
}
