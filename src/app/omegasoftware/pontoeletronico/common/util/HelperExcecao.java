package app.omegasoftware.pontoeletronico.common.util;


public class HelperExcecao {
	public static String getDescricao(Exception ex){
		if(ex == null)
			return "Exceuuo desconhecida";
		else{
			StackTraceElement vetor[] = ex.getStackTrace();
			String stack = "";
			if(vetor != null)
			for (StackTraceElement stackTraceElement : vetor) {
				
				if(!stackTraceElement.isNativeMethod()){
					
					stack += "\n</br>" + stackTraceElement.toString();
				} 
				
			}
			
			return ex.getMessage() + ".</br></br> Stack: " + stack;
		}
	}
	
	public static String getDescricao(Throwable ex){
		if(ex == null)
			return "Exceuuo desconhecida";
		else{
			StackTraceElement vetor[] = ex.getStackTrace();
			String stack = "";
			if(vetor != null)
			for (StackTraceElement stackTraceElement : vetor) {
				
				if(!stackTraceElement.isNativeMethod()){
					
					stack += "\n</br>" + stackTraceElement.toString();
				} 
				
			}
			
			return ex.getMessage() + ".</br></br> Stack: " + stack;
		}
	}
}
