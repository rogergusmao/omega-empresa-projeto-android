package app.omegasoftware.pontoeletronico.common.util;

import org.json.JSONException;
import org.json.JSONObject;

import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class HelperJson {

	public static Boolean getBoolean(JSONObject obj, String nome) throws JSONException{
		if(obj.isNull(nome)) return null;
		String strIsAdm = obj.getString(nome);
		
		return  strIsAdm != null && strIsAdm.equalsIgnoreCase("1");
	}
	
	public static String getString(JSONObject obj, String nome) throws JSONException{
		if(obj.isNull(nome)) return null;
		String v = obj.getString(nome);
		return v;
	}
	
	public static String getIntegerAsString(JSONObject obj, String nome) throws JSONException{
		if(obj.isNull(nome)) return null;
		String v = obj.getString(nome);
		if(HelperInteger.parserInteger(v) == null) return null;
		return v;
	}
	
	public static Integer getInteger(JSONObject obj, String nome) throws JSONException{
		if(obj.isNull(nome)) return null;
		String v = obj.getString(nome);
		return HelperInteger.parserInteger(v) ;
		
	}
	
}
