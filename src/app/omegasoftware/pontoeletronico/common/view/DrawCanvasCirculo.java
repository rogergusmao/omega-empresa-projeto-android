package app.omegasoftware.pontoeletronico.common.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.phone.HelperScreen;
import app.omegasoftware.pontoeletronico.phone.HelperScreen.ConfiguracaoTela;

public class DrawCanvasCirculo extends View
{
    Context context;
    int cor ;
    ConfiguracaoTela conf ;
    int raioDoCirculo;
    Paint paint = new Paint();
    int x, y;
    public DrawCanvasCirculo(Context mContext, int raioDoCirculo, int cor, int x, int y)
    {
        super(mContext);
        context = mContext;
        this.x = x;
        this.y = y;
        this.cor = cor;
        HelperScreen helper = new HelperScreen();
        conf = helper.getConfiguracaoTela(context);
        this.raioDoCirculo = raioDoCirculo;
    }
    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        try{
            paint.setColor(cor);
            paint.setAlpha(255);
            paint.setStrokeWidth(3.0f);
            paint.setStyle(Paint.Style.STROKE);
            
            canvas.drawCircle(x, y, raioDoCirculo, paint);
            invalidate();
        } catch(Exception ex){
        	SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
        }
        
    }

}