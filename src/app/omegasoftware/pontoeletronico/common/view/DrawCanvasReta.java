package app.omegasoftware.pontoeletronico.common.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.phone.HelperScreen;
import app.omegasoftware.pontoeletronico.phone.HelperScreen.ConfiguracaoTela;

public class DrawCanvasReta extends View
{
    Context context;
    int cor = 0xFF0000;
    ConfiguracaoTela conf ;
    int largura, altura;
    Paint paint = new Paint();
    int x, y;
    public DrawCanvasReta(Context mContext, int largura, int altura, int cor, int x, int y)
    {
        super(mContext);
        context = mContext;
        this.x = x;
        this.y = y;
        this.cor = cor;
        HelperScreen helper = new HelperScreen();
        conf = helper.getConfiguracaoTela(context);
        this.largura = largura;
        this.altura = altura;
    }
    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        try{
            paint.setColor(cor);
            paint.setAlpha(255);
            paint.setStrokeWidth(3.0f);
            paint.setStyle(Paint.Style.STROKE);
            
            canvas.drawLine(x, y, x + largura, y + altura, paint);
            invalidate();
        } catch(Exception ex){
        	SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
        }
        
    }

}