package app.omegasoftware.pontoeletronico.database;

import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteStatement;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.primitivetype.HelperDouble;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperLong;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class Attribute{

	public enum SQLLITE_TYPE { 
		TEXT, 
		LONG, 
		DOUBLE, 
		FLOAT,
		BLOB, 
		REAL, 
		INTEGER,
		INT,
		VARCHAR, 
		CHAR,
		TINYTEXT,
		DATETIME,	
		DATE,
		TIME};
	
	public static String TAG = "Attribute";
	
	public enum TYPE_RELATION_FK { CASCADE, SET_NULL, NO_ACTION, RESTRICT, SET_DEFAULT }; 
	
	
	
	public TYPE_RELATION_FK onUpdate = null;
	public TYPE_RELATION_FK onDelete = null;
	public String name;
	public SQLLITE_TYPE sqlType;
	public boolean isNull;
	public String strDefault;
	public boolean isPrimaryKey;
	public String strValue = null;
	public String strLabel;
	public boolean isMasculino ;
	public int size;
	public String attributeFK;
	public String tableFK;
	public String nomeFK;
	public boolean isAutoIncrement = false;
	public Attribute atributoNormalizado = null; 
//	public boolean isNormalizado = false;
	
	public Attribute( String name,
			String label,
			boolean isMasculino,
			SQLLITE_TYPE sqlType,
			boolean isNull,
			String strDefault,
			boolean isPrimaryKey,
			String value,
			int size, 
			boolean isAutoIncrement 
			){
		this.isMasculino = isMasculino;
		this.name = name;
		this.sqlType = sqlType;
		this.isNull =isNull;
		this.strDefault = strDefault;
		this.isPrimaryKey = isPrimaryKey;
		this.strValue = value;  
		this.strLabel = label;
		if(isPrimaryKey)
			this.isAutoIncrement = isAutoIncrement;
		else this.isAutoIncrement = false;
		this.size = size;
	}
	
	private Attribute( String name,
			String label,
			boolean isMasculino,
			SQLLITE_TYPE sqlType,
			boolean isNull,
			String strDefault,
			boolean isPrimaryKey,
			String value,
			int size, 
			boolean isAutoIncrement,
			boolean isNormalizado,
			Attribute pAtributoNormalizado
			){
		this.isMasculino = isMasculino;
		this.name = name;
		this.sqlType = sqlType;
		this.isNull =isNull;
		this.strDefault = strDefault;
		this.isPrimaryKey = isPrimaryKey;
		this.strValue = value;  
		this.strLabel = label;
		if(isPrimaryKey)
			this.isAutoIncrement = isAutoIncrement;
		this.size = size;
		this.atributoNormalizado = pAtributoNormalizado;
//		isNormalizado = p_isNormalizado;
	}
	
	

	public Attribute(
			Context context,
			String name,
			String tableFK,
			String attributeFK,
			String label,
			boolean isMasculino,
			SQLLITE_TYPE sqlType,
			boolean isNull,
			String strDefault,
			boolean isPrimaryKey,
			String value, 
			int size,
			TYPE_RELATION_FK onUpdate ,
			TYPE_RELATION_FK onDelete,
			String nomeFk){
		initializeOnUpdateOnDelete(name, isNull, onUpdate, onDelete);
		this.tableFK = tableFK;
		this.attributeFK = attributeFK;
		this.isMasculino = isMasculino;
		this.name = name;
		this.sqlType = sqlType;
		this.isNull =isNull;
		this.strDefault = strDefault;
		this.isPrimaryKey = isPrimaryKey;
		this.nomeFK = nomeFk;
		
		if(value != null)
			if(value.length() > 0 )
				this.strValue = value;
		
		this.strLabel = label;
		this.isAutoIncrement = false;
		this.size = size;
	}
	
	public Attribute(
			String name,
			String tableFK,
			String attributeFK,
			String strLabel,
			boolean isMasculino,
			SQLLITE_TYPE sqlType,
			boolean isNull,
			String strDefault,
			boolean isPrimaryKey,
			String value, 
			int size,
			TYPE_RELATION_FK onUpdate ,
			TYPE_RELATION_FK onDelete,
			String nomeFk){
		initializeOnUpdateOnDelete(name, isNull, onUpdate, onDelete);
		this.tableFK = tableFK;
		this.attributeFK = attributeFK;
		this.isMasculino = isMasculino;
		this.name = name;
		this.sqlType = sqlType;
		this.isNull =isNull;
		this.strDefault = strDefault;
		this.isPrimaryKey = isPrimaryKey;

		if(value != null)
			if(value.length() > 0 )
				this.strValue = value;
		
		this.strLabel = strLabel;
		this.isAutoIncrement = false;
		this.size = size;
		this.nomeFK = nomeFk;	
	}
	
	public boolean checaValidadeParaEntrarNaClausulaInsert(){
		return ! (isAutoIncrement() && isPrimaryKey() && isEmpty());
	}

//	public Attribute( String name,
//			String tableFK,
//			String attributeFK,
//			String label,
//			boolean isMasculino,
//			SQLLITE_TYPE sqlType,
//			boolean isNull,
//			String strDefault,
//			boolean isPrimaryKey,
//			String value, 
//			int size){
//
//		this.tableFK = tableFK;
//		this.attributeFK = attributeFK;
//		this.isMasculino = isMasculino;
//		this.name = name;
//		this.sqlType = sqlType;
//		this.isNull =isNull;
//		this.strDefault = strDefault;
//		this.isPrimaryKey = isPrimaryKey;
//
//		if(value != null)
//			if(value.length() > 0 )
//				this.strValue = value;
//
//		this.strLabel = label;
//		this.isAutoIncrement = false;
//		this.size = size;
//	}
	

	
	public Attribute getNewAtributoNormalizado(Attribute pAtributoNormalizado){
		return new Attribute( 
				name + "_normalizado", 
				strLabel,
				isMasculino,
				sqlType,
				isNull, 
				strDefault, 
				isPrimaryKey, 
				strValue, 
				size, 
				isAutoIncrement,
				true,
				pAtributoNormalizado);
	}
	
	private void initializeOnUpdateOnDelete(
			String name, 
			boolean isNull, 
			TYPE_RELATION_FK onUpdate ,
			TYPE_RELATION_FK onDelete){
		if(!isNull && onUpdate == TYPE_RELATION_FK.SET_NULL){
			SingletonLog.insereErro(TAG, TAG, "Estrutura da tabela invalida, foreign key on update u SET NULL. Sendo que o atributo '" + name + "' nuo permite valores nulos.", SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			this.onUpdate = TYPE_RELATION_FK.CASCADE;
		} else
			this.onUpdate = onUpdate;
		if(!isNull && onDelete == TYPE_RELATION_FK.SET_NULL){
			SingletonLog.insereErro(TAG, TAG, "Estrutura da tabela invalida, foreign key on delete u SET NULL. Sendo que o atributo '" + name + "' nuo permite valores nulos.", SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			this.onDelete = TYPE_RELATION_FK.CASCADE;
		} else
			this.onDelete = onDelete;
	}
	
	public static String getNomeAtributoNormalizado(String pNome){
		return pNome + "_normalizado";
	}
	
	
	public int getNumeroMaximoAleatorio(){
		int tamanho = getSize();
		if(tamanho == 1) return 1;
		else return (tamanho * 10) - 1;
	}
	
	public int getSize(){
		return size;
	}
	
	public Table getTableFk(Database db){
		try{
			Table obj = db.getTable(tableFK);
		
			
			Table copia = obj.factory();
			copia.setDatabase(db);
			return copia;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BIBLIOTECA_NUVEM);
			return null;
		}
		
		
	}
	
	public Attribute getAttributeFk(Database db){
		Table t = getTableFk(db);
		return t.getAttribute(attributeFK);
	}
	String lastRandom = null;
	int contUtilizacaoRandom = 0; 
	public void populateRandom(Database db) throws Exception{
		
		if(isAutoIncrement()) return;
		
		else if(isFK()){
			
			Table t = getTableFk(db);
			Integer idFk = t.getIdAleatorio();
			if(idFk == null){
				if(!isNull() ){
					SingletonLog.insereErro(new Exception("Nuo foi possuvel encontrar uma chave estrangeira obrigatoria da tabela: " + t.getName() ), TIPO.BIBLIOTECA_NUVEM);
					idFk = t.getIdAleatorio();
				}
				setStrValue(null);
			}
			else setStrValue( String.valueOf(idFk));
						
		} 
		else{
			switch (sqlType) {
			
				case BLOB:
					setStrValue("0");
					break;
				case DATE:
					setStrValue(HelperDate.getDateAtualFormatadaParaSQL());
					break;
				case DATETIME:
					setStrValue(HelperDate.getStringDateTimeSQL(new Date()));
					break;
				case FLOAT:
				case DOUBLE:
					setStrValue(String.valueOf( HelperDouble.getRandom(getNumeroMaximoAleatorio())));
					break;
				case INT:
				case INTEGER:				
					setStrValue(String.valueOf( HelperInteger.getRandom(getNumeroMaximoAleatorio())));
					break;
				case LONG:
					setStrValue(String.valueOf( HelperInteger.getRandom(getNumeroMaximoAleatorio())));
					break;
				case REAL:
					setStrValue(String.valueOf( HelperDouble.getRandom(getNumeroMaximoAleatorio())));
					break;
				case TEXT:
					setStrValue(HelperString.generateRandomString(getSize()));
					break;
				case TIME:
					setStrValue(HelperDate.getStringDateTimeSQL(new Date()));
					break;
				case TINYTEXT:
					setStrValue(HelperString.generateRandomString(getSize()));
					break;
				case CHAR:	
				case VARCHAR:
					setStrValue(HelperString.generateRandomString(getSize()));
					break;
				default:
					throw new Exception("Tipo inexistente");
			}
						
		}
		
	}
	
	public static TYPE_RELATION_FK getTypeRelationFk(String pTipo){
		if(pTipo == null || pTipo.length() == 0) return null;
		else{
			pTipo = pTipo.toLowerCase();
			if(pTipo.compareTo("cascade") == 0) return TYPE_RELATION_FK.CASCADE;
			else if(pTipo.compareTo("set null") == 0) return TYPE_RELATION_FK.SET_NULL;
			else if(pTipo.compareTo("no action") == 0) return TYPE_RELATION_FK.NO_ACTION;
			else if(pTipo.compareTo("set default") == 0) return TYPE_RELATION_FK.SET_DEFAULT;
			else if(pTipo.compareTo("restrict") == 0) return TYPE_RELATION_FK.RESTRICT;
			else return null;
		}
	}
	
	public static String getStrTypeRelationFk(TYPE_RELATION_FK pType){
		switch (pType) {
		case CASCADE:
			return "CASCADE";
			
		case NO_ACTION:
			return "NO ACTION";
		case RESTRICT:
			return "RESTRICT";
		case SET_DEFAULT:
			return "SET DEFAULT";
		case SET_NULL:
			return "SET NULL";
		default:
			return null;
		}
	}
	
	public static ContainerTipoSqlite getContainerTipoSqlite(Context pContext, String pTipoSqlite){
		pTipoSqlite = pTipoSqlite.toLowerCase();
		int abreParenteses = pTipoSqlite.indexOf("(");
		int fechaParenteses = pTipoSqlite.indexOf(")");
		String vStrTipoSqlite = "";
		String vSrtTamanho = "";
		if(abreParenteses > 0 && fechaParenteses > abreParenteses){
			vStrTipoSqlite = pTipoSqlite.substring(0, abreParenteses);
			vSrtTamanho= pTipoSqlite.substring(abreParenteses + 1, fechaParenteses );
		}else{
			vStrTipoSqlite = pTipoSqlite;
		}
		vStrTipoSqlite = vStrTipoSqlite.toLowerCase();
		SQLLITE_TYPE vSqliteType = null;
		int tamanho = -1;
		SQLLITE_TYPE[] vEnum = SQLLITE_TYPE.values();
		for(int i = 0 ; i < vEnum.length; i++){
			if(vStrTipoSqlite.compareTo(vEnum[i].toString().toLowerCase()) == 0){
				vSqliteType = vEnum[i];
				break;
			}
		}
		if(vSqliteType == null) {
			
			SingletonLog.insereErro(TAG, "getContainerTipoSqlite", 
					"Tipo sqlite nao identificado: " + vStrTipoSqlite, SingletonLog.TIPO.BANCO);
			return null;
		}
		Integer vSize = HelperInteger.parserInteger(vSrtTamanho);
		if(vSize != null )
			tamanho =vSize;	
		else tamanho =-1;
		
		return new ContainerTipoSqlite(vSqliteType, tamanho);
	}

	public boolean isNormalizado(){
		if(atributoNormalizado == null) return false;
		else return true;
	}
	
	public Attribute getAttributeNormalizado(){
		return atributoNormalizado;
	}
	
	public boolean isEmpty(){
		if(this.strValue == null){
			return true;
		} else return false;
	}
	public boolean hasDefault(){
		if(this.strDefault == null){
			return false;
		}else if(this.strDefault.length() == 0 ){
			return false;
		} else return true;
	}
	public String getStrLabel(){
		return this.strLabel;
	}

	public boolean isAutoIncrement(){
		return this.isAutoIncrement;
	}

	public String getStrTableFK(){
		if(this.tableFK == null){
			return null;
		}else if(this.tableFK.length() == 0){
			return null;
		}else return this.tableFK; 

	}

	public String getNomeFk(){
		return this.nomeFK;
	}
	
	public String getStrAttributeFK(){
		if(this.attributeFK == null){
			return null;
		}else if(this.attributeFK.length() == 0){
			return null;
		}else return this.attributeFK; 

	}

	
	
	public boolean isFK(){
		if(this.attributeFK == null){
			return false;
		}else if(this.attributeFK.length() == 0){
			return false;
		}else return true;
	}


	public boolean put(ContentValues cv){
		//nao precisa de dar bind


		if(strValue == null){
			cv.putNull(getName());
			return false;
		}else{
			switch (sqlType) {
			case DATE:
			case DATETIME:
			case CHAR:
			case TIME:
			case VARCHAR:
			case TINYTEXT:
			case TEXT:
				cv.put(getName(), getStrValue());
				break;
			case INT:
			case INTEGER:			
			case LONG:

				try{
					Long value = Long.parseLong(strValue);
					cv.put(getName(), value);
				}catch (Exception e) {
					// TODO: handle exception
					cv.putNull(getName());
				}
				break;
				
			case REAL:
			case FLOAT:
			case DOUBLE:
				try{
					Double value = Double.parseDouble(strValue);
					cv.put(getName(),value);
				}catch (Exception e) {
					// TODO: handle exception
					cv.putNull(getName());
					
				}
				break;
			case BLOB:
				try{
					cv.put(getName(), strValue.getBytes());
				}catch (Exception e) {
					// TODO: handle exception
					cv.putNull(getName());
				}
				
				break;
			default:
				SingletonLog.insereErro(new Exception("Tipo nuo identificado" + sqlType.toString()), SingletonLog.TIPO.BANCO);
				break;
			}	
		}

		return true;
	}
	
	public boolean bind(SQLiteStatement p_insertStmt, int p_index){
		//nao precisa de dar bind


		if(strValue == null){
			p_insertStmt.bindNull(p_index);
			return false;
		}else{
			switch (sqlType) {
			case DATE:
			case DATETIME:
			case CHAR:
			case TIME:
			case VARCHAR:
			case TINYTEXT:
			case TEXT:
				p_insertStmt.bindString(p_index, strValue);				
				break;
			case INT:
			case INTEGER:			
			case LONG:

				try{
					Long value = Long.parseLong(strValue);
					p_insertStmt.bindLong(p_index, value);	
				}catch (Exception e) {
					// TODO: handle exception

					p_insertStmt.bindNull(p_index);
				}


				break;
				
			case REAL:
			case FLOAT:
			case DOUBLE:
				try{
					Double value = Double.parseDouble(strValue);
					p_insertStmt.bindDouble(p_index, value);	
				}catch (Exception e) {
					// TODO: handle exception
					p_insertStmt.bindNull(p_index);
					
				}
				break;
			case BLOB:
				try{
					
					p_insertStmt.bindBlob(p_index, strValue.getBytes());	
				}catch (Exception e) {
					// TODO: handle exception
					p_insertStmt.bindNull(p_index);
					
				}
				
				break;
			default:
				SingletonLog.insereErro(new Exception("Tipo nuo identificado" + sqlType.toString()), SingletonLog.TIPO.BANCO);
				break;
			}	
		}

		return true;
	}


	public String getName(){
		return name;
	}

	public boolean isNull(){
		return isNull;
	}
	
	public boolean notNull(){
		return !isNull;
	}

	public String getStrDefault(){
		return strDefault;
	}

	public void clear(){
		strValue = null;
	}

	public boolean isPrimaryKey(){
		return isPrimaryKey;
	}

	public TYPE_RELATION_FK getOnUpdate(){
		return onUpdate;
	}
	public String getStrOnDelete(){
		if(onDelete == null)
			return null;
		switch (onDelete) {
		case CASCADE:
			return "CASCADE";
		case NO_ACTION:
			return "NO ACTION";
		case SET_NULL:
			return "SET NULL";
		default:
			return null;
		}
	}
	public String getStrOnUpdate(){
		if(onUpdate == null)
			return null;
		switch (onUpdate) {
		case CASCADE:
			return "CASCADE";
		case NO_ACTION:
			return "NO ACTION";
		case SET_NULL:
			return "SET NULL";
		default:
			return null;
		}
	}

	public TYPE_RELATION_FK getOnRemove(){
		return onDelete;
	}
	public Integer getIntegerValue(){
		return HelperInteger.parserInteger( strValue);
	}
	public String getStrValue(){
		return strValue;
	}
	public Long getLongValue(){
		return HelperLong.parserLong( strValue);
	}
	public void setStrValue(String p_strValue){
		strValue = p_strValue;
	}

	public SQLLITE_TYPE getSQLType(){
		return sqlType;
	}

	public String getStrSQLDeclaration(){
		String strType = this.getStrSQL();
		
		if(isFlutuante() && this.size > 0 ){
			return strType + "(" + String.valueOf(size) +", 0) ";
		}
		else if(this.size > 0 ){
			return strType + "(" + String.valueOf(size) +") ";
		} 
		else return strType + " ";
	}

	public boolean isFlutuante(){
		if(sqlType == SQLLITE_TYPE.DOUBLE 
				|| sqlType == SQLLITE_TYPE.FLOAT) return true;
		else return false;
	}

	public String getStrSQLiteDeclaration(){
		String strType = this.getStrSQLite();
		
		if(isFlutuante() && this.size > 0 ){
			return strType + "(" + String.valueOf(size) +", 0) ";
		}
		else if(this.size > 0 ){
			return strType + "(" + String.valueOf(size) +") ";
		} 
		else return strType + " ";
	}
	
	public String getStrSQL(){
		if(sqlType == null){
			SingletonLog.insereErro(new Exception("Tipo SQL nulo") , SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			return null;
		}
		switch (sqlType) {
		case INT:
			return "INTEGER";
		case INTEGER:
			return "INTEGER";
		case LONG:
			return "INTEGER";
		case TEXT:
			return "TEXT";
		case FLOAT:
			return "FLOAT";
		case DOUBLE:
			return "FLOAT";
		case BLOB:
			return "BLOB";
		case REAL:
			return "REAL";
		case CHAR:
			return "CHAR";
		case VARCHAR:
			return "VARCHAR";
		case TINYTEXT:
			return "TINYTEXT";
		case DATETIME:
			return "DATETIME";	
		case DATE:
			return "DATE";
		case TIME:
			return "TIME";
		default:
			SingletonLog.insereErro(new Exception("Tipo SQL inexistente: " + sqlType.toString()), SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			return null;
		}
	}
	
	public String getStrSQLite(){
		if(sqlType == null){
			SingletonLog.insereErro(new Exception("Tipo SQL nulo") , SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			return null;
		}
		switch (sqlType) {
		case INT:
			return "INTEGER";
		case INTEGER:
			return "INTEGER";
		case LONG:
			return "INTEGER";
		case TEXT:
			if(size > 0 && size <= 512)
			return "VARCHAR";
			else
				return "TINYTEXT";
		case FLOAT:
			return "DOUBLE";
		case DOUBLE:
			return "DOUBLE";
		case BLOB:
			return "BLOB";
		case REAL:
			return "REAL";
		case CHAR:
			return "VARCHAR";
		case VARCHAR:
			return "VARCHAR";
		case TINYTEXT:
			return "TINYTEXT";
		case DATETIME:
			return "VARCHAR(22)";	
		case DATE:
			return "VARCHAR(10)";
		case TIME:
			return "VARCHAR(8)";
		default:
			SingletonLog.insereErro(new Exception("Tipo SQLite inexistente: " + sqlType.toString()), SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			return null;
		}
	}
}
