package app.omegasoftware.pontoeletronico.database;

public class ContainerKey {
	String nome;
	String[] vetorAttr;
	boolean isUnique;
	public ContainerKey(String nomeChave, String[] pVetorAttr, boolean unique){
		this.vetorAttr = pVetorAttr;
		this.isUnique = unique;
		this.nome = nomeChave;
	}
	
	public boolean isUnique(){
		return this.isUnique;
	}
	public String[] getVetorAttribute(){
		return vetorAttr;
	}
	
	
	
}
