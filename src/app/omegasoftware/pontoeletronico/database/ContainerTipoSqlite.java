package app.omegasoftware.pontoeletronico.database;

import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;

public class ContainerTipoSqlite{
	public SQLLITE_TYPE mSqliteType;
	public int mTamanho;
	public ContainerTipoSqlite(SQLLITE_TYPE pSqliteType,
	int pTamanho){
		mSqliteType = pSqliteType;

		mTamanho = pTamanho;
	}
}