

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOAndroidMetadata
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOAndroidMetadata.java
        * TABELA MYSQL:    android_metadata
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOAndroidMetadata extends Table
        {

        public static final String NAME = "android_metadata";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_ANDROID_METADATA{
					locale};
		public static final String LOCALE = "locale";


		public static String TABELAS_RELACIONADAS[] = {DAOAndroidMetadata.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOAndroidMetadata(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    LOCALE, 
                    "locale",
                    true,
                    SQLLITE_TYPE.TINYTEXT,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_ANDROID_METADATA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_ANDROID_METADATA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_ANDROID_METADATA.class);

    }
        

        } // classe: fim


        