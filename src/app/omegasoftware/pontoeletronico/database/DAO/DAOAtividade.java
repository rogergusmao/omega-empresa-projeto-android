

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOAtividade
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOAtividade.java
        * TABELA MYSQL:    atividade
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOAtividadeUnidadeMedida;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORelatorio;


        public abstract class DAOAtividade extends Table
        {

        public static final String NAME = "atividade";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_ATIVIDADE{
					id,
 			identificador,
 			nome,
 			nome_normalizado,
 			descricao,
 			empresa_id_INT,
 			atividade_unidade_medida_id_INT,
 			prazo_entrega_dia_INT,
 			duracao_horas_INT,
 			id_omega_INT,
 			cadastro_SEC,
 			cadastro_OFFSEC,
 			corporacao_id_INT,
 			relatorio_id_INT};
		public static final String ID = "id";
		public static final String IDENTIFICADOR = "identificador";
		public static final String NOME = "nome";
		public static final String NOME_NORMALIZADO = "nome_normalizado";
		public static final String DESCRICAO = "descricao";
		public static final String EMPRESA_ID_INT = "empresa_id_INT";
		public static final String ATIVIDADE_UNIDADE_MEDIDA_ID_INT = "atividade_unidade_medida_id_INT";
		public static final String PRAZO_ENTREGA_DIA_INT = "prazo_entrega_dia_INT";
		public static final String DURACAO_HORAS_INT = "duracao_horas_INT";
		public static final String ID_OMEGA_INT = "id_omega_INT";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String RELATORIO_ID_INT = "relatorio_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOAtividade.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOAtividade(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    IDENTIFICADOR, 
                    "identificador",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(getNewAtributoNormalizado(NOME));

            super.addAttribute(
                new Attribute(
                    DESCRICAO, 
                    "descricao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    512,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "atividade_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    ATIVIDADE_UNIDADE_MEDIDA_ID_INT, 
                    DAOAtividadeUnidadeMedida.NAME,
                    DAOAtividadeUnidadeMedida.ID,
                    "atividade_unidade_medida_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "atividade_FK_219207763"
                )
            );

            super.addAttribute(
                new Attribute(
                    PRAZO_ENTREGA_DIA_INT, 
                    "prazo_entrega_dia_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DURACAO_HORAS_INT, 
                    "duracao_horas_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    3,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ID_OMEGA_INT, 
                    "id_omega_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "atividade_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    RELATORIO_ID_INT, 
                    DAORelatorio.NAME,
                    DAORelatorio.ID,
                    "relatorio_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "atividade_FK_411010742"
                )
            );

			addKey("empresa_servico_FK_389770508", new String[] { EMPRESA_ID_INT });
			addKey("empresa_servico_FK_414123535", new String[] { CORPORACAO_ID_INT });
			addKey("atividade_FK_219207763", new String[] { ATIVIDADE_UNIDADE_MEDIDA_ID_INT });
			addKey("atividade_FK_411010742", new String[] { RELATORIO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_ATIVIDADE> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_ATIVIDADE>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_ATIVIDADE.class);

    }
        

        } // classe: fim


        