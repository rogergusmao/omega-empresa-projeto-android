

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOAtividadeTipo
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOAtividadeTipo.java
        * TABELA MYSQL:    atividade_tipo
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOAtividadeTipo extends Table
        {

        public static final String NAME = "atividade_tipo";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_ATIVIDADE_TIPO{
					id,
 			nome,
 			nome_normalizado,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String NOME = "nome";
		public static final String NOME_NORMALIZADO = "nome_normalizado";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOAtividadeTipo.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOAtividadeTipo(Database database){
            super(NAME, database);
            setUniqueKey( "nome_098787", new String[]{ NOME, CORPORACAO_ID_INT});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(getNewAtributoNormalizado(NOME));

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "atividade_tipo_ibfk_1"
                )
            );

			addKey("empresa_servico_tipo_FK_604431152", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_ATIVIDADE_TIPO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_ATIVIDADE_TIPO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_ATIVIDADE_TIPO.class);

    }
        

        } // classe: fim


        