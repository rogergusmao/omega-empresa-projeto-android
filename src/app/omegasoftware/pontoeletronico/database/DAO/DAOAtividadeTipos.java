

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOAtividadeTipos
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOAtividadeTipos.java
        * TABELA MYSQL:    atividade_tipos
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOAtividadeTipo;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOAtividade;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOAtividadeTipos extends Table
        {

        public static final String NAME = "atividade_tipos";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_ATIVIDADE_TIPOS{
					id,
 			atividade_tipo_id_INT,
 			atividade_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String ATIVIDADE_TIPO_ID_INT = "atividade_tipo_id_INT";
		public static final String ATIVIDADE_ID_INT = "atividade_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOAtividadeTipos.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOAtividadeTipos(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    ATIVIDADE_TIPO_ID_INT, 
                    DAOAtividadeTipo.NAME,
                    DAOAtividadeTipo.ID,
                    "atividade_tipo_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "atividade_tipos_FK_423400879"
                )
            );

            super.addAttribute(
                new Attribute(
                    ATIVIDADE_ID_INT, 
                    DAOAtividade.NAME,
                    DAOAtividade.ID,
                    "atividade_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "atividade_tipos_FK_996185303"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "atividade_tipos_FK_789062500"
                )
            );

			addKey("atividade_tipos_FK_423400879", new String[] { ATIVIDADE_TIPO_ID_INT });
			addKey("atividade_tipos_FK_996185303", new String[] { ATIVIDADE_ID_INT });
			addKey("atividade_tipos_FK_789062500", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_ATIVIDADE_TIPOS> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_ATIVIDADE_TIPOS>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_ATIVIDADE_TIPOS.class);

    }
        

        } // classe: fim


        