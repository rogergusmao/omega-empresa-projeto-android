

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOCorporacao
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOCorporacao.java
        * TABELA MYSQL:    corporacao
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOCorporacao extends Table
        {

        public static final String NAME = "corporacao";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_CORPORACAO{
					id,
 			nome,
 			nome_normalizado,
 			usuario_dropbox,
 			senha_dropbox};
		public static final String ID = "id";
		public static final String NOME = "nome";
		public static final String NOME_NORMALIZADO = "nome_normalizado";
		public static final String USUARIO_DROPBOX = "usuario_dropbox";
		public static final String SENHA_DROPBOX = "senha_dropbox";


		public static String TABELAS_RELACIONADAS[] = {DAOCorporacao.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOCorporacao(Database database){
            super(NAME, database);
            setUniqueKey( "corporacao_nome_UNIQUE", new String[]{ NOME_NORMALIZADO});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(getNewAtributoNormalizado(NOME));

            super.addAttribute(
                new Attribute(
                    USUARIO_DROPBOX, 
                    "usuario_dropbox",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SENHA_DROPBOX, 
                    "senha_dropbox",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_CORPORACAO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_CORPORACAO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_CORPORACAO.class);

    }
        

        } // classe: fim


        