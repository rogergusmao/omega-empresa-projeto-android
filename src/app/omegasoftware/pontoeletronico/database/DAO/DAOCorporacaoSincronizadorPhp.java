

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOCorporacaoSincronizadorPhp
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOCorporacaoSincronizadorPhp.java
        * TABELA MYSQL:    corporacao_sincronizador_php
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOCorporacaoSincronizadorPhp extends Table
        {

        public static final String NAME = "corporacao_sincronizador_php";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_CORPORACAO_SINCRONIZADOR_PHP{
					id,
 			last_id_sincronizador_php_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String LAST_ID_SINCRONIZADOR_PHP_INT = "last_id_sincronizador_php_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOCorporacaoSincronizadorPhp.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOCorporacaoSincronizadorPhp(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    LAST_ID_SINCRONIZADOR_PHP_INT, 
                    "last_id_sincronizador_php_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "corporacao_sincronizador_php_ibfk_1"
                )
            );

			addKey("key_6c8bdfc3dddcfb93", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_CORPORACAO_SINCRONIZADOR_PHP> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_CORPORACAO_SINCRONIZADOR_PHP>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_CORPORACAO_SINCRONIZADOR_PHP.class);

    }
        

        } // classe: fim


        