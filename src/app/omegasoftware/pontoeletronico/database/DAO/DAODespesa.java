

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAODespesa
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAODespesa.java
        * TABELA MYSQL:    despesa
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAODespesaCotidiano;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAODespesa extends Table
        {

        public static final String NAME = "despesa";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_DESPESA{
					id,
 			valor_FLOAT,
 			empresa_id_INT,
 			vencimento_SEC,
 			vencimento_OFFSEC,
 			pagamento_SEC,
 			pagamento_OFFSEC,
 			despesa_cotidiano_id_INT,
 			valor_pagamento_FLOAT,
 			cadastro_usuario_id_INT,
 			pagamento_usuario_id_INT,
 			protocolo_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String VALOR_FLOAT = "valor_FLOAT";
		public static final String EMPRESA_ID_INT = "empresa_id_INT";
		public static final String VENCIMENTO_SEC = "vencimento_SEC";
		public static final String VENCIMENTO_OFFSEC = "vencimento_OFFSEC";
		public static final String PAGAMENTO_SEC = "pagamento_SEC";
		public static final String PAGAMENTO_OFFSEC = "pagamento_OFFSEC";
		public static final String DESPESA_COTIDIANO_ID_INT = "despesa_cotidiano_id_INT";
		public static final String VALOR_PAGAMENTO_FLOAT = "valor_pagamento_FLOAT";
		public static final String CADASTRO_USUARIO_ID_INT = "cadastro_usuario_id_INT";
		public static final String PAGAMENTO_USUARIO_ID_INT = "pagamento_usuario_id_INT";
		public static final String PROTOCOLO_INT = "protocolo_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAODespesa.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAODespesa(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_FLOAT, 
                    "valor_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "despesa_FK_685638428"
                )
            );

            super.addAttribute(
                new Attribute(
                    VENCIMENTO_SEC, 
                    "vencimento_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VENCIMENTO_OFFSEC, 
                    "vencimento_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PAGAMENTO_SEC, 
                    "pagamento_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PAGAMENTO_OFFSEC, 
                    "pagamento_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DESPESA_COTIDIANO_ID_INT, 
                    DAODespesaCotidiano.NAME,
                    DAODespesaCotidiano.ID,
                    "despesa_cotidiano_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "despesa_FK_939453125"
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_PAGAMENTO_FLOAT, 
                    "valor_pagamento_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "cadastro_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "despesa_FK_777130127"
                )
            );

            super.addAttribute(
                new Attribute(
                    PAGAMENTO_USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "pagamento_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "despesa_FK_818298340"
                )
            );

            super.addAttribute(
                new Attribute(
                    PROTOCOLO_INT, 
                    "protocolo_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "despesa_FK_352508545"
                )
            );

			addKey("despesa_FK_685638428", new String[] { EMPRESA_ID_INT });
			addKey("despesa_FK_939453125", new String[] { DESPESA_COTIDIANO_ID_INT });
			addKey("despesa_FK_777130127", new String[] { CADASTRO_USUARIO_ID_INT });
			addKey("despesa_FK_818298340", new String[] { PAGAMENTO_USUARIO_ID_INT });
			addKey("despesa_FK_352508545", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_DESPESA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_DESPESA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_DESPESA.class);

    }
        

        } // classe: fim


        