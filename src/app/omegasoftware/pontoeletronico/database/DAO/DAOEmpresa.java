

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOEmpresa
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOEmpresa.java
        * TABELA MYSQL:    empresa
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOOperadora;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoDocumento;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOBairro;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCidade;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORelatorio;


        public abstract class DAOEmpresa extends Table
        {

        public static final String NAME = "empresa";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_EMPRESA{
					id,
 			nome,
 			nome_normalizado,
 			telefone1,
 			telefone2,
 			celular,
 			celular_sms,
 			operadora_id_INT,
 			fax,
 			email,
 			tipo_documento_id_INT,
 			numero_documento,
 			tipo_empresa_id_INT,
 			logradouro,
 			numero,
 			complemento,
 			bairro_id_INT,
 			cidade_id_INT,
 			latitude_INT,
 			longitude_INT,
 			foto,
 			corporacao_id_INT,
 			cadastro_SEC,
 			cadastro_OFFSEC,
 			relatorio_id_INT,
 			ind_email_valido_BOOLEAN,
 			ind_celular_valido_BOOLEAN};
		public static final String ID = "id";
		public static final String NOME = "nome";
		public static final String NOME_NORMALIZADO = "nome_normalizado";
		public static final String TELEFONE1 = "telefone1";
		public static final String TELEFONE2 = "telefone2";
		public static final String CELULAR = "celular";
		public static final String CELULAR_SMS = "celular_sms";
		public static final String OPERADORA_ID_INT = "operadora_id_INT";
		public static final String FAX = "fax";
		public static final String EMAIL = "email";
		public static final String TIPO_DOCUMENTO_ID_INT = "tipo_documento_id_INT";
		public static final String NUMERO_DOCUMENTO = "numero_documento";
		public static final String TIPO_EMPRESA_ID_INT = "tipo_empresa_id_INT";
		public static final String LOGRADOURO = "logradouro";
		public static final String NUMERO = "numero";
		public static final String COMPLEMENTO = "complemento";
		public static final String BAIRRO_ID_INT = "bairro_id_INT";
		public static final String CIDADE_ID_INT = "cidade_id_INT";
		public static final String LATITUDE_INT = "latitude_INT";
		public static final String LONGITUDE_INT = "longitude_INT";
		public static final String FOTO = "foto";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";
		public static final String RELATORIO_ID_INT = "relatorio_id_INT";
		public static final String IND_EMAIL_VALIDO_BOOLEAN = "ind_email_valido_BOOLEAN";
		public static final String IND_CELULAR_VALIDO_BOOLEAN = "ind_celular_valido_BOOLEAN";


		public static String TABELAS_RELACIONADAS[] = {DAOEmpresa.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOEmpresa(Database database){
            super(NAME, database);
            setUniqueKey( "email", new String[]{ EMAIL, CORPORACAO_ID_INT});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(getNewAtributoNormalizado(NOME));

            super.addAttribute(
                new Attribute(
                    TELEFONE1, 
                    "telefone1",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TELEFONE2, 
                    "telefone2",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CELULAR, 
                    "celular",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CELULAR_SMS, 
                    "celular_sms",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    OPERADORA_ID_INT, 
                    DAOOperadora.NAME,
                    DAOOperadora.ID,
                    "operadora_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    FAX, 
                    "fax",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    EMAIL, 
                    "email",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TIPO_DOCUMENTO_ID_INT, 
                    DAOTipoDocumento.NAME,
                    DAOTipoDocumento.ID,
                    "tipo_documento_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    NUMERO_DOCUMENTO, 
                    "numero_documento",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TIPO_EMPRESA_ID_INT, 
                    DAOTipoEmpresa.NAME,
                    DAOTipoEmpresa.ID,
                    "tipo_empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_ibfk_4"
                )
            );

            super.addAttribute(
                new Attribute(
                    LOGRADOURO, 
                    "logradouro",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    NUMERO, 
                    "numero",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    COMPLEMENTO, 
                    "complemento",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    BAIRRO_ID_INT, 
                    DAOBairro.NAME,
                    DAOBairro.ID,
                    "bairro_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_ibfk_5"
                )
            );

            super.addAttribute(
                new Attribute(
                    CIDADE_ID_INT, 
                    DAOCidade.NAME,
                    DAOCidade.ID,
                    "cidade_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_ibfk_6"
                )
            );

            super.addAttribute(
                new Attribute(
                    LATITUDE_INT, 
                    "latitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    LONGITUDE_INT, 
                    "longitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    FOTO, 
                    "foto",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    50,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    RELATORIO_ID_INT, 
                    DAORelatorio.NAME,
                    DAORelatorio.ID,
                    "relatorio_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_FK_690795899"
                )
            );

            super.addAttribute(
                new Attribute(
                    IND_EMAIL_VALIDO_BOOLEAN, 
                    "ind_email_valido_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    IND_CELULAR_VALIDO_BOOLEAN, 
                    "ind_celular_valido_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

			addKey("u_bairro_FK", new String[] { BAIRRO_ID_INT });
			addKey("u_cidade_FK", new String[] { CIDADE_ID_INT });
			addKey("empresa_FK_222015381", new String[] { CORPORACAO_ID_INT });
			addKey("empresa_FK_261840820", new String[] { TIPO_DOCUMENTO_ID_INT });
			addKey("empresa_FK_354888916", new String[] { TIPO_EMPRESA_ID_INT });
			addKey("empresa_FK_967346192", new String[] { OPERADORA_ID_INT });
			addKey("key_empr766968", new String[] { ID });
			addKey("empresa_FK_690795899", new String[] { RELATORIO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_EMPRESA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_EMPRESA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_EMPRESA.class);

    }
        

        } // classe: fim


        