

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOEmpresaAtividade
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOEmpresaAtividade.java
        * TABELA MYSQL:    empresa_atividade
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOAtividade;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOEmpresaAtividade extends Table
        {

        public static final String NAME = "empresa_atividade";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_EMPRESA_ATIVIDADE{
					id,
 			empresa_id_INT,
 			prazo_entrega_dia_INT,
 			duracao_horas_INT,
 			cadastro_SEC,
 			cadastro_OFFSEC,
 			preco_custo_FLOAT,
 			preco_venda_FLOAT,
 			atividade_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String EMPRESA_ID_INT = "empresa_id_INT";
		public static final String PRAZO_ENTREGA_DIA_INT = "prazo_entrega_dia_INT";
		public static final String DURACAO_HORAS_INT = "duracao_horas_INT";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";
		public static final String PRECO_CUSTO_FLOAT = "preco_custo_FLOAT";
		public static final String PRECO_VENDA_FLOAT = "preco_venda_FLOAT";
		public static final String ATIVIDADE_ID_INT = "atividade_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOEmpresaAtividade.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOEmpresaAtividade(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_atividade_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    PRAZO_ENTREGA_DIA_INT, 
                    "prazo_entrega_dia_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DURACAO_HORAS_INT, 
                    "duracao_horas_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    3,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PRECO_CUSTO_FLOAT, 
                    "preco_custo_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PRECO_VENDA_FLOAT, 
                    "preco_venda_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ATIVIDADE_ID_INT, 
                    DAOAtividade.NAME,
                    DAOAtividade.ID,
                    "atividade_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_atividade_FK_124084472"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_atividade_ibfk_3"
                )
            );

			addKey("empresa_servico_FK_389770508", new String[] { EMPRESA_ID_INT });
			addKey("empresa_servico_FK_414123535", new String[] { CORPORACAO_ID_INT });
			addKey("empresa_atividade_FK_124084472", new String[] { ATIVIDADE_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_EMPRESA_ATIVIDADE> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_EMPRESA_ATIVIDADE>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_EMPRESA_ATIVIDADE.class);

    }
        

        } // classe: fim


        