

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOEmpresaAtividadeCompra
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOEmpresaAtividadeCompra.java
        * TABELA MYSQL:    empresa_atividade_compra
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaCompra;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOAtividade;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOEmpresaAtividadeCompra extends Table
        {

        public static final String NAME = "empresa_atividade_compra";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_EMPRESA_ATIVIDADE_COMPRA{
					id,
 			empresa_compra_id_INT,
 			quantidade_FLOAT,
 			valor_total_FLOAT,
 			desconto_FLOAT,
 			descricao,
 			atividade_id_INT,
 			empresa_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String EMPRESA_COMPRA_ID_INT = "empresa_compra_id_INT";
		public static final String QUANTIDADE_FLOAT = "quantidade_FLOAT";
		public static final String VALOR_TOTAL_FLOAT = "valor_total_FLOAT";
		public static final String DESCONTO_FLOAT = "desconto_FLOAT";
		public static final String DESCRICAO = "descricao";
		public static final String ATIVIDADE_ID_INT = "atividade_id_INT";
		public static final String EMPRESA_ID_INT = "empresa_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOEmpresaAtividadeCompra.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOEmpresaAtividadeCompra(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_COMPRA_ID_INT, 
                    DAOEmpresaCompra.NAME,
                    DAOEmpresaCompra.ID,
                    "empresa_compra_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_atividade_compra_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    QUANTIDADE_FLOAT, 
                    "quantidade_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    false,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_TOTAL_FLOAT, 
                    "valor_total_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    false,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DESCONTO_FLOAT, 
                    "desconto_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DESCRICAO, 
                    "descricao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ATIVIDADE_ID_INT, 
                    DAOAtividade.NAME,
                    DAOAtividade.ID,
                    "atividade_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_atividade_compra_FK_668334961"
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_atividade_compra_FK_537750244"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_atividade_compra_ibfk_1"
                )
            );

			addKey("empresa_servico_compra_FK_892700196", new String[] { EMPRESA_COMPRA_ID_INT });
			addKey("empresa_servico_compra_FK_118560791", new String[] { CORPORACAO_ID_INT });
			addKey("empresa_atividade_compra_FK_668334961", new String[] { ATIVIDADE_ID_INT });
			addKey("empresa_atividade_compra_FK_537750244", new String[] { EMPRESA_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_EMPRESA_ATIVIDADE_COMPRA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_EMPRESA_ATIVIDADE_COMPRA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_EMPRESA_ATIVIDADE_COMPRA.class);

    }
        

        } // classe: fim


        