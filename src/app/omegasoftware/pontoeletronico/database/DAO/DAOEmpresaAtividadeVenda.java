

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOEmpresaAtividadeVenda
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOEmpresaAtividadeVenda.java
        * TABELA MYSQL:    empresa_atividade_venda
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaVenda;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOAtividade;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;


        public abstract class DAOEmpresaAtividadeVenda extends Table
        {

        public static final String NAME = "empresa_atividade_venda";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_EMPRESA_ATIVIDADE_VENDA{
					id,
 			empresa_venda_id_INT,
 			quantidade_FLOAT,
 			valor_total_FLOAT,
 			desconto_FLOAT,
 			corporacao_id_INT,
 			cadastro_SEC,
 			cadastro_OFFSEC,
 			atividade_id_INT,
 			empresa_id_INT,
 			protocolo_INT};
		public static final String ID = "id";
		public static final String EMPRESA_VENDA_ID_INT = "empresa_venda_id_INT";
		public static final String QUANTIDADE_FLOAT = "quantidade_FLOAT";
		public static final String VALOR_TOTAL_FLOAT = "valor_total_FLOAT";
		public static final String DESCONTO_FLOAT = "desconto_FLOAT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";
		public static final String ATIVIDADE_ID_INT = "atividade_id_INT";
		public static final String EMPRESA_ID_INT = "empresa_id_INT";
		public static final String PROTOCOLO_INT = "protocolo_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOEmpresaAtividadeVenda.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOEmpresaAtividadeVenda(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_VENDA_ID_INT, 
                    DAOEmpresaVenda.NAME,
                    DAOEmpresaVenda.ID,
                    "empresa_venda_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_atividade_venda_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    QUANTIDADE_FLOAT, 
                    "quantidade_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    false,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_TOTAL_FLOAT, 
                    "valor_total_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    false,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DESCONTO_FLOAT, 
                    "desconto_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_atividade_venda_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ATIVIDADE_ID_INT, 
                    DAOAtividade.NAME,
                    DAOAtividade.ID,
                    "atividade_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_atividade_venda_FK_500762939"
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_atividade_venda_FK_194274902"
                )
            );

            super.addAttribute(
                new Attribute(
                    PROTOCOLO_INT, 
                    "protocolo_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

			addKey("empresa_servico_venda_FK_937438965", new String[] { EMPRESA_VENDA_ID_INT });
			addKey("empresa_servico_venda_FK_338836670", new String[] { CORPORACAO_ID_INT });
			addKey("empresa_atividade_venda_FK_500762939", new String[] { ATIVIDADE_ID_INT });
			addKey("empresa_atividade_venda_FK_194274902", new String[] { EMPRESA_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_EMPRESA_ATIVIDADE_VENDA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_EMPRESA_ATIVIDADE_VENDA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_EMPRESA_ATIVIDADE_VENDA.class);

    }
        

        } // classe: fim


        