

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOEmpresaCompra
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOEmpresaCompra.java
        * TABELA MYSQL:    empresa_compra
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOFormaPagamento;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORelatorio;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstado;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstadoCorporacao;


        public abstract class DAOEmpresaCompra extends Table
        {

        public static final String NAME = "empresa_compra";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_EMPRESA_COMPRA{
					id,
 			forma_pagamento_id_INT,
 			valor_total_FLOAT,
 			usuario_id_INT,
 			minha_empresa_id_INT,
 			outra_empresa_id_INT,
 			corporacao_id_INT,
 			data_SEC,
 			data_OFFSEC,
 			vencimento_SEC,
 			vencimento_OFFSEC,
 			relatorio_id_INT,
 			registro_estado_id_INT,
 			registro_estado_corporacao_id_INT,
 			fechamento_SEC,
 			fechamento_OFFSEC,
 			valor_pago_FLOAT,
 			desconto_FLOAT,
 			protocolo_INT};
		public static final String ID = "id";
		public static final String FORMA_PAGAMENTO_ID_INT = "forma_pagamento_id_INT";
		public static final String VALOR_TOTAL_FLOAT = "valor_total_FLOAT";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String MINHA_EMPRESA_ID_INT = "minha_empresa_id_INT";
		public static final String OUTRA_EMPRESA_ID_INT = "outra_empresa_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String DATA_SEC = "data_SEC";
		public static final String DATA_OFFSEC = "data_OFFSEC";
		public static final String VENCIMENTO_SEC = "vencimento_SEC";
		public static final String VENCIMENTO_OFFSEC = "vencimento_OFFSEC";
		public static final String RELATORIO_ID_INT = "relatorio_id_INT";
		public static final String REGISTRO_ESTADO_ID_INT = "registro_estado_id_INT";
		public static final String REGISTRO_ESTADO_CORPORACAO_ID_INT = "registro_estado_corporacao_id_INT";
		public static final String FECHAMENTO_SEC = "fechamento_SEC";
		public static final String FECHAMENTO_OFFSEC = "fechamento_OFFSEC";
		public static final String VALOR_PAGO_FLOAT = "valor_pago_FLOAT";
		public static final String DESCONTO_FLOAT = "desconto_FLOAT";
		public static final String PROTOCOLO_INT = "protocolo_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOEmpresaCompra.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOEmpresaCompra(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    FORMA_PAGAMENTO_ID_INT, 
                    DAOFormaPagamento.NAME,
                    DAOFormaPagamento.ID,
                    "forma_pagamento_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_compra_ibfk_5"
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_TOTAL_FLOAT, 
                    "valor_total_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_compra_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    MINHA_EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "minha_empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_compra_ibfk_6"
                )
            );

            super.addAttribute(
                new Attribute(
                    OUTRA_EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "outra_empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_compra_ibfk_7"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_compra_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_SEC, 
                    "data_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_OFFSEC, 
                    "data_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VENCIMENTO_SEC, 
                    "vencimento_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VENCIMENTO_OFFSEC, 
                    "vencimento_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    RELATORIO_ID_INT, 
                    DAORelatorio.NAME,
                    DAORelatorio.ID,
                    "relatorio_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_compra_FK_584411621"
                )
            );

            super.addAttribute(
                new Attribute(
                    REGISTRO_ESTADO_ID_INT, 
                    DAORegistroEstado.NAME,
                    DAORegistroEstado.ID,
                    "registro_estado_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_compra_FK_608856201"
                )
            );

            super.addAttribute(
                new Attribute(
                    REGISTRO_ESTADO_CORPORACAO_ID_INT, 
                    DAORegistroEstadoCorporacao.NAME,
                    DAORegistroEstadoCorporacao.ID,
                    "registro_estado_corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_compra_FK_382690430"
                )
            );

            super.addAttribute(
                new Attribute(
                    FECHAMENTO_SEC, 
                    "fechamento_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    FECHAMENTO_OFFSEC, 
                    "fechamento_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_PAGO_FLOAT, 
                    "valor_pago_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DESCONTO_FLOAT, 
                    "desconto_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PROTOCOLO_INT, 
                    "protocolo_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

			addKey("empresa_compra_FK_903930664", new String[] { FORMA_PAGAMENTO_ID_INT });
			addKey("empresa_compra_FK_592681885", new String[] { USUARIO_ID_INT });
			addKey("empresa_compra_FK_180664062", new String[] { CORPORACAO_ID_INT });
			addKey("minha_empresa_id_INT", new String[] { MINHA_EMPRESA_ID_INT });
			addKey("outra_empresa_id_INT", new String[] { OUTRA_EMPRESA_ID_INT });
			addKey("empresa_compra_FK_584411621", new String[] { RELATORIO_ID_INT });
			addKey("empresa_compra_FK_608856201", new String[] { REGISTRO_ESTADO_ID_INT });
			addKey("empresa_compra_FK_382690430", new String[] { REGISTRO_ESTADO_CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_EMPRESA_COMPRA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_EMPRESA_COMPRA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_EMPRESA_COMPRA.class);

    }
        

        } // classe: fim


        