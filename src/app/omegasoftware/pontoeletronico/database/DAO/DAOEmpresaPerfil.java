

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOEmpresaPerfil
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOEmpresaPerfil.java
        * TABELA MYSQL:    empresa_perfil
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPerfil;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOEmpresaPerfil extends Table
        {

        public static final String NAME = "empresa_perfil";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_EMPRESA_PERFIL{
					id,
 			empresa_id_INT,
 			perfil_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String EMPRESA_ID_INT = "empresa_id_INT";
		public static final String PERFIL_ID_INT = "perfil_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOEmpresaPerfil.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOEmpresaPerfil(Database database){
            super(NAME, database);
            setUniqueKey( "empresa_id_INT", new String[]{ EMPRESA_ID_INT, PERFIL_ID_INT, CORPORACAO_ID_INT});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_perfil_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    PERFIL_ID_INT, 
                    DAOPerfil.NAME,
                    DAOPerfil.ID,
                    "perfil_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_perfil_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_perfil_ibfk_1"
                )
            );

			addKey("empresa_perfil_FK_569122315", new String[] { EMPRESA_ID_INT });
			addKey("empresa_perfil_FK_645446777", new String[] { PERFIL_ID_INT });
			addKey("empresa_perfil_FK_11688232", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_EMPRESA_PERFIL> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_EMPRESA_PERFIL>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_EMPRESA_PERFIL.class);

    }
        

        } // classe: fim


        