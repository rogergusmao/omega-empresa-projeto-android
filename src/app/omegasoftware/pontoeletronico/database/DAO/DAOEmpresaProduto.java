

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOEmpresaProduto
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOEmpresaProduto.java
        * TABELA MYSQL:    empresa_produto
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOProduto;


        public abstract class DAOEmpresaProduto extends Table
        {

        public static final String NAME = "empresa_produto";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_EMPRESA_PRODUTO{
					id,
 			empresa_id_INT,
 			corporacao_id_INT,
 			cadastro_SEC,
 			cadastro_OFFSEC,
 			preco_custo_FLOAT,
 			preco_venda_FLOAT,
 			estoque_atual_INT,
 			estoque_minimo_INT,
 			prazo_reposicao_estoque_dias_INT,
 			codigo_barra,
 			produto_id_INT};
		public static final String ID = "id";
		public static final String EMPRESA_ID_INT = "empresa_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";
		public static final String PRECO_CUSTO_FLOAT = "preco_custo_FLOAT";
		public static final String PRECO_VENDA_FLOAT = "preco_venda_FLOAT";
		public static final String ESTOQUE_ATUAL_INT = "estoque_atual_INT";
		public static final String ESTOQUE_MINIMO_INT = "estoque_minimo_INT";
		public static final String PRAZO_REPOSICAO_ESTOQUE_DIAS_INT = "prazo_reposicao_estoque_dias_INT";
		public static final String CODIGO_BARRA = "codigo_barra";
		public static final String PRODUTO_ID_INT = "produto_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOEmpresaProduto.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOEmpresaProduto(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_produto_ibfk_4"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_produto_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PRECO_CUSTO_FLOAT, 
                    "preco_custo_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PRECO_VENDA_FLOAT, 
                    "preco_venda_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ESTOQUE_ATUAL_INT, 
                    "estoque_atual_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ESTOQUE_MINIMO_INT, 
                    "estoque_minimo_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PRAZO_REPOSICAO_ESTOQUE_DIAS_INT, 
                    "prazo_reposicao_estoque_dias_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CODIGO_BARRA, 
                    "codigo_barra",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    13,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PRODUTO_ID_INT, 
                    DAOProduto.NAME,
                    DAOProduto.ID,
                    "produto_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_produto_FK_384552002"
                )
            );

			addKey("empresa_produto_FK_453643799", new String[] { EMPRESA_ID_INT });
			addKey("empresa_produto_FK_183288574", new String[] { CORPORACAO_ID_INT });
			addKey("empresa_produto_FK_384552002", new String[] { PRODUTO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_EMPRESA_PRODUTO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_EMPRESA_PRODUTO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_EMPRESA_PRODUTO.class);

    }
        

        } // classe: fim


        