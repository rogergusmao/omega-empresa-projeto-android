

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOEmpresaProdutoVenda
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOEmpresaProdutoVenda.java
        * TABELA MYSQL:    empresa_produto_venda
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaVenda;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOProduto;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;


        public abstract class DAOEmpresaProdutoVenda extends Table
        {

        public static final String NAME = "empresa_produto_venda";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_EMPRESA_PRODUTO_VENDA{
					id,
 			empresa_venda_id_INT,
 			quantidade_FLOAT,
 			valor_total_FLOAT,
 			corporacao_id_INT,
 			desconto_FLOAT,
 			produto_id_INT,
 			descricao,
 			cadastro_SEC,
 			cadastro_OFFSEC,
 			empresa_id_INT};
		public static final String ID = "id";
		public static final String EMPRESA_VENDA_ID_INT = "empresa_venda_id_INT";
		public static final String QUANTIDADE_FLOAT = "quantidade_FLOAT";
		public static final String VALOR_TOTAL_FLOAT = "valor_total_FLOAT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String DESCONTO_FLOAT = "desconto_FLOAT";
		public static final String PRODUTO_ID_INT = "produto_id_INT";
		public static final String DESCRICAO = "descricao";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";
		public static final String EMPRESA_ID_INT = "empresa_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOEmpresaProdutoVenda.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOEmpresaProdutoVenda(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_VENDA_ID_INT, 
                    DAOEmpresaVenda.NAME,
                    DAOEmpresaVenda.ID,
                    "empresa_venda_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_produto_venda_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    QUANTIDADE_FLOAT, 
                    "quantidade_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    false,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_TOTAL_FLOAT, 
                    "valor_total_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    false,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_produto_venda_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    DESCONTO_FLOAT, 
                    "desconto_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PRODUTO_ID_INT, 
                    DAOProduto.NAME,
                    DAOProduto.ID,
                    "produto_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_produto_venda_FK_420928955"
                )
            );

            super.addAttribute(
                new Attribute(
                    DESCRICAO, 
                    "descricao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_produto_venda_FK_367370605"
                )
            );

			addKey("empresa_produto_venda_FK_890716553", new String[] { EMPRESA_VENDA_ID_INT });
			addKey("empresa_produto_venda_FK_292968750", new String[] { CORPORACAO_ID_INT });
			addKey("empresa_produto_venda_FK_420928955", new String[] { PRODUTO_ID_INT });
			addKey("empresa_produto_venda_FK_367370605", new String[] { EMPRESA_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_EMPRESA_PRODUTO_VENDA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_EMPRESA_PRODUTO_VENDA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_EMPRESA_PRODUTO_VENDA.class);

    }
        

        } // classe: fim


        