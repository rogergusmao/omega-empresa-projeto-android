

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOEmpresaVenda
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOEmpresaVenda.java
        * TABELA MYSQL:    empresa_venda
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOFormaPagamento;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstado;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstadoCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORelatorio;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOMesa;


        public abstract class DAOEmpresaVenda extends Table
        {

        public static final String NAME = "empresa_venda";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_EMPRESA_VENDA{
					id,
 			forma_pagamento_id_INT,
 			valor_total_FLOAT,
 			usuario_id_INT,
 			cliente_empresa_id_INT,
 			fornecedor_empresa_id_INT,
 			pessoa_id_INT,
 			foto,
 			corporacao_id_INT,
 			desconto_FLOAT,
 			acrescimo_FLOAT,
 			data_SEC,
 			data_OFFSEC,
 			registro_estado_id_INT,
 			registro_estado_corporacao_id_INT,
 			relatorio_id_INT,
 			descricao,
 			vencimento_SEC,
 			vencimento_OFFSEC,
 			fechamento_SEC,
 			fechamento_OFFSEC,
 			valor_pago_FLOAT,
 			mesa_id_INT,
 			identificador,
 			protocolo_INT};
		public static final String ID = "id";
		public static final String FORMA_PAGAMENTO_ID_INT = "forma_pagamento_id_INT";
		public static final String VALOR_TOTAL_FLOAT = "valor_total_FLOAT";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String CLIENTE_EMPRESA_ID_INT = "cliente_empresa_id_INT";
		public static final String FORNECEDOR_EMPRESA_ID_INT = "fornecedor_empresa_id_INT";
		public static final String PESSOA_ID_INT = "pessoa_id_INT";
		public static final String FOTO = "foto";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String DESCONTO_FLOAT = "desconto_FLOAT";
		public static final String ACRESCIMO_FLOAT = "acrescimo_FLOAT";
		public static final String DATA_SEC = "data_SEC";
		public static final String DATA_OFFSEC = "data_OFFSEC";
		public static final String REGISTRO_ESTADO_ID_INT = "registro_estado_id_INT";
		public static final String REGISTRO_ESTADO_CORPORACAO_ID_INT = "registro_estado_corporacao_id_INT";
		public static final String RELATORIO_ID_INT = "relatorio_id_INT";
		public static final String DESCRICAO = "descricao";
		public static final String VENCIMENTO_SEC = "vencimento_SEC";
		public static final String VENCIMENTO_OFFSEC = "vencimento_OFFSEC";
		public static final String FECHAMENTO_SEC = "fechamento_SEC";
		public static final String FECHAMENTO_OFFSEC = "fechamento_OFFSEC";
		public static final String VALOR_PAGO_FLOAT = "valor_pago_FLOAT";
		public static final String MESA_ID_INT = "mesa_id_INT";
		public static final String IDENTIFICADOR = "identificador";
		public static final String PROTOCOLO_INT = "protocolo_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOEmpresaVenda.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOEmpresaVenda(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    FORMA_PAGAMENTO_ID_INT, 
                    DAOFormaPagamento.NAME,
                    DAOFormaPagamento.ID,
                    "forma_pagamento_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_venda_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_TOTAL_FLOAT, 
                    "valor_total_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    false,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_venda_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    CLIENTE_EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "cliente_empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_venda_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    FORNECEDOR_EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "fornecedor_empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_venda_ibfk_7"
                )
            );

            super.addAttribute(
                new Attribute(
                    PESSOA_ID_INT, 
                    DAOPessoa.NAME,
                    DAOPessoa.ID,
                    "pessoa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_venda_ibfk_5"
                )
            );

            super.addAttribute(
                new Attribute(
                    FOTO, 
                    "foto",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    50,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_venda_ibfk_6"
                )
            );

            super.addAttribute(
                new Attribute(
                    DESCONTO_FLOAT, 
                    "desconto_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ACRESCIMO_FLOAT, 
                    "acrescimo_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_SEC, 
                    "data_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_OFFSEC, 
                    "data_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    REGISTRO_ESTADO_ID_INT, 
                    DAORegistroEstado.NAME,
                    DAORegistroEstado.ID,
                    "registro_estado_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_venda_FK_985656739"
                )
            );

            super.addAttribute(
                new Attribute(
                    REGISTRO_ESTADO_CORPORACAO_ID_INT, 
                    DAORegistroEstadoCorporacao.NAME,
                    DAORegistroEstadoCorporacao.ID,
                    "registro_estado_corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_venda_FK_407073975"
                )
            );

            super.addAttribute(
                new Attribute(
                    RELATORIO_ID_INT, 
                    DAORelatorio.NAME,
                    DAORelatorio.ID,
                    "relatorio_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_venda_FK_402862549"
                )
            );

            super.addAttribute(
                new Attribute(
                    DESCRICAO, 
                    "descricao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VENCIMENTO_SEC, 
                    "vencimento_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VENCIMENTO_OFFSEC, 
                    "vencimento_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    FECHAMENTO_SEC, 
                    "fechamento_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    FECHAMENTO_OFFSEC, 
                    "fechamento_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_PAGO_FLOAT, 
                    "valor_pago_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    MESA_ID_INT, 
                    DAOMesa.NAME,
                    DAOMesa.ID,
                    "mesa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_venda_FK_648132324"
                )
            );

            super.addAttribute(
                new Attribute(
                    IDENTIFICADOR, 
                    "identificador",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PROTOCOLO_INT, 
                    "protocolo_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

			addKey("key_empr83221", new String[] { FORMA_PAGAMENTO_ID_INT });
			addKey("key_empr213501", new String[] { USUARIO_ID_INT });
			addKey("key_empr835846", new String[] { CLIENTE_EMPRESA_ID_INT });
			addKey("key_empr655823", new String[] { FORNECEDOR_EMPRESA_ID_INT });
			addKey("key_empr330170", new String[] { PESSOA_ID_INT });
			addKey("key_empr193359", new String[] { CORPORACAO_ID_INT });
			addKey("empresa_venda_FK_985656739", new String[] { REGISTRO_ESTADO_ID_INT });
			addKey("empresa_venda_FK_407073975", new String[] { REGISTRO_ESTADO_CORPORACAO_ID_INT });
			addKey("empresa_venda_FK_402862549", new String[] { RELATORIO_ID_INT });
			addKey("empresa_venda_FK_648132324", new String[] { MESA_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_EMPRESA_VENDA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_EMPRESA_VENDA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_EMPRESA_VENDA.class);

    }
        

        } // classe: fim


        