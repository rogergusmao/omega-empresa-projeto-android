

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOEmpresaVendaParcela
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOEmpresaVendaParcela.java
        * TABELA MYSQL:    empresa_venda_parcela
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaVenda;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOEmpresaVendaParcela extends Table
        {

        public static final String NAME = "empresa_venda_parcela";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_EMPRESA_VENDA_PARCELA{
					id,
 			empresa_venda_id_INT,
 			valor_FLOAT,
 			corporacao_id_INT,
 			data_SEC,
 			data_OFFSEC,
 			pagamento_SEC,
 			pagamento_OFFSEC,
 			seq_INT};
		public static final String ID = "id";
		public static final String EMPRESA_VENDA_ID_INT = "empresa_venda_id_INT";
		public static final String VALOR_FLOAT = "valor_FLOAT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String DATA_SEC = "data_SEC";
		public static final String DATA_OFFSEC = "data_OFFSEC";
		public static final String PAGAMENTO_SEC = "pagamento_SEC";
		public static final String PAGAMENTO_OFFSEC = "pagamento_OFFSEC";
		public static final String SEQ_INT = "seq_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOEmpresaVendaParcela.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOEmpresaVendaParcela(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_VENDA_ID_INT, 
                    DAOEmpresaVenda.NAME,
                    DAOEmpresaVenda.ID,
                    "empresa_venda_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_venda_parcela_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_FLOAT, 
                    "valor_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    false,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_venda_parcela_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_SEC, 
                    "data_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_OFFSEC, 
                    "data_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PAGAMENTO_SEC, 
                    "pagamento_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PAGAMENTO_OFFSEC, 
                    "pagamento_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SEQ_INT, 
                    "seq_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

			addKey("empresa_venda_parcela_FK_200561523", new String[] { EMPRESA_VENDA_ID_INT });
			addKey("empresa_venda_parcela_FK_50445556", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_EMPRESA_VENDA_PARCELA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_EMPRESA_VENDA_PARCELA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_EMPRESA_VENDA_PARCELA.class);

    }
        

        } // classe: fim


        