

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOEmpresaVendaTemplate
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOEmpresaVendaTemplate.java
        * TABELA MYSQL:    empresa_venda_template
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOProduto;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOEmpresaVendaTemplate extends Table
        {

        public static final String NAME = "empresa_venda_template";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_EMPRESA_VENDA_TEMPLATE{
					id,
 			titulo,
 			template_json,
 			produto_id_INT,
 			empresa_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String TITULO = "titulo";
		public static final String TEMPLATE_JSON = "template_json";
		public static final String PRODUTO_ID_INT = "produto_id_INT";
		public static final String EMPRESA_ID_INT = "empresa_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOEmpresaVendaTemplate.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOEmpresaVendaTemplate(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    TITULO, 
                    "titulo",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TEMPLATE_JSON, 
                    "template_json",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    512,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PRODUTO_ID_INT, 
                    DAOProduto.NAME,
                    DAOProduto.ID,
                    "produto_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_venda_template_FK_418518066"
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_venda_template_FK_963958741"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "empresa_venda_template_FK_776367188"
                )
            );

			addKey("empresa_venda_template_FK_418518066", new String[] { PRODUTO_ID_INT });
			addKey("empresa_venda_template_FK_963958741", new String[] { EMPRESA_ID_INT });
			addKey("empresa_venda_template_FK_776367188", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_EMPRESA_VENDA_TEMPLATE> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_EMPRESA_VENDA_TEMPLATE>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_EMPRESA_VENDA_TEMPLATE.class);

    }
        

        } // classe: fim


        