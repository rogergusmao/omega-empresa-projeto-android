

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOMensagem
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOMensagem.java
        * TABELA MYSQL:    mensagem
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOMensagem;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstado;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOMensagem extends Table
        {

        public static final String NAME = "mensagem";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_MENSAGEM{
					id,
 			titulo,
 			mensagem,
 			remetente_usuario_id_INT,
 			tipo_mensagem_id_INT,
 			destinatario_usuario_id_INT,
 			destinatario_pessoa_id_INT,
 			destinatario_empresa_id_INT,
 			data_min_envio_SEC,
 			data_min_envio_OFFSEC,
 			protocolo_INT,
 			registro_estado_id_INT,
 			empresa_para_cliente_BOOLEAN,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String TITULO = "titulo";
		public static final String MENSAGEM = "mensagem";
		public static final String REMETENTE_USUARIO_ID_INT = "remetente_usuario_id_INT";
		public static final String TIPO_MENSAGEM_ID_INT = "tipo_mensagem_id_INT";
		public static final String DESTINATARIO_USUARIO_ID_INT = "destinatario_usuario_id_INT";
		public static final String DESTINATARIO_PESSOA_ID_INT = "destinatario_pessoa_id_INT";
		public static final String DESTINATARIO_EMPRESA_ID_INT = "destinatario_empresa_id_INT";
		public static final String DATA_MIN_ENVIO_SEC = "data_min_envio_SEC";
		public static final String DATA_MIN_ENVIO_OFFSEC = "data_min_envio_OFFSEC";
		public static final String PROTOCOLO_INT = "protocolo_INT";
		public static final String REGISTRO_ESTADO_ID_INT = "registro_estado_id_INT";
		public static final String EMPRESA_PARA_CLIENTE_BOOLEAN = "empresa_para_cliente_BOOLEAN";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOMensagem.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOMensagem(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    TITULO, 
                    "titulo",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    MENSAGEM, 
                    "mensagem",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    512,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    REMETENTE_USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "remetente_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "mensagem_FK_646789551"
                )
            );

            super.addAttribute(
                new Attribute(
                    TIPO_MENSAGEM_ID_INT, 
                    DAOMensagem.NAME,
                    DAOMensagem.ID,
                    "tipo_mensagem_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "mensagem_FK_970550538"
                )
            );

            super.addAttribute(
                new Attribute(
                    DESTINATARIO_USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "destinatario_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "mensagem_FK_119873046"
                )
            );

            super.addAttribute(
                new Attribute(
                    DESTINATARIO_PESSOA_ID_INT, 
                    DAOPessoa.NAME,
                    DAOPessoa.ID,
                    "destinatario_pessoa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "mensagem_FK_67901611"
                )
            );

            super.addAttribute(
                new Attribute(
                    DESTINATARIO_EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "destinatario_empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "mensagem_FK_637390137"
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_MIN_ENVIO_SEC, 
                    "data_min_envio_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_MIN_ENVIO_OFFSEC, 
                    "data_min_envio_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PROTOCOLO_INT, 
                    "protocolo_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    REGISTRO_ESTADO_ID_INT, 
                    DAORegistroEstado.NAME,
                    DAORegistroEstado.ID,
                    "registro_estado_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "mensagem_FK_258514404"
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_PARA_CLIENTE_BOOLEAN, 
                    "empresa_para_cliente_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "mensagem_FK_570434570"
                )
            );

			addKey("mensagem_FK_646789551", new String[] { REMETENTE_USUARIO_ID_INT });
			addKey("mensagem_FK_970550538", new String[] { TIPO_MENSAGEM_ID_INT });
			addKey("mensagem_FK_119873046", new String[] { DESTINATARIO_USUARIO_ID_INT });
			addKey("mensagem_FK_67901611", new String[] { DESTINATARIO_PESSOA_ID_INT });
			addKey("mensagem_FK_637390137", new String[] { DESTINATARIO_EMPRESA_ID_INT });
			addKey("mensagem_FK_258514404", new String[] { REGISTRO_ESTADO_ID_INT });
			addKey("mensagem_FK_570434570", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_MENSAGEM> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_MENSAGEM>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_MENSAGEM.class);

    }
        

        } // classe: fim


        