

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOMensagemEnvio
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOMensagemEnvio.java
        * TABELA MYSQL:    mensagem_envio
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoCanalEnvio;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOMensagem;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstado;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOMensagemEnvio extends Table
        {

        public static final String NAME = "mensagem_envio";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_MENSAGEM_ENVIO{
					id,
 			tipo_canal_envio_id_INT,
 			mensagem_id_INT,
 			cadastro_SEC,
 			cadastro_OFFSEC,
 			obs,
 			identificador,
 			tentativa_INT,
 			registro_estado_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String TIPO_CANAL_ENVIO_ID_INT = "tipo_canal_envio_id_INT";
		public static final String MENSAGEM_ID_INT = "mensagem_id_INT";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";
		public static final String OBS = "obs";
		public static final String IDENTIFICADOR = "identificador";
		public static final String TENTATIVA_INT = "tentativa_INT";
		public static final String REGISTRO_ESTADO_ID_INT = "registro_estado_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOMensagemEnvio.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOMensagemEnvio(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    TIPO_CANAL_ENVIO_ID_INT, 
                    DAOTipoCanalEnvio.NAME,
                    DAOTipoCanalEnvio.ID,
                    "tipo_canal_envio_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "mensagem_envio_FK_703460694"
                )
            );

            super.addAttribute(
                new Attribute(
                    MENSAGEM_ID_INT, 
                    DAOMensagem.NAME,
                    DAOMensagem.ID,
                    "mensagem_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "mensagem_envio_FK_757080078"
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    OBS, 
                    "obs",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    IDENTIFICADOR, 
                    "identificador",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TENTATIVA_INT, 
                    "tentativa_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    2,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    REGISTRO_ESTADO_ID_INT, 
                    DAORegistroEstado.NAME,
                    DAORegistroEstado.ID,
                    "registro_estado_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "mensagem_envio_FK_273468017"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "mensagem_envio_FK_794128418"
                )
            );

			addKey("mensagem_envio_FK_703460694", new String[] { TIPO_CANAL_ENVIO_ID_INT });
			addKey("mensagem_envio_FK_757080078", new String[] { MENSAGEM_ID_INT });
			addKey("mensagem_envio_FK_273468017", new String[] { REGISTRO_ESTADO_ID_INT });
			addKey("mensagem_envio_FK_794128418", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_MENSAGEM_ENVIO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_MENSAGEM_ENVIO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_MENSAGEM_ENVIO.class);

    }
        

        } // classe: fim


        