

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOMesaReserva
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOMesaReserva.java
        * TABELA MYSQL:    mesa_reserva
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOMesa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOMesaReserva extends Table
        {

        public static final String NAME = "mesa_reserva";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_MESA_RESERVA{
					id,
 			mesa_id_INT,
 			cadastro_SEC,
 			cadastro_OFFSEC,
 			pessoa_id_INT,
 			reserva_SEC,
 			reserva_OFFSEC,
 			confimado_SEC,
 			confirmado_OFFSEC,
 			confirmador_usuario_id_INT,
 			celular,
 			protocolo_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String MESA_ID_INT = "mesa_id_INT";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";
		public static final String PESSOA_ID_INT = "pessoa_id_INT";
		public static final String RESERVA_SEC = "reserva_SEC";
		public static final String RESERVA_OFFSEC = "reserva_OFFSEC";
		public static final String CONFIMADO_SEC = "confimado_SEC";
		public static final String CONFIRMADO_OFFSEC = "confirmado_OFFSEC";
		public static final String CONFIRMADOR_USUARIO_ID_INT = "confirmador_usuario_id_INT";
		public static final String CELULAR = "celular";
		public static final String PROTOCOLO_INT = "protocolo_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOMesaReserva.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOMesaReserva(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    MESA_ID_INT, 
                    DAOMesa.NAME,
                    DAOMesa.ID,
                    "mesa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "mesa_reserva_FK_235290527"
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PESSOA_ID_INT, 
                    DAOPessoa.NAME,
                    DAOPessoa.ID,
                    "pessoa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "mesa_reserva_FK_304656982"
                )
            );

            super.addAttribute(
                new Attribute(
                    RESERVA_SEC, 
                    "reserva_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    RESERVA_OFFSEC, 
                    "reserva_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CONFIMADO_SEC, 
                    "confimado_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CONFIRMADO_OFFSEC, 
                    "confirmado_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CONFIRMADOR_USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "confirmador_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "mesa_reserva_FK_974975586"
                )
            );

            super.addAttribute(
                new Attribute(
                    CELULAR, 
                    "celular",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PROTOCOLO_INT, 
                    "protocolo_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "mesa_reserva_FK_961578370"
                )
            );

			addKey("mesa_reserva_FK_235290527", new String[] { MESA_ID_INT });
			addKey("mesa_reserva_FK_304656982", new String[] { PESSOA_ID_INT });
			addKey("mesa_reserva_FK_974975586", new String[] { CONFIRMADOR_USUARIO_ID_INT });
			addKey("mesa_reserva_FK_961578370", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_MESA_RESERVA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_MESA_RESERVA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_MESA_RESERVA.class);

    }
        

        } // classe: fim


        