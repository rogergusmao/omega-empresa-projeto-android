

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOModelo
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOModelo.java
        * TABELA MYSQL:    modelo
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOModelo extends Table
        {

        public static final String NAME = "modelo";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_MODELO{
					id,
 			nome,
 			nome_normalizado,
 			ano_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String NOME = "nome";
		public static final String NOME_NORMALIZADO = "nome_normalizado";
		public static final String ANO_INT = "ano_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOModelo.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOModelo(Database database){
            super(NAME, database);
            setUniqueKey( "nome", new String[]{ NOME_NORMALIZADO, ANO_INT, CORPORACAO_ID_INT});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(getNewAtributoNormalizado(NOME));

            super.addAttribute(
                new Attribute(
                    ANO_INT, 
                    "ano_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    4,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "modelo_ibfk_1"
                )
            );

			addKey("modelo_ibfk_1", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_MODELO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_MODELO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_MODELO.class);

    }
        

        } // classe: fim


        