

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAONegociacaoDivida
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAONegociacaoDivida.java
        * TABELA MYSQL:    negociacao_divida
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaVenda;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORelatorio;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstado;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAONegociacaoDivida extends Table
        {

        public static final String NAME = "negociacao_divida";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_NEGOCIACAO_DIVIDA{
					id,
 			devedor_empresa_id_INT,
 			devedor_pessoa_id_INT,
 			negociador_usuario_id_INT,
 			cadastro_SEC,
 			cadastro_OFFSEC,
 			resultado_empresa_venda_id_INT,
 			relatorio_id_INT,
 			registro_estado_id_INT,
 			protocolo_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String DEVEDOR_EMPRESA_ID_INT = "devedor_empresa_id_INT";
		public static final String DEVEDOR_PESSOA_ID_INT = "devedor_pessoa_id_INT";
		public static final String NEGOCIADOR_USUARIO_ID_INT = "negociador_usuario_id_INT";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";
		public static final String RESULTADO_EMPRESA_VENDA_ID_INT = "resultado_empresa_venda_id_INT";
		public static final String RELATORIO_ID_INT = "relatorio_id_INT";
		public static final String REGISTRO_ESTADO_ID_INT = "registro_estado_id_INT";
		public static final String PROTOCOLO_INT = "protocolo_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAONegociacaoDivida.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAONegociacaoDivida(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    DEVEDOR_EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "devedor_empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "negociacao_divida_FK_620697022"
                )
            );

            super.addAttribute(
                new Attribute(
                    DEVEDOR_PESSOA_ID_INT, 
                    DAOPessoa.NAME,
                    DAOPessoa.ID,
                    "devedor_pessoa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "negociacao_divida_FK_110839843"
                )
            );

            super.addAttribute(
                new Attribute(
                    NEGOCIADOR_USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "negociador_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "negociacao_divida_FK_708282471"
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    RESULTADO_EMPRESA_VENDA_ID_INT, 
                    DAOEmpresaVenda.NAME,
                    DAOEmpresaVenda.ID,
                    "resultado_empresa_venda_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "negociacao_divida_FK_563903809"
                )
            );

            super.addAttribute(
                new Attribute(
                    RELATORIO_ID_INT, 
                    DAORelatorio.NAME,
                    DAORelatorio.ID,
                    "relatorio_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "negociacao_divida_FK_873504639"
                )
            );

            super.addAttribute(
                new Attribute(
                    REGISTRO_ESTADO_ID_INT, 
                    DAORegistroEstado.NAME,
                    DAORegistroEstado.ID,
                    "registro_estado_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "negociacao_divida_FK_729370117"
                )
            );

            super.addAttribute(
                new Attribute(
                    PROTOCOLO_INT, 
                    "protocolo_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "negociacao_divida_FK_940582276"
                )
            );

			addKey("negociacao_divida_FK_620697022", new String[] { DEVEDOR_EMPRESA_ID_INT });
			addKey("negociacao_divida_FK_110839843", new String[] { DEVEDOR_PESSOA_ID_INT });
			addKey("negociacao_divida_FK_708282471", new String[] { NEGOCIADOR_USUARIO_ID_INT });
			addKey("negociacao_divida_FK_563903809", new String[] { RESULTADO_EMPRESA_VENDA_ID_INT });
			addKey("negociacao_divida_FK_873504639", new String[] { RELATORIO_ID_INT });
			addKey("negociacao_divida_FK_729370117", new String[] { REGISTRO_ESTADO_ID_INT });
			addKey("negociacao_divida_FK_940582276", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_NEGOCIACAO_DIVIDA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_NEGOCIACAO_DIVIDA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_NEGOCIACAO_DIVIDA.class);

    }
        

        } // classe: fim


        