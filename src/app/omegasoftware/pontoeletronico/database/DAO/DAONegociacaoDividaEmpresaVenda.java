

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAONegociacaoDividaEmpresaVenda
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAONegociacaoDividaEmpresaVenda.java
        * TABELA MYSQL:    negociacao_divida_empresa_venda
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAONegociacaoDivida;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAONegociacaoDividaEmpresaVenda extends Table
        {

        public static final String NAME = "negociacao_divida_empresa_venda";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_NEGOCIACAO_DIVIDA_EMPRESA_VENDA{
					id,
 			empresa_venda_protocolo_INT,
 			negociacao_divida_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String EMPRESA_VENDA_PROTOCOLO_INT = "empresa_venda_protocolo_INT";
		public static final String NEGOCIACAO_DIVIDA_ID_INT = "negociacao_divida_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAONegociacaoDividaEmpresaVenda.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAONegociacaoDividaEmpresaVenda(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_VENDA_PROTOCOLO_INT, 
                    "empresa_venda_protocolo_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    NEGOCIACAO_DIVIDA_ID_INT, 
                    DAONegociacaoDivida.NAME,
                    DAONegociacaoDivida.ID,
                    "negociacao_divida_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "negociacao_divida_empresa_venda_FK_449615478"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "negociacao_divida_empresa_venda_FK_306701660"
                )
            );

			addKey("negociacao_divida_empresa_venda_FK_449615478", new String[] { NEGOCIACAO_DIVIDA_ID_INT });
			addKey("negociacao_divida_empresa_venda_FK_306701660", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_NEGOCIACAO_DIVIDA_EMPRESA_VENDA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_NEGOCIACAO_DIVIDA_EMPRESA_VENDA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_NEGOCIACAO_DIVIDA_EMPRESA_VENDA.class);

    }
        

        } // classe: fim


        