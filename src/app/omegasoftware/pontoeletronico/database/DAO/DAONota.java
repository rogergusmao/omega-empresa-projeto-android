

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAONota
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAONota.java
        * TABELA MYSQL:    nota
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOVeiculo;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORelatorio;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAONota extends Table
        {

        public static final String NAME = "nota";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_NOTA{
					id,
 			empresa_id_INT,
 			pessoa_id_INT,
 			usuario_id_INT,
 			veiculo_id_INT,
 			relatorio_id_INT,
 			protocolo_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String EMPRESA_ID_INT = "empresa_id_INT";
		public static final String PESSOA_ID_INT = "pessoa_id_INT";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String VEICULO_ID_INT = "veiculo_id_INT";
		public static final String RELATORIO_ID_INT = "relatorio_id_INT";
		public static final String PROTOCOLO_INT = "protocolo_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAONota.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAONota(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "nota_FK_922271729"
                )
            );

            super.addAttribute(
                new Attribute(
                    PESSOA_ID_INT, 
                    DAOPessoa.NAME,
                    DAOPessoa.ID,
                    "pessoa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "nota_FK_826232910"
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "nota_FK_654876709"
                )
            );

            super.addAttribute(
                new Attribute(
                    VEICULO_ID_INT, 
                    DAOVeiculo.NAME,
                    DAOVeiculo.ID,
                    "veiculo_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "nota_FK_336425781"
                )
            );

            super.addAttribute(
                new Attribute(
                    RELATORIO_ID_INT, 
                    DAORelatorio.NAME,
                    DAORelatorio.ID,
                    "relatorio_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "nota_FK_797149659"
                )
            );

            super.addAttribute(
                new Attribute(
                    PROTOCOLO_INT, 
                    "protocolo_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "nota_FK_750427246"
                )
            );

			addKey("nota_FK_922271729", new String[] { EMPRESA_ID_INT });
			addKey("nota_FK_826232910", new String[] { PESSOA_ID_INT });
			addKey("nota_FK_654876709", new String[] { USUARIO_ID_INT });
			addKey("nota_FK_336425781", new String[] { VEICULO_ID_INT });
			addKey("nota_FK_797149659", new String[] { RELATORIO_ID_INT });
			addKey("nota_FK_750427246", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_NOTA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_NOTA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_NOTA.class);

    }
        

        } // classe: fim


        