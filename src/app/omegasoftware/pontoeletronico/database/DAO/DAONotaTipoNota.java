

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAONotaTipoNota
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAONotaTipoNota.java
        * TABELA MYSQL:    nota_tipo_nota
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAONota;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoNota;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAONotaTipoNota extends Table
        {

        public static final String NAME = "nota_tipo_nota";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_NOTA_TIPO_NOTA{
					id,
 			nota_id_INT,
 			tipo_nota_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String NOTA_ID_INT = "nota_id_INT";
		public static final String TIPO_NOTA_ID_INT = "tipo_nota_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAONotaTipoNota.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAONotaTipoNota(Database database){
            super(NAME, database);
            setUniqueKey( "nota_id_INT", new String[]{ NOTA_ID_INT, TIPO_NOTA_ID_INT, CORPORACAO_ID_INT});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOTA_ID_INT, 
                    DAONota.NAME,
                    DAONota.ID,
                    "nota_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "nota_tipo_nota_FK_92987060"
                )
            );

            super.addAttribute(
                new Attribute(
                    TIPO_NOTA_ID_INT, 
                    DAOTipoNota.NAME,
                    DAOTipoNota.ID,
                    "tipo_nota_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "nota_tipo_nota_FK_277954101"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "nota_tipo_nota_FK_861236573"
                )
            );

			addKey("nota_tipo_nota_FK_277954101", new String[] { TIPO_NOTA_ID_INT });
			addKey("nota_tipo_nota_FK_861236573", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_NOTA_TIPO_NOTA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_NOTA_TIPO_NOTA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_NOTA_TIPO_NOTA.class);

    }
        

        } // classe: fim


        