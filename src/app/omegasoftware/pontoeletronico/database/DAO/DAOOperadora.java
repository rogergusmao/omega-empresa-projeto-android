

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOOperadora
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOOperadora.java
        * TABELA MYSQL:    operadora
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOOperadora extends Table
        {

        public static final String NAME = "operadora";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_OPERADORA{
					id,
 			nome,
 			nome_normalizado};
		public static final String ID = "id";
		public static final String NOME = "nome";
		public static final String NOME_NORMALIZADO = "nome_normalizado";


		public static String TABELAS_RELACIONADAS[] = {DAOOperadora.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOOperadora(Database database){
            super(NAME, database);
            setUniqueKey( "nome_operadora_UNIQUE", new String[]{ NOME_NORMALIZADO});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    50,
                    false
                )
            );

            super.addAttribute(getNewAtributoNormalizado(NOME));

        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_OPERADORA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_OPERADORA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_OPERADORA.class);

    }
        

        } // classe: fim


        