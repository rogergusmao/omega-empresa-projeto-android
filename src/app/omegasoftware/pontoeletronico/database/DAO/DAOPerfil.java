

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOPerfil
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOPerfil.java
        * TABELA MYSQL:    perfil
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOPerfil extends Table
        {

        public static final String NAME = "perfil";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_PERFIL{
					id,
 			nome};
		public static final String ID = "id";
		public static final String NOME = "nome";


		public static String TABELAS_RELACIONADAS[] = {DAOPerfil.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOPerfil(Database database){
            super(NAME, database);
            setUniqueKey( "nome_perfil_UNIQUE", new String[]{ NOME});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_PERFIL> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_PERFIL>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_PERFIL.class);

    }
        

        } // classe: fim


        