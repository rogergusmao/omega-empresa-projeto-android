

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOPermissao
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOPermissao.java
        * TABELA MYSQL:    permissao
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPermissao;


        public abstract class DAOPermissao extends Table
        {

        public static final String NAME = "permissao";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_PERMISSAO{
					id,
 			nome,
 			tag,
 			pai_permissao_id_INT};
		public static final String ID = "id";
		public static final String NOME = "nome";
		public static final String TAG = "tag";
		public static final String PAI_PERMISSAO_ID_INT = "pai_permissao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOPermissao.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOPermissao(Database database){
            super(NAME, database);
            setUniqueKey( "tag", new String[]{ TAG});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TAG, 
                    "tag",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PAI_PERMISSAO_ID_INT, 
                    DAOPermissao.NAME,
                    DAOPermissao.ID,
                    "pai_permissao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "permissao_ibfk_1"
                )
            );

			addKey("permissao_fk", new String[] { PAI_PERMISSAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_PERMISSAO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_PERMISSAO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_PERMISSAO.class);

    }
        

        } // classe: fim


        