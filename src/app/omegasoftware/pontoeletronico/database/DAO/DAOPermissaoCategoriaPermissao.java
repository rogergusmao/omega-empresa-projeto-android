

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOPermissaoCategoriaPermissao
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOPermissaoCategoriaPermissao.java
        * TABELA MYSQL:    permissao_categoria_permissao
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPermissao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCategoriaPermissao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOPermissaoCategoriaPermissao extends Table
        {

        public static final String NAME = "permissao_categoria_permissao";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_PERMISSAO_CATEGORIA_PERMISSAO{
					id,
 			permissao_id_INT,
 			categoria_permissao_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String PERMISSAO_ID_INT = "permissao_id_INT";
		public static final String CATEGORIA_PERMISSAO_ID_INT = "categoria_permissao_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOPermissaoCategoriaPermissao.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOPermissaoCategoriaPermissao(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    PERMISSAO_ID_INT, 
                    DAOPermissao.NAME,
                    DAOPermissao.ID,
                    "permissao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.CASCADE,
                    "permissao_categoria_permissao_ibfk_9"
                )
            );

            super.addAttribute(
                new Attribute(
                    CATEGORIA_PERMISSAO_ID_INT, 
                    DAOCategoriaPermissao.NAME,
                    DAOCategoriaPermissao.ID,
                    "categoria_permissao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.CASCADE,
                    "permissao_categoria_permissao_ibfk_4"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.CASCADE,
                    "permissao_categoria_permissao_ibfk_7"
                )
            );

			addKey("categoria_permissao_id_INT", new String[] { CATEGORIA_PERMISSAO_ID_INT });
			addKey("corporacao_id_INT", new String[] { CORPORACAO_ID_INT });
			addKey("permissao_id_INT", new String[] { PERMISSAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_PERMISSAO_CATEGORIA_PERMISSAO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_PERMISSAO_CATEGORIA_PERMISSAO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_PERMISSAO_CATEGORIA_PERMISSAO.class);

    }
        

        } // classe: fim


        