

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOPessoaEmpresa
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOPessoaEmpresa.java
        * TABELA MYSQL:    pessoa_empresa
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOProfissao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOPessoaEmpresa extends Table
        {

        public static final String NAME = "pessoa_empresa";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_PESSOA_EMPRESA{
					id,
 			pessoa_id_INT,
 			empresa_id_INT,
 			profissao_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String PESSOA_ID_INT = "pessoa_id_INT";
		public static final String EMPRESA_ID_INT = "empresa_id_INT";
		public static final String PROFISSAO_ID_INT = "profissao_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOPessoaEmpresa.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOPessoaEmpresa(Database database){
            super(NAME, database);
            setUniqueKey( "pessoa_empresa_UNIQUE", new String[]{ PESSOA_ID_INT, EMPRESA_ID_INT, PROFISSAO_ID_INT, CORPORACAO_ID_INT});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    PESSOA_ID_INT, 
                    DAOPessoa.NAME,
                    DAOPessoa.ID,
                    "pessoa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "pessoa_empresa_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "pessoa_empresa_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    PROFISSAO_ID_INT, 
                    DAOProfissao.NAME,
                    DAOProfissao.ID,
                    "profissao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "pessoa_empresa_ibfk_4"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "pessoa_empresa_ibfk_1"
                )
            );

			addKey("empresa_id_INT", new String[] { EMPRESA_ID_INT });
			addKey("fe_profissao_FK", new String[] { PROFISSAO_ID_INT });
			addKey("pessoa_empresa_FK_41381835", new String[] { PESSOA_ID_INT });
			addKey("pessoa_empresa_FK_199859619", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_PESSOA_EMPRESA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_PESSOA_EMPRESA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_PESSOA_EMPRESA.class);

    }
        

        } // classe: fim


        