

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOPessoaEmpresaRotina
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOPessoaEmpresaRotina.java
        * TABELA MYSQL:    pessoa_empresa_rotina
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoaEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOPessoaEmpresaRotina extends Table
        {

        public static final String NAME = "pessoa_empresa_rotina";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_PESSOA_EMPRESA_ROTINA{
					id,
 			pessoa_empresa_id_INT,
 			semana_INT,
 			dia_semana_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String PESSOA_EMPRESA_ID_INT = "pessoa_empresa_id_INT";
		public static final String SEMANA_INT = "semana_INT";
		public static final String DIA_SEMANA_INT = "dia_semana_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOPessoaEmpresaRotina.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOPessoaEmpresaRotina(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    PESSOA_EMPRESA_ID_INT, 
                    DAOPessoaEmpresa.NAME,
                    DAOPessoaEmpresa.ID,
                    "pessoa_empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "pessoa_empresa_rotina_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    SEMANA_INT, 
                    "semana_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    4,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DIA_SEMANA_INT, 
                    "dia_semana_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    4,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "pessoa_empresa_rotina_ibfk_2"
                )
            );

			addKey("pessoa_empresa_rotina_FK_188354492", new String[] { PESSOA_EMPRESA_ID_INT });
			addKey("pessoa_empresa_rotina_FK_969879151", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_PESSOA_EMPRESA_ROTINA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_PESSOA_EMPRESA_ROTINA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_PESSOA_EMPRESA_ROTINA.class);

    }
        

        } // classe: fim


        