

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOPessoaUsuario
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOPessoaUsuario.java
        * TABELA MYSQL:    pessoa_usuario
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOPessoaUsuario extends Table
        {

        public static final String NAME = "pessoa_usuario";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_PESSOA_USUARIO{
					id,
 			pessoa_id_INT,
 			usuario_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String PESSOA_ID_INT = "pessoa_id_INT";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOPessoaUsuario.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOPessoaUsuario(Database database){
            super(NAME, database);
            setUniqueKey( "pessoa_usuario_UNIQUE", new String[]{ PESSOA_ID_INT, USUARIO_ID_INT, CORPORACAO_ID_INT});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    PESSOA_ID_INT, 
                    DAOPessoa.NAME,
                    DAOPessoa.ID,
                    "pessoa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "pessoa_usuario_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "pessoa_usuario_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "pessoa_usuario_ibfk_3"
                )
            );

			addKey("pessoa_usuario_FK_347076416", new String[] { PESSOA_ID_INT });
			addKey("pessoa_usuario_FK_629943848", new String[] { USUARIO_ID_INT });
			addKey("pessoa_usuario_FK_759368897", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_PESSOA_USUARIO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_PESSOA_USUARIO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_PESSOA_USUARIO.class);

    }
        

        } // classe: fim


        