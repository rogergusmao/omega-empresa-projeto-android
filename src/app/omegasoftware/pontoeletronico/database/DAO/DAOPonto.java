

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOPonto
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOPonto.java
        * TABELA MYSQL:    ponto
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOProfissao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoPonto;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOPonto extends Table
        {

        public static final String NAME = "ponto";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_PONTO{
					id,
 			pessoa_id_INT,
 			foto,
 			empresa_id_INT,
 			profissao_id_INT,
 			usuario_id_INT,
 			is_entrada_BOOLEAN,
 			tipo_ponto_id_INT,
 			corporacao_id_INT,
 			latitude_INT,
 			longitude_INT,
 			data_SEC,
 			data_OFFSEC,
 			descricao,
 			endereco};
		public static final String ID = "id";
		public static final String PESSOA_ID_INT = "pessoa_id_INT";
		public static final String FOTO = "foto";
		public static final String EMPRESA_ID_INT = "empresa_id_INT";
		public static final String PROFISSAO_ID_INT = "profissao_id_INT";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String IS_ENTRADA_BOOLEAN = "is_entrada_BOOLEAN";
		public static final String TIPO_PONTO_ID_INT = "tipo_ponto_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String LATITUDE_INT = "latitude_INT";
		public static final String LONGITUDE_INT = "longitude_INT";
		public static final String DATA_SEC = "data_SEC";
		public static final String DATA_OFFSEC = "data_OFFSEC";
		public static final String DESCRICAO = "descricao";
		public static final String ENDERECO = "endereco";


		public static String TABELAS_RELACIONADAS[] = {DAOPonto.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOPonto(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    PESSOA_ID_INT, 
                    DAOPessoa.NAME,
                    DAOPessoa.ID,
                    "pessoa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "ponto_ibfk_6"
                )
            );

            super.addAttribute(
                new Attribute(
                    FOTO, 
                    "foto",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    50,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "ponto_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    PROFISSAO_ID_INT, 
                    DAOProfissao.NAME,
                    DAOProfissao.ID,
                    "profissao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "ponto_ibfk_4"
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "ponto_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    IS_ENTRADA_BOOLEAN, 
                    "is_entrada_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TIPO_PONTO_ID_INT, 
                    DAOTipoPonto.NAME,
                    DAOTipoPonto.ID,
                    "tipo_ponto_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "ponto_ibfk_5"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "ponto_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    LATITUDE_INT, 
                    "latitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    LONGITUDE_INT, 
                    "longitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_SEC, 
                    "data_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_OFFSEC, 
                    "data_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DESCRICAO, 
                    "descricao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ENDERECO, 
                    "endereco",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

			addKey("ponto_FK_228240967", new String[] { CORPORACAO_ID_INT });
			addKey("ponto_FK_329772949", new String[] { EMPRESA_ID_INT });
			addKey("ponto_FK_160339355", new String[] { PROFISSAO_ID_INT });
			addKey("ponto_FK_509979248", new String[] { USUARIO_ID_INT });
			addKey("ponto_FK_1234567", new String[] { TIPO_PONTO_ID_INT });
			addKey("pessoa_id_INT", new String[] { PESSOA_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_PONTO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_PONTO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_PONTO.class);

    }
        

        } // classe: fim


        