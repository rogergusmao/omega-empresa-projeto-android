

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOProduto
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOProduto.java
        * TABELA MYSQL:    produto
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOProdutoUnidadeMedida;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOProduto extends Table
        {

        public static final String NAME = "produto";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_PRODUTO{
					id,
 			identificador,
 			nome,
 			nome_normalizado,
 			descricao,
 			produto_unidade_medida_id_INT,
 			id_omega_INT,
 			cadastro_SEC,
 			cadastro_OFFSEC,
 			preco_custo_FLOAT,
 			prazo_reposicao_estoque_dias_INT,
 			codigo_barra,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String IDENTIFICADOR = "identificador";
		public static final String NOME = "nome";
		public static final String NOME_NORMALIZADO = "nome_normalizado";
		public static final String DESCRICAO = "descricao";
		public static final String PRODUTO_UNIDADE_MEDIDA_ID_INT = "produto_unidade_medida_id_INT";
		public static final String ID_OMEGA_INT = "id_omega_INT";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";
		public static final String PRECO_CUSTO_FLOAT = "preco_custo_FLOAT";
		public static final String PRAZO_REPOSICAO_ESTOQUE_DIAS_INT = "prazo_reposicao_estoque_dias_INT";
		public static final String CODIGO_BARRA = "codigo_barra";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOProduto.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOProduto(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    IDENTIFICADOR, 
                    "identificador",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(getNewAtributoNormalizado(NOME));

            super.addAttribute(
                new Attribute(
                    DESCRICAO, 
                    "descricao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    512,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PRODUTO_UNIDADE_MEDIDA_ID_INT, 
                    DAOProdutoUnidadeMedida.NAME,
                    DAOProdutoUnidadeMedida.ID,
                    "produto_unidade_medida_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "produto_FK_485992432"
                )
            );

            super.addAttribute(
                new Attribute(
                    ID_OMEGA_INT, 
                    "id_omega_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PRECO_CUSTO_FLOAT, 
                    "preco_custo_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PRAZO_REPOSICAO_ESTOQUE_DIAS_INT, 
                    "prazo_reposicao_estoque_dias_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CODIGO_BARRA, 
                    "codigo_barra",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    13,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "produto_ibfk_1"
                )
            );

			addKey("empresa_produto_FK_183288574", new String[] { CORPORACAO_ID_INT });
			addKey("produto_FK_485992432", new String[] { PRODUTO_UNIDADE_MEDIDA_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_PRODUTO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_PRODUTO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_PRODUTO.class);

    }
        

        } // classe: fim


        