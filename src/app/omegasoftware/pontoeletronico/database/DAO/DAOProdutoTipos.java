

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOProdutoTipos
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOProdutoTipos.java
        * TABELA MYSQL:    produto_tipos
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOProdutoTipo;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOProduto;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOProdutoTipos extends Table
        {

        public static final String NAME = "produto_tipos";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_PRODUTO_TIPOS{
					id,
 			produto_tipo_id_INT,
 			produto_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String PRODUTO_TIPO_ID_INT = "produto_tipo_id_INT";
		public static final String PRODUTO_ID_INT = "produto_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOProdutoTipos.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOProdutoTipos(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    PRODUTO_TIPO_ID_INT, 
                    DAOProdutoTipo.NAME,
                    DAOProdutoTipo.ID,
                    "produto_tipo_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "produto_tipos_FK_90423584"
                )
            );

            super.addAttribute(
                new Attribute(
                    PRODUTO_ID_INT, 
                    DAOProduto.NAME,
                    DAOProduto.ID,
                    "produto_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "produto_tipos_FK_811035157"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "produto_tipos_FK_529571533"
                )
            );

			addKey("produto_tipos_FK_90423584", new String[] { PRODUTO_TIPO_ID_INT });
			addKey("produto_tipos_FK_811035157", new String[] { PRODUTO_ID_INT });
			addKey("produto_tipos_FK_529571533", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_PRODUTO_TIPOS> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_PRODUTO_TIPOS>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_PRODUTO_TIPOS.class);

    }
        

        } // classe: fim


        