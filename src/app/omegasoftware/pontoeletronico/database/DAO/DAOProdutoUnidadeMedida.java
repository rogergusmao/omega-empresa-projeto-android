

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOProdutoUnidadeMedida
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOProdutoUnidadeMedida.java
        * TABELA MYSQL:    produto_unidade_medida
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOProdutoUnidadeMedida extends Table
        {

        public static final String NAME = "produto_unidade_medida";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_PRODUTO_UNIDADE_MEDIDA{
					id,
 			nome,
 			abreviacao,
 			is_float_BOOLEAN};
		public static final String ID = "id";
		public static final String NOME = "nome";
		public static final String ABREVIACAO = "abreviacao";
		public static final String IS_FLOAT_BOOLEAN = "is_float_BOOLEAN";


		public static String TABELAS_RELACIONADAS[] = {DAOProdutoUnidadeMedida.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOProdutoUnidadeMedida(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ABREVIACAO, 
                    "abreviacao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    IS_FLOAT_BOOLEAN, 
                    "is_float_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_PRODUTO_UNIDADE_MEDIDA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_PRODUTO_UNIDADE_MEDIDA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_PRODUTO_UNIDADE_MEDIDA.class);

    }
        

        } // classe: fim


        