

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAORecebivel
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAORecebivel.java
        * TABELA MYSQL:    recebivel
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORelatorio;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAORecebivel extends Table
        {

        public static final String NAME = "recebivel";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_RECEBIVEL{
					id,
 			valor_FLOAT,
 			devedor_empresa_id_INT,
 			devedor_pessoa_id_INT,
 			cadastro_usuario_id_INT,
 			cadastro_SEC,
 			cadastro_OFFSEC,
 			valor_pagamento_FLOAT,
 			pagamento_SEC,
 			pagamento_OFFSEC,
 			recebedor_usuario_id_INT,
 			relatorio_id_INT,
 			protocolo_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String VALOR_FLOAT = "valor_FLOAT";
		public static final String DEVEDOR_EMPRESA_ID_INT = "devedor_empresa_id_INT";
		public static final String DEVEDOR_PESSOA_ID_INT = "devedor_pessoa_id_INT";
		public static final String CADASTRO_USUARIO_ID_INT = "cadastro_usuario_id_INT";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";
		public static final String VALOR_PAGAMENTO_FLOAT = "valor_pagamento_FLOAT";
		public static final String PAGAMENTO_SEC = "pagamento_SEC";
		public static final String PAGAMENTO_OFFSEC = "pagamento_OFFSEC";
		public static final String RECEBEDOR_USUARIO_ID_INT = "recebedor_usuario_id_INT";
		public static final String RELATORIO_ID_INT = "relatorio_id_INT";
		public static final String PROTOCOLO_INT = "protocolo_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAORecebivel.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAORecebivel(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_FLOAT, 
                    "valor_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DEVEDOR_EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "devedor_empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "recebivel_FK_239410400"
                )
            );

            super.addAttribute(
                new Attribute(
                    DEVEDOR_PESSOA_ID_INT, 
                    DAOPessoa.NAME,
                    DAOPessoa.ID,
                    "devedor_pessoa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "recebivel_FK_19348144"
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "cadastro_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "recebivel_FK_829437256"
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_PAGAMENTO_FLOAT, 
                    "valor_pagamento_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PAGAMENTO_SEC, 
                    "pagamento_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PAGAMENTO_OFFSEC, 
                    "pagamento_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    RECEBEDOR_USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "recebedor_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "recebivel_FK_988525391"
                )
            );

            super.addAttribute(
                new Attribute(
                    RELATORIO_ID_INT, 
                    DAORelatorio.NAME,
                    DAORelatorio.ID,
                    "relatorio_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "recebivel_FK_1007080"
                )
            );

            super.addAttribute(
                new Attribute(
                    PROTOCOLO_INT, 
                    "protocolo_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "recebivel_FK_595886231"
                )
            );

			addKey("recebivel_FK_239410400", new String[] { DEVEDOR_EMPRESA_ID_INT });
			addKey("recebivel_FK_19348144", new String[] { DEVEDOR_PESSOA_ID_INT });
			addKey("recebivel_FK_829437256", new String[] { CADASTRO_USUARIO_ID_INT });
			addKey("recebivel_FK_988525391", new String[] { RECEBEDOR_USUARIO_ID_INT });
			addKey("recebivel_FK_1007080", new String[] { RELATORIO_ID_INT });
			addKey("recebivel_FK_595886231", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_RECEBIVEL> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_RECEBIVEL>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_RECEBIVEL.class);

    }
        

        } // classe: fim


        