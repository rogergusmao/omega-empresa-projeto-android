

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAORecebivelCotidiano
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAORecebivelCotidiano.java
        * TABELA MYSQL:    recebivel_cotidiano
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAORecebivelCotidiano extends Table
        {

        public static final String NAME = "recebivel_cotidiano";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_RECEBIVEL_COTIDIANO{
					id,
 			recebedor_empresa_id_INT,
 			cadastro_usuario_id_INT,
 			id_tipo_despesa_recebivel_INT,
 			parametro_INT,
 			parametro_OFFSEC,
 			parametro_SEC,
 			parametro_json,
 			valor_FLOAT,
 			data_limite_cotidiano_SEC,
 			data_limite_cotidiano_OFFSEC,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String RECEBEDOR_EMPRESA_ID_INT = "recebedor_empresa_id_INT";
		public static final String CADASTRO_USUARIO_ID_INT = "cadastro_usuario_id_INT";
		public static final String ID_TIPO_DESPESA_RECEBIVEL_INT = "id_tipo_despesa_recebivel_INT";
		public static final String PARAMETRO_INT = "parametro_INT";
		public static final String PARAMETRO_OFFSEC = "parametro_OFFSEC";
		public static final String PARAMETRO_SEC = "parametro_SEC";
		public static final String PARAMETRO_JSON = "parametro_json";
		public static final String VALOR_FLOAT = "valor_FLOAT";
		public static final String DATA_LIMITE_COTIDIANO_SEC = "data_limite_cotidiano_SEC";
		public static final String DATA_LIMITE_COTIDIANO_OFFSEC = "data_limite_cotidiano_OFFSEC";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAORecebivelCotidiano.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAORecebivelCotidiano(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    RECEBEDOR_EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "recebedor_empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "recebivel_cotidiano_FK_447082519"
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "cadastro_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "recebivel_cotidiano_FK_452484131"
                )
            );

            super.addAttribute(
                new Attribute(
                    ID_TIPO_DESPESA_RECEBIVEL_INT, 
                    "id_tipo_despesa_recebivel_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    3,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PARAMETRO_INT, 
                    "parametro_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PARAMETRO_OFFSEC, 
                    "parametro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PARAMETRO_SEC, 
                    "parametro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PARAMETRO_JSON, 
                    "parametro_json",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    512,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_FLOAT, 
                    "valor_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_LIMITE_COTIDIANO_SEC, 
                    "data_limite_cotidiano_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_LIMITE_COTIDIANO_OFFSEC, 
                    "data_limite_cotidiano_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "recebivel_cotidiano_FK_900634766"
                )
            );

			addKey("recebivel_cotidiano_FK_447082519", new String[] { RECEBEDOR_EMPRESA_ID_INT });
			addKey("recebivel_cotidiano_FK_452484131", new String[] { CADASTRO_USUARIO_ID_INT });
			addKey("recebivel_cotidiano_FK_900634766", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_RECEBIVEL_COTIDIANO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_RECEBIVEL_COTIDIANO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_RECEBIVEL_COTIDIANO.class);

    }
        

        } // classe: fim


        