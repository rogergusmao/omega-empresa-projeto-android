

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAORegistro
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAORegistro.java
        * TABELA MYSQL:    registro
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCategoriaPermissao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoRegistro;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstado;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstadoCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAORegistro extends Table
        {

        public static final String NAME = "registro";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_REGISTRO{
					id,
 			protocolo_INT,
 			cadastro_SEC,
 			cadastro_OFFSEC,
 			responsavel_usuario_id_INT,
 			responsavel_categoria_permissao_id_INT,
 			descricao,
 			tipo_registro_id_INT,
 			registro_estado_id_INT,
 			registro_estado_corporacao_id_INT,
 			id_origem_chamada_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String PROTOCOLO_INT = "protocolo_INT";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";
		public static final String RESPONSAVEL_USUARIO_ID_INT = "responsavel_usuario_id_INT";
		public static final String RESPONSAVEL_CATEGORIA_PERMISSAO_ID_INT = "responsavel_categoria_permissao_id_INT";
		public static final String DESCRICAO = "descricao";
		public static final String TIPO_REGISTRO_ID_INT = "tipo_registro_id_INT";
		public static final String REGISTRO_ESTADO_ID_INT = "registro_estado_id_INT";
		public static final String REGISTRO_ESTADO_CORPORACAO_ID_INT = "registro_estado_corporacao_id_INT";
		public static final String ID_ORIGEM_CHAMADA_INT = "id_origem_chamada_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAORegistro.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAORegistro(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    PROTOCOLO_INT, 
                    "protocolo_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    RESPONSAVEL_USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "responsavel_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_FK_991607667"
                )
            );

            super.addAttribute(
                new Attribute(
                    RESPONSAVEL_CATEGORIA_PERMISSAO_ID_INT, 
                    DAOCategoriaPermissao.NAME,
                    DAOCategoriaPermissao.ID,
                    "responsavel_categoria_permissao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_FK_883850098"
                )
            );

            super.addAttribute(
                new Attribute(
                    DESCRICAO, 
                    "descricao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TIPO_REGISTRO_ID_INT, 
                    DAOTipoRegistro.NAME,
                    DAOTipoRegistro.ID,
                    "tipo_registro_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_FK_435150146"
                )
            );

            super.addAttribute(
                new Attribute(
                    REGISTRO_ESTADO_ID_INT, 
                    DAORegistroEstado.NAME,
                    DAORegistroEstado.ID,
                    "registro_estado_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_FK_886230469"
                )
            );

            super.addAttribute(
                new Attribute(
                    REGISTRO_ESTADO_CORPORACAO_ID_INT, 
                    DAORegistroEstadoCorporacao.NAME,
                    DAORegistroEstadoCorporacao.ID,
                    "registro_estado_corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_FK_225860595"
                )
            );

            super.addAttribute(
                new Attribute(
                    ID_ORIGEM_CHAMADA_INT, 
                    "id_origem_chamada_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    3,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_FK_979919434"
                )
            );

			addKey("registro_FK_991607667", new String[] { RESPONSAVEL_USUARIO_ID_INT });
			addKey("registro_FK_883850098", new String[] { RESPONSAVEL_CATEGORIA_PERMISSAO_ID_INT });
			addKey("registro_FK_435150146", new String[] { TIPO_REGISTRO_ID_INT });
			addKey("registro_FK_886230469", new String[] { REGISTRO_ESTADO_ID_INT });
			addKey("registro_FK_225860595", new String[] { REGISTRO_ESTADO_CORPORACAO_ID_INT });
			addKey("registro_FK_979919434", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_REGISTRO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_REGISTRO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_REGISTRO.class);

    }
        

        } // classe: fim


        