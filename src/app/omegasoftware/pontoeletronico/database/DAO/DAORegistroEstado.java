

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAORegistroEstado
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAORegistroEstado.java
        * TABELA MYSQL:    registro_estado
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCategoriaPermissao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoRegistro;


        public abstract class DAORegistroEstado extends Table
        {

        public static final String NAME = "registro_estado";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_REGISTRO_ESTADO{
					id,
 			nome,
 			prazo_dias_INT,
 			tipo_registro_id_INT};
		public static final String ID = "id";
		public static final String NOME = "nome";

		public static final String PRAZO_DIAS_INT = "prazo_dias_INT";
		public static final String TIPO_REGISTRO_ID_INT = "tipo_registro_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAORegistroEstado.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAORegistroEstado(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    50,
                    false
                )
            );


            super.addAttribute(
                new Attribute(
                    PRAZO_DIAS_INT, 
                    "prazo_dias_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    3,
                    false
                )
            );
            super.addAttribute(
                new Attribute(
                    TIPO_REGISTRO_ID_INT, 
                    DAOTipoRegistro.NAME,
                    DAOTipoRegistro.ID,
                    "tipo_registro_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_estado_FK_634155274"
                )
            );
//			addKey("registro_estado_FK_27282714", new String[] { RESPONSAVEL_USUARIO_ID_INT });
//			addKey("registro_estado_FK_131805420", new String[] { RESPONSAVEL_CATEGORIA_PERMISSAO_ID_INT });
			addKey("registro_estado_FK_634155274", new String[] { TIPO_REGISTRO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_REGISTRO_ESTADO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_REGISTRO_ESTADO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_REGISTRO_ESTADO.class);

    }
        

        } // classe: fim


        