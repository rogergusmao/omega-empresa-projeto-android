

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAORegistroEstadoCorporacao
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAORegistroEstadoCorporacao.java
        * TABELA MYSQL:    registro_estado_corporacao
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCategoriaPermissao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoRegistro;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAORegistroEstadoCorporacao extends Table
        {

        public static final String NAME = "registro_estado_corporacao";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_REGISTRO_ESTADO_CORPORACAO{
					id,
 			nome,
 			responsavel_usuario_id_INT,
 			responsavel_categoria_permissao_id_INT,
 			prazo_dias_INT,
 			tipo_registro_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String NOME = "nome";
		public static final String RESPONSAVEL_USUARIO_ID_INT = "responsavel_usuario_id_INT";
		public static final String RESPONSAVEL_CATEGORIA_PERMISSAO_ID_INT = "responsavel_categoria_permissao_id_INT";
		public static final String PRAZO_DIAS_INT = "prazo_dias_INT";
		public static final String TIPO_REGISTRO_ID_INT = "tipo_registro_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAORegistroEstadoCorporacao.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAORegistroEstadoCorporacao(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    50,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    RESPONSAVEL_USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "responsavel_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_estado_corporacao_FK_488037109"
                )
            );

            super.addAttribute(
                new Attribute(
                    RESPONSAVEL_CATEGORIA_PERMISSAO_ID_INT, 
                    DAOCategoriaPermissao.NAME,
                    DAOCategoriaPermissao.ID,
                    "responsavel_categoria_permissao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_estado_corporacao_FK_697784424"
                )
            );

            super.addAttribute(
                new Attribute(
                    PRAZO_DIAS_INT, 
                    "prazo_dias_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    3,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TIPO_REGISTRO_ID_INT, 
                    DAOTipoRegistro.NAME,
                    DAOTipoRegistro.ID,
                    "tipo_registro_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_estado_corporacao_FK_794616700"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_estado_corporacao_FK_896209717"
                )
            );

			addKey("registro_estado_corporacao_FK_488037109", new String[] { RESPONSAVEL_USUARIO_ID_INT });
			addKey("registro_estado_corporacao_FK_697784424", new String[] { RESPONSAVEL_CATEGORIA_PERMISSAO_ID_INT });
			addKey("registro_estado_corporacao_FK_794616700", new String[] { TIPO_REGISTRO_ID_INT });
			addKey("registro_estado_corporacao_FK_896209717", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_REGISTRO_ESTADO_CORPORACAO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_REGISTRO_ESTADO_CORPORACAO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_REGISTRO_ESTADO_CORPORACAO.class);

    }
        

        } // classe: fim


        