

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAORegistroFluxoEstadoCorporacao
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAORegistroFluxoEstadoCorporacao.java
        * TABELA MYSQL:    registro_fluxo_estado_corporacao
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstadoCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstado;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstadoCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstado;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAORegistroFluxoEstadoCorporacao extends Table
        {

        public static final String NAME = "registro_fluxo_estado_corporacao";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_REGISTRO_FLUXO_ESTADO_CORPORACAO{
					id,
 			origem_registro_estado_corporacao_id_INT,
 			origem_registro_estado_id_INT,
 			destino_registro_estado_corporacao_id_INT,
 			destino_registro_estado_id_INT,
 			template_JSON,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String ORIGEM_REGISTRO_ESTADO_CORPORACAO_ID_INT = "origem_registro_estado_corporacao_id_INT";
		public static final String ORIGEM_REGISTRO_ESTADO_ID_INT = "origem_registro_estado_id_INT";
		public static final String DESTINO_REGISTRO_ESTADO_CORPORACAO_ID_INT = "destino_registro_estado_corporacao_id_INT";
		public static final String DESTINO_REGISTRO_ESTADO_ID_INT = "destino_registro_estado_id_INT";
		public static final String TEMPLATE_JSON = "template_JSON";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAORegistroFluxoEstadoCorporacao.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAORegistroFluxoEstadoCorporacao(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    ORIGEM_REGISTRO_ESTADO_CORPORACAO_ID_INT, 
                    DAORegistroEstadoCorporacao.NAME,
                    DAORegistroEstadoCorporacao.ID,
                    "origem_registro_estado_corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_fluxo_estado_corporacao_FK_25970459"
                )
            );

            super.addAttribute(
                new Attribute(
                    ORIGEM_REGISTRO_ESTADO_ID_INT, 
                    DAORegistroEstado.NAME,
                    DAORegistroEstado.ID,
                    "origem_registro_estado_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_fluxo_estado_corporacao_FK_785644532"
                )
            );

            super.addAttribute(
                new Attribute(
                    DESTINO_REGISTRO_ESTADO_CORPORACAO_ID_INT, 
                    DAORegistroEstadoCorporacao.NAME,
                    DAORegistroEstadoCorporacao.ID,
                    "destino_registro_estado_corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_fluxo_estado_corporacao_FK_761993408"
                )
            );

            super.addAttribute(
                new Attribute(
                    DESTINO_REGISTRO_ESTADO_ID_INT, 
                    DAORegistroEstado.NAME,
                    DAORegistroEstado.ID,
                    "destino_registro_estado_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_fluxo_estado_corporacao_FK_918395997"
                )
            );

            super.addAttribute(
                new Attribute(
                    TEMPLATE_JSON, 
                    "template_JSON",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    512,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "registro_fluxo_estado_corporacao_FK_13153076"
                )
            );

			addKey("registro_fluxo_estado_corporacao_FK_25970459", new String[] { ORIGEM_REGISTRO_ESTADO_CORPORACAO_ID_INT });
			addKey("registro_fluxo_estado_corporacao_FK_785644532", new String[] { ORIGEM_REGISTRO_ESTADO_ID_INT });
			addKey("registro_fluxo_estado_corporacao_FK_761993408", new String[] { DESTINO_REGISTRO_ESTADO_CORPORACAO_ID_INT });
			addKey("registro_fluxo_estado_corporacao_FK_918395997", new String[] { DESTINO_REGISTRO_ESTADO_ID_INT });
			addKey("registro_fluxo_estado_corporacao_FK_13153076", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_REGISTRO_FLUXO_ESTADO_CORPORACAO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_REGISTRO_FLUXO_ESTADO_CORPORACAO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_REGISTRO_FLUXO_ESTADO_CORPORACAO.class);

    }
        

        } // classe: fim


        