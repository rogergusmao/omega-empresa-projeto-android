

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAORelatorio
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAORelatorio.java
        * TABELA MYSQL:    relatorio
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoRelatorio;


        public abstract class DAORelatorio extends Table
        {

        public static final String NAME = "relatorio";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_RELATORIO{
					id,
 			titulo,
 			descricao,
 			usuario_id_INT,
 			corporacao_id_INT,
 			data_SEC,
 			data_OFFSEC,
 			tipo_relatorio_id_INT};
		public static final String ID = "id";
		public static final String TITULO = "titulo";
		public static final String DESCRICAO = "descricao";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String DATA_SEC = "data_SEC";
		public static final String DATA_OFFSEC = "data_OFFSEC";
		public static final String TIPO_RELATORIO_ID_INT = "tipo_relatorio_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAORelatorio.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAORelatorio(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    TITULO, 
                    "titulo",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DESCRICAO, 
                    "descricao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "relatorio_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "relatorio_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_SEC, 
                    "data_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_OFFSEC, 
                    "data_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TIPO_RELATORIO_ID_INT, 
                    DAOTipoRelatorio.NAME,
                    DAOTipoRelatorio.ID,
                    "tipo_relatorio_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "relatorio_FK_984588624"
                )
            );

			addKey("relatorio_FK_445983887", new String[] { USUARIO_ID_INT });
			addKey("relatorio_FK_395233154", new String[] { CORPORACAO_ID_INT });
			addKey("relatorio_FK_984588624", new String[] { TIPO_RELATORIO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_RELATORIO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_RELATORIO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_RELATORIO.class);

    }
        

        } // classe: fim


        