

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAORelatorioAnexo
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAORelatorioAnexo.java
        * TABELA MYSQL:    relatorio_anexo
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORelatorio;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoAnexo;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAORelatorioAnexo extends Table
        {

        public static final String NAME = "relatorio_anexo";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_RELATORIO_ANEXO{
					id,
 			relatorio_id_INT,
 			arquivo,
 			tipo_anexo_id_INT,
 			corporacao_id_INT,
 			descricao};
		public static final String ID = "id";
		public static final String RELATORIO_ID_INT = "relatorio_id_INT";
		public static final String ARQUIVO = "arquivo";
		public static final String TIPO_ANEXO_ID_INT = "tipo_anexo_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String DESCRICAO = "descricao";


		public static String TABELAS_RELACIONADAS[] = {DAORelatorioAnexo.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAORelatorioAnexo(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    RELATORIO_ID_INT, 
                    DAORelatorio.NAME,
                    DAORelatorio.ID,
                    "relatorio_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "relatorio_anexo_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    ARQUIVO, 
                    "arquivo",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    50,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TIPO_ANEXO_ID_INT, 
                    DAOTipoAnexo.NAME,
                    DAOTipoAnexo.ID,
                    "tipo_anexo_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "relatorio_anexo_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "relatorio_anexo_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    DESCRICAO, 
                    "descricao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

			addKey("relatorio_anexo_FK_268829345", new String[] { RELATORIO_ID_INT });
			addKey("relatorio_anexo_FK_663513184", new String[] { TIPO_ANEXO_ID_INT });
			addKey("relatorio_anexo_FK_730926514", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_RELATORIO_ANEXO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_RELATORIO_ANEXO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_RELATORIO_ANEXO.class);

    }
        

        } // classe: fim


        