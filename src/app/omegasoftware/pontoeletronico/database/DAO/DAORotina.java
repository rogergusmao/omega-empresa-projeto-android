

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAORotina
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAORotina.java
        * TABELA MYSQL:    rotina
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAORotina extends Table
        {

        public static final String NAME = "rotina";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_ROTINA{
					id,
 			ultima_dia_contagem_ciclo_DATE,
 			corporacao_id_INT,
 			ultima_dia_contagem_ciclo_data_SEC,
 			ultima_dia_contagem_ciclo_data_OFFSEC};
		public static final String ID = "id";
		public static final String ULTIMA_DIA_CONTAGEM_CICLO_DATE = "ultima_dia_contagem_ciclo_DATE";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String ULTIMA_DIA_CONTAGEM_CICLO_DATA_SEC = "ultima_dia_contagem_ciclo_data_SEC";
		public static final String ULTIMA_DIA_CONTAGEM_CICLO_DATA_OFFSEC = "ultima_dia_contagem_ciclo_data_OFFSEC";


		public static String TABELAS_RELACIONADAS[] = {DAORotina.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAORotina(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    ULTIMA_DIA_CONTAGEM_CICLO_DATE, 
                    "ultima_dia_contagem_ciclo_DATE",
                    true,
                    SQLLITE_TYPE.DATE,
                    false,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "rotina_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    ULTIMA_DIA_CONTAGEM_CICLO_DATA_SEC, 
                    "ultima_dia_contagem_ciclo_data_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ULTIMA_DIA_CONTAGEM_CICLO_DATA_OFFSEC, 
                    "ultima_dia_contagem_ciclo_data_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

			addKey("rotina_FK_259857177", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_ROTINA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_ROTINA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_ROTINA.class);

    }
        

        } // classe: fim


        