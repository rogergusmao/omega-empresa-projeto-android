

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaArestaGrafoHierarquiaBanco
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaArestaGrafoHierarquiaBanco.java
        * TABELA MYSQL:    sistema_aresta_grafo_hierarquia_banco
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaGrafoHierarquiaBanco;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaGrafoHierarquiaBanco;


        public abstract class DAOSistemaArestaGrafoHierarquiaBanco extends Table
        {

        public static final String NAME = "sistema_aresta_grafo_hierarquia_banco";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_ARESTA_GRAFO_HIERARQUIA_BANCO{
					id,
 			pai_sistema_grafo_hierarquia_banco_id_INT,
 			filho_sistema_grafo_hierarquia_banco_id_INT};
		public static final String ID = "id";
		public static final String PAI_SISTEMA_GRAFO_HIERARQUIA_BANCO_ID_INT = "pai_sistema_grafo_hierarquia_banco_id_INT";
		public static final String FILHO_SISTEMA_GRAFO_HIERARQUIA_BANCO_ID_INT = "filho_sistema_grafo_hierarquia_banco_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaArestaGrafoHierarquiaBanco.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaArestaGrafoHierarquiaBanco(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    PAI_SISTEMA_GRAFO_HIERARQUIA_BANCO_ID_INT, 
                    DAOSistemaGrafoHierarquiaBanco.NAME,
                    DAOSistemaGrafoHierarquiaBanco.ID,
                    "pai_sistema_grafo_hierarquia_banco_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_aresta_grafo_hierarquia_banco_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    FILHO_SISTEMA_GRAFO_HIERARQUIA_BANCO_ID_INT, 
                    DAOSistemaGrafoHierarquiaBanco.NAME,
                    DAOSistemaGrafoHierarquiaBanco.ID,
                    "filho_sistema_grafo_hierarquia_banco_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_aresta_grafo_hierarquia_banco_ibfk_2"
                )
            );

			addKey("key_4448ceb723e91dae", new String[] { PAI_SISTEMA_GRAFO_HIERARQUIA_BANCO_ID_INT });
			addKey("key_6aa89b0c65a803a0", new String[] { FILHO_SISTEMA_GRAFO_HIERARQUIA_BANCO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_ARESTA_GRAFO_HIERARQUIA_BANCO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_ARESTA_GRAFO_HIERARQUIA_BANCO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_ARESTA_GRAFO_HIERARQUIA_BANCO.class);

    }
        

        } // classe: fim


        