

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaAtributo
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaAtributo.java
        * TABELA MYSQL:    sistema_atributo
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaTabela;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaTabela;


        public abstract class DAOSistemaAtributo extends Table
        {

        public static final String NAME = "sistema_atributo";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_ATRIBUTO{
					id,
 			nome,
 			sistema_tabela_id_INT,
 			tipo_sql,
 			tamanho_INT,
 			decimal_INT,
 			not_null_BOOLEAN,
 			primary_key_BOOLEAN,
 			auto_increment_BOOLEAN,
 			valor_default,
 			fk_sistema_tabela_id_INT,
 			atributo_fk,
 			fk_nome,
 			update_tipo_fk,
 			delete_tipo_fk,
 			label,
 			unique_BOOLEAN,
 			unique_nome,
 			seq_INT,
 			excluido_BOOLEAN,
 			nome_exibicao,
 			tipo_sql_ficticio};
		public static final String ID = "id";
		public static final String NOME = "nome";
		public static final String SISTEMA_TABELA_ID_INT = "sistema_tabela_id_INT";
		public static final String TIPO_SQL = "tipo_sql";
		public static final String TAMANHO_INT = "tamanho_INT";
		public static final String DECIMAL_INT = "decimal_INT";
		public static final String NOT_NULL_BOOLEAN = "not_null_BOOLEAN";
		public static final String PRIMARY_KEY_BOOLEAN = "primary_key_BOOLEAN";
		public static final String AUTO_INCREMENT_BOOLEAN = "auto_increment_BOOLEAN";
		public static final String VALOR_DEFAULT = "valor_default";
		public static final String FK_SISTEMA_TABELA_ID_INT = "fk_sistema_tabela_id_INT";
		public static final String ATRIBUTO_FK = "atributo_fk";
		public static final String FK_NOME = "fk_nome";
		public static final String UPDATE_TIPO_FK = "update_tipo_fk";
		public static final String DELETE_TIPO_FK = "delete_tipo_fk";
		public static final String LABEL = "label";
		public static final String UNIQUE_BOOLEAN = "unique_BOOLEAN";
		public static final String UNIQUE_NOME = "unique_nome";
		public static final String SEQ_INT = "seq_INT";
		public static final String EXCLUIDO_BOOLEAN = "excluido_BOOLEAN";
		public static final String NOME_EXIBICAO = "nome_exibicao";
		public static final String TIPO_SQL_FICTICIO = "tipo_sql_ficticio";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaAtributo.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaAtributo(Database database){
            super(NAME, database);
            setUniqueKey( "nome_irtu", new String[]{ NOME, SISTEMA_TABELA_ID_INT});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    128,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SISTEMA_TABELA_ID_INT, 
                    DAOSistemaTabela.NAME,
                    DAOSistemaTabela.ID,
                    "sistema_tabela_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sa_sistema_tabela_FK"
                )
            );

            super.addAttribute(
                new Attribute(
                    TIPO_SQL, 
                    "tipo_sql",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    15,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TAMANHO_INT, 
                    "tamanho_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DECIMAL_INT, 
                    "decimal_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    NOT_NULL_BOOLEAN, 
                    "not_null_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PRIMARY_KEY_BOOLEAN, 
                    "primary_key_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    AUTO_INCREMENT_BOOLEAN, 
                    "auto_increment_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_DEFAULT, 
                    "valor_default",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    FK_SISTEMA_TABELA_ID_INT, 
                    DAOSistemaTabela.NAME,
                    DAOSistemaTabela.ID,
                    "fk_sistema_tabela_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_atributo_FK_622131348"
                )
            );

            super.addAttribute(
                new Attribute(
                    ATRIBUTO_FK, 
                    "atributo_fk",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    FK_NOME, 
                    "fk_nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    UPDATE_TIPO_FK, 
                    "update_tipo_fk",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DELETE_TIPO_FK, 
                    "delete_tipo_fk",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    LABEL, 
                    "label",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    UNIQUE_BOOLEAN, 
                    "unique_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    UNIQUE_NOME, 
                    "unique_nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SEQ_INT, 
                    "seq_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    EXCLUIDO_BOOLEAN, 
                    "excluido_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME_EXIBICAO, 
                    "nome_exibicao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    128,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TIPO_SQL_FICTICIO, 
                    "tipo_sql_ficticio",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    50,
                    false
                )
            );

			addKey("sa_sistema_tabela_FK", new String[] { SISTEMA_TABELA_ID_INT });
			addKey("sistema_atributo_FK_622131348", new String[] { FK_SISTEMA_TABELA_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_ATRIBUTO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_ATRIBUTO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_ATRIBUTO.class);

    }
        

        } // classe: fim


        