

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaBancoVersao
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaBancoVersao.java
        * TABELA MYSQL:    sistema_banco_versao
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOSistemaBancoVersao extends Table
        {

        public static final String NAME = "sistema_banco_versao";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_BANCO_VERSAO{
					id,
 			nome,
 			data_inicio_estrutura_DATETIME,
 			data_fim_estrutura_DATETIME};
		public static final String ID = "id";
		public static final String NOME = "nome";
		public static final String DATA_INICIO_ESTRUTURA_DATETIME = "data_inicio_estrutura_DATETIME";
		public static final String DATA_FIM_ESTRUTURA_DATETIME = "data_fim_estrutura_DATETIME";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaBancoVersao.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaBancoVersao(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    50,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_INICIO_ESTRUTURA_DATETIME, 
                    "data_inicio_estrutura_DATETIME",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_FIM_ESTRUTURA_DATETIME, 
                    "data_fim_estrutura_DATETIME",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_BANCO_VERSAO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_BANCO_VERSAO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_BANCO_VERSAO.class);

    }
        

        } // classe: fim


        