

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaCorporacaoSincronizador
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaCorporacaoSincronizador.java
        * TABELA MYSQL:    sistema_corporacao_sincronizador
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOSistemaCorporacaoSincronizador extends Table
        {

        public static final String NAME = "sistema_corporacao_sincronizador";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_CORPORACAO_SINCRONIZADOR{
					id,
 			dia_DATE,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String DIA_DATE = "dia_DATE";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaCorporacaoSincronizador.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaCorporacaoSincronizador(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    DIA_DATE, 
                    "dia_DATE",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_corporacao_sincronizador_ibfk_1"
                )
            );

			addKey("key_7f3c58196614913a", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_CORPORACAO_SINCRONIZADOR> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_CORPORACAO_SINCRONIZADOR>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_CORPORACAO_SINCRONIZADOR.class);

    }
        

        } // classe: fim


        