

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaCorporacaoSincronizadorTabela
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaCorporacaoSincronizadorTabela.java
        * TABELA MYSQL:    sistema_corporacao_sincronizador_tabela
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaCorporacaoSincronizador;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaTabela;


        public abstract class DAOSistemaCorporacaoSincronizadorTabela extends Table
        {

        public static final String NAME = "sistema_corporacao_sincronizador_tabela";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_CORPORACAO_SINCRONIZADOR_TABELA{
					id,
 			dia_DATE,
 			sistema_corporacao_sincronizador_id_INT,
 			sistema_tabela_id_INT};
		public static final String ID = "id";
		public static final String DIA_DATE = "dia_DATE";
		public static final String SISTEMA_CORPORACAO_SINCRONIZADOR_ID_INT = "sistema_corporacao_sincronizador_id_INT";
		public static final String SISTEMA_TABELA_ID_INT = "sistema_tabela_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaCorporacaoSincronizadorTabela.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaCorporacaoSincronizadorTabela(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    DIA_DATE, 
                    "dia_DATE",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SISTEMA_CORPORACAO_SINCRONIZADOR_ID_INT, 
                    DAOSistemaCorporacaoSincronizador.NAME,
                    DAOSistemaCorporacaoSincronizador.ID,
                    "sistema_corporacao_sincronizador_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_corporacao_sincronizador_tabela_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    SISTEMA_TABELA_ID_INT, 
                    DAOSistemaTabela.NAME,
                    DAOSistemaTabela.ID,
                    "sistema_tabela_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_corporacao_sincronizador_tabela_ibfk_2"
                )
            );

			addKey("key_b2516f4edddfa18e", new String[] { SISTEMA_CORPORACAO_SINCRONIZADOR_ID_INT });
			addKey("key_fa11478dd8ccc0c8", new String[] { SISTEMA_TABELA_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_CORPORACAO_SINCRONIZADOR_TABELA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_CORPORACAO_SINCRONIZADOR_TABELA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_CORPORACAO_SINCRONIZADOR_TABELA.class);

    }
        

        } // classe: fim


        