

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaCrudEstado
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaCrudEstado.java
        * TABELA MYSQL:    sistema_crud_estado
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaTipoOperacaoBanco;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOSistemaCrudEstado extends Table
        {

        public static final String NAME = "sistema_crud_estado";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_CRUD_ESTADO{
					id,
 			id_crud_INT,
 			id_crud_mobile_INT,
 			id_estado_crud_INT,
 			id_estado_crud_mobile_INT,
 			sistema_tipo_operacao_banco_id_INT,
 			id_sincronizador_mobile_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String ID_CRUD_INT = "id_crud_INT";
		public static final String ID_CRUD_MOBILE_INT = "id_crud_mobile_INT";
		public static final String ID_ESTADO_CRUD_INT = "id_estado_crud_INT";
		public static final String ID_ESTADO_CRUD_MOBILE_INT = "id_estado_crud_mobile_INT";
		public static final String SISTEMA_TIPO_OPERACAO_BANCO_ID_INT = "sistema_tipo_operacao_banco_id_INT";
		public static final String ID_SINCRONIZADOR_MOBILE_INT = "id_sincronizador_mobile_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaCrudEstado.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaCrudEstado(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    ID_CRUD_INT, 
                    "id_crud_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ID_CRUD_MOBILE_INT, 
                    "id_crud_mobile_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ID_ESTADO_CRUD_INT, 
                    "id_estado_crud_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ID_ESTADO_CRUD_MOBILE_INT, 
                    "id_estado_crud_mobile_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SISTEMA_TIPO_OPERACAO_BANCO_ID_INT, 
                    DAOSistemaTipoOperacaoBanco.NAME,
                    DAOSistemaTipoOperacaoBanco.ID,
                    "sistema_tipo_operacao_banco_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_crud_estado_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    ID_SINCRONIZADOR_MOBILE_INT, 
                    "id_sincronizador_mobile_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_crud_estado_ibfk_2"
                )
            );

			addKey("sistema_tipo_operacao_banco_id_INT", new String[] { SISTEMA_TIPO_OPERACAO_BANCO_ID_INT });
			addKey("corporacao_id_INT", new String[] { CORPORACAO_ID_INT });
			addKey("id_crud_INT", new String[] { ID_CRUD_INT });
			addKey("id_crud_mobile_INT", new String[] { ID_CRUD_MOBILE_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_CRUD_ESTADO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_CRUD_ESTADO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_CRUD_ESTADO.class);

    }
        

        } // classe: fim


        