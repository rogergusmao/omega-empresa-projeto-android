

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaDeletarArquivo
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaDeletarArquivo.java
        * TABELA MYSQL:    sistema_deletar_arquivo
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOSistemaDeletarArquivo extends Table
        {

        public static final String NAME = "sistema_deletar_arquivo";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_DELETAR_ARQUIVO{
					id,
 			nome,
 			path};
		public static final String ID = "id";
		public static final String NOME = "nome";
		public static final String PATH = "path";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaDeletarArquivo.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaDeletarArquivo(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    50,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PATH, 
                    "path",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    512,
                    false
                )
            );

        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_DELETAR_ARQUIVO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_DELETAR_ARQUIVO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_DELETAR_ARQUIVO.class);

    }
        

        } // classe: fim


        