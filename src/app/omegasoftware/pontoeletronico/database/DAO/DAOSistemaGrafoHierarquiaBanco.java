

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaGrafoHierarquiaBanco
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaGrafoHierarquiaBanco.java
        * TABELA MYSQL:    sistema_grafo_hierarquia_banco
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaTabela;


        public abstract class DAOSistemaGrafoHierarquiaBanco extends Table
        {

        public static final String NAME = "sistema_grafo_hierarquia_banco";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_GRAFO_HIERARQUIA_BANCO{
					id,
 			grau_INT,
 			numero_dependentes_INT,
 			sistema_tabela_id_INT};
		public static final String ID = "id";
		public static final String GRAU_INT = "grau_INT";
		public static final String NUMERO_DEPENDENTES_INT = "numero_dependentes_INT";
		public static final String SISTEMA_TABELA_ID_INT = "sistema_tabela_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaGrafoHierarquiaBanco.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaGrafoHierarquiaBanco(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    GRAU_INT, 
                    "grau_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    NUMERO_DEPENDENTES_INT, 
                    "numero_dependentes_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SISTEMA_TABELA_ID_INT, 
                    DAOSistemaTabela.NAME,
                    DAOSistemaTabela.ID,
                    "sistema_tabela_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_grafo_hierarquia_banco_ibfk_1"
                )
            );

			addKey("key_6e2ad034ab998732", new String[] { SISTEMA_TABELA_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_GRAFO_HIERARQUIA_BANCO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_GRAFO_HIERARQUIA_BANCO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_GRAFO_HIERARQUIA_BANCO.class);

    }
        

        } // classe: fim


        