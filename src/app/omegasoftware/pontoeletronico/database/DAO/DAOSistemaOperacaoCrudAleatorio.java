

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaOperacaoCrudAleatorio
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaOperacaoCrudAleatorio.java
        * TABELA MYSQL:    sistema_operacao_crud_aleatorio
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaOperacaoSistemaMobile;


        public abstract class DAOSistemaOperacaoCrudAleatorio extends Table
        {

        public static final String NAME = "sistema_operacao_crud_aleatorio";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_OPERACAO_CRUD_ALEATORIO{
					id,
 			operacao_sistema_mobile_id_INT,
 			total_insercao_INT,
 			porcentagem_remocao_FLOAT,
 			porcentagem_edicao_FLOAT,
 			sincronizar_BOOLEAN};
		public static final String ID = "id";
		public static final String OPERACAO_SISTEMA_MOBILE_ID_INT = "operacao_sistema_mobile_id_INT";
		public static final String TOTAL_INSERCAO_INT = "total_insercao_INT";
		public static final String PORCENTAGEM_REMOCAO_FLOAT = "porcentagem_remocao_FLOAT";
		public static final String PORCENTAGEM_EDICAO_FLOAT = "porcentagem_edicao_FLOAT";
		public static final String SINCRONIZAR_BOOLEAN = "sincronizar_BOOLEAN";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaOperacaoCrudAleatorio.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaOperacaoCrudAleatorio(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    OPERACAO_SISTEMA_MOBILE_ID_INT, 
                    DAOSistemaOperacaoSistemaMobile.NAME,
                    DAOSistemaOperacaoSistemaMobile.ID,
                    "operacao_sistema_mobile_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_operacao_crud_aleatorio_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    TOTAL_INSERCAO_INT, 
                    "total_insercao_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PORCENTAGEM_REMOCAO_FLOAT, 
                    "porcentagem_remocao_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    3,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PORCENTAGEM_EDICAO_FLOAT, 
                    "porcentagem_edicao_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    3,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SINCRONIZAR_BOOLEAN, 
                    "sincronizar_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

			addKey("operacao_sistema_mobile_id_INT", new String[] { OPERACAO_SISTEMA_MOBILE_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_OPERACAO_CRUD_ALEATORIO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_OPERACAO_CRUD_ALEATORIO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_OPERACAO_CRUD_ALEATORIO.class);

    }
        

        } // classe: fim


        