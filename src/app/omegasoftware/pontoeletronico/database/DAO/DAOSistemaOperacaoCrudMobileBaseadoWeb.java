

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaOperacaoCrudMobileBaseadoWeb
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaOperacaoCrudMobileBaseadoWeb.java
        * TABELA MYSQL:    sistema_operacao_crud_mobile_baseado_web
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOSistemaOperacaoCrudMobileBaseadoWeb extends Table
        {

        public static final String NAME = "sistema_operacao_crud_mobile_baseado_web";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_OPERACAO_CRUD_MOBILE_BASEADO_WEB{
					id,
 			operacao_sistema_mobile_id_INT,
 			url_json_operacoes_web,
 			sincronizar_BOOLEAN};
		public static final String ID = "id";
		public static final String OPERACAO_SISTEMA_MOBILE_ID_INT = "operacao_sistema_mobile_id_INT";
		public static final String URL_JSON_OPERACOES_WEB = "url_json_operacoes_web";
		public static final String SINCRONIZAR_BOOLEAN = "sincronizar_BOOLEAN";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaOperacaoCrudMobileBaseadoWeb.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaOperacaoCrudMobileBaseadoWeb(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    OPERACAO_SISTEMA_MOBILE_ID_INT, 
                    "operacao_sistema_mobile_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    URL_JSON_OPERACOES_WEB, 
                    "url_json_operacoes_web",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SINCRONIZAR_BOOLEAN, 
                    "sincronizar_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_OPERACAO_CRUD_MOBILE_BASEADO_WEB> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_OPERACAO_CRUD_MOBILE_BASEADO_WEB>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_OPERACAO_CRUD_MOBILE_BASEADO_WEB.class);

    }
        

        } // classe: fim


        