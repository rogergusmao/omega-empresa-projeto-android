

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaOperacaoDownloadBancoMobile
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaOperacaoDownloadBancoMobile.java
        * TABELA MYSQL:    sistema_operacao_download_banco_mobile
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaOperacaoSistemaMobile;


        public abstract class DAOSistemaOperacaoDownloadBancoMobile extends Table
        {

        public static final String NAME = "sistema_operacao_download_banco_mobile";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_OPERACAO_DOWNLOAD_BANCO_MOBILE{
					id,
 			path_script_sql_banco,
 			observacao,
 			operacao_sistema_mobile_id_INT,
 			popular_tabela_BOOLEAN,
 			drop_tabela_BOOLEAN,
 			destino_banco_id_INT};
		public static final String ID = "id";
		public static final String PATH_SCRIPT_SQL_BANCO = "path_script_sql_banco";
		public static final String OBSERVACAO = "observacao";
		public static final String OPERACAO_SISTEMA_MOBILE_ID_INT = "operacao_sistema_mobile_id_INT";
		public static final String POPULAR_TABELA_BOOLEAN = "popular_tabela_BOOLEAN";
		public static final String DROP_TABELA_BOOLEAN = "drop_tabela_BOOLEAN";
		public static final String DESTINO_BANCO_ID_INT = "destino_banco_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaOperacaoDownloadBancoMobile.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaOperacaoDownloadBancoMobile(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    PATH_SCRIPT_SQL_BANCO, 
                    "path_script_sql_banco",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    OBSERVACAO, 
                    "observacao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    OPERACAO_SISTEMA_MOBILE_ID_INT, 
                    DAOSistemaOperacaoSistemaMobile.NAME,
                    DAOSistemaOperacaoSistemaMobile.ID,
                    "operacao_sistema_mobile_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_operacao_download_banco_mobile_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    POPULAR_TABELA_BOOLEAN, 
                    "popular_tabela_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DROP_TABELA_BOOLEAN, 
                    "drop_tabela_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DESTINO_BANCO_ID_INT, 
                    "destino_banco_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

			addKey("ghjfghj", new String[] { OPERACAO_SISTEMA_MOBILE_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_OPERACAO_DOWNLOAD_BANCO_MOBILE> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_OPERACAO_DOWNLOAD_BANCO_MOBILE>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_OPERACAO_DOWNLOAD_BANCO_MOBILE.class);

    }
        

        } // classe: fim


        