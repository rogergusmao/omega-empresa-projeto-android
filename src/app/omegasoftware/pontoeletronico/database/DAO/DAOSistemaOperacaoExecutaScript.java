

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaOperacaoExecutaScript
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaOperacaoExecutaScript.java
        * TABELA MYSQL:    sistema_operacao_executa_script
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOSistemaOperacaoExecutaScript extends Table
        {

        public static final String NAME = "sistema_operacao_executa_script";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_OPERACAO_EXECUTA_SCRIPT{
					id,
 			xml_script_sql_banco_ARQUIVO,
 			operacao_sistema_mobile_id_INT};
		public static final String ID = "id";
		public static final String XML_SCRIPT_SQL_BANCO_ARQUIVO = "xml_script_sql_banco_ARQUIVO";
		public static final String OPERACAO_SISTEMA_MOBILE_ID_INT = "operacao_sistema_mobile_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaOperacaoExecutaScript.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaOperacaoExecutaScript(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    XML_SCRIPT_SQL_BANCO_ARQUIVO, 
                    "xml_script_sql_banco_ARQUIVO",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    OPERACAO_SISTEMA_MOBILE_ID_INT, 
                    "operacao_sistema_mobile_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

			addKey("operacao_download_banco_mobile_FK_422485351", new String[] { OPERACAO_SISTEMA_MOBILE_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_OPERACAO_EXECUTA_SCRIPT> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_OPERACAO_EXECUTA_SCRIPT>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_OPERACAO_EXECUTA_SCRIPT.class);

    }
        

        } // classe: fim


        