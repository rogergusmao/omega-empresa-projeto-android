

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaOperacaoSistemaMobile
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaOperacaoSistemaMobile.java
        * TABELA MYSQL:    sistema_operacao_sistema_mobile
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOSistemaOperacaoSistemaMobile extends Table
        {

        public static final String NAME = "sistema_operacao_sistema_mobile";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_OPERACAO_SISTEMA_MOBILE{
					id,
 			tipo_operacao_sistema_id_INT,
 			estado_operacao_sistema_mobile_id_INT,
 			mobile_identificador_id_INT,
 			data_abertura_DATETIME,
 			data_processamento_DATETIME,
 			data_conclusao_DATETIME};
		public static final String ID = "id";
		public static final String TIPO_OPERACAO_SISTEMA_ID_INT = "tipo_operacao_sistema_id_INT";
		public static final String ESTADO_OPERACAO_SISTEMA_MOBILE_ID_INT = "estado_operacao_sistema_mobile_id_INT";
		public static final String MOBILE_IDENTIFICADOR_ID_INT = "mobile_identificador_id_INT";
		public static final String DATA_ABERTURA_DATETIME = "data_abertura_DATETIME";
		public static final String DATA_PROCESSAMENTO_DATETIME = "data_processamento_DATETIME";
		public static final String DATA_CONCLUSAO_DATETIME = "data_conclusao_DATETIME";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaOperacaoSistemaMobile.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaOperacaoSistemaMobile(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    TIPO_OPERACAO_SISTEMA_ID_INT, 
                    "tipo_operacao_sistema_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ESTADO_OPERACAO_SISTEMA_MOBILE_ID_INT, 
                    "estado_operacao_sistema_mobile_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    MOBILE_IDENTIFICADOR_ID_INT, 
                    "mobile_identificador_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_ABERTURA_DATETIME, 
                    "data_abertura_DATETIME",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_PROCESSAMENTO_DATETIME, 
                    "data_processamento_DATETIME",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_CONCLUSAO_DATETIME, 
                    "data_conclusao_DATETIME",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_OPERACAO_SISTEMA_MOBILE> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_OPERACAO_SISTEMA_MOBILE>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_OPERACAO_SISTEMA_MOBILE.class);

    }
        

        } // classe: fim


        