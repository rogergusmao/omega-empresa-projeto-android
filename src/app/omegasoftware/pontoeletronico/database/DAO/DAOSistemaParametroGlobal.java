

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaParametroGlobal
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaParametroGlobal.java
        * TABELA MYSQL:    sistema_parametro_global
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOSistemaParametroGlobal extends Table
        {

        public static final String NAME = "sistema_parametro_global";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_PARAMETRO_GLOBAL{
					id,
 			nome,
 			valor_string,
 			valor_INT,
 			valor_DATETIME,
 			valor_DATE,
 			valor_FLOAT,
 			valor_BOOLEAN,
 			valor_SEC,
 			valor_OFFSEC};
		public static final String ID = "id";
		public static final String NOME = "nome";
		public static final String VALOR_STRING = "valor_string";
		public static final String VALOR_INT = "valor_INT";
		public static final String VALOR_DATETIME = "valor_DATETIME";
		public static final String VALOR_DATE = "valor_DATE";
		public static final String VALOR_FLOAT = "valor_FLOAT";
		public static final String VALOR_BOOLEAN = "valor_BOOLEAN";
		public static final String VALOR_SEC = "valor_SEC";
		public static final String VALOR_OFFSEC = "valor_OFFSEC";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaParametroGlobal.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaParametroGlobal(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    250,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_STRING, 
                    "valor_string",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    250,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_INT, 
                    "valor_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_DATETIME, 
                    "valor_DATETIME",
                    true,
                    SQLLITE_TYPE.DATETIME,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_DATE, 
                    "valor_DATE",
                    true,
                    SQLLITE_TYPE.DATE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_FLOAT, 
                    "valor_FLOAT",
                    true,
                    SQLLITE_TYPE.DOUBLE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_BOOLEAN, 
                    "valor_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_SEC, 
                    "valor_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VALOR_OFFSEC, 
                    "valor_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_PARAMETRO_GLOBAL> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_PARAMETRO_GLOBAL>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_PARAMETRO_GLOBAL.class);

    }
        

        } // classe: fim


        