

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaRegistroHistorico
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaRegistroHistorico.java
        * TABELA MYSQL:    sistema_registro_historico
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaRegistroSincronizadorAndroidParaWeb;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaTabela;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOSistemaRegistroHistorico extends Table
        {

        public static final String NAME = "sistema_registro_historico";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_REGISTRO_HISTORICO{
					id,
 			crud,
 			sistema_registro_sincronizador_id_INT,
 			sistema_tabela_id_INT,
 			id_tabela_INT,
 			id_tabela_mobile_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String CRUD = "crud";
		public static final String SISTEMA_REGISTRO_SINCRONIZADOR_ID_INT = "sistema_registro_sincronizador_id_INT";
		public static final String SISTEMA_TABELA_ID_INT = "sistema_tabela_id_INT";
		public static final String ID_TABELA_INT = "id_tabela_INT";
		public static final String ID_TABELA_MOBILE_INT = "id_tabela_mobile_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaRegistroHistorico.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaRegistroHistorico(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    CRUD, 
                    "crud",
                    true,
                    SQLLITE_TYPE.TEXT,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SISTEMA_REGISTRO_SINCRONIZADOR_ID_INT, 
                    DAOSistemaRegistroSincronizadorAndroidParaWeb.NAME,
                    DAOSistemaRegistroSincronizadorAndroidParaWeb.ID,
                    "sistema_registro_sincronizador_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_registro_historico_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    SISTEMA_TABELA_ID_INT, 
                    DAOSistemaTabela.NAME,
                    DAOSistemaTabela.ID,
                    "sistema_tabela_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_registro_historico_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    ID_TABELA_INT, 
                    "id_tabela_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ID_TABELA_MOBILE_INT, 
                    "id_tabela_mobile_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_registro_historico_ibfk_2"
                )
            );

			addKey("sistema_registro_sincronizador_id_INT", new String[] { SISTEMA_REGISTRO_SINCRONIZADOR_ID_INT });
			addKey("corporacao_id_INT", new String[] { CORPORACAO_ID_INT });
			addKey("sistema_tabela_id_INT", new String[] { SISTEMA_TABELA_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_REGISTRO_HISTORICO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_REGISTRO_HISTORICO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_REGISTRO_HISTORICO.class);

    }
        

        } // classe: fim


        