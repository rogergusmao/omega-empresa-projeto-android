

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaRegistroSincronizadorWebParaAndroid
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaRegistroSincronizadorWebParaAndroid.java
        * TABELA MYSQL:    sistema_registro_sincronizador_web_para_android
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaTabela;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaTipoOperacaoBanco;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOSistemaRegistroSincronizadorWebParaAndroid extends Table
        {

        public static final String NAME = "sistema_registro_sincronizador_web_para_android";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_REGISTRO_SINCRONIZADOR_WEB_PARA_ANDROID{
					id,
 			sistema_tabela_id_INT,
 			id_tabela_INT,
 			sistema_tipo_operacao_id_INT,
 			corporacao_id_INT,
 			imei,
 			sistema_produto_INT,
 			sistema_projetos_versao_INT};
		public static final String ID = "id";
		public static final String SISTEMA_TABELA_ID_INT = "sistema_tabela_id_INT";
		public static final String ID_TABELA_INT = "id_tabela_INT";
		public static final String SISTEMA_TIPO_OPERACAO_ID_INT = "sistema_tipo_operacao_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String IMEI = "imei";
		public static final String SISTEMA_PRODUTO_INT = "sistema_produto_INT";
		public static final String SISTEMA_PROJETOS_VERSAO_INT = "sistema_projetos_versao_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaRegistroSincronizadorWebParaAndroid.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaRegistroSincronizadorWebParaAndroid(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    SISTEMA_TABELA_ID_INT, 
                    DAOSistemaTabela.NAME,
                    DAOSistemaTabela.ID,
                    "sistema_tabela_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_registro_sincronizador_web_para_android_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    ID_TABELA_INT, 
                    "id_tabela_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SISTEMA_TIPO_OPERACAO_ID_INT, 
                    DAOSistemaTipoOperacaoBanco.NAME,
                    DAOSistemaTipoOperacaoBanco.ID,
                    "sistema_tipo_operacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_registro_sincronizador_web_para_android_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_registro_sincronizador_web_para_android_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    IMEI, 
                    "imei",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SISTEMA_PRODUTO_INT, 
                    "sistema_produto_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SISTEMA_PROJETOS_VERSAO_INT, 
                    "sistema_projetos_versao_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

			addKey("key_b5d062ceb0542de6", new String[] { SISTEMA_TABELA_ID_INT });
			addKey("key_27e4fcf168a77d56", new String[] { SISTEMA_TIPO_OPERACAO_ID_INT });
			addKey("key_a7cac0eecb5c738f", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_REGISTRO_SINCRONIZADOR_WEB_PARA_ANDROID> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_REGISTRO_SINCRONIZADOR_WEB_PARA_ANDROID>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_REGISTRO_SINCRONIZADOR_WEB_PARA_ANDROID.class);

    }
        

        } // classe: fim


        