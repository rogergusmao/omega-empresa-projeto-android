

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaSequencia
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaSequencia.java
        * TABELA MYSQL:    sistema_sequencia
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOSistemaSequencia extends Table
        {

        public static final String NAME = "sistema_sequencia";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_SEQUENCIA{
					id,
 			tabela,
 			id_ultimo_INT};
		public static final String ID = "id";
		public static final String TABELA = "tabela";
		public static final String ID_ULTIMO_INT = "id_ultimo_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaSequencia.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaSequencia(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    TABELA, 
                    "tabela",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    128,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ID_ULTIMO_INT, 
                    "id_ultimo_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_SEQUENCIA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_SEQUENCIA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_SEQUENCIA.class);

    }
        

        } // classe: fim


        