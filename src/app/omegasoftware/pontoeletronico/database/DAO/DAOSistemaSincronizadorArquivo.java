

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaSincronizadorArquivo
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaSincronizadorArquivo.java
        * TABELA MYSQL:    sistema_sincronizador_arquivo
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOSistemaSincronizadorArquivo extends Table
        {

        public static final String NAME = "sistema_sincronizador_arquivo";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_SINCRONIZADOR_ARQUIVO{
					id,
 			arquivo,
 			is_zip_BOOLEAN,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String ARQUIVO = "arquivo";
		public static final String IS_ZIP_BOOLEAN = "is_zip_BOOLEAN";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaSincronizadorArquivo.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaSincronizadorArquivo(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    ARQUIVO, 
                    "arquivo",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    50,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    IS_ZIP_BOOLEAN, 
                    "is_zip_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_sincronizador_arquivo_ibfk_1"
                )
            );

			addKey("key_8875c4777b1d3c9f", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_SINCRONIZADOR_ARQUIVO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_SINCRONIZADOR_ARQUIVO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_SINCRONIZADOR_ARQUIVO.class);

    }
        

        } // classe: fim


        