

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaTabela
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaTabela.java
        * TABELA MYSQL:    sistema_tabela
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOSistemaTabela extends Table
        {

        public static final String NAME = "sistema_tabela";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_TABELA{
					id,
 			nome,
 			data_modificacao_estrutura_DATETIME,
 			frequencia_sincronizador_INT,
 			banco_mobile_BOOLEAN,
 			banco_web_BOOLEAN,
 			transmissao_web_para_mobile_BOOLEAN,
 			transmissao_mobile_para_web_BOOLEAN,
 			tabela_sistema_BOOLEAN,
 			excluida_BOOLEAN,
 			atributos_chave_unica,
 			nome_exibicao};
		public static final String ID = "id";
		public static final String NOME = "nome";
		public static final String DATA_MODIFICACAO_ESTRUTURA_DATETIME = "data_modificacao_estrutura_DATETIME";
		public static final String FREQUENCIA_SINCRONIZADOR_INT = "frequencia_sincronizador_INT";
		public static final String BANCO_MOBILE_BOOLEAN = "banco_mobile_BOOLEAN";
		public static final String BANCO_WEB_BOOLEAN = "banco_web_BOOLEAN";
		public static final String TRANSMISSAO_WEB_PARA_MOBILE_BOOLEAN = "transmissao_web_para_mobile_BOOLEAN";
		public static final String TRANSMISSAO_MOBILE_PARA_WEB_BOOLEAN = "transmissao_mobile_para_web_BOOLEAN";
		public static final String TABELA_SISTEMA_BOOLEAN = "tabela_sistema_BOOLEAN";
		public static final String EXCLUIDA_BOOLEAN = "excluida_BOOLEAN";
		public static final String ATRIBUTOS_CHAVE_UNICA = "atributos_chave_unica";
		public static final String NOME_EXIBICAO = "nome_exibicao";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaTabela.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaTabela(Database database){
            super(NAME, database);
            setUniqueKey( "nome", new String[]{ NOME});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    128,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_MODIFICACAO_ESTRUTURA_DATETIME, 
                    "data_modificacao_estrutura_DATETIME",
                    true,
                    SQLLITE_TYPE.DATE,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    FREQUENCIA_SINCRONIZADOR_INT, 
                    "frequencia_sincronizador_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    2,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    BANCO_MOBILE_BOOLEAN, 
                    "banco_mobile_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    BANCO_WEB_BOOLEAN, 
                    "banco_web_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TRANSMISSAO_WEB_PARA_MOBILE_BOOLEAN, 
                    "transmissao_web_para_mobile_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TRANSMISSAO_MOBILE_PARA_WEB_BOOLEAN, 
                    "transmissao_mobile_para_web_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TABELA_SISTEMA_BOOLEAN, 
                    "tabela_sistema_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    EXCLUIDA_BOOLEAN, 
                    "excluida_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ATRIBUTOS_CHAVE_UNICA, 
                    "atributos_chave_unica",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    512,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME_EXIBICAO, 
                    "nome_exibicao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    128,
                    false
                )
            );

        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_TABELA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_TABELA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_TABELA.class);

    }
        

        } // classe: fim


        