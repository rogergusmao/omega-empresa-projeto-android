

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOSistemaUsuarioMensagem
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOSistemaUsuarioMensagem.java
        * TABELA MYSQL:    sistema_usuario_mensagem
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaTipoUsuarioMensagem;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOSistemaUsuarioMensagem extends Table
        {

        public static final String NAME = "sistema_usuario_mensagem";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_SISTEMA_USUARIO_MENSAGEM{
					id,
 			tag,
 			mensagem,
 			sistema_tipo_usuario_mensagem_id_INT,
 			usuario_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String TAG = "tag";
		public static final String MENSAGEM = "mensagem";
		public static final String SISTEMA_TIPO_USUARIO_MENSAGEM_ID_INT = "sistema_tipo_usuario_mensagem_id_INT";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOSistemaUsuarioMensagem.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOSistemaUsuarioMensagem(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    TAG, 
                    "tag",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    MENSAGEM, 
                    "mensagem",
                    true,
                    SQLLITE_TYPE.TINYTEXT,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SISTEMA_TIPO_USUARIO_MENSAGEM_ID_INT, 
                    DAOSistemaTipoUsuarioMensagem.NAME,
                    DAOSistemaTipoUsuarioMensagem.ID,
                    "sistema_tipo_usuario_mensagem_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_usuario_mensagem_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_usuario_mensagem_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "sistema_usuario_mensagem_ibfk_1"
                )
            );

			addKey("key_d18df5d66c46e28e", new String[] { SISTEMA_TIPO_USUARIO_MENSAGEM_ID_INT });
			addKey("key_f65825b393b9371f", new String[] { USUARIO_ID_INT });
			addKey("key_3285575d473e6dcf", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_SISTEMA_USUARIO_MENSAGEM> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_SISTEMA_USUARIO_MENSAGEM>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_SISTEMA_USUARIO_MENSAGEM.class);

    }
        

        } // classe: fim


        