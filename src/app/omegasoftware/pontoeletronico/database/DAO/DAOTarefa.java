

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOTarefa
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOTarefa.java
        * TABELA MYSQL:    tarefa
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCategoriaPermissao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOVeiculo;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCidade;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCidade;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaEquipe;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaAtividade;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoTarefa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaVenda;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstado;
		import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstadoCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOAtividade;


        public abstract class DAOTarefa extends Table
        {

        public static final String NAME = "tarefa";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_TAREFA{
					id,
 			criado_pelo_usuario_id_INT,
 			categoria_permissao_id_INT,
 			veiculo_id_INT,
 			usuario_id_INT,
 			origem_pessoa_id_INT,
 			origem_empresa_id_INT,
 			origem_logradouro,
 			origem_numero,
 			origem_cidade_id_INT,
 			origem_latitude_INT,
 			origem_longitude_INT,
 			origem_latitude_real_INT,
 			origem_longitude_real_INT,
 			destino_pessoa_id_INT,
 			destino_empresa_id_INT,
 			destino_logradouro,
 			destino_numero,
 			destino_cidade_id_INT,
 			destino_latitude_INT,
 			destino_longitude_INT,
 			destino_latitude_real_INT,
 			destino_longitude_real_INT,
 			tempo_estimado_carro_INT,
 			tempo_estimado_a_pe_INT,
 			distancia_estimada_carro_INT,
 			distancia_estimada_a_pe_INT,
 			titulo,
 			titulo_normalizado,
 			descricao,
 			corporacao_id_INT,
 			empresa_equipe_id_INT,
 			inicio_hora_programada_SEC,
 			inicio_hora_programada_OFFSEC,
 			data_exibir_SEC,
 			data_exibir_OFFSEC,
 			inicio_SEC,
 			inicio_OFFSEC,
 			fim_SEC,
 			fim_OFFSEC,
 			cadastro_SEC,
 			cadastro_OFFSEC,
 			empresa_atividade_id_INT,
 			tipo_tarefa_id_INT,
 			empresa_venda_id_INT,
 			id_prioridade_INT,
 			registro_estado_id_INT,
 			registro_estado_corporacao_id_INT,
 			percentual_completo_INT,
 			prazo_SEC,
 			prazo_OFFSEC,
 			atividade_id_INT,
 			protocolo_empresa_venda_INT,
 			protocolo_empresa_atividade_venda_INT};
		public static final String ID = "id";
		public static final String CRIADO_PELO_USUARIO_ID_INT = "criado_pelo_usuario_id_INT";
		public static final String CATEGORIA_PERMISSAO_ID_INT = "categoria_permissao_id_INT";
		public static final String VEICULO_ID_INT = "veiculo_id_INT";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String ORIGEM_PESSOA_ID_INT = "origem_pessoa_id_INT";
		public static final String ORIGEM_EMPRESA_ID_INT = "origem_empresa_id_INT";
		public static final String ORIGEM_LOGRADOURO = "origem_logradouro";
		public static final String ORIGEM_NUMERO = "origem_numero";
		public static final String ORIGEM_CIDADE_ID_INT = "origem_cidade_id_INT";
		public static final String ORIGEM_LATITUDE_INT = "origem_latitude_INT";
		public static final String ORIGEM_LONGITUDE_INT = "origem_longitude_INT";
		public static final String ORIGEM_LATITUDE_REAL_INT = "origem_latitude_real_INT";
		public static final String ORIGEM_LONGITUDE_REAL_INT = "origem_longitude_real_INT";
		public static final String DESTINO_PESSOA_ID_INT = "destino_pessoa_id_INT";
		public static final String DESTINO_EMPRESA_ID_INT = "destino_empresa_id_INT";
		public static final String DESTINO_LOGRADOURO = "destino_logradouro";
		public static final String DESTINO_NUMERO = "destino_numero";
		public static final String DESTINO_CIDADE_ID_INT = "destino_cidade_id_INT";
		public static final String DESTINO_LATITUDE_INT = "destino_latitude_INT";
		public static final String DESTINO_LONGITUDE_INT = "destino_longitude_INT";
		public static final String DESTINO_LATITUDE_REAL_INT = "destino_latitude_real_INT";
		public static final String DESTINO_LONGITUDE_REAL_INT = "destino_longitude_real_INT";
		public static final String TEMPO_ESTIMADO_CARRO_INT = "tempo_estimado_carro_INT";
		public static final String TEMPO_ESTIMADO_A_PE_INT = "tempo_estimado_a_pe_INT";
		public static final String DISTANCIA_ESTIMADA_CARRO_INT = "distancia_estimada_carro_INT";
		public static final String DISTANCIA_ESTIMADA_A_PE_INT = "distancia_estimada_a_pe_INT";
		public static final String TITULO = "titulo";
		public static final String TITULO_NORMALIZADO = "titulo_normalizado";
		public static final String DESCRICAO = "descricao";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String EMPRESA_EQUIPE_ID_INT = "empresa_equipe_id_INT";
		public static final String INICIO_HORA_PROGRAMADA_SEC = "inicio_hora_programada_SEC";
		public static final String INICIO_HORA_PROGRAMADA_OFFSEC = "inicio_hora_programada_OFFSEC";
		public static final String DATA_EXIBIR_SEC = "data_exibir_SEC";
		public static final String DATA_EXIBIR_OFFSEC = "data_exibir_OFFSEC";
		public static final String INICIO_SEC = "inicio_SEC";
		public static final String INICIO_OFFSEC = "inicio_OFFSEC";
		public static final String FIM_SEC = "fim_SEC";
		public static final String FIM_OFFSEC = "fim_OFFSEC";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";
		public static final String EMPRESA_ATIVIDADE_ID_INT = "empresa_atividade_id_INT";
		public static final String TIPO_TAREFA_ID_INT = "tipo_tarefa_id_INT";
		public static final String EMPRESA_VENDA_ID_INT = "empresa_venda_id_INT";
		public static final String ID_PRIORIDADE_INT = "id_prioridade_INT";
		public static final String REGISTRO_ESTADO_ID_INT = "registro_estado_id_INT";
		public static final String REGISTRO_ESTADO_CORPORACAO_ID_INT = "registro_estado_corporacao_id_INT";
		public static final String PERCENTUAL_COMPLETO_INT = "percentual_completo_INT";
		public static final String PRAZO_SEC = "prazo_SEC";
		public static final String PRAZO_OFFSEC = "prazo_OFFSEC";
		public static final String ATIVIDADE_ID_INT = "atividade_id_INT";
		public static final String PROTOCOLO_EMPRESA_VENDA_INT = "protocolo_empresa_venda_INT";
		public static final String PROTOCOLO_EMPRESA_ATIVIDADE_VENDA_INT = "protocolo_empresa_atividade_venda_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOTarefa.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOTarefa(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    CRIADO_PELO_USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "criado_pelo_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_ibfk_4"
                )
            );

            super.addAttribute(
                new Attribute(
                    CATEGORIA_PERMISSAO_ID_INT, 
                    DAOCategoriaPermissao.NAME,
                    DAOCategoriaPermissao.ID,
                    "categoria_permissao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_ibfk_7"
                )
            );

            super.addAttribute(
                new Attribute(
                    VEICULO_ID_INT, 
                    DAOVeiculo.NAME,
                    DAOVeiculo.ID,
                    "veiculo_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_ibfk_10"
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_ibfk_13"
                )
            );

            super.addAttribute(
                new Attribute(
                    ORIGEM_PESSOA_ID_INT, 
                    DAOPessoa.NAME,
                    DAOPessoa.ID,
                    "origem_pessoa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_ibfk_8"
                )
            );

            super.addAttribute(
                new Attribute(
                    ORIGEM_EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "origem_empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_ibfk_15"
                )
            );

            super.addAttribute(
                new Attribute(
                    ORIGEM_LOGRADOURO, 
                    "origem_logradouro",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ORIGEM_NUMERO, 
                    "origem_numero",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ORIGEM_CIDADE_ID_INT, 
                    DAOCidade.NAME,
                    DAOCidade.ID,
                    "origem_cidade_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_ibfk_14"
                )
            );

            super.addAttribute(
                new Attribute(
                    ORIGEM_LATITUDE_INT, 
                    "origem_latitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ORIGEM_LONGITUDE_INT, 
                    "origem_longitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ORIGEM_LATITUDE_REAL_INT, 
                    "origem_latitude_real_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ORIGEM_LONGITUDE_REAL_INT, 
                    "origem_longitude_real_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DESTINO_PESSOA_ID_INT, 
                    DAOPessoa.NAME,
                    DAOPessoa.ID,
                    "destino_pessoa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    DESTINO_EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "destino_empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_ibfk_12"
                )
            );

            super.addAttribute(
                new Attribute(
                    DESTINO_LOGRADOURO, 
                    "destino_logradouro",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DESTINO_NUMERO, 
                    "destino_numero",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DESTINO_CIDADE_ID_INT, 
                    DAOCidade.NAME,
                    DAOCidade.ID,
                    "destino_cidade_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_ibfk_5"
                )
            );

            super.addAttribute(
                new Attribute(
                    DESTINO_LATITUDE_INT, 
                    "destino_latitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DESTINO_LONGITUDE_INT, 
                    "destino_longitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DESTINO_LATITUDE_REAL_INT, 
                    "destino_latitude_real_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DESTINO_LONGITUDE_REAL_INT, 
                    "destino_longitude_real_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TEMPO_ESTIMADO_CARRO_INT, 
                    "tempo_estimado_carro_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TEMPO_ESTIMADO_A_PE_INT, 
                    "tempo_estimado_a_pe_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DISTANCIA_ESTIMADA_CARRO_INT, 
                    "distancia_estimada_carro_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DISTANCIA_ESTIMADA_A_PE_INT, 
                    "distancia_estimada_a_pe_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TITULO, 
                    "titulo",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(getNewAtributoNormalizado(TITULO));

            super.addAttribute(
                new Attribute(
                    DESCRICAO, 
                    "descricao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    512,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_ibfk_11"
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_EQUIPE_ID_INT, 
                    DAOEmpresaEquipe.NAME,
                    DAOEmpresaEquipe.ID,
                    "empresa_equipe_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_FK_603546143"
                )
            );

            super.addAttribute(
                new Attribute(
                    INICIO_HORA_PROGRAMADA_SEC, 
                    "inicio_hora_programada_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    INICIO_HORA_PROGRAMADA_OFFSEC, 
                    "inicio_hora_programada_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_EXIBIR_SEC, 
                    "data_exibir_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_EXIBIR_OFFSEC, 
                    "data_exibir_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    INICIO_SEC, 
                    "inicio_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    INICIO_OFFSEC, 
                    "inicio_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    FIM_SEC, 
                    "fim_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    FIM_OFFSEC, 
                    "fim_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_ATIVIDADE_ID_INT, 
                    DAOEmpresaAtividade.NAME,
                    DAOEmpresaAtividade.ID,
                    "empresa_atividade_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.SET_NULL,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_FK_189971924"
                )
            );

            super.addAttribute(
                new Attribute(
                    TIPO_TAREFA_ID_INT, 
                    DAOTipoTarefa.NAME,
                    DAOTipoTarefa.ID,
                    "tipo_tarefa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    4,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_FK_751190186"
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_VENDA_ID_INT, 
                    DAOEmpresaVenda.NAME,
                    DAOEmpresaVenda.ID,
                    "empresa_venda_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.SET_NULL,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_FK_443054199"
                )
            );

            super.addAttribute(
                new Attribute(
                    ID_PRIORIDADE_INT, 
                    "id_prioridade_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    4,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    REGISTRO_ESTADO_ID_INT, 
                    DAORegistroEstado.NAME,
                    DAORegistroEstado.ID,
                    "registro_estado_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_FK_972564698"
                )
            );

            super.addAttribute(
                new Attribute(
                    REGISTRO_ESTADO_CORPORACAO_ID_INT, 
                    DAORegistroEstadoCorporacao.NAME,
                    DAORegistroEstadoCorporacao.ID,
                    "registro_estado_corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_FK_558166504"
                )
            );

            super.addAttribute(
                new Attribute(
                    PERCENTUAL_COMPLETO_INT, 
                    "percentual_completo_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    2,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PRAZO_SEC, 
                    "prazo_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PRAZO_OFFSEC, 
                    "prazo_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    ATIVIDADE_ID_INT, 
                    DAOAtividade.NAME,
                    DAOAtividade.ID,
                    "atividade_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.SET_NULL,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_FK_575897217"
                )
            );

            super.addAttribute(
                new Attribute(
                    PROTOCOLO_EMPRESA_VENDA_INT, 
                    "protocolo_empresa_venda_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PROTOCOLO_EMPRESA_ATIVIDADE_VENDA_INT, 
                    "protocolo_empresa_atividade_venda_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    20,
                    false
                )
            );

			addKey("tarefa_FK_490081787", new String[] { ORIGEM_CIDADE_ID_INT });
			addKey("tarefa_FK_618682861", new String[] { DESTINO_CIDADE_ID_INT });
			addKey("tarefa_FK_90545654", new String[] { CORPORACAO_ID_INT });
			addKey("tabela_FK_100", new String[] { USUARIO_ID_INT });
			addKey("tarefa_FK_529296875", new String[] { CRIADO_PELO_USUARIO_ID_INT });
			addKey("tarefa_FK_70098877", new String[] { CATEGORIA_PERMISSAO_ID_INT });
			addKey("tarefa_FK_751892090", new String[] { VEICULO_ID_INT });
			addKey("tarefa_FK_718139649", new String[] { ORIGEM_PESSOA_ID_INT });
			addKey("tarefa_FK_466461182", new String[] { ORIGEM_EMPRESA_ID_INT });
			addKey("tarefa_FK_517883301", new String[] { DESTINO_PESSOA_ID_INT });
			addKey("tarefa_FK_919769288", new String[] { DESTINO_EMPRESA_ID_INT });
			addKey("tarefa_FK_603546143", new String[] { EMPRESA_EQUIPE_ID_INT });
			addKey("tarefa_FK_751190186", new String[] { TIPO_TAREFA_ID_INT });
			addKey("tarefa_FK_972564698", new String[] { REGISTRO_ESTADO_ID_INT });
			addKey("tarefa_FK_558166504", new String[] { REGISTRO_ESTADO_CORPORACAO_ID_INT });
			addKey("tarefa_FK_189971924", new String[] { EMPRESA_ATIVIDADE_ID_INT });
			addKey("tarefa_FK_443054199", new String[] { EMPRESA_VENDA_ID_INT });
			addKey("tarefa_FK_575897217", new String[] { ATIVIDADE_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_TAREFA> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_TAREFA>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_TAREFA.class);

    }
        

        } // classe: fim


        