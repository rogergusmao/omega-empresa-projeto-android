

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOTarefaCotidianoItem
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOTarefaCotidianoItem.java
        * TABELA MYSQL:    tarefa_cotidiano_item
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOTarefaCotidiano;


        public abstract class DAOTarefaCotidianoItem extends Table
        {

        public static final String NAME = "tarefa_cotidiano_item";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_TAREFA_COTIDIANO_ITEM{
					id,
 			descricao,
 			corporacao_id_INT,
 			seq_INT,
 			latitude_INT,
 			longitude_INT,
 			tarefa_cotidiano_id_INT};
		public static final String ID = "id";
		public static final String DESCRICAO = "descricao";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String SEQ_INT = "seq_INT";
		public static final String LATITUDE_INT = "latitude_INT";
		public static final String LONGITUDE_INT = "longitude_INT";
		public static final String TAREFA_COTIDIANO_ID_INT = "tarefa_cotidiano_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOTarefaCotidianoItem.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOTarefaCotidianoItem(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    DESCRICAO, 
                    "descricao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.SET_NULL,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_cotidiano_item_FK_381195068"
                )
            );

            super.addAttribute(
                new Attribute(
                    SEQ_INT, 
                    "seq_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    LATITUDE_INT, 
                    "latitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    LONGITUDE_INT, 
                    "longitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TAREFA_COTIDIANO_ID_INT, 
                    DAOTarefaCotidiano.NAME,
                    DAOTarefaCotidiano.ID,
                    "tarefa_cotidiano_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.SET_NULL,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_cotidiano_item_FK_630126953"
                )
            );

			addKey("tarefa_cotidiano_item_FK_381195068", new String[] { CORPORACAO_ID_INT });
			addKey("tarefa_cotidiano_item_FK_630126953", new String[] { TAREFA_COTIDIANO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_TAREFA_COTIDIANO_ITEM> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_TAREFA_COTIDIANO_ITEM>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_TAREFA_COTIDIANO_ITEM.class);

    }
        

        } // classe: fim


        