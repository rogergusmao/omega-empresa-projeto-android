

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOTarefaItem
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOTarefaItem.java
        * TABELA MYSQL:    tarefa_item
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOTarefa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOTarefaItem extends Table
        {

        public static final String NAME = "tarefa_item";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_TAREFA_ITEM{
					id,
 			descricao,
 			data_conclusao_SEC,
 			data_conclusao_OFFSEC,
 			tarefa_id_INT,
 			cadastro_SEC,
 			cadastro_OFFSEC,
 			criado_pelo_usuario_id_INT,
 			executada_pelo_usuario_id_INT,
 			corporacao_id_INT,
 			seq_INT,
 			latitude_INT,
 			longitude_INT,
 			latitude_real_INT,
 			longitude_real_INT};
		public static final String ID = "id";
		public static final String DESCRICAO = "descricao";
		public static final String DATA_CONCLUSAO_SEC = "data_conclusao_SEC";
		public static final String DATA_CONCLUSAO_OFFSEC = "data_conclusao_OFFSEC";
		public static final String TAREFA_ID_INT = "tarefa_id_INT";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";
		public static final String CRIADO_PELO_USUARIO_ID_INT = "criado_pelo_usuario_id_INT";
		public static final String EXECUTADA_PELO_USUARIO_ID_INT = "executada_pelo_usuario_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String SEQ_INT = "seq_INT";
		public static final String LATITUDE_INT = "latitude_INT";
		public static final String LONGITUDE_INT = "longitude_INT";
		public static final String LATITUDE_REAL_INT = "latitude_real_INT";
		public static final String LONGITUDE_REAL_INT = "longitude_real_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOTarefaItem.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOTarefaItem(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    DESCRICAO, 
                    "descricao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_CONCLUSAO_SEC, 
                    "data_conclusao_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_CONCLUSAO_OFFSEC, 
                    "data_conclusao_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TAREFA_ID_INT, 
                    DAOTarefa.NAME,
                    DAOTarefa.ID,
                    "tarefa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_item_FK_979400635"
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CRIADO_PELO_USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "criado_pelo_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_item_FK_803283692"
                )
            );

            super.addAttribute(
                new Attribute(
                    EXECUTADA_PELO_USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "executada_pelo_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_item_FK_903411866"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "tarefa_item_FK_176757812"
                )
            );

            super.addAttribute(
                new Attribute(
                    SEQ_INT, 
                    "seq_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    LATITUDE_INT, 
                    "latitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    LONGITUDE_INT, 
                    "longitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    LATITUDE_REAL_INT, 
                    "latitude_real_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    LONGITUDE_REAL_INT, 
                    "longitude_real_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

			addKey("tarefa_item_FK_979400635", new String[] { TAREFA_ID_INT });
			addKey("tarefa_item_FK_803283692", new String[] { CRIADO_PELO_USUARIO_ID_INT });
			addKey("tarefa_item_FK_903411866", new String[] { EXECUTADA_PELO_USUARIO_ID_INT });
			addKey("tarefa_item_FK_176757812", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_TAREFA_ITEM> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_TAREFA_ITEM>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_TAREFA_ITEM.class);

    }
        

        } // classe: fim


        