

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOTarefaRelatorio
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOTarefaRelatorio.java
        * TABELA MYSQL:    tarefa_relatorio
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOTarefaRelatorio extends Table
        {

        public static final String NAME = "tarefa_relatorio";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_TAREFA_RELATORIO{
					id,
 			tarefa_id_INT,
 			relatorio_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String TAREFA_ID_INT = "tarefa_id_INT";
		public static final String RELATORIO_ID_INT = "relatorio_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOTarefaRelatorio.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOTarefaRelatorio(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    TAREFA_ID_INT, 
                    "tarefa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    RELATORIO_ID_INT, 
                    "relatorio_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

			addKey("tarefa_relatorio_FK_286987304", new String[] { TAREFA_ID_INT });
			addKey("tarefa_relatorio_FK_220855713", new String[] { RELATORIO_ID_INT });
			addKey("tarefa_relatorio_FK_496887207", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_TAREFA_RELATORIO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_TAREFA_RELATORIO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_TAREFA_RELATORIO.class);

    }
        

        } // classe: fim


        