

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOTarefaTag
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOTarefaTag.java
        * TABELA MYSQL:    tarefa_tag
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOTarefaTag extends Table
        {

        public static final String NAME = "tarefa_tag";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_TAREFA_TAG{
					id,
 			tarefa_id_INT,
 			tag_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String TAREFA_ID_INT = "tarefa_id_INT";
		public static final String TAG_ID_INT = "tag_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOTarefaTag.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOTarefaTag(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    TAREFA_ID_INT, 
                    "tarefa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TAG_ID_INT, 
                    "tag_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

			addKey("tarefa_tag_FK_665161133", new String[] { TAREFA_ID_INT });
			addKey("tarefa_tag_FK_816802979", new String[] { TAG_ID_INT });
			addKey("tarefa_tag_FK_330139160", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_TAREFA_TAG> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_TAREFA_TAG>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_TAREFA_TAG.class);

    }
        

        } // classe: fim


        