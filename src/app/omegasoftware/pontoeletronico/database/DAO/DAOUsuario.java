

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOUsuario
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOUsuario.java
        * TABELA MYSQL:    usuario
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;


        public abstract class DAOUsuario extends Table
        {

        public static final String NAME = "usuario";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_USUARIO{
					id,
 			nome,
 			nome_normalizado,
 			email,
 			senha,
 			status_BOOLEAN,
 			pagina_inicial,
 			cadastro_SEC,
 			cadastro_OFFSEC};
		public static final String ID = "id";
		public static final String NOME = "nome";
		public static final String NOME_NORMALIZADO = "nome_normalizado";
		public static final String EMAIL = "email";
		public static final String SENHA = "senha";
		public static final String STATUS_BOOLEAN = "status_BOOLEAN";
		public static final String PAGINA_INICIAL = "pagina_inicial";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";


		public static String TABELAS_RELACIONADAS[] = {DAOUsuario.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOUsuario(Database database){
            super(NAME, database);
            setUniqueKey( "email", new String[]{ EMAIL});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(getNewAtributoNormalizado(NOME));

            super.addAttribute(
                new Attribute(
                    EMAIL, 
                    "email",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SENHA, 
                    "senha",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    STATUS_BOOLEAN, 
                    "status_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PAGINA_INICIAL, 
                    "pagina_inicial",
                    true,
                    SQLLITE_TYPE.TEXT,
                    true,
                    null,
                    false,
                    null,
                    0,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_USUARIO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_USUARIO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_USUARIO.class);

    }
        

        } // classe: fim


        