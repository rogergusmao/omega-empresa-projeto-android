

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOUsuarioCategoriaPermissao
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOUsuarioCategoriaPermissao.java
        * TABELA MYSQL:    usuario_categoria_permissao
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCategoriaPermissao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOUsuarioCategoriaPermissao extends Table
        {

        public static final String NAME = "usuario_categoria_permissao";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_USUARIO_CATEGORIA_PERMISSAO{
					id,
 			usuario_id_INT,
 			categoria_permissao_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String CATEGORIA_PERMISSAO_ID_INT = "categoria_permissao_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOUsuarioCategoriaPermissao.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOUsuarioCategoriaPermissao(Database database){
            super(NAME, database);
            setUniqueKey( "usuario_id_INT", new String[]{ USUARIO_ID_INT, CORPORACAO_ID_INT});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_categoria_permissao_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    CATEGORIA_PERMISSAO_ID_INT, 
                    DAOCategoriaPermissao.NAME,
                    DAOCategoriaPermissao.ID,
                    "categoria_permissao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_categoria_permissao_ibfk_8"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_categoria_permissao_ibfk_9"
                )
            );

			addKey("usuario_categoria_permissao_FK_734741211", new String[] { CATEGORIA_PERMISSAO_ID_INT });
			addKey("usuario_categoria_permissao_FK_776031494", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_USUARIO_CATEGORIA_PERMISSAO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_USUARIO_CATEGORIA_PERMISSAO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_USUARIO_CATEGORIA_PERMISSAO.class);

    }
        

        } // classe: fim


        