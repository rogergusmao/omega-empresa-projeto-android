

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOUsuarioCorporacao
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOUsuarioCorporacao.java
        * TABELA MYSQL:    usuario_corporacao
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOUsuarioCorporacao extends Table
        {

        public static final String NAME = "usuario_corporacao";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_USUARIO_CORPORACAO{
					id,
 			usuario_id_INT,
 			corporacao_id_INT,
 			status_BOOLEAN,
 			is_adm_BOOLEAN,
 			cadastro_SEC,
 			cadastro_OFFSEC};
		public static final String ID = "id";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String STATUS_BOOLEAN = "status_BOOLEAN";
		public static final String IS_ADM_BOOLEAN = "is_adm_BOOLEAN";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";


		public static String TABELAS_RELACIONADAS[] = {DAOUsuarioCorporacao.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOUsuarioCorporacao(Database database){
            super(NAME, database);
            setUniqueKey( "usuario_corporacao_UNIQUE", new String[]{ USUARIO_ID_INT, CORPORACAO_ID_INT});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_corporacao_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_corporacao_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    STATUS_BOOLEAN, 
                    "status_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    IS_ADM_BOOLEAN, 
                    "is_adm_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

			addKey("usuario_corporacao_FK_972991944", new String[] { USUARIO_ID_INT });
			addKey("usuario_corporacao_FK_135986328", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_USUARIO_CORPORACAO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_USUARIO_CORPORACAO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_USUARIO_CORPORACAO.class);

    }
        

        } // classe: fim


        