

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOUsuarioPosicao
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOUsuarioPosicao.java
        * TABELA MYSQL:    usuario_posicao
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOVeiculoUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOUsuarioPosicao extends Table
        {

        public static final String NAME = "usuario_posicao";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_USUARIO_POSICAO{
					id,
 			veiculo_usuario_id_INT,
 			usuario_id_INT,
 			latitude_INT,
 			longitude_INT,
 			velocidade_INT,
 			foto_interna,
 			foto_externa,
 			corporacao_id_INT,
 			fonte_informacao_INT,
 			cadastro_SEC,
 			cadastro_OFFSEC};
		public static final String ID = "id";
		public static final String VEICULO_USUARIO_ID_INT = "veiculo_usuario_id_INT";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String LATITUDE_INT = "latitude_INT";
		public static final String LONGITUDE_INT = "longitude_INT";
		public static final String VELOCIDADE_INT = "velocidade_INT";
		public static final String FOTO_INTERNA = "foto_interna";
		public static final String FOTO_EXTERNA = "foto_externa";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String FONTE_INFORMACAO_INT = "fonte_informacao_INT";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";


		public static String TABELAS_RELACIONADAS[] = {DAOUsuarioPosicao.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOUsuarioPosicao(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    VEICULO_USUARIO_ID_INT, 
                    DAOVeiculoUsuario.NAME,
                    DAOVeiculoUsuario.ID,
                    "veiculo_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_posicao_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_posicao_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    LATITUDE_INT, 
                    "latitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    LONGITUDE_INT, 
                    "longitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VELOCIDADE_INT, 
                    "velocidade_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    5,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    FOTO_INTERNA, 
                    "foto_interna",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    FOTO_EXTERNA, 
                    "foto_externa",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_posicao_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    FONTE_INFORMACAO_INT, 
                    "fonte_informacao_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    2,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

			addKey("usuario_posicao_FK_244567871", new String[] { CORPORACAO_ID_INT });
			addKey("usuario_posicao_FK_205352783", new String[] { USUARIO_ID_INT });
			addKey("usuario_posicao_FK_64941406", new String[] { VEICULO_USUARIO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_USUARIO_POSICAO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_USUARIO_POSICAO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_USUARIO_POSICAO.class);

    }
        

        } // classe: fim


        