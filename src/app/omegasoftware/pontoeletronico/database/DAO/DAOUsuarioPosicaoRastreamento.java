

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOUsuarioPosicaoRastreamento
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOUsuarioPosicaoRastreamento.java
        * TABELA MYSQL:    usuario_posicao_rastreamento
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOVeiculoUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOUsuarioPosicaoRastreamento extends Table
        {

        public static final String NAME = "usuario_posicao_rastreamento";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_USUARIO_POSICAO_RASTREAMENTO{
					id,
 			data_DATE,
 			hora_TIME,
 			veiculo_usuario_id_INT,
 			usuario_id_INT,
 			latitude_INT,
 			longitude_INT,
 			velocidade_INT,
 			foto_interna,
 			foto_externa,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String DATA_DATE = "data_DATE";
		public static final String HORA_TIME = "hora_TIME";
		public static final String VEICULO_USUARIO_ID_INT = "veiculo_usuario_id_INT";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String LATITUDE_INT = "latitude_INT";
		public static final String LONGITUDE_INT = "longitude_INT";
		public static final String VELOCIDADE_INT = "velocidade_INT";
		public static final String FOTO_INTERNA = "foto_interna";
		public static final String FOTO_EXTERNA = "foto_externa";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOUsuarioPosicaoRastreamento.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOUsuarioPosicaoRastreamento(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_DATE, 
                    "data_DATE",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    HORA_TIME, 
                    "hora_TIME",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VEICULO_USUARIO_ID_INT, 
                    DAOVeiculoUsuario.NAME,
                    DAOVeiculoUsuario.ID,
                    "veiculo_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_posicao_rastreamento_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_posicao_rastreamento_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    LATITUDE_INT, 
                    "latitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    LONGITUDE_INT, 
                    "longitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VELOCIDADE_INT, 
                    "velocidade_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    5,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    FOTO_INTERNA, 
                    "foto_interna",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    FOTO_EXTERNA, 
                    "foto_externa",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_posicao_rastreamento_ibfk_3"
                )
            );

			addKey("key_b55d575ed2dd00e0", new String[] { VEICULO_USUARIO_ID_INT });
			addKey("key_b3a9a8520118b71d", new String[] { USUARIO_ID_INT });
			addKey("key_f783193890e9e5ae", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_USUARIO_POSICAO_RASTREAMENTO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_USUARIO_POSICAO_RASTREAMENTO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_USUARIO_POSICAO_RASTREAMENTO.class);

    }
        

        } // classe: fim


        