

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOUsuarioServico
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOUsuarioServico.java
        * TABELA MYSQL:    usuario_servico
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOServico;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOUsuarioServico extends Table
        {

        public static final String NAME = "usuario_servico";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_USUARIO_SERVICO{
					id,
 			status_BOOLEAN,
 			usuario_id_INT,
 			servico_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String STATUS_BOOLEAN = "status_BOOLEAN";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String SERVICO_ID_INT = "servico_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOUsuarioServico.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOUsuarioServico(Database database){
            super(NAME, database);
            setUniqueKey( "servico_id_INT", new String[]{ SERVICO_ID_INT, USUARIO_ID_INT, CORPORACAO_ID_INT});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    STATUS_BOOLEAN, 
                    "status_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_servico_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    SERVICO_ID_INT, 
                    DAOServico.NAME,
                    DAOServico.ID,
                    "servico_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_servico_FK_777465821"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_servico_ibfk_9"
                )
            );

			addKey("usuario_servico_FK_158416748", new String[] { USUARIO_ID_INT });
			addKey("usuario_servico_FK_909790039", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_USUARIO_SERVICO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_USUARIO_SERVICO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_USUARIO_SERVICO.class);

    }
        

        } // classe: fim


        