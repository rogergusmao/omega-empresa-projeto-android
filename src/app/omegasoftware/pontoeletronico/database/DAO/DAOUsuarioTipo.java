

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOUsuarioTipo
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOUsuarioTipo.java
        * TABELA MYSQL:    usuario_tipo
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOUsuarioTipo extends Table
        {

        public static final String NAME = "usuario_tipo";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_USUARIO_TIPO{
					id,
 			nome,
 			nome_visivel,
 			status_BOOLEAN,
 			pagina_inicial,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String NOME = "nome";
		public static final String NOME_VISIVEL = "nome_visivel";
		public static final String STATUS_BOOLEAN = "status_BOOLEAN";
		public static final String PAGINA_INICIAL = "pagina_inicial";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOUsuarioTipo.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOUsuarioTipo(Database database){
            super(NAME, database);
            setUniqueKey( "usuario_tipo_UNIQUE", new String[]{ NOME, CORPORACAO_ID_INT});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME, 
                    "nome",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    false,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    NOME_VISIVEL, 
                    "nome_visivel",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    STATUS_BOOLEAN, 
                    "status_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    PAGINA_INICIAL, 
                    "pagina_inicial",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    255,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_tipo_ibfk_1"
                )
            );

			addKey("usuario_tipo_FK_915130616", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_USUARIO_TIPO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_USUARIO_TIPO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_USUARIO_TIPO.class);

    }
        

        } // classe: fim


        