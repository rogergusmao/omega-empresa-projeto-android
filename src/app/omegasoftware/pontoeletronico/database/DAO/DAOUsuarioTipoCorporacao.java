

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOUsuarioTipoCorporacao
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOUsuarioTipoCorporacao.java
        * TABELA MYSQL:    usuario_tipo_corporacao
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuarioTipo;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOUsuarioTipoCorporacao extends Table
        {

        public static final String NAME = "usuario_tipo_corporacao";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_USUARIO_TIPO_CORPORACAO{
					id,
 			usuario_id_INT,
 			usuario_tipo_id_INT,
 			corporacao_id_INT};
		public static final String ID = "id";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String USUARIO_TIPO_ID_INT = "usuario_tipo_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOUsuarioTipoCorporacao.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOUsuarioTipoCorporacao(Database database){
            super(NAME, database);
            setUniqueKey( "usuario_tipo_corporacao_UNIQUE", new String[]{ USUARIO_ID_INT, CORPORACAO_ID_INT});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_tipo_corporacao_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_TIPO_ID_INT, 
                    DAOUsuarioTipo.NAME,
                    DAOUsuarioTipo.ID,
                    "usuario_tipo_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_tipo_corporacao_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "usuario_tipo_corporacao_ibfk_3"
                )
            );

			addKey("usuario_tipo_corporacao_FK_644226074", new String[] { USUARIO_ID_INT });
			addKey("usuario_tipo_corporacao_FK_503631592", new String[] { USUARIO_TIPO_ID_INT });
			addKey("usuario_tipo_corporacao_FK_788208008", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_USUARIO_TIPO_CORPORACAO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_USUARIO_TIPO_CORPORACAO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_USUARIO_TIPO_CORPORACAO.class);

    }
        

        } // classe: fim


        