

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOVeiculoRegistro
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOVeiculoRegistro.java
        * TABELA MYSQL:    veiculo_registro
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOVeiculoUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoVeiculoRegistro;


        public abstract class DAOVeiculoRegistro extends Table
        {

        public static final String NAME = "veiculo_registro";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_VEICULO_REGISTRO{
					id,
 			entrada_BOOLEAN,
 			veiculo_usuario_id_INT,
 			latitude_INT,
 			longitude_INT,
 			corporacao_id_INT,
 			quilometragem_INT,
 			observacao,
 			data_SEC,
 			data_OFFSEC,
 			tipo_veiculo_registro_id_INT};
		public static final String ID = "id";
		public static final String ENTRADA_BOOLEAN = "entrada_BOOLEAN";
		public static final String VEICULO_USUARIO_ID_INT = "veiculo_usuario_id_INT";
		public static final String LATITUDE_INT = "latitude_INT";
		public static final String LONGITUDE_INT = "longitude_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String QUILOMETRAGEM_INT = "quilometragem_INT";
		public static final String OBSERVACAO = "observacao";
		public static final String DATA_SEC = "data_SEC";
		public static final String DATA_OFFSEC = "data_OFFSEC";
		public static final String TIPO_VEICULO_REGISTRO_ID_INT = "tipo_veiculo_registro_id_INT";


		public static String TABELAS_RELACIONADAS[] = {DAOVeiculoRegistro.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOVeiculoRegistro(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    ENTRADA_BOOLEAN, 
                    "entrada_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    VEICULO_USUARIO_ID_INT, 
                    DAOVeiculoUsuario.NAME,
                    DAOVeiculoUsuario.ID,
                    "veiculo_usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "veiculo_registro_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    LATITUDE_INT, 
                    "latitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    LONGITUDE_INT, 
                    "longitude_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "veiculo_registro_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    QUILOMETRAGEM_INT, 
                    "quilometragem_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    OBSERVACAO, 
                    "observacao",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    512,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_SEC, 
                    "data_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_OFFSEC, 
                    "data_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    TIPO_VEICULO_REGISTRO_ID_INT, 
                    DAOTipoVeiculoRegistro.NAME,
                    DAOTipoVeiculoRegistro.ID,
                    "tipo_veiculo_registro_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "veiculo_registro_FK_612823486"
                )
            );

			addKey("veiculo_usuario_id_INT", new String[] { VEICULO_USUARIO_ID_INT });
			addKey("corporacao_id_INT", new String[] { CORPORACAO_ID_INT });
			addKey("veiculo_registro_FK_612823486", new String[] { TIPO_VEICULO_REGISTRO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_VEICULO_REGISTRO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_VEICULO_REGISTRO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_VEICULO_REGISTRO.class);

    }
        

        } // classe: fim


        