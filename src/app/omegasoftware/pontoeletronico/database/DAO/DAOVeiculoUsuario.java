

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOVeiculoUsuario
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOVeiculoUsuario.java
        * TABELA MYSQL:    veiculo_usuario
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOVeiculo;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOVeiculoUsuario extends Table
        {

        public static final String NAME = "veiculo_usuario";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_VEICULO_USUARIO{
					id,
 			usuario_id_INT,
 			veiculo_id_INT,
 			is_ativo_BOOLEAN,
 			corporacao_id_INT,
 			cadastro_SEC,
 			cadastro_OFFSEC};
		public static final String ID = "id";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String VEICULO_ID_INT = "veiculo_id_INT";
		public static final String IS_ATIVO_BOOLEAN = "is_ativo_BOOLEAN";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String CADASTRO_SEC = "cadastro_SEC";
		public static final String CADASTRO_OFFSEC = "cadastro_OFFSEC";


		public static String TABELAS_RELACIONADAS[] = {DAOVeiculoUsuario.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOVeiculoUsuario(Database database){
            super(NAME, database);
            setUniqueKey( "usuario_id_INT", new String[]{ USUARIO_ID_INT, VEICULO_ID_INT, CORPORACAO_ID_INT});
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "veiculo_usuario_ibfk_5"
                )
            );

            super.addAttribute(
                new Attribute(
                    VEICULO_ID_INT, 
                    DAOVeiculo.NAME,
                    DAOVeiculo.ID,
                    "veiculo_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "veiculo_usuario_ibfk_4"
                )
            );

            super.addAttribute(
                new Attribute(
                    IS_ATIVO_BOOLEAN, 
                    "is_ativo_BOOLEAN",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    1,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "veiculo_usuario_ibfk_6"
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_SEC, 
                    "cadastro_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CADASTRO_OFFSEC, 
                    "cadastro_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

			addKey("veiculo_usuario_FK_266174316", new String[] { VEICULO_ID_INT });
			addKey("veiculo_usuario_FK_358489990", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_VEICULO_USUARIO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_VEICULO_USUARIO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_VEICULO_USUARIO.class);

    }
        

        } // classe: fim


        