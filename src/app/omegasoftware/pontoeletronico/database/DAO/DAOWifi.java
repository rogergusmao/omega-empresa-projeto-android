

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOWifi
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOWifi.java
        * TABELA MYSQL:    wifi
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoa;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOWifi extends Table
        {

        public static final String NAME = "wifi";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_WIFI{
					id,
 			network_id,
 			rssi,
 			ssid,
 			usuario,
 			senha,
 			ip,
 			link_speed,
 			mac_address,
 			frequency,
 			bssid,
 			empresa_id_INT,
 			pessoa_id_INT,
 			contador_verificacao_INT,
 			corporacao_id_INT,
 			data_SEC,
 			data_OFFSEC};
		public static final String ID = "id";
		public static final String NETWORK_ID = "network_id";
		public static final String RSSI = "rssi";
		public static final String SSID = "ssid";
		public static final String USUARIO = "usuario";
		public static final String SENHA = "senha";
		public static final String IP = "ip";
		public static final String LINK_SPEED = "link_speed";
		public static final String MAC_ADDRESS = "mac_address";
		public static final String FREQUENCY = "frequency";
		public static final String BSSID = "bssid";
		public static final String EMPRESA_ID_INT = "empresa_id_INT";
		public static final String PESSOA_ID_INT = "pessoa_id_INT";
		public static final String CONTADOR_VERIFICACAO_INT = "contador_verificacao_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String DATA_SEC = "data_SEC";
		public static final String DATA_OFFSEC = "data_OFFSEC";


		public static String TABELAS_RELACIONADAS[] = {DAOWifi.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOWifi(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    NETWORK_ID, 
                    "network_id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    RSSI, 
                    "rssi",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SSID, 
                    "ssid",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO, 
                    "usuario",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    100,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    SENHA, 
                    "senha",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    IP, 
                    "ip",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    LINK_SPEED, 
                    "link_speed",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    MAC_ADDRESS, 
                    "mac_address",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    50,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    FREQUENCY, 
                    "frequency",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    BSSID, 
                    "bssid",
                    true,
                    SQLLITE_TYPE.VARCHAR,
                    true,
                    null,
                    false,
                    null,
                    30,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    EMPRESA_ID_INT, 
                    DAOEmpresa.NAME,
                    DAOEmpresa.ID,
                    "empresa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "wifi_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    PESSOA_ID_INT, 
                    DAOPessoa.NAME,
                    DAOPessoa.ID,
                    "pessoa_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "wifi_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    CONTADOR_VERIFICACAO_INT, 
                    "contador_verificacao_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "wifi_ibfk_3"
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_SEC, 
                    "data_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_OFFSEC, 
                    "data_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

			addKey("empresa_id_INT", new String[] { EMPRESA_ID_INT });
			addKey("pessoa_id_INT", new String[] { PESSOA_ID_INT });
			addKey("corporacao_id_INT", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_WIFI> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_WIFI>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_WIFI.class);

    }
        

        } // classe: fim


        