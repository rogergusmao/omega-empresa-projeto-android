

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  DAOWifiRegistro
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         DAOWifiRegistro.java
        * TABELA MYSQL:    wifi_registro
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.DAO;

        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.ProtocoloEntidade;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    

        		import android.content.Context;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOWifi;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
		import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;


        public abstract class DAOWifiRegistro extends Table
        {

        public static final String NAME = "wifi_registro";
        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        		public static enum ATRIBUTOS_WIFI_REGISTRO{
					id,
 			wifi_id_INT,
 			usuario_id_INT,
 			corporacao_id_INT,
 			data_inicio_SEC,
 			data_inicio_OFFSEC,
 			data_fim_SEC,
 			data_fim_OFFSEC};
		public static final String ID = "id";
		public static final String WIFI_ID_INT = "wifi_id_INT";
		public static final String USUARIO_ID_INT = "usuario_id_INT";
		public static final String CORPORACAO_ID_INT = "corporacao_id_INT";
		public static final String DATA_INICIO_SEC = "data_inicio_SEC";
		public static final String DATA_INICIO_OFFSEC = "data_inicio_OFFSEC";
		public static final String DATA_FIM_SEC = "data_fim_SEC";
		public static final String DATA_FIM_OFFSEC = "data_fim_OFFSEC";


		public static String TABELAS_RELACIONADAS[] = {DAOWifiRegistro.NAME};
        // *************************
        // CONSTRUTOR
        // *************************
        public DAOWifiRegistro(Database database){
            super(NAME, database);
            
        
            super.addAttribute(
                new Attribute(
                    ID, 
                    "id",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    false,
                    null,
                    true,
                    null,
                    11,
                    true
                )
            );

            super.addAttribute(
                new Attribute(
                    WIFI_ID_INT, 
                    DAOWifi.NAME,
                    DAOWifi.ID,
                    "wifi_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "wifi_registro_FK_898468018"
                )
            );

            super.addAttribute(
                new Attribute(
                    USUARIO_ID_INT, 
                    DAOUsuario.NAME,
                    DAOUsuario.ID,
                    "usuario_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "wifi_registro_ibfk_1"
                )
            );

            super.addAttribute(
                new Attribute(
                    CORPORACAO_ID_INT, 
                    DAOCorporacao.NAME,
                    DAOCorporacao.ID,
                    "corporacao_id_INT",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    11,
                    TYPE_RELATION_FK.CASCADE,
                    TYPE_RELATION_FK.SET_NULL,
                    "wifi_registro_ibfk_2"
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_INICIO_SEC, 
                    "data_inicio_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_INICIO_OFFSEC, 
                    "data_inicio_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_FIM_SEC, 
                    "data_fim_SEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    10,
                    false
                )
            );

            super.addAttribute(
                new Attribute(
                    DATA_FIM_OFFSEC, 
                    "data_fim_OFFSEC",
                    true,
                    SQLLITE_TYPE.INTEGER,
                    true,
                    null,
                    false,
                    null,
                    6,
                    false
                )
            );

			addKey("wifi_registro_FK_898468018", new String[] { WIFI_ID_INT });
			addKey("wifi_registro_ibfk_1", new String[] { USUARIO_ID_INT });
			addKey("wifi_registro_ibfk_2", new String[] { CORPORACAO_ID_INT });
        }
          
            @Override
            public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

            @Override
            public void onSynchronized(Context pContext, String pNewId, String pOldId) {
                    // TODO Auto-generated method stub

            }

        

    public ProtocoloEntidade<ATRIBUTOS_WIFI_REGISTRO> factoryProtocoloJson(JSONObject jsonObj) throws Exception{
        return new ProtocoloEntidade<ATRIBUTOS_WIFI_REGISTRO>(
                jsonObj, 
                factory(), 
                ATRIBUTOS_WIFI_REGISTRO.class);

    }
        

        } // classe: fim


        