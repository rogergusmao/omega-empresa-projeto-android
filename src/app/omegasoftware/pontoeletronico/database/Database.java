package app.omegasoftware.pontoeletronico.database;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException.CODE;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.database.synchronize.DownloadBanco;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_BOOLEAN;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public abstract class Database {
	private static final String TAG = "Database";

	final long DEFAULT_WAIT_TIME_BEGIN_TRANSACTION = 7;
	final TimeUnit DEFAULT_TIME_UNIT_OF_WAIT_TIME_BEGIN_TRANSACTION = TimeUnit.SECONDS;

	public static enum ERRO_SQL {
		CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI_CASCADE(-24), CHAVE_DUPLICADA(
				1062), CHAVE_INEXISTENTE(-22), NAO_IDENTIFICADO(-1), ERRO_EXECUCAO(
				-2), DUPLICADA(1);

		int id;

		ERRO_SQL(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}

	}

	private Context context;
	public static String NULL = "null";
	private OpenHelper openHelper;

	public void log(String msg){
		if(openHelper != null) openHelper.log(msg);
	}
	public static enum ORIGEM_DATABASE {
		DOWNLOAD, ASSETS, ESTRUTURA_DINAMICA
	};

	// CONTROLADOR DE VERSAO DE BANCO DE DADOS
	public static enum TIPO_COMANDO_BANCO {
		DROP_TABLE(1), DROP_COLUMN(2), DROP_INDEX(3), DROP_FOREIGN_KEY(4), MODIFY_COLUMN(
				5), RENAME_TABLE(6), CREATE_KEY(7), CREATE_TABLE(8), ADD_COLUMN(
				9), CREATE_FOREIGN_KEY(10), DROP_UNIQUE_KEY(11), CREATE_UNIQUE_KEY(
				12), INSERT_INTO_REGISTER(13), DELETE_REGISTER(14), UPDATE_REGISTER(
				15), RENAME_ATTRIBUTE(16), CONSULTA_SIMPLES(-1);
		int id;

		TIPO_COMANDO_BANCO(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}
	};
	static boolean isCheckCreated = false;
	static boolean isCheckInitialized = false;
	static ReentrantLock lockIsCheckInitialized = new ReentrantLock();
	static boolean isDatabaseFromFile = false;
	String databaseName = "";

	int databaseVersion = 0;
	String databasePath = "";


	public void deletaBanco() {
		try {
			context.deleteDatabase(databaseName);


		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}
		isCheckInitialized = false;
		isCheckCreated = false;
		PontoEletronicoSharedPreference.saveValor(context,
				TIPO_BOOLEAN.RESETAR_BANCO_DE_DADOS, false);
		SharedPreferences sp = context.getSharedPreferences(
				OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = sp.edit();
		vEditor.putBoolean(OmegaConfiguration.DATABASE_INITIALIZED, false);
		vEditor.putBoolean(OmegaConfiguration.DATABASE_CREATED, false);
		vEditor.putInt(OmegaConfiguration.VERSION_CODE, -1);

		vEditor.commit();
	}
	public Database(Database dbCopia) throws OmegaDatabaseException{
		try {
			this.context = dbCopia.getContext();
			this.databasePath = dbCopia.getDatabasePath();
			this.databaseName = dbCopia.getName();
			this.databaseVersion = dbCopia.getDatabaseVersion();

			this.openHelper = dbCopia.getOpenHelper();
			this.transaction = dbCopia.getTransaction();

		} catch (Exception ex) {

			SingletonLog.insereErro(ex, TIPO.BANCO);
			throw new OmegaDatabaseException(CODE.CONSTRUCTOR_DATABASE, "Construtor dbCopia", ex);
		}
	}

	public Transaction getTransaction(){
		return transaction;
	}

	public Database(Context context, String p_databaseName,
					int p_databaseVersion, String p_databasePath) throws OmegaDatabaseException {
		try {
			this.context = context;
			this.databasePath = p_databasePath;
			this.databaseName = p_databaseName;
			this.databaseVersion = p_databaseVersion;

			openHelper = OpenHelper.getInstance(p_databaseName,
					p_databaseVersion, p_databasePath, context);

		} catch (Exception ex) {

			SingletonLog.insereErro(ex, TIPO.BANCO);

			throw new OmegaDatabaseException(
					CODE.CONSTRUCTOR_DATABASE,
					"[sfgserg]Erro durante no construtor Database. Segue os parametros"
							+ ". Banco: " + p_databaseName
							+ ". Version: " + p_databaseVersion
							+ ". Path: " + p_databasePath,
					ex);
		}
	}


	public Database(Context context, String databaseName, int databaseVersion,
					String databasePath, boolean pForceOpenHelper) throws OmegaDatabaseException {
		try {
			this.context = context;
			this.databasePath = databasePath;
			this.databaseName = databaseName;
			this.databaseVersion = databaseVersion;

			openHelper = OpenHelper.getInstance(
					databaseName,
					databaseVersion,
					databasePath,
					context,
					pForceOpenHelper);
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.BANCO);
			throw new OmegaDatabaseException(
					CODE.CONSTRUCTOR_DATABASE,
					"Erro durante no construtor Database. Segue os parametros"
							+ ". Banco: " + databaseName
							+ ". Version: " + databaseVersion
							+ ". Path: " + databasePath
							+ ". Force: " + pForceOpenHelper,
					ex);
		}
	}

	// ===============================================================================
	// GET'S
	public int getDatabaseVersion() {
		return databaseVersion;
	}

	public String getDatabasePath() {
		return databasePath;
	}

	public String getName() {
		return this.databaseName;
	}

	public String getShortName() {
		String name = getName();
		if (name != null && name.length() > 0) {
			int index = name.indexOf(".db");
			if (index >= 0) {
				return name.substring(0, index);
			}
		}
		return name;
	}

	public boolean beginTransactionSyncDefaultTime() throws InterruptedException{
		return beginTransaction(DEFAULT_WAIT_TIME_BEGIN_TRANSACTION, DEFAULT_TIME_UNIT_OF_WAIT_TIME_BEGIN_TRANSACTION);
	}

	public boolean beginTransaction(long time, TimeUnit tu) throws InterruptedException {

		if (openHelper != null) {
			transaction = openHelper.beginTransaction(time, tu);
			if(transaction == null)
				return false;

			openHelper.setPragmaOn(transaction);
			return true;
		}
		return false;
	}

	public boolean beginTransaction() {

		if (openHelper != null) {
			transaction = openHelper.beginTransaction();
			if(transaction == null)
				return false;

			openHelper.setPragmaOn(transaction);
			return true;
		}
		return false;
	}

	public void endTransaction(boolean successfull) {

		if (openHelper != null) {
			openHelper.endTransaction(transaction, successfull);
			transaction = null;
		}
	}

	public boolean isTransactionStarted(){
		return transaction != null;
	}

	public Context getContext() {
		return context;
	}

	public OpenHelper getOpenHelper() {
		return openHelper;
	}

	// ===============================================================================
	// CRIADOR DE BANCO DE DADOS

	public boolean isDatabaseCreated() {
		SharedPreferences vSavedPreferences = context.getSharedPreferences(
				OmegaConfiguration.PREFERENCES_NAME, 0);
		int oldVersionCode = vSavedPreferences.getInt(
				OmegaConfiguration.VERSION_CODE, -1);
		if (oldVersionCode < 0)
			return false;
		else
			return true;
	}

	public void criarTodasAsChaves() {
		Table[] tables = getVetorTable();
		for (int i = 0; i < tables.length; i++) {
			Table t = tables[i];
			ArrayList<String> consultasChave = t.getVetorQueryCreateIndex();
			if (consultasChave != null && consultasChave.size() > 0) {
				for (int j = 0; j < consultasChave.size(); j++) {
					try {

						queryUpdateStructure(TIPO_COMANDO_BANCO.CREATE_KEY,
								consultasChave.get(j));
					} catch (Exception ex) {

						if (OmegaConfiguration.DEBUGGING) {
							SingletonLog.insereWarning(ex, TIPO.SINCRONIZADOR);
						}
					}
				}
			}
			String queryUniqueKey = t.getQueryUniqueKey();
			if (queryUniqueKey != null && queryUniqueKey.length() > 0) {
				try {

					queryUpdateStructure(TIPO_COMANDO_BANCO.CREATE_UNIQUE_KEY,
							queryUniqueKey);
				} catch (Exception ex) {

					if (OmegaConfiguration.DEBUGGING) {
						SingletonLog.insereWarning(ex, TIPO.SINCRONIZADOR);
					}
				}
			}

			// if(t.containerUniqueKey != null
			// && t.containerUniqueKey.getVetorAttribute() != null
			// && t.containerUniqueKey.getVetorAttribute().length > 0){
			//
			// String uk = "";
			//
			// HelperString.getRandomName("UK_");
			// try {
			// String atributos =
			// HelperString.getStrSeparateByDelimiterColumn(t.containerUniqueKey.getVetorAttribute(),
			// ",");
			// queryUpdateStructure(
			// TIPO_COMANDO_BANCO.CREATE_UNIQUE_KEY,
			// "CREATE UNIQUE INDEX " +uk+ " ON " + t.getName() + "(" +
			// atributos + ")");
			// } catch (Exception ex) {

			// if(OmegaConfiguration.DEBUGGING){
			// SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			// }
			// }
			// }
		}
	}

	public static boolean procedimentoCriaBanco(Context context,
												String databaseName, String databasePath, int databaseVersion)
			throws Exception {
		if (!isCheckInitialized) {
			if(lockIsCheckInitialized.tryLock()){
				try{
					if (!isCheckInitialized) {
						boolean databaseInitialized = false;
						int versionCode = -2;
						try {
							versionCode = context.getPackageManager().getPackageInfo(
									context.getPackageName(), 0).versionCode;
						} catch (NameNotFoundException e) {

							SingletonLog.insereErro(e, TIPO.BANCO);
						}

						SharedPreferences vSavedPreferences = context.getSharedPreferences(
								OmegaConfiguration.PREFERENCES_NAME, 0);
						int oldVersionCode = vSavedPreferences.getInt(
								OmegaConfiguration.VERSION_CODE, -1);
						databaseInitialized = vSavedPreferences.getBoolean(
								OmegaConfiguration.DATABASE_INITIALIZED, false);
						if (!databaseInitialized || versionCode != oldVersionCode) {
							DownloadBanco downloadBanco = new DownloadBanco(context);
							InterfaceMensagem msg = downloadBanco
									.receiveFileAndCreateDatabase(databaseName,
											databasePath, databaseVersion);
							if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
									.getId()) {
								databaseInitialized = true;
								SharedPreferences.Editor vEditor = vSavedPreferences.edit();
								vEditor.putBoolean(OmegaConfiguration.DATABASE_INITIALIZED,
										true);
								if (versionCode >= 1) {
									vEditor.putInt(OmegaConfiguration.VERSION_CODE,
											versionCode);
								}

								vEditor.commit();
								isCheckInitialized = true;
								return true;
							}

						} else {
							isCheckInitialized = true;
						}
						return databaseInitialized;
					}
				}finally{
					lockIsCheckInitialized.unlock();
				}
			}
		}
		return false;

	}

	public static void createStructureIfNecessary(Context c) throws OmegaDatabaseException{
		if(!isCheckCreated ){


			if( !isDatabaseCreated(c)){
				Database db = null;
				try{
					db = new DatabasePontoEletronico(c);

					db.createDatabaseStructure();
					db.checkSharedPreferenceDatabaseCreated(true);
				}finally{
					if(db != null) db.close();
				}
			}
			isCheckCreated = true;
		}
	}

	public static boolean isDatabaseInitialized(Context pContext) {
		if (pContext == null)
			return false;

		SharedPreferences vSavedPreferences = pContext.getSharedPreferences(
				OmegaConfiguration.PREFERENCES_NAME, 0);
		if (!vSavedPreferences.contains(OmegaConfiguration.DATABASE_INITIALIZED))
			return false;
		else {
			return vSavedPreferences.getBoolean(
					OmegaConfiguration.DATABASE_INITIALIZED, false);
		}
	}


	public static boolean isDatabaseCreated(Context pContext) {
		if (pContext == null)
			return false;

		SharedPreferences vSavedPreferences = pContext.getSharedPreferences(
				OmegaConfiguration.PREFERENCES_NAME, 0);
		if (!vSavedPreferences.contains(OmegaConfiguration.DATABASE_CREATED))
			return false;
		else {
			return vSavedPreferences.getBoolean(
					OmegaConfiguration.DATABASE_CREATED, false);
		}
	}

	public void checkSharedPreferenceDatabaseInitialized() {

		SharedPreferences vSavedPreferences = context.getSharedPreferences(
				OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putBoolean(OmegaConfiguration.DATABASE_INITIALIZED, true);
		vEditor.commit();
		Database.isCheckInitialized = true;
	}

	public void checkSharedPreferenceDatabaseCreated(boolean created) {

		SharedPreferences vSavedPreferences = context.getSharedPreferences(
				OmegaConfiguration.PREFERENCES_NAME, 0);
		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putBoolean(OmegaConfiguration.DATABASE_CREATED, created);
		vEditor.commit();
		Database.isCheckCreated = created;
	}


	public String verificaEstruturaTabelaAndroidEWeb(Table pTabela) {

		String vStrListaAtributo = HelperString
				.getStrSeparateByDelimiterColumn(
						pTabela.getListNameAttribute(), ",");
		String vInformacao =null;
//		String vInformacao = HelperHttpPost
//				.getConteudoStringPost(
//						context,
//						OmegaConfiguration.URL_REQUEST_VERIFICA_ESTRUTURA_TABELA_ANDROID(),
//						new String[] { "tabela", "lista_atributo" },
//						new String[] { pTabela.getName(), vStrListaAtributo });
		return vInformacao;
	}

	public String verificaEstruturaBancoAndroidEWeb() {
		EXTDAOSistemaTabela vObj = new EXTDAOSistemaTabela(this);
		vObj.setAttrValue(EXTDAOSistemaTabela.BANCO_MOBILE_BOOLEAN, "1");
		vObj.setAttrValue(EXTDAOSistemaTabela.BANCO_WEB_BOOLEAN, "1");
		ArrayList<Table> vListaTabela = vObj.getListTable();
		ArrayList<String> vListaNomeTabela = new ArrayList<String>();
		if (vListaTabela != null)
			for (Table table : vListaTabela) {
				vListaNomeTabela.add(table
						.getStrValueOfAttribute(EXTDAOSistemaTabela.NOME));
			}

		String vStrListaTabela = HelperString.getStrSeparateByDelimiterColumn(
				vListaNomeTabela, ",");
		String vInformacao = null;
//		String vInformacao = HelperHttpPost
//				.getConteudoStringPost(
//						context,
//						OmegaConfiguration.URL_REQUEST_VERIFICA_ESTRUTURA_BANCO_ANDROID(),
//						new String[] { "lista_tabela" },
//						new String[] { vStrListaTabela });
		return vInformacao;
	}

	public void verificaEstruturaWeb() throws Exception {
		while (true) {
			String est = verificaEstruturaBancoAndroidEWeb();
			Log.v(TAG, est);
			String vEstruturaTabela = "";

			EXTDAOSistemaTabela vObj = new EXTDAOSistemaTabela(this);
			vObj.setAttrValue(EXTDAOSistemaTabela.BANCO_MOBILE_BOOLEAN, "1");
			vObj.setAttrValue(EXTDAOSistemaTabela.BANCO_WEB_BOOLEAN, "1");
			ArrayList<Table> vListTabela = vObj.getListTable();
			if (vListTabela != null)
				for (Table vTuplaSistemaTabela : vListTabela) {
					Table vTable = getTable(vTuplaSistemaTabela
							.getStrValueOfAttribute(EXTDAOSistemaTabela.NOME));
					if (vTable != null)
						vEstruturaTabela += verificaEstruturaTabelaAndroidEWeb(vTable);
				}
			Log.v(TAG, vEstruturaTabela);
		}

	}

	public boolean queryUpdateStructure(TIPO_COMANDO_BANCO tipo, String query)
			throws Exception {
		switch (tipo) {
			case CONSULTA_SIMPLES:
			case DROP_TABLE:
			case CREATE_KEY:
			case CREATE_UNIQUE_KEY:
			case RENAME_TABLE:
			case ADD_COLUMN:
			case CREATE_TABLE:

				openHelper.execSQL(transaction, query);
				return true;

			case INSERT_INTO_REGISTER:
				// INSERT INTO ".$pObjTabelaHomologacao->getNome()." ($selectHom)
				// SELECT $selectProd FROM ".$pObjTabelaProducao->getNome();

				// A consulta executada de insercao nao possui parametros,
				// copia todos os dados de uma tabela para outra

				openHelper.rawQuery(transaction,query, new String[] {});
				break;
			default:
				throw new Exception("Operauuo nuo mapeada: " + tipo.toString());

		}
		return true;
	}

	public static TIPO_COMANDO_BANCO getTipoComandoBanco(int id) {
		TIPO_COMANDO_BANCO vetor[] = TIPO_COMANDO_BANCO.values();
		for (int i = 0; i < vetor.length; i++) {
			if (vetor[i].getId() == id)
				return vetor[i];
		}
		SingletonLog.insereErro(new Exception(
				"Tipo de comando nao identificado: " + id), TIPO.BANCO);
		return null;
	}

	// ===============================================================================
	// ESTRUTURA BANCO DE DADOS
	public ArrayList<String> getListNameHierarquicalOrderOfVetorTable(
			String[] pVetorTabela) {
		if (pVetorTabela == null || pVetorTabela.length == 0)
			return new ArrayList<String>();
		ArrayList<String> vList = new ArrayList<String>();
		Table vVetor[] = getVetorTable();
		for (Table vTable : vVetor) {
			for (String pTableName : pVetorTabela) {
				if (vTable.isEqual(pTableName))
					vList.add(pTableName);
			}
		}
		return vList;
	}

	public String[] getVetorNameHierarquicalOrderOfVetorTable(
			String[] pVetorTabela) {
		String[] vetor = null;
		ArrayList<String> lista = getListNameHierarquicalOrderOfVetorTable(pVetorTabela);
		vetor = new String[lista.size()];
		lista.toArray(vetor);
		return vetor;
	}

	public ArrayList<Table> getListTableHierarquicalOrderOfVetorTable(
			Table[] pVetorTabela) {
		ArrayList<Table> vList = new ArrayList<Table>();
		Table vVetor[] = getVetorTable();
		for (Table vTable : vVetor) {
			for (Table pTable : pVetorTabela) {
				if (vTable.isEqual(pTable.getName()))
					vList.add(pTable.factory());
			}
		}
		return vList;
	}

	public ArrayList<Table> getListTableDependent(String pStrName) {
		ArrayList<Table> vList = new ArrayList<Table>();
		Table vVetor[] = getVetorTable();
		for (Table vTable : vVetor) {
			if (vTable.isTableDependent(pStrName))
				vList.add(vTable.factory());
		}
		return vList;
	}

	public static String[] getListOfTableSearch(String pListTableAll[],
												String[] pListTableSearch) {
		ArrayList<String> vList = new ArrayList<String>();
		for (String vTableSearch : pListTableSearch) {
			for (String vTable : pListTableAll) {

				if (vTable.compareTo(vTableSearch) == 0)
					vList.add(vTable);
			}
		}
		String vVetor[] = new String[vList.size()];
		vList.toArray(vVetor);
		return vVetor;
	}

	public boolean hasTable(String p_nomeTabela) {

		Table vVetor[] = getVetorTable();
		for (Table table : vVetor) {
			if (table.isEqual(p_nomeTabela)) {
				return true;
			}
		}
		return false;
	}

	public abstract Table[] getVetorTable();

	public void populate() {
		Table vVetor[] = getVetorTable();
		for (Table table : vVetor) {
			table.populate();
		}
	}

	public String[] ordenaTabelas(String[] tabelas) {
		String[] ordenadas = new String[tabelas.length];
		String[] tabelasDoBanco = this.getVetorNomeTabela();
		int iOrdenadas = 0;
		for (int i = 0; i < tabelasDoBanco.length; i++) {
			int j = 0;
			for (; j < tabelas.length; j++) {

				if (tabelasDoBanco[i].equalsIgnoreCase(tabelas[j])) {
					ordenadas[iOrdenadas++] = tabelas[j];
					break;
				}
			}
			if (iOrdenadas == tabelas.length)
				break;
		}
		return ordenadas;
	}

	public Table getTable(String pTableName) throws Exception {
		Table vVetor[] = getVetorTable();
		for (Table table : vVetor) {
			if (table.getName().compareTo(pTableName) == 0)
				return table;

		}
		throw new Exception("Tentou construir tabela nuo inexistente: "
				+ pTableName);

	}

	public boolean createTableStructure(String pTableName) throws Exception {
		Table vTable = getTable(pTableName);
		if (vTable != null)
			return createTableStructure(vTable);
		else {
			Log.e(TAG, "Tabela inexistente: " + pTableName);
			return false;
		}
	}

	public int updateSqliteSequence(String tableName, long value) throws Exception {
		// UPDATE "main"."sqlite_sequence" SET seq = 84 WHERE name = 'bairro'
		//String q = "UPDATE sqlite_sequence SET seq = ? WHERE name = ? AND seq < ?";
		Cursor c = null;
		try {
			String strValue = String.valueOf(value);
			//openHelper.execSQL("UPDATE sqlite_sequence SET seq = '"+strValue+"' WHERE name = '"+tableName+"' AND seq < '"+strValue+"'");
			ContentValues cv = new ContentValues();
			cv.put("seq", value);
			int total = openHelper.update(transaction,"sqlite_sequence", cv, "name = ? AND seq < ? " , new String[]{tableName, strValue});
			return total;
		} finally {
			if (c != null && !c.isClosed())
				c.close();
		}

	}

	public Long getSequence(String tableName) {
		String q = "SELECT seq FROM sqlite_sequence WHERE name = ? ";
		Cursor cursor = null;

		try {
			cursor = openHelper.rawQuery(transaction,q, new String[] { tableName });
			if (cursor.moveToFirst()) {

				long result = cursor.getLong(0);
				return result;

			}

		} finally {
			if (cursor != null )
				cursor.close();
		}
		return null;

	}

	public void renomearTabela(String antigo, String novo) {
		String q = "ALTER TABLE \"" + antigo + "\" RENAME TO \"" + novo + "\"";
		openHelper.execSQL(transaction,q);
	}

	public void deletarTabela(String nome) {
		String q = "DROP TABLE \"" + nome + "\"";
		openHelper.execSQL(transaction,q);
	}

	public void recriarTabela(String name) throws Exception {
		Table t = factoryTable(name);
		recriarTabela(t);
	}

	public void removerChavesDaTabela(Table t) throws Exception {
		ArrayList<String> querysRemocoes = t.getVetorQueryRemoveIndex();
		if (querysRemocoes != null && querysRemocoes.size() > 0) {
			for (String qRemove : querysRemocoes) {
				try {
					openHelper.execSQL(transaction,qRemove);
				} catch (Exception ex) {
					SingletonLog.insereErro(ex, TIPO.BANCO);
					String msg = ex.getMessage();
					if (!msg.startsWith("no such index:")) {
						throw ex;
					}
				}

			}
		}
	}

	public void recriarTabela(Table t) throws Exception {

		// [2015-11-09 17:49:33.170] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// ALTER TABLE "main"."bairro" RENAME TO "_bairro_old_20151109"
		// 128 carcteres limite tabanho nome tabela
		String tabelaTemp = HelperString.getRandomName(t.getName(), 50);

		Table old = t.factory();
		old.setName(tabelaTemp);

		String createTable = t.getStrQueryCreate();
		String strSelectInsert = t.getStrQuerySelectInsertCopia(old);
		ArrayList<String> queryCreateIndex = t.getVetorQueryCreateIndex();
		Long seq = getSequence(t.getName());
		openHelper.setPragmaOff(transaction);
		removerChavesDaTabela(t);

		renomearTabela(t.getName(), old.getName());
		openHelper.execSQL(transaction,createTable);
		openHelper.execSQL(transaction,strSelectInsert);
		openHelper.execSQLs(transaction,queryCreateIndex);
		if (seq != null)
			updateSqliteSequence(t.getName(), seq);
		// deletarTabela(old.getName());

		// [2015-11-09 17:49:33.185] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// DROP INDEX "main"."bairro_bairr590027_KEY"
		//
		// [2015-11-09 17:49:33.185] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// DROP INDEX "main"."bairro_bairr909211_KEY"
		//
		// [2015-11-09 17:49:33.185] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// DROP INDEX "main"."bairro_unique_bair345917"
		//
		// [2015-11-09 17:49:33.185] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// DROP INDEX "main"."key_59b68adb809292f5"
		//
		// [2015-11-09 17:49:33.185] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// DROP INDEX "main"."key_9398ae67abeb9df5"
		//
		// [2015-11-09 17:49:33.185] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// DROP INDEX "main"."unique_bair203186"

		// [2015-11-09 17:49:33.201] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// CREATE TABLE "main"."bairro" (
		// "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
		// "nome" varchar(100) NOT NULL,
		// "nome_normalizado" varchar(100) NOT NULL,
		// "cidade_id_INT" int(11) NOT NULL,
		// "corporacao_id_INT" int(11) NOT NULL,
		// CONSTRAINT "bairr590027_KEY_FK" FOREIGN KEY ("corporacao_id_INT")
		// REFERENCES "corporacao" ("id") ON DELETE CASCADE ON UPDATE CASCADE
		// )
		//
		//
		// [2015-11-09 17:49:33.201] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// INSERT INTO "main"."bairro" ("id", "nome", "nome_normalizado",
		// "cidade_id_INT", "corporacao_id_INT") SELECT "id", "nome",
		// "nome_normalizado", "cidade_id_INT", "corporacao_id_INT" FROM
		// "_bairro_old_20151109"

		// [2015-11-09 17:49:33.201] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// CREATE INDEX "main"."bairro_bairr590027_KEY"
		// ON "bairro" ("corporacao_id_INT" ASC)
		//
		// [2015-11-09 17:49:33.201] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// CREATE INDEX "main"."bairro_bairr909211_KEY"
		// ON "bairro" ("cidade_id_INT" ASC)
		//
		// [2015-11-09 17:49:33.201] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// CREATE INDEX "main"."bairro_unique_bair345917"
		// ON "bairro" ("nome_normalizado" ASC, "cidade_id_INT" ASC,
		// "corporacao_id_INT" ASC)
		//
		// [2015-11-09 17:49:33.201] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// CREATE INDEX "main"."key_59b68adb809292f5"
		// ON "bairro" ("cidade_id_INT" ASC)
		//
		// [2015-11-09 17:49:33.201] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// CREATE INDEX "main"."key_9398ae67abeb9df5"
		// ON "bairro" ("corporacao_id_INT" ASC)
		//
		// [2015-11-09 17:49:33.201] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// CREATE UNIQUE INDEX "main"."unique_bair203186"
		// ON "bairro" ("nome_normalizado" ASC, "cidade_id_INT" ASC,
		// "corporacao_id_INT" ASC)
		//
		// [2015-11-09 17:49:33.201] [000000] [contatoempresanuvemf7b47]
		// [SQLITE]
		// UPDATE "main"."sqlite_sequence" SET seq = 84 WHERE name = 'bairro'

	}

	public boolean createTableStructure(Table vTable) {

		if (vTable != null) {
			try {
				if (isTableCreated(vTable.getName())) {

					return true;
				}

				else {
					String q = vTable.getStrQueryCreate();
					openHelper.execSQL(transaction,q);
					Log.i(TAG, q);
				}

			} catch (Exception ex) {
				Log.e(TAG, HelperExcecao.getDescricao(ex));
				SingletonLog.insereErro(ex, SingletonLog.TIPO.BANCO);
				return false;
			}

			return true;
		}
		return false;
	}

	public Table factoryTable(String p_nomeTabela) {
		Table vVetor[] = getVetorTable();
		for (Table table : vVetor) {
			if (table.isEqual(p_nomeTabela)) {
				Table newTable = table.factory();
				newTable.setDatabase(this);
				return newTable;
			}
		}
		return null;
	}

	public boolean createDatabaseStructure() {
		boolean validade = true;

		Table vVetor[] = getVetorTable();
		for (Table table : vVetor) {
			boolean vValidadeLocal = createTableStructure(table);
			if (!validade)
				validade = vValidadeLocal;
		}
		return validade;
	}

	public void dropDatabase() {
		Table vVetor[] = getVetorTable();
		for (Table table : vVetor) {
			try {
				dropTableOfDatabase(table.getName());
			} catch (Exception e) {

				SingletonLog.insereErro(e, TIPO.BANCO);
			}
		}
	}

	public String[] getVetorNomeTabela() {
		Table vVetor[] = getVetorTable();
		String[] nomeTabela = new String[vVetor.length];
		int i = 0;
		for (Table table : vVetor) {
			String strName = table.getName();
			nomeTabela[i] = strName;
			i += 1;
		}
		return nomeTabela;

	}
	public Boolean exists(String pQuery, String[] pVetorArg) {
		Cursor cursor = null;
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg);

			if (cursor.moveToFirst()) {

				return true;
			}
			return false;

		} finally{
			if(cursor != null ){
				cursor.close();
			}
		}
	}


	public Long getFirstLong(String pQuery, String[] pVetorArg) {
		Cursor cursor = null;
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg);

			if (cursor.moveToFirst()) {

				return cursor.isNull(0) ? null : cursor.getLong(0);

			}
			return null;

		}finally{
			if(cursor != null ){
				cursor.close();
			}
		}
	}

	public Tuple<Long, Long> getTwoLongs(String pQuery, String[] pVetorArg) {
		Cursor cursor = null;
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg);

			if (cursor.moveToFirst()) {

				Long id0=cursor.isNull(0) ? null : cursor.getLong(0);
				Long id1=cursor.isNull(1) ? null : cursor.getLong(1);

				return new Tuple<Long, Long>(id0, id1);
			}
			return null;

		}finally{
			if(cursor != null ){
				cursor.close();
			}
		}
	}
	public Long[] getThreeLongs(String pQuery, String[] pVetorArg) {
		Cursor cursor = null;
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg);

			if (cursor.moveToFirst()) {

				Long id0=cursor.isNull(0) ? null : cursor.getLong(0);
				Long id1=cursor.isNull(1) ? null : cursor.getLong(1);
				Long id2=cursor.isNull(2) ? null : cursor.getLong(2);

				return new Long[]{id0, id1,id2};
			}
			return null;

		}finally{
			if(cursor != null ){
				cursor.close();
			}
		}
	}
	public String getFirstString(String pQuery, String[] pVetorArg) {
		Cursor cursor = null;
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg);

			if (cursor.moveToFirst()) {

				return cursor.isNull(0) ? null : cursor.getString(0);

			}
			return null;

		} finally{
			if(cursor != null){
				cursor.close();
			}
		}
	}
	public Tuple<Integer,String> getFirstTupleIntByString(String pQuery, String[] pVetorArg) {
		Cursor cursor = null;
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg);

			if (cursor.moveToFirst()) {

				return new Tuple<Integer, String>(cursor.getInt(0), cursor.isNull(1) ? null : cursor.getString(1));

			}
			return null;

		}finally{
			if(cursor != null ){
				cursor.close();
			}
		}
	}

	public Tuple<String,String> getFirstTupleStringByString(String pQuery, String[] pVetorArg) {
		Cursor cursor = null;
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg);

			if (cursor.moveToFirst()) {

				return new Tuple<String, String>(
						cursor.isNull(0) ? null : cursor.getString(0)
						, cursor.isNull(1) ? null : cursor.getString(1));

			}
			return null;

		}finally{
			if(cursor != null ){
				cursor.close();
			}
		}
	}
	public String[] getVetorString(String pQuery, String[] pVetorArg, int qtd) {
		Cursor cursor = null;
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg);

			if (cursor.moveToFirst()) {

				String [] v = new String[qtd];
				for (int i = 0 ; i < v.length; i++){
					v[i] =cursor.isNull(i) ? null : cursor.getString(i);
				}
				return v;
			}
			return null;

		}finally{
			if(cursor != null ){
				cursor.close();
			}
		}
	}
	//O metodo nunca retorna nulo para chave, logo nuo passe atributos que aceitem nulo
	//como primeiro argumento da consulta
	// SELECT id, nome FROM teste - id nunca u nulo
	public List< Tuple<Integer,String>> getListTupleIntByString(String pQuery, String[] pVetorArg) {
		Cursor cursor = null;
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg);

			List< Tuple<Integer,String>>  list = null;
			if (cursor.moveToFirst()) {
				list = new LinkedList< Tuple<Integer,String>>();
				do {
					list.add(new Tuple<Integer, String>(cursor.getInt(0), cursor.isNull(1) ? null : cursor.getString(1)));
				} while (cursor.moveToNext());
			}
			return list;

		} finally{
			if(cursor != null ){
				cursor.close();
			}
		}
	}
	// ===============================================================================
	// CONSULTAS PADROES
	public ResultSet getResultSet(String pQuery, String[] pVetorArg) {
		Cursor cursor = null;
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg, null);
			String[] vVetorNomeAtributo = cursor.getColumnNames();
			ResultSet rs = new ResultSet(vVetorNomeAtributo);
			if (cursor.moveToFirst()) {

				do {
					String[] vTupla = new String[vVetorNomeAtributo.length];
					for (int i = 0; i < vVetorNomeAtributo.length; i++)
						vTupla[i] = cursor.getString(i);
					rs.insereTupla(vTupla);

				} while (cursor.moveToNext());
			}
			return rs.isVazio() ? null : rs;

		} finally{
			if(cursor != null ){
				cursor.close();
			}
		}
	}

	public Long getResultSetAsLong(String pQuery, int indiceColuna,
								   String[] pVetorArg) {
		Cursor cursor = null;
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg);

			if (cursor.moveToFirst()) {
				do {
					return cursor.getLong(indiceColuna);
				} while (cursor.moveToNext());

			}

			return null;
		} finally{
			if(cursor != null ){
				cursor.close();
			}
		}
	}

	public String getResultSetAsString(String pQuery, int indiceColuna,
									   String[] pVetorArg) {
		Cursor cursor = null;
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg);

			if (cursor.moveToFirst()) {
				do {
					return cursor.getString(indiceColuna);
				} while (cursor.moveToNext());

			}

			return null;

		} finally{
			if (cursor != null ) {
				cursor.close();
			}
		}
	}

	public static void closeLog(){
		OpenHelper.closeLog();
	}

	public ResultSet getResultSetThrowsException(String pQuery,
												 String[] pVetorArg) throws Exception {

		Cursor cursor =null;
		try{
			cursor = openHelper.rawQuery(transaction, pQuery, pVetorArg);
			String[] vVetorNomeAtributo = cursor.getColumnNames();
			ResultSet rs = new ResultSet(vVetorNomeAtributo);
			if (cursor.moveToFirst()) {
				do {
					String[] vTupla = new String[vVetorNomeAtributo.length];
					for (int i = 0; i < vVetorNomeAtributo.length; i++)
						vTupla[i] = cursor.getString(i);
					rs.insereTupla(vTupla);

				} while (cursor.moveToNext());
			}
			return rs.isVazio() ? null : rs;
		}finally{

			if (cursor != null )
				cursor.close();
		}
	}

	public Cursor rawQuery(String consulta, String[] parametros) {
		return openHelper.rawQuery(transaction, consulta, parametros,null) ;
	}
	public Cursor rawQuery(String consulta, String[] parametros, SQLiteDatabase writaDatabase) {
		return openHelper.rawQuery(transaction, consulta, parametros, writaDatabase);
	}

	public String[] getResultSetComoVetorDoUmUnicoObjeto(String pQuery,
														 String[] pVetorArg) {
		return getResultSetComoVetorDoUmUnicoObjeto(pQuery, pVetorArg, 0);
	}

	public String[] getResultSetComoVetorDoUmUnicoObjeto(String pQuery,
														 String[] pVetorArg, int pIndice) {
		Cursor cursor = null;
		ArrayList<String> listResult = new ArrayList<String>();
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg);

			if (cursor.moveToFirst()) {

				do {

					String token = cursor.getString(pIndice);
					listResult.add(token);

				} while (cursor.moveToNext());
			}

		} finally{
			if (cursor != null ) {
				cursor.close();
			}
		}


		String[] vetor = new String[listResult.size()];
		listResult.toArray(vetor);
		return vetor;

	}

	public List<Long> getResultSetAsListLong(
			String pQuery,
			String[] pVetorArg,
			int pIndice) {
		Cursor cursor = null;
		ArrayList<Long> listResult = new ArrayList<Long>();
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg);

			if (cursor.moveToFirst()) {

				do {
					if(cursor.isNull(pIndice)) listResult.add(null);
					else listResult.add(cursor.getLong(pIndice));

				} while (cursor.moveToNext());
			}

		} finally{
			if (cursor != null ) {
				cursor.close();
			}
		}
		return listResult;
	}

	public List<Tuple<Long, String>> getResultSetAsListTupleLongString(
			String pQuery,
			String[] pVetorArg,
			int pIndice) {
		Cursor cursor = null;
		ArrayList<Tuple<Long, String>> listResult = new ArrayList<Tuple<Long, String>>();
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg);

			if (cursor.moveToFirst()) {

				do {
					if(cursor.isNull(pIndice)) listResult.add(null);
					else listResult.add(
							new Tuple<Long, String>(
									cursor.getLong(0),
									cursor.getString(1)) );

				} while (cursor.moveToNext());
			}

		} finally{
			if (cursor != null ) {
				cursor.close();
			}
		}
		return listResult;
	}


	public List<String> getResultSetAsListString(
			String pQuery,
			String[] pVetorArg,
			int pIndice) {
		Cursor cursor = null;
		ArrayList<String> listResult = new ArrayList<String>();
		try {
			cursor = openHelper.rawQuery(transaction,pQuery, pVetorArg);

			if (cursor.moveToFirst()) {

				do {
					if(cursor.isNull(pIndice)) listResult.add(null);
					else listResult.add(cursor.getString(pIndice));

				} while (cursor.moveToNext());
			}

		} finally{
			if (cursor != null ) {
				cursor.close();
			}
		}
		return listResult;
	}


	public String[] getResultSetDoPrimeiroObjeto(String p_strQuery,
												 String[] p_vetorArg, int pNumberOfSelectArg) {
		Cursor cursor = null;
		try {
			ArrayList<String> listResult = new ArrayList<String>();

			cursor = openHelper.rawQuery(transaction,p_strQuery, p_vetorArg);

			if (cursor.moveToFirst()) {

				do {
					for (int i = 0; i < pNumberOfSelectArg; i++) {
						String token = cursor.getString(i);
						listResult.add(token);
					}
					break;

				} while (cursor.moveToNext());
			}

			String[] vetor = new String[listResult.size()];
			listResult.toArray(vetor);
			return vetor;
		} finally{

			if (cursor != null) {
				cursor.close();

			}
		}

	}

	public Integer[] getResultSetDoPrimeiroObjetoAsInteger(String p_strQuery,
														   String[] p_vetorArg, int pNumberOfSelectArg) {
		Cursor cursor = null;
		try {


			cursor = openHelper.rawQuery(transaction,p_strQuery, p_vetorArg);

			if (cursor.moveToFirst()) {
				Integer[] result = new Integer[pNumberOfSelectArg];
				do {
					for (int i = 0; i < result.length; i++) {
						if(cursor.isNull(i))result[i] =  null;
						else result[i] =  cursor.getInt(i);
					}
					return result;
				} while (cursor.moveToNext());
			}
			return null;
		} finally{
			if (cursor != null) {
				cursor.close();

			}
		}

	}

	public String getFirstOrDefault(String p_strQuery,
									String[] p_vetorArg, int pNumberOfSelectArg) {
		Cursor cursor = null;
		try {

			cursor = openHelper.rawQuery(transaction,p_strQuery, p_vetorArg);

			if (cursor.moveToFirst()) {

				do {

					String token = cursor.getString(pNumberOfSelectArg);
					return token;

				} while (cursor.moveToNext());
			}
			return null;
		} finally{
			if (cursor != null ) {
				cursor.close();

			}
		}
	}

	public boolean getResultSetAsContains(
			String p_strQuery,
			String[] p_vetorArg) {
		Cursor cursor = null;
		try {

			cursor = openHelper.rawQuery(transaction,p_strQuery, p_vetorArg);

			if (cursor.moveToFirst()) {
				return true;
			}
			return false;
		} finally{
			if (cursor != null ) {
				cursor.close();

			}
		}
	}

	public boolean isTableCreated(String pTabela) {
		String query = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
		ResultSet rs = getResultSet(query, new String[] { pTabela });
		if (rs == null || rs.getTotalTupla() == 0)
			return false;
		else
			return true;

	}



	public boolean isIdInTable(String id, String pTabela) {
		String query = "SELECT 1 FROM "+pTabela+" WHERE id=?";
		Cursor cursor = null;
		try {
			cursor = openHelper.rawQuery(transaction,query, new String[]{id});

			if (cursor.moveToFirst()) {
				return true;
			}else return false;

		}finally{
			if(cursor != null ){
				cursor.close();
			}
		}
	}

	public void dropTableOfDatabase(String pTableName) throws Exception {

		try {
			openHelper.execSQL(transaction,"DROP TABLE IF EXISTS " + pTableName);
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.BANCO);
		}
	}

	public void setPragmaOn() {
		openHelper.setPragmaOn(transaction);
	}

	public void setPragmaOff() {
		openHelper.setPragmaOff(transaction);
	}

	public void close() {
		close(false);
	}

	public void close(boolean forcar) {
		try{
			if (forcar && openHelper != null){
				OpenHelper.close();
				openHelper = null;
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.BANCO);
		}
	}


	public void execSQLs(ArrayList<String> querys){
		openHelper.execSQLs(transaction, querys);
	}

	public void execSQL(String consulta) {
		openHelper.execSQL(transaction, consulta);
	}

	public int delete(String tabela, String where, String[] parametros)
			throws Exception {
		return openHelper.delete(transaction, tabela, where, parametros);
	}

	public Long insertOrThrow(String table, String nullColumnHack, ContentValues values) {
		return openHelper.insertOrThrow(transaction, table, nullColumnHack, values);
	}



	public Cursor query(
			String table, String[] columns, String selection,
			String[] selectionArgs, String groupBy, String having,
			String orderBy) {
		return openHelper.query(transaction, table, columns, selection, selectionArgs, groupBy, having,
				orderBy, null);
	}

	public Cursor query(String table, String[] columns, String selection,
						String[] selectionArgs, String groupBy, String having,
						String orderBy, String limit) {
		return openHelper.query(transaction,table, columns, selection,
				selectionArgs, groupBy, having,
				orderBy, limit);
	}
	public int update(String tableName, ContentValues cv, String where,
					  String[] parametros) throws Exception {
		return 	openHelper.update(transaction, tableName, cv, where,
				parametros) ;
	}

	public ConflictUpdateId updateId(String tableName, String oldId,
									 String newId, OmegaLog logSincronizacao) {
		return openHelper.updateId( transaction, tableName, oldId,
				newId, logSincronizacao) 	;
	}



	public long getMinId(String tableName){
		return openHelper.getMinId(transaction, tableName);
	}

	public Long getLong(String query, String[] parametros){
		return openHelper.getLong(transaction, query, parametros);
	}

	private Transaction transaction ;

	public void teste(){
//		try{
//			OpenHelper oh = OpenHelper.getInstance(context);
//			SQLiteDatabase db = oh.getReadableDatabase();
//			db.delete("pessoa", "id = ?", new String[]{"0"});
//			db.close();			
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, TIPO.PAGINA);
//		}
//		
////		try{
////			OpenHelper oh = OpenHelper.getInstance(context);
////			SQLiteDatabase db = oh.getWritableDatabase();
////			db.delete("pessoa", "id = ?", new String[]{"0"});
////			db.close();			
////		}catch(Exception ex){
////			SingletonLog.insereErro(ex, TIPO.PAGINA);
////		}
//		
//
//		try{
//			OpenHelper oh = OpenHelper.getInstance(context);
//			SQLiteDatabase db1 = oh.getReadableDatabase();
//			oh.getWritableDatabase();
//			db1.delete("pessoa", "id = ?", new String[]{"0"});
//			db1.close();			
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, TIPO.PAGINA);
//		}
//		
//		try{
//			OpenHelper oh = OpenHelper.getInstance(context);
//			oh.getWritableDatabase();
//			SQLiteDatabase db1 = oh.getReadableDatabase();
//			db1.delete("pessoa", "id = ?", new String[]{"0"});
//			db1.close();			
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, TIPO.PAGINA);
//		}

	}

}
