

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DatabasePontoEletronico
    * DATA DE GERAÇÃO: 19.02.2018
    * ARQUIVO:         DatabasePontoEletronico.java
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import android.content.Context;
    import android.os.Environment;
    
    	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOAndroidMetadata;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOApi;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOApp;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOAreaMenu;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCorporacao;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEstadoCivil;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOOperadora;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPerfil;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPermissao;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProdutoUnidadeMedida;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOServico;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSexo;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaBancoVersao;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaDeletarArquivo;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoCrudMobileBaseadoWeb;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoExecutaScript;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoSistemaMobile;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaParametroGlobal;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaSequencia;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoDownloadArquivo;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoMobile;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoOperacaoBanco;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoUsuarioMensagem;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTag;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefaRelatorio;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefaTag;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoAnexo;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoCanalEnvio;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoPonto;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoRegistro;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoRelatorio;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoTarefa;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoVeiculoRegistro;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOAtividadeTipo;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOAtividadeUnidadeMedida;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCategoriaPermissao;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCorporacaoSincronizadorPhp;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOFormaPagamento;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOModelo;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculo;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPais;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOBairro;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProdutoTipo;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProfissao;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORotina;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaParametroGlobalCorporacao;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaSincronizadorArquivo;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoDocumento;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoEmpresa;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoNota;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioTipo;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPermissaoCategoriaPermissao;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProduto;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOProdutoTipos;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoCrudAleatorio;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaOperacaoDownloadBancoMobile;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaAtributo;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizadorTabela;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaGrafoHierarquiaBanco;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaArestaGrafoHierarquiaBanco;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCrudEstado;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaHistoricoSincronizador;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorWebParaAndroid;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORegistroEstado;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORegistroEstadoCorporacao;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORegistro;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORegistroFluxoEstadoCorporacao;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORelatorio;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOAtividade;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOAtividadeTipos;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaAtividade;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAODespesaCotidiano;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAODespesa;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaCompra;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaAtividadeCompra;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaCompraParcela;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaProdutoCompra;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaEquipe;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaPerfil;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaProduto;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaVendaTemplate;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOMesa;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORecebivelCotidiano;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioHorarioTrabalho;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOApiId;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaVenda;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaAtividadeVenda;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaProdutoVenda;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresaVendaParcela;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAONegociacaoDivida;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAONegociacaoDividaEmpresaVenda;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefaItem;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOMensagem;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOMensagemEnvio;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOMesaReserva;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAONota;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAONotaTipoNota;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresa;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEmpresaRotina;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaEquipe;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaUsuario;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPonto;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORecebivel;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefaCotidiano;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefaCotidianoItem;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOWifi;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOWifiRegistro;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORede;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORedeEmpresa;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORelatorioAnexo;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorAndroidParaWeb;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroHistorico;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaUsuarioMensagem;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioCategoriaPermissao;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioCorporacao;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioFoto;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioFotoConfiguracao;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioServico;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioTipoCorporacao;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculoUsuario;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioPosicao;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioPosicaoRastreamento;
	import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOVeiculoRegistro;

    
    import app.omegasoftware.pontoeletronico.database.Database;
	import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
	import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************
    public class DatabasePontoEletronico extends Database {
	

    
        
    

    
    // *************************
    // DECLARAÇÃO DE TODAS AS TABELAS DO BANCO DE DADOS
    // *************************
    private static Table __vectorTable[] = new Table[] { 
		new EXTDAOAndroidMetadata(null), 
		new EXTDAOApi(null), 
		new EXTDAOApp(null), 
		new EXTDAOAreaMenu(null), 
		new EXTDAOCorporacao(null),

		new EXTDAOEstadoCivil(null), 
		new EXTDAOOperadora(null), 
		new EXTDAOPerfil(null), 
		new EXTDAOPermissao(null), 
		new EXTDAOProdutoUnidadeMedida(null), 
		new EXTDAOServico(null), 
		new EXTDAOSexo(null), 
		new EXTDAOSistemaBancoVersao(null), 
		new EXTDAOSistemaDeletarArquivo(null), 
		new EXTDAOSistemaOperacaoCrudMobileBaseadoWeb(null), 
		new EXTDAOSistemaOperacaoExecutaScript(null), 
		new EXTDAOSistemaOperacaoSistemaMobile(null), 
		new EXTDAOSistemaParametroGlobal(null), 
		new EXTDAOSistemaSequencia(null), 
		new EXTDAOSistemaTabela(null), 
		new EXTDAOSistemaTipoDownloadArquivo(null), 
		new EXTDAOSistemaTipoMobile(null), 
		new EXTDAOSistemaTipoOperacaoBanco(null), 
		new EXTDAOSistemaTipoUsuarioMensagem(null), 
		new EXTDAOTag(null), 
		new EXTDAOTarefaRelatorio(null), 
		new EXTDAOTarefaTag(null), 
		new EXTDAOTipoAnexo(null), 
		new EXTDAOTipoCanalEnvio(null), 
		new EXTDAOTipoPonto(null), 
		new EXTDAOTipoRegistro(null),
			new EXTDAORegistroEstado(null),
		new EXTDAOTipoRelatorio(null), 
		new EXTDAOTipoTarefa(null), 
		new EXTDAOTipoVeiculoRegistro(null), 
		new EXTDAOUsuario(null), 
		new EXTDAOAtividadeTipo(null), 
		new EXTDAOAtividadeUnidadeMedida(null), 
		new EXTDAOCategoriaPermissao(null), 
		new EXTDAOCorporacaoSincronizadorPhp(null), 
		new EXTDAOFormaPagamento(null), 
		new EXTDAOModelo(null), 
		new EXTDAOVeiculo(null), 
		new EXTDAOPais(null), 
		new EXTDAOUf(null), 
		new EXTDAOCidade(null), 
		new EXTDAOBairro(null), 
		new EXTDAOProdutoTipo(null), 
		new EXTDAOProfissao(null), 
		new EXTDAORotina(null), 
		new EXTDAOSistemaCorporacaoSincronizador(null), 
		new EXTDAOSistemaParametroGlobalCorporacao(null), 
		new EXTDAOSistemaSincronizadorArquivo(null), 
		new EXTDAOTipoDocumento(null), 
		new EXTDAOTipoEmpresa(null), 
		new EXTDAOTipoNota(null), 
		new EXTDAOUsuarioTipo(null), 
		new EXTDAOPermissaoCategoriaPermissao(null), 
		new EXTDAOProduto(null), 
		new EXTDAOProdutoTipos(null), 
		new EXTDAOSistemaOperacaoCrudAleatorio(null), 
		new EXTDAOSistemaOperacaoDownloadBancoMobile(null), 
		new EXTDAOSistemaAtributo(null), 
		new EXTDAOSistemaCorporacaoSincronizadorTabela(null), 
		new EXTDAOSistemaGrafoHierarquiaBanco(null), 
		new EXTDAOSistemaArestaGrafoHierarquiaBanco(null), 
		new EXTDAOSistemaCrudEstado(null), 
		new EXTDAOSistemaHistoricoSincronizador(null), 
		new EXTDAOSistemaRegistroSincronizadorWebParaAndroid(null), 

		new EXTDAORegistroEstadoCorporacao(null), 
		new EXTDAORegistro(null), 
		new EXTDAORegistroFluxoEstadoCorporacao(null), 
		new EXTDAORelatorio(null), 
		new EXTDAOEmpresa(null), 
		new EXTDAOAtividade(null), 
		new EXTDAOAtividadeTipos(null), 
		new EXTDAOEmpresaAtividade(null), 
		new EXTDAODespesaCotidiano(null), 
		new EXTDAODespesa(null), 
		new EXTDAOEmpresaCompra(null), 
		new EXTDAOEmpresaAtividadeCompra(null), 
		new EXTDAOEmpresaCompraParcela(null), 
		new EXTDAOEmpresaProdutoCompra(null), 
		new EXTDAOEmpresaEquipe(null), 
		new EXTDAOEmpresaPerfil(null), 
		new EXTDAOEmpresaProduto(null), 
		new EXTDAOEmpresaVendaTemplate(null), 
		new EXTDAOMesa(null), 
		new EXTDAORecebivelCotidiano(null), 
		new EXTDAOUsuarioHorarioTrabalho(null), 
		new EXTDAOPessoa(null), 
		new EXTDAOApiId(null), 
		new EXTDAOEmpresaVenda(null), 
		new EXTDAOEmpresaAtividadeVenda(null), 
		new EXTDAOEmpresaProdutoVenda(null), 
		new EXTDAOEmpresaVendaParcela(null), 
		new EXTDAONegociacaoDivida(null), 
		new EXTDAONegociacaoDividaEmpresaVenda(null), 
		new EXTDAOTarefa(null), 
		new EXTDAOTarefaItem(null), 
		new EXTDAOMensagem(null), 
		new EXTDAOMensagemEnvio(null), 
		new EXTDAOMesaReserva(null), 
		new EXTDAONota(null), 
		new EXTDAONotaTipoNota(null), 
		new EXTDAOPessoaEmpresa(null), 
		new EXTDAOPessoaEmpresaRotina(null), 
		new EXTDAOPessoaEquipe(null), 
		new EXTDAOPessoaUsuario(null), 
		new EXTDAOPonto(null), 
		new EXTDAORecebivel(null), 
		new EXTDAOTarefaCotidiano(null), 
		new EXTDAOTarefaCotidianoItem(null), 
		new EXTDAOWifi(null), 
		new EXTDAOWifiRegistro(null), 
		new EXTDAORede(null), 
		new EXTDAORedeEmpresa(null), 
		new EXTDAORelatorioAnexo(null), 
		new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(null), 
		new EXTDAOSistemaRegistroHistorico(null), 
		new EXTDAOSistemaUsuarioMensagem(null), 
		new EXTDAOUsuarioCategoriaPermissao(null), 
		new EXTDAOUsuarioCorporacao(null), 
		new EXTDAOUsuarioFoto(null), 
		new EXTDAOUsuarioFotoConfiguracao(null), 
		new EXTDAOUsuarioServico(null), 
		new EXTDAOUsuarioTipoCorporacao(null), 
		new EXTDAOVeiculoUsuario(null), 
		new EXTDAOUsuarioPosicao(null), 
		new EXTDAOUsuarioPosicaoRastreamento(null), 
		new EXTDAOVeiculoRegistro(null)};
    // *************************
    // CONSTRUTOR
    // *************************
    public static final String DATABASE_PATH = Environment.getDataDirectory()+ "/contatoempresanuvem.db";
    public static final String DATABASE_NAME =  "contatoempresanuvem.db";
    public static final int DATABASE_VERSION = 1;    

    public DatabasePontoEletronico(Context context) throws OmegaDatabaseException {
            super(
                    context,
                    DATABASE_NAME, 
                    DATABASE_VERSION, 
                    DATABASE_PATH);
            //populate();


    }

    public DatabasePontoEletronico(Context context, boolean pOpen) throws OmegaDatabaseException {
            super(
                    context,
                    DATABASE_NAME, 
                    DATABASE_VERSION, 
                    DATABASE_PATH,
                    pOpen);
            //populate();


    }
    
    // *************************
    // FACTORY
    // *************************
    public Database factory() throws OmegaDatabaseException {
        return new DatabasePontoEletronico(this.getContext());

    }
    
        @Override
	public Table[] getVetorTable() {
		// TODO Auto-generated method stub
		return __vectorTable;
	}

    } // classe: fim
    

    