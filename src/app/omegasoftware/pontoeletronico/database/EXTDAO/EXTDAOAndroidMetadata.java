

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOAndroidMetadata
    * DATA DE GERAuuO: 15.06.2013
    * ARQUIVO:         EXTDAOAndroidMetadata.java
    * TABELA MYSQL:    android_metadata
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOAndroidMetadata;

	

    
        
    public class EXTDAOAndroidMetadata extends DAOAndroidMetadata
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOAndroidMetadata(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOAndroidMetadata(this.getDatabase());

    }
    

    } // classe: fim
    

    