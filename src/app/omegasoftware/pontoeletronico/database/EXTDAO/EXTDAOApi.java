

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOApi
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOApi.java
        * TABELA MYSQL:    api
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOApi;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOApi extends DAOApi
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOApi(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOApi(this.getDatabase());

        }
        

        } // classe: fim


        