

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOApp
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOApp.java
    * TABELA MYSQL:    app
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOApp;

	

    
        
    public class EXTDAOApp extends DAOApp
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOApp(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOApp(this.getDatabase());

    }
    

    } // classe: fim
    

    