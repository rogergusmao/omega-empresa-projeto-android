

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOAtividadeTipo
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOAtividadeTipo.java
        * TABELA MYSQL:    atividade_tipo
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOAtividadeTipo;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOAtividadeTipo extends DAOAtividadeTipo
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOAtividadeTipo(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOAtividadeTipo(this.getDatabase());

        }
        

        } // classe: fim


        