

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOAtividadeTipos
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOAtividadeTipos.java
        * TABELA MYSQL:    atividade_tipos
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOAtividadeTipos;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOAtividadeTipos extends DAOAtividadeTipos
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOAtividadeTipos(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOAtividadeTipos(this.getDatabase());

        }
        

        } // classe: fim


        