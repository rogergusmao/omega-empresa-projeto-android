

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOAtividadeUnidadeMedida
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOAtividadeUnidadeMedida.java
        * TABELA MYSQL:    atividade_unidade_medida
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOAtividadeUnidadeMedida;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOAtividadeUnidadeMedida extends DAOAtividadeUnidadeMedida
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOAtividadeUnidadeMedida(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOAtividadeUnidadeMedida(this.getDatabase());

        }
        

        } // classe: fim


        