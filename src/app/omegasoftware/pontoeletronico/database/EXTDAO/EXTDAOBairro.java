

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOBairro
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOBairro.java
    * TABELA MYSQL:    bairro
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.LinkedHashMap;

import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOBairro;

	

    
        
    public class EXTDAOBairro extends DAOBairro
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOBairro(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOBairro(this.getDatabase());

    }
    

	public LinkedHashMap<String, String> getAllBairros()
	{
		
		this.clearData();
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		
		for(Table iEntry : this.getListTable())
		{
			EXTDAOBairro cidade = (EXTDAOBairro) iEntry;
			hashTable.put(cidade.getAttribute(EXTDAOBairro.ID).getStrValue(), 
					cidade.getAttribute(EXTDAOBairro.NOME).getStrValue());
		}
		
		return hashTable;
	}
	
	public LinkedHashMap<String, String> getBairrosFromCidade(String p_cidadeId)
	{
		this.clearData();
		this.setAttrValue(EXTDAOBairro.CIDADE_ID_INT, p_cidadeId);
		
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		for(Table iEntry : this.getListTable(new String[]{EXTDAOBairro.NOME}, false))
		{
			EXTDAOBairro cidade = (EXTDAOBairro) iEntry;
			String idBairro = cidade.getAttribute(EXTDAOBairro.ID).getStrValue();
			String dscBairro = cidade.getAttribute(EXTDAOBairro.NOME).getStrValue();
			if(dscBairro != null){
				hashTable.put(idBairro, dscBairro.toUpperCase());
			}
			
		}
		
		return hashTable;
		
	}
    } // classe: fim
    

    