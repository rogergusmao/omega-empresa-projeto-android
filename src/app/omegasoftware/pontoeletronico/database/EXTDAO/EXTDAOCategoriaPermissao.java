

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOCategoriaPermissao
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOCategoriaPermissao.java
    * TABELA MYSQL:    categoria_permissao
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.LinkedHashMap;

import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOCategoriaPermissao;

	

    
        
    public class EXTDAOCategoriaPermissao extends DAOCategoriaPermissao
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOCategoriaPermissao(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOCategoriaPermissao(this.getDatabase());

    }
    

	public static LinkedHashMap<String, String> getAllCategoriaPermissao(Database db)
	{
		EXTDAOCategoriaPermissao vObj = new EXTDAOCategoriaPermissao(db);
		return vObj.getHashMapIdByDefinition(true);
	}
    } // classe: fim
    

    