

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOCidade
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOCidade.java
    * TABELA MYSQL:    cidade
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.LinkedHashMap;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOCidade;
	import app.omegasoftware.pontoeletronico.database.Tuple;


	public class EXTDAOCidade extends DAOCidade
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOCidade(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOCidade(this.getDatabase());

    }
    

	public String getNomeEstado(String pIdCidade){
		try{
		this.clearData();
		this.setAttrValue(ID, pIdCidade);
		this.select();
		String vUF = this.getStrValueOfAttribute(EXTDAOCidade.UF_ID_INT);
		EXTDAOUf vObjUF = new EXTDAOUf(this.getDatabase());
		vObjUF.setAttrValue(EXTDAOUf.ID, vUF);
		vObjUF.select();
		String vNomeUF = vObjUF.getStrValueOfAttribute(EXTDAOUf.NOME);
		return vNomeUF;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
return null;
		}
	}

	public LinkedHashMap<String, String> getCidadeFromEstado(String p_ufId)
	{
		this.clearData();
		this.setAttrValue(EXTDAOCidade.UF_ID_INT, p_ufId);
		
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		for(Table iEntry : this.getListTable(new String[]{EXTDAOCidade.NOME}, false))
		{
			EXTDAOCidade cidade = (EXTDAOCidade) iEntry;
			String id = cidade.getAttribute(EXTDAOCidade.ID).getStrValue();
			String dsc = cidade.getAttribute(EXTDAOCidade.NOME).getStrValue();
			if(dsc != null){
				hashTable.put(id, dsc.toUpperCase());
			}
			
		}
		
		return hashTable;
		
	}

	public LinkedHashMap<String, String> getAllCidades()
	{
		
		this.clearData();
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		
		for(Table iEntry : this.getListTable(new String[]{EXTDAOCidade.NOME}, false))
		{
			EXTDAOCidade cidade = (EXTDAOCidade) iEntry;
			String strIdCidade = cidade.getAttribute(EXTDAOCidade.ID).getStrValue();
			String strNomeCidade = cidade.getAttribute(EXTDAOCidade.NOME).getStrValue();
			 
			if(strNomeCidade != null){
				String strNomeCidadeModificado = strNomeCidade.toUpperCase();
				hashTable.put(strIdCidade, strNomeCidadeModificado);	
			}
			
		}
		
		return hashTable;
		
	}

	public static String[] getNomeCidadeEEstado(Database db, String idCidade ){

		return db.getVetorString(
				"SELECT c.nome, uf.nome, p.nome, c.id, uf.id, p.id " +
				" FROM cidade c " +
				"	LEFT JOIN uf " +
				"		ON c.uf_id_INT = uf.id " +
				"	LEFT JOIN pais p " +
				"		ON p.id = uf.pais_id_INT " +
				" WHERE c.id = ?",
				new String[]{idCidade},
				6);
	}


    } // classe: fim
    

    