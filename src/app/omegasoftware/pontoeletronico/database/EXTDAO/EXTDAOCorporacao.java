

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOCorporacao
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOCorporacao.java
    * TABELA MYSQL:    corporacao
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.LinkedHashMap;

import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacao;

	

    
        
    public class EXTDAOCorporacao extends DAOCorporacao
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOCorporacao(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOCorporacao(this.getDatabase());

    }
    


	public static LinkedHashMap<String, String> getAllCorporacao(DatabasePontoEletronico db)
	{
		 
		EXTDAOCorporacao vObj = new EXTDAOCorporacao(db);
		LinkedHashMap<String, String> vHash = vObj.getHashMapIdByDefinition(true);
		return vHash;
	}
	
	public static String[] getStrVetorAllCorporacao(DatabasePontoEletronico db)
	{
		 
		EXTDAOCorporacao vObj = new EXTDAOCorporacao(db);
		return vObj.getVetorStringDefinition(true);
		
	}
	
	public LinkedHashMap<String, String> getAllCidades()
	{
		
		this.clearData();
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		
		for(Table iEntry : this.getListTable(new String[]{EXTDAOCorporacao.NOME}, false))
		{
			EXTDAOCorporacao cidade = (EXTDAOCorporacao) iEntry;
			String strIdCidade = cidade.getAttribute(EXTDAOCorporacao.ID).getStrValue();
			String strNomeCidade = cidade.getAttribute(EXTDAOCorporacao.NOME).getStrValue();
			 
			if(strNomeCidade != null){
				String strNomeCidadeModificado = strNomeCidade.toUpperCase();
				hashTable.put(strIdCidade, strNomeCidadeModificado);	
			}
			
		}
		
		return hashTable;
		
	}
	
	public static void insertCorporacaoSeInexistente(Database db) throws Exception{
		if(!db.exists("SELECT 1 FROM corporacao WHERE id = ? ", new String[]{OmegaSecurity.getIdCorporacao()})){
			EXTDAOCorporacao obj = new EXTDAOCorporacao(db);
			obj.setAttrValue(EXTDAOCorporacao.ID, OmegaSecurity.getIdCorporacao());
			obj.setAttrValue(EXTDAOCorporacao.NOME, OmegaSecurity.getCorporacao());
			obj.formatToSQLite();
			obj.insert(false);	
		} else {
			EXTDAOCorporacao obj = new EXTDAOCorporacao(db);
			obj.select(OmegaSecurity.getIdCorporacao());
			obj.setAttrValue(EXTDAOCorporacao.ID, OmegaSecurity.getIdCorporacao());
			obj.setAttrValue(EXTDAOCorporacao.NOME, OmegaSecurity.getCorporacao());
			obj.formatToSQLite();
			obj.update(false);
		}
	}
	
	public static Long getIdCorporacao(Database db, String nome){
		return db.getLong("SELECT id FROM corporacao WHERE nome = ? ", new String[]{nome});
	}
	
    } // classe: fim
    

    