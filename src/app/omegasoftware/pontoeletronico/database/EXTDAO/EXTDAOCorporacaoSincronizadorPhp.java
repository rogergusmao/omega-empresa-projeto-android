

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOCorporacaoSincronizadorPhp
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOCorporacaoSincronizadorPhp.java
    * TABELA MYSQL:    corporacao_sincronizador_php
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOCorporacaoSincronizadorPhp;

	

    
        
    public class EXTDAOCorporacaoSincronizadorPhp extends DAOCorporacaoSincronizadorPhp
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOCorporacaoSincronizadorPhp(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOCorporacaoSincronizadorPhp(this.getDatabase());

    }
    

    } // classe: fim
    

    