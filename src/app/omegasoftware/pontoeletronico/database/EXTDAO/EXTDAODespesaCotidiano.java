

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAODespesaCotidiano
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAODespesaCotidiano.java
        * TABELA MYSQL:    despesa_cotidiano
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAODespesaCotidiano;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAODespesaCotidiano extends DAODespesaCotidiano
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAODespesaCotidiano(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAODespesaCotidiano(this.getDatabase());

        }
        

        } // classe: fim


        