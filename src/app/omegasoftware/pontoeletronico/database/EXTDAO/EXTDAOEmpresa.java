

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOEmpresa
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOEmpresa.java
    * TABELA MYSQL:    empresa
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailEmpresaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.ContainerPrograma;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.Notificacao;
import app.omegasoftware.pontoeletronico.common.Adapter.EmpresaAdapter;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresa;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.phone.HelperPhone;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_INT;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOEmpresa extends DAOEmpresa
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOEmpresa(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOEmpresa(this.getDatabase());

    }

    public String getDescricao(){
		String nome = this.getStrValueOfAttribute(EXTDAOEmpresa.NOME);
		if(nome == null || nome.length() == 0)
			nome =  this.getStrValueOfAttribute(EXTDAOEmpresa.EMAIL);
		return nome;
	}


	public ArrayList<Long> getIdListBySearchParameters(
			EmpresaAdapter pEmpresaAdapter,
			boolean isAsc) throws Exception {


		String select = "SELECT DISTINCT t."
				+ EXTDAOEmpresa.ID + " ";

		String queryFromTables =  "FROM " + EXTDAOEmpresa.NAME + " t ";

		String queryWhereClausule = ""; 
		ArrayList<String> args = new ArrayList<String>();

		if( pEmpresaAdapter.cidadeId != null){
			queryWhereClausule += " t." + EXTDAOEmpresa.CIDADE_ID_INT + " = ? ";
			args.add(pEmpresaAdapter.cidadeId);
		}

		if(pEmpresaAdapter.bairroId != null ){
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " t." + EXTDAOEmpresa.BAIRRO_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND t." + EXTDAOEmpresa.BAIRRO_ID_INT + " = ? " ;

			args.add(pEmpresaAdapter.bairroId);
		}


		if(pEmpresaAdapter.nomeEmpresa != null ){
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " t." + EXTDAOEmpresa.BAIRRO_ID_INT + " = ? " ;
			else 
				queryWhereClausule += " AND t." + EXTDAOEmpresa.BAIRRO_ID_INT + " = ? " ;

			args.add(pEmpresaAdapter.bairroId);
		}

		if(pEmpresaAdapter.tipoEmpresaId != null){
			queryFromTables +=  ", " + EXTDAOEmpresa.NAME + " fe ";
			if(queryWhereClausule.length() ==  0)
				queryWhereClausule += " t." + EXTDAOEmpresa.ID + "= ?";
			else 
				queryWhereClausule += " AND t." + EXTDAOEmpresa.ID + "= ?";

			queryWhereClausule += " AND fe." + EXTDAOEmpresa.TIPO_EMPRESA_ID_INT + " = ?" ;
			args.add(pEmpresaAdapter.tipoEmpresaId);
		}

		//			if(pEmpresaAdapter.setorId != null){
		//				queryFromTables +=  ", " + EXTDAOSetor.NAME + " fe ";
		//				if(queryWhereClausule.length() ==  0)
		//					queryWhereClausule += " t." + EXTDAOSetor.ID + " = fe." + EXTDAOSetor.EMPRESA_ID_INT;
		//				else 
		//					queryWhereClausule += " AND t." + EXTDAOSetor.ID + " = fe." + EXTDAOSetor.EMPRESA_ID_INT;
		//				
		//				queryWhereClausule += " AND fe." + EXTDAOSetor.EMPRESA_ID_INT + " = ?" ;
		//				args.add(pEmpresaAdapter.setorId);
		//			}

		String orderBy = "";
		if (!isAsc) {
			orderBy += " ORDER BY p." + EXTDAOEmpresa.NOME
					+ " DESC";
		} else {
			orderBy += " ORDER BY  p." + EXTDAOEmpresa.NOME
					+ " ASC";
		}

		String query = select + queryFromTables;
		if(queryWhereClausule.length() > 0 )
			query 	+= " WHERE " + queryWhereClausule;

		if(orderBy.length() > 0 ){
			query += orderBy;  
		} 

		String[] vetorArg = new String [args.size()];
		args.toArray(vetorArg);
		
		Cursor cursor = null;
		try{
			cursor = database.rawQuery(query, vetorArg);
			// Cursor cursor = oh.query(true, EXTDAOEmpresa.NAME,
			// new String[]{EXTDAOEmpresa.ID_PRESTADOR}, "", new String[]{},
			// null, "", "", "");
			String[] chaves = new String[] { EXTDAOEmpresa.ID };
			
			return HelperDatabase.convertCursorToArrayListId(cursor, chaves);
		}finally{
			if(cursor != null && !cursor.isClosed()){
				cursor.close();
			}
		}
		
	}

	

    public Long getTotalFuncionarios(){
    	
    	String q = "SELECT COUNT(pe.pessoa_id_INT)" +
    			"	FROM empresa e" +
    			"		JOIN pessoa_empresa pe" +
    			"			ON pe.empresa_id_INT = e.id" + 
    			"	WHERE e.id = ? ";
    	
    	return getDatabase().getResultSetAsLong(q, 0, new String[]{this.getId()});
    	
    }
    
	
	@Override
	public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
//		try{
		
//		String vStrQuery = HelperHttpPost.getConteudoStringPost(
//				pContext,
//				OmegaConfiguration.URL_REQUEST_UPDATE_OPERADORA_EMPRESA(),
//				new String[] {
//						"usuario", 
//						"id_corporacao",
//						"corporacao", 
//						"senha", 
//						"imei",
//						"id_programa",
//						"id_tupla"
//				}, 
//				new String[] {
//						OmegaSecurity.getIdUsuario(), 
//						OmegaSecurity.getIdCorporacao(),
//						OmegaSecurity.getCorporacao(),
//						OmegaSecurity.getSenha(),
//						HelperPhone.getIMEI(),
//						ContainerPrograma.getIdPrograma(),
//						pNewId});
//
//		if(vStrQuery != null){
//			//			Nao houve modificacao no id da operadora
//			if(vStrQuery.startsWith("TRUE 1")) return;
//			else if(!vStrQuery.startsWith("FALSE")){ //"Houve algum erro";
//
//				String vIdOperadora = HelperString.checkIfIsNull(vStrQuery);
//				EXTDAOEmpresa vObjEmpresa = new EXTDAOEmpresa(this.getDatabase());
//				vObjEmpresa.setAttrValue(EXTDAOEmpresa.ID, pOldId);
//				if(vObjEmpresa.select()){
//					vObjEmpresa.setAttrValue(EXTDAOEmpresa.OPERADORA_ID_INT, vIdOperadora);
//					vObjEmpresa.update(false);
//				}
//			}
//		}
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//		
//		}
	}

	public Long getTotalFuncionarioNaoCompareceram(HelperDate data ){
		
		EXTDAOTipoPonto.TIPO_PONTO tipoPonto = EXTDAOTipoPonto.TIPO_PONTO.TURNO;
		
    	String q = "SELECT COUNT( DISTINCT (p.id)) " +
    			"	FROM pessoa p JOIN pessoa_empresa pe ON p.id = pe.pessoa_id_INT" + 
    			"	WHERE pe.empresa_id_INT = ? AND (SELECT COUNT (p2.id)" +
    			"			FROM ponto p2" +
    			"			WHERE p2.tipo_ponto_id_INT = ? " +
    			"				AND p2.empresa_id_INT = pe.empresa_id_INT" +
    			"				AND p2.pessoa_id_INT = p.id" +
    			"				AND p2.dia_DATE = ?) = 0";
    	
    	
    	
    	return getDatabase().getResultSetAsLong(q, 0, 
    			new String[]{
    				this.getId(),
	    			String.valueOf( tipoPonto.getId()),
	    			data.getDataSQL()});
    	
    }
	
    
    public Long getTotalFuncionarioNoIntervaloOuTurno(
    		HelperDate data, 
    		EXTDAOTipoPonto.TIPO_PONTO tipoPonto){
    	
    	String q = "SELECT COUNT( DISTINCT (p.pessoa_id_INT)) " +
    			"	FROM ponto p " + 
    			"	WHERE p.empresa_id_INT = ? " +
    			"		AND p.tipo_ponto_id_INT = ?" +
    			"		AND p.dia_DATE = ? " +
    			"		AND (SELECT COUNT (p2.id)" +
    			"			FROM ponto p2" +
    			"			WHERE p2.tipo_ponto_id_INT = ? " +
    			"				AND p2.empresa_id_INT = ?" +
    			"				AND p2.id != p.id" +
    			"				AND p2.dia_DATE || ' ' || p2.hora_TIME > p.dia_DATE || ' ' || p.hora_TIME  ) = 0";
    	
    	
    	
    	
    	return getDatabase().getResultSetAsLong(q, 0, new String[]{
    			this.getId(), 
    			String.valueOf( tipoPonto.getId()),
    			data.getDataSQL(),
    			String.valueOf( tipoPonto.getId()), 
    			this.getId()});
    	
    }
    public static String getNomeEmpresaIndefinida(Context context){
    	return context.getString(R.string.empresa_indefinida);
    }
    public static Long getIdEmpresaIndefinida(Database db){
    	return getIdEmpresaIndefinida(db, OmegaSecurity.getIdCorporacao(), OmegaSecurity.getIdUsuario());
    }
	public static Long getIdEmpresaIndefinida(Database db, String idCorporacao, String idUsuario){
		
		String email = db.getContext().getString(R.string.email_indefinido);
		
		Long id = db.getFirstLong("SELECT id FROM empresa WHERE email = ? AND corporacao_id_INT = ? ", 
				new String[]{email, idCorporacao});
		if(id != null) return id;
		
		EXTDAOEmpresa objEmpresa = new EXTDAOEmpresa(db);
		objEmpresa.setAttrValue(EXTDAOEmpresa.EMAIL,email );
		
		objEmpresa.setAttrValue(EXTDAOEmpresa.NOME, db.getContext().getString(R.string.empresa_indefinida));
		objEmpresa.formatToSQLite();
		objEmpresa.setAttrValue(EXTDAOEmpresa.CORPORACAO_ID_INT, idCorporacao);
		return objEmpresa.insert(true, idCorporacao, idUsuario);
		
	}
	
	public static String getIdEmpresaParaRelogioDePonto(Database db){
		int ultimoIdEmpresa = PontoEletronicoSharedPreference.getValor(db.getContext(), TIPO_INT.ULTIMO_ID_EMPRESA_RELOGIO_PONTO, 0);
		if(ultimoIdEmpresa != 0) return String.valueOf(ultimoIdEmpresa);
		String q = "SELECT e.id "
				+ "FROM empresa e "
				+ "JOIN empresa_perfil ep ON e.id = ep.empresa_id_INT "
				+ "WHERE e.corporacao_id_INT = ? AND ep.perfil_id_INT = ? ";
		Long idEmpresa =db.getFirstLong(q, new String[]{OmegaSecurity.getIdCorporacao(), EXTDAOPerfil.ID_STR_EMPRESA_CORPORACAO});
		if(idEmpresa == null){
			q = "SELECT e.id "
					+ "FROM empresa e "
					+ "WHERE e.corporacao_id_INT = ? "
					+ " LIMIT 0, 1";
			idEmpresa =db.getFirstLong(q, new String[]{OmegaSecurity.getIdCorporacao()});
			if(idEmpresa == null)
				idEmpresa =EXTDAOEmpresa.getIdEmpresaIndefinida(db);
		}
		if(idEmpresa == null) return null;
		
		PontoEletronicoSharedPreference.saveValor(
				db.getContext(), TIPO_INT.ULTIMO_ID_EMPRESA_RELOGIO_PONTO,idEmpresa);
		
		return String.valueOf( idEmpresa);
		
	}
	
	public static LinkedHashMap<String, String> getAllEmpresa(Database db)
	{
		EXTDAOEmpresa vObj = new EXTDAOEmpresa(db);
		return vObj.getHashMapIdByDefinition(true);
	}

	public class NotificacaoEmpresa extends Notificacao{

		public NotificacaoEmpresa(Context c) {
			super(c);

		}

		@Override
		public void adicionaParametrosAcao(Intent intent) {
			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, getId());
			
		}

		
		
		@Override
		public Class<?> getClasseAcao() {

			return DetailEmpresaActivityMobile.class;
		}

		@Override
		public String getSubtitulo() {
			
			return getDescricao(); 
		}

		@Override
		public SEXO getSexo() {
			return Table.SEXO.FEMININO;
		}

		@Override
		protected int getIdStringSufixoTitulo() {
			
			return R.string.empresa;
		}
		
	}
	
	//Funcao deve ser sobrescrevida pelas entidades que iruo emitir notificacao
	@Override
	public Notificacao getNotificacao(Context c){
		return new NotificacaoEmpresa(c);
	}
	
	
	public static String getNomeEmpresa(Database db, String id){

		return db.getFirstString("SELECT nome FROM empresa WHERE id = ? AND corporacao_id_INT = ? ",
				new String[]{id, OmegaSecurity.getIdCorporacao()});
	}
    } // classe: fim
    

    