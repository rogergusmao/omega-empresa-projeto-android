

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOEmpresaAtividade
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOEmpresaAtividade.java
        * TABELA MYSQL:    empresa_atividade
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaAtividade;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOEmpresaAtividade extends DAOEmpresaAtividade
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOEmpresaAtividade(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOEmpresaAtividade(this.getDatabase());

        }
        

        } // classe: fim


        