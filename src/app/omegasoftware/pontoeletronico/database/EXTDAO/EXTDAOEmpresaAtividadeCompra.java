

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOEmpresaAtividadeCompra
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOEmpresaAtividadeCompra.java
        * TABELA MYSQL:    empresa_atividade_compra
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaAtividadeCompra;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOEmpresaAtividadeCompra extends DAOEmpresaAtividadeCompra
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOEmpresaAtividadeCompra(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOEmpresaAtividadeCompra(this.getDatabase());

        }
        

        } // classe: fim


        