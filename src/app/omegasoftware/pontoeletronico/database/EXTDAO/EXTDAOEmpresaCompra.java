

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOEmpresaCompra
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOEmpresaCompra.java
    * TABELA MYSQL:    empresa_compra
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaCompra;

	

    
        
    public class EXTDAOEmpresaCompra extends DAOEmpresaCompra
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOEmpresaCompra(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOEmpresaCompra(this.getDatabase());

    }
    

    } // classe: fim
    

    