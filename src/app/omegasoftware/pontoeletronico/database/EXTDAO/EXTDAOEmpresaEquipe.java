

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOEmpresaEquipe
        * DATA DE GERAuuO: 21.08.2017
        * ARQUIVO:         EXTDAOEmpresaEquipe.java
        * TABELA MYSQL:    empresa_equipe
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaEquipe;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAuuO DA CLASSE
        // **********************


    
        

        public class EXTDAOEmpresaEquipe extends DAOEmpresaEquipe
        {


        // *************************
        // DECLARAuuO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOEmpresaEquipe(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOEmpresaEquipe(this.getDatabase());

        }
        

        } // classe: fim


        