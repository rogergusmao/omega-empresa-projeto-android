

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOEmpresaPerfil
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOEmpresaPerfil.java
    * TABELA MYSQL:    empresa_perfil
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.ArrayList;

import android.database.Cursor;
import app.omegasoftware.pontoeletronico.database.Database;

import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaPerfil;

	

    
        
    public class EXTDAOEmpresaPerfil extends DAOEmpresaPerfil
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOEmpresaPerfil(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOEmpresaPerfil(this.getDatabase());

    }
    

    public ArrayList<Table> getListTuplaEmpresaSemSerDaMinhaCorporacao(){		

		try{
		
			
			Cursor cursor = null;
//			if(!database.isOpen())
//				database.open();
			
			

			cursor = database.query(getName(), null, PERFIL_ID_INT + "  != ?", new String[]{EXTDAOPerfil.ID_STR_EMPRESA_CORPORACAO}, null,  "", null);

			ArrayList<Table> listEXTDAO = new ArrayList<Table>();
			try{
				if (cursor.moveToFirst()) {

					do {

						Table table = this.factory();

						boolean validade = false;
						for(int i = 0; i < listSortNameAttribute.size(); i ++){
							String strNameAttr = listSortNameAttribute.get(i);
							String strValue = cursor.getString(i);
							table.setAttrValue(strNameAttr, strValue);
							validade = true;
						}
						if(validade){
							listEXTDAO.add(table);
						}

						if(cursor.isLast()) break;
					}while (cursor.moveToNext()); 
				}
			} catch(Exception ex){
				//if(ex != null)
					//Log.d(TAG, ex.getMessage());
			}

			if (cursor != null && !cursor.isClosed()) {

				cursor.close();
			}
			return listEXTDAO;
		}catch(Exception ex){
			
			//if(ex != null)
				//Log.d(TAG, ex.getMessage());
		}
		return null;
	}
    
    } // classe: fim
    

    