

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOEmpresaProdutoVenda
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOEmpresaProdutoVenda.java
    * TABELA MYSQL:    empresa_produto_venda
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaProdutoVenda;

	

    
        
    public class EXTDAOEmpresaProdutoVenda extends DAOEmpresaProdutoVenda
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOEmpresaProdutoVenda(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOEmpresaProdutoVenda(this.getDatabase());

    }
    

    } // classe: fim
    

    