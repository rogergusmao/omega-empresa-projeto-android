

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOEmpresaVenda
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOEmpresaVenda.java
    * TABELA MYSQL:    empresa_venda
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaVenda;

	

    
        
    public class EXTDAOEmpresaVenda extends DAOEmpresaVenda
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOEmpresaVenda(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOEmpresaVenda(this.getDatabase());

    }
    

    } // classe: fim
    

    