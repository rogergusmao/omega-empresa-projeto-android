

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOEmpresaVendaParcela
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOEmpresaVendaParcela.java
    * TABELA MYSQL:    empresa_venda_parcela
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaVendaParcela;

	

    
        
    public class EXTDAOEmpresaVendaParcela extends DAOEmpresaVendaParcela
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOEmpresaVendaParcela(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOEmpresaVendaParcela(this.getDatabase());

    }
    

    } // classe: fim
    

    