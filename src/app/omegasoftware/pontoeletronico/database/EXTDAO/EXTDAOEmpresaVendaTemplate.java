

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOEmpresaVendaTemplate
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOEmpresaVendaTemplate.java
        * TABELA MYSQL:    empresa_venda_template
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOEmpresaVendaTemplate;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOEmpresaVendaTemplate extends DAOEmpresaVendaTemplate
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOEmpresaVendaTemplate(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOEmpresaVendaTemplate(this.getDatabase());

        }
        

        } // classe: fim


        