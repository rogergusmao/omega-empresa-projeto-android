

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOEstadoCivil
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOEstadoCivil.java
    * TABELA MYSQL:    estado_civil
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOEstadoCivil;

	

    
        
    public class EXTDAOEstadoCivil extends DAOEstadoCivil
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOEstadoCivil(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOEstadoCivil(this.getDatabase());

    }
    

    } // classe: fim
    

    