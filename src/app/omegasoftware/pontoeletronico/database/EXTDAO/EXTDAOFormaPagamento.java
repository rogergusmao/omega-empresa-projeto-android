

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOFormaPagamento
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOFormaPagamento.java
    * TABELA MYSQL:    forma_pagamento
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.LinkedHashMap;

import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOFormaPagamento;

	

    
        
    public class EXTDAOFormaPagamento extends DAOFormaPagamento
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOFormaPagamento(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOFormaPagamento(this.getDatabase());

    }
    

	public static LinkedHashMap<String, String> getAllCorporacao(DatabasePontoEletronico db)
	{
		 
		EXTDAOFormaPagamento vObj = new EXTDAOFormaPagamento(db);
		LinkedHashMap<String, String> vHash = vObj.getHashMapIdByDefinition(true);
		return vHash;
	}
	
	public static String[] getStrVetorAllCorporacao(DatabasePontoEletronico db)
	{
		 
		EXTDAOFormaPagamento vObj = new EXTDAOFormaPagamento(db);
		return vObj.getVetorStringDefinition(true);
		
	}
	
	public LinkedHashMap<String, String> getAllCidades()
	{
		
		this.clearData();
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		
		for(Table iEntry : this.getListTable(new String[]{EXTDAOFormaPagamento.NOME}, false))
		{
			EXTDAOFormaPagamento cidade = (EXTDAOFormaPagamento) iEntry;
			String strIdCidade = cidade.getAttribute(EXTDAOFormaPagamento.ID).getStrValue();
			String strNomeCidade = cidade.getAttribute(EXTDAOFormaPagamento.NOME).getStrValue();
			 
			if(strNomeCidade != null){
				String strNomeCidadeModificado = strNomeCidade.toUpperCase();
				hashTable.put(strIdCidade, strNomeCidadeModificado);	
			}
			
		}
		
		return hashTable;
		
	}
    } // classe: fim
    

    