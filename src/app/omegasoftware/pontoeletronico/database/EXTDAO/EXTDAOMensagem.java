

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOMensagem
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOMensagem.java
        * TABELA MYSQL:    mensagem
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOMensagem;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOMensagem extends DAOMensagem
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOMensagem(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOMensagem(this.getDatabase());

        }
        

        } // classe: fim


        