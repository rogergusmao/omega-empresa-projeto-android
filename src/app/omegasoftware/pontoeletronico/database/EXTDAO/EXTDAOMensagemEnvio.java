

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOMensagemEnvio
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOMensagemEnvio.java
        * TABELA MYSQL:    mensagem_envio
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOMensagemEnvio;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOMensagemEnvio extends DAOMensagemEnvio
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOMensagemEnvio(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOMensagemEnvio(this.getDatabase());

        }
        

        } // classe: fim


        