

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOModelo
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOModelo.java
    * TABELA MYSQL:    modelo
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;

import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOModelo;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

	

    
        
    public class EXTDAOModelo extends DAOModelo
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOModelo(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOModelo(this.getDatabase());

    }
    

	public LinkedHashMap<String, String> getDistinctHashAnoModeloOfNomeModelo(String p_nomeModelo)
	{
		this.clearData();
		if(p_nomeModelo.compareTo(OmegaConfiguration.UNEXISTENT_ID_IN_DB) == 0 )
			return new LinkedHashMap<String, String>();
		this.setAttrValue(EXTDAOModelo.NOME, "%" + p_nomeModelo + "%");
		
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		ArrayList<Table> vListTableModelo = this.getListTable(new String[]{EXTDAOModelo.ANO_INT});
		HashSet<Integer> vHashSetAnoModelo = new HashSet<Integer>();
		
		for(Table iEntry : vListTableModelo)
		{
			EXTDAOModelo modelo = (EXTDAOModelo) iEntry;
			
			String dsc = modelo.getAttribute(EXTDAOModelo.ANO_INT).getStrValue();
			Integer vValue = HelperInteger.parserInt(dsc);
			if(dsc != null)
				if(vHashSetAnoModelo.contains(vValue)){
					continue;
				}
				else{
					vHashSetAnoModelo.add(vValue);
					vValue += 1;
					String vStrIndexHashTable = String.valueOf(dsc);	
					hashTable.put(vStrIndexHashTable, dsc.toUpperCase());
				}
				
		}
		
		return hashTable;
		
	}
	public String getDescricao(){
		String ano = this.getStrValueOfAttribute(EXTDAOModelo.ANO_INT);
		String nome = this.getStrValueOfAttribute(EXTDAOModelo.NOME);
		return nome + " - " + ano;
	}
	
	public LinkedHashMap<String, String> getAnoModeloFromNomeModelo(String p_nomeModelo)
	{
		this.clearData();
		this.setAttrValue(EXTDAOModelo.NOME, p_nomeModelo);
		
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		
		for(Table iEntry : this.getListTable(new String[]{EXTDAOModelo.ANO_INT}))
		{
			EXTDAOModelo modelo = (EXTDAOModelo) iEntry;
			String id = modelo.getAttribute(EXTDAOModelo.ID).getStrValue();
			String dsc = modelo.getAttribute(EXTDAOModelo.ANO_INT).getStrValue();
			if(dsc != null){
				hashTable.put(id, dsc.toUpperCase());
			}
		}
		
		return hashTable;
		
	}
    } // classe: fim
    

    