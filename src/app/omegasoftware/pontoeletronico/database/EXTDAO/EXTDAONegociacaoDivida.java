

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAONegociacaoDivida
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAONegociacaoDivida.java
        * TABELA MYSQL:    negociacao_divida
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAONegociacaoDivida;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAONegociacaoDivida extends DAONegociacaoDivida
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAONegociacaoDivida(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAONegociacaoDivida(this.getDatabase());

        }
        

        } // classe: fim


        