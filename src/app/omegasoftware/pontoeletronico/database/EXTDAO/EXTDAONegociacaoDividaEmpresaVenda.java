

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAONegociacaoDividaEmpresaVenda
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAONegociacaoDividaEmpresaVenda.java
        * TABELA MYSQL:    negociacao_divida_empresa_venda
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAONegociacaoDividaEmpresaVenda;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAONegociacaoDividaEmpresaVenda extends DAONegociacaoDividaEmpresaVenda
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAONegociacaoDividaEmpresaVenda(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAONegociacaoDividaEmpresaVenda(this.getDatabase());

        }
        

        } // classe: fim


        