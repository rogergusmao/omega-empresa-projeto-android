

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAONotaTipoNota
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAONotaTipoNota.java
        * TABELA MYSQL:    nota_tipo_nota
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAONotaTipoNota;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAONotaTipoNota extends DAONotaTipoNota
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAONotaTipoNota(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAONotaTipoNota(this.getDatabase());

        }
        

        } // classe: fim


        