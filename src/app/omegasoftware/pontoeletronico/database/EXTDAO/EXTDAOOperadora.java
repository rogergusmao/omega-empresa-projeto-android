

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOOperadora
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOOperadora.java
    * TABELA MYSQL:    operadora
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOOperadora;

	

    
        
    public class EXTDAOOperadora extends DAOOperadora
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOOperadora(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOOperadora(this.getDatabase());

    }
    

    } // classe: fim
    

    