

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOPais
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOPais.java
    * TABELA MYSQL:    pais
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.LinkedHashMap;

import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOPais;

	

    
        
    public class EXTDAOPais extends DAOPais
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOPais(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOPais(this.getDatabase());

    }
    

	public LinkedHashMap<String, String> getAllCidades()
	{
		
		this.clearData();
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		
		for(Table iEntry : this.getListTable(new String[]{EXTDAOPais.NOME}))
		{
			EXTDAOPais cidade = (EXTDAOPais) iEntry;
			String strIdCidade = cidade.getAttribute(EXTDAOPais.ID).getStrValue();
			String strNomeCidade = cidade.getAttribute(EXTDAOPais.NOME).getStrValue();
			 
			if(strNomeCidade != null){
				String strNomeCidadeModificado = strNomeCidade.toUpperCase();
				hashTable.put(strIdCidade, strNomeCidadeModificado);	
			}
			
		}
		
		return hashTable;
		
	}
    } // classe: fim
    

    