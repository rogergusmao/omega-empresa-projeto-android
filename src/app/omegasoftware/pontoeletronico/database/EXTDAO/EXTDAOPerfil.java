

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOPerfil
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOPerfil.java
    * TABELA MYSQL:    perfil
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import android.content.Context;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOPerfil;

	

    
        
    public class EXTDAOPerfil extends DAOPerfil
    {

    	public final static int ID_PARCEIRO = 1;
    	public final static int ID_CLIENTE = 2;
    	public final static int ID_FORNECEDOR = 3;
    	public final static int ID_EMPRESA_CORPORACAO = 4;
    	
    	public final static String ID_STR_PARCEIRO = "1";
    	public final static String ID_STR_CLIENTE = "2";
    	public final static String ID_STR_FORNECEDOR = "3";
    	public final static String ID_STR_EMPRESA_CORPORACAO = "4";
    	
    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOPerfil(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOPerfil(this.getDatabase());

    }
    

public static String getNomePerfil(Context pContext, int pIdPerfil){
	switch (pIdPerfil) {
	case ID_CLIENTE:
		return pContext.getResources().getString(R.string.empresa_papel_cliente);
		
	case ID_PARCEIRO:
			
		return pContext.getResources().getString(R.string.empresa_papel_parceiro);
	case ID_FORNECEDOR:
		
		return pContext.getResources().getString(R.string.empresa_papel_fornecedor);
	case ID_EMPRESA_CORPORACAO:
		
		return pContext.getResources().getString(R.string.empresa_papel_empresa_corporacao);
	

	default:
		return null;
	}
}
    } // classe: fim
    

    