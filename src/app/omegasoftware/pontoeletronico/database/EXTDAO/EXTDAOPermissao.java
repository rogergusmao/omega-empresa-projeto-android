

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOPermissao
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOPermissao.java
    * TABELA MYSQL:    permissao
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.HashSet;

import android.database.Cursor;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.database.Database;

import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOPermissao;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

	

    
        
    public class EXTDAOPermissao extends DAOPermissao
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOPermissao(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOPermissao(this.getDatabase());

    }
    
    

    public static HashSet<Integer> getListaPermissao(Database db, String categoriaPermissao){
    	String q = "SELECT DISTINCT p.id " +
    	" FROM " + 
    	" permissao p " + 
    	" JOIN permissao_categoria_permissao pcp ON pcp.permissao_id_INT = p.id " +
    	" WHERE pcp.categoria_permissao_id_INT = ?";
    	
		String[] vetorArg = new String[] {categoriaPermissao};
		
		HashSet<Integer> list = new HashSet<Integer>();
		
		Cursor cursor = null;
		
		try{
			cursor = db.rawQuery(q, vetorArg);
			if (cursor.moveToFirst()) {
				
				Integer id = HelperInteger.parserInteger( cursor.getString(0));
				if(id == null ){
					throw new Exception("Consulta invalida: " + q);
				}
				list.add(id);
				
			}
		} catch(Exception ex){
			SingletonLog.insereErro(
					TAG, 
					"getListaPermissao", 
					HelperExcecao.getDescricao(ex), 
					SingletonLog.TIPO.BANCO);
		} finally{
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}	
		}
		
		return list;
    }
    

    } // classe: fim
    

    