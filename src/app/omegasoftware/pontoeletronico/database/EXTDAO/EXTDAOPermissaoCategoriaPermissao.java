

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOPermissaoCategoriaPermissao
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOPermissaoCategoriaPermissao.java
    * TABELA MYSQL:    permissao_categoria_permissao
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.container.ContainerMenuOption;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOPermissaoCategoriaPermissao;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

	

    
        
    public class EXTDAOPermissaoCategoriaPermissao extends DAOPermissaoCategoriaPermissao
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOPermissaoCategoriaPermissao(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOPermissaoCategoriaPermissao(this.getDatabase());

    }
    
    public boolean hasPermissaoToRemove(String pTableName){
    	try{
		if(pTableName == null) return false;
		else if(pTableName.length() == 0 ) return false;
		else{
			if(OmegaSecurity.isAdm()) return true;
			else{
				EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(this.getDatabase());
				vObjUsuario.setAttrValue(EXTDAOUsuario.ID, OmegaSecurity.getIdUsuario());
				EXTDAOCategoriaPermissao vTuplaCategoriaPermissao = vObjUsuario.getObjCategoriaPermissao();
				String vIdCategoriaPermissao = vTuplaCategoriaPermissao.getStrValueOfAttribute(EXTDAOCategoriaPermissao.ID);

				EXTDAOPermissao vObjPermissao  = new EXTDAOPermissao(this.getDatabase());
				vObjPermissao.setAttrValue(EXTDAOPermissao.TAG, "Remove" + HelperString.ucFirst(pTableName));
				EXTDAOPermissao vTuplaPermissao = (EXTDAOPermissao) vObjPermissao.getFirstTupla();
				if(vTuplaPermissao == null) return false;

				String vIdPermissao = vTuplaPermissao.getStrValueOfAttribute(EXTDAOPermissao.ID);
				this.clearData();

				this.setAttrValue(EXTDAOPermissaoCategoriaPermissao.PERMISSAO_ID_INT, vIdPermissao);
				this.setAttrValue(EXTDAOPermissaoCategoriaPermissao.CATEGORIA_PERMISSAO_ID_INT, vIdCategoriaPermissao);

				EXTDAOPermissaoCategoriaPermissao vTuplaPermissaoCategoriaPermissao = (EXTDAOPermissaoCategoriaPermissao) this.getFirstTupla();
				if(vTuplaPermissaoCategoriaPermissao == null) return false;
				else return true;
			}
		}
    	}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
		return false;
		}
	}
	public boolean hasPermissaoToEdit(String pTableName){
		try{
		//		TODO verificar se eh adm ou nao, antes de fazer a verificacao
		if(pTableName == null) return false;
		else if(pTableName.length() == 0 ) return false;
		else{
			if(OmegaSecurity.isAdm()) return true;
			else{
				EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(this.getDatabase());
				vObjUsuario.setAttrValue(EXTDAOUsuario.ID, OmegaSecurity.getIdUsuario());
				EXTDAOCategoriaPermissao vTuplaCategoriaPermissao = vObjUsuario.getObjCategoriaPermissao();
				String vIdCategoriaPermissao = vTuplaCategoriaPermissao.getStrValueOfAttribute(EXTDAOCategoriaPermissao.ID);

				EXTDAOPermissao vObjPermissao  = new EXTDAOPermissao(this.getDatabase());
				vObjPermissao.setAttrValue(EXTDAOPermissao.TAG, "Edit" + HelperString.ucFirst(pTableName));

				EXTDAOPermissao vTuplaPermissao = (EXTDAOPermissao) vObjPermissao.getFirstTupla();
				if(vTuplaPermissao == null) return false;

				String vIdPermissao = vTuplaPermissao.getStrValueOfAttribute(EXTDAOPermissao.ID);
				String vTagPermissao = vTuplaPermissao.getStrValueOfAttribute(EXTDAOPermissao.TAG);
				if(!ContainerMenuOption.isContainerItemExistent(vTagPermissao))
					return false;
				this.clearData();

				this.setAttrValue(EXTDAOPermissaoCategoriaPermissao.PERMISSAO_ID_INT, vIdPermissao);
				this.setAttrValue(EXTDAOPermissaoCategoriaPermissao.CATEGORIA_PERMISSAO_ID_INT, vIdCategoriaPermissao);

				EXTDAOPermissaoCategoriaPermissao vTuplaPermissaoCategoriaPermissao = (EXTDAOPermissaoCategoriaPermissao) this.getFirstTupla();

				if(vTuplaPermissaoCategoriaPermissao != null ) return true;
				else return false;
			}
		}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
		return false;
		}
	}
    } // classe: fim
    

    