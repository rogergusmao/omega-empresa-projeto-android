

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOPessoa
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOPessoa.java
    * TABELA MYSQL:    pessoa
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.content.Context;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.ContainerPrograma;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoa;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.phone.HelperPhone;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

	

    
        
    public class EXTDAOPessoa extends DAOPessoa
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOPessoa(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOPessoa(this.getDatabase());

    }

    public String getIdUsuario(){
    	EXTDAOPessoaUsuario objPessoaUsuario = new EXTDAOPessoaUsuario(this.database);
    	objPessoaUsuario.setAttrValue(EXTDAOPessoaUsuario.PESSOA_ID_INT, this.getId());
    	Table reg=  objPessoaUsuario.getFirstTupla();
    	if(reg != null){
    		return reg.getStrValueOfAttribute(EXTDAOPessoaUsuario.USUARIO_ID_INT);
    	} else return null;
    }
    
    public ArrayList<Table> getPessoasEmpresa(String idEmpresa){
    	String id = super.getId(); 
    	EXTDAOPessoaEmpresa objPessoaEmpresa = new EXTDAOPessoaEmpresa(getDatabase());
    	objPessoaEmpresa.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, id);
    	if(idEmpresa != null)
    		objPessoaEmpresa.setAttrValue(EXTDAOPessoaEmpresa.EMPRESA_ID_INT, idEmpresa);
    	
    	ArrayList<Table> registros = objPessoaEmpresa.getListTable();
    	return registros;
    }
    
	public static LinkedHashMap<String, String> getAllPessoa(Database db)
	{
		EXTDAOPessoa vObj = new EXTDAOPessoa(db);
		return vObj.getHashMapIdByDefinition(true);
	}
	

	public String getCidade() throws Exception
	{	
		return getNomeDaChaveExtrangeira(EXTDAOPessoa.CIDADE_ID_INT);
	}

	public String getDescricao(){
		String nome = this.getStrValueOfAttribute(EXTDAOPessoa.NOME);
		if(nome == null || nome.length() == 0)
			nome =  this.getStrValueOfAttribute(EXTDAOPessoa.EMAIL);
		return nome;
	}

	public ArrayList<String> getSpecialities()
	{
try{
		ArrayList<String> result = new ArrayList<String>();
		EXTDAOPessoaEmpresa specialties = new EXTDAOPessoaEmpresa(this.getDatabase());
		specialties.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, this.getAttribute(EXTDAOPessoa.ID).getStrValue());

		for(Table iEntry : specialties.getListTable())
		{
			String result2 = ((EXTDAOPessoaEmpresa) iEntry).getStrProfissao();
			if(result2 != null){
				if(result2.length() > 0 ){
					result.add(result2);		
				}
			}
		}

		return result;
}catch(Exception ex){
	SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
return null;
}
	}



	public String getProfissoesString()
	{
		String result = "";
		for(String specialty : this.getSpecialities())
		{
			result += specialty + ", ";
		}

		if(!result.equals(""))
		{
			return result.substring(0, result.lastIndexOf(","));
		}
		else
		{
			return "";	
		}

	}

	public String getSex() throws Exception
	{
		ArrayList<String> vResult = new ArrayList<String>();
		EXTDAOSexo vSex = new EXTDAOSexo(this.getDatabase());
		vSex.setAttrValue(EXTDAOSexo.ID, this.getAttribute(EXTDAOPessoa.SEXO_ID_INT).getStrValue());
		for(Table iEntry : vSex.getListTable())
		{
			vResult.add(((EXTDAOSexo) iEntry).getSexDescription());
		}

		return vResult.size() > 0 ? vResult.get(0) : null; 				
	}



	public String getFormattedSex(Context p_context)
	{

		String codeSex = this.getAttribute(EXTDAOPessoa.SEXO_ID_INT).getStrValue();

		if(codeSex == null || codeSex.length() == 0 )
		{
			return "";
		}
		else if(codeSex.equals(OmegaConfiguration.MASCULIN_CODE_IN_DB))
		{
			return p_context.getResources().getString(R.string.sex_masculin);
		}
		else if(codeSex.equals(OmegaConfiguration.FEMININ_CODE_IN_DB))
		{
			return p_context.getResources().getString(R.string.sex_feminin);
		}
		else
		{
			return "";	
		}

	}

	public String getFormattedType(Context p_context)
	{

		return "";

	}




	@Override
	public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
//		try{
//
//		String vStrQuery = HelperHttpPost.getConteudoStringPost(
//				pContext,
//				OmegaConfiguration.URL_REQUEST_UPDATE_OPERADORA_PESSOA(),
//				new String[] {
//						"usuario", 
//						"id_corporacao",
//						"corporacao", 
//						"senha", 
//						"imei",
//						"id_programa",
//						"id_tupla"
//				}, 
//				new String[] {
//						OmegaSecurity.getIdUsuario(), 
//						OmegaSecurity.getIdCorporacao(),
//						OmegaSecurity.getCorporacao(),
//						OmegaSecurity.getSenha(),
//						HelperPhone.getIMEI(),
//						ContainerPrograma.getIdPrograma(),
//						pNewId});
//
//		if(vStrQuery != null){
////			Nao houve modificacao no id da operadora
//			if(vStrQuery.startsWith("TRUE 1")) return;
//			else if(!vStrQuery.startsWith("FALSE")){ //"Houve algum erro";
//
//				String vIdOperadora = HelperString.checkIfIsNull(vStrQuery);
//				EXTDAOPessoa vObjPessoa = new EXTDAOPessoa(this.getDatabase());
//				vObjPessoa.setAttrValue(EXTDAOPessoa.ID, pOldId);
//				if(vObjPessoa.select()){
//					vObjPessoa.setAttrValue(EXTDAOPessoa.OPERADORA_ID_INT, vIdOperadora);
//					vObjPessoa.update(false);
//				}
//			}
//		}
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//		
//		}
		

		
		
		
	}


	public static String getNomeDaPessoa(Database db, String idPessoa){
		return db.getFirstString("SELECT nome FROM pessoa WHERE id = ? ", new String[]{idPessoa});
	}

    } // classe: fim
    

    