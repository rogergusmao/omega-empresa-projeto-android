

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOPessoaEmpresa
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOPessoaEmpresa.java
    * TABELA MYSQL:    pessoa_empresa
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import android.database.Cursor;
import app.omegasoftware.pontoeletronico.common.container.EntradaSaida;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.ResultSet;
import app.omegasoftware.pontoeletronico.database.Table;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoaEmpresa;

	

    
        
    public class EXTDAOPessoaEmpresa extends DAOPessoaEmpresa
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOPessoaEmpresa(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOPessoaEmpresa(this.getDatabase());

    }

    public EXTDAOPessoa getObjPessoa() throws Exception{
    	Table t =getObjDaChaveExtrangeira(EXTDAOPessoaEmpresa.PESSOA_ID_INT);
    	if(t != null){
    		return (EXTDAOPessoa) t;
    	} else return null;
    }
    
    public EXTDAOProfissao getObjProfissao() throws Exception{
    	
    	Table t =getObjDaChaveExtrangeira(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT);
    	if(t != null){
    		return (EXTDAOProfissao) t;
    	} else return null;
    	
    }
    
	public String getStrProfissao() throws Exception{
		EXTDAOProfissao obj = new EXTDAOProfissao(this.database);
		
		String idProfissao = this.getAttribute(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT).getStrValue();
		
		if(idProfissao != null){
			
			obj.setAttrValue(EXTDAOProfissao.ID, idProfissao);
			obj.select();
			return obj.getAttribute(EXTDAOProfissao.NOME).getStrValue();
		} else return null;
		
	}
	
	public ResultSet getRegistrosDaPessoa(String idPessoa){
		String query = "SELECT p.nome, e.nome, po.nome, pe.id, e.id, p.id, po.id " +
				" FROM pessoa_empresa pe " +
				"		JOIN pessoa p " +
				"			ON pe.pessoa_id_INT = p.id " +
				"		JOIN empresa e " +
				"			ON pe.empresa_id_INT = e.id " +
				"		JOIN profissao po " +
				"			ON po.id = pe.profissao_id_INT" +
				"	WHERE pe.pessoa_id_INT = ? ";
		Database db = getDatabase();
		return db.getResultSet(query, new String[]{idPessoa});
	}
	public static Table cadastrarProfissaoIndefinidaDaPessoa(Database db, String idEmpresa, String idPessoa) throws Exception{
		Long idProfissao = EXTDAOProfissao.getProfissaoindefinida(db);
		
		EXTDAOPessoaEmpresa obj = new EXTDAOPessoaEmpresa(db);
		if(idEmpresa == null){
			Long lIdEmpresa = EXTDAOEmpresa.getIdEmpresaIndefinida(db);
			if(lIdEmpresa != null){
				idEmpresa= String.valueOf(lIdEmpresa); 
			}
		}
		obj.setAttrValue(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT, String.valueOf( idProfissao));
		obj.setAttrValue(EXTDAOPessoaEmpresa.EMPRESA_ID_INT, String.valueOf( idEmpresa));
		obj.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, String.valueOf( idEmpresa));
		Table reg=  obj.getFirstTupla();
		if(reg != null) return reg; 
		obj.formatToSQLite();
		obj.insert(true);
		return obj;
		
	}
	

	public static boolean pessoaPossuiProfissao(Database db , String idPessoa){
		String query = "SELECT 1 " +
				" FROM pessoa_empresa pe " +
				"		JOIN pessoa p " +
				"			ON pe.pessoa_id_INT = p.id " +
				"		JOIN empresa e " +
				"			ON pe.empresa_id_INT = e.id " +
				"		JOIN profissao po " +
				"			ON po.id = pe.profissao_id_INT" +
				"	WHERE pe.pessoa_id_INT = ? ";
		
		Long res = db.getResultSetAsLong(query, 0, new String[]{idPessoa}) ;
		if(res == null) return false;
		else return true;
	}
	public static Long getIdProfissaoDaPessoa(
			Database db , 
			String idPessoa, 
			String idEmpresa){
		//Vamos procurar uma profissao da pessoa na empresa, qualquer uma,
		//caso encontre, retorna ela
		String query = "";
		Long res = null;
		
		
		query = "SELECT profissao_id_INT " +
				" FROM pessoa_empresa pe " 
				+ "	WHERE pe.pessoa_id_INT = ? "
				+ "	AND pe.empresa_id_INT = ? "
				+ " LIMIT 0,1 ";
		
		res = db.getResultSetAsLong(query, 0, new String[]{idPessoa, idEmpresa}) ;
		if(res != null) return res;
		
		EXTDAOPessoaEmpresa obj = new EXTDAOPessoaEmpresa(db);
		obj.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, idPessoa);
		obj.setAttrValue(EXTDAOPessoaEmpresa.EMPRESA_ID_INT, idEmpresa);
		
		Long lidProfissao =  EXTDAOProfissao.getProfissaoindefinida(db);
		String idProfissao = lidProfissao != null ? lidProfissao.toString() : null;
	
		obj.setAttrValue(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT, idProfissao);
		obj.formatToSQLite();
		obj.insert(true);
		return lidProfissao;
	}
	public static Long getPessoaEmpresaParaBaterPontoDaPessoa(
			Database db , 
			String idPessoa, 
			String idEmpresa, 
			String idProfissao){
		//Vamos procurar uma profissao da pessoa na empresa, qualquer uma,
		//caso encontre, retorna ela
		String query = "";
		
		Long res = null;
		if(idProfissao != null){
			query = "SELECT id, profissao_id_INT " +
					" FROM pessoa_empresa pe " 
					+ "	WHERE pe.pessoa_id_INT = ? "
					+ "	AND pe.empresa_id_INT = ? "
					+ " AND pe.profissao_id_INT = ? "
					+ " LIMIT 0,1 ";
			
			res = db.getResultSetAsLong(query, 0, new String[]{idPessoa, idEmpresa, idProfissao}) ;
			if(res != null) return res;	
		}
		
		query = "SELECT id, profissao_id_INT " +
				" FROM pessoa_empresa pe " 
				+ "	WHERE pe.pessoa_id_INT = ? "
				+ "	AND pe.empresa_id_INT = ? "
				+ " LIMIT 0,1 ";
		
		res = db.getResultSetAsLong(query, 0, new String[]{idPessoa, idEmpresa}) ;
		if(res != null) return res;
		
		EXTDAOPessoaEmpresa obj = new EXTDAOPessoaEmpresa(db);
		obj.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, idPessoa);
		obj.setAttrValue(EXTDAOPessoaEmpresa.EMPRESA_ID_INT, idEmpresa);
		if(idProfissao == null){
			Long lidProfissao =  EXTDAOProfissao.getProfissaoindefinida(db);
			idProfissao = lidProfissao != null ? lidProfissao.toString() : null;
		}
		obj.setAttrValue(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT, idProfissao);
		obj.formatToSQLite();
		return obj.insert(true);
		
	}


	public static EntradaSaida getPessoaEmpresaParaBaterPontoDaPessoaWidget(
			Database db , 
			String idPessoa,
			String idCorporacao,
			String idUsuario){
		//Vamos procurar uma profissao da pessoa na empresa, qualquer uma,
		//caso encontre, retorna ela
		String query = "";
		
		Long[] res = null;
		
		
		query = "SELECT pe.id, pe.profissao_id_INT, pe.empresa_id_INT, p.nome, e.nome, pr.nome " +
				" FROM pessoa_empresa pe JOIN pessoa p ON pe.pessoa_id_INT = p.id"
				+ "	JOIN empresa e ON pe.empresa_id_INT = e.id"
				+ "	JOIN profissao pr ON pr.id = pe.profissao_id_INT  " 
				+ "	WHERE pe.pessoa_id_INT = ? "
				+ " LIMIT 0,1 ";
		Cursor cursor = null;
		try {
			cursor = db.rawQuery(query, new String[]{idPessoa});
			
			if (cursor.moveToFirst()) {
				EntradaSaida es= new EntradaSaida();
				es.idPessoa = idPessoa;
				es.idProfissao = cursor.isNull(1) ? null : String.valueOf( cursor.getLong(1));
				es.idEmpresa =cursor.isNull(2) ? null : String.valueOf(cursor.getLong(2));
				
				es.pessoa =cursor.isNull(3) ? null : cursor.getString(3);
				es.empresa =cursor.isNull(4) ? null : cursor.getString(4);
				es.profissao =cursor.isNull(5) ? null : cursor.getString(5);
				
				return es;
			}

		}finally{
			if(cursor != null ){
				cursor.close();
			}
		}
		
		
		EXTDAOPessoaEmpresa obj = new EXTDAOPessoaEmpresa(db);
		obj.setAttrValue(EXTDAOPessoaEmpresa.PESSOA_ID_INT, idPessoa);
		String idProfissao = EXTDAOProfissao.getProfissaoindefinida(db, idCorporacao, idUsuario).toString();
		obj.setAttrValue(EXTDAOPessoaEmpresa.PROFISSAO_ID_INT, idProfissao);
		String idEmpresa = EXTDAOEmpresa.getIdEmpresaIndefinida(db, idCorporacao, idUsuario).toString();
		obj.setAttrValue(EXTDAOPessoaEmpresa.EMPRESA_ID_INT, idEmpresa );
		obj.setAttrValue(EXTDAOPessoaEmpresa.CORPORACAO_ID_INT, idCorporacao);
		obj.formatToSQLite();
		Long idPE = obj.insert(true, idCorporacao, idUsuario);
		if(idPE == null) return null;
		
		EntradaSaida es= new EntradaSaida();
		es.idPessoa = idPessoa;
		es.idProfissao = idProfissao;
		es.idEmpresa = idEmpresa;
		
		es.pessoa = EXTDAOPessoa.getNomeDaPessoa(db, idPessoa);
		es.empresa =EXTDAOEmpresa.getNomeEmpresaIndefinida(db.getContext());
		es.profissao =EXTDAOProfissao.getNomeProfissaoIndefinida(db.getContext());
		
		
		return es;
	}

	
	
    } // classe: fim
    

    