

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOPessoaEmpresaRotina
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOPessoaEmpresaRotina.java
    * TABELA MYSQL:    pessoa_empresa_rotina
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.ArrayList;

import android.database.Cursor;
import android.util.Log;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.ResultSet;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoaEmpresaRotina;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.date.HelperDate;

	

    
        
    public class EXTDAOPessoaEmpresaRotina extends DAOPessoaEmpresaRotina
    {

    	public static String ERROR_SEU_USUARIO_NAO_ESTA_LINKADO_A_NENHUMA_PESSOA_DO_BANCO = "ERROR_SEU_USUARIO_NAO_ESTA_LINKADO_A_NENHUMA_PESSOA_DO_BANCO";
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOPessoaEmpresaRotina(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOPessoaEmpresaRotina(this.getDatabase());

    }
    

	public ResultSet getRegistrosDaPessoa(String idPessoa){
		String query = "SELECT DISTINCT per.semana_INT, " +
				"	per.dia_semana_INT, " +
				"	per.pessoa_empresa_id_INT," +
				"	e.nome " +
				" FROM pessoa_empresa_rotina per " +
				"		JOIN pessoa_empresa pe " +
				"			ON pe.id= per.pessoa_empresa_id_INT" +
				"		JOIN empresa e " +
				"			ON pe.empresa_id_INT = e.id " +
				"	WHERE pe.pessoa_id_INT = ? " +
				"	ORDER BY per.semana_INT, per.dia_semana_INT";
		Database db = getDatabase();
		return db.getResultSet(query, new String[]{idPessoa});
	}



	public ResultSet getRegistrosDaPessoa(String idPessoa, String semana, String dia){
		String query = "SELECT DISTINCT pe.id id " +
				" FROM pessoa_empresa_rotina per " +
				"		JOIN pessoa_empresa pe " +
				"			ON pe.id= per.pessoa_empresa_id_INT" +
				"	WHERE pe.pessoa_id_INT = ? " +
				"		AND semana_INT = ?" +
				"		AND dia_semana_INT = ?";
		Database db = getDatabase();
		return db.getResultSet(query, new String[]{idPessoa, semana, dia});
	}


	
	public ArrayList<Long> getIdListBySearchParameters(int pSemanaInt, int pDiaInt, String pPessoaInt){


		String querySelectIdPrestadores = "SELECT DISTINCT per."
				+ EXTDAOPessoaEmpresaRotina.ID + " ";

		String queryFromTables =  "FROM " + EXTDAOPessoaEmpresaRotina.NAME + " per JOIN " +
		EXTDAOPessoaEmpresa.NAME + " pe ON pe." + EXTDAOPessoaEmpresa.ID + " = per." + EXTDAOPessoaEmpresaRotina.PESSOA_EMPRESA_ID_INT;
		
		String queryWhereClausule = " per." + EXTDAOPessoaEmpresaRotina.SEMANA_INT + " = ? AND " +  
				" per." + EXTDAOPessoaEmpresaRotina.DIA_SEMANA_INT + " = ? AND " +
				" pe." + EXTDAOPessoaEmpresa.PESSOA_ID_INT + " = ? AND " +
				" pe." + EXTDAOPessoaEmpresa.CORPORACAO_ID_INT + " = ? ";
		
		ArrayList<String> args = new ArrayList<String>();
		args.add(String.valueOf(pSemanaInt));
		args.add(String.valueOf(pDiaInt));
		args.add(pPessoaInt);
		args.add(OmegaSecurity.getIdCorporacao());
		
		String query = querySelectIdPrestadores + queryFromTables;
		if(queryWhereClausule.length() > 0 )
			query 	+= " WHERE " + queryWhereClausule;

		String[] vetorArg = new String [args.size()];
		args.toArray(vetorArg);
		
		Cursor cursor = null;
		try{
			cursor = database.rawQuery(query, vetorArg);
			// Cursor cursor = oh.query(true, EXTDAOPessoaEmpresaRotina.NAME,
			// new String[]{EXTDAOEmpresa.ID_PRESTADOR}, "", new String[]{},
			// null, "", "", "");
			String[] vetorChave = new String[] { EXTDAOPessoaEmpresaRotina.ID };
			return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
		}finally{
			if(cursor != null && !cursor.isClosed()) cursor.close();
		}
	}

	public ArrayList<Long> getListaIdTuplaDoDiaDoCicloAtual() {
		EXTDAORotina vObjRotina = new EXTDAORotina(getDatabase());
		vObjRotina.setAttrValue(EXTDAORotina.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		EXTDAORotina vTuplaRotina = (EXTDAORotina) vObjRotina.getFirstTupla();
		EXTDAOPessoaUsuario vObjPU = new EXTDAOPessoaUsuario(getDatabase());
		String vIdPessoa = vObjPU.getIdPessoaDoUsuarioLogado();
		
		
		if(vIdPessoa == null)
			return null;
		
		//TODO tEM Q FAZER um select setando o pessoa do pessoa_empresA_id_INT
		if(vTuplaRotina != null){
			Integer vSemana = EXTDAORotina.getSemanaAtualInt();
			
			int vDiaSemana = HelperDate.getDiaSemana();
			if(vSemana != null){
				return getIdListBySearchParameters(vSemana, vDiaSemana, vIdPessoa );
				
			}else{
				Log.e(TAG, "Semana do ciclo invalido");
			}
		}
		return null;
	}
	
	
    } // classe: fim
    

    