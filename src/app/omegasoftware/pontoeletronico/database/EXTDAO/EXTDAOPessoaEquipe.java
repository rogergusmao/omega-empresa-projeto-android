

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOPessoaEquipe
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOPessoaEquipe.java
        * TABELA MYSQL:    pessoa_equipe
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoaEquipe;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOPessoaEquipe extends DAOPessoaEquipe
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOPessoaEquipe(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOPessoaEquipe(this.getDatabase());

        }
        

        } // classe: fim


        