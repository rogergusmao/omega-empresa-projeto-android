

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOPessoaUsuario
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOPessoaUsuario.java
    * TABELA MYSQL:    pessoa_usuario
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.HashMap;
import java.util.LinkedHashMap;

import android.database.Cursor;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.container.DadosPessoaUsuario;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOPessoaUsuario;

	

    
        
    public class EXTDAOPessoaUsuario extends DAOPessoaUsuario
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOPessoaUsuario(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOPessoaUsuario(this.getDatabase());

    }
    
	public String getIdPessoaDoUsuarioLogado(){
		
		this.setAttrValue(USUARIO_ID_INT, OmegaSecurity.getIdUsuario());
		Table vTupla = this.getFirstTupla();
		if(vTupla != null){
			return vTupla.getStrValueOfAttribute(PESSOA_ID_INT);
		} else return null;
	}
	
	
	public static HashMap<Long, DadosPessoaUsuario> getAllIdPessoaQuePossuiUsuario(Database db)
	{
		HashMap<Long, DadosPessoaUsuario> ids = new HashMap<Long, DadosPessoaUsuario>(); 

		String vQuery = "SELECT DISTINCT p.id, pu.usuario_id_INT, p.corporacao_id_INT, u.nome, u.email ";
		vQuery += " FROM pessoa p "
					+ " JOIN pessoa_usuario pu  ON p.id = pu.pessoa_id_INT"
					+ " JOIN usuario u ON pu.usuario_id_INT= u.id " ;
		
		Cursor cursor = null;
		try{
			cursor = db.rawQuery(vQuery,null);
			

			if (cursor.moveToFirst()) {

				do {				
					try{
						Long idCorporacao = cursor.getLong(2);
						String strIdCorporacao = idCorporacao.toString();
						Long idUsuario = cursor.getLong(1);
						
						if(idUsuario != null && idUsuario != 0 
								&& strIdCorporacao.equalsIgnoreCase(OmegaSecurity.getIdCorporacao())){
							DadosPessoaUsuario item = new DadosPessoaUsuario();
							item.emailUsuario = cursor.getString(4);
							item.nomeUsuario = cursor.getString(3);
							item.idCorporacao = idCorporacao;
							item.idUsuario = idUsuario;
							item.idPessoa = cursor.getLong(0);
							ids.put(cursor.getLong(0), item);
						} else {
							ids.put(cursor.getLong(0), null);	
						}
					}
					catch (Exception e) {
						SingletonLog.insereErro(e, TIPO.BANCO);
					}
				} while (cursor.moveToNext());
			}
		}finally{

			if (cursor != null && !cursor.isClosed()) {	    	  
				cursor.close();
			}
		}
		

		return ids;
	}
	
	public static LinkedHashMap<String, String> getAllPessoaUsuario(Database db)
	{
		
		String vQuery = "SELECT pu." + EXTDAOPessoaUsuario.ID + ", p." + EXTDAOPessoa.NOME ;
		vQuery += " FROM " + EXTDAOPessoa.NAME + " p JOIN " + EXTDAOPessoaUsuario.NAME + " pu ON p." + EXTDAOPessoa.ID + " = pu." + EXTDAOPessoaUsuario.PESSOA_ID_INT;
		LinkedHashMap<String, String> vHash = new LinkedHashMap<String, String>(); 
		
		Cursor cursor = null;
		try{
			cursor = db.rawQuery(vQuery, new String[]{});
			

			if (cursor.moveToFirst()) {

				do {				
					try{
						String vKey = cursor.getString(0);
						String vDesc = cursor.getString(1);
						if(vKey != null && vDesc != null){
							vHash.put(vKey, vDesc);
						}
					}
					catch (Exception e) {
					}
				} while (cursor.moveToNext());
			}

		}finally{
			if (cursor != null && !cursor.isClosed()) {	    	  
				cursor.close();
			}
		}
		
		return vHash;
	}
	
    } // classe: fim
    

    