

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOPonto
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOPonto.java
    * TABELA MYSQL:    ponto
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.widget.Toast;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.container.EntradaSaida;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.ResultSet;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.Tuple;
import app.omegasoftware.pontoeletronico.database.DAO.DAOPonto;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.primitivetype.HelperLong;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOPonto extends DAOPonto
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOPonto(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOPonto(this.getDatabase());

    }
    
    public class Ponto{
    	public String id;
    	public String data;
    	public String hora;
    	public Long utc;
    	public Date getObjData(){
    		Date objData;
    		HelperDate hd = new HelperDate(data + " " + hora);
    		objData = hd.getDate();
    		return objData;
    	}
    }
    
    public class PontoPessoa extends CustomItemList {
    	public String idPessoa, idEmpresa, idProfissao, idPessoaEmpresa;
    	public String pessoa, empresa, profissao;
    	public EntradaSaida intervalo;
    	public EntradaSaida turno;
    	public PontoPessoa(String idPessoaEmpresa){
    		super(idPessoaEmpresa);
    	}
    	
    	public boolean bateuTurno(){
    		return turno == null ? false : true;
    	}
    	
    	public boolean bateuIntervalo(){
    		return intervalo == null ? false : true;
    	}
    }
    public PontoPessoa getUltimoPontoDaPessoa(String idPessoa){
    	PontoPessoa obj = new PontoPessoa(null);
    	
    	obj.intervalo = getUltimaEntradaSaidaDaPessoa(idPessoa, EXTDAOTipoPonto.TIPO_PONTO.INTERVALO);
    	obj.turno = getUltimaEntradaSaidaDaPessoa(idPessoa, EXTDAOTipoPonto.TIPO_PONTO.TURNO);
    	return obj;
    }
    
    public PontoPessoa factoryPontoPessoa(String idPessoaEmpresa){
    	return new EXTDAOPonto.PontoPessoa(idPessoaEmpresa);
    }
    
    public PontoPessoa getUltimoPontoDaPessoaNaEmpresa(
    		String idPessoaEmpresa, String idPessoa, 
    		String idEmpresa, String idProfissao){
    	
    	PontoPessoa obj = new PontoPessoa(idPessoaEmpresa);
    	obj.idPessoa = idPessoa;
    	obj.idEmpresa = idEmpresa;
    	obj.idProfissao = idProfissao;
    	obj.idPessoaEmpresa = idPessoaEmpresa;
    	obj.intervalo = getUltimaEntradaSaidaDaPessoaNaEmpresa(idPessoa, 
    			idEmpresa, idProfissao, EXTDAOTipoPonto.TIPO_PONTO.INTERVALO);
    	obj.turno = getUltimaEntradaSaidaDaPessoaNaEmpresa(idPessoa,
    			idEmpresa, idProfissao, EXTDAOTipoPonto.TIPO_PONTO.TURNO);
    	
    	if(obj.intervalo == null && obj.turno == null) return null;
    	else return obj;
    }
    
    public Ponto getUltimoPonto(
			String idPessoa, 
			String idEmpresa,
			String idProfissao,
			EXTDAOTipoPonto.TIPO_PONTO tipo,
			boolean isEntrada){
		
    	Database db = getDatabase();
		String q = "SELECT " + EXTDAOPonto.ID 
				+ ", " + EXTDAOPonto.DATA_SEC 
				+ ", " + EXTDAOPonto.DATA_OFFSEC 
					+ " FROM " + EXTDAOPonto.NAME
					+ " WHERE " + EXTDAOPonto.TIPO_PONTO_ID_INT + " = ? " 
						+ "	AND " + EXTDAOPonto.PESSOA_ID_INT + " = ? "	
					+ "	AND " + EXTDAOPonto.EMPRESA_ID_INT + " = ? "
					+ "	AND " + EXTDAOPonto.IS_ENTRADA_BOOLEAN + " = ? ";
		String[] parametros = null;
		if(idProfissao != null){
			q += "	AND " + EXTDAOPonto.PROFISSAO_ID_INT + " = ? ";
			parametros = new String[]{
					String.valueOf(tipo.getId()),
					idPessoa,
					idEmpresa,
					HelperString.valueOfBooleanSQL(isEntrada),
					idProfissao
				};
		}
		else{
			q += "	AND " + EXTDAOPonto.PROFISSAO_ID_INT + " IS NULL ";
			parametros = new String[]{
					String.valueOf(tipo.getId()),
					idPessoa,
					idEmpresa,
					HelperString.valueOfBooleanSQL(isEntrada)
				};
		}
			q +="	ORDER BY " + EXTDAOPonto.DATA_SEC + " DESC "
					+ "	LIMIT 0,1 ";
		
		ResultSet rs = db.getResultSet(q, parametros);
		if(rs == null) return null;
		
		Ponto p = null;
		ArrayList<String[]> resultado = rs.getMatrizValorAtributo();
		
		if(resultado.size() > 0){
			
			String[] valores =resultado.get(0);
			p= new Ponto();
			p.id = valores[0];
			
			String sec = valores[1];
			Long secLong = HelperLong.parserLong(sec);
			if(secLong != null){
				Date d = HelperDate.getDateFromSecOfssec(secLong, null);
				
				p.data= HelperDate.getStringDate(db.getContext(), d);
				p.hora =  HelperDate.getStringTime(db.getContext(),d);	
			}
			
			return p;
		} else return null;
	}

	public EntradaSaida getUltimaEntradaSaidaDaPessoaNaEmpresa(
			String idPessoa, 
			String idEmpresa,
			String idProfissao,
			EXTDAOTipoPonto.TIPO_PONTO tipo){
		
    
		EntradaSaida ret = new EntradaSaida();
		
		ret = new EntradaSaida();
		ret.idPessoa = idPessoa;
		ret.entrada = getUltimoPonto(idPessoa, idEmpresa, idProfissao, tipo, true);
		ret.saida = getUltimoPonto(idPessoa, idEmpresa, idProfissao, tipo, false);
		if(ret.entrada == null && ret.saida == null)
			return null;
		if(ret.entrada != null){
			if(ret.saida != null ){
				Date dataEntrada = ret.entrada.getObjData();
				if(dataEntrada != null){
					
					Date dataSaida = ret.saida.getObjData();
					if(dataSaida != null){
						if(dataEntrada.compareTo(ret.saida.getObjData()) > 0){
							ret.saida = null;
						}	
					}	
				}
				
			}
		}
		return ret;
	}
	
	public EntradaSaida getUltimaEntradaSaidaDaPessoa(
			String idPessoa, 
			EXTDAOTipoPonto.TIPO_PONTO tipo){
		
    	Database db = getDatabase();
    	
    	Tuple<Long,Long> idEmpresaEProfissao = db.getTwoLongs(
    			"SELECT empresa_id_INT, profissao_id_INT "
    			+ "FROM ponto "
    			+ "WHERE pessoa_id_INT = ? "
    			+ "	AND tipo_ponto_id_INT = ? "
    			+ "	AND empresa_id_INT IS NOT NULL "
    			+ "ORDER BY data_sec DESC", new String[]{idPessoa, String.valueOf( tipo.id) });
    	if(idEmpresaEProfissao==null)
    		return null;
    	Long idEmpresa = idEmpresaEProfissao.x;
    	Long idProfissao = idEmpresaEProfissao.y;
		String q = "SELECT p.id "   
			 + ", p.is_entrada_BOOLEAN" 
			+ ", p.data_sec " 
			+ ", p.data_offsec "
			+ ", p.pessoa_id_INT"
			+ ", pe.nome"
			+ ", e.nome"
			+ ", pr.nome"
			+ " FROM ponto p "
			+ "		JOIN pessoa pe "
			+ "			ON p.pessoa_id_INT = pe.id"
			+ "		LEFT JOIN empresa e "
			+ "			ON p.empresa_id_INT = e.id "
			+ "		LEFT JOIN profissao pr "
			+ "			ON pr.id = p.profissao_id_INT "
			+ " WHERE p.tipo_ponto_id_INT = ? "
			+ "	AND p.pessoa_id_INT = ? "
			+ " AND p.empresa_id_INT = ? ";
		if(idProfissao != null){
			q+= " AND p.profissao_id_INT = ? ";
		} else {
			q+= " AND p.profissao_id_INT IS NULL";
		}
		q+= "	ORDER BY p.data_sec DESC "  
				+ "	LIMIT 0,2 ";
		
		String[] parametros = null;
		if(idProfissao != null){
			parametros =new String[]{
					String.valueOf(tipo.getId()),
					idPessoa,
					idEmpresa.toString(),
					idProfissao.toString()
				};
		} else {
			parametros =new String[]{
					String.valueOf(tipo.getId()),
					idPessoa,
					idEmpresa.toString()
				};
		}
		
		ResultSet rs = db.getResultSet(q, parametros);
		if(rs == null) return null;
		EntradaSaida ret = new EntradaSaida();
		ret.idPessoa = new String(idPessoa);
		
		ArrayList<String[]> resultado = rs.getMatrizValorAtributo();
		if(resultado.size() > 0){
			ret = new EntradaSaida();
			String[] valores =resultado.get(0);
			ret.idEmpresa = idEmpresa.toString();
			ret.idProfissao= idProfissao != null ? idProfissao.toString() : null;
			ret.pessoa = valores[5];
			ret.empresa = valores[6];
			ret.profissao= valores[7];
			ret.idPessoa = new String(idPessoa);
			Long utc = HelperLong.parserLong(valores[2]);
			if(utc == null)return null;
			String[] dataETime = HelperDate.getStringDateETimeFromUtcTimestamp(db.getContext(),utc);
			//se for entrada
			if(valores[1].equals("1")){
				
				ret.entrada= new Ponto();
				ret.entrada.id = valores[0];
				ret.entrada.data= dataETime[0];
				ret.entrada.hora = dataETime[1];
				ret.entrada.utc = utc;
			} else {
				ret.saida= new Ponto();
				ret.saida.id = valores[0];
				ret.saida.data= dataETime[0];
				ret.saida.hora = dataETime[1];
				ret.saida.utc = utc;
			}

			if(resultado.size() > 1){
				valores = resultado.get(1);
				
				utc = HelperLong.parserLong(valores[2]);
				if(utc == null)return null;
				dataETime = HelperDate.getStringDateETimeFromUtcTimestamp(db.getContext(),utc);
				if(valores[1].compareTo("1") == 0 && ret.saida != null){
					ret.entrada = new Ponto();
					ret.entrada.id = valores[0];
					ret.entrada.data = dataETime[0];
					ret.entrada.hora = dataETime[1];
					ret.entrada.utc=utc;
				} else if(ret.entrada!= null && ret.entrada.utc <= utc){
					ret.saida = new Ponto();
					ret.saida.id = valores[0];
					ret.saida.data = dataETime[0];
					ret.saida.hora = dataETime[1];
					ret.saida.utc=utc;
				}
			}
			return ret;
		} else return null;
	}

	public static void factoryToastPontoBatido(Context c){
		Toast.makeText(c, c.getString(R.string.ponto_batido), 2).show();
	}
	
	public static String getTextoDescritivo(Context c, final boolean entrada, String pessoa, String empresa) {
		try {

			String vEntradaOuSaida = "";
			if (entrada)
				vEntradaOuSaida = c.getResources().getString(R.string.ponto_entrada);
			else
				vEntradaOuSaida = c.getResources().getString(R.string.ponto_saida);
			
			return vEntradaOuSaida.toUpperCase() + " - " + pessoa.toUpperCase() + " - "+empresa.toUpperCase();

		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);

		}
		return null;
	}
	
    } // classe: fim
    

    