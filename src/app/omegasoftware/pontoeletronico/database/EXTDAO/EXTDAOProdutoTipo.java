

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOProdutoTipo
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOProdutoTipo.java
        * TABELA MYSQL:    produto_tipo
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOProdutoTipo;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOProdutoTipo extends DAOProdutoTipo
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOProdutoTipo(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOProdutoTipo(this.getDatabase());

        }
        

        } // classe: fim


        