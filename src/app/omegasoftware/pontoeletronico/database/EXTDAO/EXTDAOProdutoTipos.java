

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOProdutoTipos
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOProdutoTipos.java
        * TABELA MYSQL:    produto_tipos
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOProdutoTipos;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOProdutoTipos extends DAOProdutoTipos
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOProdutoTipos(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOProdutoTipos(this.getDatabase());

        }
        

        } // classe: fim


        