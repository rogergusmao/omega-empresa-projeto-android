

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOProdutoUnidadeMedida
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOProdutoUnidadeMedida.java
        * TABELA MYSQL:    produto_unidade_medida
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOProdutoUnidadeMedida;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOProdutoUnidadeMedida extends DAOProdutoUnidadeMedida
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOProdutoUnidadeMedida(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOProdutoUnidadeMedida(this.getDatabase());

        }
        

        } // classe: fim


        