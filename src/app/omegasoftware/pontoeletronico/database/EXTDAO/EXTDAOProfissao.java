

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOProfissao
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOProfissao.java
    * TABELA MYSQL:    profissao
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.LinkedHashMap;

import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOProfissao;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

	

    
        
    public class EXTDAOProfissao extends DAOProfissao
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOProfissao(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOProfissao(this.getDatabase());

    }
    

	public LinkedHashMap<String, String> getAllEspecialidades()
	{
		this.clearData();
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		
		for(Table iEntry : this.getListTable(new String[]{EXTDAOProfissao.NOME}))
		{
			EXTDAOProfissao especialidade = (EXTDAOProfissao) iEntry;
			hashTable.put(especialidade.getAttribute(EXTDAOProfissao.ID).getStrValue(), 
					especialidade.getAttribute(EXTDAOProfissao.NOME).getStrValue().toUpperCase());
		}
		
		return hashTable;
	}

	public static void randomPopulate(){
		Database db=null;
		try {
			db = new DatabasePontoEletronico(null);
		
			EXTDAOProfissao obj = new EXTDAOProfissao(db);
			for(int i = 10000; i < 10500; i++){
				String nome = "profissao_android_" + String.valueOf(i);
				obj.clearData();
				obj.setAttrValue(EXTDAOProfissao.NOME, nome);
				db.setPragmaOn();
				obj.formatToSQLite();
				Long j = obj.insert(true);
				if(j == null )
					Log.e(TAG, "Falha ao inserir: " + nome);
				
			}
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String adicionarRegistroSeInexistente(String profissao){
		this.clearData();
		this.setAttrValue(NOME, profissao);
		Table registro = this.getFirstTupla();
		
		if(registro != null){
			return registro.getId();
		} else {
			this.formatToSQLite();
			Long id = this.insert(true);
			if(id != null)
				return String.valueOf(id);
			else return null;
		}
	}
	public static String getNomeProfissaoIndefinida(Context c){
		return c.getString(R.string.profissao_indefinida).toUpperCase();
	}
	public static Long getProfissaoindefinida(Database db){
		return getProfissaoindefinida(db, OmegaSecurity.getIdCorporacao(), OmegaSecurity.getIdUsuario());
	}
	public static Long getProfissaoindefinida(Database db, String idCorporacao, String idUsuario){
		
		String  token =db.getContext().getString(R.string.profissao_indefinida).toUpperCase();
		Long id = db.getFirstLong("SELECT id FROM profissao WHERE nome_normalizado = ? AND corporacao_id_INT = ? ", 
				new String[]{HelperString.getWordWithoutAccent(token), idCorporacao});
		if(id!=null)return id;
		
		EXTDAOProfissao objProfissao = new EXTDAOProfissao(db);
		objProfissao.setAttrValue(EXTDAOProfissao.NOME, token);
		objProfissao.formatToSQLite();
		objProfissao.setAttrValue(EXTDAOProfissao.CORPORACAO_ID_INT, idCorporacao);
		id = objProfissao.insert(true, idCorporacao, idUsuario);
		return id;
	}
	
	
	
    } // classe: fim
    

    