

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAORecebivelCotidiano
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAORecebivelCotidiano.java
        * TABELA MYSQL:    recebivel_cotidiano
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAORecebivelCotidiano;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAORecebivelCotidiano extends DAORecebivelCotidiano
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAORecebivelCotidiano(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAORecebivelCotidiano(this.getDatabase());

        }
        

        } // classe: fim


        