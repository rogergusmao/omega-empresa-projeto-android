

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAORede
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAORede.java
    * TABELA MYSQL:    rede
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAORede;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAORede extends DAORede
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAORede(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAORede(this.getDatabase());

    }
    
    public long getTotalFuncionarioNoIntervalo(){
    	
    	String q = "SELECT COUNT(pe.pessoa_id_INT)" +
    			"	FROM rede r " +
    			"		JOIN rede_empresa re " +
    			"			ON r.id = re.rede_id_INT " +
    			"		JOIN empresa e" +
    			"			ON	e.id = re.empresa_id_INT " +
    			"		JOIN pessoa_empresa pe" +
    			"			ON pe.empresa_id_INT = e.id" + 
    			"	WHERE r.id = ? ";
    	
    	return getDatabase().getResultSetAsLong(q, 0, new String[]{this.getId()});
    	
    }
    
    
    public long getTotalFuncionarios(){
    	
    	String q = "SELECT COUNT(pe.pessoa_id_INT)" +
    			"	FROM rede r " +
    			"		JOIN rede_empresa re " +
    			"			ON r.id = re.rede_id_INT " +
    			"		JOIN empresa e" +
    			"			ON	e.id = re.empresa_id_INT " +
    			"		JOIN pessoa_empresa pe" +
    			"			ON pe.empresa_id_INT = e.id" + 
    			"	WHERE r.id = ? ";
    	
    	return getDatabase().getResultSetAsLong(q, 0, new String[]{this.getId()});
    	
    }
    
    
    
    

    } // classe: fim
    

    