

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAORedeEmpresa
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAORedeEmpresa.java
    * TABELA MYSQL:    rede_empresa
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAORedeEmpresa;

	

    
        
    public class EXTDAORedeEmpresa extends DAORedeEmpresa
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAORedeEmpresa(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAORedeEmpresa(this.getDatabase());

    }
    

    } // classe: fim
    

    