

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAORegistroEstado
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAORegistroEstado.java
        * TABELA MYSQL:    registro_estado
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstado;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAORegistroEstado extends DAORegistroEstado
        {
        public enum TIPO{
                aguardando_inicializacao(1, EXTDAOTipoRegistro.TIPO.tarefa),
                em_execucao(2, EXTDAOTipoRegistro.TIPO.tarefa),
                finalizada(3, EXTDAOTipoRegistro.TIPO.tarefa),
                cancelada(4, EXTDAOTipoRegistro.TIPO.tarefa),
                pausada(5, EXTDAOTipoRegistro.TIPO.tarefa);

            int id;
            EXTDAOTipoRegistro.TIPO tr;
            TIPO(int id, EXTDAOTipoRegistro.TIPO tr){this.id = id;this.tr=tr;}

            public int getId(){return id;}
            public  EXTDAOTipoRegistro.TIPO getTipo(){return this.tr;};

            public static EXTDAORegistroEstado.TIPO toTipoRegistroEstado(int id){
                EXTDAORegistroEstado.TIPO[] tipos = EXTDAORegistroEstado.TIPO.values();
                for(int i = 0; i < tipos.length; i++){
                    if(tipos[i].getId()==id)return tipos[i];
                }
                return null;
            }
        }

        public static void inserirRegistrosSeNecessario(Database db){


            EXTDAORegistroEstado objRE = new EXTDAORegistroEstado(db);
            TIPO[] ts = TIPO.values();
            for(int i = 0 ; i < ts.length; i++){
                TIPO t = ts[i];
                if(db.getResultSetAsContains("SELECT 1 FROM registro_estado WHERE id = ? ", new String[]{String.valueOf( t.getId())}))
                    continue;
                objRE.setIntAttrValue(EXTDAORegistroEstado.ID, t.id);
                objRE.setAttrValue(EXTDAORegistroEstado.NOME, t.toString());
                objRE.setIntAttrValue(EXTDAORegistroEstado.TIPO_REGISTRO_ID_INT, t.getTipo().getId());
                objRE.formatToSQLite();

                objRE.insert(false);
            }
        }

        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAORegistroEstado(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAORegistroEstado(this.getDatabase());

        }
        

        } // classe: fim


        