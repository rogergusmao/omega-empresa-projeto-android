

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAORegistroEstadoCorporacao
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAORegistroEstadoCorporacao.java
        * TABELA MYSQL:    registro_estado_corporacao
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroEstadoCorporacao;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAORegistroEstadoCorporacao extends DAORegistroEstadoCorporacao
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAORegistroEstadoCorporacao(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAORegistroEstadoCorporacao(this.getDatabase());

        }
        

        } // classe: fim


        