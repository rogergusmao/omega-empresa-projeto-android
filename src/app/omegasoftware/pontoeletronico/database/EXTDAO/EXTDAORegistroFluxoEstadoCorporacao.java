

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAORegistroFluxoEstadoCorporacao
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAORegistroFluxoEstadoCorporacao.java
        * TABELA MYSQL:    registro_fluxo_estado_corporacao
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAORegistroFluxoEstadoCorporacao;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAORegistroFluxoEstadoCorporacao extends DAORegistroFluxoEstadoCorporacao
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAORegistroFluxoEstadoCorporacao(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAORegistroFluxoEstadoCorporacao(this.getDatabase());

        }
        

        } // classe: fim


        