

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAORelatorio
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAORelatorio.java
    * TABELA MYSQL:    relatorio
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.LinkedHashMap;

import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAORelatorio;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoAnexo.TIPO_ANEXO;

	

    
        
    public class EXTDAORelatorio extends DAORelatorio
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAORelatorio(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAORelatorio(this.getDatabase());

    }

	public class ContainerContem{
		public boolean contemFoto;
		public boolean contemVideo;
		public boolean contemAudio;
		public boolean contemAnexo;
		
		public ContainerContem(Database db, String pId){
			EXTDAORelatorioAnexo vObjRA = new EXTDAORelatorioAnexo(db);
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pId);
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(TIPO_ANEXO.ANEXO));
			contemAnexo = vObjRA.hasTupla();
			
			vObjRA.clearData();
			
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pId);
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(TIPO_ANEXO.AUDIO));
			contemAudio = vObjRA.hasTupla();
			
			vObjRA.clearData();
			
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pId);
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(TIPO_ANEXO.VIDEO));
			contemVideo = vObjRA.hasTupla();
			
			vObjRA.clearData();
			
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pId);
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(TIPO_ANEXO.FOTO));
			contemFoto = vObjRA.hasTupla();
			
		}
	}
	
	public class ContainerContador{
		public Integer contadorFoto;
		public Integer contadorVideo;
		public Integer contadorAudio;
		public Integer contadorAnexo;
		
		public ContainerContador(Database db, String pId){
			EXTDAORelatorioAnexo vObjRA = new EXTDAORelatorioAnexo(db);
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pId);
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(TIPO_ANEXO.ANEXO));
			contadorAnexo = vObjRA.countTupla();
			
			vObjRA.clearData();
			
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pId);
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(TIPO_ANEXO.AUDIO));
			contadorAudio = vObjRA.countTupla();
			
			vObjRA.clearData();
			
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pId);
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(TIPO_ANEXO.VIDEO));
			contadorVideo = vObjRA.countTupla();
			
			vObjRA.clearData();
			
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.RELATORIO_ID_INT, pId);
			vObjRA.setAttrValue(EXTDAORelatorioAnexo.TIPO_ANEXO_ID_INT, EXTDAOTipoAnexo.getIdTipoAnexo(TIPO_ANEXO.FOTO));
			contadorFoto = vObjRA.countTupla();
			
		}
	}
	public ContainerContador getContainerContador(){
		return new ContainerContador(this.getDatabase(), this.getStrValueOfAttribute(EXTDAORelatorio.ID));
	}
	
	public ContainerContem getContainerContem(){
		return new ContainerContem(this.getDatabase(), this.getStrValueOfAttribute(EXTDAORelatorio.ID));
	}
	
	public static LinkedHashMap<String, String> getAllPessoa(Database db)
	{
		EXTDAORelatorio vObj = new EXTDAORelatorio(db);
		return vObj.getHashMapIdByDefinition(true);
	}


    } // classe: fim
    

    