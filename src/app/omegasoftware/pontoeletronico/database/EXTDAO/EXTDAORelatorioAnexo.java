

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAORelatorioAnexo
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAORelatorioAnexo.java
    * TABELA MYSQL:    relatorio_anexo
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAORelatorioAnexo;

	

    
        
    public class EXTDAORelatorioAnexo extends DAORelatorioAnexo
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAORelatorioAnexo(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAORelatorioAnexo(this.getDatabase());

    }
    

    } // classe: fim
    

    