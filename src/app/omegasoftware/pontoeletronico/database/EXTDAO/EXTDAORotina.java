

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAORotina
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAORotina.java
    * TABELA MYSQL:    rotina
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.Date;
import java.util.GregorianCalendar;

import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAORotina;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.date.HelperDate;

	

    
        
    public class EXTDAORotina extends DAORotina
    {
    	//Domingo
    	public static String DIA_INICIAL_DA_ROTINA = "2014-06-01";
    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAORotina(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAORotina(this.getDatabase());

    }
    final static HelperDate dataInicioRotina = new HelperDate(DIA_INICIAL_DA_ROTINA);
    public static Integer parseSemana(GregorianCalendar dataDesejada){
    	
		
		int vCicloAtual = 0;
		
		
		long vDiferencaDias = dataInicioRotina.getAbsDiferencaEmDias(dataDesejada);
		if(vDiferencaDias > 0 ){
			vCicloAtual += (((int)vDiferencaDias/7) % 4) + 1;
		} 
		return vCicloAtual;
    }
    public static Integer parseSemana(Date dataDesejada){
    	
		
		int vCicloAtual = 0;
		
		
		long vDiferencaDias = dataInicioRotina.getAbsDiferencaEmDias(dataDesejada);
		if(vDiferencaDias > 0 ){
			vCicloAtual += (((int)vDiferencaDias/7) % 4) + 1;
		} 
		return vCicloAtual;
    }
    
	public static Integer getSemanaAtualInt(){
		
//		String vUltimoDiaContagemCiclo = this.getStrValueOfAttribute(DAORotina.ULTIMA_DIA_CONTAGEM_CICLO_DATE);
		
		//pega a data atual do ciclo
		return parseSemana(new Date()); 
	}	

	
	public static Integer getDrawableLayoutDaSemana(int semana){
		switch (semana) {
		case 1:
			return R.drawable.ui_button_blue;
		case 2:
			return R.drawable.ui_button_orange;
		case 3:
			return R.drawable.ui_button_green;	
		case 4:
			return R.drawable.ui_button_lilac;
			
		default:
			return null;
		}
	}
	
    } // classe: fim
    

    