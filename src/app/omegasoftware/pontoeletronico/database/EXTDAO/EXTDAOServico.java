

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOServico
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOServico.java
    * TABELA MYSQL:    servico
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOServico;

	

    
        
    public class EXTDAOServico extends DAOServico
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOServico(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOServico(this.getDatabase());

    }
    

    } // classe: fim
    

    