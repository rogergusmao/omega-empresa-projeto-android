

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSexo
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSexo.java
    * TABELA MYSQL:    sexo
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOSexo;

	

    
        
    public class EXTDAOSexo extends DAOSexo
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSexo(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSexo(this.getDatabase());

    }

	public String getSexDescription() throws Exception
	{
		EXTDAOSexo vSex = new EXTDAOSexo(this.getDatabase());
		vSex.setAttrValue(EXTDAOSexo.ID, this.getAttribute(EXTDAOSexo.ID).getStrValue());
		vSex.select();		
		return vSex.getAttribute(EXTDAOSexo.NOME).getStrValue();
	}


    } // classe: fim
    

    