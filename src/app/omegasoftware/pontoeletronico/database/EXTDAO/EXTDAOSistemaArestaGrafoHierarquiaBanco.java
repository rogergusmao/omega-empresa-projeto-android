

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaArestaGrafoHierarquiaBanco
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSistemaArestaGrafoHierarquiaBanco.java
    * TABELA MYSQL:    sistema_aresta_grafo_hierarquia_banco
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaArestaGrafoHierarquiaBanco;

	

    
        
    public class EXTDAOSistemaArestaGrafoHierarquiaBanco extends DAOSistemaArestaGrafoHierarquiaBanco
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaArestaGrafoHierarquiaBanco(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaArestaGrafoHierarquiaBanco(this.getDatabase());

    }
    

    } // classe: fim
    

    