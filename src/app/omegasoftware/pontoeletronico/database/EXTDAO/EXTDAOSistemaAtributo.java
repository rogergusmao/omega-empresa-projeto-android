/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:  EXTDAOSistemaAtributo
 * DATA DE GERAuuO: 23.07.2014
 * ARQUIVO:         EXTDAOSistemaAtributo.java
 * TABELA MYSQL:    sistema_atributo
 * BANCO DE DADOS:  
 * -------------------------------------------------------
 *
 */

package app.omegasoftware.pontoeletronico.database.EXTDAO;

import java.util.ArrayList;

import android.database.Cursor;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaAtributo;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

// **********************
// DECLARAuuO DA CLASSE
// **********************

public class EXTDAOSistemaAtributo extends DAOSistemaAtributo {

	// *************************
	// DECLARAuuO DE ATRIBUTOS
	// *************************

	// *************************
	// CONSTRUTOR
	// *************************
	public EXTDAOSistemaAtributo(Database database) {
		super(database);

	}

	// *************************
	// FACTORY
	// *************************
	public Table factory() {
		return new EXTDAOSistemaAtributo(this.getDatabase());

	}

	public static ArrayList<String[]> getAtributosQueApontamParaATabela(
			Database db, String pIdSistemaTabela, OmegaLog log) {

		try {
			ArrayList<String[]> registros = new ArrayList<String[]>();
			Cursor cursor = null;
			// "SELECT id, nome, atributo_fk, update_tipo_fk, delete_tipo_fk, fk_sistema_tabela_id_INT "
			// . " FROM sistema_atributo"
			// . " WHERE fk_sistema_tabela_id_INT = '{$idSistemaTabela}' "
			// . "     AND fk_sistema_tabela_id_INT IS NOT NULL  "
			String select[] = new String[] { EXTDAOSistemaAtributo.ID,
					EXTDAOSistemaAtributo.NOME,
					EXTDAOSistemaAtributo.SISTEMA_TABELA_ID_INT,
					EXTDAOSistemaAtributo.UPDATE_TIPO_FK,
					EXTDAOSistemaAtributo.DELETE_TIPO_FK };
			String strSelect = HelperString.getStrSeparateByDelimiterColumn(
					select, ",");
			
			String query = "SELECT " + strSelect + " FROM "
					+ EXTDAOSistemaAtributo.NAME + " WHERE "
					+ EXTDAOSistemaAtributo.FK_SISTEMA_TABELA_ID_INT  + " = ? ";
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("getAtributosQueApontamParaATabela: " + query + ". Sendo idSistemaTabela: " + pIdSistemaTabela);
			
			try {

				cursor = db.rawQuery(query, new String[] { pIdSistemaTabela });

				if (cursor.moveToFirst()) {
					int cont = 0;
					do {
						cont++;
						String[] valores = new String[select.length];
						
						for (int i = 0; i < valores.length; i++)
							valores[i] = cursor.getString(i);
						registros.add(valores);
					} while (cursor.moveToNext());
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Encontrou atributos dependentes: "+ cont);
				} else if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
					
					log.escreveLog("Nuo encontrou nenhum atributo dependente");
				}
			} catch (Exception ex) {
				SingletonLog.insereErro(ex, app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO.BANCO);
			} finally{

				if (cursor != null && !cursor.isClosed()) {
					cursor.close();
				}				
			}

			return registros;
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO.BANCO);
		}
		return null;
	}

	// Atualiza as fks pessoa_id_INT que apontam para pessoa(1) em pessoa(2)
	public static InterfaceMensagem atualizaChavesFkDasTabelasDependentesParaONovoPai(
			Database db, Table objPaiAtual, String idPaiNovo, OmegaLog log) {
		try{
		if(objPaiAtual.getId().compareTo(idPaiNovo) == 0)
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("atualizaChavesFkDasTabelasDependentesParaONovoPai: " + objPaiAtual.getIdentificadorRegistro() + ". Id Pai Novo: " + idPaiNovo);
		String idSistemaTabela = EXTDAOSistemaTabela.getIdSistemaTabela(
				db, objPaiAtual.getName());
		ArrayList<String[]> atributosDependentes = EXTDAOSistemaAtributo
				.getAtributosQueApontamParaATabela(db, idSistemaTabela, log);
		
		if (atributosDependentes != null && atributosDependentes.size() > 0) {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Atualizando os ids antigos de atributos dependentes da tabela " 
						+ objPaiAtual.getIdentificadorRegistro() + ". Seus atributos dependentes suo: "
						+ HelperString.arrayToString(atributosDependentes, null,null));
			for (int l = 0; l < atributosDependentes.size(); l++) {
				String[] dadosDependente = atributosDependentes.get(l);
				String idSistemaTabelaDependente = dadosDependente[2];
				String tabelaDependente = EXTDAOSistemaTabela
						.getNomeSistemaTabela(db, idSistemaTabelaDependente);
				Table objDependente = db.factoryTable(tabelaDependente);
				if (objDependente == null) {
					return new Mensagem(
							PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
							"Nuo foi possuvel construir a tabela: "
									+ tabelaDependente);
				}
				String atributoDependente = dadosDependente[1];
				objDependente.setAttrValue(atributoDependente,
						objPaiAtual.getId());
				ArrayList<Table> registrosDependentes = objDependente
						.getListTable();
				if (registrosDependentes != null && registrosDependentes.size()>0) {
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Pai atual possui "+registrosDependentes.size()+" registros dependentes. Segue os dados do pai atual: " 
							+objPaiAtual.getIdentificadorRegistro());
					// Para todo registro dependente que possui chave
					// extrangeira para o registro atualmente modificado.
					for (int m = 0; m < registrosDependentes.size(); m++) {
						Table regDependente = registrosDependentes.get(m);
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Registro Dependente antes da atualizacao: "
								+regDependente.getIdentificadorRegistro());
						regDependente.setAttrValue(
								atributoDependente, idPaiNovo);
						boolean validade = regDependente.update(true);
						if (!validade) {
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
								Table teste = regDependente.factory();
								teste.select(regDependente.getId());
									
								log.escreveLog("Falha ao atualizar o atributo " 
									+ atributoDependente
									+ " logo seru removido. PAI: "
									+objPaiAtual.getId()
									+". DEPENDENTE: " 
									+teste.getIdentificadorRegistro());
							}
							
							try{
								registrosDependentes.get(m).remove(true);
							} catch(Exception ex){
								SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
							}
						} else {
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
								Table teste = regDependente.factory();
								teste.select(regDependente.getId());
								log.escreveLog("Registro dependente editado com sucesso, seus novos valores suo: "+teste.getIdentificadorRegistro());
							}
							
						}
					}
				}
			}
		} else {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("A tabela" + objPaiAtual.getName() + " nuo possui atributos que apontam para ela.");
		}

		return new Mensagem(
				PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
		}catch(Exception ex){
			return new Mensagem(ex);

		}
	}

	// public static function getAtributosFkDaTabela($idSistemaTabela, $db){
	// $q =
	// "SELECT id, nome, atributo_fk, update_tipo_fk, delete_tipo_fk, fk_sistema_tabela_id_INT "
	// . " FROM sistema_atributo"
	// . " WHERE sistema_tabela_id_INT = '{$idSistemaTabela}' "
	// . "     AND fk_sistema_tabela_id_INT IS NOT NULL  ";
	// $db->queryMensagem($q);
	// return Helper::getResultSetToMatriz($db->result);
	// }

} // classe: fim

