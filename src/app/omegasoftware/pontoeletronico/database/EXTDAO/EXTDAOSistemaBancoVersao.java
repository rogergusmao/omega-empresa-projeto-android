

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaBancoVersao
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSistemaBancoVersao.java
    * TABELA MYSQL:    sistema_banco_versao
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaBancoVersao;

	

    
        
    public class EXTDAOSistemaBancoVersao extends DAOSistemaBancoVersao
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaBancoVersao(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaBancoVersao(this.getDatabase());

    }
    

    } // classe: fim
    

    