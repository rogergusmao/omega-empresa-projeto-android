

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaCorporacaoSincronizador
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSistemaCorporacaoSincronizador.java
    * TABELA MYSQL:    sistema_corporacao_sincronizador
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaCorporacaoSincronizador;

	

    
        
    public class EXTDAOSistemaCorporacaoSincronizador extends DAOSistemaCorporacaoSincronizador
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************

    	public enum STATE_SINCRONIZADOR {BANCO_AINDA_NAO_CRIADO, COMPLETO, INCOMPLETO, NAO_REALIZADO, RESETAR};
    	
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaCorporacaoSincronizador(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaCorporacaoSincronizador(this.getDatabase());

    }
    

    } // classe: fim
    

    