

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaCorporacaoSincronizadorTabela
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSistemaCorporacaoSincronizadorTabela.java
    * TABELA MYSQL:    sistema_corporacao_sincronizador_tabela
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaCorporacaoSincronizadorTabela;

	

    
        
    public class EXTDAOSistemaCorporacaoSincronizadorTabela extends DAOSistemaCorporacaoSincronizadorTabela
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaCorporacaoSincronizadorTabela(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaCorporacaoSincronizadorTabela(this.getDatabase());

    }
    

    } // classe: fim
    

    