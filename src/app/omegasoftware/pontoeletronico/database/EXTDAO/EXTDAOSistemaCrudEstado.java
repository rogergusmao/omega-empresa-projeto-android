
/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:  EXTDAOSistemaCrudEstado
 * DATA DE GERAuuO: 08.01.2016
 * ARQUIVO:         EXTDAOSistemaCrudEstado.java
 * TABELA MYSQL:    sistema_crud_estado
 * BANCO DE DADOS:  
 * -------------------------------------------------------
 *
 */

package app.omegasoftware.pontoeletronico.database.EXTDAO;

import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaCrudEstado;

// **********************
// DECLARAuuO DA CLASSE
// **********************

public class EXTDAOSistemaCrudEstado extends DAOSistemaCrudEstado {

	// *************************
	// DECLARAuuO DE ATRIBUTOS
	// *************************

	// *************************
	// CONSTRUTOR
	// *************************
	public EXTDAOSistemaCrudEstado(Database database) {
		super(database);

	}

	// *************************
	// FACTORY
	// *************************
	public Table factory() {
		return new EXTDAOSistemaCrudEstado(this.getDatabase());

	}


	public Integer getIdEstadoCrud(Long idCrud, Long idCrudMobile) throws Exception {
		if(!selectCrud(
				idCrud,
				idCrudMobile))
			return null;
		else
			return getIntegerValueOfAttribute(EXTDAOSistemaCrudEstado.ID_ESTADO_CRUD_INT);
	}
	public boolean selectCrud(Long idCrud, Long idCrudMobile) throws Exception {
		return selectCrudDoTipoOperacao(
				idCrud,
				idCrudMobile,
				null);
	}
//
//	public boolean selectCrudEdicao(Long idCrud, Long idCrudMobile) throws Exception {
//		return selectCrudDoTipoOperacao(
//				idCrud,
//				idCrudMobile,
//				EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT);
//	}
//
//	public boolean selectCrudInsert(Long idCrud, Long idCrudMobile) throws Exception {
//		return selectCrudDoTipoOperacao(
//				idCrud,
//				idCrudMobile,
//				EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT);
//	}
//
//	public boolean selectCrudRemove(Long idCrud, Long idCrudMobile) throws Exception {
//		return selectCrudDoTipoOperacao(
//				idCrud, 
//				idCrudMobile,
//				EXTDAOSistemaTipoOperacaoBanco.TIPO.REMOVE);
//	}

	private boolean selectCrudDoTipoOperacao(
			Long idCrud, 
			Long idCrudMobile,
			EXTDAOSistemaTipoOperacaoBanco.TIPO tipo) throws Exception {
		clearData();
		
		if(idCrud != null){
			setLongAttrValue(EXTDAOSistemaCrudEstado.ID_CRUD_INT, idCrud);
	
		} else {
			setLongAttrValue(EXTDAOSistemaCrudEstado.ID_CRUD_MOBILE_INT, idCrudMobile);
		}
		if(tipo != null)
			setIntegerAttrValue(
					EXTDAOSistemaCrudEstado.SISTEMA_TIPO_OPERACAO_BANCO_ID_INT,
					tipo.getId());
		
		return selectFirstTupla();
	}
	

	


} // classe: fim

