

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaDeletarArquivo
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSistemaDeletarArquivo.java
    * TABELA MYSQL:    sistema_deletar_arquivo
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaDeletarArquivo;

	

    
        
    public class EXTDAOSistemaDeletarArquivo extends DAOSistemaDeletarArquivo
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaDeletarArquivo(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaDeletarArquivo(this.getDatabase());

    }
    

    } // classe: fim
    

    