

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaGrafoHierarquiaBanco
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSistemaGrafoHierarquiaBanco.java
    * TABELA MYSQL:    sistema_grafo_hierarquia_banco
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaGrafoHierarquiaBanco;

	

    
        
    public class EXTDAOSistemaGrafoHierarquiaBanco extends DAOSistemaGrafoHierarquiaBanco
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaGrafoHierarquiaBanco(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaGrafoHierarquiaBanco(this.getDatabase());

    }
    

    } // classe: fim
    

    