/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:  EXTDAOSistemaHistoricoSincronizador
 * DATA DE GERAuuO: 02.12.2014
 * ARQUIVO:         EXTDAOSistemaHistoricoSincronizador.java
 * TABELA MYSQL:    sistema_historico_sincronizador
 * BANCO DE DADOS:  
 * -------------------------------------------------------
 *
 */

package app.omegasoftware.pontoeletronico.database.EXTDAO;

import java.util.ArrayList;

import android.content.Context;
import app.omegasoftware.pontoeletronico.common.Notificacao;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaHistoricoSincronizador;
import app.omegasoftware.pontoeletronico.database.synchronize.CrudInserirEditar;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_INT;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_VETOR_STRING;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

// **********************
// DECLARAuuO DA CLASSE
// **********************

public class EXTDAOSistemaHistoricoSincronizador extends
		DAOSistemaHistoricoSincronizador {

	static Cache singletonCache;

	protected Cache factoryCache(Context context) throws OmegaDatabaseException {
		if (singletonCache == null)
			singletonCache = new Cache(context);
		return singletonCache;
	}

	protected class Cache {

		public String[] idsSistemaTabelaNotificacoes;
		public String clausulaIn = "";
		public int ultimoIdSistemaHistoricoSincronizadorLido = -1;
		public int ultimoIdSistemaHistoricoSincronizadorAnalisado = -1;
		public long ultimaContagem = -1;

		public Cache(Context context) throws OmegaDatabaseException {

			ultimoIdSistemaHistoricoSincronizadorLido = PontoEletronicoSharedPreference
					.getValor(
							context,
							TIPO_INT.ULTIMO_ID_SISTEMA_HISTORICO_SINCRONIZADOR_LIDO,
							-1);
			idsSistemaTabelaNotificacoes = PontoEletronicoSharedPreference
					.getValor(context,
							TIPO_VETOR_STRING.IDS_SISTEMA_TABELA_NOTIFICACAO);
			if (idsSistemaTabelaNotificacoes == null) {
				ArrayList<String> ids = new ArrayList<String>();
				for (int i = 0; i < Notificacao.tabelasNotificacao.length; i++) {
					String idSistemaTabela = EXTDAOSistemaTabela
							.getIdSistemaTabela(context,
									Notificacao.tabelasNotificacao[i]);
					ids.add(idSistemaTabela);
				}
				String[] vetorIds = new String[ids.size()];

				salvarIdsNotificacao(context, ids.toArray(vetorIds));
				idsSistemaTabelaNotificacoes = vetorIds;
			}
			if (idsSistemaTabelaNotificacoes != null
					&& idsSistemaTabelaNotificacoes.length > 0)
				clausulaIn = HelperString
						.getStrQueryInDatabase(idsSistemaTabelaNotificacoes.length);
		}

		public void salvarUltimaContagem(long ultimaContagem) {
			this.ultimaContagem = ultimaContagem;
		}

		public void salvarUltimoIdAnalisado(int ultimoIdAnalisado) {
			this.ultimoIdSistemaHistoricoSincronizadorAnalisado = ultimoIdAnalisado;
		}

		public void salvarUltimoIdLido(Context context, int ultimoIdLido) {
			PontoEletronicoSharedPreference.saveValor(context,
					TIPO_INT.ULTIMO_ID_SISTEMA_HISTORICO_SINCRONIZADOR_LIDO,
					ultimoIdLido);

		}

		public void salvarIdsNotificacao(Context context, String[] ids) {
			idsSistemaTabelaNotificacoes = ids;
			clausulaIn = HelperString
					.getStrQueryInDatabase(idsSistemaTabelaNotificacoes.length);
			PontoEletronicoSharedPreference.saveValor(context,
					TIPO_VETOR_STRING.IDS_SISTEMA_TABELA_NOTIFICACAO, ids);
		}
	}

	// *************************
	// DECLARAuuO DE ATRIBUTOS
	// *************************

	// *************************
	// CONSTRUTOR
	// *************************
	public EXTDAOSistemaHistoricoSincronizador(Database database) {
		super(database);

	}

	// *************************
	// FACTORY
	// *************************
	public Table factory() {
		return new EXTDAOSistemaHistoricoSincronizador(this.getDatabase());

	}

	public void atualizaUltimaNotificacaoLida(String id) throws OmegaDatabaseException {
		Cache cache = factoryCache(getDatabase().getContext());
		cache.salvarUltimoIdLido(getDatabase().getContext(),
				HelperInteger.parserInt(id));
	}

	public Long getTotalNotificacaoNaoLida() throws OmegaDatabaseException {
		Cache cache = factoryCache(getDatabase().getContext());
		if (cache.idsSistemaTabelaNotificacoes == null
				|| cache.idsSistemaTabelaNotificacoes.length == 0)
			return (long) 0;
		String strLastId = super.getLastIdInserted();

		if (strLastId != null) {
			int lastId = HelperInteger.parserInt(strLastId);

			String[] arg = null;
			if (cache.ultimoIdSistemaHistoricoSincronizadorLido > 0) {
				arg = new String[cache.idsSistemaTabelaNotificacoes.length + 1];
				for (int i = 0; i < cache.idsSistemaTabelaNotificacoes.length; i++) {
					arg[i] = cache.idsSistemaTabelaNotificacoes[i];
				}

				arg[arg.length - 1] = String
						.valueOf(cache.ultimoIdSistemaHistoricoSincronizadorLido);
			} else {
				arg = cache.idsSistemaTabelaNotificacoes;
			}

			String q = "SELECT COUNT(id)	" + "	FROM  "
					+ EXTDAOSistemaHistoricoSincronizador.NAME + "	WHERE "
					+ EXTDAOSistemaHistoricoSincronizador.SISTEMA_TABELA_ID_INT
					+ " IN (" + cache.clausulaIn + ") ";
			if (cache.ultimoIdSistemaHistoricoSincronizadorLido > 0)
				q += "	AND id > ? ";
			Long cont = getDatabase().getResultSetAsLong(q, 0, arg);
			if(cont == null) return null;
			if (lastId > cache.ultimoIdSistemaHistoricoSincronizadorAnalisado
					|| cont != cache.ultimaContagem) {
				cache.salvarUltimaContagem(cont == null ? -1 : cont);

				cache.salvarUltimoIdAnalisado(lastId);
				return cont == null ? 0 : cont;
			} else {
				cache.salvarUltimaContagem(cont == null ? -1 : cont);
				return null;
			}
		}
		return null;
	}
	//TOO VILuO 7.7%
	//sendo:
	//Table::insert 62%
	//HelperDate::getDatetimeAtualFormatadaParaSQL 33.2%
	public void gravaCrudInserirEditar(CrudInserirEditar crud, String datetime,
			String idSistemaTabela) {
		// sistema_tabela_id_INT,
		// id_tabela_INT,
		// sistema_tipo_operacao_id_INT,
		// data_DATETIME,
		// corporacao_id_INT,
		// usuario_INT
		boolean validade = false;
		if (idSistemaTabela == null)
			return;
		if (crud.idTabelaWebCrud != null) {

			this.setAttrValue(
					EXTDAOSistemaHistoricoSincronizador.ID_TABELA_INT,
					crud.idTabelaWebCrud.toString());
			validade = true;

		} else if (crud.idTabelaWeb != null) {
			this.setAttrValue(
					EXTDAOSistemaHistoricoSincronizador.ID_TABELA_INT,
					crud.idTabelaWeb.toString());
			validade = true;
		}

		if (validade) {
			this.setAttrValue(EXTDAOSistemaHistoricoSincronizador.ID, null);
			this.setAttrValue(
					EXTDAOSistemaHistoricoSincronizador.SISTEMA_TABELA_ID_INT,
					idSistemaTabela);
			this.setAttrValue(
					EXTDAOSistemaHistoricoSincronizador.SISTEMA_TIPO_OPERACAO_ID_INT,
					String.valueOf(crud.tipo.getId()));
			this.setAttrValue(
					EXTDAOSistemaHistoricoSincronizador.DATA_DATETIME,
					datetime);
			this.setAttrValue(
					EXTDAOSistemaHistoricoSincronizador.CORPORACAO_ID_INT,
					OmegaSecurity.getIdCorporacao());
			this.setAttrValue(EXTDAOSistemaHistoricoSincronizador.USUARIO_INT,
					OmegaSecurity.getIdUsuario());
			this.insert(false);
		}

	}

} // classe: fim

