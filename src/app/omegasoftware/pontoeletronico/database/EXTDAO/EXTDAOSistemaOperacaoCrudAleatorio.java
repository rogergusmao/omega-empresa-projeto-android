

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaOperacaoCrudAleatorio
    * DATA DE GERAuuO: 14.07.2013
    * ARQUIVO:         EXTDAOSistemaOperacaoCrudAleatorio.java
    * TABELA MYSQL:    sistema_operacao_crud_aleatorio
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaOperacaoCrudAleatorio;
        
    
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOSistemaOperacaoCrudAleatorio extends DAOSistemaOperacaoCrudAleatorio
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaOperacaoCrudAleatorio(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaOperacaoCrudAleatorio(this.getDatabase());

    }
    

    } // classe: fim
    

    