

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaOperacaoCrudAleatorio
    * DATA DE GERAuuO: 14.07.2013
    * ARQUIVO:         EXTDAOSistemaOperacaoCrudAleatorio.java
    * TABELA MYSQL:    sistema_operacao_crud_aleatorio
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaOperacaoCrudMobileBaseadoWeb;
        
    
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOSistemaOperacaoCrudMobileBaseadoWeb extends DAOSistemaOperacaoCrudMobileBaseadoWeb
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaOperacaoCrudMobileBaseadoWeb(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaOperacaoCrudMobileBaseadoWeb(this.getDatabase());

    }
    

    } // classe: fim
    

    