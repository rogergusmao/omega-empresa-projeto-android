

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaOperacaoDownloadBancoMobile
    * DATA DE GERAuuO: 22.06.2013
    * ARQUIVO:         EXTDAOSistemaOperacaoDownloadBancoMobile.java
    * TABELA MYSQL:    sistema_operacao_download_banco_mobile
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaOperacaoDownloadBancoMobile;
        
    
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOSistemaOperacaoDownloadBancoMobile extends DAOSistemaOperacaoDownloadBancoMobile
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaOperacaoDownloadBancoMobile(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaOperacaoDownloadBancoMobile(this.getDatabase());

    }
    

    } // classe: fim
    

    