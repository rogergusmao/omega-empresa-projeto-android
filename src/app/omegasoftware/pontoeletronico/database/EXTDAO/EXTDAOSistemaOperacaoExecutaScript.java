

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaOperacaoExecutaScript
    * DATA DE GERAuuO: 22.06.2013
    * ARQUIVO:         EXTDAOSistemaOperacaoExecutaScript.java
    * TABELA MYSQL:    sistema_operacao_executa_script
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaOperacaoExecutaScript;
        
    
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOSistemaOperacaoExecutaScript extends DAOSistemaOperacaoExecutaScript
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaOperacaoExecutaScript(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaOperacaoExecutaScript(this.getDatabase());

    }
    

    } // classe: fim
    

    