

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaOperacaoSistemaMobile
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSistemaOperacaoSistemaMobile.java
    * TABELA MYSQL:    sistema_operacao_sistema_mobile
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.ArrayList;

import android.database.Cursor;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOOperacaoCrudAleatorio;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOOperacaoDownloadBancoSQLMobile;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOOperacaoDownloadSQLiteMobile;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOOperacaoExecutaScript;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOOperacaoMonitoraView;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOOperacaoSincroniza;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOOperacaoSistemaMobile;
import app.omegasoftware.pontoeletronico.database.Database;

import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaOperacaoSistemaMobile;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOSistemaOperacaoSistemaMobile extends DAOSistemaOperacaoSistemaMobile
    {

    	public static String OPERACAO_SISTEMA_MOBILE_ID_INT = "operacao_sistema_mobile_id_INT";
    	public enum ESTADO_OPERACAO_SISTEMA_MOBILE{
    		AGUARDANDO,
    		PROCESSANDO,
    		CONCLUIDA,
    		ERRO_EXECUCAO,
    		CANCELADA,
    		AVULSA
    	}
    	
    	public enum TIPO_OPERACAO_SISTEMA{
    		DOWNLOAD_BANCO_MOBILE(1),
    		EXECUTA_SCRIPT(2),
    		CRUD_ALEATORIO(3),
    		SINCRONIZA(4),
    		REINSTALA_APLICATIVO(5),
    		MONITORA_VIEW(6),
    		DOWNLOAD_BANCO_SQLITE_MOBILE(7),
    		COMPARACAO_BANCO_SQLITE_MOBILE(8),
    		CRUD_MOBILE_BASEADO_NA_WEB(9)
    		;
    		
    		int id;
    		
    		TIPO_OPERACAO_SISTEMA(int id){
    			this.id = id;
    		}
    		public int getId(){
    			return id;
    		}
    		
    		
    		public static TIPO_OPERACAO_SISTEMA getTipoOperacaoSistema(int pId){
    			TIPO_OPERACAO_SISTEMA vetor[] = TIPO_OPERACAO_SISTEMA.values();
    			for(int i = 0 ; i < vetor.length; i++){
    				TIPO_OPERACAO_SISTEMA tipo = vetor[i];
    				if(tipo.getId() == pId) return tipo;
    			}
    			return null;
    		}
    	}
    	
    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaOperacaoSistemaMobile(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaOperacaoSistemaMobile(this.getDatabase());

    }

	public BOOperacaoSistemaMobile getObjBO(){
		TIPO_OPERACAO_SISTEMA tipo = getTipoOperacaoSistema(this.getStrValueOfAttribute(EXTDAOSistemaOperacaoSistemaMobile.TIPO_OPERACAO_SISTEMA_ID_INT));
		String id = this.getStrValueOfAttribute(EXTDAOSistemaOperacaoSistemaMobile.ID);
		if(tipo == null)
			return null;
		switch (tipo) {
		case MONITORA_VIEW:
			return new BOOperacaoMonitoraView(this.getDatabase().getContext(), this.getDatabase(), id);
		case DOWNLOAD_BANCO_MOBILE:
			return new BOOperacaoDownloadBancoSQLMobile(this.getDatabase().getContext(), this.getDatabase(), id);
		case CRUD_ALEATORIO:
			return new BOOperacaoCrudAleatorio(this.getDatabase().getContext(), this.getDatabase(), id);
		case SINCRONIZA:
			return new BOOperacaoSincroniza(this.getDatabase().getContext(), this.getDatabase(), id);
		case EXECUTA_SCRIPT:
			return new BOOperacaoExecutaScript(this.getDatabase().getContext(), this.getDatabase(), id);
		case DOWNLOAD_BANCO_SQLITE_MOBILE:
		case COMPARACAO_BANCO_SQLITE_MOBILE:
			return new BOOperacaoDownloadSQLiteMobile(this.getDatabase().getContext(), this.getDatabase(), id);	
		
		default:
			return null;
		}
	}
	public static TIPO_OPERACAO_SISTEMA getTipoOperacaoSistema(String id){
		Integer idInt = HelperInteger.parserInteger(id);
		if(idInt == null) return null;
		else return getTipoOperacaoSistema(idInt);
	}
	public static TIPO_OPERACAO_SISTEMA getTipoOperacaoSistema(int id){
		TIPO_OPERACAO_SISTEMA[] tipos = TIPO_OPERACAO_SISTEMA.values();
		for(int i = 0 ; i < tipos.length; i++){
			if (tipos[i].getId() == id) return tipos[i];
		}
		return null;
	}
	
	public static ESTADO_OPERACAO_SISTEMA_MOBILE getEstadoOperacaoSistemaMobile(int pId){
		switch (pId) {
		case 1:
			return ESTADO_OPERACAO_SISTEMA_MOBILE.AGUARDANDO;
		case 2:
			return ESTADO_OPERACAO_SISTEMA_MOBILE.PROCESSANDO;
		case 3:
			return ESTADO_OPERACAO_SISTEMA_MOBILE.CONCLUIDA;
		case 4:
			return ESTADO_OPERACAO_SISTEMA_MOBILE.ERRO_EXECUCAO;
		case 5:
			return ESTADO_OPERACAO_SISTEMA_MOBILE.CANCELADA;
		case 6:
			return ESTADO_OPERACAO_SISTEMA_MOBILE.AVULSA;
		default:
			return null;
			
		}
	}
	
	public static String getIdEstadoOperacaoSistemaMobile(ESTADO_OPERACAO_SISTEMA_MOBILE pEstado){
		switch (pEstado) {
		case AGUARDANDO:
			return "1";
		case PROCESSANDO:
			return "2";
		case CONCLUIDA:
			return "3";
		case ERRO_EXECUCAO:
			return "4";
		case CANCELADA:
			return "5";
		case AVULSA:
			return "6";
		default:
			return null;
		}
	}

	public ArrayList<String> getListaIdNoEstado(String pVetorIdEstado[]){
		
		String vConsulta = "SELECT " + EXTDAOSistemaOperacaoSistemaMobile.ID + " " +
				" FROM " + EXTDAOSistemaOperacaoSistemaMobile.NAME + " " +
				" WHERE "+ EXTDAOSistemaOperacaoSistemaMobile.ESTADO_OPERACAO_SISTEMA_MOBILE_ID_INT ;
		boolean vValidade = false;
		if(pVetorIdEstado != null && pVetorIdEstado.length > 0){
			vConsulta += " IN (";
			for (int i = 0 ; i <  pVetorIdEstado.length; i++) {
				if(!vValidade){
					vConsulta += " ? ";
					vValidade = true;
				}
				else vConsulta += ", ? ";
			}
			vConsulta += ")";
		}
		ArrayList<String> vListaRetorno = new ArrayList<String>();
		
		
		Cursor cursor = null;
		try{
			if(pVetorIdEstado != null && pVetorIdEstado.length > 0)
				cursor = database.rawQuery(vConsulta, pVetorIdEstado);
			else cursor = database.rawQuery(vConsulta, null);
			
			String vId = null;
		
			if (cursor.moveToFirst()) { 

				do {
					vId = cursor.getString(0);
					vListaRetorno.add(vId);
				}while (cursor.moveToNext()); 
			}
		} catch(Exception ex){
			
			SingletonLog.insereErro(
					ex, 
					SingletonLog.TIPO.BANCO);
		}

		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		if(vListaRetorno.size() == 0 ) return null;
		else return vListaRetorno;
		
	}

    } // classe: fim
    

    