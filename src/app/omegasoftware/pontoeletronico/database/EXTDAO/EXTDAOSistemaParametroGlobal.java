

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaParametroGlobal
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSistemaParametroGlobal.java
    * TABELA MYSQL:    sistema_parametro_global
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaParametroGlobal;

	

    
        
    public class EXTDAOSistemaParametroGlobal extends DAOSistemaParametroGlobal
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaParametroGlobal(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaParametroGlobal(this.getDatabase());

    }
    

    } // classe: fim
    

    