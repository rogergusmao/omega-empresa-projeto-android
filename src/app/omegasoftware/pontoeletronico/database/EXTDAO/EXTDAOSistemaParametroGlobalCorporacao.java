

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOSistemaParametroGlobalCorporacao
        * DATA DE GERAuuO: 24.08.2017
        * ARQUIVO:         EXTDAOSistemaParametroGlobalCorporacao.java
        * TABELA MYSQL:    sistema_parametro_global_corporacao
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaParametroGlobalCorporacao;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAuuO DA CLASSE
        // **********************


    
        

        public class EXTDAOSistemaParametroGlobalCorporacao extends DAOSistemaParametroGlobalCorporacao
        {


        // *************************
        // DECLARAuuO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOSistemaParametroGlobalCorporacao(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOSistemaParametroGlobalCorporacao(this.getDatabase());

        }
        

        } // classe: fim


        