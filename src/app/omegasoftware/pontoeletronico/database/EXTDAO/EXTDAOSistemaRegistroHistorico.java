

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaRegistroSincronizadorAndroidParaWeb
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.java
    * TABELA MYSQL:    sistema_registro_sincronizador_android_para_web
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.database.Cursor;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaRegistroHistorico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.DadosInsercaoRemocao;
import app.omegasoftware.pontoeletronico.database.synchronize.RetornoUpdateRollback;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

	

    
        
    public class EXTDAOSistemaRegistroHistorico extends DAOSistemaRegistroHistorico
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaRegistroHistorico(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaRegistroHistorico(this.getDatabase());

    }
    
    public InterfaceMensagem updateRegistroComDadosAntigoSeExistirNoHistoricoDeEdicao(
    		EXTDAOSistemaRegistroSincronizadorAndroidParaWeb objSRSAW,
    		Table registroASerRealizadoBackup,
    		OmegaLog log) throws Exception{
    	String idSistemaTabela = EXTDAOSistemaTabela.getIdSistemaTabela(getDatabase(), registroASerRealizadoBackup.getName());
    	String idCorporacao = OmegaSecurity.getIdCorporacao();
    	String idRegistroASerRealizadoOBackup = registroASerRealizadoBackup.getId();
    	if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
    		log.escreveLog("updateRegistroComDadosAntigoSeExistirNoHistoricoDeEdicao. idRegistroASerRealizadoOBackup: " + idRegistroASerRealizadoOBackup + ". IdSistemaTabela: " + idSistemaTabela + ". IdCorporacao: " + idCorporacao);
    	
		Cursor cursor = database.query(
				getName(),
				new String[]{"id", "sistema_registro_sincronizador_id_INT",  "crud"},
		            "	sistema_tabela_id_INT = ?" +
		            "	AND id_tabela_INT = ? " +
		            "	AND corporacao_id_INT = ? ", 
				new String[]{
					idSistemaTabela,
					idRegistroASerRealizadoOBackup,
					idCorporacao
				}, 
				null,
				"", 
				" id DESC ", 
				" 0, 2 ");
		
		try{
			if(cursor.moveToFirst()){
				do{
					String crud = cursor.getString(2);
					int idSistemaRegistroSincronizador = cursor.getInt(1);
					int id = cursor.getInt(0);
					JSONObject json = new JSONObject(crud);
		    		Table novo = registroASerRealizadoBackup.factory();
		    		String strIdentificador = novo.getName() + "["+id+"]"; 
		    		ArrayList<String> nomesAtributos = novo.getVetorNomeAtributo();
		    		boolean warningOcorrido = false;
		    		for (String attr : nomesAtributos) {
		    			if(json.has((attr))){
		    				String valor = json.getString(attr);
							novo.setAttrValue(attr, valor);	
		    			} else {
		    				warningOcorrido = true;
		    				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
		    					log.escreveLog("WARNING - o backup do registro " + strIdentificador + " nao possui o atributo " + attr + ". ");
		    			}					
					}
		    		if(warningOcorrido && OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
		    			log.escreveLog("O registro contido no backup e: " + crud + ". IdSistemaRegistroSincronizador: " + idSistemaRegistroSincronizador);
		    		}
		    		//boolean validadeFk = true;
		    		List<Attribute> attrFks = getListAttributeFK();
		    		if(attrFks != null){
		    			for (Attribute attrFk : attrFks) {
			    			//Para cada atributo do historico de valores do registro da tabela do banco local, registros que ainda nuo foram sincronizados
			    			//ou que estuo em processo de sincronizacao.
			    			//Verificaremos se o id do registro foi atualizado para algum novo valor
			    			
			    			//Para todos os cenurios as seguintes premissas suo validas:
			    			//a - As tabelas de todas as FKs ju possuem os ultimos dados ocorridos na web mais algumas operauues internas que ocorreram
			    			//entre o meio tempo do inicio da sincronizacao atu a fase de processamento dos dados sincronizados
			    			
			    			//Os cenurios do problema suo:
			    			//1 - A chave fk era um valor nuo definitivo na epoca que o historico foi gravado
			    			//	1.1 - Em caso de existir a operauuo de inseruuo na fila como um registro ainda nuo sincronizado, u indiferente ao processo
			    			//	1.2 - Em caso de existir a operauuo de inseruuo na fila como um registro em processo de sincronizacao
			    			//		Nesse caso, devido a urvore hierarquica de operauues podemos assumir que o crud vindo da web relacionado com a inseruuo
			    			//		ju foi executado no atual contexto, pois a tabela fk u filha, logo seus cruds suo executados sempre antes do contexto
			    			//		atual do pai.
			    			//		Logo temos que o registro relativa a sincronizacao contido na tabela sistemA_registro_sincronizador_android_para_web
			    			//		contem o novo id web do registro, pegamos esse id e atualizamos o registro de fallback
			    			//	1.3 - Em caso de existir uma operauuo de ediuuo na fila como um registro que ainda foi ou nuo sincronizado do registro da fk,
			    			//			u indiferente ao processo
			    			//  1.4 - Em caso de existir uma remouao nuo sincronizada, seta para null on set null, ou delete on cascade
			    			//	1.5 - Em caso de existir uma remouuo em processo de sincronizacao, seta para null on set null, ou delete on cascade
			    			//	
			    			
			    			//* - temos dois tipos na (fila = sistema_tabela_registro_android_para_web)
			    			//	- TIPO 1 - Registros que ainda nuo foram enviados
			    			//	- TIPO 2 - Registros que foram enviados e estuo em um processo de sincronizacao em andamento.
			    			
			    			//2 - A chave fk ju era definito
			    			//	2.1 - Em caso de ter havido a remouuo do registro da fk o valor atual u setado para null ON SET NULL, ou cascade ON CASCADE
			    			//  2.2 - Em caso de ediuuo e inseruuo u indiferente para esse processo.

			    			//No fim su nos importa inseruues que estuo em processo de sincronizacao
			    			//e remouues dentro ou fora do processo de sincronizacao
			    			
			    			if(attrFk.isNull() )
			    				continue;
			    			
			    			String idSincronizadorFK = EXTDAOSistemaTabela.getIdSistemaTabela(this.database, attrFk.tableFK);
			    			DadosInsercaoRemocao dados = objSRSAW.getIdsDasOperacoesDeInsercaoERemocao(idSincronizadorFK, attrFk.getStrValue());
			    			
			    			if(dados != null){
			    				if(dados.idTipoOperacao.equals(EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT)){
				    				if(dados.idTabelaWeb != null){
				    					attrFk.setStrValue(String.valueOf( dados.idTabelaWeb));
				    					continue;
				    				}
				    			} //else if(dados.idTipoOperacao.equals(EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE)){
			    				//TODO verificar CASCADE ou SET NULL
			    				//attrFk.setStrValue(null);	
			    				//}
				    				
			    			}
			    			
			    			if(!database.isIdInTable(attrFk.getStrValue(), attrFk.getStrTableFK())){
			    				//TODO verificar CASCADE ou SET NULL
			    				attrFk.setStrValue(null);
			    			} 
						}
		    		}
		    		
		    		
		    		if(novo.update(false)){
		    			boolean ultimoRegistro = !cursor.moveToNext();
		    			//remove o historico dessa edicao que de nada mais vale
		    			objSRSAW.remove(String.valueOf(idSistemaRegistroSincronizador), false);
		    			this.remove(String.valueOf(id), false);
		    			return new RetornoUpdateRollback(novo, ultimoRegistro);
		    		} else {
		    			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
		    				log.escreveLog("[4er54t]Falha ao tentar realizar o fallback dos dados do registro [" 
				    			+ registroASerRealizadoBackup.getIdentificadorRegistro() 
				    			+ "] para o novo registro [" 
				    			+novo.getIdentificadorRegistro()
				    			+ "] ");
		    		}
		    		
	    		}
	    		while(cursor.moveToNext());
			}
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Nuo existem mais dados de backup para fallback do registro "
					+ registroASerRealizadoBackup.getName() 
					+ "::" 
					+ registroASerRealizadoBackup.getId());
			
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, "Nuo foi possuvel realizar o fallback do registro da tabela " + registroASerRealizadoBackup.getName()  + "::" + registroASerRealizadoBackup.getId());
		}finally{
			if(cursor != null)cursor.close();
		}
		
    }
    
    
    public int atualizaIdsDoRegistroPeloIdMobileOriginal(
			String idTabelaSincronizador, 
			String idMobileOriginal,
			String novoId) throws JSONException, Exception {
		
		
		ContentValues cv = new ContentValues();
		cv.put(EXTDAOSistemaRegistroHistorico.ID_TABELA_INT, novoId);
		
		return database.update(
			EXTDAOSistemaRegistroHistorico.NAME, 
			cv, 
			EXTDAOSistemaRegistroHistorico.ID_TABELA_MOBILE_INT + " = ? "
			+ " AND "+ EXTDAOSistemaRegistroHistorico.SISTEMA_TABELA_ID_INT + " = ? ", 
			new String[]{
					idMobileOriginal,
				idTabelaSincronizador
			}
		);
	}
    
    public int atualizaIdsDoRegistroPeloIdMobileAtual(
			String idTabelaSincronizador, 
			String antigoId, 
			String novoId) throws JSONException, Exception {
		
		
		ContentValues cv = new ContentValues();
		cv.put(EXTDAOSistemaRegistroHistorico.ID_TABELA_INT, novoId);
		
		return database.update(
			EXTDAOSistemaRegistroHistorico.NAME, 
			cv, 
			EXTDAOSistemaRegistroHistorico.ID_TABELA_INT + " = ? "
			+ " AND "+ EXTDAOSistemaRegistroHistorico.SISTEMA_TABELA_ID_INT + " = ? ", 
			new String[]{
				antigoId,
				idTabelaSincronizador
			}
		);
	}
    

    public boolean removerTodasOsRegistrosQueAcabaramDeSerSincronizados(String ultimoIdIncluso,OmegaLog log)
			throws Exception {
    	
    	if( ultimoIdIncluso != null){
    		int total = database.delete(
    				this.getName(),
    		    	" ( "
    		    	+" select ? " 
    		    	+" from sistema_registro_sincronizador_android_para_web srsaw " 
    		    	+" where srsaw.id = sistema_registro_sincronizador_id_INT "
    		    	+" and srsaw.id < ? "  
    		    	+ " and srsaw.sincronizando_BOOLEAN = ? " 
    		    	+ "	LIMIT 0,1	) = ?",
    				new String[] { "1", ultimoIdIncluso, "1", "1" });
    		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
    			log.escreveLog("SistemaRegistroHistorico::removerTodasOsRegistrosQueAcabaramDeSerSincronizados - Ultimo Id: " + ultimoIdIncluso + ". Removeu ao todo: " + String.valueOf(total));
    	} else {
    		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
    			log.escreveLog("SistemaRegistroHistorico::removerTodasOsRegistrosQueAcabaramDeSerSincronizados - Nuo houve remouues pois o ultimo id u nulo ");
    	}
		// if (ultimoIdIncluso != null) {
		

		// } else {
		// database.delete(this.getName(), "",
		// new String[] {});
		// }

		return true;

	}
} // classe: fim
    

    