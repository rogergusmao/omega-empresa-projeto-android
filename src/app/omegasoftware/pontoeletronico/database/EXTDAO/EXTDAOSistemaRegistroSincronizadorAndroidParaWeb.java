/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:  EXTDAOSistemaRegistroSincronizadorAndroidParaWeb
 * DATA DE GERAuuO: 08.06.2013
 * ARQUIVO:         EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.java
 * TABELA MYSQL:    sistema_registro_sincronizador_android_para_web
 * BANCO DE DADOS:  
 * -------------------------------------------------------
 *
 */

package app.omegasoftware.pontoeletronico.database.EXTDAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Stack;

import org.json.JSONException;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
import app.omegasoftware.pontoeletronico.database.ConflictUpdateId;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.ResultSet;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaRegistroSincronizadorAndroidParaWeb;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador.STATE_SINCRONIZADOR;
import app.omegasoftware.pontoeletronico.database.synchronize.ContainerEXTDAO;
import app.omegasoftware.pontoeletronico.database.synchronize.ContainerFastSendData;
import app.omegasoftware.pontoeletronico.database.synchronize.ContainerQueryEXTDAO;
import app.omegasoftware.pontoeletronico.database.synchronize.ContainerQueryInsert;
import app.omegasoftware.pontoeletronico.database.synchronize.ContainerSendData;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizacaoException;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho.Item;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;
// **********************
// DECLARAuuO DA CLASSE
// **********************

public class EXTDAOSistemaRegistroSincronizadorAndroidParaWeb extends DAOSistemaRegistroSincronizadorAndroidParaWeb {

	// *************************
	// DECLARAuuO DE ATRIBUTOS
	// *************************

	// *************************
	// CONSTRUTOR
	// *************************
	public EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(Database database) {
		super(database);

	}

	// *************************
	// FACTORY
	// *************************
	public Table factory() {
		return new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(this.getDatabase());

	}

	public boolean removerTodasOsRegistrosQueAcabaramDeSerSincronizados(String ultimoIdIncluso, OmegaLog log)
			throws Exception {

		if (ultimoIdIncluso != null) {
			int total = database.delete(
					this.getName(), "id <= ? " + " AND "
							+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN + " = ? ",
					new String[] { ultimoIdIncluso, "1" });
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog(
						"SistemaRegistroSincronizadorAndroidParaWeb::removerTodasOsRegistrosQueAcabaramDeSerSincronizados - Ultimo Id: "
								+ ultimoIdIncluso + ". Removeu ao todo: " + String.valueOf(total));
		} else {
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog(
						"SistemaRegistroSincronizadorAndroidParaWeb::removerTodasOsRegistrosQueAcabaramDeSerSincronizados - Nuo houve remouues pois o ultimo id u nulo");
		}
		// if (ultimoIdIncluso != null) {

		// } else {
		// database.delete(this.getName(), "",
		// new String[] {});
		// }

		return true;

	}

	static boolean sequenceInicializada = false;

	public static void initSequences(Context context) throws Exception {

		Database db = null;
		try {

			STATE_SINCRONIZADOR state = RotinaSincronizador.getStateSincronizador(context, null);
			if (state == STATE_SINCRONIZADOR.COMPLETO && !sequenceInicializada) {
				db = new DatabasePontoEletronico(context);
				sequenceInicializada = true;
				SincronizadorEspelho.Item itemSinc = SincronizadorEspelho.getItem(context, null);
				if (itemSinc != null) {
					if (itemSinc.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb != null
							&& itemSinc.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb > 0)
						db.updateSqliteSequence("sistema_registro_sincronizador_android_para_web",
								itemSinc.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb);
				}

				String q = "SELECT " + " t.id, " + " MAX(s.id_tabela_INT)," + " MAX(s.id_tabela_mobile_INT)" + " FROM"
						+ " sistema_registro_sincronizador_android_para_web s"
						+ " JOIN sistema_tabela t ON s.sistema_tabela_id_INT = t.id"
						+ " JOIN sqlite_sequence ss ON t.nome = ss.name" + " WHERE" + " ss.seq > s.id_tabela_INT"
						+ " OR ss.seq > s.id_tabela_mobile_INT" + " GROUP BY" + " t.id";
				ResultSet rs = db.getResultSet(q, null);
				if (rs != null) {
					for (int i = 0; i < rs.getTotalTupla(); i++) {
						String[] valores = rs.getTupla(i);
						String tabela = EXTDAOSistemaTabela.getNomeSistemaTabela(db, valores[0]);
						if (valores[1] != null && valores[2] != null) {
							if (valores[1].compareTo(valores[2]) > 0)
								db.updateSqliteSequence(tabela, HelperInteger.parserLong(valores[1]));
							else
								db.updateSqliteSequence(tabela, HelperInteger.parserLong(valores[2]));
						} else if (valores[1] != null)
							db.updateSqliteSequence(tabela, HelperInteger.parserLong(valores[1]));
						else if (valores[2] != null)
							db.updateSqliteSequence(tabela, HelperInteger.parserLong(valores[2]));

					}
				} else
					sequenceInicializada = false;
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.BANCO);
			sequenceInicializada = false;
			throw ex;
		} finally {
			if (db != null)
				db.close();
		}
	}

	public int removerTodosAsOperacoesDoRegistro(String pIdTabela, String idTabelaMobile, String pIdSistemaTabela)
			throws Exception {

		if (pIdTabela == null || pIdTabela.length() == 0)
			return 0;
		else {
			if (idTabelaMobile == null) {
				int vNumberRowsAffected = database.delete(this.getName(),
						ID_TABELA_INT + " = ? AND " + SISTEMA_TABELA_ID_INT + " = ?",
						new String[] { pIdTabela, pIdSistemaTabela });
				return vNumberRowsAffected;
			} else {
				int vNumberRowsAffected;

				vNumberRowsAffected = database.delete(this.getName(),
						ID_TABELA_INT + " = ? AND " + SISTEMA_TABELA_ID_INT + " = ? " + " AND " + ID_TABELA_MOBILE_INT
								+ " = ? AND " + SISTEMA_TABELA_ID_INT + " = ? ",
						new String[] { pIdTabela, idTabelaMobile, pIdSistemaTabela });

				return vNumberRowsAffected;
			}

		}

	}

	public int removerTodosAsOperacoesDoRegistroNaoSincronizadas(String pIdTabela, String idTabelaMobile,
			String pIdSistemaTabela) throws Exception {

		if (pIdTabela == null || pIdTabela.length() == 0)
			return 0;
		else {
			if (idTabelaMobile == null) {
				int vNumberRowsAffected = database
						.delete(this.getName(),
								ID_TABELA_INT + " = ? AND " + SISTEMA_TABELA_ID_INT + " = ? AND "
										+ SINCRONIZANDO_BOOLEAN + " != ?",
								new String[] { pIdTabela, pIdSistemaTabela, "1" });
				return vNumberRowsAffected;
			} else {
				int vNumberRowsAffected = database.delete(this.getName(),
						ID_TABELA_INT + " = ? AND " + SISTEMA_TABELA_ID_INT + " = ? " + " AND " + ID_TABELA_MOBILE_INT
								+ " = ? AND " + SISTEMA_TABELA_ID_INT + " = ?  AND " + SINCRONIZANDO_BOOLEAN + " != ?",
						new String[] { pIdTabela, idTabelaMobile, pIdSistemaTabela, "1" });
				return vNumberRowsAffected;
			}

		}

	}

	public int removerAsOperacoesDoRegistroDoTipo(String pIdTabela, String idTabelaMobile, String pIdSistemaTabela,
			String pIdTipo) throws Exception {

		if (pIdTabela == null || pIdTabela.length() == 0)
			return 0;
		else {
			if (idTabelaMobile == null) {
				int vNumberRowsAffected = database.delete(
						this.getName(), ID_TABELA_INT + " = ? AND " + SISTEMA_TABELA_ID_INT + " = ? AND "
								+ SISTEMA_TIPO_OPERACAO_ID_INT + " = ? ",
						new String[] { pIdTabela, pIdSistemaTabela, pIdTipo });
				return vNumberRowsAffected;
			} else {
				int vNumberRowsAffected = database.delete(this.getName(),
						ID_TABELA_INT + " = ? AND " + SISTEMA_TABELA_ID_INT + " = ? " + " AND " + ID_TABELA_MOBILE_INT
								+ " = ? AND " + SISTEMA_TABELA_ID_INT + " = ? AND " + SISTEMA_TIPO_OPERACAO_ID_INT
								+ " = ? ",
						new String[] { pIdTabela, idTabelaMobile, pIdSistemaTabela, pIdTipo });
				return vNumberRowsAffected;
			}

		}

	}

	// TODO: VILuO PERFORMACE
	// TABLE::SELECT - 41%
	// TABLE::FORMAT TO SQLITE 22%
	// THIS::getId - 18.2%
	// TABLE::getTableFk = 9.9%
	// TABLE::getTablegetIdSistemaTabela = 3.4%
	public InterfaceMensagem preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks(
			String campos[],
			String valores[], 
			Table obj, 
			Long idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb, 
			OmegaLog log) {

		// Atualiza os valores para os que estuo contidos na duplicada
		if (campos.length != valores.length)
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
					"O numero de termos do vertor 'campos' estu diferente do de valores");
		StringBuilder sbValores = new StringBuilder();
		for (int k = 0; k < valores.length; k++) {
			String valor = valores[k];

			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
				sbValores.append(campos[k] + " = " + valor + "; ");
			}
			obj.setAttrValue(campos[k], valor);
		}
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO ){
			log.escreveLog(TAG, "preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks", sbValores.toString());
		}

		obj.formatToSQLite();
		// Verifica as chaves extrangeiras
		boolean removidoDevidoAFk = false;
		ArrayList<Attribute> atributosFk = obj.getListAttributeFK();
		if (atributosFk != null) {
			for (int l = 0; l < atributosFk.size(); l++) {
				Attribute attr = atributosFk.get(l);
				if (attr.isEmpty())
					continue;
				String idFk = attr.getStrValue();
				// Long longIdFk = HelperInteger.parserLong(idFk);
				// if(longIdFk == null)
				// continue;
				Table tableFk = attr.getTableFk(this.getDatabase());
				String idSistemaTabelaFk = EXTDAOSistemaTabela.getIdSistemaTabela(this.getDatabase(),
						tableFk.getName());
				if (idSistemaTabelaFk == null) {
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO ) {
						log.escreveLog(TAG, "preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks",
								"Nuo encontrou o registro na tabela sistema_tabela correspondente a '" + tableFk
										+ "'. O valor u: " + idFk);
					}
					
					continue;
				}
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO ) 
				log.escreveLog(TAG, "preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks",
						"Verificando a fk do sistema tabela " + idSistemaTabelaFk + " cujo registro tem id " + idFk);
				// Se u um registro local => existia o registro mas esse foi
				// removido no meio tempo, tanto que uma nova inseruuo ocupou o
				// id
				// Ou ou se nuo for possuvel selecionar o registro
				// Entuo considera-se que a FK foi removida
				boolean fkInexistente = !tableFk.existeId(idFk);
				if (isRegistroLocal(idFk, idSistemaTabelaFk,
						// null,
						// //idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
						// false,
						log) || fkInexistente) {
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO ) {
						if (fkInexistente)
							log.escreveLog(TAG, "preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks",
									"FK inexistent");
						else
							log.escreveLog(TAG, "preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks",
									"FK corresponde ao id da tabela sistema_registro_sincronizador_android_para_web  - id local: "
											+ idFk + ". Id sistema_Tabela - " + idSistemaTabelaFk);	
					}
					
					// Caso alguma dependente nao exista mais
					// considera-se que essa foi removida, logo analisa
					// o graud de dependencia no caso de remocao

					if (attr.onDelete == TYPE_RELATION_FK.CASCADE) {
						removidoDevidoAFk = true;
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO ) 
						log.escreveLog(TAG, "preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks",
								"Fk responsavel pela remouuo em cascata do registro");
						break;
						// nao executa a operacao de insercao
					} else if (attr.onDelete == TYPE_RELATION_FK.SET_NULL) {
						attr.setStrValue(null);
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO ) 
						log.escreveLog(TAG, "preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks",
								"Atributo teve o valor atribudo para nulo");
					}
				}
			}
		}

		if (removidoDevidoAFk) {
			//
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.REGISTRO_REMOVIDO_DEVIDO_ON_DELETE_CASCADE);
		} else
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
	}

	public boolean isTuplaSincronizadaNoServidor(String pNomeTabela, String pId) {
		// TODO otimizar
		clearData();
		setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT,
				EXTDAOSistemaTabela.getIdSistemaTabela(database, pNomeTabela));
		setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT, pId);
		setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT,
				OmegaSecurity.getIdCorporacao());
		// setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.USUARIO_ID_INT,
		// OmegaSecurity.getIdUsuario());
		ArrayList<Table> vListTable = getListTable();
		if (vListTable == null)
			return true;
		else if (vListTable.size() == 0)
			return true;
		else
			return false;
	}

	// public void removeListTuplaReferenteATupla(String pNomeTabela, String
	// pId, boolean pIsToInsertInTableSynchronize){
	// ArrayList<Table> vListReferente =
	// this.getRegistrosReferenteATupla(pNomeTabela, pId);
	// for (Table vTuplaReferente : vListReferente) {
	// vTuplaReferente.remove(pIsToInsertInTableSynchronize);
	// }
	// }
	public String getUltimoIdDoRegistroDaTabelaASerSincronizado(String pNomeTabela) throws JSONException, Exception {
		String q = "SELECT " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + " FROM "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME + " WHERE "
				// +
				// EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.USUARIO_ID_INT
				// + " = ? "
				// + " AND "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT + " = ? " + " AND "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT + " = ? ";
		String ultimoId = getIdDoUltimoSistemaRegistroSincronizadorAndroidParaWeb();
		String args[] = null;
		if (ultimoId != null) {
			q += " AND " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + " > ? ";
			args = new String[] {
					// OmegaSecurity.getIdUsuario(),
					OmegaSecurity.getIdCorporacao(),
					EXTDAOSistemaTabela.getIdSistemaTabela(database, EXTDAOUsuarioServico.NAME), ultimoId };

		} else
			args = new String[] {
					// OmegaSecurity.getIdUsuario(),
					OmegaSecurity.getIdCorporacao(),
					EXTDAOSistemaTabela.getIdSistemaTabela(database, EXTDAOUsuarioServico.NAME) };

		q += " ORDER BY " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + " DESC ";
		q += " LIMIT 0,1 ";

		Cursor c = null;
		try {
			c = database.rawQuery(q, args);
			if (c.moveToFirst()) {
				return c.getString(0);
			}
		} finally {
			if (c != null)
				c.close();
		}
		return null;
	}

	public String getUltimoId(String ultimoId) throws JSONException, Exception {
		return getUltimoId( ultimoId, OmegaSecurity.getIdCorporacao()) ;
	}
	public String getUltimoId(String ultimoId, String idCorporacao) throws JSONException, Exception {
		StringBuilder q = new StringBuilder("SELECT MAX(" + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + ") "
				+ " FROM " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME + " WHERE "
				// +
				// EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.USUARIO_ID_INT
				// + " = ? "
				// + " AND "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT + " = ? ");
		// String ultimoId =
		// getIdDoUltimoSistemaRegistroSincronizadorAndroidParaWeb();
		String args[] = null;
		if (ultimoId != null) {
			q.append(" AND " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + " > ? ");
			args = new String[] { idCorporacao, ultimoId };
		} else {
			args = new String[] { idCorporacao };
		}
		q.append(" LIMIT 0,1 ");

		Cursor c = null;
		try {
			c = database.rawQuery(q.toString(), args);
			if (c.moveToFirst()) {
				return c.getString(0);
			}
		} finally {
			if (c != null)
				c.close();
		}
		return null;
	}
	public Long getUltimoIdParaEnvio(
			Long idMinimoExcluso, 
			long limite
		) throws JSONException, Exception {
		
		return getUltimoIdParaEnvio(
				idMinimoExcluso, 
				limite,
				OmegaSecurity.getIdCorporacao());
	}
	public Long getUltimoIdParaEnvio(
			Long idMinimoExcluso, 
			long limite,
			String idCorporacao
		) throws JSONException, Exception {
		
		String q = "SELECT id " + " FROM " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME + " WHERE "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT + " = ? ";
		if (idMinimoExcluso != null) {
			q += " AND id > " + idMinimoExcluso;
		}
		// String ultimoId =
		// getIdDoUltimoSistemaRegistroSincronizadorAndroidParaWeb();
		// String args[] = null;
		// if (ultimoId != null) {
		// q += " AND " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID
		// + " > ? ";
		// args = new String[] { OmegaSecurity.getIdCorporacao(), ultimoId };
		// } else {
		String args[] = new String[] { idCorporacao };
		// }
		q += " ORDER BY id ASC";
		q += " LIMIT 0, " + limite;

		Cursor c = null;
		try {
			c = database.rawQuery(q, args);
			long ultimo = -1;
			while (c.moveToNext()) {

				ultimo = c.getLong(0);
			}
			return ultimo;
		} finally {
			if (c != null)
				c.close();
		}
	}

	public boolean isRegistroRemovidoLocalmente(String idTabela, String idSistemaTabela, OmegaLog log)
			throws JSONException, Exception {
		Long id = getId(idTabela, idSistemaTabela, EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE, false, log);
		if (id != null)
			return true;
		else
			return false;
	}

	public boolean isRegistroRemovidoLocalmenteDeAcordoComIdTabelaMobileOriginal(String idTabelaAtual,
			String idTabelaMobileOriginal, String idSistemaTabela, OmegaLog log) throws JSONException, Exception {
		Long id = getIdPeloIdMobileAtualEOriginal(idTabelaAtual, idTabelaMobileOriginal, idSistemaTabela,
				EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE, null, log);
		if (id != null)
			return true;
		else
			return false;

	}

	public String getPrimeiroId() throws JSONException, Exception {
		return getPrimeiroId(OmegaSecurity.getIdCorporacao());
	}
	
	public String getPrimeiroId(String idCorporacao) throws JSONException, Exception {

		String q = "SELECT MIN(" + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + ") " + " FROM "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME + " WHERE "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT + " = ? ";
		String args[] = null;

		// q += " AND "
		// +
		// EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN
		// + " != ? ";
		args = new String[] { idCorporacao };

		q += " LIMIT 0,1 ";

		Cursor c = null;
		try {
			c = database.rawQuery(q, args);
			if (c.moveToFirst()) {
				return c.getString(0);
			}
		} finally {
			if (c != null && !c.isClosed())
				c.close();
		}
		return null;
	}

	public static Integer[] getIdDoRegistroNaFilaDeSincronizacao(Database db, String pIdTupla, String pNomeTabela,
			Boolean sincronizando, OmegaLog log) {
		if (pIdTupla == null || pIdTupla.length() == 0)
			return null;
		else if (pNomeTabela == null || pNomeTabela.length() == 0)
			return null;

		String query = "SELECT " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + ", "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN + " FROM "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME + " WHERE "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT + " = ? " + "	AND "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT + " = ? ";
		query += " AND " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT + " = ? ";
		String[] args = null;
		if (sincronizando != null) {
			if (sincronizando)
				query += " AND " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN + " = ? ";
			else
				query += " AND " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN + " != ? ";
			args = new String[] { pIdTupla, EXTDAOSistemaTabela.getIdSistemaTabela(db, pNomeTabela),
					EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT, "1" };

		} else {
			args = new String[] { pIdTupla, EXTDAOSistemaTabela.getIdSistemaTabela(db, pNomeTabela),
					EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT };
		}

		Integer[] rs = db.getResultSetDoPrimeiroObjetoAsInteger(query, args, 2);
		String strRs = rs == null ? "nuo encontrou" : "encontrou";
		if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("getIdDoRegistroNaFilaDeSincronizacao - Rs: " + strRs);
		return rs;
	}

	public ArrayList<Table> getRegistrosReferenteATupla(String pNomeTabela, String pId) {
		clearData();
		setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT,
				EXTDAOSistemaTabela.getIdSistemaTabela(database, pNomeTabela));
		setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT, pId);
		setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT,
				OmegaSecurity.getIdCorporacao());
		return getListTable();
	}

	static String idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb = null;

	public String getIdDoUltimoSistemaRegistroSincronizadorAndroidParaWeb() throws JSONException, Exception {
		if (idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb == null) {
			SincronizadorEspelho sinc = SincronizadorEspelho.getInstance(getDatabase().getContext());
			Item item = sinc.getItem(getDatabase().getContext());
			Long id = item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb;
			idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb = id == null ? "" : String.valueOf(id);
		}

		return idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb.length() == 0 ? null
				: idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb;
	}

	public boolean isRegistroLocal(String strIdTabelaWeb, String idSistemaTabela, OmegaLog log) {
		// Long idTabelaWeb = Long.parseLong(strIdTabelaWeb);
		Long idTabelaWeb = HelperInteger.parserLong(strIdTabelaWeb);
		if (idTabelaWeb == null)
			return true;
		// PREMISSA ADOTADA, AS INSERuuES DE TABELAS QUE NAO SAO DO SISTEMA
		// LOCALMENTE u REALIZADA COM A UTILIZACAO DE ID NEGATIVO
		if (idTabelaWeb < 0)
			return true;
		else
			return false;

		// TODO modo antigo de busca do registor local - NAO DELETARRRR
		// Cursor cursor = null;
		// try {
		//
		//
		// String args[] = null;
		// String query = " SELECT
		// "+EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT+"
		// " + " FROM " + NAME + " as t LEFT JOIN "
		// + EXTDAOSistemaCrudEstado.NAME + " ce ON t.id = ce."
		// + EXTDAOSistemaCrudEstado.ID_SINCRONIZADOR_MOBILE_INT
		// + " WHERE ";
		//
		// query += " (ce.id IS NULL "
		// + " OR ce."
		// + EXTDAOSistemaCrudEstado.ID_ESTADO_CRUD_INT
		// + " = ? "
		// + " ) AND "
		// + " t."
		// + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT
		// + " = ? AND "
		// + " t."
		// +
		// EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT
		// + " = ? AND "
		// + " t."
		// +
		// EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT
		// + " IN ( ?, ? ) AND "
		// + " t."
		// + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT
		// + " = ? " +
		// " ORDER BY t.id DESC " +
		// " LIMIT 0,1";
		//
		// args = new String[] {
		// EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO.getStrId(),
		// strIdTabelaWeb, idSistemaTabela,
		// EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT,
		// EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE,
		// OmegaSecurity.getIdCorporacao() };
		//
		// cursor = database.rawQuery(query, args);
		// int tipoOperacao = 0;
		// boolean local = false;
		// if (cursor.moveToFirst()) {
		//
		//
		// tipoOperacao = cursor.getInt(0);
		//
		// if(tipoOperacao ==
		// EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT.getId()){
		// local = true;
		// }
		// else if (tipoOperacao ==
		// EXTDAOSistemaTipoOperacaoBanco.TIPO.REMOVE.getId()){
		// local = false;
		// }
		// //nesse caso o registro possui tanto a operauuo de inseruuo quanto a
		// de remouuo.
		// if(!local){
		// log.escreveLog("O registro (Id Sistema Tabela: "+idSistemaTabela+".
		// Id: "+strIdTabelaWeb+") possui tanto registro de sincronizacao de
		// operauuo de remouuo quanto inseruuo, logo nuo u local");
		//
		// }
		//
		// }
		// return local;
		//
		// } finally {
		// if (cursor != null)
		// cursor.close();
		// }

	}

	// public Long getId(String idTabela, String idSistemaTabela, String
	// idTipoOperacao) {
	// return getId(idTabela, idSistemaTabela, idTipoOperacao, null);
	// }
	public Long getId(String idTabela, String idSistemaTabela, String idTipoOperacao, Boolean sincronizando,
			OmegaLog log) throws JSONException, Exception {
		Cursor cursor = null;
		try {
			// ultimoIdSincronizado =

			String args[] = null;
			String query = " SELECT t.id " + " FROM " + NAME + " as t" + " WHERE " + " t."
					+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT + " = ? AND " + " t."
					+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT + " = ? AND " + " t."
					+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT + " = ? AND "
					+ " t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT + " = ? ";

			// so utiliza o limitante ultimoId se for restaurar registros que
			// estao sendo sincronizados
			String idSinc = getIdDoUltimoSistemaRegistroSincronizadorAndroidParaWeb();

			if (idSinc != null && sincronizando != null && sincronizando == true) {

				query += " AND t. " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + " > ? ";
				if (sincronizando != null) {
					String valorSinc = "1";
					if (sincronizando)
						query += " AND t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN
								+ " = ? ";
					else
						query += " AND t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN
								+ " != ? ";

					args = new String[] { idTabela, idSistemaTabela, idTipoOperacao, OmegaSecurity.getIdCorporacao(),
							idSinc, valorSinc };
				} else
					args = new String[] { idTabela, idSistemaTabela, idTipoOperacao, OmegaSecurity.getIdCorporacao(),
							idSinc };

			} else {
				args = new String[] { idTabela, idSistemaTabela, idTipoOperacao, OmegaSecurity.getIdCorporacao() };
				if (sincronizando != null) {
					String valorSinc = "1";
					if (sincronizando)
						query += " AND t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN
								+ " = ? ";
					else
						query += " AND t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN
								+ " != ? ";

					args = new String[] { idTabela, idSistemaTabela, idTipoOperacao, OmegaSecurity.getIdCorporacao(),
							valorSinc };
				} else
					args = new String[] { idTabela, idSistemaTabela, idTipoOperacao, OmegaSecurity.getIdCorporacao() };
			}

			cursor = database.rawQuery(query, args);
			if (cursor.moveToFirst()) {
				long res = cursor.getLong(0);
				return res;
			} else
				return null;

		} finally {
			if (cursor != null)
				cursor.close();
		}

	}

	public Long getIdPeloIdMobileOriginal(String idTabelaMobileOriginal, String idSistemaTabela, String idTipoOperacao,
			Boolean sincronizando, OmegaLog log) throws JSONException, Exception {
		Cursor cursor = null;
		try {
			// ultimoIdSincronizado =

			String args[] = null;
			String query = " SELECT * " + " FROM " + NAME + " as t" + " WHERE " + " t."
					+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_MOBILE_INT + " = ? AND " + " t."
					+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT + " = ? AND " + " t."
					+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT + " = ? AND "
					+ " t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT + " = ? ";

			// so utiliza o limitante ultimoId se for restaurar registros que
			// estao sendo sincronizados
			String idSinc = getIdDoUltimoSistemaRegistroSincronizadorAndroidParaWeb();

			if (idSinc != null && sincronizando != null && sincronizando == true) {

				query += " AND t. " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + " > ? ";
				if (sincronizando != null) {
					String valorSinc = "1";
					if (sincronizando)
						query += " AND t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN
								+ " = ? ";
					else
						query += " AND t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN
								+ " != ? ";

					args = new String[] { idTabelaMobileOriginal, idSistemaTabela, idTipoOperacao,
							OmegaSecurity.getIdCorporacao(), idSinc, valorSinc };
				} else
					args = new String[] { idTabelaMobileOriginal, idSistemaTabela, idTipoOperacao,
							OmegaSecurity.getIdCorporacao(), idSinc };

			} else {
				args = new String[] { idTabelaMobileOriginal, idSistemaTabela, idTipoOperacao,
						OmegaSecurity.getIdCorporacao() };
				if (sincronizando != null) {
					String valorSinc = "1";
					if (sincronizando)
						query += " AND t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN
								+ " = ? ";
					else
						query += " AND t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN
								+ " != ? ";

					args = new String[] { idTabelaMobileOriginal, idSistemaTabela, idTipoOperacao,
							OmegaSecurity.getIdCorporacao(), valorSinc };
				} else
					args = new String[] { idTabelaMobileOriginal, idSistemaTabela, idTipoOperacao,
							OmegaSecurity.getIdCorporacao() };
			}

			cursor = database.rawQuery(query, args);
			if (cursor.moveToFirst()) {
				long res = cursor.getLong(0);
				return res;
			} else
				return null;

		} finally {
			if (cursor != null)
				cursor.close();
		}

	}

	public Long getIdPeloIdMobileAtualEOriginal(String idTabelaMobileAtual, String idTabelaMobileOriginal,
			String idSistemaTabela, String idTipoOperacao, Boolean sincronizando, OmegaLog log)
			throws JSONException, Exception {
		Cursor cursor = null;
		try {
			// ultimoIdSincronizado =

			String args[] = null;
			String query = " SELECT * " + " FROM " + NAME + " as t" + " WHERE " + " t."
					+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_MOBILE_INT + " = ? AND " + " t."
					+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT + " = ? AND " + " t."
					+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT + " = ? AND " + " t."
					+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT + " = ? AND "
					+ " t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT + " = ? ";

			// so utiliza o limitante ultimoId se for restaurar registros que
			// estao sendo sincronizados
			String idSinc = getIdDoUltimoSistemaRegistroSincronizadorAndroidParaWeb();

			if (idSinc != null && sincronizando != null && sincronizando == true) {

				query += " AND t. " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + " > ? ";
				if (sincronizando != null) {
					String valorSinc = "1";
					if (sincronizando)
						query += " AND t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN
								+ " = ? ";
					else
						query += " AND t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN
								+ " != ? ";

					args = new String[] { idTabelaMobileOriginal, idTabelaMobileAtual, idSistemaTabela, idTipoOperacao,
							OmegaSecurity.getIdCorporacao(), idSinc, valorSinc };
				} else
					args = new String[] { idTabelaMobileOriginal, idTabelaMobileAtual, idSistemaTabela, idTipoOperacao,
							OmegaSecurity.getIdCorporacao(), idSinc };

			} else {
				args = new String[] { idTabelaMobileOriginal, idSistemaTabela, idTipoOperacao,
						OmegaSecurity.getIdCorporacao() };
				if (sincronizando != null) {
					String valorSinc = "1";
					if (sincronizando)
						query += " AND t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN
								+ " = ? ";
					else
						query += " AND t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN
								+ " != ? ";

					args = new String[] { idTabelaMobileOriginal, idTabelaMobileAtual, idSistemaTabela, idTipoOperacao,
							OmegaSecurity.getIdCorporacao(), valorSinc };
				} else
					args = new String[] { idTabelaMobileOriginal, idTabelaMobileAtual, idSistemaTabela, idTipoOperacao,
							OmegaSecurity.getIdCorporacao() };
			}

			cursor = database.rawQuery(query, args);
			if (cursor.moveToFirst()) {
				long res = cursor.getLong(0);
				return res;
			} else
				return null;

		} finally {
			if (cursor != null)
				cursor.close();
		}
	}

	public void updateIdTabelaWebNaOperacaoDeInsert(String tableName, String idTabela, String idTabelaWeb)
			throws JSONException, Exception {
		String where = ID_TABELA_INT + " = ? " + " AND " + SISTEMA_TIPO_OPERACAO_ID_INT + " = ? " + " AND "
				+ CORPORACAO_ID_INT + " = ? " + " SET " + ID_TABELA_WEB_INT + " = ? ";
		String[] args = null;
		String ultimoId = getIdDoUltimoSistemaRegistroSincronizadorAndroidParaWeb();
		if (ultimoId != null) {
			where += "AND " + ID + " <= ? ";
			args = new String[] { idTabela, EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT,
					OmegaSecurity.getIdCorporacao(), idTabelaWeb, ultimoId };
		} else {
			args = new String[] { idTabela, EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT,
					OmegaSecurity.getIdCorporacao(), idTabelaWeb };
		}
		

		ContentValues cv = new ContentValues();
		cv.put(ID_TABELA_WEB_INT, idTabelaWeb);
		database.update(super.getName(), cv, where, args);
	}

	public boolean updateIdSynchronized(String tableName, String oldReferenceId, String oldId, String newId,
			OmegaLog log, boolean updateOldReferenceSync) throws JSONException, Exception {

		if (oldId != null && newId != null && oldId.equalsIgnoreCase(newId)) {
			return true;
		}

		if (newId != null) {
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("updateIdSynchronized " + tableName + " - " + oldId + " para " + newId);
			this.setAttrValue(ID_UNIVERSAL, newId);
		} else {
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("updateIdSynchronized: tranferindo o id " + tableName + " - " + oldId
						+ " para o proximo autoincremente.");
		}

		// Atualiza todos os atributos da tabela
		if (oldId == null)
			return false;
		else {
			Database database = this.getDatabase();
			if (database == null)
				return false;
			else {

				ConflictUpdateId con = database.updateId(tableName, oldId, newId, log);
				if (con.numberRowsAffected <= 0) {
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("updateIdSynchronized - falha ao atualizar o id");
					return false;
				} else {
					if (newId == null && con.newIdGenerated != null) {
						newId = con.newIdGenerated;
						this.setAttrValue(ID_UNIVERSAL, con.newIdGenerated);
					}
					if (con.conflictedNewId != null) {
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
							log.escreveLog(
									"updateIdSynchronized - atualizou o id com sucesso. Verificando se existem registros de"
											+ " sincronizacao na pilha android-web para atualizar o respectivo id com o novo criado. "
											+ tableName + " - " + newId + " para " + con.conflictedNewId);
							log.escreveLog(
									"updateIdSynchronized - Temos que verificar se o mesmo possui registro na pilha de sincornizacao andorid->web.");
						}
						// insere na tabela sincronizador
						Boolean validade = atualizaIdASerSincronizadoSeExistente(tableName, null, newId,
								con.conflictedNewId, log);

						// Se nao existia registro a ser sincronizado
						if (validade == null) {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("updateIdSynchronized - nao existem registros na pilha 1.1 ");

						} else if (!validade) {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog(
										"updateIdSynchronized - falha durante atualizacao dos ids na pilha 1.1: Id "
												+ newId + " para " + con.conflictedNewId);
						} else {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog(
										"updateIdSynchronized - ids da pilha 1.1 foram atualizados: " + validade);

						}
						if (updateOldReferenceSync) {
							validade = atualizaIdASerSincronizadoSeExistente(tableName, oldReferenceId, oldId, newId,
									log);
							if (validade == null) {
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("updateIdSynchronized - nao existem registros na pilha 1.2 ");
								return true;

							} else if (!validade) {
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog(
											"updateIdSynchronized - falha durante atualizacao dos ids na pilha 1.2: Id "
													+ oldId + " para " + newId);
							} else {
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog(
											"updateIdSynchronized - ids da pilha 1.2 foram atualizados: " + validade);

							}
						} else {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("updateIdSynchronized - nuo atualizaremos os id's sync relativos ao id: "
										+ oldId);
						}

						return validade;
					} else {
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog(
									"updateIdSynchronized - Sucesso! Nao houve id conflitante que teve que ser corrigido.");
						Boolean validade = atualizaIdASerSincronizadoSeExistente(tableName, oldReferenceId, oldId,
								newId, log);
						// Se nao existia registro a ser sincronizado
						if (validade == null) {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("updateIdSynchronized - nao existem registros na pilha_2");
							return true;
						} else {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("updateIdSynchronized - ids da pilha_2 foram atualizados com sucesso");
							return validade;
						}

					}

				}

			}
		}
	}

	public Boolean atualizaIdASerSincronizadoSeExistente(String tableName, String tabelaIdMobileReferencia,
			String tabelaIdMobileAtual, String tabelaIdNovo, OmegaLog log) throws JSONException, Exception {
		if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("atualizaIdASerSincronizadoSeExistente" + ". tableName: " + tableName
					+ ". tabelaIdMobileReferencia: " + tabelaIdMobileReferencia + ". tabelaIdMobileAtual: "
					+ tabelaIdMobileAtual + ". tabelaIdNovo: " + tabelaIdNovo);
		String idTabelaSincronizador = EXTDAOSistemaTabela.getIdSistemaTabela(getDatabase(), tableName);
		ArrayList<String> idsSincronizador = null;

		RetornoOperacoesNaoSincronizado ret = existeRegistroDeInsercaoERemocaoNaoSincronizado(idTabelaSincronizador,
				tabelaIdNovo, tabelaIdNovo, log);

		if (ret.existeInsercao && ret.existeRemocao) {
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog(
						"Existem operauuo nao sincronizadas de insercao e remocao de um registro do novo id a ser ocupado, "
								+ tableName + "[" + tabelaIdNovo
								+ "]. Logo iremos remover os registros do sincronizador relacionados...");
			int totalRemovido = removerTodosAsOperacoesDoRegistro(tabelaIdNovo, tabelaIdNovo, idTabelaSincronizador);
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Total removido: " + totalRemovido);
		} else {
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog(
						"Nuo existem operauuo nao sincronizadas de [insercao && remocao] de um registro do novo id a ser ocupado, "
								+ tableName + "[" + tabelaIdNovo + "] . ");
		}

		if (tabelaIdMobileAtual != null
				&& (tabelaIdMobileReferencia == null || tabelaIdMobileReferencia.compareTo(tabelaIdMobileAtual) < 0)) {
			if (tabelaIdNovo.compareTo(tabelaIdMobileAtual) > 0)
				updateSequence(HelperInteger.parserLong(tabelaIdNovo));
			else
				updateSequence(HelperInteger.parserLong(tabelaIdMobileAtual));
		} else if (tabelaIdMobileReferencia != null) {
			if (tabelaIdNovo.compareTo(tabelaIdMobileReferencia) > 0)
				updateSequence(HelperInteger.parserLong(tabelaIdNovo));
			else
				updateSequence(HelperInteger.parserLong(tabelaIdMobileReferencia));
		}

		if (tabelaIdMobileReferencia != null) {

			idsSincronizador = getIdsSistemaRegistroSincronizadorAndroidParaWebPeloIdMobileOriginal(
					idTabelaSincronizador, tabelaIdMobileReferencia);
			if (idsSincronizador == null || idsSincronizador.size() == 0) {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("[ewrtyeruy]Nao existem registros de sincronizacao do registro " + tableName + "["
							+ tabelaIdMobileReferencia + "] que havia sido enviado, que atualmente ocupa o id "
							+ tableName + "[" + tabelaIdMobileAtual + "]");
				return null;
			} else {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("[ewrt234yeruy]Existem " + idsSincronizador.size()
							+ " registros de sincronizacao do registro " + tableName + "[" + tabelaIdMobileReferencia
							+ "] que havia sido enviado, que atualmente ocupa o id " + tableName + "["
							+ tabelaIdMobileAtual + "]");
			}
		} else {

			idsSincronizador = getIdsSistemaRegistroSincronizadorAndroidParaWebPeloIdMobileAtual(idTabelaSincronizador,
					tabelaIdMobileAtual);
			if (idsSincronizador == null || idsSincronizador.size() == 0) {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("[wety5e435t]Nao existem registros de sincronizacao do registro " + tableName + "["
							+ tabelaIdMobileAtual + "]");
				return null;
			} else {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("[wety5et]Existem " + idsSincronizador.size()
							+ " registros de sincronizacao do registro " + tableName + "[" + tabelaIdMobileAtual + "]");
			}
		}

		EXTDAOSistemaRegistroSincronizadorAndroidParaWeb sincronizador = (EXTDAOSistemaRegistroSincronizadorAndroidParaWeb) factory();
		boolean validade = true;
		if (idsSincronizador != null) {
			for (int i = 0; i < idsSincronizador.size(); i++) {
				String idSincronizacao = idsSincronizador.get(i);
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
					if (tabelaIdMobileReferencia != null)
						log.escreveLog(
								"[wetyt28453]Atualizacao do registro sistema_registro_sincronizador_android_para_web["
										+ idSincronizacao + "], alterando o valor do atributo " + ID_TABELA_INT + " = "
										+ tabelaIdNovo + " relacionada ao registro " + tableName + "["
										+ tabelaIdMobileReferencia + "]");
					else
						log.escreveLog(
								"[wetyt23]Atualizacao do registro sistema_registro_sincronizador_android_para_web["
										+ idSincronizacao + "], alterando o valor do atributo " + ID_TABELA_INT + " = "
										+ tabelaIdNovo + " relacionada ao registro " + tableName + "["
										+ tabelaIdMobileAtual + "]");
				}
				if (sincronizador.select(idSincronizacao)) {
					sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT,
							tabelaIdNovo);
					if (!sincronizador.update(false)) {
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog(
									"[wetyt23]Falha durante a  atualizacao do registro sistema_registro_sincronizador_android_para_web "
											+ idSincronizacao);
						if (validade)
							validade = false;
					}
				}
			}
		}

		EXTDAOSistemaRegistroHistorico objSRH = new EXTDAOSistemaRegistroHistorico(getDatabase());
		int totalAtualizacao = 0;
		if (tabelaIdMobileReferencia != null) {
			totalAtualizacao = objSRH.atualizaIdsDoRegistroPeloIdMobileOriginal(idTabelaSincronizador,
					tabelaIdMobileReferencia, tabelaIdNovo);
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[tabelaIdMobileReferencia] Foram atualizado " + totalAtualizacao + " registros: "
						+ tabelaIdMobileReferencia + " para " + tabelaIdNovo);
		} else {
			totalAtualizacao = objSRH.atualizaIdsDoRegistroPeloIdMobileAtual(idTabelaSincronizador, tabelaIdMobileAtual,
					tabelaIdNovo);
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[tabelaIdMobileAtual] Foram atualizado " + totalAtualizacao + " registros: "
						+ tabelaIdMobileAtual + " para " + tabelaIdNovo);
		}
		if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
			if (totalAtualizacao > 0) {
				log.escreveLog("Foram atualizados " + totalAtualizacao + " registros");
			} else {
				log.escreveLog(
						"Nao houve atualizacoes de registros da tabela sistema_registro_sincronizador_android_para_web");
			}
		}
		return validade;
	}

	public ArrayList<String> getIdsSistemaRegistroSincronizadorAndroidParaWebPeloIdMobileAtual(
			String idTabelaSincronizador, String idTabelaInt
	// , boolean utilizarLimiteDaUltimaSinc
	) throws JSONException, Exception {

		ArrayList<String> ids = new ArrayList<String>();

		String q = "SELECT  " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + " FROM "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME + " WHERE "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT + " = ? " + " AND "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT + " = ? " + " AND "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT + " = ? ";

		String[] args = null;
		args = new String[] { idTabelaInt, idTabelaSincronizador, OmegaSecurity.getIdCorporacao() };

		Cursor c = null;

		try {
			c = database.rawQuery(q, args);
			while (c.moveToNext()) {
				String id = c.getString(0);
				ids.add(id);
			}
			return ids;
		} finally {
			if (c != null) {
				c.close();
			}
		}
	}

	public ArrayList<String> getIdsSistemaRegistroSincronizadorAndroidParaWebPeloIdMobileOriginal(
			String idTabelaSincronizador, String idTabelaMobileReferenciaInt
	// , boolean utilizarLimiteDaUltimaSinc
	) throws JSONException, Exception {

		ArrayList<String> ids = new ArrayList<String>();

		String q = "SELECT  " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + " FROM "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME + " WHERE "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_MOBILE_INT + " = ? " + " AND "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT + " = ? " + " AND "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT + " = ? ";

		String[] args = null;
		args = new String[] { idTabelaMobileReferenciaInt, idTabelaSincronizador, OmegaSecurity.getIdCorporacao() };

		Cursor c = null;

		try {
			c = database.rawQuery(q, args);
			while (c.moveToNext()) {
				String id = c.getString(0);
				ids.add(id);
			}
			return ids;
		} finally {
			if (c != null) {
				c.close();
			}
		}
	}

	public class DadosInsercaoRemocao {
		public String id;
		public Integer idTipoOperacao;
		public Integer idTabelaWeb;
		boolean existiaInsercao = false;
	}

	public class RetornoOperacoesNaoSincronizado {

		public boolean existeInsercao = false;
		public boolean existeRemocao = false;

	}

	public RetornoOperacoesNaoSincronizado existeRegistroDeInsercaoERemocaoNaoSincronizado(String idTabelaSincronizador,
			String id, String idMobileOriginal, OmegaLog log) {

		String q = "SELECT  distinct("

				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT + ") " + " FROM "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME + " WHERE "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT + " = ? " + " 	AND "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_MOBILE_INT + " = ? " + " 	AND "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT + " = ? " + " 	AND "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT + " = ? "
				// caso da remocao ou insercao
				+ " AND " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT
				+ " IN (?, ?) " + " AND " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN
				+ " != ? ";

		String[] args = null;
		args = new String[] { id, idMobileOriginal, idTabelaSincronizador, OmegaSecurity.getIdCorporacao(),
				EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE,
				EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT, "1" };

		Cursor c = null;

		try {
			c = database.rawQuery(q, args);

			int intInsert = Integer.parseInt(EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT);
			int intRemocao = Integer.parseInt(EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE);

			RetornoOperacoesNaoSincronizado ret = new RetornoOperacoesNaoSincronizado();
			while (c.moveToNext()) {

				int idSistemaTipoOp = c.getInt(0);

				if (idSistemaTipoOp == intInsert) {
					ret.existeInsercao = true;
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog(
								"existeRegistroDeInsercaoERemocaoNaoSincronizado::Existe insercao armazenada do id: "
										+ id);

				} else if (idSistemaTipoOp == intRemocao) {
					ret.existeRemocao = true;
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog(
								"existeRegistroDeInsercaoERemocaoNaoSincronizado::Existe remocao armazenada do id: "
										+ id);
				}

			}
			return ret;
		} finally {
			if (c != null && !c.isClosed())
				c.close();
		}

	}

	public DadosInsercaoRemocao getIdsDasOperacoesDeInsercaoERemocao(String vIdTabelaSincronizador, String idTabelaInt)
			throws JSONException, Exception {

		String ultimoId = getIdDoUltimoSistemaRegistroSincronizadorAndroidParaWeb();

		String q = "SELECT  " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + ", "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT + ", "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_WEB_INT + " FROM "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME + " WHERE "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT + " = ? " + " 	AND "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT + " = ? " + " 	AND "
				+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT + " = ? "
				// caso da remocao
				+ " AND (" + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT + " = ? ";
		if (ultimoId != null) {
			// caso da insercao que estu em processo de sinc
			q += " OR (" + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT + " = ? "
					+ " AND " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + " <= ? " + " AND "
					+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN + " = ? " + " ) ";
		}
		q += " ) ";

		String[] args = null;
		if (ultimoId != null) {

			args = new String[] { idTabelaInt, vIdTabelaSincronizador, OmegaSecurity.getIdCorporacao(),
					EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE,
					EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT, ultimoId, "1" };
		} else {
			args = new String[] { idTabelaInt, vIdTabelaSincronizador, OmegaSecurity.getIdCorporacao(),
					EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE, ultimoId };
		}

		Cursor c = null;

		try {
			c = database.rawQuery(q, args);

			DadosInsercaoRemocao dadosInsercao = null;
			DadosInsercaoRemocao dadosRemocao = null;
			int intInsert = Integer.parseInt(EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT);
			int intRemocao = Integer.parseInt(EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE);
			while (c.moveToNext()) {
				DadosInsercaoRemocao dados = new DadosInsercaoRemocao();
				String id = c.getString(0);
				dados.id = id;
				int idSistemaTipoOp = c.getInt(1);

				dados.idTipoOperacao = idSistemaTipoOp;
				if (!c.isNull(2)) {
					int idTabelaWeb = c.getInt(2);
					dados.idTabelaWeb = idTabelaWeb;
				}
				if (idSistemaTipoOp == intInsert)
					dadosInsercao = dados;
				else if (idSistemaTipoOp == intRemocao)
					dadosRemocao = dados;

			}
			if (dadosRemocao != null && dadosInsercao != null)
				dadosRemocao.existiaInsercao = true;
			// prioridade para remcoao
			return dadosRemocao == null ? dadosInsercao : dadosRemocao;
		} finally {
			if (c != null)
				c.close();
		}
	}

	// protected ArrayList<Long> getListIdWithInsertAction() {
	// Cursor cursor = null;
	// try{
	//
	// String q = " SELECT * " +
	// " FROM " + NAME + " as t " +
	// " WHERE " +
	// " t." +
	// EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT
	// + " = ? " +
	// " AND t." +
	// EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT +
	// " = ? " ;
	// String args[] = null;
	// String ultimoId =
	// getIdDoUltimoSistemaRegistroSincronizadorAndroidParaWeb();
	// if(ultimoId != null){
	// q += " AND t." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID +
	// " > ? ";
	// args = new String[]
	// {EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT,
	// OmegaSecurity.getIdCorporacao(), ultimoId};
	// } else {
	// args = new String[]
	// {EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT,
	// OmegaSecurity.getIdCorporacao()};
	// }
	//
	//
	// cursor = database.rawQuery(q, args);
	//
	// String[] vetorChave = new String[] { EXTDAOEmpresa.ID };
	//
	// return HelperDatabase.convertCursorToArrayListId(cursor, vetorChave);
	//
	//
	// } catch (Exception e) {
	// SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
	// }finally{
	// if(cursor != null && ! cursor.isClosed()){
	// cursor.close();
	// }
	// }
	//
	// return null;
	// }

	public boolean isEmptyDataOfTableInSincronizador(String pIdSistemaTabela) {

		Cursor cursor = null;
		try {
			cursor = database.query(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME,
					new String[] { ID, CORPORACAO_ID_INT },
					EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT + " = ? AND "
							+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT + " = "
							+ OmegaSecurity.getIdCorporacao(),
					new String[] { pIdSistemaTabela }, "", "", null, "1");
			boolean validade = true;

			if (cursor.moveToFirst()) {

				do {
					validade = false;
					break;

				} while (cursor.moveToNext());
			}

			return validade;
		} finally {
			if (cursor != null)
				cursor.close();
		}

	}

	public ArrayList<String> getListTableToSync() {
		return getListTableToSync(null);
	}
	public ArrayList<String> getListTableToSync(String ultimoId) {
		return getListTableToSync(ultimoId, OmegaSecurity.getIdCorporacao());
	}
	public ArrayList<String> getListTableToSync(String ultimoId, String idCorporacao) {
		ArrayList<String> vList = new ArrayList<String>();
		Cursor cursor = null;
		try {
			String vQuery = "SELECT DISTINCT(t." + EXTDAOSistemaTabela.NOME + ") " + " FROM "
					+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME + " AS rs " + " INNER JOIN "
					+ EXTDAOSistemaTabela.NAME + " AS t " + "		ON rs."
					+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT + " = t."
					+ EXTDAOSistemaTabela.ID + " WHERE rs."
					+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT + " = ? ";
			if (ultimoId != null) {
				vQuery += " AND rs." + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + " <= ? ";
				cursor = database.rawQuery(vQuery, new String[] { idCorporacao, ultimoId });
			} else {
				cursor = database.rawQuery(vQuery, new String[] { idCorporacao});
			}

			String vStrNameTable = null;
			if (cursor.moveToFirst()) {

				do {
					vStrNameTable = cursor.getString(0);
					if (vStrNameTable != null && vStrNameTable.length() > 0)
						vList.add(vStrNameTable);
				} while (cursor.moveToNext());
			}

			return vList;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public ContainerFastSendData getContainerFastSendDataOfTable(String pTableName) throws Exception {
		String idSistemaTabela = EXTDAOSistemaTabela.getIdSistemaTabela(database, pTableName);
		if (idSistemaTabela == null)
			return null;
		else if (isEmptyDataOfTableInSincronizador(idSistemaTabela))
			return null;
		Database vDatabase = database;

		ArrayList<ContainerEXTDAO> vListContainerQueryInsert = new ArrayList<ContainerEXTDAO>();
		ArrayList<ContainerEXTDAO> vListContainerQueryUpdate = new ArrayList<ContainerEXTDAO>();
		ArrayList<ContainerEXTDAO> vListContainerQueryRemove = new ArrayList<ContainerEXTDAO>();
		HashSet<String> idsInseridos = new HashSet<String>();
		HashSet<String> idsRemovidos = new HashSet<String>();
		// Lista de tabelas para serem removidas
		this.clearData();
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT,
				EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE);
		// this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.USUARIO_ID_INT,
		// OmegaSecurity.getIdUsuario());
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT, idSistemaTabela);
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT,
				OmegaSecurity.getIdCorporacao());
		Table objTable = vDatabase.factoryTable(pTableName);
		ContainerEXTDAO vContainerEXTDAORemove = new ContainerEXTDAO(pTableName);
		ArrayList<Table> vListTableToRemove = this
				.getListTable(new String[] { EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID });
		for (Table vTupleToRemove : vListTableToRemove) {
			String vIdSincronizador = vTupleToRemove
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID);
			String vId = vTupleToRemove
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT);

			objTable.clearData();
			objTable.setAttrValue(ID_UNIVERSAL, vId);

			vContainerEXTDAORemove.addCorpo(vIdSincronizador, vId);
			idsRemovidos.add(vId);
		}

		if (vContainerEXTDAORemove.isInitialized())
			vListContainerQueryRemove.add(vContainerEXTDAORemove);

		// Lista de tabelas para serem enviadas
		this.clearData();
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT,
				EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT);
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.USUARIO_ID_INT,
				OmegaSecurity.getIdUsuario());
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT, idSistemaTabela);
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT,
				OmegaSecurity.getIdCorporacao());
		// pega toda tupla da tabela em questao para ser inserida
		ArrayList<Table> vListTableToInsert = this
				.getListTable(new String[] { EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID });
		// this.clearData();
		// this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT,
		// vIdSistemaTabela);
		// ArrayList<Table> vListTableToInsert2 = this.getListTable(new
		// String[]{EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID});

		ContainerEXTDAO vContainerEXTDAOInsert = new ContainerEXTDAO(pTableName);

		// Para cada tupla de interesse
		for (Table vTupleToInsert : vListTableToInsert) {
			String vIdSincronizador = vTupleToInsert
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID);
			String vId = vTupleToInsert
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT);

			if (idsInseridos.contains(vId))
				continue;
			else if (idsRemovidos.contains(vId)) {

				this.clearData();
				// retira a referencia de insercao
				this.remove(vIdSincronizador, false);
				continue;
			}
			objTable.clearData();
			objTable.setAttrValue(ID_UNIVERSAL, vId);
			if (objTable.select()) {
				idsInseridos.add(vId);
				vContainerEXTDAOInsert.addCorpo(vIdSincronizador, vId);
			}

		}
		if (vContainerEXTDAOInsert.isInitialized())
			vListContainerQueryInsert.add(vContainerEXTDAOInsert);

		this.clearData();
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT,
				EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_EDIT);
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.USUARIO_ID_INT,
				OmegaSecurity.getIdUsuario());
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT, idSistemaTabela);
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT,
				OmegaSecurity.getIdCorporacao());
		// pega toda tupla da tabela em questao para ser inserida
		ArrayList<Table> vListTableToUpdate = this
				.getListTable(new String[] { EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID });
		ContainerEXTDAO vContainerEXTDAOUpdate = new ContainerEXTDAO(pTableName);

		// Para cada tupla de interesse
		for (Table vTupleToUpdate : vListTableToUpdate) {
			String vIdSincronizador = vTupleToUpdate
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID);
			String vId = vTupleToUpdate
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT);
			if (idsInseridos.contains(vId)) {
				// Foi inserido antes de sincronizar, logo a consulta de
				// insercao ja satisfaz essa situacao
				this.clearData();
				this.remove(vIdSincronizador, false);
				continue;
			} else if (idsRemovidos.contains(vId)) {
				// vContainerEXTDAORemove.removeCorpo(vId);
				// Foi removido antes de sincronizar
				this.clearData();
				this.remove(vIdSincronizador, false);
				continue;
			} else {
				objTable.clearData();

				if (objTable.select(vId)) {
					vContainerEXTDAOUpdate.addCorpo(vIdSincronizador, vId);

				}
			}
		}
		if (vContainerEXTDAOUpdate.isInitialized())
			vListContainerQueryUpdate.add(vContainerEXTDAOUpdate);

		return new ContainerFastSendData(vListContainerQueryInsert, vListContainerQueryUpdate,
				vListContainerQueryRemove);
	}

	public ContainerQueryEXTDAO getContainerRemove(OmegaLog log, String pNomeTabela, String idSistemaTabela,
			HashMap<Integer, Integer> idsRemovidos, HashMap<Integer, Integer> idsRemovidosPorMobileOriginal,
			Long ultimoIdSincronizador) {

		this.clearData();
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT,
				EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE);

		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT, idSistemaTabela);
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT,
				OmegaSecurity.getIdCorporacao());

		ContainerQueryEXTDAO container = new ContainerQueryEXTDAO(pNomeTabela);
		ArrayList<Table> vListTableToRemove = this.getListTableComIdMenoOuIgualrQue(
				new String[] { EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID }, false,
				ultimoIdSincronizador.toString());
		for (Table vTupleToRemove : vListTableToRemove) {
			String strIdSincronizador = vTupleToRemove
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID);
			String strIdTabelaInt = vTupleToRemove
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT);
			String strIdTabelaMobileInt = vTupleToRemove
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_MOBILE_INT);
			Integer idTabelaInt = HelperInteger.parserInteger(strIdTabelaInt);
			Integer idTabelaMobileInt = HelperInteger.parserInteger(strIdTabelaMobileInt);
			Integer idSincronizador = HelperInteger.parserInteger(strIdSincronizador);
			if (idTabelaInt == null
					|| (idsRemovidos.containsKey(idTabelaInt) && idsRemovidos.get(idTabelaInt) == idTabelaMobileInt)) {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("[REMOVIDO Y] cancelado registro da operauuo de remocao " + vTupleToRemove.getName()
							+ "[" + strIdSincronizador + "] cujo registro ID_TABELA_INT u " + idTabelaInt);
				continue;
			} else {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("[OK] registro da operauuo de remocao " + vTupleToRemove.getName() + "["
							+ strIdSincronizador + "] cujo registro ID_TABELA_INT u " + idTabelaInt);
			}
			container.addCorpo(strIdSincronizador, strIdTabelaInt, strIdTabelaMobileInt);
			idsRemovidos.put(idTabelaInt, idSincronizador);
			idsRemovidosPorMobileOriginal.put(idTabelaInt, idTabelaMobileInt);
		}
		return container;
	}

	public boolean isSincronizacaoDoRegistroCancelada() {
		String strSincronizando = getStrValueOfAttribute(
				EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN);
		if (strSincronizando == null || strSincronizando.compareTo("0") == 0) {
			return true;
		} else
			return false;
	}

	public ContainerQueryEXTDAO getContainerEditComDependentes(OmegaLog log, Table table, String idSistemaTabela,
			HashMap<Integer, Integer> idsInseridosPorMobileOriginal,
			HashMap<Integer, Integer> idsInseridosPorSincronizador,
			HashMap<Integer, Integer> idsRemovidosPorMobileOriginal,
			HashMap<Integer, Integer> idsRemovidosPorSincronizador, ArrayList<String> tabelasASeremSincronizadas,
			Long ultimoIdSincronizador, ContainerQueryEXTDAO containerRemove, boolean somenteEnviandoDados)
			throws Exception {

		HashMap<Integer, Integer> idsEditados = new HashMap<Integer, Integer>();
		this.clearData();
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT,
				EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_EDIT);
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT, idSistemaTabela);
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT,
				OmegaSecurity.getIdCorporacao());
		// pega toda tupla da tabela em questao para ser inserida
		ArrayList<Table> listTableToUpdate = this.getListTableComIdMenoOuIgualrQue(
				new String[] { EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID }, false,
				ultimoIdSincronizador.toString());
		ContainerQueryEXTDAO containerQueryEXTDAOUpdate = new ContainerQueryEXTDAO(table.getName());
		if (listTableToUpdate == null || listTableToUpdate.size() == 0) {
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Nuo existem operauues de edit a serem enviadas do (mobile->web) na tabela: "
						+ table.getName());
			return containerQueryEXTDAOUpdate;
		}
		// Para cada tupla de interesse
		for (int iTabela = 0; iTabela < listTableToUpdate.size(); iTabela++) {
			Table tupleToUpdate = listTableToUpdate.get(iTabela);
			ArrayList<String> idsDependentes = new ArrayList<String>();
			ArrayList<String> atributosDependentes = new ArrayList<String>();

			String strIdSincronizador = tupleToUpdate
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID);
			Integer idSincronizador = HelperInteger.parserInteger(strIdSincronizador);
			// TODO remover o log pois u pesado
			String idSistemaTabelaAux = tupleToUpdate
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT);
			String strId = tupleToUpdate
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT);
			String strIdMobileOriginal = tupleToUpdate
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_MOBILE_INT);
			Integer id = HelperInteger.parserInteger(strId);
			Integer idMobileOriginal = HelperInteger.parserInteger(strIdMobileOriginal);
			if (id == null)
				continue;
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[ANALISANDO REGISTRO PARA SINCRONIZACAO 12edd6546] O id "
						+ EXTDAOSistemaTabela.getNomeSistemaTabela(database, idSistemaTabela) + "[" + strId
						+ "] do crud edit idSincronizador " + strIdSincronizador + ". Sendo o id " + id
						+ " . E idMobile: " + idMobileOriginal);
			if (idsInseridosPorMobileOriginal.containsKey(id)
					&& (idsInseridosPorMobileOriginal.get(id).equals(idMobileOriginal)
							|| idsInseridosPorSincronizador.get(id) < idSincronizador)) {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("[REMOVIDO D] ");
				if (strIdSincronizador != null) {
					Integer totalRemovido = remove(strIdSincronizador, false);
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
						if (totalRemovido != null && totalRemovido > 0) {
							log.escreveLog("[REMOVIDO D] totalRemovido: " + totalRemovido);
						} else {
							log.escreveLog("[REMOVIDO D] Warning - totalRemovido nulo ou zero. R.: " + totalRemovido);
						}
					}
				}
				continue;

			} else if ((idsRemovidosPorSincronizador.containsKey(id)
					&& idsRemovidosPorSincronizador.get(id) > idSincronizador)

					|| containerRemove.foiRemovido(id, idSincronizador)) {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("[REMOVIDO E] ");
				if (strIdSincronizador != null) {
					Integer totalRemovido = remove(strIdSincronizador, false);
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
						if (totalRemovido != null && totalRemovido > 0) {
							log.escreveLog("[REMOVIDO E1] Total " + totalRemovido);
						} else {
							log.escreveLog("[REMOVIDO E1] Warning Total nulo ou zero. R.: " + totalRemovido);
						}
					}
				}
				continue;
			} else if (idsEditados.containsKey(id) && idsEditados.get(id).equals(idMobileOriginal)) {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("[REMOVIDO F] ");
				if (strIdSincronizador != null) {
					Integer totalRemovido = remove(strIdSincronizador, false);
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
						if (totalRemovido != null && totalRemovido > 0) {
							log.escreveLog("[REMOVIDO F] totalRemovido: " + totalRemovido);
						} else {
							log.escreveLog("[REMOVIDO F] Warning - total nulo ou zero. R.: " + totalRemovido);
						}
					}
				}
				continue;
			} else {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
					if (idsInseridosPorMobileOriginal.containsKey(id)) {
						log.escreveLog("Obs65126.: Existe id em idsInseridos: Id: " + id + ". Id Mobile: "
								+ idsInseridosPorMobileOriginal.get(id) + ". Id Sincronizador: "
								+ idsInseridosPorSincronizador.get(id));
					}
					if (idsRemovidosPorSincronizador.containsKey(id)) {
						log.escreveLog("Obs2313.: Existe id em idsRemovidos: Id: " + id
								+ ". Id Op Remove no Sincronizador: " + idsRemovidosPorSincronizador.get(id));
					}
					if (idsEditados.containsKey(id)) {
						log.escreveLog("Obs6575.: Existe id em idsEditados: Id: " + id + ". Id Mobile: "
								+ idsEditados.get(id));
					}

					log.escreveLog("[OK] O id " + EXTDAOSistemaTabela.getNomeSistemaTabela(database, idSistemaTabelaAux)
							+ "[" + id + "] do crud edit idSincronizador " + strIdSincronizador + " foi considerado");
				}
				idsEditados.put(id, idMobileOriginal);
			}
			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb objSRSAW = (EXTDAOSistemaRegistroSincronizadorAndroidParaWeb) tupleToUpdate;
			boolean crudCancelado = objSRSAW.isSincronizacaoDoRegistroCancelada();
			boolean crudRemovido = false;
			if (!crudCancelado || somenteEnviandoDados) {
				table.clearData();

				if (table.select(id.toString())) {
					if (tabelasASeremSincronizadas != null && tabelasASeremSincronizadas.size() > 1) {
						ArrayList<Attribute> atributos = table.getListAttributeFK();
						if (atributos != null && atributos.size() > 0) {
							for (int i = 0; i < atributos.size(); i++) {
								Attribute a = atributos.get(i);
								for (int j = 0; j < tabelasASeremSincronizadas.size(); j++) {
									if (a.tableFK.equalsIgnoreCase(tabelasASeremSincronizadas.get(j))) {
										String idFK = a.getStrValue();
										Integer[] rs = getIdDoRegistroNaFilaDeSincronizacao(getDatabase(), idFK,
												a.tableFK, null, log);

										if (rs != null) {
											if (rs[1] == null || rs[1] == 0) {
												if (somenteEnviandoDados) {
													if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
														log.escreveLog(
																"[hrtcsad123]Entrou um registro do sincronizador que possui a FK "
																		+ a.getName() + "[" + idFK
																		+ "] que nuo estu sendo sincronizado nessa rodada. Como estamos apenas enviando os dados, uma exceuuo seru lanuada para PARAR o processo de sincronizacao. Para que na pruxima rodada o registro referente a FK faua parte do processo de sincronizacao, nuo havendo mais necessidade de cancelamento.");
													throw new SincronizacaoException(
															"Entrou um registro do sincronizador maior que o atual: "
																	+ ultimoIdSincronizador);
												} else {
													if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
														log.escreveLog("[lknsebvsner123]A FK " + a.getName() + "["
																+ idFK
																+ "] nuo estu na fila para sincronizacao. Logo o crud seru cancelado.");
													crudCancelado = true;
													break;
												}
											} else if (rs[1] != null && rs[1] == 1) {
												if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
													log.escreveLog("[lkmrtkbtklm123] Atributo FK dependente "
															+ a.getName() + "[" + idFK + "] do crud atual.");
												idsDependentes.add(rs[0].toString());
												atributosDependentes.add(a.getName());
											}
										}
									}
								}
								if (crudCancelado) {
									break;
								}
							}
						}
					}
					if (!crudCancelado || somenteEnviandoDados) {
						containerQueryEXTDAOUpdate.addCorpo(null, table.getValoresMenosId(), strIdSincronizador, strId,
								strIdMobileOriginal, idsDependentes, atributosDependentes);
					}
				} else {
					Integer idSincronizadorAux = null;
					boolean contemChaveRemovida = idsRemovidosPorSincronizador.containsKey(id);
					boolean contemChaveMobileRemovida = false;
					if (contemChaveRemovida) {
						idSincronizadorAux = idsRemovidosPorSincronizador.get(id);
						contemChaveMobileRemovida = idSincronizadorAux > idSincronizador;
					}

					Integer idMobileAux2 = null;
					Integer idSincronizadorAux2 = null;
					boolean contemChaveMobileInserida = false;
					boolean contemChaveInserida = idsInseridosPorMobileOriginal.containsKey(id);
					if (contemChaveInserida) {
						idMobileAux2 = idsInseridosPorMobileOriginal.get(id);
						idSincronizadorAux2 = idsInseridosPorSincronizador.get(id);
						contemChaveMobileInserida = idMobileAux2.equals(idMobileOriginal);
					}
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog(
								"Id inexistente na base, logo crud edit cancelado. Registro relacionado ao crud cancelado: "
										+ table.getName() + "[" + strId + "]. Id da op no sincronizador: "
										+ idSincronizador + " - contemChaveRemovida: " + contemChaveRemovida
										+ ". contemChaveMobileRemovida: " + contemChaveMobileRemovida
										+ " - Id Op Remove no Sincronizador: " + idSincronizadorAux
										+ ". Id mobile original: " + idMobileOriginal + " - contemChaveInserida: "
										+ contemChaveInserida + ". contemChaveMobileInserida: "
										+ contemChaveMobileInserida + " - Id da op insert no sincornizador: "
										+ idSincronizadorAux2 + ". IdMobileOriginal::id - " + idMobileAux2 + "::"
										+ idMobileOriginal);

					crudRemovido = true;
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO ) 
					log.escreveLog("Cancelado pois nuo existe mais na base local");
				}
			}
			if (!somenteEnviandoDados) {
				if (crudRemovido) {
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Removendo operacao edit...");
					remove(strIdSincronizador, false);
				} else if (crudCancelado) {
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Cancelando operacao edit...");
					String strUltimoIdSincronizador = ultimoIdSincronizador.toString();
					cancelaSincronizacaoInsertEditRelacionadasAoRegistro(idSistemaTabela, id.toString(),
							strUltimoIdSincronizador, listTableToUpdate, iTabela, log);

					cancelaSincronizacaoDosDependentesDoRegistro(idSistemaTabela, id.toString(),
							strUltimoIdSincronizador, log);
				}
				if (idsInseridosPorMobileOriginal.containsKey(id)) {
					idsInseridosPorMobileOriginal.remove(id);
					idsInseridosPorSincronizador.remove(id);
				}
				if (idsEditados.containsKey(id)) {
					idsEditados.remove(id);
				}
				if (idsRemovidosPorSincronizador.containsKey(id)) {
					idsRemovidosPorSincronizador.remove(id);
				}
			}

		}
		return containerQueryEXTDAOUpdate;
	}

	public ContainerQueryInsert getContainerInsertComDependetes(OmegaLog log, Table table, String idSistemaTabela,
			HashMap<Integer, Integer> idsInseridosPorMobileOriginal,
			HashMap<Integer, Integer> idsInseridosPorSincronizador,
			HashMap<Integer, Integer> idsRemovidosPorSincronizador, ArrayList<String> tabelasASeremSincronizadas,
			Long ultimoIdSincronizador, ContainerQueryEXTDAO containerRemove, boolean somenteEnviandoDados)
			throws Exception {

		String pNomeTabela = table.getName();
		// Lista de tabelas para serem enviadas
		this.clearData();
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT,
				EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT);
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT, idSistemaTabela);
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT,
				OmegaSecurity.getIdCorporacao());
		// pega toda tupla da tabela em questao para ser inserida
		ArrayList<Table> registrosInsertDaTabela = this.getListTableComIdMenoOuIgualrQue(
				new String[] { EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID }, false,
				ultimoIdSincronizador.toString());

		ContainerQueryInsert vContainer = new ContainerQueryInsert(pNomeTabela, null);
		if (registrosInsertDaTabela == null || registrosInsertDaTabela.size() == 0) {
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Nuo existem operauues de inseruuo a serem enviadas do (mobile->web) na tabela: "
						+ pNomeTabela);
			return vContainer;
		}

		// Para cada tupla de interesse
		for (int iRegistro = 0; iRegistro < registrosInsertDaTabela.size(); iRegistro++) {
			Table registroInsert = registrosInsertDaTabela.get(iRegistro);
			ArrayList<String> idsDependentes = new ArrayList<String>();
			ArrayList<String> atributosDependentes = new ArrayList<String>();
			String strIdSincronizador = registroInsert
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID);
			Integer idSincronizador = HelperInteger.parserInteger(strIdSincronizador);

			String strId = registroInsert
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT);
			String strIdMobile = registroInsert
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_MOBILE_INT);
			Integer id = HelperInteger.parserInteger(strId);
			Integer idMobileOriginal = HelperInteger.parserInteger(strIdMobile);

			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[ANALISANDO REGISTRO PARA SINCRONIZACAO] O id "
						+ EXTDAOSistemaTabela.getNomeSistemaTabela(database, idSistemaTabela) + "[" + strId + "] "
						+ "do crud insert idSincronizador " + strIdSincronizador + ". Sendo o id " + id
						+ " . E idMobile: " + idMobileOriginal + ". idSincronizador: " + idSincronizador);
			if (idsInseridosPorMobileOriginal.containsKey(id)
					&& idsInseridosPorMobileOriginal.get(id).equals(idMobileOriginal)) {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog(
							"[REMOVIDO Z] O id " + EXTDAOSistemaTabela.getNomeSistemaTabela(database, idSistemaTabela)
									+ "[" + strId + "] do crud insert idSincronizador " + strIdSincronizador
									+ " foi desconsiderado pois ju existe um crud insert do registro");
				continue;
			} else if (idsRemovidosPorSincronizador.containsKey(id)

					&& idsRemovidosPorSincronizador.get(id) > idSincronizador) {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog(
							"[REMOVIDO A] O id " + EXTDAOSistemaTabela.getNomeSistemaTabela(database, idSistemaTabela)
									+ "[" + strId + "] do crud insert idSincronizador " + strIdSincronizador
									+ " foi desconsiderado pois ju existe um crud remove do registro");
				// remove o crud remove mobile
				String idSincronizadorRemove = containerRemove.removeCorpo(log, strId, strIdMobile);
				if (idSincronizadorRemove != null) {

					Integer totalRemovido = remove(idSincronizadorRemove, false);
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
						if (totalRemovido != null && totalRemovido > 0) {
							log.escreveLog("[REMOVIDO A] O id "
									+ EXTDAOSistemaTabela.getNomeSistemaTabela(database, idSistemaTabela) + "[" + strId
									+ "] do crud remove idSincronizador " + strIdSincronizador
									+ " foi desconsiderado pois ju existe um crud remove do registro");
						} else {
							log.escreveLog("[REMOVIDO A] Warning - ao remover o id "
									+ EXTDAOSistemaTabela.getNomeSistemaTabela(database, idSistemaTabela) + "[" + strId
									+ "] do crud remove idSincronizador " + strIdSincronizador
									+ " foi desconsiderado pois ju existe um crud remove do registro");
						}
					}

				}
				// remove o crud insert mobile
				if (strIdSincronizador != null) {
					Integer totalRemovido = remove(strIdSincronizador, false);
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
						if (totalRemovido != null && totalRemovido > 0) {

							log.escreveLog("[REMOVIDO A2] O id "
									+ EXTDAOSistemaTabela.getNomeSistemaTabela(database, idSistemaTabela) + "[" + strId
									+ "] do crud insert idSincronizador " + strIdSincronizador
									+ " foi desconsiderado pois ju existe um crud remove do registro");
						} else {
							log.escreveLog("[REMOVIDO A2] Warning - ao remover o id "
									+ EXTDAOSistemaTabela.getNomeSistemaTabela(database, idSistemaTabela) + "[" + strId
									+ "] do crud insert idSincronizador " + strIdSincronizador
									+ " foi desconsiderado pois ju existe um crud remove do registro");
						}
					}
				}

				continue;
			} else if (containerRemove.foiRemovido(id, idSincronizador)) {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("[REMOVIDO B123] O id "
							+ EXTDAOSistemaTabela.getNomeSistemaTabela(database, idSistemaTabela) + "[" + strId
							+ "] do crud insert idSincronizador " + strIdSincronizador
							+ " foi desconsiderado pois ju existe um crud remove do registro");
				if (strIdSincronizador != null) {
					Integer totalRemovido = remove(strIdSincronizador, false);
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
						if (totalRemovido != null && totalRemovido > 0) {

							log.escreveLog("[REMOVIDO B3] O id "
									+ EXTDAOSistemaTabela.getNomeSistemaTabela(database, idSistemaTabela) + "[" + strId
									+ "] do crud insert idSincronizador " + strIdSincronizador
									+ " foi desconsiderado pois ju existe um crud remove do registro");
						} else {

							log.escreveLog("[REMOVIDO B3] Warning - ao remover o id "
									+ EXTDAOSistemaTabela.getNomeSistemaTabela(database, idSistemaTabela) + "[" + strId
									+ "] do crud insert idSincronizador " + strIdSincronizador
									+ " foi desconsiderado pois ju existe um crud remove do registro");
						}
					}

				}
				continue;
			} else {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) 
				log.escreveLog("[OK] O id " + EXTDAOSistemaTabela.getNomeSistemaTabela(database, idSistemaTabela) + "["
						+ strId + "] do crud insert idSincronizador " + strIdSincronizador + " foi considerado.");
				idsInseridosPorMobileOriginal.put(id, idMobileOriginal);
				idsInseridosPorSincronizador.put(id, idSincronizador);
			}
			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb objSRSAW = (EXTDAOSistemaRegistroSincronizadorAndroidParaWeb) registroInsert;
			boolean crudCancelado = objSRSAW.isSincronizacaoDoRegistroCancelada();
			if (!crudCancelado || somenteEnviandoDados) {
				table.clearData();
				if (table.select(strId)) {
					if (tabelasASeremSincronizadas != null && tabelasASeremSincronizadas.size() > 1) {
						ArrayList<Attribute> atributos = table.getListAttributeFK();
						for (int i = 0; i < atributos.size(); i++) {
							Attribute a = atributos.get(i);
							for (int j = 0; j < tabelasASeremSincronizadas.size(); j++) {
								if (a.tableFK.equalsIgnoreCase(tabelasASeremSincronizadas.get(j))) {
									String idFK = a.getStrValue();

									Integer[] rs = getIdDoRegistroNaFilaDeSincronizacao(getDatabase(), idFK, a.tableFK,
											null, log);
									if (rs != null) {

										// if(ultimoIdSincronizador != null &&
										// auxInt > ultimoIdSincronizador){
										// log.escreveLog("Entrou um registro do
										// sincronizador maior que o atual: "
										// + ultimoIdSincronizador);
										// throw new
										// Exception("Entrou um registro do
										// sincronizador maior que o atual: "
										// + ultimoIdSincronizador);
										// }

										// se o registro nao esta sendo
										// sincronizado
										if (rs[1] == null || rs[1] == 0) {
											if (somenteEnviandoDados) {
												if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) 
												log.escreveLog(
														"[hrtcsad]Entrou um registro do sincronizador que possui a FK "
																+ a.getName() + "[" + idFK
																+ "] que nuo estu sendo sincronizado nessa rodada. Como estamos apenas enviando os dados, uma exceuuo seru lanuada para PARAR o processo de sincronizacao. Para que na pruxima rodada o registro referente a FK faua parte do processo de sincronizacao, nuo havendo mais necessidade de cancelamento.");
												throw new SincronizacaoException(
														"Entrou um registro do sincronizador maior que o atual: "
																+ ultimoIdSincronizador);
											} else {
												if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) 
												log.escreveLog("[lknsebvsner]A FK " + a.getName() + "[" + idFK
														+ "] nuo estu na fila para sincronizacao. Logo o crud seru cancelado.");
												crudCancelado = true;
												break;
											}
										} else if (rs[1] != null && rs[1] == 1) {
											if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) 
											log.escreveLog("[lkmrtkbtklm] Atributo FK dependente " + a.getName() + "["
													+ idFK + "] do crud atual.");
											idsDependentes.add(rs[0].toString());
											atributosDependentes.add(a.getName());
										}
									}
								}
							}
						}
					}

					if (!crudCancelado || somenteEnviandoDados) {
						vContainer.addCorpo(null, table.getValoresMenosId(), strIdSincronizador, strId, strIdMobile,
								idsDependentes, atributosDependentes);
					}
				} else {
					boolean contemChaveRemovida = idsRemovidosPorSincronizador.containsKey(id);
					boolean contemChaveMobileRemovida = false;
					Integer idSincronizadorOpRemove = null;
					if (contemChaveRemovida) {
						idSincronizadorOpRemove = idsRemovidosPorSincronizador.get(id);
						contemChaveMobileRemovida = idSincronizadorOpRemove.equals((int) idMobileOriginal);
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) 
						log.escreveLog(
								"Id inexistente na base, logo crud insert cancelado. Registro relacionado ao crud cancelado: "
										+ table.getName() + "[" + strId + "] " + " - contemChaveRemovida: "
										+ contemChaveRemovida + ". contemChaveMobileRemovida: "
										+ contemChaveMobileRemovida + " - IdSincronizador: " + idSincronizadorOpRemove
										+ ". Id: " + idMobileOriginal);
						// TODO depurar
						crudCancelado = true;
					} else { // TODO esse else nao existia

						// TODO verificar se a remocao nao vai impactar no
						// resultado da sincronizacao
						// tive que fazer essa alteracao pois ao passar enviar
						// somente fragmentos da arvore,
						// diversas vezes a seuuo selecionada nuo inseria
						// registros para serem sincronizados
						// pois quase todos eram cancelados

						Integer totalRemovido = objSRSAW.removerTodosAsOperacoesDoRegistro(strId, null,
								idSistemaTabela);
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
							if (totalRemovido != null && totalRemovido > 0) {
								log.escreveLog("[REMOVIDO B89] O id "
										+ EXTDAOSistemaTabela.getNomeSistemaTabela(database, idSistemaTabela) + "["
										+ strId + "] do crud insert idSincronizador " + strIdSincronizador
										+ " foi desconsiderado assim como todos os outros registros de sincronizacao relacionados ao registro");
							} else {
								log.escreveLog("[REMOVIDO B89] Warning - ao remover o id "
										+ EXTDAOSistemaTabela.getNomeSistemaTabela(database, idSistemaTabela) + "["
										+ strId + "] do crud insert idSincronizador " + strIdSincronizador
										+ " foi desconsiderado assim como todos os outros registros de sincronizacao relacionados ao registro");
							}
						}

					}

					// log.escreveLog("Id inexistente na base, logo crud insert
					// cancelado. Registro relacionado ao crud cancelado: "
					// + table.getName()
					// + "["
					// + strId
					// + "] "
					// + " - contemChaveRemovida: "
					// + contemChaveRemovida
					// + ". contemChaveMobileRemovida: "
					// + contemChaveMobileRemovida
					// + " - IdSincronizador: "
					// + idSincronizadorOpRemove
					// + ". Id: " + idMobileOriginal);
					// crudCancelado = true;

				}
			}
			if (crudCancelado && !somenteEnviandoDados) {
				String strUltimoId = ultimoIdSincronizador.toString();
				cancelaSincronizacaoInsertEditRelacionadasAoRegistro(idSistemaTabela, strId, strUltimoId,
						registrosInsertDaTabela, iRegistro, log);

				cancelaSincronizacaoDosDependentesDoRegistro(idSistemaTabela, strId, strUltimoId, log);

				if (idsInseridosPorMobileOriginal.containsKey(id)) {
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Obs6513.: Existe id em idsInseridos. Id: " + id + ". Id Mobile: "
								+ idsInseridosPorMobileOriginal.get(id) + ". IdSincronizador: "
								+ idsInseridosPorSincronizador.get(id));
					idsInseridosPorMobileOriginal.remove(id);
					idsInseridosPorSincronizador.remove(id);
				}
				if (idsRemovidosPorSincronizador.containsKey(id)) {
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Obs2475.: Existe id em idsRemovidos: Id: " + id
								+ ". Id Op Remove no Sincronizador: " + idsRemovidosPorSincronizador.get(id));
					idsRemovidosPorSincronizador.remove(id);
				}
			}

		}
		return vContainer;
	}

	// public ContainerQueryInsert getContainerInsert(
	// Table table,
	// HashSet<String> vListIdOfTableInserted,
	// HashSet<String> vListIdOfTableRemoved,
	// ContainerQueryEXTDAO vContainerQueryEXTDAORemove){
	// String pNomeTabela = table.getName();
	// //Lista de tabelas para serem enviadas
	// this.clearData();
	// this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT,
	// EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT);
	// //
	// this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.USUARIO_ID_INT,
	// OmegaSecurity.getIdUsuario());
	// this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT,
	// EXTDAOSistemaTabela.getIdSistemaTabela(database,pNomeTabela) );
	// setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT,
	// OmegaSecurity.getIdCorporacao());
	// //pega toda tupla da tabela em questao para ser inserida
	// ArrayList<Table> vListTableToInsert = this.getListTable(
	// new String[]{EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID});
	//
	//
	// ContainerQueryInsert vContainer = new ContainerQueryInsert(pNomeTabela,
	// null);
	//
	// //Para cada tupla de interesse
	// for (Table vTupleToInsert : vListTableToInsert) {
	// String vIdSincronizador =
	// vTupleToInsert.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID);
	// String vId =
	// vTupleToInsert.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT);
	//
	// if(vListIdOfTableInserted.contains(vId)) continue;
	// else if(vListIdOfTableRemoved.contains(vId)) {
	//
	// vContainerQueryEXTDAORemove.removeCorpo(vId);
	// }
	// table.clearData();
	// table.setAttrValue(ID_UNIVERSAL, vId);
	// table.select();
	// vListIdOfTableInserted.add(vId);
	//
	// vContainer.addCorpo("(" + table.getStrCorpoInsert() + ") ",
	// vIdSincronizador, vId);
	// }
	// return vContainer;
	// }

	public ContainerSendData getContainerSendDataOfTable(String pNomeTabela) throws Exception {

		Database vDatabase = this.getDatabase();
		if (isEmpty())
			return null;
		ArrayList<ContainerQueryInsert> vListContainerQueryInsert = new ArrayList<ContainerQueryInsert>();
		ArrayList<ContainerQueryEXTDAO> vListContainerQueryUpdate = new ArrayList<ContainerQueryEXTDAO>();
		ArrayList<ContainerQueryEXTDAO> vListContainerQueryRemove = new ArrayList<ContainerQueryEXTDAO>();
		if (vDatabase == null)
			return null;

		HashSet<String> vListIdOfTableInserted = new HashSet<String>();
		HashSet<String> vListIdOfTableRemoved = new HashSet<String>();
		// Lista de tabelas para serem removidas
		this.clearData();
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT,
				EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE);
		// this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.USUARIO_ID_INT,
		// OmegaSecurity.getIdUsuario());
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT,
				EXTDAOSistemaTabela.getIdSistemaTabela(database, pNomeTabela));
		setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT,
				OmegaSecurity.getIdCorporacao());

		Table vObjTable = vDatabase.factoryTable(pNomeTabela);
		ContainerQueryEXTDAO vContainerQueryEXTDAORemove = new ContainerQueryEXTDAO(pNomeTabela);
		ArrayList<Table> vListTableToRemove = this
				.getListTable(new String[] { EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID });
		for (Table vTupleToRemove : vListTableToRemove) {
			String vIdSincronizador = vTupleToRemove
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID);
			String vId = vTupleToRemove
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT);

			vObjTable.clearData();
			vObjTable.setAttrValue(ID_UNIVERSAL, vId);
			vObjTable.select();
			vListIdOfTableRemoved.add(vId);

			String strRemove = vObjTable.getStrQueryRemoveSQL();
			if (strRemove != null && strRemove.length() > 0) {
				vContainerQueryEXTDAORemove.addCorpo(strRemove, vIdSincronizador, vId, null);
				vListIdOfTableRemoved.add(vId);
			}
		}

		// Lista de tabelas para serem enviadas
		this.clearData();
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT,
				EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT);
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.USUARIO_ID_INT,
				OmegaSecurity.getIdUsuario());
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT,
				EXTDAOSistemaTabela.getIdSistemaTabela(database, pNomeTabela));
		setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT,
				OmegaSecurity.getIdCorporacao());
		// pega toda tupla da tabela em questao para ser inserida
		ArrayList<Table> vListTableToInsert = this
				.getListTable(new String[] { EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID });

		ContainerQueryInsert vContainer = new ContainerQueryInsert(pNomeTabela, vObjTable.getStrCabecalhoInsert());

		// Para cada tupla de interesse
		for (Table vTupleToInsert : vListTableToInsert) {
			String vIdSincronizador = vTupleToInsert
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID);
			String vId = vTupleToInsert
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT);

			if (vListIdOfTableInserted.contains(vId))
				continue;
			else if (vListIdOfTableRemoved.contains(vId)) {
				vContainerQueryEXTDAORemove.removeCorpo(null, vId, null);
				continue;
			}
			vObjTable.clearData();
			vObjTable.setAttrValue(ID_UNIVERSAL, vId);
			vObjTable.select();
			vListIdOfTableInserted.add(vId);

			vContainer.addCorpo("(" + vObjTable.getStrCorpoInsert() + ")", vIdSincronizador, vId);

		}
		if (vContainer.isInitialized())
			vListContainerQueryInsert.add(vContainer);

		this.clearData();

		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT,
				EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_EDIT);
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.USUARIO_ID_INT,
				OmegaSecurity.getIdUsuario());
		this.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT, pNomeTabela);
		setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT,
				OmegaSecurity.getIdCorporacao());
		// pega toda tupla da tabela em questao para ser inserida
		ArrayList<Table> vListTableToUpdate = this
				.getListTable(new String[] { EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID });
		ContainerQueryEXTDAO vContainerQueryEXTDAOUpdate = new ContainerQueryEXTDAO(pNomeTabela);

		// Para cada tupla de interesse
		for (Table vTupleToUpdate : vListTableToUpdate) {
			String vIdSincronizador = vTupleToUpdate
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID);
			String vId = vTupleToUpdate
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT);
			if (vListIdOfTableInserted.contains(vId))
				continue;
			else if (vListIdOfTableRemoved.contains(vId)) {
				continue;
			}

			vObjTable.clearData();
			vObjTable.setAttrValue(ID_UNIVERSAL, vId);
			vObjTable.select();

			String strUpdate = vObjTable.getStrQueryUpdateSQL();
			if (strUpdate != null && strUpdate.length() > 0) {
				vContainerQueryEXTDAOUpdate.addCorpo(strUpdate, vIdSincronizador, vId);
				vListIdOfTableInserted.add(vId);
			}
		}
		if (vContainerQueryEXTDAOUpdate.isInitialized())
			vListContainerQueryUpdate.add(vContainerQueryEXTDAOUpdate);

		return new ContainerSendData(vListContainerQueryInsert, vListContainerQueryUpdate, vListContainerQueryRemove);
	}

	public ContainerSendData getContainerSendData(Database db) throws Exception {
		String[] vVetorTableToSendData = OmegaConfiguration.LISTA_TABELA_SINCRONIZACAO_UPLOAD_CONTINUO(db);

		ContainerSendData vTotalContainer = new ContainerSendData();
		// Lista de tabelas para serem enviadas
		for (String vTableName : vVetorTableToSendData) {

			ContainerSendData vContainerSendDataOfTable = getContainerSendDataOfTable(vTableName);
			if (vContainerSendDataOfTable != null)
				vTotalContainer.copy(vContainerSendDataOfTable);
		}

		return vTotalContainer;
	}

	public void setEstadoSincronizando(String ultimoId, boolean sincronizando, OmegaLog log)
			throws JSONException, Exception {
		if (ultimoId == null)
			return;
		if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("setEstadoSincronizando - sistemaRegistroSincronizadorAndroidParaWeb[" + ultimoId + "]: "
					+ sincronizando);
		String where = ID + " <= ? ";
		String[] args = null;

		args = new String[] { ultimoId };

		Database db = getDatabase();

		ContentValues cv = null;
		if (sincronizando)
			cv = getContentValuesSincronizando();
		else
			cv = getContentValuesNaoSincronizando();
		database.update(super.getName(), cv, where, args);
	}

	ContentValues cvSincronizando = null;
	ContentValues cvNaoSincronizando = null;

	public ContentValues getContentValuesSincronizando() {
		if (cvSincronizando == null) {
			cvSincronizando = new ContentValues();
			cvSincronizando.put(SINCRONIZANDO_BOOLEAN, "1");
		}
		return cvSincronizando;
	}

	public ContentValues getContentValuesNaoSincronizando() {
		if (cvNaoSincronizando == null) {
			cvNaoSincronizando = new ContentValues();
			cvNaoSincronizando.putNull(SINCRONIZANDO_BOOLEAN);
		}
		return cvNaoSincronizando;
	}

	public void setEstadoSincronizandoDoId(String id, boolean sincronizando) throws JSONException, Exception {
		if (id == null)
			return;

		String where = ID + " = ? ";
		String[] args = null;

		args = new String[] { id };

		

		ContentValues cv = null;
		if (sincronizando)
			cv = getContentValuesSincronizando();
		else
			cv = getContentValuesNaoSincronizando();

		database.update(super.getName(), cv, where, args);
	}

	@SuppressLint("UseSparseArrays")
	public void cancelaSincronizacaoDosDependentesDoRegistro(String pIdSistemaTabela, String idRegistro,
			String ultimoIdSincronizador, OmegaLog log) throws Exception {

		

		Cursor cursor = null;
		// "SELECT id, nome, atributo_fk, update_tipo_fk, delete_tipo_fk,
		// fk_sistema_tabela_id_INT "
		// . " FROM sistema_atributo"
		// . " WHERE fk_sistema_tabela_id_INT = '{$idSistemaTabela}' "
		// . " AND fk_sistema_tabela_id_INT IS NOT NULL "

		// cidade[2]
		// ^
		// |
		// bairro[1] (cidade_id_INT)

		// vai pegar os registros de sincronizacao de bairro
		String query = "SELECT s.id, s.id_tabela_INT, sa.sistema_tabela_id_INT, sa.id, sa.nome "
				+ " FROM sistema_registro_sincronizador_android_para_web s" + " JOIN sistema_atributo sa"
				+ " ON s.sistema_tabela_id_INT = sa.sistema_tabela_id_INT" + " WHERE sa.fk_sistema_tabela_id_INT = ? "
				+ " AND s.id <= ? " + " AND s.sincronizando_BOOLEAN = ? " + " AND s.sistema_tipo_operacao_id_INT != ?"
				+ "	ORDER BY s.sistema_tabela_id_INT, sa.id ";

		if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("getRegistrosDoSincronizadorQueDependemDoRegistroEQueEstaoSincronizando: " + query
					+ ". Sendo idSistemaTabela: " + pIdSistemaTabela);

		try {

			cursor = database.rawQuery(query, new String[] { pIdSistemaTabela, ultimoIdSincronizador, "1",
					EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE });
			Stack<Integer> idsSincronizador = new Stack<Integer>();

			Stack<Integer> idsTabelaWeb = new Stack<Integer>();
			Stack<Integer> idsSistemaTabela = new Stack<Integer>();
			Stack<Integer> idsSistemaAtributo = new Stack<Integer>();
			HashMap<Integer, String> nomesAtributos = new HashMap<Integer, String>();
			try {
				if (cursor.moveToFirst()) {
					short cont = 0;
					do {
						cont++;

						idsSincronizador.add(cursor.getInt(0));
						idsTabelaWeb.add(cursor.getInt(1));
						idsSistemaTabela.add(cursor.getInt(2));
						idsSistemaAtributo.add(cursor.getInt(3));
						if (!nomesAtributos.containsKey(idsSistemaAtributo.peek())) {
							nomesAtributos.put(idsSistemaAtributo.peek(), cursor.getString(4));
						}

					} while (cursor.moveToNext());
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Encontrou atributos dependentes: " + cont);

				} else {
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Nuo encontrou nenhum atributo dependente");
				}
			} finally {

				if (cursor != null && !cursor.isClosed()) {
					cursor.close();
					cursor = null;
				}
			}

			if (!idsTabelaWeb.empty()) {
				// Para cada registro de sincronizacao do bairro, vai verificar
				// se os atributos que apontam para cidade
				// e algum deles estao apontando para o atual registro cidade[2]

				LinkedList<String> ids = new LinkedList<String>();
				HashMap<Integer, Integer> idWebPorIdSincronizador = new HashMap<Integer, Integer>();
				Integer lastIdSistemaTabela = idsSistemaTabela.peek();
				Integer lastIdSistemaAtributo = idsSistemaAtributo.peek();

				short cont = 1;

				while (!idsTabelaWeb.empty()) {
					// TODO alerar 1 para 200
					if (cont % 200 == 0 || idsSistemaTabela.peek() != lastIdSistemaTabela
							|| idsSistemaAtributo.peek() != lastIdSistemaAtributo) {

						ProcedimentoCancelaSincronizacaoDosRegistros(idRegistro, lastIdSistemaTabela,
								idWebPorIdSincronizador, ids, lastIdSistemaAtributo, nomesAtributos, log);

						lastIdSistemaAtributo = idsSistemaAtributo.pop();
						lastIdSistemaTabela = idsSistemaTabela.pop();
						ids.clear();
						idWebPorIdSincronizador.clear();
						cont = 1;
					} else {
						idsSistemaTabela.pop();
						idsSistemaAtributo.pop();
					}
					idWebPorIdSincronizador.put(idsTabelaWeb.peek(), idsSincronizador.pop());
					ids.add(String.valueOf(idsTabelaWeb.pop()));

					cont++;
				}
				if (!ids.isEmpty()) {
					ProcedimentoCancelaSincronizacaoDosRegistros(idRegistro, lastIdSistemaTabela,
							idWebPorIdSincronizador, ids, lastIdSistemaAtributo, nomesAtributos, log);
				}

				ids.clear();
				ids = null;
				idWebPorIdSincronizador.clear();
				idWebPorIdSincronizador = null;
			}
		} catch (Exception ex) {
			// SingletonLog.insereErro(ex,
			// app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO.BANCO);
			// return new Mensagem(ex);
			throw new SincronizacaoException(ex.toString());
		}

	}

	private void cancelaSincronizacaoInsertEditRelacionadasAoRegistro(String idSistemaTabela, String idRegistro,
			String idUltimoRegistro, ArrayList<Table> registrosSincronizadorAndroidParaWeb, int iRegistroAtual,
			OmegaLog log) throws Exception {
		

		ContentValues cv = getContentValuesNaoSincronizando();
		Table registroAtual = registrosSincronizadorAndroidParaWeb.get(iRegistroAtual);
		int totalCancelado = database.update(NAME, cv,
				EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT + " = ? " + " AND "
						+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN + " = ? " + " AND "
						+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT + " != ? "
						+ " AND " + EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT + " = ? " + " AND "
						+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + " <= ? " + " AND "
						+ EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + " >= ? ",
				new String[] { idSistemaTabela, "1", EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE, idRegistro,
						idUltimoRegistro, registroAtual.getId() });
		if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog(
					"cancelaSincronizacaoInsertEditRelacionadasAoRegistro - Total Cancelado Banco: " + totalCancelado);
		int totalCanceladoVetor = 0;
		for (int i = iRegistroAtual; i < registrosSincronizadorAndroidParaWeb.size(); i++) {
			Table table = registrosSincronizadorAndroidParaWeb.get(i);
			String idSistemaTabela2 = table
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT);
			if (!idSistemaTabela2.equalsIgnoreCase(idSistemaTabela)) {
				continue;
			}
			String idRegistro2 = table
					.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT);
			if (!idRegistro2.equalsIgnoreCase(idRegistro)) {
				continue;
			}
			// TODO: averiguar
			// String idTipoOperacao2 =
			// table.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT);
			// if(idTipoOperacao2.equalsIgnoreCase(EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE)){
			// continue;
			// }
			totalCanceladoVetor++;
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog(
						"cancelaSincronizacaoInsertEditRelacionadasAoRegistro - Cancelando: sistemaRegistroSincornizadorAndroidParaWeb: "
								+ table.getId());
			// se chegou ate aqui entao o crud deve ser CANCELADO
			table.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SINCRONIZANDO_BOOLEAN, null);
		}
		if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("cancelaSincronizacaoInsertEditRelacionadasAoRegistro - Total Cancelado Vetor: "
					+ totalCanceladoVetor);
	}

	private void ProcedimentoCancelaSincronizacaoDosRegistros(String idRegistro, Integer lastIdSistemaTabela,
			HashMap<Integer, Integer> idWebPorIdSincronizador, LinkedList<String> ids, Integer idSistemaAtributo,
			HashMap<Integer, String> nomesAtributos, OmegaLog log) throws Exception {
		if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("ProcedimentoCancelaSincronizacaoDosRegistros; Id do registro: " + idRegistro
					+ ". Id Sistema Tabela: " + lastIdSistemaTabela);
		Database db = getDatabase();

		LinkedList<String> idsSincQueNaoSeraoSincronizadosAgora = new LinkedList<String>();
		String lastAtributo = nomesAtributos.get(idSistemaAtributo);
		String nomeTabela = EXTDAOSistemaTabela.getNomeSistemaTabela(db, lastIdSistemaTabela.toString());
		// tomando em conta que o registro pai u a cidade e o filho os
		// bairros(aponta para cidade com cidade_id_INT)
		// dentre os bairros que estao na fila da sincronizacao, temos que
		// encontrar aqueles que apontam
		// para o id do atual registro cidade ques estu sendo cancelado
		String q2 = "SELECT DISTINCT(id) " + " FROM " + nomeTabela + " WHERE id in "
				+ Table.makePlaceholders(ids.size()) + " AND " + lastAtributo + " = ?";
		ids.add(idRegistro);
		String[] args = new String[ids.size()];
		ids.toArray(args);
		Cursor cursor = database.rawQuery(q2, args);

		try {
			if (cursor.moveToFirst()) {
				Integer cont2 = idsSincQueNaoSeraoSincronizadosAgora.size();
				do {
					Integer idSinc = idWebPorIdSincronizador.get(cursor.getInt(0));
					idsSincQueNaoSeraoSincronizadosAgora.add(idSinc.toString());
					if (cont2 % 100 == 0) {
						String[] args2 = new String[idsSincQueNaoSeraoSincronizadosAgora.size()];
						idsSincQueNaoSeraoSincronizadosAgora.toArray(args2);
						database.update(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME,
								getContentValuesNaoSincronizando(), EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID
										+ " in " + Table.makePlaceholders(idsSincQueNaoSeraoSincronizadosAgora.size()),
								args2);
					}
					cont2++;
				} while (cursor.moveToNext());
			}
		} finally {
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();

				cursor = null;
			}
		}
		if (!idsSincQueNaoSeraoSincronizadosAgora.isEmpty()) {
			String[] args2 = new String[idsSincQueNaoSeraoSincronizadosAgora.size()];
			idsSincQueNaoSeraoSincronizadosAgora.toArray(args2);
			database.update(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME, getContentValuesNaoSincronizando(),
					EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID + " in "
							+ Table.makePlaceholders(idsSincQueNaoSeraoSincronizadosAgora.size()),
					args2);
		}
	}

	public void cancelaTodosOsRegistrosQueEstaoSincronizando() throws Exception {
		database.update(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME, getContentValuesNaoSincronizando(), null,
				null);
	}

} // classe: fim
