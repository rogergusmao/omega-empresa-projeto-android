

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaRegistroSincronizadorWebParaAndroid
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSistemaRegistroSincronizadorWebParaAndroid.java
    * TABELA MYSQL:    sistema_registro_sincronizador_web_para_android
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaRegistroSincronizadorWebParaAndroid;

	

    
        
    public class EXTDAOSistemaRegistroSincronizadorWebParaAndroid extends DAOSistemaRegistroSincronizadorWebParaAndroid
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaRegistroSincronizadorWebParaAndroid(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaRegistroSincronizadorWebParaAndroid(this.getDatabase());

    }
    

    } // classe: fim
    

    