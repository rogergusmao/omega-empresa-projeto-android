

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOSistemaSequencia
        * DATA DE GERAuuO: 01.06.2016
        * ARQUIVO:         EXTDAOSistemaSequencia.java
        * TABELA MYSQL:    sistema_sequencia
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaSequencia;


        // **********************
        // DECLARAuuO DA CLASSE
        // **********************


    
        

        public class EXTDAOSistemaSequencia extends DAOSistemaSequencia
        {


        // *************************
        // DECLARAuuO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOSistemaSequencia(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOSistemaSequencia(this.getDatabase());

        }
        

        } // classe: fim


        