

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaSincronizadorArquivo
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSistemaSincronizadorArquivo.java
    * TABELA MYSQL:    sistema_sincronizador_arquivo
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.io.File;
import java.util.ArrayList;

import android.database.Cursor;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.database.Database;

import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaSincronizadorArquivo;
import app.omegasoftware.pontoeletronico.file.FileWriterHttpPostManager;
import app.omegasoftware.pontoeletronico.file.FilesZipper;
import app.omegasoftware.pontoeletronico.primitivetype.HelperBoolean;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOSistemaSincronizadorArquivo extends DAOSistemaSincronizadorArquivo
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaSincronizadorArquivo(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaSincronizadorArquivo(this.getDatabase());

    }
    

	public void insertFileInDatabase(String pNomeArquivo, boolean pIsZip){
		setAttrValue(EXTDAOSistemaSincronizadorArquivo.ARQUIVO, pNomeArquivo);
		setAttrValue(EXTDAOSistemaSincronizadorArquivo.IS_ZIP_BOOLEAN, HelperString.valueOfBooleanSQL(pIsZip));
		formatToSQLite();
		insert(false);
	}

	public class ContainerSincronizaFile{
		
		ArrayList<String> listStrId = new ArrayList<String>();
		ArrayList<String> listStrFile = new ArrayList<String>();
		ArrayList<Boolean> listIsZip = new ArrayList<Boolean>();
		public ContainerSincronizaFile(
				ArrayList<String> pListStrId,
				ArrayList<String> pListStrFile,
				ArrayList<Boolean> pListStrIsZip){
			listStrId = pListStrId;
			listStrFile = pListStrFile;
			listIsZip = pListStrIsZip;
		}
		
		public int getCount(){
			return listStrId.size();
		}
		
		public ArrayList<String> getListStrId(){
			return listStrId;
		}
		public ArrayList<String> getListStrFile(){
			return listStrFile;
		}
		public ArrayList<Boolean> getListIsZip(){
			return listIsZip;
		}
		
		
	}
	
	public int getCountOfFileToSend(){		
		
		Cursor cursor = null;
		try{
			

			String vQuery = "SELECT COUNT(sf." + EXTDAOSistemaSincronizadorArquivo.ID + ") AS sf_id " +   
					" FROM " + EXTDAOSistemaSincronizadorArquivo.NAME + " AS sf " ;

			cursor = database.rawQuery(vQuery, null);
			
			if (cursor.moveToFirst()) {
				String vToken = cursor.getString(0);
				return HelperInteger.parserInt(vToken);
			}
			
			return 0;
		}catch(Exception ex){
			//if(ex != null)
				//Log.d(TAG, ex.getMessage());
		} finally{
//			if(oh != null)
//				oh.close();
			if (cursor != null && !cursor.isClosed()) 	    	  
				cursor.close();
		}
		
		return 0;
	}

	public static int procedureSendFileAndRemoveTuplaInDatabase(
			EXTDAOSistemaSincronizadorArquivo pObjSincronizadorFile,
			FileWriterHttpPostManager pFilesManager,
			String pLimitTupla){
		int vNumberOfFileSend = 0;
		try{
			
			ContainerSincronizaFile vContainer = pObjSincronizadorFile.getListLastFileToSend(pLimitTupla);
			if(vContainer != null){
				ArrayList<Boolean> listIsZip = vContainer.getListIsZip();
				ArrayList<String> listFile = vContainer.getListStrFile();
				ArrayList<String> listStrId = vContainer.getListStrId();
				ArrayList<File> listFileToUpload = new ArrayList<File>();
				
				for(int i = 0 ; i < listFile.size(); i++) {
					Boolean vIsZip = listIsZip.get(i);
					String vFileName = listFile.get(i);
					
					String vId = listStrId.get(i);
					if(vIsZip){
						if(pFilesManager.uploadFileAndRemove(vFileName)){
							pObjSincronizadorFile.remove(vId, false);
							vNumberOfFileSend += 1;
						}
						
					} else{
						File vFile = new File(pFilesManager.getStrPath() + vFileName);
						listFileToUpload.add(vFile);
						pObjSincronizadorFile.remove(vId, false);
					}
				}
				if(listFileToUpload.size() > 0 ){
//					Retira um devido ao acrescimo de um arquivo a mais zipado
					vNumberOfFileSend += listFileToUpload.size() - 1; 
					File zipFile = FilesZipper.getNewZipFile(pFilesManager.getStrPath());
					FilesZipper filesZipper = new FilesZipper(listFileToUpload, zipFile);
					filesZipper.zip();
//					Se nao adicionar o arquivo, entao 
					if(! pFilesManager.uploadAndRemoveFile(zipFile)){
						pObjSincronizadorFile.clearData();
						pObjSincronizadorFile.setAttrValue(EXTDAOSistemaSincronizadorArquivo.ARQUIVO, zipFile.getName());
						pObjSincronizadorFile.setAttrValue(EXTDAOSistemaSincronizadorArquivo.IS_ZIP_BOOLEAN, "1");
						pObjSincronizadorFile.insert(false);
					} else vNumberOfFileSend += 1;
				}
			}
			
//			ANTES
//			filesManager.uploadFiles(LIMIT_FILE_TO_ZIP, LIMIT_FILE_TO_UPLOAD, null);
			
		} catch(Exception ex){
			//if(ex == null)
				//Log.e(TAG, "Error");
			//else Log.e(TAG, ex.getMessage());
			
		} 
		return vNumberOfFileSend;
		
	}
	public ContainerSincronizaFile getListLastFileToSend(String pNumeroDeTupla){		
		
		Cursor cursor = null;
		try{
			

			int vTotalCampos = 3;
			String vQuery = "SELECT sf." + EXTDAOSistemaSincronizadorArquivo.ID + " AS sf_id, " +
					" sf." + EXTDAOSistemaSincronizadorArquivo.ARQUIVO + ", " +
					" sf." + EXTDAOSistemaSincronizadorArquivo.IS_ZIP_BOOLEAN +" is_zip " +   
					" FROM " + EXTDAOSistemaSincronizadorArquivo.NAME + " AS sf " +
					" ORDER BY sf_id DESC " ;
			if(pNumeroDeTupla != null)
				vQuery += " LIMIT 0, " + pNumeroDeTupla;

			cursor = database.rawQuery(vQuery, null);
			
			ArrayList<String> listStrId = new ArrayList<String>();
			ArrayList<String> listStrFile = new ArrayList<String>();
			ArrayList<Boolean> listIsZip = new ArrayList<Boolean>();
			if (cursor.moveToFirst()) {
				do {
					for(int i = 0;  i < vTotalCampos ; i ++){
						String vToken = cursor.getString(i);
						switch (i) {
						case 0:
							listStrId.add(vToken);
							break;
						case 1:
							listStrFile.add(vToken);
							break;
						case 2:
							listIsZip.add(HelperBoolean.valueOfStringSQL( vToken));
							break;
							
						default:
							break;
						}
					}
						
				} while (cursor.moveToNext());
			}

			if (cursor != null && !cursor.isClosed()) {	    	  
				cursor.close();
			}
			if(listIsZip.size() > 0)
				return new ContainerSincronizaFile(listStrId, listStrFile, listIsZip);
			else return null;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BANCO);
			//if(ex != null)
				//Log.d(TAG, ex.getMessage());
		} finally{
//			if(oh != null)
//				oh.close();
			if (cursor != null && !cursor.isClosed()) 	    	  
				cursor.close();
		}
		return null;
	}
    } // classe: fim
    

    