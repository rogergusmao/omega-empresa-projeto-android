
/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:  EXTDAOSistemaTabela
 * DATA DE GERAuuO: 08.06.2013
 * ARQUIVO:         EXTDAOSistemaTabela.java
 * TABELA MYSQL:    sistema_tabela
 * BANCO DE DADOS:  
 * -------------------------------------------------------
 *
 */

package app.omegasoftware.pontoeletronico.database.EXTDAO;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import android.content.Context;
import android.database.Cursor;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.Tuple;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaTabela;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador.STATE_SINCRONIZADOR;
import app.omegasoftware.pontoeletronico.database.synchronize.RetornoOperacaoAForca;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.primitivetype.HelperBoolean;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA.TIPO;
// **********************
// DECLARAuuO DA CLASSE
// **********************

public class EXTDAOSistemaTabela extends DAOSistemaTabela {

	static ConcurrentHashMap<Integer, String> idPorNome = new ConcurrentHashMap<Integer, String>()  ;
	static ConcurrentHashMap<String, String> nomePorId = new ConcurrentHashMap<String, String>()  ;
	
	// *************************
	// DECLARAuuO DE ATRIBUTOS
	// *************************

	// *************************
	// CONSTRUTOR
	// *************************
	public EXTDAOSistemaTabela(Database database) {
		super(database);

	}

	// *************************
	// FACTORY
	// *************************
	public Table factory() {
		return new EXTDAOSistemaTabela(this.getDatabase());

	}

	@Override
	public Long insert(boolean pIsToInsertInSynchronizeTable) {
		return insert(pIsToInsertInSynchronizeTable, true, null);
	}
	
	public static String getNomeSistemaTabela(Database db, String pIdTabela) {
		
		Integer intIdTabela = HelperInteger.parserInteger( pIdTabela);
		if(intIdTabela == null)
			return null;
		try {
			if(!idPorNome.contains(intIdTabela)){
				String nome = db.getFirstString(
						"SELECT nome FROM sistema_tabela WHERE id = ?" ,
						new String [] {pIdTabela});
				if(nome != null){
					idPorNome.put(intIdTabela, nome);
					if(!nomePorId.containsKey(nome))
						nomePorId.put(nome, pIdTabela);	
				}
				
				return nome;
			} else return idPorNome.get(pIdTabela);
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.BANCO);
			return null;
		} finally {
			db.close();
		}

	}

	public static InterfaceMensagem inserirAForca(
			Database db,
			Table obj,
			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb objRegistroAndroidWeb,
			String idCrud,
			Long idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
			OmegaLog log,
			boolean provenienteDoMobile) throws Exception {
		return inserirAForca( db,
			obj,
			objRegistroAndroidWeb,
			idCrud,
			idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
			log,
			provenienteDoMobile,
			true);
	}
	private static InterfaceMensagem inserirAForca(
			Database db,
			Table obj,
			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb objRegistroAndroidWeb,
			String idCrud,
			Long idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
			OmegaLog log,
			boolean provenienteDoMobile,
			boolean tentarFallbackEmCasoDeDuplicada) throws Exception {
		
		RegistroDuplicado resultRD = obj.getRegistroDuplicadoSeExistente();
		
		Table registroDuplicado = resultRD.registro;
		
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
			log.escreveLog(
					"InserirAForca o registro: " 
					+ obj.getIdentificadorRegistro() 
					+ ". RegistroDuplicado: " 
					+ (resultRD != null ? resultRD.getDescricao() : "nulo"));
		}
		
		
		
		String id = obj.getId();
		String idSistemaTabela = EXTDAOSistemaTabela.getIdSistemaTabela(
				db, obj.getName());
		// verifica se o valor do registro ju nuo existe localmente
		// esse caso pode ocorrer durante a criauuo do banco .db na web
		// durante a gerauuo do banco de dados uma operauuo crud pode ocorrer
		// e o resultado dessa operauuo poderu ser incluido nesse .db gerado
		// e ao mesmo tempo o novo arquivo de sincronizacao gerado naquele
		// instante
		// iru ser realizado nos celulares que sincronizarem e fizerem o
		// download desse .db gerado.
		Table obj2 = db.factoryTable(obj.getName());
		boolean idOcupado = false;
		if (obj2.select(id)) {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO )
			log.escreveLog("Id que desejado estu atualmente ocupado: " + obj2.getIdentificadorRegistro() );
			idOcupado = true;
//			if (obj.comparaValores(obj2)) {
//				log.escreveLog("O registro " + obj2.getName() + "["+id+"] possui valores sao iguais aos vindos do servidor");
//				return new Mensagem(
//						PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
//			}
		}
		
		if(!idOcupado 
			&& (resultRD.chaveUnicaIndefinida || registroDuplicado == null || resultRD.possuiAtributosDaChaveUnicaComValoresNulos)
		){
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Inserindo..." );
			
			if (obj.insert(false, false, log) > 0){
				return new Mensagem(TIPO.OPERACAO_REALIZADA_COM_SUCESSO, "Registro inserido com sucesso: " + obj.getIdentificadorResumido());
			}
		}
//		if(resultRD.chaveUnicaIndefinida){
//			
//			
//			if(OmegaConfiguration.DEBUGGING){
//				log.escreveLog("Chave indefinida");
//			}
//			throw new Exception("ERRO FATAL - Chave unica indefinida para a tabela: "
//					+ obj.getName()
//					+ ". Impossibilitando a correcao do crud " + idCrud +" de inseruuo."
//					+ ". SINCRONIZADOR: " + obj.getIdentificadorRegistro()
//					+ ". LOCAL: " + obj2.getIdentificadorRegistro());
//		}
		
		// Se existe a duplicada
		if (registroDuplicado != null && !resultRD.possuiAtributosDaChaveUnicaComValoresNulos) {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
				log.escreveLog("Tentando solucionar registro duplicado. " + registroDuplicado.getIdentificadorRegistro());
			}
			String idDuplicada = registroDuplicado
					.getStrValueOfAttribute(Table.ID_UNIVERSAL);
			// Verifica se o registro estu cadastrado apenas localment -
			// operacao de insert na tabela do sincrnonizador
			if (objRegistroAndroidWeb.isRegistroLocal(
					idDuplicada, 
					idSistemaTabela,
//					null,//idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
//					false,
					log)) {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
					log.escreveLog("O registro duplicado u local.");
				}
//===================================================================================================				
//				ALTEARACAO REALIZADA NO DIA 21/11/2015
//				InterfaceMensagem msg = EXTDAOSistemaAtributo
//						.atualizaChavesFkDasTabelasDependentesParaONovoPai(db,
//								registroDuplicado, id);

//				if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
//						.getId()) {
//					registroDuplicado.remove(true);
//					return msg;
//				} else
//					return msg;
				//=============================================================================
				//DEPOIS
				//Atualiza os dados do registro local com os dados do objeto
				String novoId = obj.getId();
				obj.setId(idDuplicada);
				
				if(obj.update(false)){
					
					//depois atualiza o id do objeto local para o que veio do sincronizador
					//if(obj.updateId(novoId, log)){ableName, String oldId, String newId, OmegaLog log
					if(objRegistroAndroidWeb.updateIdSynchronized(obj.getName(), null, obj.getId(), novoId, log,true )){
						
						int totalRemovido = objRegistroAndroidWeb.removerTodosAsOperacoesDoRegistroNaoSincronizadas(obj.getName(), idDuplicada, idSistemaTabela);
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Removida "+totalRemovido+" registros de operacoes de sincronizacao local ...");
						return new Mensagem(TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
					} else{
						obj.setId(idDuplicada);
						throw new Exception(
							"O registro u duplicado com um registro que foi inserido localmente e que nuo foi sincronizado. Na tentativa de realizar a correuuo as seguintes operauues foram realizadas. " +
							"[OK] - Atualizou o registro duplicado "+idDuplicada+" com os dados do crud de insert. " +
							"[ERRO] - Atualizacao do id do registro local para o id vindo do sincronizador. " + idDuplicada + " para o id " + novoId +
							"ID CRUD " + idCrud
							+ ". DADOS VINDOS DO SINCRONIZADOR: " + obj.getIdentificadorRegistro()
							+ ". REGISTRO DUPLICADO LOCAL NuO SINCRONIZADO: " + registroDuplicado.getIdentificadorRegistro());
					}
				} else {
					obj.setId(novoId);
					throw new Exception(
							"O registro u duplicado com um registro que foi inserido localmente e que nuo foi sincronizado. Na tentativa de realizar a correuuo as seguintes operauues foram realizadas. " +
							"[ERRO] - Atualizou o registro duplicado "+idDuplicada+" com os dados do crud de insert. " +
							"[NAO EXECUTADA] - Atualizacao do id do registro local para o id vindo do sincronizador. " + idDuplicada + " para o id " + novoId +
							"ID CRUD " + idCrud
							+ ". DADOS VINDOS DO SINCRONIZADOR: " + obj.getIdentificadorRegistro()
							+ ". REGISTRO DUPLICADO LOCAL NuO SINCRONIZADO: " + registroDuplicado.getIdentificadorRegistro());
					
				}

			} 
			else {
				
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
					log.escreveLog("[qrewgth]O registro duplicado nuo u local.");
				}
				if(tentarFallbackEmCasoDeDuplicada){
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
						log.escreveLog("[qrewgth]tentarFallbackEmCasoDeDuplicada.");
					}
					//verificando se o valor atual u proveniente de ediuuo local.
					Long idOperacaoEditNaoSincronizada = objRegistroAndroidWeb.getId(
							registroDuplicado.getId(), 
							idSistemaTabela, 
							EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_EDIT, 
							false, 
							log);
					
					if(idOperacaoEditNaoSincronizada != null && idOperacaoEditNaoSincronizada != null ){
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("[qrewgth]Existe op do sincronizador de edicao que ainda nuo foi enviada no mobile local. Id do sistema registro sincronizador android para web: "+idOperacaoEditNaoSincronizada);
						EXTDAOSistemaRegistroHistorico objSRH = new EXTDAOSistemaRegistroHistorico(db);
						InterfaceMensagem msg = objSRH.updateRegistroComDadosAntigoSeExistirNoHistoricoDeEdicao(objRegistroAndroidWeb, registroDuplicado,log );
						if(msg != null 
								&& msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()){
						
							InterfaceMensagem msgRet = inserirAForca( db,
									obj,
									objRegistroAndroidWeb,
									idCrud,
									idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
									log,
									provenienteDoMobile,
									false);
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("[qrewgth]Resultado da tentativa de fallback. Id do sistema registro sincronizador android para web: "+idOperacaoEditNaoSincronizada + ". Retorno: " + msgRet.toJson());
							return msgRet;
						}
						
						return new RetornoOperacaoAForca(
								TIPO.REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC,
								registroDuplicado);
					} 
				}
				
				if (obj.comparaValores(registroDuplicado)) {
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
						log.escreveLog("O registro u exatamente igual ao contido no crud.");
					}
					return new Mensagem(TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
				} else {
					
//						String token = obj.comparaValores(registroDuplicado, false);
//						throw new Exception(
//								"O registro u duplicado com um registro que foi inserido localmente e que NuO ESTu NA LISTA PARA SER SINCRONIZADO. " +
//								" Tentamos verificar se esses registro suo pelo menos iguais, pois caso positivo nuo teria problema nuo executar o crud. Porum eles suo diferentes." +
//								"ID CRUD " + idCrud
//								+ ". DADOS VINDOS DO SINCRONIZADOR: " + obj.getIdentificadorRegistro()
//								+ ". REGISTRO DUPLICADO LOCAL FORA DA LISTA PARA SINCRONIZAuuO: " + registroDuplicado.getIdentificadorRegistro()
//								+ ". Diferenua entre os dois registros anteriores: " + token);
					return new RetornoOperacaoAForca(
							TIPO.REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC,
							registroDuplicado);
				}	
			}
		} else {
			String idQueDesejaDesocupar = obj.getId();
			
			if (objRegistroAndroidWeb.isRegistroLocal(
					idQueDesejaDesocupar, 
					idSistemaTabela, 
//					null,//idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
//					false,
					log)) {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
					log.escreveLog("Existe um registro local ocupando o id do crud.");
				}
				obj.setId(null);
				if (obj.insert(false) != null) {
					if(provenienteDoMobile){
						if (objRegistroAndroidWeb.updateIdSynchronized(obj.getName(), null, obj.getId(), idQueDesejaDesocupar, log, true)) {
							return new Mensagem(TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
						} else {
							throw new Exception("ID CRUD " + idCrud
									+ ". A inseruuo falhou, na tentativa para corrigir a operauuo de crud detectamos que o id utilizado ju esta ocupado por um registro inserido localmente que ainda nuo foi sinconizado.  " +
									"[OK] - Inserimos o crud vindo da web" +
									"[ERRO] - Tentamos trocar o ID do registro inserido localmente com o id original do crud." 
									+ ". REGISTRO VINDO DO SINCRONIZADOR: " + obj.getIdentificadorRegistro()
									+ ". ID QUE DESEJA OCUPAR: " + idQueDesejaDesocupar);
							
						}
					} else {
						if (objRegistroAndroidWeb.updateIdSynchronized(obj.getName(), null, obj.getId(), idQueDesejaDesocupar, log, false)) {
							return new Mensagem(TIPO.OPERACAO_REALIZADA_COM_SUCESSO);	
						} else {
							throw new Exception("[ASFDGSLNDHG]ID CRUD " + idCrud
									+ ". A inseruuo falhou, na tentativa para corrigir a operauuo de crud detectamos que o id utilizado ju esta ocupado por um registro inserido localmente que ainda nuo foi sinconizado.  " +
									"[OK] - Inserimos o crud vindo da web" +
									"[ERRO] - Tentamos trocar o ID do registro inserido localmente com o id original do crud." 
									+ ". REGISTRO VINDO DO SINCRONIZADOR: " + obj.getIdentificadorRegistro()
									+ ". ID QUE DESEJA OCUPAR: " + idQueDesejaDesocupar);
						}
					}
					
				} else {
					throw new Exception("ID CRUD " + idCrud
							+ ". A inseruuo falhou, na tentativa para corrigir a operauuo de crud detectamos que o id utilizado ju esta ocupado por um registro inserido localmente que ainda nuo foi sinconizado.  " +
							"[ERRO] - Inserimos o crud vindo da web" +
							"[NuO EXECUTADA] - Tentamos trocar o ID do registro inserido localmente com o id original do crud."
							+ ". REGISTRO VINDO DO SINCRONIZADOR: " + obj.getIdentificadorRegistro());
				}

			} else {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("O registro ques estu ocupando a chave primuria nuo possui nenhum crud armazenado no sincronizador local de insert. Logo nuo saiu daqui");
			}
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
				log.escreveLog("WARNING - CORREuuO A FORuA - nuo encontramos alternativas no processamento do inserirAForca");
			}
			obj.setId(idQueDesejaDesocupar);
			if(!obj.update(false)){
				throw new Exception("ID CRUD " + idCrud
				+ ". A inseruuo falhou, na tentativa para corrigir a operauuo de crud detectamos que o id utilizado ju esta ocupado por" +
				" um registro que nuo estu na lista para ser sincronizado.  "
				+ ". REGISTRO VINDO DO SINCRONIZADOR: " + obj.getIdentificadorRegistro()
				+ ". ID QUE DESEJA OCUPAR: " + idQueDesejaDesocupar);				
			} else {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("UPDATE realizado com os dados vindos da web. ID CRUD " + idCrud
							+ ". A inseruuo falhou, na tentativa para corrigir a operauuo de crud detectamos que o id utilizado ju esta ocupado por" +
							" um registro que nuo estu na lista para ser sincronizado.  "
							+ ". REGISTRO VINDO DO SINCRONIZADOR: " + obj.getIdentificadorRegistro()
							+ ". ID QUE DESEJA OCUPAR: " + idQueDesejaDesocupar);
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, "Update realizado com o identificador local nuo identifcado como id a ser sincronizado.");
			}
		}
	}
	

//	EXTDAOSistemaRegistroHistorico objSRH = null;
//	EXTDAOSistemaRegistroSincronizadorAndroidParaWeb objRegistroAndroidWeb;
//	public RetornoUpdateRollback tentativaCorrecaoRegistroDuplicadoComRollbackDosDadosLocais(String idSistemaTabela, Table registroDuplicado) throws Exception{
////		String idCorporacao = OmegaSecurity.getIdCorporacao();
////		if(objSRH == null)
////		objSRH = new EXTDAOSistemaRegistroHistorico(db);
////		if(objRegistroAndroidWeb == null)
////			objRegistroAndroidWeb = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(db);
////		RetornoUpdate retornoRegistroBackupRealizado = objSRH.updateRegistroComDadosAntigoSeExistirNoHistoricoDeEdicao(
////				objRegistroAndroidWeb,
////				registroDuplicado,
////				idSistemaTabela,
////				idCorporacao);
////		while(retornoRegistroBackupRealizado != null 
////				&& !retornoRegistroBackupRealizado.ultimoBackup){
////			retornoRegistroBackupRealizado = objSRH.updateRegistroComDadosAntigoSeExistirNoHistoricoDeEdicao(
////					objRegistroAndroidWeb,
////					registroDuplicado,
////					idSistemaTabela,
////					idCorporacao);
////			
////			//LEMBRE DE ATUALIZAR OS VALORES DO REGISTRO DO ROLLBACK ANTES DE EXECUTAR A OPERACAO DE EDIT
////			InterfaceMensagem msg= objRegistroAndroidWeb.preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks(
////					campos, 
////					crud.valores, 
////					obj);
////			
////			//Se alguma das FKs foi removida e a dependencia era de ON_DELETE_CASCADE, entao o registro
////			//atual seru removido
////			if(msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_REMOVIDO_DEVIDO_ON_DELETE_CASCADE.getId()){
////				obj.remove(false);
////				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
////			} 
////			
////			if(retornoRegistroBackupRealizado != null){
////				return retornoRegistroBackupRealizado;
//////				Table registroBkpRzd = retornoRegistroBackupRealizado.registroBackupeado;
//////				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
////			}
////		}
//		return null;
//	}
	EXTDAOSistemaRegistroHistorico objSRH = null;
	public InterfaceMensagem updateAForca(
			Database db,
			Table obj,
			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb objRegistroAndroidWeb,
			String idCrud,
			Long idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
			OmegaLog log) throws Exception {

		RegistroDuplicado resultRD = obj.getRegistroDuplicadoSeExistente();
		
		Table registroDuplicado = resultRD.registro;
		
		if(resultRD.chaveUnicaIndefinida || registroDuplicado == null || resultRD.possuiAtributosDaChaveUnicaComValoresNulos)
			if(obj.updateDoSincronizadorEspelho(log))
				return new Mensagem(TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
		
		
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("Inicio do tratamento do crud de ediuuo");
		String id = obj.getId();
		String idSistemaTabela = EXTDAOSistemaTabela.getIdSistemaTabela(
				db, obj.getName());

		// verifica se o valor do registro ju nuo existe localmente
		// esse caso pode ocorrer durante a criauuo do banco .db na web
		// durante a gerauuo do banco de dados uma operauuo crud pode ocorrer
		// e o resultado dessa operauuo poderu ser incluido nesse .db gerado
		// e ao mesmo tempo o novo arquivo de sincronizacao gerado naquele
		// instante
		// iru ser realizado nos celulares que sincronizarem e fizerem o
		// download desse .db gerado.
		Table obj2 = db.factoryTable(obj.getName());
		if (obj2.select(id)) {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Id que desejado estu atualmente ocupado: " + obj2.getName() + "["+id+"]");
//			if (obj.comparaValores(obj2)) {
//				log.escreveLog("Sucesso, o valor ju esta cadastro na base local.");
//				new Mensagem(
//						PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
//			}
		}
		

	//		if(resultRD.chaveUnicaIndefinida){
	//			String token = "Erro, a chave unica indefinida na tabela. E o registro vindo do sincronizador nuo u igual ao id ju ocupado localmente. Impossibilitando a correcao do crud de inseruuo."
	//					+ ". SINCRONIZADOR: " + obj.getIdentificadorRegistro()
	//					+ ". LOCAL SINCRONIZADO OU NuO: " + obj2.getIdentificadorRegistro();
	//			log.escreveLog(token);
	//			throw new Exception(token);
	//		}
		
		// Se existe a duplicada
		if (registroDuplicado != null && !resultRD.possuiAtributosDaChaveUnicaComValoresNulos) {
			String idDuplicada = registroDuplicado
					.getStrValueOfAttribute(Table.ID_UNIVERSAL);
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Registro duplicado: "+ idDuplicada);
			// Verifica se o registro estu cadastrado apenas localment -
			// operacao de insert na tabela do sincrnonizador

			
			//Se o registro u local
			if (objRegistroAndroidWeb.isRegistroLocal(
					idDuplicada, 
					idSistemaTabela,
//					idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
//					false,
					log )) {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("Um registro local implicou na duplicidade. Nesse ponto os dependentes desse registro local passaruo a apontar para o registro vindo do sincronizador."
						+	" DADOS VINDO DO SINCRONIZADOR: "+ obj.getIdentificadorRegistro()
						+	" REGISTRO QUE FOI INSERIDO LOCALMENTE E QUE AINDA NuO FOI SINCRONIZADO: " + registroDuplicado.getIdentificadorRegistro());
				
				InterfaceMensagem msg = EXTDAOSistemaAtributo
						.atualizaChavesFkDasTabelasDependentesParaONovoPai(db,
								registroDuplicado, id, log);

				if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Sucesso, os ids dependentes foram redirecionados, agora o registro duplicado local nuo sincronizado seru removido");
					registroDuplicado.remove(true);
					return msg;
				} else
					return msg;
				//
			} else {
				String comparacao = obj.comparaValores(registroDuplicado, false);
				if (comparacao == null) {
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Sucesso, o registro ju se encontrava localmente");
					return new Mensagem(TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
				} else {
					if(objSRH == null)
						objSRH = new EXTDAOSistemaRegistroHistorico(getDatabase());
					InterfaceMensagem msgFallback = objSRH.updateRegistroComDadosAntigoSeExistirNoHistoricoDeEdicao(objRegistroAndroidWeb, registroDuplicado, log);
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("[5awfe58]Resultado da tentantiva de backup "+ msgFallback.toJson());
					
					if(msgFallback.mCodRetorno == TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()){
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Fallback realizado com sucesso. Iremos validar novamente o registro para ver se podemos executar a edicao");
						RegistroDuplicado resultRD2 = obj.getRegistroDuplicadoSeExistente();
						Table registroDuplicado2 = resultRD2.registro;
						
						if(resultRD2.chaveUnicaIndefinida || registroDuplicado2 == null || resultRD2.possuiAtributosDaChaveUnicaComValoresNulos){
							if(obj.updateDoSincronizadorEspelho(log))
								return new Mensagem(TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
							else{
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("[mregrtttyp0]Nuo foi possuvel realizar o update ainda");
							}
						}else{
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("[mregrtttyp]Continuamos com o id duplicado");
						}
					}
					
				
					//TODO lembrar de remover, colocou aqui dia 08/07/2016 para fim de teste, remove depois do dia 20/07/2016
//					obj.getRegistroDuplicadoSeExistente();
//					throw new Exception("ID CRUD " + idCrud
//							+ ". O update falhou, na tentativa para corrigir a operauuo de crud detectamos que outro registro possui a mesma chave unica." +
//							" Esse registr o nuo esta na fila para ser sincronizado e u diferente dos dados do registro do crud vindo do sincronizador. "
//							+ ". REGISTRO VINDO DO SINCRONIZADOR: " + obj.getIdentificadorRegistro()
//							+ ". REGISTRO LOCAL FORA DA LISTA DE SINCORNIZAuuO: " + registroDuplicado.getIdentificadorRegistro()
//							+ ". DIFERENuA: " + comparacao);
					return new RetornoOperacaoAForca(
							TIPO.REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC,
							registroDuplicado);
				}
			}
		} else {
			String idQueDesejaDesocupar = obj.getId();
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("O id " + idQueDesejaDesocupar + " estu ocupado e nuo temos nenhuma incidencia de duplicidade no banco local. Tentando desocupa-lo.");
			
			if (objRegistroAndroidWeb.isRegistroLocal(
					idQueDesejaDesocupar, 
					idSistemaTabela, 
					//idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
					log)) {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("Estu ocupado por um registro inserido localmente nuo sincronizado.");
				if (objRegistroAndroidWeb.updateIdSynchronized(obj.getName(), null, obj.getId(), idQueDesejaDesocupar, log, true)) {
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Sucesso, id local nuo sincronizado foi desocupado.");
					
					return new Mensagem(TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
				} else if(!Table.existeId(db, obj.getName(), obj.getId())){ //faz mais uma verificauuo antes de fechar
					final String token = "Nuo conseguiu descocupar o registro local nuo sincronizado para que o dado vindo do sincronizador fosse atualizado. "
							+ idCrud 
						+ ". DADOS VINDO DO SINCRONIZADOR: " + obj.getIdentificadorRegistro()
						+ ". DADOS DO REGISTRO LOCAL QUE AINDA NuO FOI SINCRONIZADO: " + obj2.getIdentificadorRegistro();
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog(token);
					throw new Exception(token);
				}
			}
			final String token = "O crud de update nuo conseguiu ser realizado, checamos a existencia de algum registro com a mesma chave unica e nuo encontramos nenhum. Verificamos" +
					" se o dado atual do registro ocupado pelo id " + idQueDesejaDesocupar + " se o mesmo estu ocupado com algum registro inserido localmente " +
					"e que ainda nuo foi sincronizado, nuo encotramos nenhum registro. "
					+ "ID CRUD: " + idCrud 
					+ ". DADOS VINDO DO SINCRONIZADOR: " + obj.getIdentificadorRegistro();
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog(token);
			throw new Exception(token);
		}
	}


	public static String getIdSistemaTabela(Context c, String pNomeTabela) throws OmegaDatabaseException {
		Database db = null;
		try{
			db = new DatabasePontoEletronico(c);
			return EXTDAOSistemaTabela.getIdSistemaTabela(db, pNomeTabela);
		}finally{
			if(db != null) db.close();
		}
	
	}
	
	public static String getIdSistemaTabela(Database db, String pNomeTabela) {
		
		
		
		Cursor cursor = null;
		try {
			if(nomePorId.containsKey(pNomeTabela)) return nomePorId.get(pNomeTabela);
			
			cursor = db.rawQuery("SELECT id FROM " + EXTDAOSistemaTabela.NAME
					+ " WHERE " + EXTDAOSistemaTabela.NOME + " LIKE ?",
					new String[] { pNomeTabela });

			String vId = null;
			try {
				if (cursor.moveToFirst()) {
					vId = cursor.getString(0);
					
					nomePorId.put(pNomeTabela, vId);
					if(vId != null && !idPorNome.containsKey(vId))
						idPorNome.put(HelperInteger.parserInteger( vId), pNomeTabela);
				}
				
			} catch (Exception ex) {
				SingletonLog.insereErro(ex, SingletonLog.TIPO.BANCO);
			}

			return vId;
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);
		} finally{
			if(cursor != null ){
				cursor.close();
			}
		}
		return null;
	}

	public static Boolean isTabelaDoSistema(Database db, String pNomeTabela) {
		Cursor cursor = null;

		try {

			
			cursor = db.rawQuery("SELECT "
					+ EXTDAOSistemaTabela.TABELA_SISTEMA_BOOLEAN + " FROM "
					+ EXTDAOSistemaTabela.NAME + " WHERE "
					+ EXTDAOSistemaTabela.NOME + " LIKE ?",
					new String[] { pNomeTabela });

			try {
				if (cursor.moveToFirst()) {
					String vId = cursor.getString(0);
					if (vId == null)
						return false;
					else
						return HelperBoolean.valueOfStringSQL(vId);
				}
			} catch (Exception ex) {
			}

			
			return null;
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.BANCO);
		} finally{
			if (cursor != null ) {
				cursor.close();
			}
		}
		return null;
	}

	public Table getObjTableOfId() {
		String vNomeTabela = getStrValueOfAttribute(NOME);
		Database vDatabase = getDatabase();
		Table vTable = vDatabase.factoryTable(vNomeTabela);
		return vTable;
	}

	public static String[] getVetorDownloadContinuo(Database db) {
		EXTDAOSistemaTabela vObj = new EXTDAOSistemaTabela(db);
		vObj.setAttrValue(
				EXTDAOSistemaTabela.TRANSMISSAO_WEB_PARA_MOBILE_BOOLEAN, "1");
		vObj.setAttrValue(EXTDAOSistemaTabela.FREQUENCIA_SINCRONIZADOR_INT,
				"-1");
		ArrayList<Table> vListTabela = vObj.getListTable();
		String vetorTabela[] = null;

		if (vListTabela != null) {
			int i = 0;
			vetorTabela = new String[vListTabela.size()];
			for (Table vTuplaSistemaTabela : vListTabela) {
				vetorTabela[i] = vTuplaSistemaTabela
						.getStrValueOfAttribute(EXTDAOSistemaTabela.NOME);
				i += 1;
			}
		}
		return vetorTabela;
	}

	public static String[] getVetorUploadContinuo(Database db) {
		EXTDAOSistemaTabela vObj = new EXTDAOSistemaTabela(db);
		vObj.setAttrValue(
				EXTDAOSistemaTabela.TRANSMISSAO_MOBILE_PARA_WEB_BOOLEAN, "1");
		vObj.setAttrValue(EXTDAOSistemaTabela.FREQUENCIA_SINCRONIZADOR_INT,
				"-1");
		ArrayList<Table> vListTabela = vObj.getListTable();
		String vetorTabela[] = null;

		if (vListTabela != null) {
			int i = 0;
			vetorTabela = new String[vListTabela.size()];
			for (Table vTuplaSistemaTabela : vListTabela) {
				vetorTabela[i] = vTuplaSistemaTabela
						.getStrValueOfAttribute(EXTDAOSistemaTabela.NOME);
				i += 1;
			}
		}
		return vetorTabela;
	}

	public static String[] getVetorDownloadUnico(Database db) {
		EXTDAOSistemaTabela vObj = new EXTDAOSistemaTabela(db);
		vObj.setAttrValue(
				EXTDAOSistemaTabela.TRANSMISSAO_WEB_PARA_MOBILE_BOOLEAN, "1");
		vObj.setAttrValue(EXTDAOSistemaTabela.FREQUENCIA_SINCRONIZADOR_INT, "1");
		ArrayList<Table> vListTabela = vObj.getListTable();
		String vetorTabela[] = null;

		if (vListTabela != null) {
			int i = 0;
			vetorTabela = new String[vListTabela.size()];
			for (Table vTuplaSistemaTabela : vListTabela) {
				vetorTabela[i] = vTuplaSistemaTabela
						.getStrValueOfAttribute(EXTDAOSistemaTabela.NOME);
				i += 1;
			}
		}
		return vetorTabela;
	}
	
	static ReentrantLock lockCache = new ReentrantLock(); 
	static Boolean cacheInitialized = false;
	public static void initCache(Context context) throws Exception{
		if(!cacheInitialized){
			if(lockCache.tryLock()){
				Database db = null;
				
				try{
					if(!cacheInitialized && Database.isDatabaseInitialized(context)){
					
						STATE_SINCRONIZADOR state = RotinaSincronizador.getStateSincronizador(context, null);
						if(state != STATE_SINCRONIZADOR.COMPLETO)
							return;
							
						db = new DatabasePontoEletronico(context);
							
						final String q = "SELECT t.id, t.nome FROM sistema_tabela t";
						List<Tuple<Integer,String>> regs = db.getListTupleIntByString(q, null);
						
						if(regs != null){
							Iterator<Tuple<Integer, String>> it = regs.iterator();
							if(it.hasNext()){
								do{
									Tuple<Integer,String> tupla = it.next();
									idPorNome.put(tupla.x, tupla.y);
								}while(it.hasNext());
							}
							cacheInitialized = true;
						} 				
					}
				}catch(Exception ex){
					SingletonLog.insereErro(ex, SingletonLog.TIPO.BANCO);
					cacheInitialized=false;
					throw ex;
				}finally{
					if(db != null ) db.close();
					
					lockCache.unlock();
				}
			}
		}
		
		
	}


} // classe: fim

