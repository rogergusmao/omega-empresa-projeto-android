

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaTipoDownloadArquivo
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSistemaTipoDownloadArquivo.java
    * TABELA MYSQL:    sistema_tipo_download_arquivo
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaTipoDownloadArquivo;
import app.omegasoftware.pontoeletronico.http.HelperHttpResponse;
import app.omegasoftware.pontoeletronico.phone.HelperPhone;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOSistemaTipoDownloadArquivo extends DAOSistemaTipoDownloadArquivo
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************

    	public enum TIPO{
    		FOTO ,
    		VIDEO,
    		ANEXO,
    		AUDIO,
    		BANCO,
    		DEFAULT
    	}
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaTipoDownloadArquivo(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaTipoDownloadArquivo(this.getDatabase());

    }

	public static boolean performUpload(TIPO pTipo, File p_file, boolean pIsZIP)
	{

		//Log.d(TAG,"performUpload(): Preparing to upload a " + fileToUpload.length() + " bytes long file");
//		DefaultHttpClient dc = new DefaultHttpClient();
//		HttpPost post = new HttpPost(OmegaConfiguration.URL_REQUEST_SEND_ZIP_FILE_PHOTO());
//		
//		try {
//			FileBody bin = new FileBody(p_file);
//	
//			MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);  
//		
//			reqEntity.addPart("usuario", new StringBody(OmegaSecurity.getIdUsuario()));
//			reqEntity.addPart("id_corporacao", new StringBody(OmegaSecurity.getIdCorporacao()));
//			reqEntity.addPart("corporacao", new StringBody(OmegaSecurity.getCorporacao()));
//			reqEntity.addPart("senha", new StringBody(OmegaSecurity.getSenha()));
//			reqEntity.addPart("logfile", bin);
//			reqEntity.addPart("imei", new StringBody(HelperPhone.getIMEI()));
//			String strTipo = getId(pTipo);
//			reqEntity.addPart("tipo",new StringBody(strTipo == null ? "" : strTipo ));
//		
//
//			post.setEntity(reqEntity);
//		} catch (UnsupportedEncodingException e1) {
//			Log.e(TAG,"performUpload(): " + e1.getMessage());
//		}
//		try {
//			
//			String content = HelperHttpResponse.readResponseContent(dc.execute(post));
//			if(content.equals("TRUE"))
//			{
//				Log.d(TAG,"performUpload(): " + p_file.getName() + " was sucessfully uploaded");
//				return true;
//			} else{
//				Log.d(TAG,"performUpload(): " + content);
//				return false;
//			}
//		
//
//		} catch (ClientProtocolException e) {
//			//Log.e(TAG,"performUpload(): " + e.getMessage());
//		} catch (IOException e) {
//			//Log.e(TAG,"performUpload(): " + e.getMessage());
//		}
		return false;

	}
	

	public static String getPath(Database pDatabase, TIPO pTipo){
		EXTDAOSistemaTipoDownloadArquivo vObj = getObj(pDatabase, pTipo);
		if(vObj != null)
			return vObj.getStrValueOfAttribute(PATH);
		else return null;
	}
	
	public static String getId(TIPO pTipo){
		switch (pTipo) {
		case FOTO:
			return "1";
			
		case VIDEO:
			return "2";
			
		case ANEXO:
			return "3";
			
		case AUDIO:
			return "4";
			
		case BANCO:
			return "5";
		case DEFAULT:
			return "8";
		default:
			SingletonLog.insereErro(TAG, "getId", "Tipo nuo identificado: " + pTipo.toString(), SingletonLog.TIPO.BANCO);
			return null;
		}
	}
	
	private static EXTDAOSistemaTipoDownloadArquivo getObj(Database pDatabase, TIPO pTipo){
		try{
		EXTDAOSistemaTipoDownloadArquivo vObj = new EXTDAOSistemaTipoDownloadArquivo(pDatabase);
		switch (pTipo) {
		case FOTO:
			vObj.select("1");
			break;
		case VIDEO:
			vObj.select("2");
			break;
		case ANEXO:
			vObj.select("3");
			break;
		case AUDIO:
			vObj.select("4");
			break;
		case BANCO:
			vObj.select("5");
			break;
		case DEFAULT:
			vObj.select("8");
			break;
		default:
			SingletonLog.insereErro(TAG, "getObj", "Tipo nuo identificado: " + pTipo.toString(), SingletonLog.TIPO.BANCO);
			return null;
		}
		return vObj;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
		return null;
		}
	}


    } // classe: fim
    

    