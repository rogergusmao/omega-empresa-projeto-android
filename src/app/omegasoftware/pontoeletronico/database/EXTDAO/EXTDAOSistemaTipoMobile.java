

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaTipoMobile
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSistemaTipoMobile.java
    * TABELA MYSQL:    sistema_tipo_mobile
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaTipoMobile;

	

    
        
    public class EXTDAOSistemaTipoMobile extends DAOSistemaTipoMobile
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaTipoMobile(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaTipoMobile(this.getDatabase());

    }
    

    } // classe: fim
    

    