/*
 *
 * -------------------------------------------------------
 * NOME DA CLASSE:  EXTDAOSistemaTipoOperacaoBanco
 * DATA DE GERAuuO: 08.06.2013
 * ARQUIVO:         EXTDAOSistemaTipoOperacaoBanco.java
 * TABELA MYSQL:    sistema_tipo_operacao_banco
 * BANCO DE DADOS:  
 * -------------------------------------------------------
 *
 */

package app.omegasoftware.pontoeletronico.database.EXTDAO;

import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaTipoOperacaoBanco;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
// **********************
// DECLARAuuO DA CLASSE
// **********************

public class EXTDAOSistemaTipoOperacaoBanco extends DAOSistemaTipoOperacaoBanco {

	public enum TIPO {
		REMOVE(1), INSERT(2), EDIT(3);

		int id;

		TIPO(int id) {
			this.id = id;
		}

		public int getId() {
			return this.id;
		}
		public boolean isEqual(String strId){
			int id = HelperInteger.parserInt(strId);
			return this.isEqual(id);
					
		}
		
		public boolean isEqual(int id){
			if(this.id == id ) return true;
			else return false;
		}
	}

	public static final String INDEX_OPERACAO_REMOVE = "1";
	public static final String INDEX_OPERACAO_INSERT = "2";
	public static final String INDEX_OPERACAO_EDIT = "3";

	// *************************
	// DECLARAuuO DE ATRIBUTOS
	// *************************

	// *************************
	// CONSTRUTOR
	// *************************
	public EXTDAOSistemaTipoOperacaoBanco(Database database) {
		super(database);

	}

	// *************************
	// FACTORY
	// *************************
	public Table factory() {
		return new EXTDAOSistemaTipoOperacaoBanco(this.getDatabase());

	}

} // classe: fim

