

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaTipoUsuarioMensagem
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSistemaTipoUsuarioMensagem.java
    * TABELA MYSQL:    sistema_tipo_usuario_mensagem
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaTipoUsuarioMensagem;

	

    
        
    public class EXTDAOSistemaTipoUsuarioMensagem extends DAOSistemaTipoUsuarioMensagem
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaTipoUsuarioMensagem(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaTipoUsuarioMensagem(this.getDatabase());

    }
    

    } // classe: fim
    

    