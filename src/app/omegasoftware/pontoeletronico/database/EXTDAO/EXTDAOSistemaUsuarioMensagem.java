

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOSistemaUsuarioMensagem
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOSistemaUsuarioMensagem.java
    * TABELA MYSQL:    sistema_usuario_mensagem
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOSistemaUsuarioMensagem;

	

    
        
    public class EXTDAOSistemaUsuarioMensagem extends DAOSistemaUsuarioMensagem
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOSistemaUsuarioMensagem(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOSistemaUsuarioMensagem(this.getDatabase());

    }
    

    } // classe: fim
    

    