

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOTarefa
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOTarefa.java
    * TABELA MYSQL:    tarefa
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.text.SimpleDateFormat;
	import java.util.Date;
	import java.util.LinkedHashMap;

import android.content.Context;
	import android.database.Cursor;

	import app.omegasoftware.pontoeletronico.R;
	import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
	import app.omegasoftware.pontoeletronico.common.ItemList.AppointmentPlaceItemList;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
	import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOTarefa;
	import app.omegasoftware.pontoeletronico.date.HelperDate;
	import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;


	public class EXTDAOTarefa extends DAOTarefa
    {
		SimpleDateFormat sdf=null;
    	public enum ESTADO_TAREFA{
    		AGUARDANDO_INICIALIZACAO(EXTDAORegistroEstado.TIPO.aguardando_inicializacao)
			,	EM_EXECUCAO(EXTDAORegistroEstado.TIPO.em_execucao)
			,	FINALIZADA(EXTDAORegistroEstado.TIPO.finalizada)
			,	CANCELADA(EXTDAORegistroEstado.TIPO.cancelada)
			,	PAUSADA(EXTDAORegistroEstado.TIPO.pausada);

    		final EXTDAORegistroEstado.TIPO tipo;
    		ESTADO_TAREFA(EXTDAORegistroEstado.TIPO tipo){
    			this.tipo=tipo;
			}
			public int getId(){
    			return tipo.getId();
			}
    	}

    	public enum TIPO_ENDERECO{
    		EMPRESA(1),
    		PESSOA(2),
            AVULSO(3);
    		final int id;
    		TIPO_ENDERECO(int id){
    			this.id = id;
			}
			public int getId(){return id;}
			public static TIPO_ENDERECO getTipoEndereco(int id){

				if(TIPO_ENDERECO.EMPRESA.id == id) return TIPO_ENDERECO.EMPRESA;
				else if(TIPO_ENDERECO.PESSOA.id == id)  return TIPO_ENDERECO.PESSOA;
				else return null;

			}
    	}
    	
    	public enum TIPO_TAREFA{
    		ABERTA(1),
    		USUARIO(2),
    		CATEGORIA_PERMISSAO(3),
    		VEICULO(4),
    		VEICULO_USUARIO(5);
    		final int id;
    		TIPO_TAREFA(int id){this.id=id;}
    	}
    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************

    	String addressDestino = null;

    	
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOTarefa(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOTarefa(this.getDatabase());

    }

	public boolean isCoordenadaDestinoDefinida(){
		if(getStrValueOfAttribute(EXTDAOTarefa.DESTINO_LATITUDE_INT) == null) return false;
		else return true;
	}
	
	public boolean isCoordenadaOrigemDefinida(){
		if(getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_LATITUDE_INT) == null) return false;
		else return true;
	}
	
	public void updateCoordenadaEnderecoOrigem(){
//		try{
//		if(isCoordenadaOrigemDefinida()) return;
//		String vEndereco = getAddressOrigem();
//		if(vEndereco != null){
//			GeoPoint vPoint = HelperMapa.getGeoPointDoEndereco(vEndereco);
//			if(vPoint != null){
//				setAttrValue(EXTDAOTarefa.ORIGEM_LATITUDE_INT, String.valueOf(vPoint.getLatitudeE6()));
//				setAttrValue(EXTDAOTarefa.ORIGEM_LATITUDE_INT, String.valueOf(vPoint.getLongitudeE6()));
//				update(true);
//			}
//		}
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//		
//		}
	}

	public void updateCoordenadaEnderecoDestino() throws Exception{
//		if(isCoordenadaDestinoDefinida()) return;
//
//		String vEndereco = getAddressDestino();
//		if(vEndereco != null){
////			GeoPoint vPoint = HelperMapa.getGeoPointDoEndereco(vEndereco);
////			if(vPoint != null){
////				setAttrValue(EXTDAOTarefa.DESTINO_LATITUDE_INT, String.valueOf(vPoint.getLatitudeE6()));
////				setAttrValue(EXTDAOTarefa.DESTINO_LATITUDE_INT, String.valueOf(vPoint.getLongitudeE6()));
////				update(true);
////			}
//		}
	}
	
	public static LinkedHashMap<String,String> getAllTipoTarefaOrdenada(Context pContext){
		LinkedHashMap<String, String > vHash = new  LinkedHashMap<String, String >();
		
		String tipoUsuario = pContext.getString(R.string.form_tipo_tarefa_ordenada_usuario);
		String tipoCategoriaPermissao = pContext.getString(R.string.form_tipo_tarefa_ordenada_categoria_permissao);
		String tipoVeiculo = pContext.getString(R.string.form_tipo_tarefa_ordenada_veiculo);
		String tipoMotorista = pContext.getString(R.string.form_tipo_tarefa_ordenada_motorista);
		String tipoAberta= pContext.getString(R.string.form_tipo_tarefa_ordenada_aberta);
		
		vHash.put(String.valueOf(getIdTipoTarefa(TIPO_TAREFA.ABERTA)), tipoAberta);
		vHash.put(String.valueOf(getIdTipoTarefa(TIPO_TAREFA.USUARIO)), tipoUsuario);
		vHash.put(String.valueOf(getIdTipoTarefa(TIPO_TAREFA.CATEGORIA_PERMISSAO)), tipoCategoriaPermissao);
		vHash.put(String.valueOf(getIdTipoTarefa(TIPO_TAREFA.VEICULO)), tipoVeiculo);
		vHash.put(String.valueOf(getIdTipoTarefa(TIPO_TAREFA.VEICULO_USUARIO)), tipoMotorista);
		
		return vHash;
	}
	
	public static LinkedHashMap<String,String> getAllEstadoTarefa(Context pContext){
		LinkedHashMap<String, String > vHash = new  LinkedHashMap<String, String >();
		

		String aguardando = pContext.getString(R.string.form_estado_tarefa_aguardando_inicializacao);
		String inicializada = pContext.getString(R.string.form_estado_tarefa_inicializada);
		String finalizada = pContext.getString(R.string.form_estado_tarefa_finalizada);
				

		vHash.put(String.valueOf(getIdEstadoTarefa(ESTADO_TAREFA.AGUARDANDO_INICIALIZACAO)), aguardando);
		vHash.put(String.valueOf(getIdEstadoTarefa(ESTADO_TAREFA.EM_EXECUCAO)), inicializada);
		vHash.put(String.valueOf(getIdEstadoTarefa(ESTADO_TAREFA.FINALIZADA)), finalizada);
		
		return vHash;
	}
	
	public static LinkedHashMap<String,String> getAllTipoEndereco(Context pContext){
		LinkedHashMap<String, String > vHash = new  LinkedHashMap<String, String >();
		
		String tipoEmpresa = pContext.getString(R.string.form_tipo_endereco_empresa);
		String tipoPessoa = pContext.getString(R.string.form_tipo_endereco_pessoa);
				
		vHash.put(String.valueOf(getIdTipoEndereco(TIPO_ENDERECO.EMPRESA)), tipoEmpresa);
		vHash.put(String.valueOf(getIdTipoEndereco(TIPO_ENDERECO.PESSOA)), tipoPessoa);
		
		return vHash;
	}
	

	public static TIPO_ENDERECO getTipoEnderecoById(Integer pId){
		if(pId==null)return null;
		TIPO_ENDERECO[] enderecos = TIPO_ENDERECO.values();
		for(int i = 0 ; i < enderecos.length; i++)
			if(enderecos[i].id == pId) return enderecos[i];

		return null;
	}
	
	public static Integer getIdTipoEndereco(TIPO_ENDERECO pTipoEndereco){
		if(pTipoEndereco ==null) return null;
		return pTipoEndereco.id;
	}

	public static Integer getIdEstadoTarefa(ESTADO_TAREFA pEstadoTarefa){
		if(pEstadoTarefa ==null) return null;
		return pEstadoTarefa.tipo.getId();
	}
	
	public static ESTADO_TAREFA getEstadoTarefaById(Integer pId) {
		if(pId==null)return null;
		ESTADO_TAREFA[] estados = ESTADO_TAREFA.values();
		for(int i = 0 ; i < estados.length; i++){
			if(estados[i].tipo.getId() == pId) return estados[i];
		}
		return null;
	}
	
	
	public ESTADO_TAREFA getEstadoTarefa(){
		String strIdRegistroEstado = getStrValueOfAttribute(EXTDAOTarefa.REGISTRO_ESTADO_ID_INT);
		Integer idRegistroEstado = HelperInteger.parserInteger(strIdRegistroEstado);
		if(idRegistroEstado == null)
			return ESTADO_TAREFA.AGUARDANDO_INICIALIZACAO;
		else {
			ESTADO_TAREFA estados[] = ESTADO_TAREFA.values();
			for(int i = 0 ; i < estados.length; i++){
				if(estados[i].tipo.getId() == idRegistroEstado)
					return estados[i];
			}
			return null;
		}
	}
	
	public static TIPO_TAREFA getTipoTarefaById(Integer pId){
		if(pId == null) return null;
		TIPO_TAREFA estados[] = TIPO_TAREFA.values();
		for(int i = 0 ; i < estados.length; i++){
			if(estados[i].id == pId)
				return estados[i];
		}
		return null;
	}
	
	public static Integer getIdTipoTarefa(TIPO_TAREFA pTipoTarefa){
		if(pTipoTarefa ==null) return null;
		return pTipoTarefa.id;
	}
	
	public class ContainerResponsavel{
		String nomeResponsavel;
		String tituloResponsavel;
		public ContainerResponsavel(String pTituloResponsavel, String pNomeResponsavel){
			nomeResponsavel = pNomeResponsavel;
			tituloResponsavel = pTituloResponsavel;
		}
		
		public String getTituloResponsavel(){
			return tituloResponsavel;
		}
		
		public String getNomeResponsavel(){
			return nomeResponsavel;
		}
	}
	
	public ContainerResponsavel getContainerResponsavel(Context pContext)
	{
		try{
		//Log.d(TAG,"refreshNomeTarefa()");
		
		EXTDAOTarefa.TIPO_TAREFA vTipoTarefa = getTipoTarefa();
		
		String vNomeResponsavel = null;
		String vTituloResponsavel = null;
		
		switch (vTipoTarefa) {
		case ABERTA:
			vTituloResponsavel = pContext.getString(R.string.tarefa_usuario_responsavel);
			vNomeResponsavel = pContext.getString(R.string.tarefa_aberta_responsavel);
			break;
		case USUARIO:
			String vIdUsuarioDaTarefa = getStrValueOfAttribute(EXTDAOTarefa.USUARIO_ID_INT);
			EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(getDatabase());
			vObjUsuario.setAttrValue(EXTDAOUsuario.ID, vIdUsuarioDaTarefa);
			if(vObjUsuario.select()){
				vNomeResponsavel = vObjUsuario.getStrValueOfAttribute(EXTDAOUsuario.NOME);
				vTituloResponsavel = pContext.getString(R.string.tarefa_usuario_responsavel);
			}
			break;
		case CATEGORIA_PERMISSAO:
			EXTDAOCategoriaPermissao vObjCategoriaPermissao = new EXTDAOCategoriaPermissao(getDatabase());
			vObjCategoriaPermissao.setAttrValue(EXTDAOCategoriaPermissao.ID, getStrValueOfAttribute(EXTDAOTarefa.CATEGORIA_PERMISSAO_ID_INT));
			
			String vIdUsuarioDaTarefaCP = getStrValueOfAttribute(EXTDAOTarefa.USUARIO_ID_INT);
			String vNomeUsuarioCP = "";
			if(vIdUsuarioDaTarefaCP != null){
				EXTDAOUsuario vObjUsuarioCP = new EXTDAOUsuario(getDatabase());
				vObjUsuarioCP.setAttrValue(EXTDAOUsuario.ID, vIdUsuarioDaTarefaCP);
				
				if(vObjUsuarioCP.select())
					vNomeUsuarioCP = vObjUsuarioCP.getStrValueOfAttribute(EXTDAOUsuario.NOME);
			}
			if(vObjCategoriaPermissao.select()){
				vNomeResponsavel = vObjCategoriaPermissao.getStrValueOfAttribute(EXTDAOCategoriaPermissao.NOME);
				if(vNomeUsuarioCP != null && vNomeUsuarioCP.length() > 0 )
					vNomeResponsavel = vNomeUsuarioCP + " " +vNomeResponsavel;
				vTituloResponsavel = pContext.getString(R.string.tarefa_categoria_permissao_responsavel);
			}
			break;
		case VEICULO:
			EXTDAOVeiculo vObjVeiculo = new EXTDAOVeiculo(getDatabase());
			vObjVeiculo.setAttrValue(EXTDAOVeiculo.ID, getStrValueOfAttribute(EXTDAOTarefa.VEICULO_ID_INT));
			
			String vIdUsuarioDaTarefaV = getStrValueOfAttribute(EXTDAOTarefa.USUARIO_ID_INT);
			String vNomeUsuarioV = null;
			if(vIdUsuarioDaTarefaV != null){
				EXTDAOUsuario vObjUsuarioV = new EXTDAOUsuario(getDatabase());
				vObjUsuarioV.setAttrValue(EXTDAOUsuario.ID, vIdUsuarioDaTarefaV);
				
				if(vObjUsuarioV.select())
					vNomeUsuarioV = vObjUsuarioV.getStrValueOfAttribute(EXTDAOUsuario.NOME);
			}
			if(vObjVeiculo.select()){
				vNomeResponsavel = vObjVeiculo.getDescricao();
				if(vNomeUsuarioV != null && vNomeUsuarioV.length() > 0 )
					vNomeResponsavel = vNomeUsuarioV + " " +vNomeResponsavel;
				vTituloResponsavel = pContext.getString(R.string.tarefa_veiculo_responsavel);
			}
			
			break;
//		case VEICULO_USUARIO:
//			EXTDAOVeiculoUsuario vObjVeiculoUsuario = new EXTDAOVeiculoUsuario(getDatabase());
//			vObjVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.ID, getStrValueOfAttribute(EXTDAOTarefa.VEICULO_USUARIO_ID_INT));
//			if(vObjVeiculoUsuario.select()){
//				vNomeResponsavel = vObjVeiculoUsuario.getNome();
//				vTituloResponsavel = pContext.getString(R.string.tarefa_veiculo_usuario_responsavel);
//			}
//			break;
		default:
			
			break;
		}
		if(vTituloResponsavel != null && vNomeResponsavel != null && 
				vNomeResponsavel.length() > 0 && vTituloResponsavel.length() > 0 ){
			return new ContainerResponsavel(vTituloResponsavel, vNomeResponsavel);
		}else{
			return null;
		}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
		return null;
		}
	}
	
	public boolean isTarefaMinhaOuAbertaATodoUsuario() throws Exception{
		EXTDAOTarefa.TIPO_TAREFA vTipoTarefa = getTipoTarefa();
		switch (vTipoTarefa) {
		case ABERTA:
			return true;
			
		case CATEGORIA_PERMISSAO:
			EXTDAOUsuarioCategoriaPermissao vObjUCP = new EXTDAOUsuarioCategoriaPermissao(getDatabase());
			String vCategoriaPermissao = getStrValueOfAttribute(EXTDAOTarefa.CATEGORIA_PERMISSAO_ID_INT);
			vObjUCP.setAttrValue(EXTDAOTarefa.CATEGORIA_PERMISSAO_ID_INT, vCategoriaPermissao);
			vObjUCP.setAttrValue(EXTDAOTarefa.USUARIO_ID_INT, OmegaSecurity.getIdUsuario());
			if(vObjUCP.getFirstTupla() != null)
				return true;
			else return false;
			
		case VEICULO:
			EXTDAOVeiculoUsuario vObjUV = new EXTDAOVeiculoUsuario(getDatabase());
			String vVeiculo = getStrValueOfAttribute(EXTDAOTarefa.VEICULO_ID_INT);
			vObjUV.setAttrValue(EXTDAOVeiculoUsuario.VEICULO_ID_INT, vVeiculo);
			vObjUV.setAttrValue(EXTDAOVeiculoUsuario.USUARIO_ID_INT, OmegaSecurity.getIdUsuario());
			if(vObjUV.getFirstTupla() != null)
				return true;
			else return false;
			
//		case VEICULO_USUARIO:
//			EXTDAOVeiculoUsuario vObjUV2 = new EXTDAOVeiculoUsuario(getDatabase());
//			String vVeiculoUsuario = getStrValueOfAttribute(EXTDAOTarefa.VEICULO_USUARIO_ID_INT);
//			vObjUV2.setAttrValue(EXTDAOVeiculoUsuario.ID, vVeiculoUsuario);
//			if(vObjUV2.select()){
//				String vIdUsuario = vObjUV2.getStrValueOfAttribute(EXTDAOVeiculoUsuario.USUARIO_ID_INT);
//				if(vIdUsuario != null && vIdUsuario.length() > 0 && vIdUsuario.compareTo(OmegaSecurity.getIdUsuario()) == 0)
//					return true;
//				else return false;
//			} else return false;
		case USUARIO:
			String vUsuarioId = getStrValueOfAttribute(EXTDAOTarefa.USUARIO_ID_INT);
			if(vUsuarioId.compareTo(OmegaSecurity.getIdUsuario()) == 0)
				return true;
			else return false;
		default:
			break;
		}
		
		return false;
	}
	
	public String getIdentificadorTipoTarefa(){
		TIPO_TAREFA vTipoTarefa = getTipoTarefa();
		switch (vTipoTarefa) {
		case ABERTA:
			return null;
			
		case USUARIO:
			return getStrValueOfAttribute(EXTDAOTarefa.USUARIO_ID_INT);		
			
		case CATEGORIA_PERMISSAO:
			
			return getStrValueOfAttribute(EXTDAOTarefa.CATEGORIA_PERMISSAO_ID_INT);
		case VEICULO:
			return getStrValueOfAttribute(EXTDAOTarefa.VEICULO_ID_INT);
		
//		case VEICULO_USUARIO:
//
//			return getStrValueOfAttribute(EXTDAOTarefa.VEICULO_USUARIO_ID_INT);
		default:
			return null;
		}
	}


	public AppointmentPlaceItemList getAppointementOrigem() throws Exception {

		return getAppointementPlace(
				EXTDAOTarefa.ORIGEM_CIDADE_ID_INT
				, EXTDAOTarefa.ORIGEM_LOGRADOURO
				,EXTDAOTarefa.ORIGEM_NUMERO
		);
	}

        public AppointmentPlaceItemList getAppointementDestino() throws Exception {

            return getAppointementPlace(
                    EXTDAOTarefa.DESTINO_CIDADE_ID_INT
                    , EXTDAOTarefa.DESTINO_LOGRADOURO
                    ,EXTDAOTarefa.DESTINO_NUMERO
            );
        }


		public TIPO_TAREFA getTipoTarefa(){
		
		String vUsuario= getStrValueOfAttribute(USUARIO_ID_INT);
		String vVeiculo = getStrValueOfAttribute(VEICULO_ID_INT);
//		String vVeiculoUsuario = getStrValueOfAttribute(VEICULO_USUARIO_ID_INT);
		String vCategoriaPermissao = getStrValueOfAttribute(CATEGORIA_PERMISSAO_ID_INT);
		
		if(vCategoriaPermissao != null) return TIPO_TAREFA.CATEGORIA_PERMISSAO;
		else if(vVeiculo != null) return TIPO_TAREFA.VEICULO;
//		else if(vVeiculoUsuario != null) return TIPO_TAREFA.VEICULO_USUARIO;
		else if(vUsuario != null) return TIPO_TAREFA.USUARIO;
		else return TIPO_TAREFA.ABERTA;
	}
	

	
	@Override
	public void populate() {
		
		EXTDAOTarefa obj = new EXTDAOTarefa(super.getDatabase());
		obj.insert(true);
	}	


	public String getNomeTarefa(){


			return getDatabase().getContext().getString(R.string.tarefa) + " " + this.getIdFormatadaParaExibicao();

		
	}
	public String getDesc1Tarefa(){
		String vTitulo = getStrValueOfAttribute(EXTDAOTarefa.TITULO);
		if(vTitulo != null && vTitulo.length() > 0 ){
			if(vTitulo.length() > 30)
				return vTitulo.substring(0, 30);
			else return vTitulo.substring(0, vTitulo.length() - 1);
		}else{
			return null;
		}
	}

		public String getDesc2Tarefa(){
			String vTitulo = getStrValueOfAttribute(EXTDAOTarefa.DESCRICAO);
			if(vTitulo != null && vTitulo.length() > 0 ){
				if(vTitulo.length() > 30)
					return vTitulo.substring(0, 30);
				else return vTitulo.substring(0, vTitulo.length() - 1);
			}else{
				return null;
			}
		}

	public Date getObjDataCadastro(){
		return getDateValueOfAttribute(EXTDAOTarefa.CADASTRO_SEC, EXTDAOTarefa.CADASTRO_OFFSEC);
	}

	public String getDataCadastroFormatada(){
		Date d= getObjDataCadastro();
		if(d==null) return null;
		return HelperDate.getStringDateTime(getDatabase().getContext(), d);
	}

	public static boolean iniciarTarefa(Context c, String id) throws Exception {
		Database db = null;
		try{
			db = new DatabasePontoEletronico(c);
			EXTDAOTarefa objTarefa = new EXTDAOTarefa(db);

			if(objTarefa.select(id)){

				objTarefa.setLongAttrValue(EXTDAOTarefa.INICIO_SEC, HelperDate.getSecOffsetSegundosTimeZone(db.getContext()));
				objTarefa.setIntAttrValue(EXTDAOTarefa.INICIO_OFFSEC, HelperDate.getOffsetSegundosTimeZone());
				objTarefa.setIntAttrValue(EXTDAOTarefa.REGISTRO_ESTADO_ID_INT, ESTADO_TAREFA.EM_EXECUCAO.getId());
				objTarefa.formatToSQLite();

				return objTarefa.update(true) ;
			}
			return false;
		}finally{
			if(db != null) db.close();
		}
	}

		public static boolean finalizarTarefa(Context c, String id) throws Exception {
			Database db = null;
			try{
				db = new DatabasePontoEletronico(c);
				EXTDAOTarefa objTarefa = new EXTDAOTarefa(db);

				if(objTarefa.select(id)){

					objTarefa.setLongAttrValue(EXTDAOTarefa.FIM_SEC, HelperDate.getSecOffsetSegundosTimeZone(db.getContext()));
					objTarefa.setIntAttrValue(EXTDAOTarefa.FIM_OFFSEC, HelperDate.getOffsetSegundosTimeZone());
					objTarefa.setIntAttrValue(EXTDAOTarefa.REGISTRO_ESTADO_ID_INT, ESTADO_TAREFA.FINALIZADA.getId());
					objTarefa.formatToSQLite();

					return objTarefa.update(true) ;
				}
				return false;
			}finally{
				if(db != null) db.close();
			}
		}

        public static boolean cancelarTarefa(Context c, String id) throws Exception {
            Database db = null;
            try{
                db = new DatabasePontoEletronico(c);
                EXTDAOTarefa objTarefa = new EXTDAOTarefa(db);

                if(objTarefa.select(id)){

                    objTarefa.setLongAttrValue(EXTDAOTarefa.FIM_SEC, HelperDate.getSecOffsetSegundosTimeZone(db.getContext()));
                    objTarefa.setIntAttrValue(EXTDAOTarefa.FIM_OFFSEC, HelperDate.getOffsetSegundosTimeZone());
                    objTarefa.setIntAttrValue(EXTDAOTarefa.REGISTRO_ESTADO_ID_INT, ESTADO_TAREFA.CANCELADA.getId());
                    objTarefa.formatToSQLite();

                    return objTarefa.update(true) ;
                }
                return false;
            }finally{
                if(db != null) db.close();
            }
        }


        public class DadosEndereco {
			public TIPO_ENDERECO tipoEndereco;
			public String id;
			public String nome;
			public String idCidade;
			public String logradouro;
			public String numero;

			public DadosEndereco(
					TIPO_ENDERECO tipoEndereco,
					String id,
					String nome,
					String idCidade,
					String logradouro,
					String numero){
			    this.nome = nome;
				this.tipoEndereco=tipoEndereco;
				this.id=id;
				this.idCidade=idCidade;
				this.logradouro=logradouro;
				this.numero=numero;
			}

		}
        public DadosEndereco getDadosEnderecoOrigem(){
			String idEmpresa = getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_EMPRESA_ID_INT);
			if(idEmpresa != null){

				return new DadosEndereco(
						TIPO_ENDERECO.EMPRESA
						, idEmpresa
                        ,EXTDAOEmpresa.getNomeEmpresa(getDatabase(), idEmpresa)
						,getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_CIDADE_ID_INT)
						,getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_LOGRADOURO)
						,getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_NUMERO)
				);
			} else {
				String idPessoa =getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_PESSOA_ID_INT);
				if(idPessoa  != null){

					return new DadosEndereco(
							TIPO_ENDERECO.PESSOA
							, idPessoa
                            , EXTDAOPessoa.getNomeDaPessoa(getDatabase(), idPessoa)
							,getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_CIDADE_ID_INT)
							,getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_LOGRADOURO)
							,getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_NUMERO)
					);
				} else if(getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_CIDADE_ID_INT) != null
                        || getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_LOGRADOURO) != null) {
                    return new DadosEndereco(
                            TIPO_ENDERECO.AVULSO
                            , null
                            , null
                            ,getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_CIDADE_ID_INT)
                            ,getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_LOGRADOURO)
							,getStrValueOfAttribute(EXTDAOTarefa.ORIGEM_NUMERO)
                    );
                }
			}
			return null;
		}

		public DadosEndereco getDadosEnderecoDestino(){
			String idEmpresa = getStrValueOfAttribute(EXTDAOTarefa.DESTINO_EMPRESA_ID_INT);
			if(idEmpresa != null){

				return new DadosEndereco(
						TIPO_ENDERECO.EMPRESA
						, idEmpresa
						,EXTDAOEmpresa.getNomeEmpresa(getDatabase(), idEmpresa)
						,getStrValueOfAttribute(EXTDAOTarefa.DESTINO_CIDADE_ID_INT)
						,getStrValueOfAttribute(EXTDAOTarefa.DESTINO_LOGRADOURO)
						,getStrValueOfAttribute(EXTDAOTarefa.DESTINO_NUMERO)
				);
			} else {
				String idPessoa =getStrValueOfAttribute(EXTDAOTarefa.DESTINO_PESSOA_ID_INT);
				if(idPessoa  != null){

					return new DadosEndereco(
							TIPO_ENDERECO.PESSOA
							, idPessoa
							, EXTDAOPessoa.getNomeDaPessoa(getDatabase(), idPessoa)
							,getStrValueOfAttribute(EXTDAOTarefa.DESTINO_CIDADE_ID_INT)
							,getStrValueOfAttribute(EXTDAOTarefa.DESTINO_LOGRADOURO)
							,getStrValueOfAttribute(EXTDAOTarefa.DESTINO_NUMERO)
					);
				} else if(getStrValueOfAttribute(EXTDAOTarefa.DESTINO_CIDADE_ID_INT) != null
						|| getStrValueOfAttribute(EXTDAOTarefa.DESTINO_LOGRADOURO) != null) {
					return new DadosEndereco(
							TIPO_ENDERECO.AVULSO
							, null
							, null
							,getStrValueOfAttribute(EXTDAOTarefa.DESTINO_CIDADE_ID_INT)
							,getStrValueOfAttribute(EXTDAOTarefa.DESTINO_LOGRADOURO)
							,getStrValueOfAttribute(EXTDAOTarefa.DESTINO_NUMERO)
					);
				}
			}
			return null;
		}


    } // classe: fim
    

    