

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOTarefaCotidiano
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOTarefaCotidiano.java
        * TABELA MYSQL:    tarefa_cotidiano
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOTarefaCotidiano;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOTarefaCotidiano extends DAOTarefaCotidiano
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOTarefaCotidiano(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOTarefaCotidiano(this.getDatabase());

        }
        

        } // classe: fim


        