

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOTarefaCotidianoItem
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         EXTDAOTarefaCotidianoItem.java
        * TABELA MYSQL:    tarefa_cotidiano_item
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOTarefaCotidianoItem;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOTarefaCotidianoItem extends DAOTarefaCotidianoItem
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOTarefaCotidianoItem(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOTarefaCotidianoItem(this.getDatabase());

        }
        

        } // classe: fim


        