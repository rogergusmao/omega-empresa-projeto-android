

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOTarefaRelatorio
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         EXTDAOTarefaRelatorio.java
        * TABELA MYSQL:    tarefa_relatorio
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOTarefaRelatorio;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOTarefaRelatorio extends DAOTarefaRelatorio
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOTarefaRelatorio(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOTarefaRelatorio(this.getDatabase());

        }
        

        } // classe: fim


        