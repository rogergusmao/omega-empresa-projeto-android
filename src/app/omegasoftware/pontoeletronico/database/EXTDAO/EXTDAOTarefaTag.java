

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOTarefaTag
        * DATA DE GERAÇÃO: 19.02.2018
        * ARQUIVO:         EXTDAOTarefaTag.java
        * TABELA MYSQL:    tarefa_tag
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOTarefaTag;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOTarefaTag extends DAOTarefaTag
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOTarefaTag(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOTarefaTag(this.getDatabase());

        }
        

        } // classe: fim


        