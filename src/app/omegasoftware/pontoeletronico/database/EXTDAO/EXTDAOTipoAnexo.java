

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOTipoAnexo
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOTipoAnexo.java
    * TABELA MYSQL:    tipo_anexo
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoAnexo;

	

    
        
    public class EXTDAOTipoAnexo extends DAOTipoAnexo
    {


    	public enum TIPO_ANEXO{
    		FOTO,
    		AUDIO,
    		VIDEO,
    		ANEXO
    	}
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOTipoAnexo(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOTipoAnexo(this.getDatabase());

    }
    

	
	public static TIPO_ANEXO getTipoAnexo(String pId){
		TIPO_ANEXO vVetorTipoAnexo[] = TIPO_ANEXO.values();
		for (TIPO_ANEXO vTipoAnexo : vVetorTipoAnexo) {
			String vId = getIdTipoAnexo(vTipoAnexo);
			if(vId.compareTo(pId) == 0) return vTipoAnexo;
		}
		return null;
	}
	
	public static String getIdTipoAnexo(TIPO_ANEXO pTipo){
		switch (pTipo) {
			case FOTO:
				return "1";
			case AUDIO:
				return "2";
			case VIDEO:
				return "3";
			case ANEXO:
				return "4";
			default:
				return null;
		}
	}
    } // classe: fim
    

    