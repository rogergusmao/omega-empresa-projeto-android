

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOTipoCanalEnvio
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOTipoCanalEnvio.java
        * TABELA MYSQL:    tipo_canal_envio
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoCanalEnvio;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOTipoCanalEnvio extends DAOTipoCanalEnvio
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOTipoCanalEnvio(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOTipoCanalEnvio(this.getDatabase());

        }
        

        } // classe: fim


        