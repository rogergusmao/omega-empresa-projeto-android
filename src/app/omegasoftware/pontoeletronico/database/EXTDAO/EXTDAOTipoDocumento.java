

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOTipoDocumento
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOTipoDocumento.java
    * TABELA MYSQL:    tipo_documento
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoDocumento;

	

    
        
    public class EXTDAOTipoDocumento extends DAOTipoDocumento
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOTipoDocumento(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOTipoDocumento(this.getDatabase());

    }
    

    } // classe: fim
    

    