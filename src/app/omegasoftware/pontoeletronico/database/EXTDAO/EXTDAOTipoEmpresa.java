

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOTipoEmpresa
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOTipoEmpresa.java
    * TABELA MYSQL:    tipo_empresa
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoEmpresa;

	

    
        
    public class EXTDAOTipoEmpresa extends DAOTipoEmpresa
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOTipoEmpresa(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOTipoEmpresa(this.getDatabase());

    }
    

    } // classe: fim
    

    