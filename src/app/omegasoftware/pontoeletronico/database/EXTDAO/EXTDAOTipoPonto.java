

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOTipoPonto
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOTipoPonto.java
    * TABELA MYSQL:    tipo_ponto
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import android.app.Activity;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoPonto;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOTipoPonto extends DAOTipoPonto
    {

    	public enum TIPO_PONTO{
    		INTERVALO(3) ,
    		TURNO(1);
    		int id;
    		TIPO_PONTO(int id){
    			this.id = id;
    		}
    		public int getId(){
    			return id;
    		}
    		
    		public String getDescricao(Activity a){
    			if(this == INTERVALO){
    				return a.getResources().getString(R.string.intervalo);
    			} else 
    				return a.getResources().getString(R.string.turno);
    		}
    		
    		public static TIPO_PONTO getTipoPonto(int id){
    			TIPO_PONTO[] tipos= TIPO_PONTO.values();
    			for(int i = 0 ;  i < tipos.length; i++){
    				if(tipos[i].getId() == id)
    					return tipos[i];
    			}
    			return null;
    		}
    	}
    	
    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOTipoPonto(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOTipoPonto(this.getDatabase());

    }
    

	public static int getTipoPontoDoIdToggleButton(int pIdToggleButton){
		switch(pIdToggleButton){
			case R.id.intervalo_button:
				return 3;
				
			case R.id.turno_button:
				return 1;
				
			default:
				return 0;
		}
	}
	
    } // classe: fim
    

    