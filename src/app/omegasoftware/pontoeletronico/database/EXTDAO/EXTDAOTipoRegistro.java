

/*
*
* -------------------------------------------------------
* NOME DA CLASSE:  EXTDAOTipoRegistro
* DATA DE GERAÇÃO: 14.02.2018
* ARQUIVO:         EXTDAOTipoRegistro.java
* TABELA MYSQL:    tipo_registro
* BANCO DE DADOS:
* -------------------------------------------------------
*
*/

package app.omegasoftware.pontoeletronico.database.EXTDAO;

import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoRegistro;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;


// **********************
// DECLARAÇÃO DA CLASSE
// **********************





public class EXTDAOTipoRegistro extends DAOTipoRegistro
{
    public enum TIPO{
        tarefa(1)
        ,	compra(2)
        ,	venda(3)
        ,	negociacao_divida(4)
        ,	recebivel(5)
        ,	despesa(6)
        ,	ponto(7)
        ,	wifi(8)
        ,	nota(9);
        int id;
        TIPO(int id){this.id=id;}
        public int getId(){return this.id;}

    }
    public static void inserirRegistrosSeNecessario(Database db){
        EXTDAOTipoRegistro objRE = new EXTDAOTipoRegistro(db);
        TIPO[] ts = TIPO.values();
        for(int i = 0 ; i < ts.length; i++){
            TIPO t = ts[i];
            if(db.getResultSetAsContains("SELECT 1 FROM tipo_registro WHERE id = ? ", new String[]{String.valueOf( t.getId())}))
                continue;
            objRE.setIntAttrValue(EXTDAOTipoRegistro.ID, t.id);
            objRE.setAttrValue(EXTDAOTipoRegistro.NOME, t.toString());

            objRE.formatToSQLite();

            objRE.insert(false);
        }
    }

    // *************************
    // DECLARAÇÃO DE ATRIBUTOS
    // *************************

    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOTipoRegistro(Database database){
        super(database);

    }

    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOTipoRegistro(this.getDatabase());

    }


} // classe: fim


        