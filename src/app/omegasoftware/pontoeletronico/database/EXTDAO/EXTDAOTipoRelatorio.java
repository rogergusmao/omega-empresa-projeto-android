

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOTipoRelatorio
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOTipoRelatorio.java
        * TABELA MYSQL:    tipo_relatorio
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoRelatorio;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOTipoRelatorio extends DAOTipoRelatorio
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOTipoRelatorio(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOTipoRelatorio(this.getDatabase());

        }
        

        } // classe: fim


        