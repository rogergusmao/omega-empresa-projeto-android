

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOTipoTarefa
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOTipoTarefa.java
        * TABELA MYSQL:    tipo_tarefa
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoTarefa;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOTipoTarefa extends DAOTipoTarefa
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOTipoTarefa(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOTipoTarefa(this.getDatabase());

        }
        

        } // classe: fim


        