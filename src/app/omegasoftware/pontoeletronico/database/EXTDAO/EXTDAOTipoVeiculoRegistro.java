

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOTipoVeiculoRegistro
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOTipoVeiculoRegistro.java
        * TABELA MYSQL:    tipo_veiculo_registro
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOTipoVeiculoRegistro;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOTipoVeiculoRegistro extends DAOTipoVeiculoRegistro
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOTipoVeiculoRegistro(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOTipoVeiculoRegistro(this.getDatabase());

        }
        

        } // classe: fim


        