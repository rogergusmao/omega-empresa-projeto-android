

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOUf
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOUf.java
    * TABELA MYSQL:    uf
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.LinkedHashMap;

import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOUf;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOUf extends DAOUf
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOUf(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOUf(this.getDatabase());

    }
    

	public LinkedHashMap<String, String> getUfFromPais(String pPaisId)
	{
		this.clearData();
		this.setAttrValue(EXTDAOUf.PAIS_ID_INT, pPaisId);
		
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		for(Table iEntry : this.getListTable(new String[]{EXTDAOUf.NOME}))
		{
			EXTDAOUf uf = (EXTDAOUf) iEntry;
			String id = uf.getAttribute(EXTDAOUf.ID).getStrValue();
			String dsc = uf.getAttribute(EXTDAOUf.NOME).getStrValue();
			if(dsc != null){
				hashTable.put(id, dsc.toUpperCase());
			}
			
		}
		
		return hashTable;
		
	}
	
	

	public LinkedHashMap<String, String> getAllCidades()
	{
		
		this.clearData();
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		
		for(Table iEntry : this.getListTable(new String[]{EXTDAOUf.NOME}))
		{
			EXTDAOUf cidade = (EXTDAOUf) iEntry;
			String strIdCidade = cidade.getAttribute(EXTDAOUf.ID).getStrValue();
			String strNomeCidade = cidade.getAttribute(EXTDAOUf.NOME).getStrValue();
			 
			if(strNomeCidade != null){
				String strNomeCidadeModificado = strNomeCidade.toUpperCase();
				hashTable.put(strIdCidade, strNomeCidadeModificado);	
			}
			
		}
		
		return hashTable;
		
	}
    } // classe: fim
    

    