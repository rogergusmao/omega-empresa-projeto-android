

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOUsuario
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOUsuario.java
    * TABELA MYSQL:    usuario
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.Tuple;
import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuario;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador.STATE_SINCRONIZADOR;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho;

	

    
        
    public class EXTDAOUsuario extends DAOUsuario
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOUsuario(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOUsuario(this.getDatabase());

    }
    
    public String getDescricao(){
    	return getStrValueOfAttribute(EXTDAOUsuario.EMAIL);
    }
    

	public EXTDAOUsuarioCorporacao getObjUsuarioCorporacao(){
		EXTDAOUsuarioCorporacao vObjUC = new EXTDAOUsuarioCorporacao(this.getDatabase());
		vObjUC.setAttrValue(EXTDAOUsuarioCorporacao.USUARIO_ID_INT, this.getId());
		vObjUC.setAttrValue(EXTDAOUsuarioCorporacao.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		ArrayList<Table> vList = vObjUC.getListTable();
		
		if(vList != null){
			if(vList.size() == 1){
				EXTDAOUsuarioCorporacao vTupla = (EXTDAOUsuarioCorporacao)vList.get(0);
				return vTupla;
			}
		}
		return null;
	}
	
	public static LinkedHashMap<String, String> getAllUsuario(Database db)
	{
		 
		
		EXTDAOUsuario vObj = new EXTDAOUsuario(db);
		return vObj.getHashMapIdByDefinition(true);
	}
	
	
	
	public EXTDAOUsuarioCategoriaPermissao getObjUsuarioCategoriaPermissao(){
		EXTDAOUsuarioCategoriaPermissao vObjUCP = new EXTDAOUsuarioCategoriaPermissao(this.getDatabase());
		vObjUCP.setAttrValue(EXTDAOUsuarioCategoriaPermissao.USUARIO_ID_INT, this.getId());
		return (EXTDAOUsuarioCategoriaPermissao) vObjUCP.getFirstTupla(); 
		
	}
	
	public EXTDAOPessoaUsuario getObjPessoaUsuario(){
		EXTDAOPessoaUsuario vObjUCP = new EXTDAOPessoaUsuario(this.getDatabase());
		vObjUCP.setAttrValue(EXTDAOPessoaUsuario.USUARIO_ID_INT, this.getId());
		return (EXTDAOPessoaUsuario) vObjUCP.getFirstTupla(); 
	}
	
	public EXTDAOPessoa getObjPessoa() throws Exception{
		EXTDAOPessoaUsuario objPessoaUsuario = new EXTDAOPessoaUsuario(this.getDatabase());
		objPessoaUsuario.setAttrValue(EXTDAOPessoaUsuario.USUARIO_ID_INT, this.getId());
		
		
		Table regPessoaUsuario = (EXTDAOPessoaUsuario) objPessoaUsuario.getFirstTupla();
		if(regPessoaUsuario == null ){
			EXTDAOPessoa objPessoa = new EXTDAOPessoa(getDatabase());
			objPessoa.setAttrValue(EXTDAOPessoa.EMAIL, getStrValueOfAttribute(EXTDAOUsuario.EMAIL));
			Table regPessoa = objPessoa.getFirstTupla();
			
			if(regPessoa == null){
				Long idPessoa= null;
				objPessoa.setAttrValue(EXTDAOPessoa.EMAIL, getStrValueOfAttribute(EXTDAOUsuario.EMAIL));
				objPessoa.setAttrValue(EXTDAOPessoa.NOME, getStrValueOfAttribute(EXTDAOUsuario.NOME));
				objPessoa.formatToSQLite();
				idPessoa = objPessoa.insert(true);
				if(idPessoa == null){
					
					return null;
				} 
					
			} else{
				
				objPessoa = (EXTDAOPessoa) regPessoa;
			}
			objPessoaUsuario.clearData();
			objPessoaUsuario.setAttrValue(EXTDAOPessoaUsuario.PESSOA_ID_INT, objPessoa.getId());
			objPessoaUsuario.setLongAttrValue(EXTDAOPessoaUsuario.USUARIO_ID_INT, getLongId());
			objPessoaUsuario.formatToSQLite();
			objPessoaUsuario.insert(true);
			
			return null;
		} else {
			objPessoaUsuario = (EXTDAOPessoaUsuario) regPessoaUsuario;
			Table regPessoa = objPessoaUsuario.getObjDaChaveExtrangeira(EXTDAOPessoaUsuario.PESSOA_ID_INT);
			Long idPessoa = null;
			if(regPessoa == null){
				EXTDAOPessoa objPessoa = new EXTDAOPessoa(getDatabase());
				objPessoa.setAttrValue(EXTDAOPessoa.EMAIL, getStrValueOfAttribute(EXTDAOUsuario.EMAIL));
				regPessoa = objPessoa.getFirstTupla();
				if(regPessoa == null){
					objPessoa.setAttrValue(EXTDAOPessoa.EMAIL, getStrValueOfAttribute(EXTDAOUsuario.EMAIL));
					objPessoa.setAttrValue(EXTDAOPessoa.NOME, getStrValueOfAttribute(EXTDAOUsuario.NOME));
					objPessoa.formatToSQLite();
					idPessoa =  objPessoa.insert(true);
					if(idPessoa == null)
						return null;
				}else idPessoa = regPessoa.getLongId();
				
				objPessoaUsuario.setLongAttrValue(EXTDAOPessoaUsuario.PESSOA_ID_INT, idPessoa);
				objPessoaUsuario.formatToSQLite();
				if(!objPessoaUsuario.update(true))
					return null;
			

				return objPessoa;
			}
			return (EXTDAOPessoa)regPessoa;
		}
	}
	
	public String getCategoriaPermissao(){
		try{
		EXTDAOUsuarioCategoriaPermissao vObjUCP = new EXTDAOUsuarioCategoriaPermissao(this.getDatabase());
		vObjUCP.setAttrValue(EXTDAOUsuarioCategoriaPermissao.USUARIO_ID_INT, this.getStrValueOfAttribute(EXTDAOUsuario.ID));
		EXTDAOUsuarioCategoriaPermissao vTuplaUCP = (EXTDAOUsuarioCategoriaPermissao) vObjUCP.getFirstTupla(); 
		
		if(vTuplaUCP != null) {
			String vStrCPId = vTuplaUCP.getStrValueOfAttribute(EXTDAOUsuarioCategoriaPermissao.CATEGORIA_PERMISSAO_ID_INT);
			EXTDAOCategoriaPermissao vObjCP = new EXTDAOCategoriaPermissao(getDatabase());
			vObjCP.setAttrValue(EXTDAOCategoriaPermissao.ID, vStrCPId);
			if(vObjCP.select()){
				return vObjCP.getStrValueOfAttribute(EXTDAOCategoriaPermissao.NOME);
			}
			else return null;
			
		} else return null;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
		return null;
		}
	}
	
	public int procedimentoApagaSenhaDeTodosUsuarios(){

		try{
			
			ContentValues vCVSenha = new ContentValues();
			vCVSenha.putNull(SENHA);
			return getDatabase().update(NAME,  vCVSenha, null, null);
			
		} catch(Exception ex){
			
		}
		return 0;
	}
	
	public int procedimentoAtualizaSenhaDoUsuarioLogado(){

		try{
			
			if(select(OmegaSecurity.getIdUsuario())){
				setAttrValue(SENHA, OmegaSecurity.getSenha());
				
				if(update(false))
					return 1;
				else return 0;
				
			}
		} catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BANCO);
		}
		return 0;
	}
	
	public EXTDAOCategoriaPermissao getObjCategoriaPermissao() throws Exception{
		EXTDAOUsuarioCategoriaPermissao vObjUCP = new EXTDAOUsuarioCategoriaPermissao(this.getDatabase());
		vObjUCP.setAttrValue(EXTDAOUsuarioCategoriaPermissao.USUARIO_ID_INT, this.getStrValueOfAttribute(EXTDAOUsuario.ID));
		
		ArrayList<Table> vList = vObjUCP.getListTable();
		if(vList == null) return null;
		else if(vList.size() == 0 ) return null;
		else if(vList.size() == 1){
			EXTDAOUsuarioCategoriaPermissao vTuplaUCP = (EXTDAOUsuarioCategoriaPermissao)vList.get(0);
			String vStrCPId = vTuplaUCP.getStrValueOfAttribute(EXTDAOUsuarioCategoriaPermissao.CATEGORIA_PERMISSAO_ID_INT);
			EXTDAOCategoriaPermissao vObjCP = new EXTDAOCategoriaPermissao(getDatabase());
			vObjCP.setAttrValue(EXTDAOCategoriaPermissao.ID, vStrCPId);
			 
			if(vObjCP.select()){
				return vObjCP;
			}
			else return null;
			
		} else return null;
	}
	public static LinkedHashMap<String, String> getAllUsuario(DatabasePontoEletronico db)
	{
		EXTDAOUsuario vObj = new EXTDAOUsuario(db);
		return vObj.getHashMapIdByDefinition(true);
	}
	public static boolean existeNecessidadeDeCarregarAsPermissoes(Database db, String email, String corporacao){
		
		Long idCorporacao = EXTDAOCorporacao.getIdCorporacao(db, corporacao);
		if(idCorporacao == null) return true;
		String strCorporacao = idCorporacao.toString();
		return !db.exists("SELECT 1 "
				+ " FROM usuario u "
				// ou ele eh adm entao nao precisa
				+ "		JOIN usuario_corporacao uc "
				+ "			ON u.id = uc.usuario_id_INT "
				+ " WHERE u.email = ?  "
				+ "	 	AND uc.is_adm_BOOLEAN = 1  "
				+ "		AND uc.corporacao_id_INT = ? "
				
				+ "		UNION ALL  "
				
				// ou ele possui uma vinculacao a uma categoria de permissao sincronizada
				//para esses dois casos nao hu a necessidade de resgatar as permissoes
				+ "	SELECT 1 "
				+ "	FROM usuario u2 "
				+ "		JOIN usuario_corporacao uc2 "
				+ "			ON u2.id = uc2.usuario_id_INT "
				+ "		JOIN usuario_categoria_permissao ucp "
				+ "			ON u2.id = ucp.usuario_id_INT "
				+ " WHERE u2.email = ? "
				+ "		AND uc2.is_adm_BOOLEAN != 1 "
				+ "		AND uc2.corporacao_id_INT = ?  ", 
				new String[]{email, strCorporacao,
						email, strCorporacao});
	}
	
	public static Tuple<String, String> getNomeEEmail(Activity a, String id) throws OmegaDatabaseException{
		Database db = null;
		try{
			db = new DatabasePontoEletronico(a);
			Cursor cursor = null;
			try {
				cursor = db.rawQuery("SELECT nome, email FROM usuario WHERE id = ?", new String[]{id});
				
				if (cursor.moveToFirst()) {
					
					String nome = cursor.isNull(0) ? null : cursor.getString(0);
					String email= cursor.isNull(1) ? null : cursor.getString(1);
					return new Tuple<String,String>(nome, email);
				}
				return null;

			} finally{
				if(cursor != null){
					cursor.close();
				}
			}	
		}finally{
			if(db != null) db.close();
		}
	}
	
	public static int removerUsuariosQueNaoSaoDaCorporacao(Database db) throws Exception{
		
		int tot = db.delete(
				"usuario",
				"(SELECT 1 FROM usuario_corporacao uc WHERE uc.usuario_id_INT = id AND uc.corporacao_id_INT = ?) IS NULL", 
				new String[]{OmegaSecurity.getIdCorporacao()});
		return tot;
	}
	
	
	public static Long getIdUsuarioDoEmail(Database db, String email){
		String q= "SELECT id FROM usuario WHERE email=?";
		return db.getFirstLong(q, new String[]{email});
	}
	
	public static Long[] getIdUsuarioeIdPessoaECorporacaoDoEmail(Database db, String email, String corporacao){
		String q= "SELECT pu.usuario_id_INT , pu.pessoa_id_INT, pu.corporacao_id_INT "
				+ " FROM usuario u "
				+ "		JOIN pessoa_usuario pu "
				+ "			ON u.id = pu.usuario_id_INT "
				+ "		JOIN corporacao c"
				+ "			ON c.id = pu.corporacao_id_INT "
				+ " WHERE u.email=? AND c.nome = ?";
		return db.getThreeLongs(q, new String[]{email, corporacao});
	}
	
    } // classe: fim
    

    