

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOUsuarioCategoriaPermissao
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOUsuarioCategoriaPermissao.java
    * TABELA MYSQL:    usuario_categoria_permissao
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.ArrayList;
import java.util.HashMap;

import android.database.Cursor;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuarioCategoriaPermissao;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOUsuarioCategoriaPermissao extends DAOUsuarioCategoriaPermissao
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOUsuarioCategoriaPermissao(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOUsuarioCategoriaPermissao(this.getDatabase());

    }

    public static String getIdCategoriaPermissaoDoUsuario(Database db, String idUsuario){
    	EXTDAOUsuarioCategoriaPermissao obj = new EXTDAOUsuarioCategoriaPermissao(db);
		obj.setAttrValue(EXTDAOUsuarioCategoriaPermissao.USUARIO_ID_INT, OmegaSecurity.getIdUsuario());
		Table tupla = obj.getFirstTupla();
		String categoriaPermissao = tupla.getStrValueOfAttribute(EXTDAOUsuarioCategoriaPermissao.CATEGORIA_PERMISSAO_ID_INT);
		return categoriaPermissao;
    }
    

    public static HashMap<Integer, String> getListaPermissaoDaTela(Database db, String idPermissaoPai){
    	String q = "SELECT p.id, p.tag " +
    	" FROM " + 
    	" permissao p " + 
    	" JOIN permissao_categoria_permissao pcp ON pcp.permissao_id_INT = p.id " + 
    	" JOIN categoria_permissao cp ON pcp.categoria_permissao_id_INT = cp.id ";
    	if(idPermissaoPai == null)
    		q += " WHERE p.pai_permissao_id_INT IS NULL ";
    	else q += " WHERE p.pai_permissao_id_INT = ? ";
    	
		
		String[] vetorArg = new String[] {idPermissaoPai};
		
		
		
		Cursor cursor = null;
				
		HashMap<Integer, String> hash = new HashMap<Integer, String>();
		try{
			cursor = db.rawQuery(q, vetorArg);
			
			
			if (cursor.moveToFirst()) {
				
				Integer id = HelperInteger.parserInteger( cursor.getString(0));
				String tag = cursor.getString(1);
				if(id == null || tag == null){
					throw new Exception("Consulta invalida: " + q);
				}
				
				hash.put(id, tag);
			}
		} catch(Exception ex){
			SingletonLog.insereErro(
					TAG, 
					"select", 
					HelperExcecao.getDescricao(ex), 
					SingletonLog.TIPO.BANCO);
		}finally{
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
		}
		return hash;
    }
    public static HashMap<Integer, String> getListaPermissaoDaTelaParaACategoriaDePermissao(Database db, String idCategoriaPermissao, String idPermissaoPai){
    	 
    	String q = "SELECT p.id " +
    	" FROM " + 
    	" permissao p " + 
    	" JOIN permissao_categoria_permissao pcp ON pcp.permissao_id_INT = p.id " + 
    	" JOIN categoria_permissao cp ON pcp.categoria_permissao_id_INT = cp.id ";
    	
    	if(idPermissaoPai == null)
    		q += " WHERE p.pai_permissao_id_INT IS NULL ";
    	else q += " WHERE p.pai_permissao_id_INT = ? ";
    	
    	q += " AND cp.id = ?"; 
		
		String[] vetorArg = null;
		if(idPermissaoPai == null)
			vetorArg = new String[] {idCategoriaPermissao};
		else 
			vetorArg = new String[] {idPermissaoPai, idCategoriaPermissao};
		
		
		Cursor cursor = null;
		
		
		ArrayList<Integer> list = new ArrayList<Integer>();

		HashMap<Integer, String> hash = new HashMap<Integer, String>();
		try{
			cursor = db.rawQuery(q, vetorArg);
			if (cursor.moveToFirst()) {
				
				Integer id = HelperInteger.parserInteger( cursor.getString(0));
				String tag = cursor.getString(1);
				if(id == null || tag == null){
					throw new Exception("Consulta invalida: " + q);
				}
				
				hash.put(id, tag);
			}
		} catch(Exception ex){
			SingletonLog.insereErro(
					TAG, 
					"select", 
					HelperExcecao.getDescricao(ex), 
					SingletonLog.TIPO.BANCO);
		} finally{
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
		}
		return hash;
    }

    } // classe: fim
    

    