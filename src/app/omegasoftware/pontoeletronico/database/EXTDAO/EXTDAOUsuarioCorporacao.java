

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOUsuarioCorporacao
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOUsuarioCorporacao.java
    * TABELA MYSQL:    usuario_corporacao
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuarioCorporacao;

	

    
        
    public class EXTDAOUsuarioCorporacao extends DAOUsuarioCorporacao
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOUsuarioCorporacao(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOUsuarioCorporacao(this.getDatabase());

    }
    

    } // classe: fim
    

    