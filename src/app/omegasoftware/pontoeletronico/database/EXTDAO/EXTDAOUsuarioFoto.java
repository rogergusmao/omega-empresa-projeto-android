

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOUsuarioFoto
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOUsuarioFoto.java
    * TABELA MYSQL:    usuario_foto
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.ArrayList;

import android.database.Cursor;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;

import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuarioFoto;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOUsuarioFoto extends DAOUsuarioFoto
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOUsuarioFoto(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOUsuarioFoto(this.getDatabase());

    }
    

	public class ContainerUsuarioFoto{
		String strTotalTupla;
		ArrayList<String> listStrId = new ArrayList<String>();
		public ContainerUsuarioFoto(
				String pStrTotalTupla,
				ArrayList<String> pListStrId){
			strTotalTupla= pStrTotalTupla;
			listStrId = pListStrId;
		}
		
		public int getCount(){
			return listStrId.size();
		}
		
		public ArrayList<String> getListStrId(){
			return listStrId;
		}
		public String getStrTotalTupla(){
			return strTotalTupla;
		}
	}
	
	public ContainerUsuarioFoto getStrListLastTuplas(String pNumeroDeTupla){		
		Cursor cursor = null;
		try{
			

			int vTotalCampos = 5;
			
			
			String vQuery = "SELECT vup." + EXTDAOUsuarioFoto.ID + " AS up_id, " +
						" vup." +EXTDAOUsuarioFoto.CADASTRO_SEC + " AS data, " +
						" vup." + EXTDAOUsuarioFoto.CADASTRO_OFFSEC + " AS time, " +
						" vup." + EXTDAOUsuarioFoto.USUARIO_ID_INT + " AS usuario, " +
						" vup." + EXTDAOUsuarioFoto.FOTO_INTERNA + " AS foto_interna, " +
						" vup." + EXTDAOUsuarioFoto.FOTO_EXTERNA + " AS foto_externa " +
					" FROM " + EXTDAOUsuarioFoto.NAME + " AS vup " + 
					" WHERE vup." + EXTDAOUsuarioFoto.CORPORACAO_ID_INT + " = ? AND " +
					" vup." + EXTDAOUsuarioFoto.USUARIO_ID_INT + " = ? " +
					" ORDER BY vup." + EXTDAOUsuarioFoto.ID + " DESC " ;
			if(pNumeroDeTupla.length() > 0 )
					vQuery += " LIMIT 0, " + pNumeroDeTupla;

			cursor = database.rawQuery(vQuery, 
					new String[]{
					OmegaSecurity.getIdCorporacao(), 
					OmegaSecurity.getIdUsuario()});

			String vTotalTupla = "";
			ArrayList<String> vListStrId = new ArrayList<String>();
			if (cursor.moveToFirst()) {
				do {
					String vTupla = "";
					for(int i = 0;  i < vTotalCampos ; i ++){
						String vToken = cursor.getString(i);
						if(i == 0 && vToken != null && vToken.length() > 0 ){
							vListStrId.add(vToken);
						}
						else {
//							Nao imprime o ID
							if(vTupla.length() > 0 )
								vTupla += ";" + HelperString.getStrAndCheckIfIsNull(vToken);
							else 
								vTupla +=  HelperString.getStrAndCheckIfIsNull(vToken);
						}
					}
					if(vTupla.length() > 0 ){
						if(vTotalTupla.length() > 0 )
							vTotalTupla += "%%" + vTupla;
						else
							vTotalTupla += vTupla;
					}
						
				} while (cursor.moveToNext());
			}

			if (cursor != null && !cursor.isClosed()) {	    	  
				cursor.close();
			}
			if(vTotalTupla.length() > 0 )
				return new ContainerUsuarioFoto(vTotalTupla, vListStrId);
			else return null;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.BANCO);
			//if(ex != null)
				//Log.d(TAG, ex.getMessage());
		}finally{
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
		}
		return null;
	}

    } // classe: fim
    

    