

        /*
        *
        * -------------------------------------------------------
        * NOME DA CLASSE:  EXTDAOUsuarioHorarioTrabalho
        * DATA DE GERAÇÃO: 14.02.2018
        * ARQUIVO:         EXTDAOUsuarioHorarioTrabalho.java
        * TABELA MYSQL:    usuario_horario_trabalho
        * BANCO DE DADOS:  
        * -------------------------------------------------------
        *
        */

        package app.omegasoftware.pontoeletronico.database.EXTDAO;

        import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuarioHorarioTrabalho;
        import app.omegasoftware.pontoeletronico.database.Attribute;
        import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
        import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
        import app.omegasoftware.pontoeletronico.database.Database;
        import app.omegasoftware.pontoeletronico.database.Table;


        // **********************
        // DECLARAÇÃO DA CLASSE
        // **********************


    
        

        public class EXTDAOUsuarioHorarioTrabalho extends DAOUsuarioHorarioTrabalho
        {


        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************
        
        // *************************
        // CONSTRUTOR
        // *************************
        public EXTDAOUsuarioHorarioTrabalho(Database database){
            super(database);

        }
        
        // *************************
        // FACTORY
        // *************************
        public Table factory(){
            return new EXTDAOUsuarioHorarioTrabalho(this.getDatabase());

        }
        

        } // classe: fim


        