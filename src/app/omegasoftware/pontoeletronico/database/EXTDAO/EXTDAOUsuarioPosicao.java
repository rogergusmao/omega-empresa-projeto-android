

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOUsuarioPosicao
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOUsuarioPosicao.java
    * TABELA MYSQL:    usuario_posicao
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuarioPosicao;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSControler;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************
import app.omegasoftware.pontoeletronico.service.ServiceTiraFoto;

	

    
        
    public class EXTDAOUsuarioPosicao extends DAOUsuarioPosicao
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOUsuarioPosicao(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOUsuarioPosicao(this.getDatabase());

    }
    

	public class ContainerUsuarioPosicao{
		String strTotalTupla;
		ArrayList<String> listStrId = new ArrayList<String>();
		String lastId;
		public ContainerUsuarioPosicao(
				String pStrTotalTupla,
				ArrayList<String> pListStrId,
				String lastId){
			this.strTotalTupla= pStrTotalTupla;
			this.listStrId = pListStrId;
			this.lastId=lastId;
		}
		
		public int getCount(){
			return listStrId.size();
		}
		
		public ArrayList<String> getListStrId(){
			return listStrId;
		}
		public String getStrTotalTupla(){
			return strTotalTupla;
		}
		public String getLastId(){
			return lastId;
		}
	}
	
	public ContainerUsuarioPosicao getContainerUsuarioPosicaoDoBanco(String pNumeroDeTupla){		

		try{
			

			int vTotalCampos = 11;
			String vQuery = "SELECT up." + EXTDAOUsuarioPosicao.ID + " AS up_id, " +
						"up." +EXTDAOUsuarioPosicao.CADASTRO_SEC + " AS data, " +
						"up." + EXTDAOUsuarioPosicao.CADASTRO_OFFSEC + " AS time, " +
						"up." + EXTDAOUsuarioPosicao.VEICULO_USUARIO_ID_INT + " AS veiculo_usuario, " +
						"up." + EXTDAOUsuarioPosicao.USUARIO_ID_INT + " AS usuario, " +
						"up." + EXTDAOUsuarioPosicao.LONGITUDE_INT + " AS longitude, " +
						"up." + EXTDAOUsuarioPosicao.LATITUDE_INT + " AS latitude, " +
						"up." + EXTDAOUsuarioPosicao.VELOCIDADE_INT + " AS velocidade, " +
						"up." + EXTDAOUsuarioPosicao.FOTO_INTERNA + " AS foto_interna, " +
						"up." + EXTDAOUsuarioPosicao.FOTO_EXTERNA + " AS foto_externa, " +
						"up." + EXTDAOUsuarioPosicao.FONTE_INFORMACAO_INT +
					" FROM " + EXTDAOUsuarioPosicao.NAME + " AS up " + 
					" WHERE up." + EXTDAOUsuarioPosicao.CORPORACAO_ID_INT + " = ? " +
					" ORDER BY up." + EXTDAOUsuarioPosicao.ID + " DESC " + 
					" LIMIT 0, " + String.valueOf(pNumeroDeTupla);

			Cursor cursor = null;
			StringBuilder vTotalTupla = new StringBuilder();
			ArrayList<String> vListStrId = new ArrayList<String>();
			Long lastId = null;
			try{
				cursor = database.rawQuery(vQuery, new String[]{OmegaSecurity.getIdCorporacao()});

				if (cursor.moveToFirst()) {
					do {
						String vTupla = "";
						for(int i = 0;  i < vTotalCampos ; i ++){
							
							if(i == 0 ){
								Long idAtual = cursor.getLong(0);
								if(lastId == null || lastId < idAtual) 
									lastId = idAtual;
								
								vListStrId.add(idAtual.toString());
							}
							else {
								String vToken = cursor.getString(i);
//								Nao imprime o ID
								if(vTupla.length() > 0 )
									vTupla += ";" + HelperString.getStrAndCheckIfIsNull(vToken);
								else 
									vTupla +=  HelperString.getStrAndCheckIfIsNull(vToken);
							}
						}
						if(vTupla.length() > 0 ){
							if(vTotalTupla.length() > 0 )
								vTotalTupla .append( "%%" + vTupla);
							else
								vTotalTupla.append( vTupla);
						}
							
					} while (cursor.moveToNext());
				}	
			}finally{
				if(cursor != null && !cursor.isClosed())
					cursor.close();
			}
			if(vTotalTupla.length() > 0 )
				return new ContainerUsuarioPosicao(vTotalTupla.toString(), vListStrId, lastId.toString());
			else return null;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.UTIL_ANDROID);
		}
		return null;
	}
	
	//	Pega a lista de Funcionario_Veiculo dos veiculos ativos no momento
	public ArrayList<Long> getListIdLastPositionOfOtherCars(String pMyIdFuncionarioVeiculo) {

		
		Database database = super.getDatabase();
		
		try {
			
			
			String vQuery = "SELECT vup.id AS vup_id, vu.id AS id, MAX(vup.data_DATE || vup.hora_TIME) AS data_hora " +   
					" FROM usuario_posicao AS vup " +
					" LEFT JOIN veiculo_usuario AS vu ON vu.id=vup.veiculo_usuario_id_INT " +
					" WHERE vu.id != ? "  +
					" GROUP BY vu.id";
 
			ArrayList<String> args = new ArrayList<String>();
			args.add(pMyIdFuncionarioVeiculo);

			String[] vetorArg = new String [args.size()];
			args.toArray(vetorArg);
			Cursor cursor = null;

			//TODO o ciclo abaixo seleciona apenas a ultima posicao de cada veiculo,
			//isso deve ser realizado na propria consulta			
			
			
			ArrayList<Long> vNewListId = new ArrayList<Long>();
			try{
				cursor = database.rawQuery(vQuery, vetorArg);
	
				if (cursor.moveToFirst()) {
					do {
						String vStrId = cursor.getString(0);
						try{
							Long vId = Long.parseLong( vStrId);
							if(vId != null)
								vNewListId.add(vId);	
						}catch(Exception ex){
							
						}
						
					} while (cursor.moveToNext());
				}
			}finally{
				if(cursor != null && ! cursor.isClosed()){
					cursor.close();
				}
			}

			return vNewListId;
		} catch (Exception e) {
			
			SingletonLog.insereErro(e, SingletonLog.TIPO.UTIL_ANDROID);
		}
		return null;
	}
	
	
	public static void procedureInsertLocationInDatabase(
			Context contexto,
			int lat,
			int lng, 
			Float speed,
			GPSControler.TIPO_GPS tipoGps){
		
		Database db = null;
		try{
			if(!OmegaSecurity.isAutenticacaoRealizada()){
				return;
			}
			db = new DatabasePontoEletronico(contexto);
			EXTDAOUsuarioPosicao objUsuarioPosicao = new EXTDAOUsuarioPosicao (db); 
			long utc = HelperDate.getTimestampSegundosUTC(contexto);
			int offset = HelperDate.getOffsetSegundosTimeZone();
			
			String vStrLatitude = String.valueOf(lat);
			String vStrLongitude = String.valueOf(lng);
			
			objUsuarioPosicao.clearData();
			
			if(OmegaSecurity.getIdVeiculoUsuario() != null && OmegaSecurity.getIdVeiculoUsuario().length() > 0 )
					objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.VEICULO_USUARIO_ID_INT, OmegaSecurity.getIdVeiculoUsuario());
			objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.USUARIO_ID_INT, OmegaSecurity.getIdUsuario());
			//-15.837967133675603
			objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.LATITUDE_INT, vStrLatitude);
			objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.LONGITUDE_INT, vStrLongitude);
			if(speed!= null){
				objUsuarioPosicao.setAttrValue(
						EXTDAOUsuarioPosicao.VELOCIDADE_INT, 
						String.valueOf((int)(speed* 3.6)));	
			}

			String vLastNomeFotoInternaUsuario = ServiceTiraFoto.getLastNomeFotoInternaInserida();
			if(vLastNomeFotoInternaUsuario != null){
				objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.FOTO_INTERNA, vLastNomeFotoInternaUsuario);
				ServiceTiraFoto.clearLastNomeFotoInternaInserida();
			}

			objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.CADASTRO_SEC, String.valueOf(utc));
			objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.CADASTRO_OFFSEC, String.valueOf(offset));
			objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.FONTE_INFORMACAO_INT, tipoGps.getStrId());
			
			objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			
			objUsuarioPosicao.insert(false);
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.BANCO);
		}
		finally{
			if(db != null) db.close();
		}
		
		
	}
	
	public static boolean deleteAllRegisters(Database db, String lastId){
		try {
			db.delete(
					"usuario_posicao"
					,"id <= ? AND corporacao_id_INT = ?"
					,new String[]{
							 lastId
							, OmegaSecurity.getIdCorporacao()});
			return true;
		} catch (Exception e) {
			SingletonLog.insereErro(e, SingletonLog.TIPO.UTIL_ANDROID);
			return false;
		}
	}
	
    } // classe: fim
    

    