

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOUsuarioServico
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOUsuarioServico.java
    * TABELA MYSQL:    usuario_servico
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOUsuarioServico;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOUsuarioServico extends DAOUsuarioServico
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOUsuarioServico(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOUsuarioServico(this.getDatabase());

    }

	public static EXTDAOUsuarioServico getObjUsuarioServico(Database pDatabase , String pIdUsuario, String pIdServico){
		if(pDatabase == null) return null;
//		carregaListaDeServicosDoUsuario(pDatabase, pIdUsuario);
		EXTDAOUsuarioServico vUsuarioServico = new EXTDAOUsuarioServico(pDatabase);
		vUsuarioServico.setAttrValue(EXTDAOUsuarioServico.SERVICO_ID_INT, pIdServico);
		vUsuarioServico.setAttrValue(EXTDAOUsuarioServico.USUARIO_ID_INT, pIdUsuario);
		vUsuarioServico.setAttrValue(EXTDAOUsuarioServico.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		return (EXTDAOUsuarioServico) vUsuarioServico.getFirstTupla();
	}
	
//public static void carregaListaDeServicosDoUsuario(Database db, String usuario){
//	EXTDAOUsuarioServico vUsuarioServico = new EXTDAOUsuarioServico(db);
//	
//	vUsuarioServico.setAttrStrValue(EXTDAOUsuarioServico.USUARIO_ID_INT, usuario);
//	vUsuarioServico.setAttrStrValue(EXTDAOUsuarioServico.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
//	ArrayList<Table> tuplasServicos = vUsuarioServico.getListTable();
//	String listIdUsuario[] = new String[tuplasServicos.size()];
//	String listIdServico[] = new String[tuplasServicos.size()];
//	String listIdUsuarioServico[] = new String[tuplasServicos.size()];
//	for(int i = 0 ; i < tuplasServicos.size(); i++){
//		listIdUsuario[i] = tuplasServicos.get(i).getStrValueOfAttribute(EXTDAOUsuarioServico.USUARIO_ID_INT);
//		listIdServico[i] = tuplasServicos.get(i).getStrValueOfAttribute(EXTDAOUsuarioServico.SERVICO_ID_INT);
//		listIdUsuarioServico[i] = tuplasServicos.get(i).getStrValueOfAttribute(EXTDAOUsuarioServico.ID);
//	}
//	return;
//}


    } // classe: fim
    

    