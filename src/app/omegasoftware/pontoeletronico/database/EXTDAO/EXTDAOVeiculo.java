

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOVeiculo
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOVeiculo.java
    * TABELA MYSQL:    veiculo
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.LinkedHashMap;

import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOVeiculo;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOVeiculo extends DAOVeiculo
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOVeiculo(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOVeiculo(this.getDatabase());

    }
    
    public String getDescricao() throws Exception{
		String nome = "";
		EXTDAOModelo vObjModelo = new EXTDAOModelo(getDatabase());
		vObjModelo.setAttrValue(EXTDAOModelo.ID, getStrValueOfAttribute(MODELO_ID_INT));
		if(vObjModelo.select()){
			String vNomeModelo = vObjModelo.getStrValueOfAttribute(EXTDAOModelo.NOME);
			if(vNomeModelo != null && vNomeModelo.length() > 0) nome = vNomeModelo;
		}
		String vPlaca = getStrValueOfAttribute(PLACA);
		if(vPlaca != null && vPlaca.length() > 0)
			nome += (nome == null || nome.length() ==0) ? vPlaca : " " + vPlaca;
		return nome;
	}
    

	public static LinkedHashMap<String, String> getAllVeiculo(Database db)
	{
		EXTDAOVeiculo vObj = new EXTDAOVeiculo(db);
		return vObj.getHashMapIdByDefinition(true);
	}
	
	
    } // classe: fim
    

    