

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOVeiculoRegistro
    * DATA DE GERAuuO: 03.12.2014
    * ARQUIVO:         EXTDAOVeiculoRegistro.java
    * TABELA MYSQL:    veiculo_registro
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOVeiculoRegistro;
        
    
    // **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOVeiculoRegistro extends DAOVeiculoRegistro
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOVeiculoRegistro(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOVeiculoRegistro(this.getDatabase());

    }
    

    } // classe: fim
    

    