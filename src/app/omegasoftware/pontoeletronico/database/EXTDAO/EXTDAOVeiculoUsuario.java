

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOVeiculoUsuario
    * DATA DE GERAuuO: 08.06.2013
    * ARQUIVO:         EXTDAOVeiculoUsuario.java
    * TABELA MYSQL:    veiculo_usuario
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import java.util.ArrayList;
import java.util.LinkedHashMap;

import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOVeiculoUsuario;
// **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOVeiculoUsuario extends DAOVeiculoUsuario
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOVeiculoUsuario(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOVeiculoUsuario(this.getDatabase());

    }
    

	public String getNome() throws Exception{
		String nome = "";
		
			
		EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(getDatabase());
		vObjUsuario.setAttrValue(EXTDAOUsuario.ID, getStrValueOfAttribute(EXTDAOUsuario.ID));
		if(vObjUsuario.select()){
			String vNomeUsuario = vObjUsuario.getStrValueOfAttribute(EXTDAOUsuario.NOME);
			if(vNomeUsuario != null && vNomeUsuario.length() > 0 )
				nome += vNomeUsuario;
		}
		
		EXTDAOVeiculo vObjVeiculo = new EXTDAOVeiculo(getDatabase());
		vObjVeiculo.setAttrValue(EXTDAOVeiculo.ID, getStrValueOfAttribute(EXTDAOVeiculoUsuario.VEICULO_ID_INT));
		if(vObjVeiculo.select()){
			String vNomeVeiculo = vObjVeiculo.getDescricao();
			nome = (nome != null && nome.length() > 0) ? " " + vNomeVeiculo : vNomeVeiculo;
		}
		return nome;
	
	}
	
	public static String getIdVeiculoUsuario(Database db, String pIdUsuario, String pIdVeiculo){
		if(pIdUsuario == null || pIdVeiculo == null) return null;
		else{
			EXTDAOVeiculoUsuario vObj = new EXTDAOVeiculoUsuario(db);
			
			vObj.setAttrValue(EXTDAOVeiculoUsuario.VEICULO_ID_INT, pIdVeiculo);
			vObj.setAttrValue(EXTDAOVeiculoUsuario.USUARIO_ID_INT, pIdUsuario);
			Table vTupla = vObj.getFirstTupla();
			if(vTupla == null) return null;
			else return vTupla.getStrValueOfAttribute(EXTDAOVeiculoUsuario.ID);	
		}
	}
	
	public void desocupaVeiculo(
			String idVeiculo){
		if(idVeiculo != null && idVeiculo.length() > 0){
			clearData();
			setAttrValue(EXTDAOVeiculoUsuario.VEICULO_ID_INT, idVeiculo);
			setAttrValue(EXTDAOVeiculoUsuario.IS_ATIVO_BOOLEAN, "1");
			ArrayList<Table> registros = getListTable();
			if(registros != null ){
				//Caso ocorra algum erro durante a setagem de direuuo do veuculo
				//setamos todos os motoristas do veuculo para inativo
				
				for(int i = 0 ; i < registros.size(); i++){
					Table regVeiculoUsuario = registros.get(i);
					regVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.IS_ATIVO_BOOLEAN, "0");
					regVeiculoUsuario.formatToSQLite();
					regVeiculoUsuario.update(true);
				}	
				
			}
		}
	}
	
	public boolean dirigirVeiculo(String idVeiculoUsuario) throws Exception{
		
		if(this.select(idVeiculoUsuario)){
			
			String idVeiculo =this.getStrValueOfAttribute(EXTDAOVeiculoUsuario.VEICULO_ID_INT);
			desocupaVeiculo(idVeiculo);
			this.clearData();
			this.select(idVeiculoUsuario);
			this.setAttrValue(EXTDAOVeiculoUsuario.IS_ATIVO_BOOLEAN, "1");
			this.formatToSQLite();
			return this.update(true);
		} else 
			return false;
		
	}
	
	public Table dirigirVeiculo(String idVeiculo, String idUsuario){
		
		desocupaVeiculo(idVeiculo);
		this.clearData();
		this.setAttrValue(EXTDAOVeiculoUsuario.VEICULO_ID_INT, idVeiculo);
		this.setAttrValue(EXTDAOVeiculoUsuario.USUARIO_ID_INT, idUsuario);
		Table reg = this.getFirstTupla();
		if(reg != null){
			reg.setAttrValue(EXTDAOVeiculoUsuario.IS_ATIVO_BOOLEAN, "1");
			reg.formatToSQLite();
			if(reg.update(true)){
				return reg;
			}
		} 
		return null;
		
	}
	
	public class VeiculoDirigido{
		public EXTDAOVeiculoUsuario objVeiculoUsuario;
		public EXTDAOVeiculo objVeiculo;
		
		public VeiculoDirigido(
				EXTDAOVeiculoUsuario objVeiculoUsuario,
				EXTDAOVeiculo objVeiculo){
			this.objVeiculoUsuario = objVeiculoUsuario;
			this.objVeiculo = objVeiculo;
		}
	}
	public VeiculoDirigido factoryVeiculoDirigido(EXTDAOVeiculoUsuario objVeiculoUsuario,
			EXTDAOVeiculo objVeiculo){
		return new VeiculoDirigido(objVeiculoUsuario, objVeiculo);
	}
	
	public static VeiculoDirigido getVeiculoSendoDirigidoPeloUsuario(
			Database db, 
			String idUsuario) throws Exception{
		if(idUsuario != null && idUsuario.length() > 0){
			EXTDAOVeiculoUsuario obj = new EXTDAOVeiculoUsuario(db);
			obj.setAttrValue(EXTDAOVeiculoUsuario.USUARIO_ID_INT, idUsuario);
			obj.setAttrValue(EXTDAOVeiculoUsuario.IS_ATIVO_BOOLEAN, "1");
			ArrayList<Table> registros = obj.getListTable();
			if(registros != null ){
				//Caso ocorra algum erro durante a setagem de direuuo do veuculo
				//setamos todos os motoristas do veuculo para inativo
				if(registros.size() > 1){
					for(int i = 0 ; i < registros.size(); i++){
						Table regVeiculoUsuario = registros.get(i);
						regVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.IS_ATIVO_BOOLEAN, "0");
						regVeiculoUsuario.formatToSQLite();
						regVeiculoUsuario.update(true);
					}	
					return null;
				} else if(registros.size() == 1){
					Table regVeiculoUsuario =registros.get(0);
					Table regVeiculo = regVeiculoUsuario.getObjDaChaveExtrangeira(EXTDAOVeiculoUsuario.VEICULO_ID_INT);
					if(regVeiculo != null){
						return  obj.factoryVeiculoDirigido(
								(EXTDAOVeiculoUsuario) regVeiculoUsuario, 
								(EXTDAOVeiculo) regVeiculo);
					} else return null;
				}
					
			} else return null;
			
		}
		return null;
	}
	
	//retorna todo veiculo existente na tabela VeiculoUsuario
	public static LinkedHashMap<String, String > getAllVeiculo(Database pDatabase) throws Exception{
		EXTDAOVeiculoUsuario vVeiculoUsuario = new EXTDAOVeiculoUsuario(pDatabase);
		ArrayList<Table> vList = vVeiculoUsuario.getListTable();
		LinkedHashMap<String, String > vHash = new  LinkedHashMap<String, String >();
		for (Table vObj : vList) {

			String vVeiculo = vObj.getStrValueOfAttribute(EXTDAOVeiculoUsuario.VEICULO_ID_INT);			
			EXTDAOVeiculo vObjVeiculo = new EXTDAOVeiculo(pDatabase);
			vObjVeiculo.setAttrValue(EXTDAOVeiculo.ID, vVeiculo);
			vObjVeiculo.select();
			String vPlaca= vObjVeiculo.getStrValueOfAttribute(EXTDAOVeiculo.PLACA);
			vHash.put(vVeiculo, vPlaca );
		}
		return vHash;
	}
	
	//retorna todo usuario do veiculo em questao existente na tabela VeiculoUsuario
		public static LinkedHashMap<String, String > getAllUsuarioDoVeiculo(Database pDatabase, String pIdVeiculo) throws Exception{
			EXTDAOVeiculoUsuario vVeiculoUsuario = new EXTDAOVeiculoUsuario(pDatabase);
			vVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.VEICULO_ID_INT, pIdVeiculo);
			ArrayList<Table> vList = vVeiculoUsuario.getListTable();
			LinkedHashMap<String, String > vHash = new  LinkedHashMap<String, String >();
			for (Table vObj : vList) {
				
				String vUsuario = vObj.getStrValueOfAttribute(EXTDAOVeiculoUsuario.USUARIO_ID_INT);
				EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(pDatabase);
				vObjUsuario.setAttrValue(EXTDAOUsuario.ID, vUsuario);
				vObjUsuario.select();
				String vNome= vObjUsuario.getStrValueOfAttribute(EXTDAOUsuario.NOME);
				vHash.put(vUsuario, vNome);
			}
			return vHash;
		}
	
	//retorna todo usuario existente na tabela VeiculoUsuario
	public static LinkedHashMap<String, String > getAllVeiculoDoUsuario(Database pDatabase, String pIdUsuario) throws Exception{
		EXTDAOVeiculoUsuario vVeiculoUsuario = new EXTDAOVeiculoUsuario(pDatabase);
		vVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.USUARIO_ID_INT, pIdUsuario);
		ArrayList<Table> vList = vVeiculoUsuario.getListTable();
		LinkedHashMap<String, String > vHash = new  LinkedHashMap<String, String >();
		for (Table vObj : vList) {
			String vVeiculo = vObj.getStrValueOfAttribute(EXTDAOVeiculoUsuario.VEICULO_ID_INT);			
			EXTDAOVeiculo vObjVeiculo = new EXTDAOVeiculo(pDatabase);
			vObjVeiculo.setAttrValue(EXTDAOVeiculo.ID, vVeiculo);
			vObjVeiculo.select();
			String desc= vObjVeiculo.getDescricao();
			vHash.put(vVeiculo, desc);
		}
		return vHash;
	}
		
	//retorna todo usuario existente na tabela VeiculoUsuario
	public static LinkedHashMap<String, String > getAllUsuario(Database pDatabase) throws Exception{
		EXTDAOVeiculoUsuario vVeiculoUsuario = new EXTDAOVeiculoUsuario(pDatabase);
		ArrayList<Table> vList = vVeiculoUsuario.getListTable();
		LinkedHashMap<String, String > vHash = new  LinkedHashMap<String, String >();
		for (Table vObj : vList) {
			
			String vUsuario = vObj.getStrValueOfAttribute(EXTDAOVeiculoUsuario.USUARIO_ID_INT);
			EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(pDatabase);
			vObjUsuario.setAttrValue(EXTDAOUsuario.ID, vUsuario);
			vObjUsuario.select();
			String vNome= vObjUsuario.getStrValueOfAttribute(EXTDAOUsuario.NOME);
			vHash.put(vUsuario, vNome);
		}
		return vHash;
	}
	
	
	public static LinkedHashMap<String, String > getAllVeiculoUsuario(Database pDatabase) throws Exception{
		EXTDAOVeiculoUsuario vVeiculoUsuario = new EXTDAOVeiculoUsuario(pDatabase);
		ArrayList<Table> vList = vVeiculoUsuario.getListTable();
		LinkedHashMap<String, String > vHash = new  LinkedHashMap<String, String >();
		for (Table vObj : vList) {
			String vId = vObj.getStrValueOfAttribute(EXTDAOVeiculoUsuario.ID);
			String vVeiculo = vObj.getStrValueOfAttribute(EXTDAOVeiculoUsuario.VEICULO_ID_INT);
			String vUsuario = vObj.getStrValueOfAttribute(EXTDAOVeiculoUsuario.USUARIO_ID_INT);
			
			EXTDAOVeiculo vObjVeiculo = new EXTDAOVeiculo(pDatabase);
			vObjVeiculo.setAttrValue(EXTDAOVeiculo.ID, vVeiculo);
			vObjVeiculo.select();
			String vPlaca= vObjVeiculo.getStrValueOfAttribute(EXTDAOVeiculo.PLACA);
			
			EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(pDatabase);
			vObjUsuario.setAttrValue(EXTDAOUsuario.ID, vUsuario);
			vObjUsuario.select();
			String vNome= vObjUsuario.getStrValueOfAttribute(EXTDAOUsuario.NOME);
			vHash.put(vId, vPlaca + " " + vNome);
		}
		return vHash;
	}
    } // classe: fim
    

    