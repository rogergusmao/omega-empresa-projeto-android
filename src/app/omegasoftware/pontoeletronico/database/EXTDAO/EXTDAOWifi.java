

/*
*
* -------------------------------------------------------
* NOME DA CLASSE:  EXTDAOWifi
* DATA DE GERAuuO: 03.12.2014
* ARQUIVO:         EXTDAOWifi.java
* TABELA MYSQL:    wifi
* BANCO DE DADOS:
* -------------------------------------------------------
*
*/

package app.omegasoftware.pontoeletronico.database.EXTDAO;

import android.net.wifi.ScanResult;

import java.util.ArrayList;
import java.util.List;

import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.container.ContainerWifi;
import app.omegasoftware.pontoeletronico.database.DAO.DAOWifiRegistro;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOWifi;
import app.omegasoftware.pontoeletronico.database.Tuple;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;


// **********************
// DECLARAuuO DA CLASSE
// **********************





public class EXTDAOWifi extends DAOWifi
{


    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************

    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOWifi(Database database){
        super(database);

    }

    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOWifi(this.getDatabase());

    }

    public void removerRegistrosDaEmpresa( String idEmpresa) throws Exception {
        Database db=getDatabase();
        List<Long> ids= db.getResultSetAsListLong(
                "SELECT id FROM wifi WHERE empresa_id_INT = ? AND corporacao_id_INT = ?",
                new String[]{ idEmpresa,
                        OmegaSecurity.getIdCorporacao()},
                0);
        if(ids != null && ids.size() > 0){
            for (Long id :
                    ids) {
                if(id==null)continue;
                db.delete(getName(), "id = ?", new String[]{ id.toString()});
                insertTableSynchronize(
                        this.getName(),
                        id.toString(),
                        EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE,
                        OmegaSecurity.getIdCorporacao(), OmegaSecurity.getIdUsuario());


            }
        }
    }

    public static boolean empresaJaMapeada(Database db, String idEmpresa){

        return db.getResultSetAsContains(
                "SELECT 1 FROM wifi WHERE empresa_id_INT= ? AND corporacao_id_INT = ?",
                new String[]{idEmpresa, OmegaSecurity.getIdCorporacao()});
    }

    public static String getNomesWifi(Database db, String idEmpresa){

        List<String> nomes = db.getResultSetAsListString(
                "SELECT ssid FROM wifi WHERE empresa_id_INT= ? AND corporacao_id_INT = ?"
                , new String[]{idEmpresa, OmegaSecurity.getIdCorporacao()}
                ,0);

        return HelperString.arrayToString(nomes, ", ");
    }

    private static void atualizaDataFinal(String idWifiRegistro, EXTDAOWifiRegistro objWR, boolean sync) throws Exception {
        int offsec = HelperDate.getOffsetSegundosTimeZone();
        long sec =HelperDate.getSecOffsetSegundosTimeZone(objWR.getDatabase().getContext());
        if(objWR.select(idWifiRegistro)){
            objWR.setAttrValue(EXTDAOWifiRegistro.DATA_FIM_SEC, String.valueOf( sec));
            objWR.setAttrValue(EXTDAOWifiRegistro.DATA_FIM_OFFSEC, String.valueOf( offsec));
            objWR.formatToSQLite();
            objWR.update(sync);
        }

    }
    public static void registraProximidadeDeRedeWifiSeExistente(
            Database db, List<ScanResult> results) throws Exception {

        ContainerWifi registroWifi =EXTDAOWifiRegistro.getDadosUltimaWifiAcessada(db,OmegaSecurity.getIdUsuario());
        EXTDAOWifiRegistro objWR = new EXTDAOWifiRegistro(db);
        if(results == null || results.size() == 0){
            if(registroWifi != null){
                //se existia um registro em aberto dizendo que o wifi estava conectado
                //esse registro será finalizado

                EXTDAOWifi.atualizaDataFinal(String.valueOf(
                        registroWifi.idWifiRegistro)
                        , objWR
                        , true);
                EXTDAOWifiRegistro.clearCache();

            }
            return ;
        }

        //Dentro os registros da rede wifi que cobre atualmetne o celular
        //iremos montar a consulta para buscar os wifi's que são conhecidos pela
        //corporacao
        StringBuilder sbIn = new StringBuilder();

        List<String> parametros = new ArrayList<String>();
        parametros.add(OmegaSecurity.getIdCorporacao());
        sbIn.append("?");

        for (ScanResult sr:
                results ) {

            parametros.add(sr.BSSID );

            if(sbIn.length()>0)sbIn.append(", ");
            sbIn.append("?");

        }
        String[] s = new String[parametros.size()];
        parametros.toArray(s);
        List<Tuple<Long,String>> wifisDaCorporacao = db.getResultSetAsListTupleLongString(
                "SELECT id, bssid" +
                        " FROM wifi" +
                        " WHERE corporacao_id_INT = ?" +
                        "    AND bssid IN ("+sbIn.toString()+")" +
                        " ORDER BY link_speed DESC"
                , s
                ,0);




        if(registroWifi != null){

            //dentre as redes identificadas ele verifica se existia algum registro
            //em aberto, ou seja, se ja havia detectado que ele estava na rede wifi
            if(wifisDaCorporacao != null){
                for (Tuple<Long,String> dado :
                        wifisDaCorporacao) {
                    if (dado == null) continue;
                    //se o registro que estava em aberto foi reencontrado
                    //iremos atualizar sua data final
                    if (dado.y != null
                            && dado.y.compareTo(registroWifi.bssid) == 0) {

                        long sec = HelperDate.getSecOffsetSegundosTimeZone(db.getContext());

                        boolean registroMuitoAntigo = false;
                        //verifica se nao eh um registro muito antigo, pois se for
                        //nao iremos atualizar sua data final
                        if (registroWifi.dataFimSec != null) {

                            long totMili = sec - registroWifi.dataFimSec;

                            if (totMili > 2 * OmegaConfiguration.SERVICE_WIFI_TIMER_INTERVAL + 5000) {
                                registroMuitoAntigo = true;
                            }
                        }
                        //se for a segunda vez de incidencia a data final está nula
                        //ou se o registro for muito antigo, tipo um registro de ontem
                        //o celular desligou dentro da area, e não autalizou mais o horario
                        //final. Então iremos considerar aquele meio tempo como se ele estivesse
                        //offline
                        if (registroWifi.dataFimSec == null || !registroMuitoAntigo) {
                            //se ele estiver realmente online, atualizamos a data final de conexao
                            if (objWR.select(String.valueOf(registroWifi.idWifi))) {

                                EXTDAOWifi.atualizaDataFinal(String.valueOf(
                                        registroWifi.idWifiRegistro)
                                        , objWR
                                        , false);
                                EXTDAOWifiRegistro.updateSecCache(sec);
                                return;
                            }
                        }
                    }
                }
            }
        }
        //registraremos o wifi apenas para o sinal mais forte
        if(wifisDaCorporacao!= null && wifisDaCorporacao.size() > 0) {
            //se o registro do wifi for novo
            Tuple<Long, String> t = wifisDaCorporacao.get(0);

            objWR.clearData();
            objWR.setAttrValue(DAOWifiRegistro.WIFI_ID_INT, t.x.toString());
            objWR.setAttrValue(DAOWifiRegistro.USUARIO_ID_INT, OmegaSecurity.getIdUsuario());
            long utc = HelperDate.getTimestampSegundosUTC(db.getContext());

            objWR.setAttrValue(DAOWifiRegistro.DATA_INICIO_SEC, String.valueOf(utc));
            objWR.setAttrValue(DAOWifiRegistro.DATA_INICIO_OFFSEC, String.valueOf(HelperDate.getOffsetSegundosTimeZone()));
            objWR.formatToSQLite();
            objWR.insert(true);

            if(registroWifi != null){
                objWR.clearData();
                if(objWR.select(String.valueOf( registroWifi.idWifiRegistro))){
                    objWR.update(true);
                }
            }
        }
        //apaga a cache tanto se for um caso novo, quanto se for um caso que não
        //encontrou wifi nenhuma
        EXTDAOWifiRegistro.clearCache();
    }

} // classe: fim
    

    