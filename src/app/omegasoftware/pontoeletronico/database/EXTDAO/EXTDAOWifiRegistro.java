

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  EXTDAOWifiRegistro
    * DATA DE GERAuuO: 03.12.2014
    * ARQUIVO:         EXTDAOWifiRegistro.java
    * TABELA MYSQL:    wifi_registro
    * BANCO DE DADOS:  
    * -------------------------------------------------------
    *
    */
    
    package app.omegasoftware.pontoeletronico.database.EXTDAO;

    import android.database.Cursor;

    import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
    import app.omegasoftware.pontoeletronico.common.container.ContainerWifi;
    import app.omegasoftware.pontoeletronico.database.DAO.DAOWifi;
    import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.DAO.DAOWifiRegistro;
    import app.omegasoftware.pontoeletronico.database.Tuple;


    // **********************
    // DECLARAuuO DA CLASSE
    // **********************

	

    
        
    public class EXTDAOWifiRegistro extends DAOWifiRegistro
    {

    
    // *************************
    // DECLARAuuO DE ATRIBUTOS
    // *************************
    
    // *************************
    // CONSTRUTOR
    // *************************
    public EXTDAOWifiRegistro(Database database){
        super(database);

    }
    
    // *************************
    // FACTORY
    // *************************
    public Table factory(){
        return new EXTDAOWifiRegistro(this.getDatabase());

    }
        static class Cache{
            public ContainerWifi t ;
            public String idUsuario;
            public Cache(String idUsuario, ContainerWifi t ){
                this.t=t;
                this.idUsuario=idUsuario;
            }
            public boolean cacheValida(String idUsuario){
                if(this.idUsuario == null) return false;
                else if(this.idUsuario.compareTo(idUsuario) != 0) return  false;
                else return true;
            }

        }
        static Cache cacheWifi = null;
        public static void clearCache(){
            cacheWifi = null;
        }

        public static void updateSecCache(long sec){
            if(cacheWifi != null) cacheWifi.t.dataFimSec = sec;
        }
        public static ContainerWifi getDadosUltimaWifiAcessada(Database db, String idUsuario){


            Cursor cursor = null;
            try {
                if(cacheWifi != null && cacheWifi.cacheValida(idUsuario))
                    return cacheWifi.t;
                cursor = db.rawQuery(
                        "SELECT  wr.wifi_id_INT" +
                                ", w.bssid" +
                                ", wr.data_fim_SEC" +
                                ", wr.data_fim_OFFSEC" +
                                ", wr.id  " +
                        " FROM wifi_registro wr " +
                        "   JOIN wifi w " +
                        "       ON wr.wifi_id_INT = w.id " +
                        " WHERE wr.usuario_id_INT= ? " +
                        "   AND wr.corporacao_id_INT = ? " +
                        " ORDER BY wr.data_inicio_SEC DESC" +
                        " LIMIT 0,1" ,
                        new String[]{
                                idUsuario,
                                OmegaSecurity.getIdCorporacao()
                        });

                if (cursor.moveToFirst()) {
                    do {

                        ContainerWifi c = new ContainerWifi();
                        c.idWifi =cursor.getLong(0);
                        c.bssid=cursor.getString(1);
                        c.dataFimSec=cursor.isNull(2) ? null : cursor.getLong(2);
                        c.dataFimOffsec=cursor.isNull(3) ? null : cursor.getInt(3);
                        c.idWifiRegistro=cursor.isNull(4) ? null : cursor.getLong(4);
                        cacheWifi = new Cache(idUsuario, c);
                        return c;
                    } while (cursor.moveToNext());

                }

                return null;
            } finally{
                if(cursor != null ){
                    cursor.close();
                }
            }
        }

    } // classe: fim
    

    