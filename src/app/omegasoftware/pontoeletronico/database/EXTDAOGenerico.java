package app.omegasoftware.pontoeletronico.database;

import android.content.Context;


public class EXTDAOGenerico extends Table{
	
	public EXTDAOGenerico(Database db, String pNome) {
		super(pNome, db);
		
	}


	@Override
	public void populate() {
	}	
	@Override
	public Table factory() {
		
		return new EXTDAOGenerico(this.getDatabase(), this.getName());
	}


	@Override
	public void onSynchronizing(Context pContext, String pNewId, String pOldId) {
		
		
	}


	@Override
	public void onSynchronized(Context pContext, String pNewId, String pOldId) {
		
		
	}
	


}
