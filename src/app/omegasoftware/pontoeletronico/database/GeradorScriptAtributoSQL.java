package app.omegasoftware.pontoeletronico.database;

import android.content.Context;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;





public class GeradorScriptAtributoSQL {
	
	public static String TAG = "GeradorScriptSQL";
	Context mContext;
	Database db=null;
	public GeradorScriptAtributoSQL(Context pContext, Database db){
		mContext = pContext;
		this.db = db;
	}
	
	
	
	public static ScriptInsertAtributo getScriptAtributoInsert(Table pTable, Attribute pObjAtributoHomologacao){
		ScriptInsertAtributo objScript = new ScriptInsertAtributo();
        String vScriptInsert = "";
        //LastName 
        if(pObjAtributoHomologacao.getName() != null){
            vScriptInsert += pObjAtributoHomologacao.getName() + " ";
        }
        //varchar
        if(pObjAtributoHomologacao.getSQLType() != null){
            vScriptInsert += pObjAtributoHomologacao.getStrSQLDeclaration();
                    
        }
        if(pObjAtributoHomologacao.isAutoIncrement()){
            vScriptInsert += " AUTO_INCREMENT ";
        }
        
        //x varchar(255) NOT NULL
        if(pObjAtributoHomologacao.notNull()){
            vScriptInsert += " NOT NULL ";
        }
        //x varchar(255) NOT NULL DEFAULT 'X'
        if( pObjAtributoHomologacao.hasDefault() ) {
            vScriptInsert += " DEFAULT '" + pObjAtributoHomologacao.getStrDefault() + "' ";
        }
        
        
        
        objScript.mScriptInsert = vScriptInsert;
        
        
        if(pObjAtributoHomologacao.isPrimaryKey()){
            objScript.mIsPrimaryKey = true;
        }
        
        if(pObjAtributoHomologacao.isFK() ){
        	
        	objScript.mScriptKey = "KEY `key_" + HelperString.generateRandomString(7) + "` (`" + pObjAtributoHomologacao.getName() + "`) ";
            
            objScript.mScriptFK += "CONSTRAINT `fk_" + HelperString.generateRandomString(7) + "` FOREIGN KEY (`" + pObjAtributoHomologacao.getName() + "`)"; 
            objScript.mScriptFK += " REFERENCES `" + pObjAtributoHomologacao.getStrTableFK() + "` (`" +pObjAtributoHomologacao.getStrAttributeFK() + "`)";
            objScript.mScriptFK += " ON DELETE " +Attribute.getStrTypeRelationFk(pObjAtributoHomologacao.getOnRemove()) +
            		" ON UPDATE " + Attribute.getStrTypeRelationFk(pObjAtributoHomologacao.getOnUpdate()) +" ";
        }
        
        
        return objScript;
    }
    
	
}
