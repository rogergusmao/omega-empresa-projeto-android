package app.omegasoftware.pontoeletronico.database;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;

public class GeradorScriptBancoSQL {
	
	public static String TAG = "GeradorScriptBancoSQL";
	Context mContext;
	Database db=null;
	public GeradorScriptBancoSQL(Context pContext, Database db){
		mContext = pContext;
		this.db = db;
	}
	
	public class ConfiguracaoScriptTabela{
		boolean mDropTable = false;
		boolean mGravarTupla = false;
		
	}
	
	public ConfiguracaoScriptTabela constroi(
			boolean pDropTable,
			boolean pPopularTabela ){
		
		ConfiguracaoScriptTabela vConfiguracao = new ConfiguracaoScriptTabela();
		vConfiguracao.mDropTable = pDropTable;
		vConfiguracao.mGravarTupla = pPopularTabela;
		return vConfiguracao;
	}
	
	public static String getScriptSetForeignKeyChecks(){
		return "\n\tSET FOREIGN_KEY_CHECKS=0;\n";
	}
	
	public String getScriptEstrutura(String pPath, String pNomeArquivo, ConfiguracaoScriptTabela pConfiguracao){
		String[] vVetorNomeTabela = HelperDatabase.getListaNomeTabela(db);
		GeradorScriptTabelaSQL vGerTab = new GeradorScriptTabelaSQL(mContext, db);
		
		
		File vFile = new File(pPath, pNomeArquivo);
		try {
			if(vFile.exists())vFile.delete();
			FileWriter fw = new FileWriter(vFile, false);
			
			
			fw.append(getScriptSetForeignKeyChecks());
			fw.flush();
			for(int i = 0; i < vVetorNomeTabela.length; i++){
				if(vVetorNomeTabela[i].compareTo("sqlite_sequence") == 0 ) continue;
				
				String vNomeTabela = vVetorNomeTabela[i];
				if(pConfiguracao.mDropTable)
					fw.append(vGerTab.getScriptDropTable(vNomeTabela));
				
				String scriptCreate= vGerTab.getScriptInsert(vNomeTabela);
				
				fw.append(scriptCreate);
				fw.flush();
				//TODO retirar comentario abaixo
				//if(pConfiguracao.mGravarTupla){
					HelperDatabase.gravaScriptInsertInto(db, vNomeTabela, fw);
//					if(vNomeTabela.compareTo("sistema_registro_sincronizador_android_para_web") == 0){
//						int j = 0;
//						j+=1;
//					}
					//}
			}
			fw.close();
			return pPath + pNomeArquivo;
		} catch (IOException e) {
			SingletonLog.insereErro(
					e, 
					SingletonLog.TIPO.BIBLIOTECA_NUVEM);
		}
		return null;
	}
	
	
}

