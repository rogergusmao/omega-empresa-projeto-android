package app.omegasoftware.pontoeletronico.database;

import java.util.ArrayList;

import android.content.Context;





public class GeradorScriptTabelaSQL {
	
	public static String TAG = "GeradorScriptSQL";
	Context mContext;
	Database db=null;
	public GeradorScriptTabelaSQL(Context pContext, Database db){
		mContext = pContext;
		this.db = db;
	}
	
	public String getScriptDropTable(String pNomeTabela){
		return "\n\tDROP TABLE IF EXISTS `" + pNomeTabela + "`;\n";
	}
	
	public String getScriptInsert(String pNomeTabela){
		String vCabecalhoAtributos = "";
        String vChaves = "";
        String vChaveUnica = "";
        String vChavesExtrangeiras = "";
        String vChavePrimaria = "";
        String vConsulta = "";
        EXTDAOGenerico vTable = HelperDatabase.getObjTabelaEstruturada(db, pNomeTabela);
        ArrayList<String> vVetorNomeAtributo = vTable.getVetorNomeAtributo();
        String vStrAutoIncrement = "";
        
        
        for(int i = 0 ; i < vVetorNomeAtributo.size(); i++ ){
        	String vNomeAtributo = vVetorNomeAtributo.get(i);
        	Attribute vObjAttr = vTable.getAttribute(vNomeAtributo);
        	if(vObjAttr.isAutoIncrement() && vStrAutoIncrement.length() == 0){
        		vStrAutoIncrement = " AUTO_INCREMENT=1 ";
        	}
        	ScriptInsertAtributo objScript = GeradorScriptAtributoSQL.getScriptAtributoInsert(vTable, vObjAttr) ;
        	 if(objScript.mScriptKey != null && objScript.mScriptKey.length() > 0)
                 vChaves += vChaves.length() > 0 ? ",\n\t" + objScript.mScriptKey  :  objScript.mScriptKey + " ";
                 
             if(objScript.mScriptFK != null && objScript.mScriptFK.length() > 0)
                 vChavesExtrangeiras += vChavesExtrangeiras.length() > 0 ? ",\n\t" + objScript.mScriptFK  : "\t" + objScript.mScriptFK + " ";
             
             if(objScript.mScriptInsert != null && objScript.mScriptInsert.length()>0)
                 vCabecalhoAtributos += vCabecalhoAtributos.length() > 0 ? ",\n\t" + objScript.mScriptInsert : "\t" +objScript.mScriptInsert +" ";
                 
             if(objScript.mIsPrimaryKey)
                 vChavePrimaria += (vChavePrimaria.length() > 0) ? ", " + vNomeAtributo + " " : vNomeAtributo + " ";
        }
        ContainerKey vContainerUK = vTable.getContainerUniqueKey();
        
        if(vContainerUK != null){
        	String[] vVetorNomeAtributoUnico = vContainerUK.getVetorAttribute();
        	for (String vNomeAttr: vVetorNomeAtributoUnico) {
        		vChaveUnica += (vChaveUnica.length() > 0) ? ", `" + vNomeAttr + "`" : "\t`" + vNomeAttr + "`";	
			}
        }

        
        
        vConsulta = "\nCREATE TABLE " + vTable.getName() + " (\n" + vCabecalhoAtributos + " " ;
        if(vChavePrimaria.length() > 0)
            vConsulta += ",\n\tPRIMARY KEY(" + vChavePrimaria + ")";
        
        if(vChaves.length() > 0)
            vConsulta += ",\n\t " +vChaves + " ";
            
        if(vChavesExtrangeiras.length() > 0)
            vConsulta += ",\n" + vChavesExtrangeiras + " ";
        
        if(vChaveUnica.length() > 0)
            vConsulta += ",\n\tUNIQUE KEY `" + vTable.getName()+ "` (" +vChaveUnica + ") USING BTREE,";
        
        
        vConsulta += "\n) ENGINE=InnoDB " + vStrAutoIncrement + " DEFAULT CHARSET=latin1;\n";
        return vConsulta;
	}
	
	
}

