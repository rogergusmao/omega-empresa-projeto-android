package app.omegasoftware.pontoeletronico.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.database.Attribute.TYPE_RELATION_FK;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.file.HelperFile;
import app.omegasoftware.pontoeletronico.file.HelperZip;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.primitivetype.HelperBoolean;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;





public class HelperDatabase {
	
	public static String TAG = "HelperDatabase";

	public static String[] getListaNomeTabela(Database db){
		return db.getResultSetComoVetorDoUmUnicoObjeto("SELECT "+
	"M.name name, "+
	"M.tbl_name tbl_name, "+
	"M.rootpage rootpage, "+
	"M.sql sql "+
"FROM sqlite_master M "+
"	JOIN sqlite_master N ON M.tbl_name = N.name "+
"		AND N.type = 'table' "+
"WHERE "+
	"( "+
	"	(M.type = 'table') "+
	") "+
"ORDER BY "+
"	M.tbl_name, "+
"	M.name", new String[]{}); 
		
	}
	
	public static void gravaScriptInsertInto(Database db, String pNomeTabela, FileWriter pFw){
		Cursor cursor =  null;
		try{

			

			cursor = db.rawQuery("SELECT * FROM " + pNomeTabela, null);
			String[] vVetorNome = cursor.getColumnNames();
			if (cursor.moveToFirst()) {

				do {
					StringBuilder vBuider = new StringBuilder();
		
						
					vBuider.append("\tINSERT INTO `" + pNomeTabela + "` VALUES (");
					for (int i = 0 ; i < vVetorNome.length; i++) {
						if(i == 0){
							if(cursor.isNull(i))
								vBuider.append("null");
							else{
								vBuider.append("'");
								String vNovoValor = cursor.getString(i).replace("'", "\\'");
								vBuider.append(vNovoValor);
								vBuider.append("'");
							}
						} else{
							if(cursor.isNull(i))
								vBuider.append(", null");
							else{
								vBuider.append(", '");
								String vNovoValor = cursor.getString(i).replace("'", "\\'");
								vBuider.append(vNovoValor);
								vBuider.append("'");
							}
						}
					}
					vBuider.append(");\n");
					pFw.write(vBuider.toString());
					pFw.flush();
				} while (cursor.moveToNext());
			}

			
		}
		catch(Exception ex){
			SingletonLog.insereErro(
					ex, 
					SingletonLog.TIPO.BANCO);	
		} finally{
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();

			} 
		}
		
	}
	
	public static ResultSet getResultSetChaveExtrangeira(Database db, String pNomeTabela){
//		+----+-----+------------+-------------------+----+-----------+-----------+-------+
//		| id | seq | table      | from              | to | on_update | on_delete | match |
//		+----+-----+------------+-------------------+----+-----------+-----------+-------+
//		| 0  | 0   | uf         | uf_id_INT         | id | CASCADE   | CASCADE   | NONE  |
//		| 1  | 0   | corporacao | corporacao_id_INT | id | CASCADE   | CASCADE   | NONE  |
//		+----+-----+------------+-------------------+----+-----------+-----------+-------+
		return db.getResultSet(
				"pragma foreign_key_list('" + pNomeTabela +  "')", 
				null); 
	}
//	+-----+-----------------------------------+--------+
//	| seq | name                              | unique |
//	+-----+-----------------------------------+--------+
//	| 0   | corporacao_corporacao_nome_UNIQUE | 1      |
//	+-----+-----------------------------------+--------+
	public static ResultSet getResultSetIndex(Database db, String pNomeTabela){
		return db.getResultSet(
				"pragma index_list('" + pNomeTabela +  "')", 
				null); 
	}
	
	public static EXTDAOGenerico getObjTabelaEstruturada(Database db, String pNomeTabela){
//		 if(pNomeTabela.compareTo("empresa") == 0){
//			 int u = 0;
//			 u += 1;
//		 }
		String vNomeColunaAutoIncrement = getNomeColunaAutoIncrementoDaTabela(db, pNomeTabela);
//		(cid, name, type, notnull, dft_value, pk[primary_key])
		
		ResultSet rsColunaTabela = getResultSetColunasTabela(db, pNomeTabela);
		if(rsColunaTabela == null) return null;
		ResultSet rsChaveExtrangeira = getResultSetChaveExtrangeira(db, pNomeTabela);
		ResultSet rsChaveUnica = getResultSetIndex(db, pNomeTabela);
		
		Integer iName = rsColunaTabela.getIndexDaColuna("name");
		String [] vVetorEscopo = new String[rsColunaTabela.getTotalTupla()]; 
		for (int i = 0 ; i < rsColunaTabela.getTotalTupla(); i++) {
			
			vVetorEscopo[i] = rsColunaTabela.getTupla(i)[iName];
		}
		
		EXTDAOGenerico vTable = new EXTDAOGenerico(db, pNomeTabela);
		if(vVetorEscopo != null){
			
			for(int i = 0 ; i < vVetorEscopo.length; i++){
				
				String vNomeAtributo = vVetorEscopo[i];
//				if(vNomeAtributo.compareTo("cidade_id_INT") == 0 ){
//					int lk = 0 ;
//					lk += 1;
//				}
				String vTabelaFK= null;
				String vAtributoFK= null;
				TYPE_RELATION_FK vTypeOnUpdate= null;
				TYPE_RELATION_FK vTypeOnDelete= null;
				boolean vIsAutoIncrement = false;
				try{
					if(vNomeColunaAutoIncrement != null && vNomeAtributo.toLowerCase().compareTo(vNomeColunaAutoIncrement) == 0)
						vIsAutoIncrement = true;
					if(rsChaveExtrangeira != null){
						Integer iTable = rsChaveExtrangeira.getIndexDaColuna("table");
						Integer iFrom = rsChaveExtrangeira.getIndexDaColuna("from");
						Integer iTo= rsChaveExtrangeira.getIndexDaColuna("to");
						Integer iOnUpdate= rsChaveExtrangeira.getIndexDaColuna("on_update");
						Integer iOnDelete= rsChaveExtrangeira.getIndexDaColuna("on_delete");
						
						
						
						if(iFrom != null){
							for(int j = 0; j < rsChaveExtrangeira.getTotalTupla(); j++){
								String[] vTuplaCE = rsChaveExtrangeira.getTupla(j);
								String from = vTuplaCE[iFrom];
								if(vNomeAtributo.compareTo(from) == 0){
									vTabelaFK = vTuplaCE[iTable];
									vAtributoFK = vTuplaCE[iTo];
									vTypeOnUpdate =  Attribute.getTypeRelationFk(vTuplaCE[iOnUpdate]);
									vTypeOnDelete= Attribute.getTypeRelationFk(vTuplaCE[iOnDelete]);
									break;
								}
							}
						}
					}
					if(rsColunaTabela != null){
						
						Integer iType = rsColunaTabela.getIndexDaColuna("type");
						Integer iNotnull = rsColunaTabela.getIndexDaColuna("notnull");
						Integer iDftlValue = rsColunaTabela.getIndexDaColuna("dflt_value");
						Integer iPk = rsColunaTabela.getIndexDaColuna("pk");
						
						boolean vNotNull = false;
						String vStrDefault =  null;
						boolean vPrimaryKey = false;
						String vType = null;
						if(rsColunaTabela != null)
						for(int j = 0; j < rsColunaTabela.getTotalTupla(); j++){
							
							String iteratorNomeAtributo = rsColunaTabela.getTupla(j)[iName];
							if(vNomeAtributo.compareTo(iteratorNomeAtributo) == 0){
								String[] vTuplaCT= rsColunaTabela.getTupla(j);
								vPrimaryKey= HelperBoolean.valueOfStringSQL(vTuplaCT[iPk]) ;
								
								vStrDefault= HelperString.checkIfIsNull(vTuplaCT[iDftlValue]);
								if(vStrDefault != null && vStrDefault.compareTo("''") == 0)
									vStrDefault = null;
								vNotNull = HelperBoolean.valueOfStringSQL(vTuplaCT[iNotnull]);
								vType  = vTuplaCT[iType];
								break;
							}
						}
						ContainerTipoSqlite vContainerTS = Attribute.getContainerTipoSqlite(db.getContext(), vType);
						
						if(rsChaveUnica != null){
							iName= rsChaveUnica.getIndexDaColuna("name");
							int iUnique = rsChaveUnica.getIndexDaColuna("unique");
							@SuppressWarnings("unused")
							boolean isUnique = false;
							for(int j = 0; j < rsChaveUnica.getTotalTupla(); j++){
								
								String iteratorNomeAtributo = rsChaveUnica.getTupla(j)[iName];
								if(vNomeAtributo.compareTo(iteratorNomeAtributo) == 0){
									String[] vTuplaCU= rsChaveUnica.getTupla(j);
									isUnique = HelperBoolean.valueOfStringSQL(vTuplaCU[iUnique]) ;
									break;
								}
							}
						}
						
						if(vTabelaFK == null)
						vTable.addAttribute(new Attribute(
								vNomeAtributo, 
								vNomeAtributo, 
								false, 
								vContainerTS.mSqliteType, 
								! vNotNull, 
								vStrDefault, 
								vPrimaryKey, 
								null,
								vContainerTS.mTamanho, 
								vIsAutoIncrement));
						else{
							
							vTable.addAttribute(
								new Attribute(
									db.getContext(),
									vNomeAtributo, 
									vTabelaFK, 
									vAtributoFK, 
									vNomeAtributo, 
									false, 
									vContainerTS.mSqliteType, 
									!vNotNull, 
									vStrDefault, 
									vPrimaryKey, 
									null, 
									vContainerTS.mTamanho, 
									vTypeOnUpdate, 
									vTypeOnDelete,
									null
								)
							);
						}
					}
				} catch(Exception ex){
					SingletonLog.insereErro(
							ex, 
							SingletonLog.TIPO.BIBLIOTECA_NUVEM);
				}
			}
			
			
		}
		
		return vTable.getTotalAtributo() == 0 ? null : vTable;
	}
	
	public static String getNomeColunaAutoIncrementoDaTabela(Database db, String pNomeTabela){
		Cursor cursor = null;
		try{
			

			cursor = db.rawQuery(
					"SELECT ROWID  FROM '" +  pNomeTabela + "' LIMIT 0,1", 
					new String[]{});

			String[] vVetorNome = cursor.getColumnNames();
			if(vVetorNome != null && vVetorNome.length > 0 )
				return vVetorNome[0];
			else return null;
						
			
		}catch(Exception ex){
			SingletonLog.insereErro(
					ex, 
					SingletonLog.TIPO.BANCO);
			return null;
		}finally{
			if (cursor != null && !cursor.isClosed()) 
				cursor.close();
		}		
	}
//	+-----+------------------+--------------+---------+------------+----+
//	| cid | name             | type         | notnull | dflt_value | pk |
//	+-----+------------------+--------------+---------+------------+----+
//	| 0   | id               | INTEGER      | 1       | NULL       | 1  |
//	| 1   | nome             | varchar(100) | 1       | NULL       | 0  |
//	| 2   | nome_normalizado | varchar(100) | 1       | NULL       | 0  |
//	| 3   | usuario_dropbox  | varchar(255) | 0       | NULL       | 0  |
//	| 4   | senha_dropbox    | varchar(255) | 0       | NULL       | 0  |
//	+-----+------------------+--------------+---------+------------+----+
	public static ResultSet getResultSetColunasTabela(Database db, String pNomeTabela){
		//(cid, name, type, notnull, dft_value, pk[primary_key])
		return db.getResultSet( "pragma table_info(" + pNomeTabela +")" ,  null);
	}
	
	public static ResultSet getEstruturaTabela(Database db, String pNomeTabela){
		return db.getResultSet ( "SELECT " +
	"M.type type, " +
	"M.name name, " +
	"M.tbl_name tbl_name, " +
	"M.rootpage rootpage, " +
	"M.sql sql " +
"FROM " +
	"sqlite_master M " +
"JOIN sqlite_master N ON M.tbl_name = N.name " +
"AND N.type = 'table' " +
"WHERE " +
"	( " +
"		(M.type = 'table') " +
"		OR(M.type = 'index') " +
"		OR(M.type = 'trigger') " +
"	) " +
"AND( " +
"	M.tbl_name = '" + pNomeTabela+  "' " +
") " +
"ORDER BY " +
"	M.tbl_name, " +
"	M.name", new String[]{});
		
   }
	
	public static ArrayList<Long> convertCursorToArrayListId(Cursor pCursor, String[] pVetorChave )
	{
		ArrayList<Long> vArrayList = new ArrayList<Long>();
		
		if (pCursor.moveToFirst()) {

			do {
				
				for (int index = 0; index < pVetorChave.length; index++) {				 
					try
					{
						Long vKeyLong = Long.parseLong(pCursor.getString(index));
						vArrayList.add(vKeyLong);
					}
					catch (Exception ex) {
						SingletonLog.insereErro(
								ex, 
								SingletonLog.TIPO.BANCO);
					}
					
				}   
			} while (pCursor.moveToNext());
		}

		if (pCursor != null && !pCursor.isClosed()) {	    	  
			pCursor.close();
		}
		return vArrayList;

	}

	
	public static String[] getListaDadoBanco(Database db){
		return db.getResultSetComoVetorDoUmUnicoObjeto("SELECT "+
	"M.type type, "+
	"M.name name, "+
	"M.tbl_name tbl_name, "+
	"M.rootpage rootpage, "+
	"M.sql sql "+
"FROM "+
	"sqlite_master M "+
"JOIN sqlite_master N ON M.tbl_name = N.name "+
"AND N.type = 'table' "+
"WHERE "+
	"( "+
	"	(M.type = 'table') "+
	"	OR(M.type = 'index') "+
	"	OR(M.type = 'trigger') "+
	") "+
"ORDER BY "+
"	M.tbl_name, "+
"	M.name", new String[]{}); 
		
	}
	//CRIADOR DE BANCO DE DADOS

	public static void saveDatabaseNoArquivoTemporario(Context context, String prefixoNomeArquivo, String databaseName){
		
		OmegaFileConfiguration conf = new OmegaFileConfiguration();
		
		String path = conf.getPath(OmegaFileConfiguration.TIPO.SINCRONIZACAO);
		
		path += prefixoNomeArquivo + ".db";
		try {
			saveDatabase(context, path, databaseName );
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}
	

	public static void saveDatabaseNoArquivo(Context context, String pathOut, String prefixoNomeArquivo, String databaseName){
		try {
			Database db = new DatabasePontoEletronico(context);
			String path = pathOut + prefixoNomeArquivo;
		
			saveDatabase(context, path, databaseName );
			db.close();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}
	
	
	

	public static boolean createDatabaseFromZipFile(
			Context context, 
			File pZipFile, 
			String databasePath, 
			String databaseName,
			int databaseVersion) throws IOException {
		try{

			context.deleteDatabase(databaseName);

			OpenHelper.destroy();
			OpenHelper oh = OpenHelper.getInstance(databaseName, databaseVersion, databasePath, context, true);

			SQLiteDatabase readDb = oh.getReadableDatabase();
			HelperDatabase.log("Banco 1. ", readDb);
			readDb.close();
			OpenHelper.close();

			File databaseOut = context.getDatabasePath(databaseName);

			FileOutputStream fou = null;
			try{
				fou = new FileOutputStream(databaseOut.getAbsolutePath());
				HelperZip.unzipToFile(pZipFile,  fou);	
					
			}finally{
				if(fou != null)fou.close();	
			}
			
			oh = OpenHelper.getInstance(databaseName, databaseVersion, databasePath, context, true);
			
			SQLiteDatabase sql= oh.getReadableDatabase();
			
			HelperDatabase.log("Banco 2. ", sql);
			
			sql.close();
			OpenHelper.close();
			
			return true;
		} catch(Exception ex){
			SingletonLog.insereErro(
					ex, 
					SingletonLog.TIPO.BANCO);
			return false;
		}
	}

	
	public static void log(String identificador, SQLiteDatabase sql){
		String token = "Identificador: " + identificador + ". Versao: " + String.valueOf(sql.getVersion());
		token += ". isDbLockedByCurrentThread: " + String.valueOf( sql.isDbLockedByCurrentThread());
		token +=  ". isDbLockedByOtherThreads: " + String.valueOf(sql.isDbLockedByOtherThreads());
		token +=  ". inTransaction: " + String.valueOf(sql.inTransaction());
//		token +=  ". yieldIfContended: " + String.valueOf(sql.yieldIfContended());
//		token +=  ". yieldIfContended: " + String.valueOf(sql.yieldIfContendedSafely());
		token +=  ". isReadOnly: "+ sql.isReadOnly();
		token +=  ". isOpen: "+ sql.isOpen();
		token +=  ". getPageSize: "+ String.valueOf( sql.getPageSize());
		token +=  ". getMaximumSize: "+ String.valueOf( sql.getMaximumSize());
		token +=  ". getPath: "+ String.valueOf( sql.getPath());
		
		
		Log.i(TAG, token);
	}
	
	public static boolean bkpCreateDatabaseFromZipFileNaoFuncionou(
			Context context, 
			File pZipFile, 
			String databasePath, 
			String databaseName,
			int databaseVersion) throws IOException {
		try{
//			if(checkDataBase(context, databasePath)){
				context.deleteDatabase(databaseName);
//			}
			
			OmegaFileConfiguration ofc = new OmegaFileConfiguration();
			String path = ofc.getPath(OmegaFileConfiguration.TIPO.SINCRONIZACAO);
			String pathArquivoDb = path + databaseName + "_temp.db";
			FileOutputStream fou = new FileOutputStream(pathArquivoDb);
			HelperZip.unzipToFile(pZipFile,  fou);
			fou.close();
			pZipFile.delete();
			
			
			OpenHelperArquivo oha = OpenHelperArquivo.getInstance(context, databaseVersion, pathArquivoDb, databasePath, databaseName);
			
			oha.createDatabase();
			oha.close();
			return true;
		} catch(Exception ex){
			SingletonLog.insereErro(
					ex, 
					SingletonLog.TIPO.BANCO);
			return false;
		}
	}

	public static boolean saveDatabase(Context context, String pathFileOut, String databaseName) throws IOException {
		
		File databaseIn = context.getDatabasePath(databaseName);
		
	
        FileChannel src = new FileInputStream(databaseIn).getChannel();
        FileChannel dst = new FileOutputStream(pathFileOut).getChannel();
        dst.transferFrom(src, 0, src.size());
        src.close();
        dst.close();
    
		
//        InputStream in = new FileInputStream(databaseIn);
//        OutputStream out = new FileOutputStream(new File( pathFileOut));
//
//        // Transfer bytes from in to out
//        byte[] buf = new byte[1024];
//        int len;
//        while ((len = in.read(buf)) > 0) {
//            out.write(buf, 0, len);
//        }
//        in.close();
//        out.close();
        context.openOrCreateDatabase(databaseName, Context.MODE_PRIVATE, null);
		return true;
	}
	
	
	public static boolean copyDataBase(Context context, String pVetorFileDatabasePart[], String databaseName) throws IOException {
		// TODO retirar comentario abaxo
		// String vetorArquivoBanco[] =
		// FileSystemHelper.getListOfFileInDirectoryAssets("database",
		// context);

		

		// Path to the just created empty db
		File databaseOut = context.getDatabasePath(databaseName);
		String outFileName = databaseOut.getAbsolutePath();
		if (pVetorFileDatabasePart.length > 0)
			return HelperFile.mergeListFileInAssetsInFile(
					pVetorFileDatabasePart, outFileName, context);
		else
			return true;

	}
	
	

	
	/**
	 * Copies your database from your local assets-folder to the just
	 * created empty database in the system folder, from where it can be
	 * accessed and handled. This is done by transfering bytestream.
	 * */
	public static boolean copyDataBase(Context context, String pPathDatabase, String databaseName) throws IOException {
		// Path to the just created empty db
		File databaseOut = context.getDatabasePath(databaseName);
		
		File vDatabaseFile = new File(pPathDatabase);
		vDatabaseFile.renameTo(databaseOut);
		return true;
	}

	/**
	 * Creates a empty database on the system and rewrites it with your own
	 * database.
	 * */
	public static boolean createDatabase(String databasePath) throws IOException {
		try{
			SQLiteDatabase db = SQLiteDatabase.openDatabase(databasePath, null,
					SQLiteDatabase.CREATE_IF_NECESSARY);
			
			return true;
		} catch(Exception ex){
			return false;
		}
		

	}

	/**
	 * Check if the database already exist to avoid re-copying the file each
	 * time you open the application.
	 * 
	 * @return true if it exists, false if it doesn't
	 */
	public static boolean checkDataBase(Context c,String databasePath) {
		boolean validade = true;
		SQLiteDatabase checkDB = null;

		try {
			checkDB = c.openOrCreateDatabase(
					databasePath, 
					SQLiteDatabase.OPEN_READONLY,
					null);

		} catch (SQLiteException e) {
			validade = false;
			
		}

		if (checkDB != null) {
			checkDB.close();
		}

		return validade;
	}
	
}
