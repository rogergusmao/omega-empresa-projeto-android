package app.omegasoftware.pontoeletronico.database;


public class OmegaDatabaseException extends Exception {
    /**
	 * 
	 */
	public enum CODE{
		BEGIN_TRANSACTION_TIMEOUT,
		SEQUENCE_UPDATE_FAIL,
		CONSTRUCTOR_DATABASE
		
	}
	private static final long serialVersionUID = 1L;
	
	CODE code;
	
	public OmegaDatabaseException(CODE code, String message) {
        super(message);
        this.code = code;
    }
	
	public OmegaDatabaseException(CODE code, String message, Exception ex) {
        super(message, ex);
        this.code = code;
    }
	
	public CODE getCode(){return code;}
}
