package app.omegasoftware.pontoeletronico.database;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import app.omegasoftware.pontoeletronico.TabletActivities.ResetarActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.thread.HelperThread;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.pontoeletronico.PrincipalActivity;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

class OpenHelper {
	// IMPORTANTISSIMO
	// https://www.sqlite.org/rescode.html#readonly_rollback

	private DataOpenHelper helperReadable;
	private DataOpenHelper helperWritable;
	private String databasePath;
	private static OpenHelper helper;
	static Lock lockCRUD = new ReentrantLock(true);

	static OmegaLog log = null;

	Context context = null;

	private OpenHelper(String databaseName, int databaseVersion, String databasePath, Context context) {
		this.context=context;
		
		this.databasePath = databasePath;
		helperReadable = new DataOpenHelper(databaseName, databaseVersion, databasePath, context, false, log, this);
		helperWritable = new DataOpenHelper(databaseName, databaseVersion, databasePath, context, true, log, this);
		
		initLog(this.context,databaseName);
	}
	public void log(String msg){
		if(log != null){
			log.escreveLog(msg);
		}
	}
	
	public static void close() {
		if(helper != null){
			if (helper.helperReadable != null) {
				helper.helperReadable.close();
				helper.helperReadable = null;
			}
			if (helper.helperWritable != null) {
				helper.helperWritable.close();
				helper.helperWritable = null;
			}	
			helper = null;
		}
	}

	public SQLiteDatabase getReadableDatabase() {
		final SQLiteDatabase readableDatabase = helperReadable.getReadableDatabase();
		return readableDatabase;
	}

	public SQLiteDatabase getWritableDatabase() {
		final SQLiteDatabase readableDatabase = helperWritable.getWritableDatabase();
		return readableDatabase;
	}

	public static OpenHelper getInstance(String databaseName, int databaseVersion, String databasePath,
			Context context) throws Exception {
		return getInstance(databaseName, databaseVersion, databasePath, context, false);
	}

	private static void initLog(Context c, String databaseName) {
		
		String data = HelperDate.getNomeArquivoGeradoACadaXMinutos();

		String nomeArquivo = databaseName + "_" + data;
		nomeArquivo = nomeArquivo.replace('.', '_');
		nomeArquivo += ".log";
		if (log != null && !log.getNomeArquivo().equals(nomeArquivo)) {
			log.close();
			log = null;
		}
		if (log == null)
			log = new OmegaLog(nomeArquivo, OmegaFileConfiguration.TIPO.LOG,OmegaConfiguration.DEBUGGING_DATABASE_SQLITE, c);
	}

	
    private static Object lock2 = new Object();
	public static OpenHelper getInstance(String databaseName, int databaseVersion, String databasePath, Context context,
			boolean force) throws Exception {

		if (force || helper == null) {
		   synchronized(lock2) {
			   if (force || helper == null){
				   if(helper!=null)
						close();	  

					initLog(context, databaseName);
					Random r = new Random(1000);
					final int LIMITE_TENTATIVA = 3;
					for(int i = 0 ; i < LIMITE_TENTATIVA; i++){
						try{
							helper = new OpenHelper(databaseName, databaseVersion, databasePath, context);
							return helper;
						}catch(Exception ex){
							if(i == LIMITE_TENTATIVA - 1){
								SingletonLog.insereErro(ex, TIPO.BANCO);
								throw ex;
							}
							else{
								SingletonLog.insereWarning(ex, TIPO.BANCO);
								HelperThread.sleep(r.nextInt(1500));
							}
						}
					}
			   }
	        }
			
			
			
		}
		return helper;
	}

	public static void destroy() {
		if(helper == null) return;
		try {
			if (helper.helperReadable != null) {
				helper.helperReadable.close();
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.BANCO);
			if (log != null)
				log.escreveLog("destroy 1", ex);
		} finally {
			helper.helperReadable = null;
		}
		try {
			if (helper.helperWritable != null) {
				helper.helperWritable.close();
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.BANCO);
			if (log != null)
				log.escreveLog("destroy 2", ex);
		} finally {
			helper.helperWritable = null;
		}
		try {
			if (OpenHelper.log != null) {
				OpenHelper.log.close();
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.BANCO);
			if (log != null)
				log.escreveLog("destroy 3", ex);
		} finally {
			OpenHelper.log = null;
		}
		helper = null;
	}

	

	public static void closeLog() {
		if (log != null) {
			log.close();
		}
	}

	public Transaction beginTransaction() {
		try {
			
			return beginTransaction(null, null);
			
		} catch (SQLiteException e) {
			SingletonLog.insereErro(e, TIPO.BANCO);
			if (log != null)
				log.escreveLog("beginTransaction", e);
			return null;
		}catch (InterruptedException e) {
			SingletonLog.insereErro(e, TIPO.BANCO);
			if (log != null)
				log.escreveLog("beginTransaction", e);
			return null;
		}
	}

	public Transaction beginTransaction(Long time, TimeUnit tu) throws InterruptedException {
		boolean lock = false;
		if (time != null && tu != null)
			lock = lockCRUD.tryLock(time, tu);
		else
			lock = lockCRUD.tryLock();
		if (lock) {
			boolean validade = true;
			try {
				if (log != null)
					log.escreveLog("beginTransaction - transauuo criada");
				Transaction transaction = new Transaction(helperWritable.getWritableDatabase());

				setPragmaOn(transaction);
				return transaction;

			} catch (SQLiteException ex) {
				SingletonLog.insereErro(ex, TIPO.BANCO);
				if (log != null)
					log.escreveLog("beginTransaction", ex);
				validade = false;
				throw ex;
			} finally {
				if (!validade)
					lockCRUD.unlock();
			}
		} else {
			if (log != null)
				log.escreveLog("beginTransaction - [oi3498] ju existe uma transauuo em aberto");
			return null;
		}
	}

	public void endTransaction(Transaction transaction, boolean successfull) {

		if (transaction != null) {
			try {
				if (log != null)
					log.escreveLog("endTransaction: " + successfull);
				transaction.endTransaction(successfull);

			} catch (SQLiteException ex) {
				SingletonLog.insereErro(ex, TIPO.BANCO);
				if (log != null)
					log.escreveLog("endTransaction", ex);
				throw ex;
			} finally {
				transaction = null;
				lockCRUD.unlock();
			}
		} else {
			if (log != null)
				log.escreveLog("endTransaction: nuo existe trasauuo em aberto");

		}
	}

	public int delete(Transaction transaction, String tabela, String where, String[] parametros)
			throws SQLiteException {

		if (transaction != null) {
			try {
				if (log != null)
					log.escreveLog("transaction::delete. Antes. Tabela: " + tabela + ". Where " + where + ". Parametros: "
							+ HelperString.arrayToString(parametros));

				int totalRemovido = transaction.writableDatabase.delete(tabela, where, parametros);
				if (log != null)
					log.escreveLog("transaction::delete. Depois, total removido: "+totalRemovido+". Tabela: " + tabela + ". Where " + where + ". Parametros: "
							+ HelperString.arrayToString(parametros));
				return totalRemovido;
			} catch (SQLiteException ex) {
				if (log != null)
					log.escreveLog("transaction::delete", ex);
				throw ex;
			}
		}
		synchronized (lockCRUD) {
			try {
				if (log != null)
					log.escreveLog("delete. Antes. Tabela: " + tabela + ". Where " + where + ". Parametros: "
							+ HelperString.arrayToString(parametros));
				final SQLiteDatabase writableDatabase = getWritableDatabase(transaction);

				int totalRemovido = writableDatabase.delete(tabela, where, parametros);
				if (log != null)
					log.escreveLog("delete. Depois, total removido: "+totalRemovido+".Tabela: " + tabela + ". Where " + where + ". Parametros: "
							+ HelperString.arrayToString(parametros));
				return totalRemovido;
			} catch (SQLiteException ex) {
				if (log != null)
					log.escreveLog("delete", ex);
				throw ex;
			}
		}
	}

	public void setPragmaOn(Transaction transaction) throws SQLiteException {
		try {
			if (log != null)
				log.escreveLog("transaction::setPragmaOn");
			Cursor c = rawQuery(transaction, "PRAGMA foreign_keys=ON;", null, null);

			c.close();
		} catch (SQLiteException ex) {
			if (log != null)
				log.escreveLog("transaction::setPragmaOn", ex);
			throw ex;
		}
	}

	public void setPragmaOn(SQLiteDatabase writaDatabase) {
		Cursor c = null;
		try {
			if (log != null)
				log.escreveLog("setPragmaOn");
			c = rawQuery(null, "PRAGMA foreign_keys=ON;", null, writaDatabase);
		} catch (Exception ex) {
			if (log != null)
				log.escreveLog("setPragmaOn", ex);

		} finally {
			if (c != null)
				c.close();
		}
	}

	public void setPragmaOff(Transaction transaction) {
		Cursor c = null;
		try {
			if (log != null)
				log.escreveLog("transaction::setPragmaOff");
			c = rawQuery(transaction, "PRAGMA foreign_keys=OFF;", null, null);

		} catch (Exception ex) {
			if (log != null)
				log.escreveLog("transaction::setPragmaOff", ex);

		} finally {
			if (c != null)
				c.close();
		}
	}

	public void setPragmaOff(SQLiteDatabase writaDatabase) {
		Cursor c = null;
		try {
			if (log != null)
				log.escreveLog("setPragmaOff SQLiteDatabase");
			c = rawQuery(null, "PRAGMA foreign_keys=OFF;", null, writaDatabase);
		} catch (Exception ex) {
			if (log != null)
				log.escreveLog("setPragmaOff", ex);
		} finally {
			if (c != null)
				c.close();
		}
	}

	public void execSQLs(Transaction transaction, ArrayList<String> querys) throws SQLiteException {
		if (log != null)
			log.escreveLog("execSQLs");
		if (querys == null || querys.size() == 0)
			return;
		for (String qRemove : querys) {
			execSQL(transaction, qRemove);
		}
	}

	public void execSQL(Transaction transaction, String consulta) throws SQLiteException {
		if (transaction != null) {
			try {
				if (log != null)
					log.escreveLog("transaction::execSQL - " + consulta);
				transaction.writableDatabase.execSQL(consulta);
			} catch (SQLiteException ex) {
				if (log != null)
					log.escreveLog("transaction::execSQL", ex);
				throw ex;
			}
		}
		synchronized (lockCRUD) {
			try {
				if (log != null)
					log.escreveLog("execSQL - " + consulta);
				final SQLiteDatabase writableDatabase = getWritableDatabase(transaction);
				writableDatabase.execSQL(consulta);
			} catch (SQLiteException ex) {
				if (log != null)
					log.escreveLog("execSQL", ex);
				throw ex;
			}
		}
	}

	private long getMaxIdMaisUm(SQLiteDatabase writableDatabase, String tableName, OmegaLog omegaLog) {
		if (log != null)
			log.escreveLog("getMaxIdMaisUm " + tableName);
		String query = "SELECT max(id) FROM  " + tableName;
		Cursor cursor = null;
		try {
			cursor = writableDatabase.rawQuery(query, null);
			long seq = -1;
			if (cursor.moveToFirst()) {
				seq = cursor.getLong(0);
				seq += 1;
			}
			cursor.close();
			return seq;
		} finally {
			if (cursor != null)
				cursor = null;
		}
	}

	public long getMinId(Transaction transaction, String tableName) {
		Cursor cursor = null;
		try {
			final String query = "SELECT min(id) FROM  " + tableName;
			cursor = rawQuery(transaction, query, null);
			long seq = -1;
			if (cursor.moveToFirst()) {
				seq = cursor.getLong(0);
			}
			return seq;
		} finally {
			if (cursor != null)
				cursor.close();
		}
	}
	
	
	public Long getLong(Transaction transaction, String query, String[] parametros ) {
		Cursor cursor = null;
		try {
			
			cursor = rawQuery(transaction, query, parametros);
			
			if (cursor.moveToFirst()) {
				long seq = cursor.getLong(0);
				return seq;
			}
			return null;
		} finally {
			if (cursor != null)
				cursor.close();
		}
	}
	

	public long getMaxId(Transaction transaction, String tableName) {
		Cursor cursor = null;
		try {
			final String query = "SELECT max(id)  FROM  " + tableName;
			cursor = rawQuery(transaction, query, null);
			long seq = -1;
			if (cursor.moveToFirst()) {
				seq = cursor.getLong(0);
			}
			cursor.close();

			return seq;
		} finally {
			if (cursor != null)
				cursor.close();
		}
	}

	private void updateSequence(SQLiteDatabase writableDatabase, String tableName, long seq, OmegaLog omegaLog)
			throws SQLiteException, OmegaDatabaseException {
		try {
			if (log != null)
				log.escreveLog("updateSequence " + tableName + " para " + seq);
			ContentValues cvUpdate = new ContentValues();
			long novoSeq = seq;
			cvUpdate.put("seq", seq);
			int total = writableDatabase.update("SQLITE_SEQUENCE", cvUpdate, "name = ? ", new String[] { tableName });
			if (total > 0) {
				if (log != null) {
					log.escreveLog("ok - updateSequence " + tableName + " para " + seq);
					omegaLog.escreveLog("Atualizado SEQ - id + 1: " + novoSeq);
				}
			} else {
				if (log != null)
					log.escreveLog("falha - updateSequence " + tableName + " para " + seq);
				throw new OmegaDatabaseException(OmegaDatabaseException.CODE.SEQUENCE_UPDATE_FAIL,
						"Falha durante a atualizacao da sequence da tabela " + tableName + " para "
								+ String.valueOf(seq));
			}
		} catch (SQLiteException ex) {
			if (log != null)
				log.escreveLog("updateSequence", ex);
			throw ex;
		}

	}

	private boolean existeId(SQLiteDatabase writableDatabase, String tableName, String id) {
		if (log != null)
			log.escreveLog("existeId - " + tableName + "[" + id + "]");
		final String queryCheckID = "SELECT 1 FROM " + tableName + " WHERE id = ? LIMIT 0,1";
		Cursor cursor = null;
		try {

			cursor = writableDatabase.rawQuery(queryCheckID, new String[] { String.valueOf(id) });

			if (cursor.moveToFirst()) {
				if (log != null)
					log.escreveLog("existeId - true");
				return true;
			}
			if (log != null)
				log.escreveLog("existeId - false");
			return false;
		} finally {
			if (cursor != null)
				cursor.close();
		}
	}

	private long getSequence(SQLiteDatabase writableDatabase, String tableName, OmegaLog omegaLog) {
		if (log != null)
			log.escreveLog("getSequence - " + tableName);
		long seq = -1;
		final String query = "SELECT seq FROM SQLITE_SEQUENCE " + " WHERE name LIKE ?";
		Cursor cursor = null;
		try {
			cursor = writableDatabase.rawQuery(query, new String[] { tableName });

			if (cursor.moveToFirst()) {
				seq = cursor.getInt(0);
				if (log != null) {
					log.escreveLog("getSequence - res:" + seq);
				}
				if(omegaLog!=null)
				omegaLog.escreveLog("SEQ id escolhido = " + seq);
			}
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return seq;
	}

	private ConflictUpdateId procedureUpdateId(SQLiteDatabase writableDatabase, String tableName, String oldId,
			String newId, OmegaLog omegaLog) {
		if (log != null)
			log.escreveLog("procedureUpdateId - " + tableName + ": " + oldId + " => " + newId);
		ConflictUpdateId con = new ConflictUpdateId();

		long seq = -1;
		try {
			boolean newIdValidado = false;
			if (newId == null) {
				if (omegaLog != null)
					omegaLog.escreveLog("NewId nulo => O sistema pega o proximoId autoincrement");
				seq = getSequence(writableDatabase, tableName, omegaLog);
				if (seq > 0)
					seq += 1;
				if (omegaLog != null)
					omegaLog.escreveLog("[NewId] SEQ id escolhido = " + seq);
				boolean getMaxId = false;
				if (seq > 0) {
					if (omegaLog != null)
						omegaLog.escreveLog("[NewId] Existe id " + tableName + "[" + seq + "] ???");
					getMaxId = existeId(writableDatabase, tableName, String.valueOf(seq));
					if (omegaLog != null) {
						if (getMaxId) {
							omegaLog.escreveLog("[NewId] Existe id " + tableName + "[" + seq + "] .");

						} else {
							omegaLog.escreveLog("[NewId] Nuo existe o id " + tableName + "[" + seq + "]!!!");

						}
					}
				} else
					getMaxId = true;

				if (getMaxId) {
					if (omegaLog != null)
						omegaLog.escreveLog("[NewId] Procurando o max id + 1 ");
					seq = getMaxIdMaisUm(writableDatabase, tableName, omegaLog);
					if (omegaLog != null)
						omegaLog.escreveLog("[NewId] MAX id + 1 - Seq escolzchido = " + seq);
				}

				if (seq <= 0) {
					con.conflictedNewId = null;
					con.numberRowsAffected = 0;
					con.successfull = false;
					return con;
				} else {
					newId = String.valueOf(seq);
					con.newIdGenerated = newId;
				}
				newIdValidado = true;
				seq = -1;
			}

			ContentValues cv = new ContentValues();
			cv.put("id", newId);
			int numberRowsAffected = -1;
			if (newIdValidado || !existeId(writableDatabase, tableName, newId)) {

				try {
					if (omegaLog != null)
						omegaLog.escreveLog("Atualizando A id " + tableName + " " + oldId + " => " + newId);
					numberRowsAffected = writableDatabase.update(tableName, cv, "id= ?", new String[] { oldId });
				} catch (Exception ex) {
					// Caso for duplicada
					numberRowsAffected = -1;
					if (omegaLog != null)
						omegaLog.escreveLog("Erro A durante a atualizacao " + tableName + " " + oldId + " => " + newId
								+ ". " + HelperExcecao.getDescricao(ex));
				}
			} else {
				if (omegaLog != null)
					omegaLog.escreveLog("Nem tentou a atualizacao A id " + tableName + " " + oldId + " => " + newId
							+ " pois ju vimos que o id [" + newId + "] estu ocupado");
			}

			boolean houveModificacaoB = false;
			String oldB = null;
			long newB = -1;
			// Se o identificador esta ocupado, entao tenta desocupa-lo
			if (numberRowsAffected <= 0) {
				if (omegaLog != null)
					omegaLog.escreveLog(
							"O id estu ocupado, tentaremos desocupa-lo com a sequence, caso nao tenha sucesso, tentaremos com max id");
				seq = getSequence(writableDatabase, tableName, omegaLog);
				if (seq > 0)
					seq += 1;
				if (omegaLog != null)
					omegaLog.escreveLog("SEQ id escolhido = " + seq);
				boolean getMaxId = false;
				if (seq > 0) {
					if (omegaLog != null)
						omegaLog.escreveLog("Existe id " + tableName + "[" + seq + "] ???");
					getMaxId = existeId(writableDatabase, tableName, String.valueOf(seq));
					if (omegaLog != null) {
						if (getMaxId) {
							omegaLog.escreveLog("Existe id " + tableName + "[" + seq + "] .");
						} else {
							omegaLog.escreveLog("Nuo existe o id " + tableName + "[" + seq + "]!!!");
						}
					}
				} else
					getMaxId = true;

				if (getMaxId) {
					if (omegaLog != null)
						omegaLog.escreveLog("Procurando o max id + 1 ");
					seq = getMaxIdMaisUm(writableDatabase, tableName, omegaLog);
					if (omegaLog != null)
						omegaLog.escreveLog("MAX id + 1 - Seq escolzchido = " + seq);
				}

				if (seq > 0) {

					// Atualiza o valor da tupla ainda nuo sincronizada
					// para o maior numero possivel
					ContentValues cv2 = new ContentValues();
					cv2.put("id", seq);
					try {
						if (omegaLog != null)
							omegaLog.escreveLog(
									"Tentativa de atualizar B1 id " + tableName + " " + newId + " => " + seq);
						numberRowsAffected = writableDatabase.update(tableName, cv2, "id= ?", new String[] { newId });
						if (numberRowsAffected > 0) {
							houveModificacaoB = true;
							oldB = newId;
							newB = seq;
							updateSequence(writableDatabase, tableName, seq, omegaLog);

						} else if (omegaLog != null) {
							omegaLog.escreveLog(
									"Nuo conseguiu atualizar B1 : " + tableName + " " + newId + " => " + seq + ". ");
						}
					} catch (Exception ex) {
						if (omegaLog != null)
							omegaLog.escreveLog("Erro B1 durante a atualizacao " + tableName + " " + newId + " => "
									+ seq + ". " + HelperExcecao.getDescricao(ex));
						numberRowsAffected = -1;
					}

					if (numberRowsAffected <= 0) {
						seq = getMaxIdMaisUm(writableDatabase, tableName, omegaLog);
						if (seq > 0) {

							cv2.clear();
							cv2.put("id", seq);
							if (omegaLog != null)
								omegaLog.escreveLog(
										"Tentativa atualizando B2 id " + tableName + " " + newId + " => " + seq);
							numberRowsAffected = writableDatabase.update(tableName, cv2, "id= ?",
									new String[] { newId });
							if (numberRowsAffected > 0) {
								houveModificacaoB = true;
								oldB = newId;
								newB = seq;
								updateSequence(writableDatabase, tableName, seq, omegaLog);
							} else if (omegaLog != null) {

								omegaLog.escreveLog("Nuo conseguiu atualizar B2 ");
								if (existeId(writableDatabase, tableName, String.valueOf(seq))) {
									omegaLog.escreveLog(
											"Realmente a tentativa de B2 iria falhar, pois o id " + seq + " existia");
								} else {
									omegaLog.escreveLog("De acordo com o SELECT o id " + seq + " nuo existe");
								}
							}

						} else if (omegaLog != null)
							omegaLog.escreveLog("Falha durante consulta Max + 1 para tentativa B2 ");
					}

					// Atualiza o valor do id antigo para o id contido na
					// WEB
					// que estava ocupado ate o momento
					if (numberRowsAffected > 0) {
						try {
							if (omegaLog != null)
								omegaLog.escreveLog("Atualizando C id " + tableName + " " + oldId + " => " + newId);
							numberRowsAffected = writableDatabase.update(tableName, cv, "id= ?",
									new String[] { oldId });
						} catch (Exception ex) {
							numberRowsAffected = -1;
							if (omegaLog != null)
								omegaLog.escreveLog("Erro C durante a atualizacao " + tableName + " " + oldId + " => "
										+ newId + ". " + HelperExcecao.getDescricao(ex));
						}
						if (numberRowsAffected > 0) {

							con.successfull = true;
						} else {
							if (omegaLog != null)
								omegaLog.escreveLog("Nuo conseguiu atualizar C");
						}
					} else {
						if (omegaLog != null) {
							omegaLog.escreveLog("Nuo conseguiu nem tentar atualizar C " + oldId + " => " + newId);

							if (existeId(writableDatabase, tableName, oldId))
								omegaLog.escreveLog("Existia o oldId: " + oldId);
							else
								omegaLog.escreveLog("Nuo existia o oldId: " + oldId);
							if (existeId(writableDatabase, tableName, newId))
								omegaLog.escreveLog("Existia o newId: " + newId);
							else
								omegaLog.escreveLog("Nuo existia o newId: " + newId);
						}

					}
				}

				if (seq >= 0 && numberRowsAffected > 0)
					con.conflictedNewId = String.valueOf(seq);
				else if (numberRowsAffected < 0 && houveModificacaoB) {
					ContentValues cv3 = new ContentValues();
					cv3.clear();
					cv3.put("id", newB);
					if (omegaLog != null)
						omegaLog.escreveLog("Tentativa de voltar B id " + tableName + " " + oldB + " => " + newB);
					numberRowsAffected = writableDatabase.update(tableName, cv3, "id= ?", new String[] { oldB });
					if (numberRowsAffected < 0)
						throw new Exception("Falhou a tentativa de voltar B id " + tableName + " " + oldB + " => "
								+ newB + " isso u um erro fatal.");
				}

			} else {
				con.conflictedNewId = null;
				con.successfull = true;
			}
			con.numberRowsAffected = numberRowsAffected;
		} catch (Exception ex) {
			con.conflictedNewId = null;
			con.numberRowsAffected = 0;
			con.successfull = false;

			if (log != null)
				log.escreveLog("procedureUpdateId - failed", ex);
		} finally {
			if(log!=null)
			log.escreveLog("procedureUpdateId - Result:" + con.successfull);
		}
		return con;
	}

	public boolean updateSequence(Transaction transaction, String tableName, long seq) throws SQLiteException {

		if (log != null)
			log.escreveLog("updateSequence " + tableName + " para " + seq);

		ContentValues cvUpdate = new ContentValues();
		cvUpdate.put("seq", seq);

		int total = update(transaction, "SQLITE_SEQUENCE", cvUpdate, "name = ? ", new String[] { tableName });
		if (total > 0) {
			return true;
		} else {
			return false;
		}

	}

	public ConflictUpdateId updateId(Transaction transaction, String tableName, String oldId, String newId,
			OmegaLog logSincronizacao) {
		if (transaction != null) {
			try {
				if (logSincronizacao != null)
					logSincronizacao
							.escreveLog("transaction::procedureUpdateId - procedure com transacao ja existente ");

				ConflictUpdateId cu = procedureUpdateId(transaction.writableDatabase, tableName, oldId, newId,
						logSincronizacao);

				return cu;
			} catch (SQLiteException ex) {
				if (log != null)
					log.escreveLog("transaction::updateId", ex);
				throw ex;
			}
		}

		synchronized (lockCRUD) {
			final SQLiteDatabase writableDatabase = getWritableDatabase(transaction);
			writableDatabase.beginTransaction();
			if (log != null)
				log.escreveLog("updateId transacao so para essa operacao - beginTransaction");
			try {
				if (logSincronizacao != null)
					logSincronizacao
							.escreveLog("procedureUpdateId com transacao isolada, criada apenas para essa atualizacao");
				ConflictUpdateId cu = procedureUpdateId(writableDatabase, tableName, oldId, newId, logSincronizacao);

				if (cu.successfull) {
					if (log != null)
						log.escreveLog("writableDatabase.setTransactionSuccessful - OK ");
					writableDatabase.setTransactionSuccessful();
				} else {
					if (log != null)
						log.escreveLog("writableDatabase.setTransactionSuccessful - NOT SUCCESSFULL");
				}

				return cu;
			} catch (SQLiteException ex) {
				if (log != null)
					log.escreveLog("updateId", ex);
				throw ex;
			} finally {
				writableDatabase.endTransaction();
			}

		}

	}

	public Cursor query(Transaction transaction, String table, String[] columns, String selection,
			String[] selectionArgs, String groupBy, String having, String orderBy) {
		return query(transaction, table, columns, selection, selectionArgs, groupBy, having, orderBy, null);
	}

	public Cursor query(Transaction transaction, String table, String[] columns, String selection,
			String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
		if (transaction != null) {
			try {
				if (log != null)
					log.escreveLog("transaction::query - Tabela: " + table + ". Colunas: "
							+ HelperString.arrayToString(columns) + ". Selection: " + selection + ". Selection args: "
							+ HelperString.arrayToString(selectionArgs) + ". Grupo By" + groupBy + ". Having: " + having
							+ ". OrderBy: " + orderBy + ". Limit" + limit);

				// //if(!writableDatabase.isOpen()) open();
				return transaction.writableDatabase.query(table, columns, selection, selectionArgs, groupBy, having,
						orderBy, limit);
			} catch (SQLiteException ex) {
				if (log != null)
					log.escreveLog("transaction::query", ex);
				throw ex;
			}
		}
		try {
			if (log != null)
				log.escreveLog("query - Tabela: " + table + ". Colunas: " + HelperString.arrayToString(columns)
						+ ". Selection: " + selection + ". Selection args: " + HelperString.arrayToString(selectionArgs)
						+ ". Grupo By" + groupBy + ". Having: " + having + ". OrderBy: " + orderBy + ". Limit" + limit);
			final SQLiteDatabase readableDatabase = getReadableDatabase(transaction);
			// //if(!writableDatabase.isOpen()) open();
			return readableDatabase.query(table, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
		} catch (SQLiteException ex) {
			if (log != null)
				log.escreveLog("query", ex);
			throw ex;
		}
	}

	public int update(Transaction transaction, String tableName, ContentValues cv, String where, String[] parametros)
			throws SQLiteException {

		if (transaction != null) {
			try {
				if (log != null)
					log.escreveLog("transaction::update. Table: " + tableName + ". CV: " + cv.toString() + ". Where: "
							+ where + ". Parametros: " + HelperString.arrayToString(parametros));

				return transaction.writableDatabase.update(tableName, cv, where, parametros);
			} catch (SQLiteException ex) {
				if (log != null)
					log.escreveLog("transaction::update", ex);
				throw ex;
			}
		}

		synchronized (lockCRUD) {
			try {
				if (log != null)
					log.escreveLog("update sem transaction. Table: " + tableName + ". CV: " + cv.toString()
							+ ". Where: " + where + ". Parametros: " + HelperString.arrayToString(parametros));
				final SQLiteDatabase writableDatabase = getWritableDatabase(transaction);
				return writableDatabase.update(tableName, cv, where, parametros);
			} catch (SQLiteException ex) {
				if (log != null)
					log.escreveLog("update", ex);
				throw ex;
			}
		}

	}

	public Cursor rawQuery(Transaction transaction, String consulta, String[] parametros) {
		return rawQuery(transaction, consulta, parametros, null);
	}

	public Cursor rawQuery(Transaction transaction, String consulta, String[] parametros,
			SQLiteDatabase writaDatabase) {
		if (writaDatabase == null) {

			if (transaction != null) {
				try {
					if (log != null)
						log.escreveLog("transaction::rawQuery - " + consulta + ". Parametros: "
								+ HelperString.arrayToString(parametros));

					return transaction.writableDatabase.rawQuery(consulta, parametros);
				} catch (SQLiteException ex) {
					if (log != null)
						log.escreveLog("transaction::rawQuery", ex);
					throw ex;
				}
			}
			try {
				if (log != null)
					log.escreveLog("rawQuery sem transacao - " + consulta + ". Parametros: "
							+ HelperString.arrayToString(parametros));
				final SQLiteDatabase readableDatabase = getReadableDatabase(transaction);
				return readableDatabase.rawQuery(consulta, parametros);
			} catch (SQLiteException ex) {
				if (log != null)
					log.escreveLog("rawQuery", ex);
				throw ex;
			}

		} else {
			try {
				if (log != null)
					log.escreveLog(
							"rawQuery - " + consulta + ". Parametros: " + HelperString.arrayToString(parametros));
				return writaDatabase.rawQuery(consulta, parametros);
			} catch (SQLiteException ex) {
				if (log != null)
					log.escreveLog("pWritable::rawQuery", ex);
				throw ex;
			}
		}

	}

	public boolean isOpen(SQLiteDatabase writableDatabase) {
		try {
			boolean isOpen = writableDatabase.isOpen();
			if (log != null)
				log.escreveLog("isOpen: " + isOpen);
			return isOpen;
		} catch (SQLiteException ex) {
			if (log != null)
				log.escreveLog("isOpen", ex);
			throw ex;
		}
	}

	public Long insertOrThrow(Transaction transaction, String table, String nullColumnHack, ContentValues values) {
		if (transaction != null) {
			try {
				if (log != null)
					log.escreveLog("transaction::insertOrThrow: " + table + ". nullColumnHack: " + nullColumnHack
							+ ". CV: " + values.toString());
				Long id = transaction.writableDatabase.insertOrThrow(table, nullColumnHack, values);
				if (log != null)
					log.escreveLog("Novo id: " + id);
				return id;
			} catch (SQLiteException ex) {
				if (log != null)
					log.escreveLog("transaction::insertOrThrow", ex);
				throw ex;
			}
		}
		try {
			synchronized (lockCRUD) {
				if (log != null)
					log.escreveLog("insertOrThrow: " + table + ". nullColumnHack: " + nullColumnHack + ". CV: "
							+ values.toString());
				final SQLiteDatabase writableDatabase = getWritableDatabase(transaction);

				Long id = writableDatabase.insertOrThrow(table, nullColumnHack, values);
				if (log != null)
					log.escreveLog("Novo id: " + id);
				return id;
			}
		} catch (SQLiteException ex) {
			if (log != null)
				log.escreveLog("insertOrThrow", ex);
			throw ex;
		}
	}

	private SQLiteDatabase getWritableDatabase(Transaction transaction) {
		if (transaction != null) {
			try {
				if (log != null)
					log.escreveLog("transaction::getWritableDatabase");
				return transaction.writableDatabase;
			} catch (SQLiteException ex) {
				if (log != null)
					log.escreveLog("transaction::getWritableDatabase", ex);
				throw ex;
			}
		}
		try {
			if (log != null)
				log.escreveLog("getWritableDatabase");

			return helperWritable.getWritableDatabase();
		} catch (SQLiteException ex) {
			if (log != null)
				log.escreveLog("getWritableDatabase", ex);
			throw ex;
		}

	}

	private SQLiteDatabase getReadableDatabase(Transaction transaction) {
		if (transaction != null) {
			try {
				if (log != null)
					log.escreveLog("transaction::getReadableDatabase");
				return transaction.writableDatabase;
			} catch (SQLiteException ex) {
				if (log != null)
					log.escreveLog("transaction::getReadableDatabase", ex);
				throw ex;
			}
		}
		try {
			if (log != null)
				log.escreveLog("getReadableDatabase");
			
			return helperReadable.getReadableDatabase();
		} catch (SQLiteException ex) {
			if (log != null)
				log.escreveLog("getReadableDatabase", ex);
			throw ex;
		}

	}

	public boolean checkDatabase() {
		if (log != null)
			log.escreveLog("checkDatabase");
		SQLiteDatabase checkDB = null;
		boolean validade = true;
		try {
			if (log != null)
				log.escreveLog("checkDatabase - opening...");
			checkDB = SQLiteDatabase.openDatabase(databasePath, null, SQLiteDatabase.OPEN_READONLY);
			if (checkDB != null){
				if(log!=null)
				log.escreveLog("checkDatabase - aberta com sucesso!");
			}

		} catch (SQLiteException e) {
			if (log != null)
				log.escreveLog("checkDatabase - falha durante a abertura: " + HelperExcecao.getDescricao(e));
			validade = false;
		}
		if (checkDB != null) {
			if (log != null)
				log.escreveLog("checkDatabase - fechando a conexuo...");
			checkDB.close();
			if (log != null)
				log.escreveLog("checkDatabase - conexuo fechada!");
		}

		return validade;
	}

}

class DataOpenHelper extends SQLiteOpenHelper {

	public static String TAG = "DataOpenHelper";

	public String databasePath;
	boolean writable = false;
	OmegaLog log;
	OpenHelper controler;
	Context context;
	DataOpenHelper(String databaseName, int databaseVersion, String databasePath, Context context, boolean writable,
			OmegaLog log, OpenHelper controler) {
		super(context, databaseName, null, databaseVersion);
		this.writable = writable;
		this.log = log;
		this.controler = controler;
		this.databasePath = databasePath;
		this.context=context;
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		if (log != null)
			log.escreveLog("onOpen");
		super.onOpen(db);

		controler.setPragmaOn(db);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		if (log != null)
			log.escreveLog("onCreate");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//			if(newVersion > oldVersion){
//				EXTDAOSistemaCorporacaoSincronizador.STATE_SINCRONIZADOR state = RotinaSincronizador.getStateSincronizador(
//						context, null);
//				switch (state) {
//					case COMPLETO:
//							SincronizadorEspelho.updateEstadoEstruturaMigrada(this.context);
//						break;
//					default:
//						break;
//				}
//			}
	}

}