package app.omegasoftware.pontoeletronico.database;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public class OpenHelperArquivo extends SQLiteOpenHelper{
	
	 
	    //singleton/ single instance reference of database instance
	    private static OpenHelperArquivo _dbHelper;
	 
	    //The Android's default system path of your application database.
	    //private static String DB_PATH = "/data/data/YOUR_PACKAGE/databases/";
	    private String DB_PATH = null;
	    //database name
	    private String DB_NAME = null;
	    //reference of database
	    
	    //
	 
	 
	    //
	    protected Context _context;
	    //Contains search result when user search with any text
	 
	 
	    String arquivo = null;
	    private OpenHelperArquivo(Context context, int versao, String arquivo, String pathBanco, String nomeBanco)
	    {
	        super(context, nomeBanco, null, versao);
	        this._context = context;
	        this.DB_PATH = pathBanco;
	        this.DB_NAME = nomeBanco;
	        this.arquivo = arquivo;
	    }
	    public static OpenHelperArquivo getInstance(Context context, int versao, String arquivo, String pathBanco, String nomeBanco)
	    {
	        if(_dbHelper == null)
	        {
	            _dbHelper = new OpenHelperArquivo(context, versao, arquivo,  pathBanco,  nomeBanco);
	        }
	        return _dbHelper;
	    }
	   
	 
	    private boolean checkDataBase()
	    {
	        SQLiteDatabase checkDB = null;
	 
	        try
	        {
	            String myPath = DB_PATH + DB_NAME;
	            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
	 
	        }catch(SQLiteException e)
	        {
	            //database does't exist yet.
	        }
	        if(checkDB != null)
	        {
	            checkDB.close();
	        }
	 
	        return checkDB != null ? true : false;
	    }
	    private void copyDataBase() throws IOException
	    {
	        //Open your local db as the input stream
	        InputStream myInput = new FileInputStream(this.arquivo);
	 
	        // Path to the just created empty db
	        
	 
	        //Open the empty db as the output stream
	        OutputStream myOutput = new FileOutputStream(DB_PATH);
	 
	        //transfer bytes from the inputfile to the outputfile
	        byte[] buffer = new byte[1024];
	        int length;
	        while ((length = myInput.read(buffer))>0)
	        {
	            myOutput.write(buffer, 0, length);
	        }
	        //Close the streams
	        myOutput.flush();
	        myOutput.close();
	        myInput.close();
	    
	    }
	    
	    public void onCreate(SQLiteDatabase db) {
	    	
	    }
		
		public void createDatabase() {
			boolean dbExist = checkDataBase();
		 
	        if(dbExist)
	        {
	            //do nothing - database already exist
	        }else{
	 
	            
	 
	            try {
//	            	this.close();
	            	SQLiteDatabase db = this.getReadableDatabase();
	                
	                db.close();
	                copyDataBase();
	 
	            } catch (IOException e) {
	 
	                throw new Error("Error copying database");
	 
	            }
	        }
			
		}
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			
			
		}
	 
	
}
