package app.omegasoftware.pontoeletronico.database;

import java.util.ArrayList;
import java.util.Random;

import android.database.Cursor;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.primitivetype.HelperBoolean;

public class PopulateTable {
	int totalInsercao;
	double porcentagemRemocao, porcentagemEdicao;
	int MAXIMO_EDICAO = 10;
	int MAXIMO_INSERCAO = 10;
	int MAXIMO_REMOCAO = 10;
	Database database;
	boolean sync;
	Table t;
	private static int MAXIMO_ITERACOES = 2;
	Random r = new Random();
	
	public PopulateTable(Database database, int totalInsercao,
			double porcentagemRemocao, double porcentagemEdicao, boolean sync) {

		this.totalInsercao = totalInsercao;
		this.porcentagemRemocao = porcentagemRemocao / 100;
		this.porcentagemEdicao = porcentagemEdicao / 100;
		this.database = database;
		this.sync = sync;

	}

	public boolean populate(Table t) throws Exception {
		
		int totalInsercao = (int)( MAXIMO_INSERCAO * r.nextDouble() );
		totalInsercao = totalInsercao == 0 ? 1 : totalInsercao;
		for (int k = 0; k < totalInsercao; k++) {
			t.clearData();
			ArrayList<String> vetor = t.getVetorNomeAtributo();
			for (int i = 0; i < vetor.size(); i++) {
				Attribute attr = t.getAttribute(vetor.get(i));
				attr.populateRandom(database);
				if (attr.getStrValue() == null
						&& (!attr.isNull() && !attr.isPrimaryKey())) {
					SingletonLog.insereErro(new Exception(
							"Impossivel gerar valor randomico para o atributo: "
									+ t.getName() + "::" + attr.getName()),
							TIPO.BIBLIOTECA_NUVEM);
					return false;
				}

			}
			t.formatToSQLite();
			t.insert(sync, false);
//			if (id == null || id <= 0)
//				He false;
		}
		return true;
		// db.endTransaction();
	}

	public void edit(Table t) throws Exception {
		t.clearData();
		int cont = 0;
		int total = t.countTupla();
//		int totalEdicao = (int) (total * porcentagemEdicao);
		int totalEdicao = total > MAXIMO_EDICAO ? MAXIMO_EDICAO : total;
		//totalEdicao = (int)( MAXIMO_EDICAO * r.nextDouble() );
		totalEdicao = totalEdicao == 0 ? 1 : totalEdicao;
		// SQLiteDatabase db = database.getOpenHelper();
		// db.beginTransaction();

		int it = 0;
		while (true) {
			Cursor cursor = t.getCursorListId();
			if(cursor != null){
				try{
					if (cursor.moveToFirst()) {

						do {

							String strId = cursor.getString(0);
							if (HelperBoolean.getRandom() || totalEdicao == 1) {

								t.clearData();
								t.populateRandom();
								t.setAttrValue(Table.ID_UNIVERSAL, strId);
								t.formatToSQLite();
								t.update(sync, false);

								cont += 1;

							}
							if (cont >= total || cont >= totalEdicao)
								break;
							if (cursor.isLast())
								break;
						} while (cursor.moveToNext());
					}
				}finally{
					if (cursor != null)
						cursor.close();
				}
			}
			it += 1;

			if (it >= MAXIMO_ITERACOES)
				break;
			if (cont >= total || cont >= totalEdicao)
				break;
		}

		// db.endTransaction();
	}

	public void remove(Table t) throws Exception {
		t.clearData();
		int cont = 0;
		t.formatToSQLite();
		int total = t.countTupla();
		int totalRemocao = total > MAXIMO_REMOCAO ? MAXIMO_REMOCAO : total;
		//totalRemocao = (int)( MAXIMO_REMOCAO * r.nextDouble() );
		totalRemocao = totalRemocao == 0 ? 1 : totalRemocao;
		// SQLiteDatabase db = database.getOpenHelper();
		// db.beginTransaction();

		int it = 0;
		while (true) {
			Cursor cursor = t.getCursorListId();
			if (cursor.moveToFirst()) {

				do {

					String strId = cursor.getString(0);
					if (HelperBoolean.getRandom() || totalRemocao == 1) {
						t.remove(strId, sync);
						cont += 1;
					}
					if (cont >= total || cont >= totalRemocao)
						break;
					if (cursor.isLast())
						break;
				} while (cursor.moveToNext());
			}
			if (cursor != null)
				cursor.close();
			it += 1;

			if (it >= MAXIMO_ITERACOES)
				break;
			if (cont >= total || cont >= totalRemocao)
				break;
		}

		// db.endTransaction();
	}

}
