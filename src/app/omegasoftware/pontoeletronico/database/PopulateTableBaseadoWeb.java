package app.omegasoftware.pontoeletronico.database;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.http.HelperHttpPostReceiveFile;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;

public class PopulateTableBaseadoWeb {
	
	Database database;
	String urlJson;
	boolean sync;
	File tempFile;
	Context context;
	Table[] vetorTabela;
	
	
	private static String OPERACAO_DELETE = "D";
	public static String OPERACAO_INSERT = "I";
	public static String OPERACAO_UPDATE = "U";

	public PopulateTableBaseadoWeb(Database database, String urlJson, Context context, boolean sync) {

		this.urlJson = urlJson;
		this.database = database;
		this.sync = sync;
		this.context = context;
		
	}
		
	private boolean generateTempFile(){
		
		File directory = context.getCacheDir();
		boolean success = true;
		
		try {
			
			this.tempFile = File.createTempFile("populate_table", "json", directory);
		
		} 
		catch (IOException e) {
			
			SingletonLog.insereErro(new Exception(
					"Impossivel gerar arquivo temporurio para armazenar JSON: Url=" + this.urlJson),
					TIPO.BIBLIOTECA_NUVEM);
			success = false;
			
		}
		
		return success;
				
	}
	
	public Boolean downloadJson(){
		
		boolean vValidade = false;
		
		if(this.generateTempFile()){
		
			if(urlJson != null && !urlJson.equals("")){
				vValidade = InterfaceMensagem.validaChamadaWebservice( HelperHttpPostReceiveFile.DownloadFileFromHttpPost(context, this.urlJson, this.tempFile.getAbsolutePath()));
			}
			
		}
		
		return vValidade;
		
	}
	
	private static String convertStreamToString(InputStream is) throws Exception {
		
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();
	    String line = null;
	    while ((line = reader.readLine()) != null) {
	      sb.append(line).append("\n");
	    }
	    reader.close();
	    return sb.toString();
	    
	}

	private static String getStringFromFile (File file) throws Exception {

	    FileInputStream fin = new FileInputStream(file);
	    String ret = convertStreamToString(fin);
	    
	    fin.close();        
	    return ret;
	    
	}
	
	private Table getTableFromVetor(String nomeTabela){
		
		Table t = null;
		
		for (int i = 0; i < vetorTabela.length; i++) {

			if (vetorTabela[i].name.toLowerCase().equals(nomeTabela.toLowerCase())) {
				t = vetorTabela[i];
				break;
			}

		}
		
		return t;
		
	}
	
	private String getRandomOperation(String baseOperation){
		
		if(baseOperation.equals(OPERACAO_INSERT) || 
				baseOperation.equals(OPERACAO_UPDATE)){
			
			double rand = Math.random();
			return (rand < 0.5) ? OPERACAO_INSERT : OPERACAO_UPDATE;
						
		}
		else if(baseOperation.equals(OPERACAO_DELETE)){
			
			return OPERACAO_DELETE;
			
		}
		
		return null;
		
	}
	
	public void parseJson(){
		
		JSONObject vObj;
		
		try {

			String vStrJson = getStringFromFile(this.tempFile);
			this.vetorTabela = database.getVetorTable();
			
			if(vStrJson == null) return;
			
			vObj = new JSONObject(vStrJson);				
			Iterator<?> keys = vObj.keys();

			while( keys.hasNext() ) {
				
			    String nomeTabela = (String)keys.next();
			    Table table = getTableFromVetor(nomeTabela);
			    
			    //array de operauues
			    if ( vObj.get(nomeTabela) instanceof JSONArray ) {
			    
			    	if(table != null){
			    		
			    		Boolean tabelaDoSistema = EXTDAOSistemaTabela
								.isTabelaDoSistema(database, nomeTabela);

						// se nuo existir registro na sistema_tabela, retorna null e
						// registra no log
						if (tabelaDoSistema == null) {

							SingletonLog.insereErro(new Exception(
									"Tabela nuo mapeada no sistema_tabela, cujo nome u: "
											+ nomeTabela), TIPO.BIBLIOTECA_NUVEM);
							continue;

						}
						else if (tabelaDoSistema){
						
							continue;
							
						}
						else {
			        	
					    	JSONArray operations = (JSONArray) vObj.get(nomeTabela);			    	
					    	for(int i=0; i < operations.length(); i++){
					    		
					    		//atributos da operauuo
					    		if(operations.get(i) instanceof JSONObject){
					    			
					    			JSONObject operacao = (JSONObject) operations.get(i);
					    			
					    			String tipoOperacao = operacao.getString("operation");
					    			String pk = operacao.getString("primaryKey");
					    			JSONObject fields = operacao.getJSONObject("fields");
					    			
					    			table.setId(pk);
					    			
					    			if(fields != null){
						    			
					    				Iterator<?> fieldNames = vObj.keys();
						    			while( fieldNames.hasNext() ) {
						    				
						    				String nomeAttributo = (String)fieldNames.next();
						    				table.setAttrValue(nomeAttributo, fields.getString(nomeAttributo));
						    				
						    				//Attribute attr = table.getAttribute(nomeAttributo);
						    				//attr.setStrValue(fields.getString(nomeAttributo));
						    									    				
						    			}
						    			
						    			String operacaoExecutar = getRandomOperation(tipoOperacao);
						    			
						    			if(operacaoExecutar.equals(OPERACAO_INSERT)){
						    				
						    				
						    				
						    			}
						    			else if(operacaoExecutar.equals(OPERACAO_UPDATE)){
						    				
						    				
						    				
						    			}
						    			else if(operacaoExecutar.equals(OPERACAO_DELETE)){
						    				
						    				
						    				
						    			}
					    			
					    			}
					    			
					    		}
					    		
					    	}
					    	
				    	}
	
				    }
				    
			    }
			    
			}
							
		}
		catch(JSONException e) {
			
			Log.e("consultaUltimaVersao", HelperExcecao.getDescricao(e));
			
		}
		catch(Exception e) {
			
			Log.e("consultaUltimaVersao", HelperExcecao.getDescricao(e));
			
		}
		
	}

}
