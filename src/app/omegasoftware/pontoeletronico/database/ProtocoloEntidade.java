package app.omegasoftware.pontoeletronico.database;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.webservice.ContainerProtocoloEntidade;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceProtocoloEnum;

public class ProtocoloEntidade <E extends Enum<E>> extends InterfaceProtocoloEnum{
	
	public static String TAG = "ProtocoloEntidade";
	
	protected Table objEXTDAO;
	
	ContainerProtocoloEntidade<E> vetorContainer[];
	
	
	public ProtocoloEntidade(
			JSONObject pJsonObject,
			Table objEXTDAO,
			Class<E> atributos,
			ContainerProtocoloEntidade<E> vetorContainer[]) throws Exception {
		
		super(pJsonObject, atributos);
		this.objEXTDAO= objEXTDAO; 

		for (Enum<E> enumVal: atributos.getEnumConstants()) {
			boolean achou = false;
            for (ContainerProtocoloEntidade<E> containerProtocoloEntidade : vetorContainer) {
				if(containerProtocoloEntidade.enumReferencia == enumVal){
					achou = true;
					break;
				}
            	
			}
			if(!achou)
				SingletonLog.insereErro(
					new Exception("Atributo para ser persistido nuo foi mapeado para a chave: " + enumVal.toString()),
					SingletonLog.TIPO.BIBLIOTECA_NUVEM);
        }  
		this.vetorContainer = vetorContainer;
	}
	
//	Considera que todo enum tem o mesmo nome dos atributos da tabela
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ProtocoloEntidade(
			JSONObject pJsonObject,
			Table objEXTDAO,
			Class<E> atributos) throws Exception {
		
		super(pJsonObject, atributos);
		this.objEXTDAO= objEXTDAO;
		
		
		vetorContainer = new ContainerProtocoloEntidade[hashNomePorValor.keySet().size()]; 
		Iterator<Enum<?>> it = hashNomePorValor.keySet().iterator();
		Enum<?> key;
		int i = 0;
		while(it.hasNext()){
			key = it.next();
			vetorContainer[i] = new ContainerProtocoloEntidade(key, key.toString());
			i += 1;
		}
	}
	
	public String persiste(Database db, boolean isToSync){
		if(objEXTDAO != null){
			objEXTDAO.setDatabase(db);
			Table copy =  objEXTDAO.factory();
			for (ContainerProtocoloEntidade<E> containerProtocoloEntidade : vetorContainer) {
				String nomeAtributo = containerProtocoloEntidade.nomeAtributo;
				String valorAtributo = super.getAtributo(containerProtocoloEntidade.enumReferencia);
				try {
					
					if(nomeAtributo.equalsIgnoreCase("id") && copy.select(valorAtributo)){
						
						return  valorAtributo;
					} else if(nomeAtributo.equalsIgnoreCase("operacao_sistema_mobile_id_INT")){
						
						copy.setAttrValue("operacao_sistema_mobile_id_INT", valorAtributo);
						Table x= copy.getFirstTupla();
						if(x != null)
							return  x.getId();
						
						
					}
				} catch (Exception e) {
					SingletonLog.insereErro(e, TIPO.BIBLIOTECA_NUVEM);
				}
					
				
				objEXTDAO.setAttrValue(nomeAtributo, valorAtributo);
			}
			
				
			objEXTDAO.formatToSQLite();
			Long id = objEXTDAO.insert(isToSync);
			if(id != null)
				return String.valueOf(id);
			else return null;
		} else return null;
			
	}

	
	public JSONObject getJsonObject() {
		JSONObject jsonObject= new JSONObject();
	    try {
	    	for (ContainerProtocoloEntidade<E> containerProtocoloEntidade : vetorContainer) {
				String valorAtributo = super.getAtributo(containerProtocoloEntidade.enumReferencia);
				String nomeAtributo = containerProtocoloEntidade.nomeAtributo;
				
				jsonObject.put(nomeAtributo, valorAtributo);
			}
	    	
	        return jsonObject;
	    } catch (JSONException e) {
	        
	        e.printStackTrace();
	        return null;
	    }
	}

	@Override
	public InterfaceProtocoloEnum factory(Context pContext,
			JSONObject pJsonObject) {
		
		return null;
	}

	
}
