package app.omegasoftware.pontoeletronico.database;

import java.util.ArrayList;

import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;




public class ResultSet {
	
	String[] mVetorNomeAtributo; 
	ArrayList<String[]> mMatrizValorAtributo;
//	public ResultSet(String[] pVetorNomeAtributo, ArrayList<String[]> pMatrizValorAtributo){
//		mVetorNomeAtributo = pVetorNomeAtributo;
//		mMatrizValorAtributo = pMatrizValorAtributo;
//				
//	}
	
	public ResultSet(String[] pVetorNomeAtributo){
		mVetorNomeAtributo = pVetorNomeAtributo;
		mMatrizValorAtributo = new ArrayList<String[]>();
				
	}
	
	public Integer getIndexDaColuna(String pNomeAtributo){
		if(mVetorNomeAtributo != null && mVetorNomeAtributo.length > 0 ){
			for (int i = 0 ; i < mVetorNomeAtributo.length; i++) {
				if(mVetorNomeAtributo[i].compareTo(pNomeAtributo) == 0 )
					return i;
				
			}
			
		}
		return null;
	}
	
	public boolean isVazio(){
		if(mMatrizValorAtributo.size() == 0 ) return true;
		else return false;
	}
	public String[] getVetorNomeAtributo(){
		return mVetorNomeAtributo;
	}
	
	public void insereTupla(String[] pTupla){
		if(pTupla == null)return;
		mMatrizValorAtributo.add(pTupla);
	}
	
	public ArrayList<String[]> getMatrizValorAtributo(){
		return mMatrizValorAtributo;
	} 

	public ArrayList<String> getValoresStringDoAtributo(String pNomeAtributo){
		Integer index = getIndexDaColuna(pNomeAtributo);
		if(index == null ) return null;
		else {
			ArrayList<String> lista = new ArrayList<String>(); 
			for(int i = 0 ; i < mMatrizValorAtributo.size(); i++){
				lista.add(mMatrizValorAtributo.get(i)[index]);
			}
			return lista;
		}
	}
	
	public String findRegistro(String atributoFind, String find){
		return findRegistro(atributoFind, find, atributoFind);
	}
	
	public String findRegistro(String atributoFind, String find, String atributoDesejado){
		Integer index = getIndexDaColuna(atributoFind);
		Integer indexDesejado = getIndexDaColuna(atributoDesejado);
		if(index == null || mMatrizValorAtributo==null || mMatrizValorAtributo.size() == 0
				|| indexDesejado == null) return null;
		else {
			for(int i = 0 ; i < mMatrizValorAtributo.size(); i++){
				String token = mMatrizValorAtributo.get(i)[index];
				if(token != null 
						&& token.compareTo(find) == 0)
					return mMatrizValorAtributo.get(i)[indexDesejado];
			}
			return null;
		}
	}
	
	public ArrayList<Long> getValoresLongDoAtributo(String pNomeAtributo){
		Integer index = getIndexDaColuna(pNomeAtributo);
		if(index == null || mMatrizValorAtributo==null || mMatrizValorAtributo.size() == 0) return null;
		else {
			ArrayList<Long> lista = new ArrayList<Long>(); 
			for(int i = 0 ; i < mMatrizValorAtributo.size(); i++){
				lista.add(HelperInteger.parserLong( mMatrizValorAtributo.get(i)[index]));
			}
			return lista;
		}
	}
	
	public int getTotalTupla(){
		if(mMatrizValorAtributo == null) return 0;
		else return mMatrizValorAtributo.size();
	}
	
	public String[] getTupla(int pIndex){
		if(pIndex < 0 || pIndex > mMatrizValorAtributo.size()) return null;
		else if (mMatrizValorAtributo == null || mMatrizValorAtributo.size() == 0 )
			return null;
		else {
			return mMatrizValorAtributo.get(pIndex);
		}
	}

	
	
}
