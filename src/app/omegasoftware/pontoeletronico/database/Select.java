package app.omegasoftware.pontoeletronico.database;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class Select extends InterfaceProtocolo{
	public HashMap<Enum<?>, Integer> hashNomePorValor = new HashMap<Enum<?>, Integer>();
	ArrayList<String[]> valores = new ArrayList<String[]>();
	int ponteiro = 0;
	
	public <E extends Enum<E>> Select(Class<E> atributos){
		int i = 0;
		for (Enum<E> enumVal: atributos.getEnumConstants()) {  
            
			hashNomePorValor.put(enumVal, i++);
        }  
	}
	
	
	
	public boolean first(){
		if(valores.size() == 0){
			return false;
		} else{
			ponteiro = 0;
			return true;
		}
	}
	
	public String getValor(Enum<?> e){
		
		int index = hashNomePorValor.get(e);
		return valores.get(ponteiro)[index];
		
	}

	public boolean next(){
		if(ponteiro + 1 < valores.size())
			return false;
		else{
			ponteiro++;
			return true;
		}
	}
}
