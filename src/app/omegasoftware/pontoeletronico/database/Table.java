package app.omegasoftware.pontoeletronico.database;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.Notificacao;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.Adapter.AppointmentPlaceAdapter;
import app.omegasoftware.pontoeletronico.common.ItemList.AppointmentPlaceItemList;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseAdapterListActivity;
import app.omegasoftware.pontoeletronico.common.activity.OmegaDatabaseArrayAdapterListActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCidade;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOEmpresa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPais;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaHistoricoSincronizador;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroHistorico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorAndroidParaWeb;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoDownloadArquivo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoDownloadArquivo.TIPO;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoOperacaoBanco;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUf;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.database.synchronize.SingletonControladorChaveUnica;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.gpsnovo.GeoPoint;
import app.omegasoftware.pontoeletronico.mapa.HelperMapa;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
import app.omegasoftware.pontoeletronico.webservice.ServicosCobranca;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public abstract class Table {
	public enum SEXO {
		MASCULINO(1), FEMININO(2);
		int id = 0;

		SEXO(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}

		public static SEXO toSexo(int id) {
			for (SEXO sexo : SEXO.values()) {
				if (sexo.getId() == id) {
					return sexo;
				}
			}
			return null;
		}
	}

	public String name;
	public static final String ID_UNIVERSAL = "id";
	public static final String NOME_UNIVERSAL = "nome";
	public static final String CPF_UNIVERSAL = "cpf";
	public static final String RG_UNIVERSAL = "rg";
	public static final String TELEFONE_UNIVERSAL = "telefone";
	public static final String TELEFONE_1_UNIVERSAL = "telefone1";
	public static final String TELEFONE_2_UNIVERSAL = "telefone2";
	public static final String FAX_UNIVERSAL = "fax";
	public static final String CELULAR_UNIVERSAL = "celular";
	public static final String CELULAR_SMS_UNIVERSAL = "celular_sms";
	public static final String SENHA_UNIVERSAL = "senha";

	public static String CEP_UNIVERSAL = "cep";
	public static String LOGRADOURO_UNIVERSAL = "logradouro";
	public static String EMAIL_UNIVERSAL = "email";
	public static String NUMERO_UNIVERSAL = "numero";
	public static String COMPLEMENTO_UNIVERSAL = "complemento";
	public static String ESTADO_ID_INT_UNIVERSAL = "uf_id_INT";
	public static String CIDADE_ID_INT_UNIVERSAL = "cidade_id_INT";
	public static String PAIS_ID_INT_UNIVERSAL = "pais_id_INT";
	public static String BAIRRO_ID_INT_UNIVERSAL = "bairro_id_INT";
	public static String SEXO_ID_INT_UNIVERSAL = "sexo_id_INT";

	public static String CORPORACAO_ID_INT = "corporacao_id_INT";

	public static String TAG = "Table";

	public ContainerKey containerUniqueKey = null;
	public ArrayList<ContainerKey> containersChaves = null;
	public Hashtable<String, Attribute> hashNameAttributeByAttribute;
	public Database database;
	public ArrayList<String> listSortNameAttribute;

	// private static LinkedHashMap<String, String> hashNomeTabelaPorIdTabela =
	// new LinkedHashMap<String, String>();

	public void fixFKAttributesProperties() {

		for (String attrName : hashNameAttributeByAttribute.keySet()) {

			if (attrName.toLowerCase().contains("_id_int")) {

				String refTableName = attrName.toLowerCase().replace("_id_int", "");
				String refFieldName = "id";

				String tabelaFKAnterior = hashNameAttributeByAttribute.get(attrName).tableFK;
				String atributoFKAnterior = hashNameAttributeByAttribute.get(attrName).attributeFK;
				if (tabelaFKAnterior == null || !tabelaFKAnterior.toLowerCase().equals(refTableName)
						|| atributoFKAnterior == null || !atributoFKAnterior.equals(refFieldName)) {

				}

				// hashNameAttributeByAttribute.get(attrName).tableFK =
				// refTableName;
				// hashNameAttributeByAttribute.get(attrName).attributeFK =
				// refFieldName;

			}

		}

	}

	public void addKey(String nomeChave, String[] atributos) {
		if (atributos == null || atributos.length == 0)
			return;
		else if (containersChaves == null)
			containersChaves = new ArrayList<ContainerKey>();
		containersChaves.add(new ContainerKey(nomeChave, atributos, false));
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentificadorResumido() {
		return getName() + "[" + getId() + "]";
	}

	public String getIdentificadorRegistro() {
		StringBuilder sb = new StringBuilder();
		sb.append("Tabela: " + this.getName() + ". VALUES [ ");
		for (String attr : this.listSortNameAttribute) {
			String content = getStrValueOfAttribute(attr);
			sb.append(attr + "::" + HelperString.formatarParaImpressao(content) + "<br/>");
		}
		sb.append("]");
		return sb.toString();
	}

	public String getStrQuerySelectInsertCopia(Table old) {
		// INSERT INTO "main"."bairro" ("id", "nome", "nome_normalizado",
		// "cidade_id_INT", "corporacao_id_INT") SELECT "id", "nome",
		// "nome_normalizado", "cidade_id_INT", "corporacao_id_INT" FROM
		// "_bairro_old_20151109"

		String select = HelperString.arrayToStringSQL(listSortNameAttribute);

		String q = "INSERT INTO \"" + getName() + "\" (" + select + ") SELECT " + select + " FROM " + old.getName();
		return q;
	}

	public ArrayList<String> getVetorNomeAtributo() {
		return listSortNameAttribute;
	}

	// Funcao deve ser sobrescrevida pelas entidades que iruo emitir notificacao
	public Notificacao getNotificacao(Context c) {
		return null;
	}

	public boolean comparaValores(Table t) {
		return comparaValores(t, true) == null ? true : false;
	}

	public boolean comparaValores(String[] campos, String[] valores) {
		if (valores == null || campos == null || campos.length != valores.length)
			return false;
		for (int i = 0; i < campos.length; i++) {
			String valor = getStrValueOfAttribute(campos[i]);
			if (valor == null && valores[i] == null)
				continue;
			else if (valor != null && valores[i] != null && valor.compareTo(valores[i]) == 0)
				continue;
			else
				return false;
		}
		return true;
	}

	public String comparaValores(Table t, boolean retornarNaPrimeiraDif) {
		ArrayList<String> atributos1 = this.getVetorNomeAtributo();
		boolean iguais = true;
		StringBuilder sb = null;
		new StringBuilder();
		// verifica se os registros sao iguais
		for (int i = 0; i < atributos1.size(); i++) {
			String nomeAttr = atributos1.get(i);
			if (nomeAttr.equalsIgnoreCase(Table.SENHA_UNIVERSAL)) {
				continue;
			}

			String v1 = this.getStrValueOfAttribute(nomeAttr);
			String v2 = t.getStrValueOfAttribute(nomeAttr);

			if ((v1 != null && v2 != null && v1.equalsIgnoreCase(v2)) || (v1 == null && v2 == null)) {
				continue;
			} else {
				iguais = false;
				if (sb == null)
					sb = new StringBuilder();
				sb.append(" . " + nomeAttr + " - ");
				sb.append(HelperString.formatarParaImpressao(v1));
				sb.append("::" + HelperString.formatarParaImpressao(v2) + " ");
				if (retornarNaPrimeiraDif)
					break;
			}
		}
		return iguais ? null : sb.toString();
	}

	public void setId(String id) {
		this.setAttrValue(ID_UNIVERSAL, id);
	}

	public void setLongId(Long id) {
		this.setAttrValue(ID_UNIVERSAL, String.valueOf(id));
	}

	public int getTotalAtributo() {
		if (listSortNameAttribute == null)
			return 0;
		else
			return listSortNameAttribute.size();

	}

	public static boolean existeId(Database database, String tabela, String idTabela) {

		String args[] = { idTabela };
		String vQuery = " SELECT 1 " + " FROM " + tabela + " as t " + " WHERE " + " t." + ID_UNIVERSAL
				+ " = ? LIMIT 0,1";

		Cursor cursor = null;
		try {
			cursor = database.rawQuery(vQuery, args);
			if (cursor.moveToFirst())
				return true;
			else
				return false;
		} finally {
			if (cursor != null)
				cursor.close();
		}

	}

	public boolean existeId(String idTabela) {

		String args[] = { idTabela };
		String vQuery = " SELECT 1 FROM " + getName() + " as t " + " WHERE " + " t." + ID_UNIVERSAL + " = ? ";

		Cursor cursor = null;
		try {
			cursor = database.rawQuery(vQuery, args);
			if (cursor.moveToFirst())
				return true;
			else
				return false;
		} finally {
			if (cursor != null)
				cursor.close();
		}

	}

	public Table(String p_name, Database p_database) {
		listSortNameAttribute = new ArrayList<String>();
		name = p_name;
		database = p_database;
		hashNameAttributeByAttribute = new Hashtable<String, Attribute>();
	}

	public void setUniqueKey(String nome, String pVetorAttributeName[]) {

		if (pVetorAttributeName == null)
			return;
		else if (pVetorAttributeName.length == 0)
			return;
		else {
			containerUniqueKey = new ContainerKey(nome, pVetorAttributeName, true);
		}

	}

	public ContainerKey getContainerUniqueKey() {
		return containerUniqueKey;
	}

	public void setIdAttrCorporacaoIfExists() {
		Attribute vAttr = getAttribute("corporacao_id_INT");
		if (vAttr != null) {
			vAttr.setStrValue(OmegaSecurity.getIdCorporacao());
		}
	}

	public enum TIPO_ATUALIZACAO_ENDERECO {
		ENDERECO_OK, SEM_ENDERECO, SEM_COORDENADA_NO_ENDERECO
	}

	public class ContainerAtualizaEndereco {
		public TIPO_ATUALIZACAO_ENDERECO tipoAtualizacaoEndereco;
		public GeoPoint geoPoint;

		public ContainerAtualizaEndereco(TIPO_ATUALIZACAO_ENDERECO pTipoAtualizacaoEndereco, GeoPoint pGeoPoint) {
			geoPoint = pGeoPoint;
			tipoAtualizacaoEndereco = pTipoAtualizacaoEndereco;

		}
		public ContainerAtualizaEndereco() {

		}
	}

	public ContainerAtualizaEndereco atualizaLatitudeELongitude(Context pContext, Database db, String pId)
			throws Exception {
		Table vObjEmpresa = this.factory();

		if (vObjEmpresa.select(pId)) {
			String vStrEndereco = vObjEmpresa.getEndereco(pContext);
			if (vStrEndereco == null || vStrEndereco.length() == 0) {
				return new ContainerAtualizaEndereco(TIPO_ATUALIZACAO_ENDERECO.SEM_ENDERECO, null);
			}
			GeoPoint vGeo = HelperMapa.getGeoPointDoEndereco(vStrEndereco);
			if (vGeo != null) {

				vObjEmpresa.setAttrValue(EXTDAOEmpresa.LATITUDE_INT, String.valueOf(vGeo.getLatitudeE6()));
				vObjEmpresa.setAttrValue(EXTDAOEmpresa.LONGITUDE_INT, String.valueOf(vGeo.getLongitudeE6()));
				vObjEmpresa.update(true);
				return new ContainerAtualizaEndereco(TIPO_ATUALIZACAO_ENDERECO.ENDERECO_OK, vGeo);
			} else {
				return new ContainerAtualizaEndereco(TIPO_ATUALIZACAO_ENDERECO.SEM_COORDENADA_NO_ENDERECO, null);
			}
		}
		return null;
	}

	public String getDiretorioDefault() {
		String path = EXTDAOSistemaTipoDownloadArquivo.getPath(this.getDatabase(), TIPO.DEFAULT);
		return path;
	}

	public String getStrListAttrValueWithoutCorporacaoESenhaAttr() {
		StringBuilder vToken = new StringBuilder();

		for (String vNomeAttr : listSortNameAttribute) {
			Attribute vAttr = getAttribute(vNomeAttr);
			if (vAttr == null)
				continue;
			if (vAttr.isAutoIncrement() && vAttr.isPrimaryKey())
				continue;
			String vAttrToLower = vNomeAttr.toLowerCase();
			if (vAttrToLower.compareTo("corporacao_id_int") == 0)
				continue;
			else if (vAttrToLower.compareTo("senha") == 0)
				continue;
			else {
				String vValue = "";
				if (vAttr.isEmpty())
					vValue = "null";
				else
					vValue = vAttr.getStrValue();

				if (vToken.length() == 0)
					vToken.append(vValue);
				else
					vToken.append(OmegaConfiguration.DELIMITER_QUERY_COLUNA + vValue);
			}
		}

		return vToken.toString();
	}

	public String getStrListAttrWithoutCorporacaoAndSenhaIfExist() {
		String vToken = "";

		for (String vAttrNome : listSortNameAttribute) {

			Attribute vAttr = getAttribute(vAttrNome);
			if (vAttr == null)
				continue;
			if (vAttr.isAutoIncrement() && vAttr.isPrimaryKey())
				continue;

			else {
				String vAttrToLower = vAttrNome.toLowerCase();
				if (vAttrToLower.compareTo("corporacao_id_int") == 0)
					continue;
				else if (vAttrToLower.compareTo("senha") == 0)
					continue;
				else if (vToken.length() == 0)
					vToken += vAttrNome;
				else
					vToken += OmegaConfiguration.DELIMITER_QUERY_COLUNA + vAttrNome;
			}
		}

		return vToken;
	}

	public String getStrQueryWhereUniqueId() {

		if (containerUniqueKey == null)
			return null;
		else {

			String vVetorNameAttribute[] = containerUniqueKey.getVetorAttribute();
			String vQuery = "WHERE ";
			String vQueryWhere = "";
			for (String vAttrName : vVetorNameAttribute) {
				Attribute vAttr = getAttribute(vAttrName);
				String vLowerName = vAttrName.toLowerCase();
				if (vLowerName.compareTo("corporacao_id_int") == 0)
					continue;
				else if (!vAttr.isEmpty()) {
					// if(vAttr.getSQLType() == SQL_TYPE.TEXT){
					// if(vQueryWhere.length() == 0 )
					// vQueryWhere += vAttr.getName() + " = '" +
					// vAttr.getStrValue() + "' ";
					// else vQueryWhere += " AND " + vAttr.getName() + " = '" +
					// vAttr.getStrValue() + "' ";
					// }
					// else{
					if (vQueryWhere.length() == 0)
						vQueryWhere += vAttr.getName() + " = '" + vAttr.getStrValue() + "' ";
					else
						vQueryWhere += " AND " + vAttr.getName() + " = '" + vAttr.getStrValue() + "' ";
					// }
				} else
					return null;
			}
			return vQuery + vQueryWhere;
		}

	}

	public void populate() {

	}

	public boolean isEqual(String p_nomeTabela) {
		String nomeTabela = "";
		if (p_nomeTabela == null) {
			return false;
		} else if (p_nomeTabela.length() == 0)
			return false;
		nomeTabela = p_nomeTabela.toLowerCase();
		String myName = name.toString().toLowerCase();

		if (nomeTabela.compareTo(myName) == 0) {
			return true;
		} else
			return false;
	}

	public JSONObject getJsonObj() throws JSONException {

		JSONObject jsonObject = new JSONObject();
		for (String vAttrName : this.listSortNameAttribute) {
			jsonObject.put(vAttrName, getStrValueOfAttribute(vAttrName));

		}
		return jsonObject;
	}

	public ArrayList<Attribute> getListAttributeFKOfTable(String pStrName) {
		ArrayList<Attribute> vListAttribute = new ArrayList<Attribute>();
		if (pStrName == null)
			return null;
		else if (pStrName.length() == 0)
			return null;
		pStrName = pStrName.toLowerCase();
		for (String vAttrName : this.listSortNameAttribute) {
			Attribute vAttr = this.getAttribute(vAttrName);
			if (vAttr.isFK()) {
				String vTable = vAttr.getStrTableFK();
				if (vTable.compareTo(pStrName) == 0)
					vListAttribute.add(vAttr);
			}
		}
		return vListAttribute;
	}

	public boolean isTableDependent(String pStrName) {

		if (pStrName == null)
			return false;
		else if (pStrName.length() == 0)
			return false;
		pStrName = pStrName.toLowerCase();
		for (String vAttrName : this.listSortNameAttribute) {
			Attribute vAttr = this.getAttribute(vAttrName);
			if (vAttr.isFK()) {
				String vTable = vAttr.getStrTableFK();
				if (vTable.compareTo(pStrName) == 0)
					return true;
			}
		}
		return false;
	}

	public ArrayList<Attribute> getListAttributeFK() {
		ArrayList<Attribute> vListAttribute = new ArrayList<Attribute>();
		for (String vAttrName : this.listSortNameAttribute) {
			Attribute vAttr = this.getAttribute(vAttrName);
			if (vAttr.isFK())
				vListAttribute.add(vAttr);
		}
		return vListAttribute;
	}

	public ArrayList<String> getListNameAttribute() {
		ArrayList<String> vListAttribute = new ArrayList<String>();
		for (String vAttrName : this.listSortNameAttribute)
			vListAttribute.add(vAttrName);

		return vListAttribute;
	}

	public String[] getAtributosMenosId() {
		//TODO cachear esses dados

		//String[] atributos = new String[this.listSortNameAttribute.size() - desconto];
		ArrayList<String> atributos = new ArrayList<String>(this.listSortNameAttribute.size());
		int i = 0;
		for (String attrName : this.listSortNameAttribute) {
			if (attrName.equalsIgnoreCase(Table.ID_UNIVERSAL)
					|| attrName.equalsIgnoreCase(Table.SENHA_UNIVERSAL) )
				continue;

			//atributos[i++] = attrName;
			atributos.add(attrName);
		}
		String[] ret = new String[atributos.size()];
		atributos.toArray(ret);
		return ret;
	}

	private class ContainerOperationUpdate {
		String newId;
		ArrayList<Table> listTable;
		Attribute attribute;

		public ContainerOperationUpdate(String pNewId, ArrayList<Table> pListTable, Attribute pAttribute) {
			newId = pNewId;
			listTable = pListTable;
			attribute = pAttribute;
		}

		public boolean execute() {
			boolean vValidade = true;
			for (Table vTableDependent : listTable) {
				vTableDependent.setAttrValue(attribute.getName(), newId);
				boolean vValidadeUpdate = vTableDependent.update(true);
				if (!vValidadeUpdate)
					vValidade = false;
				vTableDependent.clearData();
			}
			return vValidade;
		}
	}

	public ArrayList<ContainerOperationUpdate> getListContainerOperationUpdateOfTableDependent(String pOldId,
																							   String pNewId, Table pTableDependent) {

		ArrayList<ContainerOperationUpdate> vListContainerOperationUpdate = new ArrayList<ContainerOperationUpdate>();

		ArrayList<Attribute> vListAttribute = pTableDependent.getListAttributeFKOfTable(getName());

		// vListAttribute = {fucionario_id_INT}
		for (Attribute vAttr : vListAttribute) {
			pTableDependent.clearData();

			pTableDependent.setAttrValue(vAttr.getName(), pOldId);
			ArrayList<Table> vListTableLinked = pTableDependent.getListTable(new String[] { ID_UNIVERSAL }, true);
			vListContainerOperationUpdate.add(new ContainerOperationUpdate(pNewId, vListTableLinked, vAttr));

		}
		pTableDependent.clearData();
		return vListContainerOperationUpdate;
	}

	// /DEPRECATED
	public boolean updateListIdToNewId(String[] pListOldId, String[] pListNewId) throws JSONException, Exception {
		boolean vValidade = true;
		for (int i = pListNewId.length - 1; i >= 0; i--) {

			String pOldId = pListOldId[i];
			String pNewId = pListNewId[i];
			if (pOldId == null || pNewId == null)
				continue;
			else if (pOldId.length() == 0 || pNewId.length() == 0)
				continue;
				// Se os id's forem iguais, entao nao a necessidade de
				// atualiza-lo
			else if (pOldId.compareTo(pNewId) == 0)
				continue;
			else {
				Integer vIndexCheck = HelperInteger.parserInteger(pNewId);
				if (vIndexCheck == null)
					continue;
				else if (vIndexCheck < 0)
					continue;
				else {
					this.clearData();
					this.setAttrValue(ID_UNIVERSAL, pOldId);
					EXTDAOSistemaRegistroSincronizadorAndroidParaWeb objSRSAW = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(
							getDatabase());
					boolean vValidadeUpdate = objSRSAW.updateIdSynchronized(this.getName(), null, this.getId(), pNewId,
							null, true);
					if (!vValidadeUpdate)
						vValidade = false;
				}
			}
		}
		return vValidade;

	}

	public boolean updateListIdInListTableDependent(String[] pListOldId, String[] pListNewId) {
		ArrayList<Table> vListTableDependent = database.getListTableDependent(getName());
		boolean vValidade = true;
		for (Table vTableDependent : vListTableDependent) {
			ArrayList<ContainerOperationUpdate> vTotalListContainerOperationUpdate = new ArrayList<ContainerOperationUpdate>();

			for (int i = 0; i < pListNewId.length; i++) {
				String pOldId = pListOldId[i];
				String pNewId = pListNewId[i];
				if (pNewId.compareTo("null") == 0) {
					// Log.d(TAG , "Id nulo");
					continue;
				} else if (pNewId.compareTo(pOldId) == 0)
					continue;
				// todo atributo da tabela que faz referencia para a tabela em
				// questao
				// this: Funcionario
				// vListTableDependent = {EmpresaFuncionario}
				// tablea = EmpresaFuncionario
				ArrayList<ContainerOperationUpdate> vListContainerOperationOfTable = getListContainerOperationUpdateOfTableDependent(
						pOldId, pNewId, vTableDependent);
				// Armazena todas as tuplas q irao sofrer modificacao
				for (ContainerOperationUpdate containerOperationUpdate : vListContainerOperationOfTable) {
					vTotalListContainerOperationUpdate.add(containerOperationUpdate);
				}
			}
			// uma vez identificada executa as operacoes
			for (ContainerOperationUpdate containerOperationUpdate : vTotalListContainerOperationUpdate) {
				containerOperationUpdate.execute();
			}
		}
		return vValidade;
	}

	public String getEndereco(Context pContext) throws Exception {
		AppointmentPlaceAdapter vAppointment = getAppointmentPlacesAdapter(pContext);
		AppointmentPlaceItemList vItem = vAppointment.getItem(0);
		return vItem.getStrAddress();
	}

	public AppointmentPlaceAdapter getAppointmentPlacesAdapter(Context p_context) throws Exception {
		String idCidade = this.getStrValueOfAttribute(Table.CIDADE_ID_INT_UNIVERSAL);
		String uf = null;
		String pais = null;
		if (idCidade != null) {
			EXTDAOCidade vCidade = new EXTDAOCidade(this.getDatabase());
			vCidade.setAttrValue(EXTDAOCidade.ID, idCidade);
			vCidade.select();
			String vIdUf = vCidade.getStrValueOfAttribute(EXTDAOCidade.UF_ID_INT);

			if (vIdUf != null) {
				uf = vCidade.getNomeDaChaveExtrangeira(EXTDAOCidade.UF_ID_INT);
				EXTDAOUf vObjUf = new EXTDAOUf(this.getDatabase());
				vObjUf.setAttrValue(EXTDAOUf.ID, vIdUf);
				if (vObjUf.select()) {
					pais = vObjUf.getNomeDaChaveExtrangeira(EXTDAOUf.PAIS_ID_INT);
				}
			}

		} else if (this.isEqual(EXTDAOUf.NAME)) {
			uf = this.getStrValueOfAttribute(EXTDAOUf.NOME);
			pais = this.getNomeDaChaveExtrangeira(EXTDAOUf.PAIS_ID_INT);
		} else if (this.isEqual(EXTDAOPais.NAME)) {

			pais = this.getStrValueOfAttribute(EXTDAOPais.NOME);
		}
		String vStrId = this.getStrValueOfAttribute(ID_UNIVERSAL);
		Integer vId = null;
		if (vStrId != null)
			vId = Integer.valueOf(vStrId);
		if(vId == null){
			return null;
		}
		ArrayList<AppointmentPlaceItemList> result = new ArrayList<AppointmentPlaceItemList>();
		AppointmentPlaceItemList place = new AppointmentPlaceItemList(
				p_context,
				vId,
				this.getStrValueOfAttribute(LOGRADOURO_UNIVERSAL),
				this.getStrValueOfAttribute(NUMERO_UNIVERSAL),
				this.getStrValueOfAttribute(COMPLEMENTO_UNIVERSAL),
				getNomeDaChaveExtrangeira(BAIRRO_ID_INT_UNIVERSAL),
				getNomeDaChaveExtrangeira(CIDADE_ID_INT_UNIVERSAL),
				uf,
				pais,
				this.getStrValueOfAttribute(EXTDAOPessoa.CEP_UNIVERSAL),
				this.getPhones(),
				this.getStrValueOfAttribute(EXTDAOPessoa.EMAIL_UNIVERSAL));

		result.add(place);

		AppointmentPlaceAdapter adapter = new AppointmentPlaceAdapter(p_context, result);

		return adapter;

	}

	public Integer getIntValueOfAttribuite(String pAttrName) {
		String token = getStrValueOfAttribute(EXTDAOSistemaHistoricoSincronizador.SISTEMA_TIPO_OPERACAO_ID_INT);
		if (token != null) {
			return HelperInteger.parserInt(token);
		} else
			return null;
	}

	public Integer getIntegerValueOfAttribute(String pAttrName) {
		return HelperInteger.parserInteger(getStrValueOfAttribute(pAttrName));
	}

	public Long getLongValueOfAttribute(String pAttrName) {
		return HelperInteger.parserLong(getStrValueOfAttribute(pAttrName));
	}

	public HelperDate getHelperDateValueOfAttribute(String attrSec, String attrOffsec){
		Date d= getDateValueOfAttribute(attrSec, attrOffsec);
		if(d != null){
			return new HelperDate(d);
		}else return null;
	}

	public Calendar getCalenadarValueOfAttribute(String attrSec, String attrOffsec){
		Date d= getDateValueOfAttribute(attrSec, attrOffsec);
		if(d != null){
			return HelperDate.toCalendar(d);
		}else return null;
	}


	public Date getDateValueOfAttribute(String attrSec, String attrOffsec){
		Long sec= getLongValueOfAttribute(attrSec);
		Integer offsec  = getIntegerValueOfAttribute(attrOffsec);
		if(sec == null) return null;
		else return HelperDate.getDateFromSecOfssec(sec, offsec, new SimpleDateFormat());
	}

	public String getDatePorExtensoValueOfAttribute(String attrSec, String attrOffsec){
		Date d = getDateValueOfAttribute(attrSec, attrOffsec);
		if(d != null) {
			return HelperDate.getDataFormatadaPorExtenso(d);
		} else return null;
	}

	public String getStrValueOfAttribute(String pAttrName) {
		Attribute vAttr = this.getAttribute(pAttrName);
		if (vAttr == null)
			return null;
		else
			return vAttr.getStrValue();

	}

	public LinkedHashMap<String, String> getHashMapIdByDefinition(boolean p_isToUpperCase) {
		return getHashMapIdByDefinition(p_isToUpperCase, NOME_UNIVERSAL);
	}

	public LinkedHashMap<String, String> getHashMapIdByDefinition(boolean p_isToUpperCase, String pNomeAttr) {

		this.clearData();
		// setAttrCorporacaoIdIntIfExist();

		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();
		ArrayList<Table> vList = this.getListTable(new String[] { "id" }, true);
		if (vList != null && vList.size() > 0)
			for (Table iEntry : vList) {
				Table obj = iEntry;
				String strId = obj.getAttribute("id").getStrValue();
				String strNome = obj.getAttribute(pNomeAttr).getStrValue();

				if (strNome != null) {
					String strNomeModificado = strNome;
					if (p_isToUpperCase) {
						strNomeModificado = strNomeModificado.toUpperCase();
					} else
						strNomeModificado = strNomeModificado.toLowerCase();
					hashTable.put(strId, strNomeModificado);
				}

			}

		return hashTable;

	}

	public boolean containsAttribute(String pStrAttribute) {

		if (getAttribute(pStrAttribute) == null)
			return false;
		else
			return true;
	}

	public LinkedHashMap<String, String> getHashMapIdByAttributeValue(boolean p_isToUpperCase,
																	  String pStrNameAttribute) {

		// this.clearData();
		LinkedHashMap<String, String> hashTable = new LinkedHashMap<String, String>();

		if (!containsAttribute(pStrNameAttribute))
			return null;
		ArrayList<Table> vListTupla = this.getListTable(new String[] { "id" }, null, true);
		if (vListTupla != null) {
			for (Table iEntry : vListTupla) {

				Table obj = iEntry;
				String strId = obj.getAttribute("id").getStrValue();
				String strNome = obj.getAttribute(pStrNameAttribute).getStrValue();

				if (strNome != null) {
					String strNomeModificado = strNome;
					if (p_isToUpperCase) {
						strNomeModificado = strNomeModificado.toUpperCase();
					}
					hashTable.put(strId, strNomeModificado);
				}

			}
		}

		return hashTable;
	}

	public String[] getVetorStringDefinition(boolean p_isToUpperCase) {

		this.clearData();

		ArrayList<Table> vListTable = this.getListTable(new String[] { "id" }, true);
		ArrayList<String> vListToken = new ArrayList<String>();

		for (Table iEntry : vListTable) {
			Table obj = iEntry;

			String strNome = obj.getAttribute("nome").getStrValue();
			if (strNome != null) {
				String strNomeModificado = strNome.toUpperCase();
				if (p_isToUpperCase) {
					strNomeModificado = strNomeModificado.toUpperCase();
				}
				vListToken.add(strNomeModificado);
			}

		}

		String vVetorTokenRetorno[] = new String[vListTable.size()];
		vListToken.toArray(vVetorTokenRetorno);
		return vVetorTokenRetorno;

	}

	public Table getObjDaChaveExtrangeira(String pNomeChave) throws Exception {
		Attribute attr = this.getAttribute(pNomeChave);
		if (attr == null)
			return null;
		String p_id = attr.getStrValue();
		if (p_id == null)
			return null;
		else if (p_id.length() == 0)
			return null;

		String nameTableFK = attr.getStrTableFK();
		Database database = this.getDatabase();

		Table tableFK = database.factoryTable(nameTableFK);
		tableFK.setAttrValue(ID_UNIVERSAL, p_id);
		tableFK.select();
		return tableFK;
	}

	public String getNomeDaChaveExtrangeira(String pNomeChave) throws Exception {
		Attribute attr = this.getAttribute(pNomeChave);
		if (attr == null)
			return null;
		String p_id = attr.getStrValue();
		if (p_id == null)
			return null;
		else if (p_id.length() == 0)
			return null;

		String nameTableFK = attr.getStrTableFK();
		Database database = this.getDatabase();

		Table tableFK = database.factoryTable(nameTableFK);
		tableFK.setAttrValue(ID_UNIVERSAL, p_id);
		tableFK.select();

		Attribute attrNomeTabelaFK = tableFK.getAttribute(NOME_UNIVERSAL);
		if (attrNomeTabelaFK != null) {
			return attrNomeTabelaFK.getStrValue();
		} else
			return null;
	}

	public String getValorDoAtributoDaChaveExtrangeira(String p_nomeChave, String pNomeDoAtributoNaTabelaExtrangeira)
			throws Exception {
		Attribute attr = this.getAttribute(p_nomeChave);
		if (attr == null)
			return null;
		String p_id = attr.getStrValue();
		if (p_id == null)
			return null;
		else if (p_id.length() == 0)
			return null;

		String nameTableFK = attr.getStrTableFK();
		Database database = this.getDatabase();

		Table tableFK = database.factoryTable(nameTableFK);
		tableFK.setAttrValue(ID_UNIVERSAL, p_id);
		if (tableFK.select()) {

			Attribute attrNomeTabelaFK = tableFK.getAttribute(pNomeDoAtributoNaTabelaExtrangeira);
			if (attrNomeTabelaFK != null) {
				return attrNomeTabelaFK.getStrValue();
			} else
				return null;
		} else
			return null;
	}

	public String getNomeDoIdentificadorDaTabela(String p_id, Table p_table) throws Exception {

		p_table.setAttrValue(ID_UNIVERSAL, p_id);
		p_table.select();
		return p_table.getAttribute(NOME_UNIVERSAL).getStrValue();
	}

	public void setDatabase(Database p_database) {
		database = p_database;
	}

	public ArrayList<String> getPhones() {
		ArrayList<String> phones = new ArrayList<String>();
		String phone = getStrValueOfAttribute(TELEFONE_UNIVERSAL);
		String phone1 = getStrValueOfAttribute(TELEFONE_1_UNIVERSAL);
		String phone2 = getStrValueOfAttribute(TELEFONE_2_UNIVERSAL);
		String celular = getStrValueOfAttribute(CELULAR_UNIVERSAL);

		String[] telefones = { phone, phone1, phone2, celular };

		for (String telefone : telefones) {
			if (telefone != null)
				phones.add(telefone);

		}

		return phones;

	}

	public String getName() {
		return this.name;
	}

	public Context getContext(){
		if(this.database == null) return null;
		else return this.database.getContext();
	}

	public Database getDatabase() {
		return this.database;
	}

	public void addAttribute(Attribute p_attribute) {
		if (p_attribute == null)
			return;
		else {
			String strNameAttribute = p_attribute.getName();
			if (!hashNameAttributeByAttribute.containsKey(strNameAttribute)) {
				hashNameAttributeByAttribute.put(strNameAttribute, p_attribute);
				listSortNameAttribute.add(strNameAttribute);
			}
		}
	}

	List<Attribute> listAttributeKey = null;

	public List<Attribute> getListAttributeKey() {
		if (listAttributeKey == null)
			listAttributeKey = new ArrayList<Attribute>();
		else
			return listAttributeKey;

		Iterator<Map.Entry<String, Attribute>> itr1 = hashNameAttributeByAttribute.entrySet().iterator();
		while (itr1.hasNext()) {
			Map.Entry<String, Attribute> entry = itr1.next();
			entry.getKey();

			Attribute attr = entry.getValue();
			if (attr.isPrimaryKey()) {
				listAttributeKey.add(attr);
			}
		}
		return listAttributeKey;
	}

	public Attribute getAttribute(String strNameAttribute) {
		if (hashNameAttributeByAttribute.containsKey(strNameAttribute)) {
			return hashNameAttributeByAttribute.get(strNameAttribute);
		} else {
			String strNameAttr = strNameAttribute.toLowerCase();

			Iterator<Map.Entry<String, Attribute>> itr1 = hashNameAttributeByAttribute.entrySet().iterator();

			while (itr1.hasNext()) {
				Map.Entry<String, Attribute> entry = itr1.next();
				String strNameElement = entry.getKey();

				if (strNameElement.toLowerCase().equals(strNameAttr)) {

					return entry.getValue();
				}
			}

			return null;
		}

	}

	public void setIntegerAttrValue(String p_strName, Integer p_strValue) {
		setAttrValue(p_strName, String.valueOf(p_strValue));
	}

	public void setLongAttrValue(String p_strName, Long p_strValue) {
		Attribute attr = this.getAttribute(p_strName);
		if (attr != null) {
			if (p_strValue != null)
				attr.setStrValue(String.valueOf(p_strValue));
			else
				attr.setStrValue(null);
		}
	}
	public void setIntAttrValue(String p_strName, int value) {
		Attribute attr = this.getAttribute(p_strName);
		if (attr != null) {
			attr.setStrValue(String.valueOf( value));
		}
	}

	public void clearAttrValue(String p_strName) {
		Attribute attr = this.getAttribute(p_strName);
		if (attr != null) {
			attr.setStrValue(null);
		}
	}
	public void setAttrValue(String p_strName, String p_strValue) {
		Attribute attr = this.getAttribute(p_strName);
		if (attr != null) {
			attr.setStrValue(p_strValue);
		}
	}

	public void setAttrValue(String p_strName, boolean p_strValue) {
		Attribute attr = this.getAttribute(p_strName);
		if (attr != null) {

			attr.setStrValue(p_strValue ? "1" : "0");
		}
	}

	public Integer remove(boolean pIsToInsertInSynchronizeTable) {

		try {
			String pId = this.getStrValueOfAttribute(ID_UNIVERSAL);
			if (OmegaConfiguration.DEBUGGING)
				database.log("remove 1. " + getName() + "::" + pId + ", sync: " + pIsToInsertInSynchronizeTable);

			return removeThrowException(pId, pIsToInsertInSynchronizeTable, true, false);
		} catch (Exception e) {
			return null;
		}
	}

	public Integer remove(String pId, boolean pIsToInsertInSynchronizeTable) {
		try {
			if (OmegaConfiguration.DEBUGGING)
				database.log("remove 2. " + getName() + "::" + pId + ", sync: " + pIsToInsertInSynchronizeTable);
			return removeThrowException(pId, pIsToInsertInSynchronizeTable, true, false);
		} catch (Exception ex) {
			return null;
		}

	}

	public int deleteAllTuples(boolean pIsToInsertInSynchronizeTable) throws Exception {
		if (pIsToInsertInSynchronizeTable) {

			throw new Exception("Nuo implementada");
		} else {
			return database.delete(this.getName(), "", new String[] {});
		}
	}

	public boolean removerTodasOsRegistrosMenoresOuIguais(String ultimoIdIncluso) throws Exception {

		// if (ultimoIdIncluso != null) {
		database.delete(this.getName(), "id <= ?", new String[] { ultimoIdIncluso });

		// } else {
		// database.delete(this.getName(), "",
		// new String[] {});
		// }

		return true;

	}

	public Integer removeThrowException(String pId, boolean pIsToInsertInSynchronizeTable, boolean imprimirErro,
										boolean lancarExcecao) throws Exception {

		if (pId == null || pId.length() == 0)
			return null;
		else {
			int numberRowsAffected = 0;
			boolean beginTransaction = false;
			boolean successfull = true;

			try {
				if (OmegaConfiguration.DEBUGGING)
					database.log("removeThrowException. " + getName() + "::" + pId + ", sync: "
							+ pIsToInsertInSynchronizeTable);
				boolean hasToSync = DatabaseConfiguration.IS_SYNCHRONIZE_ON
						&& !this.getName().equals(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME)
						&& pIsToInsertInSynchronizeTable;
				if (hasToSync && !database.isTransactionStarted()) {
					beginTransaction = database.beginTransactionSyncDefaultTime();
					if (!beginTransaction)
						throw new OmegaDatabaseException(OmegaDatabaseException.CODE.BEGIN_TRANSACTION_TIMEOUT, null);
				}

				numberRowsAffected = database.delete(this.getName(), "id = ?", new String[] { pId });
				if (numberRowsAffected > 0 && hasToSync) {
					insertTableSynchronize(this.getName(), pId.toString(),
							EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE);
				}
			} catch (Exception ex) {
				SingletonLog.insereErro(ex, SingletonLog.TIPO.BANCO);
				successfull = false;
				if (lancarExcecao)
					throw ex;
				else
					return null;
			} finally {
				if (beginTransaction)
					database.endTransaction(successfull);
			}

			return numberRowsAffected;
		}

	}

	public boolean isEmpty() {

		Cursor cursor = database.query(getName(), new String[] { ID_UNIVERSAL }, null, null, "", "", null, "1");
		boolean validade = true;
		try {

			if (cursor.moveToFirst()) {

				do {
					validade = false;
					break;

				} while (cursor.moveToNext());
			}
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return validade;
	}

	public boolean select(String pId) throws Exception {
		if (pId == null)
			return false;
		this.setAttrValue(ID_UNIVERSAL, pId);
		if(! select()){
			this.clearAttrValue(ID_UNIVERSAL);
			return false;
		} else
			return true;
	}

	// Select first tuple of result, and load all data into this object
	public boolean select() throws Exception {

		if (OmegaConfiguration.TODAS_AS_TABELAS_POSSUI_ID_COMO_CHAVE) {

			int index = 0;

			String id = getId();
			if (id == null || id.length() == 0)
				return false;
			Cursor cursor = database.query(getName(), null, "id = ?", new String[] { id }, null, "", "", "1");

			boolean validadeConsulta = false;
			try {
				clearData();
				if (cursor.moveToFirst()) {

					Attribute[] attrs = getAttributesFromCursor(cursor);
					boolean validade = true;
					if (attrs == null || attrs.length == 0) {
						SingletonLog.insereErro(new Exception("Consulta sem atributos mapeados"),
								SingletonLog.TIPO.BANCO);

					}
					if (validade) {

						do {
							validadeConsulta = true;
							index = 0;
							for (Attribute attr : attrs) {

								String strValue = null;
								if(attr == null) continue;
								switch (attr.getSQLType()) {
									case FLOAT:
									case DOUBLE:
										double value = cursor.getDouble(index);
										strValue = String.valueOf(value);
										break;
									case BLOB:
										throw new Exception("Mapear BLOB");
									default:
										strValue = cursor.getString(index);
										break;
								}

								attr.setStrValue(strValue);
								index += 1;
							}
						} while (cursor.moveToNext());
					}

				}
			} finally {
				if (cursor != null) {
					cursor.close();
				}
			}

			return validadeConsulta;
		} else {

			List<Attribute> vetorAttrKey = this.getListAttributeKey();
			ArrayList<String> vListArg = new ArrayList<String>();

			int index = 0;
			for (Attribute attr : vetorAttrKey) {
				String vStrValue = attr.getStrValue();
				if (HelperString.checkIfIsNull(vStrValue) != null) {
					vListArg.add(vStrValue);
					index += 1;
				}
			}
			String[] vetorArg = new String[vListArg.size()];
			vListArg.toArray(vetorArg);
			String querSelect = this.getStrQueryWhereDoSelect(false, vetorAttrKey);

			Cursor cursor = database.query(getName(), null, querSelect, vetorArg, null, "", "", "1");

			boolean validadeConsulta = false;
			try {
				clearData();
				if (cursor.moveToFirst()) {

					Attribute[] attrs = getAttributesFromCursor(cursor);
					boolean validade = true;
					if (attrs == null || attrs.length == 0) {
						SingletonLog.insereErro(new Exception("Consulta sem atributos mapeados"),
								SingletonLog.TIPO.BANCO);

					}
					if (validade) {

						do {
							validadeConsulta = true;
							index = 0;
							for (Attribute attr : attrs) {
								if (attr == null)
									continue;
								String strValue = null;

								switch (attr.getSQLType()) {
									case FLOAT:
									case DOUBLE:
										double value = cursor.getDouble(index);
										strValue = String.valueOf(value);
										break;
									case BLOB:
										throw new Exception("Mapear BLOB");
									default:
										strValue = cursor.getString(index);
										break;
								}

								attr.setStrValue(strValue);
								index += 1;
							}
						} while (cursor.moveToNext());
					}

				}
			} finally {
				if (cursor != null) {
					cursor.close();
				}
			}

			return validadeConsulta;
		}

	}

	public String[] getVetorValueOfAttributeWithValue() {
		ArrayList<String> vListValue = getListValueOfAttributeWithValue();
		if (vListValue.size() == 0)
			return null;
		String vVetor[] = new String[vListValue.size()];
		return vListValue.toArray(vVetor);
	}

	public ArrayList<String> getListValueOfAttributeWithValue() {
		Enumeration<String> listNameStrAttr = hashNameAttributeByAttribute.keys();

		ArrayList<String> vListContent = new ArrayList<String>();
		while (listNameStrAttr.hasMoreElements()) {
			String strNameElement = listNameStrAttr.nextElement();
			Attribute attr = hashNameAttributeByAttribute.get(strNameElement);
			if (!attr.isEmpty()) {
				vListContent.add(attr.getStrValue());

			}
		}

		return vListContent;
	}

	public ArrayList<Attribute> getListAttributeWithStrValue() {

		Enumeration<String> listNameStrAttr = hashNameAttributeByAttribute.keys();
		ArrayList<Attribute> listReturn = new ArrayList<Attribute>();

		while (listNameStrAttr.hasMoreElements()) {
			String strNameElement = listNameStrAttr.nextElement();
			Attribute attr = hashNameAttributeByAttribute.get(strNameElement);
			if (!attr.isEmpty()) {
				listReturn.add(attr);
			}
		}
		return listReturn;
	}

	public Table getFirstTupla() {
		return getFirstTupla(null, true);
	}

	public Table getFirstTupla(String p_vectorOrderAttrNameToOrderBy[], boolean pIsToSetIdCorporacao) {
		ArrayList<Table> vListTupla = getListTable(p_vectorOrderAttrNameToOrderBy, " 0, 1 ", pIsToSetIdCorporacao,
				true);
		if (vListTupla != null && vListTupla.size() > 0) {
			Table vTupla = vListTupla.get(0);
			return vTupla;
		}
		return null;
	}

	private String getOrderBy(String p_vectorOrderAttrNameToOrderBy[]) {
		return getOrderBy(p_vectorOrderAttrNameToOrderBy, true);
	}

	private String getOrderBy(String p_vectorOrderAttrNameToOrderBy[], boolean orderByAsc) {
		if (p_vectorOrderAttrNameToOrderBy == null) {
			return "";
		} else if (p_vectorOrderAttrNameToOrderBy.length == 0) {
			return "";
		} else {
			String clause = "";
			for (String attr : p_vectorOrderAttrNameToOrderBy) {
				if (clause.length() == 0) {
					clause += attr + " ";
				} else {
					clause += ", " + attr + " ";
				}
			}
			if (!orderByAsc) {
				clause += " DESC ";
			}
			return clause;
		}
	}

	public String getStrQueryRemoveSQL() {
		String id = getStrValueOfAttribute(ID_UNIVERSAL);
		if (id == null)
			return null;

		String sql = " DELETE FROM " + this.name + " WHERE id = '" + id + "' ";

		return sql;
	}
//
//	public String[] getValores() {
//		String id = getStrValueOfAttribute(ID_UNIVERSAL);
//		if (id == null)
//			return null;
//
//		String[] valores = new String[this.listSortNameAttribute.size()];
//		int i = 0;
//		for (String attrName : this.listSortNameAttribute) {
//			if (attrName.equalsIgnoreCase(Table.SENHA_UNIVERSAL))
//				continue;
//			String value = getStrValueOfAttribute(attrName);
//			valores[i++] = value;
//		}
//		return valores;
//	}

	public String[] getValoresMenosId() {
		String id = getStrValueOfAttribute(ID_UNIVERSAL);
		if (id == null)
			return null;
		List<String> valores = new ArrayList<String>(this.listSortNameAttribute.size() - 1);
		//String[] valores = new String[this.listSortNameAttribute.size() - 1];
		int i = 0;
		for (String attrName : this.listSortNameAttribute) {
			if (attrName.equalsIgnoreCase(Table.ID_UNIVERSAL)
					|| attrName.equalsIgnoreCase(Table.SENHA_UNIVERSAL))
				continue;
			String value = getStrValueOfAttribute(attrName);
			valores.add(value);
		}
		String[] ret = new String[valores.size()];
		valores.toArray(ret);
		return ret;
	}

//	public String getStrSetOfQueryUpdateSQL() {
//		String id = getStrValueOfAttribute(ID_UNIVERSAL);
//		if (id == null)
//			return null;
//		String upd = "";
//		for (String attrName : this.listSortNameAttribute) {
//			if (attrName.equalsIgnoreCase(Table.SENHA_UNIVERSAL))
//				continue;
//			String vValue = getStrValueOfAttribute(attrName);
//			if (vValue != null) {
//				if (upd.length() == 0)
//					upd += attrName + " = " + formatarCampoTextoParaSQL(vValue);
//				else
//					upd += ", " + attrName + " = " + formatarCampoTextoParaSQL(vValue);
//
//			}
//		}
//
//		String sql = upd;
//
//		return sql;
//	}

	public String getStrQueryUpdateSQL() {
		String id = getStrValueOfAttribute(ID_UNIVERSAL);
		if (id == null)
			return null;
		String upd = "";
		for (String attrName : this.listSortNameAttribute) {
			if (attrName.equalsIgnoreCase(Table.SENHA_UNIVERSAL))
				continue;
			String vValue = getStrValueOfAttribute(attrName);
			if (vValue != null) {
				if (upd.length() == 0)
					upd += attrName + " = " + formatarCampoTextoParaSQL(vValue);
				else
					upd += ", " + attrName + " = " + formatarCampoTextoParaSQL(vValue);

			}
		}

		String sql = " UPDATE " + this.name + " SET " + upd + " WHERE id = '" + id + "' ";

		return sql;
	}

	public void insereErroForeignKeyMismatch(Exception ex, SingletonLog.TIPO pTipoLogErro) {
		try {

			String id = this.getStrValueOfAttribute(ID_UNIVERSAL);

			if (id != null && id.length() > 0) {
				// boolean registroExiste = false;
				// if(obj.select(id)){
				// registroExiste = true;
				// }
				String obs = "Erro na tabela " + getName() + ". O registro que sofreu a busca de id " + id;

				// if (!registroExiste)
				// obs += " nuo existe na base";

				SingletonLog.insereErro(ex, pTipoLogErro, obs);
			} else {
				SingletonLog.insereErro(ex, pTipoLogErro);
			}
		} catch (Exception ex1) {
			SingletonLog.insereErro(ex1, pTipoLogErro);
		}
	}

	public boolean updateDoSincronizadorEspelho(OmegaLog log) {
		boolean validade = false;
		try {
			validade = updateThrowsException(false);

		} catch (Exception ex) {

			String msg = "Falha durante a atualizacao do registro proveniente do sincronizador: "
					+ getIdentificadorRegistro() + " Excecao: " + ex.getMessage()
					+ ". ISSO NuO OCASIONOU A PARADA DA SINCRONIZAuuO POIS ESTAMOS NO MuTODO updateAForca.";
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog(msg);
			SingletonLog.insereErro(new Exception(msg), SingletonLog.TIPO.SINCRONIZADOR);
		}
		return validade;
	}

	public boolean updateThrowsException(boolean pIsToInsertInTableSincronizador) throws Exception {

		String id = getStrValueOfAttribute(ID_UNIVERSAL);
		String tableName = this.getName();
		// Atualiza todos os atributos da tabela
		ContentValues content = new ContentValues();
		for (String attrName : this.listSortNameAttribute) {
			if (attrName.equalsIgnoreCase(ID_UNIVERSAL))
				continue;
			else {
				String vValue = getStrValueOfAttribute(attrName);

				if (vValue == null)
					content.putNull(attrName);
				else
					content.put(attrName, vValue);
			}
		}

		if (id == null)
			return false;

		OpenHelper oh = null;
		Database database = this.getDatabase();

		if (database == null)
			return false;
		else {
			oh = database.getOpenHelper();
			if (oh == null)
				return false;
			else {

				int numberRowsAffected = 0;
				boolean successfull = true;
				boolean beginTransaction = false;
				try {
					boolean validadeSinc = pIsToInsertInTableSincronizador && DatabaseConfiguration.IS_SYNCHRONIZE_ON
							&& !this.getName().equals(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME);
					String json = null;
					if (validadeSinc) {
						Table obj = factory();
						obj.select(this.getId());
						json = obj.getJsonConteudo();

						if (!database.isTransactionStarted()) {
							beginTransaction = database.beginTransactionSyncDefaultTime();
							if (!beginTransaction)
								throw new OmegaDatabaseException(OmegaDatabaseException.CODE.BEGIN_TRANSACTION_TIMEOUT,
										null);
						}
					}

					numberRowsAffected = database.update(tableName, content, " id= ?", new String[] { id });
					// insere na tabela sincronizador
					if (numberRowsAffected > 0 && validadeSinc) {
						Long idSinc = insertTableSynchronize(this.getName(), id.toString(),
								EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_EDIT);
						if (json != null && idSinc != null)
							insereCrudHistoricoSincronizadorParaEdicao(json, idSinc, getId());
					}
					if (numberRowsAffected > 0)
						return true;
					else
						return false;
				} catch (Exception ex) {
					SingletonLog.insereErro(ex, SingletonLog.TIPO.BANCO);
					successfull = false;
					throw ex;
				} finally {
					if (beginTransaction)
						database.endTransaction(successfull);
				}

			}

		}
	}

	public boolean update(boolean pIsToInsertInTableSincronizador, boolean imprimirErro) {
		try {
			return updateThrowsException(pIsToInsertInTableSincronizador);
		} catch (Exception ex) {
			if (imprimirErro)
				insereErroForeignKeyMismatch(ex, SingletonLog.TIPO.BANCO);
			return false;
		}
	}

	public boolean update(boolean pIsToInsertInTableSincronizador) {
		try {
			return updateThrowsException(pIsToInsertInTableSincronizador);
		} catch (Exception ex) {
			insereErroForeignKeyMismatch(ex, SingletonLog.TIPO.BANCO);
			return false;
		}
	}

	public void updateSequence(long seq) throws Exception {

		Database db = getDatabase();
		db.updateSqliteSequence(getName(), seq);
	}

	public boolean updateId(String newId, OmegaLog log) {
		String oldId = getStrValueOfAttribute(ID_UNIVERSAL);
		if (oldId != null && newId != null && oldId.equalsIgnoreCase(newId))
			return true;
		this.setAttrValue(ID_UNIVERSAL, newId);

		// Atualiza todos os atributos da tabela
		if (oldId == null)
			return false;
		else {
			Database database = this.getDatabase();
			if (database == null)
				return false;
			else {
				ConflictUpdateId con = database.updateId(this.getName(), oldId, newId, log);
				if (con.numberRowsAffected <= 0)
					return false;
				else {
					return true;

				}
			}
		}
	}

	public String getStrQueryInsertSQL() {
		String vCabecalho = getStrCabecalhoInsert();
		String vCorpoInsert = getStrCorpoInsert();
		if (vCabecalho != null && vCorpoInsert != null && vCabecalho.length() > 0 && vCorpoInsert.length() > 0)
			return vCabecalho + "(" + vCorpoInsert + ")";
		return null;
	}

	StringBuilder corpoSB = null;

	public StringBuilder getCorpoSB() {
		if (corpoSB == null)
			corpoSB = new StringBuilder();
		else
			corpoSB.setLength(0);
		return corpoSB;
	}

	public String getStrCabecalhoInsert() {

		StringBuilder corpoSB = getCorpoSB();
		for (String vNameAttr : this.listSortNameAttribute) {
			Attribute vAttr = this.getAttribute(vNameAttr);
			if (vAttr == null)
				continue;
			else if (vAttr.isAutoIncrement() && vAttr.isPrimaryKey())
				continue;
			if (corpoSB.length() == 0)
				corpoSB.append(" `" + vNameAttr + "` ");
			else
				corpoSB.append(", `" + vNameAttr + "` ");

		}
		if (corpoSB.length() > 0)
			return "INSERT INTO " + getName() + " (" + corpoSB.toString() + ") VALUES";
		else
			return null;
	}

	public static String formatarCampoTextoParaSQL(String pTexto) {
		if (pTexto == null)
			return "null";
		else if (pTexto.length() == 0)
			return "";
		else {
			String strConteudoIdentado = pTexto.replace("'", "\'");
			return " '" + strConteudoIdentado + "' ";
		}
	}

	public String getStrCorpoInsert() {
		StringBuilder corpoSB = getCorpoSB();
		for (String vNameAttr : this.listSortNameAttribute) {
			Attribute attr = this.getAttribute(vNameAttr);
			if (attr == null)
				continue;
			else if (attr.isAutoIncrement() && attr.isPrimaryKey())
				continue;
			String strValue = attr.getStrValue();
			if (corpoSB.length() == 0)
				corpoSB.append(" " + formatarCampoTextoParaSQL(strValue));
			else
				corpoSB.append(", " + formatarCampoTextoParaSQL(strValue));

		}
		if (corpoSB.length() > 0)
			return "(" + corpoSB.toString() + ") ";
		else
			return null;
	}

	public ArrayList<Integer> getAllListId() {
		return getAllListId(null);
	}

	public ArrayList<Integer> getAllListId(int limitInferior, int qtd) {
		return getAllListId(" LIMIT " + String.valueOf(limitInferior) + ", " + String.valueOf(qtd));
	}

	private ArrayList<Integer> getAllListId(String limit) {
		Cursor cursor = null;
		ArrayList<Integer> ids = null;
		try {
			cursor = database.query(getName(), new String[] { ID_UNIVERSAL }, null, null, null, "", ID_UNIVERSAL,
					" LIMIT 0, " + limit);

			ids = new ArrayList<Integer>();

			if (cursor.moveToFirst()) {

				do {

					String value = cursor.getString(0);
					Integer index = HelperInteger.parserInt(value);
					if (index != null)
						ids.add(index);
				} while (cursor.moveToNext());
			}

			return ids;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public String getCorporacaoSeExitente() {
		Attribute vAttr = getAttribute(CORPORACAO_ID_INT);
		String corporacao = "";
		if (vAttr != null)
			corporacao = vAttr.getStrValue();
		corporacao = (corporacao != null && corporacao.length() > 0) ? corporacao : null;
		return corporacao;
	}

	public Cursor getCursorListId() {

		Cursor cursor = null;

		String corporacao = getCorporacaoSeExitente();
		String where = null;
		String args[] = null;
		if (corporacao != null) {
			where = CORPORACAO_ID_INT + " = ? ";
			args = new String[] { corporacao };
		}
		cursor = database.query(getName(), new String[] { ID_UNIVERSAL }, where, args, null, "", ID_UNIVERSAL);

		return cursor;

	}

	public ArrayList<Table> getListTable() {
		return getListTable(null, true);
	}

	public ArrayList<Table> getListTable(String p_vectorOrderAttrNameToOrderBy[]) {
		return getListTable(p_vectorOrderAttrNameToOrderBy, true);
	}

	public boolean hasTupla() {
		Cursor cursor = null;
		try {

			String tableName = getName();

			ArrayList<String> listArgs = new ArrayList<String>();
			ArrayList<Attribute> vetorAttributeWithValue = this.getListAttributeWithStrValue();

			for (Attribute attr : vetorAttributeWithValue) {

				String vStrValue = attr.getStrValue();
				if (HelperString.checkIfIsNull(vStrValue) != null) {
					listArgs.add(vStrValue);
				}
			}

			String where = this.getWhere(vetorAttributeWithValue);
			String query = "SELECT id FROM " + tableName;

			String[] vetorArg = new String[listArgs.size()];
			listArgs.toArray(vetorArg);

			if (where != null && where.length() > 0)
				query += " WHERE " + where;
			query += " LIMIT 0,1 ";

			if (listArgs.size() > 0)
				cursor = database.rawQuery(query, vetorArg);
			else
				cursor = database.rawQuery(query, null);

			String vId = null;

			if (cursor.moveToFirst()) {

				do {
					vId = cursor.getString(0);
					break;
				} while (cursor.moveToNext());
			}

			return HelperInteger.parserInteger(vId) != null ? true : false;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public String getId() {
		return this.getStrValueOfAttribute(ID_UNIVERSAL);
	}

	public Integer getIntegerId() {
		return this.getIntegerValueOfAttribute(ID_UNIVERSAL);
	}

	public Long getLongId() {
		return this.getLongValueOfAttribute(ID_UNIVERSAL);
	}

	public Integer getIdAleatorio() {
		if (getName().compareTo(CORPORACAO_ID_INT) == 0)
			return HelperInteger.parserInteger(OmegaSecurity.getIdCorporacao());
		Table aux = this.factory();
		aux.formatToSQLite();

		// Seta a corporacao se necessario, contando apenas o numero de tuplas
		// pertinente a corporacao
		Integer total = aux.countTupla();
		if (total == null || total <= 0)
			return null;
		else {
			int indice = 0;
			if (total > 1) {
				indice = HelperInteger.getRandom(total);
			}
			Integer ind = aux.getIdNoIndice(indice);
			if (ind == null && total > 0) {
				total = aux.countTupla();
				ind = aux.getIdNoIndice(indice);
			}
			return ind;
		}

	}

	// Conta a partir de 0
	public Integer getIdNoIndice(int indice) {
		Cursor cursor = null;
		try {

			ArrayList<String> vListArg = new ArrayList<String>();
			ArrayList<Attribute> vetorAttributeWithStrValue = this.getListAttributeWithStrValue();

			for (Attribute attr : vetorAttributeWithStrValue) {

				String vStrValue = attr.getStrValue();
				if (HelperString.checkIfIsNull(vStrValue) != null) {
					vListArg.add(vStrValue);
				}
			}

			String[] vetorArg = new String[vListArg.size()];
			vListArg.toArray(vetorArg);
			String vQueryWhere = this.getWhere(vetorAttributeWithStrValue);

			cursor = database.query(getName(), new String[] { ID_UNIVERSAL }, vQueryWhere, vetorArg, null, null, null,
					String.valueOf(indice) + ", 1");

			if (cursor.moveToFirst()) {

				do {
					String value = cursor.getString(0);
					return HelperInteger.parserInteger(value);

				} while (cursor.moveToNext());
			}

			return null;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public Integer countTupla() {
		Cursor cursor = null;
		try {

			String vStrTableName = getName();

			ArrayList<String> vListArg = new ArrayList<String>();
			ArrayList<Attribute> vetorAttributeWithStrValue = this.getListAttributeWithStrValue();

			for (Attribute attr : vetorAttributeWithStrValue) {

				String vStrValue = attr.getStrValue();
				if (HelperString.checkIfIsNull(vStrValue) != null) {
					vListArg.add(vStrValue);
				}
			}

			String vQueryWhere = this.getWhere(vetorAttributeWithStrValue);
			String vQuery = "SELECT COUNT(" + ID_UNIVERSAL + ") FROM " + vStrTableName;

			String[] vetorArg = new String[vListArg.size()];
			vListArg.toArray(vetorArg);

			if (vQueryWhere != null && vQueryWhere.length() > 0)
				vQuery += " WHERE " + vQueryWhere;
			if (vListArg.size() > 0)
				cursor = database.rawQuery(vQuery, vetorArg);
			else
				cursor = database.rawQuery(vQuery, null);

			String vId = null;
			try {
				if (cursor.moveToFirst()) {

					do {
						vId = cursor.getString(0);
						break;
					} while (cursor.moveToNext());
				}
			} catch (Exception ex) {
				SingletonLog.insereErro(ex, SingletonLog.TIPO.BANCO);

			}

			return HelperInteger.parserInteger(vId);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public Attribute[] getAttributesFromCursor(Cursor c) {
		String[] vetor = c.getColumnNames();
		Attribute[] attrs = new Attribute[vetor.length];
		int i = 0;
		for (String nomeAttr : vetor) {
			attrs[i++] = getAttribute(nomeAttr);
		}
		return attrs;
	}

	public ArrayList<Table> getListTable(String atributosOrderBy[], boolean setarACorporacaoSeExistente) {
		Cursor cursor = null;

		try {
			if (setarACorporacaoSeExistente)
				setIdAttrCorporacaoIfExists();
			ArrayList<Attribute> vetorAttributeWithStrValue = this.getListAttributeWithStrValue();
			boolean validadeArg = false;
			ArrayList<String> listArg = new ArrayList<String>();

			for (Attribute attr : vetorAttributeWithStrValue) {
				// vetorArg[index] = vetorChave[index] + "=" +
				// p_tupleKey.getStrValue(strKey);
				if (!validadeArg) {
					validadeArg = true;
				}
				String valor = attr.getStrValue();
				if (HelperString.checkIfIsNull(valor) != null) {
					listArg.add(valor);
				}
			}
			String[] vetorArg = new String[listArg.size()];
			listArg.toArray(vetorArg);

			String querWhere = null;

			if (!validadeArg) {
				cursor = database.query(getName(), null, null, null, null, "", this.getOrderBy(atributosOrderBy));
			} else {
				querWhere = this.getWhere(vetorAttributeWithStrValue);
				String orderBy = this.getOrderBy(atributosOrderBy);
				cursor = database.query(getName(), null, querWhere, vetorArg, null, "", orderBy);
			}

			ArrayList<Table> listEXTDAO = new ArrayList<Table>();
			try {
				if (cursor.moveToFirst()) {
					Attribute[] attrs = getAttributesFromCursor(cursor);
					boolean validade = true;
					if (attrs == null || attrs.length == 0) {
						SingletonLog.insereErro(new Exception("Consulta sem atributos mapeados"),
								SingletonLog.TIPO.BANCO);

					}
					if (validade) {
						do {
							Table table = this.factory();
							boolean validade2 = false;
							for (int i = 0; i < attrs.length; i++) {
								if (attrs[i] == null)
									continue;
								String nome = attrs[i].getName();

								String valor = cursor.getString(i);
								table.setAttrValue(nome, valor);
								validade2 = true;
							}
							if (validade2) {
								listEXTDAO.add(table);
							}
							if (cursor.isLast())
								break;
						} while (cursor.moveToNext());
					}
				}
			} catch (Exception ex) {
				SingletonLog.insereErro(ex, SingletonLog.TIPO.BANCO);
			}

			return listEXTDAO;
		} finally {
			if (cursor != null) {

				cursor.close();
			}
		}
	}

	public ArrayList<Table> getListTableComIdMenoOuIgualrQue(String atributosOrderBy[],
															 boolean setarACorporacaoSeExistente, String idLimiteIncluso) {
		Cursor cursor = null;

		try {
			if (setarACorporacaoSeExistente)
				setIdAttrCorporacaoIfExists();
			ArrayList<Attribute> vetorAttributeWithStrValue = this.getListAttributeWithStrValue();
			boolean validadeArg = false;
			ArrayList<String> listArg = new ArrayList<String>();

			for (Attribute attr : vetorAttributeWithStrValue) {
				// vetorArg[index] = vetorChave[index] + "=" +
				// p_tupleKey.getStrValue(strKey);
				if (!validadeArg) {
					validadeArg = true;
				}
				String valor = attr.getStrValue();
				if (HelperString.checkIfIsNull(valor) != null) {
					listArg.add(valor);
				}
			}
			String appendWhere = "";
			if (idLimiteIncluso != null) {

				if (listArg.size() > 0) {
					appendWhere += " AND ";
				}
				appendWhere += " id <= ? ";
				listArg.add(idLimiteIncluso);
			}

			String[] vetorArg = new String[listArg.size()];
			listArg.toArray(vetorArg);

			String queryWhere = null;

			queryWhere = this.getWhere(vetorAttributeWithStrValue);
			if (appendWhere.length() > 0) {
				if (queryWhere != null && queryWhere.length() > 0)
					queryWhere += appendWhere;
				else
					queryWhere = appendWhere;
			}

			String orderBy = this.getOrderBy(atributosOrderBy);
			if (queryWhere != null && queryWhere.length() > 0)
				cursor = database.query(getName(), null, queryWhere, vetorArg, null, "", orderBy);
			else
				cursor = database.query(getName(), null, null, null, null, "", orderBy);
			ArrayList<Table> listEXTDAO = new ArrayList<Table>();

			if (cursor.moveToFirst()) {
				Attribute[] attrs = getAttributesFromCursor(cursor);
				boolean validade = true;
				if (attrs == null || attrs.length == 0) {
					SingletonLog.insereErro(new Exception("Consulta sem atributos mapeados"), SingletonLog.TIPO.BANCO);

				}
				if (validade) {
					do {
						Table table = this.factory();
						boolean validade2 = false;
						for (int i = 0; i < attrs.length; i++) {
							if (attrs[i] == null)
								continue;
							String nome = attrs[i].getName();

							String valor = cursor.getString(i);
							table.setAttrValue(nome, valor);
							validade2 = true;
						}
						if (validade2) {
							listEXTDAO.add(table);
						}
						if (cursor.isLast())
							break;
					} while (cursor.moveToNext());
				}
			}

			return listEXTDAO;
		} finally {
			if (cursor != null) {

				cursor.close();
			}
		}

	}

	/* A consulta deve retornar somente os atributos da tabela */
	public ArrayList<Table> getListTable(String query, String args[]) {
		Cursor cursor = null;
		try {
			cursor = database.rawQuery(query, args);

			ArrayList<Table> listEXTDAO = new ArrayList<Table>();

			if (cursor.moveToFirst()) {

				do {

					Table table = this.factory();

					boolean validade = false;
					for (int i = 0; i < listSortNameAttribute.size(); i++) {
						String strNameAttr = listSortNameAttribute.get(i);
						String strValue = cursor.getString(i);
						table.setAttrValue(strNameAttr, strValue);
						validade = true;
					}
					if (validade) {
						listEXTDAO.add(table);
					}
				} while (cursor.moveToNext());
			}
			return listEXTDAO;
		} finally {
			if (cursor != null) {

				cursor.close();
			}
		}
	}

	public Table[] getVetorTable(String p_vectorOrderAttrNameToOrderBy[], String pLimit, boolean pIsToSetIdCorporacao) {
		ArrayList<Table> list = getListTable(p_vectorOrderAttrNameToOrderBy, pLimit, pIsToSetIdCorporacao);
		if (list == null)
			return null;
		Table[] vetor = new Table[list.size()];
		for (int i = 0; i < vetor.length; i++) {
			vetor[i] = list.get(i);
		}
		return vetor;
	}

	public ArrayList<Table> getListTable(String p_vectorOrderAttrNameToOrderBy[], String pLimit,
										 boolean pIsToSetIdCorporacao) {
		return getListTable(p_vectorOrderAttrNameToOrderBy, pLimit, pIsToSetIdCorporacao, true);
	}

	public ArrayList<Table> getListTable(String p_vectorOrderAttrNameToOrderBy[], String pLimit,
										 boolean pIsToSetIdCorporacao, boolean orderByAsc) {
		Cursor cursor = null;
		try {
			if (pIsToSetIdCorporacao) {
				setIdAttrCorporacaoIfExists();
			}
			ArrayList<Attribute> vetorAttributeWithStrValue = this.getListAttributeWithStrValue();
			boolean validadeArg = false;
			ArrayList<String> vListArg = new ArrayList<String>();

			for (Attribute attr : vetorAttributeWithStrValue) {

				// vetorArg[index] = vetorChave[index] + "=" +
				// p_tupleKey.getStrValue(strKey);
				if (!validadeArg) {
					validadeArg = true;
				}
				String vStrValue = attr.getStrValue();
				if (HelperString.checkIfIsNull(vStrValue) != null) {
					vListArg.add(vStrValue);
				}
			}
			String[] vetorArg = new String[vListArg.size()];
			vListArg.toArray(vetorArg);

			String querWhere = null;

			if (!validadeArg) {

				cursor = database.query(getName(), null, null, null, null, "",
						this.getOrderBy(p_vectorOrderAttrNameToOrderBy, orderByAsc), pLimit);
			} else {
				querWhere = this.getWhere(vetorAttributeWithStrValue);
				String vOrderBy = this.getOrderBy(p_vectorOrderAttrNameToOrderBy, orderByAsc);

				cursor = database.query(getName(), null, querWhere, vetorArg, null, "", vOrderBy, pLimit);
			}

			ArrayList<Table> listEXTDAO = new ArrayList<Table>();

			if (cursor.moveToFirst()) {
				Attribute[] attrs = getAttributesFromCursor(cursor);
				boolean validade = true;
				if (attrs == null || attrs.length == 0) {
					SingletonLog.insereErro(new Exception("Consulta sem atributos mapeados"), SingletonLog.TIPO.BANCO);

				}
				if (validade) {
					do {

						Table table = this.factory();

						boolean validade2 = false;
						for (int i = 0; i < attrs.length; i++) {
							if (attrs[i] == null)
								continue;
							String strNameAttr = attrs[i].getName();
							String strValue = cursor.getString(i);
							table.setAttrValue(strNameAttr, strValue);
							validade2 = true;
						}
						if (validade2) {
							listEXTDAO.add(table);
						}

						if (cursor.isLast())
							break;
					} while (cursor.moveToNext());
				}
			}

			return listEXTDAO;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

	}

	public boolean selectFirstTupla() throws Exception {
		return selectFirstTupla(null, false, true);
	}

	public boolean selectFirstTupla(String attrsOrderBy[], boolean setIdCorporacao, boolean orderByAsc)
			throws Exception {
		Cursor cursor = null;
		try {

			if (setIdCorporacao) {
				setIdAttrCorporacaoIfExists();
			}

			Enumeration<String> listNameStrAttr = hashNameAttributeByAttribute.keys();

			ArrayList<String> args = new ArrayList<String>();
			StringBuilder where = new StringBuilder();
			while (listNameStrAttr.hasMoreElements()) {
				String strNameElement = listNameStrAttr.nextElement();
				Attribute attr = hashNameAttributeByAttribute.get(strNameElement);
				if (!attr.isEmpty()) {
					String vStrValue = attr.getStrValue();
					if (where.length() > 0) {
						where.append(" AND ");
					}
					if (HelperString.checkIfIsNull(vStrValue) != null) {
						args.add(vStrValue);
						Attribute.SQLLITE_TYPE sqlType = attr.getSQLType();
						if (isConsultaLike(sqlType))
							where.append(attr.getName() + " LIKE ? ");
						else
							where.append(attr.getName() + " = ? ");
					} else {
						where.append(attr.getName() + "  IS NULL ");
					}
				}
			}

			String sqlWhere = null;
			if (where.length() > 0)
				sqlWhere = where.toString();

			String sqlOrderBy = this.getOrderBy(attrsOrderBy, orderByAsc);
			if (args.size() == 0) {
				cursor = database.query(getName(), null, sqlWhere, null, null, "", sqlOrderBy, "0, 1");
			} else {
				String[] vetorArg = new String[args.size()];
				args.toArray(vetorArg);

				cursor = database.query(getName(), null, sqlWhere, vetorArg, null, "", sqlOrderBy, "0, 1");
			}

			if (cursor.moveToFirst()) {
				Attribute[] attrs = getAttributesFromCursor(cursor);
				boolean validade = true;
				if (attrs == null || attrs.length == 0) {
					SingletonLog.insereErro(new Exception("Consulta sem atributos mapeados"), SingletonLog.TIPO.BANCO);

				}
				if (validade) {
					for (int i = 0; i < attrs.length; i++) {
						if (attrs[i] == null)
							continue;
						String strValue = cursor.getString(i);
						attrs[i].setStrValue(strValue);
					}
				}
				return validade;
			}
			return false;

		} finally {
			if (cursor != null) {

				cursor.close();
			}
		}
	}

	public Table getFirstTupla(String p_vectorOrderAttrNameToOrderBy[], String pLimit) {
		Cursor cursor = null;
		try {
			ArrayList<Attribute> vetorAttributeWithValue = this.getListAttributeWithStrValue();
			boolean validadeArg = false;
			ArrayList<String> listArg = new ArrayList<String>();

			for (Attribute attr : vetorAttributeWithValue) {

				// vetorArg[index] = vetorChave[index] + "=" +
				// p_tupleKey.getStrValue(strKey);
				if (!validadeArg) {
					validadeArg = true;
				}
				String vStrValue = attr.getStrValue();
				if (HelperString.checkIfIsNull(vStrValue) != null) {
					listArg.add(vStrValue);
				}
			}
			String[] vetorArg = new String[listArg.size()];
			listArg.toArray(vetorArg);

			String querWhere = null;

			if (!validadeArg) {

				cursor = database.query(getName(), null, null, null, null, "",
						this.getOrderBy(p_vectorOrderAttrNameToOrderBy), pLimit);
			} else {
				querWhere = this.getWhere(vetorAttributeWithValue);
				String vOrderBy = this.getOrderBy(p_vectorOrderAttrNameToOrderBy);
				cursor = database.query(getName(), null, querWhere, vetorArg, null, "", vOrderBy, pLimit);
			}

			Table tupla = null;

			if (cursor.moveToFirst()) {
				Attribute[] attrs = getAttributesFromCursor(cursor);
				boolean validade = true;
				if (attrs == null || attrs.length == 0) {
					SingletonLog.insereErro(new Exception("Consulta sem atributos mapeados"), SingletonLog.TIPO.BANCO);

				}
				if (validade) {
					do {
						tupla = this.factory();
						boolean validade2 = false;
						for (int i = 0; i < attrs.length; i++) {
							if (attrs[i] == null)
								continue;
							String name = attrs[i].getName();
							String value = cursor.getString(i);
							tupla.setAttrValue(name, value);
							validade2 = true;
						}
						if (validade2) {
							return tupla;
						}

					} while (cursor.moveToNext());
				}
			}
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return null;
	}

	public String getLastIdInserted() {
		Cursor cursor = null;
		try {
			String tableName = getName();
			cursor = database.rawQuery("SELECT MAX(id) FROM " + tableName, null);
			String id = null;
			if (cursor.moveToFirst()) {
				id = cursor.getString(0);
			}
			return id;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public abstract Table factory();

	public abstract void onSynchronizing(Context pContext, String pNewId, String pOldId);

	public void onSynchronized(Context pContext, String pNewId, String pOldId) {

	}

	// boolean pIsAtualizacao,
	// boolean pIsInsercao,
	// boolean pIsRemocao
	public Long insertTableSynchronize(String pStrTable, String pIdInTable, String pTipoOperacaoBanco, String idCorporacao, String idUsuario)
			throws Exception {

		return insertTableSynchronize(pStrTable, pIdInTable, pTipoOperacaoBanco, false, idCorporacao, idUsuario);

	}

	public Long insertTableSynchronize(String pStrTable, String pIdInTable, String pTipoOperacaoBanco)
			throws Exception {

		return insertTableSynchronize(
				pStrTable
				, pIdInTable
				, pTipoOperacaoBanco
				, false
				,OmegaSecurity.getIdCorporacao()
				, OmegaSecurity.getIdUsuario());

	}

	HelperDate helperDate = null;

	public HelperDate getHelperDate() {
		if (helperDate == null)
			helperDate = new HelperDate();
		return helperDate;
	}

	// boolean pIsAtualizacao,
	// boolean pIsInsercao,
	// boolean pIsRemocao
	public Long insertTableSynchronize(String pStrTable, String pIdInTable, String pTipoOperacaoBanco,
									   boolean insereSemLogErro, String idCorporacao, String idUsuario) throws Exception {

		EXTDAOSistemaRegistroSincronizadorAndroidParaWeb sincronizador = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(
				getDatabase());
		String vIdTabelaSincronizador = EXTDAOSistemaTabela.getIdSistemaTabela(getDatabase(), pStrTable);
		if (vIdTabelaSincronizador == null)
			return null;
		sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT,
				vIdTabelaSincronizador);
		sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT, pIdInTable);
		sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_MOBILE_INT, pIdInTable);
		sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT,
				pTipoOperacaoBanco);

		sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT, idCorporacao);
		sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.USUARIO_ID_INT,
				idUsuario);
		sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CRIADO_EM_DATETIME,
				getHelperDate().getDatetimeAtualFormatadaParaSQLFast(getDatabase().getContext()));
		sincronizador.formatToSQLite();
		// CrudInserirEditar crudIE = new CrudInserirEditar(tipo);

		if (insereSemLogErro) {

			Long idSincronizador = sincronizador.insert(false, false, null);

			if (idSincronizador != null) {

				return idSincronizador;
			} else {

				return null;
			}
		} else {
			Long idSincronizador = sincronizador.insert(false);
			return idSincronizador;
		}
	}

	public String getJsonConteudo() throws JSONException {
		JSONObject result = new JSONObject();
		for (String attr : this.listSortNameAttribute) {
			String value = getStrValueOfAttribute(attr);
			if (value == null)
				result.put(attr, JSONObject.NULL);
			else
				result.put(attr, value);
		}
		String json = result.toString();
		return json;
	}

	EXTDAOSistemaRegistroHistorico objSRH = null;

	private Long insereCrudHistoricoSincronizadorParaEdicao(String jsonConteudo,
															Long idSistemaRegistroSincronizadorAndroidParaWeb, String pIdInTable) throws Exception {

		if (idSistemaRegistroSincronizadorAndroidParaWeb != null) {

			String idCorporacao = OmegaSecurity.getIdCorporacao();
			String idSistemaTabela = EXTDAOSistemaTabela.getIdSistemaTabela(getDatabase(), getName());
			if (objSRH == null)
				objSRH = new EXTDAOSistemaRegistroHistorico(this.getDatabase());
			else
				objSRH.clearData();
			objSRH.setAttrValue(EXTDAOSistemaRegistroHistorico.CRUD, jsonConteudo);
			objSRH.setAttrValue(EXTDAOSistemaRegistroHistorico.SISTEMA_REGISTRO_SINCRONIZADOR_ID_INT,
					String.valueOf(idSistemaRegistroSincronizadorAndroidParaWeb));
			objSRH.setAttrValue(EXTDAOSistemaRegistroHistorico.SISTEMA_TABELA_ID_INT, idSistemaTabela);
			objSRH.setAttrValue(EXTDAOSistemaRegistroHistorico.ID_TABELA_INT, pIdInTable);
			objSRH.setAttrValue(EXTDAOSistemaRegistroHistorico.ID_TABELA_MOBILE_INT, pIdInTable);
			objSRH.setAttrValue(EXTDAOSistemaRegistroHistorico.CORPORACAO_ID_INT, idCorporacao);
			objSRH.formatToSQLite();
			Long val = objSRH.insert(false);
			if (val == null) {
				SingletonLog.insereErro(
						new Exception(
								"Atenuuo: ocorreu uma falha ao salvar o backup do registro antes da execuuuo da operauuo de ediuuo a ser sincronizada."),
						SingletonLog.TIPO.SINCRONIZADOR);
			}
			return val;

		} else
			return null;
	}

	private void formatInsertToSQLiteAttributoNormalizado() {
		for (String vAttributeName : listSortNameAttribute) {
			Attribute vAttr = getAttribute(vAttributeName);
			if (vAttr != null && vAttr.isNormalizado()) {
				Attribute vAttrFoiNormalizado = vAttr.getAttributeNormalizado();
				if (!vAttrFoiNormalizado.isEmpty()) {
					String vValue = vAttrFoiNormalizado.getStrValue();
					String vWordWithoutAccent = HelperString.getWordWithoutAccent(vValue);
					vAttr.setStrValue(vWordWithoutAccent);
				} else {
					vAttr.setStrValue(null);
				}
			}
		}
	}

	public void formatToSQLite() {
		setIdAttrCorporacaoIfExists();
		formatInsertToSQLiteAttributoNormalizado();
	}

	public Long insert(boolean pIsToInsertInSynchronizeTable) {

		try {
			return insertThrowException(pIsToInsertInSynchronizeTable, true, null,
					false, OmegaSecurity.getIdCorporacao(), OmegaSecurity.getIdUsuario());
		} catch (Exception ex) {
			// a excacao ja foi inserida
			return null;
		}
	}

	public Long insert(boolean pIsToInsertInSynchronizeTable, String idCorporacao, String idUsuario) {

		try {
			return insertThrowException(pIsToInsertInSynchronizeTable, true, null, false, idCorporacao, idUsuario);
		} catch (Exception ex) {
			// a excacao ja foi inserida
			return null;
		}
	}

	public String getArgs() {
		StringBuilder stArgs = new StringBuilder();
		StringBuilder stAtributos = new StringBuilder();
		for (String strNameElement : listSortNameAttribute) {
			Attribute attr = hashNameAttributeByAttribute.get(strNameElement);
			stAtributos.append("," + strNameElement);
			stArgs.append("," + attr.getStrValue());
		}
		return "ATRIBUTOS: " + stAtributos.toString() + ". VALORES: " + stArgs.toString();
	}

	public Long insert(boolean pIsToInsertInSynchronizeTable, boolean imprimirErro) {
		try {
			return insertThrowException(
					pIsToInsertInSynchronizeTable, imprimirErro, null, false,
					OmegaSecurity.getIdCorporacao(), OmegaSecurity.getIdUsuario());
		} catch (Exception ex) {
			// a excacao ja foi inserida
			return null;
		}
	}

	public Long insert(boolean pIsToInsertInSynchronizeTable, boolean imprimirErro, OmegaLog log) {
		try {
			return insertThrowException(pIsToInsertInSynchronizeTable, imprimirErro,
					log, false, OmegaSecurity.getIdCorporacao(), OmegaSecurity.getIdUsuario());
		} catch (Exception ex) {
			// a excacao ja foi inserida
			return null;
		}
	}
	//	public Long insertThrowException(boolean pIsToInsertInSynchronizeTable, boolean imprimirErro, OmegaLog log,
//			boolean lancarExcecao) throws Exception {
//		return insertThrowException(pIsToInsertInSynchronizeTable, imprimirErro, log,
//				lancarExcecao, OmegaSecurity.getIdCorporacao()); 
//	}
	public Long insertThrowException(boolean pIsToInsertInSynchronizeTable, boolean imprimirErro, OmegaLog log,
									 boolean lancarExcecao,
									 String idCorporacao, String idUsuario) throws Exception {
		try {
			if (pIsToInsertInSynchronizeTable && this.getId() == null) {
				String idSistemaTabela = EXTDAOSistemaTabela.getIdSistemaTabela(getDatabase(), getName());
				if (idSistemaTabela != null) {
					Long newId = SingletonControladorChaveUnica.getNewId(HelperInteger.parserInteger(idSistemaTabela));
					if (newId == null)
						return null;
					this.setLongAttrValue(ID_UNIVERSAL, newId);
				}
			}

			ContentValues cv = new ContentValues();
			// insertStmt = oh.compileStatement(vStrQueryInsert);

			boolean vValidade = false;
			for (String strNameElement : listSortNameAttribute) {
				Attribute attr = hashNameAttributeByAttribute.get(strNameElement);
				if (!attr.checaValidadeParaEntrarNaClausulaInsert())
					continue;

				// if(attr.bind( insertStmt, index)){
				if (attr.put(cv)) {
					if (!vValidade) {
						vValidade = true;
					}
				}

			}
			if (!vValidade)
				return null;
			boolean hasToSync = false;
			boolean successfull = true;
			boolean beginTransaction = false;
			Long id = null;
			try {
				hasToSync = DatabaseConfiguration.IS_SYNCHRONIZE_ON
						&& !this.getName().equals(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.NAME)
						&& pIsToInsertInSynchronizeTable;

				if (hasToSync && !database.isTransactionStarted()) {
					beginTransaction = database.beginTransactionSyncDefaultTime();
					if (!beginTransaction)
						throw new OmegaDatabaseException(OmegaDatabaseException.CODE.BEGIN_TRANSACTION_TIMEOUT, null);
				}
				id = database.insertOrThrow(getName(), null, cv);

				if (id != null) {
					this.setAttrValue(ID_UNIVERSAL, id.toString());

					if (hasToSync)
						insertTableSynchronize(this.getName(), id.toString(),
								EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT,
								idCorporacao, idUsuario);

				}
				return id;
			} catch (Exception ex) {
				SingletonLog.insereErro(ex, SingletonLog.TIPO.BANCO);
				successfull = false;
				throw ex;
			} finally {
				if (beginTransaction)
					database.endTransaction(successfull);
			}
		} catch (SQLiteConstraintException ex) {

			if (imprimirErro) {
				if (log != null && OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
					log.escreveLog("Excecao capturada: " + ex.toString());
				}
				String vStrQueryInsert = "";
				vStrQueryInsert = getStrQueryInsert();
				if (vStrQueryInsert == null)
					return null;
				String erroDetalhado = "Tabela: " + getName() + ". Consulta: " + vStrQueryInsert + ". Argumentos: "
						+ getArgs();
				SingletonLog.insereErro(erroDetalhado, ex, SingletonLog.TIPO.BANCO);
			}
			if (lancarExcecao)
				throw ex;
			else
				return null;

		} catch (Exception ex) {

			if (imprimirErro) {
				if (log != null && OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
					log.escreveLog("Excecao capturada: " + ex.toString());
				}
				String vStrQueryInsert = "";
				vStrQueryInsert = getStrQueryInsert();
				if (vStrQueryInsert == null)
					return null;
				String erroDetalhado = "Tabela: " + getName() + ". Consulta: " + vStrQueryInsert + ". Argumentos: "
						+ getArgs();
				SingletonLog.insereErro(erroDetalhado, ex, SingletonLog.TIPO.BANCO);

			}
			throw ex;

		}

	}

	public void populateRandom() throws Exception {
		for (int k = 0; k < listSortNameAttribute.size(); k++) {
			String nomeAttr = listSortNameAttribute.get(k);
			Attribute attr = hashNameAttributeByAttribute.get(nomeAttr);
			if (attr.isAutoIncrement())
				continue;
			else {
				attr.populateRandom(getDatabase());
			}
		}
	}

	public void clearData() {
		for (String strNameElement : listSortNameAttribute) {
			Attribute attr = hashNameAttributeByAttribute.get(strNameElement);
			attr.clear();
		}
	}

	public ArrayList<Integer> removeAllTuplas(boolean sync) throws Exception {
		ArrayList<Integer> vListId = getAllListId();
		if (vListId != null)
			for (Integer vId : vListId) {
				if (vId != null)
					remove(String.valueOf(vId), sync);
			}
		return vListId;
	}

	public String getStrQueryInsert() {

		String strDefTotal = "";
		for (String strNameElement : listSortNameAttribute) {

			Attribute attr = hashNameAttributeByAttribute.get(strNameElement);

			if (!(attr.isAutoIncrement() && attr.isPrimaryKey() && attr.isEmpty())) {
				String strDef = " " + attr.getName();

				if (strDefTotal.length() > 0) {
					strDefTotal += ", " + strDef;
				} else {
					strDefTotal += strDef;
				}
			}
		}

		String strCreate = "INSERT INTO " + name + " ( " + strDefTotal + " )";

		strDefTotal = "";
		for (String strNameElement : listSortNameAttribute) {
			Attribute attr = hashNameAttributeByAttribute.get(strNameElement);

			if (!(attr.isAutoIncrement() && attr.isPrimaryKey() && attr.isEmpty())) {
				if (strDefTotal.length() > 0) {
					strDefTotal += ", ?";
				} else {
					strDefTotal += "?";
				}
			}
		}
		strCreate += " VALUES (" + strDefTotal + "); ";
		return strCreate;
	}

	public boolean isConsultaLike(Attribute.SQLLITE_TYPE sqlType) {
		switch (sqlType) {
			case CHAR:
			case VARCHAR:
			case TINYTEXT:
			case TEXT:
				return true;

			default:
				return false;

		}
	}

	public String getStrQueryWhereDoSelect(boolean pConsideraValorNulo, List<Attribute> vetorAttrKey) {

		String strDefTotal = "";
		for (Attribute attr : vetorAttrKey) {
			String token = "";
			if (attr.getStrValue() != null) {
				Attribute.SQLLITE_TYPE sqlType = attr.getSQLType();
				if (isConsultaLike(sqlType))
					token = attr.getName() + " LIKE ? ";
				else
					token = attr.getName() + " = ? ";

			} else if (pConsideraValorNulo) {
				token = attr.getName() + "  IS NULL ";
			}
			if (token.length() > 0) {
				if (strDefTotal.length() > 0) {
					strDefTotal += " AND " + token;
				} else {
					strDefTotal += token;
				}
			}
			// }
		}

		return strDefTotal;
	}

	public String getWhere(ArrayList<Attribute> p_listAttr) {
		if (p_listAttr == null || p_listAttr.size() == 0) {
			return null;
		}

		StringBuilder strDefTotal = new StringBuilder();
		for (Attribute attr : p_listAttr) {
			StringBuilder strDef = new StringBuilder();

			if (HelperString.checkIfIsNull(attr.getStrValue()) != null) {
				Attribute.SQLLITE_TYPE sqlType = attr.getSQLType();
				if (isConsultaLike(sqlType))
					strDef.append(attr.getName() + " LIKE ? ");
				else
					strDef.append(attr.getName() + " = ? ");
			} else {
				strDef.append(attr.getName() + "  IS NULL ");
			}
			if (strDefTotal.length() > 0) {
				strDefTotal.append(" AND ");
				strDefTotal.append(strDef);
			} else {
				strDefTotal.append(strDef);
			}

			// }
		}

		return strDefTotal.toString();
	}

	public ArrayList<String> getVetorQueryCreateIndex() {

		ArrayList<String> vList = new ArrayList<String>();
		if (this.containersChaves != null && this.containersChaves.size() > 0) {
			for (Integer i = 0; i < this.containersChaves.size(); i++) {
				ContainerKey containerKey = this.containersChaves.get(i);
				String atributosChave = HelperString.getStrSeparateByColumn(containerKey.vetorAttr, ",");
				String vIndex = "";
				if (containerKey.nome != null && containerKey.nome.length() > 0) {
					vIndex = "CREATE INDEX IF NOT EXISTS " + containerKey.nome + " ON  " + getName() + " ("
							+ atributosChave + " ASC); ";
				} else {
					vIndex = "CREATE INDEX IF NOT EXISTS indice_" + getName() + "_" + i.toString() + " ON " + getName()
							+ " (" + atributosChave + " ASC); ";
				}

				vList.add(vIndex);
			}
		}

		return vList;
	}

	public Attribute getNewAtributoNormalizado(String pStrName) {
		Attribute vAttr = getAttribute(pStrName);
		if (vAttr != null) {
			return vAttr.getNewAtributoNormalizado(vAttr);
		} else
			return null;
	}

	public String getStrQueryCreate() {

		String strDefTotal = "";
		String strFKTotal = "";
		boolean vIsChaveDefinida = false;

		for (String strNameElement : listSortNameAttribute) {

			Attribute attr = hashNameAttributeByAttribute.get(strNameElement);
			String strDef = "";

			if (attr.isPrimaryKey() && attr.isAutoIncrement()) {
				strDef += attr.getName() + " " + attr.getStrSQLite();
				strDef += " PRIMARY KEY ";
				strDef += " AUTOINCREMENT ";
				vIsChaveDefinida = true;

			} else {
				strDef += attr.getName() + " " + attr.getStrSQLiteDeclaration();
			}

			if (!attr.isNull) {
				strDef += " NOT NULL ";
			}

			if (attr.getStrDefault() != null) {
				strDef += " DEFAULT '" + attr.getStrDefault() + "'";
			}
			// if(attr.isPrimaryKey){
			// strDef += " PRIMARY KEY ";
			// }

			String strFK = "";

			if (attr.getStrTableFK() != null && attr.getStrAttributeFK() != null) {
				if (attr.getNomeFk() != null && attr.getNomeFk().length() > 0) {
					strFK = " CONSTRAINT " + attr.getNomeFk() + " FOREIGN KEY (" + attr.getName() + ") REFERENCES "
							+ attr.getStrTableFK() + " (" + attr.getStrAttributeFK() + ") " + " ON UPDATE "
							+ attr.getStrOnUpdate() + " ON DELETE " + attr.getStrOnDelete();
				} else {
					strFK = " FOREIGN KEY (" + attr.getName() + ") REFERENCES " + attr.getStrTableFK() + " ("
							+ attr.getStrAttributeFK() + ") " + " ON UPDATE " + attr.getStrOnUpdate() + " ON DELETE "
							+ attr.getStrOnDelete();
				}

				strFKTotal += ", " + strFK;
			}

			if (strDefTotal.length() > 0) {
				strDefTotal += ", " + strDef;
			} else {
				strDefTotal += strDef;
			}
		}

		String strKey = "";
		List<Attribute> listStrKey = this.getListAttributeKey();
		if (!vIsChaveDefinida)
			if (listStrKey.size() > 1) {

				for (Attribute attr : listStrKey) {
					String strAttr = attr.getName();
					if (strKey.length() > 0)
						strKey += ", " + strAttr;
					else
						strKey += strAttr;
				}
				strKey += " CONSTRAINT pk_primary_" + getName() + " PRIMARY KEY ( " + strKey + " ) ";
			} else {
				Attribute attr = listStrKey.get(0);
				strKey += " PRIMARY KEY (" + attr.getName() + ") ";
			}

		String strCreate = " CREATE TABLE IF NOT EXISTS " + name + " (" + strDefTotal;
		if (strKey.length() > 0)
			strCreate += " , " + strKey;
		if (strFKTotal.length() > 0)
			strCreate += strFKTotal;

		strCreate += "); ";

		return strCreate;
	}

	public String getStrQueryRemoveIndex(String index) {
		return "DROP INDEX \"" + index + "\"";
	}

	public ArrayList<String> getVetorQueryRemoveIndex() {
		ArrayList<String> querys = new ArrayList<String>();
		for (String attrName : this.listSortNameAttribute) {
			Attribute attr = getAttribute(attrName);
			if (attr.isFK()) {
				String fk = attr.getNomeFk();
				querys.add(getStrQueryRemoveIndex(fk));
			}
		}
		if (this.containersChaves != null) {
			for (Integer i = 0; i < this.containersChaves.size(); i++) {
				ContainerKey containerKey = this.containersChaves.get(i);

				if (containerKey.nome != null && containerKey.nome.length() > 0) {
					querys.add(getStrQueryRemoveIndex(containerKey.nome));
				}
			}
		}

		return querys;
	}

	public String getQueryUniqueKey() {
		String vStrUniqueKey = "";
		if (containerUniqueKey != null) {
			// CREATE UNIQUE INDEX i1 ON parent(c, d);
			String vTableName = getName();
			String vVetorUniqueKeyAttr[] = containerUniqueKey.vetorAttr;
			// String vFKName = "UNIQUE_KEY_" + vTableName + "_0";
			String vFKName = "";
			if (containerUniqueKey.nome != null && containerUniqueKey.nome.length() > 0) {
				vFKName = containerUniqueKey.nome;
			} else {
				vFKName = "UK_" + vTableName + "_0";
			}
			vFKName = containerUniqueKey.nome;
			// vStrUniqueKey = " UNIQUE KEY `" + vFKName + "` (" ;
			vStrUniqueKey = "CREATE UNIQUE INDEX IF NOT EXISTS " + vFKName + " ON " + vTableName + " (";
			String vStrVetorAttrUnique = "";
			for (String vAttrUniqueKey : vVetorUniqueKeyAttr) {
				if (vStrVetorAttrUnique.length() > 0) {
					vStrVetorAttrUnique += "," + vAttrUniqueKey;
				} else {
					vStrVetorAttrUnique += vAttrUniqueKey;
				}

			}
			vStrUniqueKey += vStrVetorAttrUnique + ") ";
		}
		return vStrUniqueKey;
	}

	public class RegistroDuplicado {
		public boolean chaveUnicaIndefinida = false;
		public Table registro = null;
		// http://stackoverflow.com/questions/4081783/unique-key-with-nulls
		// Nulo destroi a integridade de UNIQUE KEY
		public boolean possuiAtributosDaChaveUnicaComValoresNulos = false;

		public RegistroDuplicado(boolean chaveUnicaIndefinida, Table registro,
								 boolean possuiAtributosDaChaveUnicaComValoresNulos) {
			this.chaveUnicaIndefinida = chaveUnicaIndefinida;
			this.registro = registro;
			this.possuiAtributosDaChaveUnicaComValoresNulos = possuiAtributosDaChaveUnicaComValoresNulos;
		}

		public String getDescricao() {
			String desc = chaveUnicaIndefinida ? "Chave unica indefinida" : "Chave unica definida";
			desc += (registro != null) ? ". Registro duplicado de id: " + registro.getIdentificadorRegistro()
					: ". Nuo encontrou registro duplicado";
			desc += possuiAtributosDaChaveUnicaComValoresNulos ? ". Possui valores nulos na chave."
					: ". Nuo possui valores nulos na chave.";

			return desc;
		}
	}

	public RegistroDuplicado getRegistroDuplicadoSeExistente() {
		return getRegistroDuplicadoSeExistente(null);
	}

	public RegistroDuplicado getRegistroDuplicadoSeExistente(Table pObjAux) {
		Table obj2 = null;
		if (pObjAux != null)
			obj2 = pObjAux;
		else
			obj2 = factory();
		// Verifica se existe duplicada
		ContainerKey uniqueKey = getContainerUniqueKey();
		String atributos[] = null;
		if (uniqueKey != null)
			atributos = uniqueKey.getVetorAttribute();

		if (uniqueKey == null || atributos == null || atributos.length == 0) {
			return new RegistroDuplicado(true, null, false);
		} else {
			boolean possuiValorNulo = false;
			for (int k = 0; k < atributos.length; k++) {
				String valor = getStrValueOfAttribute(atributos[k]);
				if (valor != null) {
					obj2.setAttrValue(atributos[k], valor);
				} else
					possuiValorNulo = true;
			}
			Table registroDuplicado = obj2.getFirstTupla();

			if (registroDuplicado != null && registroDuplicado.getId().equals(this.getId()))
				registroDuplicado = null;
			return new RegistroDuplicado(false, registroDuplicado, possuiValorNulo);
		}

	}

	// Select first tuple of result, and load all data into this object
	public Boolean existeId() {

		String id = getId();
		if (id == null || id.length() == 0)
			return false;
		Cursor cursor = database.query(getName(), new String[] { "1" }, "id = ?", new String[] { id }, null, "", "",
				"1");

		try {
			clearData();
			if (cursor.moveToFirst()) {
				return true;
			}
			return false;
		} catch (Exception ex) {
			SingletonLog.insereErro(TAG, "select", HelperExcecao.getDescricao(ex), SingletonLog.TIPO.BANCO);
			return null;
		} finally {
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
		}

	}

	public static String makePlaceholders(int tot) {
		StringBuilder sb = new StringBuilder();
		sb.append("(? ");
		for (int i = 1; i < tot; i++) {
			sb.append(", ?");
		}
		sb.append(") ");
		return sb.toString();
	}

	public static Mensagem procedimentoDelete(Activity activity, String tabela, String id, CustomItemList item) {
		Mensagem msg = null;
		Database db = null;
		try {
			db = new DatabasePontoEletronico(activity);

			Table vObj = db.factoryTable(tabela);
			boolean validade = false;
			vObj.setAttrValue(Table.ID_UNIVERSAL, id);
			if (!vObj.select(id)) {

				validade = true;
				msg = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
						activity.getString(R.string.removido_sucesso));
				return msg;
			}
			if (tabela.compareTo(EXTDAOUsuario.NAME) == 0) {
				EXTDAOUsuario vObjUsuario = (EXTDAOUsuario) vObj;

				ServicosCobranca sc = new ServicosCobranca(activity);
				msg = sc.removerCliente(vObjUsuario.getStrValueOfAttribute(EXTDAOUsuario.EMAIL));

				if (msg != null && msg.ok()) {
					vObj.remove(false);
					return msg;
				} else if (msg == null)
					msg = Mensagem.factoryMsgSemInternet(activity);

				return msg;
			} else {

				// EXTDAOSistemaRegistroSincronizadorAndroidParaWeb
				// vObjSincronizador = new
				// EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(db);
				// se a tupla nao estiver sincronizada ainda, entao cancela
				// todas as
				// operacoes no banco refrente a tupla em questao.

				Integer intValidade2 = vObj.remove(true);
				validade = intValidade2 == null ? null : (intValidade2 <= 0 ? false : true);
				if (validade) {
					msg = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
							activity.getString(R.string.removido_sucesso));
				} else {
					msg = new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
							activity.getString(R.string.falha_remocao));
				}
				return msg;
			}
		} catch (Exception e) {
			SingletonLog.insereErro(e, SingletonLog.TIPO.FORMULARIO);
			msg = new Mensagem(e);
			return msg;
		} finally {
			if (db != null)
				db.close();
			try {
				if (msg.ok()) {

					if (activity instanceof OmegaDatabaseArrayAdapterListActivity) {
						OmegaDatabaseArrayAdapterListActivity myActivity = (OmegaDatabaseArrayAdapterListActivity) activity;
						if (item != null)
							myActivity.deleteItem(item);
					} else if (activity instanceof OmegaDatabaseAdapterListActivity) {
						OmegaDatabaseAdapterListActivity a = (OmegaDatabaseAdapterListActivity) activity;
						a.performExecute();
					}
				}
			} catch (Exception ex) {
				SingletonLog.insereErro(ex, SingletonLog.TIPO.FORMULARIO);

			}

			try {

				FactoryAlertDialog.showDialog(activity, msg);
			} catch (Exception ex) {
				SingletonLog.insereErro(ex, SingletonLog.TIPO.FORMULARIO);
			}
		}
	}

	public AppointmentPlaceItemList getAppointementPlace(
			String attrCidade, String attrLogradouro, String attrNumero
	) throws Exception {

		String idCidade = getStrValueOfAttribute(attrCidade);
		String[] cidadeEUfEPais = new String[] {null,null,null};
		if(idCidade != null){
			cidadeEUfEPais = EXTDAOCidade.getNomeCidadeEEstado(getDatabase(), idCidade);
		}

		AppointmentPlaceItemList vAppointementOrigem = new AppointmentPlaceItemList(
				null,
				0,
				getStrValueOfAttribute(attrLogradouro),
				getStrValueOfAttribute(attrNumero),
				null,
				null,
				cidadeEUfEPais[0],
				cidadeEUfEPais[1],
				cidadeEUfEPais[2],
				null,
				null,
				null);
		return vAppointementOrigem;
	}

	public void setCurrentDatetimeAttrSecOffsec(String attrSec, String attrOffsec){
		this.setLongAttrValue(attrSec, HelperDate.getSecOffsetSegundosTimeZone(getContext()));
		this.setIntAttrValue(attrOffsec, HelperDate.getOffsetSegundosTimeZone());
	}

	public void setDateAttrValue(String attrSec, String attrOffsec, Date date){
		long utc = HelperDate.getUTCSecFromDateCurrentTimezone(getContext(), date);
		int offsec=  HelperDate.getOffsetSegundosTimeZone();
		this.setIntAttrValue(attrOffsec, offsec);
		this.setLongAttrValue(attrSec, utc);
	}

	public void setStrDateAttrValue(String attrSec, String attrOffsec, String date){
		long utc = HelperDate.getUTCSecFromStrDateCurrentTimezone(getContext(), date);
		int offsec=  HelperDate.getOffsetSegundosTimeZone();
		this.setIntAttrValue(attrOffsec, offsec);
		this.setLongAttrValue(attrSec, utc);
	}

	public void setStrDatetimeAttrValue(String attrSec, String attrOffsec, String datetime){
		long utc = HelperDate.getUTCSecFromStrDatetimeCurrentTimezone(getContext(), datetime);
		int offsec=  HelperDate.getOffsetSegundosTimeZone();
		this.setIntAttrValue(attrOffsec, offsec);
		this.setLongAttrValue(attrSec, utc);
	}

	public String getIdFormatadaParaExibicao(){
		String vId = getId();
		if(vId != null && vId.startsWith("-"))
			vId = vId.replace('-', '#');
		return vId;
	}
}
