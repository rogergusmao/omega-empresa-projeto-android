package app.omegasoftware.pontoeletronico.database;

import android.database.sqlite.SQLiteDatabase;

public class Transaction{
	protected SQLiteDatabase writableDatabase ;
	public boolean successfull = true;
	public Transaction(SQLiteDatabase writableDatabase) {
		
		this.writableDatabase = writableDatabase;
		
		this.writableDatabase.beginTransaction();
		
		successfull = true;
	}
	public void setErroTransaction(){
		successfull = false;
	}
	public void endTransaction(boolean pSuccessfull){
		if(successfull)
			successfull = pSuccessfull;
		
		if(writableDatabase != null){
			if(successfull){
				writableDatabase.setTransactionSuccessful();	
			} 
			
			writableDatabase.endTransaction();
		}
		
//		writableDatabase.close();
		writableDatabase = null;
		
	}
}
