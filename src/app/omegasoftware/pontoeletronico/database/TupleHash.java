package app.omegasoftware.pontoeletronico.database;

import java.util.Enumeration;
import java.util.Hashtable;



public class TupleHash {
	private Hashtable<String, String> hashStrAttributeByStrValue;
	public TupleHash(){
		hashStrAttributeByStrValue = new Hashtable<String, String>();
	}
	
	public boolean isVazia(){
		if(hashStrAttributeByStrValue.size() == 0){
			return true;
		} else return false;
	}
	public boolean addAttrValue(String p_attrName, String p_strValue){
		if(hashStrAttributeByStrValue.contains(p_attrName)){
			return false;
		}else{
			hashStrAttributeByStrValue.put(p_attrName, p_strValue);
			return true;
		}
	}
	
	public String getStrValue(String p_attrName){
		Enumeration<String> listKey = hashStrAttributeByStrValue.keys();
		while(listKey.hasMoreElements()){
			String strKey = listKey.nextElement();
			if(strKey.compareTo(p_attrName) == 0)
				return hashStrAttributeByStrValue.get(p_attrName);
		}
				
		return null;
		
	}
	
	public Enumeration<String> getEnumarationStrKeyNameAttr(){
		return hashStrAttributeByStrValue.keys();
	}
	
	public String[] getVectorStrKeyNameAttr(){
	
	Enumeration<String> enumarationKeyStrNameAttr = hashStrAttributeByStrValue.keys();
	
	int count = 0;
    while(enumarationKeyStrNameAttr.hasMoreElements()){
  	  enumarationKeyStrNameAttr.nextElement();
  	  count += 1;
    }
    enumarationKeyStrNameAttr = hashStrAttributeByStrValue.keys();
    String[] vetor = new String[count];
    int index = 0;
    while(enumarationKeyStrNameAttr.hasMoreElements()){
    	  String strNameAttr = enumarationKeyStrNameAttr.nextElement();
    	  vetor[index] = strNameAttr;
    	  index += 1;
      }
    return vetor;
}
	
}
