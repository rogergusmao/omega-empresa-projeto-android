package app.omegasoftware.pontoeletronico.database.synchronize;

import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.database.Database;

public class AutoFlushOperacoesSqlite{
	final int LIMITE_TRANSACOES_SEGUIDAS = 10;
	
	int iTransaction = 0;
	Database db = null;
	OmegaLog log;
	public AutoFlushOperacoesSqlite(Database db, OmegaLog log){
		this.db = db;
		this.log =log;
		this.iTransaction = 0;
	}
	public boolean execute(boolean erro, boolean aForca){
		if(aForca || (!erro && ++iTransaction % LIMITE_TRANSACOES_SEGUIDAS == 0)){
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[esrfgthj] Flush dos comandos do banco");
			db.endTransaction(true);
			boolean validade = db.beginTransaction();
			iTransaction = 0;
			return validade;
		} else return true;
	}
}