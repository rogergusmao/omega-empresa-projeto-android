package app.omegasoftware.pontoeletronico.database.synchronize;

import java.util.ArrayList;

import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;



public class ContainerEXTDAO{

//	public enum TYPE {INSERCAO, REMOCAO};
//	Contem toda a modificacao de uma dada operacao
//	(INSERCAO, REMOACAO, UPDATE), para uma data tabela
	
	
	private ArrayList<String> listIdEXTDAOSincronizador= new ArrayList<String>();
	private ArrayList<String> listIdEXTDAO = new ArrayList<String>();
	String nameTable;
	
	//Utilizado para TYPE = INSERCAO ou TYPE = REMOCAO
	public ContainerEXTDAO(String pNameTable){
		nameTable = pNameTable;
	}
	public String getNameTable(){
		return nameTable;
	}
	public void removeCorpo(String pIdEXTDAO){
		
		for(int i = 0 ; i < listIdEXTDAO.size(); i ++){
			String vId = listIdEXTDAO.get(i);
			if(vId.compareTo(pIdEXTDAO) == 0){
				listIdEXTDAOSincronizador.remove(i);
				listIdEXTDAO.remove(i);
				return;
				
			}
		}
		
		
	}
	public boolean containIdDaTabela(String pId){
		for (String vId : listIdEXTDAO) {
			if(vId.compareTo(pId) == 0 ) return true;
		}
		return false;
	}
	
	public String getStringVetorId(){
		String vToken = "";
		for (String vId : listIdEXTDAO) {
			if(vToken.length() == 0 )
				vToken += vId;
			else vToken += OmegaConfiguration.DELIMITER_QUERY_COLUNA + vId;
		}
		return vToken;
	}
	public String getId(int pPosition){
		if(pPosition < listIdEXTDAO.size()){
			return  listIdEXTDAO.get(pPosition);
			
		}
		return null;
	}
	public void addCorpo(String pIdSincronizador, String pId){
		
		listIdEXTDAOSincronizador.add(pIdSincronizador);
		listIdEXTDAO.add(pId);
	}
	

	
	
	public ArrayList<String> getListIdEXTDAO(){
		return listIdEXTDAO;
	}
	
	public ArrayList<String> getListIdEXTDAOSincronizador(){
		return listIdEXTDAOSincronizador;
	}
	
	public boolean isInitialized(){
		if(listIdEXTDAO.size() == 0 ) return false;
		else return true;
	}
	
}