package app.omegasoftware.pontoeletronico.database.synchronize;

import java.util.ArrayList;

public class ContainerFastSendData{
	public ArrayList<ContainerEXTDAO> listInsertContainerEXTDAO ;
	public ArrayList<ContainerEXTDAO> listUpdateContainerEXTDAO ;
	public ArrayList<ContainerEXTDAO> listRemoveContainerEXTDAO ;
	
	public ContainerFastSendData(){
		listInsertContainerEXTDAO  = new ArrayList<ContainerEXTDAO>();
		listUpdateContainerEXTDAO = new ArrayList<ContainerEXTDAO>();
		listRemoveContainerEXTDAO  = new ArrayList<ContainerEXTDAO>();
	}
	public ContainerFastSendData(
			ArrayList<ContainerEXTDAO> p_listContainerInsert ,
			ArrayList<ContainerEXTDAO> p_listUpdate, 
			ArrayList<ContainerEXTDAO> p_listRemoveContainerQueryEXTDAO){
		
		listInsertContainerEXTDAO  = p_listContainerInsert;
		listUpdateContainerEXTDAO = p_listUpdate;
		listRemoveContainerEXTDAO = p_listRemoveContainerQueryEXTDAO;
	}
	
	public boolean isEmpty(){
		if(listInsertContainerEXTDAO.size() == 0 &&
				listUpdateContainerEXTDAO.size() == 0 &&
				listRemoveContainerEXTDAO.size() == 0 )
			return true;
		else return false;
	}
	
	public void copy(ContainerFastSendData pContainer){
		if(pContainer == null) return;
		ArrayList<ContainerEXTDAO> vListContainerInsert = pContainer.getListInsertContainerEXTDAO();
		ArrayList<ContainerEXTDAO> vListContainerUpdate = pContainer.getListUpdateContainerEXTDAO();
		ArrayList<ContainerEXTDAO> vListContainerRemove = pContainer.getListRemoveContainerEXTDAO();
		listInsertContainerEXTDAO.addAll(vListContainerInsert);
		listUpdateContainerEXTDAO.addAll(vListContainerUpdate);
		listRemoveContainerEXTDAO.addAll(vListContainerRemove);
		
	}
	
	public ArrayList<ContainerEXTDAO> getListInsertContainerEXTDAO(){
		return listInsertContainerEXTDAO;
	}
	
	public ArrayList<ContainerEXTDAO> getListRemoveContainerEXTDAOOfTable(String pStrTable){
		if(pStrTable == null) return null;
		else if (pStrTable.length() == 0 ) return null;
		ArrayList<ContainerEXTDAO> vListReturn = new ArrayList<ContainerEXTDAO>(); 
		for (ContainerEXTDAO vContainerEXTDAO : listRemoveContainerEXTDAO) {
			String vTableName = vContainerEXTDAO.getNameTable();
			if(vTableName != null)
				if(vTableName.length() > 0 ){
					if(vTableName.compareTo(pStrTable) == 0 ){
						vListReturn.add(vContainerEXTDAO);
					}
				}
			
		}
		return vListReturn;
	}
	

	public ArrayList<ContainerEXTDAO> getListInsertContainerEXTDAOOfTable(String pStrTable){
		if(pStrTable == null) return null;
		else if (pStrTable.length() == 0 ) return null;
		ArrayList<ContainerEXTDAO> vListReturn = new ArrayList<ContainerEXTDAO>(); 
		for (ContainerEXTDAO vContainerEXTDAO : listInsertContainerEXTDAO) {
			String vTableName = vContainerEXTDAO.getNameTable();
			if(vTableName != null)
				if(vTableName.length() > 0 ){
					if(vTableName.compareTo(pStrTable) == 0 ){
						vListReturn.add(vContainerEXTDAO);
					}
				}
			
		}
		return vListReturn;
	}
	
	public boolean addCorpoEdit(String pNomeTabela, String pIdSincronizador, String pIdNaTabela){
		ArrayList<ContainerEXTDAO> vList =  getListUpdateContainerEXTDAOOfTable(pNomeTabela);
		
		for (ContainerEXTDAO vContainerEXTDAO : vList) {
			if(vContainerEXTDAO.containIdDaTabela(pIdNaTabela))
				return false;
			
		}
		ContainerEXTDAO vContainer = null;
		if(vList.size() == 0 ){
			vContainer = new ContainerEXTDAO(pNomeTabela);
			listUpdateContainerEXTDAO.add(vContainer);
		}
		else vContainer = vList.get(vList.size() - 1);
		
		vContainer.addCorpo(pIdSincronizador, pIdNaTabela);
		return true;
		
	}
	public ArrayList<ContainerEXTDAO> getListUpdateContainerEXTDAOOfTable(String pStrTable){
		if(pStrTable == null) return null;
		else if (pStrTable.length() == 0 ) return null;
		ArrayList<ContainerEXTDAO> vListReturn = new ArrayList<ContainerEXTDAO>(); 
		for (ContainerEXTDAO vContainerEXTDAO : listUpdateContainerEXTDAO) {
			String vTableName = vContainerEXTDAO.getNameTable();
			if(vTableName != null)
				if(vTableName.length() > 0 ){
					if(vTableName.compareTo(pStrTable) == 0 ){
						vListReturn.add(vContainerEXTDAO);
					}
				}
			
		}
		return vListReturn;
	}
	
	public ArrayList<ContainerEXTDAO> getListRemoveContainerEXTDAO(){
		return listRemoveContainerEXTDAO;
	}
	
	public ArrayList<ContainerEXTDAO> getListUpdateContainerEXTDAO(){
		return listUpdateContainerEXTDAO;
	}
	
}
