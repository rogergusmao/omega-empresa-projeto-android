package app.omegasoftware.pontoeletronico.database.synchronize;

import java.util.ArrayList;

import android.util.SparseIntArray;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class ContainerQueryEXTDAO{

//	public enum TYPE {INSERCAO, REMOCAO};
//	Contem toda a modificacao de uma dada operacao
//	(INSERCAO, REMOACAO, UPDATE), para uma data tabela
	private ArrayList<String[]> idsDependentesPorCorpo = new ArrayList<String[]>();
	private ArrayList<String[]> atributosDependentesPorCorpo = new ArrayList<String[]>();
	private ArrayList<String> listCorpo = new ArrayList<String>();
	private ArrayList<String[]> listValores = new ArrayList<String[]>();
	private ArrayList<String> listIdEXTDAOSincronizador= new ArrayList<String>();
	private ArrayList<String> listIdEXTDAO = new ArrayList<String>();
	private ArrayList<String> listIdEXTDAOMobileOriginal = new ArrayList<String>();
	private SparseIntArray  listIdEXTDAORemovido = new SparseIntArray();
	String nameTable;
	public ArrayList<String[]> getIdsDependentesPorCorpo(){
		return idsDependentesPorCorpo;
	}
	public ArrayList<String[]> getAtributosDependentesPorCorpo(){
		return atributosDependentesPorCorpo;
	}
	//Utilizado para TYPE = INSERCAO ou TYPE = REMOCAO
	public ContainerQueryEXTDAO(String pNameTable){
		nameTable = pNameTable;
	}
	public String getNameTable(){
		return nameTable;
	}
	public boolean foiRemovido(Integer pIdEXTDAO, Integer idSincronizadorOpInsertEdit){
		int index = listIdEXTDAORemovido.indexOfKey(pIdEXTDAO);
		if(index >= 0 ){
			return listIdEXTDAORemovido.get(index) > idSincronizadorOpInsertEdit;
		} else 
			return false;
	}
	 
	public String removeCorpo(OmegaLog log, String pIdEXTDAO, String idMobileOriginal){
		///Retorna o id do sincornizador
		int vIndex = 0 ;
		
		
		for (String vId : listIdEXTDAO) {
			if(vId.compareTo(pIdEXTDAO) == 0){
				
				if(log != null && OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog(
					"ContainerQueryEXTDAO::removeCorpo - o registro da operauuo foi desconsiderado para envio da sincronizacao do mobile para web. IdSistemaRegistroSincronizadorAndroidParaWeb [" + listIdEXTDAOSincronizador.get(vIndex) + "] "
					+ " relacionado a "+this.nameTable+"[" + vId + "]."  );
				listValores.remove(vIndex);
				if(listCorpo .size() > vIndex)
					listCorpo.remove(vIndex);
				
				String idSincronizador = listIdEXTDAOSincronizador.get(vIndex);
				listIdEXTDAOSincronizador.remove(vIndex);
				listIdEXTDAORemovido.append(HelperInteger.parserInt( pIdEXTDAO), HelperInteger.parserInt(idSincronizador));
				
				listIdEXTDAOMobileOriginal.remove(vIndex);
				listIdEXTDAO.remove(vIndex);
				
				if(atributosDependentesPorCorpo.size() > vIndex)
					atributosDependentesPorCorpo.remove(vIndex);
				if(idsDependentesPorCorpo.size() > vIndex)
					idsDependentesPorCorpo.remove(vIndex);
				return idSincronizador;
			}
			vIndex += 1;
		}
		return null;
	}
	public String getId(int pPosition){
		if(pPosition < listIdEXTDAO.size()){
			return  listIdEXTDAO.get(pPosition);
			
		}
		return null;
	}
	public void addCorpo(String pCorpo,  String[] valores, String pIdSincronizador, String pId, String idTabelaMobileOriginal, ArrayList<String> listaDependentes, ArrayList<String> listaAtributosDependentes){
		if(listaDependentes != null && listaDependentes.size() > 0){
			String[] idsDependentes = new String[listaDependentes.size()];
			listaDependentes.toArray(idsDependentes);
			idsDependentesPorCorpo.add(idsDependentes);	
		} else
			idsDependentesPorCorpo.add(null);
		
		if(listaAtributosDependentes != null && listaAtributosDependentes.size() > 0){
			String[] attrsDependentes = new String[listaAtributosDependentes.size()];
			listaAtributosDependentes.toArray(attrsDependentes);
			atributosDependentesPorCorpo.add(attrsDependentes);	
		} else
			atributosDependentesPorCorpo.add(null);
		
		
		addCorpo(valores, pIdSincronizador, pId, idTabelaMobileOriginal);
	}
	
	public void addCorpo(String[] valores, String pIdSincronizador, String pId, String pIdTabelaMobile){
		listValores.add(valores);
		listCorpo.add(null);
		listIdEXTDAOSincronizador.add(pIdSincronizador);
		listIdEXTDAO.add(pId);
		listIdEXTDAOMobileOriginal.add(pIdTabelaMobile);
	}
	
	public void addCorpo(String pCorpo, String pIdSincronizador, String pId, String pIdTabelaMobile){
		listCorpo.add(pCorpo);
		listIdEXTDAOSincronizador.add(pIdSincronizador);
		listIdEXTDAO.add(pId);
		listValores.add(null);
		listIdEXTDAOMobileOriginal.add(pIdTabelaMobile);
	}
	public void addCorpo(String pIdSincronizador, String pId, String pIdTabelaMobile){
		listCorpo.add(null);
		listIdEXTDAOSincronizador.add(pIdSincronizador);
		listIdEXTDAO.add(pId);
		listValores.add(null);
		listIdEXTDAOMobileOriginal.add(pIdTabelaMobile);
	}

	public ArrayList<String> getIdsEXTDAO(){
		return listIdEXTDAO;
	}
	
	public ArrayList<String> getListIdEXTDAOSincronizador(){
		return listIdEXTDAOSincronizador;
	}
	
	public boolean isInitialized(){
		if(listCorpo.size() == 0 ) return false;
		else return true;
	}
	
	public ArrayList<String[]> getListValores(){
		return  listValores;
	}
	public ArrayList<String> getListCorpo(){
		return listCorpo;
	}
}
