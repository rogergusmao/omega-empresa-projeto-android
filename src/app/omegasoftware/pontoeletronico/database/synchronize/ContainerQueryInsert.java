package app.omegasoftware.pontoeletronico.database.synchronize;

import java.util.ArrayList;


public class ContainerQueryInsert extends ContainerQueryEXTDAO{
	
//	public enum TYPE {INSERCAO, REMOCAO};
	
	private String cabecalho = "";
	private String[] listStrQueryInsert = null;
	
	//Utilizado para TYPE = INSERCAO ou TYPE = REMOCAO
	public ContainerQueryInsert(String pNameTable, String pCabecalho){
		super(pNameTable);;
		
		cabecalho = pCabecalho;
	}
	
	public String getCabecalho(){
		return cabecalho;
	}
	
	public String[] getListStrQueryInsert(){
		if(listStrQueryInsert != null) return listStrQueryInsert;
		ArrayList<String> vList = super.getListCorpo();
		String[] vVetorListQuery = new String[vList.size()];
		
		int i = 0 ;
		for (String vCorpo : vList) {
			String vQueryInsert = cabecalho + vCorpo;
			vVetorListQuery[i] = vQueryInsert;
			i += 1;
		}
		listStrQueryInsert = vVetorListQuery;
		return listStrQueryInsert;
	}
	
	public String getStrQueryMultipleInsert(){
		ArrayList<String> vList = super.getListCorpo();
		
		String vQuery = "";
		if(cabecalho.length() > 0 ){
			vQuery += cabecalho; 
		} else return null;
		boolean vValidade = false;
		for (String vCorpo : vList) {
			if(! vValidade){
				vQuery += vCorpo;
				vValidade = true;
			}
			else {
				vQuery += "," + vCorpo;
			}
		}
		return vQuery;
	}
	
}