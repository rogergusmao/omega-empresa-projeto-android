package app.omegasoftware.pontoeletronico.database.synchronize;

import java.util.ArrayList;

import android.content.Context;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCorporacaoSincronizadorPhp;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorWebParaAndroid;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoOperacaoBanco;

public class ContainerReceiveInformationDatabase {
	ArrayList<ContainerTableInformation> listRemoveContainerTableInformation = new ArrayList<ContainerTableInformation>();
	ArrayList<ContainerTableInformation> listEditContainerTableInformation = new ArrayList<ContainerTableInformation>();
	ArrayList<ContainerTableInformation> listInsertContainerTableInformation = new ArrayList<ContainerTableInformation>();
	Context context;
	Database database;

	public ContainerReceiveInformationDatabase(Context pContext) throws Exception{
		context = pContext;
		database = new DatabasePontoEletronico(context);
		loadFromDatabase();
		database.close();
	}

	public void loadFromDatabase() throws Exception{
		
		EXTDAOSistemaRegistroSincronizadorWebParaAndroid vObjSincronizadorPHP = new EXTDAOSistemaRegistroSincronizadorWebParaAndroid(database);
		vObjSincronizadorPHP.setAttrValue(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.SISTEMA_TIPO_OPERACAO_ID_INT, EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE);
		vObjSincronizadorPHP.setAttrValue(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		
		//Loading vListRemoveObjSincronizadorPHP
		ArrayList<Table> vListRemoveObjSincronizadorPHP = vObjSincronizadorPHP.getListTable();
		if(vListRemoveObjSincronizadorPHP != null)
			for (Table vTuplaSincronizadorPHP : vListRemoveObjSincronizadorPHP) {
				String vIdTabelaSincronizador = vTuplaSincronizadorPHP.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.SISTEMA_TABELA_ID_INT);
				String vIdTabela = vTuplaSincronizadorPHP.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.ID_TABELA_INT);
				String vIdSincronizador = vTuplaSincronizadorPHP.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.ID);
				ContainerTableInformation vContainer = getRemoveContainerTableInformation(vIdTabelaSincronizador) ;
				if(vContainer == null)
					vContainer = new ContainerTableInformation(vIdTabelaSincronizador, EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE);
	
				vContainer.addTupla(context, vIdSincronizador, vIdTabela, vObjSincronizadorPHP);
	
				listRemoveContainerTableInformation.add(vContainer);
			}
		vObjSincronizadorPHP.setAttrValue(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.SISTEMA_TIPO_OPERACAO_ID_INT, EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT);
		//Loading vListRemoveObjSincronizadorPHP
		ArrayList<Table> vListInsertObjSincronizadorPHP = vObjSincronizadorPHP.getListTable();
		if(vListInsertObjSincronizadorPHP != null)
			for (Table vTuplaSincronizadorPHP : vListInsertObjSincronizadorPHP) {
				String vIdTabelaSincronizador = vTuplaSincronizadorPHP.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.SISTEMA_TABELA_ID_INT);
				String vIdTabela = vTuplaSincronizadorPHP.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.ID_TABELA_INT);
				String vIdSincronizador = vTuplaSincronizadorPHP.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.ID);
				ContainerTableInformation vContainer = getInsertContainerTableInformation(vIdTabelaSincronizador) ;
				if(vContainer == null)
					vContainer = new ContainerTableInformation(vIdTabelaSincronizador, EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT);
	
				vContainer.addTupla(context,vIdSincronizador, vIdTabela, vObjSincronizadorPHP);
	
				listInsertContainerTableInformation.add(vContainer);
			}

		vObjSincronizadorPHP.setAttrValue(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.SISTEMA_TIPO_OPERACAO_ID_INT, EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_EDIT);
		//Loading vListRemoveObjSincronizadorPHP
		ArrayList<Table> vListEditObjSincronizadorPHP = vObjSincronizadorPHP.getListTable();
		if(vListEditObjSincronizadorPHP != null)
			for (Table vTuplaSincronizadorPHP : vListEditObjSincronizadorPHP) {
				String vIdTabelaSincronizador = vTuplaSincronizadorPHP.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.SISTEMA_TABELA_ID_INT);
				String vIdTabela = vTuplaSincronizadorPHP.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.ID_TABELA_INT);
				String vIdSincronizador = vTuplaSincronizadorPHP.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.ID);
				ContainerTableInformation vContainer = getEditContainerTableInformation(vIdTabelaSincronizador) ;
				if(vContainer == null)
					vContainer = new ContainerTableInformation(vIdTabelaSincronizador, EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_EDIT);
	
				vContainer.addTupla(context, vIdSincronizador, vIdTabela, vObjSincronizadorPHP);
	
				listEditContainerTableInformation.add(vContainer);
			}
		//Loading vListEditObjSincronizadorPHP
		database.close();
	}



	public boolean isEmpty(){
		if(listEditContainerTableInformation.size() == 0 && 
				listInsertContainerTableInformation.size() == 0 && 
				listRemoveContainerTableInformation.size() == 0){
			return true;
		} else return false;
	}
	public void removeRemoveContainerTableInformation(ContainerTableInformation pContainer){
		int i = 0 ;
		for (ContainerTableInformation vContainer : listRemoveContainerTableInformation) {

			if(vContainer == pContainer)
				break;
			i += 1;
		}
		if(i < listRemoveContainerTableInformation.size())
		listRemoveContainerTableInformation.remove(i);
	}
	public void removeInsertContainerTableInformation(ContainerTableInformation pContainer){
		int i = 0 ;
		for (ContainerTableInformation vContainer : listInsertContainerTableInformation) {

			if(vContainer == pContainer)
				break;
			i += 1;
		}
		if(i < listInsertContainerTableInformation.size())
			listInsertContainerTableInformation.remove(i);
	}

	public void removeEditContainerTableInformation(ContainerTableInformation pContainer){
		int i = 0 ;
//		listEditContainerTableInformation.remove(pContainer);
		for (ContainerTableInformation vContainer : listEditContainerTableInformation) {

			if(vContainer == pContainer)
				break;
			i += 1;
		}
		if(i < listEditContainerTableInformation.size())
		listEditContainerTableInformation.remove(i);
	}

	public boolean isEmptyListInsertContainerTableInformation(){
		if(listInsertContainerTableInformation.size() == 0 ) return true;
		else return false;
	}

	public boolean isEmptyListEditContainerTableInformation(){
		if(listEditContainerTableInformation.size() == 0 ) return true;
		else return false;
	}
	
	public boolean isEmptyListRemoveContainerTableInformation(){
		if(listRemoveContainerTableInformation.size() == 0 ) return true;
		else return false;
	}

	public ContainerTableInformation getInsertContainerTableInformation(String pIdTabelaSincronizador){
		for (ContainerTableInformation vContainer : listInsertContainerTableInformation) {
			if(vContainer.isTabelaSincronizador(pIdTabelaSincronizador))
				return vContainer;
		}
		return null;
	}

	public ContainerTableInformation getEditContainerTableInformation(String pIdTabelaSincronizador){
		for (ContainerTableInformation vContainer : listEditContainerTableInformation) {
			if(vContainer.isTabelaSincronizador(pIdTabelaSincronizador))
				return vContainer;
		}
		return null;
	}
	

	//Save Plano ID in the preferences
	public static void saveUltimoIdSincronizadorAndroid(Database db, String pUltimoIdSincronizador)
	{
		EXTDAOCorporacaoSincronizadorPhp vObjCorporacaoSincronizadorPHP = new EXTDAOCorporacaoSincronizadorPhp(db);
		vObjCorporacaoSincronizadorPHP.setAttrValue(EXTDAOCorporacaoSincronizadorPhp.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		EXTDAOCorporacaoSincronizadorPhp vTuplaCorporacaoSincronizadorPHP = (EXTDAOCorporacaoSincronizadorPhp) vObjCorporacaoSincronizadorPHP.getFirstTupla();
		if(vTuplaCorporacaoSincronizadorPHP != null){
			vTuplaCorporacaoSincronizadorPHP.setAttrValue(EXTDAOCorporacaoSincronizadorPhp.LAST_ID_SINCRONIZADOR_PHP_INT, pUltimoIdSincronizador);
			vTuplaCorporacaoSincronizadorPHP.update(false);
		} else {
			vObjCorporacaoSincronizadorPHP.setAttrValue(EXTDAOCorporacaoSincronizadorPhp.LAST_ID_SINCRONIZADOR_PHP_INT, pUltimoIdSincronizador);
			vObjCorporacaoSincronizadorPHP.insert(false);
		}
	}
	
	
	public static String getSavedUltimoIdSincronizadorAndroid(Context pContext, Database pDatabase)
	{
		EXTDAOCorporacaoSincronizadorPhp vObjCorporacaoSincronizadorPHP = new EXTDAOCorporacaoSincronizadorPhp(pDatabase);
		vObjCorporacaoSincronizadorPHP.setAttrValue(EXTDAOCorporacaoSincronizadorPhp.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		EXTDAOCorporacaoSincronizadorPhp vTuplaCorporacaoSincronizadorPHP = (EXTDAOCorporacaoSincronizadorPhp) vObjCorporacaoSincronizadorPHP.getFirstTupla();
		if(vTuplaCorporacaoSincronizadorPHP != null){
			return vTuplaCorporacaoSincronizadorPHP.getStrValueOfAttribute(EXTDAOCorporacaoSincronizadorPhp.LAST_ID_SINCRONIZADOR_PHP_INT);
		} else return "-1";
	}


	public void clear(){
		listRemoveContainerTableInformation.clear();
		listEditContainerTableInformation.clear();
		listInsertContainerTableInformation.clear();
	}
	public ArrayList<ContainerTableInformation> getListaRemoveContainerTableInformation(){
		return listRemoveContainerTableInformation;
	}

	public ArrayList<ContainerTableInformation> getListaEditContainerTableInformation(){
		return listEditContainerTableInformation;
	}

	public ArrayList<ContainerTableInformation> getListaInsertContainerTableInformation(){
		return listInsertContainerTableInformation;
	}



//	public String requestPostInServerPHP(Database pDatabase){
//		String vPost = HelperHttpPost.getConteudoStringPost(
//				context, 
//				OmegaConfiguration.URL_REQUEST_LISTA_INFORMACAO_SINCRONIZADOR(), 
//				new String[] {"usuario", "id_corporacao", "corporacao", "senha", "ultimo_id_sincronizador_android", "imei", "id_programa"},
//				new String[] {OmegaSecurity.getIdUsuario(), OmegaSecurity.getIdCorporacao(), OmegaSecurity.getCorporacao(),  OmegaSecurity.getSenha(), getSavedUltimoIdSincronizadorAndroid(context, pDatabase), HelperPhone.getIMEI(), ContainerPrograma.getIdPrograma()});
//		return vPost;
//	}

	public ContainerTableInformation getRemoveContainerTableInformation(String pIdTabelaSincronizador){
		for (ContainerTableInformation vContainer : listRemoveContainerTableInformation) {
			if(vContainer.isTabelaSincronizador(pIdTabelaSincronizador))
				return vContainer;
		}
		return null;
	}

	
//	public static boolean saveUltimoIdSincronizadorFromServer(Context pContext, Database pDatabase){
//		String vPost = HelperHttpPost.getConteudoStringPost(
//				pContext, 
//				OmegaConfiguration.URL_REQUEST_SEND_ULTIMO_ID_SINCRONIZADOR_ANDROID(), 
//				new String[] {"usuario", "id_corporacao", "corporacao", "senha", "ultimo_id_sincronizador_android"},
//				new String[] {OmegaSecurity.getIdUsuario(), OmegaSecurity.getIdCorporacao(), OmegaSecurity.getCorporacao(), OmegaSecurity.getSenha(), getSavedUltimoIdSincronizadorAndroid(pContext, pDatabase)});
//		
//
//		if(vPost == null) return false;
//		else if (vPost.length() == 0 ) return true;
//
//		
//		String vVetorLinha[] = vPost.split(OmegaConfiguration.DELIMITER_QUERY_UNIQUE_LINHA);
//		if(vVetorLinha == null ) return false;
//		//[0] - EDIT/REMOVE/INSERT
//		//[2] - (VALORES)
//		else if (vVetorLinha.length < 2) return false;
//		String vTipoOperacaoBanco[] = {"LAST_ID_SINCRONIZADOR"};
//		int vIndex = 0;
//		
//
//		for (String vLinha : vVetorLinha) {
//
//			if(vLinha.startsWith("OP%%")){
//				String vOperation = vLinha.substring(4, vLinha.length());
//				int i = vIndex;
//				for(; i < vTipoOperacaoBanco.length; i ++){
//					if(vOperation.compareTo(vTipoOperacaoBanco[i]) == 0){
//						vIndex = i;
//						break;
//					}
//						
//				}
//				if(i == vTipoOperacaoBanco.length)
//					break;
//				continue;
//			} else{
//
//				String vVetorColuna[] = vLinha.split( OmegaConfiguration.DELIMITER_QUERY_UNIQUE_COLUNA);
//
//				if(vVetorColuna.length == 1){
//					String vLastIdSincronizador = vVetorColuna[0];
//					if(vLastIdSincronizador != null)
//						if(vLastIdSincronizador.length() > 0 )
//							switch (vIndex) {
//							//LAST_ID_SINCRONIZADOR
//							case 0:
//								saveUltimoIdSincronizadorAndroid(pDatabase ,vLastIdSincronizador);
//								break;
//
//							default:
//								break;
//							}
//				}
//			}
//
//		}
//		return true;
//		
//	}
	
//	public boolean loadListaInformacaoSincronizador(){
//		Database pDatabase = null;
//		try{
//			pDatabase = new DatabasePontoEletronico(context);
//			String vPost = requestPostInServerPHP(pDatabase);
//
//			if(vPost == null) return false;
//			else if (vPost.length() == 0 ) return false;
//
//			EXTDAOSistemaRegistroSincronizadorWebParaAndroid vObjSincronizadorPHP = new EXTDAOSistemaRegistroSincronizadorWebParaAndroid(pDatabase); 
//			String vVetorLinha[] = vPost.split( OmegaConfiguration.DELIMITER_QUERY_UNIQUE_LINHA);
//			if(vVetorLinha == null ){
//				
//				return false;
//			}
//			//[0] - EDIT/REMOVE/INSERT
//			//[2] - (VALORES)
//			else if (vVetorLinha.length < 2){
//				
//				return false;
//			}
//			String vTipoOperacaoBanco[] = {"LAST_ID_SINCRONIZADOR", "REMOVE", "EDIT", "INSERT"};
//			int vIndex = 0;
//			
//
//			for (String vLinha : vVetorLinha) {
//
//				if(vLinha.startsWith("OP%%")){
//					String vOperation = vLinha.substring(4, vLinha.length());
//					int i = vIndex;
//					for(; i < vTipoOperacaoBanco.length; i ++){
//						if(vOperation.compareTo(vTipoOperacaoBanco[i]) == 0){
//							vIndex = i;
//							break;
//						}
//							
//					}
//					if(i == vTipoOperacaoBanco.length)
//						break;
//					continue;
//				} else{
//
//					String vVetorColuna[] = vLinha.split( OmegaConfiguration.DELIMITER_QUERY_UNIQUE_COLUNA);
//
//					if(vVetorColuna.length == 3){
//						String vIdSincronizador = vVetorColuna[0];
//						String vIdTabela = vVetorColuna[1];
//						String vIdTabelaSincronizador = vVetorColuna[2];
//						if(vIdTabelaSincronizador == null || vIdTabela == null || vIdSincronizador == null) 
//							continue;
//
//
//						switch (vIndex) {
//						//REMOVE
//						case 1:
//							ContainerTableInformation vContainerRemove = getRemoveContainerTableInformation(vIdTabelaSincronizador); 
//							if(vContainerRemove == null ){
//								vContainerRemove = new ContainerTableInformation(vIdTabelaSincronizador, EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE);
//								listRemoveContainerTableInformation.add(vContainerRemove);
//							}
//							vContainerRemove.addTupla(vIdSincronizador, vIdTabela, vObjSincronizadorPHP);
//							break;
//							//EDIT
//						case 2:
//							ContainerTableInformation vContainerEdit = getEditContainerTableInformation(vIdTabelaSincronizador); 
//							if(vContainerEdit == null ){
//								vContainerEdit = new ContainerTableInformation(vIdTabelaSincronizador, EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_EDIT);
//								listEditContainerTableInformation.add(vContainerEdit);
//							}
//							vContainerEdit.addTupla(vIdSincronizador, vIdTabela, vObjSincronizadorPHP);
//							break;
//							//INSERT
//						case 3:
//							ContainerTableInformation vContainerInsert = getInsertContainerTableInformation(vIdTabelaSincronizador); 
//							if(vContainerInsert == null ){
//								vContainerInsert = new ContainerTableInformation(vIdTabelaSincronizador, EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT);
//								listInsertContainerTableInformation.add(vContainerInsert);
//							}
//							vContainerInsert.addTupla(vIdSincronizador, vIdTabela, vObjSincronizadorPHP);
//							break;
//
//						default:
//							break;
//						}
//					} else if(vVetorColuna.length == 1){
//						String vLastIdSincronizador = vVetorColuna[0];
//						if(vLastIdSincronizador != null)
//							if(vLastIdSincronizador.length() > 0 )
//								switch (vIndex) {
//								//LAST_ID_SINCRONIZADOR
//								case 0:
//									saveUltimoIdSincronizadorAndroid(database, vLastIdSincronizador);
//									break;
//
//								default:
//									break;
//								}
//					}
//				}
//
//			}
//			
//			return true;
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
//			return false;
//		}finally{
//			if(pDatabase != null) pDatabase.close();
//		}
//		
//	}
}

