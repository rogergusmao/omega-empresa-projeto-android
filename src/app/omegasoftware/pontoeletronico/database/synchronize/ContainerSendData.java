package app.omegasoftware.pontoeletronico.database.synchronize;

import java.util.ArrayList;

public class ContainerSendData{
	public ArrayList<ContainerQueryInsert> listContainerInsert ;
	public ArrayList<ContainerQueryEXTDAO> listUpdateContainerQueryEXTDAO ;
	public ArrayList<ContainerQueryEXTDAO> listRemoveContainerQueryEXTDAO ;
	
	public ContainerSendData(){
		listContainerInsert  = new ArrayList<ContainerQueryInsert>();
		listUpdateContainerQueryEXTDAO = new ArrayList<ContainerQueryEXTDAO>();
		listRemoveContainerQueryEXTDAO  = new ArrayList<ContainerQueryEXTDAO>();
	}
	public ContainerSendData(
			ArrayList<ContainerQueryInsert> p_listContainerInsert ,
			ArrayList<ContainerQueryEXTDAO> p_listUpdate, 
			ArrayList<ContainerQueryEXTDAO> p_listRemoveContainerQueryEXTDAO){
		
		listContainerInsert  = p_listContainerInsert;
		listUpdateContainerQueryEXTDAO = p_listUpdate;
		listRemoveContainerQueryEXTDAO = p_listRemoveContainerQueryEXTDAO;
	}
	
	public void copy(ContainerSendData pContainer){
		if(pContainer == null) return;
		ArrayList<ContainerQueryInsert> vListContainerInsert = pContainer.getListContainerInsert();
		ArrayList<ContainerQueryEXTDAO> vListContainerUpdate = pContainer.getListUpdateContainerQueryEXTDAO();
		ArrayList<ContainerQueryEXTDAO> vListContainerRemove = pContainer.getListRemoveContainerQueryEXTDAO();
		listContainerInsert.addAll(vListContainerInsert);
		listUpdateContainerQueryEXTDAO.addAll(vListContainerUpdate);
		listRemoveContainerQueryEXTDAO.addAll(vListContainerRemove);
		
	}
	
	public ArrayList<ContainerQueryInsert> getListContainerInsert(){
		return listContainerInsert;
	}
	
	public ArrayList<ContainerQueryEXTDAO> getListRemoveContainerQueryEXTDAO(){
		return listRemoveContainerQueryEXTDAO;
	}
	
	public ArrayList<ContainerQueryEXTDAO> getListUpdateContainerQueryEXTDAO(){
		return listUpdateContainerQueryEXTDAO;
	}
	
}
