package app.omegasoftware.pontoeletronico.database.synchronize;

import java.util.ArrayList;

import android.content.Context;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorWebParaAndroid;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.phone.HelperPhone;



public class ContainerTableInformation {
	public static String TAG = "ContainerTableInformation";
	String idTabelaSincronizador;
	ArrayList<String> listId = new ArrayList<String>();
	ArrayList<String> listIdSincronizador= new ArrayList<String>();
	String idTipoOperacaoBanco = null;
	//Codigo da tabela na tabela sistema_tabela no PHP 
	public ContainerTableInformation(
			String pIdTabelaSincronizador, 
			String pIdTipoOperacaoBanco){
		idTabelaSincronizador = pIdTabelaSincronizador;
		idTipoOperacaoBanco = pIdTipoOperacaoBanco;
	}
	
	
	
	public String getIdSistemaTabela(){
		return idTabelaSincronizador;
	}
	
	public boolean isEmpty(){
		if(listId.size() == 0 && listIdSincronizador.size() == 0)return true;
		else return false;
	}
	
	public Table getObjTable(Database pDatabase) throws Exception{
		EXTDAOSistemaTabela vTabelaSincronizador = new EXTDAOSistemaTabela(pDatabase);
		vTabelaSincronizador.setAttrValue(EXTDAOSistemaTabela.ID, idTabelaSincronizador);
		vTabelaSincronizador.select();
		Table vObj = vTabelaSincronizador.getObjTableOfId();
		
		return vObj;
	}
	
	public boolean isTabelaSincronizador(String pIdTabelaSincronizador){
		if(idTabelaSincronizador.compareTo(pIdTabelaSincronizador) == 0){
			return true;
		} else return false;
	}
	
	public void addTupla(
			Context c, 
			String pIdSincronizador, 
			String pIdNaTabela, 
			EXTDAOSistemaRegistroSincronizadorWebParaAndroid pObj
		) throws Exception{
		listId.add(pIdNaTabela);
		listIdSincronizador.add(pIdSincronizador);
		pObj.setAttrValue(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.ID, pIdSincronizador);
		pObj.setAttrValue(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.SISTEMA_TABELA_ID_INT, idTabelaSincronizador);
		pObj.setAttrValue(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.SISTEMA_TIPO_OPERACAO_ID_INT, idTipoOperacaoBanco);
		pObj.setAttrValue(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.ID_TABELA_INT, pIdNaTabela);
		pObj.setAttrValue(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		pObj.setAttrValue(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.IMEI, HelperPhone.getIMEI(c));
		
		pObj.insert(false);
	}
	
	public ArrayList<String> getListId(){
		return listId;
	}
	
	public ArrayList<String> getListIdSincronizador(){
		return listIdSincronizador;
	}
	
	public String getListOfId(int pLimit){
		if(pLimit < 0 ) pLimit = listIdSincronizador.size();
		else if(listId.size() < pLimit) pLimit = listId.size();
		String vToken = "";
		for(int i = 0 ; i < pLimit; i++){
			String vStrId = listId.get(i);
			if(vToken.length() > 0 )
				vToken += OmegaConfiguration.DELIMITER_QUERY_COLUNA + vStrId;
			else vToken += vStrId;
		}
		return vToken;
	}
	
	public int removeListOfId(int pNumberOfItemStartForBegin, EXTDAOSistemaRegistroSincronizadorWebParaAndroid pObjSincronizadorPHP){
		try{
		if(listId.size() < pNumberOfItemStartForBegin) pNumberOfItemStartForBegin = listId.size();
		else if(pNumberOfItemStartForBegin < 0 ) pNumberOfItemStartForBegin = listId.size();
		for(int i = 0 ; i < pNumberOfItemStartForBegin; i++){
			listId.remove(0);
			String vIdSincronizador = listIdSincronizador.get(0);
			pObjSincronizadorPHP.clearData();
			pObjSincronizadorPHP.setAttrValue(EXTDAOSistemaRegistroSincronizadorWebParaAndroid.ID, vIdSincronizador);
			pObjSincronizadorPHP.select();
			try{
				
				pObjSincronizadorPHP.remove( false);
			} catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			}
			listIdSincronizador.remove(0);
		}
		return pNumberOfItemStartForBegin;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
		return pNumberOfItemStartForBegin;
		}
	}
	
	public String getStrListOfIdSincronizador(int pLimit){
		if(pLimit < 0 ) pLimit = listIdSincronizador.size();
		else if(listIdSincronizador.size() < pLimit) pLimit = listIdSincronizador.size();
		String vToken = "";
		for(int i = 0 ; i < pLimit; i++){
			String vStrId = listIdSincronizador.get(i);
			if(vToken.length() > 0 )
				vToken += OmegaConfiguration.DELIMITER_QUERY_COLUNA + vStrId;
			else vToken += vStrId;
		}
		
		return vToken;
	}
	
	
}
