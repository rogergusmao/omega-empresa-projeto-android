package app.omegasoftware.pontoeletronico.database.synchronize;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.json.JSONException;

import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.database.Attribute;
import app.omegasoftware.pontoeletronico.database.Attribute.SQLLITE_TYPE;
import app.omegasoftware.pontoeletronico.database.ContainerKey;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCrudEstado;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaHistoricoSincronizador;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroHistorico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorAndroidParaWeb;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoOperacaoBanco;
import app.omegasoftware.pontoeletronico.database.synchronize.EstadoCrud.ESTADO;
import app.omegasoftware.pontoeletronico.database.synchronize.ItemParseSincronizacao.ETAPA;
import app.omegasoftware.pontoeletronico.database.synchronize.ParseJson.PonteiroJson;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperLong;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class ControladorParseSincronizacao {
	final String TAG = "ControladorParseSincronizacao";

	public enum ORIGEM_CRUD {
		WEB(1), MOBILE(2);
		int id;

		ORIGEM_CRUD(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}
	}

	Database db=null;
	Context context;

	ParseSincronizacao parsesSincronizacao[];
	ParseSincronizacaoMobile parseSincronizacaoMobile;
	ItemParseSincronizacao itemParseSincronizacao;

	EXTDAOSistemaHistoricoSincronizador objSistemaHistoricoSincronizador;
	EXTDAOSistemaRegistroSincronizadorAndroidParaWeb objSRAW;
	EXTDAOSistemaCrudEstado objSistemaCrudEstado;
	EXTDAOSistemaRegistroHistorico objSRH;
	OmegaLog log;
	Long idUltimoRegistroAntesDeGerarBanco,
			idUltimoRegistroDepoisDeGerarBanco,
			idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco,
			idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco,
			idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
			idSincronizacaoNoMomentoDeCriacaoDoBancoSqliteLocal;
	boolean primeiraSincronizacao = false;

	public ControladorParseSincronizacao(
			Context c, 
			OmegaLog log,
			Long idUltimoRegistroAntesDeGerarBanco,
			Long idUltimoRegistroDepoisDeGerarBanco,
			Long idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco,
			Long idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco,
			Long idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
			Long idSincronizacaoNoMomentoDeCriacaoDoBancoSqliteLocal,
			boolean primeiraSincronizacao) throws JSONException, OmegaDatabaseException {
		
		this.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb = idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb;
		
		this.context = c;
		this.log = log;
		
		this.idSincronizacaoNoMomentoDeCriacaoDoBancoSqliteLocal = idSincronizacaoNoMomentoDeCriacaoDoBancoSqliteLocal;
		
		this.idUltimoRegistroAntesDeGerarBanco = idUltimoRegistroAntesDeGerarBanco;
		this.idUltimoRegistroDepoisDeGerarBanco = idUltimoRegistroDepoisDeGerarBanco;
		this.idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco = idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco;
		this.idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco = idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco;
		this.primeiraSincronizacao = primeiraSincronizacao;
		
		initDatabase();
	}

	
	public void initDatabase() throws OmegaDatabaseException{
		
		
		if(this.objSRH == null){
			Database db = new DatabasePontoEletronico(context);
			this.db = db;
			this.objSRH = new EXTDAOSistemaRegistroHistorico(db);
			this.objSistemaCrudEstado = new EXTDAOSistemaCrudEstado(db);
			this.objSRAW = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(db);
			this.objSistemaHistoricoSincronizador = new EXTDAOSistemaHistoricoSincronizador(
					db);
			this.itemParseSincronizacao = new ItemParseSincronizacao(
					objSistemaCrudEstado);
		} else {
			Database db = new DatabasePontoEletronico(context);
			this.db = db;
			this.objSRH.setDatabase(this.db);
			this.objSistemaCrudEstado.setDatabase(this.db);
			this.objSRAW.setDatabase(this.db);
			this.objSistemaHistoricoSincronizador.setDatabase(this.db);
		}
	}
	
	public void apagarDados() throws Exception {

		if (itemParseSincronizacao.crudsSincronizacao != null
				&& itemParseSincronizacao.crudsSincronizacao.length > 0) {
			this.parsesSincronizacao = new ParseSincronizacao[itemParseSincronizacao.crudsSincronizacao.length];
			for (int i = 0; i < itemParseSincronizacao.crudsSincronizacao.length; i++) {
				itemParseSincronizacao.crudsSincronizacao[i].apagarDados();
			}
		}
		if (itemParseSincronizacao.crudsSincronizacaoMobile != null)
			itemParseSincronizacao.crudsSincronizacaoMobile.apagarDados();

		itemParseSincronizacao.apagarDados(context);
	}

	public void iniciarNovaSincronizacao(Long idSincronizacaoMobile) throws JSONException {
		itemParseSincronizacao.etapa = ItemParseSincronizacao.ETAPA.INICIO;
		itemParseSincronizacao.iTabelas = 0;
		itemParseSincronizacao.salvar(context);
	}

	public void carregarDaMemoria(
			Long idSincronizacaoAtual,
			Long[] idsSincronizacao, 
			Long idSincronizacaoMobile, 
			boolean ultimaIteracaoDeArquivosDaSincronizacao)
			throws Exception {
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("carregarDaMemoria arquivos recebidos da web para finalizar a sincronizacao");
		itemParseSincronizacao.carregaDaMemoria(
				context, 
				idsSincronizacao,
				idSincronizacaoMobile, 
				ultimaIteracaoDeArquivosDaSincronizacao);
		inicializa(idSincronizacaoAtual, idSincronizacaoMobile);
	}

	public void inicializa(
			Long idSincronizacaoAtual,
			Long idSincronizacaoMobile) {
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("inicializa objetos parsers dos arquivos recebidos da web relacionados a sincronizacao e sincronizacao_mobile");
		if (itemParseSincronizacao.crudsSincronizacao != null
				&& itemParseSincronizacao.crudsSincronizacao.length > 0) {
			this.parsesSincronizacao = new ParseSincronizacao[itemParseSincronizacao.crudsSincronizacao.length];
			for (int i = 0; i < itemParseSincronizacao.crudsSincronizacao.length; i++) {
				this.parsesSincronizacao[i] = new ParseSincronizacao(
						db,
						context,
						itemParseSincronizacao.crudsSincronizacao[i].id, 
						log);
			}
		}
		if(itemParseSincronizacao.isUltimaIteracaoDeArquivoDaSincronizacao())
			this.parseSincronizacaoMobile = new ParseSincronizacaoMobile(
				this.db,
				idSincronizacaoAtual,
				itemParseSincronizacao.crudsSincronizacaoMobile.id,
				this.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb, 
				log);
	}

	public void atualizaStatusCrudsMobile(String tabela) throws Exception {
		//SOMENTE NO ULTIMO ENLACE DE SINCORNIZACAO QUE  O ARQUIVO DO MOBILE VINDO DA WEB u TRATATADO
		if (itemParseSincronizacao.isUltimaIteracaoDeArquivoDaSincronizacao()
				&& parseSincronizacaoMobile != null
				&& this.parseSincronizacaoMobile.isOpen()) {

			PonteiroJson ponteiroTabela = parseSincronizacaoMobile
					.getTabelaAtual();
			if (ponteiroTabela == null)
				this.parseSincronizacaoMobile.close();
			else {
				CrudsSincronizacao crudsSincronizacaoMobile = null;
				String tabelaAtualDaSinc = ponteiroTabela.conteudo;
				if (tabelaAtualDaSinc != null
						&& tabelaAtualDaSinc.equalsIgnoreCase(tabela)) {
					String idSistemaTabela = EXTDAOSistemaTabela
							.getIdSistemaTabela(db, tabelaAtualDaSinc);
					crudsSincronizacaoMobile = parseSincronizacaoMobile
							.mapeaOperacoesDaTabela(HelperLong
									.parserInteger(idSistemaTabela));

					parseSincronizacaoMobile.proximaTabela();

					// Atualizando estado dos cruds mobile
					if (crudsSincronizacaoMobile.hashEditar != null) {
						Set<Long> idsEdicao = crudsSincronizacaoMobile.hashEditar
								.keySet();
						Iterator<Long> itEdicao = idsEdicao.iterator();
						while (itEdicao.hasNext()) {
							Long id = itEdicao.next();
							CrudInserirEditar crud = crudsSincronizacaoMobile.hashEditar
									.get(id);
							// Integer idEstado =
							// itemParseSincronizacao.crudsSincronizacaoMobile.hashEdicao
							// .get(crud.idCrud);
							Integer idEstado = objSistemaCrudEstado
									.getIdEstadoCrud(crud.idCrud,
											crud.idCrudMobile);
							if (idEstado != null)
								crud.estado = ItemCruds.getEstado(idEstado);
							else
								crud.estado = EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO;

						}
					}

					if (crudsSincronizacaoMobile.hashInserir != null) {
						Set<Long> idsInsercao = crudsSincronizacaoMobile.hashInserir
								.keySet();
						Iterator<Long> itInsercao = idsInsercao.iterator();
						while (itInsercao.hasNext()) {
							Long id = itInsercao.next();
							CrudInserirEditar crud = crudsSincronizacaoMobile.hashInserir
									.get(id);
							// Integer idEstado =
							// itemParseSincronizacao.crudsSincronizacaoMobile.hashInsercao
							// .get(crud.idCrud);
							Integer idEstado = objSistemaCrudEstado
									.getIdEstadoCrud(crud.idCrud,
											crud.idCrudMobile);
							if (idEstado != null)
								crud.estado = ItemCruds.getEstado(idEstado);
							else
								crud.estado = EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO;
						}
					}

					if (crudsSincronizacaoMobile.hashRemover != null) {
						Set<Long> idsRemocao = crudsSincronizacaoMobile.hashRemover
								.keySet();
						Iterator<Long> itRemover = idsRemocao.iterator();
						while (itRemover.hasNext()) {
							Long id = itRemover.next();
							CrudRemover crud = crudsSincronizacaoMobile.hashRemover
									.get(id);
							// Integer idEstado =
							// itemParseSincronizacao.crudsSincronizacaoMobile.hashRemocao
							// .get(crud.idCrud);
							Integer idEstado = objSistemaCrudEstado
									.getIdEstadoCrud(crud.idCrud,
											crud.idCrudMobile);
							if (idEstado != null)
								crud.estado = ItemCruds.getEstado(idEstado);
							else
								crud.estado = EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO;
						}
					}

				}
			}

		}
	}

	public void atualizaStatusCruds(
			ArrayList<CrudsSincronizacao> crudsSincronizacaoQuePossuemATabela,
			ParsersDaTabela parsersDaTabela) throws Exception {
		// Atualizando estado dos cruds para AGUARDANDO_PROCESSAMENTO
		for (int j = 0; j < crudsSincronizacaoQuePossuemATabela.size(); j++) {
			@SuppressWarnings("unused")
			Integer jItemParseSincronizacaoCrudSinc = parsersDaTabela.indicesOriginais
					.get(j);
			CrudsSincronizacao itemParseCruds = crudsSincronizacaoQuePossuemATabela
					.get(j);
			if (itemParseCruds.hashEditar != null) {
				Set<Long> idsEdicao = itemParseCruds.hashEditar.keySet();
				Iterator<Long> itEdicao = idsEdicao.iterator();
				while (itEdicao.hasNext()) {
					Long id = itEdicao.next();
					CrudInserirEditar crud = itemParseCruds.hashEditar.get(id);

					// Integer idEstado =
					// itemParseSincronizacao.crudsSincronizacao[jItemParseSincronizacaoCrudSinc].hashEdicao
					// .get(crud.idCrud);
					Integer idEstado = objSistemaCrudEstado.getIdEstadoCrud(
							crud.idCrud, crud.idCrudMobile);
					if (idEstado != null)
						crud.estado = ItemCruds.getEstado(idEstado);
					else
						crud.estado = EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO;

				}
			}

			if (itemParseCruds.hashInserir != null) {
				Set<Long> idsInsercao = itemParseCruds.hashInserir.keySet();
				Iterator<Long> itInsercao = idsInsercao.iterator();
				while (itInsercao.hasNext()) {
					Long id = itInsercao.next();
					CrudInserirEditar crud = itemParseCruds.hashInserir.get(id);
					// Integer idEstado =
					// itemParseSincronizacao.crudsSincronizacao[jItemParseSincronizacaoCrudSinc].hashInsercao
					// .get(crud.idCrud);
					Integer idEstado = objSistemaCrudEstado.getIdEstadoCrud(
							crud.idCrud, crud.idCrudMobile);
					if (idEstado != null)
						crud.estado = ItemCruds.getEstado(idEstado);
					else
						crud.estado = EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO;
				}
			}

			if (itemParseCruds.hashRemover != null) {
				Set<Long> idsRemocao = itemParseCruds.hashRemover.keySet();
				Iterator<Long> itRemover = idsRemocao.iterator();
				while (itRemover.hasNext()) {
					Long id = itRemover.next();
					CrudRemover crud = itemParseCruds.hashRemover.get(id);
					// Integer idEstado =
					// itemParseSincronizacao.crudsSincronizacao[jItemParseSincronizacaoCrudSinc].hashRemocao
					// .get(crud.idCrud);
					Integer idEstado = objSistemaCrudEstado.getIdEstadoCrud(
							crud.idCrud, crud.idCrudMobile);
					if (idEstado != null)
						crud.estado = ItemCruds.getEstado(idEstado);
					else
						crud.estado = EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO;
				}
			}
		}
	}

	public CrudsSincronizacao mapearCrudsMobileDaTabela(String tabela)
			throws Exception {
		CrudsSincronizacao crudsSincronizacaoMobileDaTabela = null;
		if (parseSincronizacaoMobile != null) {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("mapearCrudsMobileDaTabela 1");
			if (this.parseSincronizacaoMobile.isOpen()) {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("mapearCrudsMobileDaTabela is open!!!");
				PonteiroJson ponteiroTabela = parseSincronizacaoMobile
						.getTabelaAtual();

				if (ponteiroTabela == null) {
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("ponteiro nulo");
					this.parseSincronizacaoMobile.close();
				} else {
					String tabelaAtualDaSinc = ponteiroTabela.conteudo;
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("ponteiro na tabela: " + tabelaAtualDaSinc);
					if (tabelaAtualDaSinc != null
							&& tabelaAtualDaSinc.equalsIgnoreCase(tabela)) {
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("encontrou a tabela " + tabela + "!!! ");
						String idSistemaTabela = EXTDAOSistemaTabela
								.getIdSistemaTabela(this.db,
										tabelaAtualDaSinc);
						crudsSincronizacaoMobileDaTabela = parseSincronizacaoMobile
								.mapeaOperacoesDaTabela(HelperLong
										.parserInteger(idSistemaTabela));
						parseSincronizacaoMobile.proximaTabela();
					} else {
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Nuo encontrou a tabela " + tabela
								+ "!!! ");
					}
				}
			}
		}
		return crudsSincronizacaoMobileDaTabela;
	}

	public CrudsSincronizacao[] mapearCrudsWebDaTabela(

	ArrayList<ParseSincronizacao> parsesQuePossuemATabela) throws Exception {
		if (parsesQuePossuemATabela == null
				|| parsesQuePossuemATabela.size() == 0)
			return null;
		CrudsSincronizacao[] crudsSincronizacaoQuePossuemATabela = new CrudsSincronizacao[parsesQuePossuemATabela
				.size()];

		for (int j = 0; j < parsesQuePossuemATabela.size(); j++) {
			ParseSincronizacao parse = parsesQuePossuemATabela.get(j);

			CrudsSincronizacao cruds = parse
					.mapeaOperacoesDaTabela(HelperLong
							.parserInteger(idSistemaTabela));
			parse.proximaTabela();
			crudsSincronizacaoQuePossuemATabela[j] = cruds;
		}
		return crudsSincronizacaoQuePossuemATabela;
	}

	CrudsSincronizacao[] crudsSincronizacaoQuePossuemATabela;
	CrudsSincronizacao crudsSincronizacaoMobileDaTabela;
	SincronizacaoTabela sincronizacaoTabela;
	SincronizacaoMobileTabela sincronizacaoMobileTabela;
	String tabela;
	String idSistemaTabela;
	AutoFlushOperacoesSqlite autoflushSqlite;

	ParsersDaTabela parsersDaTabela;
	String[] tabelas;

	public void movePonteirosDoArquivoDeSincronizacaoParaAProximaTabela()
			throws Exception {
		for (int i = 0; i < itemParseSincronizacao.iTabelas; i++) {

			tabela = tabelas[i];
			idSistemaTabela = EXTDAOSistemaTabela.getIdSistemaTabela(
					this.db, tabela);
			if(idSistemaTabela == null){
				throw new Exception("Id sistema tabela nulo!!!");
			}
			Integer intIdSistemaTabela = HelperLong
					.parserInteger(idSistemaTabela);
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Processando os dados da tabela " + tabela);
			// Pega todos os arquivos da sincronizacao que possuem a dados da
			// tabela
			parsersDaTabela = getParsesQuePossuemDadosDaTabela(tabela);
			ArrayList<ParseSincronizacao> parsesQuePossuemTabela = parsersDaTabela.parsers;
			ArrayList<CrudsSincronizacao> crudsSincronizacaoQuePossuemATabela = new ArrayList<CrudsSincronizacao>();
			int iParse = 0;
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Temos "
					+ String.valueOf(parsesQuePossuemTabela.size())
					+ " arquivos com dados da tabela");
			while (iParse < parsesQuePossuemTabela.size()) {

				ParseSincronizacao parse = parsesQuePossuemTabela.get(iParse);
				CrudsSincronizacao cruds = parse
						.mapeaOperacoesDaTabela(intIdSistemaTabela);
				parse.proximaTabela();
				if (cruds == null) {
					parsesQuePossuemTabela.remove(iParse);
					parsersDaTabela.indicesOriginais.remove(iParse);
					iParse--;
				} else {
					crudsSincronizacaoQuePossuemATabela.add(cruds);
					iParse++;
				}
			}
			atualizaStatusCrudsMobile(tabela);

			// Atualiza o estado para AGUARDANDO_PROCESSAMENTO
			atualizaStatusCruds(crudsSincronizacaoQuePossuemATabela,
					parsersDaTabela);

		}
	}
	@SuppressWarnings("unused")
	public Long[] atualizaSequenceLocal() throws Exception{
		Long idInicial = null;
		Long idFinal = null;
		String origem = "-";
//		String origemInicio = "-";
		if (crudsSincronizacaoMobileDaTabela != null
				&& crudsSincronizacaoMobileDaTabela.crudsInserirMobile != null) {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("atualizaSequenceLocal cruds insert");
			

			Iterator<CrudInserirEditar> it = crudsSincronizacaoMobileDaTabela.crudsInserirMobile
					.iterator();
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Atualizando sequences para evitar utilização de ID que seru ocupado recentemente");
			
			while (it.hasNext()) {
				CrudInserirEditar crudI = it.next();
				if(crudI == null) continue;
				String strIdAux = crudI.getIdFinalWeb();
				Long idAux = HelperLong.parserLong(strIdAux);
				if(idAux != null){
					if( idFinal == null || idFinal < idAux){
						idFinal = idAux;
//						origem = "idFinal crudsSincronizacaoMobileDaTabela.crudsInserirMobile";
					}
					
					if(idInicial == null || idInicial > idAux){
						idInicial = idAux;
//						origemInicio = "idInicial crudsSincronizacaoMobileDaTabela.crudsInserirMobile";
					}	
				}
			}
		}
		
		if (crudsSincronizacaoMobileDaTabela != null
				&& crudsSincronizacaoMobileDaTabela.hashEditar != null) {
			Set<Long> idsWeb = crudsSincronizacaoMobileDaTabela.hashEditar
					.keySet();
			Iterator<Long> it = idsWeb.iterator();
			
			while (it.hasNext()) {
				Long idWeb = it.next();
				CrudInserirEditar crudI = crudsSincronizacaoMobileDaTabela.hashEditar
						.get(idWeb);
				
				
				if(crudI == null) continue;
				String strIdAux = crudI.getIdFinalWeb();
				Long idAux = HelperLong.parserLong(strIdAux);
				
				if(idAux != null && (idFinal == null || idFinal < idAux)){
					idFinal = idAux;
					origem = "idFinal crudsSincronizacaoMobileDaTabela.hashEditar";
				}	
				

				if(idInicial == null || idInicial > idAux){
					idInicial = idAux;
//					origemInicio = "idInicial crudsSincronizacaoMobileDaTabela.hashEditar";
				}	
			}
		}
		
		
		ArrayList<ParseSincronizacao> parsesQuePossuemATabela = parsersDaTabela.parsers;
		if (parsesQuePossuemATabela != null){
			// Percorre todas as operacoes de inseruuo dos arquivos
			// de sincronizacao
			for (int j = 0; j < parsesQuePossuemATabela.size(); j++) {
				if (crudsSincronizacaoQuePossuemATabela != null
						&& crudsSincronizacaoQuePossuemATabela[j] != null
						&& crudsSincronizacaoQuePossuemATabela[j].hashInserir != null) {
					Set<Long> idsWeb = crudsSincronizacaoQuePossuemATabela[j].hashInserir
							.keySet();
	
					Iterator<Long> it = idsWeb.iterator();
	
					while (it.hasNext()) {
						Long idWeb = it.next();
	
						CrudInserirEditar crudI = crudsSincronizacaoQuePossuemATabela[j].hashInserir
								.get(idWeb);

						if(crudI == null) continue;
						String strIdAux = crudI.getIdFinalWeb();
						Long idAux = HelperLong.parserLong(strIdAux);
						
						if(idAux != null && (idFinal == null || idFinal < idAux)){
							idFinal = idAux;
							origem = "idFinal crudsSincronizacaoQuePossuemATabela[j].hashInserir J: " + j;
						}	
						
						if(idInicial == null || idInicial > idAux){
							idInicial = idAux;
//							origemInicio = "idInicial crudsSincronizacaoQuePossuemATabela[j].hashInserir J: " + j;
						}	
					}
				}
			}
			for (int j = 0; j < parsesQuePossuemATabela.size(); j++) {
				
				ParseSincronizacao auxParser = parsesQuePossuemATabela.get(j);
				if (crudsSincronizacaoQuePossuemATabela != null
						&& crudsSincronizacaoQuePossuemATabela[j] != null
						&& crudsSincronizacaoQuePossuemATabela[j].hashEditar != null) {
					Set<Long> idsWeb = crudsSincronizacaoQuePossuemATabela[j].hashEditar
							.keySet();
					Iterator<Long> it = idsWeb.iterator();
	
					while (it.hasNext()) {
						Long idWeb = it.next();
						CrudInserirEditar crudI = crudsSincronizacaoQuePossuemATabela[j].hashEditar
								.get(idWeb);

						if(crudI == null) continue;
						String strIdAux = crudI.getIdFinalWeb();
						Long idAux = HelperLong.parserLong(strIdAux);
						
						if(idAux != null && (idFinal == null || idFinal < idAux)){
							idFinal = idAux;
							origem = "crudsSincronizacaoQuePossuemATabela[j].hashEditar. J: " + j;
						}
						

						if(idInicial == null || idInicial > idAux){
							idInicial = idAux;
//							origemInicio = "idInicial crudsSincronizacaoQuePossuemATabela[j].hashEditar. J: " + j;
						}	
					}
				}
			}
		}
		
		if(idFinal != null){
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Atualizando sequences para id " + idFinal + " ... A origem do novo id veio de:" + origem);
			int totalAlterado = db.updateSqliteSequence(this.tabela, idFinal);
			if(totalAlterado > 0 ){ 
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("Sequence alterado!");
			}
			else if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO) log.escreveLog("Sequence nao alterado!");
			Long[] inicioEFim = new Long[] {idInicial, idFinal};
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Liberando o intervalo de identificadores ["+idInicial+":"+idFinal+"] da " + tabela + " ...");
			for(long i = idInicial; i <= idFinal; i ++){
				String strI = String.valueOf( i);
				if(objSRAW.isRegistroLocal(strI, idSistemaTabela, log)){
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Liberando o id " + tabela +"[" +  strI + "] ...");
					if(objSRAW.updateIdSynchronized(tabela, null,  strI,  null, log, true)){
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Liberado o id " + tabela +"[" +  strI + "]!");
					}
					else if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Nuo foi liberado o id " + tabela +"[" +  strI + "]!");
				}
						
			}
			
			return inicioEFim;
		} else {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Nuo atualizou a sequence da tabela pois o maxId encontrado foi nulo");
			return null;
		}
	}
	
	public InterfaceMensagem processaCrudsInsercaoSincMobile() throws Exception {
		InterfaceMensagem msg = null;
		
		if (crudsSincronizacaoMobileDaTabela != null
				&& crudsSincronizacaoMobileDaTabela.crudsInserirMobile != null
				&& parseSincronizacaoMobile != null) {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Analisando cruds insert");
			
			
			
			Iterator<CrudInserirEditar> it = crudsSincronizacaoMobileDaTabela.crudsInserirMobile
					.iterator();
			
			while (it.hasNext()) {
				
				CrudInserirEditar crudInserir = it.next();

				if (crudInserir.estado != EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO) {
					continue;
				}
				boolean erro = false;
				try{
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Processando crud insert mobile: "
							+ crudInserir.getIdentificador());
					// Atualiza os identificadores das operacoes de
					// inseruuo que foram realizadas
					parseSincronizacaoMobile.lerValoresDoCrud(crudInserir);

					msg = sincronizacaoMobileTabela.processaCrudInserir(
							crudsSincronizacaoMobileDaTabela.getCampos(),
							crudInserir, log);
					if (!msg.ok()) {
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Crud cancelado do id "
								+ crudInserir.getIdentificador());
						crudInserir.estado = ESTADO.CANCELADA;
						itemParseSincronizacao.crudsSincronizacaoMobile
								.setEstadoCrudInserirEditar(crudInserir,
								// EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT,
										log);
						erro = true;
						if (!msg.erro()) {
							
							return new Mensagem(new Exception(
									"processaCrudsEdicaoSincMobile: Resultado - "
											+ msg.getIdentificador() + ". Crud: "
											+ crudInserir.getIdentificador()));
						} else{
							return msg;
						}
					} else {

						crudInserir.estado = ESTADO.EXECUTADA;
						itemParseSincronizacao.crudsSincronizacaoMobile
								.setEstadoCrudInserirEditar(crudInserir,
								// EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT,
										log);
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("[etydg]Crud processado com sucesso do id "
								+ crudInserir.getIdentificador());
						
						
					}
				}finally{
					if(!autoflushSqlite.execute(erro, false)) return MENSAGEM_FALHA_BANCO;
				}
			}
		}
		return msg;
	}
	
	public InterfaceMensagem processaCrudsRemocaoSinc(
			Integer idMobileIdentificador) throws Exception {
		if (parsersDaTabela == null)
			return null;
		InterfaceMensagem msg = null;
		ArrayList<ParseSincronizacao> parsesQuePossuemATabela = parsersDaTabela.parsers;
		if (parsesQuePossuemATabela == null)
			return null;
		// Percorrendo as operacoes de remocao das
		// sincronizacoes
		
		
		for (int j = 0; j < parsesQuePossuemATabela.size(); j++) {
			ParseSincronizacao auxParserSincronizacao = parsesQuePossuemATabela
					.get(j);
			boolean sincronizacaoOcorreuAntesDaCriacaoDoBanco = idSincronizacaoNoMomentoDeCriacaoDoBancoSqliteLocal >= HelperLong
					.parserInt(auxParserSincronizacao.idSincronizacao);

			if (crudsSincronizacaoQuePossuemATabela != null
					&& crudsSincronizacaoQuePossuemATabela[j] != null
					&& crudsSincronizacaoQuePossuemATabela[j].hashRemover != null) {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("Analisando cruds remove do parser "
						+ auxParserSincronizacao.getIdentificador());

				Set<Long> idsWeb = crudsSincronizacaoQuePossuemATabela[j].hashRemover
						.keySet();
				// Percorrendo todas as operacoes de remocao
				// contidas no arquivo de sincronizacao
				Iterator<Long> it = idsWeb.iterator();

				while (it.hasNext()) {
					Long idWeb = it.next();
					CrudRemover crudRemover = crudsSincronizacaoQuePossuemATabela[j].hashRemover
							.get(idWeb);
					if (crudRemover.estado != EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO
							&& crudRemover.estado != EstadoCrud.ESTADO.CANCELADA)
						continue;
					boolean erro = false;
					try{
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Processando crud remover: "
								+ crudRemover.getIdentificador());
						// Verifica toda operauuo de ediuuo contida
						// nos arquivos de sincronizacao anteriores
						// atu o atual.
						for (int k = 0; k <= j; k++) {
							// Cancela todas as operacoes de edicao
							if (crudsSincronizacaoQuePossuemATabela[k].hashEditar != null
									&& crudsSincronizacaoQuePossuemATabela[k].hashEditar
											.containsKey(idWeb)) {
								CrudInserirEditar crudEditar = crudsSincronizacaoQuePossuemATabela[k].hashEditar
										.get(idWeb);
								crudEditar.estado = EstadoCrud.ESTADO.CANCELADA;
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Cancelado o crud '"
										+ crudEditar.getIdentificador()
										+ "'  . Devido ao crud '"
										+ crudRemover.getIdentificador() + "'");
								itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
										.get(k)].setEstadoCrudInserirEditar(
										crudEditar,
										// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
										log);
							}

							// Cancela todas as operacoes de edicao
							if (crudsSincronizacaoQuePossuemATabela[k].hashInserir != null
									&& crudsSincronizacaoQuePossuemATabela[k].hashInserir
											.containsKey(idWeb)) {
								CrudInserirEditar crudEditar = crudsSincronizacaoQuePossuemATabela[k].hashInserir
										.get(idWeb);
								crudEditar.estado = EstadoCrud.ESTADO.CANCELADA;
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Cancelado o crud '"
										+ crudEditar.getIdentificador()
										+ "'  . Devido ao crud '"
										+ crudRemover.getIdentificador() + "'");
								itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
										.get(k)].setEstadoCrudInserirEditar(
										crudEditar,
										// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
										log);
							}
						}

						// Cancela a verificacao dos cruds mobile de
						// inseruuo e ediuuo se existentes

						if (crudsSincronizacaoMobileDaTabela != null
								&& crudsSincronizacaoMobileDaTabela.hashEditar != null
								&& crudsSincronizacaoMobileDaTabela.hashEditar
										.containsKey(idWeb)) {
							CrudInserirEditar crudEditar = crudsSincronizacaoMobileDaTabela.hashEditar
									.get(idWeb);
							crudEditar.estado = EstadoCrud.ESTADO.CANCELADA;
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("Cancelado o crud '"
									+ crudEditar.getIdentificador()
									+ "'  . Devido ao crud '"
									+ crudRemover.getIdentificador() + "'");
							itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
									.get(j)].setEstadoCrudInserirEditar(crudEditar,
							// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
									log);
						}

						boolean crudSaiuDoProprioMobile = crudRemover.idMobileIdentificador == idMobileIdentificador
								&& HelperLong
										.parserLong(auxParserSincronizacao.idSincronizacao) > this.idSincronizacaoNoMomentoDeCriacaoDoBancoSqliteLocal
								&& !primeiraSincronizacao;

						if (!crudSaiuDoProprioMobile) {
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("Crud nao saiu do mobile - Executando o crud remover '"
									+ crudRemover.getIdentificador() + "'");
							msg = sincronizacaoTabela
									.processaCrudRemove(crudRemover);

							if (msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
									.getId()) {
								erro = msg. erro();
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Erro durante a execuuuo do crud '"
										+ crudRemover.getIdentificador() + "'. "
										+ msg.mMensagem);
								crudRemover.estado = EstadoCrud.ESTADO.CANCELADA;
								itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
										.get(j)].setEstadoCrudRemover(crudRemover);
								return msg;
							} else {
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("[5645Y6UH]Sucesso durante a execuuuo do crud '"
										+ crudRemover.getIdentificador()
										+ "'. "
										+ msg.mMensagem);
								crudRemover.estado = EstadoCrud.ESTADO.EXECUTADA;
								itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
										.get(j)].setEstadoCrudRemover(crudRemover);
								
							}

						} else if (sincronizacaoOcorreuAntesDaCriacaoDoBanco) {
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("Nuo seru executado o crud pois u proveniente do atual mobile '"
									+ crudRemover.getIdentificador() + "'");
							// So eh cancelada se o registro nem
							// mesmo existir na base local
							crudRemover.estado = EstadoCrud.ESTADO.CANCELADA;
							itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
									.get(j)].setEstadoCrudRemover(crudRemover);
						} else {
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("A remouuo seru executado na pelo SincronizadorMobile '"
									+ crudRemover.getIdentificador() + "'");

							crudRemover.estado = EstadoCrud.ESTADO.EXECUTADA;
							itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
									.get(j)].setEstadoCrudRemover(crudRemover);	
						}
					}finally{
						if(!autoflushSqlite.execute(erro, false)) return MENSAGEM_FALHA_BANCO;
					}
				}
			}
		}
		return msg;
	}

	public InterfaceMensagem processaCrudsRemocaoSincMobile() throws Exception {
		InterfaceMensagem msg = null;
		// Executando operacoes de remocao do arquivo sincmobile
		if (crudsSincronizacaoMobileDaTabela != null
				&& crudsSincronizacaoMobileDaTabela.hashRemover != null) {
			Set<Long> idsWeb = crudsSincronizacaoMobileDaTabela.hashRemover
					.keySet();
			
			Iterator<Long> it = idsWeb.iterator();
			
			while (it.hasNext()) {
				Long idWeb = it.next();
				CrudRemover crudRemover = crudsSincronizacaoMobileDaTabela.hashRemover
						.get(idWeb);

				if (crudRemover.estado != EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO)
					continue;
				try{
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("processaCrudsRemocaoSincMobile - Processando crud: "
							+ crudRemover.getIdentificador());
					if (crudsSincronizacaoQuePossuemATabela != null) {
						// Verifica toda operauuo de ediuuo contida nos
						// arquivos de sincronizacao e cancela
						for (int k = 0; k < crudsSincronizacaoQuePossuemATabela.length; k++) {
							// Cancela todas as operacoes de edicao
							if (crudsSincronizacaoQuePossuemATabela[k].hashEditar != null
									&& crudsSincronizacaoQuePossuemATabela[k].hashEditar
											.containsKey(idWeb)) {
								CrudInserirEditar crudEditarAux = crudsSincronizacaoQuePossuemATabela[k].hashEditar
										.get(idWeb);
								crudEditarAux.estado = EstadoCrud.ESTADO.CANCELADA;
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Cancelado o crud '"
										+ crudEditarAux.getIdentificador()
										+ "'  . Devido ao crud '"
										+ crudRemover.getIdentificador() + "'");
								itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
										.get(k)].setEstadoCrudInserirEditar(
										crudEditarAux,
										// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
										log);
							}

							// Cancela todas as operacoes de edicao
							if (crudsSincronizacaoQuePossuemATabela[k].hashInserir != null
									&& crudsSincronizacaoQuePossuemATabela[k].hashInserir
											.containsKey(idWeb)) {
								CrudInserirEditar crudInserirAux = crudsSincronizacaoQuePossuemATabela[k].hashInserir
										.get(idWeb);
								crudInserirAux.estado = EstadoCrud.ESTADO.CANCELADA;
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Cancelado o crud '"
										+ crudInserirAux.getIdentificador()
										+ "'  . Devido ao crud '"
										+ crudRemover.getIdentificador() + "'");
								itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
										.get(k)].setEstadoCrudInserirEditar(
										crudInserirAux,
										// EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT,
										log);
							}
						}
					}

					// Cancela a verificacao dos cruds mobile de
					// ediuuo se existentes
					if (crudsSincronizacaoMobileDaTabela != null
							&& crudsSincronizacaoMobileDaTabela.hashEditar != null
							&& crudsSincronizacaoMobileDaTabela.hashEditar
									.containsKey(idWeb)) {
						CrudInserirEditar crudEditar = crudsSincronizacaoMobileDaTabela.hashEditar
								.get(idWeb);
						crudEditar.estado = EstadoCrud.ESTADO.CANCELADA;
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Cancelado o crud '"
								+ crudEditar.getIdentificador()
								+ "'  . Devido ao crud '"
								+ crudRemover.getIdentificador() + "'");
						itemParseSincronizacao.crudsSincronizacaoMobile
								.setEstadoCrudInserirEditar(crudEditar,
								// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
										log);
					}
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Sucesso durante a realizacao do processamento '"
							+ crudRemover.getIdentificador()
							+ ". Obs.: Essa operauuo nunca implica em erro no mobile.");
					crudRemover.estado = EstadoCrud.ESTADO.EXECUTADA;
					
					
				}finally{
					if(!autoflushSqlite.execute(false,false)) return MENSAGEM_FALHA_BANCO;
				}
			}
		}
		return msg;
	}

	public InterfaceMensagem processaCrudsEdicaoSincMobile() throws Exception {
		InterfaceMensagem msg = null;
		
		// Executando as opeacoes de edicao do arquivo
		// sinc_mobile
		if (crudsSincronizacaoMobileDaTabela != null
				&& crudsSincronizacaoMobileDaTabela.hashEditar != null
				&& parseSincronizacaoMobile != null) {
			Set<Long> idsWeb = crudsSincronizacaoMobileDaTabela.hashEditar
					.keySet();
			Iterator<Long> it = idsWeb.iterator();

			while (it.hasNext()) {
				
				Long idWeb = it.next();
				CrudInserirEditar crudEditar = crudsSincronizacaoMobileDaTabela.hashEditar
						.get(idWeb);
				if (crudEditar.estado != EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO)
					continue;
				boolean erro = false;
				try{
					parseSincronizacaoMobile.lerValoresDoCrud(crudEditar);
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("processaCrudsEdicaoSincMobile - Processando crud: "
							+ crudEditar.getIdentificador());
					if (crudsSincronizacaoQuePossuemATabela != null) {
						// Verifica toda operauuo de ediuuo contida nos
						// arquivos de sincronizacao e cancela
						for (int k = 0; k < crudsSincronizacaoQuePossuemATabela.length; k++) {
							// Verifica se a operacao de insercao do registro
							// atualmente editado
							// esta contida em algum dos arquivos
							// caso positivo essa seru cancelada, pois a ediuuo
							// contem os dados
							// mais recentes relacionados ao registro
							if (crudsSincronizacaoQuePossuemATabela[k].hashInserir != null
									&& crudsSincronizacaoQuePossuemATabela[k].hashInserir
											.containsKey(idWeb)) {
								CrudInserirEditar crudInserirAux = crudsSincronizacaoQuePossuemATabela[k].hashInserir
										.get(idWeb);
								crudInserirAux.estado = EstadoCrud.ESTADO.CANCELADA;
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Cancelado o crud (Marcador-jhbgkifyuktyf) '"
										+ crudInserirAux.getIdentificador()
										+ "'  . Devido ao crud '"
										+ crudEditar.getIdentificador() + "'");
								itemParseSincronizacao.crudsSincronizacaoMobile
										.setEstadoCrudInserirEditar(crudInserirAux,
										// EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT,
												log);
							}
						}
					}
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Executando o crud editar 1as56d4f '"
							+ crudEditar.getIdentificador() + "'");
					msg = sincronizacaoMobileTabela.processaCrudEditar(
							crudsSincronizacaoMobileDaTabela.getCampos(),
							crudEditar, log);
					if (!msg.ok()) {
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Ocorreu um erro durante a execuuuo do crud '"
								+ crudEditar.getIdentificador()
								+ "' - "
								+ msg.mMensagem);
						crudEditar.estado = ESTADO.CANCELADA;

						itemParseSincronizacao.crudsSincronizacaoMobile
								.setEstadoCrudInserirEditar(crudEditar,
								// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
										log);
						erro = true;
						if (!msg.erro()) {
							
							return new Mensagem(new Exception(
									"processaCrudsEdicaoSincMobile: Resultado - "
											+ msg.toJson() + ". Crud: "
											+ crudEditar.getIdentificador()));
						} else
							return msg;

					} else {
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("[4OIJFG]Sucesso durante a execuuuo do crud '"
								+ crudEditar.getIdentificador());
						crudEditar.estado = ESTADO.EXECUTADA;
						
					}
				}finally{
					if(!autoflushSqlite.execute(erro, false)) return MENSAGEM_FALHA_BANCO;
				}
			}
		}

		return msg;
	}
	public class CrudRemocaoMaisRecente{
		public CrudRemover crudRemoverMaisRecente = null;
		public CrudsSincronizacao crudsSincMaisRecente = null;
		public ParseSincronizacao parseSincronizacaoMaisRecente = null;
		public int k = 0;

		public CrudRemocaoMaisRecente(int k, CrudRemover crudEditarMaisRecente,
				CrudsSincronizacao crudsSincMaisRecente,
				ParseSincronizacao parseSincronizacaoMaisRecente) {
			this.k = k;
			this.crudRemoverMaisRecente = crudEditarMaisRecente;
			this.crudsSincMaisRecente = crudsSincMaisRecente;
			this.parseSincronizacaoMaisRecente = parseSincronizacaoMaisRecente;
		}
	}
	public class CrudMaisRecente {
		public CrudInserirEditar crudEditarMaisRecente = null;
		public CrudsSincronizacao crudsSincMaisRecente = null;
		public ParseSincronizacao parseSincronizacaoMaisRecente = null;
		public int k = 0;

		public CrudMaisRecente(int k, CrudInserirEditar crudEditarMaisRecente,
				CrudsSincronizacao crudsSincMaisRecente,
				ParseSincronizacao parseSincronizacaoMaisRecente) {
			this.k = k;
			this.crudEditarMaisRecente = crudEditarMaisRecente;
			this.crudsSincMaisRecente = crudsSincMaisRecente;
			this.parseSincronizacaoMaisRecente = parseSincronizacaoMaisRecente;
		}
	}

	public CrudMaisRecente getCrudEdicaoMaisRecente(int iParserInicioBusca,
			Long idWeb, CrudInserirEditar crudAtual) throws Exception {
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
		log.escreveLog("getCrudEdicaoMaisRecente. Crud atual: " + crudAtual.getIdentificador() + ". Id Web Desejado: " + idWeb);
		ArrayList<ParseSincronizacao> parsesQuePossuemATabela = parsersDaTabela.parsers;
		// Verifica a existencia de uma operacao de edicao posterior a de
		// insercao, procurando o dado mais recente relativo ao registro
		CrudInserirEditar crudEditarMaisRecente = null;
		CrudsSincronizacao crudsSincMaisRecente = null;
		ParseSincronizacao parseSincronizacaoMaisRecente = null;
		// Faz a busca atu o sincronizador atual, se
		// no pruprio sincronizador atual
		// existir uma operauuo de ediuuo, essa teru
		// valores mais recentes do que
		// a operauuo de inseruuo.
		CrudMaisRecente c = null;
		if(crudsSincronizacaoQuePossuemATabela != null){
			for (int k = parsesQuePossuemATabela.size() - 1; k >= 0; k--) {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("Buscando no arquivo: " + parsesQuePossuemATabela.get(k).getIdentificador() + " ...");
				if (crudsSincronizacaoQuePossuemATabela[k]== null
						|| crudsSincronizacaoQuePossuemATabela[k].hashEditar == null)
					continue;
				else if (crudsSincronizacaoQuePossuemATabela[k].hashEditar
						.containsKey(idWeb)) {
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Encontrou!!!" );
					parseSincronizacaoMaisRecente = parsesQuePossuemATabela.get(k);

					crudEditarMaisRecente = crudsSincronizacaoQuePossuemATabela[k].hashEditar
							.get(idWeb);
					if (crudEditarMaisRecente == crudAtual){
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Mas nuo vale pois u o mesmo que o crud de entrada!" );
						continue;
					} else if(crudEditarMaisRecente != null && crudEditarMaisRecente.estado != EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO){
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Mas nuo vale por conta do seu estado: " + crudEditarMaisRecente.getIdentificador() );
						continue;
					} else if(c == null){
						//se ainda nuo foi encontrado o crud mais recente de ediuuo relacionado ao registro
						crudsSincMaisRecente = crudsSincronizacaoQuePossuemATabela[k];
						
						c = new CrudMaisRecente(k, crudEditarMaisRecente,
								crudsSincMaisRecente, parseSincronizacaoMaisRecente);
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("getCrudEdicaoMaisRecente. Crud editar mais recente encontrado: " 
								+ crudEditarMaisRecente.getIdentificador() );
					} else {
						//se o registor de edicao mais recente ju foi encontrado, entuo todo outro registro de ediuuo
						//deve ser descartado
						CrudInserirEditar crudCancelado =crudsSincronizacaoQuePossuemATabela[k].hashEditar
								.get(idWeb);
						if(crudCancelado.estado == ESTADO.AGUARDANDO_PROCESSAMENTO){
							crudCancelado.estado=ESTADO.CANCELADA;
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("Cancelado crud antigo relacionado ao registro... Crud Cancelado: " + crudCancelado.getIdentificador());
						}
					}
				}
			}
		}
		
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO && c == null )log.escreveLog("Nuo encontrou" );
		return c;
	}
	

	public void cancelarCrudsDoRegistro(int iParserInicioBusca, Long idWeb) throws Exception {
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("cancelarCrudsDoRegistro. Registro ["+idWeb+"]");
		ArrayList<ParseSincronizacao> parsesQuePossuemATabela = parsersDaTabela.parsers;
		if(crudsSincronizacaoQuePossuemATabela != null){
			for (int k = parsesQuePossuemATabela.size() - 1; k >= 0; k--) {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("Cancelando no arquivo: " + parsesQuePossuemATabela.get(k).getIdentificador() + " ...");
				if (crudsSincronizacaoQuePossuemATabela[k]== null
						|| crudsSincronizacaoQuePossuemATabela[k].hashEditar == null)
					continue;
				else if (crudsSincronizacaoQuePossuemATabela[k].hashEditar
						.containsKey(idWeb)) {
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Cancelando ..." );
					
					//se o registor de edicao mais recente ju foi encontrado, entuo todo outro registro de ediuuo
					//deve ser descartado
					CrudInserirEditar crudCancelado =crudsSincronizacaoQuePossuemATabela[k].hashEditar
							.get(idWeb);
					if(crudCancelado.estado == ESTADO.AGUARDANDO_PROCESSAMENTO){
						crudCancelado.estado=ESTADO.CANCELADA;
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("[sdfoiasdfimo] Crud Cancelado: " + crudCancelado.getIdentificador());
					}
				}
			}
		}
	}


	public CrudRemocaoMaisRecente getCrudRemocaoMaisRecente(int iParserInicioBusca,
			Long idWeb, CrudInserirEditar crudAtual) throws Exception {
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("getCrudRemocaoMaisRecente. Crud atual: " + crudAtual.getIdentificador() + ". Id Web Desejado: " + idWeb);
		ArrayList<ParseSincronizacao> parsesQuePossuemATabela = parsersDaTabela.parsers;
		if(parsesQuePossuemATabela == null) return null;
		// Verifica a existencia de uma operacao de edicao posterior a de
		// insercao, procurando o dado mais recente relativo ao registro
		CrudRemover crudRemoverMaisRecente = null;
		CrudsSincronizacao crudsSincMaisRecente = null;
		ParseSincronizacao parseSincronizacaoMaisRecente = null;
		// Faz a busca atu o sincronizador atual, se
		// no pruprio sincronizador atual
		// existir uma operauuo de ediuuo, essa teru
		// valores mais recentes do que
		// a operauuo de inseruuo.
		if(crudsSincronizacaoQuePossuemATabela != null){
			for (int k = parsesQuePossuemATabela.size() - 1; k >= 0; k--) {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("Buscando no arquivo: " + parsesQuePossuemATabela.get(k).getIdentificador() + " ...");
				if (crudsSincronizacaoQuePossuemATabela[k] == null
						|| crudsSincronizacaoQuePossuemATabela[k].hashRemover == null)
					continue;
				else if (crudsSincronizacaoQuePossuemATabela[k].hashRemover
						.containsKey(idWeb)) {
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Encontrou!!!");
					parseSincronizacaoMaisRecente = parsesQuePossuemATabela.get(k);
		
					crudRemoverMaisRecente = crudsSincronizacaoQuePossuemATabela[k].hashRemover
							.get(idWeb);
					
					if (crudRemoverMaisRecente != null && crudRemoverMaisRecente.estado != EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO){
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Mas nuo vale por conta do seu estado: " + crudRemoverMaisRecente.getIdentificador() );
						continue;
					}
					
					crudsSincMaisRecente = crudsSincronizacaoQuePossuemATabela[k];
					
					CrudRemocaoMaisRecente c = new CrudRemocaoMaisRecente(k, crudRemoverMaisRecente,
							crudsSincMaisRecente, parseSincronizacaoMaisRecente);
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("getCrudRemocaoMaisRecente. Crud remouuo mais recente encontrado: "
							+ crudRemoverMaisRecente.getIdentificador() );
					return c;
				}
			}
		}
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("Nuo encontrou" );
		return null;
	}

	
	public InterfaceMensagem atualizacaoDosDadosDoRegistroDuplicadoQueNaoPossuiOpInsertNaFilaLocalSeExistirDadosWebDoReg(
			HashSet<Long> idsWebMapeados, int jParserAtual,
			Long idWebDuplicado, String[] camposCrudQueFalhou,
			CrudInserirEditar crudQueFalhou) throws Exception {
		
		boolean idWebDuplicadoLocal = objSRAW.isRegistroLocal(idWebDuplicado.toString(), idSistemaTabela, log);
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("atualizacaoDosDadosDoRegistroDuplicadoQueNaoPossuiOpInsertNaFilaLocalSeExistirDadosWebDoReg. Id web duplicado: " + idWebDuplicado
				+ ". Crud que falhou: " + crudQueFalhou.getIdentificador());
		idsWebMapeados.add(crudQueFalhou.idTabelaWeb);
		if(!idWebDuplicadoLocal){
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[lkenrerwer4] O id da web duplicada " + idWebDuplicado + " corresponde a um registro nuo local, ou seja, ju foi sincronizado anteriormente.");
			CrudRemocaoMaisRecente c2 = getCrudRemocaoMaisRecente(jParserAtual, idWebDuplicado, crudQueFalhou);
			if(c2 != null){
				InterfaceMensagem msg = sincronizacaoTabela.processaCrudRemove(c2.crudRemoverMaisRecente);

				if (msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
						.getId()) {
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("[lkenrer]Erro durante a execuuuo do crud '"
							+ c2.crudRemoverMaisRecente.getIdentificador() + "'. "
							+ msg.mMensagem);
					c2.crudRemoverMaisRecente.estado = EstadoCrud.ESTADO.CANCELADA;
					itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
							.get(c2.k)].setEstadoCrudRemover(c2.crudRemoverMaisRecente);
					return msg;
				} else {
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("[lkenrwerer]Sucesso durante a execuuuo do crud '"
							+ c2.crudRemoverMaisRecente.getIdentificador()
							+ "'. "
							+ msg.mMensagem);
					c2.crudRemoverMaisRecente.estado = EstadoCrud.ESTADO.EXECUTADA;
					itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
							.get(c2.k)].setEstadoCrudRemover(c2.crudRemoverMaisRecente);
					return new Mensagem(
							PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
				}
			}	
		}
		
		CrudMaisRecente crudMaisRecenteDoRegistroDuplicado = getCrudEdicaoMaisRecente(
				jParserAtual, idWebDuplicado, crudQueFalhou);
		if (crudMaisRecenteDoRegistroDuplicado == null) {
			return new Mensagem(
					new Exception(
							"O registro u duplicado com um registro que foi inserido localmente e que NuO ESTu NA LISTA PARA SER SINCRONIZADO. "
									+ " Tentamos verificar se esses registro suo pelo menos iguais, pois caso positivo nuo teria problema nuo executar o crud. Porum eles suo diferentes."
									+ ". DADOS VINDOS DO SINCRONIZADOR: "
									+ crudQueFalhou.getIdentificador()
									+ ". ID REGISTRO DUPLICADO LOCAL FORA DA LISTA PARA SINCRONIZAuuO: "
									+ idWebDuplicado));
		} 
		
		crudMaisRecenteDoRegistroDuplicado.parseSincronizacaoMaisRecente
				.lerValoresDoCrud(crudMaisRecenteDoRegistroDuplicado.crudEditarMaisRecente);
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("Processando outro crud da atual sincronizacao para tentar resolver o problema. Crud sendo executado: " + crudMaisRecenteDoRegistroDuplicado.crudEditarMaisRecente.getIdentificador() + " ...");
		InterfaceMensagem msg = sincronizacaoTabela.processaCrudEdit(
				crudMaisRecenteDoRegistroDuplicado.crudsSincMaisRecente
						.getCampos(),
				crudMaisRecenteDoRegistroDuplicado.crudEditarMaisRecente);

		if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
				.getId()) {
			crudMaisRecenteDoRegistroDuplicado.crudEditarMaisRecente.estado = ESTADO.EXECUTADA;

			itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
					.get(crudMaisRecenteDoRegistroDuplicado.k)]
					.setEstadoCrudInserirEditar(
							crudMaisRecenteDoRegistroDuplicado.crudEditarMaisRecente,
							// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
							log);

			return new Mensagem(
					PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
		} else if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC
				.getId()) {
			// Tentou executar a ediuuo no registro local permanente(id
			// definido) que implicava em uma duplicidade
			// na atual operauuo de ediuuo que chamou essa rotina para tentar
			// liberar a chave unica
			// com uma possuvel ediuuo vinda nesse ultimo pacote de
			// sincronizacao
			RetornoOperacaoAForca retorno = (RetornoOperacaoAForca) msg;
			Long idWebDuplicadoFilho = HelperLong
					.parserLong(retorno.registroDuplicado.getId());
			if (!idsWebMapeados.contains(idWebDuplicadoFilho)) {
				// ele tambum possui o mesmo problema do pai, existe um registro
				// que ocasiona duplicidade nele
				
				msg = atualizacaoDosDadosDoRegistroDuplicadoQueNaoPossuiOpInsertNaFilaLocalSeExistirDadosWebDoReg(
						idsWebMapeados,
						jParserAtual,
						idWebDuplicadoFilho,
						crudMaisRecenteDoRegistroDuplicado.crudsSincMaisRecente.getCampos(),
						crudMaisRecenteDoRegistroDuplicado.crudEditarMaisRecente);
				if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
						.getId()) {
					crudMaisRecenteDoRegistroDuplicado.parseSincronizacaoMaisRecente
							.lerValoresDoCrud(crudMaisRecenteDoRegistroDuplicado.crudEditarMaisRecente);

					msg = sincronizacaoTabela
							.processaCrudEdit(
									crudMaisRecenteDoRegistroDuplicado.crudsSincMaisRecente.camposEditar,
									crudMaisRecenteDoRegistroDuplicado.crudEditarMaisRecente);

					if (msg != null
							&& msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
									.getId()) {
						crudMaisRecenteDoRegistroDuplicado.crudEditarMaisRecente.estado = ESTADO.EXECUTADA;

						itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
								.get(crudMaisRecenteDoRegistroDuplicado.k)]
								.setEstadoCrudInserirEditar(
										crudMaisRecenteDoRegistroDuplicado.crudEditarMaisRecente,
										// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
										log);

						return new Mensagem(
								PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
					} else
						return msg;

				} else
					return msg;

			} else {

				Iterator<Long> it = idsWebMapeados.iterator();
				StringBuilder sbIds = new StringBuilder();
				while (it.hasNext())
					sbIds.append(it.next() + ", ");

				return new Mensagem(
						new Exception(
								"Durante a tentativa de correuuo do crud abaixo que possui "
										+ ". DADOS VINDOS DO SINCRONIZADOR: "
										+ crudQueFalhou.getIdentificador()
										+ ". ID REGISTRO DUPLICADO LOCAL FORA DA LISTA PARA SINCRONIZAuuO: "
										+ idWebDuplicado
										+ ". ID REGISTRO FILHO DO DUPLICADO LOCAL FORA DA LISTA PARA SINCRONIZAuuO: "
										+ idWebDuplicadoFilho
										+ ". Temos um ciclo durante a tentativa de correuuo da ediuuo, segue os identficadores: "
										+ sbIds.toString()));
			}
		} else
			return msg;

	}

	public InterfaceMensagem processaCrudsInsercaoSinc(
			Integer idMobileIdentificador) throws Exception {
		InterfaceMensagem msg = null;
		if (parsersDaTabela == null)
			return null;
		ArrayList<ParseSincronizacao> parsesQuePossuemATabela = parsersDaTabela.parsers;
		if (parsesQuePossuemATabela == null)
			return null;
		// Percorre todas as operacoes de inseruuo dos arquivos
		// de sincronizacao
		
		for (int j = 0; j < parsesQuePossuemATabela.size(); j++) {
			ParseSincronizacao auxParserSincronizacao = parsesQuePossuemATabela
					.get(j);
			if (crudsSincronizacaoQuePossuemATabela != null
					&& crudsSincronizacaoQuePossuemATabela[j] != null
					&& crudsSincronizacaoQuePossuemATabela[j].hashInserir != null) {
				Set<Long> idsWeb = crudsSincronizacaoQuePossuemATabela[j].hashInserir
						.keySet();
				boolean sincronizacaoOcorreuAntesDaCriacaoDoBanco = idSincronizacaoNoMomentoDeCriacaoDoBancoSqliteLocal >= HelperLong
						.parserInt(auxParserSincronizacao.idSincronizacao);

				Iterator<Long> it = idsWeb.iterator();

				while (it.hasNext()) {
					Long idWeb = it.next();

					CrudInserirEditar crudInserir = crudsSincronizacaoQuePossuemATabela[j].hashInserir
							.get(idWeb);
					if (crudInserir.estado != EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO)
						continue;
					boolean erro = false;
					try{
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("processaCrudsInsercaoSinc - Processando crud: "
								+ crudInserir.getIdentificador());

						//
						if (sincronizacaoOcorreuAntesDaCriacaoDoBanco) {
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("O crud ocorreu antes da criacao do banco. Logo estamos cancelando o crud '"
									+ crudInserir.getIdentificador());

							crudInserir.estado = ESTADO.CANCELADA;
							itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
									.get(j)].setEstadoCrudInserirEditar(
									crudInserir,
									// EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT,
									log);

							if (msg != null
									&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
											.getId()){
								
								erro = msg. erro();
								return msg;
							}
						} else if(primeiraSincronizacao){
							//verifica se o registro u proveniente do .db inicial
							//e se caso ocorreu uma remocao local do registro
							Long idOpRemocaoNaoSinc = objSRAW.getId(String.valueOf(idWeb), idSistemaTabela, EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE, false, log);
							if(idOpRemocaoNaoSinc != null){
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("O registro "  + tabela + "["+idWeb+"] u proveniente do .db e estamos na primeira sincornizacao. O que aconteceu aqui foi que esse registro foi removido antes da primeira sincornizacao ter sido finalizada. Logo esse crud de insert deve ser desconsiderado.");
								
								crudInserir.estado = ESTADO.CANCELADA;
								itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
										.get(j)].setEstadoCrudInserirEditar(
										crudInserir,
										// EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT,
										log);
								
								
								cancelarCrudsDoRegistro(j, idWeb);
								
								continue;
							}
						}
						CrudMaisRecente crudEdicaoMaisRecente = getCrudEdicaoMaisRecente(
								j, idWeb, crudInserir);
						// Se a edicao mais recente ja foi executada
						// entao na hu a necessidade do crud atual ser executado
						// Duas condicioniais sao analisadas para finalizar a
						// questao
						// a primeira verifica se o registro de insert ja veio com
						// os dados do banco na primeira sincornizacao. Casdo
						// positivo considerados o crud de insert como executado
						if (crudEdicaoMaisRecente != null
								&& crudEdicaoMaisRecente.crudEditarMaisRecente.estado != null
								&& crudEdicaoMaisRecente.crudEditarMaisRecente.estado != EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO) {
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("Crud processado com sucesso do id "
									+ crudInserir.getIdentificador());

							crudInserir.estado = ESTADO.EXECUTADA;
							itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
									.get(j)].setEstadoCrudInserirEditar(
									crudInserir,
									// EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT,
									log);

							continue;
						}

						boolean crudInsertSaiuDoProprioMobile = crudInserir.idMobileIdentificador == idMobileIdentificador
								&& HelperLong
										.parserLong(auxParserSincronizacao.idSincronizacao) > this.idSincronizacaoNoMomentoDeCriacaoDoBancoSqliteLocal
								&& !primeiraSincronizacao;
					    if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					    	log.escreveLog("O crud insert saiu do proprio mobile? "
								+ crudInsertSaiuDoProprioMobile);
						if (!crudInsertSaiuDoProprioMobile) {
							// se o crud nao saiu do proprio mobile e ainda nao foi
							// executado no mobile
							// se possuir um crud mais recente relacionado ao
							// idTabelaWeb
							// o mesmo devera executar com os valores do crud editar

							// Verifica a existencia de uma operacao de edicao
							// posterior a de
							// insercao que ainda nao foi executada,
							// procurando o dado mais recente relativo ao registro

							if (crudEdicaoMaisRecente != null
									&& (crudEdicaoMaisRecente.crudEditarMaisRecente.estado == null || crudEdicaoMaisRecente.crudEditarMaisRecente.estado == EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO)) {
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Transformando o crud mais recente '"
										+ crudEdicaoMaisRecente.crudEditarMaisRecente
												.getIdentificador()
										+ "' em um crud INSERT. Setando o idTabelaMobile para "
										+ crudInserir.idTabelaMobile);
								crudEdicaoMaisRecente.crudEditarMaisRecente.idTabelaMobile = crudInserir.idTabelaMobile;

								msg = processaCrudInsercaoSinc(
										parsesQuePossuemATabela,
										crudEdicaoMaisRecente.k,
										crudEdicaoMaisRecente.crudEditarMaisRecente,
										idMobileIdentificador,
										sincronizacaoOcorreuAntesDaCriacaoDoBanco);
								if (msg != null
										&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
												.getId()){
									
									erro = msg.erro();
									
									return retornoErro(msg, j, crudInserir,
											crudEdicaoMaisRecente);
								}
								else {
									if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
										log.escreveLog("Crud processado com sucesso do id[sdgftert] "
											+ crudInserir.getIdentificador());

									crudInserir.estado = ESTADO.EXECUTADA;
									itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
											.get(j)].setEstadoCrudInserirEditar(
											crudInserir,
											// EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT,
											log);

									continue;
								}
							}
						}

						msg = processaCrudInsercaoSinc(parsesQuePossuemATabela, j,
								crudInserir, idMobileIdentificador,
								sincronizacaoOcorreuAntesDaCriacaoDoBanco);
						if (msg != null
								&& msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
										.getId()){
							erro = msg.erro();
							return retornoErro(msg, j, crudInserir,
									crudEdicaoMaisRecente);
						}
						else
							continue;									
					}finally{
						if(!autoflushSqlite.execute(erro, false)) return MENSAGEM_FALHA_BANCO;
					}
					

				}
			}
		}
		return msg;
	}

	public InterfaceMensagem retornoErro(InterfaceMensagem msg, int j,
			CrudInserirEditar crudInserir, CrudMaisRecente crudEdicaoMaisRecente)
			throws Exception {
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
			log.escreveLog("[245687321]Crud cancelado do id "
					+ crudInserir.getIdentificador());
			log.escreveLog(msg.toJson());	
		}
		
		crudInserir.estado = ESTADO.CANCELADA;
		itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
				.get(j)].setEstadoCrudInserirEditar(crudInserir,
		// EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT,
				log);
		if (crudEdicaoMaisRecente != null) {
			crudEdicaoMaisRecente.crudEditarMaisRecente.estado = ESTADO.CANCELADA;
			itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
					.get(crudEdicaoMaisRecente.k)].setEstadoCrudInserirEditar(
					crudEdicaoMaisRecente.crudEditarMaisRecente,
					// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
					log);

		}

		if (msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId()) {
			return new Mensagem(new Exception(
					"retornoErro[21657]: Resultado - "
							+ msg.getIdentificador() + ". Crud: "
							+ crudInserir.getIdentificador()));
		}
		return msg;
	}

	public InterfaceMensagem processaCrudInsercaoSinc(
			ArrayList<ParseSincronizacao> parsesQuePossuemATabela, int j,
			CrudInserirEditar crudInserir, Integer idMobileIdentificador,
			boolean sincronizacaoOcorreuAntesDaCriacaoDoBanco) throws Exception {
		ParseSincronizacao auxParser = parsesQuePossuemATabela.get(j);
		InterfaceMensagem msg = null;
		boolean crudSaiuDoProprioMobile = crudInserir.idMobileIdentificador == idMobileIdentificador
				&& HelperLong.parserLong(auxParser.idSincronizacao) > this.idSincronizacaoNoMomentoDeCriacaoDoBancoSqliteLocal
				&& !primeiraSincronizacao;
		if (!crudSaiuDoProprioMobile) {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("O crud nuo saiu do moble em questuo. Lendo valores do crud dentro do arquiv...");
			auxParser.lerValoresDoCrud(crudInserir);
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Executando o crud inserir '"
					+ crudInserir.getIdentificador() + "'");
			msg = sincronizacaoTabela.processaCrudInserir(
					crudsSincronizacaoQuePossuemATabela[j].getCampos(),
					crudInserir, crudSaiuDoProprioMobile);
			
			if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC
					.getId()) {
				//Confere se existe algum  
				
				// Se o registro que ocasionou a duplicada nao possui nenhum
				// dado no historico
				RetornoOperacaoAForca retorno = (RetornoOperacaoAForca) msg;

				HashSet<Long> hashIdsWebMapeadosPelaCorrecao = new HashSet<Long>();
				InterfaceMensagem msgTentativaAtualizacaoDadoMaisRecente = atualizacaoDosDadosDoRegistroDuplicadoQueNaoPossuiOpInsertNaFilaLocalSeExistirDadosWebDoReg(
						hashIdsWebMapeadosPelaCorrecao, 
						j,
						HelperLong.parserLong(retorno.registroDuplicado.getId()),
						crudsSincronizacaoQuePossuemATabela[j].getCampos(),
						crudInserir);
				boolean tentarRollbackEmCasoDeErro = false;
				// faz uma nova tentativa da operauuo
				if (msgTentativaAtualizacaoDadoMaisRecente.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
						.getId()) {
					msg = sincronizacaoTabela
							.processaCrudInserir(
									crudsSincronizacaoQuePossuemATabela[j].camposInserir,
									crudInserir, crudSaiuDoProprioMobile);
					if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC
							.getId()) {
						tentarRollbackEmCasoDeErro = true;
					}
				} else {
					tentarRollbackEmCasoDeErro = true;
				}

				if (tentarRollbackEmCasoDeErro) {
					InterfaceMensagem msgFallback = objSRH
							.updateRegistroComDadosAntigoSeExistirNoHistoricoDeEdicao(
									objSRAW, retorno.registroDuplicado, log);
					if (msgFallback.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
							.getId()) {
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Tentantiva de correuuo do crud '"
								+ crudInserir.getIdentificador()
								+ " realizando fallback foi realizada com sucesso.");
						msg = sincronizacaoTabela
								.processaCrudInserir(
										crudsSincronizacaoQuePossuemATabela[j].camposInserir,
										crudInserir, crudSaiuDoProprioMobile);

					}else {
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("[brtq2r4423]Resultado da tentantiva de correuuo do crud '"
								+ crudInserir.getIdentificador()
								+ ": " + msgFallback.toJson());
						
						msg = processaRegistroDuplicadoNaForca(retorno.registroDuplicado);
						
						if(msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()){
							msg = sincronizacaoTabela
									.processaCrudInserir(
											crudsSincronizacaoQuePossuemATabela[j].camposInserir,
											crudInserir, crudSaiuDoProprioMobile);
							if (msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
									.getId()) {
								msg = new Mensagem(
										PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
										"[87e9547t]Nuo foi possuvel realizar a inseruuo: "
												+ crudInserir
														.getIdentificador() + ". Msg: " + msg.getIdentificador());
							}
						}
					}
				}
			}

			if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
					.getId()) {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("[EGSDFG]Sucesso durante a execuuuo do crud '"
						+ crudInserir.getIdentificador());
				objSistemaHistoricoSincronizador.gravaCrudInserirEditar(
						crudInserir,
						parsesQuePossuemATabela.get(j).datetimeSql,
						idSistemaTabela);
				crudInserir.estado = ESTADO.EXECUTADA;
				itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
						.get(j)].setEstadoCrudInserirEditar(crudInserir,
				// EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT,
						log);

				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
					InterfaceMensagem msgValidacao = sincronizacaoTabela
							.validaCrudInserirEditar(crudInserir);
					if (msgValidacao == null)
						log.escreveLog("Crud validado "
								+ crudInserir.toString());
					else
						log.escreveLog("Crud invalidado "
								+ crudInserir.toString() + " - "
								+ msgValidacao.mMensagem);	
				
					
				}

			} else {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("Ocorreu um erro durante a execuuuo do crud '"
						+ crudInserir.getIdentificador() + "' - "
						+ msg.mMensagem);
				crudInserir.estado = ESTADO.CANCELADA;
				itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
						.get(j)].setEstadoCrudInserirEditar(crudInserir,
				// crudInserir.tipo,
						log);
				if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC
						.getId()) {
					msg = new Mensagem(
							new Exception(
									"Erro durante o processamento do crud "
											+ crudInserir.getIdentificador()
											+ ". Ultima tentativa resultou em REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC"));
				}

				return msg;
			}
		} else if (sincronizacaoOcorreuAntesDaCriacaoDoBanco) {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Nuo seru executado o crud pois u proveniente do atual mobile e seru CANCELADO'"
					+ crudInserir.getIdentificador() + "'");
			crudInserir.estado = ESTADO.CANCELADA;

		} else {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Nuo seru executado na funcao processaCrudsInsercaoSinc o crud pois u proveniente do atual mobile, mas sera na processaCrudsInsercaoSincMobile[kjawshei] '"
					+ crudInserir.getIdentificador() + "'");

			// FEITO ONTEM, ACREDITANDO QUE OS CRUDS INSERTS BEM SUCEDIDOS SO
			// ESTAVAM NOS ARQUIVOS SINC E NuO NO SINC_MOBILE
			// ME ENGANEI
			// log.escreveLog("Seru executado na SincronizacaoMobile pois u proveniente do atual mobile '"
			// + crudInserir
			// .getIdentificador()
			// + "'");
			//
			// msg = sincronizacaoMobileTabela
			// .processaCrudInserir(
			// crudsSincronizacaoQuePossuemATabela[j].camposInserir,
			// crudInserir,
			// log);
			// if (msg.mCodRetorno !=
			// PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
			// .getId()) {
			// log.escreveLog("Crud cancelado do id "
			// + crudInserir.getIdentificador());
			// crudInserir.estado = ESTADO.CANCELADA;
			// itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
			// .get(j)]
			// .setEstadoCrudInserir(crudInserir,log);
			//
			// if(msg.mCodRetorno !=
			// PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId()){
			// return new Mensagem(new
			// Exception("processaCrudsEdicaoSincMobile: Resultado - " +
			// msg.getIdentificador() + ". Crud: " +
			// crudInserir.getIdentificador()));
			// } else
			// return msg;
			// } else {
			//
			// log.escreveLog("Crud processado com sucesso do id "
			// + crudInserir.getIdentificador());
			// crudInserir.estado = ESTADO.EXECUTADA;
			// itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
			// .get(j)]
			// .setEstadoCrudInserir(crudInserir,log);
			//
			//
			// }

		}
		return null;

	}

	public InterfaceMensagem processaRegistroDuplicadoNaForca(Table registroDuplicado){
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("[lwk3merotw3et] WARNING - O registro duplicado teru uma trativa imaginando que nas pruximas sincronizauues o seu valor seru regularizado.",
					new Date(), 
					Log.WARN);
		ContainerKey ck = registroDuplicado.getContainerUniqueKey();
		String[] attrsUnique = ck.getVetorAttribute();
		for (String strAttr : attrsUnique) {
			Attribute attr = registroDuplicado.getAttribute(strAttr);
			if(attr != null ){
//				if(attr.isNull()){
//					registroDuplicado.setAttrValue(strAttr, null);
//					break;
//				}
//				else
				
				if(!attr.isFK() && !attr.isAutoIncrement() && !attr.isPrimaryKey()){
					if(attr.isNormalizado())
						attr = attr.getAttributeNormalizado();
					SQLLITE_TYPE type = attr.getSQLType();
					switch(type){
					
					case INT:
					case INTEGER:
					case DOUBLE:
					case FLOAT:
					case REAL:
					case BLOB:
					case LONG:
						
						Integer rand = HelperInteger.getRandom(attr.getSize());
						
						attr.setStrValue(String.valueOf(rand));	
					
						break;
					case TEXT:
					case TINYTEXT:
					case CHAR:
					case VARCHAR:
						String strRand = HelperString.generateRandomString(attr.getSize());
						
						attr.setStrValue(strRand);
						break;
						default:
							break;
					}
				}
						 
			}
			
		}
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("[lwk3merotw3et] WARNING - " + registroDuplicado.getIdentificadorRegistro(), new Date(), Log.WARN);
		registroDuplicado.formatToSQLite();
		if(registroDuplicado.update(false)){
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[lwk3merotw3et] WARNING - tratativa realizada com sucesso.", new Date(), Log.WARN);
			return new Mensagem(
					PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, 
					"[imj08f032409]Realizada com ressalvas!");
		} else {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[lwk3merotw3et] WARNING - falha durante a tatativa!", new Date(), Log.WARN);
			return new Mensagem(
		
				PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, 
				"[onwet3094tr3] Erro durante o processamento do registro duplicado");
		}
	}
	
	public InterfaceMensagem processaCrudsEdicaoSinc(
			Integer idMobileIdentificador) throws Exception {
		if (parsersDaTabela == null)
			return null;
		InterfaceMensagem msg = null;
		ArrayList<ParseSincronizacao> parsesQuePossuemATabela = parsersDaTabela.parsers;
		if (parsesQuePossuemATabela == null)
			return null;
		
		// Percorre todas as opeacoes de edicao do arquivo de
		// sincronizacao
		for (int j = 0; j < parsesQuePossuemATabela.size(); j++) {
			ParseSincronizacao auxParser = parsesQuePossuemATabela.get(j);
			if (crudsSincronizacaoQuePossuemATabela != null
					&& crudsSincronizacaoQuePossuemATabela[j] != null
					&& crudsSincronizacaoQuePossuemATabela[j].hashEditar != null) {
				Set<Long> idsWeb = crudsSincronizacaoQuePossuemATabela[j].hashEditar
						.keySet();
				Iterator<Long> it = idsWeb.iterator();

				while (it.hasNext()) {
					
					Long idWeb = it.next();
					CrudInserirEditar crudEditar = crudsSincronizacaoQuePossuemATabela[j].hashEditar
							.get(idWeb);
					if (crudEditar.estado != EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO)
						continue;
					boolean erro = false;
					try{
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("processaCrudsEdicaoSinc - Processando crud: "
								+ crudEditar.getIdentificador());
	
						auxParser.lerValoresDoCrud(crudEditar);
						boolean sincronizacaoOcorreuAntesDaCriacaoDoBanco = idSincronizacaoNoMomentoDeCriacaoDoBancoSqliteLocal >= HelperLong
								.parserInt(auxParser.idSincronizacao);
	
						// A operacao ja foi executada na base local
	
						CrudMaisRecente crudEdicaoMaisRecente = getCrudEdicaoMaisRecente(
								j, idWeb, crudEditar);
						if (crudEdicaoMaisRecente != null) {
							boolean crudSaiuDoProprioMobile = crudEdicaoMaisRecente.crudEditarMaisRecente.idMobileIdentificador == idMobileIdentificador
									&& HelperLong
											.parserLong(crudEdicaoMaisRecente.parseSincronizacaoMaisRecente.idSincronizacao) > this.idSincronizacaoNoMomentoDeCriacaoDoBancoSqliteLocal
									&& !primeiraSincronizacao;
							// Se a operacao mais recente ju foi
							// executada no banco local
							if ((crudEdicaoMaisRecente.crudEditarMaisRecente.estado != EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO)
									|| crudSaiuDoProprioMobile) {
								crudEditar.estado = ESTADO.CANCELADA;
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Cancelado o crud '"
										+ crudEditar.getIdentificador()
										+ "' pois ju foi realizado o crud mais recente "
										+ crudEdicaoMaisRecente.crudEditarMaisRecente
												.getIdentificador());
								itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
										.get(j)].setEstadoCrudInserirEditar(
										crudEditar,
										// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
										log);
	
								continue;
							} else {
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
									log.escreveLog("Cancelado o crud '"
										+ crudEditar.getIdentificador()
										+ "' para realizar o crud mais recente '"
										+ crudEdicaoMaisRecente.crudEditarMaisRecente
												.getIdentificador() + "'");
									log.escreveLog("Executando o crud editar mais recente '"
										+ crudEdicaoMaisRecente.crudEditarMaisRecente
												.getIdentificador() + "'");
								}
								crudEdicaoMaisRecente.parseSincronizacaoMaisRecente
										.lerValoresDoCrud(crudEdicaoMaisRecente.crudEditarMaisRecente);
								msg = sincronizacaoTabela
										.processaCrudEdit(
												crudEdicaoMaisRecente.crudsSincMaisRecente
														.getCampos(),
												crudEdicaoMaisRecente.crudEditarMaisRecente);
								// Como nuo tem registro de operauuo insert na fila
								// sistema_registro_sincroniizador_android_para_web
								// logo ele u permanente, ou seja, ju estu com o id
								// sincronizado com o banco web.
								// Entuo ele pode ter sido editado localmente o que
								// estu resultando na duplicidade com os dados do
								// crud
								// vindos do sincronizador
								if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC
										.getId()) {
	
									RetornoOperacaoAForca retorno = (RetornoOperacaoAForca) msg;
									Long idDuplicado = HelperLong
											.parserLong(retorno.registroDuplicado
													.getId());
									if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
										log.escreveLog("Tentantiva de correuuo do crud '"
											+ crudEditar.getIdentificador()
											+ " procurando operauues de edit mais recentes para liberar a duplicada "
											+ idDuplicado);
									HashSet<Long> idsWebMapeados = new HashSet<Long>();
									InterfaceMensagem msgTentativaAtualizacaoMaisRecente = atualizacaoDosDadosDoRegistroDuplicadoQueNaoPossuiOpInsertNaFilaLocalSeExistirDadosWebDoReg(
											idsWebMapeados,
											crudEdicaoMaisRecente.k,
											idDuplicado,
											crudEdicaoMaisRecente.crudsSincMaisRecente
													.getCampos(),
											crudEdicaoMaisRecente.crudEditarMaisRecente);
									boolean tentarRollbackEmCasoDeErro = false;
									if (msgTentativaAtualizacaoMaisRecente.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
											.getId()) {
										if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
											log.escreveLog("Tentantiva de correuuo do crud '"
												+ crudEditar.getIdentificador()
												+ " procurando operauues de edit mais recentes para liberar a duplicada realizada com sucesso.");
										// conseguiu alterar os dados do registro
										// que atrapalha a execucao da ediuuo
										// devido a duplicidade da chave unica.
										// Agora com os novos dados nesse registro
										// a ediuuo nuo vai implicar em duplicata
										msg = sincronizacaoTabela
												.processaCrudEdit(
														crudEdicaoMaisRecente.crudsSincMaisRecente.camposEditar,
														crudEdicaoMaisRecente.crudEditarMaisRecente);
										if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC
												.getId())
											tentarRollbackEmCasoDeErro = true;
									} else
										tentarRollbackEmCasoDeErro = true;
	
									if (tentarRollbackEmCasoDeErro) {
										if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
											log.escreveLog("Tentantiva de correuuo do crud '"
												+ crudEditar.getIdentificador()
												+ " realizando fallback do registro duplicado se existir op de ediuuo em processo de sincronizada ou a ser sincronizada.");
										// Nuo foi possuvel resolver com o processo
										// de busca de dados vindos do sincronizador
										// para liberar a chave unica da duplicada
										// Nesse caso tentamos o fallback do
										// registro da duplicada
	
										InterfaceMensagem msgFallback = objSRH
												.updateRegistroComDadosAntigoSeExistirNoHistoricoDeEdicao(
														objSRAW,
														retorno.registroDuplicado,
														log);
										
										if (msgFallback.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
												.getId()) {
											if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
												log.escreveLog("Tentantiva de correuuo do crud '"
													+ crudEditar.getIdentificador()
													+ " realizando fallback foi realizada com sucesso.");
											msg = sincronizacaoTabela
													.processaCrudEdit(
															crudEdicaoMaisRecente.crudsSincMaisRecente.camposEditar,
															crudEdicaoMaisRecente.crudEditarMaisRecente);
											if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC
													.getId()) {
												msg = new Mensagem(
														PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
														"[c2323efwrgf]Nuo foi possuvel realizar a ediuuo: "
																+ crudEdicaoMaisRecente.crudEditarMaisRecente
																		.getIdentificador());
											}
										} else {
											if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
												log.escreveLog("[brt]Resultado da tentantiva de correuuo do crud '"
													+ crudEditar.getIdentificador()
													+ ": " + msgFallback.toJson());
											msg= processaRegistroDuplicadoNaForca(retorno.registroDuplicado);
											if(msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()){
												msg = sincronizacaoTabela
														.processaCrudEdit(
																crudEdicaoMaisRecente.crudsSincMaisRecente.camposEditar,
																crudEdicaoMaisRecente.crudEditarMaisRecente);
												if (msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {
													msg = new Mensagem(
															PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
															"[342949ew7]Nuo foi possuvel realizar a ediuuo: "
																	+ crudEdicaoMaisRecente.crudEditarMaisRecente
																			.getIdentificador() + ". Msg: " + msg.getIdentificador());
												}
											}
											
										}
									}
	
								}
								
								if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
										.getId()) {
									if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
										log.escreveLog("[LKERNLK]Sucesso durante a execuuuo do crud '"
											+ crudEditar.getIdentificador());
	
									objSistemaHistoricoSincronizador
											.gravaCrudInserirEditar(
													crudEdicaoMaisRecente.crudEditarMaisRecente,
													crudEdicaoMaisRecente.parseSincronizacaoMaisRecente.datetimeSql,
													idSistemaTabela);
	
									crudEdicaoMaisRecente.crudEditarMaisRecente.estado = ESTADO.EXECUTADA;
									crudEditar.estado = ESTADO.CANCELADA;
									itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
											.get(crudEdicaoMaisRecente.k)]
											.setEstadoCrudInserirEditar(
													crudEdicaoMaisRecente.crudEditarMaisRecente,
													// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
													log);
									itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
											.get(j)].setEstadoCrudInserirEditar(
											crudEditar,
											// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
											log);
	
								} else {
									if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
										log.escreveLog("Ocorreu um erro durante a execuuuo do crud '"
											+ crudEditar.getIdentificador()
											+ "' - " + msg.getIdentificador());
									crudEditar.estado = ESTADO.CANCELADA;
									itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
											.get(j)].setEstadoCrudInserirEditar(
											crudEditar,
											// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
											log);
									if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC
											.getId()) {
										msg = new Mensagem(
												new Exception(
														"Erro durante o processamento do crud "
																+ crudEditar
																		.getIdentificador()
																+ ". Ultima tentativa resultou em REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC"));
									}
									erro = msg.erro();
									return msg;
								}
	
							}
	
						} else {
							boolean crudSaiuDoProprioMobile = crudEditar.idMobileIdentificador == idMobileIdentificador
									&& HelperLong
											.parserLong(auxParser.idSincronizacao) > this.idSincronizacaoNoMomentoDeCriacaoDoBancoSqliteLocal
									&& !primeiraSincronizacao;
							if (!crudSaiuDoProprioMobile) {
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Executando o crud editar [sdtytr] '"
										+ crudEditar.getIdentificador() + "'");
								msg = sincronizacaoTabela
										.processaCrudEdit(
												crudsSincronizacaoQuePossuemATabela[j].camposEditar,
												crudEditar);
	
								if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC
										.getId()) {
									RetornoOperacaoAForca retorno = (RetornoOperacaoAForca) msg;
									HashSet<Long> idsWebMapeados = new HashSet<Long>();
									InterfaceMensagem msgTentativaAtualizacaoComCrudMaisRecente = atualizacaoDosDadosDoRegistroDuplicadoQueNaoPossuiOpInsertNaFilaLocalSeExistirDadosWebDoReg(
											idsWebMapeados,
											j,
											HelperLong
													.parserLong(retorno.registroDuplicado
															.getId()),
											crudsSincronizacaoQuePossuemATabela[j].camposEditar,
											crudEditar);
									boolean tentarRollbackEmCasoDeErro = false;
									if (msgTentativaAtualizacaoComCrudMaisRecente.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
											.getId()) {
										msg = sincronizacaoTabela
												.processaCrudEdit(
														crudsSincronizacaoQuePossuemATabela[j].camposEditar,
														crudEditar);
										if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC
												.getId()) {
	
											tentarRollbackEmCasoDeErro = true;
										}
									} else {
										tentarRollbackEmCasoDeErro = true;
									}
	
									if (tentarRollbackEmCasoDeErro) {
										InterfaceMensagem msgFallback = objSRH
												.updateRegistroComDadosAntigoSeExistirNoHistoricoDeEdicao(
														objSRAW,
														retorno.registroDuplicado,
														log);
										if (msgFallback.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
												.getId()) {
											if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
												log.escreveLog("Tentantiva de correuuo do crud '"
													+ crudEditar.getIdentificador()
													+ " realizando fallback foi realizada com sucesso.");
											msg = sincronizacaoTabela
													.processaCrudEdit(
															crudsSincronizacaoQuePossuemATabela[j].camposEditar,
															crudEditar);
	
										}
									}
								}
	
								if (msg.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
										.getId()) {
									if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
										log.escreveLog("Ocorreu um erro durante a execuuuo do crud '"
											+ crudEditar.getIdentificador()
											+ "' - " + msg.mMensagem);
	
									crudEditar.estado = ESTADO.CANCELADA;
									itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
											.get(j)].setEstadoCrudInserirEditar(
											crudEditar,
											// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
											log);
									erro = msg.erro();
									return msg;
								} else {
									if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
										log.escreveLog("[LKMSDFLKJ]Sucesso durante a execuuuo do crud '"
											+ crudEditar.getIdentificador());
									objSistemaHistoricoSincronizador
											.gravaCrudInserirEditar(
													crudEditar,
													parsesQuePossuemATabela.get(j).datetimeSql,
													idSistemaTabela);
	
									crudEditar.estado = ESTADO.EXECUTADA;
									itemParseSincronizacao.crudsSincronizacao[parsersDaTabela.indicesOriginais
											.get(j)].setEstadoCrudInserirEditar(
											crudEditar,
											// EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
											log);
								}
							} else if (sincronizacaoOcorreuAntesDaCriacaoDoBanco) {
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Nuo seru executado o crud pois u proveniente do atual mobile '"
										+ crudEditar.getIdentificador() + "'");
								crudEditar.estado = ESTADO.CANCELADA;
								continue;
							} else {
								// log.escreveLog("Nuo seru executado na funcao processaCrudsEdicaoSinc o crud pois u proveniente do atual mobile, mas sera executado na funcao processaCrudsEdicaoSincMobile '"
								// + crudEditar
								// .getIdentificador()
								// + "'");
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
									log.escreveLog("A ediuuo seru executado na SincronizacaoMobile pois u proveniente do atual mobile '"
											+ crudEditar.getIdentificador() + "'");
		
									if (crudsSincronizacaoMobileDaTabela == null
											|| crudsSincronizacaoMobileDaTabela.hashEditar == null
											|| !crudsSincronizacaoMobileDaTabela.hashEditar
													.containsKey(crudEditar.idTabelaWeb)) {
										// throw new
										// Exception("Os cruds mobile deveriam ter mapeado o crud: "
										// + crudInserir.idCrud);
										log.escreveLog("SincronizacaoMobile nuo possui o id tabela web "
												+ crudEditar.idTabelaWeb
												+ " proveniente do crud editar");
									}
								}
								continue;
							}
	
						}
					}finally{
						if(!autoflushSqlite.execute(erro, false)) return MENSAGEM_FALHA_BANCO;
					}
				}
			}
		}
		return msg;
	}

	final Mensagem MENSAGEM_FALHA_BANCO = new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_SEM_SER_EXCECAO, "Nuo foi possuvel montar a transauuo do banco");
	
	public InterfaceMensagem parse(Integer idMobileIdentificador)
			throws Exception {
		int limiteI = itemParseSincronizacao.crudsSincronizacao == null ? 1 : itemParseSincronizacao.crudsSincronizacao.length;
		
		if(itemParseSincronizacao.indiceInicioCrudsSincronizacao == null){
			itemParseSincronizacao.indiceInicioCrudsSincronizacao = 0;
			
			itemParseSincronizacao.indiceFimCrudsSincronizacao = limiteI > ItemParseSincronizacao.LIMITE_ARQUIVO_SINCROINACAO_POR_ITERACAO 
				? ItemParseSincronizacao.LIMITE_ARQUIVO_SINCROINACAO_POR_ITERACAO - 1
				: limiteI - 1;
		
			itemParseSincronizacao.salvar(context);
		}
		
		boolean ultimaIteracao = false;
		while(itemParseSincronizacao.indiceFimCrudsSincronizacao <= limiteI - 1
						&& !ultimaIteracao){
			if(itemParseSincronizacao.indiceFimCrudsSincronizacao == limiteI - 1)
				ultimaIteracao = true;
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("itemParseSincronizacao: " + itemParseSincronizacao.getIdentificador());
			
			InterfaceMensagem msg = leCabecalho();
			if (msg != null && (msg.erro() || msg.erroDownload())){
				return msg;
			}else {
				try{
					tabelas = getTabelasContidasNosArquivosDeSincronizacao();

					if (tabelas == null || tabelas.length == 0) {
						apagaArquivosDaIteracao();
						if(itemParseSincronizacao.isUltimaIteracaoDeArquivoDaSincronizacao()){
							final String token = "Nuo existem dados a serem sincronizados proveniente dos arquivos recebidos da web.";
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog(token);
							return new Mensagem(PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO, token);					
						} else {
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("vai para o proximo conjunto de sincronizacao!!");
							//vai para o proximo conjunto de sincronizacao
							continue;
						}
					} else {
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Tabelas contidas nas sincronizacoes: " + HelperString.arrayToString(tabelas));
					}

					if (itemParseSincronizacao.iTabelas >= tabelas.length ) {
						apagaArquivosDaIteracao();
						if(itemParseSincronizacao.isUltimaIteracaoDeArquivoDaSincronizacao()){
							
							itemParseSincronizacao.etapa = ItemParseSincronizacao.ETAPA.FIM;
							itemParseSincronizacao.salvar(context);
							final String token = "Todos os dados recebidos da web foram processados. Ao todo "
									+ String.valueOf(tabelas.length) + " tabelas";
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog(token);
							return new Mensagem(
									PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
						} else {
							continue;
						}
					}

					// Movendo os ponteiros nos arquivos de sincronizacao
					movePonteirosDoArquivoDeSincronizacaoParaAProximaTabela();

					if (itemParseSincronizacao.etapa != ItemParseSincronizacao.ETAPA.FIM) {
									
						for (; itemParseSincronizacao.iTabelas < tabelas.length; itemParseSincronizacao.iTabelas++) {
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("[kndlfos435oij]Executando " 
									+ itemParseSincronizacao.iTabelas 
									+ " de " 
									+ tabelas.length + " tabelas.");
							
							itemParseSincronizacao.salvar(context);

							tabela = tabelas[itemParseSincronizacao.iTabelas];

							idSistemaTabela = EXTDAOSistemaTabela.getIdSistemaTabela(
									db, tabela);
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("Verificando quais arquivos recebidos da sincronizacao web possuem a tabela "
									+ tabela);
							if(idSistemaTabela == null){
								throw new Exception("[jkjefgg] Sistema tabela nulo.");
							}
							// Pega todos os arquivos da sincronizacao que possuem a dados
							// da tabela
							parsersDaTabela = getParsesQuePossuemDadosDaTabela(tabela);

							ArrayList<ParseSincronizacao> parsesQuePossuemATabela = parsersDaTabela == null ? null
									: parsersDaTabela.parsers;
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
								if (parsesQuePossuemATabela != null
										&& parsesQuePossuemATabela.size() >= 0)
									log.escreveLog("Ao todo temos "
											+ parsesQuePossuemATabela.size()
											+ " parsers que possuem o registros de sincornizacao da tabela: "
											+ tabela);
								else 
									log.escreveLog("Nao temos parsers nessa sincronizacao que possuem a tabela: " + tabela);
							}
							// faz a leitura no arquivo recebido da web referente aos
							// arquivos web recebidos. No caso de resete, ou de
							// sincronizacao por um telefone que nuo a realiza a bastante
							// tempo
							// constumam ter mais de um arquivo para sincronizacao
							crudsSincronizacaoQuePossuemATabela = mapearCrudsWebDaTabela(parsesQuePossuemATabela);
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
								if (crudsSincronizacaoQuePossuemATabela == null)
									log.escreveLog("Nuo existem cruds web para a tabela");
								else
									log.escreveLog("Mapeado os cruds web para a tabela");
							}
							// faz a leitura no arquivo recebido da web referente ao arquivo
							// enviado pelo mobile no inicio da sincronizacao

							crudsSincronizacaoMobileDaTabela = mapearCrudsMobileDaTabela(tabela);
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
								if (crudsSincronizacaoMobileDaTabela == null)
									log.escreveLog("Nuo existem cruds mobile para a tabela");
								else
									log.escreveLog("Mapeado os cruds mobile");
							}
							if (crudsSincronizacaoQuePossuemATabela == null
									&& crudsSincronizacaoMobileDaTabela == null) {
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Como nuo existem cruds a serem executados nem do arquivo de sincornizacao moblie nem no da web, entuo prosseguiremos para a proxima tabela");
								continue;
							}

							sincronizacaoTabela = new SincronizacaoTabela(
									context,
									db,
									tabela,
									log,
									this.idUltimoRegistroAntesDeGerarBanco,
									this.idUltimoRegistroDepoisDeGerarBanco,
									this.idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco,
									this.idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco,
									this.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb);

							sincronizacaoMobileTabela = new SincronizacaoMobileTabela(
									context,
									db,
									tabela,
									log,
									this.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb);
							boolean validade = true;
							try {
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Inciando transaction no banco de dados ");
								autoflushSqlite= new AutoFlushOperacoesSqlite(sincronizacaoTabela.db, log); 
								if(!sincronizacaoTabela.db.beginTransaction())
									return MENSAGEM_FALHA_BANCO;
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Etapa "
										+ itemParseSincronizacao.etapa.toString());
								
								// sincronizacaoTabela.db.setPragmaOff();
								if (itemParseSincronizacao.etapa == ItemParseSincronizacao.ETAPA.INICIO
										|| itemParseSincronizacao.etapa == ItemParseSincronizacao.ETAPA.CRUDS_INSERCAO_SINC_MOBILE) {
									if(itemParseSincronizacao.etapa == ItemParseSincronizacao.ETAPA.INICIO){
										itemParseSincronizacao.etapa = ItemParseSincronizacao.ETAPA.CRUDS_INSERCAO_SINC_MOBILE;
										itemParseSincronizacao.salvar(context);
									}
									//TODO com a utilizacao de numero negativo para inseruues locais a atualizacao da sequence para prevenca de duplicadade deixou de ser um problema
									//Long[] inicioEFim = atualizaSequenceLocal();
									
									msg = processaCrudsInsercaoSincMobile();
									if(msg != null){
										if (msg.erro()){
											return msg;
										}
										if(!autoflushSqlite.execute(false,true)) return MENSAGEM_FALHA_BANCO;
									}

									
									itemParseSincronizacao.etapa = ItemParseSincronizacao.ETAPA.CRUDS_REMOCAO_SINC;
									itemParseSincronizacao.salvar(context);
								}

								if (itemParseSincronizacao.etapa == ItemParseSincronizacao.ETAPA.CRUDS_REMOCAO_SINC) {

									msg = processaCrudsRemocaoSinc(idMobileIdentificador);
									if(msg != null){
										if (msg.erro())
											return msg;
										if(!autoflushSqlite.execute(false,true)) return MENSAGEM_FALHA_BANCO;	
									}
									itemParseSincronizacao.etapa = ItemParseSincronizacao.ETAPA.CRUDS_REMOCAO_SINC_MOBILE;
									itemParseSincronizacao.salvar(context);
								}

								if (itemParseSincronizacao.etapa == ItemParseSincronizacao.ETAPA.CRUDS_REMOCAO_SINC_MOBILE) {

									msg = processaCrudsRemocaoSincMobile();
									if(msg != null){
										if (msg.erro())
											return msg;
										if(!autoflushSqlite.execute(false,true)) return MENSAGEM_FALHA_BANCO;	
									}
									itemParseSincronizacao.etapa = ItemParseSincronizacao.ETAPA.CRUDS_EDICAO_SINC_MOBILE;
									itemParseSincronizacao.salvar(context);
								}

								if (itemParseSincronizacao.etapa == ItemParseSincronizacao.ETAPA.CRUDS_EDICAO_SINC_MOBILE) {

									msg = processaCrudsEdicaoSincMobile();
									if(msg != null){
										if (msg.erro())
											return msg;
										if(!autoflushSqlite.execute(false,true)) return MENSAGEM_FALHA_BANCO;	
									}
									itemParseSincronizacao.etapa = ItemParseSincronizacao.ETAPA.CRUDS_INSERCAO_SINC;
									itemParseSincronizacao.salvar(context);
								}

								if (itemParseSincronizacao.etapa == ItemParseSincronizacao.ETAPA.CRUDS_INSERCAO_SINC) {

									msg = processaCrudsInsercaoSinc(idMobileIdentificador);
									if(msg != null){
										if (msg.erro())
											return msg;
										if(!autoflushSqlite.execute(false,true)) return MENSAGEM_FALHA_BANCO;	
									}

									itemParseSincronizacao.etapa = ItemParseSincronizacao.ETAPA.CRUDS_EDICAO_SINC;
									itemParseSincronizacao.salvar(context);
								}

								if (itemParseSincronizacao.etapa == ItemParseSincronizacao.ETAPA.CRUDS_EDICAO_SINC) {

									msg = processaCrudsEdicaoSinc(idMobileIdentificador);
									if(msg != null){
										if (msg.erro())
											return msg;
										if(!autoflushSqlite.execute(false,true)) return MENSAGEM_FALHA_BANCO;	
									}

									itemParseSincronizacao.etapa = ItemParseSincronizacao.ETAPA.INICIO;
									itemParseSincronizacao.salvar(context);
								}

							} catch (Exception ex) {
								validade = false;
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Ocorreu uma excecao: "
										+ HelperExcecao.getDescricao(ex));
								SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
								return new Mensagem(ex);
							} finally {
								String strMsg = "Transauuo do parser de sincronizacao web/mobile para a tabela: "
										+ tabela + " foi concluido com ";
								if (validade)
									strMsg = "[OK] " + strMsg + " SUCESSO";
								else
									strMsg = "[ERRO] " + strMsg + " ERRO";
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog(strMsg);
								sincronizacaoTabela.db.endTransaction(true);
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
									if (msg != null )
										log.escreveLog("[fge234sg]Ultima mensagem de processamento do arquivo: " + msg.toJson());
									log.escreveLog("[kndlfosegoij]Finalizou a execuuuo da tabela da iteracao " + itemParseSincronizacao.iTabelas + " de " + (tabelas.length - 1)+ " tabelas.");
								}
							}
						}
					}
					
				}finally{
					fechaArquivos();
					
					if ((tabelas == null || tabelas.length == 0)
							|| (itemParseSincronizacao.iTabelas >= tabelas.length && itemParseSincronizacao.etapa == ETAPA.INICIO )
							|| itemParseSincronizacao.etapa == ItemParseSincronizacao.ETAPA.FIM){
						apagaArquivosDaIteracao();
						itemParseSincronizacao.indiceInicioCrudsSincronizacao = itemParseSincronizacao.indiceFimCrudsSincronizacao + 1;
						itemParseSincronizacao.indiceFimCrudsSincronizacao = 
							limiteI > itemParseSincronizacao.indiceFimCrudsSincronizacao + ItemParseSincronizacao.LIMITE_ARQUIVO_SINCROINACAO_POR_ITERACAO 					
								? itemParseSincronizacao.indiceFimCrudsSincronizacao + ItemParseSincronizacao.LIMITE_ARQUIVO_SINCROINACAO_POR_ITERACAO 
								: limiteI - 1;
						itemParseSincronizacao.etapa = ETAPA.INICIO;
						itemParseSincronizacao.iTabelas = 0;
					}
					itemParseSincronizacao.salvar(context);
				}
			}
			
		}
		itemParseSincronizacao.etapa = ItemParseSincronizacao.ETAPA.FIM;
		itemParseSincronizacao.salvar(context);
		
		return new Mensagem(
				PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
				"A sincronizacao foi finalizada com sucesso.");
	}

	public class ParsersDaTabela {
		ArrayList<ParseSincronizacao> parsers;
		ArrayList<Integer> indicesOriginais;
	}

	public ParsersDaTabela getParsesQuePossuemDadosDaTabela(String tabela)
			throws Exception {
		ArrayList<ParseSincronizacao> parsesQuePossuemATabela = new ArrayList<ParseSincronizacao>();
		ArrayList<Integer> indicesOriginal = new ArrayList<Integer>();
		if(parsesSincronizacao != null  && parsesSincronizacao.length > 0){
			for (int j = itemParseSincronizacao.indiceInicioCrudsSincronizacao; 
					j <= itemParseSincronizacao.indiceFimCrudsSincronizacao
							&& j < parsesSincronizacao.length; 
					j++) {
				if (!this.parsesSincronizacao[j].isOpen())
					continue;
				PonteiroJson tabelaAtualDaSinc = this.parsesSincronizacao[j]
						.getTabelaAtual();
				if (tabelaAtualDaSinc == null) {
					this.parsesSincronizacao[j].close();
					continue;
				} else {
					String nomeTabelaAtual = tabelaAtualDaSinc.conteudo;
					if (nomeTabelaAtual != null && nomeTabelaAtual.length() > 0
							&& nomeTabelaAtual.equalsIgnoreCase(tabela)) {
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("O parser["+j+"] da sincronizacao: " + this.parsesSincronizacao[j].idSincronizacao + " possui a tabela " + tabela);
						parsesQuePossuemATabela.add(this.parsesSincronizacao[j]);
						indicesOriginal.add(j);
					}
				}
			}
		}
		

		ParsersDaTabela obj = new ParsersDaTabela();
		obj.parsers = parsesQuePossuemATabela;
		obj.indicesOriginais = indicesOriginal;
		return obj;
	}

	private String[] getTabelasContidasNosArquivosDeSincronizacao() {
		HashSet<String> hash = new HashSet<String>();
		if (parsesSincronizacao != null && parsesSincronizacao.length > 0) {
			for (int i = itemParseSincronizacao.indiceInicioCrudsSincronizacao; 
					i <= this.itemParseSincronizacao.indiceFimCrudsSincronizacao
							&& i < parsesSincronizacao.length; 
					i++) {
				ArrayList<String> tabelasDaSinc = parsesSincronizacao[i].tabelas;
				for (int j = 0; j < tabelasDaSinc.size(); j++) {
					if (!hash.contains(tabelasDaSinc.get(j))) {
						hash.add(tabelasDaSinc.get(j));
					}
				}
			}
		}
		ArrayList<String> tabelasDaSinc = null;
		if(parseSincronizacaoMobile != null){
			tabelasDaSinc =parseSincronizacaoMobile.tabelas;
				if (tabelasDaSinc != null) {
					for (int i = 0; i < tabelasDaSinc.size(); i++) {
						if (!hash.contains(tabelasDaSinc.get(i))) {
							hash.add(tabelasDaSinc.get(i));
						}
					}
				}
		}
		

		Object[] objects = hash.toArray();
		String[] tabelas = new String[objects.length];
		for (int i = 0; i < objects.length; i++) {
			tabelas[i] = objects[i].toString();
		}
		String[] ordenadas = this.db.ordenaTabelas(tabelas);
		return ordenadas;

	}

	public boolean checaArquivosDosParsers(boolean downloadDaUltimaSincronizacaoRealizada) {
		try{
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("checaArquivosDosParsers");
			if(this.parsesSincronizacao != null && parsesSincronizacao.length > 0){
				for (int i = 0; i < this.parsesSincronizacao.length; i++) {
					if (!this.parsesSincronizacao[i].fileExists())
						return false;
				}
			}
			
			if (downloadDaUltimaSincronizacaoRealizada
					&& this.parseSincronizacaoMobile != null 
					&& !this.parseSincronizacaoMobile.fileExists())
				return false;
			return true;
		}catch(Exception ex){
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("checaArquivosDosParsers error!",ex);
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			return false;
		}
		
	}
	
	
	public void apagaArquivosDaIteracao() throws Exception {
		if(log != null)
		log.escreveLog("apagaArquivosDaIteracao[OMNAFIN]. INICIO: " + this.itemParseSincronizacao.indiceInicioCrudsSincronizacao 
				+ ". FIM: " + this.itemParseSincronizacao.indiceFimCrudsSincronizacao);
		if(parsesSincronizacao != null && parsesSincronizacao.length > 0){
			for (int i = this.itemParseSincronizacao.indiceInicioCrudsSincronizacao; 
					i <= this.itemParseSincronizacao.indiceFimCrudsSincronizacao && i < parsesSincronizacao.length;
					i++) {
				ParseSincronizacao parseSinc = parsesSincronizacao[i];
				parseSinc.apagaArquivo();
			}	
		}
		
		
		if(itemParseSincronizacao.isUltimaIteracaoDeArquivoDaSincronizacao()){
			if(parseSincronizacaoMobile != null)
				parseSincronizacaoMobile.apagaArquivo();
		}
		
	}
	public InterfaceMensagem leCabecalho() throws Exception {
		try {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog(TAG, "leCabecalho Web");
			if (parsesSincronizacao != null
					&& this.parsesSincronizacao.length > 0) {

				for (int i = this.itemParseSincronizacao.indiceInicioCrudsSincronizacao; 
						i <= this.itemParseSincronizacao.indiceFimCrudsSincronizacao
								&& i < this.parsesSincronizacao.length;
						i++) {
					
					ParseSincronizacao parseSinc = parsesSincronizacao[i];
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog(TAG, "leCabecalho",
							"Lendo cabecalho[" +String.valueOf( i)+ "] - "
									+ parseSinc.getIdentificador());
					parseSinc.open();
					Mensagem msg = parseSinc.leCabecalho();
					if (PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR
							.isEqual(msg.mCodRetorno)) {
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog(TAG, "leCabecalho",
								"Erro com o servidor durante a leitura do cabecalho da sincronizacao  "
										+ parseSinc.getIdentificador()
										+ ". Msg: " + msg.toJson());
						parseSinc.apagaArquivo();
						return msg;
					} else {
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog(TAG, "leCabecalho",
								"Cabecalho lido com sucesso da sincronizacao "
										+ parseSinc.getIdentificador()
										+ ". Msg: " + msg.toJson());
					}
				}
			} else {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog(TAG, "Sem arquivos de sincronizacao");
			}
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog(TAG, "leCabecalho Mobile ");
			Mensagem msg = null;
			if(this.itemParseSincronizacao.isUltimaIteracaoDeArquivoDaSincronizacao()
					&& this.parseSincronizacaoMobile != null){
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog(TAG, "[fjsdkjf]UltimaIteracaoDaSincronizacao");
				if (!this.parseSincronizacaoMobile.open()) {
					msg = new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_DOWNLOAD);
				} else
					msg = this.parseSincronizacaoMobile.leCabecalho();
				
				if (PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR
						.isEqual(msg.mCodRetorno)) {
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog(TAG, "leCabecalho",
							"Erro com o servidor durante a leitura do cabecalho da sincrnoizacao mobile  "
									+ parseSincronizacaoMobile.getIdentificador());
					parseSincronizacaoMobile.apagaArquivo();
					return msg;
				} else if (PROTOCOLO_SISTEMA.TIPO.ERRO_DOWNLOAD
						.isEqual(msg.mCodRetorno)) {
					parseSincronizacaoMobile.apagaArquivo();
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog(
							TAG,
							"leCabecalho",
							"Falha durante a leitura da do arquivo recebido da web da sincronizacao mobile  "
									+ parseSincronizacaoMobile.getIdentificador());
					return msg;
				} else if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
						.getId()) {
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog(
							TAG,
							"leCabecalho",
							"Cabecalho lido com sucesso do arquivo recebido da web da sincronizacao mobile "
									+ parseSincronizacaoMobile.getIdentificador());
					return msg;
				}
			} else {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog(TAG, "[fjsdkjf] NuO u a UltimaIteracaoDaSincronizacao logo o parser mobile nuo seru lido.");
			}

			return new Mensagem(
					PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
		} catch (Exception ex) {
			return new Mensagem(ex);
		}
	}
	
	public InterfaceMensagem fechaArquivos(){
		try {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog(TAG, "fechaArquivos Web");
			if (parsesSincronizacao != null
					&& this.parsesSincronizacao.length > 0) {

				for (int i = this.itemParseSincronizacao.indiceInicioCrudsSincronizacao; 
						i <= this.itemParseSincronizacao.indiceFimCrudsSincronizacao
								&& i < parsesSincronizacao.length;
						i++) {
					ParseSincronizacao parseSinc = parsesSincronizacao[i];
					parseSinc.close();
				}
			} else {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog(TAG, "fechaArquivos:: Sem arquivos de sincronizacao");
			}
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog(TAG, "fechaArquivos Mobile ");
			
			if(this.itemParseSincronizacao.isUltimaIteracaoDeArquivoDaSincronizacao()){
				if(parseSincronizacaoMobile != null)
					parseSincronizacaoMobile.close();
			} else {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog(TAG, "[4ew654f8w4] NuO u a UltimaIteracaoDaSincronizacao logo o parser mobile nuo seru lido.");
			}

			return new Mensagem(
					PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
		} catch (Exception ex) {
			return new Mensagem(ex);
		}
	}

	//
	// public static boolean idsSincronizacaoInicializado(Context context) {
	// boolean inicializado = PontoEletronicoSharedPreference.getValor(
	// context, TIPO_BOOLEAN.INICIALIZADO_IDS_SINCRONIZACAO);
	// if (inicializado) {
	// return true;
	// }
	//
	// Long idsSincronizacao[] = PontoEletronicoSharedPreference.getValor(
	// context, TIPO_VETOR_INT.IDS_SINCRONIZACAO);
	//
	// if (idsSincronizacao == null || idsSincronizacao.length == 0) {
	// String ultimoIdSincronizacao = PontoEletronicoSharedPreference
	// .getValor(context, TIPO_STRING.ULTIMO_ID_SINCRONIZACAO);
	//
	// if (ultimoIdSincronizacao == null)
	// return true;
	// else
	// return false;
	// } else {
	//
	// return true;
	// }
	//
	// }

	public void finalizaSincronizacao(Context c) {
		// PontoEletronicoSharedPreference.saveValor(
		// c,
		// TIPO_STRING.ULTIMO_ID_SINCRONIZACAO,
		// String.valueOf(itemParseSincronizacao.idSincronizacaoAtual));
		if (parsesSincronizacao != null && parsesSincronizacao.length > 0) {
			for (int i = 0; i < parsesSincronizacao.length; i++) {
				ParseSincronizacao p = parsesSincronizacao[i];
				p.close();
			}
		}
		if(parseSincronizacaoMobile != null)
			parseSincronizacaoMobile.close();
	}

}
