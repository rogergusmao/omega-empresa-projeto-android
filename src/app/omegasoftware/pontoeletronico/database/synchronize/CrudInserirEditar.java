package app.omegasoftware.pontoeletronico.database.synchronize;

import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoOperacaoBanco;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class CrudInserirEditar {
	public Long idCrud;
	public Integer idCrudOrigem;
	public Long idSistemaRegistroSincronizador;
	// crud_mobile::id_tabela_web - nas operacoes de insercao vindas do mobile
	// ele sempre estu vazio, pois o
	// id final ainda nuo foi determinado
	public Long idTabelaWeb;
	public Long idTabelaMobile;
	public Integer idSistemaTabela;
	public Integer idMobileIdentificador;
	public boolean valoresNulo = false;
	public Long posicaoInicioValores;
	public Long posicaoFinalValores;
	public Long posicaoInicioValoresDuplicada;
	public Long posicaoFinalValoresDuplicada;
	public Long posicaoInicioDependentes;
	public Long posicaoFinalDependentes;
	public EstadoCrud.ESTADO estado;

	public Long idCrudMobile;
	public Long idSincronizadorMobile;
	public Integer erroSql;
	// crud.id_tabela_web
	public Long idTabelaWebCrud;
	public Long idTabelaWebDuplicada;

	public String[] valores;
	public String[] valoresDuplicada;
	public CrudDependente[] crudsDependentes;

	public EXTDAOSistemaTipoOperacaoBanco.TIPO tipo;

	public CrudInserirEditar(EXTDAOSistemaTipoOperacaoBanco.TIPO tipo) {
		estado = EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO;
		this.tipo = tipo;
	}
	
	public boolean provenienteDaWeb(){
		return idCrudMobile == null ? true : false;
	}
	
	public String getIdentificador() {
		return "Crud " + tipo.toString() + ". Sistema tabela: "
				+ HelperString.formatarParaImpressao(idSistemaTabela)
				+ ". Id Web: "
				+ HelperString.formatarParaImpressao(idTabelaWeb)
				+ ". Id Crud: " + HelperString.formatarParaImpressao(idCrud)
				+ ". Id Crud Mobile: " + HelperString.formatarParaImpressao(idCrudMobile)
				+ ". Id idTabelaMobile: " + HelperString.formatarParaImpressao(idTabelaMobile)
				+ ". Id do sistema_registro_sincronizador: " + HelperString.formatarParaImpressao(idSincronizadorMobile)
				+ ". Erro Sql: " + HelperString.formatarParaImpressao(erroSql)
				+ ". Valores nulo: " + HelperString.formatarParaImpressao(valoresNulo)
				+ ". Estado: " + (estado != null ? estado.toString() : "-");
	}
	
	public String getIdFinalWeb(){
		String idTabelaWeb = null;
		if(this.idTabelaWeb != null)
			idTabelaWeb = this.idTabelaWeb.toString();
		else if(idTabelaWebCrud != null)
			idTabelaWeb = idTabelaWebCrud.toString();
		else if(idTabelaWebDuplicada != null)
			idTabelaWeb = idTabelaWebDuplicada.toString();
		return idTabelaWeb;
	}

}
