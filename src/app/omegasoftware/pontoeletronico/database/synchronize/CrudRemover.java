package app.omegasoftware.pontoeletronico.database.synchronize;

import app.omegasoftware.pontoeletronico.database.synchronize.ParseJson.PonteiroJson;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class CrudRemover {
	public Long idCrud;
	//
	public Long idTabelaWeb;
	public Integer idMobileIdentificador;
	public Integer idCrudOrigem;
	public PonteiroJson inicioObjeto;
	public EstadoCrud.ESTADO estado;
	
	public Long idCrudMobile;
	public Integer erroSql;
	public Long idSincronizadorMobile;
	public Long idSistemaRegistroSincronizador;
	public Integer idSistemaTabela;
	public boolean provenienteDaWeb(){
		return idCrudMobile == null ? true : false;
	}

	public CrudRemover(){
		estado = EstadoCrud.ESTADO.AGUARDANDO_PROCESSAMENTO;
	}

	public String getIdentificador() {
		return "Crud REMOVER. . Sistema tabela: "
				+ HelperString.formatarParaImpressao(idSistemaTabela)
				+ ". Id Web: "
				+ HelperString.formatarParaImpressao(idTabelaWeb)
				+ ". Id Crud: " + HelperString.formatarParaImpressao(idCrud)
				+ ". Estado: " + (estado != null ? estado.toString() : "-");
	}
}
