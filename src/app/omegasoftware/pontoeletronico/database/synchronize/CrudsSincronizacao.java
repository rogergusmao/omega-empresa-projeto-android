package app.omegasoftware.pontoeletronico.database.synchronize;

import java.util.HashMap;
import java.util.LinkedList;

public class CrudsSincronizacao {
	public HashMap<Long, CrudRemover> hashRemover = null;
	public HashMap<Long, CrudInserirEditar> hashEditar= null;
	//idTabelaWeb Por Crud 
	public HashMap<Long, CrudInserirEditar> hashInserir= null;
	public LinkedList<CrudInserirEditar> crudsInserirMobile = null;
	public String[] camposEditar = null;
	public String[] camposInserir = null;
	
	public String[] getCampos(){
		return camposInserir != null && camposInserir.length > 0  ? camposInserir : camposEditar;
	}
	
}
