package app.omegasoftware.pontoeletronico.database.synchronize;

import java.io.File;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.Sessao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.webservice.ServicosSincronizadorWeb;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;
import app.omegasoftware.pontoeletronico.webservice.protocolo.ProtocoloStatusBancoSqlite;

public class DownloadBanco {

	//Constants
	public static final String TAG = "DownloadBanco";
	
	Context context ;
	
	String pathFile;
	ProtocoloConectaTelefone protocolo ;
	
	ServicosSincronizadorWeb serv;
	
	public DownloadBanco(
			Context pContext) throws Exception{
		context = pContext;
		protocolo = Sessao.getSessao(
				context, 
				OmegaSecurity.getIdCorporacao(), 
				OmegaSecurity.getIdUsuario(), 
				OmegaSecurity.getCorporacao());
		if(protocolo == null)
			throw new Exception("Falha ao gerar o protocolo da sessuo.");
	
		String mc = protocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileConectado);
		String mi = protocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador);
		
		serv = new ServicosSincronizadorWeb(context, mc, mi);
		OmegaFileConfiguration vFileConfiguration = new OmegaFileConfiguration();
		pathFile = vFileConfiguration.getPath(OmegaFileConfiguration.TIPO.SINCRONIZACAO);
		context = pContext;
	}
	
	
	
	public InterfaceMensagem receiveFileAndCreateDatabase(
			String databaseName,
			String databasePath,
			int versaoDatabase){
		ServicosSincronizadorWeb.RetornoStatus retornoStatus = null;
		File file = null;
		try{
			retornoStatus = serv.statusSincronizacaoBancoSqliteDaCorporacao();
			if(retornoStatus == null ){
				return Mensagem.factoryServidorSobrecarregado(context);
			} else if(!retornoStatus.msg.ok()){
				return retornoStatus .msg;
			}
			ProtocoloStatusBancoSqlite protocolo= retornoStatus.protocolo;
			Long idUltimaSincronizacao =protocolo.getLongAtributo(ProtocoloStatusBancoSqlite.ATRIBUTO.idUltimaSincronizacao);
			Long idUltimoRegistroAntesDeGerarBanco = protocolo.getLongAtributo(ProtocoloStatusBancoSqlite.ATRIBUTO.idUltimoRegistroAntesDeGerarBanco);
			Long idUltimoRegistroDepoisDeGerarBanco = protocolo.getLongAtributo(ProtocoloStatusBancoSqlite.ATRIBUTO.idUltimoRegistroDepoisDeGerarBanco);
			Long idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco = protocolo.getLongAtributo(ProtocoloStatusBancoSqlite.ATRIBUTO.idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco);
			Long idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco = protocolo.getLongAtributo(ProtocoloStatusBancoSqlite.ATRIBUTO.idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco);
			String ultimaMigracao = protocolo.getAtributo( ProtocoloStatusBancoSqlite.ATRIBUTO.ultimaMigracao);
			String pathCompletoArquivo = pathFile + "banco_sqlite.zip";
			InterfaceMensagem msg = null;
			
			file = new File(pathCompletoArquivo);
			msg = serv.downloadBancoSqliteDaCorporacao(pathCompletoArquivo);
			if(msg != null && msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()){
				
				if(file.exists()){
					try{
//								boolean bancoDeletado = HelperDatabase.deleteDatabaseFile(context, db.getName());
						//db.procedimentoCriaBancoDeDadosDoAquivo(vFile);
						if(HelperDatabase.createDatabaseFromZipFile(
								context, 
								file, 
								databasePath, 
								databaseName, 
								versaoDatabase)){
							
							String idTabelaSincronizador = EXTDAOSistemaTabela.getIdSistemaTabela(context, EXTDAOUsuario.NAME);
							if(idTabelaSincronizador == null){

								return new Mensagem(
										PROTOCOLO_SISTEMA.TIPO.ERRO_SEM_SER_EXCECAO,
										"Erro durante o download do banco, tente novamente mais tarde ou de uma conexuo mais rupida.");
							} else {
								SincronizadorEspelho.resetarEstado(
										null,
										context, 
										idUltimaSincronizacao == null ? -1 : idUltimaSincronizacao, 
										idUltimoRegistroAntesDeGerarBanco == null ? -1 : idUltimoRegistroAntesDeGerarBanco,
										idUltimoRegistroDepoisDeGerarBanco == null ? -1 : idUltimoRegistroDepoisDeGerarBanco,
										idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco == null ? -1 : idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco,
										idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco == null ? -1 : idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco,
												ultimaMigracao);
								
								SincronizadorEspelho.setIdSincronizadorNoMomentoQueOBancoSqliteFoiCriado(context, idUltimaSincronizacao == null ? -1 : idUltimaSincronizacao);
								return new Mensagem(
									PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
									"O banco foi criado com sucesso.");
							}
							
							
						} else {
							return new Mensagem(
									PROTOCOLO_SISTEMA.TIPO.ERRO_SEM_SER_EXCECAO,
									"Erro durante o download do banco, tente novamente mais tarde ou de uma conexuo mais rupida.");
						}
					}catch(Exception ex){
						SingletonLog.insereErro(
								ex, 
								SingletonLog.TIPO.SINCRONIZADOR);
					}	
				}	
			}
			
			return msg;
		} catch(Exception ex){
			SingletonLog.insereErro(
					ex, 
					SingletonLog.TIPO.SINCRONIZADOR);
			return new Mensagem(ex);
		} finally{
			if(file != null && file.exists()) file.delete(); 
		}
		
	}
	

}
