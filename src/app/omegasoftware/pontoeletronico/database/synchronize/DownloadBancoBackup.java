package app.omegasoftware.pontoeletronico.database.synchronize;

import java.io.File;

import android.content.Context;
import app.omegasoftware.pontoeletronico.appconfiguration.ContainerPrograma;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.HelperDatabase;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.http.HelperHttpPostReceiveFile;
import app.omegasoftware.pontoeletronico.phone.HelperPhone;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;

public class DownloadBancoBackup {

	//Constants
	public static final String TAG = "DownloadBanco";
	
	Context context ;
	Container container;
	String pathFile;

	public Container getContainer(){
		return container;
	}
	
	public class Container{
		public Integer tamanhoBytes;
		public String date;
		public String url;
		ContainerPrograma.TYPE_PROGRAM type;
		public Container(ContainerPrograma.TYPE_PROGRAM pType){
			type = pType;
		}
	
		
		public boolean isEmpty(){
			if(tamanhoBytes == null && date == null && url == null)
				return false;
			else return true;
			
		}
	}
//		public void intialize(){
//			if(ContainerPrograma.isProgramaDaEmpresa())
//				initializeFactu();
//			else{
//				switch (type) {
//				
//				case OMEGA_EMPRESA:
//					initializeOmegaEmpresa();
//					break;
//				default:
//					break;
//				}	
//			}
//		}
//		
//		public void initializeOmegaEmpresa(){
//			String vInformacao = HelperHttpPost.getConteudoStringPost(
//					context, 
//					OmegaConfiguration.URL_REQUEST_DOWNLOAD_BANCO_CORPORACAO(),
//					new String[]{"imei", "id_corporacao", "corporacao"}, 
//					new String[]{HelperPhone.getIMEI(),OmegaSecurity.getIdCorporacao(),
//							OmegaSecurity.getCorporacao(),});
//			String vVetorInformacao[] = vInformacao.split("[;]");
//			//TODO inicializar timestamp
//			tamanhoBytes = HelperInteger.parserInteger( vVetorInformacao[0]);
//			date = vVetorInformacao[1];
//			URL_PONTO_ELETRONICO = vVetorInformacao[2];
//		}
//		
//		public void initializeFactu(){
//			String vInformacao = HelperHttpPost.getConteudoStringPost(
//					context, 
//					OmegaConfiguration.URL_REQUEST_DOWNLOAD_BANCO_CORPORACAO(),
//					new String[]{"imei"}, 
//					new String[]{HelperPhone.getIMEI()});
//			String vVetorInformacao[] = vInformacao.split("[;]");
//			//TODO inicializar timestamp
//			tamanhoBytes = HelperInteger.parserInteger( vVetorInformacao[0]);
//			date = vVetorInformacao[1];
//			URL_PONTO_ELETRONICO = vVetorInformacao[2];
//		}
//	}
	
//	public DownloadBancoBackup(Context pContext){
//		OmegaFileConfiguration vFileConfiguration = new OmegaFileConfiguration();
//		pathFile = vFileConfiguration.getPathFilesToReceive();
//		context = pContext;
//	}
	
	
	
	
	
	
//	public boolean receiveFile(){
//		container = new Container(OmegaConfiguration.PROGRAM);
//		container.intialize();
//		if(container == null || !container.isEmpty()){
//			return false;
//		}
//		try{
//			
//			Boolean validade = false;
//			
//			File file = new File(pathFile + HelperPhone.getIMEI() + ".zip");
//			
//			validade = InterfaceMensagem.validaChamadaWebservice( HelperHttpPostReceiveFile.DownloadFileFromHttpPost(
//					context, 
//					container.URL_PONTO_ELETRONICO,
//					pathFile+ HelperPhone.getIMEI() + ".zip"));
//			if(validade){
//				
//				if(file.exists()){ 
//					Database db = null;
//						try{
//							db = new DatabasePontoEletronico(context, false);
//							//db.procedimentoCriaBancoDeDadosDoAquivo(vFile);
//							HelperDatabase.createDatabaseFromZipFile(context, file, db.getDatabasePath(), db.getName(),  db.getDatabaseVersion());
//						}catch(Exception ex){
//							SingletonLog.insereErro(
//									ex, 
//									SingletonLog.TIPO.BIBLIOTECA_NUVEM);
//						} finally{
//							if(db != null)
//								db.close();
//						}
//				}	
//			}
//		
//			return validade == null ? false : validade;
//		} catch(Exception ex){
//			SingletonLog.insereErro(
//					ex, 
//					SingletonLog.TIPO.BIBLIOTECA_NUVEM);
//		}
//		return false;
//		}

	

}
