package app.omegasoftware.pontoeletronico.database.synchronize;

public class EstadoCrud {
	public enum ESTADO{
		AGUARDANDO_PROCESSAMENTO(1),
		EXECUTADA(2),
		CANCELADA(3);

		final int id;

		ESTADO(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}
		public String getStrId(){
			return String.valueOf(id);
		}
	}
}
