package app.omegasoftware.pontoeletronico.database.synchronize;

import android.content.Context;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCrudEstado;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoOperacaoBanco;

import app.omegasoftware.pontoeletronico.database.synchronize.EstadoCrud.ESTADO;

public class ItemCruds {
	EXTDAOSistemaCrudEstado objSistemaCrudEstado;
	public enum TIPO_ITEM_CRUDS {
		SINCRONIZACAO(1), SINCRONIZACAO_MOBILE(2);
		Integer id;

		TIPO_ITEM_CRUDS(Integer id) {
			this.id = id;
		}

		public Integer getId() {
			return this.id;
		}
	}

	public Long id;
	public TIPO_ITEM_CRUDS tipo;
//	public HashSharedPreference hashRemocao;
//	public HashSharedPreference hashInsercao;
//	public HashSharedPreference hashEdicao;
//	EXTDAOSistemaCrudEstado objSEC ;
	public ItemCruds(Context c, TIPO_ITEM_CRUDS tipo, Long id, EXTDAOSistemaCrudEstado objSistemaCrudEstado) throws Exception {
		this.id = id;
		this.tipo = tipo;
		this.objSistemaCrudEstado=objSistemaCrudEstado;
//		String sufixoId = this.getSufixoId();
//		this.hashRemocao = new HashSharedPreference(c, "hashRemocao_"
//				+ sufixoId);
//		this.hashInsercao = new HashSharedPreference(c, "hashInsercao_"
//				+ sufixoId);
//		objSEC = new EXTDAOSistemaCrudEstado(new DatabasePontoEletronico(c));
		
//		this.hashEdicao = new HashSharedPreference(c, "hashEdicao_"
//				+ sufixoId);
	}

	public void apagarDados() throws Exception {
//		this.hashRemocao.apagarHash();
//		this.hashInsercao.apagarHash();
		objSistemaCrudEstado.deleteAllTuples(false);
//		this.hashEdicao.apagarHash();
	}

	public static ESTADO getEstado(Integer id) {
		ESTADO[] vetor = ESTADO.values();
		for (int i = 0; i < vetor.length; i++) {
			if (vetor[i].getId() == id)
				return vetor[i];
		}
		return null;
	}

	public static TIPO_ITEM_CRUDS getTipo(int id) {
		TIPO_ITEM_CRUDS[] vetor = TIPO_ITEM_CRUDS.values();
		for (int i = 0; i < vetor.length; i++) {
			if (vetor[i].getId() == id)
				return vetor[i];
		}
		return null;
	}

	public String getSufixoId() {
		return tipo.toString() + "_" + this.id.toString();
	}

	public void setEstadoCrudRemover(CrudRemover c) throws Exception {
//		if (c.idCrud != null) {
//			hashRemocao.put(c.idCrud, c.estado.getId());
//		} else if (c.idCrudMobile != null) {
//			hashRemocao.put(c.idCrudMobile, c.estado.getId());
//		} else
//			throw new Exception("Condicional nuo programada");
		
		
		boolean existe = objSistemaCrudEstado.selectCrud(c.idCrud, c.idCrudMobile);
		if(!existe){
			objSistemaCrudEstado.clearData();
			

			objSistemaCrudEstado.setAttrValue(
					EXTDAOSistemaCrudEstado.SISTEMA_TIPO_OPERACAO_BANCO_ID_INT, 
					String.valueOf( EXTDAOSistemaTipoOperacaoBanco.TIPO.REMOVE.getId()));
			if(c.idCrud != null)
				objSistemaCrudEstado.setAttrValue(
						EXTDAOSistemaCrudEstado.ID_CRUD_INT, 
						String.valueOf( c.idCrud));
			else
				objSistemaCrudEstado.setAttrValue(
						EXTDAOSistemaCrudEstado.ID_CRUD_INT, 
						null);
			if(c.idCrudMobile != null)
				objSistemaCrudEstado.setAttrValue(
						EXTDAOSistemaCrudEstado.ID_CRUD_MOBILE_INT, 
						String.valueOf( c.idCrudMobile));
			else
				objSistemaCrudEstado.setAttrValue(
						EXTDAOSistemaCrudEstado.ID_CRUD_MOBILE_INT, 
						null);
			
			if(c.idSincronizadorMobile != null)
				objSistemaCrudEstado.setAttrValue(
						EXTDAOSistemaCrudEstado.ID_SINCRONIZADOR_MOBILE_INT, 
						String.valueOf( c.idSincronizadorMobile));
			else
				objSistemaCrudEstado.setAttrValue(
						EXTDAOSistemaCrudEstado.ID_SINCRONIZADOR_MOBILE_INT, 
						null);
			
			objSistemaCrudEstado.setAttrValue(EXTDAOSistemaCrudEstado.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
		} 
		objSistemaCrudEstado.setAttrValue(
				EXTDAOSistemaCrudEstado.ID_ESTADO_CRUD_INT, 
				String.valueOf( c.estado.getId()));
		
		if(!existe)
			objSistemaCrudEstado.insertThrowException(false,false,null, true, OmegaSecurity.getIdCorporacao(), OmegaSecurity.getIdUsuario());
		else
			objSistemaCrudEstado.updateThrowsException(false);
	}

	public void setEstadoCrudInserirEditar(
			CrudInserirEditar c, 
			OmegaLog log) throws Exception {
		
		boolean existe = objSistemaCrudEstado.selectCrud(c.idCrud, c.idCrudMobile);
		if(!existe){
			objSistemaCrudEstado.clearData();
			if(c.idCrud != null)
				objSistemaCrudEstado.setAttrValue(
						EXTDAOSistemaCrudEstado.ID_CRUD_INT, 
						String.valueOf( c.idCrud));
			else
				objSistemaCrudEstado.setAttrValue(
						EXTDAOSistemaCrudEstado.ID_CRUD_INT, 
						null);
			if(c.idCrudMobile != null)
				objSistemaCrudEstado.setAttrValue(
						EXTDAOSistemaCrudEstado.ID_CRUD_MOBILE_INT, 
						String.valueOf( c.idCrudMobile));
			else
				objSistemaCrudEstado.setAttrValue(
						EXTDAOSistemaCrudEstado.ID_CRUD_MOBILE_INT, 
						null);
			if(c.idSincronizadorMobile != null)
				objSistemaCrudEstado.setAttrValue(
						EXTDAOSistemaCrudEstado.ID_SINCRONIZADOR_MOBILE_INT, 
						String.valueOf( c.idSincronizadorMobile));
			else
				objSistemaCrudEstado.setAttrValue(
						EXTDAOSistemaCrudEstado.ID_SINCRONIZADOR_MOBILE_INT, 
						null);
			objSistemaCrudEstado.setAttrValue(
					EXTDAOSistemaCrudEstado.SISTEMA_TIPO_OPERACAO_BANCO_ID_INT, 
					String.valueOf( c.tipo.getId()));
			
			objSistemaCrudEstado.setAttrValue(
					EXTDAOSistemaCrudEstado.CORPORACAO_ID_INT, 
					OmegaSecurity.getIdCorporacao());
		}
		
		objSistemaCrudEstado.setAttrValue(
				EXTDAOSistemaCrudEstado.ID_ESTADO_CRUD_INT, 
				String.valueOf( c.estado.getId()));
		
		if(!existe)
			objSistemaCrudEstado.insertThrowException(false,false,null, true, OmegaSecurity.getIdCorporacao(), OmegaSecurity.getIdUsuario());
		else
			objSistemaCrudEstado.updateThrowsException(false);
		
		
	}
	
}
