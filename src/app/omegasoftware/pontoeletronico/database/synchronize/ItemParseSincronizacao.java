package app.omegasoftware.pontoeletronico.database.synchronize;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCrudEstado;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_STRING;

public class ItemParseSincronizacao {
	public enum ETAPA {
		INICIO(0), CRUDS_INSERCAO_SINC_MOBILE(1), CRUDS_REMOCAO_SINC(2), CRUDS_REMOCAO_SINC_MOBILE(
				3), CRUDS_EDICAO_SINC_MOBILE(4), CRUDS_INSERCAO_SINC(5), CRUDS_EDICAO_SINC(
				6), FIM(7);
		int id;

		ETAPA(int id) {
			this.id = id;
		}

		public int getId() {
			return this.id;
		}
	}
	
	EXTDAOSistemaCrudEstado objSistemaCrudEstado;
	public ETAPA etapa;
	Integer iTabelas;
	public ItemCruds[] crudsSincronizacao;
	public ItemCruds crudsSincronizacaoMobile;
	public Integer indiceInicioCrudsSincronizacao;
	public Integer indiceFimCrudsSincronizacao;
	public static final int LIMITE_ARQUIVO_SINCROINACAO_POR_ITERACAO = 2;
	public boolean ultimaIteracaoDeArquivosDaSincronizacao;
	
	public String getIdentificador(){
		StringBuilder sb = new StringBuilder();
		if(etapa == null)
			sb.append("Etapa: nulo");
		else sb.append("Etapa: " + etapa.toString());
		
		if(indiceInicioCrudsSincronizacao == null)
			sb.append("indiceInicioCrudsSincronizacao: nulo");
		else sb.append("indiceInicioCrudsSincronizacao: " + indiceInicioCrudsSincronizacao);
		
		if(indiceFimCrudsSincronizacao == null)
			sb.append("indiceFimCrudsSincronizacao: nulo");
		else sb.append("indiceFimCrudsSincronizacao: " + indiceFimCrudsSincronizacao);
		
		if(indiceFimCrudsSincronizacao == null)
			sb.append("ultimaIteracaoDeArquivosDaSincronizacao: nulo");
		else sb.append("ultimaIteracaoDeArquivosDaSincronizacao: " + ultimaIteracaoDeArquivosDaSincronizacao);
		
		if(iTabelas == null)
			sb.append("iTabelas: nulo");
		else sb.append("iTabelas: " + iTabelas);

		if(crudsSincronizacao == null)
			sb.append("crudsSincronizacao: nulo");
		else sb.append("crudsSincronizacao: " + crudsSincronizacao.length);
		
		if(crudsSincronizacaoMobile == null)
			sb.append("crudsSincronizacaoMobile: nulo");
		else sb.append("crudsSincronizacaoMobile: " + crudsSincronizacaoMobile.getSufixoId());
		
		return sb.toString();
	}
	
	public ItemParseSincronizacao(EXTDAOSistemaCrudEstado objSistemaCrudEstado) {
		iTabelas = 0;
		this.objSistemaCrudEstado=objSistemaCrudEstado;
	}

	public void apagarDados(Context c) throws JSONException {

		iTabelas = 0;
		etapa = null;
		indiceFimCrudsSincronizacao = null;
		indiceInicioCrudsSincronizacao = null;
		ultimaIteracaoDeArquivosDaSincronizacao = false;
		salvar(c);
	}
	
	public boolean isUltimaIteracaoDeArquivoDaSincronizacao(){
		return ultimaIteracaoDeArquivosDaSincronizacao;
	}
	
	public boolean salvar(Context c) throws JSONException {

		JSONObject obj = new JSONObject();
		if (etapa != null)
			obj.put("etapa", etapa.getId());
		else
			obj.put("etapa", null);
		obj.put("iTabelas", iTabelas);
		obj.put("indiceInicioCrudsSincronizacao", indiceInicioCrudsSincronizacao);
		obj.put("indiceFimCrudsSincronizacao", indiceFimCrudsSincronizacao);
		obj.put("ultimaIteracaoDeArquivosDaSincronizacao", ultimaIteracaoDeArquivosDaSincronizacao);
		
		String strJson = obj.toString();
		PontoEletronicoSharedPreference.saveValor(c,
				TIPO_STRING.JSON_ITEM_PARSE_SINCRONIZACAO, strJson);
		return true;
	}

	public boolean carregaDaMemoria(
			Context c, 
			Long[] idsSincronizacao,
			Long idSincronizacaoMobile, 
			boolean ultimaIteracaoDeArquivosDaSincronizacao) throws Exception {

		String json = PontoEletronicoSharedPreference.getValor(c,
				TIPO_STRING.JSON_ITEM_PARSE_SINCRONIZACAO);
		if (json == null)
			return false;

		this.ultimaIteracaoDeArquivosDaSincronizacao = ultimaIteracaoDeArquivosDaSincronizacao;
		JSONObject obj = new JSONObject(json);
		if (idsSincronizacao != null && idsSincronizacao.length > 0) {

			crudsSincronizacao = new ItemCruds[idsSincronizacao.length];
			for (int i = 0; i < idsSincronizacao.length; i++) {
				long idSincronizacao = idsSincronizacao[i];
				crudsSincronizacao[i] = new ItemCruds(
						c,
						ItemCruds.TIPO_ITEM_CRUDS.SINCRONIZACAO, 
						idSincronizacao,
						objSistemaCrudEstado);
			}

		}
		if (idSincronizacaoMobile != null && this.ultimaIteracaoDeArquivosDaSincronizacao) {
			crudsSincronizacaoMobile = new ItemCruds(
					c,
					ItemCruds.TIPO_ITEM_CRUDS.SINCRONIZACAO_MOBILE,
					idSincronizacaoMobile,
					objSistemaCrudEstado);
		}

		if (!obj.isNull("iTabelas")) {
			iTabelas = obj.getInt("iTabelas");
		}
		if (!obj.isNull("etapa")) {
			int idEtapa = obj.getInt("etapa");
			etapa = getEtapa(idEtapa);
		}
		if (!obj.isNull("indiceInicioCrudsSincronizacao")) {
			this.indiceInicioCrudsSincronizacao = obj.getInt("indiceInicioCrudsSincronizacao");
		}
		if (!obj.isNull("indiceFimCrudsSincronizacao")) {
			this.indiceFimCrudsSincronizacao = obj.getInt("indiceFimCrudsSincronizacao");
		}
		
		
		return true;
	}



	public static ETAPA getEtapa(int id) {
		ETAPA[] vetor = ETAPA.values();
		for (int i = 0; i < vetor.length; i++) {
			int codigo = vetor[i].getId();
			if (codigo == id) {
				return vetor[i];
			}
		}
		return null;
	}

}