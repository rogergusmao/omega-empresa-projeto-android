package app.omegasoftware.pontoeletronico.database.synchronize;


public class NewIdException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NewIdException(String message) {
        super(message);
    }
	
	public NewIdException(String message,Exception ex) {
        super(message,ex);
    }
}
