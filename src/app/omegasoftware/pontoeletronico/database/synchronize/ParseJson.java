package app.omegasoftware.pontoeletronico.database.synchronize;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.StringBufferInputStream;
import java.util.ArrayList;
import java.util.Stack;

//import javax.xml.transform.stream.StreamResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.ValidadeArquivoJson;
import app.omegasoftware.pontoeletronico.file.HelperFile;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class ParseJson {
	final static String TAG = "ParseJson"; 
	final boolean TRACE_ROUTE = OmegaConfiguration.DEBUGGING_SINCRONIZACAO;
	public enum TIPO_JSON {
		VETOR(1, '[', ']'), OBJETO(2, '{', '}'), STRING(3, '\"', '\"'), NOME_ATRIBUTO(
				4, '\"', '\"'), NUMERICO(5, '\0', '\0'), NULL(6, '\0', '\0');
		
		public int id;
		private char delimitadorInicial;
		private char delimitadorFinal;

		TIPO_JSON(int id, char inicio, char fim) {
			this.id = id;
			this.delimitadorInicial = inicio;
			this.delimitadorFinal = fim;
		}

		public Boolean verificaDelimitadorInicial(char letra) {
			if (this == NUMERICO) {
				// se for um numero [48] = 0 [57] = 9
				byte byteLetra = (byte) letra;
				if (byteLetra >= 48 && byteLetra <= 57) {
					return true;
				} else
					return false;
			} else if (this == NULL) {
				// se for um numero [48] = 0 [57] = 9
				if ((int) letra == 'n') {
					return true;
				} else
					return false;
			}

			else {
				if (this.delimitadorInicial == letra) {
					return true;
				} else
					return false;
			}
		}

		public Boolean verificaDelimitadorFinal(char letra,
				Character ultimaLetra) {
			if (this == NULL) {
				if (ultimaLetra == null)
					return false;
				else if (letra == 'l' && ultimaLetra == 'l')
					return true;
				else
					return false;
			} else if (this == NUMERICO) {
				if (ultimaLetra != null) {
					byte byteLetra = (byte) letra;
					char cUltima = ultimaLetra;
					// se for um numero [48] = 0 [57] = 9
					if ((byteLetra < 48 || byteLetra > 57)
							&& (cUltima >= 48 && cUltima <= 57)) {
						return true;
					} else
						return false;
				} else
					return false;

			} else {
				if (this.delimitadorFinal == letra) {
					return true;
				} else
					return false;
			}
		}
	}

	public class PonteiroJson {
		public int nivel;
		public String nome;
		public String conteudo;
		public TIPO_JSON tipoJson;
		public Integer posicaoVetor;
		public Integer numeroDeTermos;
		public REFERENCIA_JSON refereciaJson;
		public long posicaoArquivo;

		public boolean isEmpty(){
			return nome == null || nome.length() == 0 ? true : false;
		}
		
		public Integer getConteudoInteger() {
			return HelperInteger.parserInteger(conteudo);
		}
		
		public Long getConteudoLong() {
			return HelperInteger.parserLong(conteudo);
		}

		public String getConteudo() {
			return conteudo;
		}

		public String getIdentificador() {
			String nomePonteiro = null;
			if (nome != null && nome.length() > 0) {
				nomePonteiro = nome;
				String strRefJson = "";
				if (refereciaJson != null) {
					strRefJson = refereciaJson.toString();
				} else
					strRefJson = "REFERENCIA_JSON_NULL";

				String strTipoJson = null;
				if (tipoJson != null)
					strTipoJson = tipoJson.toString();
				else
					strTipoJson = "TIPO_JSON_NULL";

				return nomePonteiro + " - " + strTipoJson + " : " + strRefJson;
			} 
			return null;
		}

		public boolean isEqual(String pNome) {
			if (nome == null)
				return false;
			else {
				return nome.equalsIgnoreCase(pNome);
			}
		}

		// public boolean finalObjetoDoVetor(String vetor){
		//
		// }
		public boolean finalObjeto(String nome) {
			if (this.nome == null || !this.nome.equalsIgnoreCase(nome))
				return false;
			else
				return finalObjeto();
		}

		public boolean finalObjeto() {
			if (tipoJson == TIPO_JSON.OBJETO
					&& refereciaJson == REFERENCIA_JSON.DELIMITADOR_FINAL)
				return true;
			else
				return false;
		}

		public boolean inicioObjeto() {
			if (tipoJson == TIPO_JSON.OBJETO
					&& refereciaJson == REFERENCIA_JSON.DELIMITADOR_INICIAL)
				return true;
			else
				return false;
		}

		public boolean finalVetor(String nome) {
			if (this.nome == null || !this.nome.equalsIgnoreCase(nome))
				return false;
			else
				return finalVetor();
		}

		public boolean finalVetor() {
			if (tipoJson == TIPO_JSON.VETOR
					&& refereciaJson == REFERENCIA_JSON.DELIMITADOR_FINAL)
				return true;
			else
				return false;
		}

		public boolean isNull() {
			if (tipoJson == TIPO_JSON.NULL)
				return true;
			else
				return false;

		}

		public boolean inicioVetor() {
			if (tipoJson == TIPO_JSON.VETOR
					&& refereciaJson == REFERENCIA_JSON.DELIMITADOR_INICIAL)
				return true;
			else
				return false;
		}
	}

	public enum REFERENCIA_JSON {
		DELIMITADOR_INICIAL, DELIMITADOR_FINAL, CONTEUDO_COMPLETO
	}

	Stack<PonteiroJson> pilha;

	InputStream inputStream = null;
	RandomAccessFile reader = null;
	RandomAccessFile readerAux = null;
	File f = null;
	OmegaLog log = null;
	public ParseJson(File f, OmegaLog log ) {
		this.f = f;
		buffer = new byte[1024];
		pilha = new Stack<ParseJson.PonteiroJson>();
		this.log = log;
	}
	public String getPath(){
		return f != null ? f.getAbsolutePath() : "";
	}
	public boolean open() throws FileNotFoundException {
//		try {
			inputStream = new FileInputStream(f);
			
			reader = new RandomAccessFile(this.f, "r");
			readerAux = new RandomAccessFile(this.f, "r");
			return true;
//		} catch (FileNotFoundException e) {

//			SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
//			return false;
//		}

	}
	
	public static ValidadeArquivoJson validaArquivoJson(String path){
		FileReader fr = null;
		try{


			File f = new File(path);
			 fr = new FileReader(f);
			char[] buf = new char[256];
			int totalLido = fr.read(buf);
			if(totalLido == -1 )
				return null;
			Boolean validadeJson = null;
			StringBuilder sb = new StringBuilder();
			while(totalLido > 0){
//				char[] busca = new char[] {'<', 'b', 'r', ' ', '<'};
//				int iBusca = '';
				sb.append(buf, 0, totalLido);
				for(int i =  0; i < totalLido; i++){
					if(buf[i] == '{'){
						validadeJson = true;
						break;
					} else if(buf[i] != ' '){
						//invalido
						validadeJson =false;
						break;
					}
				}
				if(validadeJson != null)
					break;
				fr.read(buf);	
			}
			if(validadeJson == null || !validadeJson){
				
				SingletonLog.insereErro(TAG, "validaArquivoJson", sb.toString(), TIPO.HTTP);	
			}
			if(validadeJson == null) return null;
			else {
				if(validadeJson)
					return new ValidadeArquivoJson(true, sb.toString());
				else 
					return new ValidadeArquivoJson(false, sb.toString());
			}
			
		}catch(Exception ex){
			
			return new ValidadeArquivoJson(false, ex.toString());
		} finally{
			if(fr != null)
				try {
					fr.close();
				} catch (IOException e) {
					SingletonLog.insereErro(e, TIPO.UTIL_ANDROID);
				}
		}
	}
	
	public String[] leVetorNaPosicao(long posicaoInicial, long posicaoFinal)
			throws Exception {
		String s2= null;
		try {
			readerAux.seek(posicaoInicial);
			byte[] buf = new byte[(int) (posicaoFinal - posicaoInicial)];
			//long lidos = readerAux.read(buf);
			long lidos = readerAux.read(buf);
			if(lidos <=0)return null;
//			for (int i = 0; i < lidos; i++) {
//
//				char letra = (char) (buf[i] & 0xFF);
//			}
			s2 = new String(buf, "UTF8");
//			FileInputStream is = new FileInputStream(f);
//			
//			BufferedReader bfr = new BufferedReader(new InputStreamReader(is, "UTF-8"));
//			StringBuilder tot = new StringBuilder();
//			String readLine="";
//			while ((readLine = bfr.readLine()) != null) 
//	        {
//				tot.append(readLine);
//	        }
//			bfr.close();
//			is.close();

			
			JSONArray jsonArray = new JSONArray(s2);
			String[] vetor = new String[jsonArray.length()];
			for (int i = 0; i < vetor.length; i++) {
				String valor = jsonArray.isNull(i) ? null : jsonArray.getString(i);
				if(OmegaConfiguration.DEBUGGING){
					if(valor  != null && valor .compareTo("null") == 0){
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("leVetorNaPosicao: encontrou nulo por escrito !!! TODO: remover esse trecho");
						valor = null;
					}
				}
				
				//vetor[i] = new String(valor, Charset.UTF_8);
				vetor[i] = valor;
				// if(vetor[i] != null &&
				// vetor[i].equalsIgnoreCase("null"))
				// vetor[i] = null;
			}
			return vetor;
		} catch (Exception ex) {
			
			throw new Exception("Trecho: " + s2.toString() 
					+ ". Posicao Inicial: " + String.valueOf(posicaoInicial)
					+ ". Posicao final: " + String.valueOf(posicaoFinal));
		}

	}

	public JSONArray bkpLeJSONArrayNaPosicao(long posicaoInicial, long posicaoFinal)
			throws IOException, JSONException {
		readerAux.seek(posicaoInicial);
		byte[] buf = new byte[(int) (posicaoFinal - posicaoInicial)];
		
		long lidos = readerAux.read(buf);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < lidos; i++) {

			char letra = (char) (buf[i] & 0xFF);
			sb.append(letra);
		}
		JSONArray jsonArray = new JSONArray(sb.toString());
		return jsonArray;
	}


	public JSONArray leJSONArrayNaPosicao(long posicaoInicial, long posicaoFinal)
			throws IOException, JSONException {
		readerAux.seek(posicaoInicial);
		byte[] buf = new byte[(int) (posicaoFinal - posicaoInicial)];
		
		long lidos = readerAux.read(buf);
		if(lidos <=0)return null;

		String s2 = new String(buf, "UTF8");
		JSONArray jsonArray = new JSONArray(s2);
		return jsonArray;
	}

	public boolean close() {
		try {
			if(reader != null)reader.close();
			if(readerAux != null)readerAux.close();
			if(inputStream != null)inputStream.close();
			return true;
		} catch (IOException e) {
			SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
			return false;
		}

	}

	byte[] buffer;
	int iBuffer;
	int lidos = 0;
	Character ultimaLetra = null;
	Character letra = null;
	Byte bLetra = null;
	PonteiroJson ponteiro = null;

	PonteiroJson ultimoPonteiro = null;

	public PonteiroJson getUltimoPonteiro() {
		return ultimoPonteiro;
	}

	public PonteiroJson getProximoPonteiro(String nomeAtributo)
			throws Exception {
		PonteiroJson ponteiro = getProximoPonteiro();
		if (ponteiro != null && ponteiro.nome.equalsIgnoreCase(nomeAtributo)) {
			return ponteiro;
		} else
			return null;
	}

	public PonteiroJson getProximoPonteiroThrowException(String nomeAtributo)
			throws Exception {
			
		PonteiroJson ponteiro = getProximoPonteiro();
//		if(getPonteiroLeitura() >= 136248){
//			int k = 0;
//			k++;
//		}
		if (ponteiro != null && ponteiro.nome != null
				&& ponteiro.nome.equalsIgnoreCase(nomeAtributo)) {
			return ponteiro;
		} else {
			String trecho = getTrecho();
			if (ponteiro != null)
				throw new Exception("Formato invulido do arquivo "
						+ this.f.getName() + "::" + getPonteiroLeitura()
						+ ", era esperado o atributo '" + nomeAtributo
						+ "' e nuo " + ponteiro.getIdentificador() + ". Trecho do arquivo: " + trecho);
			else
				throw new Exception("Formato invulido do arquivo "
						+ this.f.getName() + "::" + getPonteiroLeitura()
						+ ", era esperado o atributo '" + nomeAtributo
						+ "' e veio um ponteiro nulo. "+ ". Trecho do arquivo: " + trecho);
		}
	}

	public void posicionaLeitorNoPonteiro(PonteiroJson ponteiro)
			throws IOException {
		long posicao = ponteiro.posicaoArquivo;
		reader.seek(posicao);
	}

	public void posicionaLeitorNoPonteiro(Long posicao) throws IOException {

		reader.seek(posicao);
	}

	public PonteiroJson getProximoPonteiro() throws Exception {
		PonteiroJson p = proximoObjeto(true);
		if(TRACE_ROUTE && OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
			if(p != null){
				String ide = p.getIdentificador();
				if(ide != null )
					log.escreveLog("getProximoPonteiro: " + ide);
				else 
					log.escreveLog("getProximoPonteiro: null(2)" );
			} else {
				log.escreveLog("getProximoPonteiro: null");
			}
			
		}
		return p;
	}
	int TAMANHO_TRECHO = 1000;
	public String getTrecho() throws IOException{
		long posicao= getPonteiroLeitura();
		
		long limiteInferior = posicao;
		if(posicao > TAMANHO_TRECHO ) limiteInferior -= TAMANHO_TRECHO;
		StringBuilder trecho = new StringBuilder();
		posicionaLeitorNoPonteiro(limiteInferior);
		for(long i = limiteInferior ; i <= posicao; i++)
			trecho.append(proximaLetra());
		long i = posicao + 1;  
		trecho.append("$$$");
		Character letra = proximaLetra();
		while(i < posicao + TAMANHO_TRECHO && letra != null){
			trecho.append(letra);
			proximaLetra();
			i++;
		}
		posicionaLeitorNoPonteiro(posicao);
		return trecho.toString();
	}

	private PonteiroJson proximoObjeto(boolean recursivo) throws Exception {

		PonteiroJson ponteiro = null;
		try {
			while (proximaLetra() != null
					&& (letra == ':' || letra == ',' || letra == ' '))
				;

			if (letra == null)
				return null;
			else {
				TIPO_JSON[] tipos = TIPO_JSON.values();
				for (int i = 0; i < tipos.length; i++) {

					if (tipos[i].verificaDelimitadorInicial(letra)) {
						PonteiroJson topo = null;
						if (pilha.size() > 0)
							topo = pilha.peek();

						if (tipos[i] == TIPO_JSON.STRING
								&& ultimoPonteiro != null
								&& ultimoPonteiro.tipoJson != TIPO_JSON.NOME_ATRIBUTO
								&& topo != null && !topo.inicioVetor()) {
							StringBuilder sbAtributo = new StringBuilder();
							while (proximaLetra() != null) {
								if (tipos[i].verificaDelimitadorFinal(letra,
										ultimaLetra))
									break;
								sbAtributo.append(letra.toString());

							}
							String atributo = sbAtributo.toString();
							// vai para o proximo objeto para detectar o seu
							// tipo
							if (recursivo) {

								ultimoPonteiro = new PonteiroJson();
								ultimoPonteiro.refereciaJson = REFERENCIA_JSON.CONTEUDO_COMPLETO;
								ultimoPonteiro.nivel = pilha.size();
								ultimoPonteiro.posicaoArquivo = getPonteiroLeitura();
								ultimoPonteiro.conteudo = atributo;
								ultimoPonteiro.tipoJson = TIPO_JSON.NOME_ATRIBUTO;
								ponteiro = proximoObjeto(false);
								ponteiro.nome = atributo;
							}
							return ponteiro;
						} else if (tipos[i] == TIPO_JSON.STRING
								|| tipos[i] == TIPO_JSON.NUMERICO
								|| tipos[i] == TIPO_JSON.NULL) {
							StringBuilder sbConteudo = new StringBuilder();

							if (tipos[i] == TIPO_JSON.NULL
									|| tipos[i] == TIPO_JSON.NUMERICO)
								sbConteudo.append(letra.toString());

							while (proximaLetra() != null) {
								if (tipos[i].verificaDelimitadorFinal(letra,
										ultimaLetra)){
									if(tipos[i] == TIPO_JSON.NULL
											|| tipos[i] == TIPO_JSON.NUMERICO)
										sbConteudo.append(letra);
									break;
								}
								sbConteudo.append(letra);
							}

							ponteiro = new PonteiroJson();
							ponteiro.refereciaJson = REFERENCIA_JSON.CONTEUDO_COMPLETO;
							ponteiro.nivel = pilha.size();
							ponteiro.posicaoArquivo = getPonteiroLeitura();
							String conteudo = sbConteudo.toString();
							if (tipos[i] == TIPO_JSON.NULL) {
								// setando o valor do conteudo para nulo se a
								// string for 'null'
								if (conteudo.trim().equalsIgnoreCase("null"))
									conteudo = null;
							}
							ponteiro.conteudo = conteudo;
							ponteiro.tipoJson = tipos[i];
							break;

						} else if (tipos[i] == TIPO_JSON.OBJETO) {

							if (pilha.size() == 0) {
								ponteiro = new PonteiroJson();
								ponteiro.posicaoArquivo = getPonteiroLeitura();
								ponteiro.refereciaJson = REFERENCIA_JSON.DELIMITADOR_INICIAL;
								ponteiro.nivel = pilha.size();
								ponteiro.conteudo = null;
								ponteiro.tipoJson = tipos[i];
								pilha.push(ponteiro);
								break;
							} else {
								ponteiro = new PonteiroJson();
								ponteiro.posicaoArquivo = getPonteiroLeitura();
								ponteiro.refereciaJson = REFERENCIA_JSON.DELIMITADOR_INICIAL;
								if (ultimoPonteiro.tipoJson == TIPO_JSON.NOME_ATRIBUTO) {

									ponteiro.nivel = pilha.size();
									ponteiro.nome = ultimoPonteiro.conteudo;
									ponteiro.tipoJson = tipos[i];
									pilha.push(ponteiro);
									break;
								} else {

									if (topo.tipoJson == TIPO_JSON.VETOR) {

										ponteiro.nivel = pilha.size();
										ponteiro.nome = ultimoPonteiro.conteudo;
										ponteiro.tipoJson = tipos[i];
										ponteiro.posicaoVetor = topo.numeroDeTermos++;
										pilha.push(ponteiro);
										break;
									}

								}

							}
						} else if (tipos[i] == TIPO_JSON.VETOR) {

							if (ultimoPonteiro.tipoJson == TIPO_JSON.NOME_ATRIBUTO) {
								ponteiro = new PonteiroJson();
								ponteiro.posicaoArquivo = getPonteiroLeitura();
								ponteiro.refereciaJson = REFERENCIA_JSON.DELIMITADOR_INICIAL;
								ponteiro.nivel = pilha.size();
								ponteiro.nome = ultimoPonteiro.conteudo;
								ponteiro.tipoJson = tipos[i];
								ponteiro.numeroDeTermos = 0;
								pilha.push(ponteiro);
								break;
							} else {
								throw new Exception(
										"Condicional nao programada -1"
												+ f.getName() + "::"
												+ getPonteiroLeitura());
							}

						} else
							throw new Exception("Condicional nao programada -2"
									+ f.getName() + "::" + getPonteiroLeitura());

					} else if (tipos[i].verificaDelimitadorFinal(letra,
							ultimaLetra)) {
						if (tipos[i] == TIPO_JSON.OBJETO
								|| tipos[i] == TIPO_JSON.VETOR) {
							PonteiroJson ultimoPontoRef = pilha.peek();
							if (ultimoPontoRef.tipoJson == tipos[i]) {
								PonteiroJson removido = pilha.pop();
								ponteiro = new PonteiroJson();
								ponteiro.posicaoArquivo = getPonteiroLeitura();
								ponteiro.refereciaJson = REFERENCIA_JSON.DELIMITADOR_FINAL;
								ponteiro.nivel = pilha.size();
								ponteiro.nome = removido.nome;
								ponteiro.tipoJson = tipos[i];

								break;
							} else
								throw new Exception(
										"Condicional nao programada 0"
												+ f.getName() + "::"
												+ getPonteiroLeitura());

						} else
							throw new Exception("Condicional nao programada 2"
									+ f.getName() + "::" + getPonteiroLeitura());
					}
				}
				return ponteiro;
			}
		} finally {
			ultimoPonteiro = ponteiro;
		}

	}

	// long ponteiroArquivo = 0;
	public long getPonteiroLeitura() throws IOException {
		long ponteiro = reader.getFilePointer();
		ponteiro -= (lidos - iBuffer);
		return ponteiro;
	}

	private Byte proximoByte() {
		try {
			if(reader == null)
				return null;
			if (iBuffer >= lidos && lidos >= 0) {
				lidos = reader.read(buffer);
				if (lidos <= 0)
					return null;
				else
					iBuffer = 0;
			}
			return buffer[iBuffer++];
		} catch (IOException e) {
			SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
			return null;
		}
	}

	public Character proximaLetra() {
		Byte b1 = proximoByte();
		// Byte b2= proximoByte();
		// if(b1 == null || b2 == null) return null;
		if (b1 == null)
			return null;
		bLetra = b1;
		ultimaLetra = letra;
		letra = (char) (b1 & 0xFF);
		return letra;
	}
	
	public String getProximoObjeto() {

		try {
			final String strNull = "null";
			int iNull = 0;
			final char[] constNull = strNull.toCharArray();
			//StringBuilder sb = new StringBuilder();
			ArrayList<Byte> list = new ArrayList<Byte>(1024);
			boolean comecou = false;
			int nivel = 0;
			if (!ultimoPonteiro.inicioObjeto())
				throw new Exception(
						"O mutodo deve ser chamado apenas quando o ponteiro estiver no inicio do vetor"
								+ f.getName() + "::" + getPonteiroLeitura());
			while (letra != null) {
				if (letra == null)
					return null;
				else if (letra == '{') {
					nivel++;
					comecou = true;
				} else if (comecou && letra == '}' && ultimaLetra != '\\') {

					if (nivel == 1)
						break;
					else
						nivel--;
				} else if (!comecou && letra == constNull[iNull]) {
					iNull++;
					if (iNull == constNull.length)
						return null;
				} else if (comecou) {
					//sb.append(letra);
					list.add(bLetra);
				}
				proximaLetra();
			}
			//if (sb.length() > 0) {
			if(list.size() > 0) {
				ultimoPonteiro.refereciaJson = REFERENCIA_JSON.DELIMITADOR_FINAL;
				ultimoPonteiro.posicaoArquivo = getPonteiroLeitura();
				pilha.pop();
				//return sb.toString();
				byte[] buf = new byte[list.size()];
				for(int i = 0 ; i < list.size(); i++){
					buf[i] = (byte)list.get(i);
				}
				
				String n = new String( buf, "UTF8");
				
				return n;
			} else
				return null;
		} catch (Exception e) {

			SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
			return null;
		}
	}

	public String[] getProximoVetor() {

		try {
			final String strNull = "null";
			int iNull = 0;
			final char[] constNull = strNull.toCharArray();
			StringBuilder sb = new StringBuilder();
			boolean comecou = false;
			int nivel = 0;
			if(ultimoPonteiro.isNull()){
				return null;
			}
			else if (!ultimoPonteiro.inicioVetor())
				throw new Exception(
						"O mutodo deve ser chamado apenas quando o ponteiro estiver no inicio do vetor."
								+ f.getName() + "::" + getPonteiroLeitura());
			while (letra != null) {
				if (letra == null)
					return null;
				else if (letra == '[') {
					nivel++;
					comecou = true;
					sb.append(letra);
				} else if (comecou && letra == ']' && ultimaLetra != '\\') {

					if (nivel == 1) {
						sb.append(letra);
						break;
					} else
						nivel--;
				} else if (!comecou && letra == constNull[iNull]) {
					iNull++;
					if (iNull == constNull.length)
						return null;
				} else if (comecou) {
					sb.append(letra);
				}
				proximaLetra();
			}
			if (sb.length() > 0) {
				ultimoPonteiro.refereciaJson = REFERENCIA_JSON.DELIMITADOR_FINAL;
				ultimoPonteiro.posicaoArquivo = getPonteiroLeitura();
				pilha.pop();
				String token = sb.toString();
				JSONArray jsonCampos = new JSONArray(token);
				String[] campos = new String[jsonCampos.length()];
				for (int i = 0; i < jsonCampos.length(); i++) {
					campos[i] = jsonCampos.getString(i);
				}
				return campos;
			} else
				return null;
		} catch (Exception e) {

			SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
			return null;
		}
	}

	public JSONObject getProximoJSONObject() throws JSONException {
		String str = getProximoObjeto();
		if (str == null)
			return null;
		else {
			JSONObject json = new JSONObject(str);
			return json;
		}
	}
	public static Integer getCodRetorno(String path) {
		String json =null; 
		JSONObject vObj;
		try {
			json = HelperFile.getFileContents(new File(path));
			vObj = new JSONObject(json);
			int vCodRetorno = vObj.getInt("mCodRetorno");
			return vCodRetorno;
		} catch (Exception e) {
			SingletonLog.insereErro(
					e,
					SingletonLog.TIPO.BIBLIOTECA_NUVEM,
					json);
			return null;
		}	
	}
}
