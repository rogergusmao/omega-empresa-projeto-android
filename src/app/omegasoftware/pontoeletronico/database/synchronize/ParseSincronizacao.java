package app.omegasoftware.pontoeletronico.database.synchronize;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoOperacaoBanco;
import app.omegasoftware.pontoeletronico.database.synchronize.ParseJson.PonteiroJson;
import app.omegasoftware.pontoeletronico.database.synchronize.ParseJson.REFERENCIA_JSON;
import app.omegasoftware.pontoeletronico.database.synchronize.ParseJson.TIPO_JSON;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class ParseSincronizacao {
	File f;
	Database db=null;
	Context context;
	String idSincronizacao;
	OmegaLog log;
	ParseSincronizacaoMobile parseSincronizacaoMobile;

	public enum ORIGEM_CRUD {
		WEB(1), MOBILE(2);
		int id;

		ORIGEM_CRUD(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}
	}

	public boolean fileExists() {
		if (this.f == null)
			return true;
		else
			return this.f.exists();
	}

	ArrayList<Integer> idsRemovidos = new ArrayList<Integer>();
	ArrayList<Integer> idsMobileIdentificador = new ArrayList<Integer>();

	ArrayList<Integer> idsInseridos = new ArrayList<Integer>();
	ArrayList<Integer> idsEditados = new ArrayList<Integer>();
	PonteiroJson tabelaAtual;

	// boolean primeiroParsemanetoQueOMobileEstaRealizandoNaVida = false;
	ParseJson parse;
	ArrayList<String> tabelas = null;
	String datetimeSql = "";

	public String getIdentificador() {
		String identificadorErro = "IdSincronizacao: " + idSincronizacao;
		return identificadorErro;
	}

	public ParseSincronizacao(Database db, Context c, Long idSincronizacao, OmegaLog log) {
		this.f = getFileSincronizacao(idSincronizacao);
		this.db = db;
		this.context = c;

		this.idSincronizacao = String.valueOf(idSincronizacao);
		this.tabelas = new ArrayList<String>();
		this.log = log;

	}

	public String getIdSincronizacao() {
		return idSincronizacao.toString();
	}

	public void apagaArquivo() {
		if (this.f != null) {
			if (parse != null)
				parse.close();
			if (this.f.exists())
				this.f.delete();
		}

	}

	public static File getFileSincronizacao(Long idSincronizacao) {
		OmegaFileConfiguration ofc = new OmegaFileConfiguration();
		String path = ofc.getPath(OmegaFileConfiguration.TIPO.SINCRONIZACAO);
		String pathFile = path + OmegaSecurity.getCorporacao() + "_" + idSincronizacao.toString()
				+ "_sincronizacao.zip";
		File f = new File(pathFile);
		return f;
	}

	public PonteiroJson getTabelaAtual() throws Exception {
		if (tabelaAtual == null) {
			tabelaAtual = proximaTabela();
		}
		return tabelaAtual;
	}

	public Mensagem leCabecalho() throws Exception {
		PonteiroJson ponteiro = parse.getProximoPonteiro();
		if (ponteiro == null)
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO);
		PonteiroJson sincronizacao = parse.getProximoPonteiro("sincronizacao");
		if (sincronizacao != null) {
			String idSincronizacao = sincronizacao.conteudo;
			if (!idSincronizacao.equalsIgnoreCase(this.idSincronizacao))
				throw new Exception("O arquivo contem outra sincronizacao. Deveria conter o id_sincronizador = "
						+ this.idSincronizacao + " mas contem o " + idSincronizacao);
		} else {
			ponteiro = parse.getUltimoPonteiro();
			if (ponteiro != null && ponteiro.nome != null && ponteiro.nome.equalsIgnoreCase("mCodRetorno")
					&& PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.isEqual(ponteiro.conteudo)) {
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_DOWNLOAD);
			} else
				throw new Exception(
						"Nao encontrou o identificador de sincronizacao no arquivo: " + this.idSincronizacao);
		}
		PonteiroJson ponteiroData = parse.getProximoPonteiro("data");
		if (ponteiroData != null) {
			String strData = ponteiroData.conteudo;
			datetimeSql = strData;

		}

		PonteiroJson ponteiroTabelas = parse.getProximoPonteiro("tabelas");
		if (ponteiroTabelas != null) {
			ponteiroTabelas = parse.getProximoPonteiro();
			while (ponteiroTabelas != null && (ponteiroTabelas.tipoJson != TIPO_JSON.VETOR
					&& ponteiroTabelas.refereciaJson != REFERENCIA_JSON.DELIMITADOR_FINAL)) {
				String tabela = ponteiroTabelas.conteudo;
				tabelas.add(tabela);
				ponteiroTabelas = parse.getProximoPonteiro();
			}
		}

		PonteiroJson ponteiroCruds = parse.getProximoPonteiro("cruds");
		if (ponteiroCruds == null)
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO);
		else {
			if (ponteiroCruds.tipoJson == TIPO_JSON.VETOR
					&& ponteiroCruds.refereciaJson == REFERENCIA_JSON.DELIMITADOR_INICIAL)
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
			else
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO);
		}
	}

	public String[] lerValoresDoCrud(CrudInserirEditar crud) throws Exception {

		if (crud.valoresNulo)
			return null;
		String[] valores = parse.leVetorNaPosicao((long) crud.posicaoInicioValores, (long) crud.posicaoFinalValores);
		crud.valores = valores;

		return valores;
	}

	public MapeamentoCrudInserirEditar mapeaCrudInserirEditar(EXTDAOSistemaTipoOperacaoBanco.TIPO tipo,
			Integer idSistemaTabela) throws Exception {

		PonteiroJson ponteiro = parse.getProximoPonteiroThrowException("campos");
		// if(ponteiro.tipoJson == TIPO_JSON.STRING &&
		// ponteiro.conteudo.equalsIgnoreCase("null"))
		// return null;
		// ignora o conteudo do vetor
		String[] vetorCampos = parse.getProximoVetor();
		MapeamentoCrudInserirEditar mapa = new MapeamentoCrudInserirEditar();
		mapa.campos = vetorCampos;

		HashMap<Long, CrudInserirEditar> cruds = new HashMap<Long, CrudInserirEditar>();

		ponteiro = parse.getProximoPonteiroThrowException("protocolos");
		if (ponteiro.inicioVetor()) {
			ponteiro = parse.getProximoPonteiro();
			while (!ponteiro.finalVetor()) {

				ponteiro = parse.getProximoPonteiroThrowException("idCrud");
				Long idCrud = ponteiro.getConteudoLong();

				ponteiro = parse.getProximoPonteiroThrowException("idCrudOrigem");
				Integer idCrudOrigem = ponteiro.getConteudoInteger();

				ponteiro = parse.getProximoPonteiroThrowException("idTabelaWeb");
				Long idTabelaWeb = ponteiro.getConteudoLong();

				ponteiro = parse.getProximoPonteiroThrowException("idMobileIdentificador");
				Integer idMobileIdentificador = ponteiro.getConteudoInteger();

				ponteiro = parse.getProximoPonteiroThrowException("idSistemaRegistroSincronizador");
				Long idSistemaRegistroSincronizador = ponteiro.getConteudoLong();

				ponteiro = parse.getProximoPonteiroThrowException("valores");
				long ponteiroInicioValores = parse.getPonteiroLeitura();
				// ignora o vetor de valores
				parse.getProximoVetor();
				long ponteiroFinalValores = parse.getPonteiroLeitura();
				parse.getPonteiroLeitura();
				CrudInserirEditar crud = null;
				crud = new CrudInserirEditar(tipo);
				crud.idCrud = idCrud;
				if (ponteiro.isNull())
					crud.valoresNulo = true;
				else {
					crud.posicaoInicioValores = ponteiroInicioValores - 1;
					crud.posicaoFinalValores = ponteiroFinalValores;
				}

				crud.idMobileIdentificador = idMobileIdentificador;
				crud.idTabelaWeb = idTabelaWeb;
				crud.idCrudOrigem = idCrudOrigem;
				crud.idSistemaRegistroSincronizador = idSistemaRegistroSincronizador;
				crud.idSistemaTabela = idSistemaTabela;
				if (!cruds.containsKey(idTabelaWeb)) {
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("ParserSincronizacao::mapeaCrudInserirEditar::Id Sincronizacao: "
								+ this.idSincronizacao + ". Id Crud " + idCrud + " mapeado. Tipo: " + tipo.toString()
								+ " INSERIDO!!!");
					cruds.put(idTabelaWeb, crud);
				} else {
					// Considera sempre o identificador mais novo
					CrudInserirEditar c = cruds.get(idTabelaWeb);
					cruds.remove(idTabelaWeb);
					cruds.put(idTabelaWeb, crud);
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("ParserSincronizacao::mapeaCrudInserirEditar::Id Sincronizacao: "
								+ this.idSincronizacao + ". Id Crud " + idCrud + " mapeado. Tipo: " + tipo.toString()
								+ " ATUALIZADO pois ju existia o crud mais antigo " + c.getIdentificador()
								+ " relacionado ao mesmo id web.");
				}
				ponteiro = parse.getProximoPonteiro();
				if (!ponteiro.finalObjeto())
					throw new Exception(
							"Formato invulido do arquivo, era esperado o delimitador do final do objeto crud de remouuo.");
				ponteiro = parse.getProximoPonteiro();
			}
		}

		mapa.cruds = cruds;
		return mapa;

	}

	public HashMap<Long, CrudRemover> mapeaCrudRemover(Integer idSistemaTabela) throws Exception {

		PonteiroJson ponteiro = parse.getProximoPonteiro("cruds");
		if (ponteiro.inicioVetor()) {
			ponteiro = parse.getProximoPonteiro();
			if (ponteiro.finalVetor())
				return null;
		}
		HashMap<Long, CrudRemover> hashRemover = new HashMap<Long, CrudRemover>();

		while (ponteiro != null && !ponteiro.finalVetor()) {
			PonteiroJson inicioObjeto = ponteiro;
			ponteiro = parse.getProximoPonteiro();
			if(ponteiro == null || !ponteiro.isEqual("idCrud")){
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("mapeaCrudRemover::Id Sincronizacao: " + this.idSincronizacao + ". Entrou no caso excecao de fim de vetor!");
				continue;
			}
			Long idCrud = ponteiro.getConteudoLong();
			ponteiro = parse.getProximoPonteiroThrowException("idTabelaWeb");
			Long idTabelaWeb = ponteiro.getConteudoLong();
			ponteiro = parse.getProximoPonteiroThrowException("idMobileIdentificador");
			Integer idMobileIdentificador = ponteiro.getConteudoInteger();
			ponteiro = parse.getProximoPonteiroThrowException("idCrudOrigem");
			Integer idCrudOrigem = ponteiro.getConteudoInteger();
			ponteiro = parse.getProximoPonteiroThrowException("idSistemaRegistroSincronizador");
			Long idSistemaRegistroSincronizador = ponteiro.getConteudoLong();

			// final objeto
			ponteiro = parse.getProximoPonteiro();
			// inicio proximo objeto ou final vetor
			ponteiro = parse.getProximoPonteiro();
			if (!hashRemover.containsKey(idTabelaWeb)) {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("mapeaCrudRemover::Id Sincronizacao: " + this.idSincronizacao + ". Id Crud: "
							+ idCrud + " mapeado e inserido");

				CrudRemover crud = new CrudRemover();
				crud.idCrud = idCrud;

				crud.inicioObjeto = inicioObjeto;
				crud.idMobileIdentificador = idMobileIdentificador;
				crud.idTabelaWeb = idTabelaWeb;
				crud.idCrudOrigem = idCrudOrigem;
				crud.idSistemaRegistroSincronizador = idSistemaRegistroSincronizador;
				crud.idSistemaTabela = idSistemaTabela;
				if (crud.idSistemaTabela == null) {
					throw new Exception("Id sistema tabela nulo");
				}
				hashRemover.put(idTabelaWeb, crud);
			} else {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("mapeaCrudRemover::Id Sincronizacao: " + this.idSincronizacao + ". Id Crud " + idCrud
							+ " mapeado e ignorado pois ju temos uma remouuo do registro de id: " + idTabelaWeb);
			}
			// if(!ponteiro.finalObjeto())
			// throw new Exception("Formato invulido do arquivo, era esperado o
			// delimitador do final do objeto crud de remouuo.");
		}

		return hashRemover;

	}

	public CrudsSincronizacao mapeaOperacoesDaTabela(Integer idSistemaTabela) throws Exception {

		
		PonteiroJson ponteiro = parse.getProximoPonteiro();

		if (ponteiro.nome != null && ponteiro.nome.equalsIgnoreCase("operacoes")) {
			ponteiro = parse.getProximoPonteiro();
		}

		if (ponteiro.nome != null && ponteiro.tipoJson == TIPO_JSON.VETOR
				&& ponteiro.refereciaJson == REFERENCIA_JSON.DELIMITADOR_INICIAL) {
			ponteiro = parse.getProximoPonteiro();
			if (ponteiro.nome != null && ponteiro.tipoJson == TIPO_JSON.VETOR
					&& ponteiro.refereciaJson == REFERENCIA_JSON.DELIMITADOR_FINAL)
				return null;
		}
		HashMap<Long, CrudRemover> hashRemover = null;
		HashMap<Long, CrudInserirEditar> hashEditar = null;
		HashMap<Long, CrudInserirEditar> hashInserir = null;

		String[] camposInserir = null;
		String[] camposEditar = null;
		// Se for um elemento do vetor operacoes
		while (!ponteiro.finalVetor("operacoes")) {

			ponteiro = parse.getProximoPonteiroThrowException("tipoOperacao");
			Integer tipoOperacaoBanco = ponteiro.getConteudoInteger();
			if (EXTDAOSistemaTipoOperacaoBanco.TIPO.REMOVE.isEqual(tipoOperacaoBanco)) {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("mapeaOperacoesDaTabela:: call mapeaCrudRemover");
				// msg = processaCrudRemove(json);
				hashRemover = mapeaCrudRemover(idSistemaTabela);
			} else if (EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT.isEqual(tipoOperacaoBanco)) {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("mapeaOperacoesDaTabela:: call mapeaCrudInserirEditar tipo INSERT");
				// msg = processaCrudInsert(json);
				MapeamentoCrudInserirEditar mapa = mapeaCrudInserirEditar(EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT,
						idSistemaTabela);
				hashInserir = mapa.cruds;
				camposInserir = mapa.campos;
			} else if (EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT.isEqual(tipoOperacaoBanco)) {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("mapeaOperacoesDaTabela:: call mapeaCrudInserirEditar tipo EDIT");
				// msg = processaCrudEdit(json);
				MapeamentoCrudInserirEditar mapa = mapeaCrudInserirEditar(EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT,
						idSistemaTabela);
				hashEditar = mapa.cruds;
				camposEditar = mapa.campos;
			}
			// Fim do objeto termo do vetor PROTOCOLOS
			ponteiro = parse.getProximoPonteiro();

			ponteiro = parse.getProximoPonteiro();
		}
		// Final do objeto
		ponteiro = parse.getProximoPonteiro();
		CrudsSincronizacao cruds = new CrudsSincronizacao();
		cruds.hashRemover = hashRemover;
		cruds.hashEditar = hashEditar;
		cruds.hashInserir = hashInserir;
		cruds.camposEditar = camposEditar;
		cruds.camposInserir = camposInserir;
		return cruds;
	}

	public PonteiroJson proximaTabela() throws Exception {

		// Nessa fase do processo o mobile iru apenas atualizar os
		// identificadores de ediuuo e inseruuo,
		// o restante dos dados relacionados a duplicatas seruo processados
		// naturalmente durante o parseamento
		// dos dados contidos nos arquivos de sincronizacao

		// Atributo codRetorno = parse.getProximoAtributo("mCodRetorno");
		// sincronizacao

		
		PonteiroJson ponteiro = parse.getProximoPonteiro();

		if (ponteiro.tipoJson == TIPO_JSON.OBJETO && ponteiro.refereciaJson == REFERENCIA_JSON.DELIMITADOR_INICIAL
				&& ponteiro.posicaoVetor != null) {

			tabelaAtual = parse.getProximoPonteiro("tabela");
			return tabelaAtual;

		} else
			return null;
	}

	boolean isParseOpen = false;

	public boolean open() throws FileNotFoundException {
		try {
			if (parse != null)
				parse.close();
			parse = new ParseJson(this.f, log);
			isParseOpen = parse.open();
			return isParseOpen;
		} catch (IOException ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);
			return false;
		} catch (Exception ex2) {
			SingletonLog.insereErro(ex2, SingletonLog.TIPO.SINCRONIZADOR);
			return false;
		}
	}

	public boolean isOpen() {
		return isParseOpen;
	}

	public void close() {
		if (parse != null) {
			parse.close();
		}
		isParseOpen = false;
	}

	public static Boolean validaArquivoSincronizacaoWeb(String path) {
		File f;
		FileReader fr = null;
		try {

			f = new File(path);
			fr = new FileReader(f);

			String startsWith = "{\"sincronizacao\":";
			char[] buf = new char[startsWith.length()];
			int totalLido = fr.read(buf);
			if (totalLido == -1)
				return null;

			int iStartsWith = 0;
			if (totalLido == startsWith.length()) {
				for (int i = 0; i < totalLido; i++) {

					if ((char) buf[i] != startsWith.charAt(iStartsWith++)) {
						return false;
					}
				}
				return true;
			}
			return null;
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);
			return false;
		} finally {
			if (fr != null)
				try {
					fr.close();
				} catch (IOException e) {

					SingletonLog.insereErro(e, SingletonLog.TIPO.SINCRONIZADOR);

				}
		}
	}
	
	// Processa as operacoes 'remove' do arquivo crud_mobile
	// public Mensagem processaCrudEdit(JSONObject pJson){
	//
	// //Processa a ediuuo de um identificador uma unica vez, comeuando do
	// ultimo arquivo de sincronizacao
	// //atu o primeiro
	// //crud_id_INT != null // Operacao foi realizada com sucesso
	// //CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI_CASCADE - o
	// registro deve ser removido
	// //Database::CHAVE_DUPLICADA => Atualiza o identificador local e os dados
	// locais, su u executado depois
	// // do parseamento dos arquivos de sincronizacao
	// //Database::ERRO_EXECUCAO => Resetar o banco local
	// //Erro nao identificado - atualiza os dados com os valores da web - esse
	// passo so sera realizado quando
	// // os arquivos da sincronizacao forem executados.
	// try {
	// String nomeTabela =pJson.getString("tabela");
	// String idSistemaTabela = EXTDAOSistemaTabela.getIdSistemaTabela(context,
	// nomeTabela);
	// JSONArray jsonCampos = pJson.getJSONArray("campos");
	// String[] campos = new String[jsonCampos.length()];
	// for(int i = 0; i < campos.length; i++){
	// campos[i] = jsonCampos.getString(i);
	// }
	// JSONArray jsonProtocolos = pJson.getJSONArray("protocolos");
	//
	// for(int i = 0; i < jsonProtocolos.length(); i++){
	// JSONObject jsonProtocolo = jsonProtocolos.getJSONObject(i);
	// String idCrud = jsonProtocolo.getString("idCrud");
	// String idMobileIdentificador=
	// jsonProtocolo.getString("idMobileIdentificador");
	// String idTabelaWeb = jsonProtocolo.getString("idTabelaWeb");
	// int crudOrigem = jsonProtocolo.getInt("idCrudOrigem");
	// if(crudOrigem == ORIGEM_CRUD.MOBILE.getId()){
	// if(!primeiroParsemanetoQueOMobileEstaRealizandoNaVida &&
	// idMobileIdentificador.equalsIgnoreCase(this.idMobileIdentificador)){
	// //A operacao foi executada pelo proprio mobile, logo nuo tem a
	// necessidade de repeti-la no banco local
	// continue;
	// }
	// }
	//
	//
	//
	// JSONArray jsonValores = jsonProtocolo.getJSONArray("valores");
	// String[] valores = new String[jsonValores.length()];
	// for(int j = 0; j < valores.length; j++){
	// valores[j] = jsonValores.getString(j);
	// }
	// if(campos.length != valores.length){
	// return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
	// "O numero temos do vetor 'campos' passado no crud_mobile deve ser o mesmo
	// dos valores."
	// + nomeTabela
	// + getIdentificadorErro(
	// idTabelaWeb,
	// idCrud));
	// }
	//
	// EXTDAOGenerico obj = new EXTDAOGenerico(this.db, nomeTabela);
	// for(int k = 0 ; k < valores.length; k++){
	// String valor = valores[k];
	//
	// obj.setAttrValue(campos[k], valores[k]);
	// }
	//
	// String id = obj.getStrValueOfAttribute(Table.ID_UNIVERSAL);
	//
	// //Verifica as chaves extrangeiras
	// boolean removido = false;
	// ArrayList<Attribute> atributosFk = obj.getListAttributeFK();
	// for(int l = 0 ; l < atributosFk.size(); l++){
	// Attribute attr = atributosFk.get(l);
	// String idFk = attr.getStrValue();
	// Table tableFk = attr.getTableFk(db);
	// //Se o atributo nao existe
	// if(!tableFk.select(idFk)){
	// //Verifica se ela foi removida anteriormente
	// //Verifica a existencia dos atributos no banco local
	// //Caso nao exista
	// // Verifica se estu no buffer dos id's removidos da tabela
	// // Caso positivo, verifica se a relacao u cascade ou set null, se
	// cascade, para a inseruuo. C.c. seta para nulo.
	//
	//// if(parseSincronizacaoMobile.idsRemovidos.contains(idFk)
	//// || this.idsRemovidos.contains(idFk)){
	////
	// if(attr.onUpdate == TYPE_RELATION_FK.CASCADE){
	// removido = true;
	// break;
	// //nao executa a operacao de insercao
	// } else if(attr.onUpdate == TYPE_RELATION_FK.SET_NULL){
	//
	// attr.setStrValue(null);
	// //nao executa a operacao de insercao
	// }
	//// }
	// }
	// //
	// }
	// if(removido){
	// //o registro teve de ser removido devido a remouuo do pai, logo nuo hu a
	// necessidade de inseri-lo
	// continue;
	// }
	// //Verifica se o motivo do ID existe
	// Table registro = obj.getFirstTupla();
	// if(registro == null){
	// //Considera-se no fim dos casos que o registro foi removido alguma hora
	// durante a sincronizacao
	// continue;
	//
	// }
	// obj.formatToSQLite();
	// //Tenta atualizar o registro
	// if(!obj.update(false) ){
	// EXTDAOGenerico obj2 = new EXTDAOGenerico(this.db, nomeTabela);
	// //Verifica se existe duplicada
	// ContainerUniqueKey uniqueKey = obj.containerUniqueKey;
	// String atributos[] = uniqueKey.getVetorAttribute();
	// for(int k = 0 ; k < atributos.length; k++){
	// String valor = obj.getStrValueOfAttribute(atributos[i]);
	// obj2.setAttrValue(atributos[i], valor);
	// }
	// registro = obj2.getFirstTupla();
	// //Se existe a duplicada
	// if(registro != null){
	// String idDuplicada = registro.getStrValueOfAttribute(Table.ID_UNIVERSAL);
	// //Esse registro deixara de existir e o conteudo vindo da WEB iru assumir
	// o lugar
	// //Sera atualizado o identificador das tabelas filhas que apontam para
	// //o registro.
	//
	// ArrayList<String[]> atributosDependentes =
	// EXTDAOSistemaAtributo.getAtributosQueApontamParaATabela(db,
	// idSistemaTabela);
	// for(int l = 0 ; l < atributosDependentes.size(); l ++){
	//// EXTDAOSistemaAtributo.ID ,
	//// EXTDAOSistemaAtributo.NOME ,
	//// EXTDAOSistemaAtributo.SISTEMA_ATRIBUTO_FK,
	//// EXTDAOSistemaAtributo.FK_SISTEMA_TABELA_ID_INT,
	//// EXTDAOSistemaAtributo.UPDATE_TIPO_FK ,
	//// EXTDAOSistemaAtributo.DELETE_TIPO_FK
	// String idSistemaTabelaDependente = atributosDependentes.get(l)[3];
	// String tabelaDependente =
	// EXTDAOSistemaTabela.getNomeSistemaTabela(context,
	// HelperInteger.parserInt( idSistemaTabelaDependente));
	// Table objDependente = db.factoryTable(tabelaDependente);
	// if(objDependente == null){
	// return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, "Nuo foi
	// possuvel construir a tabela: " + tabelaDependente);
	// }
	// String atributoDependente = atributosDependentes.get(l)[1];
	// objDependente.setAttrValue(atributoDependente, idDuplicada);
	// ArrayList<Table> registrosDependentes = objDependente.getListTable();
	// for(int m = 0 ; m < registrosDependentes.size(); m++){
	// registrosDependentes.get(m).setAttrValue(atributoDependente, id);
	// registrosDependentes.get(m).update(false);
	// }
	// }
	//
	// registro.remove(false);
	// //Retira a operauuo de inseruuo do sincronizador se existente
	// String idSincronizador =
	// registro.getIdSistemaRegistroSincronizadorAndroidParaWeb(idDuplicada);
	// if(idSincronizador != null){
	// EXTDAOSistemaRegistroSincronizadorAndroidParaWeb sincronizador = new
	// EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(this.db);
	// sincronizador.remove(idSincronizador, false);
	// }
	// if(obj.update(false)){
	// //operacao realizada com sucesso
	// continue;
	// } else {
	// return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, "Falha ao
	// inserir o registro, mesmo depois de tratar a duplicata.");
	// }
	// } else {
	// return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, "Motivo
	// desconhecido.");
	// }
	// } else {
	// idsEditados.add(HelperInteger.parserInteger( obj.getId()));
	// }
	// }
	// return new
	// Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
	// } catch (JSONException e) {
	// return new Mensagem(e);
	// }
	//
	// }
	//
	// {"protocolos":[{"idTabelaWebCrud":null,"idTabelaWeb":null,"idSincronizadorMobile":"2","erroSql":"1062",
	// "idTabelaMobile":"10999","valores":["10999","HJ","HJ","1"],"idSistemaTabela":"10","idCrud":null,
	// "idCrudMobile":"19","idTabelaWebDuplicada":"10999"}],"tabela":"profissao","tipoOperacao":"2","campos":["id","nome","nome_normalizado","corporacao_id_INT"]}
	// public Mensagem processaCrudInsert(JSONObject pJson) throws Exception{
	// //Processa a inseruuo de um identificador uma unica vez, comeuando do
	// ultimo arquivo de sincronizacao
	// //atu o primeiro
	// //crud_id_INT != null // Operacao foi realizada com sucesso
	// String nomeTabela =pJson.getString("tabela");
	// String idSistemaTabela = EXTDAOSistemaTabela.getIdSistemaTabela(context,
	// nomeTabela);
	// JSONArray jsonCampos = pJson.getJSONArray("campos");
	// String[] campos = new String[jsonCampos.length()];
	// for(int i = 0; i < campos.length; i++){
	// campos[i] = jsonCampos.getString(i);
	// }
	// JSONArray jsonProtocolos = pJson.getJSONArray("protocolos");
	//
	// for(int i = 0; i < jsonProtocolos.length(); i++){
	// JSONObject jsonProtocolo = jsonProtocolos.getJSONObject(i);
	// String idCrud = jsonProtocolo.getString("idCrud");
	// String idMobileIdentificador=
	// jsonProtocolo.getString("idMobileIdentificador");
	// String idTabelaWeb = jsonProtocolo.getString("idTabelaWeb");
	//
	// if(jsonProtocolo.getInt("idCrudOrigem") == ORIGEM_CRUD.MOBILE.getId()){
	// if(!primeiroParsemanetoQueOMobileEstaRealizandoNaVida &&
	// idMobileIdentificador.equalsIgnoreCase(this.idMobileIdentificador)){
	// //A operacao foi executada pelo proprio mobile, logo nuo tem a
	// necessidade de repeti-la no banco local
	// continue;
	// }
	// }
	//
	//
	// JSONArray jsonValores = jsonProtocolo.getJSONArray("valores");
	// String[] valores = new String[jsonValores.length()];
	// for(int j = 0; j < valores.length; j++){
	// valores[j] = jsonValores.getString(j);
	// }
	// EXTDAOGenerico obj = new EXTDAOGenerico(this.db, nomeTabela);
	// if(campos.length != valores.length)
	// return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, "O numero
	// de termos do vertor 'campos' estu diferente do de valores");
	// for(int k = 0 ; k < valores.length; k++){
	// String valor = valores[k];
	// obj.setAttrValue(campos[k], valores[k]);
	// }
	// obj.formatToSQLite();
	// String id = obj.getStrValueOfAttribute(Table.ID_UNIVERSAL);
	//
	// //Verifica as chaves extrangeiras
	// boolean removido = false;
	// ArrayList<Attribute> atributosFk = obj.getListAttributeFK();
	// for(int l = 0 ; l < atributosFk.size(); l++){
	// Attribute attr = atributosFk.get(l);
	// String idFk = attr.getStrValue();
	// Table tableFk = attr.getTableFk(db);
	// //Se o atributo nao existe
	// if(!tableFk.select(idFk)){
	// //Verifica se ela foi removida anteriormente
	//
	// //Verifica a existencia dos atributos no banco local
	// //Caso nao exista
	// // Verifica se estu no buffer dos id's removidos da tabela
	// // Caso positivo, verifica se a relacao u cascade ou set null, se
	// cascade, para a inseruuo. C.c. seta para nulo.
	//
	//// if(parseSincronizacaoMobile.idsRemovidos.contains(idFk)
	//// || this.idsRemovidos.contains(idFk)){
	////
	// if(attr.onUpdate == TYPE_RELATION_FK.CASCADE){
	// removido = true;
	// break;
	// //nao executa a operacao de insercao
	// } else if(attr.onUpdate == TYPE_RELATION_FK.SET_NULL){
	//
	// attr.setStrValue(null);
	// //nao executa a operacao de insercao
	// }
	//// }
	// }
	// //
	// }
	// if(removido){
	// //o registro teve de ser removido devido a remouuo do pai, logo nuo hu a
	// necessidade de inseri-lo
	// continue;
	// }
	// //Tenta inserir o registro
	// if(obj.insert(false) < 0){
	// EXTDAOGenerico obj2 = new EXTDAOGenerico(this.db, nomeTabela);
	// obj2.setAttrValue(EXTDAOGenerico.ID_UNIVERSAL,
	// obj.getStrValueOfAttribute(Table.ID_UNIVERSAL));
	// //Verifica se o motivo do ID ja estu ocupado
	// Table registro = obj2.getFirstTupla();
	// if(registro != null){
	//
	// obj.setAttrValue(Table.ID_UNIVERSAL, null);
	// //Tenta inserir denovo, agora utilizando o identificador gerado pelo
	// banco local
	// if(obj.insert(false) > 0){
	// //Se esse conseguir ser inserido, tenta atualizar o identificador
	// if(!obj.updateIdSynchronized(id)){
	// return new Mensagem(TIPO.ERRO_SINCRONIZACAO, "Erro de uma atualizacao do
	// identificador do banco local que deveria ter sido executada sem
	// problemas.");
	// } else{
	// //Operacao realizada com sucesso
	//
	// }
	// }
	// }
	//
	// //Verifica se existe duplicada
	// ContainerUniqueKey uniqueKey = obj.containerUniqueKey;
	// String atributos[] = uniqueKey.getVetorAttribute();
	// obj2.clearData();
	//
	// for(int k = 0 ; k < atributos.length; k++){
	// String valor = obj.getStrValueOfAttribute(atributos[i]);
	// obj2.setAttrValue(atributos[i], valor);
	// }
	// registro = obj2.getFirstTupla();
	// //Se existe a duplicada
	// if(registro != null){
	// String idDuplicada = registro.getStrValueOfAttribute(Table.ID_UNIVERSAL);
	// //Verifica se o registro estu cadastrado apenas localment - operacao de
	// insert na tabela do sincrnonizador
	// String idRegistroSincLocal =
	// EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.getIdDoRegistroNaFilaDeSincronizacao(db,
	// idDuplicada, registro.getName());
	// if(idRegistroSincLocal != null && idRegistroSincLocal.length() > 0){
	// //Esse registro deixara de existir e o conteudo vindo da WEB iru assumir
	// o lugar
	// //Sera atualizado o identificador das tabelas filhas que apontam para
	// //o registro.
	//
	// ArrayList<String[]> atributosDependentes =
	// EXTDAOSistemaAtributo.getAtributosQueApontamParaATabela(db,
	// idSistemaTabela);
	// for(int l = 0 ; l < atributosDependentes.size(); l ++){
	//// EXTDAOSistemaAtributo.ID ,
	//// EXTDAOSistemaAtributo.NOME ,
	//// EXTDAOSistemaAtributo.SISTEMA_ATRIBUTO_FK,
	//// EXTDAOSistemaAtributo.FK_SISTEMA_TABELA_ID_INT,
	//// EXTDAOSistemaAtributo.UPDATE_TIPO_FK ,
	//// EXTDAOSistemaAtributo.DELETE_TIPO_FK
	// String idSistemaTabelaDependente = atributosDependentes.get(l)[3];
	// String tabelaDependente =
	// EXTDAOSistemaTabela.getNomeSistemaTabela(context,
	// HelperInteger.parserInt( idSistemaTabelaDependente));
	// Table objDependente = db.factoryTable(tabelaDependente);
	// if(objDependente == null){
	// return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, "Nuo foi
	// possuvel construir a tabela: " + tabelaDependente);
	// }
	// String atributoDependente = atributosDependentes.get(l)[1];
	// objDependente.setAttrValue(atributoDependente, idDuplicada);
	// ArrayList<Table> registrosDependentes = objDependente.getListTable();
	// for(int m = 0 ; m < registrosDependentes.size(); m++){
	// registrosDependentes.get(m).setAttrValue(atributoDependente, id);
	// registrosDependentes.get(m).update(false);
	// }
	// }
	//
	// registro.remove(false);
	// String idSincronizador =
	// registro.getIdSistemaRegistroSincronizadorAndroidParaWeb(idDuplicada);
	// if(idSincronizador != null){
	// EXTDAOSistemaRegistroSincronizadorAndroidParaWeb sincronizador = new
	// EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(this.db);
	// sincronizador.remove(idSincronizador, false);
	// }
	// if(obj.insert(false) >= 0){
	// //operacao realizada com sucesso
	// continue;
	// } else {
	// return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, "Falha ao
	// inserir o registro, mesmo depois de tratar a duplicata.");
	// }
	// } else{
	// throw new Exception("Condicional nao programada");
	// //TODO
	// //Caso 1: ocorreu a edicao na web que possibilitou a ediuuo do registro
	// na web e que ocasionou a duplicata no mobile
	// // procura a ultima operacao de edicao do idDuplicadaLocal que estu
	// armazenado nos arquivos do sincronizador
	// //controlador.getUltimaEdicaoDoRegistro(idSistemaTabela, idWeb )
	// //Caso 2: ocorreu a edicao local que ocasionou a duplicidade - adicionar
	// (1) a frente do registro 'nome'(attr descritivo) da chave unica
	// // adiciona a remocao
	// // todo registro que tiver chave desse registro removido deve ser editado
	// para apontar
	//
	// }
	//
	// } else {
	// return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, "Motivo
	// desconhecido.");
	// }
	// }
	// }
	// return new
	// Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
	//
	//
	// }

	// public void processaCrudSincronizacao(){
	// //Comecando do ultimo ate o primeiro arquivo de sincronizacao
	// //monta a arvore de dependencia das tabelas
	// //comecando da menos dependente ate a mais dependente
	//
	// //Atualiza os identificadores dos registros locais
	// //cada arquivo de sincronizacao devera conter o ultimo identificador
	// //de todas as tabelas que possuem opeacao nele
	//
	//
	//
	// // processa os cruds:
	// // processaCrudSincronizacaoRemover();
	// // processaCrudSincronizacaoInserir();
	// // processaCrudSincronizacaoEditar();
	//
	// }
	//
	// public void processaCrudSincronizacaoRemover(){
	// //Tenta remover no banco local, nao u necessurio a verificauuo de erros
	// //armazene todos os identificadores executados pelo arquivos de
	// sincronizacao
	// //inclusive os do banco local
	// }
	//
	// public void processaCrudSincronizacaoEditar(){
	//
	// //A ediuuo de um determinado registro sera realizada utilizando os dados
	// da ultima operacao de edicao
	// //contida nos arquivos da sincronizacao, ou a insercao(case nao haja
	// nenhuma operacao de edicao com o mesmo identificador nos arquivos)
	// //Erros possuveis durante a execuuuo de uma operauuo de inseruuo:
	// // 1 - CHAVE_DUPLICADA com algum registro local
	// // - Se o registro local possui uma operauuo(ediuuo ou inseruuo) ainda
	// nuo sincronizada - entao os dados locais seruo sobrescritos com os dados
	// do insert,
	// // e a operacao de 'insert' sera retirada da tabela de sincronizacao
	// android->web
	// // - Se o registro local nuo possui uma operauuo(ediuuo ou inseruuo) para
	// envio
	// // Verifica se o identificador foi atualizado como nas etapas:
	// // processaCrudEditMobile ou processaCrudInsertMobile
	// // Caso positivo: atualiza os dados com os dados do arquivo mobile que
	// ainda nao haviam sido executados
	// // Caso negativo: erro de sincronizacao, resetar o banco
	// // 2 - ALGUMA CHAVE INVALIDA DOS ATRIBUTOS DO REGISTRO - resetar o banco
	// // 3 - ERRO NAO IDENTIFICADO - resetar o banco
	// }
	//
	// public void processaCrudSincronizacaoInserir(){
	// //O mobile devera percorrer o arquivo de sincronizacao verificar o maior
	// ultimo id de cada tabela
	// //depois para cada tabela ele devera atualizar os ids dos registros
	// //locais que ainda nao foram sincronizados para um numero maior que esses
	// // isso possibilitaru que nuo ocorra erros durante a operacao de insercao
	// //contida no arquivo de sincronizacao
	//
	// //Seru verificado se o identificador da insercao nao consta nas operacoes
	// de remocao contidas nos
	// //outros arquivso do sincronizador e com os dados de remocao do banco
	// local que ainda nao foram sincronizados
	// // Caso positivo: essa insercao nao sera realizada e a verificacao dos
	// dependentes que possuem
	// // chave extrangeira para esse registro nao inserido, tal como
	// PESSOA_EMPRESA::PESSOA_ID_INT
	// // relacionado com o atributo PESSOA::ID, seruo atualizadas para:
	// // CASCADE: nuo seru realizada a operacao de insercao do registro
	// PESSOA_EMPRESA,
	// // esse identificador seru armazenado junto com as operacoes de remouuo
	// // SET NULL: o identificador seru setado para nulo, e o fluxo tentaru
	// novamente realizar a operauuo de inseruuo
	//
	// //A insercao de um determinado registro sera realizada utilizando os
	// dados da ultima operacao de edicao
	// //contida nos arquivos da sincronizacao, ou a insercao(caso nao haja
	// nenhuma operacao de edicao com o mesmo identificador nos arquivos, se nuo
	// os valores da ediuuo iru prevalecer)
	// //Erros possuveis durante a execuuuo de uma operauuo de inseruuo:
	// // 1 - CHAVE_DUPLICADA com algum registro local
	// // - Se o registro local possui uma operauuo(ediuuo ou inseruuo) ainda
	// nuo sincronizada - entao os dados locais seruo sobrescritos com os dados
	// do insert,
	// // e a operacao de 'insert' sera retirada da tabela de sincronizacao
	// android->web
	// // - Se o registro local nuo possui uma operauuo(ediuuo ou inseruuo) para
	// envio
	// // Verifica se o identificador foi atualizado como nas etapas:
	// // processaCrudEditMobile ou processaCrudInsertMobile
	// // Caso positivo: atualiza os dados com os dados do arquivo mobile que
	// ainda nao haviam sido executados
	// // Caso negativo: erro de sincronizacao, resetar o banco
	// // 2 - ALGUMA CHAVE INVALIDA DOS ATRIBUTOS DO REGISTRO - resetar o banco
	// // 3 - ERRO NAO IDENTIFICADO - resetar o banco
	// }
	//
}
