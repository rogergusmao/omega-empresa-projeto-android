package app.omegasoftware.pontoeletronico.database.synchronize;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAOGenerico;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorAndroidParaWeb;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoOperacaoBanco;
import app.omegasoftware.pontoeletronico.database.synchronize.ParseJson.PonteiroJson;
import app.omegasoftware.pontoeletronico.database.synchronize.ParseJson.REFERENCIA_JSON;
import app.omegasoftware.pontoeletronico.database.synchronize.ParseJson.TIPO_JSON;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.primitivetype.HelperLong;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA.TIPO;

public class ParseSincronizacaoMobile {
	File f;
	Database db = null;
	String idSincronizacao;
	String idSincronizacaoMobile;
	PonteiroJson tabelaAtual;
	ArrayList<Long> idsRemovidos = new ArrayList<Long>();
	ArrayList<Long> idsInseridos = new ArrayList<Long>();
	ArrayList<Long> idsEditados = new ArrayList<Long>();
	Long idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb;
	ParseJson parse;
	ArrayList<String> tabelas;
	OmegaLog log;
	EXTDAOSistemaRegistroSincronizadorAndroidParaWeb objSRSAW = null;

	public ParseSincronizacaoMobile(Database db, Long idSincronizacao, Long idSincronizacaoMobile,
			Long idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb, OmegaLog log) {
		this.log = log;
		this.idSincronizacaoMobile = String.valueOf(idSincronizacaoMobile);
		this.idSincronizacao = String.valueOf(idSincronizacao);
		this.f = getFileSincronizacaoMobile(String.valueOf(idSincronizacaoMobile));
		this.db = db;
		objSRSAW = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(db);
		this.tabelas = new ArrayList<String>();
		this.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb = idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb;

	}

	public String getIdentificador() {
		return "Id sincronizacao: " + idSincronizacao + ". Id sincronizacao mobile: " + idSincronizacaoMobile;
	}

	public PonteiroJson getTabelaAtual() throws Exception {
		if (tabelaAtual == null) {
			tabelaAtual = proximaTabela();
		}
		return tabelaAtual;
	}

	public static File getFileSincronizacaoMobile(String idSincronizacaoMobile) {
		OmegaFileConfiguration ofc = new OmegaFileConfiguration();
		String path = ofc.getPath(OmegaFileConfiguration.TIPO.SINCRONIZACAO);
		String pathFile = path + OmegaSecurity.getCorporacao() + "_" + idSincronizacaoMobile
				+ "_sincronizacao_mobile.zip";
		File f = new File(pathFile);
		return f;
	}

	public void lerValoresDoCrud(CrudInserirEditar crud) throws Exception {

		if (crud.posicaoInicioValores != null && crud.posicaoFinalValores != null) {
			String[] valores = parse.leVetorNaPosicao((long) crud.posicaoInicioValores,
					(long) crud.posicaoFinalValores);
			crud.valores = valores;
		}

		if (crud.posicaoInicioValoresDuplicada != null && crud.posicaoFinalValoresDuplicada != null) {
			String[] valores = parse.leVetorNaPosicao((long) crud.posicaoInicioValoresDuplicada,
					(long) crud.posicaoFinalValoresDuplicada);
			crud.valoresDuplicada = valores;
		}

		if (crud.posicaoInicioDependentes != null && crud.posicaoFinalDependentes != null) {
			JSONArray jsonDependentes = parse.leJSONArrayNaPosicao((long) crud.posicaoInicioDependentes,
					(long) crud.posicaoFinalDependentes);
			if (jsonDependentes != null && jsonDependentes.length() > 0) {
				CrudDependente[] cs = new CrudDependente[jsonDependentes.length()];
				for (int i = 0; i < jsonDependentes.length(); i++) {
					JSONObject json = jsonDependentes.getJSONObject(i);
					String idCrudMobile = json.getString("idCrudMobile");
					String tipoOperacao = json.getString("tipoOperacao");
					String idSincronizadorMobile = json.getString("idSincronizadorMobile");
					String atributo = json.getString("atributo");
					String tabela = json.getString("tabela");
					CrudDependente c = new CrudDependente();
					c.atributo = atributo;
					c.tabela = tabela;
					c.tipoOperacao = HelperLong.parserInteger(tipoOperacao);
					c.idSincronizadorMobile = HelperLong.parserLong(idSincronizadorMobile);
					c.idCrudMobile = HelperLong.parserLong(idCrudMobile);
					cs[i] = c;
				}
				crud.crudsDependentes = cs;
			}
		}

	}

	boolean isParseOpen = false;

	public boolean open() throws FileNotFoundException {
		try {
			if (parse != null)
				parse.close();
			parse = new ParseJson(this.f, log);
			isParseOpen = parse.open();
			return isParseOpen;
		} catch (IOException ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);
			return false;
		} catch (Exception ex2) {
			SingletonLog.insereErro(ex2, SingletonLog.TIPO.SINCRONIZADOR);
			return false;
		}

	}

	public boolean fileExists() {
		if (this.f == null)
			return true;
		else
			return this.f.exists();
	}

	public boolean isOpen() {
		return isParseOpen;
	}

	public void close() {
		if (parse != null)
			parse.close();
		isParseOpen = false;
	}

	public void apagaArquivo() {
		if (this.f != null) {
			if (parse != null)
				parse.close();
			if (this.f.exists())
				this.f.delete();
		}

	}

	public Mensagem leCabecalho() throws Exception {
		if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("ParserSincronizacaoMobile::leCabecalho");
		parse.getProximoPonteiro();

		PonteiroJson sincronizacao = parse.getProximoPonteiro("sincronizacao");
		if (sincronizacao != null) {
			String idSincronizacao = sincronizacao.conteudo;
			if (!idSincronizacao.equalsIgnoreCase(this.idSincronizacao))
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_DOWNLOAD,
						"O arquivo contem outra sincronizacao. Deveria conter o id_sincronizador = "
								+ this.idSincronizacao + ". No lugar contem o id_sincronizador: " + idSincronizacao
								+ ". Path: " + parse.getPath());
		} else {
			PonteiroJson ponteiro2 = parse.getUltimoPonteiro();
			if (ponteiro2 != null && ponteiro2.nome != null && ponteiro2.nome.equalsIgnoreCase("mCodRetorno")
					&& PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.isEqual(ponteiro2.conteudo)) {
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_DOWNLOAD);
			} else
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_DOWNLOAD,
						"Nao encontrou o identificador de sincronizacao no arquivo: " + this.idSincronizacao
								+ ". Path: " + parse.getPath());
		}

		PonteiroJson sincronizacaoMobile = parse.getProximoPonteiro("sincronizacao_mobile");
		if (sincronizacaoMobile != null) {
			// String idSincronizacaoMobile = sincronizacaoMobile.conteudo;
			// VALIDAuuO NuO u VALIDA DEVIDO AO RESET
			// if (!idSincronizacaoMobile
			// .equalsIgnoreCase(this.idSincronizacaoMobile))
			// throw new Exception(
			// "O arquivo contem outra sincronizacao. Deveria conter o
			// id_sincronizador_mobile = "
			// + this.idSincronizacaoMobile
			// + " mas contem o "
			// + idSincronizacaoMobile);
		} else {
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_DOWNLOAD,
					"Nao encontrou o identificador de sincronizacao_mobile no arquivo: " + this.idSincronizacaoMobile);
		}

		PonteiroJson ponteiroTabelas = parse.getProximoPonteiro("tabelas");
		if (ponteiroTabelas != null) {
			ponteiroTabelas = parse.getProximoPonteiro();
			while (ponteiroTabelas != null && (ponteiroTabelas.tipoJson != TIPO_JSON.VETOR
					&& ponteiroTabelas.refereciaJson != REFERENCIA_JSON.DELIMITADOR_FINAL)) {
				String tabela = ponteiroTabelas.conteudo;
				tabelas.add(tabela);
				ponteiroTabelas = parse.getProximoPonteiro();
			}
		}
		PonteiroJson ponteiroCruds = parse.getProximoPonteiro("cruds");
		if (ponteiroCruds == null)
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO);
		else {
			if (ponteiroCruds.tipoJson == TIPO_JSON.VETOR
					&& ponteiroCruds.refereciaJson == REFERENCIA_JSON.DELIMITADOR_INICIAL)
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
			else
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO);
		}
	}

	public PonteiroJson proximaTabela() throws Exception {

		// Nessa fase do processo o mobile iru apenas atualizar os
		// identificadores de ediuuo e inseruuo,
		// o restante dos dados relacionados a duplicatas seruo processados
		// naturalmente durante o parseamento
		// dos dados contidos nos arquivos de sincronizacao

		// Atributo codRetorno = parse.getProximoAtributo("mCodRetorno");
		// sincronizacao

		PonteiroJson ponteiro = parse.getProximoPonteiro();

		if (ponteiro.tipoJson == TIPO_JSON.OBJETO && ponteiro.refereciaJson == REFERENCIA_JSON.DELIMITADOR_INICIAL
				&& ponteiro.posicaoVetor != null) {

			tabelaAtual = parse.getProximoPonteiro("tabela");
			return tabelaAtual;

		} else
			return null;
	}

	public CrudsSincronizacao mapeaOperacoesDaTabela(Integer idSistemaTabela) throws Exception {
		if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("ParserSincronizacaoMobile::mapeaOperacoesDaTabela. Id Sistema tabela: " + idSistemaTabela);

		PonteiroJson ponteiro = parse.getProximoPonteiro();

		if (ponteiro.nome != null && ponteiro.nome.equalsIgnoreCase("operacoes")) {

			ponteiro = parse.getProximoPonteiro();
		}

		if (ponteiro.nome != null && ponteiro.tipoJson == TIPO_JSON.VETOR
				&& ponteiro.refereciaJson == REFERENCIA_JSON.DELIMITADOR_INICIAL) {
			ponteiro = parse.getProximoPonteiro();
			if (ponteiro.nome != null && ponteiro.tipoJson == TIPO_JSON.VETOR
					&& ponteiro.refereciaJson == REFERENCIA_JSON.DELIMITADOR_FINAL)
				return null;
		}
		HashMap<Long, CrudRemover> hashRemover = null;

		HashMap<Long, CrudInserirEditar> hashEditar = null;
		HashMap<Long, CrudInserirEditar> hashInserir = null;
		String[] camposInserir = null;
		String[] camposEditar = null;
		LinkedList<CrudInserirEditar> listCrudsInserirMobile = null;
		// Se for um elemento do vetor operacoes
		while (!ponteiro.finalVetor("operacoes")) {

			ponteiro = parse.getProximoPonteiroThrowException("tipoOperacao");
			Integer tipoOperacaoBanco = ponteiro.getConteudoInteger();
			if (EXTDAOSistemaTipoOperacaoBanco.TIPO.REMOVE.isEqual(tipoOperacaoBanco)) {
				// msg = processaCrudRemove(json);
				hashRemover = mapeaCrudRemover(idSistemaTabela);
			} else if (EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT.isEqual(tipoOperacaoBanco)) {
				// msg = processaCrudInsert(json);
				MapeamentoCrudInserirEditar mapa = mapeaCrudInserir(idSistemaTabela);
				hashInserir = mapa.cruds;
				camposInserir = mapa.campos;
				listCrudsInserirMobile = mapa.crudsInserirMobile;
			} else if (EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT.isEqual(tipoOperacaoBanco)) {
				// msg = processaCrudEdit(json);
				MapeamentoCrudInserirEditar mapa = mapeaCrudEditar(idSistemaTabela);
				hashEditar = mapa.cruds;
				camposEditar = mapa.campos;
			}
			// Fim do objeto termo do vetor PROTOCOLOS
			ponteiro = parse.getProximoPonteiro();

			ponteiro = parse.getProximoPonteiro();
		}
		// Final do objeto
		ponteiro = parse.getProximoPonteiro();
		CrudsSincronizacao cruds = new CrudsSincronizacao();
		cruds.hashRemover = hashRemover;
		cruds.hashEditar = hashEditar;
		cruds.hashInserir = hashInserir;
		cruds.camposEditar = camposEditar;
		cruds.camposInserir = camposInserir;
		cruds.crudsInserirMobile = listCrudsInserirMobile;
		return cruds;
	}

	public MapeamentoCrudInserirEditar mapeaCrudInserir(Integer idSistemaTabela) throws Exception {

		PonteiroJson ponteiro = parse.getProximoPonteiroThrowException("campos");
		// if(ponteiro.tipoJson == TIPO_JSON.STRING &&
		// ponteiro.conteudo.equalsIgnoreCase("null"))
		// return null;
		// ignora o conteudo do vetor
		String[] vetorCampos = parse.getProximoVetor();
		MapeamentoCrudInserirEditar mapa = new MapeamentoCrudInserirEditar();
		mapa.campos = vetorCampos;

		HashMap<Long, CrudInserirEditar> cruds = new HashMap<Long, CrudInserirEditar>();
		LinkedList<CrudInserirEditar> crudsInserirMobile = new LinkedList<CrudInserirEditar>();
		ponteiro = parse.getProximoPonteiroThrowException("protocolos");
		if (ponteiro.inicioVetor()) {
			ponteiro = parse.getProximoPonteiro();
			while (!ponteiro.finalVetor()) {

				ponteiro = parse.getProximoPonteiroThrowException("idCrud");
				Long idCrud = ponteiro.getConteudoLong();
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
					if (idCrud != null)
						log.escreveLog(
								"ParserSincronizacaoMobile::mapeaCrudInserir. Id Crud Mapeado: " + idCrud.toString());
					else
						log.escreveLog(
								"ParserSincronizacaoMobile::mapeaCrudInserir. Id Crud Mapeado nulo. O atual conteudo do ponteiro u "
										+ ponteiro.getIdentificador());
				}

				ponteiro = parse.getProximoPonteiroThrowException("idTabelaMobile");
				Long idTabelaMobile = ponteiro.getConteudoLong();
				ponteiro = parse.getProximoPonteiroThrowException("idSincronizadorMobile");
				Long idSincronizadorMobile = ponteiro.getConteudoLong();

				ponteiro = parse.getProximoPonteiroThrowException("idTabelaWeb");
				Long idTabelaWeb = ponteiro.getConteudoLong();

				ponteiro = parse.getProximoPonteiroThrowException("idTabelaWebCrudMobile");
				Long idTabelaWebCrudMobile = ponteiro.getConteudoLong();
				if (idTabelaWeb == null)
					idTabelaWeb = idTabelaWebCrudMobile;

				ponteiro = parse.getProximoPonteiroThrowException("idTabelaWebDuplicada");
				Long idTabelaWebDuplicada = ponteiro.getConteudoLong();

				ponteiro = parse.getProximoPonteiroThrowException("idCrudMobile");
				Long idCrudMobile = ponteiro.getConteudoLong();

				ponteiro = parse.getProximoPonteiroThrowException("idSistemaTabela");
				ponteiro.getConteudoLong();
				// Long idSistemaTabela = ponteiro.getConteudoLong();

				ponteiro = parse.getProximoPonteiroThrowException("erroSql");
				Integer erroSql = ponteiro.getConteudoInteger();

				ponteiro = parse.getProximoPonteiroThrowException("idTabelaWebCrud");
				Long idTabelaWebCrud = ponteiro.getConteudoLong();

				CrudInserirEditar crud = new CrudInserirEditar(EXTDAOSistemaTipoOperacaoBanco.TIPO.INSERT);
				ponteiro = parse.getProximoPonteiroThrowException("valores");
				if (ponteiro.inicioVetor()) {
					crud.posicaoInicioValores = parse.getPonteiroLeitura() - 1;
					// ignora o vetor de valores
					parse.getProximoVetor();
					crud.posicaoFinalValores = parse.getPonteiroLeitura();
				}

				ponteiro = parse.getProximoPonteiroThrowException("valores_duplicada");
				if (ponteiro.inicioVetor()) {
					crud.posicaoInicioValoresDuplicada = parse.getPonteiroLeitura() - 1;
					// ignora o vetor de valores
					parse.getProximoVetor();
					crud.posicaoFinalValoresDuplicada = parse.getPonteiroLeitura();
				}

				ponteiro = parse.getProximoPonteiroThrowException("cruds_dependentes");
				if (ponteiro.inicioVetor()) {
					crud.posicaoInicioDependentes = parse.getPonteiroLeitura() - 1;
					// ignora o vetor de valores
					parse.getProximoVetor();
					crud.posicaoFinalDependentes = parse.getPonteiroLeitura();
				}

				parse.getPonteiroLeitura();
				crud.idTabelaMobile = idTabelaMobile;
				crud.idCrud = idCrud;
				crud.idSincronizadorMobile = idSincronizadorMobile;
				crud.idCrudMobile = idCrudMobile;
				crud.erroSql = erroSql;
				crud.idTabelaWebCrud = idTabelaWebCrud;
				crud.idTabelaWeb = idTabelaWeb;
				crud.idTabelaWebDuplicada = idTabelaWebDuplicada;
				crud.idSistemaTabela = idSistemaTabela;
				if (idTabelaWeb != null) {
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("ParserSincronizacaoMobile::mapeaCrudInserir. Id Tabela Web: "
								+ idTabelaWeb.toString());
					if (!cruds.containsKey(idTabelaWeb)) {
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog(
									"ParserSincronizacaoMobile::mapeaCrudInserir. Id Crud Mapeado: " + idCrud.toString()
											+ ". Foi adicionado a hash com a chave do idTabelaWeb: " + idTabelaWeb);
						cruds.put(idTabelaWeb, crud);
						crudsInserirMobile.add(crud);
					} else {
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("ParserSincronizacaoMobile::mapeaCrudInserir. Id Crud Mapeado: "
									+ idCrud.toString() + " foi aceito em prol do antigo que estava mapeado "
									+ cruds.get(idTabelaMobile).idCrud);
						// Considera sempre o identificador mais novo
						CrudInserirEditar crudASerRemovido = cruds.get(idTabelaWeb);
						crudsInserirMobile.remove(crudASerRemovido);
						cruds.remove(idTabelaWeb);

						cruds.put(idTabelaWeb, crud);
						crudsInserirMobile.add(crud);
					}
				} else {
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("ParserSincronizacaoMobile::mapeaCrudInserir. Id Tabela Web nulo.");
					crudsInserirMobile.add(crud);
				}

				ponteiro = parse.getProximoPonteiro();
				if (!ponteiro.finalObjeto())
					throw new Exception(
							"Formato invulido do arquivo, era esperado o delimitador do final do objeto crud de remouuo.");
				ponteiro = parse.getProximoPonteiro();
			}
		}

		mapa.cruds = cruds;
		mapa.crudsInserirMobile = crudsInserirMobile;
		return mapa;

	}

	public MapeamentoCrudInserirEditar mapeaCrudEditar(Integer idSistemaTabela) throws Exception {

		PonteiroJson ponteiro = parse.getProximoPonteiroThrowException("campos");
		// if(ponteiro.tipoJson == TIPO_JSON.STRING &&
		// ponteiro.conteudo.equalsIgnoreCase("null"))
		// return null;
		// ignora o conteudo do vetor
		String[] vetorCampos = parse.getProximoVetor();
		MapeamentoCrudInserirEditar mapa = new MapeamentoCrudInserirEditar();
		mapa.campos = vetorCampos;

		HashMap<Long, CrudInserirEditar> cruds = new HashMap<Long, CrudInserirEditar>();

		ponteiro = parse.getProximoPonteiroThrowException("protocolos");
		if (ponteiro.inicioVetor()) {
			ponteiro = parse.getProximoPonteiro();
			while (!ponteiro.finalVetor()) {

				ponteiro = parse.getProximoPonteiroThrowException("idCrud");
				Long idCrud = ponteiro.getConteudoLong();

				ponteiro = parse.getProximoPonteiroThrowException("idTabelaWeb");
				Long idTabelaWeb = ponteiro.getConteudoLong();

				ponteiro = parse.getProximoPonteiroThrowException("idTabelaWebCrudMobile");
				Long idTabelaWebCrudMobile = ponteiro.getConteudoLong();
				if (idTabelaWeb == null)
					idTabelaWeb = idTabelaWebCrudMobile;

				ponteiro = parse.getProximoPonteiroThrowException("idTabelaWebDuplicada");
				Long idTabelaWebDuplicada = ponteiro.getConteudoLong();

				ponteiro = parse.getProximoPonteiroThrowException("idSincronizadorMobile");
				Long idSincronizadorMobile = ponteiro.getConteudoLong();

				ponteiro = parse.getProximoPonteiroThrowException("idCrudMobile");
				Long idCrudMobile = ponteiro.getConteudoLong();

				ponteiro = parse.getProximoPonteiroThrowException("erroSql");
				Integer erroSql = ponteiro.getConteudoInteger();

				ponteiro = parse.getProximoPonteiroThrowException("idTabelaWebCrud");
				Long idTabelaWebCrud = ponteiro.getConteudoLong();

				CrudInserirEditar crud = new CrudInserirEditar(EXTDAOSistemaTipoOperacaoBanco.TIPO.EDIT);
				ponteiro = parse.getProximoPonteiroThrowException("valores");
				if (ponteiro.inicioVetor()) {
					crud.posicaoInicioValores = parse.getPonteiroLeitura() - 1;
					// ignora o vetor de valores
					parse.getProximoVetor();
					crud.posicaoFinalValores = parse.getPonteiroLeitura();
				}

				ponteiro = parse.getProximoPonteiroThrowException("valores_duplicada");
				if (ponteiro.inicioVetor()) {
					crud.posicaoInicioValoresDuplicada = parse.getPonteiroLeitura() - 1;
					// ignora o vetor de valores
					parse.getProximoVetor();
					crud.posicaoFinalValoresDuplicada = parse.getPonteiroLeitura();
				}

				ponteiro = parse.getProximoPonteiroThrowException("cruds_dependentes");
				if (ponteiro.inicioVetor()) {
					crud.posicaoInicioDependentes = parse.getPonteiroLeitura() - 1;
					// ignora o vetor de valores
					parse.getProximoVetor();
					crud.posicaoFinalDependentes = parse.getPonteiroLeitura();
				}

				parse.getPonteiroLeitura();

				crud.idCrud = idCrud;
				crud.idSincronizadorMobile = idSincronizadorMobile;
				crud.idCrudMobile = idCrudMobile;
				crud.erroSql = erroSql;
				crud.idTabelaWebCrud = idTabelaWebCrud;
				crud.idTabelaWeb = idTabelaWeb;
				crud.idTabelaWebDuplicada = idTabelaWebDuplicada;
				crud.idSistemaTabela = idSistemaTabela;
				if (!cruds.containsKey(idTabelaWeb)) {

					cruds.put(idTabelaWeb, crud);
				} else {
					// Considera sempre o identificador mais novo
					cruds.remove(idTabelaWeb);
					cruds.put(idTabelaWeb, crud);
				}
				ponteiro = parse.getProximoPonteiro();
				if (!ponteiro.finalObjeto())
					throw new Exception(
							"Formato invulido do arquivo, era esperado o delimitador do final do objeto crud de remouuo.");
				ponteiro = parse.getProximoPonteiro();
			}
		}

		mapa.cruds = cruds;
		return mapa;

	}

	public HashMap<Long, CrudRemover> mapeaCrudRemover(Integer idSistemaTabela) throws Exception {

		PonteiroJson ponteiro = parse.getProximoPonteiro("cruds");
		if (ponteiro.inicioVetor()) {
			ponteiro = parse.getProximoPonteiro();
			if (ponteiro.finalVetor())
				return null;
		}
		HashMap<Long, CrudRemover> hashRemover = new HashMap<Long, CrudRemover>();

		while (!ponteiro.finalVetor()) {
			PonteiroJson inicioObjeto = ponteiro;
			ponteiro = parse.getProximoPonteiroThrowException("idCrud");
			Long idCrud = ponteiro.getConteudoLong();
			ponteiro = parse.getProximoPonteiroThrowException("idTabelaWeb");
			Long idTabelaWeb = ponteiro.getConteudoLong();
			ponteiro = parse.getProximoPonteiroThrowException("idSincronizadorMobile");
			Long idSincronizadorMobile = ponteiro.getConteudoLong();
			ponteiro = parse.getProximoPonteiroThrowException("idCrudMobile");
			Long idCrudMobile = ponteiro.getConteudoLong();
			ponteiro = parse.getProximoPonteiroThrowException("erroSql");
			Integer erroSql = ponteiro.getConteudoInteger();

			// ponteiro =
			// parse.getProximoPonteiroThrowException("idSistemaRegistroSincronizador");
			// Long idSistemaRegistroSincronizador = ponteiro.getConteudoLong();
			Long idSistemaRegistroSincronizador = null; // os cruds nunca suo
														// web nos arquivos
														// mobiles

			// final objeto
			ponteiro = parse.getProximoPonteiro();
			// proximo objeto
			ponteiro = parse.getProximoPonteiro();

			if (!hashRemover.containsKey(idTabelaWeb)) {
				CrudRemover crud = new CrudRemover();
				crud.idCrud = idCrud;
				crud.inicioObjeto = inicioObjeto;
				crud.idCrudMobile = idCrudMobile;
				crud.idTabelaWeb = idTabelaWeb;
				crud.idSincronizadorMobile = idSincronizadorMobile;
				crud.idSistemaRegistroSincronizador = idSistemaRegistroSincronizador;
				crud.idSistemaTabela = idSistemaTabela;
				if (crud.idSistemaTabela == null) {
					throw new Exception("Id sistema tabela nulo");
				}
				crud.erroSql = erroSql;
				hashRemover.put(idTabelaWeb, crud);
			}

			// if(!ponteiro.finalObjeto())
			// throw new
			// Exception("Formato invulido do arquivo, era esperado o
			// delimitador do final do objeto crud de remouuo.");
		}

		return hashRemover;

	}

	// Processa as operacoes 'remove' do arquivo crud_mobile
	public Mensagem processaCrudRemoveMobile(JSONObject pJson) {
		// Nao eh necessario fazer nenhuma verificacao
		// apenas guardas os identificadores removidos
		// Processa a inseruuo de um identificador uma unica vez, comeuando do
		// ultimo arquivo de sincronizacao
		// atu o primeiro
		// crud_id_INT != null // Operacao foi realizada com sucesso

		try {

			JSONArray jsonCruds = pJson.getJSONArray("cruds");

			for (int i = 0; i < jsonCruds.length(); i++) {
				JSONObject jsonCrud = jsonCruds.getJSONObject(i);
				// jsonCrud.getString( "idSistemaTabela");
				String idTabelaWeb = jsonCrud.getString("idTabelaWeb");
				// Long erroSql = HelperLong.parserLong(
				// jsonCrud.getString("erroSql"));
				// if(erroSql != null){
				// if(Database.ERRO_SQL.CHAVE_INEXISTENTE){
				//
				// }
				// }
				// jsonCrud.getString("idSincronizadorMobile");
				// jsonCrud.getString("idCrudMobile");
				// jsonCrud.getString("idCrud");
				idsRemovidos.add(HelperLong.parserLong(idTabelaWeb));
			}

			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);

		} catch (JSONException e) {

			return new Mensagem(e);
		}
	}

	public String getIdentificadorErro(String idTabelaMobile, String idTabelaWebDuplicada, String idTabelaWeb,
			String idCrud, String idCrudMobile) {
		String identificadorErro = ". IdSincronizacao: " + idSincronizacao + ". IdSincronizacaoMobile : "
				+ idSincronizacaoMobile + ". Id Mobile: " + (idTabelaMobile == null ? "-" : idTabelaMobile)
				+ ". Id Duplicada Web: " + (idTabelaWebDuplicada == null ? "-" : idTabelaWebDuplicada) + ". Id Web: "
				+ (idTabelaWeb == null ? "-" : idTabelaWeb) + ". Id Crud: " + (idCrud == null ? "-" : idCrud)
				+ ". Id Crud Mobile: " + idCrudMobile;
		return identificadorErro;
	}

	// Processa as operacoes 'remove' do arquivo crud_mobile
	public Mensagem processaCrudEditMobile(JSONObject pJson) throws Exception {

		// Processa a ediuuo de um identificador uma unica vez, comeuando do
		// ultimo arquivo de sincronizacao
		// atu o primeiro
		// crud_id_INT != null // Operacao foi realizada com sucesso
		// CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI_CASCADE - o
		// registro deve ser removido
		// Database::CHAVE_DUPLICADA => Atualiza o identificador local e os
		// dados locais, su u executado depois
		// do parseamento dos arquivos de sincronizacao
		// Database::ERRO_EXECUCAO => Resetar o banco local
		// Erro nao identificado - atualiza os dados com os valores da web -
		// esse passo so sera realizado quando
		// os arquivos da sincronizacao forem executados.
		try {
			String nomeTabela = pJson.getString("tabela");
			JSONArray jsonCampos = pJson.getJSONArray("campos");
			String[] campos = new String[jsonCampos.length()];
			for (int i = 0; i < campos.length; i++) {
				campos[i] = jsonCampos.getString(i);
			}
			JSONArray jsonProtocolos = pJson.getJSONArray("protocolos");

			for (int i = 0; i < jsonProtocolos.length(); i++) {

				JSONObject jsonProtocolo = jsonProtocolos.getJSONObject(i);
				String idTabelaWeb = jsonProtocolo.getString("idTabelaWeb");

				Long erroSql = HelperLong.parserLong(jsonProtocolo.getString("erroSql"));
				String idTabelaMobile = jsonProtocolo.getString("idTabelaMobile");
				String idCrud = jsonProtocolo.getString("idCrud");
				String idCrudMobile = jsonProtocolo.getString("idCrudMobile");
				String idTabelaWebDuplicada = jsonProtocolo.getString("idTabelaWebDuplicada");

				// JSONArray jsonValores =
				// jsonProtocolo.getJSONArray("valores");
				// if(jsonValores != null){
				// String[] valores = new String[jsonValores.length()];
				// for (int j = 0; j < valores.length; j++) {
				// valores[j] = jsonValores.getString(j);
				// }
				// if (valores.length > 0 && campos.length != valores.length) {
				// return new Mensagem(
				// PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
				// "O numero temos do vetor 'campos' passado no crud_mobile deve
				// ser o mesmo dos valores."
				// + nomeTabela
				// + getIdentificadorErro(idTabelaMobile,
				// idTabelaWebDuplicada, idTabelaWeb,
				// idCrud, idCrudMobile));
				// }
				// }

				if (erroSql != null) {
					// se o registro nao existe mais localmente
					if (!Table.existeId(this.db, nomeTabela, idTabelaWeb)) {

						continue;
					}

					// CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI_CASCADE
					// - o registro deve ser removido
					// Database::CHAVE_DUPLICADA => Atualiza o identificador
					// local e os dados locais, su u executado depois
					// do parseamento dos arquivos de sincronizacao
					// Database::ERRO_EXECUCAO => Resetar o banco local
					if (erroSql == Database.ERRO_SQL.CHAVE_DUPLICADA.getId() && idTabelaWebDuplicada != null
							&& idTabelaWebDuplicada.length() > 0) {

						JSONArray jsonValoresDuplicada = jsonProtocolo.getJSONArray("valores_duplicada");
						String[] valoresDuplicada = new String[jsonValoresDuplicada.length()];
						for (int j = 0; j < valoresDuplicada.length; j++) {
							valoresDuplicada[j] = jsonValoresDuplicada.getString(j);
						}
						if (valoresDuplicada.length > 0 && campos.length != valoresDuplicada.length) {
							return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
									"O numero temos do vetor 'campos' passado no crud_mobile deve ser o mesmo dos valores_duplicada."
											+ nomeTabela + getIdentificadorErro(idTabelaMobile, idTabelaWebDuplicada,
													idTabelaWeb, idCrud, idCrudMobile));
						}
						// Somente o identificador seru atualizado nesse
						// momento,
						// o restante dos dados seru atualizado naturalmente
						// quando as
						// operauues do arquivo de sincronizacao comeuarem a
						// serem executadas
						if (valoresDuplicada.length > 0) {

							EXTDAOGenerico obj = (EXTDAOGenerico) this.db.factoryTable(nomeTabela);

							for (int k = 0; k < campos.length; k++) {
								if (campos.equals(Table.ID_UNIVERSAL))
									continue;
								obj.setAttrValue(campos[k], valoresDuplicada[k]);
							}
							obj.setId(idTabelaWeb);
							obj.formatToSQLite();
							obj.update(false);
							//
							// if
							// (!idTabelaWebDuplicada.equalsIgnoreCase(idTabelaWeb))
							// {
							//
							// String idSistemaTabela =
							// EXTDAOSistemaTabela.getIdSistemaTabela(this.db,
							// nomeTabela);
							//
							//
							//
							// // verifica se a duplicata ja nao foi inserida
							// // no banco local
							// //
							// if (Table.existeId(db, nomeTabela,
							// idTabelaWebDuplicada)) {
							// // Pego todos os dependentes atuais do
							// // registro antigo e atualizo seus
							// // identificadores
							// // para o valor da duplicata
							// Long idSincDaDuplicadaLocal = objSRSAW.getId(
							// idTabelaWebDuplicada.toString(),
							// idSistemaTabela,
							// EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT);
							// //se o registro da duplicada u local
							// if(idSincDaDuplicadaLocal > 0){
							// boolean validade = true;
							//
							// // Atualiza o identificador local para o da
							// // duplicada
							// validade =
							// objSRSAW.updateIdSynchronized(nomeTabela,
							// idTabelaWeb, idTabelaWebDuplicada.toString(),
							// log);
							// if (!validade) {
							// return new Mensagem(
							// PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
							// "Falha ao atualizar o identificador duplicado da
							// tabela. Tabela: "
							// + nomeTabela
							// + getIdentificadorErro(
							// idTabelaMobile,
							// idTabelaWebDuplicada,
							// idTabelaWeb,
							// idCrud,
							// idCrudMobile));
							// } else
							// continue; //operacao realizada com sucesso
							// } else {
							// //a duplicada web ocasionada ja existe na base
							// local, ou seja, ja foi sincronizada
							//
							// }
							//
							// }
							// }
						} else {
							return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
									"O id da chave duplicada estu vazio e deveria estar preenchido. Tabela: "
											+ nomeTabela + getIdentificadorErro(idTabelaMobile, idTabelaWebDuplicada,
													idTabelaWeb, idCrud, idCrudMobile));
						}
					} else if (erroSql == Database.ERRO_SQL.CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI_CASCADE
							.getId()) {
						EXTDAOGenerico obj = (EXTDAOGenerico) this.db.factoryTable(nomeTabela);
						obj.setAttrValue(Table.ID_UNIVERSAL, idTabelaWeb);
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("processaCrudEditMobile [lrke3ntn] remove: " + idTabelaWeb);
						obj.remove(true);
					} else {
						return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
								"Erro nuo identificado durante o processamento do crud_mobile na web." + nomeTabela
										+ getIdentificadorErro(idTabelaMobile, idTabelaWebDuplicada, idTabelaWeb,
												idCrud, idCrudMobile));
					}

				} else {
					// A atualizacao foi bem sucedida
					continue;
				}

			}
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
		} catch (JSONException e) {
			return new Mensagem(e);
		}

	}

	// {"protocolos":[{"idTabelaWebCrud":null,"idTabelaWeb":null,"idSincronizadorMobile":"2","erroSql":"1062",
	// "idTabelaMobile":"10999","valores":["10999","HJ","HJ","1"],"idSistemaTabela":"10","idCrud":null,
	// "idCrudMobile":"19","idTabelaWebDuplicada":"10999"}],"tabela":"profissao","tipoOperacao":"2","campos":["id","nome","nome_normalizado","corporacao_id_INT"]}
	public Mensagem processaCrudInsertMobile(JSONObject pJson) throws Exception {
		// Processa a inseruuo de um identificador uma unica vez, comeuando do
		// ultimo arquivo de sincronizacao
		// atu o primeiro
		// crud_id_INT != null // Operacao foi realizada com sucesso
		String nomeTabela = pJson.getString("tabela");
		JSONArray jsonCampos = pJson.getJSONArray("campos");
		String[] campos = new String[jsonCampos.length()];
		for (int i = 0; i < campos.length; i++) {
			campos[i] = jsonCampos.getString(i);
		}
		JSONArray jsonProtocolos = pJson.getJSONArray("protocolos");

		for (int i = 0; i < jsonProtocolos.length(); i++) {
			JSONObject jsonProtocolo = jsonProtocolos.getJSONObject(i);
			String idTabelaWeb = jsonProtocolo.getString("idTabelaWeb");
			// String idSincronizadorMobile =
			// jsonProtocolo.getString("idSincronizadorMobile");
			Long erroSql = HelperLong.parserLong(jsonProtocolo.getString("erroSql"));
			String idTabelaMobile = jsonProtocolo.getString("idTabelaMobile");
			String idCrud = jsonProtocolo.getString("idCrud");
			String idCrudMobile = jsonProtocolo.getString("idCrudMobile");
			String idTabelaWebDuplicada = jsonProtocolo.getString("idTabelaWebDuplicada");

			JSONArray jsonValores = jsonProtocolo.getJSONArray("valores");
			String[] valores = new String[jsonValores.length()];
			for (int j = 0; j < valores.length; j++) {
				valores[j] = jsonValores.getString(j);
			}
			if (campos.length != valores.length) {
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
						"O numero temos do vetor 'campos' passado no crud_mobile deve ser o mesmo dos valores."
								+ nomeTabela + ". Id Mobile: " + idTabelaMobile);
			}
			String idSincronizadorMobile = jsonProtocolo.getString("idSincronizadorMobile");
			String idTabela = idTabelaMobile;

			if (objSRSAW.select(idSincronizadorMobile)) {
				// talvez o id tenha mudado
				idTabela = objSRSAW
						.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT);
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO && !idTabela.equals(idTabelaMobile))
					log.escreveLog("Id tabela local atualizado u : " + idTabela);
			} else {
				throw new Exception(
						"O id sincronizador mobile deve estar preenchido para todos os registros do crud mobile");
			}

			if (erroSql != null) {

				if (erroSql == Database.ERRO_SQL.CHAVE_DUPLICADA.getId()) {
					if (idTabelaWebDuplicada != null && idTabelaWebDuplicada.length() > 0) {

						boolean validade = true;
						if (!idTabelaWebDuplicada.equalsIgnoreCase(idTabela)) {
							EXTDAOGenerico obj = new EXTDAOGenerico(this.db, nomeTabela);
							if (!obj.existeId(idTabelaWebDuplicada)) {
								// Atualiza o identificador local para o da
								// duplicada
								validade = objSRSAW.updateIdSynchronized(nomeTabela, null, idTabela,
										idTabelaWebDuplicada.toString(), log, true);
								if (!validade) {
									if (!Table.existeId(db, nomeTabela, idTabela)) {
										return new Mensagem(TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
												"Checando novamente, reparamos que o registro " + idTabela
														+ " foi removido pelo usuurio "
														+ "do mobile durante o a atualziacao de id. Logo desconsideramos a falha de atualizacao "
														+ "de id e continuaremos com o processo. Crud 'finalizado' com sucesso. 65487");
									}
									return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
											"Falha ao atualizar o identificador duplicado da tabela. Tabela: "
													+ nomeTabela + getIdentificadorErro(idTabelaMobile,
															idTabelaWebDuplicada, idTabelaWeb, idCrud, idCrudMobile));
								} else {
									objSRSAW.updateIdTabelaWebNaOperacaoDeInsert(nomeTabela, idTabela,
											idTabelaWebDuplicada);
									return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
								}

							} else {
								String idSistemaTabela = EXTDAOSistemaTabela.getIdSistemaTabela(db, nomeTabela);
								// Long idSincLocalDaDuplicada =
								// objSRSAW.getId(idTabela, idSistemaTabela,
								// EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT);

								if (objSRSAW.isRegistroLocal(idTabelaWebDuplicada, idSistemaTabela,
										// null,//idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
										// false,
										log)) {

									// if(idSincLocalDaDuplicada > 0){ //o valor
									// atualmente ocupado pela duplicada u
									// local?
									// Atualiza o identificador local para o da
									// duplicada
									validade = objSRSAW.updateIdSynchronized(nomeTabela, null, idTabela,
											idTabelaWebDuplicada.toString(), log, true);
									if (!validade) {

										return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
												"Falha ao atualizar o identificador duplicado da tabela. Tabela: "
														+ nomeTabela
														+ getIdentificadorErro(idTabelaMobile, idTabelaWebDuplicada,
																idTabelaWeb, idCrud, idCrudMobile));
									} else {
										objSRSAW.updateIdTabelaWebNaOperacaoDeInsert(nomeTabela, idTabela,
												idTabelaWebDuplicada);
										return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
									}
								} else {
									// db.getListTableDependent(nomeTabela);
									// se ja for o registro final, entao deleta
									// o atual registro local, lembrando de
									// atualizar os registros que apontam para o
									// mesmo, para apontar para a duplicada
									if (obj.updateListIdInListTableDependent(new String[] { idTabela },
											new String[] { idTabelaWebDuplicada })) {
										if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
											log.escreveLog("processaCrudInsertMobile - remove [l99fdkvefv] - "
													+ nomeTabela + "::" + idTabela);
										obj.remove(idTabela, false);
										return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
									} else {
										throw new Exception(
												"Falha durante a atualizacao dos ids dos dependentes ... asdrqwer");
									}

								}
							}

						} else
							return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
						// Os dados da duplicata da web seruo atualizados no
						// registro local,
						// durante o parseamento dos arquivos de sincronizacao

					} else {
						return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
								"O id da chave duplicada estu vazio e deveria estar preenchido. Tabela: " + nomeTabela
										+ getIdentificadorErro(idTabelaMobile, idTabelaWebDuplicada, idTabelaWeb,
												idCrud, idCrudMobile));
					}
				} else if (erroSql == Database.ERRO_SQL.CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI_CASCADE
						.getId()) {
					EXTDAOGenerico obj = new EXTDAOGenerico(this.db, nomeTabela);
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("processaCrudInsertMobile [qw342r] : " + idTabela);
					obj.setAttrValue(Table.ID_UNIVERSAL, idTabela);
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Remove registro [lgfeioerw] : " + idTabela);
					obj.remove(false);
					objSRSAW.updateIdTabelaWebNaOperacaoDeInsert(obj.getName(), idTabela, "-1");
				} else {
					return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
							"Erro nuo identificado durante o processamento do crud_mobile na web." + nomeTabela
									+ getIdentificadorErro(idTabelaMobile, idTabelaWebDuplicada, idTabelaWeb, idCrud,
											idCrudMobile));
				}

			} else if (idCrud != null && idCrud.length() > 0 && idTabelaWeb != null && idTabelaWeb.length() > 0) {

				boolean validade = objSRSAW.updateIdSynchronized(nomeTabela, null, idTabela, idTabelaWeb.toString(),
						log, true);
				if (!validade) {
					return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
							"Falha ao atualizar o identificador duplicado da tabela. Tabela: " + nomeTabela
									+ getIdentificadorErro(idTabelaMobile, idTabelaWebDuplicada, idTabelaWeb, idCrud,
											idCrudMobile));
				} else {
					objSRSAW.updateIdTabelaWebNaOperacaoDeInsert(nomeTabela, idTabela, idTabelaWeb);
				}
			} else {
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
						"Condiuuo nuo programada durante o processamento das operacoes de insert da sincronizacao_mobile."
								+ nomeTabela + getIdentificadorErro(idTabelaMobile, idTabelaWebDuplicada, idTabelaWeb,
										idCrud, idCrudMobile));
			}

		}
		return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);

	}

	public static Boolean validaArquivoSincronizacaoMobile(String path) {
		File f;
		FileReader fr = null;
		try {

			f = new File(path);
			fr = new FileReader(f);

			String startsWith = "{\"sincronizacao\":";
			char[] buf = new char[startsWith.length()];
			int totalLido = fr.read(buf);
			if (totalLido == -1)
				return null;

			int iStartsWith = 0;
			if (totalLido == startsWith.length()) {
				for (int i = 0; i < totalLido; i++) {

					if ((char) buf[i] != startsWith.charAt(iStartsWith++)) {
						return false;
					}
				}
				return true;
			}

			return null;
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);
			return false;
		} finally {
			if (fr != null)
				try {
					fr.close();
				} catch (IOException e) {

					SingletonLog.insereErro(e, SingletonLog.TIPO.SINCRONIZADOR);

				}
		}
	}

}
