package app.omegasoftware.pontoeletronico.database.synchronize;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class RetornoOperacaoAForca extends InterfaceMensagem{
	
	public Table registroDuplicado;
	public RetornoOperacaoAForca(PROTOCOLO_SISTEMA.TIPO protocoloSistema, String msg) {
		super(protocoloSistema, msg);
		
	}
	
	public RetornoOperacaoAForca(PROTOCOLO_SISTEMA.TIPO protocoloSistema, Table registroDuplicado){
		super(protocoloSistema, null);
		
		this.registroDuplicado = registroDuplicado;
	}

	@Override
	protected void preInicializa(Context context, JSONObject pJsonObject)
			throws JSONException {
		
		
	}

	@Override
	protected ContainerJson procedimentoToJson() throws JSONException {
		return null;
	}
}
