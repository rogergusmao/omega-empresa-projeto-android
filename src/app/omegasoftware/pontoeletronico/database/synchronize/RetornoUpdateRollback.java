package app.omegasoftware.pontoeletronico.database.synchronize;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class RetornoUpdateRollback extends InterfaceMensagem{
	public Table registroBackupeado;
	public boolean ultimoBackup;
	public RetornoUpdateRollback(PROTOCOLO_SISTEMA.TIPO protocoloSistema, String msg) {
		super(protocoloSistema, msg);
		
	}
		
	public RetornoUpdateRollback(
			Table registroBackupeado,
			boolean ultimoBackup){
		super(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, null);
		this.registroBackupeado = registroBackupeado;
		this.ultimoBackup = ultimoBackup;
	}

	@Override
	protected void preInicializa(Context context, JSONObject pJsonObject)
			throws JSONException {
		
		
	}

	@Override
	protected ContainerJson procedimentoToJson() throws JSONException {
		
		return null;
	}
}