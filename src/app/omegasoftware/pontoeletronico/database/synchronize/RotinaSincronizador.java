package app.omegasoftware.pontoeletronico.database.synchronize;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.ResetarActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.SynchronizeReceiveActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.widget.UltimoPontoWidget.UpdateService;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.ControladorServicoInterno;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity.STATE_LOAD_USER_HTTP_POST;
import app.omegasoftware.pontoeletronico.common.dialog.FactoryAlertDialog;
import app.omegasoftware.pontoeletronico.common.thread.HelperThread;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORegistroEstado;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTipoRegistro;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.Tuple;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCorporacao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOCorporacaoSincronizadorPhp;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoa;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador.STATE_SINCRONIZADOR;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorAndroidParaWeb;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_BOOLEAN;
import app.omegasoftware.pontoeletronico.pontoeletronico.PrincipalActivity;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.service.ControlerServicoInterno;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class RotinaSincronizador {

//	static STATE_SINCRONIZADOR state;
//	public static void updateState(STATE_SINCRONIZADOR state){
//		RotinaSincronizador.state =state;
//	}
	public static STATE_SINCRONIZADOR getStateSincronizador(Context c, String idCorporacao) {
//		if(state != null) return state;
		
		if (PontoEletronicoSharedPreference.getValor(c,
				TIPO_BOOLEAN.RESETAR_BANCO_DE_DADOS))
			return STATE_SINCRONIZADOR.RESETAR;
		else if (!DatabasePontoEletronico
				.isDatabaseCreated(c)) {
			return STATE_SINCRONIZADOR.BANCO_AINDA_NAO_CRIADO;
		}else if (!DatabasePontoEletronico
				.isDatabaseInitialized(c)) {
			return STATE_SINCRONIZADOR.NAO_REALIZADO;
		} else {
			if(idCorporacao == null)
				idCorporacao = OmegaSecurity.getIdCorporacao();
			Integer pIntIdCorporacao = HelperInteger.parserInteger(idCorporacao);
			
			Tuple<Integer, String> registro = SincronizadorEspelho.getCorporacaoSincronizada(c);
			if(registro == null) return STATE_SINCRONIZADOR.NAO_REALIZADO;
			else if(pIntIdCorporacao != null){
				if( (int)registro.x == (int)pIntIdCorporacao) 
					return STATE_SINCRONIZADOR.COMPLETO;
				else return STATE_SINCRONIZADOR.NAO_REALIZADO;
			} else 	return STATE_SINCRONIZADOR.COMPLETO;
		}
		
	}
	
	public static void criaDialogoParaResetarOBancoDeDadosSeNecessario(
			final Activity a) {
		if (OmegaConfiguration.DEBUG_RESETAR_BANCO_QUANDO_OCORRER_ERROR_DE_SINC && 
				PontoEletronicoSharedPreference.getValor(a,
				TIPO_BOOLEAN.RESETAR_BANCO_DE_DADOS)) {
			DialogInterface.OnClickListener dialogResetar = new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						Intent vIntent2 = new Intent(a,
								ResetarActivityMobile.class);
						vIntent2.putExtra(
								OmegaConfiguration.SEARCH_FIELD_MESSAGE,
								"Ocorreu um erro durante a sincronizacao dos dados. Por motivo de seguranua vocu deve resetar os seus dados.");
						a.startActivityForResult(
								vIntent2,
								OmegaConfiguration.STATE_FORM_CONFIRMACAO_RESETAR);

						break;

					case DialogInterface.BUTTON_NEGATIVE:

						break;
					}
				}
			};

			FactoryAlertDialog
					.showAlertDialog(
							a,
							"Ocorreu um erro durante a sincronizacao dos dados. Por motivo de seguranua vocu deve resetar os seus dados.",
							dialogResetar);
		}

	}

//	public static STATE_SINCRONIZADOR bkpGetStateSincronizador(Database db) {
//		EXTDAOSistemaCorporacaoSincronizador obj = new EXTDAOSistemaCorporacaoSincronizador(
//				db);
//		if (PontoEletronicoSharedPreference.getValor(db.getContext(),
//				TIPO_BOOLEAN.RESETAR_BANCO_DE_DADOS))
//			return STATE_SINCRONIZADOR.RESETAR;
//		else if (!DatabasePontoEletronico
//				.isDatabaseInitialized(db.getContext())) {
//			return STATE_SINCRONIZADOR.NAO_REALIZADO;
//		}
//		obj.clearData();
//		obj.setAttrValue(
//				EXTDAOSistemaCorporacaoSincronizador.CORPORACAO_ID_INT,
//				OmegaSecurity.getIdCorporacao());
//
//		String vDiaDate = null;
//		String vIdCorporacaoSincronizador = null;
//		EXTDAOSistemaCorporacaoSincronizador vTuplaCS = (EXTDAOSistemaCorporacaoSincronizador) obj
//				.getFirstTupla();
//		if (vTuplaCS != null) {
//
//			vIdCorporacaoSincronizador = vTuplaCS
//					.getStrValueOfAttribute(EXTDAOSistemaCorporacaoSincronizador.ID);
//			vDiaDate = vTuplaCS
//					.getStrValueOfAttribute(EXTDAOSistemaCorporacaoSincronizador.DIA_DATE);
//
//		} else {
//			return STATE_SINCRONIZADOR.NAO_REALIZADO;
//		}
//
//		ArrayList<String> vListTableOrder = obj.getDatabase()
//				.getListNameHierarquicalOrderOfVetorTable(
//						OmegaConfiguration
//								.LISTA_TABELA_SINCRONIZACAO_DOWNLOAD_UNICO(obj
//										.getDatabase()));
//
//		for (String strNomeTabela : vListTableOrder) {
//			
//			String vIdTabelaSincronizador = EXTDAOSistemaTabela.getIdSistemaTabela(obj.getDatabase(), strNomeTabela);
//			if (vIdTabelaSincronizador == null) {
//				// Log.d(TAG, "Tabela : " + strNomeTabela +
//				// ". Nao eh existente na sistema_tabela.");
//				return STATE_SINCRONIZADOR.NAO_REALIZADO;
//			}
//			// TODAS AS ATUALIZACOES TEM QUE TER SIDO REALIZADAS NA MESMA DATA
//			EXTDAOSistemaCorporacaoSincronizadorTabela vObjCSTS = new EXTDAOSistemaCorporacaoSincronizadorTabela(
//					obj.getDatabase());
//			vObjCSTS.setAttrValue(
//					EXTDAOSistemaCorporacaoSincronizadorTabela.SISTEMA_CORPORACAO_SINCRONIZADOR_ID_INT,
//					vIdCorporacaoSincronizador);
//			vObjCSTS.setAttrValue(
//					EXTDAOSistemaCorporacaoSincronizadorTabela.SISTEMA_TABELA_ID_INT,
//					vIdTabelaSincronizador);
//			EXTDAOSistemaCorporacaoSincronizadorTabela vTuplaCSTS = (EXTDAOSistemaCorporacaoSincronizadorTabela) vObjCSTS
//					.getFirstTupla();
//			if (vTuplaCSTS != null) {
//				String vStrDate = vTuplaCSTS
//						.getStrValueOfAttribute(EXTDAOSistemaCorporacaoSincronizadorTabela.DIA_DATE);
//				if (vStrDate.compareTo(vDiaDate) != 0)
//					return STATE_SINCRONIZADOR.INCOMPLETO;
//
//			} else {
//				return STATE_SINCRONIZADOR.INCOMPLETO;
//			}
//
//		}
//		return STATE_SINCRONIZADOR.COMPLETO;
//	}

	public static boolean procedimentoSincronizacaoNaoRealizada(
			Activity c,
			boolean salvarSenhaDoUsuarioLogado) {
		
		ControlerServicoInterno controladorServicoInterno = null;
		
		try {
			// Pausando todos os serviuos do OmegaEmpresa
			controladorServicoInterno = ControlerServicoInterno.constroi(c);
			controladorServicoInterno.stopAllServices();
			
			Thread.sleep(1000);

			// Baixa o banco de dados e cria o banco em comum com a web
			if (Database.procedimentoCriaBanco(c,
					DatabasePontoEletronico.DATABASE_NAME,
					DatabasePontoEletronico.DATABASE_PATH,
					DatabasePontoEletronico.DATABASE_VERSION)) {
				
				Database db2 = null;
				try{
					db2 = new DatabasePontoEletronico(c, true);
					db2.setPragmaOff();
					db2.createDatabaseStructure();					
				} finally{
					if(db2 != null)db2.close(true);
				}
				Database db3 = null;
				try{
					db3 = new DatabasePontoEletronico(c, true);
					db3.setPragmaOff();
					EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(db3);

					vObjUsuario.procedimentoApagaSenhaDeTodosUsuarios();
					
					EXTDAOCorporacao.insertCorporacaoSeInexistente(db3);
					//Garante que a versao atual terá os estados necessários para execução
					EXTDAOTipoRegistro.inserirRegistrosSeNecessario(db3);

					EXTDAORegistroEstado.inserirRegistrosSeNecessario(db3);
					
					if (OmegaSecurity.loadUserHttpPost(
							OmegaSecurity.getEmail(),
							OmegaSecurity.getSenha(),
							OmegaSecurity.getCorporacao(), 
							c, 
							true) == STATE_LOAD_USER_HTTP_POST.ERROR)
						return false;
					
					String[] retorno = SynchronizeReceiveActivityMobile
							.updateDataDaSincronizacaoDaCorporacaoSincronizador(
									c,
									db3);
					if (retorno == null)
						return false;

					if (!SynchronizeReceiveActivityMobile
									.updateDataDaSincronizacaoDaCorporacaoSincronizador(
											c, db3, retorno[0], retorno[1])) {
						return false;
					} 				
					
				} finally{
					if(db3 != null)db2.close(true);
				}
				
				Database db4 = null;
				try{
					
					db4 = new DatabasePontoEletronico(c, true);
					db4.criarTodasAsChaves();
	
				} finally{
					if(db4 != null) db4.close(true);
				}
				
				Database db5 = null;
				try{
					
					db5 = new DatabasePontoEletronico(c, true);
					db5.getOpenHelper();
					
					
				} finally{
					if(db5 != null) db5.close(true);
				}
				
				EXTDAOSistemaTabela.initCache(c);
				controladorServicoInterno.startAllServicesAutenticados();
				
				SincronizadorEspelho.setCorporacaoSincronizada(c, HelperInteger.parserInteger( OmegaSecurity.getIdCorporacao()), OmegaSecurity.getCorporacao());
				
				Database db6 = null;
				try{
					db6 = new DatabasePontoEletronico(c,true);
					
					EXTDAOUsuario.removerUsuariosQueNaoSaoDaCorporacao(db6);
					
					EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(db6);
					vObjUsuario.procedimentoAtualizaSenhaDoUsuarioLogado();
					
					vObjUsuario.select(OmegaSecurity.getIdUsuario());
					EXTDAOPessoa objPessoa = vObjUsuario.getObjPessoa();
					if(objPessoa != null){
						;
						objPessoa.setAttrValue(EXTDAOUsuario.NOME, OmegaSecurity.getEmail());
						objPessoa.setAttrValue(EXTDAOUsuario.EMAIL, OmegaSecurity.getEmail());
						objPessoa.formatToSQLite();
						objPessoa.update(false);
					}
					
				}finally{
					if(db6 != null) db6.close(true);
				}
				
				return true;
			} else {
				RotinaSincronizador.resetar(c, true);
				return false;
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			RotinaSincronizador.resetar(c, true);
			return false;
		}
	}

	public static void actionLogout(Context pContext, boolean sairParaTelLogin) {
		logout(pContext);
		
		Intent intent = new Intent(pContext, PrincipalActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		if(sairParaTelLogin){
			OmegaSecurity.apagarLoginAutomatico(pContext);
		}
		intent.putExtra(OmegaConfiguration.PARAM_EXIT, !sairParaTelLogin);
		pContext.startActivity(intent);
	}


	public static void logout(Context a) {
		
		
		OmegaSecurity.logout();
		
		ControlerServicoInterno controleSI = ControlerServicoInterno.getSingleton();
		if (controleSI != null)
			controleSI.stopOnlyServicosAutenticados();
	
		a.startService(new Intent(a, UpdateService.class));
		
		Intent broadcastIntent = new Intent();
		broadcastIntent.setAction("com.package.ACTION_LOGOUT");
		a.sendBroadcast(broadcastIntent);
	}
	
	
	public static boolean deletarBancoDeDados(Context context,
			String pIdCorporacao, boolean resetarAForca) {
		Database db = null;

		try {
			db = new DatabasePontoEletronico(context);
			SincronizadorEspelho.resetarEstado(context);
			db.deletaBanco();
			return true;

		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			return false;
		}

	}

	public static Mensagem resetar(Context context, boolean resetarAForca) {
		Database db = null;
		File vFileDatabase = null;

		try {
			db = new DatabasePontoEletronico(context);
			vFileDatabase = context.getApplicationContext().getDatabasePath(
					db.getName());
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		} finally {
			if (db != null)
				db.close();
		}

		try{
			SingletonControladorChaveUnica singleton = SingletonControladorChaveUnica.getSingleton();
			if(singleton != null) singleton.emptyCache();
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}
		
		try {
			boolean upload = false;
//			if (OmegaConfiguration.DEBUGGING) {
//				// Envia o banco para analise
//				OmegaFileConfiguration vConfiguration = new OmegaFileConfiguration();
//				File zipFile = FilesZipper.getNewZipFile(vConfiguration
//						.getPathFilesTemp());
//				File vVetorFileToUpload[] = new File[1];
//				vVetorFileToUpload[0] = vFileDatabase;
//				HelperZip.zipVetorFile(vVetorFileToUpload, zipFile);
//				FileHttpPost vFileHttpPost = null;
////				FileHttpPost vFileHttpPost = new FileHttpPost(
////						OmegaConfiguration.URL_REQUEST_SEND_ZIP_FILE_PHOTO(),
////						null);
//				upload = vFileHttpPost.performUpload(
//						zipFile, 
//						true,
//						EXTDAOSistemaTipoDownloadArquivo.TIPO.BANCO);
//
//				zipFile.delete();
//			} else
				upload = true;

			if (upload || resetarAForca) {
				SincronizadorEspelho.resetarEstado(context);
				db.deletaBanco();
				db.createDatabaseStructure();
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, 
						context.getString(R.string.resetar_ok));
			} else
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_PROCESSO_RESETAR, 
						context.getString(R.string.resetar_erro));
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_PROCESSO_RESETAR, 
					context.getString(R.string.resetar_erro));
		}

	}

	public static void limparDadosCorporacao(Context pActivity,
			String pIdCorporacao) {

		Database db = null;
		try{
			db = new DatabasePontoEletronico(pActivity);
			EXTDAOCorporacaoSincronizadorPhp vObjCorporacaoSincronizadorPHP = new EXTDAOCorporacaoSincronizadorPhp(
					db);
			vObjCorporacaoSincronizadorPHP.setAttrValue(
					EXTDAOCorporacaoSincronizadorPhp.CORPORACAO_ID_INT,
					pIdCorporacao);
			ArrayList<Table> vListTupla = vObjCorporacaoSincronizadorPHP
					.getListTable();
			if (vListTupla != null) {
				if (vListTupla.size() > 0) {
					for (Table vTupla : vListTupla) {
						try{
							vTupla.remove(false);
						} catch(Exception ex){
							SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
						}
					}
				}
			}

			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb vObjSincronizador = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(
					db);
			vObjSincronizador
					.setAttrValue(
							EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT,
							pIdCorporacao);
			ArrayList<Table> vListTuplaSincronizador = vObjSincronizador
					.getListTable();
			if (vListTuplaSincronizador != null) {
				if (vListTuplaSincronizador.size() > 0) {
					for (Table vTupla : vListTuplaSincronizador) {
						try{
							vTupla.remove(false);
						} catch(Exception ex){
							SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
						}
					}
				}
			}
			EXTDAOCorporacao vObjCorporacao = new EXTDAOCorporacao(db);
			vObjCorporacao.setAttrValue(EXTDAOCorporacao.ID, pIdCorporacao);
			try{
				vObjCorporacao.remove(false);
			} catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			}
		} catch (OmegaDatabaseException e) {
			SingletonLog.factoryToast(pActivity, e, SingletonLog.TIPO.SINCRONIZADOR);
		}finally{
			if(db != null) db.close();
		}
		
	}

//	public static boolean inicializaBancoPeloServidor(Context pActivity,
//			boolean pSalvarSenha) throws Exception {
//		boolean vValidade = false;
//
//		if (Database.procedimentoCriaBanco(pActivity,
//				DatabasePontoEletronico.DATABASE_NAME,
//				DatabasePontoEletronico.DATABASE_PATH,
//				DatabasePontoEletronico.DATABASE_VERSION)) {
//
//			Database db2 = new DatabasePontoEletronico(pActivity);
//			db2.createDatabaseStructure();
//			db2.close();
//			Database db3 = new DatabasePontoEletronico(pActivity);
//
//			EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(db3);
//			vObjUsuario.procedimentoApagaSenhaDeTodosUsuarios();
//			if (pSalvarSenha)
//				vObjUsuario.procedimentoAtualizaSenhaDoUsuarioLogado();
//
//			EXTDAOCorporacao.insertCorporacaoSeInexistente(db3);
//			
//			String[] retorno = SynchronizeReceiveActivityMobile
//					.updateDataDaSincronizacaoDaCorporacaoSincronizador(
//							pActivity, db3);
//			if (retorno == null)
//				vValidade = false;
//
//			if (vValidade
//					&& SynchronizeReceiveActivityMobile
//							.updateDataDaSincronizacaoDaCorporacaoSincronizador(
//									pActivity, db3, retorno[0], retorno[1]))
//				vValidade = true;
//			else
//				vValidade = false;
//
//			// Database db5 = new DatabasePontoEletronico(this);
//			// db5.verificaEstruturaWeb();
//
//			db3.close();
//
//		}
//		return vValidade;
//	}
	
	public static InterfaceMensagem procedimentoResetar(Activity context, boolean resetarAForca){
		ControlerServicoInterno controladorServicoInterno = null;
		ControladorServicoInterno controladorSI = null;
		InterfaceMensagem msg =null;
		try{
			if(!Database.isDatabaseInitialized(context) ){
				msg= new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, 
						context.getString(R.string.sem_corporacao_carregada));
				return msg;
			} else {
				//Pausando todos os serviuos do OmegaEmpresa
				controladorServicoInterno = ControlerServicoInterno.constroi(context);	
				controladorServicoInterno.stopAllServices();
				//Pausando todos os serviuos da Biblioteca Nuvem
				controladorSI = ControladorServicoInterno.constroi(context);
				controladorSI.stopAllServices();	
				
				Thread.sleep(1000);
				
				SincronizadorEspelho syncEspelho ;	
				syncEspelho = SincronizadorEspelho.getInstance(context);
				msg =syncEspelho.resetar();
				if((msg != null && msg.ok() || msg.resultadoVazio()) || resetarAForca)
					msg = RotinaSincronizador.resetar(
							context,   
							true);
				return msg;
			}
			
			
		}
		catch(Exception e)
		{	
			SingletonLog.insereErro(e, TIPO.SINCRONIZADOR);
			if(resetarAForca){
				return RotinaSincronizador.resetar(
						context,   
						true);
			}
			msg = new Mensagem(e);
			return msg;
		} finally{
			HelperThread.sleep(1000);
			if((msg != null && msg.ok()) || resetarAForca){
				
				if(controladorServicoInterno != null)
					controladorServicoInterno.startAllServicesNaoAutenticados();
				
			} else {
				if(OmegaSecurity.isAutenticacaoRealizada()){
					if(controladorServicoInterno != null)
						controladorServicoInterno.startAllServicesAutenticados();	
				} else {
					if(controladorServicoInterno != null)
						controladorServicoInterno.startAllServicesNaoAutenticados();
				}
				
				if(controladorSI != null)
					controladorSI.startAllServices();
				
			
			}	
		}
	}
}
