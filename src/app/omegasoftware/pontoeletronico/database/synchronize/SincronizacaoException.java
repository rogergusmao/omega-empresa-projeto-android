package app.omegasoftware.pontoeletronico.database.synchronize;


public class SincronizacaoException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SincronizacaoException(String message) {
        super(message);
    }
}