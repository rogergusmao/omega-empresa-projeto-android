package app.omegasoftware.pontoeletronico.database.synchronize;

import android.content.Context;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Database.ERRO_SQL;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaAtributo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorAndroidParaWeb;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoOperacaoBanco;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA.TIPO;

public class SincronizacaoMobileTabela {
	EXTDAOSistemaRegistroSincronizadorAndroidParaWeb objRegistroAndroidWeb;
	String tabela;
	String idSistemaTabela;
	OmegaLog log;
	Context context;
	Database db=null;

	Table obj = null;
	EXTDAOSistemaTabela objSistemaTabela = null;
	Long idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb = null; 
	public SincronizacaoMobileTabela(Context c, Database db, String tabela, OmegaLog log,Long idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb ) throws OmegaDatabaseException {
		this.db = db;
		this.tabela = tabela;
		this.objRegistroAndroidWeb = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(db);
		this.objSistemaTabela = new EXTDAOSistemaTabela(db);
		this.obj = db.factoryTable(tabela);
		this.context = c;
		this.idSistemaTabela = EXTDAOSistemaTabela.getIdSistemaTabela(
				c,
				tabela);
		this.log = log;
		this.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb=idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb;
	}

	public InterfaceMensagem processaCrudInserir(
			String[] campos,
			CrudInserirEditar crud,
			OmegaLog log) throws Exception {
		obj.clearData();
		// Se a operauuo de inseruuo ocorreu como esperado entuo o identificador
		// local u atualizado
//		log.escreveLog("processaCrudInserir: " + crud.getIdentificador());
		String idTabelaMobileAtualizado = crud.idTabelaMobile != null ? String.valueOf(crud.idTabelaMobile) : null;
		String idTabelaMobileOriginal =null; 
		if(crud.idTabelaMobile == null){
			return new Mensagem(
				PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, 
				"Condicional nao programada awe5q3w4r5t. Crud: " + crud.getIdentificador());
		}else if(!objRegistroAndroidWeb.select(crud.idSincronizadorMobile.toString())){
			return new Mensagem(
					PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, 
					"[lkmfesrter]O banco local deveria conter o registro do crud que estu sendo sincronizado: " + crud.getIdentificador());
		} else {
			Long bkpIdTabelaMobile = crud.idTabelaMobile;
			
			idTabelaMobileAtualizado = objRegistroAndroidWeb.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT);
			idTabelaMobileOriginal = objRegistroAndroidWeb.getStrValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_MOBILE_INT);
			//crud.idTabelaMobile = objRegistroAndroidWeb.getIntegerValueOfAttribute(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT);
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("idTabelaMobile atualizado devido a operauues internas da propria sincronizacao que o deslocaram que estava quando foi gerado o arquivo de sincronizacao mobile para web. Logo o seu id que antes era : "
						+ bkpIdTabelaMobile + " para " + idTabelaMobileAtualizado);
			
			
		} 
		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("Verificando se a tupla (" + idTabelaMobileAtualizado+", "+idTabelaMobileOriginal.toString() + ") foi removida durante a sincornizacao");
		
		
		//TODO metodologia de verificacao antiga se o registo havia sido removido localmene
//		boolean registroFoiRemovidoDuranteASincronizacao= false;
//		if(idTabelaMobileOriginal != null){
//			log.escreveLog("Id tabela mobile original: " + idTabelaMobileOriginal);
//			registroFoiRemovidoDuranteASincronizacao= objRegistroAndroidWeb.isRegistroRemovidoLocalmenteDeAcordoComIdTabelaMobileOriginal(
//					idTabelaMobileAtualizado, idTabelaMobileOriginal, idSistemaTabela, log);
//			if(registroFoiRemovidoDuranteASincronizacao)
//				log.escreveLog("[tyuiykj]O registro (crud "+crud.idCrud+", crud mobile "+ crud.idCrudMobile+ ") foi removido localmente durante a sincronizacao");
//			else if(!idTabelaMobileAtualizado.equals(idTabelaMobileOriginal)){
//				log.escreveLog("Verificacao 2");
//				registroFoiRemovidoDuranteASincronizacao= objRegistroAndroidWeb.isRegistroRemovidoLocalmenteDeAcordoComIdTabelaMobileOriginal(
//						idTabelaMobileAtualizado, idTabelaMobileAtualizado, idSistemaTabela, log);
//				if(registroFoiRemovidoDuranteASincronizacao)
//					log.escreveLog("[wserferhg]O registro do (crud "+crud.idCrud+", crud mobile "+ crud.idCrudMobile+ ") foi removido localmente durante a sincronizacao");
//			}
//		}
		String strIdTabelaMobile = crud.idTabelaMobile != null ? crud.idTabelaMobile.toString() : null;
		// Caso positivo remove
		if (
				//registroFoiRemovidoDuranteASincronizacao || 
			!Table.existeId(db, obj.getName(), idTabelaMobileAtualizado)) {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("O registro foi removido durante a sincronizacao, logo o crud "+crud.idCrud+" nao sera executado. Atualiza os identificadores dos cruds que ainda nao foram sincronizados");
			String idTabelaWeb = null;
			boolean idProvenienteDaWebDuplicada = false;
			if(crud.idTabelaWeb != null)
				idTabelaWeb = crud.idTabelaWeb.toString();
			else if(crud.idTabelaWebCrud != null)
				idTabelaWeb = crud.idTabelaWebCrud.toString();
			else if(crud.idTabelaWebDuplicada != null){
				idTabelaWeb = crud.idTabelaWebDuplicada.toString();
				idProvenienteDaWebDuplicada =  true;
			}
				
			if(idTabelaWeb != null){

				if(Table.existeId(db, obj.getName(), idTabelaWeb) ){
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
						log.escreveLog("Liberando o registro " + obj.getName() + "["+idTabelaWeb+"] do banco local, atualmente ocupado...");
					}
					objRegistroAndroidWeb.updateIdSynchronized(obj.getName(), null, idTabelaWeb, null, log, true);
				}
				if(!idProvenienteDaWebDuplicada){
					
					objRegistroAndroidWeb.atualizaIdASerSincronizadoSeExistente(obj.getName(), strIdTabelaMobile, idTabelaMobileAtualizado,  idTabelaWeb, log);
				}
				else {
					
					int totRemovido = objRegistroAndroidWeb.removerTodosAsOperacoesDoRegistro(idTabelaMobileAtualizado, idTabelaMobileOriginal, idSistemaTabela);
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Como o registro sofreu duplicada lu em cima, e aqui foi removido antes de finalizar a sincronizacao. Nesse caso removemos todas as operacoes que seriam sincronizadas do registro atual, pois o mesmo nem chegou a ser executado em produuuo. Total removido: " + totRemovido);
				}
//				TODO [21ED54GHR65GH1] antes
//				if(Table.existeId(db, obj.getName(), idTabelaWeb) ){
//					objRegistroAndroidWeb.updateIdSynchronized(obj.getName(), null, idTabelaWeb, null, log, true);
//					
//				}
			} else {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("O registro " + obj.getName() + "["+idTabelaWeb+"] estu desocupado!");
			}
			
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
		} 
		
		if (crud.idTabelaWebCrud != null && idTabelaMobileAtualizado != null && crud.erroSql == null) {
			
			
			log.escreveLog("idTabelaMobile: " + idTabelaMobileAtualizado);
			if (!objRegistroAndroidWeb.updateIdSynchronized(
					obj.getName(), 
					strIdTabelaMobile,
					idTabelaMobileAtualizado, 
					crud.idTabelaWebCrud.toString(), 
					log, 
					true)) {
				if(!Table.existeId(db, tabela, idTabelaMobileAtualizado)){
					
					this.log.escreveLog("Checando novamente, reparamos que o registro "+idTabelaMobileAtualizado+" foi removido pelo usuurio do mobile durante o a atualziacao de id. Logo desconsideramos a falha de atualizacao de id e continuaremos com o processo. Crud 'finalizado' com sucesso. "
									+ crud.getIdentificador());
					objRegistroAndroidWeb.atualizaIdASerSincronizadoSeExistente(obj.getName(), crud.idTabelaMobile.toString(), idTabelaMobileAtualizado, crud.idTabelaWeb.toString(), log);
//					objRegistroAndroidWeb.removerTodosAsOperacoesDoRegistro(crud.idTabelaMobile.toString(), crud.idSistemaTabela.toString());
					return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
//					return new Mensagem(TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
				} else {
					return new Mensagem(TIPO.ERRO_COM_SERVIDOR,
							"Erro durante a atualizacao do identificador do banco local 1."
									+ crud.getIdentificador());	
				}
			} else {
				obj.setId(crud.idTabelaWebCrud.toString());
				String strIdTabelaWebCrud= crud.idTabelaWebCrud.toString();
				if(campos != null && crud.valores != null && campos.length == crud.valores.length 
						&& obj.select(strIdTabelaWebCrud)
						&& !obj.comparaValores(campos, crud.valores)
						&& objRegistroAndroidWeb.getId(
								strIdTabelaWebCrud, 
								idSistemaTabela, 
								EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_EDIT, 
								false, 
								log) == null){
					log.escreveLog("Atualizando dados do registro com os dados vindos da web");
					
					InterfaceMensagem msg = objRegistroAndroidWeb
							.preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks(
									campos, 
									crud.valores, 
									obj,
									idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
									log);
					if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_REMOVIDO_DEVIDO_ON_DELETE_CASCADE
							.getId()) {
						this.log.escreveLog("Removido onCascade[jrlkjndsf] - " + obj.getName() + "::"+ crud.idTabelaWeb);
						obj.remove(crud.idTabelaWeb.toString(), true);
						return new Mensagem(
								PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);	
					} else {
						this.log.escreveLog("updateAForca [elrkmnklge]");
						
						// atualiza os valores
						
						
						return objSistemaTabela.updateAForca(db, obj, objRegistroAndroidWeb,crud.idCrud.toString(), idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb , log);
					}
				}
				
				this.log.escreveLog("Id atualizado com sucesso");
				// Operacao realizada com sucesso
				return new Mensagem(
						PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
			}
		

		} else {
			
			
			if(crud.idTabelaWebDuplicada != null){
				if (crud.valoresDuplicada != null && crud.valoresDuplicada.length > 0) {
					log.escreveLog("idTabelaWebDuplicada: " + crud.idTabelaWebDuplicada);
					// verifica se o registro existe na base local
					if ( obj.select(crud.idTabelaWebDuplicada.toString())) {
						log.escreveLog("Existe um registro no banco local com o mesmo id do registro vindo da duplicada da web.");
						// se o registro existir, verifica se nuo u pura
						// coincidencia de uma inseruuo local que ocupou o id
						if (objRegistroAndroidWeb.isRegistroLocal(
										crud.idTabelaWebDuplicada.toString(), 
										idSistemaTabela,
//										null,//idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
//										false,
										log )){
							log.escreveLog("O registro "+crud.idTabelaWebDuplicada.toString()+" no banco local estu ocupado atualmente com um novo registro que ainda nuo foi sincronizado. Tentaremos desocupar o id." );
							// Se positivo(REGISTRO LOCAL), apenas substitui o
							// identificador local para um id novo, e atualiza o
							// registro da
							// insercao para o identificador final igual o da id da
							// duplicada web
							if (!objRegistroAndroidWeb.updateIdSynchronized(
									obj.getName(),
									strIdTabelaMobile,
									idTabelaMobileAtualizado,
									crud.idTabelaWebDuplicada.toString(), 
									log, 
									true)) {
								log.escreveLog("Falha durante a tentativa de liberauuo do id: " + obj.getId() );
								return new Mensagem(TIPO.ERRO_COM_SERVIDOR,
										"Erro durante a atualizacao do identificador do banco local 2."
												+ crud.getIdentificador());
							} else {
								log.escreveLog("Id "+obj.getId()+" liberado. Iremos verificar as FKs para tentar executar a operacao novamente" );
								// preenche os valores do atributo do registro,
								// verificando as FK's
								InterfaceMensagem msg = objRegistroAndroidWeb
										.preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks(
												campos, 
												crud.valoresDuplicada, 
												obj, 
												idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
												log);
								// Se alguma das FKs foi removida e a dependencia
								// era de ON_DELETE_CASCADE, entao o registro
								// atual seru removido
//								if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_REMOVIDO_DEVIDO_ON_DELETE_CASCADE
//										.getId()) {
//									log.escreveLog("Como o registro havia sido removido on cascade por conta de outra remouuo, iremos" );
//									obj.remove(false);
//									return new Mensagem(
//											PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
//								} else 
								if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
									.getId()) {
									log.escreveLog("Fks atualizadas, tentando novamente executar o crud... " );
									// atualiza os valores
									msg =  objSistemaTabela.updateAForca(db,
											obj,
											objRegistroAndroidWeb,
											crud.getIdentificador(),
											idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
											log);
									return msg;
								} 
								else{
									log.escreveLog("Falha durante a atualizacao das FKs " );
									return msg;
								}
							}
						} else {
							log.escreveLog("O registro atualmente ocupado pelo id da duplicada ["+crud.idTabelaWebDuplicada+"] nuo u local." );
							if (obj.select(idTabelaMobileAtualizado)){
								log.escreveLog("Iremos atualizar todas as FKs que apontam para o registro " + idTabelaMobileAtualizado+ " para o id da duplicada " +crud.idTabelaWebDuplicada );
								// Se for um registro web-android
								// se o registro que ocasionou a duplicada ja existe na
								// base local
								//selecionamos para atribuir as suas dependentes para a duplicada
								//e entao removemos-o
								
								
								InterfaceMensagem msg = EXTDAOSistemaAtributo
										.atualizaChavesFkDasTabelasDependentesParaONovoPai(
												db, 
												obj,
												crud.idTabelaWebDuplicada.toString(),
												log);

								if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO
										.getId()) {
									obj.remove(idTabelaMobileAtualizado, false);
								}
								return msg;
							} else {

								log.escreveLog("processaCrudInserir - o registro " + obj.getName() + "["+idTabelaMobileAtualizado+"] foi removido do banco local. Logo nuo haveru necessidade de processar o crud.");
							}
							
						}
					} else {
						String strTabelaWebDuplicada = String.valueOf( crud.idTabelaWebDuplicada);
						Long idSincRemoveDuplicada = objRegistroAndroidWeb.getId(strTabelaWebDuplicada, idSistemaTabela, EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE, null, log);
						if(idSincRemoveDuplicada != null){
							log.escreveLog("[gnrngenk]O id "+crud.idTabelaWebDuplicada+" vindo da web que ocasionou a duplicada foi removido localmente, logo seru removido.");
							log.escreveLog("processaCrudInserir [qw342r] : " + crud.idTabelaWebDuplicada);
							obj.remove(false);
							objRegistroAndroidWeb.removerTodosAsOperacoesDoRegistro(strTabelaWebDuplicada, null, idSistemaTabela);
							return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
						} else {
							
						
							log.escreveLog("O id "+crud.idTabelaWebDuplicada+" vindo da web que ocasionou a duplicada estu desocupado. Entuo verificaremos se o registro desse mobile que havia sido enviado para sincronizacao " +  idTabelaMobileAtualizado + " se foi removido nesse meio tempo.");
							
							// se o identificador da duplicata nao existir
							// verifica se existe uma operacao de remocao para ela
							// localmente
							//atualiza o id atual para o da
								// duplicada assim como os valores dos atributos
							log.escreveLog(
									"O registro ainda existe no banco local, logo o mesmo se 'tornaru' o registro da duplicada, atualiza o id atual para o da duplicada. Da registro "
							+obj.getName() 
							+ "["+idTabelaMobileAtualizado+"] para o novo id " +crud.idTabelaWebDuplicada.toString()
							+ " visando que esse registro que ocasionou a duplicada, se torne o da WEB.");
							if (!objRegistroAndroidWeb.updateIdSynchronized(
									obj.getName(),
									strIdTabelaMobile,
									idTabelaMobileAtualizado,
									crud.idTabelaWebDuplicada.toString(), 
									log, 
									true)) {
								log.escreveLog("Falha durante a atualizacao do indicador");
								return new Mensagem(TIPO.ERRO_COM_SERVIDOR,
										"Erro durante a atualizacao do identificador do banco local 3."
												+ crud.getIdentificador());
							} else if(crud.valoresDuplicada != null 
									&& crud.valoresDuplicada.length > 0) {
								// Atualizou os identificadores das duplicadas
								log.escreveLog("Com o id liberado tentaremos corrigir os valores das FKs para atualizacao dos dados do registro local que havia ocasionado a duplicada com os valores da duplicada vindos da web.");
								// preenche os valores do atributo do registro,
								// verificando as FK's
								InterfaceMensagem msg = objRegistroAndroidWeb
										.preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks(
												campos, 
												crud.valoresDuplicada, 
												obj, 
												idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
												log);
								if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_REMOVIDO_DEVIDO_ON_DELETE_CASCADE
										.getId()) {
									log.escreveLog("Um dos valores da FK havia sido removido on cascade, logo o registro atual tambem seru.");
									obj.remove(
									crud.idTabelaWebDuplicada.toString(),
									false);
									return new Mensagem(
											PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
								} else {
									log.escreveLog("[lekfrkljer]Valores atualizados, upadte a forua...");
									
									// atualiza os valores
									msg =  objSistemaTabela.updateAForca(db,
											obj, 
											objRegistroAndroidWeb,
											crud.getIdentificador(),
											idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
											log);
									if(msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId())
										log.escreveLog("Update a forca realizado com sucesso");
									return msg;
								}
							
							}
						}
					}
				} else {
					log.escreveLog("O registro enviado sofreu duplicada com um registro da web. Porem a web quando estava criadno o arquivo de sincornizacao viu que o registro duplicado havia sido removido entre o momento que a sincronizacao comecou do lado da web ate o momento de escrever o arquivo de retorno para o mobile. Nesse caso, consideramos que o registro enviado pelo mobile na teoria foi removido pela web, que u quem manda.");
					if(idTabelaMobileAtualizado != null){
						log.escreveLog("[warefdf]Removendo o registro mobile da base local: " + idTabelaMobileAtualizado);
						obj.remove(idTabelaMobileAtualizado, false);
					}
					return new Mensagem(
							PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
				}
			} else if (crud.erroSql != null 
					&& crud.erroSql != ERRO_SQL.CHAVE_DUPLICADA.getId()
					&& crud.erroSql != ERRO_SQL.DUPLICADA.getId()) {
				log.escreveLog("O crud teve o seguinte erro ocasionado: " + crud.erroSql + ". Crud: " + crud.getIdentificador());
				if(idTabelaMobileAtualizado != null){
					log.escreveLog("Removendo o registro mobile da base local: " + idTabelaMobileAtualizado);
					obj.remove(idTabelaMobileAtualizado, false);
				}
				return new Mensagem(
						PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
			}
		}
		
		return new Mensagem(
				PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, 
				"[uklm9i04590]Condicional nao programada. Crud: " + crud.getIdentificador());
		
	}

	public InterfaceMensagem processaCrudEditar(
			String[] campos,
			CrudInserirEditar crud,
			OmegaLog log) throws Exception {
		obj.clearData();
		// Se nuo existir mais o identificador, considera-se que foi removido
 		if (!obj.select(crud.idTabelaWeb.toString())){
 			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
 				log.escreveLog("Registro " + obj.getName() + " ["+crud.idTabelaWeb+"] inexistente na base. Crud concluido com sucesso.");
			return new Mensagem(
					PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
 		}
		// Se a ediuuo nuo foi processada
//		if (crud.idCrud == null 
//			&& crud.erroSql == null
//			&& crud.valores != null 
//			&& crud.valores.length > 0) {
//
//			// se o crud mobile nuo foi executado na WEB entuo,
//			// retorna os valores originais do registro
//			InterfaceMensagem msg = objRegistroAndroidWeb
//					.preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks(
//							campos, crud.valores, obj, log);
//			if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_REMOVIDO_DEVIDO_ON_DELETE_CASCADE
//					.getId()) {
//				obj.remove(crud.idTabelaWeb.toString(), false);
//				return new Mensagem(
//						PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
//			} else {
//				// atualiza os valores
//				msg =  objSistemaTabela.updateAForca(
//						db, 
//						obj,
//						objRegistroAndroidWeb,
//						crud.getIdentificador(),
//						log);
//				return msg;
//			}
//		} else 
		if (crud.idCrud != null && crud.erroSql == null) {
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Registro " + obj.getName() + " ["+crud.idTabelaWeb+"] foi realizado com sucesso na base web. Logo nuo fazemos nada aqui localmente.");
			// se foi executado e nuo teve erro algum
			return new Mensagem(
					PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
		} else if(crud.erroSql != null){

			// se o registro ju havia sido removido na WEB, e a ediuuo chegou
			// apus o ocorrido.
			if(crud.erroSql == Database.ERRO_SQL.CRUD_MOBILE_DE_INSERCAO_CANCELADO_DEVIDO_A_REMOCAO_PAI_CASCADE.getId()){
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("Registro " + obj.getName() + " ["+crud.idTabelaWeb+"] foi removido ON CASCADE na web.");
				obj.remove(crud.idTabelaWeb.toString(), true);
				return new Mensagem(
						PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);	
			}
			else if(crud.erroSql == Database.ERRO_SQL.CHAVE_INEXISTENTE.getId()){
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("Registro " + obj.getName() + " ["+crud.idTabelaWeb+"] nuo existia na web esse registro, logo a ediuuo nuo foi realizada. Iremos remover o registro localmente.");
				obj.remove(crud.idTabelaWeb.toString(), true);
				return new Mensagem(
						PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
			}
			else if (crud.erroSql != null
				&& crud.idTabelaWeb != null 
				&& crud.valores != null 
				&& crud.valores.length > 0) {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("Registro " + obj.getName() + " ["+crud.idTabelaWeb+"] nuo foi executado devido ao erro sql: " + crud.erroSql + ". Logo iremos retornar o valor do registro para o mesmo vindo da web.");
				// se o crud mobile nuo foi executado na WEB entuo,
				// ele foi punido devido a demora de sincronizacao
				// e deve retornar os valores originais do registro
				InterfaceMensagem msg = objRegistroAndroidWeb
						.preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks(
								campos, 
								crud.valores, 
								obj,
								idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
								log);
				if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_REMOVIDO_DEVIDO_ON_DELETE_CASCADE
						.getId()) {
					obj.remove(crud.idTabelaWeb.toString(), true);
					return new Mensagem(
							PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);	
				} else {
					// atualiza os valores
					msg = objSistemaTabela.updateAForca(
							db, 
							obj,
							objRegistroAndroidWeb,
							crud.getIdentificador(),
							idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
							log);
					return msg;
				}
			}
		}
		
		return new Mensagem(
				PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, 
				"[54ewr65]Condicional nuo programada crud edit mobile. Crud: " + crud.getIdentificador());
	}

}
