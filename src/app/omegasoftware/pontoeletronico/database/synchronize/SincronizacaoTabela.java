package app.omegasoftware.pontoeletronico.database.synchronize;

import android.content.Context;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorAndroidParaWeb;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.database.synchronize.EstadoCrud.ESTADO;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA.TIPO;

public class SincronizacaoTabela {
	EXTDAOSistemaRegistroSincronizadorAndroidParaWeb objRegistroAndroidWeb;
	EXTDAOSistemaTabela objSistemaTabela;
	String tabela;
	String idSistemaTabela;
	OmegaLog log;
	Context c; 
	Database db=null;
	Long idUltimoRegistroAntesDeGerarBanco;
	Long idUltimoRegistroDepoisDeGerarBanco;
	Long idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco;
	Long idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco;
	Long idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb;
	
	Table obj = null;
	public SincronizacaoTabela(
			Context c, 
			Database db, 
			String tabela,
			OmegaLog log,
			Long idUltimoRegistroAntesDeGerarBanco,
			Long idUltimoRegistroDepoisDeGerarBanco,
			Long idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco,
			Long idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco,
			Long idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb) throws OmegaDatabaseException{
		this.db = db;
		this.tabela = tabela;
		this.log = log;
		this.objRegistroAndroidWeb = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(db);
		objSistemaTabela= new EXTDAOSistemaTabela(db);
		this.idUltimoRegistroAntesDeGerarBanco = idUltimoRegistroAntesDeGerarBanco;
		this.idUltimoRegistroDepoisDeGerarBanco = idUltimoRegistroDepoisDeGerarBanco;
		this.idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco=idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco;
		this.idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco=idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco;
		this.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb = idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb;
		this.obj = db.factoryTable(tabela);
		this.c = c;
		

		
		idSistemaTabela = EXTDAOSistemaTabela.getIdSistemaTabela(c, tabela);
	}
	
	public InterfaceMensagem validaCrudInserirEditar(CrudInserirEditar crud){
		if(crud.estado == ESTADO.EXECUTADA){
			if(obj.existeId(crud.idTabelaWeb.toString())){
				return null;
			}
		}
		return new Mensagem(TIPO.ERRO_COM_SERVIDOR, "O registro nuo foi encontrado " + obj.getName() + "::" + crud.idTabelaWeb.toString());
	}
	
	public InterfaceMensagem processaCrudInserir(String[] campos, CrudInserirEditar crud, boolean crudSaiuDoProprioMobile) throws Exception{
		
		if(crud.valoresNulo){
			//considera que o registro foi removido na web durante a sincronizacao, e em algumas sincronizacoes posteriores
			//a sua remouuo seru enviada de fato
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("crud.valoresNulo: true. Logo considera que o registro foi removido na web durante a sincronizacao, e em algumas sincronizacoes posteriores");
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
		}
		
		InterfaceMensagem msg= objRegistroAndroidWeb.preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks(
				campos, 
				crud.valores, 
				obj,
				idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
				log);
		
		if(msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_REMOVIDO_DEVIDO_ON_DELETE_CASCADE.getId()){
//			obj.remove(crud.idTabelaWeb.toString(), false);
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
		} else{
			//atualiza os valores
			InterfaceMensagem msg2 = null;
			try{
				if(crudProvenienteDoBancoDaPrimeiraSincronizacao(
						crud.idSistemaRegistroSincronizador,
						crud.idCrud,
						crud.provenienteDaWeb(),
						false
				)){
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Crud u proveniente do banco inicialmente sincronizado");
					//o crud ju veio no .db proveniente do servidor
					return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
				}
				msg2 = EXTDAOSistemaTabela.inserirAForca(
						db, 
						obj, 
						objRegistroAndroidWeb, 
						crud.getIdentificador(),
						idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
						log,
						crudSaiuDoProprioMobile);
				if( msg2.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC.getId()){
					if(crudProvenienteDoBancoDaPrimeiraSincronizacao(
							crud.idSistemaRegistroSincronizador,
							crud.idCrud,
							crud.provenienteDaWeb(),
							true
							))
						return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
					return msg2;
				}
				if(msg2.mCodRetorno != PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()){
					if(crudProvenienteDoBancoDaPrimeiraSincronizacao(
							crud.idSistemaRegistroSincronizador,
							crud.idCrud,
							crud.provenienteDaWeb(),
							true
							))
						return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
				}
				
				return msg2;
			}catch(Exception ex ){
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("[ert4]processaCrudInserir:: Erro: " + HelperExcecao.getDescricao(ex));
				
				if(crudProvenienteDoBancoDaPrimeiraSincronizacao(
						crud.idSistemaRegistroSincronizador,
						crud.idCrud,
						crud.provenienteDaWeb(),
						true
						))
					return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
				else 
					return new Mensagem(ex);
				
			}
			
		}
	}

	//Processa as operacoes 'remove' do arquivo crud_mobile  
	public Mensagem processaCrudRemove(CrudRemover crudRemover){
		//Nao eh necessario fazer nenhuma verificacao
		//apenas guardas os identificadores removidos
		//Processa a inseruuo de um identificador uma unica vez, comeuando do ultimo arquivo de sincronizacao
		//atu o primeiro
		//crud_id_INT != null // Operacao foi realizada com sucesso
		try {
			
			if(crudProvenienteDoBancoDaPrimeiraSincronizacao(
					crudRemover.idSistemaRegistroSincronizador, 
					crudRemover.idCrud, 
					crudRemover.provenienteDaWeb(),
					false)){
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("processaCrudRemove:: Crud proveniente da primeira sincronizacao, logo nuo seru executado");
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
			}
			
			String strIdTabelaWeb = crudRemover.idTabelaWeb.toString();
			if(obj.existeId(strIdTabelaWeb)){
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("processaCrudRemove:: Exise id web no banco local: " + strIdTabelaWeb);
				//se for um registro novo, ou seja, o registro que estu sendo removido nem chegou a ser
				//inserido no banco web, estu apenas no banco local
				
				//se nuo era um registro local, entuo o mesmo deve ser removido
				if (!objRegistroAndroidWeb.isRegistroLocal(
						strIdTabelaWeb, 
						idSistemaTabela, 
//						idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
//						false,
						log)) {
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("processaCrudRemove:: Removendo o registro..." );
					//REMOVER TODOS OS REGISTROS DO SINCRONIZADOR LOCAL REFERENCIADOS COM A TABELA
					
					objRegistroAndroidWeb.removerTodosAsOperacoesDoRegistro(strIdTabelaWeb, null, crudRemover.idSistemaTabela.toString());
					log.escreveLog("remove [23redwg] - " + obj.getName() + "::" + strIdTabelaWeb);
					
					Integer validade = obj.remove(strIdTabelaWeb, false);
					if(validade != null){
						
						if(validade > 0){
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("processaCrudRemove:: Registro removido com sucesso." );
							return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, "Registro "+strIdTabelaWeb+" removido com sucesso");
						}
						else{
							if(crudProvenienteDoBancoDaPrimeiraSincronizacao(
									crudRemover.idSistemaRegistroSincronizador, 
									crudRemover.idCrud, 
									crudRemover.provenienteDaWeb(),
									true)){
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("processaCrudRemove 15asd4651:: crudProvenienteDoBancoDaPrimeiraSincronizacao " );
								return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
							}
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("processaCrudRemove asd5465:: erro " );
							return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, "Nuo conseguiu remover o Id: " + strIdTabelaWeb);
						}
					}
					if(crudProvenienteDoBancoDaPrimeiraSincronizacao(
							crudRemover.idSistemaRegistroSincronizador, 
							crudRemover.idCrud, 
							crudRemover.provenienteDaWeb(),
							true)){
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("processaCrudRemove 5as2d1rf:: crudProvenienteDoBancoDaPrimeiraSincronizacao " );
						return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
					}
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("processaCrudRemove 999asd:: erro " );
					return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, "Erro durante a remouuo do id " + obj.getId() + " na tabela " + obj.getName() + ". Id do crud de remouuo: " + crudRemover.idCrud );
					
				} else { //else se for local deixa ele quieto na dele, logo logo vai ser processado
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("processaCrudRemove 999asd:: registro local, logo na teoria o registro da web que estava no banco local ju foi removido" );
				}
				
			} else {
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("processaCrudRemove:: Removendo todas as operauues do registro [egrrth]..." );
				int totalRemovido = objRegistroAndroidWeb.removerTodosAsOperacoesDoRegistro(strIdTabelaWeb, null, crudRemover.idSistemaTabela.toString());
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("processaCrudRemove:: Total de registros da tabela de sincronizacao que foram removidos: " + totalRemovido );
			}
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, "O registro ju havia sido removido");
		
		} catch (Exception e) {
			
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[lkndfgf35]processaCrudRemover::" + HelperExcecao.getDescricao(e));
			
			if(crudProvenienteDoBancoDaPrimeiraSincronizacao(
					crudRemover.idSistemaRegistroSincronizador, 
					crudRemover.idCrud, 
					crudRemover.provenienteDaWeb(),
					true))
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
			else 
				return new Mensagem(e);
		}
	}

		
	public boolean crudProvenienteDoBancoDaPrimeiraSincronizacao(
			Long idSistemaRegistroSincronizador, 
			Long idCrud, 
			boolean provenienteDaWeb,
			boolean considerarALimitanteDoIdSincronizadorAposGerarOBanco){
		if(idSistemaRegistroSincronizador == null)
			return false;
		if(considerarALimitanteDoIdSincronizadorAposGerarOBanco){
			if(provenienteDaWeb){
				if(this.idUltimoRegistroDepoisDeGerarBanco >= idSistemaRegistroSincronizador
					|| this.idUltimoRegistroAntesDeGerarBanco >= idSistemaRegistroSincronizador
						)
					return true;

			} 
			if(this.idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco >= idCrud 
					|| this.idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco >= idCrud
					)
				return true;
			else return false;
		} else {
			if(provenienteDaWeb){
				if(
					this.idUltimoRegistroAntesDeGerarBanco >= idSistemaRegistroSincronizador
						)
					return true;
					
			} 
			if(this.idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco >= idCrud 
				
					)
				return true;
			else return false;
		}
		
	
		 
	}
	
	
	
	
	public InterfaceMensagem processaCrudEdit(String[] campos, CrudInserirEditar crud) throws Exception{
		String strIdTabelaWeb = crud.idTabelaWeb.toString(); 
		if(crud.valoresNulo){
			log.escreveLog("processaCrudEdit - remove [eokeriokmovfd.,dklerj] : " + strIdTabelaWeb);
			Integer totalRemovido = obj.remove(strIdTabelaWeb, true);
			//considera que o registro foi removido na web durante a sincronizacao, e em algumas sincronizacoes posteriores
			//a sua remouuo seru enviada de fato
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("crud.valoresNulo: true. Logo considera que o registro foi removido na web durante a sincronizacao, e em algumas sincronizacoes posteriores: "+ totalRemovido);
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
		}
		if(!obj.existeId(strIdTabelaWeb)){
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("O registro nuo existe mais no banco local: " + obj.getId());
			//Considera-se no fim dos casos que o registro foi removido alguma hora durante a sincronizacao
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
			
		}
		InterfaceMensagem msg= objRegistroAndroidWeb.preencheOsValoresDaSincronizacaoWebNoRegistroVerificandoAsFks(
				campos, 
				crud.valores, 
				obj, 
				idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
				log);
		
		//Se alguma das FKs foi removida e a dependencia era de ON_DELETE_CASCADE, entao o registro
		//atual seru removido
		if(msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_REMOVIDO_DEVIDO_ON_DELETE_CASCADE.getId()){
			Integer totalRemovido = obj.remove(false);
			if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Alguma das FKs foi removida e a dependencia era de ON_DELETE_CASCADE, entao o registro. TOtal removido: " + totalRemovido);
			return  new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
		} else if(msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()){
			if(crudProvenienteDoBancoDaPrimeiraSincronizacao(
					crud.idSistemaRegistroSincronizador, 
					crud.idCrud, 
					crud.provenienteDaWeb(),
					false))
				return  new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
			try{
				//atualiza os valores
				msg = objSistemaTabela.updateAForca(
						db, 
						obj, 
						objRegistroAndroidWeb, 
						crud.getIdentificador(), 
						idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
						log);
				
				if(msg != null && msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC.getId()){ 
					
					return msg;
				}else if(msg != null && msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()){
					return msg;
				}
				else {
					if(crudProvenienteDoBancoDaPrimeiraSincronizacao(
							crud.idSistemaRegistroSincronizador,
							crud.idCrud,
							crud.provenienteDaWeb(),
							true
							)){
						msg.mCodRetorno =PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId(); 
						return msg;
					}
					else{ 
						return msg;
					}
				}
			}catch(Exception ex){
				if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("[ert4]processaCrudEdit:: Erro: " + HelperExcecao.getDescricao(ex));
				
				if(crudProvenienteDoBancoDaPrimeiraSincronizacao(
						crud.idSistemaRegistroSincronizador, 
						crud.idCrud, 
						crud.provenienteDaWeb(),
						true)){
					return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
				}
				else{ 
					return new Mensagem(ex);
				}
			}
			
		} else return  msg;
	}
}
