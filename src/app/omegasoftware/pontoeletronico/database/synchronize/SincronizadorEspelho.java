package app.omegasoftware.pontoeletronico.database.synchronize;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.Log;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemConfiguracao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.Sessao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.BO.BOOperacaoDownloadSQLiteMobile;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloProcessoEmAberto;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.ValidadeArquivoJson;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.thread.HelperThread;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.Tuple;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroHistorico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorAndroidParaWeb;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoOperacaoBanco;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.file.HelperFile;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.http.HelperHttp;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_BOOLEAN;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_INT;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_STRING;
import app.omegasoftware.pontoeletronico.pontoeletronico.PrincipalActivity;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperLong;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
import app.omegasoftware.pontoeletronico.service.ESTADO_SINCRONIZADOR;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiveBroadcastSincronizador;
import app.omegasoftware.pontoeletronico.webservice.PacoteCorpo;
import app.omegasoftware.pontoeletronico.webservice.PacoteEditInsert;
import app.omegasoftware.pontoeletronico.webservice.PacoteRemove;
import app.omegasoftware.pontoeletronico.webservice.ServicosSincronizadorWeb;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemProtocolo;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemToken;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA.TIPO;
import app.omegasoftware.pontoeletronico.webservice.protocolo.ProtocoloStatusSincronizacao;

public class SincronizadorEspelho {
	public static final String ID_CORPORACAO_SINCRONIZADA = "ID_CORPORACAO_SINCRONIZADA";
	public static final String CORPORACAO_SINCRONIZADA = "CORPORACAO_SINCRONIZADA";
	final long LIMITE_REGISTROS_ENVIADOS_POR_SINCRONIZACAO = 75;
	long limiteVariavelRegistrosEnviadosPorSincronizacao = LIMITE_REGISTROS_ENVIADOS_POR_SINCRONIZACAO;
	final boolean ENVIAR_ARQUIVO_LOG_EM_CASO_DE_SUCESSO = false;

	ServicosSincronizadorWeb servWeb = null;
	OmegaLog log;
	
	// ControladorParseSincronizacao controlador ;
	EXTDAOSistemaRegistroSincronizadorAndroidParaWeb obj;
	EXTDAOSistemaRegistroHistorico objSRH;

	Context context;
	static SincronizadorEspelho singleton;
	String vetorTabelaUpload[];
	String vetorTabelaDownload[];
	String mobileIdentificador;
	String mobileConectado;

	public static Tuple<Integer, String> getCorporacaoSincronizada(Context pContext) {
		SharedPreferences vSavedPreferences = pContext.getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);
		if (!vSavedPreferences.contains(CORPORACAO_SINCRONIZADA))
			return null;
		else {
			int intId = vSavedPreferences.getInt(ID_CORPORACAO_SINCRONIZADA, -1);

			Integer id = intId == -1 ? null : intId;
			if (id == null)
				return null;
			String corporacao = vSavedPreferences.getString(CORPORACAO_SINCRONIZADA, null);

			return new Tuple<Integer, String>(id, corporacao);
		}
	}

	public static void setCorporacaoSincronizada(Context pContext, Integer idCorporacao, String corporacao) {
		if (idCorporacao == null)
			idCorporacao = -1;
		SharedPreferences vSavedPreferences = pContext.getSharedPreferences(OmegaConfiguration.PREFERENCES_NAME, 0);

		SharedPreferences.Editor vEditor = vSavedPreferences.edit();
		vEditor.putInt(ID_CORPORACAO_SINCRONIZADA, idCorporacao);
		vEditor.putString(CORPORACAO_SINCRONIZADA, corporacao);
		vEditor.commit();
	}

	public enum ESTADO_SINCRONIZACAO_MOBILE {
		OCORREU_UM_ERRO_FATAL(-3), ACABOU_DE_INICIAR_O_BANCO(-2), PRIMEIRA_VEZ(
				-1), MOBILE_ESTA_ENVIANDO_O_ARQUIVO_DE_SINCRONIZACAO(10), FILA_ESPERA_PARA_SINCRONIZACAO(
						1), INSERINDO_DADOS_DO_ARQUIVO_MOBILE_NO_BANCO_ESPELHO(
								2), PROCESSANDO_DADOS_DO_BANCO_ESPELHO_NO_BANCO_WEB(
										3), OCORREU_UM_ERRO_LEVE_QUE_SO_ENVIARA_OS_DADOS_LOCAIS_QUANDO_RESETAR(
												4), AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE(
														5), DOWNLOAD_DOS_DADOS_DE_SINCRONIZACAO(
																-4), AGUARDANDO_PROCESSAMENTO_DO_ARQUIVO_WEB_NO_MOBILE(
																		6), FINALIZANDO(-7), FINALIZADO(
																				7), SETANDO_ERRO_FATAL_NA_WEB(
																						-8), RESETADO(8), CANCELADO(
																								9), ESTRUTURA_MIGRADA(
																										10), AGUARDANDO_USUARIO_RESETAR(
																												11);

		final int id;

		ESTADO_SINCRONIZACAO_MOBILE(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}
	}

	public enum ESTADO_SINCRONIZACAO {
		INSERINDO_OPERACOES_DOS_ARQUIVOS_MOBILES_NO_BANCO_ESPELHO(
				1), PROCESSANDO_OPERACOES_DO_BANCO_ESPELHO_NO_BANCO_WEB(2), ERRO_SINCRONIZACAO_MOBILE(
						3), ERRO_SINCRONIZACAO(4), INSERINDO_OPERACOES_DO_BANCO_WEB_NO_BANCO_ESPELHO(
								5), GERANDO_ARQUIVO_DE_DADOS_WEB_PARA_CELULAR(6), FINALIZADO(7);

		final int id;

		ESTADO_SINCRONIZACAO(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}
	}

	// Item item;
	public static ESTADO_SINCRONIZACAO getEstadoSincronizacao(int id) {
		ESTADO_SINCRONIZACAO[] vetor = ESTADO_SINCRONIZACAO.values();
		for (int i = 0; i < vetor.length; i++) {
			int codigo = vetor[i].getId();
			if (codigo == id) {
				return vetor[i];
			}
		}
		return null;
	}

	public static ESTADO_SINCRONIZACAO_MOBILE getEstadoSincronizacaoMobile(int id) {
		ESTADO_SINCRONIZACAO_MOBILE[] vetor = ESTADO_SINCRONIZACAO_MOBILE.values();
		for (int i = 0; i < vetor.length; i++) {
			int codigo = vetor[i].getId();
			if (codigo == id) {
				return vetor[i];
			}
		}
		return null;
	}

	public static class Item {
		public Long[] idsSincronizacao;
		public Long[] idsSincronizacaoIteracao;

		public Boolean enviouDadosSincronizacaoMobile;
		public Long idSincronizacaoAtual;
		public Long idSincronizacaoMobile;
		public ESTADO_SINCRONIZACAO_MOBILE estado;

		public Long idUltimaSincronizacao;
		public Boolean primeiraSincronizacao;
		public Boolean possuiArquivoCrudWebParaMobile;
		public Long idUltimoRegistroAntesDeGerarBanco;
		public Long idUltimoRegistroDepoisDeGerarBanco;
		public Long idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco;
		public Long idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco;
		public Long idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb;

		public Integer indiceUltimoDownloadSincronizacaoRealizada;
		public Integer indicePrimeiroDownloadSincronizacaoRealizada;
		public Integer contadorTentativasDownload;
		public OmegaLog log;
		public String ultimaMigracao;
		private String mensagemDialogSincronizador;
		public String getMensagemDialogSincronizador(){
			return mensagemDialogSincronizador;
		}
		public void appendMensageDialogSincronizador(String msg){
			if(msg == null || msg.length() == 0) return;
			if(mensagemDialogSincronizador  != null && mensagemDialogSincronizador .length() > 0 )
				mensagemDialogSincronizador  += "\n";
			mensagemDialogSincronizador += "["+ HelperDate.getStringDateTimeSQL(new Date()) + "] - "+ msg;
			
			if(mensagemDialogSincronizador.length() > 360){
				
				mensagemDialogSincronizador = mensagemDialogSincronizador.substring(mensagemDialogSincronizador.length() - 360);
			}
		}
		public Item(OmegaLog log) {
			contadorTentativasDownload = 0;
			this.log = log;

		}

		public boolean downloadDaUltimaSincronizacaoRealizada() {
			return item.indiceUltimoDownloadSincronizacaoRealizada == item.idsSincronizacao.length;
		}

		public void setEstado(ESTADO_SINCRONIZACAO_MOBILE estado) {
			if (estado == ESTADO_SINCRONIZACAO_MOBILE.DOWNLOAD_DOS_DADOS_DE_SINCRONIZACAO
					&& this.estado != ESTADO_SINCRONIZACAO_MOBILE.DOWNLOAD_DOS_DADOS_DE_SINCRONIZACAO) {
				this.contadorTentativasDownload = 0;
			}
			this.estado = estado;
		}

		public void registraNovaTentativaFalhaDeDownloadDeArquivoDeSincronizacao() {
			this.contadorTentativasDownload++;
		}

		public boolean totalDeTentativasDeDownloadForamEsgotadas() {
			if (this.contadorTentativasDownload < OmegaConfiguration.LIMITE_TENTATIVA_DOWNLOAD_SINCRONIZACAO) {
				return false;
			} else
				return true;
		}

		public String getIdentificador() {
			String token = "";
			token += ". Id Sincronizacao Mobile: " + HelperString.formatarParaImpressao(idSincronizacaoMobile);
			token += ". Id Sincronizacao Atual: " + HelperString.formatarParaImpressao(idSincronizacaoAtual);
			token += ". Id Sincronizacoes: " + HelperString.arrayToStringFirstAndLast(idsSincronizacao, ",");

			token += ". Id Sincronizacoes Iteracao: " + HelperString.arrayToString(idsSincronizacaoIteracao, ",");

			token += ". Estado: " + (estado == null ? "-" : estado.toString());
			token += ". Id Ultima Sincronizacao: " + HelperString.formatarParaImpressao(idUltimaSincronizacao);
			token += ". Primeira Sincronizacao? " + HelperString.formatarParaImpressao(primeiraSincronizacao);
			token += ". Arquivo Crud Para Mobile? "
					+ HelperString.formatarParaImpressao(possuiArquivoCrudWebParaMobile);

			token += ". Ultimo registro antes de gerar o banco? "
					+ HelperString.formatarParaImpressao(idUltimoRegistroAntesDeGerarBanco);

			token += ". Ultimo registro antes de gerar o banco? "
					+ HelperString.formatarParaImpressao(idUltimoRegistroDepoisDeGerarBanco);

			token += ". Ultimo crud antes de gerar o banco? "
					+ HelperString.formatarParaImpressao(idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco);

			token += ". Ultimo crud depois de gerar o banco? "
					+ HelperString.formatarParaImpressao(idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco);

			token += ". indiceSincronizadorDownloadRealizado: "
					+ HelperString.formatarParaImpressao(indiceUltimoDownloadSincronizacaoRealizada);

			token += ". Data da ultima migracao: " + ultimaMigracao;

			return token;
		}

		public String getConteudoJson(Context c) {
			String json = PontoEletronicoSharedPreference.getValor(c, TIPO_STRING.JSON_ITEM_SINCRONIZACAO);
			return json;
		}

		public boolean salvar(Context c) throws JSONException {

			JSONObject obj = new JSONObject();

			obj.put("idUltimaSincronizacao", idUltimaSincronizacao);

			obj.put("enviouDadosSincronizacaoMobile", enviouDadosSincronizacaoMobile);
			obj.put("idSincronizacaoAtual", idSincronizacaoAtual);
			obj.put("idUltimoRegistroAntesDeGerarBanco", idUltimoRegistroAntesDeGerarBanco);
			obj.put("idUltimoRegistroDepoisDeGerarBanco", idUltimoRegistroDepoisDeGerarBanco);

			obj.put("idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco",
					idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco);
			obj.put("idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco",
					idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco);

			obj.put("idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb",
					idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb);

			obj.put("estado", estado.getId());

			obj.put("idSincronizacaoMobile", idSincronizacaoMobile);

			obj.put("primeiraSincronizacao", primeiraSincronizacao);
			obj.put("possuiArquivoCrudWebParaMobile", possuiArquivoCrudWebParaMobile);
			obj.put("indiceUltimoDownloadSincronizacaoRealizada", indiceUltimoDownloadSincronizacaoRealizada);
			obj.put("indicePrimeiroDownloadSincronizacaoRealizada", indicePrimeiroDownloadSincronizacaoRealizada);

			if (this.idsSincronizacao != null && this.idsSincronizacao.length > 0) {
				JSONArray ids = new JSONArray();
				for (int i = 0; i < this.idsSincronizacao.length; i++) {
					ids.put(this.idsSincronizacao[i]);
				}
				obj.put("idsSincronizacao", ids);

			} else
				obj.put("idsSincronizacao", null);

			if (this.idsSincronizacaoIteracao != null && this.idsSincronizacaoIteracao.length > 0) {
				JSONArray ids = new JSONArray();
				for (int i = 0; i < this.idsSincronizacaoIteracao.length; i++) {
					ids.put(this.idsSincronizacaoIteracao[i]);
				}
				obj.put("idsSincronizacaoIteracao", ids);

			} else
				obj.put("idsSincronizacaoIteracao", null);

			obj.put("ultimaMigracao", ultimaMigracao);
			obj.put("mensagemDialogSincronizador", mensagemDialogSincronizador);
			String strJson = obj.toString();

			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO && log != null)
				log.escreveLog("Salvando o item ... " + strJson);
			PontoEletronicoSharedPreference.saveValor(c, TIPO_STRING.JSON_ITEM_SINCRONIZACAO, strJson);
			return true;
		}

		public boolean carregaDaMemoria(Context c) throws JSONException {

			String json = PontoEletronicoSharedPreference.getValor(c, TIPO_STRING.JSON_ITEM_SINCRONIZACAO);
			if (json == null)
				return false;

			JSONObject obj = new JSONObject(json);

			if (obj.has("idsSincronizacao") && !obj.isNull("idsSincronizacao")) {
				JSONArray jIdsSincronizacao = obj.getJSONArray("idsSincronizacao");
				if (jIdsSincronizacao != null && jIdsSincronizacao.length() > 0) {
					idsSincronizacao = new Long[jIdsSincronizacao.length()];
					for (int i = 0; i < jIdsSincronizacao.length(); i++) {
						idsSincronizacao[i] = jIdsSincronizacao.getLong(i);
					}

				}
			}

			if (obj.has("idsSincronizacaoIteracao") && !obj.isNull("idsSincronizacaoIteracao")) {
				JSONArray jIdsSincronizacaoIteracao = obj.getJSONArray("idsSincronizacaoIteracao");
				if (jIdsSincronizacaoIteracao != null && jIdsSincronizacaoIteracao.length() > 0) {
					idsSincronizacaoIteracao = new Long[jIdsSincronizacaoIteracao.length()];
					for (int i = 0; i < jIdsSincronizacaoIteracao.length(); i++) {
						idsSincronizacaoIteracao[i] = jIdsSincronizacaoIteracao.getLong(i);
					}
				}
			}

			if (!obj.isNull("enviouDadosSincronizacaoMobile")) {
				enviouDadosSincronizacaoMobile = obj.getBoolean("enviouDadosSincronizacaoMobile");
			}

			if (!obj.isNull("idSincronizacaoAtual")) {
				idSincronizacaoAtual = obj.getLong("idSincronizacaoAtual");
			}
			if (!obj.isNull("idSincronizacaoMobile")) {
				idSincronizacaoMobile = obj.getLong("idSincronizacaoMobile");
			}
			if (!obj.isNull("estado")) {
				int idEstado = obj.getInt("estado");
				setEstado(getEstadoSincronizacaoMobile(idEstado));
			}
			if (!obj.isNull("idUltimaSincronizacao")) {
				idUltimaSincronizacao = obj.getLong("idUltimaSincronizacao");
			}
			if (!obj.isNull("primeiraSincronizacao")) {
				primeiraSincronizacao = obj.getBoolean("primeiraSincronizacao");
			}
			if (obj.has("possuiArquivoCrudWebParaMobile") && !obj.isNull("possuiArquivoCrudWebParaMobile")) {
				possuiArquivoCrudWebParaMobile = obj.getBoolean("possuiArquivoCrudWebParaMobile");
			}

			if (!obj.isNull("idUltimoRegistroAntesDeGerarBanco")) {
				idUltimoRegistroAntesDeGerarBanco = obj.getLong("idUltimoRegistroAntesDeGerarBanco");
			}

			if (!obj.isNull("idUltimoRegistroDepoisDeGerarBanco")) {
				idUltimoRegistroDepoisDeGerarBanco = obj.getLong("idUltimoRegistroDepoisDeGerarBanco");
			}

			if (!obj.isNull("idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco")) {
				idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco = obj
						.getLong("idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco");
			}

			if (!obj.isNull("idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco")) {
				idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco = obj
						.getLong("idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco");
			}

			if (!obj.isNull("idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb")) {
				idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb = obj
						.getLong("idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb");
			}
			if (!obj.isNull("indiceUltimoDownloadSincronizacaoRealizada")) {
				indiceUltimoDownloadSincronizacaoRealizada = obj.getInt("indiceUltimoDownloadSincronizacaoRealizada");
			}
			if (!obj.isNull("indicePrimeiroDownloadSincronizacaoRealizada")) {
				indicePrimeiroDownloadSincronizacaoRealizada = obj
						.getInt("indicePrimeiroDownloadSincronizacaoRealizada");
			}

			if (!obj.isNull("ultimaMigracao")) {
				ultimaMigracao = obj.getString("ultimaMigracao");
			}
			
			if (!obj.isNull("mensagemDialogSincronizador")) {
				mensagemDialogSincronizador = obj.getString("mensagemDialogSincronizador");
			}

			return true;
		}

	}

	public static void setIdSincronizadorNoMomentoQueOBancoSqliteFoiCriado(Context c, long ultimoIdSinc) {

		PontoEletronicoSharedPreference.saveValor(c, PontoEletronicoSharedPreference.TIPO_INT.ULTIMO_ID_SINCRONIZACAO,
				ultimoIdSinc);
	}

	public static void resetarEstado(OmegaLog log, Context c, Long ultimoIdSinc, long idUltimoRegistroAntesDeGerarBanco,
			long idUltimoRegistroDepoisDeGerarBanco, long idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco,
			long idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco, String ultimaMigracao) throws JSONException {
		if (item == null)
			item = new Item(log);
		// public Integer[] idsSincronizacao;
		item.idsSincronizacao = null;
		item.idsSincronizacaoIteracao = null;
		// public Integer iTabelas;

		// public Boolean enviouDadosSincronizacaoMobile;
		item.enviouDadosSincronizacaoMobile = null;
		// public Integer idSincronizacaoAtual;
		item.idSincronizacaoAtual = null;
		// public Integer idSincronizacaoMobile;
		// public ESTADO estado;
		item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.ACABOU_DE_INICIAR_O_BANCO);
		// public Integer id da tabela sincronizacao::sincronizador_web
		item.idUltimaSincronizacao = ultimoIdSinc;
		// public Boolean primeiraSincronizacao;
		item.primeiraSincronizacao = true;
		// public Boolean possuiArquivoCrudWebParaMobile;
		item.possuiArquivoCrudWebParaMobile = null;

		item.idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco = idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco;
		item.idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco = idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco;

		item.idUltimoRegistroAntesDeGerarBanco = idUltimoRegistroAntesDeGerarBanco;

		item.idUltimoRegistroDepoisDeGerarBanco = idUltimoRegistroDepoisDeGerarBanco;

		item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb = null;

		item.indiceUltimoDownloadSincronizacaoRealizada = null;
		// A ultima migracao su u mudada quando u difernete de nula
		if (ultimaMigracao != null)
			item.ultimaMigracao = ultimaMigracao;

		item.salvar(c);
	}

	public static void resetarEstado(Context context) {
		PontoEletronicoSharedPreference.saveValor(context,
				PontoEletronicoSharedPreference.TIPO_INT.ESTADO_SINCRONIZADOR,
				ESTADO_SINCRONIZACAO_MOBILE.ACABOU_DE_INICIAR_O_BANCO.getId());
		PontoEletronicoSharedPreference.saveValor(context,
				PontoEletronicoSharedPreference.TIPO_INT.ULTIMO_ID_SINCRONIZACAO, -1);
		setCorporacaoSincronizada(context, null, null);
	}

	public static Long getIdSincronizacaoNoMomentoDaCriacaoDoBancoLocal(Context c) {
		return PontoEletronicoSharedPreference.getValorLong(c,
				PontoEletronicoSharedPreference.TIPO_INT.ULTIMO_ID_SINCRONIZACAO, -1);
	}

	
	private static String ARQUIVO_LOG(){
		return "sincronizacao_mobile"+HelperDate.getNomeArquivoGeradoACadaXMinutos()+".log";
	}
	private static String ARQUIVO_LOG_COMPLETO(){
		return "sincronizacao_mobile_completo"+HelperDate.getNomeArquivoGeradoACadaXMinutos()+".log";
	}

	private SincronizadorEspelho(Database db) throws Exception {
		this.context = db.getContext();

		obj = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(db);
		objSRH = new EXTDAOSistemaRegistroHistorico(db);

		vetorTabelaUpload = OmegaConfiguration.LISTA_TABELA_SINCRONIZACAO_UPLOAD_CONTINUO(db);
		vetorTabelaDownload = OmegaConfiguration.LISTA_TABELA_SINCRONIZACAO_DOWNLOAD_CONTINUO(db);
//		if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
			log = new OmegaLog(
					ARQUIVO_LOG(), 
					OmegaFileConfiguration.TIPO.LOG, 
					OmegaConfiguration.DEBUGGING_SINCRONIZACAO,
					this.context);
//		}
			
	}

	public static SincronizadorEspelho getInstance(Context pContext) throws Exception {
		if (singleton == null) {
			Database db = null;
			try {
				// TODO 27/11/2016 - removi a condicional, pois ju verificamos
				// se houve erro fatal ou nao na ultima sincornizacao
				// nao sei se vai dar certo
				// STATE_SINCRONIZADOR state =
				// RotinaSincronizador.getStateSincronizador(pContext);
				// if(state == STATE_SINCRONIZADOR.COMPLETO){
				db = new DatabasePontoEletronico(pContext);
				singleton = new SincronizadorEspelho(db);
				// }
			} finally {
				if (db != null)
					db.close();
			}

		}
		return singleton;
	}

	public InterfaceMensagem verificaMobileIdentificador() {
//		if (OmegaSecurity.isAutenticacaoRealizada()) {
			ProtocoloConectaTelefone protocolo = Sessao.getSessao(context, OmegaSecurity.getIdCorporacao(),
					OmegaSecurity.getIdUsuario(), OmegaSecurity.getCorporacao());
			if (protocolo == null){
				servWeb = new ServicosSincronizadorWeb(context, null, null);
				return Mensagem.factoryMsgSemInternet(context);
			}
			else {
				mobileConectado = protocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileConectado);
				mobileIdentificador = protocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador);
				servWeb = new ServicosSincronizadorWeb(context, mobileConectado, mobileIdentificador);
				return new Mensagem(TIPO.OPERACAO_REALIZADA_COM_SUCESSO, "");
			}

//		} else {
//			ProtocoloConectaTelefone protocolo = Sessao.getSessao(
//					context, 
//					null,
//					null,
//					null);
//			if (protocolo == null){
//				return Mensagem.factoryMsgSemInternet(context);
//			}
//			else {
//				mobileConectado = protocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileConectado);
//				mobileIdentificador = protocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador);
//				servWeb = new ServicosSincronizadorWeb(context, mobileConectado, mobileIdentificador);
//				return new Mensagem(TIPO.OPERACAO_REALIZADA_COM_SUCESSO, "");
//			}
//		}
	}

	public static ESTADO_SINCRONIZACAO_MOBILE getEstadoSalvo(Context c) {
		int idEstado = PontoEletronicoSharedPreference.getValor(c, TIPO_INT.ESTADO_SINCRONIZADOR, -1);
		if (idEstado < 0)
			return null;
		else
			return getEstadoSincronizacaoMobile(idEstado);

	}

	public InterfaceMensagem getMensagemErroFatal() {
		return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
				"Ocorreu um problema durante a sincronizacao, realize o processo de RESETAR para retornar ao normal. Sentimos muito.");
	}

	public InterfaceMensagem getMensagemEstruturaMigrada() {
		return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ESTRUTURA_MIGRADA,
				"O pacote da corporauuo foi alterado, consequentemente sua estrutura foi migrada. Favor resetar o dispositivo.");
	}

	public InterfaceMensagem getMensagemCancelado() {
		return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, "A sincronizacao foi cancelada.");
	}
	
	public InterfaceMensagem getMensagemOffline() {
		return new Mensagem(PROTOCOLO_SISTEMA.TIPO.SEM_CONEXAO_A_INTERNET, "A sincronizacao foi cancelada.");
	}

	public Item getItem(Context context) throws JSONException, Exception {
		return getItem(context, log);
	}

	public static Item getItem(Context context, OmegaLog log) throws JSONException, Exception {

		Item item = new Item(log);
		if (!item.carregaDaMemoria(context)) {
			if (log != null && OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("item nuo estu na memoria, resetando os dados... " + item.getIdentificador());
			resetarEstado(log, context, (long) -1, (long) -1, (long) -1, (long) -1, (long) -1, null);

			if (!item.carregaDaMemoria(context)) {
				if (log != null && OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("Mesmo apus resetar os dados ocorreu uma falha ao tentar carregar os dados do item");
				return null;
			}
		} else if (log != null && OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("item carregado da memoria: " + item.getIdentificador());
		return item;
	}

	public boolean validaEstadoItemParaFilaEspera(Item item) {
		return item != null && item.idSincronizacaoMobile != null;
	}

	// static boolean passou = false;
	// ESTADO estado = null;
	private final ReentrantLock lock = new ReentrantLock();
	static AtomicInteger iTentativa = new AtomicInteger(0);
	final static int MAX_TENTATIVA = 3;
	static Item item = null;
	final static int MAX_SEGUNDOS_PARA_LOCK_RESET = 10;
	
	public InterfaceMensagem resetar() {
		
		try {
			for(int i = 0 ; i < 3 ; i++)
			{
				if ( lock.tryLock(SincronizadorEspelho.MAX_SEGUNDOS_PARA_LOCK_RESET, TimeUnit.SECONDS )) {
					try {
						Database db = null;
						InterfaceMensagem mensagem =null;
						boolean realizouAutenticacaoAutomatica = false;
						try {
							
							if(!HelperHttp.isConnected(context)){
								return Mensagem.factoryMsgSemInternet(context);
							}
							else if(!Database.isDatabaseInitialized(context) ){
								return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, 
										context.getString(R.string.sem_corporacao_carregada));
							}
							else if (!OmegaSecurity.isAutenticacaoRealizada()){
								if(OmegaSecurity.setCorporacaoSincronizadaTemporariamente(context)==null){
									realizouAutenticacaoAutomatica = true; 
									return new Mensagem(
											PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
											"Nuo havia corporacao sincronizada");
								}
//								if(OmegaSecurity.realizaLoginOfflineComOUsuarioSalvo(context))
//									realizouAutenticacaoAutomatica = true; 
//								
//								if(!realizouAutenticacaoAutomatica)
//									return Mensagem.factoryAreaRestrita(context);
							}
							db = new DatabasePontoEletronico(context);
							
							obj = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(db);
							if (item == null)
								item = getItem(context, log);
							if (item == null) {
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("[lkmsfgioepokrtopker]Item nulo");
								return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_SEM_SER_EXCECAO, "[ulms]Item de controle nulo");
							}
						
							
							//x.x.toString(),
							//x.y
							
							mensagem = enviandoDadosDaSincronizacaoDoMobileParaWeb(
									context, 
									false, 
									log,
									true, 
									item);
							if (mensagem == null) {
								mensagem = Mensagem.factoryMsgSemInternet(context);
							} 
						} catch (OmegaDatabaseException ode){
							SingletonLog.insereWarning(ode, SingletonLog.TIPO.SINCRONIZADOR);
							return Mensagem.factoryBancoOmegaErro(context);
						} catch (Exception ex) {
							SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);
							
							return new Mensagem(ex);
						} finally {
								
								if(!realizouAutenticacaoAutomatica){
//									PrincipalActivity.logout(context);
									OmegaSecurity.clearCorporacaoSincronizadaTemporariamente();
								}
								if (db != null)
									db.close();
							
								InterfaceMensagem msgRotinaException = rotinaSalvarItem();
								if (msgRotinaException != null)
									return msgRotinaException;
							
						}
						
						return mensagem;
						
					} finally{
						lock.unlock();
					}
				}	
			}			 
			return new Mensagem(
					PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO, 
					"Nuo foi possuvel sincronizar os dados que ainda nuo foram enviados ao servidor.");
		} catch (InterruptedException e) {
			return new Mensagem(
				PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO, 
				context.getString(R.string.sincronizacao_em_andamento));
		}
	}
	public InterfaceMensagem executa(boolean pIsToSincronizarManualmente, boolean somenteEnvieOsDados) {
		if (!OmegaSecurity.isAutenticacaoRealizada())
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO_A_AREA_LOGADA);
		if ( lock.tryLock()) {			
			Database db = null;
			try {
				db = new DatabasePontoEletronico(context);
				if(!Database.isDatabaseInitialized(context) ){
					return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO, "A sincronizacao ju estu sendo realizada");
				}
				obj = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(db);
				objSRH = new EXTDAOSistemaRegistroHistorico(db);

				EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.initSequences(context);
				if(!OmegaSecurity.isAutenticacaoRealizada())
					return Mensagem.factoryAreaRestrita(context);
				InterfaceMensagem msgMobileIdentificador = verificaMobileIdentificador();
				if (msgMobileIdentificador.mCodRetorno != TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId())
					return msgMobileIdentificador;
				if (item == null)
					item = getItem(context, log);
				if (item == null) {
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("[lkmsfgioepokrtopker]Item nulo");
					return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_SEM_SER_EXCECAO, "[ulms]Item de controle nulo");
				}
				InterfaceMensagem mensagem = null;
				ProtocoloStatusSincronizacao protocolo = null;
				// item.estado =
				// ESTADO_SINCRONIZACAO_MOBILE.AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE;
				// item.salvar(context);

				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog(item.getIdentificador());

				boolean loop = true;
				while (loop) {
					if (!OmegaSecurity.isAutenticacaoRealizada())
						return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO_A_AREA_LOGADA);

					loop = false;
					switch (item.estado) {
					case ACABOU_DE_INICIAR_O_BANCO:
						item.appendMensageDialogSincronizador( "Acabou de iniciar o App!");
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("INICIO: " + item.estado.toString());
						iTentativa.set(0);
						mensagem = this.servWeb
								.mobileResetado(context,
										item.idUltimaSincronizacao != null && item.idUltimaSincronizacao > 0
												? String.valueOf(item.idUltimaSincronizacao) : null,
										item.ultimaMigracao);
						
						if (mensagem != null ) {
							if(mensagem.ok()){
								item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.PRIMEIRA_VEZ);
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Mobile resetado - novo estado: " + item.estado.toString());
							}  else if (mensagem.estruturaMigrada()) {
								item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.ESTRUTURA_MIGRADA);
								break;
							}else {
								return mensagem;
							}
						} else {

							break;
						}
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("FIM: " + item.estado.toString());
					case FINALIZADO:
					case MOBILE_ESTA_ENVIANDO_O_ARQUIVO_DE_SINCRONIZACAO:
					case PRIMEIRA_VEZ:
						item.appendMensageDialogSincronizador("Envia requisiuuo para iniciar sincronizacao...");
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("INICIO: " + item.estado.toString());

						ESTADO_SINCRONIZADOR.sendServiceStateMessage(context, ESTADO_SINCRONIZADOR.enviando);

						mensagem = enviandoDadosDaSincronizacaoDoMobileParaWeb(
								context, 
								item.primeiraSincronizacao, 
								log,
								somenteEnvieOsDados, 
								item);
						if (mensagem == null) {
							mensagem = new Mensagem(PROTOCOLO_SISTEMA.TIPO.SEM_CONEXAO_A_INTERNET,
									"[lknsgf]Mensagem nula");
							break;
						} else if (mensagem.emManutencao()) {
							break;
						} else if (mensagem.erro()) {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
								log.escreveLog(mensagem.getIdentificador());
								log.escreveLog("FIM: " + item.estado.toString());
							}
							break;
						} else if (mensagem.estruturaMigrada()) {
							item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.ESTRUTURA_MIGRADA);
							break;
						} else {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO_VERBOSO) {
								String nomeArquivo = "bancosqlite_" + OmegaSecurity.getCorporacao();
								nomeArquivo += "_" + item.idSincronizacaoAtual + "_" + item.idSincronizacaoMobile
										+ ".sql";
								BOOperacaoDownloadSQLiteMobile.processaAvulso(context, nomeArquivo);
							}
							if (somenteEnvieOsDados && (mensagem.ok() || mensagem.resultadoVazio())) {
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Sincronizacao finalizada pois su u para enviar dados");
								mensagem = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
										"O telefone enviou os para sincronizacao. A sincronizacao foi finalizada, pois o modo de entrada somenteEnvio estu setado.");
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
									log.escreveLog(mensagem.getIdentificador());
									log.escreveLog("FIM: " + item.estado.toString());
								}
								item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.FINALIZANDO);
								break;
							} else if (mensagem.ok() 
									|| mensagem.resultadoVazio()
									|| mensagem.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.PROCESSO_EM_ABERTO.getId()) {
								String token = "O telefone enviou os para sincronizacao.";
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog(token);
								mensagem = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_INCOMPLETA, token);
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
									log.escreveLog(mensagem.getIdentificador());
									log.escreveLog("FIM: " + item.estado.toString());
								}
								item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.FILA_ESPERA_PARA_SINCRONIZACAO);

								break;
							} else {
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
									log.escreveLog(mensagem.getIdentificador());
									log.escreveLog("FIM: " + item.estado.toString());
								}
								break;
							}

						}

					case FILA_ESPERA_PARA_SINCRONIZACAO:
					case INSERINDO_DADOS_DO_ARQUIVO_MOBILE_NO_BANCO_ESPELHO:
					case PROCESSANDO_DADOS_DO_BANCO_ESPELHO_NO_BANCO_WEB:
						item.appendMensageDialogSincronizador("Aguardando processamento da sincronizacao...");
						
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("INICIO: " + item.estado.toString());
						ESTADO_SINCRONIZADOR.sendServiceStateMessage(context, ESTADO_SINCRONIZADOR.parado);

						if (!validaEstadoItemParaFilaEspera(item)) {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("[23okn]O estado do item era invulido para atual situauuo. Estado: "
										+ item.estado);
							item.estado = ESTADO_SINCRONIZACAO_MOBILE.FINALIZADO;
							item.salvar(context);

							return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_SEM_SER_EXCECAO,
									"O estado do item era invulido para atual situauuo");
						}

						if (item.estado != ESTADO_SINCRONIZACAO_MOBILE.AGUARDANDO_PROCESSAMENTO_DO_ARQUIVO_WEB_NO_MOBILE) {
							protocolo = servWeb.statusSincronizacaoMobile(item, log);
							if (protocolo == null) {
								mensagem = new Mensagem(PROTOCOLO_SISTEMA.TIPO.SEM_CONEXAO_A_INTERNET, "O telefone estu desconectado da internet.");
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
									log.escreveLog(mensagem.getIdentificador());
									log.escreveLog("FIM: " + item.estado.toString());
								}
								break;
							}
						}
						if (protocolo.estadoSincronizacaoMobile == ESTADO_SINCRONIZACAO_MOBILE.OCORREU_UM_ERRO_FATAL) {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
								log.escreveLog("Protocolo ocorreu um erro fatal");

								log.escreveLog("FIM: " + item.estado.toString());
							}
							item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.SETANDO_ERRO_FATAL_NA_WEB);
							break;
						} else if (protocolo.estadoSincronizacaoMobile == ESTADO_SINCRONIZACAO_MOBILE.ESTRUTURA_MIGRADA) {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
								log.escreveLog("Sincronizacao finalizada pois o banco de dados foi migrado");

								log.escreveLog("FIM: " + item.estado.toString());
							}

							item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.ESTRUTURA_MIGRADA);
							break;
						} else if (protocolo.estadoSincronizacaoMobile == ESTADO_SINCRONIZACAO_MOBILE.RESETADO) {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
								log.escreveLog("Sincronizacao finalizada pois o mobile foi resetado");

								log.escreveLog("FIM: " + item.estado.toString());
							}

							item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.FINALIZADO);
							break;
						} else if (protocolo.estadoSincronizacaoMobile == ESTADO_SINCRONIZACAO_MOBILE.OCORREU_UM_ERRO_LEVE_QUE_SO_ENVIARA_OS_DADOS_LOCAIS_QUANDO_RESETAR) {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
								log.escreveLog("Sincronizacao finalizada pois ocorreu um erro fatal");

								log.escreveLog("FIM: " + item.estado.toString());
							}

							item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.SETANDO_ERRO_FATAL_NA_WEB);
							mensagem = getMensagemErroFatal();
							break;
						} else if (protocolo.estadoSincronizacaoMobile == null
								|| protocolo.estadoSincronizacaoMobile == ESTADO_SINCRONIZACAO_MOBILE.FILA_ESPERA_PARA_SINCRONIZACAO) {
							Boolean isPrimeiraVez  = protocolo.getBooleanAtributo(ProtocoloStatusSincronizacao.ATRIBUTO.isPrimeiraVez);
							isPrimeiraVez = isPrimeiraVez==null ? false : isPrimeiraVez;
							if ((item.enviouDadosSincronizacaoMobile == null || !item.enviouDadosSincronizacaoMobile)
									&& (!isPrimeiraVez  || iTentativa.addAndGet(1) >= MAX_TENTATIVA)) {
								iTentativa.set(0);
								// String ultimo =
								// obj.getUltimoId(obj.getIdDoUltimoSistemaRegistroSincronizadorAndroidParaWeb());
								String ultimo = obj.getUltimoId(null);
								// Se o sincronizador mobile possui dados para serem
								// sincronizados e a ultima sincronizacao nao enviou
								// dado algum entao o mobile tenta sair da fila de espera para
								// enviar o arquivo
								if (ultimo != null
								// && !item.primeiraSincronizacao
								) {
									if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
										log.escreveLog(
												"Novos dados apareceram para o mobile enviar para web. Retirando o mobile da fila de espera. "
														+ ultimo);

									mensagem = retiraMobileDaFilaDeEspera(protocolo, item.primeiraSincronizacao,
											somenteEnvieOsDados, item);
									if (mensagem.ok()) {
										// item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.MOBILE_ESTA_ENVIANDO_O_ARQUIVO_DE_SINCRONIZACAO);
										mensagem = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_INCOMPLETA,
												"O telefone enviou os dados para sincronizacao.");
									}
								} else if (somenteEnvieOsDados) {
									if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
										log.escreveLog("Sincronizacao finalizada pois su u para enviar dados");
									mensagem = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
											"O telefone enviou os para sincronizacao. A sincronizacao foi finalizada, pois o modo de entrada somenteEnvio estu setado.");
								}
							}
						} else if (protocolo.estadoSincronizacaoMobile == ESTADO_SINCRONIZACAO_MOBILE.AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE) {
							item.setEstado(protocolo.estadoSincronizacaoMobile);
							item.idSincronizacaoAtual = HelperInteger.parserLong(
									protocolo.getAtributo(ProtocoloStatusSincronizacao.ATRIBUTO.idSincronizacaoAtual));
							item.idSincronizacaoMobile = HelperInteger.parserLong(protocolo
									.getAtributo(ProtocoloStatusSincronizacao.ATRIBUTO.idSincronizacaoMobileAtual));
							item.idsSincronizacao = protocolo.idsSincronizacao;
							String attr = protocolo
									.getAtributo(ProtocoloStatusSincronizacao.ATRIBUTO.possuiArquivoCrudWebParaMobile);
							if (attr != null && attr.equalsIgnoreCase("true")) {
								item.possuiArquivoCrudWebParaMobile = true;
							} else
								item.possuiArquivoCrudWebParaMobile = false;
							item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.DOWNLOAD_DOS_DADOS_DE_SINCRONIZACAO);
							item.registraNovaTentativaFalhaDeDownloadDeArquivoDeSincronizacao();
							item.salvar(context);
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("Download dos arquivos de sincronizacao. Atual Item: "
										+ item.getConteudoJson(context));

							if (protocolo.estadoSincronizacao != null
									&& protocolo.estadoSincronizacao == ESTADO_SINCRONIZACAO.FINALIZADO) {
								final String token = "Sincronizacao finalizada por parte da web.";
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog(token);
								mensagem = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, token);
							} else {
								final String token = "Arquivos de sincronizacao da web estuo disponuveis para download. Aguarde...";
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog(token);
								mensagem = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_INCOMPLETA, token);
							}
						} else {
							if (protocolo.estadoSincronizacaoMobile != null) {
								item.setEstado(protocolo.estadoSincronizacaoMobile);
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog(
											"Novo estado: " + (item.estado == null ? "-" : item.estado.toString()));
							} else {
								final String token = "Nuo existem dados para sincronizar.";
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog(token);
								mensagem = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, token);
							}
						}

						if (!protocolo.possuiArquivoCrudMobile() && !protocolo.possuiArquivoCrudWebParaMobile()
								&& protocolo.estadoSincronizacaoMobile == ESTADO_SINCRONIZACAO_MOBILE.FILA_ESPERA_PARA_SINCRONIZACAO) {
							final String token = "Nuo existem dados a serem sincronizados no momento.";
							mensagem = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, token);
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog(token);
						}

						if (mensagem == null) {
							if (protocolo.msg != null && protocolo.msg.length() > 0)
								mensagem = new Mensagem(PROTOCOLO_SISTEMA.TIPO.AGUARDANDO, protocolo.msg);
							else
								mensagem = new Mensagem(PROTOCOLO_SISTEMA.TIPO.AGUARDANDO,
										"Aguardando na fila de espera do sincronizador");

						}
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
							log.escreveLog(mensagem.getIdentificador());
							log.escreveLog("FIM ITERACAO: " + item.estado.toString());
						}
						break;
					case AGUARDANDO_DOWNLOAD_DOS_DADOS_PELO_MOBILE:
					case DOWNLOAD_DOS_DADOS_DE_SINCRONIZACAO:
						item.appendMensageDialogSincronizador("Aguardando download do mobile dos dados processados...");
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("INICIO: " + item.getIdentificador());
						mensagem = procedimentoDownloadDadosDeSincronizacao(item, true);
						if(mensagem.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ERRO_ARQUIVO_SINCRONIZACAO_INEXISTENTE.getId()
								|| mensagem.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ERRO_SINCRONIZACAO.getId()){
							item.estado = ESTADO_SINCRONIZACAO_MOBILE.SETANDO_ERRO_FATAL_NA_WEB;
							break;
						}else if(mensagem.mCodRetorno==PROTOCOLO_SISTEMA.TIPO.SINCRONIZACAO_JA_FINALIZADA_PELO_MOBILE.getId()){
							item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.FINALIZADO);
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
								log.escreveLog("Sincronizacao ja havia sido finalizada pelo mobile");

								log.escreveLog("FIM: " + item.estado.toString());
							}
							
						}
						else if (mensagem.estruturaMigrada()) {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
								log.escreveLog("Sincronizacao finalizada pois o mobile foi resetado");

								log.escreveLog("FIM: " + item.estado.toString());
							}
							item.estado = ESTADO_SINCRONIZACAO_MOBILE.ESTRUTURA_MIGRADA;
														
							break;
						} else if (!mensagem.ok()) {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
								log.escreveLog(mensagem.getIdentificador());
								log.escreveLog("FIM ITERACAO: " + item.estado.toString());
							}
							break;
						} else // continua o fluxo
							item.salvar(context);

					case AGUARDANDO_PROCESSAMENTO_DO_ARQUIVO_WEB_NO_MOBILE:
						item.appendMensageDialogSincronizador("Aguardando processamento dos dados pelo mobile...");
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("INICIO: " + item.estado.toString());
						mensagem = processandoArquivoSincronizacao(item);
						if(mensagem.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ERRO_ARQUIVO_SINCRONIZACAO_INEXISTENTE.getId()
								|| mensagem.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ERRO_SINCRONIZACAO.getId()){
							item.estado = ESTADO_SINCRONIZACAO_MOBILE.SETANDO_ERRO_FATAL_NA_WEB;
							break;
						}
						else if(mensagem.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.SINCRONIZACAO_JA_FINALIZADA_PELO_MOBILE.getId()){
							item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.FINALIZADO);
							break;
						}
						else if (mensagem.estruturaMigrada()) {
							item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.ESTRUTURA_MIGRADA);
							break;
						} else if (mensagem.erroDownload()) {
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog(
									"[weokmeriug]Temos o erro de download de algum arquivo, iremos retornar os marcadores de download: "
											+ item.indiceUltimoDownloadSincronizacaoRealizada + "::"
											+ item.indicePrimeiroDownloadSincronizacaoRealizada);
							item.indiceUltimoDownloadSincronizacaoRealizada = item.indicePrimeiroDownloadSincronizacaoRealizada;
							item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.DOWNLOAD_DOS_DADOS_DE_SINCRONIZACAO);

							InterfaceMensagem msgRotinaException2 = rotinaSalvarItem();
							if (msgRotinaException2 != null)
								return msgRotinaException2;

							loop = true;

							HelperThread.sleep(OmegaConfiguration.SERVICE_SYNCH_TIMER_INTERVAL_ERRO_DOWNLOAD);
							break;
						} else if (mensagem.erro()) {

							if (item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb != null
									&& item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb >= 0) {
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("[oippp]removerTodasOsRegistrosMenoresOuIguais a "
											+ item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb.toString());

								objSRH.removerTodasOsRegistrosQueAcabaramDeSerSincronizados(
										item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb.toString(), log);

								obj.removerTodasOsRegistrosQueAcabaramDeSerSincronizados(
										item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb.toString(), log);

								// objSRH.removerRegistrosDaSincronizacao(String
								// .valueOf(item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb));
								// obj.removerTodasOsRegistrosMenoresOuIguais(String
								// .valueOf(item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb));
							} else {
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("[kjuhyukg654]Nuo removeu, segue a desc do item: "
											+ item.getIdentificador());
							}
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog(mensagem.mMensagem);
							// mensagem = new Mensagem(
							// PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
							// "Ocorreu um problema durante a sincronizacao,
							// realize o processo de RESETAR para retornar ao
							// normal. Sentimos muito.");
							item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.SETANDO_ERRO_FATAL_NA_WEB);
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("FIM ITERACAO: " + mensagem.getIdentificador());
							break;
						} else if (mensagem.ok() || mensagem.resultadoVazio()) {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog(mensagem.toJson());
							if (!item.downloadDaUltimaSincronizacaoRealizada()) {
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog(
											"[wert34p89jwf] Download da ultima sincronizacao ainda nao foi realizado");
								item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.DOWNLOAD_DOS_DADOS_DE_SINCRONIZACAO);

								InterfaceMensagem msgRotinaException3 = rotinaSalvarItem();
//								if (HelperHttp.isServerOnline(context))
									loop = true;
//								else
//									break;
								if (msgRotinaException3 != null)
									return msgRotinaException3;
								
								HelperThread.sleep(OmegaConfiguration.SERVICE_SYNCH_TIMER_INTERVAL_SUB_ROUTINE);
								break;
							}
							if (item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb != null
									&& item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb >= 0) {
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("[kuyfggf]removerTodasOsRegistrosMenoresOuIguais a "
											+ item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb.toString());
								objSRH.removerTodasOsRegistrosQueAcabaramDeSerSincronizados(
										String.valueOf(item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb), log);
								obj.removerTodasOsRegistrosQueAcabaramDeSerSincronizados(
										String.valueOf(item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb), log);

							} else {
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog(
											"removerTodasOsRegistrosMenoresOuIguais  - Nuo removeu, segue a desc do item: "
													+ item.getIdentificador());
							}

							// segue o fluxo
							item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.FINALIZANDO);
							mensagem = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_INCOMPLETA,
									"Finalizando o processo de sincronizacao.");
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
								log.escreveLog(mensagem.getIdentificador());
								log.escreveLog("FIM ITERACAO: " + item.estado.toString());
							}
						} else {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
								log.escreveLog(mensagem.getIdentificador());
								log.escreveLog("FIM ITERACAO: " + item.estado.toString());
							}
							break;
						}

					case FINALIZANDO:
						item.appendMensageDialogSincronizador("Finalizando ciclo da sincronizacao do mobile...");
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("INICIO: " + item.estado.toString());
						ESTADO_SINCRONIZADOR.sendServiceStateMessage(context, ESTADO_SINCRONIZADOR.parado);
						log.close();
						if (!ENVIAR_ARQUIVO_LOG_EM_CASO_DE_SUCESSO) {
							appendToLogCompleto(log);
						}

						Mensagem statusFinalizacao = servWeb.finalizaSincronizacaoMobile(context,
								item != null && item.idSincronizacaoMobile != null
										? item.idSincronizacaoMobile.toString() : null,
								log.getPathArquivo(), item.ultimaMigracao);
						if(statusFinalizacao != null){
							if (statusFinalizacao.resultadoVazio() || statusFinalizacao.ok()) {

								// Persistindo os dados
								ControladorParseSincronizacao controlador2 = new ControladorParseSincronizacao(context, log,
										item.idUltimoRegistroAntesDeGerarBanco, item.idUltimoRegistroDepoisDeGerarBanco,
										item.idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco,
										item.idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco,
										item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
										getIdSincronizacaoNoMomentoDaCriacaoDoBancoLocal(context),
										item.primeiraSincronizacao);

								controlador2.apagarDados();

								item.idUltimaSincronizacao = item.idSincronizacaoAtual;
								item.idSincronizacaoAtual = null;
								item.possuiArquivoCrudWebParaMobile = null;
								item.primeiraSincronizacao = false;
								item.idsSincronizacao = null;
								item.idSincronizacaoMobile = null;
								item.enviouDadosSincronizacaoMobile = null;
								item.contadorTentativasDownload = 0;
								item.indiceUltimoDownloadSincronizacaoRealizada = 0;
								item.indicePrimeiroDownloadSincronizacaoRealizada = 0;
								item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb = null;

								item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.FINALIZADO);
								
								appendToLogCompleto(log);
								mensagem = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
										"O processo de sincronizacao foi realizado com sucesso");
							} else {
								mensagem = statusFinalizacao;
								if (mensagem.estruturaMigrada()) {
									item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.ESTRUTURA_MIGRADA);
									break;
								}
							}
						}
						else {
							mensagem= Mensagem.factoryMsgSemInternet(context);
						}
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
							log.escreveLog(mensagem.getIdentificador());
							log.escreveLog("FIM: " + item.estado.toString());
						}
						break;

					case SETANDO_ERRO_FATAL_NA_WEB:
						item.appendMensageDialogSincronizador("Ocorreu um erro fatal, seru necessurio resertar o aplicativo!");
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("INICIO: " + item.estado.toString());

						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO_VERBOSO) {
							String nomeArquivo = "bancosqlite_" + OmegaSecurity.getCorporacao();
							nomeArquivo += "_" + item.idSincronizacaoAtual + "_" + item.idSincronizacaoMobile
									+ "_SETANDO_ERRO_FATAL_NA_WEB.sql";
							BOOperacaoDownloadSQLiteMobile.processaAvulso(context, nomeArquivo);
						}
						if (item.idSincronizacaoMobile != null) {
							log.close();
							Boolean status = servWeb.erroSincronizacaoMobile(
									context,
									item != null ? String.valueOf(item.idSincronizacaoAtual) : null,
									item != null ? String.valueOf(item.idSincronizacaoMobile) : null,
									log.getPathArquivo());
							if (status != null && status) {
								appendToLogCompleto(log);
								
								
								item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.OCORREU_UM_ERRO_FATAL);
							} else
								break;
						} else
							item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.OCORREU_UM_ERRO_FATAL);
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("FIM: " + item.estado.toString());
					case ESTRUTURA_MIGRADA:

					case OCORREU_UM_ERRO_FATAL:
					case CANCELADO:
					case RESETADO:
						if (item.estado == ESTADO_SINCRONIZACAO_MOBILE.ESTRUTURA_MIGRADA){
							mensagem = getMensagemEstruturaMigrada();
						}
						else if (item.estado == ESTADO_SINCRONIZACAO_MOBILE.OCORREU_UM_ERRO_FATAL){
							mensagem = getMensagemErroFatal();
						}else if(item.estado == ESTADO_SINCRONIZACAO_MOBILE.CANCELADO){
							mensagem = getMensagemCancelado();
						}
						else{
							mensagem = new Mensagem(
									PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
									"Devido a uma manutenuuo u resetar o aplicativo.");
						}
						
						item.appendMensageDialogSincronizador(mensagem.mMensagem);
						PontoEletronicoSharedPreference.saveValor(context, TIPO_BOOLEAN.RESETAR_BANCO_DE_DADOS, true);

						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
							log.escreveLog(mensagem.getIdentificador());
							log.escreveLog( item.estado.toString());
						}
						item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.AGUARDANDO_USUARIO_RESETAR);

						break;
					case AGUARDANDO_USUARIO_RESETAR:
						mensagem = new Mensagem(
								PROTOCOLO_SISTEMA.TIPO.AGUARDANDO_USUARIO_RESETAR);
						break;					
						
					default:
						break;
					}

				}

				return mensagem;
			}catch (OmegaDatabaseException ode){
				SingletonLog.insereWarning(ode, SingletonLog.TIPO.SINCRONIZADOR);
				return Mensagem.factoryBancoOmegaErro(context);
			}
			catch (Exception ex) {
				SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("[34defger]executa", ex);
				return new Mensagem(ex);
			} finally {
				try {
					ReceiveBroadcastSincronizador.sendBroadCast(context, null);
					if (db != null)
						db.close();
					try {
						log.close();
					} catch (Exception ex) {
						SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("[lkndfgf3awer5]executa::", ex);
					}
					InterfaceMensagem msgRotinaException = rotinaSalvarItem();
					if (msgRotinaException != null)
						return msgRotinaException;
				} finally {
					lock.unlock();
				}
			}

		} else {
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO, "A sincronizacao ju estu sendo realizada");
		}
	}
	
	private void appendToLogCompleto(OmegaLog log){
		if(log != null && OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
			
			HelperFile.appendLogFileToMe(log.getPathArquivo(), ARQUIVO_LOG_COMPLETO());
			log.delete();
			log=new OmegaLog(
				ARQUIVO_LOG(), 
				OmegaFileConfiguration.TIPO.LOG,
				OmegaConfiguration.DEBUGGING_SINCRONIZACAO,
				this.context);
		}
	}
	private InterfaceMensagem rotinaSalvarItem() {
		try {
			if (item != null)
				item.salvar(context);
			return null;
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[lkndfgf35]executa::", ex);

			return new Mensagem(ex);
		}
	}

	public InterfaceMensagem procedimentoDownloadDadosDeSincronizacao(Item item, boolean iniciaNovaSincronizacao)
			throws JSONException {
		
		if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("procedimentoDownloadDadosDeSincronizacao INICIO: " + item.getIdentificador());
		try{
			InterfaceMensagem mensagem = recebendoDadosSincronizacao(item, log, iniciaNovaSincronizacao);
			if (mensagem.ok()) {
				if (iniciaNovaSincronizacao) {
					// Persistindo os dados
					ControladorParseSincronizacao controlador2 = new ControladorParseSincronizacao(
							context, 
							log,
							item.idUltimoRegistroAntesDeGerarBanco, 
							item.idUltimoRegistroDepoisDeGerarBanco,
							item.idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco,
							item.idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco,
							item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
							getIdSincronizacaoNoMomentoDaCriacaoDoBancoLocal(context), 
							item.primeiraSincronizacao);
					controlador2.iniciarNovaSincronizacao(item.idSincronizacaoMobile);

					item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.AGUARDANDO_PROCESSAMENTO_DO_ARQUIVO_WEB_NO_MOBILE);
					item.salvar(context);
				}
				final String token = "O processamento dos dados recebidos da sincronizacao acontecera em alguns instantes";

				mensagem = new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, token);
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
					log.escreveLog(token);
					log.escreveLog(mensagem.getIdentificador());
					log.escreveLog("FIM: " + item.estado.toString());
				}

			} else {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
					log.escreveLog(mensagem.getIdentificador());
					log.escreveLog("FIM: " + item.estado.toString());
				}
			}

			return mensagem;
		}catch(OmegaDatabaseException ode){
			SingletonLog.factoryToast(this.context, ode, SingletonLog.TIPO.BANCO);
			return Mensagem.factoryBancoOmegaErro(context);
		}
		
	}

	static HelperDate ultimaRetirada = null;

	public InterfaceMensagem retiraMobileDaFilaDeEspera(ProtocoloStatusSincronizacao protocolo,
			boolean primeiraSincronizacaoDaVida, boolean somenteEnviandoDados, Item item) {
		try {
			if (ultimaRetirada != null && ultimaRetirada.getAbsDiferencaEmSegundos(
					new Date()) < OmegaConfiguration.LOAD_RETIRADA_MOBILE_DA_FILA_ESPERA_INTERVAL_SEGUNDOS) {
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO,
						"Nuo completou ainda o tempo munimo de espera para retirar o mobile da" + " fila de espera: "
								+ OmegaConfiguration.LOAD_RETIRADA_MOBILE_DA_FILA_ESPERA_INTERVAL_SEGUNDOS
								+ " segundos");
			}

			ultimaRetirada = new HelperDate();

			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Retirando mobile da fila de espera");
			if(primeiraSincronizacaoDaVida){

				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO,
						"Nuo foi possuvel retirar o mobile da fila de espera pois u a primeira sincronizacao de todas.");
			}
			
			Boolean retirado = servWeb.retiraMobileDaFilaDeEspera(context, item.idSincronizacaoMobile, log);
			if (retirado == null) {

				return Mensagem.factoryServidorSobrecarregado(context);
			} else {
				if (retirado) {

					InterfaceMensagem msg = enviandoDadosDaSincronizacaoDoMobileParaWeb(context,
							primeiraSincronizacaoDaVida, log, somenteEnviandoDados, item);

					return msg;
				} else {

					return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO,
							"Nuo foi possuvel retirar o mobile da fila de espera para enviar os dados de sincronizacao, teru que esperar a pruxima.");
				}
			}

		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[34r]retiraMobileDaFilaDeEspera", ex);
			return new Mensagem(ex);
		}
	}

	public InterfaceMensagem recebendoDadosSincronizacao(Item item, OmegaLog log, boolean iniciaNovaSincronizacao) {
		try {
			InterfaceMensagem msg = null;

			ArrayList<Long> idsSincronizacaoNaoVazia = new ArrayList<Long>();
			item.possuiArquivoCrudWebParaMobile = false;
			// if(item.idsSincronizacao != null){
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("recebendoDadosSincronizacao: inicia nova sincronizacao? " + iniciaNovaSincronizacao);
			if (iniciaNovaSincronizacao) {

				if (item.indiceUltimoDownloadSincronizacaoRealizada == null)
					item.indiceUltimoDownloadSincronizacaoRealizada = 0;
				item.indicePrimeiroDownloadSincronizacaoRealizada = item.indiceUltimoDownloadSincronizacaoRealizada;
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("[d23rf15]indicePrimeiroDownloadSincronizacaoRealizada: "
							+ item.indicePrimeiroDownloadSincronizacaoRealizada);

				for (int i = item.indicePrimeiroDownloadSincronizacaoRealizada; i < item.idsSincronizacao.length
						&& idsSincronizacaoNaoVazia
								.size() < ItemParseSincronizacao.LIMITE_ARQUIVO_SINCROINACAO_POR_ITERACAO; i++) {

					Long idSinc = item.idsSincronizacao[i];
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("[wevgw456sdfg]Id sincronizacao: " + idSinc);
					if (idSinc > 0) {

						msg = recebendoDadosSincronizacao(context, idSinc, item.idSincronizacaoMobile,
								item.ultimaMigracao);
						if(msg == null)
							return getMensagemCancelado();
						if (msg.resultadoVazio()) {
							item.indiceUltimoDownloadSincronizacaoRealizada++;
							// nao faz nada
						} else if (!msg.ok()) {
							
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("Falha durante o download do arquivo de sincronizacao "
										+ HelperString.formatarParaImpressao(idSinc));
							return msg;
						} else  {
							item.possuiArquivoCrudWebParaMobile = true;
							item.indiceUltimoDownloadSincronizacaoRealizada++;
							idsSincronizacaoNaoVazia.add(idSinc);
						}
					} else {
						item.indiceUltimoDownloadSincronizacaoRealizada++;
					}
				}
			} else {
				for (int i = 0; i < item.idsSincronizacaoIteracao.length; i++) {

					Long idSinc = item.idsSincronizacaoIteracao[i];
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("[q3v45we5y4]Id sincronizacao: " + idSinc);
					if (idSinc > 0) {
						msg = recebendoDadosSincronizacao(context, idSinc, item.idSincronizacaoMobile,
								item.ultimaMigracao);
						if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO.getId()) {

							// nao faz nada
						} else if (!msg.ok()) {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("Falha durante o download do arquivo de sincronizacao "
										+ HelperString.formatarParaImpressao(idSinc));
							return msg;
						} else if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {
							item.possuiArquivoCrudWebParaMobile = true;
							idsSincronizacaoNaoVazia.add(idSinc);
						}
					}
				}
			}

			item.idsSincronizacaoIteracao = new Long[idsSincronizacaoNaoVazia.size()];
			idsSincronizacaoNaoVazia.toArray(item.idsSincronizacaoIteracao);

			if (item.downloadDaUltimaSincronizacaoRealizada()) {
				msg = recebendoDadosSincronizacaoMobile(context, item.idSincronizacaoMobile.toString(),
						item.ultimaMigracao);

				if (msg != null) {
					if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO.getId()) {
						item.possuiArquivoCrudWebParaMobile = false;
					} else if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Dados da sincronizacao mobile recebidos com sucesso.");
						return msg;
					} else {
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Falha durante o download da sincronizacao mobile. Msg: " + msg.mMensagem);
						return msg;
					}
				} else {
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog(
								"Falha ao receber os dados relacionados a sincronizacao mobile - Servidor fora do ar.");
				}
			}

			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
					"Download dos dados da sincronizacao web foi realizado com sucesso.");

		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[lknsfdjk]RecebendoDadosSincronizacao", ex);
			return new Mensagem(ex);
		}
	}

	public InterfaceMensagem processandoArquivoSincronizacao(Item item) {
		// /storage/emulated/0/PontoEletronico/SendFoto/sincronizacao_mobile.zip
		try {

			ControladorParseSincronizacao controlador = new ControladorParseSincronizacao(context, log,
					item.idUltimoRegistroAntesDeGerarBanco, item.idUltimoRegistroDepoisDeGerarBanco,
					item.idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco,
					item.idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco,
					item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb,
					getIdSincronizacaoNoMomentoDaCriacaoDoBancoLocal(context), item.primeiraSincronizacao);

			controlador.carregarDaMemoria(item.idSincronizacaoAtual, item.idsSincronizacaoIteracao,
					item.idSincronizacaoMobile, item.downloadDaUltimaSincronizacaoRealizada());

			if (!controlador.checaArquivosDosParsers(item.downloadDaUltimaSincronizacaoRealizada())) {
				InterfaceMensagem mensagem = procedimentoDownloadDadosDeSincronizacao(item, false);
				if (mensagem == null || mensagem.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId()) {
					if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog(
							"Temos o erro de download de algum arquivo, iremos retornar os marcadores de download: "
									+ item.indiceUltimoDownloadSincronizacaoRealizada + "::"
									+ item.indicePrimeiroDownloadSincronizacaoRealizada);
					item.indiceUltimoDownloadSincronizacaoRealizada = item.indicePrimeiroDownloadSincronizacaoRealizada;
					item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.DOWNLOAD_DOS_DADOS_DE_SINCRONIZACAO);
					item.salvar(context);
					return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_DOWNLOAD,
							"Os arquivos de sincronizacao foram deletados e u necessurio realizar o download deles novamente para continuar.");
				}else if(!mensagem.ok() && !mensagem.resultadoVazio()){
					return mensagem; 
				} 
			}

			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Iniciando parse da sincronizacao: " + item.getIdentificador()
						+ ". Mobile Identificador: " + mobileIdentificador);

			// EXECUTANDO O PARSER ...
			InterfaceMensagem msgParser = controlador.parse(HelperInteger.parserInteger(mobileIdentificador));
			// if(msgParser != null && msgParser.ok()){
			// controlador.apagaArquivosDaIteracao();
			// }
			String strMsgParser = msgParser == null ? "null" : msgParser.toJson();
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[kmeri]Resultado parser: " + strMsgParser);
			return msgParser;

		} catch (Exception e) {
			SingletonLog.insereErro(e, SingletonLog.TIPO.SINCRONIZADOR);
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[lkns234fdjk]processandoArquivoSincronizacao", e);
			return new Mensagem(e);
		}
	}

	// /So pode existir um arquivo sincronizacao_mobile no celular
	public InterfaceMensagem recebendoDadosSincronizacaoMobile(Context context, String idSincronizacaoMobile,
			String ultimaMigracao) {

		File f = ParseSincronizacaoMobile.getFileSincronizacaoMobile(idSincronizacaoMobile);
		if (f.exists()) {
			ValidadeArquivoJson validade =ParseJson.validaArquivoJson(f.getAbsolutePath());
			if (validade == null)
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO);
			else if (validade.validade || !ParseSincronizacaoMobile.validaArquivoSincronizacaoMobile(f.getAbsolutePath())) {
				f.delete();
			} else {
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
			}

		}

		ServicosSincronizadorWeb servWeb = new ServicosSincronizadorWeb(context, mobileConectado, mobileIdentificador);
		InterfaceMensagem validade = servWeb.downloadDadosSincronizacaoMobile(context, f.getPath(), idSincronizacaoMobile,
				ultimaMigracao);
		return validade;

	}

	

	// /So pode existir um arquivo sincronizacao no celular
	public InterfaceMensagem recebendoDadosSincronizacao(Context context, Long idSincronizacao,
			Long idSincronizacaoMobile, String ultimaMigracao) {
		File f = ParseSincronizacao.getFileSincronizacao(idSincronizacao);

		if (f.exists()) {
			Boolean v = ParseSincronizacao.validaArquivoSincronizacaoWeb(f.getAbsolutePath());
			if( v == null || !v){
				if(v != null )
					f.delete();
			} else {
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
			}
		}

		InterfaceMensagem msgAux = servWeb.downloadDadosSincronizacao(context, f.getPath(), idSincronizacao,
				idSincronizacaoMobile, ultimaMigracao);
		
		return msgAux;
	}

	// private void arquivarBancoAntesDeIniciarASincronizacao(){
	// Database db = new Database();
	// db.
	// }
	private InterfaceMensagem iniciaSincronizacaoMobileSemArquivo(
			Context context, 
			String primeiroId, 
			String ultimoId,
			boolean primeiraSincronizacaoDaVida, 
			OmegaLog log, 
			boolean somenteEnviandoDados, 
			Item item)
			throws JSONException {
		if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
			log.escreveLog("Mobile entrando na fila da sincronizacao sem enviar dados ...");
		InterfaceMensagem msg = servWeb.uploadDadosSincronizadorMobile(
				null, null, log, primeiroId, ultimoId,
				item.ultimaMigracao);
		
		if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.PROCESSO_EM_ABERTO.getId()) {

			MensagemProtocolo msgProtocolo = (MensagemProtocolo) msg;

			ProtocoloProcessoEmAberto protocolo = (ProtocoloProcessoEmAberto) msgProtocolo.mObj;

			//pode ocorrer da sincronizacao do mobile ja ter sido executada
			//AINDA NAO VALIDEI A TRECHO ABAIXO
//			Long novoIdSincronizacaoMobile = protocolo.getLongAtributo(ProtocoloProcessoEmAberto.ATRIBUTO.idSincronizacaoMobile);
//			if(item.primeiraSincronizacao  
//					&& item.idSincronizacaoMobile != null 
//					&& novoIdSincronizacaoMobile != item.idSincronizacaoMobile){
//				item.primeiraSincronizacao = false;
//			}
			item.idSincronizacaoMobile = protocolo
					.getLongAtributo(ProtocoloProcessoEmAberto.ATRIBUTO.idSincronizacaoMobile);
			
			item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb = protocolo.getLongAtributo(
					ProtocoloProcessoEmAberto.ATRIBUTO.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb);
			item.enviouDadosSincronizacaoMobile = protocolo
					.getBooleanAtributo(ProtocoloProcessoEmAberto.ATRIBUTO.enviouDadosSincronizacaoMobile);
			
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[Aqwe2] Ju existe um processo em aberto. Id da sincronizacao mobile: "
						+ item.idSincronizacaoMobile);

			return msg;
		} else if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {
			item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb = null;
			MensagemToken msgToken = (MensagemToken) msg;
			String strIdSincronizacaoMobile = msgToken.mValor;
			item.idSincronizacaoMobile = HelperInteger.parserLong(strIdSincronizacaoMobile);
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog(
						"O mobile entrou na sincronizacao mobile[" + strIdSincronizacaoMobile + "] sem enviar dados!");
			item.salvar(context);
			if (ultimoId == null)
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO.getId(),
						"O mobile entrou na fila de sincronizacao mobile [" + strIdSincronizacaoMobile
								+ "] e nuo tinha dados para sincronizar.");
			else
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO.getId(),
						"O mobile entrou na fila de sincronizacao mobile [" + strIdSincronizacaoMobile
								+ "] e nuo enviou os dados pois u o primeiro processo que estu participando.");
		} else
			return msg;
	}

	private int escreveDadosMobileParaWeb(
			String primeiroId, 
			String ultimoId, 
			String completePathFile,
			ArrayList<String> tabelasSincronizadas, 
			boolean somenteEnviandoDados) throws Exception {
		FileWriter fw = null;
		boolean imprimiuObjeto = false;
		int totalItensImpresso = 0;
		try {
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("Escrevendo dados do mobile para enviar para web. Ultimo Id: " + ultimoId
						+ ". Primeiro Id: " + primeiroId);
			obj.setEstadoSincronizando(ultimoId, true, log);
			long intUltimoId = HelperInteger.parserLong(ultimoId);
			
			fw = new FileWriter(completePathFile, false);
			

			fw.write("{\"ultimoIdWeb\":\"" + ultimoId + "\", \"crud_mobile\":[");
			int cont = 0;

			Database db = null;
			try {
				db = new DatabasePontoEletronico(context);
				for (String tabelaUpload : vetorTabelaUpload) {

					for (String tableToSync : tabelasSincronizadas) {

						if (tabelaUpload.equalsIgnoreCase(tableToSync)) {

							int auxTotalItensImpresso = gravaDadosDaTabelaMobileWeb(db, context,
									db.factoryTable(tabelaUpload), fw, imprimiuObjeto, tabelasSincronizadas,
									intUltimoId, somenteEnviandoDados);
							if (auxTotalItensImpresso > 0 && !imprimiuObjeto)
								imprimiuObjeto = true;
							totalItensImpresso += auxTotalItensImpresso;
							cont++;
							if (cont == tabelasSincronizadas.size())
								break;
						}
					}
					if (cont == tabelasSincronizadas.size())
						break;
				}
			} finally {
				if (db != null)
					db.close();
				db = null;
			}
			fw.write("]}");
		} finally {
			if (fw != null) {
				fw.flush();
				fw.close();
			}

			fw = null;
		}

		return totalItensImpresso;
	}
	
	public InterfaceMensagem enviandoDadosDaSincronizacaoDoMobileParaWeb(
			Context context,
			boolean primeiraSincronizacaoDaVida, 
			OmegaLog log,
			boolean resetando,
			Item item) {

		OmegaFileConfiguration ofc = new OmegaFileConfiguration();

		String completePathFile = null;
		try {
			String pathFile = ofc.getPath(OmegaFileConfiguration.TIPO.SINCRONIZACAO);
			String nome = HelperString.getRandomName("sincronizacao_") + ".out";
			completePathFile = pathFile + nome;

			String ultimoId = obj.getUltimoId(null);
			if(ultimoId == null && resetando)
				return new Mensagem(
						PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, 
						"Como somenteEnviaDados estava setado, e nuo temos dados de sincronizacao para ser enviado. Entuo paramos o processo, e consideramos que foi REALIZADO_COM_SUCESSO!");
			String primeiroId = obj.getPrimeiroId();

			int totalObjetoImpresso = 0;
			boolean imprimiuObjeto = false;
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("[lkmsgr]Ultimo id: " + (ultimoId == null ? "-" : ultimoId));
			// Se o telefone nao possui dados a serem enviados
			if (ultimoId == null || primeiraSincronizacaoDaVida // O MOBILE
																// NUNCA DEVERu
																// ENVIAR DADOS
																// NA PRIMEIRA
																// SINCRONIZACAO
																// - OUVIU???
																// NUNCA REITRA
																// ESSA REGRA
			) {
				//nunca chegaru aqui enquanto estiver resetando, pois ja retornou na condicional la em cima
				return iniciaSincronizacaoMobileSemArquivo(
						context, 
						primeiroId, 
						ultimoId, 
						primeiraSincronizacaoDaVida,
						log, 
						resetando, 
						item);
			} else {
				long intPrimeiroId = primeiroId == null ? 0 : HelperInteger.parserLong(primeiroId);

				long intUltimoId = HelperInteger.parserLong(ultimoId);
				ArrayList<String> tabelasSincronizadas = null;
				if (!resetando && intUltimoId - intPrimeiroId > limiteVariavelRegistrosEnviadosPorSincronizacao) {
					Long ultimoIdAtual = HelperLong.parserLong(
							obj.getUltimoId(
									null));
					Long novointUltimoId = obj.getUltimoIdParaEnvio(
							null,
							limiteVariavelRegistrosEnviadosPorSincronizacao);
					if (novointUltimoId == null)
						novointUltimoId = intUltimoId;

					while (novointUltimoId != null && novointUltimoId <= intUltimoId && !imprimiuObjeto) {

						boolean analiseLimiteDinamicoLigada = true;
						// se nuo tivermos tantos itens assim na fila de espera
						// nao vale o esforuo de ligar a alterauuo dinamica de
						// limite
						// logo verificamos se seruo necessurias 2
						// sincroniza~uues para sincronizar
						if (novointUltimoId + (LIMITE_REGISTROS_ENVIADOS_POR_SINCRONIZACAO * 2) > ultimoIdAtual) {
							analiseLimiteDinamicoLigada = false;
							limiteVariavelRegistrosEnviadosPorSincronizacao = LIMITE_REGISTROS_ENVIADOS_POR_SINCRONIZACAO;
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("[g3t6eh]Limite alterado para o original: "
									+ limiteVariavelRegistrosEnviadosPorSincronizacao);
						}
						for (; novointUltimoId != null && novointUltimoId <= intUltimoId && !imprimiuObjeto;

								novointUltimoId = obj.getUltimoIdParaEnvio(
										novointUltimoId,
										limiteVariavelRegistrosEnviadosPorSincronizacao)) {

							tabelasSincronizadas = obj.getListTableToSync(String.valueOf(novointUltimoId));

							if (tabelasSincronizadas == null || tabelasSincronizadas.size() == 0) {
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog(
											"[lkmsadfkm]Nuo existem tabelas a serem sincronizadas. Vai para o proximo ultimoIdInt");

							} else {
								if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
									log.escreveLog("Ultimo id alterado de " + intUltimoId + " para " + novointUltimoId);
								intUltimoId = novointUltimoId;
								ultimoId = String.valueOf(novointUltimoId);
								break;
							}
						}

						if (tabelasSincronizadas == null || tabelasSincronizadas.size() == 0) {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("Nuo existem tabelas a serem sincronizadas.");
							return new Mensagem(PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO,
									"Nuo existem tabelas a serem sincronizadas para a web.");
						} else {
							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("[atgq2345]Primeiro Id: " + primeiroId + ". Ultimo Id: " + intUltimoId);
							int auxTotalObjetoImpresso = escreveDadosMobileParaWeb(
									primeiroId, 
									ultimoId,
									completePathFile, 
									tabelasSincronizadas, 
									resetando);

							if (auxTotalObjetoImpresso > 0) {
								imprimiuObjeto = true;
								totalObjetoImpresso += auxTotalObjetoImpresso;
							}
							boolean cancelarEnvioArquivo = false;
							if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("[234rf34re] analiseLimiteDinamicoLigada: " + analiseLimiteDinamicoLigada);
							if (limiteVariavelRegistrosEnviadosPorSincronizacao > LIMITE_REGISTROS_ENVIADOS_POR_SINCRONIZACAO
									&& totalObjetoImpresso > LIMITE_REGISTROS_ENVIADOS_POR_SINCRONIZACAO) {

								limiteVariavelRegistrosEnviadosPorSincronizacao = LIMITE_REGISTROS_ENVIADOS_POR_SINCRONIZACAO;
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("[213edfzxcv]Limite alterado para o original: "
										+ limiteVariavelRegistrosEnviadosPorSincronizacao);
								cancelarEnvioArquivo = true;

							}

							if (!cancelarEnvioArquivo && analiseLimiteDinamicoLigada && novointUltimoId < ultimoIdAtual
									&& (double) (totalObjetoImpresso
											/ LIMITE_REGISTROS_ENVIADOS_POR_SINCRONIZACAO) < 0.3
									&& novointUltimoId
											+ (LIMITE_REGISTROS_ENVIADOS_POR_SINCRONIZACAO * 2) < ultimoIdAtual) {
								limiteVariavelRegistrosEnviadosPorSincronizacao *= 1.5;
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("[234rf]Limite expandido para a proxima sincronizacao: "
										+ limiteVariavelRegistrosEnviadosPorSincronizacao);
							}
							if (cancelarEnvioArquivo) {
								if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("[234rf] Iteracao cancelada: "
										+ limiteVariavelRegistrosEnviadosPorSincronizacao);
								// volta a iteracao para gerar o arquivo
								// novamente
								novointUltimoId = obj.getUltimoIdParaEnvio(null,
										limiteVariavelRegistrosEnviadosPorSincronizacao);
								HelperFile.removeFileIfExists(completePathFile);
								obj.cancelaTodosOsRegistrosQueEstaoSincronizando();
								imprimiuObjeto = false;
								totalObjetoImpresso = 0;
								continue;
							}

							if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
								log.escreveLog("[atgq2345]Imprimiu Objeto: " + imprimiuObjeto);
						}

					}

				} else {
					tabelasSincronizadas = obj.getListTableToSync(ultimoId);
					if (tabelasSincronizadas == null || tabelasSincronizadas.size() == 0) {
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Nuo existem tabelas a serem sincronizadas.");
						return new Mensagem(PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO,
								"Nuo existem tabelas a serem sincronizadas para a web.");
					}
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("[dfokmifsd]Primeiro Id: " + primeiroId + ". Ultimo Id: " + intUltimoId);

					int totalImpresso = escreveDadosMobileParaWeb(primeiroId, ultimoId, completePathFile,
							tabelasSincronizadas, resetando);
					if (totalImpresso > 0)
						imprimiuObjeto = true;
				}

				if (!imprimiuObjeto) {
					boolean validadeRemocao = obj.removerTodasOsRegistrosQueAcabaramDeSerSincronizados(ultimoId, log);
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
						log.escreveLog(
								"Os registros da lista de sincronizacao que foram analisados dever ser removidos pois ju foram analisados. Removendo ...");
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO){
						if (validadeRemocao)
							log.escreveLog("removido com sucesso da pilha do historico");
						else
							log.escreveLog("falha ao remover da pilha do historico");
						}
					}
					if(resetando){
						if(OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("Fim do processo reset! Sucesso!");
						return new Mensagem(
								PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
								"Nuo havia dados para serem sincronizados");
					}else
						return iniciaSincronizacaoMobileSemArquivo(context, primeiroId, ultimoId,
							primeiraSincronizacaoDaVida, log, resetando, item);
				}
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("Enviando dados ...");
				if (resetando) {
					verificaMobileIdentificador();
					InterfaceMensagem msg = servWeb.uploadDadosSincronizadorMobileAntesDoReset(
							completePathFile, 
							resetando, 
							log,
							primeiroId, 
							ultimoId, 
							item.ultimaMigracao);
					if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
						log.escreveLog("somenteEnviandoDados::removerTodasOsRegistrosMenoresOuIguais[kuytychjkj] a "
								+ ultimoId.toString());
					if(msg != null && msg.ok()){
						objSRH.removerTodasOsRegistrosQueAcabaramDeSerSincronizados(ultimoId, log);
						obj.removerTodasOsRegistrosQueAcabaramDeSerSincronizados(ultimoId, log);	
					}
					return msg;

				} else {
					InterfaceMensagem msg = servWeb.uploadDadosSincronizadorMobile(
							completePathFile, 
							resetando, 
							log,
							primeiroId, 
							ultimoId, 
							item.ultimaMigracao);

					if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.PROCESSO_EM_ABERTO.getId()) {

						MensagemProtocolo msgProtocolo = (MensagemProtocolo) msg;
						ProtocoloProcessoEmAberto protocolo = (ProtocoloProcessoEmAberto) msgProtocolo.getProtocolo();

						item.idSincronizacaoMobile = protocolo
								.getLongAtributo(ProtocoloProcessoEmAberto.ATRIBUTO.idSincronizacaoMobile);
						item.enviouDadosSincronizacaoMobile = protocolo
								.getBooleanAtributo(ProtocoloProcessoEmAberto.ATRIBUTO.enviouDadosSincronizacaoMobile);
						item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb = protocolo.getLongAtributo(
								ProtocoloProcessoEmAberto.ATRIBUTO.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb);
						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("[A] Ju existe um processo em aberto. Id da sincronizacao mobile: "
									+ item.idSincronizacaoMobile);
						return msg;
					} else if (msg.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {
						MensagemToken msgToken = (MensagemToken) msg;
						String strIdSincronizacaoMobile = msgToken.mValor;
						if (resetando) 
							item.estado = ESTADO_SINCRONIZACAO_MOBILE.RESETADO;
						else
							item.estado = ESTADO_SINCRONIZACAO_MOBILE.FILA_ESPERA_PARA_SINCRONIZACAO;
						item.idDoUltimoSistemaRegistroSincronizadorAndroidParaWeb = HelperInteger.parserLong(ultimoId);
						item.idSincronizacaoMobile = HelperInteger.parserLong(strIdSincronizacaoMobile);
						item.enviouDadosSincronizacaoMobile = true;
						item.salvar(context);

						if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
							log.escreveLog("Dados enviados com sucesso.");

						

						return new Mensagem((int) PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId(),
								"O mobile entrou na fila de sincronizacao e enviou dados para sincronizar.");
					} else
						return msg;
				}
				
			}

		}
		catch (OmegaDatabaseException ode){
			SingletonLog.insereWarning(ode, SingletonLog.TIPO.SINCRONIZADOR);
			return Mensagem.factoryBancoOmegaErro(context);
		}
		catch (Exception ex) {

			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO) {
				log.escreveLog("[kmldf]enviandoDadosDaSincronizacaoDoMobileParaWeb", ex);
				String msgEx = ex.getMessage();
				if (msgEx != null && msgEx.contains("database disk image is malformed")) {
					Database db2 = null;
					try{
						db2 = new DatabasePontoEletronico(context);
						Cursor c = null;
						try {
							db2 = new DatabasePontoEletronico(context);
							c = db2.rawQuery("pragma integrity_check", null);

							StringBuilder sb = new StringBuilder();
							if (c.moveToFirst()) {
								String valor = c.getString(0);

								sb.append(valor);
								while (c.moveToNext()) {
									sb.append(valor);
								}
							}
							Log.i("SincronizadorEspelho", sb.toString());
						} finally {
							if (c != null)
								c.close();
						}
					}catch(Exception ex2){
						SingletonLog.insereErro(ex2, SingletonLog.TIPO.SINCRONIZADOR);
					}
					

				}

			}
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SINCRONIZADOR);
			return new Mensagem(ex);
		} finally {

			HelperFile.removeFileIfExists(completePathFile);
			
		}
	}

	// retorna true se) imprimiu alguma coisa
	@SuppressLint("UseSparseArrays")
	public int gravaDadosDaTabelaMobileWeb(Database db, Context c, Table table, FileWriter fw, boolean imprimiuObjeto,
			ArrayList<String> tabelasASeremSincronizadas, Long ultimoIdSincronizador, boolean somenteEnviandoDados)
			throws Exception {
		int totalImpresso = 0;
		HashMap<Integer, Integer> idsInseridosPorSincronizador = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> idsInseridosPorMobileOriginal = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> idsRemovidosPorSincronizador = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> idsRemovidosPorMobileOriginal = new HashMap<Integer, Integer>();
		String idSistemaTabela = EXTDAOSistemaTabela.getIdSistemaTabela(db, table.getName());
		ContainerQueryEXTDAO containerRemove = obj.getContainerRemove(log, table.getName(), idSistemaTabela,
				idsRemovidosPorSincronizador, idsRemovidosPorMobileOriginal, ultimoIdSincronizador);

		ContainerQueryEXTDAO containerInsert = obj.getContainerInsertComDependetes(log, table, idSistemaTabela,
				idsInseridosPorMobileOriginal, idsInseridosPorSincronizador, idsRemovidosPorSincronizador,
				tabelasASeremSincronizadas, ultimoIdSincronizador, containerRemove, somenteEnviandoDados);

		ContainerQueryEXTDAO containerEdit = obj.getContainerEditComDependentes(log, table, idSistemaTabela,
				idsInseridosPorMobileOriginal, idsInseridosPorSincronizador, idsRemovidosPorMobileOriginal,
				idsRemovidosPorSincronizador, tabelasASeremSincronizadas, ultimoIdSincronizador, containerRemove,
				somenteEnviandoDados);

		if (containerRemove.isInitialized()) {

			ArrayList<String> ids = containerRemove.getIdsEXTDAO();
			ArrayList<String> idsSincronizador = containerRemove.getListIdEXTDAOSincronizador();
			PacoteRemove pacote = new PacoteRemove();
			pacote.tabela = table.getName();
			pacote.tipoOperacao = EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_REMOVE;

			pacote.ids = new String[ids.size()];
			ids.toArray((String[]) pacote.ids);
			pacote.idsSincronizador = new String[ids.size()];
			idsSincronizador.toArray((String[]) pacote.idsSincronizador);

			JSONObject json = pacote.getJson(c);
			String strJson = json.toString();
			if (imprimiuObjeto)
				fw.write(",");
			fw.write(strJson);
			imprimiuObjeto = true;
			totalImpresso += ids.size();

		}
		if (containerInsert.isInitialized()) {

			PacoteEditInsert pacote = new PacoteEditInsert();

			ArrayList<String> idsSincronizador = containerInsert.getListIdEXTDAOSincronizador();
			ArrayList<String> ids = containerInsert.getIdsEXTDAO();
			ArrayList<String[]> idsDependentesPorCorpo = containerInsert.getIdsDependentesPorCorpo();
			ArrayList<String[]> atributosDependentesPorCorpo = containerInsert.getAtributosDependentesPorCorpo();
			ArrayList<String[]> registros = containerInsert.getListValores();

			pacote.campos = table.getAtributosMenosId();
			pacote.protocolos = new PacoteCorpo[ids.size()];
			pacote.tabela = table.getName();
			pacote.tipoOperacao = EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT;
			for (int i = 0; i < ids.size(); i++) {

				pacote.protocolos[i] = new PacoteCorpo();
				pacote.protocolos[i].id = ids.get(i);
				pacote.protocolos[i].idSincronizador = idsSincronizador.get(i);
				// pacote.protocolos[i].corpo = corpos.get(i);
				pacote.protocolos[i].valores = registros.get(i);

				pacote.protocolos[i].idsDependentes = idsDependentesPorCorpo.get(i);
				pacote.protocolos[i].atributosDependentes = atributosDependentesPorCorpo.get(i);
				totalImpresso++;
			}
			JSONObject json = pacote.getJson(c);
			if (imprimiuObjeto)
				fw.write(",");
			String strJson = json.toString();
			fw.write(strJson);
			imprimiuObjeto = true;

		}

		if (containerEdit.isInitialized()) {

			PacoteEditInsert pacote = new PacoteEditInsert();

			ArrayList<String> idsSincronizador = containerEdit.getListIdEXTDAOSincronizador();
			ArrayList<String[]> idsDependentesPorCorpo = containerEdit.getIdsDependentesPorCorpo();
			ArrayList<String[]> atributosDependentesPorCorpo = containerEdit.getAtributosDependentesPorCorpo();
			ArrayList<String[]> registros = containerEdit.getListValores();
			ArrayList<String> ids = containerEdit.getIdsEXTDAO();
			pacote.protocolos = new PacoteCorpo[ids.size()];
			pacote.tipoOperacao = EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_EDIT;
			pacote.campos = table.getAtributosMenosId();
			pacote.tabela = table.getName();
			for (int i = 0; i < ids.size(); i++) {

				pacote.protocolos[i] = new PacoteCorpo();

				// pacote.protocolos[i].corpo = corpos.get(i);
				pacote.protocolos[i].valores = registros.get(i);

				pacote.protocolos[i].idsDependentes = idsDependentesPorCorpo.get(i);
				pacote.protocolos[i].atributosDependentes = atributosDependentesPorCorpo.get(i);
				pacote.protocolos[i].id = ids.get(i);
				pacote.protocolos[i].idSincronizador = idsSincronizador.get(i);

			}
			totalImpresso += ids.size();
			JSONObject json = pacote.getJson(c);
			String strJson = json.toString();
			if (imprimiuObjeto)
				fw.write(",");
			fw.write(strJson);
			imprimiuObjeto = true;

		}
		// return imprimiuObjeto;
		return totalImpresso;
	}

	public static void updateEstadoEstruturaMigrada(Context context) {
		try{
			Item item = getItem(context, null);
			item.setEstado(ESTADO_SINCRONIZACAO_MOBILE.ESTRUTURA_MIGRADA);
			item.salvar(context);
		}catch(Exception ex)
		{
			SingletonLog.insereErro(ex,SingletonLog.TIPO.SINCRONIZADOR);
		}

	}
}
