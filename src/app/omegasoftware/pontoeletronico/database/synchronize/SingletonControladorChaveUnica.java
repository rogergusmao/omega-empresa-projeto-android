package app.omegasoftware.pontoeletronico.database.synchronize;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.dialog.HelperDialog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.ResultSet;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador.STATE_SINCRONIZADOR;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class SingletonControladorChaveUnica {
	
	static SingletonControladorChaveUnica  singleton = null; 
	
	ConcurrentHashMap<Integer, AtomicLong> hash;
	
	static ReentrantLock re = new ReentrantLock();
	static ReentrantLock lockInitSequence = new ReentrantLock();
	static ReentrantLock lockInitSRAW = new ReentrantLock();
	static boolean initCache = false;
	static boolean initSequence = false;
	static boolean initSRAW = false;
	
	Context context;
	
	public static SingletonControladorChaveUnica getSingleton(Context context) throws Exception {
		if(singleton == null){
			if(re.tryLock()){
				try{
					
					if(singleton == null && Database.isDatabaseInitialized(context)){
						singleton = new SingletonControladorChaveUnica(context);	
					}	
				}finally{
					re.unlock();
				}	
			}
		}
		
		return singleton;
	}
	
	public static SingletonControladorChaveUnica getSingleton(){
		
		return singleton;
	}
	private SingletonControladorChaveUnica(Context context) throws Exception{
		this.context=context;
		hash = new ConcurrentHashMap<Integer, AtomicLong>();
		initCache(context);
		
	}
	public void emptyCache(){
		if(hash != null)hash.clear();
		initSequence = false;
		initSRAW = false;
	}
	public boolean initCache() throws Exception{
		return initCache(context);
	}
	public boolean initCache(Context context) throws Exception{
		try{
			STATE_SINCRONIZADOR state= RotinaSincronizador.getStateSincronizador(context, null);
			
			initUsingSequence(context, state);
			
			initUsingTableSistemaRegistroSincronizadorAndroidParaWeb(context, state );
			return true;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
			throw ex;
			
		}
	}
	
	
	private void initUsingSequence(Context context, STATE_SINCRONIZADOR state){
		DatabasePontoEletronico db = null;
		try{
			if(context != null && !initSequence &&  lockInitSequence.tryLock()){
				try{
					if(!initSequence){
						if(state == null) state = RotinaSincronizador.getStateSincronizador(context, null);
						if(state != STATE_SINCRONIZADOR.COMPLETO)
							return ;
						db = new DatabasePontoEletronico(context);
						String[] tabelas = db.getVetorNomeTabela();
						
						
						EXTDAOSistemaTabela obj = new EXTDAOSistemaTabela(db);
						ArrayList<Table> regs= obj.getListTable();
						if(regs != null){
							for (String tabela : tabelas) {
								if(tabela.equalsIgnoreCase("android_metadata") ) 
									continue;
								String idReg= EXTDAOSistemaTabela.getIdSistemaTabela(context, tabela);
								if(idReg == null) continue;
								Long idMin = db.getMinId(tabela);

								idMin = idMin == null || idMin > 0 ? -1 : idMin ;
								//oh.query(tabela,string[]{EXTDAOSistemaTabela.ID} , , selectionArgs, groupBy, having, orderBy)
								hash.put(HelperInteger.parserInteger( idReg), new AtomicLong( idMin));
							}
						}
						initSequence = true;
					}
				}finally{
					lockInitSequence.unlock();
				}	
			}
			
			
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}finally{
			if(db != null) db.close();
		}
	}
	private void initUsingTableSistemaRegistroSincronizadorAndroidParaWeb(Context context, STATE_SINCRONIZADOR state){
		
		try{
		if(context != null && !initSRAW &&  lockInitSRAW.tryLock()){
			try{
				if(!initSRAW){
					
					Database db = null;
					try{
						if(state == null) state = RotinaSincronizador.getStateSincronizador(context, null);
						if(state != STATE_SINCRONIZADOR.COMPLETO)
							return ;
						
					
						db = new DatabasePontoEletronico(context);
							
						final String q = "SELECT "
								+ " t.id, "
			+ " MIN(s.id_tabela_INT),"
			+ " MIN(s.id_tabela_mobile_INT)"
			+ " FROM"
			+ " 	sistema_registro_sincronizador_android_para_web s"
			+ " JOIN sistema_tabela t ON s.sistema_tabela_id_INT = t.id"
			+ " GROUP BY"
			+ " t.id";
						
						ResultSet rs = db.getResultSet(q , null);
						if(rs != null){
							for(int i = 0 ; i < rs.getTotalTupla(); i++){
								String[] valores= rs.getTupla(i);
								Integer idTabela = HelperInteger.parserInteger(valores[0]);
								if(valores[1] != null && valores[2] != null ){
									if(valores[1].compareTo( valores[2]) < 0 )
										tryUpdate(idTabela, HelperInteger.parserLong( valores[1]));
									else
										tryUpdate(idTabela, HelperInteger.parserLong( valores[2]));
								}else if(valores[1] != null)
									tryUpdate(idTabela, HelperInteger.parserLong( valores[1]));
								else if(valores[2] != null)
									tryUpdate(idTabela, HelperInteger.parserLong( valores[2]));
							}
						}
						
						
					
					}catch(Exception ex){
						SingletonLog.insereErro(ex, TIPO.BANCO);
						
					}finally{
						if(db != null ) db.close();
					}
					initSRAW = true;
				}
			}finally{
				lockInitSRAW.unlock();
			}	
		}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}
	}
	private void tryUpdate(int idTabela, long id){
		if(hash.containsKey(idTabela)){
			AtomicLong minId = hash.get(idTabela);
			if(minId.get() > id){
				hash.remove(idTabela);
				hash.put(idTabela, new AtomicLong( id));
			}
		}
	}
	
	public static Long getNewId(Integer idSistemaTabela) throws NewIdException{
		SingletonControladorChaveUnica s = SingletonControladorChaveUnica.getSingleton();
		if(s != null){
			if(s.hash.containsKey(idSistemaTabela)){
				AtomicLong a = s.hash.get(idSistemaTabela);
				Long id = a.decrementAndGet();
				return id;
			} else {
				try {
					s.initCache();
				} catch (Exception e) {
					throw new NewIdException("[A]New id nulo. Sistema tabela: "+idSistemaTabela+" !!! Exception ao inicializar!", e);
				}
				if(s.hash.containsKey(idSistemaTabela)){
					AtomicLong a = s.hash.get(idSistemaTabela);
					Long id = a.decrementAndGet();
					return id;
				} 
				else throw new NewIdException("[B]New id nulo. Sistema tabela: "+idSistemaTabela+" !!! A cache nuo foi inicializada!");
			}
		} else return null;
		
	}
	
	
}
