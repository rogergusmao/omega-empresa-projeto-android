package app.omegasoftware.pontoeletronico.database.synchronize;

import java.util.ArrayList;
import java.util.regex.Pattern;

import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.TabletActivities.SynchronizeReceiveActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.ContainerPrograma;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorWebParaAndroid;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoOperacaoBanco;
import app.omegasoftware.pontoeletronico.http.HelperHttp;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.phone.HelperPhone;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_BOOLEAN;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
import app.omegasoftware.pontoeletronico.service.ESTADO_SINCRONIZADOR;

//public class SynchronizeFastReceiveDatabase {
//	public static final String TAG = "SynchronizeFastReceiveDatabase";
//	int numeroReceiveTupla;
//	String vetorTabela[];
//	String vetorIdTabelaSincronizador[];
//	int indexTabela = 0 ;
//	ContainerReceiveInformationDatabase container;
//	SynchronizeFastSendDatabase syncSend;
//	Context context;
//	static SynchronizeFastReceiveDatabase singleton = null;
//	
//	private static boolean IS_RUNNING = false;
//
//	Pattern vPatternLinha = Pattern.compile(OmegaConfiguration.DELIMITER_QUERY_UNIQUE_LINHA);
//	Pattern vPatternColuna = Pattern.compile(OmegaConfiguration.DELIMITER_QUERY_UNIQUE_COLUNA);
//	
//	//PROCEDIMENTOS
//	//CICLO [0]*[1-4]1
//	//0 - SEND ALL INFORMATION
//	//1 - RECEIVE LOAD INFORMATION
//	//2 - EXECUTE OPERACAO REMOVE
//	//3 - REVEIVE OPERACAO INSERT
//	//4 - REVEIVE OPERACAO EDIT
//	String vetorProcedimentos[] = new String[] {
//			"RECEIVE LOAD INFORMATION", 
//			"EXECUTE OPERACAO REMOVE", 
//			"REVEIVE OPERACAO INSERT",
//	"REVEIVE OPERACAO EDIT"};
//
//	int vIndexProcedimento = 0;
//	
//	public static SynchronizeFastReceiveDatabase getInstance(){
//		
//		return singleton;
//	}
//	
//	public static SynchronizeFastReceiveDatabase getInstance(Context pContext,
//			int pNumeroReceiveTupla){
//		if(singleton == null)
//			singleton = new SynchronizeFastReceiveDatabase(pContext, pNumeroReceiveTupla);
//		return singleton;
//	}
//
//	String[] vetorTabelaUpload;
//	String[] vetorTabelaDownload;
//	private SynchronizeFastReceiveDatabase(
//			Context pContext,
//			int pNumeroReceiveTupla){
//		context = pContext;
//		numeroReceiveTupla = pNumeroReceiveTupla;
//		inicializa();
//	}
//	
//	private void inicializa(){
//		
//		if(vetorTabela != null) return;
//		
//		Database database = new DatabasePontoEletronico(context);
//		
//		if(vetorTabelaUpload == null || vetorTabelaUpload.length == 0)
//			vetorTabelaUpload = OmegaConfiguration.LISTA_TABELA_SINCRONIZACAO_UPLOAD_CONTINUO(database);
//		
//		if(vetorTabelaDownload == null || vetorTabelaDownload.length == 0)
//			vetorTabelaDownload = OmegaConfiguration.LISTA_TABELA_SINCRONIZACAO_DOWNLOAD_CONTINUO(database);
//		if(vetorTabelaDownload != null && vetorTabelaDownload.length > 0){
//			ArrayList<String> vListTable = database.getListNameHierarquicalOrderOfVetorTable(vetorTabelaDownload);
//			vetorTabela = new String[vListTable.size()];
//			vListTable.toArray(vetorTabela);
//			vetorIdTabelaSincronizador = new String[vListTable.size()];
//			int i = 0 ;
//			
//			for (String vNomeTabela : vListTable) {
//				
//				String vIdTabelaSincronizador = EXTDAOSistemaTabela.getIdSistemaTabela(database, vNomeTabela);
//				vetorIdTabelaSincronizador[i] = vIdTabelaSincronizador;
//				i += 1;
//			}
//		}
//		
//		if(vetorTabelaUpload != null && vetorTabelaUpload.length > 0)
//			syncSend = new SynchronizeFastSendDatabase(context, vetorTabelaUpload);
//	}
//	
//	private SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS procedureRemove(){
//		ArrayList<ContainerTableInformation> vListRemove = container.getListaRemoveContainerTableInformation();
//		
//		boolean vValidade= false;
//		if(! container.isEmptyListRemoveContainerTableInformation()){
//			Database database = null;
//			try{
//				database = new DatabasePontoEletronico(context);
//				EXTDAOSistemaRegistroSincronizadorWebParaAndroid vObjSincronizadorPHP = new EXTDAOSistemaRegistroSincronizadorWebParaAndroid(database);
//				
//				for (int i = 0 ; i < vListRemove.size(); i++) {
//					ContainerTableInformation vContainerRemove = vListRemove.get(i);
//					ArrayList<String> vListId = vContainerRemove.getListId();
//					if(vListId.size() > 0 )
//						if(!vValidade)
//							vValidade= true;
//					Table vObj = vContainerRemove.getObjTable(database);
//					if(vObj == null) {
//						Log.e(TAG, "A tabela u inexistente no banco de dados android: id sistema_tabela = " + vContainerRemove.idTabelaSincronizador);
//						
//						vContainerRemove.removeListOfId(-1, vObjSincronizadorPHP);
//						SingletonLog.insereErro(
//								TAG, 
//								"procedureEditOfContainerTableInformation", 
//								"Tabela u inexistente no banco de dados: idSistemaTabela = " + vContainerRemove.idTabelaSincronizador,
//								SingletonLog.TIPO.SINCRONIZADOR);
//						return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//						
//					}
//					for (String vId : vListId) {
//						vObj.setAttrValue(Table.ID_UNIVERSAL, vId);
//						vObj.remove(false);
//					}
//					 
//					vContainerRemove.removeListOfId(-1, vObjSincronizadorPHP);
//					if(vContainerRemove.isEmpty()){
//						container.removeRemoveContainerTableInformation(vContainerRemove);
//						vContainerRemove = null;
//					}
//				}
//				
//			}catch(Exception ex){
//				SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
//				return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//			} 
//		}
//		
//		return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//	}
//
//	
//	private SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS procedureEditOfContainerTableInformation(Database database, ContainerTableInformation pContainerInsert, int pLimit){
//		//String vStrListId = pContainerInsert.getListOfId(pLimit);
//		String vStrListSincronizadorId = pContainerInsert.getStrListOfIdSincronizador(pLimit);
//		
//		String vResult = HelperHttpPost.getConteudoStringPost(
//				context, 
//				OmegaConfiguration.URL_REQUEST_SEND_LIST_QUERY_OF_LIST_ID_SINCRONIZADOR(), 
//				new String[]{
//						"usuario",
//						"id_corporacao", 
//						"corporacao", 
//						"senha", 
//						"imei",
//						"id_programa",
//						"vetor_id_sincronizador", 
//						"id_sistema_tabela", 
//						"id_tipo_operacao_banco"},
//				new String[]{
//						OmegaSecurity.getIdUsuario(), 
//						OmegaSecurity.getIdCorporacao(),
//						OmegaSecurity.getCorporacao(), 
//						OmegaSecurity.getSenha(), 
//						HelperPhone.getIMEI(),
//						ContainerPrograma.getIdPrograma(),
//						vStrListSincronizadorId, 
//						pContainerInsert.getIdSistemaTabela(), 
//						EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_EDIT});
//		//		So se falhar o resultado como nulo
//		if(vResult == null) return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.SERVIDOR_FORA_DO_AR ;
//		else if(! vResult.startsWith("ERROR")){
//			
//			//String vVetorTupla[] = vResult.split( OmegaConfiguration.DELIMITER_QUERY_UNIQUE_LINHA);
//			String vVetorTupla[] = vPatternLinha.split(vResult);
//			//INSERT ...
//			//ATTR 1, ..., ATTR N
//			if(vVetorTupla != null && vVetorTupla.length > 2 ){
//					
//					Table vObjTable=null;
//					try {
//						vObjTable = pContainerInsert.getObjTable(database);
//					} catch (Exception e) {
//						SingletonLog.insereErro(e, TIPO.PAGINA);
//					}
//					if(vObjTable == null) {
//						Log.e(TAG, "A tabela u inexistente no banco de dados android: id sistema_tabela = " + pContainerInsert.idTabelaSincronizador);
//						SingletonLog.insereErro(
//								TAG, 
//								"procedureEditOfContainerTableInformation", 
//								"Tabela u inexistente no banco de dados: idSistemaTabela = " + pContainerInsert.idTabelaSincronizador,
//								SingletonLog.TIPO.SINCRONIZADOR);
//						
//						EXTDAOSistemaRegistroSincronizadorWebParaAndroid vObjSincronizadorPHP = new EXTDAOSistemaRegistroSincronizadorWebParaAndroid(database); 
//						pContainerInsert.removeListOfId(pLimit, vObjSincronizadorPHP);
//						return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL ;
//					}
//					String vCabecalhoInsert = vVetorTupla[0];
//					if(! vCabecalhoInsert.startsWith("UPDATE")) 
//						return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO ;
//					String vStrVetorAtributo = vVetorTupla[1];
//					//String vVetorAtributo[] = vStrVetorAtributo.split(OmegaConfiguration.DELIMITER_QUERY_UNIQUE_COLUNA);
//					String vVetorAtributo[] = vPatternColuna.split(vStrVetorAtributo);
//					for(int i = 2; i < vVetorTupla.length ; i ++){
//						String vTupla = vVetorTupla[i];
//						if(vTupla == null || vTupla.length() == 0 ) continue;
//						else{
//							String vVetorValorAtributo[] = vPatternColuna.split(vTupla);
//							//String vVetorValorAtributo[] = vTupla.split( OmegaConfiguration.DELIMITER_QUERY_UNIQUE_COLUNA);
//							if(! (vVetorValorAtributo.length == vVetorAtributo.length)) continue;
//							for(int j = 0 ; j < vVetorValorAtributo.length; j ++){
//								String vAttrName = vVetorAtributo[j];
//								String vAttrValue = HelperString.checkIfIsNull(vVetorValorAtributo[j]);
//								vObjTable.setAttrValue(vAttrName, vAttrValue);
//							}
//							if(!vObjTable.update(false))
//								return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//						}
//					}
//				
//			} else return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//			
//		} else{
//			SingletonLog.insereErro(
//					TAG, 
//					"procedureEditOfContainerTableInformation", 
//					"Erro na postagem: " + vResult, 
//					SingletonLog.TIPO.SINCRONIZADOR);
//			return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL ;
//		}
//		EXTDAOSistemaRegistroSincronizadorWebParaAndroid vObjSincronizadorPHP = new EXTDAOSistemaRegistroSincronizadorWebParaAndroid(database); 
//		pContainerInsert.removeListOfId(pLimit, vObjSincronizadorPHP);
//		
//		return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO ;
//	}
//
//	//	return number of post's executed
//	private SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS procedureInsertOfContainerTableInformation(Database database, ContainerTableInformation pContainerInsert, int pLimit){
//		//String vStrListId = pContainerInsert.getListOfId(pLimit);
//		String vStrListSincronizadorId = pContainerInsert.getStrListOfIdSincronizador(pLimit);
//		
//		//String vId ="sistema_tabela: " + pContainerInsert.getIdSistemaTabela(); 
//		if(vStrListSincronizadorId.length() == 0 ) return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//		String vResult = HelperHttpPost.getConteudoStringPost(
//				context, 
//				OmegaConfiguration.URL_REQUEST_SEND_LIST_QUERY_OF_LIST_ID_SINCRONIZADOR(), 
//				new String[]{
//						"usuario",
//						"id_corporacao",
//						"corporacao", 
//						"senha", 
//						"imei",
//						"id_programa",
//						"vetor_id_sincronizador", 
//						"id_sistema_tabela", 
//						"id_tipo_operacao_banco"},
//				new String[]{
//						OmegaSecurity.getIdUsuario(), 
//						OmegaSecurity.getIdCorporacao(), 
//						OmegaSecurity.getCorporacao(), 
//						OmegaSecurity.getSenha(), 
//						HelperPhone.getIMEI(),
//						ContainerPrograma.getIdPrograma(),
//						vStrListSincronizadorId, 
//						pContainerInsert.getIdSistemaTabela(), 
//						EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_INSERT});
//		//		So se falhar o resultado como nulo
//		if(vResult == null) return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.SERVIDOR_FORA_DO_AR;
//		else if (vResult.length() == 0 ) return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//		else if(! vResult.startsWith("ERROR")){
//			
//			//String vVetorTupla[] = vResult.split(OmegaConfiguration.DELIMITER_QUERY_UNIQUE_LINHA);
//			String vVetorTupla[] = vPatternLinha.split(vResult);
//			if(vVetorTupla != null){
//				//INSERT ...
//				//ATTR 1, ..., ATTR N
//				if(vVetorTupla.length > 2 ){
//					Table vObjTable=null;
//					try {
//						vObjTable = pContainerInsert.getObjTable(database);
//					} catch (Exception e) {
//						
//						e.printStackTrace();
//					}
//					if(vObjTable == null) {
//						Log.e(TAG, "A tabela u inexistente no banco de dados android: id sistema_tabela = " + pContainerInsert.idTabelaSincronizador);
//						EXTDAOSistemaRegistroSincronizadorWebParaAndroid vObjSincronizadorPHP = new EXTDAOSistemaRegistroSincronizadorWebParaAndroid(database);
//						pContainerInsert.removeListOfId(pLimit,vObjSincronizadorPHP);
//						return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//					}
//					String vCabecalhoInsert = vVetorTupla[0];
//					if(! vCabecalhoInsert.startsWith("INSERT INTO")) return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//					String vStrVetorAtributo = vVetorTupla[1];
//					//String vVetorAtributo[] = vStrVetorAtributo.split( OmegaConfiguration.DELIMITER_QUERY_UNIQUE_COLUNA);
//					String vVetorAtributo[] = vPatternColuna.split(vStrVetorAtributo);
//					for(int i = 2; i < vVetorTupla.length ; i ++){
//						String vTupla = vVetorTupla[i];
//						if(vTupla == null) continue;
//						else if (vTupla.length() == 0 ) continue;
//						else{
//							String vVetorValorAtributo[] = vPatternColuna.split(vTupla);
//							//String vVetorValorAtributo[] = vTupla.split( OmegaConfiguration.DELIMITER_QUERY_UNIQUE_COLUNA);
//							if(! (vVetorValorAtributo.length == vVetorAtributo.length)) continue;
//							for(int j = 0 ; j < vVetorValorAtributo.length; j ++){
//								String vAttrName = vVetorAtributo[j];
//								String vAttrValue = HelperString.checkIfIsNull(vVetorValorAtributo[j]);
//								vObjTable.setAttrValue(vAttrName, vAttrValue);
//							}
//							Long novoId = vObjTable.insert(false);
//							if(novoId != null)
//								return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//						}
//					}
//				}
//			}
//
//		} else{
//			SingletonLog.insereErro(
//					TAG, 
//					"procedureInsertOfContainerTableInformation", 
//					"Erro na postagem: " + vResult, 
//					SingletonLog.TIPO.SINCRONIZADOR);
//			return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//		}
//		EXTDAOSistemaRegistroSincronizadorWebParaAndroid vObjSincronizadorPHP = new EXTDAOSistemaRegistroSincronizadorWebParaAndroid(database);
//		pContainerInsert.removeListOfId(pLimit,vObjSincronizadorPHP);		
//		return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//	}
//
//
//	private SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS procedureInsert(){
//		
//		if(! container.isEmptyListInsertContainerTableInformation()){
//			ContainerTableInformation vInsertContainerTableInformation = null;
//			for(; indexTabela < vetorTabela.length; indexTabela++){
//				String vIndexTabelaNaTabelaSincronizador =vetorIdTabelaSincronizador [indexTabela];
//				vInsertContainerTableInformation = container.getInsertContainerTableInformation(vIndexTabelaNaTabelaSincronizador);
//				if(vInsertContainerTableInformation == null) continue;
//				else if(vInsertContainerTableInformation.isEmpty()){
//					container.removeInsertContainerTableInformation(vInsertContainerTableInformation);
//					vInsertContainerTableInformation = null;
//				} else break;
//			}
//			if(vInsertContainerTableInformation == null){
//				indexTabela = 0;
//				
//				return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//			}
//			else{
//				Database database = null;
//				try{
//					database = new DatabasePontoEletronico(context);
//					SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS codigo =procedureInsertOfContainerTableInformation(database, vInsertContainerTableInformation, numeroReceiveTupla); 
//					if(codigo != SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO) return codigo;
//				
//					if(vInsertContainerTableInformation.isEmpty()){
//						container.removeInsertContainerTableInformation(vInsertContainerTableInformation);
//						if(indexTabela < vetorTabela.length - 1)
//							indexTabela += 1;
//						else{
//							indexTabela = 0;
//						}
//					}
//					return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//				}catch(Exception ex){
//					
//					SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
//					return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//				}
//			}
//		} 
//			
//		return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//		
//	}
//	
//	
//	private SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS procedureEdit(){
//		
//		if(! container.isEmptyListEditContainerTableInformation()){
//			ContainerTableInformation vEditContainerTableInformation = null;
//			for(; indexTabela < vetorIdTabelaSincronizador.length; indexTabela++){
//				String vIndexTabelaNaTabelaSincronizador =vetorIdTabelaSincronizador [indexTabela];
//				vEditContainerTableInformation = container.getEditContainerTableInformation(vIndexTabelaNaTabelaSincronizador);
//				if(vEditContainerTableInformation == null) continue;
//				else if(vEditContainerTableInformation.isEmpty()){
//					container.removeEditContainerTableInformation(vEditContainerTableInformation);
//					vEditContainerTableInformation = null;
//					
//				}else break;
//			}
//			//Se acabou o ciclo
//			if(vEditContainerTableInformation == null){
//				indexTabela = 0;
//				
//				return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//			}
//			else{ 
//				Database database = null;
//				try{
//					database = new DatabasePontoEletronico(context);
//					SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS codigo =procedureEditOfContainerTableInformation(database, vEditContainerTableInformation, numeroReceiveTupla);
//					if(codigo != SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO)
//						return codigo;
//					
//					if(vEditContainerTableInformation.isEmpty()){
//						container.removeEditContainerTableInformation(vEditContainerTableInformation);
//						if(indexTabela < vetorTabela.length - 1)
//							indexTabela += 1;
//						else{
//							indexTabela = 0;
//							
//						}
//					}
//					return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//					
//				}catch(Exception ex){
//					SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
//					return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL ;
//				}
//				
//			}
//		} else {
//			
//			return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//		}
//	}
//	
//	public boolean isRunning(){
//		return IS_RUNNING;
//	}
//	
//	
//	
//	public SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS run(boolean pIsToSincronizarManualmente, boolean somenteEnvieOsDados){
//		inicializa();
//		SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS estado = procedureRun(pIsToSincronizarManualmente, somenteEnvieOsDados);
//		if(estado == SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL)
//			PontoEletronicoSharedPreference.saveValor(context, TIPO_BOOLEAN.RESETAR_BANCO_DE_DADOS, true);
//		
//		return estado;
//	}
//	
//	
//	
//	private SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS procedureRun(boolean pIsToSincronizarManualmente, boolean somenteEnvieOsDados ){
//		
//		boolean resetarBancoDeDados= false;
//		SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS estado = SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO; 
//	
//		try{
//			if(IS_RUNNING) {
//				estado =SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.SINCRONIZADOR_JA_ESTA_EM_EXECUCAO; 
//				return estado;
//			}
//			else IS_RUNNING = true;
//			resetarBancoDeDados= PontoEletronicoSharedPreference.getValor(context, TIPO_BOOLEAN.RESETAR_BANCO_DE_DADOS);	
//	//		Checa se a sincronizacao eh automatica
//			if(!pIsToSincronizarManualmente){
//				boolean vIsSincronizacaoAutomatica = SynchronizeReceiveActivityMobile.getSavedSincronizacaoAutomatica(context);
//				
//				if(!vIsSincronizacaoAutomatica) {
//					ESTADO_SINCRONIZADOR.sendServiceStateMessage(context, ESTADO_SINCRONIZADOR.inativo);
//					try{
//						Thread.sleep(1000);	
//					}catch(Exception ex){
//						
//					}
//					
//					estado = SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.SINCRONIZADOR_ESTA_NO_MODO_MANUAL;
//					return estado;
//				}
//			}
////			if(!HelperHttp.isServerOnline(context)){
////				estado =SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.SERVIDOR_FORA_DO_AR; 
////				return estado;
////			}
//			if(container == null)
//				container = new ContainerReceiveInformationDatabase(context);
//			//		SEND ALL INFORMATION
//			ESTADO_SINCRONIZADOR.sendServiceStateMessage(context, ESTADO_SINCRONIZADOR.enviando);
//			try{
//				Thread.sleep(1000);	
//			}catch(Exception ex){
//				
//			}
//		}
//		catch(Exception ex2){
//			SingletonLog.insereErro(ex2, TIPO.SINCRONIZADOR);
//			return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//		}
//		
//		try{
//			if(syncSend == null)
//				return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//			estado = syncSend.sendDataOfHierarchyDatabase();
//			
//			if(estado != SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO)
//				return estado;
//			else if(resetarBancoDeDados)
//				//CASO tenha uma ordem para resetar o banco de dados entuo nuo u necessurio receber os dados
//				return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//			if(somenteEnvieOsDados)
//				return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//			ESTADO_SINCRONIZADOR.sendServiceStateMessage(context, ESTADO_SINCRONIZADOR.recebendo);
//				
//	//		RECEIVE LOAD INFORMATION
//			if(container.isEmpty()){
//				container.loadListaInformacaoSincronizador();
//				if(container.isEmpty()){
//					return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//				}
//			}	
//	//		EXECUTE OPERACAO REMOVE ANDROID
//			estado = procedureRemove();
//			if(estado != SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO)
//				return estado;
//	//		REVEIVE OPERACAO INSERT PHP => ANDROID
//			estado = procedureInsert();
//			if(estado != SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO)
//				return estado;
//			
//	//		REVEIVE OPERACAO EDIT PHP => ANDROID
//			estado = procedureEdit();
//			
//			return estado;
//		} catch(Exception ex){
//			SingletonLog.insereErro(
//					ex, 
//					SingletonLog.TIPO.SINCRONIZADOR);
//			return SynchronizeFastSendDatabase.CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//		} finally{
//			ESTADO_SINCRONIZADOR.sendServiceStateMessage(context, ESTADO_SINCRONIZADOR.parado);
//			IS_RUNNING = false;
//		}
//	}
//}
//
