package app.omegasoftware.pontoeletronico.database.synchronize;



import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.appconfiguration.ContainerPrograma;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemSharedPreference;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemSharedPreference.TIPO_STRING;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorAndroidParaWeb;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTipoOperacaoBanco;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.phone.HelperPhone;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

//DEPRECATED
//public class SynchronizeFastSendDatabase  {
//
//	Context context;
//	
//	static String POST_KEY_CABECALHO = "cabecalho";
//	static String POST_KEY_CORPO = "corpo";
//	private final static String TAG = "SynchronizeFastSendDatabase";
//	
//	public enum CODIGO_RETORNO_ENVIO_DOS_DADOS{
//		OPERACAO_REALIZADA_COM_SUCESSO,
//		SERVIDOR_FORA_DO_AR,
//		ERRO_FATAL,
//		SINCRONIZADOR_JA_ESTA_EM_EXECUCAO,
//		SINCRONIZADOR_ESTA_NO_MODO_MANUAL
//	}
//
//	String[] listTableToSend;
//	String URL = null;
//
//	public SynchronizeFastSendDatabase(
//			Context context, 
//			String[] listTableToSend){
//
//		this.context = context;
//		Database   db = new DatabasePontoEletronico(context);
//		ArrayList<String> vListTable = db.getListNameHierarquicalOrderOfVetorTable(listTableToSend);
//		this.listTableToSend = new String[vListTable.size()];
//		vListTable.toArray(this.listTableToSend);
//	}
//
//
//
//	//	Envia tabela a tabela e recebe os id's correspondentes no banco do servidor,
//	//		atualizando cada tabela dependente a ser enviada
//	//		Ex.: Funcionario, FuncionarioEmpresa
//	public CODIGO_RETORNO_ENVIO_DOS_DADOS sendDataOfHierarchyDatabase(){
//		if(OmegaConfiguration.DEBUGGING)
//			Log.d("sendDataOfHierarchyDatabase", "Inicia de envio dos dados de sincronizacao android->web");
//		
//		Database db = null;
//		ArrayList<String> tabelasSync = null;
//		try{
//			db = new DatabasePontoEletronico(context);
//			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb sincronizador ;
//			sincronizador = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(db);
//			tabelasSync = sincronizador.getListTableToSync();
//			
//		}finally{
//			if(db != null) db.close();
//		}
//		try{
//			if(tabelasSync == null || tabelasSync.size() == 0 ){
//				if(OmegaConfiguration.DEBUGGING){
//					Log.d("sendDataOfHierarchyDatabase", "Nuo existe tabelas a serem sincronizadas");
//					Log.d("sendDataOfHierarchyDatabase", "Finalizado o processo de sincronizacao das tabelas" );
//				}
//				return CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//			}
//			else{
//				if(OmegaConfiguration.DEBUGGING){
//					
//					String tabelasASeremSincronizadas = HelperString.getStrSeparateByDelimiterColumn(tabelasSync, ",");	
//					Log.d("sendDataOfHierarchyDatabase", "Tabelas a ser sincronizadas: " + tabelasASeremSincronizadas);
//				}
//				
//				for (String vNomeTabela : listTableToSend) {
//					String chave=  null;
//					for (String vTableToSync : tabelasSync) {
//						
//						if(vNomeTabela.compareTo(vTableToSync) == 0 ){
//							CODIGO_RETORNO_ENVIO_DOS_DADOS estadoDoSincronizadorAposEnvioDaTabela = sendDataOfTable(vNomeTabela);
//							if(estadoDoSincronizadorAposEnvioDaTabela != CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO)
//								return estadoDoSincronizadorAposEnvioDaTabela;
//							chave = vNomeTabela;
//							break;
//						}
//					}
//					if(chave != null) tabelasSync.remove(chave);
//					if(tabelasSync.size() == 0)break;
//				}
//				
//				if(OmegaConfiguration.DEBUGGING){
//					Log.d("sendDataOfHierarchyDatabase", "Finalizado o processo de sincronizacao das tabelas" );
//				}
//			}
//		} catch(Exception ex){
//			Log.e("sendDataOfHierarchyDatabase", ex.getMessage());
//			
//			SingletonLog.insereErro(
//					ex, 
//					SingletonLog.TIPO.SINCRONIZADOR);
//			return CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//		
//		}
//		return CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//	}
//
//
//	private CODIGO_RETORNO_ENVIO_DOS_DADOS postEdit(Database db, Table pObj, ContainerEXTDAO pContainerQueryEdit){
//		
//
//		ArrayList<String> vListIdEXTDAO = pContainerQueryEdit.getListIdEXTDAO();
//		StringBuilder vStrVetorIdUpdate = new StringBuilder(); 
//		
//		StringBuilder vStrVetorTupla = new StringBuilder();
//		for (String vId : vListIdEXTDAO) {
//			try {
//				if(pObj.select(vId)){				
//					String vStrVetorValueAttr = pObj.getStrListAttrValueWithoutCorporacaoESenhaAttr();
//					if(vStrVetorTupla.length() == 0){
//						vStrVetorTupla.append( vStrVetorValueAttr);
//						vStrVetorIdUpdate.append(vId); 
//					}
//					else{
//						vStrVetorTupla.append(OmegaConfiguration.DELIMITER_QUERY_LINHA + vStrVetorValueAttr);
//						vStrVetorIdUpdate.append(OmegaConfiguration.DELIMITER_QUERY_COLUNA + vId);
//					}
//				}
//			} catch (Exception e) {
//				
//				e.printStackTrace();
//			}
//		}
//		if(vStrVetorTupla.length() == 0 || vStrVetorIdUpdate.length() == 0 ) return CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//
//		String vNomeTabela = pObj.getName();
//		
//		String vIdTabelaSincronizador = EXTDAOSistemaTabela.getIdSistemaTabela(db, vNomeTabela);
//		//TODO criar rotina para que caso o identificador nao seja identificado, esse devera ser restaurado offline ou online
//		String vStrVetorNomeAttr = pObj.getStrListAttrWithoutCorporacaoAndSenhaIfExist();
//		String post = HelperHttpPost.getConteudoStringPost(
//				context, 
//				OmegaConfiguration.URL_REQUEST_RECEIVE_LIST_UPDATE(), 
//				new String[] {
//						"usuario", 
//						"corporacao", 
//						"senha", 
//						"imei",
//						"id_programa",
//						"nome_tabela", 
//						"sistema_tabela", 
//						"vetor_nome_atributo",
//						"vetor_valor_atributo", 
//						"vetor_id_edit"}, 
//				new String[] {
//						OmegaSecurity.getIdUsuario(), 
//						OmegaSecurity.getIdCorporacao(), 
//						OmegaSecurity.getSenha(), 
//						HelperPhone.getIMEI(),
//						ContainerPrograma.getIdPrograma(),
//						vNomeTabela, 
//						vIdTabelaSincronizador, 
//						vStrVetorNomeAttr, 
//						vStrVetorTupla.toString(), 
//						vStrVetorIdUpdate.toString()}
//				);
//		if(post == null){ 
//			
//			SingletonLog.insereErro(
//					TAG, 
//					"postEdit", 
//					"Servidor fora do ar", 
//					SingletonLog.TIPO.SINCRONIZADOR);
//			return CODIGO_RETORNO_ENVIO_DOS_DADOS.SERVIDOR_FORA_DO_AR;
//		
//		}
//		else{
//			if(post.length() == 0) return CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//			else if(post.startsWith("FALSE")) return CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//			else return CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//		}
//	}
//
//	private String[] postInsert(Database db, Table pObj, ContainerEXTDAO pContainerQueryInsert ){
//		String vNomeTabela = pObj.getName();
//		
//		String vIdTabelaSincronizador = EXTDAOSistemaTabela.getIdSistemaTabela(db, vNomeTabela);
//		//TODO criar rotina caso o identificador nao seja reconhecido, esse devera ser restaurado online ou offline
//		if(vIdTabelaSincronizador == null){
//			//Log.d(TAG, "A tabela " + vNomeTabela + " nao se encontra na sistema_tabela, por favor insira a tupla referente.");
//			return null;
//		}
//		String vStrVetorNomeAttr = pObj.getStrListAttrWithoutCorporacaoAndSenhaIfExist();
//
//		ArrayList<String> vListOldId = pContainerQueryInsert.getListIdEXTDAO();
//		String[] vVetorIdReturn = new String[vListOldId.size()];
//		int i = 0 ;
//		String vStrVetorTupla = "";
//		for (String vId : vListOldId) {
//			pObj.clearData();
//			pObj.setAttrValue(Table.ID_UNIVERSAL, vId);
//			try {
//				if(pObj.select()){
//					String vStrVetorValueAttr = pObj.getStrListAttrValueWithoutCorporacaoESenhaAttr();
//					if(vStrVetorTupla.length() == 0)
//						vStrVetorTupla += vStrVetorValueAttr;
//					else
//						vStrVetorTupla += OmegaConfiguration.DELIMITER_QUERY_LINHA + vStrVetorValueAttr;
//				}
//				else vVetorIdReturn[i] = OmegaConfiguration.CODIGO_INEXISTENTE;
//			} catch (Exception e) {
//				
//				e.printStackTrace();
//			}
//			i += 1;
//		}
//
//		//se todos os codigos forem inexistente
//		if(vStrVetorTupla.length() == 0 ) return vVetorIdReturn;
//
//
//
//		String vResult = HelperHttpPost.getConteudoStringPost(
//				context, 
//				OmegaConfiguration.URL_REQUEST_RECEIVE_LIST_INSERT_AND_RETURN_ID_DATABASE(), 
//				new String[] {
//						"usuario",
//						"id_corporacao",
//						"corporacao", 
//						"senha", 
//						"imei",
//						"id_programa",
//						"nome_tabela", 
//						"sistema_tabela", 
//						"vetor_nome_atributo", 
//						"vetor_valor_atributo"}, 
//				new String[] {
//						OmegaSecurity.getIdUsuario(), 
//						OmegaSecurity.getIdCorporacao(),
//						OmegaSecurity.getCorporacao(),
//						OmegaSecurity.getSenha(), 
//						HelperPhone.getIMEI(),
//						ContainerPrograma.getIdPrograma(),
//						vNomeTabela, 
//						vIdTabelaSincronizador, 
//						vStrVetorNomeAttr, 
//						vStrVetorTupla
//					}
//				);
//		if(vResult == null) return null;
//		else if(vResult.startsWith("ERROR")) return null;
//		else{
//			String vVetorReceive[] = vResult.split(OmegaConfiguration.DELIMITER_QUERY_UNIQUE_LINHA);
//			int vIndexVetorReturn = 0;
//			for(i = 0 ; i < vVetorIdReturn.length; i ++)
//			{
//				String vToken = vVetorIdReturn[i];
//				if(vToken == null){
//					
//					vVetorIdReturn[i] = vVetorReceive[vIndexVetorReturn];
//					vIndexVetorReturn += 1;
//				}
//				else if(vToken.compareTo(OmegaConfiguration.CODIGO_INEXISTENTE) == 0) continue;
//				else {
//					if(vVetorReceive[i] == null || vVetorReceive[i].length() == 0)
//						SingletonLog.insereErro(new Exception("Falha na inseruuo durante a sincronizacao: " + vListOldId.get(i)), TIPO.SINCRONIZADOR);
//					vVetorIdReturn[i] = vVetorReceive[vIndexVetorReturn];
//					vIndexVetorReturn += 1;
//				}
//			}
//			return vVetorIdReturn;
//		}
//	}
//	
//	private CODIGO_RETORNO_ENVIO_DOS_DADOS procedureEdit(
//			String pNomeTabela,
//			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb sincronizador, 
//			ContainerFastSendData container){
//		
//		try{
//			Table vObjTable = sincronizador.getDatabase().factoryTable(pNomeTabela);
//			//Atualiza Tuplas ja sincronizadas
//			ArrayList<ContainerEXTDAO> containersUpdateEXTDAO = container.getListUpdateContainerEXTDAOOfTable(pNomeTabela);
//			
//			for (ContainerEXTDAO containerEXTDAO : containersUpdateEXTDAO) {
//				CODIGO_RETORNO_ENVIO_DOS_DADOS codigo = postEdit(sincronizador.getDatabase(), vObjTable, containerEXTDAO);
//				if(codigo != CODIGO_RETORNO_ENVIO_DOS_DADOS.SERVIDOR_FORA_DO_AR){
//					ArrayList<String> vListIdEXTDAO = containerEXTDAO.getListIdEXTDAO();
//					 
//					for (String vId : vListIdEXTDAO) {
//						vObjTable.clearData();
//						vObjTable.setAttrValue(Table.ID_UNIVERSAL, vId);
//						if(vObjTable.select()){
//							vObjTable.onSynchronizing(context, vId, vId);
//						}
//					}
//					ArrayList<String> vListIdSincronizador = containerEXTDAO.getListIdEXTDAOSincronizador();
//
//					for (String vIdSincronizador : vListIdSincronizador) {
//						sincronizador.remove(vIdSincronizador, false);
//					}	
//				} else return codigo;
//
//			}
//			return CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//		}
//		catch(Exception ex){
//			//TODO deve ser forcada a reinicializacao do banco de dados
//			if(ex != null){
//				Log.e(TAG, ex.getMessage());
//				SingletonLog.insereErro( 
//						TAG, 
//						"sendDataOfTable - Edit", 
//						HelperExcecao.getDescricao(ex), 
//						SingletonLog.TIPO.SINCRONIZADOR);
//			}
//			//else Log.e(TAG, "Error desconhecido");
//			return CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//		} finally{
//			
//		}
//	}
//	
//	private CODIGO_RETORNO_ENVIO_DOS_DADOS procedureInsert(
//			String pNomeTabela,
//			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb sincronizador,
//			ContainerFastSendData container){
//		
//		try{
//			
//			
//			Table objTable = sincronizador.getDatabase().factoryTable(pNomeTabela);
//			//		Insere novas tuplas ainda nao sincronizadas, caso sejam duplicadas entao atualiza o id da tupla em questao com o 
//			//		id da tupla ja existente
//			ArrayList<ContainerEXTDAO> vListContainerQueryInsert = container.getListInsertContainerEXTDAOOfTable(pNomeTabela);
//			if(vListContainerQueryInsert != null){
//				if(vListContainerQueryInsert.size() > 0 ){	
//					ArrayList<String> idsToEdit= new ArrayList<String>(); 
//					for (ContainerEXTDAO vContainerQueryInsert : vListContainerQueryInsert) {
//						ArrayList<String> vListOldId = vContainerQueryInsert.getListIdEXTDAO();
//						String[] vVetorListOldId = new String[vListOldId.size()];
//						vListOldId.toArray(vVetorListOldId );
//						//Recebe nova lista de insercao
//						String newIds[] = postInsert(sincronizador.getDatabase(), objTable, vContainerQueryInsert);
//						
//						if(newIds != null){
//							if(newIds.length > 0){
//								int i = 0 ;
//								ArrayList<Integer> idsDuplicada = new ArrayList<Integer>();
//								String getDuplicada = "";
//
//								for (String vNewId : newIds) {
//									if(vNewId.compareTo(OmegaConfiguration.CODIGO_DUPLICADA) == 0){
//
//										String vIdTable = vContainerQueryInsert.getId(i);
//										objTable.clearData();
//
//										if(objTable.select(vIdTable)){
//											String whereDuplicada = objTable.getStrQueryWhereUniqueId();
//											if(whereDuplicada != null){
//												if(getDuplicada.length() == 0 )
//													getDuplicada += whereDuplicada;
//												else
//													getDuplicada +=  OmegaConfiguration.DELIMITER_QUERY_LINHA + whereDuplicada;
//												idsDuplicada.add(i);
//											}	
//										}
//										
//									}else{
//										objTable.onSynchronizing(context, vNewId, vListOldId.get(i));
//									}
//									i += 1;
//								}
//								//Caso a insercao tenha falhado por causa da duplicidade
//								if(getDuplicada.length() > 0){
//									//TODO criar no futuro modo de restauracao de lista de duplicadas
//									boolean validadeDuplicada = false;
//									for(int k = 0 ; k < 3 ; k++){
//										String vRetorno =  HelperHttpPost.getConteudoStringPost(
//												context,
//												OmegaConfiguration.URL_REQUEST_VETOR_QUERY_SELECT_AND_GET_LIST_ID(), 
//												new String[] {"usuario", "id_corporacao", "corporacao", "senha", "nome_tabela", "query"}, 
//												new String[] {OmegaSecurity.getIdUsuario(), OmegaSecurity.getIdCorporacao(), OmegaSecurity.getCorporacao(), OmegaSecurity.getSenha(), pNomeTabela, getDuplicada});
//										if(vRetorno != null) {
//											if(vRetorno.length() > 0 ){
//												validadeDuplicada = true;
//												if(vRetorno.contains(OmegaConfiguration.CODIGO_INEXISTENTE))
//													return CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//												String[] vVetorTokenDuplicada = vRetorno.split(OmegaConfiguration.DELIMITER_QUERY_UNIQUE_LINHA);
//												int j = 0;
//												for (Integer vIdDuplicada : idsDuplicada) {
//													idsToEdit.add(vVetorTokenDuplicada[j]);
//													newIds[vIdDuplicada] = vVetorTokenDuplicada[j];
//													j += 1;
//												}
//												break;
//											}
//										}	
//									}
//									if(!validadeDuplicada){
//										return CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//									}
//								}
//
//								Table vObj = sincronizador.getDatabase().factoryTable(pNomeTabela);
//								//TODO se nao for valido deve ser executado a reinicializacao do banco
//								vObj.updateListIdToNewId(vVetorListOldId, newIds);
//								//							Atualiza as tabelas dependentes com o id da nova tabela
//								//							vObj.updateListIdInListTableDependent(vVetorListOldId, vListNewId);
//								ArrayList<String> vListIdSincronizador = vContainerQueryInsert.getListIdEXTDAOSincronizador();
//
//								for (String vIdSincronizador : vListIdSincronizador) {
//									
//									Integer vValidadeDelete = sincronizador.remove(vIdSincronizador, false);
//									if(vValidadeDelete == null){
//										SingletonLog.insereErro(
//												TAG, 
//												"sendDataOfTable", 
//												"Falha de deleuuo de um registro do sincronizador android para web",
//												SingletonLog.TIPO.SINCRONIZADOR);
//										
//									}
//
//								}	
//								for (String vIdToEdit : idsToEdit) {
//									sincronizador.clearData();
//									sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TIPO_OPERACAO_ID_INT, EXTDAOSistemaTipoOperacaoBanco.INDEX_OPERACAO_EDIT);
//									sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.USUARIO_ID_INT, OmegaSecurity.getIdUsuario());
//									sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
//									sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_TABELA_ID_INT, EXTDAOSistemaTabela.getIdSistemaTabela(context, pNomeTabela));
//									sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.ID_TABELA_INT, vIdToEdit);
//									sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.IMEI, HelperPhone.getIMEI());
//									sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_PRODUTO_INT, BibliotecaNuvemSharedPreference.getValor(context, TIPO_STRING.SISTEMA_PRODUTO_ID));
//									sincronizador.setAttrValue(EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.SISTEMA_PROJETOS_VERSAO_INT, BibliotecaNuvemSharedPreference.getValor(context, TIPO_STRING.SISTEMA_PROJETOS_VERSAO_ID));
//									Long vIdSincronizador = sincronizador.insert(false);
//
//									if(vIdSincronizador != null){
//										Log.e("procedureInsert", "Falha de inseruuo de registro duplicado");
//										SingletonLog.insereErro(
//												TAG, 
//												"sendDataOfTable", 
//												"Falha de inseruuo de registro para ediuuo de um registro que ju existia no banco web",
//												SingletonLog.TIPO.SINCRONIZADOR);
//										return CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//									}
//								}
//							} else {
//								//TODO deve ser forcada a reinicializacao do banco de dados
//								SingletonLog.insereErro(
//										TAG, 
//										"sendDataOfTable", 
//										"Falha ao requisitar os dados do banco da web para inseruuo no banco android, da tabela: " + pNomeTabela,
//										SingletonLog.TIPO.SINCRONIZADOR);
//								return CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//
//							}
//						} else {
////							TODO deve ser forcada a reinicializacao do banco de dados
//							SingletonLog.insereErro(
//									TAG, 
//									"sendDataOfTable", 
//									"Servidor fora do ar", 
//									SingletonLog.TIPO.SINCRONIZADOR);
//							return CODIGO_RETORNO_ENVIO_DOS_DADOS.SERVIDOR_FORA_DO_AR;
//						}
//					}
//				}
//			}
//		}
//		catch(Exception ex){
//			SingletonLog.insereErro( 
//					TAG, 
//					"sendDataOfTable - Insert", 
//					HelperExcecao.getDescricao(ex), 
//					SingletonLog.TIPO.SINCRONIZADOR);
//			
//			return CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//		}  
//		return CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//	}
//
//	
//	
//	private CODIGO_RETORNO_ENVIO_DOS_DADOS procedureDelete(
//			String pNomeTabela, 
//			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb sincronizador, 
//			ContainerFastSendData container){
//		
//		try{	
//			
//			ArrayList<ContainerEXTDAO> containersRemoveEXTDAO = container.getListRemoveContainerEXTDAOOfTable(pNomeTabela);
//			//		Remove somente as tuplas ja sincronizadas
//			for (ContainerEXTDAO containerEXTDAO : containersRemoveEXTDAO) {
//				String vIdTabelaSincronizador = EXTDAOSistemaTabela.getIdSistemaTabela(sincronizador.getDatabase(), containerEXTDAO.getNameTable());
//
//				ArrayList<String> identificadoresDaTabela = containerEXTDAO.getListIdEXTDAO();
//				
//				String strIdentificadoresLocal = containerEXTDAO.getStringVetorId();
//				if(strIdentificadoresLocal.length() == 0 ) continue;
//				else{
//					String post = HelperHttpPost.getConteudoStringPost(
//							context,
//							OmegaConfiguration.URL_REQUEST_RECEIVE_LIST_REMOVE_DATABASE(),
//							new String[] {
//									"usuario",
//									"id_corporacao",
//									"corporacao", 
//									"senha", 
//									"imei",
//									"id_programa",
//									"vetor_id_remove", 
//									"nome_tabela", 
//									"sistema_tabela"}, 
//							new String[] {
//									OmegaSecurity.getIdUsuario(), 
//									OmegaSecurity.getIdCorporacao(),
//									OmegaSecurity.getCorporacao(), 
//									OmegaSecurity.getSenha(),
//									HelperPhone.getIMEI(),
//									ContainerPrograma.getIdPrograma(),
//									strIdentificadoresLocal, 
//									containerEXTDAO.getNameTable(), 
//									vIdTabelaSincronizador});
//					if(post != null){
//						ArrayList<String> identificadoresDoSincronizador = containerEXTDAO.getListIdEXTDAOSincronizador();
//						for(int i = 0 ; i < identificadoresDaTabela.size(); i++){
//							
//							String vIdSincronizador = identificadoresDoSincronizador.get(i);
//							sincronizador.remove(vIdSincronizador, false);
//						}
//					} else return CODIGO_RETORNO_ENVIO_DOS_DADOS.SERVIDOR_FORA_DO_AR;
//				}
//			}
//			
//		}
//		catch(Exception ex){
//			SingletonLog.insereErro( 
//					TAG, 
//					"sendDataOfTable - Delete", 
//					HelperExcecao.getDescricao(ex), 
//					SingletonLog.TIPO.SINCRONIZADOR);
//
//			return CODIGO_RETORNO_ENVIO_DOS_DADOS.ERRO_FATAL;
//		} 
//		return CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//	}
//	
//	private void atualizaEstado(CODIGO_RETORNO_ENVIO_DOS_DADOS estado , CODIGO_RETORNO_ENVIO_DOS_DADOS codigo){
//		switch (codigo) {
//		case ERRO_FATAL:
//			estado = codigo;
//			break;
//		case OPERACAO_REALIZADA_COM_SUCESSO:
//			if(estado == null) estado = codigo;
//			break;
//		case SERVIDOR_FORA_DO_AR:
//			if(estado == null || estado == CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO)
//				estado = codigo;
//			break;
//		
//			
//		default:
//			break;
//		}
//	}
//
//	public CODIGO_RETORNO_ENVIO_DOS_DADOS sendDataOfTable(String nomeTabela){
//		try{
//		
//			if(OmegaConfiguration.DEBUGGING )
//				Log.d("sendDataOfTable", "Inicia envio dos dados da tabela: " + nomeTabela);
//			boolean validade = true;
//			Database db = new DatabasePontoEletronico(context);
//			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb sincronizador = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(db);
//			
//			ContainerFastSendData container = sincronizador.getContainerFastSendDataOfTable(nomeTabela);
//			if(container == null ) return CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//			
//			CODIGO_RETORNO_ENVIO_DOS_DADOS estado = CODIGO_RETORNO_ENVIO_DOS_DADOS.OPERACAO_REALIZADA_COM_SUCESSO;
//			
//			
//			CODIGO_RETORNO_ENVIO_DOS_DADOS codigo = procedureDelete(nomeTabela, sincronizador, container);
//			if(codigo == CODIGO_RETORNO_ENVIO_DOS_DADOS.SERVIDOR_FORA_DO_AR) return codigo;
//			else atualizaEstado(estado, codigo);
//			
//			codigo = procedureInsert(nomeTabela, sincronizador, container);
//			if(codigo == CODIGO_RETORNO_ENVIO_DOS_DADOS.SERVIDOR_FORA_DO_AR) return codigo;
//			else atualizaEstado(estado, codigo);
//			
//			codigo = procedureEdit(nomeTabela, sincronizador, container);
//			if(codigo == CODIGO_RETORNO_ENVIO_DOS_DADOS.SERVIDOR_FORA_DO_AR) return codigo;
//			else atualizaEstado(estado, codigo);
//			
//			if(OmegaConfiguration.DEBUGGING ){
//				String prefixo = "";
//				if(!validade)
//					prefixo = "[ERRO] ";
//				else prefixo = "[OK] ";
//				Log.d("sendDataOfTable", prefixo + "Finalizado o envio dos dados da tabela: " + nomeTabela);
//			}
//			
//			return estado;
//		
//		} catch(Exception ex){
//			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
//			return null;
//		}
//			
//	}
//
//
//}
//
//
