package app.omegasoftware.pontoeletronico.database.synchronize;



import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;

//public class SynchronizeReceiveDatabase  {
//	public static final String TAG = "SynchronizeReceiveDatabase";
//	short searchMode;
//	int idLayout;
//	Context context;
//	
//	static String POST_KEY = "tabela";
//	public String PATH_FILE_OUT ;
//
//	public static int ERROR_POST = -1;
//	
//	Database db = null;
//	
//	String url = OmegaConfiguration.URL_REQUEST_SINCRONIZA_TABELA_ANDROID_DATABASE();
//
//	public SynchronizeReceiveDatabase(Context p_context, Database p_database){
//		if(p_context != null && p_database != null){
//			
//			context = p_context;
//			db = p_database;
//		}
//	}
//	
//	public SynchronizeReceiveDatabase(Context p_context, Database p_database, String pURL){
//		if(p_context != null && p_database != null){
//			url = pURL;
//			context = p_context;
//			db = p_database;
//		}
//	}
//	
//		
//	public String getStrQueryMultipleInsertTable(String p_nomeTabela){
//		String nomeTabelaLowerCase = null; 
//		if(p_nomeTabela == null){
//			return null;
//		} else{
//			nomeTabelaLowerCase = p_nomeTabela.toLowerCase();
//		}
//		//Log.d(TAG, "Recebendo valor da tabela: " + p_nomeTabela);
//		String vetorKey[] = {"usuario", "corporacao", "id_corporacao", "senha", "tabela"};
//		String vetorContent[] = {OmegaSecurity.getIdUsuario(), OmegaSecurity.getCorporacao(), OmegaSecurity.getIdCorporacao(),  OmegaSecurity.getSenha(), nomeTabelaLowerCase};
//		String vResultPost = null;
//		if(p_nomeTabela.equalsIgnoreCase("usuario")){
////			vResultPost =  HelperHttpPost.getConteudoStringPost(
////					context,
////					OmegaConfiguration.URL_REQUEST_SINCRONIZA_TABELA_USUARIO_ANDROID_DATABASE(), 
////					vetorKey, 
////					vetorContent, 
////					50000);
//		} else{
////			vResultPost =  HelperHttpPost.getConteudoStringPost(
////					context,
////					url, 
////					vetorKey, 
////					vetorContent, 
////					50000);	
//		}
//		
//		return vResultPost;
//	}
//	@Override
//	protected void finalize(){
//		try{
//			this.db.close();
//			super.finalize();
//		} catch(Exception ex){
//			
//		} catch (Throwable e) {
//			
//			e.printStackTrace();
//		}
//	}
//
////	public void procedureReceiveDataOfDatabase(){
////		
////		Database db = new DatabasePontoEletronico(context);
////		db.dropAndInitializeDatabase();
////		
////		//Table[] vetorTable = db.getVectorTable();
////		String[] vetorNomeTabela = db.getVetorNomeTabela();
////
////
////		//String[] vetorNomeTabela = {"sexo"};
////		for (String strName : vetorNomeTabela) {
////			receiveData(strName, false);
////
////		}
////		return ;
////
////	}
//	public int receiveData(String p_nomeTabela, boolean removeTodaTuplaAntesDeInserir){
//		return receiveData(p_nomeTabela, OmegaConfiguration.NUMERO_TENTATIVAS, removeTodaTuplaAntesDeInserir);
//	}
//	
//	public int receiveData(String p_nomeTabela, int numeroDeTentativas, boolean removeTodaTuplaAntesDeInserir) {
//		int numberOfRowsAffected = 0;
//		if(! db.hasTable(p_nomeTabela)) 
//			return ERROR_POST;
//		
//		
//		String query = null;
//		
//			
//		for(int i = 0 ; i < numeroDeTentativas; i ++){
//			if(i > 0 )
//				try {
//					Thread.sleep(500);
//				} catch (InterruptedException e) {
//					
//					//e.printStackTrace();
//				}
//			query = getStrQueryMultipleInsertTable(p_nomeTabela);
//			if(query != null) {
//				if(query.compareTo("FALSE 2") == 0) return 0;
//				break;
//			}
//			
//		}
//		if(query == null) 
//			return ERROR_POST;
//		else if(query.trim().length() == 0 ) return 0;
//		Table vObjTable = db.factoryTable(p_nomeTabela);
//		
//		vObjTable.clearData();
//		if(removeTodaTuplaAntesDeInserir){
//			
//			try {
//				vObjTable.removeAllTuplas(false);
//			} catch (Exception e) {
//				SingletonLog.insereErro(e, SingletonLog.TIPO.SINCRONIZADOR);
//			}
//		}
//		
//		String[] vetor  = query.split("VALUES");
//		if(vetor.length == 2){
//			String cabecalhoInsert = vetor[0];
//			String conteudoInsert = vetor[1];
//			String[] vetorInsert = conteudoInsert.split("%%");
//			for (String insert : vetorInsert) {
//				if (insert == null) continue;
//				else if(insert.trim().length() == 0 ) continue;
//
//				String queryInsert = cabecalhoInsert + " VALUES " + insert;
//				try{
//					
//					db.execSQL(queryInsert);
//					numberOfRowsAffected += 1;
//				} catch(Exception ex){
//					SingletonLog.insereErro(
//							ex, 
//							SingletonLog.TIPO.SINCRONIZADOR);
//					Log.d("receiveData", "Cause: " + ex.getCause());
//				}
//			}
//		}
//
//		return numberOfRowsAffected;
//
//	}
//
//
//}
//
//
