package app.omegasoftware.pontoeletronico.database.synchronize;



import java.util.ArrayList;

import org.json.JSONException;

import android.content.Context;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorAndroidParaWeb;
import app.omegasoftware.pontoeletronico.file.FileWriterHttpPostManager;
import app.omegasoftware.pontoeletronico.file.FileWriterManager;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
//DEPRECATED
//public class SynchronizeSendDatabase  {
//
//	short searchMode;
//	int idLayout;
//	Context context;
//
//
//	static String POST_KEY_CABECALHO = "cabecalho";
//	static String POST_KEY_CORPO = "corpo";
////	private final static String TAG = "SynchronizeSendDatabase";
//	Database database;
//
//	FileWriterManager filesManager = null;
//	boolean isInFile = false;
//	boolean isSendInformationNowWithFile = false;
//	String[] listTableToSend;
//	String URL = null;
//	EXTDAOSistemaRegistroSincronizadorAndroidParaWeb sincronizador ;
//	public enum TYPE {INDIVIDUAL_QUERY_FILE_POST, MULTIPLE_QUERY_FILE_POST, MULTIPLE_QUERY_POST};
//
//	public TYPE type;
//
//	public Database getDatabase(){
//		return database;
//	}
//
//	private SynchronizeSendDatabase(
//			Context p_context, 
//			String[] pListTableToSend,
//			String pURL ){
//
//		context = p_context;
//		database = new DatabasePontoEletronico(p_context);
//		isInFile = false;
//		URL = pURL;
//		listTableToSend = pListTableToSend;
//		sincronizador = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(database);
//	}
//
//	private SynchronizeSendDatabase(
//			Context p_context,
//			String[] pListTableToSend, 
//			String pPathToWriteInformation, 
//			String pDelimiterQuery,
//			String pURL){
//
//		context = p_context;
//		database = new DatabasePontoEletronico(p_context);
//
//		isInFile = true;
//		isSendInformationNowWithFile = true;
//		listTableToSend = pListTableToSend;
//		//		p_context.getString(R.string.app_name) + "_ReceiveSynchronizeData"
//
//		filesManager = new FileWriterHttpPostManager(
//				p_context,
//				pURL,
//				pPathToWriteInformation, 
//				pDelimiterQuery);
//		sincronizador = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(database);
//	}
//
//	private SynchronizeSendDatabase(
//			Context p_context, 
//			String[] pListTableToSend, 
//			String pPathToWriteInformation, 
//			String pDelimiterQuery, 
//			boolean pIsToSendInformationNow){
//
//		context = p_context;
//		database = new DatabasePontoEletronico(p_context);
//		isInFile = true;
//		isSendInformationNowWithFile = pIsToSendInformationNow;
//		listTableToSend = pListTableToSend;
//		sincronizador = new EXTDAOSistemaRegistroSincronizadorAndroidParaWeb(database);
//		//		p_context.getString(R.string.app_name) + "_ReceiveSynchronizeData"
//		if(pIsToSendInformationNow){
//			filesManager = new FileWriterHttpPostManager(
//					p_context,
//					OmegaConfiguration.URL_REQUEST_FILE_ZIPPER_QUERY(),
//					pPathToWriteInformation, 
//					pDelimiterQuery);
//
//		} else{
//			filesManager = new FileWriterManager( 
//					pPathToWriteInformation, 
//					pDelimiterQuery);
//		}
//
//	}
//
//
//	public void closeDatabase(){
//		if(database != null)
//			this.database.close();
//	}
//
//	public boolean getListIdOfQuery(String query){
//		if(this.isInFile) return sendDataInFile(query);
//		else return sendQueryPost(query);
//	}
//
//	public boolean sendQuery(String query){
//		if(this.isInFile) return sendDataInFile(query);
//		else return sendQueryPost(query);
//	}
//
//	//  Recebe o ContainerQueryInsert de uma Tabela, contendo varios comandos INSERT's, eh 
//	//	enviado uma consulta contendo multiplos Insert's.
//	public boolean sendStrQueryMultipleInsertTable(ContainerQueryInsert pContainerQueryInsert){
//		if( ! pContainerQueryInsert.isInitialized()) return false; 
//
//		String vQuery = pContainerQueryInsert.getStrQueryMultipleInsert();
//		return sendQuery(vQuery);
//	}
//
//	public String[] sendVetorQueryInsertTable(ContainerQueryInsert pContainerQueryInsert){
//
//		if( ! pContainerQueryInsert.isInitialized()) return null; 
//		//retorna a lista de ids inseridos
//		String[] vVetorQueryInsert = pContainerQueryInsert.getListStrQueryInsert();
//		String vQueryInsert = "";
//		for (String vStrQuery : vVetorQueryInsert) {
//			if(vQueryInsert.length() == 0)
//				vQueryInsert += vStrQuery;
//			else
//				vQueryInsert += OmegaConfiguration.DELIMITER_QUERY_LINHA +  vStrQuery;
//		}
//		return sendQueryPostAndGetListId(vQueryInsert);
//	}
//
//	//  Recebe o ContainerQueryInsert de uma Tabela, contendo varios comandos INSERT's, eh 
//	//	enviado uma consulta contendo multiplos Insert's.
//	public String getStrQueryPost(String query){
//		String vetorKey[] = {"query"};
//		String vetorContent[] = {query};
//
//		String vRetorno =  HelperHttpPost.getConteudoStringPost(
//				context,
//				URL, 
//				vetorKey, 
//				vetorContent);
//		return vRetorno;
//	}
//	@Override
//	protected void finalize(){
//
//		try{
//			if(this.database != null)
//				closeDatabase();
//			try {
//				super.finalize();
//			} catch (Throwable e) {
//				
//				e.printStackTrace();
//			}
//		} catch(Exception ex){
//
//		}
//	}
//	//  Recebe o ContainerQueryInsert de uma Tabela, contendo varios comandos INSERT's, eh 
//	//	enviado uma consulta contendo multiplos Insert's.
//	public boolean sendQueryPost(String query){
//		String vetorKey[] = {"query"};
//		String vetorContent[] = {query};
//
//		String vRetorno =  HelperHttpPost.getConteudoStringPost(
//				context,
//				URL, 
//				vetorKey, 
//				vetorContent);
//		if(vRetorno == null) return false;
//		else{
//			if(vRetorno.length() > 0 ){
//				if(vRetorno.compareTo("TRUE") == 0 ) return true;
//				else return false;
//			} else return false;
//		}
//	}
//
//	public String[] sendQueryPostAndGetListId(String query){
//		String vetorKey[] = {"query"};
//		String vetorContent[] = {query};
//
//		String vRetorno =  HelperHttpPost.getConteudoStringPost(
//				context,
//				OmegaConfiguration.URL_REQUEST_VETOR_QUERY_INSERT_AND_GET_LIST_ID(), 
//				vetorKey, 
//				vetorContent);
//		if(vRetorno == null) return null;
//		else{
//			if(vRetorno.length() > 0 ){
//				String[] vVetorToken = vRetorno.split(OmegaConfiguration.DELIMITER_QUERY_UNIQUE_LINHA);
//				return vVetorToken;
//			} else return null;
//		}
//	}
//
//	public boolean sendDataInFile(String query){
//		if(this.filesManager != null){
//			//Log.d(TAG,"writeLocationInFile(): Writing latitude and logintude in file");
//			this.filesManager.writeLogInformation(query);
//		}
//		return true;
//	}
//
//	//	Envia tabela a tabela e recebe os id's correspondentes no banco do servidor,
//	//		atualizando cada tabela dependente a ser enviada
//	//		Ex.: Funcionario, FuncionarioEmpresa
//	public boolean sendDataOfHierarchyDatabase(){
//
//		boolean vValidade = true;
//		try{
//
//			ArrayList<String> vListTabelaSync = sincronizador.getListTableToSync();
//			if(vListTabelaSync == null ) return true;
//			else if(vListTabelaSync.size() == 0 ) return true;
//			else{
//				for (String vTableToSync : vListTabelaSync) {
//					for (String vNomeTabela : listTableToSend) {
//						if(vNomeTabela.compareTo(vTableToSync) == 0 )
//							sendDataOfTable(vNomeTabela);
//					}
//				}
//			}
//		} catch(Exception ex){
//			//Log.d("sendData", ex.getMessage());
//		}
//
//		if(this.isSendInformationNowWithFile){
//			FileWriterHttpPostManager vPost = (FileWriterHttpPostManager) filesManager;
//			return vPost.launchUploaderTask();
//		}
//		return vValidade;
//	}
//
//	public void sendAllDataOfTable(String pNomeTabela){
//
//	}
//
//	public void sendDataOfTable(String pNomeTabela){
//		
//		ContainerSendData vContainerSendDataOfTable;
//		try {
//			vContainerSendDataOfTable = sincronizador.getContainerSendDataOfTable(pNomeTabela);
//		
//		boolean vValidade = false;
//		ArrayList<ContainerQueryEXTDAO> vListContainerQueryRemoveEXTDAO = vContainerSendDataOfTable.getListRemoveContainerQueryEXTDAO();
//		//		Remove somente as tuplas ja sincronizadas
//		for (ContainerQueryEXTDAO vContainerQueryEXTDAO : vListContainerQueryRemoveEXTDAO) {
//			ArrayList<String> vListQuery = vContainerQueryEXTDAO.getListCorpo();
//			ArrayList<String> vListIdSincronizador = vContainerQueryEXTDAO.getListIdEXTDAOSincronizador();
//			if(vListQuery.size() == 0 ) continue;
//			else{
//				for(int i = 0 ; i < vListQuery.size(); i++){
//					String vStrQuery = vListQuery.get(i);
//					String vIdSincronizador = vListIdSincronizador.get(i);
//					if(sendQuery(vStrQuery)){
//						try {
//							sincronizador.remove(vIdSincronizador, false);
//						} catch (Exception e) {
//							
//							e.printStackTrace();
//						}
//					} 
//				}
//			}
//		}
//		//		Insere novas tuplas ainda nao sincronizadas, caso sejam duplicadas entao atualiza o id da tupla em questao com o 
//		//		id da tupla ja existente
//		ArrayList<ContainerQueryInsert> vListContainerQueryInsert = vContainerSendDataOfTable.getListContainerInsert();
//		if(vListContainerQueryInsert != null){
//			if(vListContainerQueryInsert.size() > 0 ){	
//				for (ContainerQueryInsert vContainerQueryInsert : vListContainerQueryInsert) {
//					ArrayList<String> vListOldId = vContainerQueryInsert.getIdsEXTDAO();
//					String[] vVetorListOldId = new String[vListOldId.size()];
//					vListOldId.toArray(vVetorListOldId );
//					//					Recebe nova lista de insercao
//					String vListNewId[] = sendVetorQueryInsertTable(vContainerQueryInsert);
//
//					if(vListNewId != null){
//						if(vListNewId.length > 0){
//							int i = 0 ;
//							ArrayList<Integer> vListIdDuplicada = new ArrayList<Integer>();
//							String vQueryDuplicada = "";
//							Table vObjTableDuplicada = database.factoryTable(vContainerQueryInsert.getNameTable());
//							for (String vNewId : vListNewId) {
//								if(vNewId.compareTo(OmegaConfiguration.CODIGO_DUPLICADA) == 0){
//
//									String vIdTable = vContainerQueryInsert.getId(i);
//									vObjTableDuplicada.setAttrValue( Table.ID_UNIVERSAL, vIdTable);
//									try {
//										vObjTableDuplicada.select();
//									} catch (Exception e) {
//										
//										e.printStackTrace();
//									}
//									String vQueryTuplaDuplicada = vObjTableDuplicada.getStrQueryWhereUniqueId();
//									if(vQueryTuplaDuplicada != null){
//										if(vQueryDuplicada.length() == 0 )
//											vQueryDuplicada += vQueryTuplaDuplicada;
//										else
//											vQueryDuplicada +=  OmegaConfiguration.DELIMITER_QUERY_LINHA + vQueryTuplaDuplicada;
//										vListIdDuplicada.add(i);
//									}
//								}
//								i += 1;
//							}
//							//							Caso a insercao tenha falhado por causa da duplicidade
//							if(vQueryDuplicada.length() > 0){
//								String vRetorno =  HelperHttpPost.getConteudoStringPost(
//										context,
//										OmegaConfiguration.URL_REQUEST_VETOR_QUERY_SELECT_AND_GET_LIST_ID(), 
//										new String[] {"usuario", "id_corporacao", "corporacao", "senha", "nome_tabela", "query"}, 
//										new String[] {OmegaSecurity.getIdUsuario(),OmegaSecurity.getIdCorporacao(), OmegaSecurity.getCorporacao(), OmegaSecurity.getSenha(), pNomeTabela, vQueryDuplicada});
//								if(vRetorno != null) {
//									if(vRetorno.length() > 0 ){
//										String[] vVetorTokenDuplicada = vRetorno.split(OmegaConfiguration.DELIMITER_QUERY_UNIQUE_LINHA);
//										int j = 0;
//										for (Integer vIdDuplicada : vListIdDuplicada) {
//											vListNewId[vIdDuplicada] = vVetorTokenDuplicada[j];
//											j += 1;
//										}		
//									}
//								}
//							}
//
//							Table vObj = database.factoryTable(pNomeTabela);
//							try {
//								vObj.updateListIdToNewId(vVetorListOldId, vListNewId);
//							} catch (JSONException e1) {
//								
//								e1.printStackTrace();
//							} catch (Exception e1) {
//								
//								e1.printStackTrace();
//							}
//							//							Atualiza as tabelas dependentes com o id da nova tabela
//							//							vObj.updateListIdInListTableDependent(vVetorListOldId, vListNewId);
//
//							ArrayList<String> vListIdSincronizador = vContainerQueryInsert.getListIdEXTDAOSincronizador();
//
//							for (String vIdSincronizador : vListIdSincronizador) {
//								//						TODO retirar comentario abaixo
//								try {
//									sincronizador.remove(vIdSincronizador, false);
//								} catch (Exception e) {
//									
//									e.printStackTrace();
//								}
//							}	
//						} else {
//							if(vValidade) vValidade = false;
//
//						}
//					} else {
//						if(vValidade) vValidade = false;
//					}
//				}
//			}
//		}				
//		//Atualiza Tuplas ja sincronizadas
//		ArrayList<ContainerQueryEXTDAO> vListContainerQueryUpdateEXTDAO = vContainerSendDataOfTable.getListUpdateContainerQueryEXTDAO();
//
//		for (ContainerQueryEXTDAO vContainerQueryEXTDAO : vListContainerQueryUpdateEXTDAO) {
//			ArrayList<String> vListQuery = vContainerQueryEXTDAO.getListCorpo();
//			ArrayList<String> vListIdSincronizador = vContainerQueryEXTDAO.getListIdEXTDAOSincronizador();
//			if(vListQuery.size() == 0 ) continue;
//			else{
//				for(int i = 0 ; i < vListQuery.size(); i++){
//					String vStrQuery = vListQuery.get(i);
//					String vIdSincronizador = vListIdSincronizador.get(i);
//					if(sendQuery(vStrQuery)){
//						//							TODO retirar comentario abaixo
//						try {
//							sincronizador.remove(vIdSincronizador, false);
//						} catch (Exception e) {
//							
//							e.printStackTrace();
//						}
//					} else if(vValidade) vValidade = false;
//				}
//			}
//		}
//		closeDatabase();
//		} catch (Exception e2) {
//			
//			e2.printStackTrace();
//		}
//	}
//	// Envia as tabelas que nao possuem dependencia, Funcionario, Empresa, 
//	//	public boolean sendDataOfIdependentTable(){
//	//		boolean vValidade = false;
//	//		try{
//	//
//	//			EXTDAOSincronizador sincronizador = new EXTDAOSincronizador(database);
//	//			ContainerSendData vContainerSendData = sincronizador.getContainerSendData();
//	//
//	//			ArrayList<ContainerQueryInsert> vListContainerQueryInsert = vContainerSendData.getListContainerInsert();
//	//			for (ContainerQueryInsert vContainerQueryInsert : vListContainerQueryInsert) {
//	//				if(sendStrQueryMultipleInsertTable(vContainerQueryInsert)){
//	//
//	//					ArrayList<String> vListIdSincronizador = vContainerQueryInsert.getListIdEXTDAOSincronizador();
//	//					if(!vValidade ) vValidade = true;
//	//					for (String vIdSincronizador : vListIdSincronizador) {
//	//						//						TODO retirar comentario abaixo
//	//						sincronizador.remove(vIdSincronizador, false);
//	//					}
//	//				}
//	//			}
//	//
//	//			ArrayList<ContainerQueryEXTDAO> vListContainerQueryEXTDAO = vContainerSendData.getListUpdateContainerQueryEXTDAO();
//	//			for (ContainerQueryEXTDAO vContainerQueryEXTDAO : vListContainerQueryEXTDAO) {
//	//				ArrayList<String> vListQuery = vContainerQueryEXTDAO.getListCorpo();
//	//				ArrayList<String> vListIdSincronizador = vContainerQueryEXTDAO.getListIdEXTDAOSincronizador();
//	//				if(vListQuery.size() == 0 ) continue;
//	//				else{
//	//
//	//					for(int i = 0 ; i < vListQuery.size(); i++){
//	//						if(!vValidade ) vValidade = true;
//	//						String vStrQuery = vListQuery.get(i);
//	//						String vIdSincronizador = vListIdSincronizador.get(i);
//	//						if(sendQuery(vStrQuery)){
//	//							//							TODO retirar comentario abaixo
//	//							sincronizador.remove(vIdSincronizador, false);
//	//						}
//	//					}
//	//				}
//	//			}
//	//
//	//		} catch(Exception ex){
//	//			Log.d("sendData", ex.getMessage());
//	//		}
//	//		if(this.isSendInformationNowWithFile && vValidade){
//	//			FileWriterHttpPostManager vPost = (FileWriterHttpPostManager) filesManager;
//	//			vPost.launchUploaderTask();
//	//		}
//	//		return vValidade;
//	//
//	//	}
//
//
//}
//
//
