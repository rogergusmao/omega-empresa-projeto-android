package app.omegasoftware.pontoeletronico.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.content.Context;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.listener.DateClickListener;

public class ContainerClickListenerDate {
	public Integer mes=null;
	public Integer dia=null;
	public Integer ano=null;
	private Button button;

	DateClickListener clickListener = null;
	

	
	public ContainerClickListenerDate(Button pButton, DateClickListener vListener){

        final Calendar c = Calendar.getInstance();
        ano = c.get(Calendar.YEAR);
        mes = c.get(Calendar.MONTH);
        dia = c.get(Calendar.DAY_OF_MONTH);
        button = pButton;
        clickListener = vListener;
		
	}
	
	public GregorianCalendar getGregorianCalendar(){
		return new GregorianCalendar(ano, mes, dia);
	}
	
	public void setDateOfView(Context c, Integer pAno, Integer pMes, Integer pDia){
		if(pAno != null && pMes != null && pDia != null){
		ano = pAno;
		mes = pMes;
		dia = pDia;
		
		clickListener.setDateOfView(c, pAno, pMes, pDia);
		}
	}
	
	public Date getDate(){
		return new Date(ano,mes,dia);
	}
	
	public TextView getButton(){
		return button;
	}
	public void setTextView(Context c){

		SimpleDateFormat sdf = new SimpleDateFormat(HelperDate.DATEFORMAT(c));
		GregorianCalendar gc = getGregorianCalendar();
		String data = sdf.format(gc.getTime());
		button.setText(data);
	}
	
	public Integer getAno(){
		return ano;
	}
	
	public Integer getMes(){
		return mes;
	}
	
	public Integer getDia(){
		return dia;
	}
	
	public void setAno(Integer pAno){
		ano = pAno;
	}
	
	public void setMes(Integer pMes){
		mes = pMes;
	}
	
	public void setDia(Integer pDia){
		dia = pDia;
	}
	public interface OnChangeDate{
		public abstract void onChange(DatePicker view, int year, int monthOfYear,
	            int dayOfMonth);
	}
	public OnChangeDate getOnChangeDate(){
		return onChangeDate;
	}
	OnChangeDate onChangeDate = null;
	public void setOnChangeDate(OnChangeDate onChangeDate){
    	this.onChangeDate=onChangeDate;  
    }
}
