package app.omegasoftware.pontoeletronico.date;

import java.sql.Time;
import java.util.Calendar;


import android.widget.Button;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.listener.HourClickListener;

public class ContainerClickListenerHour {
	
	public Integer minuto=null;
	public Integer hora=null;
	private Button button;
	HourClickListener listener;
	
	public ContainerClickListenerHour(
			Button pButton, 
			HourClickListener pListener){

        final Calendar c = Calendar.getInstance();
        hora = c.get(Calendar.HOUR);
    
        minuto = c.get(Calendar.MINUTE);
        button = pButton;
        listener = pListener;
	}
	
	public Time getTime(){
		
		return new Time(hora, minuto, 0);
	}
	
	public TextView getButton(){
		return button;
	}
	public void setTextView(){
		button.setText(new StringBuilder()
        // Month is 0 based so add 1
        .append(pad(hora)).append(":")
        .append(pad(minuto)));
	}
	private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }
	public Integer getHora(){
		return hora;
	}
		
	public Integer getMinuto(){
		return minuto;
	}
	
	public void setHora(Integer pHora){
		hora = pHora;
	}
	
	public void setOfTimeView(Integer pHour, Integer pMinute){
		if(pHour != null && pMinute != null){
			hora = pHour;
			minuto = pMinute;
			listener.setTimeOfView(pHour, pMinute);
		}
	}
	
	public void setMinuto(Integer pMinuto){
		minuto = pMinuto;
	}
}
