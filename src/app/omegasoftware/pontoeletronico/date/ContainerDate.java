package app.omegasoftware.pontoeletronico.date;

import java.util.Calendar;


public class ContainerDate {
	public Integer mes=null;
	public Integer dia=null;
	public Integer ano=null;
	
	
	public ContainerDate(){

        final Calendar c = Calendar.getInstance();
        ano = c.get(Calendar.YEAR);
        mes = c.get(Calendar.MONTH);
        dia = c.get(Calendar.DAY_OF_MONTH);
	
	}
	
	public ContainerDate(int ano, int mes, int dia){

        
		this.ano = ano;
		this.mes = mes;
		this.dia = dia;
	
	}
	
	

	
	public Integer getAno(){
		return ano;
	}
	
	public Integer getMes(){
		return mes;
	}
	
	public Integer getDia(){
		return dia;
	}
	
	public void setAno(Integer pAno){
		ano = pAno;
	}
	
	public void setMes(Integer pMes){
		mes = pMes;
	}
	
	public void setDia(Integer pDia){
		dia = pDia;
	}
}
