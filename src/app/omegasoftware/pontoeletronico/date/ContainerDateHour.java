package app.omegasoftware.pontoeletronico.date;

public class ContainerDateHour {
	public Integer tempoHora=null;
	public Integer tempoMinuto=null;
	public Integer tempoDia=null;

	public ContainerDateHour(Integer pTempoDia, Integer pTempoHora, Integer pTempoMinuto){
		tempoHora = pTempoHora;
		tempoMinuto = pTempoMinuto;
		tempoDia = pTempoDia;
	}
}
