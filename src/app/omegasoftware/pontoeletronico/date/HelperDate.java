/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package app.omegasoftware.pontoeletronico.date;



import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import android.app.Activity;
import android.content.Context;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

/**
 * Basic example of using date and time widgets, including
 * {@link android.app.TimePickerDialog} and {@link android.widget.DatePicker}.
 *
 * Also provides a good example of using {@link Activity#onCreateDialog},
 * {@link Activity#onPrepareDialog} and {@link Activity#showDialog} to have the
 * activity automatically save and restore the state of the dialogs.
 */
public class HelperDate{

	// where we display the selected date and time
	//TODO colocar no string.xml - baseado na lingua
	static  String __DATETIMEFORMAT = null;
	static  String __DATETIMEMILISECONDSFORMAT = null;
	static  String __DATEFORMAT = null;
	static  String __TIMEFORMAT = null;
	
	
	public static String DATETIMEMILISECONDSFORMAT(Context c){
		if(__DATETIMEMILISECONDSFORMAT != null)
			return __DATETIMEMILISECONDSFORMAT;
		
		__DATETIMEMILISECONDSFORMAT = c.getString(R.string.datetimemiliseconds_format);
		return __DATETIMEMILISECONDSFORMAT;
	}
	public static String DATETIMEFORMAT(Context c){
		if(__DATETIMEFORMAT != null)
			return __DATETIMEFORMAT;
		
		__DATETIMEFORMAT = c.getString(R.string.datetime_format);
		return __DATETIMEFORMAT;
	}
	public static String DATEFORMAT(Context c){
		if(__DATEFORMAT != null)
			return __DATEFORMAT;
		
		__DATEFORMAT = c.getString(R.string.date_format);
		return __DATEFORMAT;
	}
	public static String TIMEFORMAT(Context c){
		if(__TIMEFORMAT != null)
			return __TIMEFORMAT;
		
		__TIMEFORMAT = c.getString(R.string.time_format);
		return __TIMEFORMAT;
	}
	// date and time
	private Integer mYear;
	//[0, 11]
	private Integer mMonth0a11;
	//[0, 31]
	private Integer mDay;
	private Integer mHour;
	private Integer mMinute;
	private Integer mSegundos;
	
	public HelperDate() {

		final Calendar ca = Calendar.getInstance();
		mYear = ca.get(Calendar.YEAR);
		mMonth0a11 = ca.get(Calendar.MONTH);
		mDay = ca.get(Calendar.DAY_OF_MONTH);
		mHour = ca.get(Calendar.HOUR_OF_DAY);
		mMinute = ca.get(Calendar.MINUTE);
		mSegundos = ca.get(Calendar.SECOND);
		
	}
	
	
	public enum TIPO_DATA{
		DATE, 
		TIME
	}
	
	public TIPO_DATA getTipoData(){
		if(mYear != null && mMonth0a11 != null && mDay != null && mHour != null && mMinute != null)
			return TIPO_DATA.DATE;
		else return TIPO_DATA.TIME;
	}
	public Date getTime(){
		if(mHour != null && mMinute != null){
			return new Time(mHour, mMinute, 0);
		} else return null;
		
	}
	
	public static int getDiaSemana(){
		
		Date d = new Date();
		
		GregorianCalendar calendar = new GregorianCalendar();
		
		int i = calendar.get(Calendar.DAY_OF_WEEK);
		return i;
	}
	
	public int getSemana(){
		SimpleDateFormat sdf = new SimpleDateFormat("W");
		Date d = new Date();
		
		String dayOfTheWeek = sdf.format(d);
		
		return HelperInteger.parserInt(dayOfTheWeek);
	}
	public HelperDate(Date d){
		Calendar ca = toCalendar(d);
		mYear = ca.get(Calendar.YEAR);
		mMonth0a11 = ca.get(Calendar.MONTH);
		mDay = ca.get(Calendar.DAY_OF_MONTH);
		mHour = ca.get(Calendar.HOUR_OF_DAY);
		mMinute = ca.get(Calendar.MINUTE);
		mSegundos = ca.get(Calendar.SECOND);
	}
	
	
	public Date getDate(){
		if(mYear != null && mMonth0a11 != null && mDay != null && mHour != null && mMinute != null && mSegundos != null){
			return new Date(mYear - 1900, mMonth0a11, mDay, mHour, mMinute , mSegundos);
		} else if(mYear != null && mMonth0a11 != null && mDay != null ){
			return new Date(mYear - 1900, mMonth0a11, mDay);
		}else return null;
		
	}
	
	public GregorianCalendar getGregorianCalendar(){
		if(mYear != null && mMonth0a11 != null && mDay != null && mHour != null && mMinute != null && mSegundos != null){
			return new GregorianCalendar(mYear, mMonth0a11, mDay, mHour, mMinute , mSegundos);
		} else if(mYear != null && mMonth0a11 != null && mDay != null ){
			return new GregorianCalendar(mYear, mMonth0a11, mDay);
		}else return null;
		
	}

	public HelperDate(int ano, int mes0a11, int diaMes){
		mYear = ano;
		mMonth0a11 = mes0a11;
		mDay = diaMes;
		
	}
	//	2012-03-22 11:00 || 2012-03-22 || 11:00 
	public HelperDate(String pDateSQL){
		if(pDateSQL != null){
			String vVetorDateTime[] = pDateSQL.split("[ ]+");
			//se formato americano
			if(vVetorDateTime[0].contains("-")){
				
				String vVetorDate[] = vVetorDateTime[0].split("[-]+");
				if(vVetorDate != null && vVetorDate.length == 3){
					mYear = HelperInteger.parserInt(vVetorDate[0]);
					mMonth0a11 = HelperInteger.parserInt(vVetorDate[1]) - 1;
					mDay = HelperInteger.parserInt(vVetorDate[2]);
				}
			} else {
				String vVetorDate[] = vVetorDateTime[0].split("[/]+");
				if(vVetorDate != null && vVetorDate.length == 3){
					mYear = HelperInteger.parserInt(vVetorDate[2]);
					mMonth0a11 = HelperInteger.parserInt(vVetorDate[1]) - 1;
					mDay = HelperInteger.parserInt(vVetorDate[0]);
				}
			}

			String vStrTime = null;
			if(vVetorDateTime.length == 2){
				vStrTime = vVetorDateTime[1];
				
			} else if(pDateSQL.contains(":")){
				//se nao contem o date e contem somente time
				vStrTime = pDateSQL;
			}
			if(vStrTime != null){
				String vVetorTime[] = vStrTime.split("[:]+");
				if(vVetorTime != null){
					if(vVetorTime.length >= 2){
						mHour = HelperInteger.parserInt(vVetorTime[0]);
						mMinute = HelperInteger.parserInt(vVetorTime[1]);
						if(vVetorTime.length >= 3){
							mSegundos = HelperInteger.parserInt(vVetorTime[2]);	
						}
					}
				}
			}
			
		}
	}

	public Integer getYear(){
		return mYear;
	}
	//[0,11]
	public Integer getMonth(){
		return mMonth0a11;
	}
	//[1, 12]
	public Integer getMonthSQL(){
		return mMonth0a11 + 1;
	}

	public Integer getDay(){
		return mDay;
	}
	
	public Integer getHour(){
		return mHour;
	}

	public Integer getMinute(){
		return mMinute;
	}
	public Integer getSegundos(){
		return mSegundos;
	}

	public static String getHoraFormatadaExibicao(String pTime){
		String vVetorTime[] = pTime.split( "[:]+");
		if(vVetorTime == null) return null; 
		else if(vVetorTime.length == 2){
			return vVetorTime[0] + ":" + vVetorTime[1] + ":00"; 
		}else if(vVetorTime.length == 3){
			return vVetorTime[0] + ":" + vVetorTime[1] + ":" + vVetorTime[2]; 
		}
		
		else return pTime ;
	}
	
	public static String getHoraFormatadaSQL(String pTime){
		String vVetorTime[] = pTime.split("[:]+");
		if(vVetorTime == null) return null; 
		else if(vVetorTime.length == 2){
			return vVetorTime[0] + ":" + vVetorTime[1] + ":00"; 
		}else if(vVetorTime.length == 3){
			return vVetorTime[0] + ":" + vVetorTime[1] + ":" + vVetorTime[2]; 
		} else return pTime ;
	}
	
	public String getHoraSQL(){
		String hora = String.format("%02d:%02d:%02d", mHour , mMinute, mSegundos);
		return hora;
	}
	
	public String getDataSQL(){
		String hora = String.format("%04d-%d-%d", mYear, mMonth0a11 + 1, mDay);
		return hora;
	}
	
	public String getDatetimeSQL(){
		String hora = String.format("%04d-%02d-%02d %02d:%02d:%02d", 
				mYear, mMonth0a11 + 1, mDay,
				mHour, mMinute, mSegundos);
		return hora;
	}

	public static String getDatetimeFortamataSQL(String datetime){
		if(datetime == null || datetime.length() == 0 ) return null;
		else {
			String[] vetor = datetime.split( "[ ]+");
			if(vetor.length == 2){
				String novaData = getDataFormatadaSQL(vetor[0]);
				String novaHora = getHoraFormatadaSQL(vetor[1]);
				return novaData + " " + novaHora;	
			} else return null;
			
		}
	}
	
	public static String getDataFormatadaSQL(String pDate){
		String vVetorData[] = pDate.split( "[-]+");
		if(vVetorData == null) return null;
		else if(vVetorData.length == 3){
			String vMes = "";
			if(vVetorData[0] != null){
				if(vVetorData[0].length() == 1 )
					vMes = "0" + vVetorData[0];
				else vMes = vVetorData[0];
			}
			String vDia = "";
			if(vVetorData[1] != null){
				if(vVetorData[1].length() == 1 )
					vDia = "0" + vVetorData[1];
				else vDia = vVetorData[1];
			}
			return vVetorData[2]  + "-" + vMes + "-" + vDia; 
		} else return null;
	}
	
	public static String getDataFormatadaParaExibicao(String pDate){
		String vVetorData[] = pDate.split("[-]+");
		if(vVetorData == null) return null;
		else if(vVetorData.length == 3){
			String vMes = "";
			if(vVetorData[1] != null){
				if(vVetorData[1].length() == 1 )
					vMes = "0" + vVetorData[1];
				else vMes = vVetorData[1];
			}
			String vDia = "";
			if(vVetorData[2] != null){
				if(vVetorData[2].length() == 1 )
					vDia = "0" + vVetorData[2];
				else vDia = vVetorData[2];
			}
			return   vDia + "/" + vMes + "/"  + vVetorData[0]; 
		} else return null;
	}
	
	public String getDateAndTimeForFileName(){
		StringBuilder date = new StringBuilder();

		date.append(mYear).append("_");
		// Month is 0 based so add 1
		date.append(mMonth0a11 + 1).append("_");
		date.append(mDay).append("_");
		date.append(pad(mHour)).append("_");
		date.append(pad(mMinute));
		return date.toString();
	}

	public String getDateAndTimeDisplay(){
		StringBuilder date = new StringBuilder();

		date.append(mYear).append("-");
		// Month is 0 based so add 1
		date.append(mMonth0a11 + 1).append("-");
		date.append(mDay).append(" ");
		date.append(pad(mHour)).append(":");
		date.append(pad(mMinute)).append(":");
		date.append(pad(mSegundos));
		return date.toString();
	}

	public String getDateDisplay(){
		StringBuilder date = new StringBuilder();

		date.append(mYear).append("-");
		// Month is 0 based so add 1
		date.append(mMonth0a11 + 1).append("-");
		date.append(mDay);
		return date.toString();
	}

	public String getTimeDisplay(){
		StringBuilder date = new StringBuilder();

		date.append(pad(mHour)).append(":");
		date.append(pad(mMinute)).append(":");
		date.append(pad(mSegundos));
		return date.toString();
	}

	public Calendar getCalendar(){
		Date vDate = getDate();
		if(vDate != null)
			return getCalendar(vDate);
		else return null;
	}
	
	public static Calendar getCalendar(Date pDate){
		Calendar vCalendar = Calendar.getInstance();
		vCalendar.setTime(pDate);
		return vCalendar;
	}
	
	public long getAbsDiferencaEmDias(Date pDate){
		Date vDate = getDate();
		long diff = vDate.getTime() - pDate.getTime(); //result in millis
		//(24 dias, 60 min * 1min *  1 segundo)
		double vDoubleDiff = diff / (24 * 60 * 60 * 1000);
		long vIntDiff = (long)vDoubleDiff;
		vIntDiff += 1;
		long days = Math.abs(vIntDiff);
		
		return days;
	}
	
	public long getAbsDiferencaEmMinutos(Date pDate){
		Date vDate = getDate();
		long diff = vDate.getTime() - pDate.getTime(); //result in millis
		//(24 dias, 60 min * 1min *  1 segundo)
		double vDoubleDiff = diff / (60 * 1000);
		long vIntDiff = (long)vDoubleDiff;
		vIntDiff += 1;
		long days = Math.abs(vIntDiff);
		
		return days;
	}

	public long getAbsDiferencaEmSegundos(Date pDate){
		Date vDate = getDate();
		long diff = vDate.getTime() - pDate.getTime(); //result in millis
		//(24 dias, 60 min * 1min *  1 segundo)
		double vDoubleDiff = diff / (1000);
		long vIntDiff = (long)vDoubleDiff;
		vIntDiff += 1;
		long days = Math.abs(vIntDiff);
		
		return days;
	}

	public long getAbsDiferencaEmHoras(Date pDate){
		Date vDate = getDate();
		long diff = vDate.getTime() - pDate.getTime(); //result in millis
		//(24 dias, 60 min * 1min *  1 segundo)
		double vDoubleDiff = diff / (60*60*1000);
		long vIntDiff = (long)vDoubleDiff;
		vIntDiff += 1;
		long days = Math.abs(vIntDiff);
		
		return days;
	}

	public long getAbsDiferencaEmDias(GregorianCalendar pDate){
		GregorianCalendar vDate = getGregorianCalendar();
		long diff = vDate.getTimeInMillis() - pDate.getTimeInMillis(); //result in millis
		//(24 dias, 60 min * 1min *  1 segundo)
		double vDoubleDiff = diff / (24 * 60 * 60 * 1000);
		long vIntDiff = (long)vDoubleDiff;
		vIntDiff += 1;
		long days = Math.abs(vIntDiff);
		
		return days;
	}
	
	
	

	public static ContainerDateHour getStrSomaDiaDataHora(
			Integer data, Integer hora, Integer minuto, 
			Integer dataAdd, Integer horaAdd, Integer minutoAdd){
		if(data == null) data = new Integer(0);
		if(hora == null) hora = new Integer(0);
		if(minuto == null) minuto = new Integer(0);
		if(dataAdd == null) dataAdd = new Integer(0);
		if(horaAdd == null) horaAdd = new Integer(0);
		if(minutoAdd == null) minutoAdd = new Integer(0);

		Integer vSomaMinuto = minuto + minutoAdd;

		if(vSomaMinuto >= 60){
			horaAdd += 1;
			vSomaMinuto -= 60;
		}
		Integer vSomaHora = hora + horaAdd;

		int vSomaData = data + dataAdd;
		if(vSomaHora >= 24 ){
			vSomaHora -= 24;
			vSomaData += 1;
		}
		return new ContainerDateHour(vSomaData, vSomaHora , vSomaMinuto);
	}

	public static ContainerDateHour getStrSomaDataHora(Integer hora, Integer minuto, Integer horaAdd, Integer minutoAdd){


		if(hora == null) hora = new Integer(0);
		if(minuto == null) minuto = new Integer(0);

		if(horaAdd == null) horaAdd = new Integer(0);
		if(minutoAdd == null) minutoAdd = new Integer(0);

		Integer vSomaMinuto = minuto + minutoAdd;

		if(vSomaMinuto >= 60){
			horaAdd += 1;
			vSomaMinuto -= 60;
		}
		Integer vSomaHora = hora + horaAdd;


		return new ContainerDateHour(null, vSomaHora , vSomaMinuto);
	}

	public static String getDatetimeAtualFormatadaParaExibicao(Context c){

		SimpleDateFormat sdf = new SimpleDateFormat(DATETIMEFORMAT(c));
		Date agora = new Date();

		return sdf.format(agora);
	}
	
	
	SimpleDateFormat sdfDatetime = null;
	private SimpleDateFormat getSDFDatetime(Context c){
		if(sdfDatetime == null)
			sdfDatetime = new SimpleDateFormat(DATETIMEFORMAT(c));
		return sdfDatetime;
	}
	
	SimpleDateFormat sdfDatetimeMilisecond = null;
	private SimpleDateFormat getSDFDatetimeMilisecond(){
		if(sdfDatetimeMilisecond == null)
			sdfDatetimeMilisecond = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		return sdfDatetimeMilisecond;
	}
	public String getDatetimeAtualFormatadaParaSQLFast(Context c){

		SimpleDateFormat sdf = getSDFDatetime(c);
		Date agora = new Date();

		return sdf.format(agora);
	}
	
	public String getDatetimeComMilisegundoSQL(){
		SimpleDateFormat sdf = getSDFDatetimeMilisecond();
		Date agora = new Date();

		return sdf.format(agora);
		
	}
	
	public String formataDatetimeComMilisegundosSQL(Date agora){
		SimpleDateFormat sdf = getSDFDatetimeMilisecond();
		
		return sdf.format(agora);
	}
	public static String getDateAtualFormatadaParaSQL(){

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date agora = new Date();

		return sdf.format(agora);
	}

	public static String getDataFormatadaPorExtenso(Date d){
		if(d==null)return null;
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
		return sdf.format(d);
	}
	
	public static String getDatetimeParaNomeDeArquivo(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		Date agora = new Date();

		return sdf.format(agora);
	}
	public static String getDateParaNomeDeArquivo(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date agora = new Date();

		return sdf.format(agora);
	}
	public static String getNomeArquivoGeradoACadaXMinutos(){
		return getNomeArquivoGeradoACadaXMinutos(30);
	}
	//Utilize 30 como default
	public static String getNomeArquivoGeradoACadaXMinutos(int maxMinParaNovoArquivo){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
		Date agora = new Date();

		String token = sdf.format(agora);
		String strMin= token.substring(10);
		Integer min = HelperInteger.parserInt(strMin);
		Integer x =  min / 30 * 30;
		//min = (min % (60 / maxMinParaNovoArquivo))*maxMinParaNovoArquivo;
		
		String nome = token.substring(0, 8) + "_"+ token.substring(8,10) +String.format("%02d", x) ;
		return nome;
	}
	public static String getDateAtualFormatada(Context c){

		SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT(c));
		Date agora = new Date();

		return sdf.format(agora);
	}
	SimpleDateFormat sdf = null;
	private SimpleDateFormat getSimpleDateFormat (){
		if(sdf == null)
			sdf = new SimpleDateFormat();
		
		return sdf;
	}
	public String getDateAtualFormatadaFast(){
		
		SimpleDateFormat sdf = getSimpleDateFormat();
		Date agora = new Date();

		return sdf.format(agora);
	}

	
	



	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}
	
	
	enum TEMPO_MILISEGUNDOS{
		SEGUNDOS(1, 1000, R.string.segundo, R.string.segundos),
		MINUTO(1, 60 * 1000, R.string.minuto, R.string.minutos),
		HORA(2, 60 * 60 * 1000, R.string.hora, R.string.horas),
		DIA(3, 24 * 60 * 60 * 1000, R.string.dia, R.string.dias),
		MES(4, 30 * 24 * 60 * 60 * 1000, R.string.mes, R.string.meses),
		ANO(5, 12 * 30 * 24 * 60 * 60 * 1000, R.string.ano, R.string.anos);
		long id;
		long milisegundos;
		int singular;
		int plural;
		TEMPO_MILISEGUNDOS(int id, int milisegundos, int singular, int plural){
			this.id = id;
			this.milisegundos = milisegundos;
			this.singular = singular;
			this.plural = plural;
		}
		public long getId(){
			return id;
		}
		public long getMilisegundos(){
			return milisegundos;
		}
		
		public long calculaInteiro(long diferenca){
			double d = ((double)diferenca) / getMilisegundos();
			double numero = d;
			long t = (long) numero;
			return t;
		}
		
		public String getDescricao(Context c,long diferenca){
			
			double d = ((double)diferenca) / getMilisegundos();
			double numero = d;
			long t = (long) numero;

				
			String desc = null;
			if(t == 1){
				desc = c.getResources().getString(R.string.ha)+ " " + String.valueOf(t) +" " + c.getResources().getString(singular) ;	
			} else {
				desc = c.getResources().getString(R.string.ha) + " " + String.valueOf(t) +  " " + c.getResources().getString(plural) ;
			}
			
			
			return desc;
		}
	}
	public static int[] TEMPO_GASTO = new int[]{}; 
	
	public String getDateAgo(Context c){
		Date d = getDate();
		Date now = new Date();
		long totalData =  d.getTime();
		long totalNow = now.getTime();
		long diferenca = totalNow - totalData;
		TEMPO_MILISEGUNDOS[] tempos = TEMPO_MILISEGUNDOS.values();
		TEMPO_MILISEGUNDOS tempoUtilizado = null;
		for(int i = 0 ; i < tempos.length; i++){
			
			TEMPO_MILISEGUNDOS tempo =  tempos[i];
			//if(tempo.getMilisegundos() > diferenca)
			if(tempo.calculaInteiro(diferenca) == 0)
				break;
			else 
				tempoUtilizado = tempo;	
		
		}
		if(tempoUtilizado != null){
			return tempoUtilizado.getDescricao(c, diferenca);
		} else return null;
		
	}
	
	public static int getOffsetSegundosTimeZone(){
		Calendar cal = Calendar.getInstance();
		TimeZone tz = cal.getTimeZone();
		return tz.getOffset(new Date().getTime()) / 1000  ;
	}
	public static long getSecOffsetSegundosTimeZone(Context c){
		return getTimestampSegundosUTC(c);
	}
	public static long getTimestampSegundosUTC(Context c){
		Date date = GetUTCdatetimeAsDate(c);
		return date.getTime() /1000;
	}


	public static Date GetUTCdatetimeAsDate(Context c)
	{
		Date d = new Date();
	    //note: doesn't check for null
	    return StringDatetimeToDate(c, GetUTCdatetimeAsAmericanString(c, d));
	}
	public static String GetUTCdatetimeAsAmericanString(Context c, Date d)
	{
	    final SimpleDateFormat sdf = new SimpleDateFormat(DATETIMEFORMAT(c));
	    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
	    final String utcTime = sdf.format(d);

	    return utcTime;
	}
	public static Date StringDatetimeToDate(Context c, String StrDate)
	{
	    Date dateToReturn = null;
	    SimpleDateFormat dateFormat = new SimpleDateFormat(DATETIMEFORMAT(c));

	    try
	    {
	        dateToReturn = (Date)dateFormat.parse(StrDate);
	    }
	    catch (ParseException e)
	    {
	        SingletonLog.insereErro(e, TIPO.UTIL_ANDROID);
	    }

	    return dateToReturn;
	}
	public static long getUTCSecFromStrDatetimeCurrentTimezone(Context c, String datetime){

		Date d=HelperDate.StringDatetimeToDate(c, datetime);

		return getUTCSecFromDateCurrentTimezone(c,d);
	}
	public static long getUTCSecFromStrDateCurrentTimezone(Context c, String date){

		Date d=HelperDate.StringDateToDate(c, date);

		return getUTCSecFromDateCurrentTimezone(c,d);
	}
	public static long getUTCSecFromDateCurrentTimezone(Context c, Date time){

		SimpleDateFormat outputFmt = new SimpleDateFormat(DATETIMEFORMAT(c));
		outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
		String utc = outputFmt.format(time);
		Date dUtc = StringDatetimeToDate(c, utc);
		return dUtc.getTime() /1000;
	}
	public static Date StringDateToDate(Context c, String StrDate)
	{
		Date dateToReturn = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT(c));

		try
		{
			dateToReturn = (Date)dateFormat.parse(StrDate);
		}
		catch (ParseException e)
		{
			SingletonLog.insereErro(e, TIPO.UTIL_ANDROID);
		}

		return dateToReturn;
	}
	
	public static String getStringDate(Context c,Date d)
	{
	    
		SimpleDateFormat	dateFormat = new SimpleDateFormat(DATEFORMAT(c));	    
	    return dateFormat.format(d);
	}
	public static String getStringDateFromUtcTimestamp(Context c,long time){
		Date d = HelperDate.getDateFromSecOfssec(time, null);
		return getStringDate(c,d);
	}
	public static String getStringTimeFromUtcTimestamp(Context c,long time){
		Date d = HelperDate.getDateFromSecOfssec(time, null);
		return getStringTime(c,d);
	}
	public static String[] getStringDateETimeFromUtcTimestamp(Context c,long time){
		Date d = HelperDate.getDateFromSecOfssec(time, null);
		return getStringDateETimeFromDate(c,d);
	}
	public static String[] getStringDateETimeFromDate(Context c,Date d){
		SimpleDateFormat	dateFormat = new SimpleDateFormat(DATEFORMAT(c));
		SimpleDateFormat	timeFormat = new SimpleDateFormat(TIMEFORMAT(c));
	    return new String[]{dateFormat.format(d), timeFormat.format(d)};
	}
	
	public static String getStringTime(Context c,Date d )
	{   
		SimpleDateFormat timeFormat = new SimpleDateFormat(TIMEFORMAT(c));	    
	    return timeFormat.format(d);
	}
	
	public static String getStringDateTime(Context c,Date d )
	{
		if(d==null) return null;
		SimpleDateFormat timeFormat = new SimpleDateFormat(DATETIMEFORMAT(c));	    
	    return timeFormat.format(d);
	}

	public static Date getDateFromSecOfssec(long segundos, long offsec, SimpleDateFormat sdf){
		if(sdf== null)
			sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));


		segundos += offsec;
		Date d= new Date(segundos * 1000);

		return d;
	}
	
	public static Date getDateFromSecOfssec(long segundos, SimpleDateFormat sdf){
		 if(sdf== null)
	    sdf = new SimpleDateFormat();
	    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
	    
	    long milisegundos = getOffsetSegundosTimeZone();
	    segundos += milisegundos;
	    Date d= new Date(segundos * 1000);
	    
	    return d;
	}
	
	public static String getStringDateFromSecOfssec(Context c,long segundos){
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATETIMEFORMAT(c));
		Date d= getDateFromSecOfssec(segundos, null);
		String utcTime = dateFormat.format(d);
		return utcTime;
	}

	public static String getTimeAtualFormatada(Context c){

		SimpleDateFormat sdf = new SimpleDateFormat(TIMEFORMAT(c));
		Date agora = new Date();

		return sdf.format(agora);
	}
	public static String getStringDateTimeSQL(Date d )
	{   
		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	    
	    return timeFormat.format(d);
	}
	
	public static String getStringDateSQL(Date d )
	{   
		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");	    
	    return timeFormat.format(d);
	}
	
	public static String getStringTimeSQL(Date d )
	{   
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");	    
	    return timeFormat.format(d);
	}

	public static Calendar toCalendar(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

}
