package app.omegasoftware.pontoeletronico.file;

import java.io.File;
import java.io.FileFilter;

public class CleanDir {

	
	
	public CleanDir(){
		
	}
	
	public void deleteOldFiles(String path, int maxSizeKb){
		long maxSizeB = maxSizeKb * 1000;
//		maxSizeB = 1000000;
		long total =HelperFile.getFilesSize(path);
		if(total > maxSizeB){
			
			File[] fs= HelperFile.getFilesOrderByLastDateModified(path, true, null);
			if(fs!= null && fs.length > 0){
				for (File file : fs) {
					
					if(file.isDirectory()) continue;
					else {
						total-= file.length();
						file.delete();
						
						if(total <= maxSizeB) break;
					}
				}
			}	
		}
		
	}
	
	
	public void deleteOldFiles(File vDirectory, int maxSizeKb){
		long maxSizeB = maxSizeKb * 1000;
//		maxSizeB = 1000000;
		long total =HelperFile.getFilesSize(vDirectory);
		if(total > maxSizeB){
			
			File[] fs= HelperFile.getFilesOrderByLastDateModified(vDirectory, true, null);
			if(fs!= null && fs.length > 0){
				for (File file : fs) {
					
					if(file.isDirectory()) continue;
					else {
						total-= file.length();
						file.delete();
						
						if(total <= maxSizeB) break;
					}
				}
			}	
		}
		
	}
	
	
	public void deleteOldFiles(File[] vfs, int maxSizeKb){
		long maxSizeB = maxSizeKb * 1000;
//		maxSizeB = 1000000;
		long total =HelperFile.getFilesSize(vfs);
		if(total > maxSizeB){
			
			File[] fs= HelperFile.getFilesOrderByLastDateModified(vfs, true, null);
			if(fs!= null && fs.length > 0){
				for (File file : fs) {
					
					if(file.isDirectory()) continue;
					else {
						total-= file.length();
						file.delete();
						
						if(total <= maxSizeB) break;
					}
				}
			}	
		}
		
	}
	
	
	
}
