package app.omegasoftware.pontoeletronico.file;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import app.omegasoftware.pontoeletronico.http.HelperHttpResponse;
import app.omegasoftware.pontoeletronico.phone.HelperPhone;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class DirectoryHttpPostManager {

//	private static final String TAG = "FileHttpPostManager";

//	private String deviceID;
//	private String pathDirectory;
//	private Context context;
//	private String urlPost;
//
//	public DirectoryHttpPostManager(Context p_parentContext, String pPathDirectory, String pUrlPost) throws Exception
//	{
//
//		this.context = p_parentContext;
//		//Forca a criacao do diretorio
//		HelperFile.createDirectory(pPathDirectory);
//		this.pathDirectory = pPathDirectory;
//		this.urlPost = pUrlPost;
//
//		this.deviceID = HelperPhone.getIMEI(this.context);
//	}
//
//
//
//	private File getNewZipFile()
//	{
//		return new File(pathDirectory, this.generateRandomZipFileName());
//	}
//
//
//	private String generateRandomZipFileName()
//	{
//		return HelperString.generateRandomString(10) + OmegaFileConstants.ZIP_FILES_EXTENSION;
//	}
//
//
//	public File getFileToUpload()
//	{
//		ArrayList<File> filesToZip = new ArrayList<File>();
//		File baseDirectory = new File(pathDirectory);
//		if(baseDirectory.isDirectory())
//		{
//			for(File file : baseDirectory.listFiles())
//			{
//				
//				if(! file.canWrite()) continue;
//				
//				if(file.getName().endsWith(OmegaFileConstants.ZIP_FILES_EXTENSION))
//				{
//					//Log.d(TAG,"getFileToUpload(): I won't zip " + file.getName() + " cause it is already zipped");
//					continue;
//				}
//
//
//				if(file.length() != 0)
//				{
//					//Log.d(TAG,"getFileToUpload(): File " + file.getName() + " will be zipped");
//					filesToZip.add(file);	
//				}
//				else
//				{
//					//Log.d(TAG,"getFileToUpload(): File " + file.getName() + " is empty and will be deleted");
//					file.delete();
//				}
//
//			}
//		}
//		if(!filesToZip.isEmpty())
//		{
//			File zipFile = this.getNewZipFile();
//			FilesZipper filesZipper = new FilesZipper(filesToZip, zipFile);
//			filesZipper.zip();
//			return zipFile;	
//		}
//		else
//		{
//			//Log.d(TAG,"getFileToUpload(): No file to upload");
//			return null;
//		}
//	}
//
//	public void launchUploaderTask()
//	{
//
//		
//		new Runnable() {
//			public void run() {
//				uploadFiles();
//			}
//		}.run();
//
//	}
//
//	public void uploadFiles()
//	{
//		this.performUpload(this.getFileToUpload());
//		for(File file : this.getZipFiles())
//		{
//			this.performUpload(file);
//		}
//	}
//
//	public void performUpload(File p_file)
//	{
//
//		File fileToUpload = p_file;
//		if(fileToUpload == null)
//		{
//			return;
//		}
//
//		//Log.d(TAG,"performUpload(): Preparing to upload a " + fileToUpload.length() + " bytes long file");
//
//		DefaultHttpClient dc = new DefaultHttpClient();
//		HttpPost post = new HttpPost(this.urlPost);
//		FileBody bin = new FileBody(fileToUpload);
//
//		MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);  
//		try {
//			reqEntity.addPart("logfile", bin);
//			reqEntity.addPart("imei",new StringBody(this.deviceID));
//		} catch (UnsupportedEncodingException e1) {
//			//Log.e(TAG,"performUpload(): " + e1.getMessage());
//		}
//
//		post.setEntity(reqEntity);
//
//		try {
//			String content = HelperHttpResponse.readResponseContent(dc.execute(post));
//			if(content.equals("OK"))
//			{
//				//Log.d(TAG,"performUpload(): " + fileToUpload.getName() + " was sucessfully uploaded");
//				fileToUpload.delete();
//				//Log.d(TAG,"performUpload(): " + fileToUpload.getName() + " was deleted");
//			}
//
//		} catch (ClientProtocolException e) {
//			//Log.e(TAG,"performUpload(): " + e.getMessage());
//		} catch (IOException e) {
//			//Log.e(TAG,"performUpload(): " + e.getMessage());
//		}
//
//	}
//
//	private ArrayList<File> getZipFiles()
//	{
//		ArrayList<File> zipFiles = new ArrayList<File>();
//		//Log.d(TAG,"getZipFiles(): Searching for not uploaded zip files");
//		File baseDirectory = new File(pathDirectory);
//		if(baseDirectory.isDirectory())
//		{
//			for(File file : baseDirectory.listFiles())
//			{
//				if(file.getName().endsWith(OmegaFileConstants.ZIP_FILES_EXTENSION))
//				{
//					zipFiles.add(file);
//				}
//			}
//		}
//
//		//Log.d(TAG,"getZipFiles(): Found " + zipFiles.size() + " zip files");
//		return zipFiles;
//
//	}

}
