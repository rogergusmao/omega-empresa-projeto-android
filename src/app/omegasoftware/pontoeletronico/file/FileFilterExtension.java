package app.omegasoftware.pontoeletronico.file;

import java.io.File;
import java.io.FileFilter;

public class FileFilterExtension implements FileFilter {
	String[] vVetorExtension;
	//pega arquivos que nao possuem sufixo com "."
	public static String FILE_WITHOUT_EXTENSION = "FILE_WITHOUT_EXTENSION";
	//extensao deve conter o "."
	public  FileFilterExtension(String p_extension){
		
		vVetorExtension = new String[] {p_extension};
		
	}
	
public  FileFilterExtension(String[] pVetorExtension){
		
		vVetorExtension = pVetorExtension;
		
	}
	
	public boolean isExtensionExistent(String pNameFile){
		for (String vExtension : vVetorExtension) {
			if( vExtension.equals(FILE_WITHOUT_EXTENSION)){
				if(! pNameFile.contains(".")) return true ;
			}else if(pNameFile.endsWith(vExtension)){
				return true;
			} 
		}
		return false;
	}
	
	public boolean accept(File pFile) {
		
		if(pFile.isDirectory()) return false;
		else {
			String pNameFile = pFile.getName();
			if(pNameFile == null) return false;
			else return isExtensionExistent(pNameFile);
		}
	}

}
