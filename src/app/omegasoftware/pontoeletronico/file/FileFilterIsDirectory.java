package app.omegasoftware.pontoeletronico.file;

import java.io.File;
import java.io.FileFilter;

public class FileFilterIsDirectory implements FileFilter {
	boolean isDirectoryNeeded = false;
	FileFilter  filterExtension = null;
	public FileFilterIsDirectory(boolean pIsDirectoryNeeded){
		isDirectoryNeeded = pIsDirectoryNeeded;
	}
	public FileFilterIsDirectory(boolean pIsDirectoryNeeded, FileFilter  pfilterExtension){
		isDirectoryNeeded = pIsDirectoryNeeded;
		filterExtension = pfilterExtension;
	}
	
	
	public boolean accept(File pFile) {
		
		if(pFile.isDirectory()){
			if(! isDirectoryNeeded)
				return false ;
			else if (filterExtension != null){
				return filterExtension.accept(pFile);
			} else return true;
		}
		else{
			
			if(! isDirectoryNeeded){
				if (filterExtension == null)
					return true;
				else return filterExtension.accept(pFile);
			}
			else return false;
		}
	}

}
