package app.omegasoftware.pontoeletronico.file;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.http.HelperHttpResponse;
import app.omegasoftware.pontoeletronico.phone.HelperPhone;

public class FileHttpPost {

//	private static final String TAG = "FileWriterHttpPost";

	private String urlPost;
	private String pathDirectory;
	Context c;
	public FileHttpPost(Context c, String pUrlPost, String pPathDirectory) {
		pathDirectory = pPathDirectory;
		this.urlPost = pUrlPost;
		this.c=c;
	}

	public FileHttpPost(Context c, String pUrlPost) {
		this.urlPost = pUrlPost;
		this.c=c;
	}

	public String getStrPath() {
		return pathDirectory;
	}

	public class MyFileFilter implements FileFilter {
		int count = 0;
		int maxFile = 0;

		public MyFileFilter(int pMaxOfFile) {
			maxFile = pMaxOfFile;
		}

		public boolean accept(File pathname) {
			

			if (count >= maxFile) {
				return false;
			} else {
				count += 1;
				return true;
			}
		}

	}

	public String uploadAndRemoveFile(File pFile) {
		return uploadAndRemoveFile(pFile, null, null);

	}

	public String uploadFile(File pFile, String[] pVetorNomePost,
			String[] pVetorValorPost) {
		if (pFile == null)
			return null;
		String vNameFile = pFile.getName();
		boolean vIsZIP = false;
		if (vNameFile.contains(".zip"))
			vIsZIP = true;
		if(pFile.exists() && pFile.length() > 0){
			String vResposta = this.performUpload(pFile, vIsZIP, pVetorNomePost,
					pVetorValorPost);
			if (vResposta != null) {

				return vResposta;
			} else
				return null;	
		} else return null;
		

	}

	public String uploadAndRemoveFile(File pFile, String[] pVetorNomePost,
			String[] pVetorValorPost) {
		if (pFile == null)
			return null;
		String vNameFile = pFile.getName();
		boolean vIsZIP = false;
		if (vNameFile.contains(".zip"))
			vIsZIP = true;
		if(pFile.length() > 0){
			String vResposta = this.performUpload(pFile, vIsZIP, pVetorNomePost,
					pVetorValorPost);
			if (vResposta != null) {
				pFile.delete();
				return vResposta;
			} else
				return null;	
		} else {
			pFile.delete();
			return null;
		}
		

	}

	public String uploadFileAndRemove(String pNameFile) {
		boolean vIsZIP = false;
		if (pNameFile.contains(".zip"))
			vIsZIP = true;
		File vFileToSend = new File(this.pathDirectory + pNameFile);

		if (vFileToSend.isFile() && vFileToSend.length() > 0) {
			String vConteudo = this.performUpload(vFileToSend, vIsZIP, null, null);
			
			if (vConteudo != null)
				vFileToSend.delete();
			return vConteudo;
		} else
			return null;
	}

	public String performUpload(File p_file, boolean pIsZIP, String pNomePost[], String pConteudoPost[]){
		return performUpload(p_file, pIsZIP, pNomePost, pConteudoPost, true);
	}
	public String performUpload(File p_file, boolean pIsZIP, String pNomePost[], String pConteudoPost[], boolean setarConstantes)
	{

		File fileToUpload = p_file;
		
		
		if(fileToUpload == null)
		{
			return null;
		}

		//Log.d(TAG,"performUpload(): Preparing to upload a " + fileToUpload.length() + " bytes long file");

		DefaultHttpClient dc = new DefaultHttpClient();
		HttpPost post = new HttpPost(urlPost);
		FileBody bin = new FileBody(fileToUpload);
		
		MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		String[] valores = null;
		String[] constantes = null;
		try {
			constantes = new String[]{
					"usuario",
					"id_corporacao",
					"corporacao",
					"senha",
					"imei"};
			valores = new String[]{
					OmegaSecurity.getIdUsuario(),
					OmegaSecurity.getIdCorporacao(),
					OmegaSecurity.getCorporacao(),
					OmegaSecurity.getSenha(),
					HelperPhone.getIMEI(c)
			};
			for(int i = 0 ; i < constantes.length; i++){
				String val = valores[i] == null ? "" : valores[i];
				FormBodyPart bodyPart=new FormBodyPart(constantes[i], new StringBody(val));
				reqEntity.addPart(bodyPart);
			}
				
			reqEntity.addPart("logfile", bin);
			
			if(OmegaConfiguration.DEBUGGING ){
//				InputStream is = reqEntity.getContent();
//				String parametros = HelperHttpPost.inputStreamToString(is);
				Log.i("performUpload", "multipart entity - " + fileToUpload.getAbsolutePath());
				
			}
			
			if(pNomePost != null && pConteudoPost != null
					&& pNomePost.length > 0 
					&& pNomePost.length == pConteudoPost.length ){
				for(int i = 0 ; i < pNomePost.length; i++){
					String value = pConteudoPost[i];
					if(value == null) value = "";
					reqEntity.addPart(pNomePost[i], new StringBody(value));
				}
			}
		} catch (Exception ex) {
			
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			HelperHttpPost.logHttp(this.c,urlPost, pNomePost, pConteudoPost, ex, null,constantes, valores);
			return null;
		}

		post.setEntity(reqEntity);
Exception ex1= null;
		try {
			
			String content = HelperHttpResponse.readResponseContent(dc.execute(post));
			
			HelperHttpPost.logHttp(this.c, urlPost + " " + p_file.getAbsolutePath() , pNomePost,   pConteudoPost, null, content,constantes, valores);
			
			return content;
		

		} catch (ClientProtocolException ex) {
			
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			ex1 = ex;
		} catch (IOException ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			ex1 = ex;
		}
		
		HelperHttpPost.logHttp(this.c,urlPost, pNomePost, pConteudoPost, ex1, null ,constantes, valores);
		return null;

	}
	
	public String performUploadAnonimo(File p_file, boolean pIsZIP, String pNomePost[], String pConteudoPost[], boolean setarConstantes)
	{

		File fileToUpload = p_file;
		
		
		if(fileToUpload == null)
		{
			return null;
		}

		//Log.d(TAG,"performUpload(): Preparing to upload a " + fileToUpload.length() + " bytes long file");

		DefaultHttpClient dc = new DefaultHttpClient();
		HttpPost post = new HttpPost(urlPost);
		FileBody bin = new FileBody(fileToUpload);
		
		MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		String[] valores = null;
		String[] constantes = null;
		try {
			reqEntity.addPart("logfile", bin);
			
			if(OmegaConfiguration.DEBUGGING ){
//				InputStream is = reqEntity.getContent();
//				String parametros = HelperHttpPost.inputStreamToString(is);
				Log.i("performUpload", "multipart entity - " + fileToUpload.getAbsolutePath());
				
			}
			
			if(pNomePost != null && pConteudoPost != null
					&& pNomePost.length > 0 
					&& pNomePost.length == pConteudoPost.length ){
				for(int i = 0 ; i < pNomePost.length; i++){
					String value = pConteudoPost[i];
					if(value == null) value = "";
					reqEntity.addPart(pNomePost[i], new StringBody(value));
				}
			}
		} catch (Exception ex) {
			
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			HelperHttpPost.logHttp(this.c, urlPost, pNomePost, pConteudoPost, ex, null,constantes, valores);
			return null;
		}

		post.setEntity(reqEntity);
Exception ex1= null;
		try {
			
			String content = HelperHttpResponse.readResponseContent(dc.execute(post));
			
			HelperHttpPost.logHttp(this.c, urlPost + " " + p_file.getAbsolutePath() , pNomePost,   pConteudoPost, null, content,constantes, valores);
			
			return content;
		

		} catch (ClientProtocolException ex) {
			
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			ex1 = ex;
		} catch (IOException ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			ex1 = ex;
		}
		
		HelperHttpPost.logHttp(this.c,urlPost, pNomePost, pConteudoPost, ex1, null ,constantes, valores);
		return null;

	}

//	
//	public Boolean performUpload(File pFile, boolean pIsZIP,
//			EXTDAOSistemaTipoDownloadArquivo.TIPO pTipo) throws Exception {
//
//		String vURL = "";
//		vURL = urlPost;
//		if (pFile == null) {
//			return false;
//		}
//
//		DefaultHttpClient dc = new DefaultHttpClient();
//		HttpPost post = new HttpPost(vURL);
//
//		FileBody bin = new FileBody(pFile);
//
//		MultipartEntity reqEntity = new MultipartEntity(
//				HttpMultipartMode.BROWSER_COMPATIBLE);
//	
//		reqEntity.addPart("tipo", new StringBody(
//				EXTDAOSistemaTipoDownloadArquivo.getId(pTipo)));
//		reqEntity.addPart("usuario",
//				new StringBody(OmegaSecurity.getIdUsuario()));
//		reqEntity.addPart("id_corporacao",
//				new StringBody(OmegaSecurity.getIdCorporacao()));
//		reqEntity.addPart("corporacao",
//				new StringBody(OmegaSecurity.getCorporacao()));
//		reqEntity
//				.addPart("senha", new StringBody(OmegaSecurity.getSenha()));
//		reqEntity.addPart("logfile", bin);
//		reqEntity.addPart("imei", new StringBody(HelperPhone.getIMEI(c)));
//	
//
//		post.setEntity(reqEntity);
//
//		try {
//
//			String content = HelperHttpResponse.readResponseContent(dc
//					.execute(post));
//			
//			if (content == null)
//				return null;
//			else if (content.equals("TRUE")) {
//				Log.d(TAG, "performUpload(): " + pFile.getName()
//						+ " was sucessfully uploaded");
//				return true;
//			} else {
//				Log.d(TAG, "performUpload(): " + content);
//				return false;
//			}
//
//		} catch (ClientProtocolException e) {
//			SingletonLog.insereErro(e, TIPO.HTTP);
//			return null;
//			// Log.e(TAG,"performUpload(): " + e.getMessage());
//		} catch (IOException e) {
//			SingletonLog.insereErro(e, TIPO.HTTP);
//			return null;
//			// Log.e(TAG,"performUpload(): " + e.getMessage());
//		}
//
//	}

}
