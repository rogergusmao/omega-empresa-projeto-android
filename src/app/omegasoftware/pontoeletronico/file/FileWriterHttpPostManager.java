package app.omegasoftware.pontoeletronico.file;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.http.HelperHttpResponse;
import app.omegasoftware.pontoeletronico.phone.HelperPhone;

public class FileWriterHttpPostManager extends FileWriterManager{

	private static final String TAG = "FileWriterHttpPostManager";

//	private String deviceID;
//	private Context context;
	private String urlPost;
	
	public FileWriterHttpPostManager(
			Context p_parentContext, 
			String pUrlPost, 
			String pPathDirectory, 
			String pDemiliterMessageInFile)
	{
		super( pPathDirectory, pDemiliterMessageInFile);
		
//		this.context = p_parentContext;
		//Forca a criacao do diretorio
		//HelperFile.createDirectory(pPathDirectory);
		this.urlPost = pUrlPost;
	}

	private File getNewZipFile()
	{
		return new File(pathDirectory, 
				HelperFile.generateRandomFileName(OmegaFileConstants.ZIP_FILES_EXTENSION));
	}

	
	
	public class MyFileFilter implements FileFilter{
		int count = 0;
		int maxFile = 0;
		public MyFileFilter(int pMaxOfFile){
			maxFile = pMaxOfFile;
		}
		public boolean accept(File pathname) {
			
			
			if(count >= maxFile){
				return false;
			}
			else{
				count += 1;
				return true;
			}
		}
		
	}
	
	public String getStrPath(){
		return pathDirectory;
	}
	
	public File[] getAllFileOfPathToUpload(){
File baseDirectory = new File(pathDirectory);
		
		if(baseDirectory.isDirectory())
		{
			File vVetorFileOfDir[] = baseDirectory.listFiles();
			return vVetorFileOfDir;
		}else return null;
	}
	
	
	public File[] getFileToUpload(int pLimitFileZip, int pLimitFileToUpload, String pExtension)
	{
		ArrayList<File> filesToZip = new ArrayList<File>();
		ArrayList<File> filesZipped = new ArrayList<File>();
		MyFileFilter vMyFileFilter = new MyFileFilter(pLimitFileToUpload + pLimitFileZip);
		File baseDirectory = new File(pathDirectory);
		
		if(baseDirectory.isDirectory())
		{
			File vVetorFileOfDir[] = baseDirectory.listFiles(vMyFileFilter);
			for(File file : vVetorFileOfDir)
			{
				if(pExtension != null)
					if(!file.getName().endsWith(pExtension)) continue;
				if(filesZipped.size() >= pLimitFileToUpload && pLimitFileToUpload > 0) 
					break;
				else if(file.getName().endsWith(OmegaFileConstants.ZIP_FILES_EXTENSION))
				{
					//Log.d(TAG,"getFileToUpload(): I won't zip " + file.getName() + " cause it is already zipped");
					filesZipped.add(file);
					continue;
				}
				else if(!file.getName().equals(this.currentWriteableFile.getName()))
				{
					if(file.length() != 0)
					{
						//Log.d(TAG,"getFileToUpload(): File " + file.getName() + " will be zipped");
						filesToZip.add(file);	
						if(filesToZip.size() >= pLimitFileZip && pLimitFileZip > 0){
							break;
						}
					}
					else
					{
						//Log.d(TAG,"getFileToUpload(): File " + file.getName() + " is empty and will be deleted");
						file.delete();
					}
				}
				else
				{
					//Log.d(TAG,"getFileToUpload(): File " + file.getName() + " is the current file");
				}
			}
		}
		File zipFile = null;
		if(!filesToZip.isEmpty())
		{
			zipFile = this.getNewZipFile();
			FilesZipper filesZipper = new FilesZipper(filesToZip, zipFile);
			filesZipper.zip();	
		}
		if(zipFile != null)
			filesZipped.add(zipFile);
		if(filesZipped.size() == 0 )
		{
			//Log.d(TAG,"getFileToUpload(): No file to upload");
			return null;
		}else{
			File[] vVetorFile = new File[filesZipped.size()];
			filesZipped.toArray(vVetorFile);
			return vVetorFile;
		}

	}

	public boolean launchUploaderTask()
	{

		this.createNewFile();
//		new Runnable() {
//			public void run() {
		return uploadFiles(-1, -1, null);
//			}
//		}.run();

	}
	
	public boolean uploadFiles(File pVetorFile[])
	{
		
		if(pVetorFile == null) return true;
		boolean vValidade = true;
		
		for(File file : pVetorFile)
		{	
			String vNameFile = file.getName();
			boolean vIsZIP = false;
			if(vNameFile.contains(".zip"))
				vIsZIP = true;
			boolean vValidadeTemp = this.upload(file, vIsZIP);
			
			if(!vValidadeTemp) vValidade = false;
			else{
				file.delete();
			}
		}

		return vValidade;
	}
	
	public boolean uploadAndRemoveFile(File pFile){
		if(pFile == null) return false;
		String vNameFile = pFile.getName();
		boolean vIsZIP = false;
		if(vNameFile.contains(".zip"))
			vIsZIP = true;
		if(this.upload(pFile, vIsZIP)){
			pFile.delete();
			return true;
		} else return false;
		
	}
	
	public boolean uploadFiles(ArrayList<File> pVetorFile)
	{
		
		if(pVetorFile == null) return true;
		boolean vValidade = true;
		
		for(File file : pVetorFile)
		{	
			boolean vValidadeTemp = uploadAndRemoveFile(file);
			
			if(!vValidadeTemp) vValidade = false;
			
		}

		return vValidade;
	}
	
	
	public boolean uploadFiles(int pLimitFileToZip, int pLimitFileToUpload, String pExtension)
	{
		File[] vVetorFile = this.getFileToUpload(pLimitFileToZip, pLimitFileToUpload, pExtension);
		if(vVetorFile == null) return true;
		boolean vValidade = true;
		int vNumberFileUploaded = 0;
		for(File file : vVetorFile)
		{
			
			String vNameFile = file.getName();
			boolean vIsZIP = false;
			if(vNameFile.contains(".zip"))
				vIsZIP = true;
			boolean vValidadeTemp = this.upload(file, vIsZIP);
			
			if(!vValidadeTemp) vValidade = false;
			else{
				vNumberFileUploaded += 1;
				file.delete();
				if(pLimitFileToUpload >= 0 && vNumberFileUploaded >= pLimitFileToUpload)
					break;
			}
		}

		return vValidade;
	}
	
	public boolean uploadFileAndRemove(String pNameFile)
	{
		boolean vIsZIP = false;
		if(pNameFile.contains(".zip"))
			vIsZIP = true;
		File vFileToSend = new File(this.pathDirectory + pNameFile);
		boolean vValidade = true;
		if(vFileToSend.isFile()){
			vValidade = this.upload(vFileToSend, vIsZIP);
			
			if(vValidade) 
				vFileToSend.delete();
		}
		return vValidade;
	}
	
	

	public boolean upload(File p_file, boolean pIsZIP)
	{
		
		return false;
//		File fileToUpload = p_file;
//		
//		String vURL = "";
//		if(pIsZIP)
//			vURL = OmegaConfiguration.URL_REQUEST_FILE_ZIPPER_QUERY();
//		else vURL = urlPost;
//		if(fileToUpload == null)
//		{
//			return false;
//		}
//
//		//Log.d(TAG,"performUpload(): Preparing to upload a " + fileToUpload.length() + " bytes long file");
//
//		DefaultHttpClient dc = new DefaultHttpClient();
//		HttpPost post = new HttpPost(vURL);
//		FileBody bin = new FileBody(fileToUpload);
//
//		MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);  
//		try {
//			reqEntity.addPart("usuario", new StringBody(OmegaSecurity.getIdUsuario()));
//			reqEntity.addPart("corporacao", new StringBody(OmegaSecurity.getCorporacao()));
//			reqEntity.addPart("id_corporacao", new StringBody(OmegaSecurity.getIdCorporacao()));
//			reqEntity.addPart("senha", new StringBody(OmegaSecurity.getSenha()));
//			reqEntity.addPart("logfile", bin);
//			reqEntity.addPart("imei", new StringBody(HelperPhone.getIMEI()));
//		} catch (UnsupportedEncodingException e1) {
//			Log.e(TAG,"performUpload(): " + e1.getMessage());
//		}
//
//		post.setEntity(reqEntity);
//
//		try {
//			
//			String content = HelperHttpResponse.readResponseContent(dc.execute(post));
//			if(content.equals("TRUE"))
//			{
//				Log.d(TAG,"performUpload(): " + fileToUpload.getName() + " was sucessfully uploaded");
//			} else{
//				Log.d(TAG,"performUpload(): " + content);
//				return false;
//			}
//		
//
//		} catch (ClientProtocolException e) {
//			//Log.e(TAG,"performUpload(): " + e.getMessage());
//		} catch (IOException e) {
//			//Log.e(TAG,"performUpload(): " + e.getMessage());
//		}
//		return true;

	}
	
	
}
