package app.omegasoftware.pontoeletronico.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Stack;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;

public class FileWriterManager {

	private static final String TAG = "FileHttpPostManager";

	protected Stack<String> stackedMessages;
	protected File currentWriteableFile;
	
	protected FileOutputStream currentWriteableFileStream;
	
	protected String pathDirectory;
	protected String delimiterMessageInFile;
	boolean mIsToSendAllFilesOfDirectory = true;
	public FileWriterManager(
			String pPathDirectory, 
			String pDelimiterMessageInFile,
			boolean pIsToSendAllFilesOfDirectory)
	{	
		//Forca a criacao do diretorio
		HelperFile.createDirectory(pPathDirectory);
		this.pathDirectory = pPathDirectory;
		delimiterMessageInFile = pDelimiterMessageInFile;

		this.stackedMessages = new Stack<String>();
		mIsToSendAllFilesOfDirectory = pIsToSendAllFilesOfDirectory;
		if(mIsToSendAllFilesOfDirectory)
			initializeFile();

	}
	
	public FileWriterManager(
			String pPathDirectory, 
			String pDelimiterMessageInFile)
	{	
		this(
				pPathDirectory, 
				pDelimiterMessageInFile, 
				true);
	}
	
	@Override
	protected void finalize(){
		try{
			if(currentWriteableFileStream != null){
				currentWriteableFileStream.close();
			}
		} catch(Exception ex){

			SingletonLog.insereWarning(ex, TIPO.UTIL_ANDROID);
		} finally {
			try{
				super.finalize();	
			}
			catch (Throwable e) {

				
				e.printStackTrace();
			}
		}

	}

	protected void initializeFile(){

		File file = new File(pathDirectory);

		if(file.isDirectory()){
			File vLastFileModifie = HelperFile.getFileModified(
					pathDirectory, 
					false, 
					new FileFilterExtension(new String[]{FileFilterExtension.FILE_WITHOUT_EXTENSION, ".jpg" }));
			if(vLastFileModifie != null){
				this.currentWriteableFile = vLastFileModifie;
				try {
					this.currentWriteableFileStream = new FileOutputStream(this.currentWriteableFile, true);
				} catch (FileNotFoundException e) {
					
					e.printStackTrace();
				}
			} else createNewFile();
		}
		else{
			return ;
		}

	}

	protected void createNewFile()
	{
		//If this is not the first file being created
		if(this.currentWriteableFile != null)
		{
			if(this.currentWriteableFile.length() != 0)
			{
				try {
					this.currentWriteableFileStream.close();
				} catch (IOException e) {
					//Log.d(TAG,"createNewFile(): Could not close file output stream");
				}	
			}
			else
			{
				return;
			}
		}

		//Log.d(TAG,"createNewFile(): Creating new file");
		//Creates a new file
		this.currentWriteableFile = new File(pathDirectory, HelperFile.generateRandomFileName(null));
		try {
			//Creates a new file stream
			this.currentWriteableFileStream = new FileOutputStream(this.currentWriteableFile);
		} catch (FileNotFoundException e) {
			//Log.d(TAG,"TruckTrackerFilesManager(): Could not create file output stream");
			this.currentWriteableFileStream = null;
		}
	}


	public void writeLogInformation(String p_text)
	{
		this.emptyMessagesStack();
		this.checkIfStackIsFull();
		this.stackedMessages.add(p_text);
		this.emptyMessagesStack();
	}

	protected void checkIfStackIsFull()
	{
		if(this.stackedMessages.size() > OmegaFileConstants.MAX_MESSAGES_STACK_SIZE)
		{
			//Log.d(TAG,"checkIfStackIsFull(): Stack was full");
			this.stackedMessages.remove(this.stackedMessages.lastElement());
		}
	}

	protected void emptyMessagesStack()
	{
		//Log.d(TAG,"emptyMessagesStack(): Emptying stack");
		Stack<String> auxiliarStack = new Stack<String>();
		while(!this.stackedMessages.isEmpty())
		{
			//Log.d(TAG,"emptyMessagesStack(): Stack size = "  + this.stackedMessages.size());
			String message = this.stackedMessages.pop();
			if(!this.writeInFile(message))
			{
				auxiliarStack.add(message);
			}
		}
		this.stackedMessages = auxiliarStack;
	}

	protected boolean writeInFile(String p_text)
	{
		try
		{
			String text = delimiterMessageInFile + p_text ;
			//Log.d(TAG,"writeInFile(): " + text);
			this.currentWriteableFileStream.write(text.getBytes());
			this.currentWriteableFileStream.flush();
			return true;
		}
		catch(Exception e)
		{
			this.createNewFile();
			return false;
		}
	}

}
