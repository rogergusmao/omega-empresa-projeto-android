package app.omegasoftware.pontoeletronico.file;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.FileDateListComparator;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.phone.HelperPhone;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

@SuppressLint("SdCardPath")
public class HelperFile {

	    private HelperFile() {
	        super();
	    }
	    public static void removeFileIfExists(String pathDir){
	    	if(pathDir == null || pathDir.length() ==0)
	    		return;
	    	File d =new File(pathDir);
	    	if(d.exists()){
	    		if(d.isDirectory()) removeDirectoryAndAllFiles(pathDir);
	    		else d.delete();
	    	}
	    }
	    public static void removeDirectoryAndAllFiles(String pathDir){
	    	try{
		    	File d =new File(pathDir);
		    	if(d.exists()){
		    		File[] fs = d.listFiles();
		    		for(int i =  0; i < fs.length; i++){
		    			try{
		    				fs[i].delete();	
		    	    	}catch(Exception ex){
		    	    		SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
		    	    	}
		    		}
		    		d.delete();
		    	}
	    	}catch(Exception ex){
	    		SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
	    	}
	    }
	    
	    public static void removeDirectoryAndAllFiles(File[] fs){
	    	try{

	    		for(int i =  0; i < fs.length; i++){
	    			try{
	    				fs[i].delete();	
	    	    	}catch(Exception ex){
	    	    		SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
	    	    	}
	    		}
	    	
	    	}catch(Exception ex){
	    		SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
	    	}
	    }
	    
	    public static void copy(File src, File dst) throws IOException {
	        InputStream in = new FileInputStream(src);
	        OutputStream out = new FileOutputStream(dst);

	        // Transfer bytes from in to out
	        byte[] buf = new byte[1024];
	        int len;
	        while ((len = in.read(buf)) > 0) {
	            out.write(buf, 0, len);
	        }
	        out.close();
	        in.close();
	        
	    }
	    public static String getFileWithUniqueName(){
			
			String vData = HelperDate.getDatetimeParaNomeDeArquivo();
			String vUsuario = OmegaSecurity.getIdUsuario();
			Random vRandom = new Random();
			Integer vRandomNumber = vRandom.nextInt();
			if(vRandomNumber < 0 ) vRandomNumber = -vRandomNumber;
			
			String vFileName = vData + "_" + vUsuario + "_" + vRandomNumber.toString(); 
			
			return vFileName;
		}
	    public static String getPathOfFile(String pPath){
	    	if(pPath == null || pPath.length() == 0 ) return "";
	    	else{
	    		
	    		int vLastIndex = pPath.lastIndexOf("/");
	    		return pPath.substring(0, vLastIndex);
	    	}
	    }
	    
		public static boolean StoreByteImage(Context mContext, byte[] imageData,
				int quality, String expName) {

	        File sdImageMainDirectory = new File("/sdcard");
			FileOutputStream fileOutputStream = null;
			
			try {

				BitmapFactory.Options options=new BitmapFactory.Options();
				options.inSampleSize = 5;
				
				Bitmap myImage = BitmapFactory.decodeByteArray(imageData, 0,
						imageData.length,options);

				
				fileOutputStream = new FileOutputStream(
						sdImageMainDirectory.toString() +"/image.jpg");
								
	  
				BufferedOutputStream bos = new BufferedOutputStream(
						fileOutputStream);

				myImage.compress(CompressFormat.JPEG, quality, bos);

				bos.flush();
				bos.close();

			} catch (FileNotFoundException e) {
				
				e.printStackTrace();
			} catch (IOException e) {
				
				e.printStackTrace();
			}

			return true;
		}
		
		public static String generateRandomFileName(String pExtension)
		{
			if(pExtension == null) return HelperString.generateRandomString(10);
			else return HelperString.generateRandomString(10) + pExtension;
		}	
		public static long getFilesSize(File[] files)
		{
			long fileSize = 0;

			for(File file : files)
			{
				fileSize += file.length();
			}

			return fileSize;
		}
		
		public static long getFilesSize(List<File> files)
		{
			long fileSize = 0;

			for(File file : files)
			{
				fileSize += file.length();
			}

			return fileSize;
		}
		public static long getFilesSize(File baseDirectory)
		{
			long fileSize = 0;


			if(baseDirectory.isDirectory())
			{
				for(File file : baseDirectory.listFiles())
				{
					fileSize += file.length();
				}
			}
			return fileSize;
		}
		public static long getFilesSize(String pPath)
		{
			long fileSize = 0;

			File baseDirectory = new File(pPath);
			if(baseDirectory.isDirectory())
			{
				for(File file : baseDirectory.listFiles())
				{
					fileSize += file.length();
				}
			}
			return fileSize;
		}

		public static boolean createDirectory(String p_strPath){
			File f = new File(p_strPath);
			
			if (!f.exists()) {
				boolean vValidade = f.mkdirs();
				
				return vValidade;
			}
			return false;
		}
	// assets/ is root
		public static String[] getListOfFileInDirectoryAssets(String p_strPath, Context p_context){
			
			try{
				AssetManager assetManager = p_context.getAssets();
				return assetManager.list( p_strPath);	
			}catch (Exception e) {
				// TODO: handle exception
				//Log.e("FileSystemHelper::getListOfFileInDirectoryAssets", "Error when try read path '" + p_strPath + "': " + e.getMessage());
				return null;
			}
		}
		//p_vetorFilePath - the path have to be considering 'assets/' as root.
		public static boolean mergeListFileInAssetsInFile(String p_vetorFilePath[], 
				String p_strPathOutFile, 
				Context p_context){

			try
			{
				FileOutputStream Fos = new FileOutputStream( p_strPathOutFile );
				for (String  pathFile : p_vetorFilePath) {

					InputStream inputFile = p_context.getAssets().open( pathFile);

					
					try
					{
						inputFile.available();
					}
					catch ( IOException e)
					{
						return false;
					}

					// Reading and writing the file Method 1 :
					byte[] buffer = new byte[256];
					int len = 0;
					while(len != -1){
						try
						{
							len = inputFile.read(buffer);
						}
						catch ( IOException e)
						{
							//Log.e("FileSystemHelper::mergeListFileInAssetsInFile", "Can't read file: " + pathFile + ". Error: " + e.getMessage() );					
							return false;
						}
						if(len > 0 ){
							Fos.write( buffer, 0 , len );	
						}
							
					}
					inputFile.close();
				}    
				Fos.close();
			}
			catch( IOException e)
			{
				return false;
			}
			return true;
		}


		public static boolean CutFilesInSizeParts(String InputFileName, String OutputFileName, int MaxPartSize)
		{
			FileInputStream fis = null;
			try
			{
				File f = new File(InputFileName);
				fis = new FileInputStream(f);
				int TotalLength = fis.available();
				byte[] buffer = new byte[ TotalLength + 1 ];
				int len = fis.read( buffer );

				int nbPart = len / MaxPartSize + 1;
				int CurPos = 0;

				for ( int i = 0; i < nbPart; i++ )    
				{
					int PartLen = MaxPartSize;
					if ( CurPos + PartLen >= len )
						PartLen = len - CurPos;
					String outRealFileName = OutputFileName + (i+1);
					FileOutputStream fos = null;
					try{
						fos = new FileOutputStream(outRealFileName);
						fos.write(buffer, CurPos, PartLen);
					}finally{
						if(fos != null) fos.close();
					}
					
					CurPos += PartLen;
				}
			}
			catch( IOException e)
			{
				return false;
			} finally{
				if(fis != null)
					try {
						fis.close();
					} catch (IOException e) {
						SingletonLog.insereWarning(e, TIPO.UTIL_ANDROID);
					}
			}
			return true;
		}
		public static String getNameOfFileInPath(String pPath){
	    	if(pPath == null || pPath.length() == 0 ) return "";
	    	else{
	    		
	    		int vLastIndex = pPath.lastIndexOf("/");
	    		if(vLastIndex == pPath.length() - 1)
	    			return "";
	    		else return pPath.substring(vLastIndex + 1, pPath.length() );
	    	}
	    }

		public static File getFileModified(String pPath , boolean pIsFirstFileModifie, FileFilter pFileFilter){
			
			
			File[] vListFileSort = HelperFile.getFilesOrderByLastDateModified(pPath, pIsFirstFileModifie, pFileFilter);
			if(vListFileSort == null) return null;
			else if(vListFileSort.length == 0 ) return null;
			if(pIsFirstFileModifie)
			return vListFileSort[0];
			else return vListFileSort[vListFileSort.length - 1];
			
		}
public static File[] getFilesOrderByLastDateModified( File vDirectory, boolean pIsAsc, FileFilter pFileFilter){
			
			
			ArrayList<File> vList = new ArrayList<File>(); 
			if(vDirectory.isDirectory()){
				File vVetorFile[] = vDirectory.listFiles(new FileFilterIsDirectory(false, pFileFilter));
				
				for (File vFile : vVetorFile) {
					vList.add(vFile);
				}
				
				Collections.sort(vList, new FileDateListComparator(pIsAsc));
			}
			
			File[] vNewVetorFile = new File[vList.size()];
			vList.toArray(vNewVetorFile);
			
			return vNewVetorFile;
		}
		
		public static File[] getFilesOrderByLastDateModified( String pPath, boolean pIsAsc, FileFilter pFileFilter){
			
			File vDirectory = new File(pPath);
			ArrayList<File> vList = new ArrayList<File>(); 
			if(vDirectory.isDirectory()){
				File vVetorFile[] = vDirectory.listFiles(new FileFilterIsDirectory(false, pFileFilter));
				
				for (File vFile : vVetorFile) {
					vList.add(vFile);
				}
				
				Collections.sort(vList, new FileDateListComparator(pIsAsc));
			}
			
			File[] vNewVetorFile = new File[vList.size()];
			vList.toArray(vNewVetorFile);
			
			return vNewVetorFile;
		}
		
	    public static String getFileContents(final File file) throws IOException {
	        final InputStream inputStream = new FileInputStream(file);
	        final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	        
	        final StringBuilder stringBuilder = new StringBuilder();
	        
	        boolean done = false;
	        
	        while (!done) {
	            final String line = reader.readLine();
	            done = (line == null);
	            
	            if (line != null) {
	                stringBuilder.append(line);
	            }
	        }
	        
	        reader.close();
	        inputStream.close();
	        
	        return stringBuilder.toString();
	    }
	    
	    public static void appendLogFileToMe(String pathIn, String pathOut){
			
			if(! HelperPhone.isFullDisk()){
				return;
			}
			FileReader fr =null;
			FileWriter fw = null;
			try {
				File fin = new File(pathIn);
				if(!fin.exists())
					return;
							
				File f = new File(pathOut);
				if(!f.exists()) {
					if(!f.createNewFile())
						throw new Exception("Falha ao criar um novo arquivo");
				}
				
				
				fw = new FileWriter(pathOut, true);
				fr=new FileReader(pathIn);
				
				int totalLido = 0;
				char[] buf = new char[1024];
				while((totalLido = fr.read(buf)) > 0){
					fw.write(buf, 0, totalLido);
					fw.flush();
				}
				
			} catch (Exception e) {
				SingletonLog.insereErro(e, SingletonLog.TIPO.SINCRONIZADOR);
			} finally{
				if(fr != null ){
					try {
						fr.close();
					} catch (IOException e) {
						SingletonLog.insereErro(e, SingletonLog.TIPO.SINCRONIZADOR);
					} 
				}
				if(fw != null){
					try{
						fw.close();
					} catch (IOException e) {
						SingletonLog.insereErro(e, SingletonLog.TIPO.SINCRONIZADOR);
					} 
				}
			}
		}
	    
public static File[] getFilesOrderByLastDateModified( File[] vVetorFile, boolean pIsAsc, FileFilter pFileFilter){
			
			
			ArrayList<File> vList = new ArrayList<File>(); 
			
			for (File vFile : vVetorFile) {
				vList.add(vFile);
			}
			
			Collections.sort(vList, new FileDateListComparator(pIsAsc));
			
			
			File[] vNewVetorFile = new File[vList.size()];
			vList.toArray(vNewVetorFile);
			
			return vNewVetorFile;
		}
		
}
