package app.omegasoftware.pontoeletronico.file;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;

public class HelperZip {

	private static final int BUFFER = 2048;
	public static final void zipDirectory( File directory, BufferedOutputStream pOutStream ) throws IOException {
		ZipOutputStream zos = new ZipOutputStream( pOutStream);
		zip( directory, directory, zos );
		zos.close();
	}
	
	

	public static void zipVetorFile(File pVetorFile[], File pDiretorioRaiz, BufferedOutputStream pOutStream){
		try{
			ZipOutputStream zos = new ZipOutputStream( pOutStream);
			for(File file : pVetorFile) {
				if(file == null){
					continue;
				}
				if(file.isDirectory()){
					zip( file, pDiretorioRaiz, zos );
				} else{
					zipFile(file, zos);
				}
			}
			zos.close();
		} catch(Exception e) { 
			//Log.e(TAG,"zip(): " + e.getMessage()); 
		}
		
	}
	
	public static void zipVetorFile(File pVetorFile[], File pFileZip){
		try{			
			ZipOutputStream zos = new ZipOutputStream( new FileOutputStream( pFileZip ));
			String vPath = HelperFile.getPathOfFile( pFileZip.getPath());
			
			
			File base = new File(vPath);
			if(base.exists() && base.isDirectory()){
				for(File file : pVetorFile) {
					if(file == null){
						continue;
					}
					if(file.isDirectory()){
						zip( file, base, zos );
					} else{
						zipFile(file, zos);
					}
				}
			}
			zos.close();
		} catch(Exception e) {
			SingletonLog.insereErro(e, TIPO.PAGINA);
			 
		}
		
	}
	
	public static final void zipFile(File file, ZipOutputStream out){
		BufferedInputStream origin = null; 
		
		try {
			byte data[] = new byte[BUFFER]; 
			if(file == null){
				return;
			}
			//Log.d(TAG, "zip(): Zipping file " + file.getName()); 
			FileInputStream fi = new FileInputStream(file); 
			origin = new BufferedInputStream(fi, BUFFER); 
			ZipEntry entry = new ZipEntry(file.getName());
			out.putNextEntry(entry); 
			int count; 
			while ((count = origin.read(data, 0, BUFFER)) != -1) { 
				out.write(data, 0, count); 
			} 
			origin.close();	
			fi.close();
			
		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.PAGINA);
			return;
		} 
	}
	
	

	public static final void zipFile(File file, BufferedOutputStream zipFile){
		BufferedInputStream origin = null; 
		
		try {

			ZipOutputStream out = new ZipOutputStream(zipFile); 

			byte data[] = new byte[BUFFER]; 
			if(file == null){
				return;
			}
			//Log.d(TAG, "zip(): Zipping file " + file.getName()); 
			FileInputStream fi = new FileInputStream(file); 
			origin = new BufferedInputStream(fi, BUFFER); 
			ZipEntry entry = new ZipEntry(file.getName());
			out.putNextEntry(entry); 
			int count; 
			while ((count = origin.read(data, 0, BUFFER)) != -1) { 
				out.write(data, 0, count); 
			} 
			origin.close();
			fi.close();
			out.close();
		} catch (Exception e) {
			
			return;
		} 
	}
	
	
	
	public static final void zipFile(File file, File zipFile){
		BufferedInputStream origin = null; 
		FileOutputStream dest;
		ZipOutputStream out =null;
		try {
			dest = new FileOutputStream(zipFile);

			out = new ZipOutputStream(new BufferedOutputStream(dest)); 

			byte data[] = new byte[BUFFER]; 
			if(file == null){
				return;
			}
			//Log.d(TAG, "zip(): Zipping file " + file.getName()); 
			FileInputStream fi = new FileInputStream(file); 
			origin = new BufferedInputStream(fi, BUFFER); 
			ZipEntry entry = new ZipEntry(file.getName());
			out.putNextEntry(entry); 
			int count; 
			while ((count = origin.read(data, 0, BUFFER)) != -1) { 
				out.write(data, 0, count); 
			} 
			origin.close();
			fi.close();
			out.close();
		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.UTIL_ANDROID);
			return;
		} finally{
			if(out != null)
				try {
					out.close();
				} catch (IOException e) {
					SingletonLog.insereErro(e, TIPO.UTIL_ANDROID);
				}
		}
	}
	
	public static final void zipDirectory( File directory, File zip ) throws IOException {
		ZipOutputStream zos = new ZipOutputStream( new FileOutputStream( zip ) );
		zip( directory, directory, zos );
		zos.close();
	}

	private static final void zip(File directory, File base,
			ZipOutputStream zos) throws IOException {
		File[] files = directory.listFiles();
		byte[] buffer = new byte[8192];
		int read = 0;
		if(files != null)
		for (int i = 0, n = files.length; i < n; i++) {
			if (files[i].isDirectory()) {
				zip(files[i], base, zos);
			} else {
				FileInputStream in = new FileInputStream(files[i]);
				ZipEntry entry = new ZipEntry(files[i].getPath().substring(
						base.getPath().length() + 1));
				zos.putNextEntry(entry);
				while (-1 != (read = in.read(buffer))) {
					zos.write(buffer, 0, read);
				}
				in.close();
			}
		}
	}
	public static final void unzipToFile(File zip, FileOutputStream fou) throws IOException {
		ZipFile archive = new ZipFile(zip);
		
		Enumeration<? extends ZipEntry> e = archive.entries();
		if (e.hasMoreElements()) {
			ZipEntry entry = (ZipEntry) e.nextElement();
			

			InputStream in = archive.getInputStream(entry);
			
			
			

			byte[] buffer = new byte[8192];
			int read;

			while (-1 != (read = in.read(buffer))) {
				fou.write(buffer, 0, read);
			}
			in.close();
			
			
			archive.close();
		}
	}
	
	public static final void unzipToDirectory(File zip, File pPathDirectoryOut) throws IOException {
		ZipFile archive = new ZipFile(zip);
		Enumeration<? extends ZipEntry> e = archive.entries();
		while (e.hasMoreElements()) {
			ZipEntry entry = (ZipEntry) e.nextElement();
			File file = new File(pPathDirectoryOut, entry.getName());
			if (entry.isDirectory() && !file.exists()) {
				file.mkdirs();
			} else {
				if (!file.getParentFile().exists()) {
					file.getParentFile().mkdirs();
				}

				InputStream in = archive.getInputStream(entry);
				BufferedOutputStream out = new BufferedOutputStream(
						new FileOutputStream(file));

				byte[] buffer = new byte[8192];
				int read;

				while (-1 != (read = in.read(buffer))) {
					out.write(buffer, 0, read);
				}
				in.close();
				out.close();
			}
		}
	}
	

	public static void zipVetorFile(File pVetorFile[], FileOutputStream pFileZip){
		ZipOutputStream zos = null;
		try{			
			zos = new ZipOutputStream( pFileZip);
			
			for(File file : pVetorFile) {
				if(file == null){
					continue;
				}
				zipFile(file, zos);
			}
		
			
		} catch(Exception e) {
			SingletonLog.insereErro(e, TIPO.PAGINA);
			 
		}finally{
			if(zos!=null)
				try {
					zos.close();
				} catch (IOException e) {
					SingletonLog.insereErro(e, TIPO.PAGINA);
				}
		}
		
	}
}
