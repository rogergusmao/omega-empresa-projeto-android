package app.omegasoftware.pontoeletronico.file;

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap.CompressFormat;
import android.os.Environment;

public class OmegaFileConfiguration {

	//File storage configuration
	public static final String BASE_DIRECTORY_PATH = "";
	public static final String LOG_FILES_EXTENSION = ".txt";
	public static final String ZIP_FILES_EXTENSION = ".zip";
	public static final String PICTURE_FILES_EXTENSION = ".jpeg";
	public static final int PERCENT_QUALITY = 50;
	public static final CompressFormat COMPRESS_FORMAT = CompressFormat.JPEG;
	
	public static final boolean IS_TO_SEND_DATA_IN_FILE = true;
	
	public static String root;
	
	public static String getPath(){
		if(root == null){
			root = Environment.getExternalStorageDirectory().getAbsolutePath() + "/PontoEletronico/"; 
		}
		return root;
	}
	public enum TIPO{
		FOTO,
		LOG_ENVIO,
		SINCRONIZACAO,
		//LOG_ERRO,
		//LOG_WARNING,
		LOG,
		AUDIO,
		VIDEO,
		ANEXO,
		SCREENSHOT
	}
	public String getPath(TIPO tipo){
		String path = getPath() + tipo.toString();
		HelperFile.createDirectory(path);
		return path + "/";
	}
	
	//Para diretorios de armazenamento interno do app
	public String getPathPrivate(TIPO tipo, Context context){
		String path =  tipo.toString();
		File mydir = context.getDir(path, Context.MODE_PRIVATE); //Creating an internal dir;
		if (!mydir.exists())
		{
		     mydir.mkdirs();
		}
		return path + "/";
	}
	public File getDirPrivate(TIPO tipo, Context context){
		String path =  tipo.toString();
		File mydir = context.getDir(path, Context.MODE_PRIVATE); //Creating an internal dir;
		if (!mydir.exists())
		{
		     mydir.mkdirs();
		}
		return mydir;
	}
	
	
	
	public static String getFileUniqueName(String pExtension){
		return HelperFile.getFileWithUniqueName() + pExtension;
	}

}
