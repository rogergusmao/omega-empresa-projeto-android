package app.omegasoftware.pontoeletronico.file;

import android.graphics.Bitmap;

public class OmegaFileConstants {

	//Error logging configuration
	public static final String ERROR_LOG_FILES_EXTENSION = ".log";
	
	//Camera configuration
	public static final Bitmap.CompressFormat PICTURE_COMPRESS_FORMAT = Bitmap.CompressFormat.JPEG;
	public static final int PICTURE_COMPRESS_QUALITY = 50;
	
	//Timing configurations
	public static final long INITIAL_DELAY = 2000;
	public static final long REPORTER_TIMER_INTERVAL = 30000;
	public static final long UI_UPDATE_TIMER_INTERVAL = 10000;
	public static final long PICTURE_TAKER_TIMER_INTERVAL = 120000;
	public static final long TAKE_PICTURE_DELAY = 2000;
	
	//Intents configuration
	public static final String REFRESH_UI_INTENT = "omega.trucktracker.UPDATE_UI_INTENT";
	public static final String TAKE_PICTURE_INTENT = "omega.trucktracker.TAKE_PICTURE_INTENT";
	
	//File storage configuration
	public static final String BASE_DIRECTORY_PATH = "";
	public static final String LOG_FILES_EXTENSION = ".txt";
	public static final String ZIP_FILES_EXTENSION = ".zip";
	public static final String PICTURE_FILES_EXTENSION = ".jpeg";
	
	
	
	public static final int MAX_MESSAGES_STACK_SIZE = 2000;
	
	//GPS configuration
	public static final int MIN_TIME_LOCATION_UPDATE = 5000; //milliseconds
	public static final int MIN_DISTANCE_LOCATION_UPDATE = 5; //meters

	//Service state intent extras configuration
	public static final String SS_GPS_STATE = "SS_GPS_STATE";
	public static final String SS_INTERNET_STATE = "SS_INTERNET_STATE";
	public static final String SS_STORED_FILES_SIZE = "SS_STORED_FILES_SIZE";
	
	//Internet states
	public static final int INTERNET_INACTIVE = -1;
	public static final int INTERNET_ACTIVE_DISCONNECTED = 0;
	public static final int INTERNET_ACTIVE_CONNECTED = 1;
	
	
}
