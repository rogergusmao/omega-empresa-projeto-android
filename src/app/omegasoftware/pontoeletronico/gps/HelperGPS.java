package app.omegasoftware.pontoeletronico.gps;

//public class HelperGPS{
//
////	private static final String TAG = "HelperGPS";
//	
//	/**
//	 * This method checks if GPS is enabled
//	 * @param p_context
//	 * @return {boolean}
//	 */
//    public static boolean checkIfGPSIsEnabled(Context p_context)
//    {
//		//Getting location manager service
//    	LocationManager manager = (LocationManager) p_context.getSystemService(Context.LOCATION_SERVICE);
//    	
//    	//Check if GPS is available
//        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//        	//It is available
//        	//Log.d(TAG, "checkIfGPSIsEnabled(): GPS enabled");
//        	return true;
//        }
//        else
//        {
//        	//Log.d(TAG, "checkIfGPSIsEnabled(): GPS not enabled");
//        	return false;
//        }
//    }
//    
//    public static boolean startGPSIfNecessary(Context p_context){
//		if(!HelperGPS.checkIfGPSIsEnabled(p_context))
//		{
//			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//			p_context.startActivity(intent);
//			if(HelperGPS.checkIfGPSIsEnabled(p_context)) return true;
//			else return false;
//		} else 
//			return true;
//
//    }
//    
//	public static String generateRandomString()
//	{
//		Random random = new Random();
//		String hash = Long.toHexString(random.nextLong());
//		return hash;
//	}
//	
//	public static String getIMEI(Context p_context)
//	{
//		TelephonyManager telephonyManager = (TelephonyManager) p_context.getSystemService(Context.TELEPHONY_SERVICE);
//		return telephonyManager.getDeviceId();
//	}
//	
//	public static String readResponseContent(HttpResponse p_response) throws IllegalStateException, IOException
//	{
//		Reader reader = null;
//		try {
//			reader = new InputStreamReader(p_response.getEntity().getContent());
//			StringBuffer sb = new StringBuffer();
//			{
//				int read;
//				char[] cbuf = new char[1024];
//				while ((read = reader.read(cbuf)) != -1)
//					sb.append(cbuf, 0, read);
//			}
//			return sb.toString();
//		} finally {
//			if (reader != null) {
//				try {
//					reader.close();
//				} catch (IOException e) {
//					SingletonLog.insereWarning(e, SingletonLog.TIPO.UTIL_ANDROID);
//				}
//			}
//		}
//	}
//	
//	public static String formatBytesSize(long p_size)
//	{
//		if(p_size < 1024)
//		{
//			return p_size + " B";
//		}
//		else if(p_size > 1024 && p_size < 1024*1024)
//		{
//			return (p_size/1024) + " KB";
//		}
//		else if(p_size > 1024*1024)
//		{
//			return (p_size/(1024*1024)) + " MB";
//		}
//		return "";
//	}
//	
//    
//}
