package app.omegasoftware.pontoeletronico.gps;

//public class VeiculoGPSLocationListener{
//
//	
//
//	private Database db = null;
//	
//	String idUsuario = null;
//	private EXTDAOUsuarioPosicao objUsuarioPosicao = null;
//	
//	
//	ReceiverBroadcastLocalizacao receiverLocalizacao;
//	
//	public VeiculoGPSLocationListener(
//			Context pContext){
//		
//		receiverLocalizacao = new ReceiverBroadcastLocalizacao(pContext, this);
//		idUsuario = OmegaSecurity.getIdUsuario();
//		if(pContext != null){
//			try {
//				db = new DatabasePontoEletronico(pContext);
//			} catch (OmegaDatabaseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			objUsuarioPosicao = new EXTDAOUsuarioPosicao(db);
//			
//		}
//		ContainerLocalizacao vContainer = GPSControler.getLocalizacao();
//		procedureInsertLocationInDatabase(vContainer);
//
//
//	}
//
//	
//
//	@Override
//	protected void finalize(){
//		
//		try {
//			receiverLocalizacao.unregisterReceiverIfExist();
//			if(db != null)
//				db.close();
//			super.finalize();
//		} catch (Throwable e) {
//			SingletonLog.insereWarning(e, SingletonLog.TIPO.UTIL_ANDROID);
//		}
//
//	}
//	
//	private void procedureInsertLocationInDatabase(ContainerLocalizacao pContainerLocalizacao){
//		if(pContainerLocalizacao == null) return;
//		
//		HelperDate vHelperDate = new HelperDate();
//		Integer vLatitude = pContainerLocalizacao.getLatitude();
//		Integer vLongitude = pContainerLocalizacao.getLongitude();
//		
//		if(vLatitude == null || vLongitude == null) return;
//		
//		String vStrLatitude = String.valueOf(vLatitude);
//		String vStrLongitude = String.valueOf(vLongitude);
//		this.objUsuarioPosicao.clearData();
//		if(OmegaSecurity.getIdVeiculoUsuario() != null && OmegaSecurity.getIdVeiculoUsuario().length() > 0 )
//				objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.VEICULO_USUARIO_ID_INT, OmegaSecurity.getIdVeiculoUsuario());
//		objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.USUARIO_ID_INT, idUsuario);
//		//-15.837967133675603
//		objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.LATITUDE_INT, vStrLatitude);
//		objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.LONGITUDE_INT, vStrLongitude);
//		if(pContainerLocalizacao.getVelocidade() != null){
//			objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.VELOCIDADE_INT, String.valueOf((int)(pContainerLocalizacao.getVelocidade() * 3.6)));	
//		}
//
//		String vLastNomeFotoInternaUsuario = ServiceTiraFoto.getLastNomeFotoInternaInserida();
//		if(vLastNomeFotoInternaUsuario != null){
//			objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.FOTO_INTERNA, vLastNomeFotoInternaUsuario);
//			ServiceTiraFoto.clearLastNomeFotoInternaInserida();
//		}
//
//		objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.CADASTRO_SEC, vHelperDate.getDateDisplay());
//		objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.CADASTRO_OFFSEC, vHelperDate.getTimeDisplay());
////		vEXTDAOUsuarioPosicao.setAttrStrValue(EXTDAOUsuarioPosicao.HORA_TIME, vHelperDate.getTimeDisplay());
//		objUsuarioPosicao.setAttrValue(EXTDAOUsuarioPosicao.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
//		
//		objUsuarioPosicao.insert(false);
//		db.close();
//	}
//
//	public void insertLocationInDatabase(ContainerLocalizacao pContainerLocalizacao){
//		if(pContainerLocalizacao != null ){
//			procedureInsertLocationInDatabase(pContainerLocalizacao);
//		}
//	}
//
//	
//}
//
