package app.omegasoftware.pontoeletronico.gpsnovo;


import java.util.Date;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSControler.TIPO_GPS;
import app.omegasoftware.pontoeletronico.mapa.HelperMapa;

public class AGPSServiceLocationListener implements LocationListener {

	static GeoPoint geoPoint;
	static protected LocationManager locationManagerAGPS;
	
	static Date lastUpdateDate = null;
	static protected Context context = null;
	private static AGPSServiceLocationListener singletonAGPSServiceLocationListenerAGPS;
	
	protected AGPSServiceLocationListener(Context pContext){

		locationManagerAGPS = (LocationManager) pContext.getSystemService(Context.LOCATION_SERVICE);
		
		context = pContext;
		loadLocationManagerAGPS(this);		
	}
	
	public Date getLastUpdateDate(){
		return lastUpdateDate;
	}
	
	public Float getSpeed(){
		if(geoPoint != null)
			return geoPoint.getSpeed();
		else return null;
	}
	
	public static AGPSServiceLocationListener construct(Context pContext){
		if(singletonAGPSServiceLocationListenerAGPS == null) singletonAGPSServiceLocationListenerAGPS = new AGPSServiceLocationListener(pContext);
		
		if(!isActiveAGPS()){
			loadLocationManagerAGPS(singletonAGPSServiceLocationListenerAGPS);
		}
		
		return  singletonAGPSServiceLocationListenerAGPS;
	}
	
	public static boolean isActiveAGPS(){
		return hasLocation();
	}
	
	public static boolean hasLocation(){
		if(geoPoint == null) return false;
		else return true;
	}
	
	public static void loadLocationManagerAGPS(LocationListener pLocationListener){
		try{
			
			
			locationManagerAGPS.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER, 
					GPSConfiguration.MIN_TIME_LOCATION_UPDATE, 
					GPSConfiguration.MIN_DISTANCE_LOCATION_UPDATE_AGPS,
					pLocationListener);
			
			Location loc = locationManagerAGPS.getLastKnownLocation( LocationManager.NETWORK_PROVIDER);
			
			updateLocalizacao(loc);
			
		} catch(Exception ex){
			SingletonLog.insereWarning(ex, SingletonLog.TIPO.UTIL_ANDROID);
		}
		
	}
	
	
	public Double getLongitudeDouble(){
		Integer longitude = getLongitude();
		if(longitude != null) {
			double vValor = longitude / 1000000;
			return vValor;
		}
		else return null;
	}
	
	public Double getLatitudeDouble(){
		Integer latitude = getLatitude();
		if(latitude != null) {
			double vValor = latitude / 1000000;
			return vValor;
		}
		else return null;
	}

	public Integer getLatitude(){
		
		if(geoPoint != null) return geoPoint.getLatitudeE6();
		else {
			return null;
		}
	}
	
	public Integer getLongitude(){
		if(geoPoint != null) return geoPoint.getLongitudeE6();
		else {
			return null;
		}
		
	}

	public GeoPoint getGeoPoint(){

		return geoPoint;
	}

	public void onProviderDisabled(String arg0) {
		
		
	}

	public void onProviderEnabled(String arg0) {
		
		
	}

	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		
		
	}

	public void onLocationChanged(Location pLocation) {
		try{
			updateLocalizacao(pLocation);
				
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.UTIL_ANDROID);
		}		
	}
	
	private static void updateLocalizacao(Location pLocation){
		if(pLocation==null)return;
		lastUpdateDate = new Date();
		geoPoint = HelperMapa.getGeoPoint(pLocation);
		
		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_LOCALIZACAO);
		context.sendBroadcast(updateUIIntent);
		 
		GPSControler.persistirLocalizacaoNoBanco(
				TIPO_GPS.AGPS,
				context,
				geoPoint.getLatitudeE6(), 
				geoPoint.getLongitudeE6(),
				geoPoint.getSpeed());
	}


	public String getLocationPrint(String p_delimitador){
		if(geoPoint == null){
			return null;
		}
		else return String.valueOf(geoPoint.getLatitudeE6()/1000000) + p_delimitador + String.valueOf(geoPoint.getLongitudeE6()/1000000);
	}

}
