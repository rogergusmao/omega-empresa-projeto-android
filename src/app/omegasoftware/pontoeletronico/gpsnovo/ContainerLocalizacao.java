package app.omegasoftware.pontoeletronico.gpsnovo;

public class ContainerLocalizacao{
	public int latitude;
	public int longitude;
	public Float velocidade;
	
	public ContainerLocalizacao(int pLatitude, int pLongitude, float pVelocidade){
		latitude = pLatitude;
		longitude= pLongitude;
		velocidade = pVelocidade;
	}
	
	public int getLatitude(){
		return latitude;
	}
	
	public int getLongitude(){
		return longitude;
	}
	
	public Float getVelocidade(){
		return velocidade;
	}
}
