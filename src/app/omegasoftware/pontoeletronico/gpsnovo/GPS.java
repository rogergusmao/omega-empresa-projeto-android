package app.omegasoftware.pontoeletronico.gpsnovo;




import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import app.omegasoftware.pontoeletronico.mapa.HelperMapa;


public class GPS
{
    private LocationManager lm;
    private LocationListener locationListener;
//    private OmegaMapActivity activity;
//    public InfoItemizedOverlay infoOverlay;
    
	private double ultimaLatitude;
	private double ultimaLongitude;
	public HelperMapa mapaHelper;
	
    public GPS(Context act){
    	
//    	activity = (OmegaMapActivity ) act;

    	lm = (LocationManager) act.getSystemService(Context.LOCATION_SERVICE);    
    	
	    locationListener = new MyLocationListener();
	    
	    lm.requestLocationUpdates(
	        LocationManager.GPS_PROVIDER, 
	        0, 
	        0, 
	        locationListener);        

    }
    
    
    
    public double getLatitude(){
    	return ultimaLatitude;
    }
    
    public double getLongitude(){
    	return ultimaLongitude;
    }
    
    public static double metrosToGraus(double p_metros){
    	double graus = 0 ;
    	
    	graus = p_metros / 111120;
    	return graus;
    }
    
    public static double grausToMetros(double p_graus){
    	double metros = 0 ;
    	
    	metros = p_graus * 111120;
    	return metros;
    }
    
    
    
    private class MyLocationListener implements LocationListener 
    {    	
        public void onLocationChanged(Location loc) {
            
        	if (loc != null) {

        			
        		ultimaLatitude = loc.getLatitude();
        		ultimaLongitude = loc.getLongitude();
            }
        }

        public void onProviderDisabled(String provider) {
            
        }

        public void onProviderEnabled(String provider) {
            
        }

        public void onStatusChanged(String provider, int status, 
            Bundle extras) {
            
        }
        
    }        
    
}
