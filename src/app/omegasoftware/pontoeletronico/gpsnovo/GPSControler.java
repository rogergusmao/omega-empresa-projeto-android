package app.omegasoftware.pontoeletronico.gpsnovo;

import android.app.Activity;
import android.content.Context;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioPosicao;

public class GPSControler {
	static app.omegasoftware.pontoeletronico.gpsnovo.GPSServiceLocationListener locationListenerGPS = null;
	static app.omegasoftware.pontoeletronico.gpsnovo.AGPSServiceLocationListener locationListenerAGPS = null;
	
	private GPSControler(Activity pActivity){
		if(locationListenerGPS == null)
			locationListenerGPS = app.omegasoftware.pontoeletronico.gpsnovo.GPSServiceLocationListener.construct(pActivity);
		
		if(locationListenerAGPS == null)
			locationListenerAGPS = app.omegasoftware.pontoeletronico.gpsnovo.AGPSServiceLocationListener.construct(pActivity);		
	}
	
	public static void construct(Activity pActivity){
		if(locationListenerAGPS == null || locationListenerGPS == null)
			new GPSControler(pActivity);
	}
	
	public static boolean hasLocation(){
		if(!GPSServiceLocationListener.hasLocation() && !AGPSServiceLocationListener.hasLocation())
			return false;
		else return true;
	}
	
	public enum TIPO_GPS{
		NENHUM(null), GPS(1), AGPS(2);
		Integer id;
		TIPO_GPS(Integer id){
			this.id=id;
		}
		public Integer getId(){return id;}
		public String getStrId(){
			if(id==null) return null; 
			else return id.toString();
		}
	}
	
	
	public static TIPO_GPS getTipoGps(){
		TIPO_GPS vTipoGPS = TIPO_GPS.NENHUM;
		if(GPSServiceLocationListener.hasLocation()
			&& AGPSServiceLocationListener.hasLocation()
			&& locationListenerGPS != null
			&& locationListenerGPS.getLastUpdateDate() != null
			&& locationListenerAGPS != null
			&& locationListenerAGPS.getLastUpdateDate() != null )
		{
			//da muita prioridade para o GPS comparado ao AGPS
			long vLastGPS = locationListenerGPS.getLastUpdateDate().getTime() + (2* GPSConfiguration.MIN_TIME_LOCATION_UPDATE);
			long vLastAGPS = locationListenerAGPS.getLastUpdateDate().getTime();
			
			
			if(vLastGPS < vLastAGPS) vTipoGPS = TIPO_GPS.AGPS;
			else vTipoGPS = TIPO_GPS.GPS;
			return vTipoGPS;
		}
		else if(GPSServiceLocationListener.hasLocation())
			return TIPO_GPS.GPS;	
		else if(AGPSServiceLocationListener.hasLocation())
			return TIPO_GPS.AGPS;
		 else return null;
	}
	public static ContainerLocalizacao getLocalizacao(){
		TIPO_GPS tipo = getTipoGps();
		if(tipo!=null)
			switch (tipo) {
				case AGPS:
					return new ContainerLocalizacao(locationListenerAGPS.getLatitude(), locationListenerAGPS.getLongitude(), locationListenerAGPS.getSpeed());
					
				case GPS:
					return new ContainerLocalizacao(locationListenerGPS.getLatitude(), locationListenerGPS.getLongitude(), locationListenerGPS.getSpeed());
					
				default:
					return null;
			}
		else
			return null;
	
	}
	
	
	public static void persistirLocalizacaoNoBanco(
			TIPO_GPS tipoOrigem,
			Context context,
			int lat, 
			int lng, 
			float speed){
		if(tipoOrigem==null)return;
		if(tipoOrigem == TIPO_GPS.AGPS){
			TIPO_GPS ultimo = getTipoGps();
			//da prioridade para o AGPS
			if(ultimo != null 
				&& ultimo == TIPO_GPS.AGPS){
				EXTDAOUsuarioPosicao.procedureInsertLocationInDatabase(
						context,
						lat, 
						lng,
						speed,
						tipoOrigem);	
			}
				
		} else{
			EXTDAOUsuarioPosicao.procedureInsertLocationInDatabase(
					context,
					lat, 
					lng,
					speed,
					tipoOrigem);
		}
		
		
	}
}
