package app.omegasoftware.pontoeletronico.gpsnovo;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;

public class GPSServiceBroadcastReceiver extends BroadcastReceiver{
	

	public static final String TAG = "GPSServiceBroadcastReceiver";
	private GPSContainer gpsContainer;
	
	public GPSServiceBroadcastReceiver(Activity p_activity){
		gpsContainer = new GPSContainer();

        try {
        	//Starts the TruckTrackerService
        	Intent serviceIntent = new Intent(p_activity, GPSService.class);
        	p_activity.startService(serviceIntent);
        }
        catch (Exception e) {
        	SingletonLog.insereWarning(e, SingletonLog.TIPO.UTIL_ANDROID);
        }
	}
	@Override
	public void onReceive(Context context, Intent intent) {

		if(intent.getAction().equals(GPSConfiguration.REFRESH_UI_INTENT)) {
			gpsContainer.loadIntent(intent);

		}
	}
	public GPSContainer getGPSContainer(){
		return gpsContainer;
	}
}

