package app.omegasoftware.pontoeletronico.gpsnovo;




public class GeoPoint {
	int lat, lng;
	Float speed;
	public GeoPoint(int lat, int lng, Float speed){
		this.lat=lat;
		this.lng=lng;
		this.speed=speed;
	}
	public int getLatitudeE6(){
		return lat;
	}
	public int getLongitudeE6(){
		return lng;
	}
	public float getSpeed(){
		return speed;
	}

}
