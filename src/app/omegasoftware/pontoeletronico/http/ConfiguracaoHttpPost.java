package app.omegasoftware.pontoeletronico.http;

import org.apache.http.client.CookieStore;

import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;


public class ConfiguracaoHttpPost {
	public boolean setCookie = false;
	public CookieStore cookieToSend = null;
	public int pMilisecondTimeOut = OmegaConfiguration.HTTP_TIMEOUT_MILISEGUNDOS;
}
