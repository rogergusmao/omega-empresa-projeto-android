package app.omegasoftware.pontoeletronico.http;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;

public class ConnectionChangeReceiver extends BroadcastReceiver
{
  @Override
  public void onReceive( Context context, Intent intent )
  {
	  try{
		  ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
		    NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
		    NetworkInfo mobNetInfo = connectivityManager.getNetworkInfo(     ConnectivityManager.TYPE_MOBILE );
		    if ( activeNetInfo != null )
		    {
		      Toast.makeText( context, "Active Network Type : " + activeNetInfo.getTypeName(), Toast.LENGTH_SHORT ).show();
		    }
		    if( mobNetInfo != null )
		    {
		      Toast.makeText( context, "Mobile Network Type : " + mobNetInfo.getTypeName(), Toast.LENGTH_SHORT ).show();
		    }	  
	  } catch(Exception ex){
		  Toast.makeText( context, "Telefone offline.", Toast.LENGTH_SHORT ).show();
		  SingletonLog.insereWarning(ex, SingletonLog.TIPO.UTIL_ANDROID);
	  }
    
  }
}

