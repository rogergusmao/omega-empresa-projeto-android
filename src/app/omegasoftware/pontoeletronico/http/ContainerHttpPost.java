package app.omegasoftware.pontoeletronico.http;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.Header;
import org.apache.http.client.methods.HttpPost;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;

public class ContainerHttpPost{
	public static String TAG = "ContainerHttpPost";
	HttpPost httpPost;
	InputStream inputStream;
	Header[] cookieStore;
	public ContainerHttpPost(HttpPost pHttpPost, InputStream pInputStream, Header[] cookieStore){
		httpPost = pHttpPost;
		inputStream = pInputStream;
		this.cookieStore = cookieStore;
	}
	
	public Header[]  getCookieStore(){
		return  cookieStore;
	}
	
	public HttpPost getHttpPost(){
		return httpPost;
	}
	
	public InputStream getInputStream(){
		return inputStream;
	}
	
	public void closeConnection(){
		if(inputStream != null){
			try {
				inputStream.close();
				inputStream = null;
			} catch (IOException e) {
				SingletonLog.insereWarning(e, SingletonLog.TIPO.UTIL_ANDROID);
			}
		}
		if(httpPost != null){
			httpPost.abort();
			httpPost = null;
		}
	}
}