package app.omegasoftware.pontoeletronico.http;

import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.date.HelperDate;

public class HelperHttp {

	//
	// public static Mensagem getStatus(Context c, boolean chamadaHttp){
	//
	// if(!isConnected(c)){
	// return new Mensagem(PROTOCOLO_SISTEMA.TIPO.SEM_CONEXAO_A_INTERNET, "O
	// telefone estu desconectado da internet.");
	//
	// } else {
	// Boolean isServerOnline = isServerOnline(c, chamadaHttp);
	// if(isServerOnline == null){
	// if(chamadaHttp){
	// isServerOnline = isServerOnline(c, chamadaHttp);
	// if(isServerOnline){
	// return new
	// Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
	// } else{
	// return new Mensagem(PROTOCOLO_SISTEMA.TIPO.SERVIDOR_FORA_DO_AR, "O
	// servidor estu em manutenuuo no momento, tente novamente mais tarde.");
	// }
	// }
	// return new
	// Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
	//
	// } else if(!isServerOnline){
	// return new Mensagem(PROTOCOLO_SISTEMA.TIPO.SERVIDOR_FORA_DO_AR, "O
	// servidor estu em manutenuuo no momento, tente novamente mais tarde.");
	// } else return new
	// Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
	// }
	// }

	public static boolean isConnectedWifi(Context pContext) {
		ConnectivityManager cm = (ConnectivityManager) pContext.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifiNetwork != null && wifiNetwork.isConnected()) {
			return true;
		} else return false;
		
	}

	public static boolean isConnectedMobileChip(Context pContext) {
		ConnectivityManager cm = (ConnectivityManager) pContext.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (wifiNetwork != null && wifiNetwork.isConnected()) {
			return true;
		} else return false;
		
	}
	
	public static boolean isConnected(Context pContext) {
		ConnectivityManager cm = (ConnectivityManager) pContext.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifiNetwork != null && wifiNetwork.isConnected()) {
			return true;
		}

		NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (mobileNetwork != null && mobileNetwork.isConnected()) {
			return true;
		}

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (activeNetwork != null && activeNetwork.isConnected()) {
			return true;
		}

		return false;
	}

	// public static boolean isServerOnline(Context pContext, int
	// pNumeroTentativas){
	//
	// for(int i = 0 ; i < pNumeroTentativas; i++){
	// if(isServerOnline(pContext)) return true;
	// try {
	// Thread.sleep(500);
	// } catch (InterruptedException e) {
	// SingletonLog.insereWarning(e, TIPO.UTIL_ANDROID);
	// }
	// }
	// return false;
	// }

	// public static boolean isConnected(Context pContext, int
	// pNumeroTentativas){

	// if(OmegaConfiguration.DEBUGGING)
	// return true;
	// Random r = new Random();
	// for(int i = 0 ; i < pNumeroTentativas; i++){
	//
	// if(isConnected(pContext)) return true;
	// try {
	// Thread.sleep(r.nextInt(r.nextInt(5000)));
	// } catch (InterruptedException e) {
	// SingletonLog.insereWarning(e, TIPO.UTIL_ANDROID);
	// }
	// }
	// return false;
	// }

	public static DefaultHttpClient getHttpClientWithouthVerifyPermissions(HttpParams pParams) {
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		schemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(), 443));
		if (pParams == null)
			pParams = new BasicHttpParams();
		HttpProtocolParams.setVersion(pParams, HttpVersion.HTTP_1_1);

		ClientConnectionManager cm = new SingleClientConnManager(pParams, schemeRegistry);
		return new DefaultHttpClient(cm, pParams);
	}

	static HelperDate ultimaVerificacao = null;
	static ReentrantLock re = new ReentrantLock();

	// public static Boolean isServerOnline(
	// Context pContext,
	// boolean chamadaHttp) {
	// if(chamadaHttp){
	//
	// boolean novoStatus= isUrlAtiva(pContext,
	// OmegaConfiguration.URL_REQUEST_CHECK_SERVER());
	// if(serverOnline!= novoStatus ){
	// serverOnline = novoStatus;
	// }
	// }
	// return serverOnline;
	//
	// }

	public static Boolean isUrlAtiva(Context pContext, String vUrl) {

		if (vUrl == null || vUrl.length() == 0)
			return false;
		else {
			if (re.tryLock()) {
				try {
					if (vUrl.startsWith("https"))
						return isUrlHttpsAtiva(pContext, vUrl);
					else
						return isUrlHttpAtiva(pContext, vUrl);
				} finally {
					re.unlock();
				}

			}

		}
		return null;
	}

	// private static boolean procedureIsServerOnlineHttps(
	// Context pContext,
	// boolean sendBroadCast) {
	// HttpPost httppost = null;
	// try{
	//// HostnameVerifier hostnameVerifier =
	// org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
	// HttpParams param = new BasicHttpParams();
	// //this how tiny it might seems, is actually absoluty needed. otherwise
	// http client lags for 2sec.
	// HttpProtocolParams.setVersion(param, HttpVersion.HTTP_1_1);
	// HttpConnectionParams.setConnectionTimeout( param, 1000);
	// DefaultHttpClient httpClient =
	// HelperHttp.getHttpClientWithouthVerifyPermissions(param);
	//
	// httppost = new HttpPost(OmegaConfiguration.URL_REQUEST_CHECK_SERVER());
	// httpClient.execute(httppost);
	// return true;
	// }catch(UnknownHostException ex2 ){
	// SingletonLog.insereErro("", "", "Server oFfline: UnknownHostException",
	// TIPO.HTTP);
	// }
	// catch(HttpHostConnectException ex){
	// SingletonLog.insereErro("", "", "Server oFfline:
	// HttpHostConnectException", TIPO.HTTP);
	// }
	// catch (ConnectTimeoutException ex){
	// SingletonLog.insereErro("", "", "Server oFfline:
	// ConnectTimeoutException", TIPO.HTTP);
	// } catch (Exception ex){
	// SingletonLog.insereErro("", "", "Server oFfline: Exception", TIPO.HTTP);
	// } finally{
	// if(httppost != null)
	// httppost.abort();
	//
	// }
	// return false;
	// }

	private static boolean isUrlHttpsAtiva(Context pContext, String url) {
		HttpPost httppost = null;
		try {
			// HostnameVerifier hostnameVerifier =
			// org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
			HttpParams param = new BasicHttpParams();
			// this how tiny it might seems, is actually absoluty needed.
			// otherwise http client lags for 2sec.
			HttpProtocolParams.setVersion(param, HttpVersion.HTTP_1_1);
			HttpConnectionParams.setConnectionTimeout(param, 1000);
			DefaultHttpClient httpClient = HelperHttp.getHttpClientWithouthVerifyPermissions(param);

			httppost = new HttpPost(url);
			httpClient.execute(httppost);
			return true;
		} catch (Exception ex) {
			// SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
		} finally {
			if (httppost != null)
				httppost.abort();

		}
		return false;
	}
	// private static boolean procedureIsServerOnlineHttp(
	// Context pContext,
	// boolean sendBroadCast) {
	// HttpPost httppost = null;
	// try{
	// HttpParams param = new BasicHttpParams();
	//
	// HttpProtocolParams.setVersion(param, HttpVersion.HTTP_1_1);
	// HttpConnectionParams.setConnectionTimeout( param, 2000);
	// HttpClient httpclient = new DefaultHttpClient(param);
	// httppost = new HttpPost(OmegaConfiguration.URL_REQUEST_CHECK_SERVER());
	//
	// httpclient.execute(httppost);
	// return true;
	// }catch(UnknownHostException ex2 ){
	// SingletonLog.insereErro("", "", "Server oFfline: UnknownHostException",
	// TIPO.HTTP);
	// }
	// catch(HttpHostConnectException ex ){
	// SingletonLog.insereErro("", "", "Server oFfline:
	// HttpHostConnectException", TIPO.HTTP);
	//
	// }
	// catch (ConnectTimeoutException ex){
	// SingletonLog.insereErro("", "", "Server oFfline:
	// ConnectTimeoutException", TIPO.HTTP);
	// }
	// catch(SocketException se ){
	// SingletonLog.insereErro("", "", "Server oFfline: SocketException",
	// TIPO.HTTP);
	// }
	// catch (Exception ex){
	// SingletonLog.insereErro("", "", "Server oFfline: Exception", TIPO.HTTP);
	// } finally{
	// if(httppost != null)
	// httppost.abort();
	//
	// }
	// return false;
	// }

	private static boolean isUrlHttpAtiva(Context pContext, String url) {
		HttpPost httppost = null;
		try {
			HttpParams param = new BasicHttpParams();

			HttpProtocolParams.setVersion(param, HttpVersion.HTTP_1_1);
			HttpConnectionParams.setConnectionTimeout(param, 2000);

			HttpClient httpclient = new DefaultHttpClient(param);
			// httppost = new
			// HttpPost(OmegaConfiguration.URL_REQUEST_CHECK_SERVER());
			httppost = new HttpPost(url);

			httpclient.execute(httppost);
			return true;
		} catch (HttpHostConnectException ex) { // Caso a internet seja cortada
												// no meio da operauuo
			// SingletonLog.insereWarning(ex, TIPO.UTIL_ANDROID);
		} catch (UnknownHostException ex2) {
			// SingletonLog.insereErro(ex2, TIPO.UTIL_ANDROID);
		} catch (ConnectTimeoutException ex) {
			// SingletonLog.insereWarning(ex, TIPO.UTIL_ANDROID);
		} catch (Exception ex) {
			// SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
		} finally {
			if (httppost != null)
				httppost.abort();

		}
		return false;
	}

	protected static void sendBroadcastToActivity(Context pContext, boolean pIsOnline) {
		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SERVICE_STATUS_SERVER);
		updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_STATUS_SERVER, pIsOnline);
		pContext.sendBroadcast(updateUIIntent);
	}

	public static String formatarUrl(String p_url, String[] vetorKey, String[] vetorContent) {

		if (vetorKey != null && vetorKey.length > 0) {
			String get = "";

			for (int i = 0; i < vetorKey.length; i++) {
				if (i == 0 && !p_url.contains("?"))
					get += "?";
				else
					get += "&";

				get += vetorKey[i] + "=" + (vetorContent[i] == null ? "null" : URLEncoder.encode(vetorContent[i]));
			}
			return p_url + get;

		} else
			return p_url;

	}

	public static String getStrGet(String[] vetorKey, String[] vetorContent,
			String[] parametroComplemento, String[] valoresComplemento) {

		if (vetorKey != null && vetorKey.length > 0) {
			String get = "";

			for (int i = 0; i < vetorKey.length; i++) {
				
				get += "&";
				get += vetorKey[i] + "=" + (vetorContent[i] == null ? "null" : URLEncoder.encode(vetorContent[i]));
			}
			
			if(parametroComplemento != null && parametroComplemento.length > 0){
				for(int i = 0 ; i < parametroComplemento.length; i++){
					get += "&";

					get += parametroComplemento[i] + "=" + (valoresComplemento[i] == null ? "null" : URLEncoder.encode(valoresComplemento[i]));	
					
				}
			}
			return get;

		} else
			return null;

	}

}
