package app.omegasoftware.pontoeletronico.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.message.BufferedHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.CharArrayBuffer;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class HelperHttpPost {
	
	// Constants
//	private static final String TAG = "HelperHttpPost";

	public static String ARQUIVO_LOG() {
		return "chamadas_http_" + HelperDate.getNomeArquivoGeradoACadaXMinutos() + ".log";
	}

	public static String ReadInputStream(InputStream in) throws IOException {
		StringBuffer stream = new StringBuffer();
		byte[] b = new byte[4096];
		for (int n; (n = in.read(b)) != -1;) {
			stream.append(new String(b, 0, n));
		}
		return stream.toString();
	}

	private static ContainerHttpPost executeHttpPostData(
			Context pContext, String p_url, String p_vetorKey[],
			String p_vetorContent[], ConfiguracaoHttpPost configuracaoHttpPost) {
		if (p_url == null || p_url.length() == 0)
			return null;
		else{
			Random r = new Random();
			for(int i = 0 ; i < OmegaConfiguration.MAX_TENTIVAS_HTTP; i++){
		
				ContainerHttpPost container = null;
				if (p_url.startsWith("https"))
					container =procedureExecuteHttpsPostData(
							pContext, p_url, p_vetorKey, p_vetorContent, configuracaoHttpPost);
				else
					container =procedureExecuteHttpPostData(
							pContext, p_url, p_vetorKey, p_vetorContent, configuracaoHttpPost);
				if(container != null) return container;
				else
					try {
						Thread.sleep(r.nextInt( OmegaConfiguration.MAX_TEMPO_ESPERA_CHAMADA_CONSECUTIVA_MILISEGUNDOS));
					} catch (InterruptedException e) {
						SingletonLog.insereErro(e, TIPO.HTTP);
						return null;
					};
			}
			return null;
		}
	}

	
	private static ContainerHttpPost procedureExecuteHttpsPostData(
			Context pContext, 
			String purl,
			String pvetorKey[],
			String pvetorContent[],
			ConfiguracaoHttpPost configuracaoHttpPost) {
		if(purl == null) return null;
		HttpPost httppost = null;
		try {
			// HostnameVerifier hostnameVerifier =
			// org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
			HttpParams param = new BasicHttpParams();

			// this how tiny it might seems, is actually absoluty needed.
			// otherwise http client lags for 2sec.
			HttpProtocolParams.setVersion(param, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(param, HTTP.ISO_8859_1);
			HttpProtocolParams.setHttpElementCharset(param, HTTP.ISO_8859_1);

			// int timeoutConnection = 3000;
			if(configuracaoHttpPost != null)
				HttpConnectionParams.setConnectionTimeout(param, configuracaoHttpPost.pMilisecondTimeOut);
			else
				HttpConnectionParams.setConnectionTimeout(param, OmegaConfiguration.HTTP_TIMEOUT_MILISEGUNDOS);

			DefaultHttpClient httpClient = HelperHttp.getHttpClientWithouthVerifyPermissions(param);
			if(configuracaoHttpPost != null && configuracaoHttpPost.cookieToSend!= null){
				httpClient.setCookieStore(configuracaoHttpPost.cookieToSend);
			}
			// Set verifier
			// HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

			httppost = new HttpPost(purl);

			if (pvetorKey != null && pvetorContent != null) {
				List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
				if (pvetorKey.length == pvetorContent.length) {
					for (int i = 0; i < pvetorKey.length; i++) {
						String key = pvetorKey[i];
						String content = pvetorContent[i];
						if (content != null)
							if (content.length() > 0)
								nameValuePairs.add(new BasicNameValuePair(key, content));
					}
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.ISO_8859_1));
				}
			}

			HttpResponse httpResponse = httpClient.execute(httppost);
			
//			final int statusCode = httpResponse.getStatusLine().getStatusCode();
//			if (statusCode != HttpStatus.SC_OK) {
//
//				SingletonLog.insereErro(TAG, "procedureExecuteHttpsPostData",
//						"[EAOKDS] Erro call http: " + statusCode + ". URL: " + purl, TIPO.PAGINA);
//				return null;
//			} else {
				final HttpEntity entity = httpResponse.getEntity();
				if (entity != null) {
					InputStream inputStream = null;

					inputStream = entity.getContent();
					
					return new ContainerHttpPost(
							httppost, 
							inputStream,
							configuracaoHttpPost != null && configuracaoHttpPost.setCookie 
							? httpResponse.getHeaders("Cookie") : null);
				} else {
					httppost.abort();
				}
//			}

			
		} catch (ClientProtocolException e) {
			SingletonLog.insereErro(e, TIPO.PAGINA);
			if(httppost != null)httppost.abort();
		} catch (IOException e) {
			SingletonLog.insereErro(e, TIPO.PAGINA);
			if(httppost != null)httppost.abort();
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			if(httppost != null)httppost.abort();
		} 
		return null;
	}

	static class ControlWaitException{

		HelperDate lastException = null;
		final int MAXIMO_DE_EXCECOES_SEGUIDAS_PARA_ENTRAR_EM_MODO_OCIOSO = 2;
		int iExcecao;
		public ControlWaitException(){

			//refreshPrefixoUrl(URL_PONTO_ELETRONICO);
		}

		private boolean compareDomain(String url){
//			if(URL_PONTO_ELETRONICO.startsWith(this.prefixoUrl)) return true;
//			else return false;
			return true;
		}
		private void refreshPrefixoUrl(String url ){
//			this.prefixoUrl = URL_PONTO_ELETRONICO.substring(0 , URL_PONTO_ELETRONICO.indexOf('/'));
		}
		public synchronized void refreshException(String url){
			iExcecao++;
			lastException = new HelperDate();
//			this.URL_PONTO_ELETRONICO
		}
		public synchronized boolean ocorreuExceacoRecentemente(String url){
			if(lastException == null){
				return false;
			} else if(iExcecao < MAXIMO_DE_EXCECOES_SEGUIDAS_PARA_ENTRAR_EM_MODO_OCIOSO){
				return false;
			}

			HelperDate now = new HelperDate();
			long totalMin = lastException.getAbsDiferencaEmMinutos(new Date());
			//depois de se passar o tempo de "castigo", liberamos o canal de comunicacao HTTP
			if(totalMin > OmegaConfiguration.TEMPO_MINIMO_PARA_CHAMADA_HTTP_COM_ERRO_CONSECUTIVA_MIN ){
				lastException = null;
				iExcecao = 0;
				return false;
			} else return true;
//			if(compareDomain(URL_PONTO_ELETRONICO)){
//				return true;
//			} else return false;
		}
	}

	static ControlWaitException controlIOException = new ControlWaitException();
	private static synchronized ContainerHttpPost procedureExecuteHttpPostData(
			Context pContext, 
			String purl, 
			String pvetorKey[],
			String pvetorContent[], 
			ConfiguracaoHttpPost configuracaoHttpPost) {
		// if(! HelperHttp.isConnected(pContext)) return null;
		HttpPost httppost = null;
		if(controlIOException.ocorreuExceacoRecentemente(purl)){
			return null;
		}
		try {
			HttpParams params = new BasicHttpParams();
	
			// this how tiny it might seems, is actually absoluty needed.
			// otherwise http client lags for 2sec.
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.ISO_8859_1);
			HttpProtocolParams.setHttpElementCharset(params, HTTP.ISO_8859_1);

			if(configuracaoHttpPost != null)
				HttpConnectionParams.setConnectionTimeout(params, configuracaoHttpPost.pMilisecondTimeOut);
			else
				HttpConnectionParams.setConnectionTimeout(params, OmegaConfiguration.HTTP_TIMEOUT_MILISEGUNDOS);
	
			// Crria um HttpClient
			DefaultHttpClient httpclient = new DefaultHttpClient(params);
			
			if(configuracaoHttpPost != null && 
					configuracaoHttpPost.cookieToSend != null){
				
				httpclient.setCookieStore(configuracaoHttpPost.cookieToSend);
			}

			
			// Cria um HttpPost com a URL para realizacao do Post
			httppost = new HttpPost(purl + "&XDEBUG_SESSION_START=PHPSTORM");
			
			if (pvetorKey != null 
					&& pvetorContent != null 
					&& pvetorKey.length > 0 ) {
				List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
				for (int i = 0; i < pvetorKey.length; i++) {
					String key = pvetorKey[i];
					String content = pvetorContent[i];
					if (content != null && content.length() > 0)
						nameValuePairs.add(new BasicNameValuePair(key, content));
				}

				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.ISO_8859_1));
			}
			// Executa
			HttpResponse httpResponse = httpclient.execute(httppost);
			
			
//			final int statusCode = httpResponse.getStatusLine().getStatusCode();
			final HttpEntity entity = httpResponse.getEntity();
			 
			
			if (entity != null) {
				InputStream inputStream = null;
	
				inputStream = entity.getContent();
				
				return new ContainerHttpPost(
						httppost, 
						inputStream, 
						configuracaoHttpPost == null || configuracaoHttpPost.setCookie 
						? httpResponse.getHeaders("Set-Cookie") : null);
			} else {
				httppost.abort();
				httppost = null;
			}
		
	
		} catch (ClientProtocolException e) {
			if(httppost != null)
				httppost.abort();
			SingletonLog.insereErro(e, TIPO.PAGINA);
			controlIOException.refreshException(purl);
		}
		catch (IOException e) {
			if(httppost != null)
				httppost.abort();
			SingletonLog.insereErro(e, TIPO.PAGINA);
			controlIOException.refreshException(purl);
			
		} catch (Exception ex) {
			if(httppost != null)
				httppost.abort();
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
	
	

		return null;
	}

	// Fast Implementation
	public static String inputStreamToString(InputStream is) {
		String line = "";
		StringBuilder total = new StringBuilder();

		BufferedReader rd = null;
		try {
			// Wrap a BufferedReader around the InputStream
			//rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"));
			rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"));

			// Read response until the end
			while ((line = rd.readLine()) != null) {
				total.append(line);
			}

		} catch (IOException e) {
			SingletonLog.insereWarning(e, TIPO.UTIL_ANDROID);
		} finally {
			if (rd != null)
				try {
					rd.close();
				} catch (IOException e) {
					SingletonLog.insereWarning(e, TIPO.UTIL_ANDROID);
				}
		}

		// Return full string
		return total.toString();
	}

	public static String getConteudoStringPost(
			Context pContext, 
			String p_url, 
			String[] vetorKey,
			String[] vetorContent, 
			ConfiguracaoHttpPost configuracaoHttpPost) {
		
		String conteudo = null;
		Exception ex1 = null;
		ContainerHttpPost c = null;
		try {
			c = HelperHttpPost.executeHttpPostData(
					pContext, 
					p_url, 
					vetorKey, 
					vetorContent,
					configuracaoHttpPost);
			if (c == null)
				conteudo= null;
			else conteudo = HelperHttpPost.inputStreamToString(c.getInputStream());
			
		} catch (Exception ex) {
			SingletonLog.insereWarning(ex, TIPO.UTIL_ANDROID);
			ex1 = ex;

		} finally{
			if(c != null)
				c.closeConnection();
		}
		if (OmegaConfiguration.DEBUGGING_HTTP) {
			try {
				String get = "";
				if (vetorKey != null)
					for (int i = 0; i < vetorKey.length; i++) {
						if (i == 0 && !p_url.contains("?"))
							get += "?";
						else
							get += "&";

						get += vetorKey[i] + "="
								+ (vetorContent[i] == null ? "null" : URLEncoder.encode(vetorContent[i]));
					}
				OmegaLog log  = null;
				try{
					log = new OmegaLog(
							ARQUIVO_LOG(), 
							OmegaFileConfiguration.TIPO.LOG,
							OmegaConfiguration.DEBUGGING_HTTP,
							pContext);
					String urlFinal = p_url + get;
					if (ex1 != null)
						log.escreveLog(urlFinal + " -" + HelperExcecao.getDescricao(ex1));
					else{
						int idlog = Log.VERBOSE;
						if(conteudo == null) {
							idlog = Log.ERROR;
						} else {
							if(conteudo.contains("\"mCodRetorno\":1")){
								
								idlog = Log.ERROR;
							} else if(!conteudo.contains("\"mCodRetorno\":0")
									&& !conteudo.contains("\"mCodRetorno\":11")){
								idlog = Log.WARN;
							}	
						}
						
						log.escreveLog(urlFinal + " -" + conteudo, new Date(), idlog);
					}
				} finally{
					if(log != null) log.close();
				}
				

			} catch (Exception ex) {
				SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
			}
		}
		return conteudo;
	}
	
	
	

	public static void logHttp(
			Context c,
			String p_url, String[] vetorKey, String[] vetorContent, Exception ex1, String conteudo, String[] constantes, String valoresConstantes[]){
		if (OmegaConfiguration.DEBUGGING_HTTP) {
			try {
				String get = "";
				if (vetorKey != null){
					for (int i = 0; i < vetorKey.length; i++) {
						if (i == 0 && !p_url.contains("?"))
							get += "?";
						else
							get += "&";

						get += vetorKey[i] + "="
								+ (vetorContent[i] == null ? "null" : URLEncoder.encode(vetorContent[i]));
					}
				}
				if (constantes != null){
					for (int i = 0; i < constantes.length; i++) {
						if (i == 0 && !p_url.contains("?"))
							get += "?";
						else
							get += "&";

						get += constantes[i] + "="
								+ (valoresConstantes[i] == null ? "null" : URLEncoder.encode(valoresConstantes[i]));
					}
				}
				
				OmegaLog log  = null;
				try{
					log = new OmegaLog(
							HelperHttpPost.ARQUIVO_LOG(), 
							OmegaFileConfiguration.TIPO.LOG,
							OmegaConfiguration.DEBUGGING_HTTP,
							c);
					String urlFinal = p_url + get;
					if (ex1 != null)
						log.escreveLog(urlFinal + " -" + HelperExcecao.getDescricao(ex1));
					else{
						int idlog = Log.VERBOSE;
						if(conteudo == null) {
							idlog = Log.ERROR;
						} else {
							if(conteudo.contains("\"mCodRetorno\":1")){
								
								idlog = Log.ERROR;
							} else if(!conteudo.contains("\"mCodRetorno\":0")
									&& !conteudo.contains("\"mCodRetorno\":11")){
								idlog = Log.WARN;
							}	
						}
						
						log.escreveLog(urlFinal + " -" + conteudo, new Date(), idlog);
					}
				} finally{
					if(log != null) log.close();
				}
				

			} catch (Exception ex) {
				SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
			}
		}
	}
	private static HeaderElement factoryHeaderElement(final String nome, final String valor){
		return new HeaderElement() {
			@Override
			public String getName() {
				return nome;
			}

			@Override
			public String getValue() {
				return valor;
			}

			@Override
			public NameValuePair[] getParameters() {
				return new NameValuePair[0];
			}

			@Override
			public NameValuePair getParameterByName(String s) {
				return null;
			}

			@Override
			public int getParameterCount() {
				return 0;
			}

			@Override
			public NameValuePair getParameter(int i) {
				return null;
			}
		};
	}
	public static RetornoJson getRetornoJsonPost(
			Context pContext, 
			String p_url, 
			String[] vetorKey,
			String[] vetorContent, 
			ConfiguracaoHttpPost configuracaoHttpPost) {
		if(p_url == null) return null;
		String conteudo = null;
		Exception ex1 = null;
		ContainerHttpPost c = null;
		try {
			c = HelperHttpPost.executeHttpPostData(pContext, p_url, vetorKey, vetorContent,
					configuracaoHttpPost);
			if (c == null)
				return null;
			
				
			conteudo = HelperHttpPost.inputStreamToString(c.getInputStream());
			
		} catch (Exception ex) {
			SingletonLog.insereWarning(ex, TIPO.UTIL_ANDROID);
			ex1 = ex;

		} finally{
			if(c != null)
				c.closeConnection();
		}
		RetornoJson retornoJson = new RetornoJson();
		try {
//			String tssChave = null;
//			String texpiry = null;
//			for(int i = 0; i < c.cookieStore.length; i++){
//				Header h= c.cookieStore[i];
//
//				if(h != null){
//					HeaderElement[] elements = h.getElements();
//					for(int j = 0 ; j < elements.length; j++){
//						if(elements[j].getName().compareToIgnoreCase("SS_CHAVE")==0){
//							tssChave =elements[j].getValue();
//							texpiry  = elements[j].getParameterByName("expiry").getValue();
//							break;
//						}
//					}
//				}
//				if(tssChave!=null)
//					break;
//			}
//			if(tssChave != null){
//				final String ssChave = tssChave;
//				final String expiry = texpiry;
//				Header[] newHeader = new Header[c.cookieStore.length + 1];
//				for(int i = 0; i < c.cookieStore.length; i++){
//					newHeader[i] = c.cookieStore[i];
//				}
//				String x= "[version: 0][name: SS_CHAVE][value: "+ssChave+"][domain: "
//						+ OmegaConfiguration.URL_SINCRONIZADOR_SEM_HTTP
//						+ "][path: /][expiry: "+expiry+"]";
//				CharArrayBuffer cab =new CharArrayBuffer(x.length());
//				cab.append(x);
//				newHeader[newHeader.length - 1] = new BufferedHeader(cab);
//				//newHeader[newHeader.length - 1] =;
				retornoJson.cookieStore = c.cookieStore;
				retornoJson.json = new JSONObject(conteudo);
//			}

						
		} catch (JSONException e) {
			ex1 = e;
			Mensagem m  = new Mensagem(
					PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, 
					"Json invalido: "+ conteudo);
			
			try {
				retornoJson.json = new JSONObject(m.toJson());
				retornoJson.cookieStore = c.cookieStore;
			} catch (JSONException e1) {
				SingletonLog.insereErro(e1, TIPO.HTTP);
				return null;
			}	
		}
		if (OmegaConfiguration.DEBUGGING_HTTP) {
			try {
				String get = "";
				if (vetorKey != null)
					for (int i = 0; i < vetorKey.length; i++) {
						if (i == 0 && !p_url.contains("?"))
							get += "?";
						else
							get += "&";

						get += vetorKey[i] + "="
								+ (vetorContent[i] == null ? "null" : URLEncoder.encode(vetorContent[i]));
					}
				OmegaLog log  = null;
				try{
					log = new OmegaLog(
							ARQUIVO_LOG(), 
							OmegaFileConfiguration.TIPO.LOG,
							OmegaConfiguration.DEBUGGING_HTTP,
							pContext);
					String urlFinal = p_url + get;
					if (ex1 != null)
						log.escreveLog(urlFinal + " -" + HelperExcecao.getDescricao(ex1));
					else{
						int idlog = Log.VERBOSE;
						if(conteudo.contains("\"mCodRetorno\":1")){
							
							idlog = Log.ERROR;
						} else if(!conteudo.contains("\"mCodRetorno\":0")
								&& !conteudo.contains("\"mCodRetorno\":11")){
							idlog = Log.WARN;
						}
						log.escreveLog(urlFinal + " -" + conteudo, new Date(), idlog);
					}
				} finally{
					if(log != null) log.close();
				}
				

			} catch (Exception ex) {
				SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
			}
		}
		
		
		return retornoJson;
	}

	public static String getConteudoStringPost(
			Context pContext, 
			String p_url, 
			String[] p_vetorKey,
			String[] p_vetorContent) {
		if(p_url == null) return null;
		return getConteudoStringPost(
				pContext, 
				p_url, 
				p_vetorKey, 
				p_vetorContent, 
				null);

	}

}
