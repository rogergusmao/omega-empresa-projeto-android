package app.omegasoftware.pontoeletronico.http;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.http.HttpStatus;
import org.json.JSONObject;


import android.content.Context;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.ValidadeArquivoJson;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;
import app.omegasoftware.pontoeletronico.database.synchronize.ParseJson;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class HelperHttpPostReceiveFile {
	public static String TAG = "HelperHttpPostReceiveFile";
//	private static String ARQUIVO_LOG(){ return  "chamadas_http_"+HelperDate.getDateParaNomeDeArquivo()+".log";}
	
	static ReentrantLock reentrantLock = new ReentrantLock();
	public static Mensagem DownloadFileFromHttpPost(Context pContext,
			String pURL, String pFileNameOut) {
		return DownloadFileFromHttpPost(pContext, pURL, pFileNameOut, null,
				null);
	}

	public static Mensagem DownloadFileFromHttpPost(
			Context pContext,
			String pURL, 
			String pPathFileOut, 
			String pListKey[],
			String pListContent[]) {
		
		Boolean validade = null;
		String urlParameters = "";
		try {
			if(reentrantLock.tryLock()){
				try{
					// create a new file, specifying the path, and the filename
					// which we want to save the file as.
					File file = new File(pPathFileOut);
					if (file.isFile())
						file.delete();
					//Faz somente uma chamada http pelo android por vez
					Random r = new Random();
					if (pListContent != null){

						boolean isFirstTime = true;
						for (int i = 0; i < pListContent.length; i++) {
							String vKey = pListKey[i];
							String vContent = pListContent[i];
							if (vKey != null && vContent != null) {
								if (vKey.length() > 0 && vContent.length() > 0)
									if (isFirstTime) {
										isFirstTime = false;
										urlParameters = vKey + "="
												+ URLEncoder.encode(vContent, "UTF-8");
									} else {
										urlParameters += "&" + vKey + "="
												+ URLEncoder.encode(vContent, "UTF-8");
									}
							}
						}
					}
					String bkpParameters = urlParameters;
					for(int j = 0 ; j < OmegaConfiguration.MAX_TENTIVAS_HTTP; j++){
						if(j > 0)
							Thread.sleep(r.nextInt(5000));
						urlParameters = bkpParameters;
						if(urlParameters.length() > 0) urlParameters += "&";
						urlParameters += "j" + String.valueOf( j) + "=true";
						// set the download URL, a URL_PONTO_ELETRONICO that points to a file on the internet
						// this is the file to be downloaded
						URL url = new URL(pURL);
	
						// create the new connection
						HttpURLConnection urlConnection = (HttpURLConnection) url
								.openConnection();
						try{
							// set up some things on the connection
							urlConnection.setRequestMethod("POST");
		
							urlConnection.setRequestProperty("Content-Type",
									"application/x-www-form-urlencoded");
		
							if (urlParameters.length() > 0){
								
								urlConnection.setRequestProperty("Content-Length",
										Integer.toString(urlParameters.getBytes().length));
							}
							
							urlConnection.setRequestProperty("Content-Language", "en-US");
		
							urlConnection.setUseCaches(false);
							urlConnection.setDoInput(true);
							urlConnection.setDoOutput(true);
							 
							// Send request
							DataOutputStream wr = new DataOutputStream(
									urlConnection.getOutputStream());
							wr.writeBytes(urlParameters);
							wr.flush();
							wr.close();

							// and connect!
		
							urlConnection.connect();
							
							
							final int statusCode = urlConnection.getResponseCode();
							InputStream inputStream = statusCode == HttpStatus.SC_OK ? urlConnection.getInputStream() : urlConnection.getErrorStream() ;
							try{
								if(inputStream == null) continue;
								if(statusCode == HttpStatus.SC_OK) {
									//this will be used to write the downloaded data into the file we
									// created
									FileOutputStream fileOutput = new FileOutputStream(file);
									try{
										int bufferLength = 0; // used to store a temporary size of the
																// buffer
										byte[] buffer = new byte[1024];
						
										while ((bufferLength = inputStream.read(buffer)) > 0) {
											// add the data in the buffer to the file in the file output
											// stream (the file on the sd card
											fileOutput.write(buffer, 0, bufferLength);
											fileOutput.flush();
											// add up the size so we know how much is downloaded
											if(validade == null)
												validade =true; 
										}			
									}finally{
										//close the output stream when done
										fileOutput.close();
									}
									if(validade)
										return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
									else continue;
								} else{		
									String strJson = HelperHttpPost.ReadInputStream(inputStream);
									if(strJson == null || strJson.length() == 0 ) continue;
									JSONObject json = new JSONObject(strJson);
								
									SingletonLog.insereErro(TAG, "procedureExecuteHttpsPostData",
											"[EAOKDS] Erro call http: " + statusCode + ". URL: " + pURL + " Status code: " + statusCode + ". Parameters "+urlParameters+". Json: " + strJson, TIPO.HTTP);
									
									Mensagem msgRet = new Mensagem(json.getInt("mCodRetorno"), json.isNull("mMensagem") ? null : json.getString("mMensagem"));
									return msgRet;
								} 
							}	finally{
								if(inputStream != null)inputStream.close();		
							}
						}finally{
							 urlConnection.disconnect();
						}
					}
					
				}finally{
					reentrantLock.unlock();
				}
			}
			
			return Mensagem.factoryServidorSobrecarregado(pContext);
			// catch some possible errors...
		} catch (Exception e) {
			
			SingletonLog.insereErro(e, TIPO.UTIL_ANDROID);
			//em caso de excecao retorna nulo
			
		}
		
		return Mensagem.factoryMsgSemInternet(pContext);
	}

//	public static Boolean DownloadFileFromUrl(String URL, File file) { 
//							
//		boolean vValidade = true;
//		
//		try {
//			
//			URL URL_PONTO_ELETRONICO = new URL(URL);
//			URLConnection ucon = URL_PONTO_ELETRONICO.openConnection();
//
//			/*
//			 * Define InputStreams to read from the URLConnection.
//			 */
//			InputStream is = ucon.getInputStream();
//			BufferedInputStream bis = new BufferedInputStream(is);
//
//			/*
//			 * Read bytes to the Buffer until there is nothing more to read(-1).
//			 */
//			ByteArrayBuffer baf = new ByteArrayBuffer(50);
//			int current = 0;
//			while ((current = bis.read()) != -1) {
//				baf.append((byte) current);
//			}
//
//			/* Convert the Bytes read to a String. */
//			FileOutputStream fos = new FileOutputStream(file);
//			fos.write(baf.toByteArray());
//			fos.close();
//			
//
//		}
//		catch (IOException e) {
//			
//			SingletonLog.insereErro(e, TIPO.PAGINA);
//			vValidade = false;
//			
//		}
//		
//		return vValidade;
//
//	}
	
}
