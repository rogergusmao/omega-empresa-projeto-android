package app.omegasoftware.pontoeletronico.http;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.http.HttpResponse;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;

public class HelperHttpResponse {
	
	public static String readResponseContent(HttpResponse p_response) throws IllegalStateException, IOException
	{
		Reader reader = null;
		try {
			
			reader = new InputStreamReader(p_response.getEntity().getContent());
			StringBuffer sb = new StringBuffer();
			{
				int read;
				char[] cbuf = new char[1024];
				while ((read = reader.read(cbuf)) != -1)
					sb.append(cbuf, 0, read);
			}
			return sb.toString();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					SingletonLog.insereErro(e, TIPO.BIBLIOTECA_NUVEM);
				}
			}
		}
	}
}
