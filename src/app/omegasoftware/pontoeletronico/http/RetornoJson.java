package app.omegasoftware.pontoeletronico.http;


import org.apache.http.Header;
import org.json.JSONObject;

import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;

public class RetornoJson{
	
	public JSONObject json ;
	public Header[] cookieStore;
	
	public String getCookie(String key){
		Header[] cookies = cookieStore;
		if(cookies == null || cookies.length == 0 ) return null;
		for(int i = 0 ; i < cookies.length; i++){
			Header c= cookies[i];
			String cValue = c.getValue().toString();
			int indexIgual = cValue.indexOf("=");
			String idCookie = cValue.substring(0, indexIgual);
			if( idCookie.equalsIgnoreCase(key)){
				int indexPontoVirgula =  cValue.indexOf(";",indexIgual);
				return cValue.substring(indexIgual + 1, indexPontoVirgula );
			} 
		}
		return null;
	}
	
}