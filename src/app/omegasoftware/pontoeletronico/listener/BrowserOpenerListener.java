package app.omegasoftware.pontoeletronico.listener;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.MCrypt;

public class BrowserOpenerListener implements OnClickListener {

	String url = "";

	public BrowserOpenerListener(String p_url) {
		this.url = p_url;
	}

	public void onClick(View v) {
		actionBrowser(v.getContext(), this.url);

	}

	public static void actionBrowser(Context c, String url) {
		try {
			Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse(url));
			viewIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			c.startActivity(viewIntent);
		} catch (Exception ex) {
			SingletonLog.factoryToast(c, ex, SingletonLog.TIPO.PAGINA);
		}

	}

	public static void actionrBrowserCript(Context c, String p_url, String parametros[], String valores[],
			String urlSeNaoConseguirCriptogar) {
		try {
			String url = null;
			try {
				url = MCrypt.encryptUrl(c, p_url, parametros, valores);

				if (url == null)
					url = urlSeNaoConseguirCriptogar;

			} catch (Exception e) {
				SingletonLog.insereErro(e, TIPO.UTIL_ANDROID);
				url = urlSeNaoConseguirCriptogar;
			}
			Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse(url));
			viewIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			c.startActivity(viewIntent);
		} catch (Exception ex) {
			SingletonLog.factoryToast(c, ex, SingletonLog.TIPO.PAGINA);
		}
	}

}
