package app.omegasoftware.pontoeletronico.listener;

import android.app.Activity;

import android.view.View;
import android.view.View.OnClickListener;

public class CloseButtonActivityClickListenerTablet implements OnClickListener {

	
	Activity activity ;
	
	public CloseButtonActivityClickListenerTablet(Activity p_activity )
	{
		
		activity = p_activity;
		
	}
	
	public void onClick(View v) {
	
		activity.finish();
		
	}
	
	
	
}
