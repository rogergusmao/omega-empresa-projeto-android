package app.omegasoftware.pontoeletronico.listener;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.MCrypt;

public class CriptBrowserOpenerListener implements OnClickListener {

	String url = "";
	String strGet = null;
	
	public CriptBrowserOpenerListener(
			Context c,
			String p_url, 
			String parametros[], 
			String valores[],
			String urlSeNaoConseguirCriptogar)  
	{	
		try {
			url = MCrypt.encryptUrl(c, p_url, parametros, valores);	
			
			if(url == null)
				url = urlSeNaoConseguirCriptogar;
			
		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.UTIL_ANDROID);
			url = urlSeNaoConseguirCriptogar;
		}
		
	}
	
	public void onClick(View v) {
		try{
			Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse(this.url));
			viewIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			v.getContext().startActivity(viewIntent);	
		}catch(Exception ex){
			SingletonLog.factoryToast(v.getContext(), ex, SingletonLog.TIPO.UTIL_ANDROID);
		}
		  
		
	}
	
}
