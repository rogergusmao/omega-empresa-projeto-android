package app.omegasoftware.pontoeletronico.listener;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;


public class DateClickListener implements OnClickListener
{		
	Integer idListContainerDate;
	Activity activity;
	FactoryDateClickListener factoryDateClickListener;
	public DateClickListener(
			Activity pActivity, 
			Integer pIdListContainerDate, 
			FactoryDateClickListener pFactoryDateClickListener){
		idListContainerDate = pIdListContainerDate;
		factoryDateClickListener = pFactoryDateClickListener;
		activity = pActivity;
	}
	public void onClick(View v) {
		factoryDateClickListener.setLastContainerDate(idListContainerDate);
    	activity.showDialog(FactoryDateClickListener.DATE_DIALOG_ID);
    }		
	
	public void setDateOfView(Context c, int year, int month, int day){
		factoryDateClickListener.setLastContainerDate(idListContainerDate);
		factoryDateClickListener.setDateOfView(c, year, month, day);
	}
	
}
