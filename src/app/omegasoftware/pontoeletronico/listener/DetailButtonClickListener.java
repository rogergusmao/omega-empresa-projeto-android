package app.omegasoftware.pontoeletronico.listener;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import app.omegasoftware.pontoeletronico.common.container.ContainerActionDetail;


public class DetailButtonClickListener implements OnClickListener {
	
	private String IdCustomItemList;
	short modeSearch;
	Activity a;
	String email;
	public DetailButtonClickListener(Activity a, String pIdCustomItemList, short p_modeSearch){
		this(a, pIdCustomItemList, p_modeSearch, null);
	}
	public DetailButtonClickListener(Activity a, String pIdCustomItemList, short p_modeSearch, String email)
	{
		this.a = a;
		this.IdCustomItemList = pIdCustomItemList;
		this.modeSearch = p_modeSearch;
		this.email=email;
	}

	public void onClick(View pView) {
		ContainerActionDetail.actionDetail(a, modeSearch, IdCustomItemList, email);	  
								
	}
}
