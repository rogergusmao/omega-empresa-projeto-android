package app.omegasoftware.pontoeletronico.listener;

import android.app.Activity;
import android.view.View;
import android.view.View.OnLongClickListener;
import app.omegasoftware.pontoeletronico.common.EditOrRemoveDialog;
import app.omegasoftware.pontoeletronico.common.ItemList.CustomItemList;

public class EditCustomOnLongClickListener implements OnLongClickListener{
	
		View parentView;
		String id;
		Class<?> classFormEdit;
		Class<?> classFormActionEdit;
		Activity activity;
		String nomeTabela;
		boolean isEdit; 
		boolean isRemove;
		CustomItemList item;
		public EditCustomOnLongClickListener(
				Activity pActivity, 
				String pNomeTabela,
				View pParentView, 
				String pId, 
				Class<?> pClassFormEdit,
				Class<?> pClassFormActionEdit,
				boolean pIsEdit, 
				boolean pIsRemove,
				CustomItemList item){
			nomeTabela = pNomeTabela;
			parentView = pParentView;
			id = pId;
			classFormEdit = pClassFormEdit;
			classFormActionEdit = pClassFormActionEdit;
			activity = pActivity;
			isEdit = pIsEdit;
			isRemove = pIsRemove;
			this.item = item;
		}
		
		
		public boolean onLongClick(View arg0) {
			
			EditOrRemoveDialog dialog = new EditOrRemoveDialog(
					activity, 
					id, 
					nomeTabela, 
					classFormActionEdit, 
					isRemove, 
					isEdit, 
					item);
			dialog.showAlertDialog();
//			
//			Intent intent;
//			
//			intent = new Intent(activity, classFormEdit);
//			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, id);
//			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_TABELA, nomeTabela);
//			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_CLASS, classFormActionEdit);
//			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_REMOVE_PERMITED, isRemove);
//			intent.putExtra(OmegaConfiguration.SEARCH_FIELD_IS_EDIT_PERMITED, isEdit);
//			
//			
//			activity.startActivityForResult(intent, OmegaConfiguration.ACTIVITY_FORM_EDIT);
//			

			return true;
		}
		

}
