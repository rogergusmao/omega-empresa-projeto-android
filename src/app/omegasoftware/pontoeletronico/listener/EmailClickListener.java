package app.omegasoftware.pontoeletronico.listener;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class EmailClickListener implements OnClickListener {

private Integer buttonId = null;
	
	public EmailClickListener(int pButtonId){
		buttonId = pButtonId;
	}
	public void onClick(View v) {
		
	
		TextView view = (TextView)v.findViewById(buttonId);
		if(view == null)
		{
			view = (TextView) v;
		}
		
		String emailAddress = (String) view.getText();
		
		Intent intent = new Intent(android.content.Intent.ACTION_SEND);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ emailAddress });
		intent.setType("text/plain");
		v.getContext().startActivity(intent);
		
	}
	
	
}
