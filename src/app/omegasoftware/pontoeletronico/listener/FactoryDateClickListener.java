/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package app.omegasoftware.pontoeletronico.listener;



import java.util.ArrayList;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import app.omegasoftware.pontoeletronico.R;
import android.widget.Button;
import android.widget.DatePicker;
import app.omegasoftware.pontoeletronico.date.ContainerClickListenerDate;
import app.omegasoftware.pontoeletronico.date.ContainerClickListenerDate.OnChangeDate;

/**
 * Basic example of using date and time widgets, including
 * {@link android.app.TimePickerDialog} and {@link android.widget.DatePicker}.
 *
 * Also provides a good example of using {@link Activity#onCreateDialog},
 * {@link Activity#onPrepareDialog} and {@link Activity#showDialog} to have the
 * activity automatically save and restore the state of the dialogs.
 */
public class FactoryDateClickListener {


    Activity activity;
    ArrayList<ContainerClickListenerDate> listContainerDate = new ArrayList<ContainerClickListenerDate>();
    ArrayList<OnChangeDate> listOnChangeDate = new ArrayList<OnChangeDate>();
    ContainerClickListenerDate lastContainerDate = null;
    OnChangeDate lastOnChangeDate= null;
    static public final int DATE_DIALOG_ID = 109999;
    
    public static boolean isDateButtonSet(Context pContext, Button pButtonDate){
    	String vStrSelecionarData = pContext.getResources().getString(R.string.form_selecionar_data);
    	String vStrButtonDate = pButtonDate.getText().toString();
    	if(vStrButtonDate.equals("")) return false;
    	if(vStrButtonDate.equals(vStrSelecionarData)) return false;
    	else return true;
    }
    
    public FactoryDateClickListener(Activity pActivity){
    	activity = pActivity;    
    
        
    }
    public ContainerClickListenerDate setDateClickListener(Button pButton){
    	return setDateClickListener(pButton, null);
    }
    public ContainerClickListenerDate setDateClickListener(Button pButton, OnChangeDate onChangeDate){
    	
    	DateClickListener vListener = new DateClickListener(
    			activity, 
    			listContainerDate.size(), 
    			this);
    	ContainerClickListenerDate vContainer = new ContainerClickListenerDate(pButton, vListener);
    	listContainerDate.add(vContainer);
    	listOnChangeDate.add(onChangeDate);
    	pButton.setOnClickListener(vListener);
    	return vContainer;
    }
    
    
    protected void setLastContainerDate(int pIndexContainerDate){
    	lastContainerDate = listContainerDate.get(pIndexContainerDate);
    	lastOnChangeDate = listOnChangeDate.get(pIndexContainerDate);
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
    
            case DATE_DIALOG_ID:
            	 
                return new DatePickerDialog(activity,
                            mDateSetListener,
                            lastContainerDate.getAno(), 
                            lastContainerDate.getMes(), 
                            lastContainerDate.getDia());
        }
        return null;
    }

    
    public void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
           
            case DATE_DIALOG_ID:
            	
                ((DatePickerDialog) dialog).updateDate(
                		lastContainerDate.getAno(), 
                		lastContainerDate.getMes(), 
                		lastContainerDate.getDia());
                break;
        }
    }    

    public void setDateOfView(Context c, int year, int month, int day){
    	lastContainerDate.setAno(year);
    	lastContainerDate.setMes(month);
    	lastContainerDate.setDia(day);
    	lastContainerDate.setTextView(c);
    }

   
   
    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear,
                        int dayOfMonth) {
                	setDateOfView(view.getContext(), year, monthOfYear, dayOfMonth);
                    if(lastOnChangeDate != null){
                    	lastOnChangeDate.onChange(view, year, monthOfYear, dayOfMonth);
                    }
                }
            };
}
