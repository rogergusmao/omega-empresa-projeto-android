package app.omegasoftware.pontoeletronico.listener;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class GestureOnTouchListener implements OnTouchListener{
	GestureDetector gestorDetector;
	public GestureOnTouchListener(GestureDetector pGestorDetector){
		gestorDetector = pGestorDetector;
		
	}
	
	public boolean onTouch(View v, MotionEvent event) {
		
		gestorDetector.onTouchEvent(event);
		return true;
	}
	

}
