package app.omegasoftware.pontoeletronico.listener;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;

public class HourClickListener implements OnClickListener
{


	Integer idListContainerHour;
	Activity activity;
	
	FactoryHourClickListener factory;
	public HourClickListener(
			Activity pActivity, 
			FactoryHourClickListener pFactory,
			Integer pIdListContainerHour){
		idListContainerHour = pIdListContainerHour;
		activity = pActivity;
		factory = pFactory;

	}

	public void onClick(View v) {
		factory.setLastContainerHour(idListContainerHour);
		activity.showDialog(FactoryHourClickListener.TIME_DIALOG_ID);
	}	
	
	public void setTimeOfView(int hour, int minute){
		factory.setLastContainerHour(idListContainerHour);
		factory.setDateOfView(hour, minute);
	}
}


