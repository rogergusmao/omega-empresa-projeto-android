package app.omegasoftware.pontoeletronico.listener;

import java.net.URLEncoder;
import java.util.ArrayList;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.ItemList.AppointmentPlaceItemList;

public class MapButtonClickListener implements OnClickListener {

	long addressId;
	ArrayList<String> arrayListEndereco = new ArrayList<String>();
	public MapButtonClickListener(long p_addressId, AppointmentPlaceItemList appointmentPlace)
	{
		
		this.addressId = p_addressId;
		if(appointmentPlace != null){
			String addr =appointmentPlace.getStrAddress() ;
			if(addr != null){
				arrayListEndereco.add(addr);	
			}
		}
		
	}
	
	public MapButtonClickListener(String p_adress)
	{	
		
		if(p_adress != null){
			arrayListEndereco.add(p_adress);
		}
	}
	
	public MapButtonClickListener(long p_addressId)
	{
		
		this.addressId = p_addressId;
			
	}
	
	public MapButtonClickListener(long p_addressId, ArrayList<String> p_arrayListEndereco)
	{
		
		this.addressId = p_addressId;
		if(p_arrayListEndereco != null){	
			arrayListEndereco = p_arrayListEndereco;
		}
		
	}

	public void onClick(View v) {
		
		try{		
			String url = "http://maps.google.com/maps?q=" + URLEncoder.encode( arrayListEndereco.get(0));
			
			Intent intent = new Intent(android.content.Intent.ACTION_VIEW,  Uri.parse(url));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			v.getContext().startActivity(intent);
		} catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
//		try{
		//Intent intent = new Intent(v.getContext(), ShowAddressOnMapActivityMobile.class); 
//		
//		intent.putExtra(OmegaConfiguration.MAP_ADDRESS, arrayListEndereco);
//		
//		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//		v.getContext().startActivity(intent);
//		} catch(Exception ex){
//			//Log.e("onClick", ex.toString());
//		}
		
		
	}
	
	
	
}
