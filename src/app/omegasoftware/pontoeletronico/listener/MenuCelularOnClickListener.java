package app.omegasoftware.pontoeletronico.listener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.MenuTecladoTelefoneActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class MenuCelularOnClickListener implements OnClickListener{
	Button button;
	int origemChamado;
	Activity activity;
	Handler telefoneHandler;
	MenuTelefoneOnClickListener celularSMSOnClickListener;
	public MenuCelularOnClickListener(Activity pActivity, Button pButton, int pOrigemChamado, MenuTelefoneOnClickListener pCelularSMSOnClickListener){
		origemChamado = pOrigemChamado;
		button = pButton;
		activity = pActivity;
		telefoneHandler = new ButtonTelefoneHandler();
		celularSMSOnClickListener = pCelularSMSOnClickListener;
	}
	public boolean isNumeroSet(){
		String vButtonText = button.getText().toString();
		Integer telefone = HelperInteger.parserInteger( vButtonText );
		if(vButtonText != "" &&  telefone != null) return true;
		else return false;
	}

	public void clear(){
		
		Message vMessage = new Message();
		vMessage.what = ButtonTelefoneHandler.CLEAR_TELEFONE;
		telefoneHandler.sendMessage(vMessage);
	}
	
	public Button getButton(){
		return button;
	}
	public int getOrigemChamado(){
		return origemChamado;
	}
	public Handler getHandler(){
		return telefoneHandler;
	}

	public void updateTelefone(String pNumero, boolean pIsToForce){
		if((pNumero != null && pNumero.length() > 0 && !pNumero.startsWith("DIGITE")) || pIsToForce){
			Bundle vParameter = new Bundle();
			vParameter.putString(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE, pNumero);
			Message vMessage = new Message();
			vMessage.what = ButtonTelefoneHandler.TAG_TELEFONE;
			vMessage.setData(vParameter);
			telefoneHandler.sendMessage(vMessage);
		}
	}
	
	public void onActivityResult(Intent pIntent){
		if(pIntent != null){
			Bundle vParameter = pIntent.getExtras();
			if(vParameter != null){
				Message vMessage = new Message();
				vMessage.what = ButtonTelefoneHandler.TAG_TELEFONE;
				vMessage.setData(vParameter);
				telefoneHandler.sendMessage(vMessage);
				
				String vNumeroCelular = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE);
				
				if(vNumeroCelular.length() > 0 && celularSMSOnClickListener != null ){
					String vCelularSMS = celularSMSOnClickListener.getButton().getText().toString();
					if(vCelularSMS.length() == 0 || HelperInteger.parserInteger( vCelularSMS) == null){
						celularSMSOnClickListener.updateTelefone(vNumeroCelular);
						
					}

				} 
			}
		}
	}

	public void onClick(View arg0) {
		String vConteudo = button.getText().toString();
		Intent vIntent = new Intent(activity, MenuTecladoTelefoneActivityMobile.class);
		
		vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_TECLADO_TELEFONE_ORIGEM_CHAMADO, getOrigemChamado());
		vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE, vConteudo);


		activity.startActivityForResult(vIntent, getOrigemChamado() );
	}

	public class ButtonTelefoneHandler extends Handler{
		public static final int TAG_TELEFONE = 0;
		public static final int CLEAR_TELEFONE = 1;
		final int SIZE_TEXT = 13;
		@Override
        public void handleMessage(Message msg)
        {
			switch (msg.what) {
			
			case CLEAR_TELEFONE:
				String token = activity.getResources().getString(R.string.universal_celular);
				button.setText(token);
				button.setTextSize(SIZE_TEXT);
				break;
			case TAG_TELEFONE:
				Bundle vParameter = msg.getData();
				String vNumeroTelefone = null;
				if(vParameter != null){
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE)){
						vNumeroTelefone = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE);
						if(vNumeroTelefone != null && vNumeroTelefone.length() > 0 ){
							button.setText(vNumeroTelefone);
							button.setTextSize(20);
						}
					}
				}
				
				if(vNumeroTelefone == null || vNumeroTelefone.length() == 0){
					vNumeroTelefone = activity.getResources().getString(R.string.universal_celular);
					button.setText(vNumeroTelefone);
					button.setTextSize(SIZE_TEXT);
				}
				
				button.postInvalidate();
				break;
			

			default:
				break;
			}
			
        }
	}
}
