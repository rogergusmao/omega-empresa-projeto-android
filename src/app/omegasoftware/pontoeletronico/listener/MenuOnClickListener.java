package app.omegasoftware.pontoeletronico.listener;

import android.view.View;
import android.view.View.OnClickListener;
import app.omegasoftware.pontoeletronico.TabletActivities.FactoryMenuActivityMobile;
import app.omegasoftware.pontoeletronico.common.controler.LoaderRunnable;

public class MenuOnClickListener  implements OnClickListener {
		
		FactoryMenuActivityMobile activity; 
		Class<?> classe;
		String tag;
		public MenuOnClickListener(
				FactoryMenuActivityMobile pActivity, 
				Class<?> pclasse){
			classe = pclasse;
			activity = pActivity;
			 
		}	
		public MenuOnClickListener(
				FactoryMenuActivityMobile pActivity, 
				Class<?> pclasse,
				String pTag){
			classe = pclasse;
			activity = pActivity;
			tag = pTag;
		}	
		
		public void onClick(View v) {
			
			//new LoaderAsyncTask(activity, classe, tag).execute();
			new LoaderRunnable(activity, classe, tag).execute();
		}
		
	
}
