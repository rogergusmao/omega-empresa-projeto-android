package app.omegasoftware.pontoeletronico.listener;

import android.app.Activity;
import android.content.Intent;
import app.omegasoftware.pontoeletronico.R;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import app.omegasoftware.pontoeletronico.TabletActivities.MenuTecladoTelefoneActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class MenuTelefoneOnClickListener implements OnClickListener{
	Button button;
	int origemChamado;
	Activity activity;
	Handler telefoneHandler;
	Integer idString;
	public MenuTelefoneOnClickListener(Activity pActivity, Button pButton, int pOrigemChamado, Integer idString){
		origemChamado = pOrigemChamado;
		button = pButton;
		activity = pActivity;
		telefoneHandler = new ButtonTelefoneHandler();
		this.idString = idString;
	}
	
	public MenuTelefoneOnClickListener(Activity pActivity, Button pButton, int pOrigemChamado){
		origemChamado = pOrigemChamado;
		button = pButton;
		activity = pActivity;
		telefoneHandler = new ButtonTelefoneHandler();
		this.idString = R.string.universal_telefone;
	}
	
	
	public boolean isNumeroSet(){
		String vButtonText = button.getText().toString();
		if(vButtonText != "" && HelperInteger.parserInteger(vButtonText) != null) return true;
		else return false;
	}
	public Handler getHandler(){
		return telefoneHandler;
	}
	public Button getButton(){
		return button;
	}
	public int getOrigemChamado(){
		return origemChamado;
	}

	public void updateTelefone(String pNumero){
		updateTelefone(pNumero, false);
	}
	public void updateTelefone(String pNumero, boolean pIsToForce){
		if((pNumero != null 
				&& pNumero.length() > 0 
				&& HelperInteger.parserInteger(pNumero) != null
			) 
			|| pIsToForce
		){
			Bundle vParameter = new Bundle();
			vParameter.putString(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE, pNumero);
			Message vMessage = new Message();
			vMessage.what = ButtonTelefoneHandler.TAG_TELEFONE;
			vMessage.setData(vParameter);
			telefoneHandler.sendMessage(vMessage);
		}
	}
	
	public void clear(){
		
		Message vMessage = new Message();
		vMessage.what = ButtonTelefoneHandler.CLEAR_TELEFONE;
		telefoneHandler.sendMessage(vMessage);
	}
	
	public void onActivityResult(Intent pIntent){
		if(pIntent != null){
			Bundle vParameter = pIntent.getExtras();
			if(vParameter != null){
				Message vMessage = new Message();
				vMessage.what = ButtonTelefoneHandler.TAG_TELEFONE;
				vMessage.setData(vParameter);
				telefoneHandler.sendMessage(vMessage);
			}
		}
	}

	public void onClick(View arg0) {

		String vConteudo = button.getText().toString();
		Intent vIntent = new Intent(activity, MenuTecladoTelefoneActivityMobile.class);
		
		vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_TECLADO_TELEFONE_ORIGEM_CHAMADO, getOrigemChamado());
		vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE, vConteudo);


		activity.startActivityForResult(vIntent, getOrigemChamado() );
	}

	public class ButtonTelefoneHandler extends Handler{
		public static final int TAG_TELEFONE = 0;
		public static final int CLEAR_TELEFONE = 1;
		final int TEXT_SIZE = 13;
		@Override
        public void handleMessage(Message msg)
        {
			switch (msg.what) {
			case CLEAR_TELEFONE:
				String token= activity.getResources().getString(idString );
				button.setText(token);
				button.setTextSize(TEXT_SIZE);
				break;
			
			case TAG_TELEFONE:
				Bundle vParameter = msg.getData();
				String vNumeroTelefone = null;
				if(vParameter != null){
					if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE)){
						vNumeroTelefone = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_NUMERO_TELEFONE);
						if(vNumeroTelefone != null && vNumeroTelefone.length() > 0 ){
							button.setText(vNumeroTelefone);
							button.setTextSize(20);
						}
					}
				}
				
				if(vNumeroTelefone == null || vNumeroTelefone.length() == 0){
					vNumeroTelefone = activity.getResources().getString(idString);
					button.setText(vNumeroTelefone);
					button.setTextSize(TEXT_SIZE);
				}
				
				button.postInvalidate();
				break;
			

			default:
				break;
			}
			
        }
	}
}
