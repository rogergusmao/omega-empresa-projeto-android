package app.omegasoftware.pontoeletronico.listener;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import app.omegasoftware.pontoeletronico.TabletActivities.lists.ListPontoActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOPessoaUsuario;
import app.omegasoftware.pontoeletronico.date.HelperDate;


public class MinhaPessoaEmpresaButtonClickListener implements OnClickListener {
	
	private Activity activity;
	
	
	public MinhaPessoaEmpresaButtonClickListener(
			Activity pActivity)
	{
		activity = pActivity;
	}

	public void onClick(View pView) {
		
		try{
			Intent vIntent = null; 
			
			vIntent = new Intent(
						activity.getApplicationContext(), 
						ListPontoActivityMobile.class);

			Database db = new DatabasePontoEletronico(activity);
			
			EXTDAOPessoaUsuario vObj = new EXTDAOPessoaUsuario(db);
			
			String vIdPessoa = vObj.getIdPessoaDoUsuarioLogado();
			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID_PESSOA, vIdPessoa);
			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATA_INICIAL, HelperDate.getDateAtualFormatada(activity));
			db.close();
			
			activity.startActivityForResult(vIntent, OmegaConfiguration.STATE_RASTREAR_TAREFA);
		}catch(OmegaDatabaseException e){
			SingletonLog.factoryToast(activity, e, SingletonLog.TIPO.PAGINA);
		}						
	}
}
