package app.omegasoftware.pontoeletronico.listener;


import android.content.Intent;
import android.net.Uri;
import app.omegasoftware.pontoeletronico.R;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class PhoneClickListener implements OnClickListener {

	private Integer buttonId = null;
	
	public PhoneClickListener(int pButtonId){
		buttonId = pButtonId;
	}
	
//	public PhoneClickListener(){
//		buttonId = R.id.email_or_phone_button;
//	}
	
	public void onClick(View v) {

		TextView view = null;

		
		view = (TextView) v.findViewById(buttonId);	
		
		
		String phoneNumber = (String) view.getText();

		String uri = "tel:" + phoneNumber.trim() ;
		Intent intent = new Intent(Intent.ACTION_DIAL);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setData(Uri.parse(uri));
		
		v.getContext().startActivity(intent);
		
	}
	
	
}
