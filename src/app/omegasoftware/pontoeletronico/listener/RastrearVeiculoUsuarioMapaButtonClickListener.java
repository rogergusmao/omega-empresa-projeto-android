package app.omegasoftware.pontoeletronico.listener;

import android.app.Activity;
import android.content.Intent;

import android.view.View;
import android.view.View.OnClickListener;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;

public class RastrearVeiculoUsuarioMapaButtonClickListener implements OnClickListener {

	long veiculoUsuarioId;
	Activity activity;
	
	public RastrearVeiculoUsuarioMapaButtonClickListener(
			long pVeiculoUsuarioId,
			Activity pActivity)
	{
		this.activity = pActivity;
		this.veiculoUsuarioId = pVeiculoUsuarioId;
		
	}

	public void onClick(View v) {
		Intent intent = this.activity.getIntent();
		intent.putExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, (int)this.veiculoUsuarioId);
		this.activity.setResult(OmegaConfiguration.STATE_RASTREAR_CARRO, intent);
		this.activity.finish();
	}
	
}
