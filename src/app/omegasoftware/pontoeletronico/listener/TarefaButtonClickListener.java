package app.omegasoftware.pontoeletronico.listener;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailTarefaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.TarefaItemList;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;


public class TarefaButtonClickListener implements OnClickListener {
	
	private String IdCustomItemList;
	private TarefaItemList tarefaItemList;
	private Activity activity;
	
	
	public TarefaButtonClickListener(
			Activity pActivity,
			String pIdCustomItemList, 
			TarefaItemList pTarefaItemList)
	{
		this.IdCustomItemList = pIdCustomItemList;
		this.tarefaItemList = pTarefaItemList;
		activity = pActivity;
	}

	public void onClick(View pView) {
		
		Intent vIntent = null; 
		
		vIntent = new Intent(
					activity.getApplicationContext(), 
					DetailTarefaActivityMobile.class);
		
		if(vIntent != null){
			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, IdCustomItemList);
//			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATE_TIME, this.tarefaItemList.getDate());
			DatabasePontoEletronico db=null;
			try {
				db = new DatabasePontoEletronico(activity);
			} catch (OmegaDatabaseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			String vNomeTarefa = this.tarefaItemList.getContainerItem(TarefaItemList.NOME_TAREFA).getConteudo();
			String vDataCadastro = this.tarefaItemList.getContainerItem(TarefaItemList.DATETIME_CADASTRO_TAREFA).getConteudo();
			
					
			db.close();
			
			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO_TAREFA, vNomeTarefa);
			
			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATETIME_CADASTRO, vDataCadastro);
			
			
			
			String vDataAberturaTarefa = this.tarefaItemList.getContainerItem(TarefaItemList.DATA_ABERTURA_TAREFA).getConteudo();
			if(vDataAberturaTarefa  != null)
				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATE_ABERTURA, vDataAberturaTarefa);
			
			String vDatetimeMarcadaParaInicioDaTarefa = this.tarefaItemList.getContainerItem(TarefaItemList.DATETIME_INICIO_MARCADO_DA_TAREFA).getConteudo();
			if(vDatetimeMarcadaParaInicioDaTarefa != null)
				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATETIME_INICIO_PROGRAMADO_TAREFA, vDatetimeMarcadaParaInicioDaTarefa);
			
			String vDatetimeInicioDaTarefa = this.tarefaItemList.getContainerItem(TarefaItemList.DATETIME_INICIO_TAREFA).getConteudo();
			if(vDatetimeInicioDaTarefa != null)
				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATETIME_INICIO_TAREFA, vDatetimeInicioDaTarefa);
			
			String vDatetimeFimDaTarefa = this.tarefaItemList.getContainerItem(TarefaItemList.DATETIME_FIM_TAREFA).getConteudo();
			if(vDatetimeFimDaTarefa != null)
				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATETIME_FIM_TAREFA, vDatetimeFimDaTarefa);
			
//			String vEnderecoDestino = tarefaItemList.getEnderecoDestino();
//			if(vEnderecoDestino != null)
//				vIntent.putExtra(OmegaConfiguration.FORM_FIELD_ENDERECO_DESTINO, vEnderecoDestino);
//			
//			GeoPoint vPointProprio = tarefaItemList.getGeoPointProprio();
//			if(vPointProprio != null){
//				vIntent.putExtra(OmegaConfiguration.FORM_FIELD_LATITUDE_PROPRIA, vPointProprio.getLatitudeE6());
//				vIntent.putExtra(OmegaConfiguration.FORM_FIELD_LONGITUDE_PROPRIA, vPointProprio.getLongitudeE6());
//			}
//			
//			GeoPoint vPointDestino = tarefaItemList.getGeoPointDestino();
//			if(vPointDestino != null){
//				vIntent.putExtra(OmegaConfiguration.FORM_FIELD_LATITUDE_DESTINO, vPointDestino.getLatitudeE6());
//				vIntent.putExtra(OmegaConfiguration.FORM_FIELD_LONGITUDE_DESTINO, vPointDestino.getLongitudeE6());
//			}
//			
//			GeoPoint vPointOrigem = tarefaItemList.getGeoPointOrigem();
//			if(vPointOrigem != null){
//				vIntent.putExtra(OmegaConfiguration.FORM_FIELD_LATITUDE_ORIGEM, vPointOrigem.getLatitudeE6());
//				vIntent.putExtra(OmegaConfiguration.FORM_FIELD_LONGITUDE_ORIGEM, vPointOrigem.getLongitudeE6());
//			}
			
			
			
			activity.startActivityForResult(vIntent, OmegaConfiguration.STATE_RASTREAR_TAREFA);
	
		}			  
								
	}
}
