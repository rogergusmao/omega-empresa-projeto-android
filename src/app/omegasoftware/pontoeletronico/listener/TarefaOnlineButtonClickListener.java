package app.omegasoftware.pontoeletronico.listener;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailTarefaMapActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.ItemList.TarefaOnlineItemList;

//public class TarefaOnlineButtonClickListener implements OnClickListener {
//
//	private String IdCustomItemList;
//	private TarefaOnlineItemList tarefaOnlineItemList;
//	private Activity activity;
//	private TYPE_TAREFA typeTarefa;
//	private Integer latitudeProprio;
//	private Integer longitudeProprio;
//	public TarefaOnlineButtonClickListener(
//			Activity pActivity,
//			String pIdCustomItemList,
//			TarefaOnlineItemList pTarefaOnlineItemList,
//			TYPE_TAREFA pTypeTarefa,
//			Integer pLatitudeProprio,
//			Integer pLongitudeProprio)
//	{
//		this.IdCustomItemList = pIdCustomItemList;
//		this.tarefaOnlineItemList = pTarefaOnlineItemList;
//		activity = pActivity;
//		typeTarefa = pTypeTarefa;
//		latitudeProprio = pLatitudeProprio;
//		longitudeProprio = pLongitudeProprio;
//	}
//
//	public void onClick(View pView) {
//
//		Intent vIntent = null;
//
//		vIntent = new Intent(
//					activity,
//					DetailTarefaMapActivityMobile.class);
//
//		if(vIntent != null){
//			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, IdCustomItemList);
////			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATE_TIME, this.TarefaOnlineItemList.getDate());
//
//			String vNomeTarefa = this.tarefaOnlineItemList.getContainerItem(TarefaOnlineItemList.NOME_TAREFA).getConteudo();
//			String vDataCadastro = this.tarefaOnlineItemList.getContainerItem(TarefaOnlineItemList.DATETIME_CADASTRO_TAREFA).getConteudo();
//			String vEnderecoOrigem = this.tarefaOnlineItemList.getContainerItem(TarefaOnlineItemList.ORIGEM_ENDERECO).getConteudo();
//
//
//
//			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_TITULO_TAREFA, vNomeTarefa);
//
//			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATETIME_CADASTRO, vDataCadastro);
//
//			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_ORIGEM, vEnderecoOrigem);
//
//			String vDataAberturaTarefa = this.tarefaOnlineItemList.getContainerItem(TarefaOnlineItemList.DATA_ABERTURA_TAREFA).getConteudo();
//			if(vDataAberturaTarefa  != null)
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATE_ABERTURA, vDataAberturaTarefa);
//
//			String vDatetimeMarcadaParaInicioDaTarefa = this.tarefaOnlineItemList.getContainerItem(TarefaOnlineItemList.DATETIME_INICIO_MARCADO_DA_TAREFA).getConteudo();
//			if(vDatetimeMarcadaParaInicioDaTarefa != null)
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATETIME_INICIO_PROGRAMADO_TAREFA, vDatetimeMarcadaParaInicioDaTarefa);
//
//			String vDatetimeInicioDaTarefa = this.tarefaOnlineItemList.getContainerItem(TarefaOnlineItemList.DATETIME_INICIO_TAREFA).getConteudo();
//			if(vDatetimeInicioDaTarefa != null)
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATETIME_INICIO_TAREFA, vDatetimeInicioDaTarefa);
//
//			String vDatetimeFimDaTarefa = this.tarefaOnlineItemList.getContainerItem(TarefaOnlineItemList.DATETIME_FIM_TAREFA).getConteudo();
//			if(vDatetimeFimDaTarefa != null)
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATETIME_FIM_TAREFA, vDatetimeFimDaTarefa);
//
//			String vEnderecoDestino = tarefaOnlineItemList.getContainerItem(TarefaOnlineItemList.DESTINO_ENDERECO).getConteudo();
//			if(vEnderecoDestino != null)
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ENDERECO_DESTINO, vEnderecoDestino);
//
////			GeoPoint vPointProprio = TarefaOnlineItemList.getGeoPointProprio();
//			if(latitudeProprio != null)
//				vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_LATITUDE_PROPRIA, latitudeProprio);
//
//			if(longitudeProprio != null)
//			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_LONGITUDE_PROPRIA, longitudeProprio);
////
////			GeoPoint vPointDestino = TarefaOnlineItemList.getGeoPointDestino();
////			if(vPointDestino != null){
////				vIntent.putExtra(OmegaConfiguration.FORM_FIELD_LATITUDE_DESTINO, vPointDestino.getLatitudeE6());
////				vIntent.putExtra(OmegaConfiguration.FORM_FIELD_LONGITUDE_DESTINO, vPointDestino.getLongitudeE6());
////			}
////
////			GeoPoint vPointOrigem = TarefaOnlineItemList.getGeoPointOrigem();
////			if(vPointOrigem != null){
////				vIntent.putExtra(OmegaConfiguration.FORM_FIELD_LATITUDE_ORIGEM, vPointOrigem.getLatitudeE6());
////				vIntent.putExtra(OmegaConfiguration.FORM_FIELD_LONGITUDE_ORIGEM, vPointOrigem.getLongitudeE6());
////			}
//
//			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_TYPE_TAREFA, typeTarefa);
//
//			vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, this.tarefaOnlineItemList.getVeiculoUsuairo());
//
//			activity.startActivityForResult(vIntent, OmegaConfiguration.STATE_RASTREAR_TAREFA);
//
//		}
//
//	}
//}
