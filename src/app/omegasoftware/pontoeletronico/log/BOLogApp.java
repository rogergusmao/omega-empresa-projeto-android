package app.omegasoftware.pontoeletronico.log;

import java.util.Date;

import org.json.JSONException;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import app.omegasoftware.pontoeletronico.TabletActivities.FactoryMenuActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaAuthenticatorActivity.ContainerAuthentication;
import app.omegasoftware.pontoeletronico.common.controler.ControlerProgressDialog;
import app.omegasoftware.pontoeletronico.common.thread.HelperThread;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.file.CleanDir;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_DATETIME;
import app.omegasoftware.pontoeletronico.webservice.ServicosPontoEletronico;
import app.omegasoftware.pontoeletronico.webservice.ServicosPontoEletronicoAnonimo;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class BOLogApp {

	
	public static InterfaceMensagem enviaLogImediato(Context c) {
		try {
			ServicosPontoEletronicoAnonimo s = new ServicosPontoEletronicoAnonimo(c);
			InterfaceMensagem msg = s.uploadLogErro(true);
			return msg;
		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.UTIL_ANDROID);
			return new Mensagem(e);
		}
	}
	Date ultimaTentativaFracassada = null ;
	final int TEMPO_MINIMO_MINUTOS_PROXIMA_TENTATIVA = 10;
	HelperDate hdUltimaAtualizacao =null;
	
	//Em caso de falha, su executa a cada 10 minutos. E em caso de sucesso, su no outro dia ele enviaru novamente
	public InterfaceMensagem enviaLogPeriodicamente(Context c, int tempoEspera) {
		try {
			if(ultimaTentativaFracassada != null){
				HelperDate hd = new HelperDate();
				if(hd.getAbsDiferencaEmMinutos(ultimaTentativaFracassada) < TEMPO_MINIMO_MINUTOS_PROXIMA_TENTATIVA){
					return null;
				}
			}
			if(hdUltimaAtualizacao==null)
			hdUltimaAtualizacao = PontoEletronicoSharedPreference.getValorDateTime(
					c,
					TIPO_DATETIME.ULTIMO_ENVIO_LOG);
			Date now = new Date();
			if (hdUltimaAtualizacao == null || hdUltimaAtualizacao.getAbsDiferencaEmDias(now) < 1 ) {
				HelperThread.sleep(tempoEspera);
				ServicosPontoEletronicoAnonimo s = new ServicosPontoEletronicoAnonimo(c);

				InterfaceMensagem msg = s.uploadLogErro(false);
				if (msg != null 
					&& msg.mCodRetorno!=PROTOCOLO_SISTEMA.TIPO.SOMENTE_VIA_WIFI.getId()
					&& msg.mCodRetorno!=PROTOCOLO_SISTEMA.TIPO.SEM_CONEXAO_A_INTERNET.getId()
					) {
					hdUltimaAtualizacao=new HelperDate();
					PontoEletronicoSharedPreference.saveValor(c, TIPO_DATETIME.ULTIMO_ENVIO_LOG, hdUltimaAtualizacao);
				} else {
					ultimaTentativaFracassada = new Date();
				}
				return msg;
			} else {
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.AGUARDANDO, "Ainda nuo deu o periodo de um dia");
			}
		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.UTIL_ANDROID);
			return new Mensagem(e);
		}

	}

}
