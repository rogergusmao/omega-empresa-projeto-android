


package app.omegasoftware.pontoeletronico.mapa;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.gpsnovo.GeoPoint;

public class HelperMapa  {
	
	public static GeoPoint getGeoPoint(double pLatitude, double pLongitude){
		
		GeoPoint geo = new GeoPoint( (int)(pLatitude  * 1000000), (int)(pLongitude  * 1000000), null );
		return geo;
	}
	
	
	public static String getAddressFromPoint(Context pContext, int pLatitude, int pLongitude){
		for(int j = 0 ; j < 10 ; j ++){
		  Geocoder geoCoder = new Geocoder(
				  pContext, Locale.getDefault());
              try {
//                  List<Address> addresses = geoCoder.getFromLocation(
//                      p.getLatitudeE6()  / 1E6, 
//                      p.getLongitudeE6() / 1E6, 1);
                  List<Address> addresses = geoCoder.getFromLocation(
                          pLatitude/ 1E6, 
                          pLongitude/ 1E6,
                          1);

                  String add = "";
                  if (addresses.size() > 0) 
                  {
                      for (int i=0; i<addresses.get(0).getMaxAddressLineIndex(); 
                           i++){
                    	  if(add.length() > 0 )
                         add += " " + addresses.get(0).getAddressLine(i) ;
                    	  else add += addresses.get(0).getAddressLine(i) ; 
                      }
                      return add;
                  }

//                  Toast.makeText(pContext, add, Toast.LENGTH_SHORT).show();
//                  return add;
              }
              catch (IOException e) {                
                  e.printStackTrace();
              }   
		}
              return null;
	}
	
	public static boolean isEqualGeoPoint(GeoPoint pPoint1, GeoPoint pPoint2){
		if(pPoint1 == null || pPoint2 == null) return false;
		else if (pPoint1.getLatitudeE6() == pPoint2.getLatitudeE6()) return true;
		else return false;
	}
	
	private static String getCoordenadaDoEndereco(String p_endereco){
		HttpClient httpclient = new DefaultHttpClient();   
		String result = "";
		ResponseHandler<String> handler = new BasicResponseHandler();   
		byte[] utf8s;
		try {
			utf8s = p_endereco.getBytes("UTF-8");

			String place = new String(utf8s, "UTF-8"); 
			place = place.trim().replace(" ", "+");

			HttpGet request = new HttpGet("http://maps.google.com/maps/geo?q="+place+"&output=xml&oe=utf8");

			result = httpclient.execute(request, handler);
			request.abort();
			String coordinatesConst = "<coordinates>";
			if(result.contains("<coordinates>")){
				return result.substring(result.indexOf("<coordinates>") + coordinatesConst.length(), result.indexOf("</coordinates>") );	
			}

		} catch (UnsupportedEncodingException e) {
			
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return null;
	}

	public static String getTextComDataAtual(Context c){
		return "Localizacao em " +
				HelperDate.getDatetimeAtualFormatadaParaExibicao(c);
	}
	public static String getTextComLatitudeELongitude(GeoPoint pGeoPoint){
		return "Localizacao: Lat: " + (pGeoPoint.getLatitudeE6()/1000000) + " Lng: " + (pGeoPoint.getLongitudeE6()/1000000);
	}
	
	public static GeoPoint getGeoPointDoEndereco(String pStrEndereco){
		if( pStrEndereco != null){
			for(int i = 0 ; i < 3; i  ++){
				String coordenada = getCoordenadaDoEndereco(pStrEndereco);
				if(coordenada != null){
					String vetorCoodernada[] =coordenada.split(",");

					if (vetorCoodernada.length >= 2 ){
						try{
							double latitude = Double.parseDouble(vetorCoodernada[1]);
							double longitude = Double.parseDouble(vetorCoodernada[0]);
							GeoPoint geo = HelperMapa.getGeoPoint(latitude, longitude);
							return geo;
						}catch(Exception ex){

						}

					}
				}	
			}	
		}
		return null;
	}
	
	public static  GeoPoint getGeoPoint(Integer latitude, Integer longitude, float speed){
		if(latitude == null || longitude == null) return null;
		else{
			GeoPoint p = new GeoPoint(latitude, longitude, speed);
	        return p;
		}
	}
	

	public static GeoPoint getGeoPoint(Location location){
		if(location == null) return null;
		GeoPoint p = new GeoPoint(
                (int) (location.getLatitude() * 1E6), 
                (int) (location.getLongitude() * 1E6),
                location.getSpeed());
        
		return p;
	}
	

	
	
}
