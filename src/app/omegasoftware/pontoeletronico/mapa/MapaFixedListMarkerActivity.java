package app.omegasoftware.pontoeletronico.mapa;

//public class MapaFixedListMarkerActivity extends MapActivity{
//
//	public MapView map=null;
//	//	private MyLocationOverlay me=null;
//	
//	
//
//	protected ArrayList<RotaOverlay> listRotaOverlayItem = new ArrayList<RotaOverlay>();
//	boolean isRouteDisplay = false;
//
//	protected ArrayList<CustomOverlayItem> listContainerMarcador = new ArrayList<CustomOverlayItem>();
//	
//	private List<Overlay> mapOverlays;
//
//	//Quando -1 sera o veiculo principal
//	
//	final public static int TYPE_MONITORAMENTO_MARCADOR = 2;
//
//
//	protected int typePontoMonitorado = TYPE_MONITORAMENTO_MARCADOR;
//
//	protected Integer indexPontoMonitorado = null; 
//
//	protected static final String TAG = "MapaFixedListMarkerActivity";
//
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		Bundle vParameters = getIntent().getExtras();
//		int vIdLayout = R.layout.map_layout_mobile;
//		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LAYOUT)){
//			vIdLayout = vParameters.getInt(OmegaConfiguration.SEARCH_FIELD_LAYOUT);
//		}
//		setContentView(vIdLayout);
//		
//		
//		int vListLatitude[] = null;
//		int vListLongitude[] = null;
//		int vListMarker[] = null;
//		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LIST_LATITUDE)){
//			vListLatitude = vParameters.getIntArray(OmegaConfiguration.SEARCH_FIELD_LIST_LATITUDE);
//		}
//		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LIST_LONGITUDE)){
//			vListLongitude = vParameters.getIntArray(OmegaConfiguration.SEARCH_FIELD_LIST_LONGITUDE);
//		}
//		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LIST_MARKER)){
//			vListMarker = vParameters.getIntArray(OmegaConfiguration.SEARCH_FIELD_LIST_MARKER);
//		}
//
//		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_INDEX_FOCO))
//			indexPontoMonitorado = vParameters.getInt(OmegaConfiguration.SEARCH_FIELD_INDEX_FOCO);
//		if(vListLatitude != null && vListLongitude != null){
//			if(vListLatitude.length == vListLongitude.length){
//
//				for (int i = 0 ; i < vListLatitude.length; i++) {
//					int vIdMarker = vListMarker[i];
//					int vLatitude = vListLatitude[i];
//					int vLongitude = vListLongitude[i];
//					Drawable vMarker = this.getResources().getDrawable(vIdMarker);
//					listContainerMarcador.add(
//							new CustomOverlayItem(
//									HelperMapa.getGeoPoint(vLatitude, vLongitude), 
//									"Ponto " + String.valueOf(i), 
//									"Ponto " + String.valueOf(i), 
//									vMarker));
//				}
//			}
//		}
//		int vListLatitudeRota[] = null;
//		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LIST_LATITUDE_ROTA))
//			vListLatitudeRota = vParameters.getIntArray(OmegaConfiguration.SEARCH_FIELD_LIST_LATITUDE_ROTA);
//
//		int vListLongitudeRota[] = null;
//		int color = 999;
//		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LIST_LONGITUDE_ROTA))
//			vListLongitudeRota = vParameters.getIntArray(OmegaConfiguration.SEARCH_FIELD_LIST_LONGITUDE_ROTA);
//
//		if(vParameters.containsKey(OmegaConfiguration.SEARCH_FIELD_LIST_COLOR_ROTA))
//			color = vParameters.getInt(OmegaConfiguration.SEARCH_FIELD_LIST_COLOR_ROTA);
//
//
//		if(vListLatitudeRota != null && vListLongitudeRota != null){
//			if(vListLatitudeRota.length == vListLongitudeRota.length) {
//				int primeiraLatitude = vListLatitudeRota[0];
//				int primeiraLongitude = vListLongitudeRota[0];
//				GeoPoint gp1, gp2;
//				gp1 = HelperMapa.getGeoPoint(primeiraLatitude, primeiraLongitude);
//				listRotaOverlayItem.add(
//						new RotaOverlay(gp1, gp1, 1, color));
//				gp2 = null;
//				for (int i = 1 ; i < vListLatitudeRota.length; i++) {
//					if(gp2 != null) gp1 = gp2;
//					// watch out! For GeoPoint, first:latitude, second:longitude
//					gp2 = HelperMapa.getGeoPoint(vListLatitudeRota[i], vListLongitudeRota[i]);
//
//					listRotaOverlayItem.add(
//							new RotaOverlay(gp1, gp2, 2, color));
//				}
//
//				listRotaOverlayItem.add(
//						new RotaOverlay(gp2, gp2,3, color));
//			}
//		}
//		map=(MapView)findViewById(R.id.mapview);
//		mapOverlays = map.getOverlays(); 
//		loadListCustomItem();
//	}
//
//	@Override
//	public void finish(){
//
//		try {
//
//
//			listContainerMarcador.clear();
//			listRotaOverlayItem.clear();
//			isRouteDisplay = false;
//			
//
//			super.finish();
//		} catch (Throwable e) {
//			
//			e.printStackTrace();
//		}
//	}
//	public void setPontoMonitorado(Integer pIndexMonitorado, int pTypeMonitorado){
//		indexPontoMonitorado = pIndexMonitorado;
//		typePontoMonitorado = pTypeMonitorado;
//	}
//
//	public void focoPontoMonitorado(){
//		GeoPoint vGeoPoint = null;
//
//		switch (typePontoMonitorado) {
//		
//		case TYPE_MONITORAMENTO_MARCADOR :
//			if(indexPontoMonitorado != null){
//				CustomOverlayItem customItem = listContainerMarcador.get(indexPontoMonitorado);
//				if(customItem  != null)
//					vGeoPoint = customItem.getPoint();
//			}
//			break;
//
//		default:
//			break;
//		}
//		if(vGeoPoint != null)
//			focaPonto(vGeoPoint);
//	}
//	
//	
//	
//	protected SitesOverlay addListCustomItemInListContainerMarcador(int pVetorLatitude[], int pVetorLongitude[], int pMarkerId, boolean clearPoints){
//		if(pVetorLatitude != null && pVetorLongitude != null ){
//			if(pVetorLatitude.length == pVetorLongitude.length){
//				Drawable vMarker = this.getResources().getDrawable(pMarkerId);
//				ArrayList<CustomOverlayItem> vListCustom = new ArrayList<CustomOverlayItem>(); 
//				if(vMarker == null) return null;
//				for (int i = 0 ; i < pVetorLatitude.length; i++) {
//					
//					int vLatitude = pVetorLatitude[i];
//					int vLongitude = pVetorLongitude[i];
//					CustomOverlayItem vCustom= new CustomOverlayItem(
//							HelperMapa.getGeoPoint(vLatitude, vLongitude), 
//							"Ponto " + String.valueOf(i), 
//							"Ponto " + String.valueOf(i), 
//							vMarker);
//					listContainerMarcador.add(vCustom);
//					vListCustom.add(vCustom);
//				}
//				SitesOverlay vSite = new SitesOverlay(vListCustom);
//				if(clearPoints)
//					mapOverlays.clear();
//				mapOverlays.add(vSite);
//				map.postInvalidate();
//				return vSite;
//			}
//		}
//		return null;
//	}
//	
//	public void removeSitesOverlay(SitesOverlay pSite){
//		if(mapOverlays.remove(pSite))
//			map.postInvalidate();
//	}
//
//	protected void loadListCustomItem(){
//		ArrayList<CustomOverlayItem> vList = new ArrayList<CustomOverlayItem>();
//
//		
//		GeoPoint vSecundary = null;
//		if(listContainerMarcador != null)
//			for (CustomOverlayItem customItem : listContainerMarcador) {
//				//				customItem.setMarker(iconeCarro);
//				vList.add(customItem);
//				if(vSecundary == null)
//					vSecundary = customItem.getPoint();
//			}
//
//		if(vList.size() > 0 || 
//				listRotaOverlayItem.size() > 0){
//			if(vList.size() > 0){
//				SitesOverlay vSite = new SitesOverlay(vList);
//				mapOverlays.add(vSite);
//			}
//
//			if(listRotaOverlayItem.size() > 0 && !isRouteDisplay){
//				for (Overlay customOverlay : listRotaOverlayItem) {
//					mapOverlays.add(customOverlay);	
//				}
//				isRouteDisplay = true;
//			} else if(listRotaOverlayItem.size() == 0 && isRouteDisplay){
//				listRotaOverlayItem.clear();
//				isRouteDisplay = false;
//			}
//
//			focoPontoMonitorado();
//			map.postInvalidate();
//		}
//	}
//
//
//	public void focaPonto(GeoPoint pGeoPoint){
//		map.getController().setCenter(pGeoPoint);
//		map.getController().setZoom(17);
//		map.setBuiltInZoomControls(true);  
//	}
//
//
//	public boolean onCreateOptionsMenu(Menu menu) {
//		super.onCreateOptionsMenu(menu);
//
//		MenuInflater inflater = getMenuInflater();
//		inflater.inflate(R.menu.map_menu, menu);
//		return true;
//
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//
//		switch (item.getItemId()) {
//
//		case R.id.menu_rastrear_meu_veiculo:
//			indexPontoMonitorado = -1;
//			focoPontoMonitorado();
//			//				intent = new Intent(this, ServiceActivityMobile.class);
//			break;
//		case R.id.menu_rastrear_outro_veiculo:
//			Intent intent = new Intent(this, ListVeiculoUsuarioAtivoActivityMobile.class);
//			startActivityForResult(intent, OmegaConfiguration.STATE_RASTREAR_CARRO);			
//			break;
//			//		case R.id.menu_relatorio_veiculo_bairro:
//			//				intent = new Intent(this, AboutActivityMobile.class);
//			//			break;
//
//		default:
//			return super.onOptionsItemSelected(item);
//
//		}		
//		return true;
//
//	}
//
//
//	@Override
//	public void onResume() {
//		super.onResume();
//	}  
//
//	@Override
//	public void onPause() {
//		super.onPause();
//
//		//		me.disableCompass();
//	}  
//
//
//	@Override
//	protected boolean isRouteDisplayed() {
//		return(false);
//	}
//
//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		if (keyCode == KeyEvent.KEYCODE_S) {
//			map.setSatellite(!map.isSatellite());
//			return(true);
//		}
//		else if (keyCode == KeyEvent.KEYCODE_Z) {
//			map.displayZoomControls(true);
//			return(true);
//		}
//		else if (keyCode == KeyEvent.KEYCODE_H) {
//			//			sites.toggleHeart();
//
//			return(true);
//		}
//
//		return(super.onKeyDown(keyCode, event));
//	}
//
//	private class SitesOverlay extends ItemizedOverlay<OverlayItem> {
//
//		private ArrayList<CustomOverlayItem> items=new ArrayList<CustomOverlayItem>();
//		//	    private PopupPanel panel=new PopupPanel(R.layout.unifacil_alert_dialog);
//
//		public SitesOverlay(CustomOverlayItem pOverlayItem) {
//			super(null);			
//			items.add(pOverlayItem);
//			populate();
//		}
//
//		public SitesOverlay(ArrayList<CustomOverlayItem> pListOverlayItem) {
//			super(null);			
//			if(pListOverlayItem != null)
//				for (CustomOverlayItem overlayItem : pListOverlayItem) {
//					overlayItem.setMarker(this.getMarker(overlayItem.getMarker()));
//					items.add(overlayItem);
//
//				}
//			populate();
//		}
//
//		@SuppressWarnings("unused")
//		public void addOverlay(CustomOverlayItem overlay){
//			items.add(overlay);
//			populate();
//		}
//
//		// Removes overlay item i
//		public void removeItem(int i){
//			this.items.remove(i);
//			populate();
//		}
//
//		@SuppressWarnings("unused")
//		public void clear(){
//			int i = items.size() - 1 ;
//			for (; i >= 0 ; i--) {
//				removeItem(i);
//			}
//		}
//
//		@Override
//		protected CustomOverlayItem createItem(int i) {
//			return(items.get(i));
//		}
//		@Override
//		public void draw(Canvas canvas, MapView mapView,
//				boolean shadow) {
//			super.draw(canvas, mapView, shadow);
//
//		}
//
//		@Override
//		protected boolean onTap(int i) {
//			try{
//				OverlayItem item=getItem(i);
//				GeoPoint geo=item.getPoint();
//				map.getProjection().toPixels(geo, null);
//			} catch(Exception ex){
//
//			}
//			return(true);
//		}
//
//		@Override
//		public int size() {
//			return(items.size());
//		}
//
//
//		private Drawable getMarker(Drawable marker) {
//			marker.setBounds(0, 0, marker.getIntrinsicWidth(),
//					marker.getIntrinsicHeight());
//			boundCenter(marker);
//
//			return(marker);
//		}
//
//	}
//}
