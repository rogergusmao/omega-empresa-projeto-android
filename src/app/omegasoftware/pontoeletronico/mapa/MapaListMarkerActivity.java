package app.omegasoftware.pontoeletronico.mapa;

//public class MapaListMarkerActivity extends MapActivity{
//
//	public static MapView map=null;
//	public static MyLocationOverlay me=null;
//	static private SitesOverlay sites=null;
//	static DatabasePontoEletronico db=null;
//
//	EXTDAOVeiculoUsuario extdaoVeiculoUsuario;
//	protected static final String TABELAS_RELACIONADAS[] = new String[]{EXTDAOModelo.NAME, EXTDAOVeiculo.NAME, EXTDAOUsuario.NAME, EXTDAOVeiculoUsuario.NAME};
//
//	public static ArrayList<RotaOverlay> listRotaOverlayItem = new ArrayList<RotaOverlay>();
//	static boolean isRouteDisplay = false;
//	public static ArrayList<TarefaCustomOverlayItem> listContainerMarcadorMapaTarefaOrigemPropriaConcluida = new ArrayList<TarefaCustomOverlayItem>();
//	public static ArrayList<TarefaCustomOverlayItem> listContainerMarcadorMapaTarefaOrigemPropriaExecucao = new ArrayList<TarefaCustomOverlayItem>();
//	public static ArrayList<TarefaCustomOverlayItem> listContainerMarcadorMapaTarefaOrigemAberta = new ArrayList<TarefaCustomOverlayItem>();
//	public static ArrayList<TarefaCustomOverlayItem> listContainerMarcadorMapaTarefaOrigemAbertaPropria = new ArrayList<TarefaCustomOverlayItem>();
//	public static ArrayList<VeiculoCustomOverlayItem> listContainerMarcadorMapaCarros = new ArrayList<VeiculoCustomOverlayItem>();
//
//	private List<Overlay> mapOverlays;
//
//	private static ListCarPositionIntentsReceiver listCarPositionIntentReceiver = null;
//	Intent serviceListTarefa;
//	Intent serviceListPositionOtherCarIntent;
//
//	//Quando -1 sera o veiculo principal
//	final public static int TYPE_MONITORAMENTO_LIVRE = 0;
//	final public static int TYPE_MONITORAMENTO_PROPRIO = 1;
//	final public static int TYPE_MONITORAMENTO_VEICULO_USUARIO = 2;
//	final public static int TYPE_MONITORAMENTO_TAREFA_ORIGEM = 3;
//	final public static int TYPE_MONITORAMENTO_TAREFA_DESTINO = 4;
//
//
//	private static int typePontoMonitorado = TYPE_MONITORAMENTO_PROPRIO;
//
//	static Integer indexPontoMonitorado = null; 
//
//	//private static String TAG = "MapaListMarkerActivity";
//
//	boolean isServiceInitialized = false;
//
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.map_layout_mobile);
//		try{
//		Bundle vParameters = getIntent().getExtras();
//		String vIdVeiculoUsuario = vParameters.getString(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO);
//
//		db = new DatabasePontoEletronico(this);
//		
////		SynchronizeFastReceiveDatabase vSynchronizeFastReceiveDatabase = new SynchronizeFastReceiveDatabase(
////				this,  
////				-1,
////				TABELAS_RELACIONADAS);
////		
////		vSynchronizeFastReceiveDatabase.run(true);
//		
//		
//		extdaoVeiculoUsuario = new EXTDAOVeiculoUsuario(db);
//		extdaoVeiculoUsuario.setAttrValue(EXTDAOVeiculoUsuario.ID, vIdVeiculoUsuario);
//		extdaoVeiculoUsuario.select();
//
//		map=(MapView)findViewById(R.id.mapview);
//		me=new MyLocationOverlay(this, map);
//		me.enableMyLocation();
//		map.getOverlays().add(me);
//
//		initializeListService();
//		loadListCustomItem();
//		}catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//			
//		}
//	}
//
//	public static GeoPoint getMyGeoPoint(){
//		if(me != null){
//
//			return me.getMyLocation();
//		} else return null;
//	}
//	public static Integer[] getListIdTarefaPropriaExecucao(){
//		Integer[] vListId = new Integer[MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemPropriaExecucao.size()];
//		int i = 0 ;
//		for (TarefaCustomOverlayItem vIdTarefaOrigemAberta : MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemPropriaExecucao) {
//			vListId[i]= vIdTarefaOrigemAberta.getIdTarefa();
//			i += 1;
//		}
//		return vListId;
//	}
//	public static Integer[] getListIdTarefaAberta(){
//		Integer[] vListId = new Integer[MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAberta.size()];
//		int i = 0 ;
//		for (TarefaCustomOverlayItem vIdTarefaOrigemAberta : MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAberta) {
//			vListId[i]= vIdTarefaOrigemAberta.getIdTarefa();
//			i += 1;
//		}
//		return vListId;
//	}
//
//	public static Integer[] getListIdTarefaAbertaPropria(){
//		Integer[] vListId = new Integer[MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAbertaPropria.size()];
//		int i = 0 ;
//		for (TarefaCustomOverlayItem vIdTarefaOrigemAberta : MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAbertaPropria) {
//			vListId[i]= vIdTarefaOrigemAberta.getIdTarefa();
//			i += 1;
//		}
//		return vListId;
//	}
//
//	public static VeiculoCustomOverlayItem getCustomOverlayItemDoVeiculoUsuario(Integer pIdVeiculoUsuario){
//		if(pIdVeiculoUsuario == null) return null; 
//		for (VeiculoCustomOverlayItem vCustom : MapaListMarkerActivity.listContainerMarcadorMapaCarros) {
//			if(vCustom.getIdVeiculoUsuario() == pIdVeiculoUsuario)
//				return vCustom;
//		}
//		return null;
//	}
//
//	public static TarefaCustomOverlayItem getCustomOverlayItemDaTarefa(Integer pIdTarefaOrigem){
//		TarefaCustomOverlayItem vCustom =getCustomOverlayItemDaTarefaAberta(pIdTarefaOrigem);
//		if(vCustom == null){
//			vCustom =getCustomOverlayItemDaTarefaPropria(pIdTarefaOrigem);
//		} 
//		if(vCustom == null){
//			vCustom =getCustomOverlayItemDaTarefaPropriaConcluida(pIdTarefaOrigem);
//		}
//		if(vCustom == null){
//			vCustom =getCustomOverlayItemDaTarefaPropriaExecucao(pIdTarefaOrigem);
//		}
//		return vCustom;	
//	}
//
//	public static TarefaCustomOverlayItem getCustomOverlayItemDaTarefaPropriaConcluida(Integer pIdTarefaOrigem){
//		if(pIdTarefaOrigem == null) return null; 
//		for (TarefaCustomOverlayItem vCustom : MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemPropriaConcluida) {
//			if(vCustom.getIdTarefa() == pIdTarefaOrigem)
//				return vCustom;
//		}
//		return null;
//	}
//	public static TarefaCustomOverlayItem getCustomOverlayItemDaTarefaPropriaExecucao(Integer pIdTarefaOrigem){
//		if(pIdTarefaOrigem == null) return null; 
//		for (TarefaCustomOverlayItem vCustom : MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemPropriaExecucao) {
//			if(vCustom.getIdTarefa() == pIdTarefaOrigem)
//				return vCustom;
//		}
//		return null;
//	}
//
//	public static TarefaCustomOverlayItem getCustomOverlayItemDaTarefaAberta(Integer pIdTarefaOrigem){
//		if(pIdTarefaOrigem == null) return null; 
//		for (TarefaCustomOverlayItem vCustom : MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAberta) {
//			if(vCustom.getIdTarefa() == pIdTarefaOrigem)
//				return vCustom;
//		}
//		return null;
//	}
//
//	public static TarefaCustomOverlayItem getCustomOverlayItemDaTarefaPropria(Integer pIdTarefaOrigem){
//		if(pIdTarefaOrigem == null) return null;
//		for (TarefaCustomOverlayItem vCustom : MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAbertaPropria) {
//			if(vCustom.getIdTarefa() == pIdTarefaOrigem)
//				return vCustom;
//		}
//		return null;
//	}
//
//	@Override
//	public void finish(){
//
//		try {
//			db.close();
//			//			unregisterIntentFilter();
//			if(serviceListPositionOtherCarIntent != null)
//				this.stopService(serviceListPositionOtherCarIntent);
//
//			if(serviceListTarefa != null)
//				this.stopService(serviceListTarefa);
//
//			if(listCarPositionIntentReceiver == null)
//				unregisterReceiver(listCarPositionIntentReceiver);
//
//			listContainerMarcadorMapaTarefaOrigemAbertaPropria.clear();
//			listContainerMarcadorMapaTarefaOrigemAberta.clear();
//			listContainerMarcadorMapaCarros.clear();
//			listRotaOverlayItem.clear();
//			isRouteDisplay = false;
//
//			if(me != null){
//				me.disableMyLocation();
//				me = null;
//			}
//			super.finish();
//		} catch (Throwable e) {
//			
//			e.printStackTrace();
//		}
//	}
//	public static void setPontoMonitorado(Integer pIndexMonitorado, int pTypeMonitorado){
//		indexPontoMonitorado = pIndexMonitorado;
//		typePontoMonitorado = pTypeMonitorado;
//	}
//
//	public void focoPontoMonitorado(){
//		GeoPoint vGeoPoint = null;
//
//		switch (typePontoMonitorado) {
//		case TYPE_MONITORAMENTO_PROPRIO :
//
//			if(me != null)
//				vGeoPoint = me.getMyLocation();
//			break;
//		case TYPE_MONITORAMENTO_VEICULO_USUARIO :
//			VeiculoCustomOverlayItem customItem = getCustomOverlayItemDoVeiculoUsuario(indexPontoMonitorado);
//			if(customItem  != null)
//				vGeoPoint = customItem.getPoint();
//			break;
//
//		case TYPE_MONITORAMENTO_TAREFA_ORIGEM :
//			TarefaCustomOverlayItem customItem2 = getCustomOverlayItemDaTarefa(indexPontoMonitorado);
//			if(customItem2  != null)
//				vGeoPoint = customItem2.getPoint();
//			break;
//
//		case TYPE_MONITORAMENTO_TAREFA_DESTINO :
//			TarefaCustomOverlayItem customItem3 = getCustomOverlayItemDaTarefa(indexPontoMonitorado);
//
//			if(customItem3  != null)
//				vGeoPoint = customItem3.getGeoPointDestino();
//			break;
//
//		default:
//			break;
//		}
//		if(vGeoPoint != null)
//			focaPonto(vGeoPoint);
//		else map.setBuiltInZoomControls(true);  
//	}
//
//	private void loadListCustomItem(){
//		try{
//			Drawable iconeCarro = getResources().getDrawable(R.drawable.car);
//			Drawable iconeMarcador = getResources().getDrawable(R.drawable.marcador);
//			ArrayList<CustomOverlayItem> vList = new ArrayList<CustomOverlayItem>();
//
//
//			GeoPoint vSecundary = null;
//			if(listContainerMarcadorMapaCarros != null)
//				for (CustomOverlayItem customItem : listContainerMarcadorMapaCarros) {
//					//				customItem.setMarker(iconeCarro);
//					vList.add(customItem);
//					if(vSecundary == null)
//						vSecundary = customItem.getPoint();
//				}
//
//			if(listContainerMarcadorMapaTarefaOrigemAberta != null)
//				for (CustomOverlayItem customItem : listContainerMarcadorMapaTarefaOrigemAberta) {
//					vList.add(customItem);
//					TarefaCustomOverlayItem vTarefaCustom = (TarefaCustomOverlayItem) customItem;
//					CustomOverlayItem vCustomDestino =vTarefaCustom.getDestinoCustomOverlayItem();
//					if(vCustomDestino != null) vList.add(vCustomDestino);
//				}
//
//			if(listContainerMarcadorMapaTarefaOrigemAbertaPropria != null)
//				for (CustomOverlayItem customItem : listContainerMarcadorMapaTarefaOrigemAbertaPropria) {
//					vList.add(customItem);
//					TarefaCustomOverlayItem vTarefaCustom = (TarefaCustomOverlayItem) customItem;
//					CustomOverlayItem vCustomDestino =vTarefaCustom.getDestinoCustomOverlayItem();
//					if(vCustomDestino != null) vList.add(vCustomDestino);
//				}
//
//			if(listContainerMarcadorMapaTarefaOrigemPropriaConcluida != null)
//				for (CustomOverlayItem customItem : listContainerMarcadorMapaTarefaOrigemPropriaConcluida) {
//					vList.add(customItem);
//					TarefaCustomOverlayItem vTarefaCustom = (TarefaCustomOverlayItem) customItem;
//					CustomOverlayItem vCustomDestino =vTarefaCustom.getDestinoCustomOverlayItem();
//					if(vCustomDestino != null) vList.add(vCustomDestino);
//				}
//
//			if(listContainerMarcadorMapaTarefaOrigemPropriaExecucao != null)
//				for (CustomOverlayItem customItem : listContainerMarcadorMapaTarefaOrigemPropriaExecucao) {
//					vList.add(customItem);
//					TarefaCustomOverlayItem vTarefaCustom = (TarefaCustomOverlayItem) customItem;
//					CustomOverlayItem vCustomDestino =vTarefaCustom.getDestinoCustomOverlayItem();
//					if(vCustomDestino != null) vList.add(vCustomDestino);
//				}
//			if(vList.size() > 0 || 
//					MapaListMarkerActivity.listRotaOverlayItem.size() > 0){
//				if(vList.size() > 0){
//					if( sites == null){
//						sites = new SitesOverlay(vList);
//					} else{
//						sites.clear();
//						sites = new SitesOverlay(vList);
//					}
//					mapOverlays = map.getOverlays(); 
//					mapOverlays.add(sites);
//					//				if(me != null ){
//					//					//			principalContainerMarcadorMapa.setMarker(iconeMarcador);
//					//					map.getOverlays().add(me);
//					//					me.enableMyLocation();
//					//				}
//				}
//
//				if(MapaListMarkerActivity.listRotaOverlayItem.size() > 0 && !isRouteDisplay){
//					for (Overlay customOverlay : MapaListMarkerActivity.listRotaOverlayItem) {
//						mapOverlays.add(customOverlay);	
//					}
//					isRouteDisplay = true;
//				} else if(MapaListMarkerActivity.listRotaOverlayItem.size() == 0 && isRouteDisplay){
//					MapaListMarkerActivity.listRotaOverlayItem.clear();
//					isRouteDisplay = false;
//				}
//
//				focoPontoMonitorado();
//				map.postInvalidate();
//			}
//		} catch(Exception ex){
//			//if(ex != null){
//				//Log.d(TAG, ex.getMessage());
//			//}
//		}
//	}
//
//	private void initializeListService(){
//		if(! isServiceInitialized){
//			try {
//				//Starts the TruckTrackerService
//				if(serviceListPositionOtherCarIntent == null){
//					serviceListPositionOtherCarIntent = new Intent(this, ServiceSynchronizeListPositionCar.class);
//					serviceListPositionOtherCarIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, extdaoVeiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.ID));
//					startService(serviceListPositionOtherCarIntent);	
//				}
//
//				if(serviceListTarefa == null){
//					serviceListTarefa= new Intent(this, ServiceSynchronizeListTarefaAberta.class);
//
//					serviceListTarefa.putExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, extdaoVeiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.ID));
//					startService(serviceListTarefa);
//				}
//
//				registerIntentFilter();
//			}
//			catch (Exception e) {
//				//Log.d(TAG,"onCreate(): Could not start service");
//			}
//
//
//			if(db == null)
//				try {
//					db = new DatabasePontoEletronico(this);
//				} catch (OmegaDatabaseException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			isServiceInitialized = true;
//
//		}
//
//	}
//
//	private void registerIntentFilter(){
//		if(listCarPositionIntentReceiver == null)
//			listCarPositionIntentReceiver = new ListCarPositionIntentsReceiver();
//		//Starts the TruckTrackerService
//		IntentFilter serviceListCarPositionIntent = new IntentFilter(OmegaConfiguration.REFRESH_SERVICE_LIST_POSITION_OTHER_CAR);
//		registerReceiver(listCarPositionIntentReceiver, serviceListCarPositionIntent);
//
//		IntentFilter serviceMyPositionIntent = new IntentFilter(OmegaConfiguration.REFRESH_SERVICE_MY_POSITION_OTHER_CAR);
//		registerReceiver(listCarPositionIntentReceiver, serviceMyPositionIntent);
//
//	}
//
//	public void focaPonto(GeoPoint pGeoPoint){
//		map.getController().setCenter(pGeoPoint);
//		map.getController().setZoom(17);
//		map.setBuiltInZoomControls(true);  
//	}
//
//
//	public boolean onCreateOptionsMenu(Menu menu) {
//		super.onCreateOptionsMenu(menu);
//
//		MenuInflater inflater = getMenuInflater();
//		inflater.inflate(R.menu.map_menu, menu);
//		return true;
//
//	}
//
//
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//
//		if(requestCode == OmegaConfiguration.ACTIVITY_LIST_VEICULO_USUARIO_ATIVO){
//			try{
//				if(intent != null){
//					Bundle vParams = intent.getExtras();
//
//					if(vParams != null)
//						if(vParams.containsKey(OmegaConfiguration.SEARCH_FIELD_ID)){
//							String vId = vParams.getString(OmegaConfiguration.SEARCH_FIELD_ID);
//							Integer vIntegerId = HelperInteger.parserInteger(vId);
//							setPontoMonitorado(vIntegerId, TYPE_MONITORAMENTO_PROPRIO);
//							focoPontoMonitorado();
//						}
//				}
//			}catch(Exception ex){
//				//Log.w( "onActivityResult()", "Error: " + ex.getMessage() + " \n StackTrace:" + ex.toString());	
//			}
//
//		} else if (requestCode == OmegaConfiguration.STATE_RASTREAR_CARRO) {
//			try{
//				if(intent != null){
//					Bundle vParams = intent.getExtras();
//					if(vParams != null)
//						if(vParams.containsKey(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO)){
//							try{
//								Integer veiculoUsuarioId = vParams.getInt(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, -1);
//								if(veiculoUsuarioId != null){
//									setPontoMonitorado(veiculoUsuarioId, TYPE_MONITORAMENTO_VEICULO_USUARIO);
//									focoPontoMonitorado();
//								}
//							} catch(Exception ex){
//							}
//						}	
//				}
//			}catch(Exception ex2){
//			}
//		} else if (requestCode == OmegaConfiguration.STATE_RASTREAR_TAREFA) {
//			try{
//				if(intent != null){
//					Bundle vParams = intent.getExtras();
//					if(vParams != null)
//						if(vParams.containsKey(OmegaConfiguration.SEARCH_FIELD_TAREFA)){
//							try{
//								Integer tarefaId = vParams.getInt(OmegaConfiguration.SEARCH_FIELD_TAREFA, 0);
//								if(tarefaId != 0)
//									if(vParams.containsKey(OmegaConfiguration.SEARCH_FIELD_TYPE_MONITORAMENTO)){
//										Integer vTypeMonitoramento = vParams.getInt(OmegaConfiguration.SEARCH_FIELD_TYPE_MONITORAMENTO);
//										if(vTypeMonitoramento != null){
//											switch (vTypeMonitoramento) {
//											case TYPE_MONITORAMENTO_PROPRIO:
//												setPontoMonitorado(null, TYPE_MONITORAMENTO_PROPRIO);
//												focoPontoMonitorado();
//												break;
//
//											default:
//												if(tarefaId >= 0 ){
//													setPontoMonitorado(tarefaId, vTypeMonitoramento);
//													focoPontoMonitorado();
//												}			
//												break;
//											}
//										}
//									}
//
//								if(vParams.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_DISPLAY_ROTA)){
//									Boolean isToDisplayRota  = vParams.getBoolean(OmegaConfiguration.SEARCH_FIELD_IS_DISPLAY_ROTA);
//									if(isToDisplayRota ) loadListCustomItem();
//								}
//
//
//							} catch(Exception ex){
//							}
//						}
//				}	
//			} catch(Exception ex){
//
//			}
//
//		}
//	}
//
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//
//		switch (item.getItemId()) {
//
//		case R.id.menu_rastrear_meu_veiculo:
//			typePontoMonitorado = TYPE_MONITORAMENTO_PROPRIO;
//			focoPontoMonitorado();
//			//				intent = new Intent(this, ServiceActivityMobile.class);
//			break;
//		case R.id.menu_rastrear_outro_veiculo:
//			Intent intent = new Intent(this, ListVeiculoUsuarioAtivoActivityMobile.class);
//			startActivityForResult(intent, OmegaConfiguration.STATE_RASTREAR_CARRO);			
//			break;
//			//		case R.id.menu_relatorio_veiculo_bairro:
//			//			//				intent = new Intent(this, AboutActivityMobile.class);
//			//			break;
//		case R.id.menu_lista_de_tarefa_aberta:
//			try{
//				Intent intent2 = new Intent(this, ListTarefaOnlineActivityMobile.class);
//				intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, HelperInteger.parserInt(extdaoVeiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.ID) ) );
//				intent2.putExtra(OmegaConfiguration.SEARCH_FIELD_TYPE_TAREFA, TYPE_TAREFA.ABERTA );
//				startActivityForResult(intent2, OmegaConfiguration.STATE_RASTREAR_TAREFA);
//			} catch(Exception ex){
//
//			}
//
//			break;
//		case R.id.menu_lista_de_tarefa_propria:
//			try{
//				Intent intent3 = new Intent(this, ListTarefaOnlineActivityMobile.class);
//				intent3.putExtra(OmegaConfiguration.SEARCH_FIELD_TYPE_TAREFA, TYPE_TAREFA.PROPRIA_ABERTA );
//				intent3.putExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, HelperInteger.parserInt(extdaoVeiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.ID) ) );
//				startActivityForResult(intent3, OmegaConfiguration.STATE_RASTREAR_TAREFA);	
//			} catch(Exception ex){
//
//			}
//			break;
//		case R.id.menu_lista_de_tarefa_propria_execucao:
//			try{
//				Intent intent3 = new Intent(this, ListTarefaOnlineActivityMobile.class);
//				intent3.putExtra(OmegaConfiguration.SEARCH_FIELD_TYPE_TAREFA, TYPE_TAREFA.PROPRIA_EXECUCAO );
//				intent3.putExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, HelperInteger.parserInt(extdaoVeiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.ID) ) );
//				startActivityForResult(intent3, OmegaConfiguration.STATE_RASTREAR_TAREFA);	
//			} catch(Exception ex){
//
//			}
//			break;
//
//		case R.id.menu_lista_de_tarefa_recentemente_concluida:
//			try{
//				Intent intent3 = new Intent(this, ListTarefaOnlineActivityMobile.class);
//				intent3.putExtra(OmegaConfiguration.SEARCH_FIELD_TYPE_TAREFA, TYPE_TAREFA.PROPRIA_CONCLUIDA );
//				intent3.putExtra(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO, HelperInteger.parserInt(extdaoVeiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.ID) ) );
//				startActivityForResult(intent3, OmegaConfiguration.STATE_RASTREAR_TAREFA);	
//			} catch(Exception ex){
//
//			}
//			break;
//		default:
//			return super.onOptionsItemSelected(item);
//		}		
//		return true;
//	}
//
//	@Override
//	public void onResume() {
//		super.onResume();
//		//		me.enableMyLocation();
//		registerIntentFilter();
//		//		me.enableCompass();
//	}  
//	//	@Override
//	//	public void onBackPressed(){
//	//		super.onBackPressed();
//	//		finish();
//	//	}
//
//
//	@Override
//	public void onPause() {
//
//
//		if(db != null)
//			db.close();
//		unregisterIntentFilter();
//		//		me.disableMyLocation();
//		//		me.disableCompass();
//
//		super.onPause();
//	}  
//
//	public void unregisterIntentFilter(){
//		unregisterReceiver(listCarPositionIntentReceiver);
//	}
//	@Override
//	protected boolean isRouteDisplayed() {
//		return(false);
//	}
//
//
//	@Override
//	public void onUserInteraction(){
//		if(typePontoMonitorado != TYPE_MONITORAMENTO_LIVRE)
//			setPontoMonitorado(-1, TYPE_MONITORAMENTO_LIVRE);
//	}
//
//	//	@Override
//	//	public boolean onTouchEvent(MotionEvent event){
//	//		
//	//		setPontoMonitorado(-1, TYPE_MONITORAMENTO_LIVRE);
//	//		return onTouchEvent(event);
//	//	}
//
//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//
//		if (keyCode == KeyEvent.KEYCODE_S) {
//			map.setSatellite(!map.isSatellite());
//			return(true);
//		}
//		else if (keyCode == KeyEvent.KEYCODE_Z) {
//			map.displayZoomControls(true);
//			return(true);
//		}
//		else if (keyCode == KeyEvent.KEYCODE_H) {
//			//			sites.toggleHeart();
//
//			return(true);
//		}
//
//		return(super.onKeyDown(keyCode, event));
//	}
//
//	private class SitesOverlay extends ItemizedOverlay<OverlayItem> {
//
//		private ArrayList<CustomOverlayItem> items=new ArrayList<CustomOverlayItem>();
//		//	    private PopupPanel panel=new PopupPanel(R.layout.unifacil_alert_dialog);
//
//		public SitesOverlay(CustomOverlayItem pOverlayItem) {
//			super(null);			
//			items.add(pOverlayItem);
//			populate();
//		}
//
//		public SitesOverlay(ArrayList<CustomOverlayItem> pListOverlayItem) {
//			super(null);			
//			if(pListOverlayItem != null)
//				for (CustomOverlayItem overlayItem : pListOverlayItem) {
//					overlayItem.setMarker(this.getMarker(overlayItem.getMarker()));
//					items.add(overlayItem);
//
//				}
//			populate();
//
//		}
//
//
//		@SuppressWarnings("unused")
//		public void addOverlay(CustomOverlayItem overlay){
//			items.add(overlay);
//			populate();
//		}
//
//		// Removes overlay item i
//		public void removeItem(int i){
//			this.items.remove(i);
//			populate();
//		}
//
//		public void clear(){
//			int i = items.size() - 1 ;
//			for (; i >= 0 ; i--) {
//				removeItem(i);
//			}
//		}
//
//		@Override
//		protected CustomOverlayItem createItem(int i) {
//			return(items.get(i));
//		}
//		@Override
//		public void draw(Canvas canvas, MapView mapView,
//				boolean shadow) {
//			super.draw(canvas, mapView, shadow);
//
//		}
//
//
//
//		@Override
//		public int size() {
//			return(items.size());
//		}
//
//
//		private Drawable getMarker(Drawable marker) {
//			marker.setBounds(0, 0, marker.getIntrinsicWidth(),
//					marker.getIntrinsicHeight());
//			boundCenter(marker);
//
//			return(marker);
//		}
//
//	}
//
//	private class ListCarPositionIntentsReceiver extends BroadcastReceiver {
//
////		public static final String TAG = "ListCarPositionIntentsReceiver";
//
//		@Override
//		public void onReceive(Context context, Intent intent) {
//
//			//Log.d(TAG,"onReceive(): Intent received - " + intent.getAction());
//
//			if(intent.getAction().equals(OmegaConfiguration.REFRESH_SERVICE_LIST_POSITION_OTHER_CAR)) {
//				loadListCustomItem();
//			} else if ( intent.getAction().equals(OmegaConfiguration.REFRESH_SERVICE_LIST_POSITION_TAREFA)){
//				loadListCustomItem();
//
//			}
//		}
//	}
//
//	//	private ArrayList<OverlayItem> loadListGeoPointOfOtherCarFromDatabase(){
//	//		EXTDAOUsuarioPosicao vObjVeiculoPosicao = new EXTDAOUsuarioPosicao(db);
//	//		ArrayList<GeoPoint> vListGeoPoint = new ArrayList<GeoPoint>(); 
//	//
//	//		ArrayList<Long> vListIdVeiculoPosicao = vObjVeiculoPosicao.getListIdLastPositionOfOtherCars(
//	//				extdaoVeiculoUsuario.getStrValueOfAttribute(EXTDAOVeiculoUsuario.ID));
//	//		ArrayList<OverlayItem> vListContainerMarcadorMapa = new ArrayList<OverlayItem>(); 
//	//		Drawable icone = this.getResources().getDrawable(R.drawable.car);
//	//
//	//		if(vListIdVeiculoPosicao != null){
//	//			for (Long vIdVeiculoPosicao : vListIdVeiculoPosicao) {
//	//				vObjVeiculoPosicao.clearData();
//	//				vObjVeiculoPosicao.setAttrStrValue(EXTDAOUsuarioPosicao.ID, vIdVeiculoPosicao.toString());
//	//				vObjVeiculoPosicao.select();
//	//				String vStrLatitude = vObjVeiculoPosicao.getStrValueOfAttribute(EXTDAOUsuarioPosicao.LATITUDE_INT);
//	//				String vStrLongitude = vObjVeiculoPosicao.getStrValueOfAttribute(EXTDAOUsuarioPosicao.LONGITUDE_INT);
//	//
//	//				GeoPoint vGeoPoint = HelperMapa.getGeoPoint(vStrLatitude, vStrLongitude);
//	//				if(vGeoPoint != null){
//	//					vListGeoPoint.add(vGeoPoint);
//	//					//				
//	//					//				CustomItem vCustom = new CustomItem(
//	//					//						vGeoPoint, 
//	//					//						"Car: " + HelperDate.getDataAtualFormatada(), 
//	//					//						HelperMapa.getTextComLatitudeELongitude(vGeoPoint), 
//	//					//						icone);
//	//
//	//					OverlayItem vContainerMarcadorMapa = new OverlayItem(
//	//							vGeoPoint, 
//	//							HelperDate.getDataAtualFormatada(), 
//	//							HelperMapa.getTextComLatitudeELongitude(vGeoPoint));
//	//					vContainerMarcadorMapa.setMarker(icone);
//	//					vListContainerMarcadorMapa.add(vContainerMarcadorMapa);
//	//				}
//	//
//	//			}
//	//			return vListContainerMarcadorMapa;
//	//		} else return null;
//	//	}
//
//}
