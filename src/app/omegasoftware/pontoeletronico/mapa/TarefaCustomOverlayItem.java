package app.omegasoftware.pontoeletronico.mapa;

//public class TarefaCustomOverlayItem extends CustomOverlayItem {
//
//	//caso seja direcionada a tarefa a alguem, o endereco de destino deve ser preenchido
//	Integer idTarefa = null;
//	
//	Integer idVeiculoUsuario = null;
//
//	//Pode ser nulo
//	GeoPoint ptOrigem;
//	GeoPoint ptDestino;
//	GeoPoint ptProprio;
//	
//	String origemEndereco;
//	String origemComplemento;
//	
//	String destinoComplemento;	
//	String destinoEndereco;
//
//	CustomOverlayItem customOverlayDestino = null;
//	
//	String datetimeCadastro;
//	String dateAberturaTarefa;
//	String datetimeInicioMarcadoDaTarefa;
//	String datetimeInicioTarefa;
//	String datetimeFimTarefa;
//
//	//Pode ser nulo
//	String descricao;
//
//	public TarefaCustomOverlayItem(
//			Integer pIdTarefa,
//			GeoPoint ptProprio,
//			GeoPoint ptOrigem,
//			String pOrigemEndereco,
//			String pOrigemComplemento,
//			String pDestinoEndereco,
//			String pDestinoComplemento,
//			String name, 
//			String snippet,
//			Drawable markerOrigem,
//			Drawable markerDestino,
//			Integer pIdVeiculoUsuario,
//			String pDatetimeCadastro,
//			String pDateAberturaTarefa,
//			String pDatetimeInicioMarcadoDaTarefa,
//			String pDatetimeInicioTarefa,
//			String pDatetimeFimTarefa,
//			String pDescricao) {
//		super(ptOrigem, name, snippet, markerOrigem);
//		
//		dateAberturaTarefa = pDateAberturaTarefa;
//		datetimeInicioMarcadoDaTarefa = pDatetimeInicioMarcadoDaTarefa;
//		this.marker=markerOrigem;
//		
//		origemComplemento = pOrigemComplemento;
//		origemEndereco = pOrigemEndereco;
//		
//		destinoEndereco = pDestinoEndereco; 
//		destinoComplemento = pDestinoComplemento;
//		
//		this.ptProprio = ptProprio;
//		
//		if(destinoEndereco != null)
//			ptDestino = HelperMapa.getGeoPointDoEndereco(destinoEndereco);
//		
//		idTarefa = pIdTarefa;
//		idVeiculoUsuario = pIdVeiculoUsuario;
//		datetimeCadastro = pDatetimeCadastro;
//		descricao = pDescricao;
//		datetimeInicioTarefa = pDatetimeInicioTarefa;
//		datetimeFimTarefa = pDatetimeFimTarefa;
//		if(ptDestino != null)
//			customOverlayDestino = new CustomOverlayItem(
//					ptDestino, 
//					"Destino da Tarefa " + String.valueOf(this.idTarefa), 
//					destinoEndereco, 
//					markerDestino);
//			
//		
//	}	
//	
//	public CustomOverlayItem getDestinoCustomOverlayItem(){
//		
//		return customOverlayDestino;
//	}
//	
//	
//	public String getOrigemComplemento(){
//		return origemComplemento;
//	}
//	
//	public String getDestinoComplemento(){
//		return destinoComplemento;
//	}
//	public String getDateAberturaTarefa(){
//		return dateAberturaTarefa ;	
//	}
//	
//	public String getDatetimeInicioTarefa(){
//		return datetimeInicioTarefa;
//	}
//	
//	public void setDatetimeInicioTarefa(String pDatetimeInicioTarefa){
//		datetimeInicioTarefa = pDatetimeInicioTarefa;
//	}
//	
//	public void setDatetimeFimTarefa(String pDatetimeFimTarefa){
//		datetimeFimTarefa = pDatetimeFimTarefa;
//	}
//	
//	public String getDatetimeFimTarefa(){
//		return datetimeFimTarefa;
//	}
//	
//	public String getDatetimeInicioMarcadoDaTarefa(){
//		return datetimeInicioMarcadoDaTarefa;
//	}
//	
//	public GeoPoint getGeoPointOrigem(){
//		return ptOrigem;
//	}
//	
//	public GeoPoint getGeoPointDestino(){
//		return ptDestino;
//	}
//	
//	public GeoPoint getGeoPointProprio(){
//		return ptProprio;
//	}
//
//	public String getDatetimeCadastro(){
//		return datetimeCadastro;
//	}
//
//	public String getEnderecoDestino(){
//		return destinoEndereco;
//	}
//
//	public String getEnderecoOrigem(){
//		return origemEndereco;
//	}
//
//	public Integer getIdVeiculoUsuario(){
//		return idVeiculoUsuario;
//	}
//
//	public String getDescricao(){
//		return this.descricao;
//	}
//	
//	public Integer getIdTarefa(){
//		return this.idTarefa;
//	}
//
//}
