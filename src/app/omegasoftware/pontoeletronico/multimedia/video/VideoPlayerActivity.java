package app.omegasoftware.pontoeletronico.multimedia.video;

import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.SurfaceView;
import android.widget.MediaController;
import android.widget.VideoView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.OmegaRegularActivity;

public class VideoPlayerActivity extends OmegaRegularActivity {
public static String TAG = "VideoPlayerActivity"; 
    VideoView myVideoView ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.entreteirement_activity_tablet_layout);
		myVideoView = (VideoView)findViewById(R.id.myvideoview1);
//		Button atDomiciliarCallButton = (Button) findViewById(R.id.atendimentodomiciliar_phone_button);
//		atDomiciliarCallButton.setText(R.string.atendimentodomiciliar_call_phone_number);
//		atDomiciliarCallButton.setOnClickListener(new PhoneClickListener());
//				
//		Button atDomiciliarBrowser = (Button) findViewById(R.id.atendimentodomiciliar_browser_button);		
//		atDomiciliarBrowser.setOnClickListener(new BrowserOpenerListener(getResources().getString(R.string.atendimentodomiciliar_url)));
//		
		playNovela();
		//playVideo();
	}
	
	@Override
	public boolean loadData() {
		
		return true;
	}

	@Override
	public void initializeComponents() {
		
		
	}

	public void playNovela(){
		
//		String outFileName = "/data/data/android.DoutorLaser/files/rei_do_gado.mp4"  ;
		//String outFileName = "/sdcard/O Rei do Gado - Vinheta de abertura.mp4"  ;
		String outFileName = "/mnt/sdcard/O Rei do Gado - Vinheta de abertura.mp4";
		//String outFileName = "/mnt/sdcard/giant_rubberband_ball.3gp";
//		int indiceMaximo = 7;
//		String vetorArquivoEntrada[] = new String[indiceMaximo + 1];
//		String sufixo = "O Rei do Gado - Vinheta de abertura.mp4_out_";
//		for(Integer i = 0 ; i <= indiceMaximo ; i++ ){
//			
//			vetorArquivoEntrada[i] = sufixo + i.toString(); 
//		}
//		
//		FileSystemHelper.mergeListFileInAssetsInFile(vetorArquivoEntrada, outFileName, this.getApplicationContext());

       myVideoView.setVideoURI(Uri.parse(outFileName));
       myVideoView.setMediaController(new MediaController(this));
       myVideoView.requestFocus();
       Runnable r = new Runnable() {
         public void run() {
             try {
            	 myVideoView.start();
                 
             } catch (Exception e) {
            	 SingletonLog.insereErro(e, TIPO.PAGINA);
             }
         }
     };
     new Thread(r).start();
       
	}
	
	 private void playVideo() {
		 MediaPlayer mp = new MediaPlayer();


       getWindow().setFormat(PixelFormat.TRANSPARENT);

		 String current = "";
       try {
           final String path =  "/sdcard/O Rei do Gado - Vinheta de abertura.mp4";
           
           // If the path has not changed, just start the media player
           if (path.equals(current) && mp != null) {
               mp.start();
               return;
           }
           current = path;
           SurfaceView mPreview = (SurfaceView) findViewById(R.id.myvideoview1);

//         // Set a size for the video screen
//         SurfaceHolder holder = mPreview.getHolder();
        // holder.addCallback(this);
//         holder.setFixedSize(400, 300);
           // Create a new media player and set the listeners
//           VideoView myVideoView = (VideoView)findViewById(R.id.myvideoview);
           mp.setAudioStreamType(2);

           // Set the surface for the video output
           mp.setDisplay(mPreview.getHolder());
           mp.prepare();
           //Log.v(TAG, "Duration:  ===>" + mp.getDuration());
           mp.start();
//           // Set the data source in another thread
//           // which actually downloads the mp3 or videos
//           // to a temporary location
//           Runnable r = new Runnable() {
//               public void run() {
//                   try {
//                       setDataSource(path);
//                   } catch (IOException e) {
//                       Log.e(TAG, e.getMessage(), e);
//                   }
//                   mp.prepare();
//                   Log.v(TAG, "Duration:  ===>" + mp.getDuration());
//                   mp.start();
//               }
//           };
//           new Thread(r).start();
       } catch (Exception e) {
    	   SingletonLog.insereErro(e, TIPO.PAGINA);
           if (mp != null) {
               mp.stop();
               mp.release();
           }
       }
   }

	
	public void videoPlayer(String path, String fileName, boolean autoplay){
	    //get current window information, and set format, set it up differently, if you need some special effects
	    getWindow().setFormat(PixelFormat.TRANSLUCENT);
	    //the VideoView will hold the video
	    VideoView videoHolder = new VideoView(this);
	    //MediaController is the ui control howering above the video (just like in the default youtube player).
	    videoHolder.setMediaController(new MediaController(this));
	    //assing a video file to the video holder
	    videoHolder.setVideoURI(Uri.parse(path+"/"+fileName));
	    //get focus, before playing the video.
	    videoHolder.requestFocus();
	    if(autoplay){
	        videoHolder.start();
	    }
	 
	 }
}
