package app.omegasoftware.pontoeletronico.phone;

import java.io.File;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import app.omegasoftware.pontoeletronico.date.HelperDate;

public class HelperPhone {
	public final static long ERROR = -1; 
	static String imei = null;
	static AtomicBoolean fullDisk = new AtomicBoolean(false);
	static Date lastCheckFullDisk = null; 
	static ReentrantLock reentrantLock = new ReentrantLock();
	public synchronized static void setFullDisk(boolean fullDisk){
		if(reentrantLock.tryLock()){
			HelperPhone.fullDisk.set(fullDisk);	
			lastCheckFullDisk = new Date();
		}
		
	}
	
	public static boolean externalMemoryAvailable() {
        return android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED);
    }
	final static long QUANTIDADE_MINIMA_ESPACO_MB = 5;
	final static long TEMPO_INTERVALO_MINIMO_PARA_VERIFICACAO_ESPACO = 15;
	final static long FATOR_MB = 1024 * 1024 ;
	final static long FATOR_KB = 1024 ;
	final static long FATOR_B = 1;
	static AtomicInteger counterCheckFullDisk = new AtomicInteger();
	final static int LIMITE_COUNTER_CHECK_FULL_DISK = 100; 
	public static boolean isFullDisk(){
		HelperDate hd = new HelperDate();
		if(lastCheckFullDisk != null 
				&& (counterCheckFullDisk.incrementAndGet() <  LIMITE_COUNTER_CHECK_FULL_DISK 
				|| hd.getAbsDiferencaEmMinutos(lastCheckFullDisk) < TEMPO_INTERVALO_MINIMO_PARA_VERIFICACAO_ESPACO))
			return fullDisk.get();	
		
		if(reentrantLock.tryLock()){
			try{
				counterCheckFullDisk.set(0);
				long externalBytes = getAvailableExternalMemorySize(FORMATO_ESPACO.MB);
				if(externalBytes < QUANTIDADE_MINIMA_ESPACO_MB) 
					fullDisk.set(true);
				else {
					long internalBytes = getAvailableInternalMemorySize(FORMATO_ESPACO.MB);
					if(internalBytes < QUANTIDADE_MINIMA_ESPACO_MB) fullDisk.set(true);
					else fullDisk.set(false);
				}
				lastCheckFullDisk = new Date();
				return fullDisk.get();
			}finally{
			reentrantLock.unlock();	
			}
		} else return fullDisk.get();
		
	}
	
    public static long getAvailableInternalMemorySize(FORMATO_ESPACO espaco) {
        File path = Environment.getRootDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return format(availableBlocks * blockSize, espaco);
    }

    public static long getTotalInternalMemorySize(FORMATO_ESPACO espaco) {
        File path = Environment.getRootDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return format( totalBlocks * blockSize,espaco);
    }
    
    public static Long getAvailableRamSize(Context context){
    	
//    	ActivityManager actManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
//    	MemoryInfo memInfo = new ActivityManager.MemoryInfo();
//    	actManager.getMemoryInfo(memInfo);
//    	long totalMemory = memInfo.availMem;
//    	return totalMemory;
    	Long l = Long.valueOf(-1);
    	return l;
    }
    public enum FORMATO_ESPACO{
    	B,
    	KB,
    	MB
    }
    private static long format(long bytes, FORMATO_ESPACO formato){
    	switch (formato) {
			case KB:
				return bytes/FATOR_KB;
			case MB:
				return bytes/FATOR_MB;
			case B:
			default:
				return bytes/FATOR_B;
		}
    }
    public static long getAvailableExternalMemorySize(FORMATO_ESPACO formato) {
        if (externalMemoryAvailable()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long availableBlocks = stat.getAvailableBlocks();
            long result = availableBlocks * blockSize;
            return format(result, formato);
        } else {
            return ERROR;
        }
    }

    public static long getTotalExternalMemorySize(FORMATO_ESPACO formato) {
        if (externalMemoryAvailable()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long totalBlocks = stat.getBlockCount();
            return format(totalBlocks * blockSize, formato);
        } else {
            return ERROR;
        }
    }

    public static String formatSize(long size) {
        String suffix = null;

        if (size >= 1024) {
            suffix = "KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = "MB";
                size /= 1024;
            }
        }

        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }

        if (suffix != null) resultBuffer.append(suffix);
        return resultBuffer.toString();
    }
	public HelperPhone(){
		
	}
	public static String getIMEI() throws Exception{
		
		if(imei == null)
			throw new Exception("Imei indefinido");
		return imei;
	}
	public static String getIMEI(Context p_context) throws Exception
	{
		if(imei != null) return imei;
		
		TelephonyManager telephonyManager = (TelephonyManager) p_context.getSystemService(Context.TELEPHONY_SERVICE);
		imei= telephonyManager.getDeviceId();
		if(imei == null)
			throw new Exception("Imei indefinido");
		return imei;
	}
	
	public class Container{
		public String marca;
		public String cpu;
		public String modelo;
		public Container(){
			
		}
	}
	
	public Container getInformacoesTelefone(){
		// Device model
		Container c = new Container();
		c.marca = android.os.Build.BRAND;
		c.cpu = android.os.Build.CPU_ABI;
		c.modelo = android.os.Build.MODEL;		
		return c;
		
	}
	
	
}
