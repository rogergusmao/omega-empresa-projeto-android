package app.omegasoftware.pontoeletronico.phone;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.WindowManager;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;

public class HelperScreen {

	public static File printScreen(View view, String path, String nomeArquivo){
		boolean erro = false;
		File myPath = null;
		FileOutputStream fos = null;
		try {
			View v = view.getRootView();
			v.setDrawingCacheEnabled(true);
			Bitmap b = v.getDrawingCache();
			
			myPath = new File(path, nomeArquivo);
			
		
		    fos = new FileOutputStream(myPath);
		    b.compress(Bitmap.CompressFormat.JPEG, 70, fos);
		    fos.flush();
		    fos.close();
		    return myPath;
		    
		} catch (FileNotFoundException e) {

		    SingletonLog.insereErro(e, TIPO.PAGINA);
		    erro = true;
		    return null;
		} catch (Exception e) {

			SingletonLog.insereErro(e, TIPO.PAGINA);
			erro = true;
		    return null;
		}finally{
			if(erro){
				if(myPath != null && myPath.exists())
					myPath.delete();
			}
			
		}
		
	}
	public class ConfiguracaoTela{
		public int largura ;
		public int altura;
	}
	public ConfiguracaoTela getConfiguracaoTela(Context context){
		WindowManager mWinMgr = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		ConfiguracaoTela conf = new ConfiguracaoTela();
		conf.largura = mWinMgr.getDefaultDisplay().getWidth();
        conf.altura = mWinMgr.getDefaultDisplay().getHeight();
        return conf;
	}
	
}
