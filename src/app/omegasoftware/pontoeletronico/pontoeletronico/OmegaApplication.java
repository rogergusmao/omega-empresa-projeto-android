package app.omegasoftware.pontoeletronico.pontoeletronico;

import android.app.Activity;
import android.app.Application;

public class OmegaApplication extends Application{
	static OmegaApplication singleton;
	@Override
	public void onCreate(){
		super.onCreate();
		singleton = this;
		
	}
	
	public static Application getSingleton(){
		return singleton;
	}
	public static Activity getTopActivity(){
		return null;
	}
}
