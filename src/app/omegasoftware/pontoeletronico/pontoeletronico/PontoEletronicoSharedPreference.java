package app.omegasoftware.pontoeletronico.pontoeletronico;

import android.content.Context;
import app.omegasoftware.pontoeletronico.common.HelperSharedPreference;
import app.omegasoftware.pontoeletronico.common.HelperSharedPreference.TIPO;
import app.omegasoftware.pontoeletronico.date.HelperDate;

public class PontoEletronicoSharedPreference {
	
	public static final String PREFERENCES_NAME = "PontoEletronicoSharedPreference";
	
	public enum TIPO_STRING{
		ULTIMO_ID_SINCRONIZACAO,
		JSON_ITEM_SINCRONIZACAO,
		JSON_ITEM_PARSE_SINCRONIZACAO,
		
		LOGIN_AUTOMATICO_CORPORACAO,
		LOGIN_AUTOMATICO_EMAIL,
		LOGIN_AUTOMATICO_SENHA,
		ID_SISTEMA_SIHOP,
		
		ID_SEGURANCA,
		ID_LOGIN_AUTOMATICO,
		
		CACHE_EMAIL,
		CACHE_SENHA,
		
		EMAIL_DO_ULTIMO_LOGIN_REALIZADO,
		CORPORACAO_DO_ULTIMO_LOGIN_REALIZADO
	}
	
	public enum TIPO_BOOLEAN{
		RESETAR_BANCO_DE_DADOS,
		
		LOGIN_AUTOMATICO_ATIVO
	}
	
	public enum TIPO_VETOR_STRING{
		IDS_SISTEMA_TABELA_NOTIFICACAO
	}
	
	public enum TIPO_VETOR_INT{
//		IDS_SINCRONIZACAO,
		IDS_SISTEMA_TABELA,
	}
	
	public enum TIPO_DATETIME{
		ULTIMO_ENVIO_LOG,
		ULTIMO_CLEAN_DIR_LOG
	}
	
	public enum TIPO_INT{
		ESTADO_SINCRONIZADOR,
		ULTIMO_ID_SINCRONIZACAO,
		ULTIMO_ID_SISTEMA_HISTORICO_SINCRONIZADOR_LIDO,
		ULTIMO_ID_EMPRESA_RELOGIO_PONTO,
		CURRENT_VERSION_CODE
		
	}
	
	public enum HASH{
		CRUD_INSERCAO,
		CRUD_REMOCAO,
		CRUD_EDICAO,
		
	}
	
//	public static String getValor(Context pContext, String pTipo){
//		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
//		return h.getValorString(pContext, pTipo);
//	}
//	
	public static void saveValor(Context pContext, TIPO_DATETIME chave, HelperDate hd){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		
		h.saveValorDateTime(pContext, chave.toString(), hd);
	}
	
	public static HelperDate getValorDateTime(Context c, TIPO_DATETIME tipo){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		return h.getValorDateTime(c, tipo.toString());
	}
	public static void apagarHash(Context c, HASH hash){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		h.removerTodasAsChavesComOPrefixo(c, TIPO.INT, hash.toString() );
	}
	public static void saveValor(Context pContext, HASH hash, Integer id, Integer valor){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		String chave = hash.toString() + "_" + id.toString();
		h.saveValorInt(pContext, chave, valor);
	}
	
	public static Integer getValor(Context pContext, HASH hash, Integer id){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		String chave = hash.toString() + "_" + id.toString();
		if(h.possuiChave(pContext, chave, TIPO.INT)){
			int v = h.getValorInt(pContext, chave, -1);
			return v< 0 ? null : v;
		} else return null;
	}

	
	public static void saveValor(Context pContext, TIPO_VETOR_STRING pTipo, String[] pValor){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		h.saveValorArrayString(pContext, pTipo.toString(), pValor);
	}
	
	
	public static void saveValor(Context pContext, TIPO_VETOR_INT pTipo, Integer[] pValor){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		h.saveValorArrayInteger(pContext, pTipo.toString(), pValor);
	}
	
	public static String getValor(Context pContext, String pTipo){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		return h.getValorString(pContext, pTipo);
	}
	
	public static int getValor(Context pContext, String pTipo, int defaultValor){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		return h.getValorInt(pContext, pTipo, defaultValor);
	}
	
	public static Integer[] getValor(Context pContext, TIPO_VETOR_INT pTipo){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		return h.getValorArrayInteger(pContext, pTipo.toString());
	}
	

	
	public static String[] getValor(Context pContext, TIPO_VETOR_STRING pTipo){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		return h.getValorArrayString(pContext, pTipo.toString());
	}

	public static void saveValor(Context pContext, TIPO_BOOLEAN pTipo, boolean pValor){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		h.saveValorBoolean(pContext, pTipo.toString(), pValor);
		
	}
	
	public static boolean getValor(Context pContext, TIPO_BOOLEAN pTipo){
		//saveValor(pContext, pTipo, true);
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		return h.getValorBoolean(pContext, pTipo.toString());
	}
	
	public static void saveValor(Context pContext, TIPO_STRING pTipo, String pValor){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		h.saveValorString(pContext, pTipo.toString(), pValor);
	}
	
	public static void saveValor(Context pContext, String chave, String pValor){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		h.saveValorString(pContext, chave, pValor);
	}
	
	public static String getValor(Context pContext, TIPO_STRING pTipo){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		return h.getValorString(pContext, pTipo.toString());
	}
	
	
	
	public static void saveValor(Context pContext, TIPO_INT pTipo, int pValor){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		h.saveValorInt(pContext, pTipo.toString(), pValor);					
	}

	public static void saveValor(Context pContext, TIPO_INT pTipo, long pValor){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		h.saveValorLong(pContext, pTipo.toString(), pValor);					
	}
	
	
	public static int getValor(Context pContext, TIPO_INT pTipo, int defaultValue){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		return h.getValorInt(pContext, pTipo.toString(), defaultValue);	
	}
	
	public static String getValorAsString(Context pContext, TIPO_INT pTipo){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		int valor = h.getValorInt(pContext, pTipo.toString(), -1);
		if(valor == -1) return null;
		else return String.valueOf(valor);
	}
	
	public static long getValorLong(Context pContext, TIPO_INT pTipo, int defaultValue){
		HelperSharedPreference h = new HelperSharedPreference(PREFERENCES_NAME);
		return h.getValorLong(pContext, pTipo.toString(), defaultValue);	
	}

}
