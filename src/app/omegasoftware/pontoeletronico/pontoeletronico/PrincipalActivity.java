package app.omegasoftware.pontoeletronico.pontoeletronico;

import android.app.Activity;
import android.app.Dialog;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.RemoteViews;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.MenuActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.ResetarActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.widget.UltimoPontoWidget;
import app.omegasoftware.pontoeletronico.TabletActivities.widget.UltimoPontoWidget.UpdateService;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.ControladorBibliotecaNuvem;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.ControladorServicoInterno;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.activity.HelperFonte;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaAuthenticatorActivity;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaCorporacaoSincronizador.STATE_SINCRONIZADOR;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaRegistroSincronizadorAndroidParaWeb;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaTabela;
import app.omegasoftware.pontoeletronico.database.synchronize.RotinaSincronizador;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho;
import app.omegasoftware.pontoeletronico.database.synchronize.SingletonControladorChaveUnica;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSControler;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_DATETIME;
import app.omegasoftware.pontoeletronico.routine.RotinaCleanDir;
import app.omegasoftware.pontoeletronico.service.ControlerServicoInterno;
import app.omegasoftware.pontoeletronico.service.ServiceSynch;

public class PrincipalActivity extends Activity {
	public enum VERSOES {
		V0(0, "01/01/2018", false),
		V32(33, "27/02/2018", true),
		;
		public int id ;
		public String desc;
		public boolean houveMudancaBanco;
		public int getId(){return id;}
		public boolean houveMudancaBanco(){return houveMudancaBanco;}
		VERSOES (int id, String desc, boolean houveMudancaBanco){
			this.id=id;
			this.desc =desc;
			this.houveMudancaBanco=houveMudancaBanco;
		}


		public static VERSOES getVersao(int id){
			VERSOES[] vs = VERSOES.values();
			for(int i = 0 ; i < vs.length; i++){
				if(vs[i].getId() == id)return vs[i];
			}
			return null;
		}
	}
	// private static final String TAG = "PrincipalActivity";
	public static boolean isTablet = false;
	
	HandlerBack handlerBackPressed;
	public HandlerGPS handlerGPS;
	private Boolean salvarSenha = false;
	public final int ERROR_DIALOG_SINCRONIZACAO_NAO_REALIZADA = 1;
	public final int ERRO_DE_SINCRONIZACAO_SERVIDOR_OFFLINE = 2;
	public final int ERROR_DIALOG_CHAVE_SEGURANCA_INVALIDA = 3;
	public final int DIALOG_VERSAO_HOMOLOGACAO = 4;
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_mobile);
		handlerBackPressed = new HandlerBack();
		handlerGPS = new HandlerGPS();
		new Loader().execute();
	}



	public boolean loadData() throws Exception {
		SingletonLog.constroi(this);
		
		try {
			Intent intent = getIntent();
			if (intent != null && intent.hasExtra(OmegaConfiguration.PARAM_EXIT)) {
				if (intent.getBooleanExtra(OmegaConfiguration.PARAM_EXIT, false) == true) {
					finish();
					return false;
				}
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}

		ControladorBibliotecaNuvem.carregaXMLConfiguracao(PrincipalActivity.this, false);
		
		Database.createStructureIfNecessary(this);

		HelperFonte.loadFonts(this);
		
		ControladorServicoInterno controladorSI = ControladorServicoInterno.constroi(this);
		controladorSI.startAllServices();

		checkNewVersion();

		if (OmegaSecurity.isAutenticacaoRealizada()) {
			//Se o programa ju estava aberto e o usuurio ju havia sido autenticado
			//checa novamente o status da sincronizacao, pois o usuairo pode ter saido antes do fim
			return checkSynchronizationDatabase() ;
		} else // prossegue para o   redirecionaParaPaginaDeLoginOuMenuPrincipal
			return true;
	}
static boolean versaoChecada = false;
	public void checkNewVersion() throws PackageManager.NameNotFoundException {
		try{
			if(versaoChecada ) return;
			PackageInfo packageInfo = getPackageManager()
					.getPackageInfo(getPackageName(), 0);
			int versionCode = packageInfo.versionCode;
			int old = PontoEletronicoSharedPreference.getValor(this, PontoEletronicoSharedPreference.TIPO_INT.CURRENT_VERSION_CODE, -1);

			if(old == -1 || old < versionCode){
				VERSOES versao = VERSOES.getVersao(versionCode);
				if(versao != null && versao.houveMudancaBanco()){
					upgradeDatabase();
				}
				PontoEletronicoSharedPreference.saveValor(
						this
						, PontoEletronicoSharedPreference.TIPO_INT.CURRENT_VERSION_CODE
						, versionCode);
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
		}finally {
			versaoChecada = true;
		}

	}
	private void upgradeDatabase(){
		EXTDAOSistemaCorporacaoSincronizador.STATE_SINCRONIZADOR state = RotinaSincronizador.getStateSincronizador(
						this, null);
		switch (state) {
			case COMPLETO:
					SincronizadorEspelho.updateEstadoEstruturaMigrada(this);
				break;
			default:
				break;
		}
	}
	
	public void redirecionaParaPaginaDeLoginOuMenuPrincipal() throws Exception {		
		if (OmegaSecurity.isAutenticacaoRealizada()) {

			Intent intent = new Intent(PrincipalActivity.this, MenuActivityMobile.class);
			startActivityForResult(intent, OmegaConfiguration.STATE_INITIALIZE_PONTO_ELETRONICO);
		} else {
			
			Intent intent = new Intent(this, OmegaAuthenticatorActivity.class);
			intent.putExtra(OmegaConfiguration.PARAM_AUTO_CONNECT_IF_SAVED,
					!getIntent().hasExtra(OmegaConfiguration.PARAM_EXIT));
			startActivityForResult(intent, OmegaConfiguration.STATE_AUTHENTICATION);
		}
	}


	private boolean checkSynchronizationDatabase() throws Exception {

		boolean validade = true;
	
		STATE_SINCRONIZADOR state = RotinaSincronizador.getStateSincronizador(PrincipalActivity.this, null);
		switch (state) {
		case INCOMPLETO:
			validade = false;
			break;
		case COMPLETO:
			validade = true;
			break;
		case BANCO_AINDA_NAO_CRIADO:
		case NAO_REALIZADO:

			validade = RotinaSincronizador.procedimentoSincronizacaoNaoRealizada(
					PrincipalActivity.this,
					salvarSenha);
			break;
		case RESETAR:
			//
			Intent intent = new Intent(PrincipalActivity.this, ResetarActivityMobile.class);
			startActivityForResult(intent, OmegaConfiguration.STATE_RESETAR);
			validade = false;
			break;
		default:
			validade = false;
			break;
		}
		if(validade){
			EXTDAOSistemaTabela.initCache(PrincipalActivity.this);
			
			EXTDAOSistemaRegistroSincronizadorAndroidParaWeb.initSequences(PrincipalActivity.this);

			SingletonControladorChaveUnica s = SingletonControladorChaveUnica.getSingleton(PrincipalActivity.this);
			s.initCache(PrincipalActivity.this);

			
			startService(new Intent(this, StartServicosSecundarios.class));
			
			startService(new Intent(this, UpdateService.class));
			
			this.handlerGPS.sendEmptyMessage(0);
//			try{
//				Intent intent = new Intent(this, UltimoPontoWidget.class);
//				intent.setFlags (Intent.FLAG_ACTIVITY_NEW_TASK);
//				startActivity(intent);	
//			}catch(Exception ex){
//				SingletonLog.insereErro(ex, SingletonLog.TIPO.PAGINA);
//			}
			
		}
		return validade;
	}
	
	public static class StartServicosSecundarios extends Service {
		
		@Override
		public void onStart(Intent intent, int startId) {
			ControlerServicoInterno controleSI = ControlerServicoInterno.constroi(this);
			controleSI.startAllServicesAutenticados();
			
			RotinaCleanDir rotinaCleanDirLog = new RotinaCleanDir(31, TIPO_DATETIME.ULTIMO_CLEAN_DIR_LOG);
			if (OmegaConfiguration.CLEAR_LOG) {
				rotinaCleanDirLog.runIfItsTime(this);
			}
			
			
		}

		@Override
		public IBinder onBind(Intent intent) {
			return null;
		}
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		
		try {
			switch (requestCode) {
			
			case OmegaConfiguration.STATE_INITIALIZE_PONTO_ELETRONICO:

				handlerBackPressed.sendEmptyMessage(0);
				break;
			
			case OmegaConfiguration.STATE_AUTHENTICATION:
				if (resultCode == RESULT_OK) {
					if (intent != null) {
						if (intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_GRAVA_SENHA)) {
							salvarSenha = intent.getBooleanExtra(OmegaConfiguration.SEARCH_FIELD_GRAVA_SENHA, false);
						}
					}
					
					new SincronizarBancoDeDados().execute();

				} else if (resultCode == RESULT_CANCELED) {
					finish();
				}
				break;
			case OmegaConfiguration.STATE_RESETAR:
				new Loader().execute();
			break;
			default:
				break;
			}

		} catch (Exception ex) {

			SingletonLog.openDialogError(this, ex, SingletonLog.TIPO.PAGINA);
		} 
	}


	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = null;
		try {


			dialog = new Dialog(this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			dialog.setContentView(R.layout.dialog);

			TextView vTextView = (TextView) dialog.findViewById(R.id.tv_dialog);
			Button vOkButton = (Button) dialog.findViewById(R.id.dialog_ok_button);

			switch (id) {
			case ERRO_DE_SINCRONIZACAO_SERVIDOR_OFFLINE:
				vTextView.setText(getResources().getString(R.string.error_sincronizacao_servidor_offline));
				vOkButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dismissDialog(ERRO_DE_SINCRONIZACAO_SERVIDOR_OFFLINE);
						RotinaSincronizador.actionLogout(PrincipalActivity.this, true);
					}
				});
				break;

			case ERROR_DIALOG_SINCRONIZACAO_NAO_REALIZADA:
				vTextView.setText(getResources().getString(R.string.error_primeira_sincronizacao));
				vOkButton.setOnClickListener(

						new OnClickListener() {
							public void onClick(View v) {
								dismissDialog(ERROR_DIALOG_SINCRONIZACAO_NAO_REALIZADA);
								RotinaSincronizador.actionLogout(PrincipalActivity.this, true);
							}
						});
				break;
			default:
				return super.onCreateDialog(id);

			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
		return dialog;

	}

	public class SincronizarBancoDeDados extends AsyncTask<Bundle, Integer, Boolean> {
		public String TAG = "SincronizarBancoDeDados";
		Exception ex = null;
		public SincronizarBancoDeDados() {

		}
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
		}
		

		@Override
		protected Boolean doInBackground(Bundle... params) {
			try {
				if (checkSynchronizationDatabase()) {			
					
					return true;
				} 
			} catch (Exception e) {
				
				this.ex=e;
			}

			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				if(SingletonLog.openDialogError(PrincipalActivity.this, ex, SingletonLog.TIPO.PAGINA))
					return;
				
				if (result) {
					Intent intent = new Intent(PrincipalActivity.this, MenuActivityMobile.class);
					startActivityForResult(intent, OmegaConfiguration.STATE_INITIALIZE_PONTO_ELETRONICO);
				} else {
					PrincipalActivity.this.showDialog(ERROR_DIALOG_SINCRONIZACAO_NAO_REALIZADA);
				}

			} catch (Exception e) {
				SingletonLog.openDialogError(PrincipalActivity.this, e, SingletonLog.TIPO.PAGINA);
			}

		}
	}


	

	public class HandlerBack extends Handler {

		@Override
		public void handleMessage(Message msg) {
			try{
				PrincipalActivity.this.onBackPressed();	
			}catch(Exception ex){
				//em caso de falha, finaliza o programa
				SingletonLog.insereErro(ex, TIPO.PAGINA);
				RotinaSincronizador.actionLogout(PrincipalActivity.this, false);
			}
		}
	}
	
	public class Loader extends AsyncTask<String, Integer, Boolean> {
		
		public Loader() {
			
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		
		Exception e2 = null;
		@Override
		protected Boolean doInBackground(String... params) {
			try {
				
				return loadData();
			}catch(Exception ex){
				this.e2=ex;
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			try {
				if (result) {
					redirecionaParaPaginaDeLoginOuMenuPrincipal();
				}
				if(this.e2!=null){
					SingletonLog.openDialogError(PrincipalActivity.this, this.e2, SingletonLog.TIPO.PAGINA);
				}
								
			} 
			catch (OmegaDatabaseException e) {
				SingletonLog.factoryToast(PrincipalActivity.this, e, SingletonLog.TIPO.ADAPTADOR);
				
			}
			catch (Exception ex) {
				SingletonLog.openDialogError(PrincipalActivity.this, ex, TIPO.PAGINA);
			}

		}

	}
	
	public class HandlerGPS extends Handler {

		@Override
		public void handleMessage(Message msg) {
			try{
				GPSControler.construct(PrincipalActivity.this);
			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.PAGINA);
			}
		}
	}
}
