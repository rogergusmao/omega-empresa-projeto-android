package app.omegasoftware.pontoeletronico.primitivetype;

import java.util.Random;

public class HelperBoolean {
	public static Boolean valueOfStringSQL(String pToken){
		if(pToken == null) return null;
		else if(pToken.compareTo("0") == 0 ) return false;
		else if(pToken.compareTo("1") == 0 ) return true;
		else return null;
	}
	public static boolean parserInt(int i){
		if(i == 1 ) return true;
		else return false;
	}
	public static Boolean getRandom(){
		
		Random random = new Random();		
		return random.nextBoolean();
		
	}
	
}
