package app.omegasoftware.pontoeletronico.primitivetype;

import java.util.Random;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;

public class HelperDouble {
	
	
	public static Double parserDouble(String pIndex){
		Double index = null;
		
		try{
			if(pIndex == null) return null;
			else if (pIndex.length() == 0 ) return null;
			index = Double.parseDouble(pIndex);
			return index;
		} catch(Exception ex){
			SingletonLog.insereWarning(ex, TIPO.UTIL_ANDROID);
			return null;
		}

	}
	
	public static Double getRandom(int max){
		Random random = new Random();
		
		return random.nextDouble() * max;
	}
}
