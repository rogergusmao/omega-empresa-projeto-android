package app.omegasoftware.pontoeletronico.primitivetype;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;

public class HelperFloat {
	
	
	public static Float parserFloat(String pIndex){
		Float index = null;
		
		try{
			if(pIndex == null) return null;
			else if (pIndex.length() == 0 ) return null;
			index = Float.parseFloat(pIndex);
			return index;
		} catch(Exception ex){
			SingletonLog.insereWarning(ex, TIPO.UTIL_ANDROID);
			return null;
		}

	}
}
