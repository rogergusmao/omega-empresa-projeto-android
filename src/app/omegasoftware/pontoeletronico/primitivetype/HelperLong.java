package app.omegasoftware.pontoeletronico.primitivetype;

import java.util.Random;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;

public class HelperLong {
	public static int parserBoolean(boolean v){
		if(v) return 1;
		else return 0;
	}
	
	public static int parserInt(String pIndex){
		int index = -1;
		try{
			if(pIndex == null) return -1;
			else if (pIndex.length() == 0 ) return -1;
			index = Integer.parseInt(pIndex);
			return index;
		} catch(Exception ex){
			SingletonLog.insereWarning(ex, TIPO.UTIL_ANDROID);
			return -1;
		}

	}
	
	public static int arredondarParaCima(int dividendo, int divisor){
		double divisao = (double)dividendo / divisor;
		divisao = Math.ceil(divisao);
		int totalAbas = (int)divisao;
		return totalAbas;
	}
	
	public static Integer getRandom(int max){
		if(max <= 0) return 0;
		Random random = new Random();
		try{
			
			int r = random.nextInt(max);
			return r;
		}catch(Exception ex){
			SingletonLog.insereWarning(ex, TIPO.UTIL_ANDROID);
			return null;
		}
		
		
	}
	
	
	public static Integer getIndexOfContent(Integer vVetor[] , Integer pValue){
		if(vVetor == null) return null;
		int i = 0 ;
		for (Integer vValueOfVetor : vVetor) {
			
			if(vValueOfVetor != null) {
				if(vValueOfVetor == pValue) return i;
			}
			i += 1;
		}
		return null;
	}
	
	
	public static Integer parserInteger(String pIndex){
		
		Integer index = -1;
		try{
			if(pIndex == null) return null;
			else if (pIndex.length() == 0 ) return null;
			index = Integer.parseInt(pIndex);
			return index;
		} catch(Exception ex){
			SingletonLog.insereWarning(ex, TIPO.UTIL_ANDROID);
			return null;
		}

	}
	public static Long parserLong(String pIndex){
		
		Long index = (long)-1;
		try{
			if(pIndex == null) return null;
			else if (pIndex.length() == 0 ) return null;
			index = Long.parseLong(pIndex);
			return index;
		} catch(Exception ex){
			SingletonLog.insereWarning(ex, TIPO.UTIL_ANDROID);
			return null;
		}

	}
}
