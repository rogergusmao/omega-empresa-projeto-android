package app.omegasoftware.pontoeletronico.primitivetype;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;

import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;



public class HelperString {


	  private static final String UPPERCASE_ASCII =
	    "AEIOU" // grave
	    + "AEIOUY" // acute
	    + "AEIOUY" // circumflex
	    + "AON" // tilde
	    + "AEIOUY" // umlaut
	    + "A" // ring
	    + "C" // cedilla
	    + "OU" // double acute
	    ;

	  private static final String UPPERCASE_UNICODE =
	    "\u00C0\u00C8\u00CC\u00D2\u00D9"
	    + "\u00C1\u00C9\u00CD\u00D3\u00DA\u00DD"
	    + "\u00C2\u00CA\u00CE\u00D4\u00DB\u0176"
	    + "\u00C3\u00D5\u00D1"
	    + "\u00C4\u00CB\u00CF\u00D6\u00DC\u0178"
	    + "\u00C5"
	    + "\u00C7"
	    + "\u0150\u0170"
	    ;
	  
	  public static String substring(String token, int limit){
		  return substring(token, limit, "...");
	  }
	  
		public static String substring(String token, int limit, String sufixoEmCasoDeStringMaior){
			if(token != null){
				if(token.length() > limit)
					return token.substring(0, limit) + sufixoEmCasoDeStringMaior ;
			}
			return token;
		}
	  public static String getWordWithoutAccent(String word){
		  
			 if(word == null || word.length() == 0) return null;
			 else {
				 word = Normalizer.normalize(word, Normalizer.Form.NFD);
				 word = word.replaceAll("[^\\p{ASCII}]", "");
//				    word = word.replaceAll("[uuuu]","e");
//				    word = word.replaceAll("[uu]","u");
//				    word = word.replaceAll("[uu]","i");
//				    word = word.replaceAll("[uu]","a");
//				    word = word.replaceAll("u","o");
//
//				    word = word.replaceAll("[uuuu]","E");
//				    word = word.replaceAll("[uu]","U");
//				    word = word.replaceAll("[uu]","I");
//				    word = word.replaceAll("[uu]","A");
//				    word = word.replaceAll("u","O");
				    return word;
			 }
		}
	  public static String getRandomName(String pPrefixo){
		  return getRandomName(pPrefixo, -1);  
	  }
	public static String getRandomName(String pPrefixo, int maxLength )
	{
		String token = "";
		if(pPrefixo == null) token = HelperString.generateRandomString(maxLength);
		else token = pPrefixo + HelperString.generateRandomString(maxLength) ;
		
		if(maxLength > 0 && token.length() > maxLength){
			token = token.substring(0, maxLength);
		}
		
		return token;
	}	
	  public static String valueOfBooleanSQL(boolean pValue){
		  if(pValue) return "1";
		  else return "0";
	  }
	 
	  public static String getStrQueryInDatabase(int quantidade){
		  
			StringBuilder vLine = new StringBuilder(); 
			for(int i = 0; i < quantidade; i++){
				if(i> 0 )vLine.append(",");
				vLine.append("?");
			}
			return vLine.toString();
		}
	  
	  public static String getStrSeparateByDelimiterColumnDatabase(Integer pVetorToken[]){
			String vLine =""; 
			for (Integer vToken : pVetorToken) {
				if(vToken == null) continue ;
				else if(vLine.length() > 0 ) vLine += OmegaConfiguration.DELIMITER_QUERY_COLUNA + String.valueOf( vToken);
				else vLine += String.valueOf( vToken); 
				 
			}
			return vLine;
		}
	  
	  public static String getStrSeparateByColumn(Integer[] vetor, String pDelimiter){
			StringBuilder vLine = new StringBuilder(); 
			for (Integer vToken : vetor) {
				if(vToken == null) continue;
				else if(vLine.length() > 0 ) vLine.append( pDelimiter + vToken);
				else vLine.append(vToken); 
				 
			}
			if(vLine.length() == 0 ) return null;
			else return vLine.toString();
	  }
 
	  
	  public static String getStrSeparateByColumn(String[] vetor, String pDelimiter){
			StringBuilder vLine = new StringBuilder(); 
			for (String vToken : vetor) {
				if(vToken == null) continue;
				else if(vLine.length() > 0 ) vLine.append( pDelimiter + vToken);
				else vLine.append(vToken); 
				 
			}
			if(vLine.length() == 0 ) return null;
			else return vLine.toString();
	  }
	  
	  public static String getStrSeparateByDelimiterColumn(ArrayList<String> pVetorToken, String pDelimiter){
			String vLine =""; 
			for (String vToken : pVetorToken) {
				if(vToken == null) continue;
				else if (vToken.length() == 0 ) continue;
				else if(vLine.length() > 0 ) vLine += pDelimiter + vToken;
				else vLine += vToken; 
				 
			}
			if(vLine.length() == 0 ) return null;
			else return vLine;
		}
	  
	  public static String getStrSeparateByDelimiterColumn(String pVetorToken[], String pDelimiter){
			String vLine =""; 
			for (String vToken : pVetorToken) {
				if(vToken == null) continue;
				else if (vToken.length() == 0 ) continue;
				else if(vLine.length() > 0 ) vLine += pDelimiter + vToken;
				else vLine += vToken; 
				 
			}
			if(vLine.length() == 0 ) return null;
			else return vLine;
		}
	  
		public static String getStrSeparateByDelimiterColumnDatabase(String pVetorToken[]){
			String vLine =""; 
			for (String vToken : pVetorToken) {
		
				if(vLine.length() > 0 ) vLine += OmegaConfiguration.DELIMITER_QUERY_COLUNA + vToken;
				else vLine += vToken; 
				 
			}
			return vLine;
		}
		
		public static String getStrSeparateByDelimiterLineDatabase(String pVetorToken[]){
			String vLine =""; 
			for (String vToken : pVetorToken) {
				if(vLine.length() > 0 ) vLine += OmegaConfiguration.DELIMITER_QUERY_LINHA + vToken;
				else vLine += vToken;
			}
			return vLine;
		}

		
		public static String getIndexTokenInHashMap(LinkedHashMap<String, String> pVetorToken, String pId){
			Set<String> vSet = pVetorToken.keySet();
			for (String vId : vSet) {
				if(vId.compareTo(pId) == 0 ){
					return vId;
				}
			}
			return null;
		  }
		
		public static ArrayList<String> getArrayListOfContent(LinkedHashMap<String, String> pVetorToken){
			Set<String> vSet = pVetorToken.keySet();
			ArrayList<String> vList = new ArrayList<String>(); 
			for (String vId : vSet) {
				vList.add(pVetorToken.get(vId));
				
			}
			return vList;
		  }
		
		public static String getContentInHashMap(LinkedHashMap<String, String> pVetorToken, String pId){
			Set<String> vSet = pVetorToken.keySet();
			for (String vId : vSet) {
				if(vId.compareTo(pId) == 0 ){
					return pVetorToken.get(vId);
				}
			}
			return null;
		  }
		
	  public static int getIndexTokenInVector(String[] pVetorToken, String pToken){
		  if(pVetorToken == null) return -1;
		  else if (pToken == null) return -1;
		  int i = 0 ;
		  for (String vToken : pVetorToken) {
			  if(vToken != null) 
				  if(vToken.compareTo(pToken) == 0 ) return i;
			i += 1;
		}
		  return -1;
	  }
	  
	  public static int getIndexTokenInVectorCaseInsensitive(String[] pVetorToken, String pToken){
		  if(pVetorToken == null) return -1;
		  else if (pToken == null) return -1;
		  
		  String vTokenCompare = pToken.toLowerCase();
		  for ( int i = 0 ; i < pVetorToken.length; i++) {
			  String vToken = pVetorToken[i];
			  if(vToken != null) {
				  String vTokenCompareAux = vToken.toLowerCase();
				  if(vTokenCompareAux.compareTo(vTokenCompare) == 0 ) return i;
			  }
			
		}
		  return -1;
	  }
	  
	  public static String toUpperCaseSansAccent(String txt) {
	       if (txt == null) {
	          return null;
	       }
	       String txtUpper = txt.toUpperCase();
	       StringBuilder sb = new StringBuilder();
	       int n = txtUpper.length();
	       for (int i = 0; i < n; i++) {
	          char c = txtUpper.charAt(i);
	          int pos = UPPERCASE_UNICODE.indexOf(c);
	          if (pos > -1){
	            sb.append(UPPERCASE_ASCII.charAt(pos));
	          }
	          else {
	            sb.append(c);
	          }
	       }
	       return sb.toString();
	  }	
	  
	public static String generateRandomString(int maxSize)
	{
		Random random = new Random();
		String hash = Long.toHexString(random.nextLong());
		if(maxSize > 0 && hash != null && hash.length() > maxSize)
			return hash.substring(0, maxSize);
		return hash;
	}
	
	public static String[] splitWithNoneEmptyToken(String pToken, String pDelimiter){
		if(pToken == null) return null;
		else if(pToken.length() == 0 ) return null;
		else{
			
			String vVetorToken[] = pToken.split(pDelimiter);
			ArrayList<String> vList = new ArrayList<String>();
			for (String vTokenAux : vVetorToken) {
				if(vTokenAux == null) continue;
				else if(vTokenAux.length() > 0 ){
					vList.add(vTokenAux);
				}
			}
			
			String vVetorTokenReturn[] = new String[vList.size()] ;
			vList.toArray((String[]) vVetorTokenReturn);
			return vVetorTokenReturn;
		}
	}

	public static String checkIfIsNull(String pToken){
		
		if(pToken == null ) return null;
		else if(pToken.length() == 0 ) return null;
		else{
			if(pToken.toUpperCase().compareTo("NULL") == 0 ) return null;
			else return pToken;
		}
	}
	
	public static String getStrAndCheckIfIsNull(String pToken){
		if(pToken == null) return "null";
		else if (pToken.length() == 0  ) return "null";
		else return pToken;
	}
	  public static String retiraEspacoDoInicioEFim(String pToken){
		  if(pToken!= null){
				if(pToken.length() > 1 ){
					int i = 0 ;
					for(i = 0 ; i < pToken.length(); i ++){
						char vChar = pToken.charAt(i);
						if(vChar != ' '){
							break;
						}
					}
					int j = pToken.length() - 1;
					for(j = pToken.length() - 1 ; j >= 0; j --){
						char vChar = pToken.charAt(j);
						if(vChar != ' '){
							break;
						}
					}
					if(i < pToken.length() )
						return pToken.substring(i, j + 1);					
					else return pToken;
				}
		  }
		  return pToken;
	  }
	  
	  public static String ucFirst(String pToken){
			if(pToken!= null){
				
				if(pToken.length() > 1 ){
					String vTokenSemEspacoNoInicioEFim = retiraEspacoDoInicioEFim(pToken);	
					String vNewToken = vTokenSemEspacoNoInicioEFim.substring(0, 1).toUpperCase() + 
							vTokenSemEspacoNoInicioEFim.substring(1, vTokenSemEspacoNoInicioEFim.length() ).toLowerCase() ; 
					return vNewToken;
					 
				} else{
					return pToken.toUpperCase();
				}
				
			}
			return null;
		}
		
	  public static String ucFirstForEachToken(String pToken){
		  if(pToken == null) return null;
		  String vVetorToken[] = pToken.split(" ");
		  String vNewToken = "";
		  for (String vToken : vVetorToken) {
			  if(vNewToken.length() > 0 )
			    vNewToken += " " + HelperString.ucFirst(vToken);
			  else vNewToken += HelperString.ucFirst(vToken);
			    	
		}
		  return vNewToken;
	  }

	public static String arrayToString(List<String> valores){
		return HelperString.arrayToString(valores, null);
	}
	  
	  public static String arrayToString(String[] valores){
		  return HelperString.arrayToString(valores, null);
	  }
	  
	  public static String arrayToString(String[] valores, String delimitador){
		  if(delimitador == null)
			  delimitador = ",";
		  if(valores == null) return null;
		  else if(valores.length == 0 )return null;
		  StringBuilder sb=new StringBuilder();	  
		  for (String string : valores) {
			  if(sb.length() > 0 )
				  sb.append(delimitador);
			  sb.append(string);	
		  }
		  return sb.toString();
		  
	  }

	public static String arrayToString(List<String> valores, String delimitador){
		if(delimitador == null)
			delimitador = ",";
		if(valores == null) return null;
		else if(valores.size() == 0 )return null;
		StringBuilder sb=new StringBuilder();
		for (String string : valores) {
			if(sb.length() > 0 )
				sb.append(delimitador);
			sb.append(string);
		}
		return sb.toString();

	}

	  public static String arrayToString(ArrayList<String[]> linhas, String delimitador, String quebraLinha){
		  
		  if(delimitador == null)
			  delimitador = ",";
		  if(quebraLinha == null)
			  quebraLinha= "\r\n";
		  if(linhas == null) return null;
		  else if(linhas.size() == 0 )return null;
		  StringBuilder sb=new StringBuilder();	  
		  for (String[] colunas : linhas) {
			  if(colunas == null){
				  sb.append("[null]" + quebraLinha);
			  }
			  else if(colunas.length == 0){
				  sb.append("[vazio]" + quebraLinha);
			  } else{
				  boolean primeiro = true;
				  for (String coluna : colunas) {
					if(!primeiro)sb.append(delimitador);
					else primeiro = false;
					sb.append(coluna);
				}
			  }
		  }
		  return sb.toString();
		  
	  }
	  
	  public static String formatarParaImpressao(String token){
		  return token == null ? "null" : token;
	  }
	  public static String formatarParaImpressao(Integer x ){
		  return x == null ? "null" : x.toString();
	  }
	  public static String formatarParaImpressao(Long x ){
		  return x == null ? "null" : x.toString();
	  }
	  public static String formatarParaImpressao(Boolean x ){
		  return x == null ? "null" : x.toString();
	  }
	  public static String formatarNomeDoEmail(String token){
		  if(token == null || token.length() == 0) return token;
		  int indexArroba = token.indexOf("@");
		  if(indexArroba  >= 0 )
			  token = token.substring(0, indexArroba);
		  return token;
	  }
	  public static String arrayToString(Integer[] valores, String delimitador){
		  if(delimitador == null)
			  delimitador = ",";
		  if(valores == null) return null;
		  else if(valores.length == 0 )return null;
		  StringBuilder sb=new StringBuilder();	  
		  for (Integer string : valores) {
			  if(sb.length() > 0 )
				  sb.append(delimitador);
			  
			  sb.append(string == null ? "null" : string.toString());	
		  }
		  return sb.toString();
		  
	  }
	  

	  public static String arrayToString(Long[] valores, String delimitador){
		  if(delimitador == null)
			  delimitador = ",";
		  if(valores == null) return null;
		  else if(valores.length == 0 )return null;
		  StringBuilder sb=new StringBuilder();	  
		  for (Long string : valores) {
			  if(sb.length() > 0 )
				  sb.append(delimitador);
			  
			  sb.append(string == null ? "null" : string.toString());	
		  }
		  return sb.toString();
		  
	  }
	  
	  public static String arrayToStringFirstAndLast(Long[] valores, String delimitador){
		  if(delimitador == null)
			  delimitador = ",";
		  if(valores == null) return null;
		  else if(valores.length == 0 )return null;
		  StringBuilder sb=new StringBuilder();
		  sb.append(valores[0]);
		  sb.append(" ... ");
		  sb.append(valores[valores.length - 1]);
		  
		  return sb.toString();
		  
	  }
	  
	  
	  public static String arrayToStringSQL(String[] valores){
		  
		  if(valores == null) return null;
		  else if(valores.length == 0 )return null;
		  StringBuilder sb=new StringBuilder();	  
		  for (String string : valores) {
			  if(sb.length() > 0 )
				  sb.append(",");
			  sb.append("\""+ string+ "\"");
		  }
		  return sb.toString();
		  
	  }
	  
	  public static String arrayToStringSQL(ArrayList<String> valores){
		  
		  if(valores == null) return null;
		  else if(valores.size() == 0 )return null;
		  StringBuilder sb=new StringBuilder();	  
		  for (String string : valores) {
			  if(sb.length() > 0 )
				  sb.append(",");
			  sb.append("\""+ string+ "\"");
		  }
		  return sb.toString();
		  
	  }
	
}
