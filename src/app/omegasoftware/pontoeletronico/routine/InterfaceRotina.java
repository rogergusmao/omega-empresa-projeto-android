package app.omegasoftware.pontoeletronico.routine;

import java.util.Date;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_DATETIME;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public abstract class InterfaceRotina {

	
	int TEMPO_MINIMO_MINUTOS_PROXIMA_TENTATIVA = 10;
	TIPO_DATETIME tipoDatetime;
	
	protected abstract InterfaceMensagem run(Context c);
	
	
	public InterfaceRotina(
			int TEMPO_MINIMO_MINUTOS_PROXIMA_TENTATIVA,
			TIPO_DATETIME tipoDatetime){
		this.TEMPO_MINIMO_MINUTOS_PROXIMA_TENTATIVA=TEMPO_MINIMO_MINUTOS_PROXIMA_TENTATIVA;
		this.tipoDatetime=tipoDatetime;
	}
	HelperDate hdUltimaAtualizacao = null;
	//Em caso de falha, su executa a cada 10 minutos. E em caso de sucesso, su no outro dia ele enviaru novamente
	public InterfaceMensagem runIfItsTime(Context c) {
		try {
			if(hdUltimaAtualizacao == null)
			hdUltimaAtualizacao = PontoEletronicoSharedPreference.getValorDateTime(
					c,
					tipoDatetime);
			Date now = new Date();
			long diff= hdUltimaAtualizacao != null ? hdUltimaAtualizacao.getAbsDiferencaEmMinutos(now) : -1;
			if (hdUltimaAtualizacao == null || diff > TEMPO_MINIMO_MINUTOS_PROXIMA_TENTATIVA ) {
//			if(true){
				try{
					InterfaceMensagem msg = run(c);
					
					return msg;	
				}finally{
					hdUltimaAtualizacao = new HelperDate();
					PontoEletronicoSharedPreference.saveValor(c, this.tipoDatetime, hdUltimaAtualizacao);
				}
			} else {
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.AGUARDANDO, "Ainda nuo deu o periodo de " + TEMPO_MINIMO_MINUTOS_PROXIMA_TENTATIVA + " minutos");
			}
		} catch (Exception e) {
			SingletonLog.insereErro(e, TIPO.UTIL_ANDROID);
			return new Mensagem(e);
		}
	}
	
	

}
