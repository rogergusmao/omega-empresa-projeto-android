package app.omegasoftware.pontoeletronico.routine;

import java.io.File;

import android.content.Context;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.file.CleanDir;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration.TIPO;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_DATETIME;
import app.omegasoftware.pontoeletronico.webservice.ServicosPontoEletronicoAnonimo;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;

public class RotinaCleanDir extends InterfaceRotina{

	public RotinaCleanDir(
			int TEMPO_MINIMO_MINUTOS_PROXIMA_TENTATIVA, 
			TIPO_DATETIME tipoDatetime) {
		super(TEMPO_MINIMO_MINUTOS_PROXIMA_TENTATIVA, tipoDatetime);

	}

	@Override
	protected InterfaceMensagem run(Context c) {
		
		
		OmegaFileConfiguration o = new OmegaFileConfiguration();
		CleanDir cd = new CleanDir();
		if(OmegaConfiguration.GRAVAR_LOG_MODO_PRIVATE){
			File dir= o.getDirPrivate(TIPO.LOG, c);
			cd.deleteOldFiles(dir, ServicosPontoEletronicoAnonimo.MAXIMO_KB_ENVIO_LOG_VIA_WIFI);
		}
		if(OmegaConfiguration.GRAVAR_LOG_SSD){
			
			String path = o.getPath(OmegaFileConfiguration.TIPO.LOG);
			cd.deleteOldFiles(path, ServicosPontoEletronicoAnonimo.MAXIMO_KB_ENVIO_LOG_VIA_WIFI);	
		}
		
		
		//fos = c.openFileOutput(nomeArquivo, Context.MODE_PRIVATE);
		
		
		return null;
	}

}
