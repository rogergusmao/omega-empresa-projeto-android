package app.omegasoftware.pontoeletronico.service;

import android.content.Context;
import android.content.Intent;
import app.omegasoftware.pontoeletronico.TabletActivities.SynchronizeReceiveActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.ContainerPrograma;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;

public class ControlerServicoInterno {
	//static Intent serviceSendPontoMarcado = null;
	//static Intent serviceSendTarefa = null;
	//static Intent serviceSendFileToServer = null;
	
//	static Intent serviceSendUsuarioMensagem = null;
	static Intent serviceCheckListAtiveServiceOfUser = null;
	static Intent serviceSynch = null;
	
	Context context;
	
	static ControlerServicoInterno controle = null;
	
	public static ControlerServicoInterno constroi(Context context){
		if(controle == null)
		controle = new ControlerServicoInterno(context);
		return controle;
	}
	
	public static ControlerServicoInterno getSingleton(){
		
		return controle;
	}
	
	private ControlerServicoInterno(Context context){
		this.context = context;
	}
	
	public static enum  TYPE_SERVICO {

//		SEND_FILE_TO_SERVER,
//		SEND_LOG_ERRO,
//		SEND_USUARIO_MENSAGEM,
		CHECK_LIST_ATIVE_SERVICE_OF_USER,
//		SEND_TAREFA,
		SYNCH
	};

	public boolean stopService(TYPE_SERVICO pTypeService){
		try{
			switch (pTypeService) {
			
			case CHECK_LIST_ATIVE_SERVICE_OF_USER:
				if(serviceCheckListAtiveServiceOfUser != null){
					context.stopService( serviceCheckListAtiveServiceOfUser);
					serviceCheckListAtiveServiceOfUser = null;
				}
				break;
//			case SEND_TAREFA:
//				
//				if(serviceSendTarefa != null ){
//					activity.stopService(serviceSendTarefa);
//					serviceSendTarefa = null;
//				}
//				break;
//			case SEND_FILE_TO_SERVER:
//				if(serviceSendPontoMarcado != null ){
//					activity.stopService(serviceSendPontoMarcado);
//					serviceSendPontoMarcado = null;
//				}
//				break;
//			
//			case SEND_USUARIO_MENSAGEM:
//				if(serviceSendFileToServer != null){
//					activity.stopService( serviceSendFileToServer);
//					serviceSendFileToServer = null;
//				}
//				break;
			case SYNCH:
				
				if(serviceSynch != null){
					context.stopService( serviceSynch);
					
					serviceSynch = null;
				}
				
				break;
				default: break;
				
				}
			return true;
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			return false;
		}
		
	}
	
	public void startService(TYPE_SERVICO pTypeService){
	
		switch (pTypeService) {
		case CHECK_LIST_ATIVE_SERVICE_OF_USER:
			if(serviceCheckListAtiveServiceOfUser == null){
				int vVetorIdServicoPermitido[] = ContainerPrograma.getVetorIdServicoPermissaoOnline();
				if(vVetorIdServicoPermitido != null){
					serviceCheckListAtiveServiceOfUser = new Intent(context, ServiceCheckListAtiveServiceOfUser.class);
					context.startService(serviceCheckListAtiveServiceOfUser);
				}
			}
			break;
//
//		
//		case SEND_FILE_TO_SERVER:
//			if(serviceSendFileToServer == null && 
//					ContainerPrograma.isServicoPermitido(ServiceSendFileToServer.ID)){
//				serviceSendFileToServer = new Intent(activity, ServiceSendFileToServer.class);
//				activity.startService(serviceSendFileToServer);
//			}
//			break;
//		case SEND_TAREFA:
//			if(serviceSendTarefa == null && 
//					ContainerPrograma.isServicoPermitido(ServiceSendTarefa.ID)){
//				serviceSendTarefa = new Intent(activity, ServiceSendTarefa.class);
//				activity.startService(serviceSendTarefa);
//			}
//			break;	
		case SYNCH:
			if(OmegaConfiguration.IS_SINCRONIZACAO_POR_TIMER
				&& SynchronizeReceiveActivityMobile.getSavedSincronizacaoAutomatica(context)){
					
				if(serviceSynch == null ){
					serviceSynch = new Intent(context, ServiceSynch.class);
					context.startService(serviceSynch);
				}
			}
			
			break;
//		case SEND_USUARIO_MENSAGEM:
//			if(serviceSendUsuarioMensagem == null && 
//					ContainerPrograma.isServicoPermitido(ServiceUsuarioMensagem.ID)){
//				serviceSendUsuarioMensagem = new Intent(activity, ServiceUsuarioMensagem.class);
//				activity.startService(serviceSendUsuarioMensagem);
//			
//			}
//			break;

		default:
			break;
		}
	}

	protected void startAllServices(){
		TYPE_SERVICO vVetorServico[] = TYPE_SERVICO.values();
		for (TYPE_SERVICO vTypeServico : vVetorServico) {
			startService(vTypeServico);
		}
	}
	
	public void stopOnlyServicosAutenticados(){
		stopService(TYPE_SERVICO.CHECK_LIST_ATIVE_SERVICE_OF_USER);
//		stopService(TYPE_SERVICO.SEND_FILE_TO_SERVER);
//		
//		stopService(TYPE_SERVICO.SEND_TAREFA);
//		stopService(TYPE_SERVICO.SEND_USUARIO_MENSAGEM);
		stopService(TYPE_SERVICO.SYNCH);
	}
	
	public void startAllServicesAutenticados(){
		
		startService(TYPE_SERVICO.CHECK_LIST_ATIVE_SERVICE_OF_USER);
//		startService(TYPE_SERVICO.SEND_FILE_TO_SERVER);
//		
//		startService(TYPE_SERVICO.SEND_TAREFA);
//		startService(TYPE_SERVICO.SEND_USUARIO_MENSAGEM);
		
		startService(TYPE_SERVICO.SYNCH);
		
		
	}
	
	public void startAllServicesNaoAutenticados(){

//		startService(TYPE_SERVICO.SEND_LOG_ERRO);
	}
	
	public void stopAllServices(){
		TYPE_SERVICO vVetorServico[] = TYPE_SERVICO.values();
		for (TYPE_SERVICO vTypeServico : vVetorServico) {
			stopService(vTypeServico);
		}
	}

}
