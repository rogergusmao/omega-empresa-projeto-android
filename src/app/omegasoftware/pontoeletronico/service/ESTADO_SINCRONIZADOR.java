package app.omegasoftware.pontoeletronico.service;

import android.content.Context;
import android.content.Intent;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;

public enum ESTADO_SINCRONIZADOR{
	
	enviando(1, R.drawable.sincronizador_enviando),
	parado(2, R.drawable.sincronizador_parado),
	recebendo(3, R.drawable.sincronizador_recebendo),
	inativo(4, R.drawable.sincronizador_inativo),;
	
	static ESTADO_SINCRONIZADOR estadoSincronizador = ESTADO_SINCRONIZADOR.inativo;
	
	int drawable;
	int id;
	
	public int getId(){ return id;}
	public int getDrawable(){ return drawable;}
	ESTADO_SINCRONIZADOR(int id, int drawable){
		this.id = id;
		this.drawable = drawable;
		
	}
	public static ESTADO_SINCRONIZADOR getEstadoAtual(){
		return estadoSincronizador ;
	}
	
	public static void setEstado(ESTADO_SINCRONIZADOR estado){
		
		estadoSincronizador = estado;
	}
	
	public static ESTADO_SINCRONIZADOR getEstado(int id){
		ESTADO_SINCRONIZADOR[] vetor = ESTADO_SINCRONIZADOR.values();
		for(int i  = 0 ;  i < vetor.length; i ++){
			if(vetor[i].getId() == id) return vetor[i];
		}
		return null;
	}
	
	public static void sendServiceStateMessage(Context context, ESTADO_SINCRONIZADOR estado)
	{
		if(estadoSincronizador != estado){
			ESTADO_SINCRONIZADOR.setEstado(estado);
			
			Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_ESTADO_SINCRONIZADOR);
			
			updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ESTADO_SINCRONIZADOR, estado.getId());
			
			context.sendBroadcast(updateUIIntent);	
		}
	}
	
	
}