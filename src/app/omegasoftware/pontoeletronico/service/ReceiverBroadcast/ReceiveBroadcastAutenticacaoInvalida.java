package app.omegasoftware.pontoeletronico.service.ReceiverBroadcast;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;

public class ReceiveBroadcastAutenticacaoInvalida extends BroadcastReceiver {
	
	public static void sendBroadCast(Context c)
	{
		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_AUTENTICACAO_INVALIDA);
		c.sendBroadcast(updateUIIntent);
	}
	
	private void registerActivity(){
		IntentFilter uiUpdateIntentFilter = new IntentFilter(OmegaConfiguration.REFRESH_AUTENTICACAO_INVALIDA);
		try{
			a.registerReceiver(this, uiUpdateIntentFilter);	
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SERVICO_ANDROID);
		}	
	}

	Activity a;
	public ReceiveBroadcastAutenticacaoInvalida(Activity a){
		this.a=a;
		registerActivity();
	}
//	public static final String TAG = "ReceiveRastreamentoUsuarioIntentsReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {

		//Log.d(TAG,"onReceive(): Intent received - " + intent.getAction());
		if(intent != null){
			if(intent.getAction().equals(OmegaConfiguration.REFRESH_AUTENTICACAO_INVALIDA)) {
				
				a.showDialog(OmegaConfiguration.FORM_DIALOG_AUTENTICACAO_INVALIDA);
			} 

		}
	}
	
	
}