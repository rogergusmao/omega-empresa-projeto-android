package app.omegasoftware.pontoeletronico.service.ReceiverBroadcast;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;

public class ReceiveBroadcastSincronizador extends BroadcastReceiver {
	
	Handler handler;
	Activity a;
	
	public static void sendBroadCast(Context c, String proximaData)
	{
		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SINCRONIZADOR_STATUS);
		if(proximaData != null)
		updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATA_EXIBIR, proximaData);
		c.sendBroadcast(updateUIIntent);
	}
	
	private void registerActivity(){
		IntentFilter uiUpdateIntentFilter = new IntentFilter(OmegaConfiguration.REFRESH_SINCRONIZADOR_STATUS);
		try{
			
			a.registerReceiver(this, uiUpdateIntentFilter);	
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SERVICO_ANDROID);
		}	
	}
	public void unregisterActivity(){
		try{
			a.unregisterReceiver(this);
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SERVICO_ANDROID);
		}	
	}
	
	public ReceiveBroadcastSincronizador(Activity a, Handler handler){
		this.a=a;
		this.handler = handler;
		registerActivity();
	}
//	public static final String TAG = "ReceiveRastreamentoUsuarioIntentsReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {

		//Log.d(TAG,"onReceive(): Intent received - " + intent.getAction());
		if(intent != null){
			if(intent.hasExtra(OmegaConfiguration.SEARCH_FIELD_DATA_EXIBIR)){
				String strData = intent.getStringExtra(OmegaConfiguration.SEARCH_FIELD_DATA_EXIBIR);
				Message m = new Message();
				m.obj = strData;
				handler.sendMessage(m);	
			} else handler.sendEmptyMessage(0);
				
			
		}
	}
	
	
}