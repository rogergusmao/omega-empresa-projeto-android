package app.omegasoftware.pontoeletronico.service.ReceiverBroadcast;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.ConnectionServerInformationActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.SettingActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.http.HelperHttp;

public class ReceiverBroadcastServiceConnectionServerInformation extends BroadcastReceiver {
	
	public static final String TAG = "ReceiverBroadcastServiceConnectionServerInformation";
	
	private Button mServerButton;
//	private Button mHelpButton;
	private ImageButton mSettingButton, mSettingButton2;
	
	
	Activity activity;
	
	static Drawable iconOnline ;
	static Drawable iconOffline ;
	
	static boolean isTimerSet = false;
	
	boolean isServerOnline = false;
	String tagTelaPai;
	private class BackButtonListener implements OnClickListener
	{
		public void onClick(View v) {
			activity.onBackPressed();
		}
	}

	public ReceiverBroadcastServiceConnectionServerInformation(
			Activity pActivity, String pTagTelaPai){
		activity = pActivity;
		tagTelaPai = pTagTelaPai;
		try{
			if(iconOffline == null)
				iconOffline = activity.getResources().getDrawable(R.drawable.offline);
			if(iconOnline == null)
				iconOnline = activity.getResources().getDrawable(R.drawable.online);

		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SERVICO_ANDROID);
		}
		try{
			
			View vServer = pActivity.findViewById(R.id.server_button);
			if(vServer != null)
			this.mServerButton = (Button) vServer;

			
			
			View vSetting = pActivity.findViewById(R.id.setting_button);	
			if(vSetting != null)
			mSettingButton= (ImageButton) vSetting;	
		
			View vSetting2 = pActivity.findViewById(R.id.setting2_button);	
			if(vSetting2 != null)
			mSettingButton2 = (ImageButton) vSetting2;
			
			View viewBtTopo = pActivity.findViewById(R.id.btn_voltar);
			if(viewBtTopo != null){
				ImageButton voltarTopoButton = (ImageButton) viewBtTopo;
				voltarTopoButton.setOnClickListener(new BackButtonListener());	
			}
			CustomServiceListener listener = new CustomServiceListener();
	
			if(mServerButton != null){
				this.mServerButton.setOnClickListener(listener);
				initializeServerButton();
				IntentFilter uiUpdateIntentFilter = new IntentFilter(OmegaConfiguration.REFRESH_SERVICE_STATUS_SERVER);
				try{
					activity.registerReceiver(this, uiUpdateIntentFilter);	
				}catch(Exception ex){
					SingletonLog.insereErro(ex, TIPO.SERVICO_ANDROID);
				}	
			}
			if(mSettingButton != null)
				mSettingButton.setOnClickListener(listener);
			
			if(mSettingButton2 != null)
				mSettingButton2.setOnClickListener(listener);
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
			return;
		}
	}
	
	public void unregisterReceiverIfExist(){
		if(mServerButton != null){
			try{
				activity.unregisterReceiver(this);
			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.SERVICO_ANDROID);
			}
			
		}
	}
	
	
	private void initializeServerButton(){
		
		mServerButton.setBackgroundDrawable(iconOffline);
		
	}
	
	private class CustomServiceListener implements OnClickListener
	{
		public void onClick(View v) {
			Intent intent = null;
			switch (v.getId()) {
			case R.id.server_button:
				intent = new Intent(activity, ConnectionServerInformationActivityMobile.class);
				break;
			case R.id.setting2_button:
			case R.id.setting_button:
				intent = new Intent(activity, SettingActivityMobile.class);
				intent.putExtra(
						OmegaConfiguration.SEARCH_FIELD_TAG, 
						tagTelaPai);
				
				break;
			default:
				break;
			}

			if(intent != null)
			{
				activity.startActivity(intent);	
			}
		}
	}


	static int numberCheckOffline = 0;
	static final int LIMIT_CHECK_IS_OFFLINE = 2;
	@Override
	public void onReceive(Context context, Intent intent) {

		//Log.d(TAG,"onReceive(): Intent received - " + intent.getAction());
		if ( intent.getAction().equals(OmegaConfiguration.REFRESH_SERVICE_STATUS_SERVER)){

			Bundle vParameter = intent.getExtras();
			boolean statusServer = vParameter.getBoolean(OmegaConfiguration.SEARCH_FIELD_STATUS_SERVER);
			
			if(statusServer){
				numberCheckOffline = 0;
				mServerButton.setBackgroundDrawable(iconOnline);
			} else{
				numberCheckOffline += 1;
				if(numberCheckOffline >= LIMIT_CHECK_IS_OFFLINE)
					mServerButton.setBackgroundDrawable(iconOffline);
			}
			
			
		}
	}
}
