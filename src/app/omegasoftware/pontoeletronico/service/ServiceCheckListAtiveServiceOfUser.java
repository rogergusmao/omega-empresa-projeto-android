package app.omegasoftware.pontoeletronico.service;


import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.ReentrantLock;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.IBinder;
import app.omegasoftware.pontoeletronico.appconfiguration.ContainerPrograma;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.common.controler.ControlerUsuarioServico;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.service.ServiceMyPosition.ESTADO_RASTREADOR;

public class ServiceCheckListAtiveServiceOfUser extends Service  {

	//Constants
	protected static final String TAG = "ServiceCheckListAtiveServiceOfUser";
	Intent servicoRastreador;
	Intent serviceWifi;
	Intent serviceTiraFoto;
	
	ReceiveServiceStatusIntentsReceiver serviceReceiver;
	
	private Timer myChecaServicosAtivosTimer = new Timer();
	

	
	Context context ;
	

	static Integer vetorIdServicoExistente[] = new Integer[] {
//			ServiceTiraFoto.ID_INTERNO, 
//			ServiceTiraFoto.ID_EXTERNO, 
			ServiceMyPosition.ID,
			ServiceWifi.ID
	};
	
	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){

		return super.onStartCommand(intent, flags, startId);
	}


	public boolean inativaServicosSeNecessario(Integer idsServicosAtivos[]){
		boolean vValidade = false;

		
//		Percorre todos os servicos existentes,
		for (Integer idServico : vetorIdServicoExistente) {
			boolean servicoAtivo = false;
			if(idsServicosAtivos != null && idsServicosAtivos.length>0){
				for(int i = 0 ; i <idsServicosAtivos.length; i++ ){
					//Checa se o servico esta ativo
					if(idsServicosAtivos[i] == idServico) {
						servicoAtivo=true;
					}
				}
			}
			if(!servicoAtivo)
				stopServico(idServico);
		}

		return vValidade;
	}
	private void stopServico(Integer pIdServico){
		if(pIdServico == ServiceWifi.ID){

			//ESTADO_RASTREADOR.sendBroadCastEstado(this, ESTADO_RASTREADOR.inativo);
			if(serviceWifi != null){
				stopService(serviceWifi);
			}
		}
		else if(pIdServico == ServiceMyPosition.ID){
			
			ESTADO_RASTREADOR.sendBroadCastEstado(this, ESTADO_RASTREADOR.inativo);
			if(servicoRastreador != null){
				stopService(servicoRastreador);
			}
		} else if((pIdServico == ServiceTiraFoto.ID_INTERNO || 
				pIdServico == ServiceTiraFoto.ID_EXTERNO )){
			
			if(pIdServico == ServiceTiraFoto.ID_INTERNO){
				ServiceTiraFoto.IS_INTERNO_ATIVO = false;
				if(!ServiceTiraFoto.IS_EXTERNO_ATIVO && !ServiceTiraFoto.IS_INTERNO_ATIVO){
					if(serviceTiraFoto != null)
						stopService(serviceTiraFoto);
					serviceTiraFoto = null;	
				}
			}
			else if(pIdServico == ServiceTiraFoto.ID_EXTERNO){
				ServiceTiraFoto.IS_EXTERNO_ATIVO = false;
				if(!ServiceTiraFoto.IS_EXTERNO_ATIVO && !ServiceTiraFoto.IS_INTERNO_ATIVO){
					if(serviceTiraFoto != null)
						stopService(serviceTiraFoto);
					serviceTiraFoto = null;
				}
			}
		}
		
	}

	
	public void startAndSetAtivoServicos(Integer[] idsServicosAtivos){
		if(idsServicosAtivos== null) return ;
//		Percorre todos os servicos existentes,
		for (Integer idServico : idsServicosAtivos) {	
			try{
				switch (idServico) {
					case ServiceMyPosition.ID:
					case ServiceWifi.ID:
//						if(ContainerPrograma.isServicoPermitido(ServiceMyPosition.ID)){
							if(serviceWifi== null)
								serviceWifi = new Intent(this, ServiceWifi.class);
						serviceWifi.putExtra(OmegaConfiguration.SEARCH_FIELD_USUARIO, OmegaSecurity.getIdUsuario());

							startService(serviceWifi);
//						}


					if(ContainerPrograma.isServicoPermitido(ServiceMyPosition.ID)){
						if(servicoRastreador == null)
							servicoRastreador = new Intent(this, ServiceMyPosition.class);
						servicoRastreador.putExtra(OmegaConfiguration.SEARCH_FIELD_USUARIO, OmegaSecurity.getIdUsuario());
												
						startService(servicoRastreador);
					}
					break;
				case ServiceTiraFoto.ID_INTERNO :
				case ServiceTiraFoto.ID_EXTERNO :
					if(ContainerPrograma.isServicoPermitido(ServiceTiraFoto.ID_INTERNO) || 
							ContainerPrograma.isServicoPermitido(ServiceTiraFoto.ID_EXTERNO)){
						if(serviceTiraFoto == null){
							serviceTiraFoto = new Intent(this, ServiceTiraFoto.class);
							startService(serviceTiraFoto);
						}
					}
					
					if(idServico == ServiceTiraFoto.ID_INTERNO){
						ServiceTiraFoto.IS_INTERNO_ATIVO = true;
					} else if(idServico == ServiceTiraFoto.ID_EXTERNO){
						ServiceTiraFoto.IS_EXTERNO_ATIVO = true;
					}
					break;
				default:
					break;
				}
			
			
			} catch(Exception ex){
				SingletonLog.insereErro(ex, SingletonLog.TIPO.SERVICO_ANDROID);

			}		
		}		
	}

	@Override
	public void onDestroy(){
		try{
			if(myChecaServicosAtivosTimer!=null){
				myChecaServicosAtivosTimer.cancel();
			}
			
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SERVICO_ANDROID);
		}
		
		try{
			if(serviceReceiver != null){
				unregisterReceiver(this.serviceReceiver);
				serviceReceiver = null;
			}
			if(vetorIdServicoExistente != null) {
				for (int i = 0; i < vetorIdServicoExistente.length; i++) {
					stopServico(vetorIdServicoExistente[i]);
				}
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SERVICO_ANDROID);
		}
	}


	@Override
	public void onCreate() {
		super.onCreate();
		
		try{
			if (this.serviceReceiver == null){
				this.serviceReceiver = new ReceiveServiceStatusIntentsReceiver();
				IntentFilter serviceIntent = new IntentFilter(OmegaConfiguration.REFRESH_SERVICE_STATUS_SERVICO);
				registerReceiver(serviceReceiver, serviceIntent);
			}
			setTimer();
			
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		}
		
	}
	
	public static boolean BLOCK_CHECK = false;
	
	static ReentrantLock reentrantLock = new ReentrantLock();
	
	
	public class ServicoAtivoTimerTask extends TimerTask{
		Context context;
		public ServicoAtivoTimerTask(Context pContext){
			super();
			context = pContext;
		}
		Integer vetorIdServicoAntigoNoBanco[] = null;
		
		@Override
		public void run() {
			if (reentrantLock.tryLock()){
				DatabasePontoEletronico db=null;
				try{
					if(db == null)
						db = new DatabasePontoEletronico(context);
					
					//		Se nao estiver online,
					
//					boolean houveModificacao = false;
					
					
					//					Caso tenha tido alguma mudanca no servico por parte do usuario, e esta nao tenha
					//					sido atualizada pelo fato do servidor esta offline, este procedimento impede que
					//					o satus do servico antigo sobresescreva o status do servico atual escolhido pelo usuario
					//					Logo: quando fizer a requisicao no servidor para saber os servicos que estao ativos do 
					//					usuario em questao, estes estarao atualizados
					
					
					Integer idsServicosAtivos[] = ControlerUsuarioServico.getListaIdServicoAtivoNoStatus(context, db, OmegaSecurity.getIdUsuario(), true);

					//Inativa os servicos e deleta se necessario da base local
					inativaServicosSeNecessario(idsServicosAtivos);
					//Ativa os servicos e adiciona se necessario na base local
					startAndSetAtivoServicos(idsServicosAtivos);			

				}catch(Exception ex){
					SingletonLog.insereErro(ex, TIPO.PAGINA);
				} finally{
					if(db != null)
					db.close();
					db=null;
					
					reentrantLock.unlock();
				}	
			}
		}
	}

	private void setTimer(){

		//Starts reporter timer
		this.myChecaServicosAtivosTimer.scheduleAtFixedRate(
				new ServicoAtivoTimerTask(this),
				OmegaConfiguration.SERVICE_CHECK_LIST_ATIVE_SERVICE_OF_USER_INITIAL_DELAY,
				OmegaConfiguration.SERVICE_CHECK_LIST_ATIVE_SERVICE_OF_USER_INTERVAL);

	}
	
	



	public static Integer[] getListIdServicoAtivoOnline(Context pContext, Database pDatabase, String pIdUsuario)
	{

//		Integer vVetorIdServico[] = null;


//		String vResultPost = HelperHttpPost.getConteudoStringPost(
//				pContext, 
//				OmegaConfiguration.URL_REQUEST_GET_LISTA_SERVICO_ATIVO(), 
//				new String[]{"usuario", "corporacao", "id_corporacao", "senha", "p_usuario"}, 
//				new String[]{OmegaSecurity.getIdUsuario(), OmegaSecurity.getCorporacao(), OmegaSecurity.getIdCorporacao(), OmegaSecurity.getSenha(), pIdUsuario});
//		if(vResultPost == null) return null;
//		else if (vResultPost.length() == 0 ) return new Integer[]{};
//		String vVetorStrIdServicoAtivo[] = null;
//		vVetorStrIdServicoAtivo = vResultPost.split( OmegaConfiguration.DELIMITER_QUERY_UNIQUE_COLUNA);
//		if(vVetorStrIdServicoAtivo == null) return new Integer[]{};
//		else if(vVetorStrIdServicoAtivo.length == 0) return new Integer[]{};
//		
//		vVetorIdServico = new Integer[vVetorStrIdServicoAtivo.length];
//		int i = 0 ;
//		for (String vStrIdServicoAtivo : vVetorStrIdServicoAtivo) {
//			vVetorIdServico[i] = HelperInteger.parserInteger(vStrIdServicoAtivo);
//			i += 1;
//		}
//		return vVetorIdServico;
		return null;
	}
	
	
	public static boolean checaIntegridadeDeServicosDoUsuario(Context pContext, Database pDatabase, String pIdUsuario)
	{


//			String vResultPost = HelperHttpPost.getConteudoStringPost(
//					pContext, 
//					OmegaConfiguration.URL_REQUEST_CHECA_INTEGRIDADE_DE_SERVICOS_DO_USUARIO(), 
//					new String[]{"usuario", "corporacao", "id_corporacao", "senha", "p_usuario", "imei"}, 
//					new String[]{OmegaSecurity.getIdUsuario(), OmegaSecurity.getCorporacao(), OmegaSecurity.getIdCorporacao(), OmegaSecurity.getSenha(), pIdUsuario, HelperPhone.getIMEI()});
//			if(vResultPost!= null && vResultPost.compareTo("TRUE") == 0){
//				return true;
//			}else return false;
		return false;
		
		
		
	}

	private class ReceiveServiceStatusIntentsReceiver extends BroadcastReceiver {

//		public static final String TAG = "ReceiveRastreamentoUsuarioIntentsReceiver";

		@Override
		public void onReceive(Context context, Intent intent) {

			//Log.d(TAG,"onReceive(): Intent received - " + intent.getAction());
			if(intent != null){
				if(intent.getAction().equals(OmegaConfiguration.REFRESH_SERVICE_STATUS_SERVICO)) {

					Bundle vParameter = intent.getExtras();
					if(vParameter != null){
						if(vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_IS_ATIVO) && 
								vParameter.containsKey(OmegaConfiguration.SEARCH_FIELD_SERVICO)){
							Boolean vIsAtivo = vParameter.getBoolean( OmegaConfiguration.SEARCH_FIELD_IS_ATIVO);
							Integer vIdServico = vParameter.getInt(OmegaConfiguration.SEARCH_FIELD_SERVICO);
							if(vIdServico != null && vIsAtivo != null){
								if(vIsAtivo)
									startAndSetAtivoServicos(new Integer[] {vIdServico});
								else
									inativaServicosSeNecessario(new Integer[] {vIdServico});
							}
						}

					}
				} 

			}
		}
	}
}

