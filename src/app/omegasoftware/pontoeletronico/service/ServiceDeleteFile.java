package app.omegasoftware.pontoeletronico.service;


import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaDeletarArquivo;

public class ServiceDeleteFile extends Service  {

	public static final String TAG = "ServiceDeleteFile";
	public static final int ID = 6;
	public static int LIMIT_FILE = 3;
	
	
	EXTDAOSistemaDeletarArquivo objDeletarArquivo;
	Database db=null;	
	private Timer sendData = new Timer();
	Context context ;
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	@Override
	public void onDestroy(){
		
		this.sendData.cancel();		
	}
	
	@Override
	public void onCreate() {

		super.onCreate();
//		
//		db = new DatabasePontoEletronico(this);
//		objDeletarArquivo = new EXTDAOSistemaDeletarArquivo(db);
//		ArrayList<Table> vList = objDeletarArquivo.getListTable();
//		if(vList != null && vList.size() >  0){
//			for (Table table : vList) {
//				
//				
//			}
//		}
		setTimer();
//		db.close();
	}
	
	private void sendFiles(){
		
	}
	
	private void setTimer()
	{
		//Starts reporter timer
		this.sendData.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				sendFiles();
			}
		},
		OmegaConfiguration.SERVICE_DELETE_FILE_INITIAL_DELAY,
		OmegaConfiguration.SERVICE_DELETE_FILE_TIMER_INTERVAL);
		//OmegaConfiguration.SERVICE_SEND_FILE_TO_SERVER_TIMER_INTERVAL
	}

}
