package app.omegasoftware.pontoeletronico.service;


import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioPosicao;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioPosicao.ContainerUsuarioPosicao;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.gpsnovo.ContainerLocalizacao;
import app.omegasoftware.pontoeletronico.gpsnovo.GPSControler;
import app.omegasoftware.pontoeletronico.webservice.ServicosPontoEletronico;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class ServiceMyPosition extends Service  {


	public enum ESTADO_RASTREADOR{
		inativo(1, R.drawable.rastreador_inativo),
		ativo(2, R.drawable.rastreador_ativo),
		parado(3, R.drawable.rastreador_parado);
		static ESTADO_RASTREADOR singletonEstado = inativo; 
		int drawable;
		int id;
		
		public int getId(){ return id;}
		public int getDrawable(){ return drawable;}
		ESTADO_RASTREADOR(int id, int drawable){
			this.id = id;
			this.drawable = drawable;
			
		}
		
		public static ESTADO_RASTREADOR getEstadoAtual(){
			return singletonEstado;
		}
		
		public static void setEstado(ESTADO_RASTREADOR est){
			singletonEstado = est;
		}
		
		public static ESTADO_RASTREADOR getEstado(int id){
			ESTADO_RASTREADOR[] vetor = ESTADO_RASTREADOR.values();
			for(int i  = 0 ;  i < vetor.length; i ++){
				if(vetor[i].getId() == id) return vetor[i];
			}
			return null;
		}
		
		
		public static void sendBroadCastEstado(Context context, ESTADO_RASTREADOR estado)
		{
			ESTADO_RASTREADOR.setEstado(estado);
			Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_ESTADO_RASTREADOR);
			
			updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ESTADO_RASTREADOR, estado.getId());
			
			context.sendBroadcast(updateUIIntent);
		}

	}
	
	
	//Constants
	public static final int ID = 2;
	
	public static final String TAG = "ServiceMyPosition";
	public static final String LIMIT_TUPLA_TO_SEND = "100";
	private Timer myPositionTimer = new Timer();
	
//	VeiculoGPSLocationListener myPositionInDatabase;
	//Download button
	
	
	
	

	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		int vReturn = super.onStartCommand(intent, flags, startId);
		
		return vReturn; 
	}
	
	
	@Override
	public void onDestroy(){
		
		this.myPositionTimer.cancel();
		super.onDestroy();
	}
	
	
	@Override
	public void onCreate() {
		
		super.onCreate();
		
//		myPositionInDatabase = new VeiculoGPSLocationListener(this);
		setTimer();

	}

	public void sendDataOfUsuarioPosicao(){
//		if(!HelperHttp.isServerOnline(this)) return;
		Database db = null;
		try{
			db = new DatabasePontoEletronico(this);
			if(!db.isDatabaseCreated())return;
//			db.openIfNecessary();
			EXTDAOUsuarioPosicao vObjUsuarioPosicao = new EXTDAOUsuarioPosicao(db);
			ContainerUsuarioPosicao containerUsuarioPosicao = vObjUsuarioPosicao.getContainerUsuarioPosicaoDoBanco(LIMIT_TUPLA_TO_SEND);
			
			if(containerUsuarioPosicao != null && containerUsuarioPosicao.getCount() > 0 ){
				ServicosPontoEletronico spe = new ServicosPontoEletronico(this);
				Integer res = spe.rastreamento(containerUsuarioPosicao.getStrTotalTupla());
				
				if(res != null && res == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()){
					EXTDAOUsuarioPosicao.deleteAllRegisters(db, containerUsuarioPosicao.getLastId());
				}
			}
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SERVICO_ANDROID);
		}finally{
			if(db != null) db.close();
		}
		
	}
//	

	private void setTimer(){
		//Starts reporter timer
		this.myPositionTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				enviarDadosParaOServidor();
			}
		},
		OmegaConfiguration.SERVICE_POSITION_MY_CAR_INITIAL_DELAY,
		OmegaConfiguration.SERVICE_POSITION_MY_CAR_TIMER_INTERVAL);
	}


	private void enviarDadosParaOServidor()
	{
		try{
			ESTADO_RASTREADOR.sendBroadCastEstado(this, ESTADO_RASTREADOR.ativo);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				SingletonLog.insereErro(e, TIPO.SERVICO_ANDROID);
			}
			sendDataOfUsuarioPosicao();
			 
//			if(GPSControler.hasLocation()){
//				ContainerLocalizacao vContainer = GPSControler.getLocalizacao();
//				Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SERVICE_MY_POSITION_OTHER_CAR);
//				Integer vLatitude = vContainer.getLatitude();
//				Integer vLongitude  = vContainer.getLongitude();
//				
//				HelperDate vDate = new HelperDate();
//				String vDateTime = vDate.getDateAndTimeDisplay();
//				updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_LATITUDE, vLatitude);
//				updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_LONGITUDE, vLongitude);
//				updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_VELOCIDADE, vContainer.getVelocidade());
//				updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATE_TIME, vDateTime);
//				sendBroadcast(updateUIIntent);
//			}
			ESTADO_RASTREADOR.sendBroadCastEstado(this, ESTADO_RASTREADOR.parado);
		}catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SERVICO_ANDROID);
		}
	}
	
}
