package app.omegasoftware.pontoeletronico.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.IBinder;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.http.HelperHttpPostReceiveFile;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;

public class ServiceReceiveFileOfServer extends Service  {

	//Constants
	public static final String TAG = "ServiceReceiveFileOfServer";
	
	Context context ;
	BroadcastReceiverServiceReceiveFileOfServer serviceReceiver;
	
	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){

//		Bundle vParams = intent.getExtras();
		return super.onStartCommand(intent, flags, startId);
	}
	
	
	@Override
	public void onDestroy(){
		unregisterReceiver(this.serviceReceiver);
		
		super.onDestroy();
	}
	
	
	@Override
	public void onCreate() {

		super.onCreate();
		if (this.serviceReceiver == null)
			this.serviceReceiver = new BroadcastReceiverServiceReceiveFileOfServer();
				
		IntentFilter uiUpdateIntentFilter2 = new IntentFilter(OmegaConfiguration.REFRESH_SERVICE_LAST_USUARIO_FOTO);
		registerReceiver(this.serviceReceiver, uiUpdateIntentFilter2);
	}
	
	private Boolean receiveFile(String pId, String pDatatime){
		try{
			Boolean validade = null;
//			Boolean validade = InterfaceMensagem.validaChamadaWebservice( 
//					HelperHttpPostReceiveFile.DownloadFileFromHttpPost(
//					this, 
//					OmegaConfiguration.URL_REQUEST_RECEIVE_FILE(), 
//					pId + ".jpg", 
//					new String[]{"usuario_foto"}, 
//					new String[]{pId}));
		
			
//			HelperHttpPost.saveFileHttpPostData(
//					this, 
//					OmegaConfiguration.URL_REQUEST_RECEIVE_FILE, 
//					pId + ".jpg", 
//					new String[]{"usuario_foto"}, 
//					new String[]{pId});
			if(validade)
				sendBroadcastToActivityMonitoramento(pId, pDatatime);
			
			return validade != null && validade ? true : false;
		} catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SERVICO_ANDROID);
			return false;
		}
	}
	

	//
	//Envia a confirmacao de uma imagem baixada
	private void sendBroadcastToActivityMonitoramento(String pIdUsuarioFoto, String pDatetime)
	{
		//Log.d(TAG,"sendBroadcastToActivity() ");
		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_ACTIVITY_MONITORAMENTO_REMOTO_USUARIO);
		updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, pIdUsuarioFoto);
		updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_DATE_TIME, pDatetime);
		
		sendBroadcast(updateUIIntent);
	}
	
	
	//Receber a ordem para baixar a do imagem servidor
	public class BroadcastReceiverServiceReceiveFileOfServer extends BroadcastReceiver{


		public static final String TAG = "ServiceReceiveFileOfServerBroadcastReceiver";

		@Override
		public void onReceive(Context context, Intent intent) {

			//Log.d(TAG,"onReceive(): Intent received - " + intent.getAction());

			if(intent.getAction().equals(OmegaConfiguration.REFRESH_SERVICE_LAST_USUARIO_FOTO)) {
				Bundle vParameter = intent.getExtras();
				String vId = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_ID);
				String vDatetime = vParameter.getString(OmegaConfiguration.SEARCH_FIELD_DATE_TIME);
				receiveFile(vId, vDatetime);
			}
		}
	}

}
