package app.omegasoftware.pontoeletronico.service;


import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.container.ContainerRastreamento;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioFoto;

public class ServiceReceiveUsuarioPosicaoRastreamento extends Service {

	//Constants
	public static final int ID = 4;
	public static final String TAG = "ServiceReceiveUsuarioPosicaoRastreamento";
	
	private Timer receiveServiceReceiveUsuarioPosicaoTimer = new Timer();
	String idUsuario;
	ContainerRastreamento containerRastreamento;

	EXTDAOUsuarioFoto objUsuarioFoto;
	

	DatabasePontoEletronico db=null;
	
	int numeroTuplasCarregadasNoBanco = ContainerRastreamento.LIMIT_TUPLAS ;
	int totalTuplasASeremCarregadas = ContainerRastreamento.LIMIT_TUPLAS ;
	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}

	
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		if(intent != null){
			Bundle vParams = intent.getExtras();
			if(vParams != null){
				String vIdUsuarioBusca = null;
				String vIdVeiculoBusca= null;
				String vDataInicioBusca= null;
				String vHoraInicioBusca= null;
				String vDataFimBusca= null;
				String vHoraFimBusca= null;
				String vVelocidadeMinima= null;
				Boolean vSomenteComFoto= null;
				try {
					db = new DatabasePontoEletronico(this);
				} catch (OmegaDatabaseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				objUsuarioFoto = new EXTDAOUsuarioFoto(db);
				
				if(vParams.containsKey(OmegaConfiguration.SEARCH_FIELD_USUARIO))
					vIdUsuarioBusca = vParams.getString(OmegaConfiguration.SEARCH_FIELD_USUARIO);
				if(vParams.containsKey(OmegaConfiguration.SEARCH_FIELD_VEICULO))
					vIdVeiculoBusca = vParams.getString(OmegaConfiguration.SEARCH_FIELD_VEICULO);
				if(vParams.containsKey(OmegaConfiguration.SEARCH_FIELD_DATA_INICIO_TAREFA))
					vDataInicioBusca = vParams.getString(OmegaConfiguration.SEARCH_FIELD_DATA_INICIO_TAREFA);
				if(vParams.containsKey(OmegaConfiguration.SEARCH_FIELD_DATA_FIM_TAREFA))
					vDataFimBusca = vParams.getString(OmegaConfiguration.SEARCH_FIELD_DATA_FIM_TAREFA);
				
				if(vParams.containsKey(OmegaConfiguration.SEARCH_FIELD_HORA_INICIO_TAREFA))
					vHoraInicioBusca = vParams.getString(OmegaConfiguration.SEARCH_FIELD_HORA_INICIO_TAREFA);
				if(vParams.containsKey(OmegaConfiguration.SEARCH_FIELD_HORA_FIM_TAREFA))
					vHoraFimBusca = vParams.getString(OmegaConfiguration.SEARCH_FIELD_HORA_FIM_TAREFA);
				if(vParams.containsKey(OmegaConfiguration.SEARCH_FIELD_VELOCIDADE_MINIMA))
					vVelocidadeMinima = vParams.getString(OmegaConfiguration.SEARCH_FIELD_VELOCIDADE_MINIMA);
				
				if(vParams.containsKey(OmegaConfiguration.SEARCH_FIELD_SOMENTE_COM_FOTO))
					vSomenteComFoto = vParams.getBoolean(OmegaConfiguration.SEARCH_FIELD_SOMENTE_COM_FOTO);
				containerRastreamento = new ContainerRastreamento(
						this, 
						ContainerRastreamento.LIMIT_TUPLAS, 
						vIdUsuarioBusca, 
						vIdVeiculoBusca, 
						vDataInicioBusca, 
						vHoraInicioBusca, 
						vDataFimBusca, 
						vHoraFimBusca, 
						vVelocidadeMinima, 
						vSomenteComFoto);
					
			}
		}
		return START_STICKY;
	}
	
	
	@Override
	public void onDestroy(){
		
		if(this.db != null)
			this.db.close();
		
	}
	

	@Override
	public void onCreate() {

		super.onCreate();
		
		
		setTimer();
	}

	private void stopService(){
		stopSelf();
		db.close();
	}

	private void setTimer()
	{

		//Starts reporter timer
		this.receiveServiceReceiveUsuarioPosicaoTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				
				try {
					
					Integer vNumeroTuplasBaixadas = containerRastreamento.receiveRotaUsuario();
					db.close();
//					Se nao tiver mais que baixar tuplas
					if(vNumeroTuplasBaixadas < ContainerRastreamento.LIMIT_TUPLAS){
						receiveServiceReceiveUsuarioPosicaoTimer.cancel();
						sendBroadCastToRastrearVeiculoUsuarioMap(vNumeroTuplasBaixadas);
						stopService();
					} else {
						sendBroadCastToRastrearVeiculoUsuarioMap(vNumeroTuplasBaixadas);
					}
				} catch (Exception ex) {
					
					//if(ex != null)
					//	Log.e(TAG, ex.getMessage());
					//else Log.e(TAG, "Error desconhecido");
				}
			}
		},
		OmegaConfiguration.SERVICE_RECEIVE_USUARIO_POSICAO_INITIAL_DELAY,
		OmegaConfiguration.SERVICE_RECEIVE_USUARIO_POSICAO_TIMER_INTERVAL);
	}
	private void sendBroadCastToRastrearVeiculoUsuarioMap(int pTotalTuplasBaixadas)
	{
		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SERVICE_RECEIVE_USUARIO_RASTREAMENTO_POSICAO);
		updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_TOTAL_TUPLAS_BAIXADAS, pTotalTuplasBaixadas);
		sendBroadcast(updateUIIntent);
	}
}
