package app.omegasoftware.pontoeletronico.service;


import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaSincronizadorArquivo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaSincronizadorArquivo.ContainerSincronizaFile;
import app.omegasoftware.pontoeletronico.file.FileHttpPost;
import app.omegasoftware.pontoeletronico.file.FilesZipper;
import app.omegasoftware.pontoeletronico.file.HelperFile;
import app.omegasoftware.pontoeletronico.file.HelperZip;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;

public class ServiceSendFileToServer extends Service  {

	public static final String TAG = "ServiceSendFileToServer";
	public static final int ID = 6;
	public static int LIMIT_FILE_TO_ZIP = 3;
	public static int LIMIT_FILE_TO_UPLOAD = 2;
	
	EXTDAOSistemaSincronizadorArquivo objSincronizaFile;
	Database db=null;
	FileHttpPost filesHttpPost ;	
	private Timer sendFileToServerTimer = new Timer();
	Context context ;
	public String STR_LIMIT_FILE_QUERY ="";
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	@Override
	public void onDestroy(){
		
		this.sendFileToServerTimer.cancel();		
	}
	
	@Override
	public void onCreate() {

		super.onCreate();
		STR_LIMIT_FILE_QUERY = String.valueOf(LIMIT_FILE_TO_ZIP + LIMIT_FILE_TO_UPLOAD);
		OmegaFileConfiguration vConfiguration = new OmegaFileConfiguration();
		try {
			db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		objSincronizaFile = new EXTDAOSistemaSincronizadorArquivo(db);
		
//		filesHttpPost = new FileHttpPost(
//				OmegaConfiguration.URL_REQUEST_SEND_ZIP_FILE_PHOTO(),
//				vConfiguration.getPathFilePhotoToSend());
		
		setTimer();
		db.close();
	}
	
	private void sendFiles(){
		try{
			
			
			ContainerSincronizaFile vContainer = objSincronizaFile.getListLastFileToSend(STR_LIMIT_FILE_QUERY);
			
			if(vContainer != null){
				ArrayList<Boolean> listIsZip = vContainer.getListIsZip();
				ArrayList<String> listFile = vContainer.getListStrFile();
				ArrayList<String> listStrId = vContainer.getListStrId();
				ArrayList<File> listFileToUpload = new ArrayList<File>();
				
				for(int i = 0 ; i < listFile.size(); i++) {
					Boolean vIsZip = listIsZip.get(i);
					String vFileName = HelperFile.getNameOfFileInPath(listFile.get(i));
					
					String vId = listStrId.get(i);
					if(vIsZip){
						File vFile = new File(filesHttpPost.getStrPath() + vFileName);
						if(!vFile.exists())
							objSincronizaFile.remove(vId, false);
						else if(filesHttpPost.uploadFileAndRemove(vFileName) != null)
							objSincronizaFile.remove(vId, false);
					} else{
						
						File vFile = new File(filesHttpPost.getStrPath() +  vFileName);
						if(vFile.exists())
							listFileToUpload.add(vFile);
						
						objSincronizaFile.remove(vId, false);
					}
				}
				if(listFileToUpload.size() > 0 ){
					File zipFile = FilesZipper.getNewZipFile(filesHttpPost.getStrPath());
					File vVetorFileToUpload[] = new File[listFileToUpload.size()];
					listFileToUpload.toArray((File[])vVetorFileToUpload);
					HelperZip.zipVetorFile(vVetorFileToUpload, zipFile);
//					Se nao adicionar o arquivo, entao
					 String vResposta = filesHttpPost.uploadAndRemoveFile(zipFile);
					if(vResposta != null){
						objSincronizaFile.clearData();
						objSincronizaFile.setAttrValue(EXTDAOSistemaSincronizadorArquivo.ARQUIVO, zipFile.getName());
						objSincronizaFile.setAttrValue(EXTDAOSistemaSincronizadorArquivo.IS_ZIP_BOOLEAN, "1");
						objSincronizaFile.insert(false);
					}
				}
			}
			
//			ANTES
//			filesManager.uploadFiles(LIMIT_FILE_TO_ZIP, LIMIT_FILE_TO_UPLOAD, null);
			
		} catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.PAGINA);
		} finally{
			if(db != null)db.close();
		}
	}
	
	private void setTimer()
	{
		//Starts reporter timer
		this.sendFileToServerTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				sendFiles();
			}
		},
		60000,
		120000);
		//OmegaConfiguration.SERVICE_SEND_FILE_TO_SERVER_TIMER_INTERVAL
	}

}
