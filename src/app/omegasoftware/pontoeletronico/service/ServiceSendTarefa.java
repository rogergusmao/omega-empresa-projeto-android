package app.omegasoftware.pontoeletronico.service;


import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;

public class ServiceSendTarefa extends Service  {

	//Constants
	protected static final String TAG = "ServiceSendTarefa";
	public static final int ID = 12; 
	private Timer sendDados = new Timer();
	Database db ;
	//Download button
	
	Context context ;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {

		super.onCreate();
		
		try {
			db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.close();
		setTimer();
	}

	
	@Override
	public void onDestroy(){
		
		try {
			if(sendDados != null)
			sendDados.cancel();
			
		} catch (Throwable e) {
			
			e.printStackTrace();
		}
		super.onDestroy();
	}


	private void setTimer()
	{

		//Starts reporter timer
		this.sendDados.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				launchUploaderTask();
			}
		},
		OmegaConfiguration.SERVICE_SEND_TAREFA_INITIAL_DELAY,
		OmegaConfiguration.SERVICE_SEND_TAREFA_TIMER_INTERVAL);
	}


	public void launchUploaderTask()
	{
		
		if(loadData(db, this, false)){
			sendBroadCastPontoMarcado();
		}
	}
	
	
	private static boolean isSincronizando = false;
	
	public static boolean loadData( Database db, Context pContext, boolean pIsManual) {
		
			if(isSincronizando) return true;
			isSincronizando = true;
			try{
//				if(!SincronizacaoPontoActivityMobile.getSavedSincronizacaoAutomatica(pContext) && !pIsManual) return true;
				//Envia as acoes realizadas para depois atualizar as tarefas
				//Caso a propria tarefa tenha sido atualizada, entao a sua atualizacao ira ocorrer no proximo procedimento
				loadAcaoTarefa(pContext, db);
//					SynchronizeFastReceiveDatabase.procedureRun(pContext, EXTDAOTarefa.TABELAS_RELACIONADAS, true);

			return true;
		} catch(Exception ex){
			return false;
		} finally{
			isSincronizando = false;
		}
	}

	public static boolean loadAcaoTarefa(Context pContext, Database db){
//		EXTDAOTarefaAcao vObj = new EXTDAOTarefaAcao(db);
//		vObj.setAttrValue(EXTDAOTarefaAcao.IS_ENTRADA_BOOLEAN, "1");
//		ArrayList<Table> vListTupla = vObj.getListTable();
//		if(vListTupla != null && vListTupla.size() > 0 ){
//			for (Table tupla : vListTupla) {
//				String tarefaId = tupla.getStrValueOfAttribute(EXTDAOTarefaAcao.TAREFA_ID_INT);
				
//				String vRetorno2 =  HelperHttpPost.getConteudoStringPost(
//						pContext, 
//						OmegaConfiguration.URL_REQUEST_CHECK_SET_DATA_INICIO_TAREFA(), 
//						new String[] {
//								"usuario",
//								"id_corporacao",
//								"corporacao",
//								"senha",
//								"imei",
//								"id_programa",
//								"tarefa",
//								"data",
//								"hora"}, 
//						new String[] {
//								OmegaSecurity.getIdUsuario(),
//								OmegaSecurity.getIdCorporacao(),
//								OmegaSecurity.getCorporacao(),
//								OmegaSecurity.getSenha(),
//								HelperPhone.getIMEI(),
//								ContainerPrograma.getIdPrograma(), 
//								String.valueOf(tarefaId),
//								HelperDate.getDateAtualFormatada(),
//								HelperDate.getTimeAtualFormatada()});
//				if(vRetorno2 != null){
//					try{
//						tupla.remove(false);	
//					}catch(Exception ex){
//						SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
//					}
//					
//					return true;
//				} else return false;
//				return false;
//			}
//			return true;
//		} else return true;
		return false;
	}
	
	private void sendBroadCastPontoMarcado()
	{

		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_TAREFA_ATUALIZADA);
		sendBroadcast(updateUIIntent);
	}

}
