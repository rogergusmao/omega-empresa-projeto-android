package app.omegasoftware.pontoeletronico.service;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.ReentrantLock;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.SynchronizeReceiveActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.log.BOLogApp;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_DATETIME;
import app.omegasoftware.pontoeletronico.routine.RotinaCleanDir;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiveBroadcastSincronizador;

public class ServiceSynch extends Service {

	public enum TIPO_SYNC {
		ESPELHO, FAST_SYNC
	}

	// Constants
	public static final String TAG = "ServiceSynch";

	SincronizadorEspelho syncEspelho;

	private Timer synchTimer = new Timer();
	RotinaCleanDir rotinaCleanDirLog = new RotinaCleanDir(31, TIPO_DATETIME.ULTIMO_CLEAN_DIR_LOG);

	ReentrantLock re = new ReentrantLock();
	ReentrantLock reRotina = new ReentrantLock();
//	MyIntentsReceiver myIntentReceiver = null;
	
	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}

	class ThreadPerTaskExecutor implements Executor {
		public void execute(Runnable r) {
			new Thread(r).start();

		}
	}


	public class TimerSynch extends TimerTask {
		Context context;
		BOLogApp bo = null;

		public TimerSynch(Context pContext) {
			context = pContext;
			bo = new BOLogApp();
		}

		@Override
		public void run() {
			if (!SynchronizeReceiveActivityMobile.getSavedSincronizacaoAutomatica(context)) {
				ESTADO_SINCRONIZADOR.sendServiceStateMessage(context, ESTADO_SINCRONIZADOR.inativo);
				return;
			}

			if (syncEspelho == null) {
				try {
					syncEspelho = SincronizadorEspelho.getInstance(ServiceSynch.this);
				} catch (Exception ex) {
					syncEspelho = null;
					SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
					return;
				}
			}
			if (syncEspelho != null) {
				long timespanNexTime = super.scheduledExecutionTime();

				if (re.tryLock()) {
					try {
						Date d = new Date(timespanNexTime);
						String strNextDate = HelperDate.getStringDateTime(ServiceSynch.this.getApplicationContext(), d);

						ReceiveBroadcastSincronizador.sendBroadCast(context, strNextDate);
						// new ThreadPerTaskExecutor().execute( new Runnable() {
						// public void run() {

						try {
							syncEspelho.executa(false, false);
						} catch (Exception ex) {
							SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
						}

						// }
						// });

					} finally {
						re.unlock();
					}
				}

			}
			if (reRotina.tryLock()) {
				try {
					if (OmegaConfiguration.CLEAR_LOG) {
						rotinaCleanDirLog.runIfItsTime(ServiceSynch.this);
					}

					bo.enviaLogPeriodicamente(ServiceSynch.this, 1000);
				} finally {
					reRotina.unlock();
				}
			}

		}
	}

	long lastInterval = -1;

	private void changeTimer(long initialDelayMiliseconds, long intervalMiliseconds) {
		// nao altera se o intervalo for o mesmo do atual
		if (lastInterval == intervalMiliseconds) {
			return;
		}

		if (lastInterval > 0) {
			this.synchTimer.cancel();
			this.synchTimer = new Timer();
		}
		lastInterval = intervalMiliseconds;
		// Starts reporter timer
		this.synchTimer.scheduleAtFixedRate(new TimerSynch(this), initialDelayMiliseconds, intervalMiliseconds);
	}

	private void setTimer() {
		// Starts reporter timer
		changeTimer(OmegaConfiguration.SERVICE_SYNCH_INITIAL_DELAY, OmegaConfiguration.SERVICE_SYNCH_TIMER_INTERVAL);
	}

	@Override
	public void onDestroy() {

		stopForeground(true);
		this.synchTimer.cancel();
		this.lastInterval = -1;
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		init();
		setTimer();
		
//		myIntentReceiver = new MyIntentsReceiver();
		return (START_REDELIVER_INTENT);
	}
	
	
	public void init(){
		String desc = null;
		if (!SynchronizeReceiveActivityMobile.getSavedSincronizacaoAutomatica(this)) {
			desc = getResources().getString(R.string.sincronizador_ligado);
		} else {
			desc = getResources().getString(R.string.sincronizador_desligado);
		}
		
		
//		Notification note = new Notification(R.drawable.icon, desc, System.currentTimeMillis());

		Intent i = new Intent(this, SynchronizeReceiveActivityMobile.class);
		i.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS | Intent.FLAG_ACTIVITY_NO_HISTORY
				| Intent.FLAG_ACTIVITY_NEW_TASK);

		PendingIntent pi = PendingIntent.getActivity(this, 0, i, 0);
		
		

		Notification note = new Notification.Builder(this)
				.setSmallIcon(R.drawable.icon)
				.setContentTitle(getResources().getString(R.string.app_name))
				.setContentInfo(desc)
				.setContentIntent(pi)				
				.getNotification();
				

		

		startForeground(1338, note);
	}

	@Override
	public void onCreate() {
		super.onCreate();

	}

	public static void broadcastAtualizaNotificacao(Context context){
		
		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_NOTIFICACAO_APP);
		context.sendBroadcast(updateUIIntent);
	}
	
	

	
//	private void registerIntentFilter(){
//		if(myIntentReceiver == null)
//			myIntentReceiver = new MyIntentsReceiver();
//		//Starts the TruckTrackerService
//		IntentFilter serviceListCarPositionIntent = new IntentFilter(BibliotecaNuvemConfiguracao.REFRESH_SERVICE.OPERACAO_SISTEMA_MOBILE.toString());
//		registerReceiver(myIntentReceiver, serviceListCarPositionIntent);
//	}


//	private class MyIntentsReceiver extends BroadcastReceiver {
//
//		public static final String TAG = "MyIntentsReceiver";
//
//		@Override
//		public void onReceive(Context context, Intent intent) {
//			if(OmegaSecurity.isAutenticacaoRealizada()){
//				
//			}
//			//RotinaSincronizador.getStateSincronizador(context, )
//			
//		}
//	}
}
