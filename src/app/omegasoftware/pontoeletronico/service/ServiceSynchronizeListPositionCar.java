package app.omegasoftware.pontoeletronico.service;


import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;

public class ServiceSynchronizeListPositionCar extends Service  {

	//Constants
	protected static final String TAG = "ServiceSynchronizeListPositionCar";
	
	private Timer positionOtherCarTimer = new Timer();
	String veiculoUsuario;
	TextView conteudoTabelaTextView; 
	//Download button

	Context context ;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {

		super.onCreate();

		setTimer();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){

		Bundle vParams = intent.getExtras();
		veiculoUsuario = vParams.getString(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO);
		
		return super.onStartCommand(intent, flags, startId);
	}
	
	@Override
	public void onDestroy(){
		
		try {
			
			this.positionOtherCarTimer.cancel();
			super.onDestroy();
		} catch (Throwable e) {
			
			e.printStackTrace();
		}
	}


	private void setTimer()
	{

		//Starts reporter timer
		this.positionOtherCarTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				launchUploaderTask();
			}
		},
		OmegaConfiguration.SERVICE_POSITION_OTHER_CAR_INITIAL_DELAY,
		OmegaConfiguration.SERVICE_POSITION_OTHER_CAR_TIMER_INTERVAL);
	}


	public void launchUploaderTask()
	{
	
		if(loadData() > 0){
			sendBroadCastToVeiculoMapActivity();
		}
	}
	
	
	
	
	public int loadData() {
		
		try{
//			int numberRowsAffected = synchronizeReceiveDatabase.receiveData(EXTDAOVeiculoUsuarioPosicao.NAME);
//			return numberRowsAffected;
//			String vResultPost = HelperHttpPost.getConteudoStringPost(
//					this, 
//					OmegaConfiguration.URL_REQUEST_GET_LISTA_POSICAO_VEICULO_DISPONIVEL(), 
//					new String[]{
//							"usuario", 
//							"id_corporacao",
//							"corporacao", 
//							"senha", 
//							"veiculo_usuario"}, 
//					new String[]{
//							OmegaSecurity.getIdUsuario(), 
//							OmegaSecurity.getIdCorporacao(),
//							OmegaSecurity.getCorporacao(), 
//							OmegaSecurity.getSenha(), 
//							this.veiculoUsuario});
//			
//			String vVetorVeiculoPosicao[] = vResultPost.split( OmegaConfiguration.DELIMITER_QUERY_UNIQUE_COLUNA);
//			return receiveListOfCarAndInsertInStatisList(vVetorVeiculoPosicao);
			return 0;

		} catch(Exception ex){
			SingletonLog.insereErro(ex, SingletonLog.TIPO.SERVICO_ANDROID);
			return 0;
		}
	}

	private int receiveListOfCarAndInsertInStatisList(String[] pVetorVeiculoPosicao){
		try{
		if(pVetorVeiculoPosicao == null) return 0;
		else if(pVetorVeiculoPosicao.length == 0) return 0;
		else{
//			MapaListMarkerActivity.listContainerMarcadorMapaCarros.clear();
			
			int vNumberOfInsert = 0 ;
			Drawable icone = this.getResources().getDrawable(R.drawable.car);
			
			for (String vToken : pVetorVeiculoPosicao) {
				String vVetorColumn[] = vToken.split(";");
				
				if(vVetorColumn.length == 5){
					
					
					String vStrLongitude = vVetorColumn[3];
					if(vStrLongitude == null) continue;
//					else vStrLongitude = vStrLongitude.replace(",", "."); 
					String vStrLatitude = vVetorColumn[4];
					
					if(vStrLatitude == null) continue;
//					else vStrLatitude = vStrLatitude.replace(",", ".");
					

//					GeoPoint vGeoPoint = HelperMapa.getGeoPoint(vStrLatitude, vStrLongitude);
					
//					OverlayItem vContainerMarcadorMapa = new OverlayItem(
//							vGeoPoint, 
//							HelperDate.getDataAtualFormatada(), 
//							HelperMapa.getTextComLatitudeELongitude(vGeoPoint));
					
//					VeiculoCustomOverlayItem vContainerMarcadorMapa = new VeiculoCustomOverlayItem( 
//							vGeoPoint, 
//							HelperDate.getDatetimeAtualFormatadaParaExibicao(this), 
//							HelperMapa.getTextComLatitudeELongitude(vGeoPoint), 
//							icone, 
//							vVetorColumn[2], 
//							vVetorColumn[0] + " " + vVetorColumn[1]);
					
//					vContainerMarcadorMapa.setMarker(icone);
//					MapaListMarkerActivity.listContainerMarcadorMapaCarros.add(vContainerMarcadorMapa);
					
					vNumberOfInsert += 1;
				}
				
			}
			return vNumberOfInsert;
		}
	} catch(Exception ex){
		//if(ex != null)
		//	Log.e(TAG, ex.getMessage());
		//else Log.e(TAG, "Error desconhecido");
		return 0;
	}
	}

	private void sendBroadCastToVeiculoMapActivity()
	{

		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SERVICE_LIST_POSITION_OTHER_CAR);
		sendBroadcast(updateUIIntent);
	}
	
//	private int receiveListOfCarAndInsertInDatabase(String[] pVetorVeiculoPosicao){
//		try{
//		if(pVetorVeiculoPosicao == null) return 0;
//		else if(pVetorVeiculoPosicao.length == 0) return 0;
//		else{
//			int vNumberOfInsert = 0 ;
//			EXTDAOUsuarioPosicao vObj = new EXTDAOUsuarioPosicao(this.db);
//			for (String vToken : pVetorVeiculoPosicao) {
//				String vVetorColumn[] = vToken.split(";");
//				
//				if(vVetorColumn.length == 5){
//					
//					vObj.clearData();
//					String vStrLongitude = vVetorColumn[3];
//					if(vStrLongitude == null) continue;
//					else vStrLongitude = vStrLongitude.replace(",", "."); 
//					String vStrLatitude = vVetorColumn[4];
//					if(vStrLatitude == null) continue;
//					else vStrLatitude = vStrLatitude.replace(",", ".");
//					
//					vObj.setAttrStrValue(EXTDAOUsuarioPosicao.DATA_DATE, vVetorColumn[0]);
//					vObj.setAttrStrValue(EXTDAOUsuarioPosicao.HORA_TIME, vVetorColumn[1]);
//					vObj.setAttrStrValue(EXTDAOUsuarioPosicao.VEICULO_USUARIO_ID_INT, vVetorColumn[2]);
//					vObj.setAttrStrValue(EXTDAOUsuarioPosicao.LONGITUDE_INT, vStrLongitude);
//					vObj.setAttrStrValue(EXTDAOUsuarioPosicao.LATITUDE_INT, vStrLatitude);
//					vObj.insert(false);
//					vNumberOfInsert += 1;
//				}
//				
//			}
//			return vNumberOfInsert;
//		}
//	} catch(Exception ex){
//		return 0;
//	}
//	}
	
}
