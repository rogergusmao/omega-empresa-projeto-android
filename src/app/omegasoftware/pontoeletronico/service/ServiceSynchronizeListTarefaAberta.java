package app.omegasoftware.pontoeletronico.service;

//public class ServiceSynchronizeListTarefaAberta extends Service  {

	//Constants
//	protected static final String TAG = "ServiceSynchronizeListTarefaAberta";
//
//	private DatabasePontoEletronico db=null;
//	
//	
//	
////	SynchronizeFastReceiveDatabase synchronizeReceiveDatabase;
//
//	private Timer positionTarefa = new Timer();
//	String veiculoUsuario;
//	TextView conteudoTabelaTextView; 
//	//Download button
//	String idTabelaSincronizador;
//	Context context ;
//
//	@Override
//	public IBinder onBind(Intent arg0) {
//		return null;
//	}
//
//	@Override
//	public void onCreate() {
//
//		super.onCreate();
//
//		try {
//			this.db = new DatabasePontoEletronico(this);
//		} catch (OmegaDatabaseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		idTabelaSincronizador = EXTDAOSistemaTabela.getIdSistemaTabela(db, EXTDAOTarefa.NAME);
//		
//		//v.getResources().getStringArray(R.array.receive_table_db)
////		synchronizeReceiveDatabase = new SynchronizeFastReceiveDatabase(this, -1, EXTDAOTarefa.TABELAS_RELACIONADAS);
//		
//
//		setTimer();
//		db.close();
//	}
//
//	@Override
//	public int onStartCommand(Intent intent, int flags, int startId){
//
//		Bundle vParams = intent.getExtras();
//		veiculoUsuario = vParams.getString(OmegaConfiguration.SEARCH_FIELD_VEICULO_USUARIO);
//
//		return super.onStartCommand(intent, flags, startId);
//	}
//
//	@Override
//	public void onDestroy(){
//
//		try {
//			this.db.close();
//			this.positionTarefa.cancel();
//			super.onDestroy();
//		} catch (Throwable e) {
//			
//			SingletonLog.insereErro(e, SingletonLog.TIPO.SERVICO_ANDROID);
//		}
//	}
//
//
//	private void setTimer()
//	{
//
//		//Starts reporter timer
//		this.positionTarefa.scheduleAtFixedRate(new TimerTask() {
//			@Override
//			public void run() {
//				reporterAction();
//			}
//		},
//		OmegaConfiguration.SERVICE_POSITION_TAREFA_INITIAL_DELAY,
//		OmegaConfiguration.SERVICE_POSITION_TAREFA_TIMER_INTERVAL);
//
//	}
//
//	private void reporterAction()
//	{
////		if(HelperHttp.isServerOnline(this))
////		{
//			launchUploaderTask();
////		}
//	}
//
//	public void launchUploaderTask()
//	{
//		if(loadData() > 0){
//			sendBroadCastToVeiculoMapActivity();
//		}
//	}
//
//
//
//
//	public Integer loadData() {
//		try{
//		//			Envio os dados da tabela Veiculo Posicao inseridos, no caso somentes as minhas posicoes
//		Integer vListIdTarefaMinha[] = MapaListMarkerActivity.getListIdTarefaAbertaPropria();
//		String vStrPostListTarefaMinha = HelperString.getStrSeparateByDelimiterColumnDatabase(vListIdTarefaMinha);
//		Integer vListIdTarefaAberta[] = MapaListMarkerActivity.getListIdTarefaAberta();
//		String vStrPostListTarefaAberta = HelperString.getStrSeparateByDelimiterColumnDatabase(vListIdTarefaAberta);
//		Integer vListIdTarefaExecucao[] = MapaListMarkerActivity.getListIdTarefaPropriaExecucao();
//		String vStrPostListTarefaExecucao = HelperString.getStrSeparateByDelimiterColumnDatabase(vListIdTarefaExecucao);
//		
//		String vResultPost =null;		
////			String vResultPost = HelperHttpPost.getConteudoStringPost(
////					this, 
////					OmegaConfiguration.URL_REQUEST_GET_LISTA_TAREFA(), 
////					new String[]{
////							"usuario",
////							"id_corporacao",
////							"corporacao",
////							"senha",
////							"sistema_tabela",
////							"veiculo_usuario", 
////							"tarefa_aberta", 
////					"tarefa_propria",
////					"tarefa_propria_execucao"}, 
////					new String[]{
////							OmegaSecurity.getIdUsuario(),
////							OmegaSecurity.getIdCorporacao(),
////							OmegaSecurity.getCorporacao(),
////							OmegaSecurity.getSenha(),
////							idTabelaSincronizador,
////							veiculoUsuario, 
////							vStrPostListTarefaAberta, 
////							vStrPostListTarefaMinha,
////							vStrPostListTarefaExecucao});
////			if(vResultPost != null){
////				if(vResultPost.length() > 0 ){
////					String vVetorVeiculoPosicao[] = vResultPost.split( OmegaConfiguration.DELIMITER_QUERY_UNIQUE_LINHA);
////					Integer numeroDeDelecoes = deleteTarefasObsoletas(vVetorVeiculoPosicao);
////					Integer numeroDeAtualizacoes = updateListaTarefaAbertaEPropria(vVetorVeiculoPosicao);
////					return numeroDeDelecoes + numeroDeAtualizacoes;
////				}
////			}
//			return 0;
//
//
//			//			receiveListOfCarAndInsertInDatabase(vVetorVeiculoPosicao);
//		} catch(Exception ex){
//			//if(ex != null)
//			//	Log.e(TAG, ex.getMessage());
//			//else 
//			//	Log.e(TAG, "Error desconhecido");
//			return 0;
//		}
//	}	
//
//	public static Integer getIndexTarefaAberta(Integer pIndexTarefaAberta){
//		if(pIndexTarefaAberta == null) return null;
//		for(Integer i = 0 ; i < MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAberta.size(); i++){
//			TarefaCustomOverlayItem vCustom = MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAberta.get(i);
//			if(vCustom != null){
//				Integer vIdTarefa = vCustom.getIdTarefa();
//				if(vIdTarefa == pIndexTarefaAberta) return i;
//			}
//		}
//		return null;
//	}
//
//	public static Integer getIndexTarefaAbertaPropria(Integer pIndexTarefaPropria){
//		if(pIndexTarefaPropria == null) return null;
//		for(Integer i = 0 ; i < MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAbertaPropria.size(); i++){
//			TarefaCustomOverlayItem vCustom = MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAbertaPropria.get(i);
//			if(vCustom != null){
//				Integer vIdTarefa = vCustom.getIdTarefa();
//				if(vIdTarefa == pIndexTarefaPropria) return i;
//			}
//		}
//		return null;
//	}
//	
//	public static Integer getIndexTarefaPropriaExecucao(Integer pIndexTarefaPropria){
//		if(pIndexTarefaPropria == null) return null;
//		for(Integer i = 0 ; i < MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemPropriaExecucao.size(); i++){
//			TarefaCustomOverlayItem vCustom = MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemPropriaExecucao.get(i);
//			if(vCustom != null){
//				Integer vIdTarefa = vCustom.getIdTarefa();
//				if(vIdTarefa == pIndexTarefaPropria) return i;
//			}
//		}
//		return null;
//	}
//
//	public static TarefaCustomOverlayItem getCustomOverlayTarefaAberta(Integer pIndexTarefaAberta){
//		for (TarefaCustomOverlayItem vCustom : MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAberta) {
//			Integer vIdTarefa = vCustom.getIdTarefa();
//			if(vIdTarefa == pIndexTarefaAberta) return vCustom;
//		}
//		return null;
//	}
//
//	public static TarefaCustomOverlayItem getCustomOverlayTarefaPropria(Integer pIndexTarefaAberta){
//		for (TarefaCustomOverlayItem vCustom : MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAbertaPropria) {
//			Integer vIdTarefa = vCustom.getIdTarefa();
//			if(vIdTarefa == pIndexTarefaAberta) return vCustom;
//		}
//		return null;
//	}
//
//	public static boolean removeTarefaPropriaExecucao(int pIndexVetor){
//		try{
//			if(MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemPropriaExecucao.size() > pIndexVetor && pIndexVetor >=0 ){
//				MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemPropriaExecucao.remove(pIndexVetor);
//				return true;
//			} else return false;	
//		} catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.SERVICO_ANDROID);
//			return false;
//		}
//	}
//
//	public static boolean removeTarefaAbertaPropria(int pIndexVetor){
//		try{
//			if(MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAbertaPropria.size() > pIndexVetor && pIndexVetor >=0 ){
//				MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAbertaPropria.remove(pIndexVetor);
//				return true;
//			} else return false;	
//		} catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.SERVICO_ANDROID);
//			return false;
//		}
//	}
//
//	public static boolean removeTarefaAberta(int pIndexVetor){
//		try{
//			if(MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAberta != null && 
//					MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAberta.size() > pIndexVetor && 
//					pIndexVetor >=0 ){
//				MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAberta.remove(pIndexVetor);
//				return true;
//			} else return false;	
//		} catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.SERVICO_ANDROID);
//			return false;
//		}
//
//	}
//
//	private Integer deleteTarefasObsoletas(String[] pVetorTarefa){
//		Integer numeroDeDelecoes = 0;
//		if(pVetorTarefa == null) return 0;
//		else if (pVetorTarefa.length == 0 )return 0;
//		else{
//			String vStrDelete = pVetorTarefa[pVetorTarefa.length - 1];
//
//			if(vStrDelete.startsWith("DELETE")){
//				String vVetorDelete[] = vStrDelete.split(OmegaConfiguration.DELIMITER_QUERY_UNIQUE_COLUNA);
//				if(vVetorDelete.length > 1 ){
//					for(Integer i = 1 ; i < vVetorDelete.length; i ++ ){
//						String vStrIndexTarefa = vVetorDelete[i];
//						Integer vIndexTarefa = HelperInteger.parserInteger(vStrIndexTarefa);
//
//						if(vIndexTarefa != null){
//							Integer vIndexVetorTarefaAberta = getIndexTarefaAberta(vIndexTarefa);
//							if( vIndexVetorTarefaAberta != null){
//								removeTarefaAberta(vIndexVetorTarefaAberta);
//								numeroDeDelecoes += 1;
//							} else{
//								Integer vIndexVetorTarefaPropria = getIndexTarefaAbertaPropria(vIndexTarefa);
//								if( vIndexVetorTarefaPropria != null){
//									removeTarefaAbertaPropria(vIndexVetorTarefaPropria);
//									numeroDeDelecoes += 1;
//								}
//							}
//						}
//
//					}
//
//				} else return 0;
//
//			} else return 0;
//		}
//		return numeroDeDelecoes ;
//	}
//
//
//	public static boolean trocaTuplaTarefaPropriaExecucaoParaPropriaConcluida(Integer pIndexTarefaAbertaExecucao){
//		try{
//			if(pIndexTarefaAbertaExecucao == null) return false;
//			Integer vIndexVetorTarefaAberta = getIndexTarefaPropriaExecucao(pIndexTarefaAbertaExecucao);
//			if(vIndexVetorTarefaAberta != null ){
//				TarefaCustomOverlayItem vCustom = MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemPropriaExecucao.get(vIndexVetorTarefaAberta);
//				MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemPropriaConcluida.add(vCustom);
//
//				removeTarefaPropriaExecucao(vIndexVetorTarefaAberta);
//				return true;
//			}
//		} catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.SERVICO_ANDROID);
//		}
//		return false;
//
//	}
//
//	public static boolean trocaTuplaTarefaAbertaPropriaParaPropriaExecucao(Integer pIndexTarefaAberta){
//		try{
//			if(pIndexTarefaAberta == null) return false;
//			Integer vIndexVetorTarefaAberta = getIndexTarefaAbertaPropria(pIndexTarefaAberta);
//			if(vIndexVetorTarefaAberta != null ){
//				TarefaCustomOverlayItem vCustom = MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAbertaPropria.get(vIndexVetorTarefaAberta);
//				MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemPropriaExecucao.add(vCustom);
//
//				removeTarefaAbertaPropria(vIndexVetorTarefaAberta);
//				return true;
//			}
//		} catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.SERVICO_ANDROID);
//		}
//		return false;
//
//	}
//
//	public static boolean trocaTuplaTarefaAbertaParaAbertaPropria(Integer pIndexTarefaAberta){
//		try{
//			if(pIndexTarefaAberta == null) return false;
//			Integer vIndexVetorTarefaAberta = getIndexTarefaAberta(pIndexTarefaAberta);
//			if(vIndexVetorTarefaAberta != null ){
//				TarefaCustomOverlayItem vCustom = MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAberta.get(vIndexVetorTarefaAberta);
//				MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAbertaPropria.add(vCustom);
//
//				removeTarefaAberta(vIndexVetorTarefaAberta);
//				return true;
//			}
//		} catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.SERVICO_ANDROID);
//		}
//		return false;
//
//	}
//
//	public static boolean trocaTuplaTarefaPropriaParaAberta(Integer pIndexTarefaPropria){
//		try{
//			Integer vIndexVetorTarefaAberta = getIndexTarefaAberta(pIndexTarefaPropria);
//			if(vIndexVetorTarefaAberta != null ){
//				TarefaCustomOverlayItem vCustom = MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAbertaPropria.get(vIndexVetorTarefaAberta);
//				MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAberta.add(vCustom);
//				removeTarefaAbertaPropria(vIndexVetorTarefaAberta);
//				return true;
//			}
//		} catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.SERVICO_ANDROID);
//		}
//		return false;
//	}
//	//return true se Aberta
//	//return false se Fechada
//	private boolean isTarefaAberta(Integer pIndexTarefa){
//		Integer vIndexAberta = ServiceSynchronizeListTarefaAberta.getIndexTarefaAberta(pIndexTarefa);
//		if(vIndexAberta != null ) return true;
//		else return false;
//	}
//
//	private boolean isTarefaPropria(Integer pIndexTarefa){
//		Integer vIndexPropria = ServiceSynchronizeListTarefaAberta.getIndexTarefaAbertaPropria(pIndexTarefa);
//		if(vIndexPropria != null ) return true;
//		else return false;
//	}
//
//	private Integer updateListaTarefaAbertaEPropria(String[] pVetorTarefa){
//		Integer numeroDeDelecoes = 0;
//		try{
//		
//		if(pVetorTarefa == null) return 0;
//		else if (pVetorTarefa.length == 0 )return 0;
//		else{
//			Drawable iconeTarefaOrigem = getResources().getDrawable(R.drawable.person);
//			Drawable iconeTarefaDestino = getResources().getDrawable(R.drawable.person);
//			for(Integer i = 0 ; i < pVetorTarefa.length; i++){
//				String vStrToken = pVetorTarefa[i];
//				//checa se a utima clausula eh contida por "DELETE..."
//				if(i == pVetorTarefa.length - 1) 
//					if(vStrToken.startsWith("DELETE")) return numeroDeDelecoes;
//
//				String vVetorToken[] = vStrToken.split( OmegaConfiguration.DELIMITER_QUERY_UNIQUE_COLUNA);
//				//atualizacao das atuais listas
//				if(vVetorToken.length == 0 ) continue;
//
//				String vStrIdTarefa = vVetorToken[0];
//				Integer vIndexTarefa = HelperInteger.parserInteger(vStrIdTarefa); 
//				if(vIndexTarefa != null){
//					if(vVetorToken.length == 1){
//						if(isTarefaAberta(vIndexTarefa)){
//							trocaTuplaTarefaAbertaParaAbertaPropria(vIndexTarefa);
//							numeroDeDelecoes ++;
//						} else if(isTarefaPropria(vIndexTarefa)){
//							trocaTuplaTarefaPropriaParaAberta(vIndexTarefa);
//							numeroDeDelecoes ++;
//						}
//					} else if(vVetorToken.length == 14){
//						//insercao de nova tarefa aberta ou propria
//						String vStrIdVeiculoUsuario = vVetorToken[1];
//
//						Integer vIdVeiculoUsuario = HelperInteger.parserInteger(vStrIdVeiculoUsuario);
//
//						boolean vIsTarefaAberta = true;
//
//						if(vIdVeiculoUsuario != null ){
//							vIsTarefaAberta = false;
//							//Se ja for existente o overlayItem deve continuar
//							if(getIndexTarefaAbertaPropria(vIndexTarefa) != null) continue;
//						} else{
//							//Se ja for existente o overlayItem deve continuar
//							if(getIndexTarefaAberta(vIndexTarefa) != null) continue;
//						}
//
//
//
//						TarefaCustomOverlayItem vTarefa = null;
//						String vOrigemEndereco = HelperString.checkIfIsNull(vVetorToken[2]);
//						String vOrigemComplemento = HelperString.checkIfIsNull(vVetorToken[3]);
//						String vDestinoEndereco = HelperString.checkIfIsNull(vVetorToken[4]);
//						String vDestinoComplemento = HelperString.checkIfIsNull(vVetorToken[5]);
//						String vHora = HelperString.checkIfIsNull(vVetorToken[6]);
//						String vData = HelperString.checkIfIsNull(vVetorToken[7]);
//						String vDescricao = HelperString.checkIfIsNull(vVetorToken[8]);
//						String vDataAberturaTarefa = HelperString.checkIfIsNull(vVetorToken[9]);
//						String vDataInicioMarcadoDaTarefa = HelperString.checkIfIsNull(vVetorToken[10]);
//						String vHoraInicioMarcadoDaTarefa = HelperString.checkIfIsNull(vVetorToken[11]);
//						String vDatetimeInicioMarcadoDaTarefa = null;
//						String vDateInicioTarefa = HelperString.checkIfIsNull(vVetorToken[12]);
//						String vTimeInicioTarefa = HelperString.checkIfIsNull(vVetorToken[13]);
//						String vDatetimeInicioTarefa = null;
//
//						GeoPoint vGeoPointOrigem = null;
//						if(vOrigemEndereco != null)
//							vGeoPointOrigem = HelperMapa.getGeoPointDoEndereco(vOrigemEndereco);
//
//						if(vDateInicioTarefa != null && vTimeInicioTarefa != null)
//							vDatetimeInicioTarefa = vDateInicioTarefa + " " +vTimeInicioTarefa;  
//
//						if(vDataInicioMarcadoDaTarefa != null && vHoraInicioMarcadoDaTarefa != null)
//							vDatetimeInicioMarcadoDaTarefa = vDataInicioMarcadoDaTarefa + " " + vHoraInicioMarcadoDaTarefa;
//						GeoPoint vGeoPointPrincipal = null;
//						if(MapaListMarkerActivity.me != null)
//							vGeoPointPrincipal = MapaListMarkerActivity.me.getMyLocation();
//						if(vGeoPointOrigem != null)
//							vTarefa = new TarefaCustomOverlayItem(
//									vIndexTarefa,
//									vGeoPointPrincipal,
//									vGeoPointOrigem,
//									vOrigemEndereco,
//									vOrigemComplemento,
//									vDestinoEndereco,
//									vDestinoComplemento,
//									"Tarefa " + String.valueOf(vIndexTarefa), 
//									HelperDate.getDatetimeAtualFormatadaParaExibicao(this),
//									iconeTarefaOrigem,
//									iconeTarefaDestino,
//									vIdVeiculoUsuario,
//									vData + " " + vHora, 
//									vDataAberturaTarefa,
//									vDatetimeInicioMarcadoDaTarefa,
//									vDatetimeInicioTarefa,
//									null,
//									vDescricao); 
//						numeroDeDelecoes ++;
//						if(vIsTarefaAberta){
//							MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAberta.add(vTarefa);
//						}
//						else{
//							if(vDateInicioTarefa != null)
//								MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemPropriaExecucao.add(vTarefa);
//							else 
//								MapaListMarkerActivity.listContainerMarcadorMapaTarefaOrigemAbertaPropria.add(vTarefa);
//
//						}
//					}
//				}
//			}	
//		}
//		} catch(Exception ex){
//			SingletonLog.insereErro(ex, SingletonLog.TIPO.SERVICO_ANDROID); 
//		} 
//		return numeroDeDelecoes ;
//	}
//
//
//	private void sendBroadCastToVeiculoMapActivity()
//	{
//		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SERVICE_LIST_POSITION_TAREFA);
//		sendBroadcast(updateUIIntent);
//	}

//}
