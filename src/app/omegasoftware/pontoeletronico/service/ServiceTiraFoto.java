package app.omegasoftware.pontoeletronico.service;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.os.IBinder;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaSincronizadorArquivo;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioFoto;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioFoto.ContainerUsuarioFoto;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.file.FileWriterHttpPostManager;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.http.HelperHttp;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

public class ServiceTiraFoto extends Service  implements Camera.PictureCallback {

	//Constants
	public static final int ID_INTERNO = 1;
	public static final int ID_EXTERNO = 3;
	
	public static boolean IS_INTERNO_ATIVO = false;
	public static boolean IS_EXTERNO_ATIVO = false;
	
	public static final String LIMIT_TUPLA_TO_SEND = "15";
	public static final String TAG = "ServiceTiraFoto";
	public static String lastNomeFotoInternaInserida;
	private Timer tiraFotoTimer = new Timer();
	String idUsuario;
	OmegaFileConfiguration configuration ;

	EXTDAOUsuarioFoto objUsuarioFoto;
	public static int LIMIT_FILE_TO_ZIP = 3;
	public static int LIMIT_FILE_TO_UPLOAD = 2;
	public static String LIMIT_DATA_USUARIO_FOTO_SEND = "10";
	private Camera camera;
	private Parameters cameraParameters;
	DatabasePontoEletronico db=null;

	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId){

//		Bundle vParams = intent.getExtras();
		

//		return super.onStartCommand(intent, flags, startId);
		return START_STICKY;
	}
	
	
	
	@Override
	public void onDestroy(){
		
		if(this.db != null)
			this.db.close();
		if(this.tiraFotoTimer != null)
			this.tiraFotoTimer.cancel();
		if(this.camera != null)
			this.camera.release();


//		super.onDestroy();
	}
	FileWriterHttpPostManager filesManager ;

	@Override
	public void onCreate() {

		super.onCreate();
		try {
			db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		objUsuarioFoto = new EXTDAOUsuarioFoto(db);
		idUsuario = OmegaSecurity.getIdUsuario();
		configuration = new OmegaFileConfiguration();
//		filesManager = new FileWriterHttpPostManager(
//				this,
//				OmegaConfiguration.URL_REQUEST_SEND_FILE_PHOTO(),
//				configuration.getPathFilePhotoToSend(), 
//				"");
		//tenta dar upload em todas as fotos remanescentes
//		filesManager.uploadFiles(LIMIT_FILE_TO_ZIP, LIMIT_FILE_TO_UPLOAD);
		initializeCamera();
		setTimer();

	}

	private void initializeCamera(){
		try
		{
			if(this.camera == null){
				this.camera = Camera.open();
				this.cameraParameters = this.camera.getParameters();
				this.cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_INFINITY);
				this.cameraParameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
			}

		}
		catch(Exception e)
		{
			//TODO Handle this exception
			this.camera.release();
			this.camera = null;

		}
	}

	private void setTimer()
	{

		//Starts reporter timer
		this.tiraFotoTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				sendFoto();
			}
		},
		OmegaConfiguration.SERVICE_TIRA_FOTO_INITIAL_DELAY,
		OmegaConfiguration.SERVICE_TIRA_FOTO_TIMER_INTERVAL);

	}


	public static String getLastNomeFotoInternaInserida(){
		return lastNomeFotoInternaInserida;
	}

	public static void clearLastNomeFotoInternaInserida(){
		lastNomeFotoInternaInserida = null;
	}

	private void sendFoto()
	{
		try{
			if(camera != null){
				camera.takePicture(null, null, this);
			} else {
				initializeCamera();	
			}
		} catch(Exception ex){
			camera.release();
			camera = null;
			initializeCamera();
			
			//if(ex != null)
			//	Log.e(TAG, ex.getMessage());
			//else Log.e(TAG, "Error desconhecido");
		}
	}

	public void sendDataOfUsuarioFoto(){
		
		
		try{
		EXTDAOUsuarioFoto vObjUsuarioFoto = new EXTDAOUsuarioFoto(db);
		
		ContainerUsuarioFoto vContainerUsuarioFoto = vObjUsuarioFoto.getStrListLastTuplas(LIMIT_TUPLA_TO_SEND);
		if(vContainerUsuarioFoto != null && vContainerUsuarioFoto.getCount() > 0 ){
			String vResult = null;
//			String vResult = HelperHttpPost.getConteudoStringPost(
//					this, 
//					OmegaConfiguration.URL_REQUEST_RECEIVE_LISTA_TUPLA_USUARIO_FOTO(), 
//					new String[] {
//							"usuario",
//							"id_corporacao",
//							"corporacao", 
//							"senha", 
//							"lista_tupla"}, 
//					new String[] {
//							OmegaSecurity.getIdUsuario(), 
//							OmegaSecurity.getIdCorporacao(), OmegaSecurity.getCorporacao(), 
//							OmegaSecurity.getSenha(), 
//							vContainerUsuarioFoto.getStrTotalTupla()}
//				);
			if(vResult.compareTo("TRUE") == 0 ){
				ArrayList<String> vListId = vContainerUsuarioFoto.getListStrId();
				for (String vId : vListId) {
					vObjUsuarioFoto.remove(vId, false);
				}
			}
		}
		} catch(Exception ex){
			//if(ex != null)
			//	Log.e(TAG, ex.getMessage());
			//else Log.e(TAG, "Error desconhecido"); 
		} finally{
			db.close();	
		}
		
	}
	
	public void sendDataOfUsuarioFoto2(){
		
		
		EXTDAOUsuarioFoto vObjUsuarioFoto = new EXTDAOUsuarioFoto(db);
		ArrayList<Table> vListTable = vObjUsuarioFoto.getListTable(null, " 0, " + LIMIT_DATA_USUARIO_FOTO_SEND + " ", true);
		if(vListTable != null)
			if(vListTable.size() > 0){
				String vStrListaTupla = "";
				boolean vIsFirstLine = true;
				for (Table vTuplaUsuarioFoto : vListTable) {
					if(! vIsFirstLine)
						vStrListaTupla += "%%";
					else vIsFirstLine = false; 
					
					String vData = vTuplaUsuarioFoto.getStrValueOfAttribute(EXTDAOUsuarioFoto.CADASTRO_SEC);
					vStrListaTupla +=  ";" + HelperString.getStrAndCheckIfIsNull(vData);
					String vHora = vTuplaUsuarioFoto.getStrValueOfAttribute(EXTDAOUsuarioFoto.CADASTRO_OFFSEC);
					vStrListaTupla +=  ";" + HelperString.getStrAndCheckIfIsNull(vHora);
					String vFotoInterna = vTuplaUsuarioFoto.getStrValueOfAttribute(EXTDAOUsuarioFoto.FOTO_INTERNA);
					vStrListaTupla += ";" + HelperString.getStrAndCheckIfIsNull(vFotoInterna);
					String vFotoExterna= vTuplaUsuarioFoto.getStrValueOfAttribute(EXTDAOUsuarioFoto.FOTO_EXTERNA);
					vStrListaTupla += ";" + HelperString.getStrAndCheckIfIsNull(vFotoExterna);
					
				}
				if(vStrListaTupla.length() > 0 ){
					String vResult = null;
//					String vResult = HelperHttpPost.getConteudoStringPost(
//							this, 
//							OmegaConfiguration.URL_REQUEST_RECEIVE_LISTA_TUPLA_USUARIO_FOTO(), 
//							new String[] {
//									"usuario", 
//									"id_corporacao", "corporacao", 
//									"senha", 
//									"lista_tupla"}, 
//							new String[] {
//									OmegaSecurity.getIdUsuario(), 
//									OmegaSecurity.getIdCorporacao(),OmegaSecurity.getCorporacao(),  
//									OmegaSecurity.getSenha(), 
//									vStrListaTupla}
//						);
					if(vResult.compareTo("TRUE") == 0 ){
						for (Table vTuplaUsuarioFoto : vListTable) {
							
							try{
								vTuplaUsuarioFoto.remove(false);	
							}catch(Exception ex){
								SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
							}
						}
					}
				}
			}
		
		db.close();
	}
	
	
	public void onPictureTaken(byte[] data, Camera camera) {
		
		
		//Decode the data obtained by the camera into a Bitmap
		try {
			Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);

			String vFoto = configuration.getFileUniqueName(".jpg");
			String path = configuration.getPath(OmegaFileConfiguration.TIPO.FOTO);
			File file = new File(path, vFoto);


			FileOutputStream fos = new FileOutputStream(file);
			picture.compress(
					OmegaConfiguration.PICTURE_COMPRESS_FORMAT, 
					OmegaConfiguration.PICTURE_COMPRESS_QUALITY, 
					fos);

			fos.close();
			
			HelperDate vObjDate = new HelperDate();
			String vDate = vObjDate.getDateDisplay();
			String vTime = vObjDate.getTimeDisplay();
			
			objUsuarioFoto.setAttrValue(EXTDAOUsuarioFoto.ID, null);
			objUsuarioFoto.setAttrValue(EXTDAOUsuarioFoto.USUARIO_ID_INT, idUsuario);
			objUsuarioFoto.setAttrValue(EXTDAOUsuarioFoto.FOTO_INTERNA, vFoto);
			objUsuarioFoto.setAttrValue(EXTDAOUsuarioFoto.CADASTRO_OFFSEC, vDate);
			objUsuarioFoto.setAttrValue(EXTDAOUsuarioFoto.CADASTRO_SEC, vTime);
			objUsuarioFoto.setAttrValue(EXTDAOUsuarioFoto.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
			objUsuarioFoto.insert(false);
			
			lastNomeFotoInternaInserida = vFoto;
			boolean vValidadeSend = false;
//			if(HelperHttp.isServerOnline(this)){
//				envia foto
				if(filesManager.uploadFileAndRemove(vFoto))
					vValidadeSend = true;
//				filesManager.uploadFiles(LIMIT_FILE_TO_ZIP, LIMIT_FILE_TO_UPLOAD);
				
				sendDataOfUsuarioFoto();
//			}
			if(!vValidadeSend){
				EXTDAOSistemaSincronizadorArquivo vObjSincronizadorFoto = new EXTDAOSistemaSincronizadorArquivo(db);
				vObjSincronizadorFoto.setAttrValue(EXTDAOSistemaSincronizadorArquivo.ARQUIVO, vFoto);
				vObjSincronizadorFoto.setAttrValue(EXTDAOSistemaSincronizadorArquivo.IS_ZIP_BOOLEAN, "0");
				vObjSincronizadorFoto.insert(false);
			}
			
		} catch (FileNotFoundException e) {
			//Log.e(TAG,"onPictureTaken(): " + e.getMessage());
		} catch (IOException e) {
			//Log.e(TAG,"onPictureTaken(): " + e.getMessage());
		}catch (Exception e) {
			//Log.e(TAG,"onPictureTaken(): " + e.getMessage());
		}
		db.close();
	}

}
