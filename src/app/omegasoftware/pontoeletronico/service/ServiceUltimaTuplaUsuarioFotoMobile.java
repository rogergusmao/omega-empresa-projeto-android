package app.omegasoftware.pontoeletronico.service;


import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuarioFoto;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;

public class ServiceUltimaTuplaUsuarioFotoMobile extends Service  {

	//Constants
//	private static final String TAG = "ServiceUltimaTuplaUsuarioFotoMobile";

	private DatabasePontoEletronico db=null;
	private Timer loadDataFromServerTimer = new Timer();

	//Download button
	Context context ;

	String idUsuario;

	String lastIdUsuarioFotoDownloaded;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){

		Bundle vParams = intent.getExtras();
		idUsuario = vParams.getString(OmegaConfiguration.SEARCH_FIELD_ID);
		return super.onStartCommand(intent, flags, startId);
	}

	private void setTimer()
	{

		//Starts reporter timer
		this.loadDataFromServerTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				//se nao estao pausado
				loadData();
			}
		},
		OmegaConfiguration.LOAD_TELA_MONITORAMENTO_INITIAL_DELAY,
		OmegaConfiguration.LOAD_TELA_MONITORAMENTO_TIMER_INTERVAL);
	}

	@Override
	public void onCreate() {

		super.onCreate();

		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setTimer();
	}

	@Override
	public void onDestroy(){
		this.loadDataFromServerTimer.cancel();
		if(db != null)
			db.close();
		super.onDestroy();
	}

	public boolean loadData() {

		//			Envio os dados da tabela Veiculo Posicao inseridos, no caso somentes as minhas posicoes
		try{
			String vVetorKey[] = null;
			String vVetorContent[] = null;
			if(lastIdUsuarioFotoDownloaded == null){
				vVetorKey = new String[] {"usuario"};
				vVetorContent = new String[] {idUsuario};
			} else{
				vVetorKey = new String[] {"usuario", "usuario_foto"};
				vVetorContent = new String[] {idUsuario, lastIdUsuarioFotoDownloaded};
			}
//			String vPostResult = HelperHttpPost.getConteudoStringPost(
//					this, 
//					OmegaConfiguration.URL_REQUEST_LAST_TUPLA_USUARIO_FOTO(), 
//					vVetorKey, 
//					vVetorContent);
			String vPostResult = null;
			if(vPostResult != null){

				if(vPostResult.length() > 0){

					if(! (vPostResult.compareTo("FALSE") == 0)){
						String vTupla[] = vPostResult.split("[;]+");
						if(vTupla.length == 5){
							String vIdUsuarioFoto = vTupla[0];
							String vData = vTupla[1];
							String vHora = vTupla[2];
							String vFotoInterna = vTupla[3];
							String vFotoExterna = vTupla[4];

							EXTDAOUsuarioFoto vObj = new EXTDAOUsuarioFoto(db);
							lastIdUsuarioFotoDownloaded = vIdUsuarioFoto;
							vObj.setAttrValue(EXTDAOUsuarioFoto.ID, vIdUsuarioFoto);
							vObj.setAttrValue(EXTDAOUsuarioFoto.CADASTRO_SEC, vData);
							vObj.setAttrValue(EXTDAOUsuarioFoto.CADASTRO_OFFSEC, vHora);
							vObj.setAttrValue(EXTDAOUsuarioFoto.FOTO_INTERNA, vFotoInterna);
							vObj.setAttrValue(EXTDAOUsuarioFoto.FOTO_EXTERNA, vFotoExterna);
							vObj.setAttrValue(EXTDAOUsuarioFoto.CORPORACAO_ID_INT, OmegaSecurity.getIdCorporacao());
							vObj.insert(true);

							sendBroadCastToReceiveFileOfServerActivity(vIdUsuarioFoto, vData + " " + vHora);
						}
					}
				}
			}
			return true;
		} catch(Exception ex){
			return false;
		} 
	}

	private void sendBroadCastToReceiveFileOfServerActivity(String pId, String pDatetime)
	{
		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SERVICE_LAST_USUARIO_FOTO);
		updateUIIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, pId);
		sendBroadcast(updateUIIntent);
	}
}
