package app.omegasoftware.pontoeletronico.service;


import java.util.ArrayList;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.Table;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOSistemaUsuarioMensagem;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOUsuario;
import app.omegasoftware.pontoeletronico.http.HelperHttp;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;

public class ServiceUsuarioMensagem extends Service  {

	//Constants

	public static final String TAG = "ServiceUsuarioMensagem";
	public static final int ID  = 6;
	public final int LIMIT_OF_MENSAGEM_TO_SEND = 2;
	String emailUsuario;
	String nomeUsuario;
	private Timer enviaMensagensParaOServidorTimer = new Timer();
	Context context ;
	Stack<String> vetorMensagem = new Stack<String>(); 
	Stack<String> vetorTag = new Stack<String>();
	Stack<String> vetorIdUsuarioMensagem = new Stack<String>();
	Stack<Boolean> vetorIsMelhoria = new Stack<Boolean>();
	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}


	@Override
	public void onDestroy(){

		this.enviaMensagensParaOServidorTimer.cancel();

		//		super.onDestroy();

	}




	@Override
	public void onCreate() {

		super.onCreate();
		Database db = null;
		try{
			db = new DatabasePontoEletronico(this);
		
			EXTDAOUsuario vObjUsuario = new EXTDAOUsuario(db);
			vObjUsuario.setAttrValue(EXTDAOUsuario.ID, OmegaSecurity.getIdUsuario());
			if(vObjUsuario.select()){
				emailUsuario = vObjUsuario.getStrValueOfAttribute(EXTDAOUsuario.EMAIL);
				nomeUsuario = vObjUsuario.getStrValueOfAttribute(EXTDAOUsuario.NOME);
				setTimer();
			}
		} catch(Exception ex){
			if(OmegaConfiguration.DEBUGGING)
				Log.e(TAG, ex.getMessage());
			
		} finally{
			if(db != null){
				db.close();
			}
		}
	}
	

	private boolean loadMensagensASeremEnviadas(){
		Database db=null;
		try {
			db = new DatabasePontoEletronico(context);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try{
			if(vetorMensagem.size() == 0 ){
				EXTDAOSistemaUsuarioMensagem vObjUsuarioMensagem = new EXTDAOSistemaUsuarioMensagem(db);
				vObjUsuarioMensagem.setAttrValue(EXTDAOSistemaUsuarioMensagem.USUARIO_ID_INT, OmegaSecurity.getIdUsuario());
				ArrayList<Table> vListTupla = null;
				if(LIMIT_OF_MENSAGEM_TO_SEND > 0 ){
					vListTupla = vObjUsuarioMensagem.getListTable(null, " 0, " + String.valueOf(3* LIMIT_OF_MENSAGEM_TO_SEND) + " ", true);
				} else vListTupla = vObjUsuarioMensagem.getListTable();
				if(vListTupla == null){
					return false;
				}
				for (Table tupla : vListTupla) {
					String vMensagem = tupla.getStrValueOfAttribute(EXTDAOSistemaUsuarioMensagem.MENSAGEM);
					String vTag = tupla.getStrValueOfAttribute(EXTDAOSistemaUsuarioMensagem.TAG);
					String vId = tupla.getStrValueOfAttribute(EXTDAOSistemaUsuarioMensagem.ID);
					//Boolean vIsMelhoria = HelperBoolean.valueOfStringSQL(tupla.getStrValueOfAttribute(EXTDAOSistemaUsuarioMensagem.IS_MELHORIA_BOOLEAN));
					vetorMensagem.add(vMensagem);
					vetorTag.add(vTag);
					vetorIdUsuarioMensagem.add(vId);
					//vetorIsMelhoria.add(vIsMelhoria);
				}
				return true;
			} else return true;
		}
		catch(Exception ex){
			if(OmegaConfiguration.DEBUGGING)
				Log.e(TAG, ex.getMessage());
			
			return false;
		} finally{
			if(db != null)
				db.close();
		}
	}

	private void sendMensagens(){
		if(! loadMensagensASeremEnviadas()) return;
		Database db=null;
		try {
			db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
//			if(HelperHttp.isServerOnline(this)){
				
				EXTDAOSistemaUsuarioMensagem vObjUsuarioMensagem = new EXTDAOSistemaUsuarioMensagem(db);
				for(int i = 0 ; i < vetorMensagem.size(); i ++){
					String vTag = vetorTag.peek();
					String vMensagem = vetorMensagem.peek();
					String vIdUsuarioMensagem = vetorIdUsuarioMensagem.peek();
					Boolean vIsMelhoria = vetorIsMelhoria.peek();
					if(i >= LIMIT_OF_MENSAGEM_TO_SEND && LIMIT_OF_MENSAGEM_TO_SEND >= 0)
						break;

					String vURL = null;
//					if(vIsMelhoria)
//						vURL = OmegaConfiguration.URL_REQUEST_EMAIL_MELHORIA();
//					else vURL = OmegaConfiguration.URL_REQUEST_EMAIL_ERROR();
//
//					String vStrPost = HelperHttpPost.getConteudoStringPost(
//							this, 
//							vURL, 
//							new String [] {
//									"usuario", 
//									"id_corporacao", "corporacao", 
//									"senha", 
//									"email", 
//									"tag",
//							"mensagem"}, 
//							new String [] {
//									OmegaSecurity.getIdUsuario(), 
//									OmegaSecurity.getIdCorporacao(),OmegaSecurity.getCorporacao(),
//									OmegaSecurity.getSenha(), 
//									emailUsuario,
//									vTag,
//									vMensagem});

//					if(vStrPost != null && vStrPost.compareTo("TRUE") == 0){
//						vObjUsuarioMensagem.remove(vIdUsuarioMensagem, false);
//						vetorTag.pop();
//						vetorMensagem.pop();
//						vetorIdUsuarioMensagem.pop();
//
//					}
				}
//			}
		} catch(Exception ex){
			if(OmegaConfiguration.DEBUGGING)
				Log.e(TAG, ex.getMessage());
			//else Log.e(TAG, "Error desconhecido");
		} finally{
			db.close();
		}
	}


	private void setTimer()
	{

		//Starts reporter timer
		this.enviaMensagensParaOServidorTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				sendMensagens();
			}
		},
		OmegaConfiguration.SERVICE_SEND_MENSAGE_USUARIO_INITIAL_DELAY,
		OmegaConfiguration.SERVICE_SEND_MENSAGE_USUARIO_TIMER_INTERVAL);

	}

}
