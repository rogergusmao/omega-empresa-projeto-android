package app.omegasoftware.pontoeletronico.service;


import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.IBinder;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.ReentrantLock;

import app.omegasoftware.pontoeletronico.TabletActivities.WifiActivity;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOWifi;

public class ServiceWifi extends Service {

	//Constants
	public static final int ID = 4;

	public static final String TAG = "ServiceWifi";
	public static final String LIMIT_TUPLA_TO_SEND = "100";
	private Timer wifiTimer = new Timer();


	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		int vReturn = super.onStartCommand(intent, flags, startId);
		mWifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		registerReceiver(mWifiScanReceiver,
				new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		return vReturn;
	}


	@Override
	public void onDestroy() {

		this.wifiTimer.cancel();
		super.onDestroy();
	}


	@Override
	public void onCreate() {

		super.onCreate();

//		myPositionInDatabase = new VeiculoGPSLocationListener(this);
		setTimer();

	}

	//
	WifiManager mWifiManager;
	List<ScanResult> results = null;

	private void setTimer() {
		//Starts reporter timer
		this.wifiTimer.scheduleAtFixedRate(new TimerTask() {
											   @Override
											   public void run() {
												   verificarProximidadeWifi();
											   }
										   },
				OmegaConfiguration.SERVICE_WIFI_INITIAL_DELAY,
				OmegaConfiguration.SERVICE_WIFI_TIMER_INTERVAL);
	}

	ReentrantLock re = new ReentrantLock();
	private final BroadcastReceiver mWifiScanReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context c, Intent intent) {
			try{
				if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
					verificarProximidadeWifi();
				}
			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.SERVICO_ANDROID);
			}finally {
//				mWifiScanReceiver.abortBroadcast();
			}
		}


	};

	private void verificarProximidadeWifi() {
		if (re.tryLock()) {
			try {
				results = mWifiManager.getScanResults();
				if(results == null)
				{
					mWifiScanReceiver.clearAbortBroadcast();

					mWifiManager.startScan();
					return;
				}
				Database db = null;
				try {
					db = new DatabasePontoEletronico(this);
					EXTDAOWifi.registraProximidadeDeRedeWifiSeExistente(db, results);
				} catch (Exception ex) {
					SingletonLog.factoryToast(this, ex, SingletonLog.TIPO.PAGINA);
				} finally {
					if (db != null) db.close();
				}
			} catch (Exception ex) {
				SingletonLog.insereErro(ex, TIPO.SERVICO_ANDROID);
			} finally {
				re.unlock();
			}
		}
	}


}
