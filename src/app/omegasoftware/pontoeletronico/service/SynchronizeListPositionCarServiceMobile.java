package app.omegasoftware.pontoeletronico.service;


import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.widget.TextView;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;

public class SynchronizeListPositionCarServiceMobile extends Service  {

	//Constants
//	private static final String TAG = "SynchronizeListPositionCarServiceMobile";
	
	private DatabasePontoEletronico db=null;
//	SynchronizeFastReceiveDatabase synchronizeReceiveAndSendDatabase ;

	String[] vetorNomeTabela;

	private Timer positionOtherCarTimer = new Timer();

	TextView conteudoTabelaTextView; 
	//Download button
//	private Button searchButton;
	Context context ;
//	

	@Override
	public void onDestroy(){
		this.positionOtherCarTimer.cancel();
		if(db != null)
			db.close();
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {

		super.onCreate();

		try {
			this.db = new DatabasePontoEletronico(this);
		} catch (OmegaDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//v.getResources().getStringArray(R.array.receive_table_db)
//		Envio os dados da tabela Veiculo Posicao inseridos, no caso somentes as minhas posicoes
//		synchronizeReceiveAndSendDatabase = new SynchronizeFastReceiveDatabase(
//			this,  
//			-1,
//			new String[] {EXTDAOUsuarioPosicao.NAME});

		//		synchronizeReceiveDatabase.procedureReceiveDataOfDatabase();
		vetorNomeTabela = this.db.getVetorNomeTabela();

		

		setTimer();
	}

	private void setTimer()
	{

		//Starts reporter timer
		this.positionOtherCarTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				reporterAction();
			}
		},
		OmegaConfiguration.SERVICE_POSITION_OTHER_CAR_INITIAL_DELAY,
		OmegaConfiguration.SERVICE_POSITION_OTHER_CAR_TIMER_INTERVAL);

	}

	private void reporterAction()
	{

		//Gets the Android Service which controls the internet connection
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

		//Gets information about current active network
		NetworkInfo ni = cm.getActiveNetworkInfo();

		//Is there any active network ?
		if(ni != null)
		{
			if(ni.isConnected())
			{
				launchUploaderTask();
			}
			else
			{
				//Log.d(TAG,"reporterAction(): Network is not connected");
			}	
		}
		else
		{
			//No networks available
			//Log.d(TAG,"reporterAction(): Network does not exists");
		}
	}

	public void launchUploaderTask()
	{


		new Runnable() {
			public void run() {
				if(loadData())
					sendBroadCastToVeiculoMapActivity();
			}
		}.run();
		
	}

	public boolean loadData() {		
		
		try{
//			synchronizeReceiveAndSendDatabase.run(true);
			
			return true;
		} catch(Exception ex){
			return false;
		}
 
	}

	private void sendBroadCastToVeiculoMapActivity()
	{

		Intent updateUIIntent = new Intent(OmegaConfiguration.REFRESH_SERVICE_LIST_POSITION_OTHER_CAR);
		sendBroadcast(updateUIIntent);
	}

}
