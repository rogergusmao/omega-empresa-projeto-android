package app.omegasoftware.pontoeletronico.task;

import android.database.Cursor;

/**
 * Created by W10 on 25/02/2018.
 */

public abstract class AbstractCustomCursorFactory
{
    protected String[] mProjection;


    /**
     * Initialize the factory with the given projection.
     *
     * @param projection
     *         An array of column names.
     */
    public AbstractCustomCursorFactory(String[] projection)
    {
        mProjection = projection;
    }


    /**
     * Get a new {@link Cursor} from this factory.
     *
     * @return A {@link Cursor}.
     */
    public abstract Cursor getCursor();
}
