package app.omegasoftware.pontoeletronico.task;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by W10 on 24/02/2018.
 */

public class AgrupadorTarefaAdapter extends FragmentStatePagerAdapter {
    public AgrupadorTarefaAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position)
    {
        //int pageId = mTabConfig.getVisibleItem(position).getId();
        int pageId = 1;
        TaskListFragment fragment = TaskListFragment.newInstance(position);
        fragment.setPageId(pageId);
        return fragment;

    }

    @Override
    public int getCount() {
        return 1;
    }
}
