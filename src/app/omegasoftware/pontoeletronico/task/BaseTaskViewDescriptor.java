package app.omegasoftware.pontoeletronico.task;

/**
 * Created by W10 on 25/02/2018.
 */

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.support.v4.util.SparseArrayCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.sql.Time;

import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

/**
 * A base implementation of a {@link ViewDescriptor}. It has a number of commonly used methods.
 *
 * @author Marten Gajda <marten@dmfs.org>
 */
public abstract class BaseTaskViewDescriptor implements ViewDescriptor
{

    /**
     * We use this to get the current time.
     */
    protected Time mNow;


    protected void setDueDate(TextView view, ImageView dueIcon, Time dueDate, boolean isClosed)
    {
        if (view != null && dueDate != null)
        {
//            Time now = mNow;
//            if (now == null)
//            {
//                now = mNow = new Time();
//            }
//            if (!now.timezone.equals(TimeZone.getDefault().getID()))
//            {
//                now.clear(TimeZone.getDefault().getID());
//            }
//
//            if (Math.abs(now.toMillis(false) - System.currentTimeMillis()) > 5000)
//            {
//                now.setToNow();
//                now.normalize(true);
//            }
//
//            dueDate.normalize(true);
//
//            view.setText(new DateFormatter(view.getContext()).format(dueDate, now, DateFormatContext.LIST_VIEW));
//            if (dueIcon != null)
//            {
//                dueIcon.setVisibility(View.VISIBLE);
//            }
//
//            // highlight overdue dates & times, handle allDay tasks separately
//            if ((!dueDate.allDay && dueDate.before(now) || dueDate.allDay
//                    && (dueDate.year < now.year || dueDate.yearDay <= now.yearDay && dueDate.year == now.year))
//                    && !isClosed)
//            {
//                view.setTextAppearance(view.getContext(), R.style.task_list_overdue_text);
//            }
//            else
//            {
//                view.setTextAppearance(view.getContext(), R.style.task_list_due_text);
//            }
        }
        else if (view != null)
        {
            view.setText("");
            if (dueIcon != null)
            {
                dueIcon.setVisibility(View.GONE);
            }
        }
    }


    protected void setOverlay(View view, int position, int count)
    {
        View overlayTop = getView(view, R.id.overlay_top);
        View overlayBottom = getView(view, R.id.overlay_bottom);

        if (overlayTop != null)
        {
            overlayTop.setVisibility(position == 0 ? View.VISIBLE : View.GONE);
        }

        if (overlayBottom != null)
        {
            overlayBottom.setVisibility(position == count - 1 ? View.VISIBLE : View.GONE);
        }
    }


    protected void setDescription(View view, Cursor cursor)
    {
        String description = null;
        if(description == null || description.length() == 0){
            description = cursor.getString(1);
        }

        if(description == null || description.length() == 0){
            description = cursor.getString(2);
        }

        description = HelperString.substring(description,  150);
        TextView descriptionView = (TextView) getView(view, android.R.id.text1);
        if(descriptionView!=null){
            descriptionView.setVisibility(View.VISIBLE);
            descriptionView.setText(description);
        }

//        String description = TaskFieldAdapters.DESCRIPTION.get(cursor);
//        TextView descriptionView = (TextView) getView(view, android.R.id.text1);
//        if (TextUtils.isEmpty(description))
//        {
//            descriptionView.setVisibility(View.GONE);
//        }
//        else
//        {
//            descriptionView.setVisibility(View.VISIBLE);
//            if (description.length() > 150)
//            {
//                description = description.substring(0, 150);
//            }
//            descriptionView.setText(description);
//        }
    }


    protected void setColorBar(View view, Cursor cursor)
    {
//        View colorbar = getView(view, R.id.colorbar);
//        if (colorbar != null)
//        {
//            colorbar.setBackgroundColor(TaskFieldAdapters.LIST_COLOR.get(cursor));
//        }
    }


    @SuppressLint("NewApi")
    protected void resetFlingView(View view)
    {
        View flingContentView = getView(view, getFlingContentViewId());
        if (flingContentView == null)
        {
            flingContentView = view;
        }

        if (flingContentView.getTranslationX() != 0)
        {
            flingContentView.setTranslationX(0);
            flingContentView.setAlpha(1);
        }
    }


    protected <T extends View> T getView(View view, int viewId)
    {
        SparseArrayCompat<View> viewHolder = (SparseArrayCompat<View>) view.getTag();
        if (viewHolder == null)
        {
            viewHolder = new SparseArrayCompat<View>();
            view.setTag(viewHolder);
        }
        View res = viewHolder.get(viewId);
        if (res == null)
        {
            res = view.findViewById(viewId);
            viewHolder.put(viewId, res);
        }
        return (T) res;
    }

}
