package app.omegasoftware.pontoeletronico.task;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;

/**
 * Created by W10 on 25/02/2018.
 */

public class ByProgress extends AbstractGroupingFactory
{
    /**
     * A {@link ViewDescriptor} that knows how to present the tasks in the task list.
     */
    public final ViewDescriptor TASK_VIEW_DESCRIPTOR = new BaseTaskViewDescriptor()
    {
        private int mFlingContentViewId = R.id.flingContentView;
        private int mFlingRevealLeftViewId = R.id.fling_reveal_left;
        private int mFlingRevealRightViewId = R.id.fling_reveal_right;


        @Override
        public void populateView(View view, Cursor cursor, BaseExpandableListAdapter adapter, int flags)
        {
            TextView title = (TextView) getView(view, android.R.id.title);
//            boolean isClosed = cursor.getInt(13) > 0;

            //resetFlingView(view);

            if (title != null)
            {
                String text = getTitle(cursor, view.getContext());

                title.setText(text);
//                if (isClosed)
//                {
//                    title.setPaintFlags(title.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//                }
//                else
//                {
//                    title.setPaintFlags(title.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
//                }
            }

            //setDueDate((TextView) getView(view, R.id.task_due_date), null, INSTANCE_DUE_ADAPTER.get(cursor), isClosed);

//            View divider = getView(view, R.id.divider);
//            if (divider != null)
//            {
//                divider.setVisibility((flags & FLAG_IS_LAST_CHILD) != 0 ? View.GONE : View.VISIBLE);
//            }

            // display priority
//            int priority = TaskFieldAdapters.PRIORITY.get(cursor);
//            View priorityView = getView(view, R.id.task_priority_view_medium);
//            priorityView.setBackgroundResource(android.R.color.transparent);
//            priorityView.setVisibility(View.VISIBLE);
//
//            if (priority > 0 && priority < 5)
//            {
//                priorityView.setBackgroundResource(R.color.priority_red);
//            }
//            if (priority == 5)
//            {
//                priorityView.setBackgroundResource(R.color.priority_yellow);
//            }
//            if (priority > 5 && priority <= 9)
//            {
//                priorityView.setBackgroundResource(R.color.priority_green);
//            }

//            new ProgressBackgroundView(getView(view, R.id.percentage_background_view))
//                    .update(new NullSafe<>(TaskFieldAdapters.PERCENT_COMPLETE.get(cursor)));

//            setColorBar(view, cursor);
            setDescription(view, cursor);
//            setOverlay(view, cursor.getPosition(), cursor.getCount());
        }

        /**
         * Return the title of a list group.
         *
         * @param cursor
         *         A {@link Cursor} pointing to the current group.
         *
         * @return A {@link String} with the group name.
         */
        private String getTitle(Cursor cursor, Context context)
        {
            String titulo = context.getString(R.string.tarefa) + " " +  cursor.getString(0).replace('-','#');


            titulo = HelperString.substring( titulo, 30);
            return titulo;
        }

        @Override
        public int getView()
        {

            return R.layout.task_list_element;
        }


        @Override
        public int getFlingContentViewId()
        {
            return mFlingContentViewId;
        }


        @Override
        public int getFlingRevealLeftViewId()
        {
            return mFlingRevealLeftViewId;
        }


        @Override
        public int getFlingRevealRightViewId()
        {
            return mFlingRevealRightViewId;
        }
    };

    /**
     * A {@link ViewDescriptor} that knows how to present list groups.
     */
    public final ViewDescriptor GROUP_VIEW_DESCRIPTOR = new ViewDescriptor()
    {

        @Override
        public void populateView(View view, Cursor cursor, BaseExpandableListAdapter adapter, int flags)
        {
            int position = cursor.getPosition();

            // set list title
            TextView title = (TextView) view.findViewById(android.R.id.title);
            if (title != null)
            {
                title.setText(getTitle(cursor, view.getContext()));
            }
//
//            // set list account
//            TextView text1 = (TextView) view.findViewById(android.R.id.text1);
//            if (text1 != null)
//            {
//                text1.setText(cursor.getString(3));
//            }
//
//            // set list elements
            TextView text2 = (TextView) view.findViewById(android.R.id.text2);
            int childrenCount = adapter.getChildrenCount(position);
            if (text2 != null && ((ExpandableGroupDescriptorAdapter) adapter).childCursorLoaded(position))
            {
                Resources res = view.getContext().getResources();

                text2.setText(res.getQuantityString(R.plurals.number_of_tasks, childrenCount, childrenCount));
            }
//
//            // show/hide divider
            View divider = view.findViewById(R.id.divider);
            if (divider != null)
            {
                divider.setVisibility((flags & FLAG_IS_EXPANDED) != 0 && childrenCount > 0 ? View.VISIBLE : View.GONE);
            }
//
//            View colorbar1 = view.findViewById(R.id.colorbar1);
//            View colorbar2 = view.findViewById(R.id.colorbar2);
//            View quickAddTask = view.findViewById(R.id.quick_add_task);
//            if (quickAddTask != null)
//            {
//                quickAddTask.setOnClickListener(quickAddClickListener);
//                quickAddTask.setTag(cursor.getLong(cursor.getColumnIndex(TaskLists._ID)));
//            }
//
//            if ((flags & FLAG_IS_EXPANDED) != 0)
//            {
//                if (colorbar1 != null)
//                {
//                    colorbar1.setBackgroundColor(TaskFieldAdapters.LIST_COLOR.get(cursor));
//                    colorbar1.setVisibility(View.VISIBLE);
//                }
//                if (colorbar2 != null)
//                {
//                    colorbar2.setVisibility(View.GONE);
//                }
//
//                // show quick add and hide task count
//                if (quickAddTask != null)
//                {
//                    quickAddTask.setVisibility(View.VISIBLE);
//                }
//                if (text2 != null)
//                {
//                    text2.setVisibility(View.GONE);
//                }
//            }
//            else
//            {
//                if (colorbar1 != null)
//                {
//                    colorbar1.setVisibility(View.INVISIBLE);
//                }
//                if (colorbar2 != null)
//                {
//                    colorbar2.setBackgroundColor(TaskFieldAdapters.LIST_COLOR.get(cursor));
//                    colorbar2.setVisibility(View.VISIBLE);
//                }
//
//                // hide quick add and show task count
//                if (quickAddTask != null)
//                {
//                    quickAddTask.setVisibility(View.GONE);
//                }
//                if (text2 != null)
//                {
//                    text2.setVisibility(View.VISIBLE);
//                }
//            }
        }


//        private final OnClickListener quickAddClickListener = new OnClickListener()
//        {
//
//            @Override
//            public void onClick(View v)
//            {
//                Long tag = (Long) v.getTag();
//                if (tag != null)
//                {
//                    QuickAddDialogFragment.newInstance(tag)
//                            .show(mActivity.getSupportFragmentManager(), null);
//                }
//            }
//        };


        @Override
        public int getView()
        {

            return R.layout.task_list_group;
        }


        /**
         * Return the title of a list group.
         *
         * @param cursor
         *         A {@link Cursor} pointing to the current group.
         *
         * @return A {@link String} with the group name.
         */
        private String getTitle(Cursor cursor, Context context)
        {
            return context.getString( cursor.getInt(4));
        }


        @Override
        public int getFlingContentViewId()
        {
            return -1;
        }


        @Override
        public int getFlingRevealLeftViewId()
        {
            return -1;
        }


        @Override
        public int getFlingRevealRightViewId()
        {
            return -1;
        }

    };

    private final FragmentActivity mActivity;


    public ByProgress(String authority, FragmentActivity activity)
    {
        super(authority);
        mActivity = activity;
    }


    @Override
    public ExpandableChildDescriptor makeExpandableChildDescriptor(String authority)
    {

        return new ProgressChildDescriptor().setViewDescriptor(TASK_VIEW_DESCRIPTOR);



//        return new ExpandableChildDescriptor(
//                TarefaProvider.getContentUri()
//                , new String[]{ EXTDAOTarefa.ID, EXTDAOTarefa.TITULO}
//                , " corporacao_id_INT = ? "
//                , new String[]{ OmegaSecurity.getIdCorporacao()}
//                , "cadastro_sec DESC ").setViewDescriptor(TASK_VIEW_DESCRIPTOR);
    }


    @Override
    public ExpandableGroupDescriptor makeExpandableGroupDescriptor(String authority)
    {

//        return new ExpandableGroupDescriptor(
//                new ProgressCursorLoaderFactory(ProgressCursorFactory.DEFAULT_PROJECTION),
//                makeExpandableChildDescriptor(authority)
//        ).setViewDescriptor(GROUP_VIEW_DESCRIPTOR);

        return new ExpandableGroupDescriptor(
                new ProgressCursorLoaderFactory(ProgressCursorFactory.DEFAULT_PROJECTION),
                makeExpandableChildDescriptor(authority)
        ).setViewDescriptor(GROUP_VIEW_DESCRIPTOR);
//        return null;
    }



    @Override
    public int getId()
    {
        return R.id.task_group_by_list;
    }

}
