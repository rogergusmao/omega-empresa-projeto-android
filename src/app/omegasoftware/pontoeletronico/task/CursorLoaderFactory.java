package app.omegasoftware.pontoeletronico.task;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

/**
 * Created by W10 on 25/02/2018.
 */

public class CursorLoaderFactory extends AbstractCursorLoaderFactory
{
    private final Uri mUri;
    private final String[] mProjection;
    private final String mSelection;
    private final String[] mSelectionArgs;
    private final String mSortOrder;


    /**
     * Initialize the Factory with the arguments to initialize the CursorLoader. The parameters are just passed to
     * {@link CursorLoader#CursorLoader(Context, Uri, String[], String, String[], String)}.
     *
     * @param uri
     * @param projection
     * @param selection
     * @param selectionArgs
     * @param sortOrder
     */
    public CursorLoaderFactory(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        mUri = uri;
        mProjection = projection;
        mSelection = selection;
        mSelectionArgs = selectionArgs;
        mSortOrder = sortOrder;
    }


    @Override
    public Loader<Cursor> getLoader(Context context)
    {
        return new CursorLoader(context, mUri, mProjection, mSelection, mSelectionArgs, mSortOrder);
    }

}
