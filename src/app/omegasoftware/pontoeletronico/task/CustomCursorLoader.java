package app.omegasoftware.pontoeletronico.task;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.Loader;

/**
 * Created by W10 on 25/02/2018.
 */

public class CustomCursorLoader extends Loader<Cursor>
{
    /**
     * The current Cursor.
     */
    private Cursor mCursor;

    /**
     * The factory that creates our Cursor.
     */
    private final AbstractCustomCursorFactory mCursorFactory;


    public CustomCursorLoader(Context context, AbstractCustomCursorFactory factory)
    {
        super(context);

        mCursorFactory = factory;
    }


    @Override
    public void deliverResult(Cursor cursor)
    {
        if (isReset())
        {
            // An async query came in while the loader is stopped
            if (cursor != null && !cursor.isClosed())
            {
                cursor.close();
            }
            return;
        }
        Cursor oldCursor = mCursor;
        mCursor = cursor;

        if (isStarted())
        {
            super.deliverResult(cursor);
        }

        if (oldCursor != null && oldCursor != cursor && !oldCursor.isClosed())
        {
            oldCursor.close();
        }
    }


    @Override
    protected void onStartLoading()
    {
        if (mCursor == null || takeContentChanged())
        {
            // deliver a new cursor, deliverResult will take care of the old one if any
            deliverResult(mCursorFactory.getCursor());
        }
        else
        {
            // just deliver the same cursor
            deliverResult(mCursor);
        }
    }


    @Override
    protected void onForceLoad()
    {
        // just create a new cursor, deliverResult will take care of storing the new cursor and closing the old one
        deliverResult(mCursorFactory.getCursor());
    }


    @Override
    protected void onReset()
    {
        super.onReset();

        onStopLoading();

        // ensure the cursor is closed before we release it
        if (mCursor != null && !mCursor.isClosed())
        {
            mCursor.close();
        }

        mCursor = null;
    }
}
