package app.omegasoftware.pontoeletronico.task;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;

/**
 * Created by W10 on 25/02/2018.
 */

public class EmptyCursorLoaderFactory extends CustomCursorLoader
{
    public EmptyCursorLoaderFactory(Context context, String[] projection)
    {
        super(context, new AbstractCustomCursorFactory(projection)
        {

            @Override
            public Cursor getCursor()
            {
                return new MatrixCursor(mProjection);
            }
        });
    }

}
