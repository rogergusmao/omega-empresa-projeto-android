package app.omegasoftware.pontoeletronico.task;

import android.content.Context;

import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;

/**
 * Created by W10 on 24/02/2018.
 */

public abstract class ExpandableChildDescriptor
{
    protected Uri mUri;



   private ViewDescriptor mViewDescriptor;

    /**
     * Create a new { ExpandableChildDescriptor} using the given values.
     *
     * @param uri

     */
    protected ExpandableChildDescriptor(Uri uri)
    {
        mUri = uri;
    }


    /**
     * Get a new { CursorLoader} and update it's selection arguments with the values in {@code cursor} as defined by {@code selectionColumns} in
     * { #ExpandableChildDescriptor(Uri, String[], String, String, int...)}. Also applies any selection defined by <code>filter</code>.
     *
     * @param context
     *         A { Context}.
     * @param cursor
     *         The { Cursor} containing the selection.
     * @param filter
     *
     *         An additional { AbstractFilter} to apply to the selection of the cursor.
     * @return A new { CursorLoader} instance.
     */
    //public abstract CursorLoader getCursorLoader(Context context, Cursor cursor, AbstractFilter filter);



    /**
     * Get a new { CursorLoader} and update it's selection arguments with the values in {@code cursor} as definded by {@code selectionColumns} in
     * { #ExpandableChildDescriptor(Uri, String[], String, String, int...)}
     *
     * @param context
     *         A { Context}.
     * @param cursor
     *         The { Cursor} containing the selection.
     *
     * @return A new { CursorLoader} instance.
     */
    public abstract CursorLoader getCursorLoader(Context context, Cursor cursor);




    /**
     * Set a view descriptor to use to display the children.
     *
     * @param descriptor
     *         The { ViewDescriptor} to use.
     *
     * @return This instance.
     */
    public ExpandableChildDescriptor setViewDescriptor(ViewDescriptor descriptor)
    {
        mViewDescriptor = descriptor;
        return this;
    }


    /**
     * Get the { ViewDescriptor} to use when preparing the views to display the children.
     *
     * @return The { ViewDescriptor}.
     */
    public ViewDescriptor getViewDescriptor()
    {
        return mViewDescriptor;
    }
}
