package app.omegasoftware.pontoeletronico.task;

import android.content.Context;


import android.database.Cursor;
import android.support.v4.content.Loader;

/**
 * Created by W10 on 24/02/2018.
 */

public class ExpandableGroupDescriptor
{
    private final AbstractCursorLoaderFactory mLoaderFactory;
    private final ExpandableChildDescriptor mChildDescriptor;
    private ViewDescriptor mGroupViewDescriptor;


    /**
     * Create a new descriptor for expandable groups.
     *
     * @param loaderFactory
     *         An {@link AbstractCursorLoaderFactory} instance that can return { CursorLoader}s that load the groups.
     * @param childDescriptor
     *         An {@link ExpandableChildDescriptor} that knwos how to load the children of the groups.
     */
    public ExpandableGroupDescriptor(AbstractCursorLoaderFactory loaderFactory, ExpandableChildDescriptor childDescriptor)
    {
        mLoaderFactory = loaderFactory;
        mChildDescriptor = childDescriptor;
    }


    /**
     * Get a {@link Loader} that loads the groups.
     *
     * @param context
     *         A {@link Context}.
     *
     * @return A {@link Loader}.
     */
    public Loader<Cursor> getGroupCursorLoader(Context context)
    {
        return mLoaderFactory.getLoader(context);
    }


    /**
     * Get a {@link Loader} that loads the children of the group at the current position in a {@link Cursor}.
     *
     * @param context
     *         A {@link Context}.
     * @param cursor
     *         A {@link Cursor} that points to the group to load.
     *
     * @return A {@link Loader}.
     */
    public Loader<Cursor> getChildCursorLoader(Context context, Cursor cursor)
    {
        return mChildDescriptor.getCursorLoader(context, cursor);
    }

    public Loader<Cursor> getChildCursorLoader(Context context, Cursor cursor, AbstractFilter filter)
    {
        return mChildDescriptor.getCursorLoader(context, cursor);
    }



    /**
     * Set the {@link ViewDescriptor} that knows how to populate the group views.
     *
     * @param descriptor
     *         The {@link ViewDescriptor} for the group headers.
     *
     * @return This instance.
     */
    public ExpandableGroupDescriptor setViewDescriptor(ViewDescriptor descriptor)
    {
        mGroupViewDescriptor = descriptor;
        return this;
    }


    /**
     * Get the {@link ViewDescriptor} that knows how to populate the group views.
     *
     * @return A {@link ViewDescriptor}.
     */
    public ViewDescriptor getGroupViewDescriptor()
    {
        return mGroupViewDescriptor;
    }


    /**
     * Get the {@link ViewDescriptor} that knows how to populate the child views.
     *
     * @return A {@link ViewDescriptor}.
     */
    public ViewDescriptor getElementViewDescriptor()
    {
        return mChildDescriptor.getViewDescriptor();
    }

}
