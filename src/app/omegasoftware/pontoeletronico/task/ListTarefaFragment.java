package app.omegasoftware.pontoeletronico.task;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import app.omegasoftware.pontoeletronico.R;

/**
 * Created by W10 on 23/02/2018.
 */


public class ListTarefaFragment extends Fragment {


        public ListTarefaFragment(){}

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_list_tarefa, container, false);

            ArrayList<TarefaItem> listContact = GetlistContact();
            ListView lv = (ListView)rootView.findViewById(R.id.list);
            lv.setAdapter(new ListTarefaFragmentAdapter(getActivity(), listContact));

            return rootView;
        }

        private ArrayList<TarefaItem> GetlistContact(){
            ArrayList<TarefaItem> contactlist = new ArrayList<TarefaItem>();

            TarefaItem contact = new TarefaItem();

            contact.titulo = "Topher";

            contactlist.add(contact);

            contact = new TarefaItem();
            contact.titulo = "Jean";

            contactlist.add(contact);

            contact = new TarefaItem();
            contact.titulo = "Andrew";

            contactlist.add(contact);

            return contactlist;
        }
    public class TarefaItem{
        public String titulo;
    }
    public class ListTarefaFragmentAdapter extends BaseAdapter {
        private ArrayList<ListTarefaFragment.TarefaItem> listContact;

        private LayoutInflater mInflater;

        public ListTarefaFragmentAdapter(Context photosFragment, ArrayList<ListTarefaFragment.TarefaItem> results){
            listContact = results;
            mInflater = LayoutInflater.from(photosFragment);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return listContact.size();
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return listContact.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return arg0;
        }


        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            ViewHolder holder;
            if(convertView == null){
                convertView = mInflater.inflate(R.layout.tarefa_item, null);
                holder = new ViewHolder();
                holder.txtname = (TextView) convertView.findViewById(R.id.tv_titulo);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.txtname.setText(listContact.get(position).titulo);

            return convertView;
        }

        class ViewHolder{
            TextView txtname;
        }
    }
}

