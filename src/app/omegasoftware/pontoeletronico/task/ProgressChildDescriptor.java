package app.omegasoftware.pontoeletronico.task;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;

import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORegistroEstado;

/**
 * Created by W10 on 24/02/2018.
 */

public class ProgressChildDescriptor extends  ExpandableChildDescriptor
{




    /**
     * Create a new { ExpandableChildDescriptor} using the given values.
     *
     * param uri

     */
    protected ProgressChildDescriptor()
    {
        super(TarefaProvider.getContentUri());

    }



    /**
     * Get a new { CursorLoader} and update it's selection arguments with the values in {@code cursor} as definded by {@code selectionColumns} in
     * { #ExpandableChildDescriptor(Uri, String[], String, String, int...)}
     *
     * @param context
     *         A { Context}.
     * @param cursor
     *         The { Cursor} containing the selection.
     *
     * @return A new { CursorLoader} instance.
     */
    public CursorLoader getCursorLoader(Context context, Cursor cursor)
    {
        //return getCursorLoader(context, cursor, null);



        String q ="SELECT id _id, titulo, descricao, cadastro_SEC, cadastro_OFFSEC, prazo_SEC, prazo_OFFSEC" +
                "  FROM tarefa" +
                " WHERE corporacao_id_INT = ? " ;
        String porcentagemMin = cursor.getString(1);
        String porcentagemMax = cursor.getString(2);
        int intPorcentagemMax = cursor.getInt(2);
        String[] parametros = null;
        if(intPorcentagemMax == 100){
            q += "   AND registro_estado_id_INT = ? ";
            parametros = new String[]{
                    OmegaSecurity.getIdCorporacao(),
                    String.valueOf( EXTDAORegistroEstado.TIPO.finalizada.getId()),
                    OmegaSecurity.getIdUsuario()
            };

        }else if(porcentagemMin != null
                && porcentagemMax != null
                && cursor.getInt(0) == 2 // se o id for relativo a primeira faiza de porcentagem
                ) {
            q += "   AND ((percentual_completo_INT > ? " +
                    "   AND percentual_completo_INT <= ?) " +
                    " OR registro_estado_id_INT = ?)";
            parametros = new String[]{
                    OmegaSecurity.getIdCorporacao(),
                    porcentagemMin,
                    porcentagemMax,
                    String.valueOf( EXTDAORegistroEstado.TIPO.em_execucao.getId()),
                    OmegaSecurity.getIdUsuario()

            };
        }
        else if(porcentagemMin != null && porcentagemMax != null) {
            q += "   AND ( percentual_completo_INT > ? " +
                    "   AND percentual_completo_INT <= ?)";
            parametros = new String[]{
                    OmegaSecurity.getIdCorporacao(),
                    porcentagemMin,
                    porcentagemMax,
                    OmegaSecurity.getIdUsuario()
            };
        }
        else if(porcentagemMin == null && porcentagemMax != null) {
            q +=    " AND (percentual_completo_INT is null OR percentual_completo_INT <= ?) AND (registro_estado_id_INT == ? OR registro_estado_id_INT is null)" ;
            parametros = new String[]{
                    OmegaSecurity.getIdCorporacao(),
                    porcentagemMax,
                    String.valueOf( EXTDAORegistroEstado.TIPO.aguardando_inicializacao.getId()),
                    OmegaSecurity.getIdUsuario()
            };
        }
        q += "AND (usuario_id_INT = ?"
                +"      OR  usuario_id_INT is null )" ;

        q += " ORDER BY cadastro_SEC DESC, titulo ASC ";

        return new CursorLoader(context, mUri, new String[]{}, q, parametros, null);
    }


}
