package app.omegasoftware.pontoeletronico.task;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORegistro;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAORegistroEstado;

/**
 * Created by W10 on 24/02/2018.
 */

public class RegistroEstadoChildDescriptor extends  ExpandableChildDescriptor
{




    /**
     * Create a new { ExpandableChildDescriptor} using the given values.
     *
     * param uri

     */
    protected RegistroEstadoChildDescriptor()
    {
        super(TarefaProvider.getContentUri());

    }



    /**
     * Get a new { CursorLoader} and update it's selection arguments with the values in {@code cursor} as definded by {@code selectionColumns} in
     * { #ExpandableChildDescriptor(Uri, String[], String, String, int...)}
     *
     * @param context
     *         A { Context}.
     * @param cursor
     *         The { Cursor} containing the selection.
     *
     * @return A new { CursorLoader} instance.
     */
    public CursorLoader getCursorLoader(Context context, Cursor cursor)
    {
        //return getCursorLoader(context, cursor, null);



        String q ="SELECT id _id, titulo, descricao, cadastro_SEC, cadastro_OFFSEC, prazo_SEC, prazo_OFFSEC" +
                "  FROM tarefa" +
                " WHERE corporacao_id_INT = ? " ;
        Integer  idRegistroEstado = cursor.isNull(0) ? null : cursor.getInt(0);
        EXTDAORegistroEstado.TIPO tipo =null;
        if(idRegistroEstado!=null)
        tipo = EXTDAORegistroEstado.TIPO.toTipoRegistroEstado(idRegistroEstado);

        if(tipo==null)tipo =EXTDAORegistroEstado.TIPO.aguardando_inicializacao;
        String[] parametros = new String[]{
                OmegaSecurity.getIdCorporacao(),
                String.valueOf( tipo.getId()),
                OmegaSecurity.getIdUsuario()
        };
        switch (tipo){
            case aguardando_inicializacao:
                q += " AND (registro_estado_id_INT is null OR registro_estado_id_INT =?)";
                break;
            default:
                q += " AND ( registro_estado_id_INT =?)";
                break;
        }


        q += "AND (usuario_id_INT = ?"
                +"      OR  usuario_id_INT is null )" ;

        q += " ORDER BY cadastro_SEC DESC, titulo ASC ";

        return new CursorLoader(context, mUri, new String[]{}, q, parametros, null);
    }


}
