/*
 * Copyright 2017 dmfs GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package app.omegasoftware.pontoeletronico.task;

import android.database.Cursor;
import android.database.MatrixCursor;

import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;


/**
 * A factory that builds shiny new {@link Cursor}s with progress information.
 *
 * @author Tobias Reinsch <tobias@dmfs.org>
 */
public final class RegistroEstadoCursorFactory extends AbstractCustomCursorFactory
{


    private static final Integer[] ROW_AGUARDANDO_INICIALIZACAO = new Integer[] { EXTDAOTarefa.ESTADO_TAREFA.AGUARDANDO_INICIALIZACAO.getId(), R.string.aguardando_inicializacao };
    private static final Integer[] ROW_FINALIZADA = new Integer[] { EXTDAOTarefa.ESTADO_TAREFA.FINALIZADA.getId(),  R.string.finalizada };
    private static final Integer[] ROW_EM_EXECUCAO= new Integer[] { EXTDAOTarefa.ESTADO_TAREFA.EM_EXECUCAO.getId(),  R.string.em_execucao};
    private static final Integer[] ROW_CANCELADA= new Integer[] { EXTDAOTarefa.ESTADO_TAREFA.CANCELADA.getId(),  R.string.cancelada};


    public static final String[] DEFAULT_PROJECTION = new String[] { "_id", "nome" };
    /**
     * Initialize the factory with the given projection.
     *
     * @param projection
     *         An array of column names.
     */
    public RegistroEstadoCursorFactory(String[] projection)
    {
        super(projection);
    }


    @Override
    public Cursor getCursor()
    {
        MatrixCursor result = new MatrixCursor(DEFAULT_PROJECTION);
        result.addRow(ROW_AGUARDANDO_INICIALIZACAO);
        result.addRow(ROW_EM_EXECUCAO);
        result.addRow(ROW_FINALIZADA);
        result.addRow(ROW_CANCELADA);
        return result;
    }
}
