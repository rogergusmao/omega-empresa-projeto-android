package app.omegasoftware.pontoeletronico.task;

/**
 * Created by W10 on 25/02/2018.
 */


import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.Loader;

/**
 * An {@link AbstractCursorLoaderFactory} that returns {@link CursorLoaderFactory} that know how to load cursors with progress information as values.
 *
 * @author Tobias Reinsch <tobias@dmfs.org>
 */
public class RegistroEstadoCursorLoaderFactory extends AbstractCursorLoaderFactory
{

    private final String[] mProjection;


    public RegistroEstadoCursorLoaderFactory(String[] projection)
    {
        mProjection = projection;
    }


    @Override
    public Loader<Cursor> getLoader(Context context)
    {
        return new CustomCursorLoader(context, new RegistroEstadoCursorFactory(mProjection));
    }
}
