package app.omegasoftware.pontoeletronico.task;

import android.support.v4.app.Fragment;

/**
 * Created by W10 on 25/02/2018.
 */

public final class SafeFragmentUiRunnable implements Runnable
{
    private final Fragment mFragment;
    private final Runnable mDelegate;


    public SafeFragmentUiRunnable(Fragment fragment, Runnable delegate)
    {
        mFragment = fragment;
        mDelegate = delegate;
    }


    @Override
    public void run()
    {
        if (mFragment.isAdded() && mFragment.getActivity() != null)
        {
            mDelegate.run();
        }
    }
}
