package app.omegasoftware.pontoeletronico.task;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.database.Database;
import app.omegasoftware.pontoeletronico.database.EXTDAO.DatabasePontoEletronico;
import app.omegasoftware.pontoeletronico.database.EXTDAO.EXTDAOTarefa;
import app.omegasoftware.pontoeletronico.database.OmegaDatabaseException;

/**
 * Created by W10 on 25/02/2018.
 */

public class TarefaProvider extends ContentProvider {

    public static final String PROVIDER_NAME = "workoffline.pontoeletronico";

    /** A uri to do operations on cust_master table. A content provider is identified by its uri */
    public static final Uri getContentUri(){ return Uri.parse("content://" + PROVIDER_NAME + "/tarefas" ); }
    Database db;
    /** Constants to identify the requested operation */
    private static final int TAREFAS = 1;

    private static final UriMatcher uriMatcher ;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "tarefas", TAREFAS);
    }

    @Override
    public boolean onCreate() {
        try {
            db = new DatabasePontoEletronico(this.getContext());
            return true;
        } catch (OmegaDatabaseException e) {
            SingletonLog.insereErro(e, SingletonLog.TIPO.UTIL_ANDROID);
            return false;
        }
    }

    /** A callback method which is by the default content uri */
    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        if(uriMatcher.match(uri)==TAREFAS){
//            return db.query(EXTDAOTarefa.NAME,
//                    projection,
//                    selection,
//                    selectionArgs,
//                    null,
//                    null,
//                    sortOrder);

            return db.rawQuery(
                    selection, selectionArgs);

        }else{
            return null;
        }
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
