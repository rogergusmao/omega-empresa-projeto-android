package app.omegasoftware.pontoeletronico.task;

import android.content.ContentUris;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;

import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.TabletActivities.detail.DetailTarefaActivityMobile;
import app.omegasoftware.pontoeletronico.TabletActivities.forms.FormTarefaActivityMobile;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;

/**
 * Created by W10 on 24/02/2018.
 */

public class TaskListActivity extends FragmentActivity implements TaskListFragment.Callbacks{


    /**
     * Tells the activity to display the details of the task with the URI from the intent data.
     **/
    public static final String EXTRA_DISPLAY_TASK = "org.dmfs.tasks.DISPLAY_TASK";

    /**
     * Tells the activity to select the task in the list with the URI from the intent data.
     **/
    public static final String EXTRA_FORCE_LIST_SELECTION = "org.dmfs.tasks.FORCE_LIST_SELECTION";

    private static final String TAG = "TaskListActivity";

    private final static int REQUEST_CODE_NEW_TASK = 2924;

    /**
     * The time to wait for a new key before updating the search view.
     */
    private final static int SEARCH_UPDATE_DELAY = 400; // ms

    private final static String DETAIL_FRAGMENT_TAG = "taskListActivity.ViewTaskFragment";

    /**
     * Array of {@link ExpandableGroupDescriptor}s.
     */
    private AbstractGroupingFactory[] mGroupingFactories;

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet device.
     */
//    private boolean mTwoPane;



//    @Retain(permanent = true)
    private int mCurrentPageId;

    /**
     * The current pager position
     **/
    private int mCurrentPagePosition = 0;

    private int mPreviousPagePosition = -1;

    private String mAuthority;





    private final Handler mHandler = new Handler();

//    private SearchHistoryHelper mSearchHistoryHelper;

    private boolean mAutoExpandSearchView = false;

    /**
     * The Uri of the task to display/highlight in the list view.
     **/
//    @Retain
    private Uri mSelectedTaskUri;

    /**
     * The Uri of the task to display/highlight in the list view coming from the widget.
     **/
    private Uri mSelectedTaskUriOnLaunch;

    /**
     * Indicates to display the two pane layout with details
     **/
//    @Retain
    private boolean mShouldShowDetails = false;

    /**
     * Indicates to show ViewTaskActivity when rotating to single pane.
     **/
//    @Retain
    private boolean mShouldSwitchToDetail = false;

    /**
     * Indicates the TaskListFragments to select/highlight the mSelectedTaskUri item
     **/
    private boolean mShouldSelectTaskListItem = false;

    /**
     * Indicates a transient state after rotation to redirect to the TaskViewActivtiy
     **/
    private boolean mTransientState = false;


    private ViewPager mViewPager;
    private AgrupadorTarefaAdapter mPagerAdapter;
    ImageButton mImageButtonCadastrar;


    public TaskListActivity() {

    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_task_list);

        mPagerAdapter = new AgrupadorTarefaAdapter(getSupportFragmentManager());


        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mPagerAdapter);
        mImageButtonCadastrar = (ImageButton)findViewById(R.id.btn_cadastrar);
        mImageButtonCadastrar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent vIntent = new Intent(
                                TaskListActivity.this,
                                FormTarefaActivityMobile.class);

                        startActivity(vIntent);
                    }
                }
        );

        ImageButton btnVoltarDesktop = (ImageButton) findViewById(R.id.btn_voltar);
        btnVoltarDesktop.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
//        mViewPager.setCurrentItem(1);
    }

    //ByProgress byList=   new ByProgress(null, this);
    ByRegistroEstado byList=   new ByRegistroEstado(null, this);
    public ExpandableGroupDescriptor getGroupDescriptor(int pageId)
    {
//        for (AbstractGroupingFactory factory : mGroupingFactories)
//        {
//            if (factory.getId() == pageId)
//            {
//                return factory.getExpandableGroupDescriptor();
//            }
//        }
        return byList.getExpandableGroupDescriptor();
    }




    @Override
    protected void onResume()
    {
//        updateTitle(mCurrentPageId);
        super.onResume();
    }


    @Override
    protected void onNewIntent(Intent intent)
    {
        resolveIntentAction(intent);
        super.onNewIntent(intent);
    }


    @Override
    protected void onDestroy()
    {
        super.onDestroy();
//        mSearchHistoryHelper.close();
    }

    private void resolveIntentAction(Intent intent)
    {
        // check which task should be selected
//        if (intent.hasExtra(EXTRA_DISPLAY_TASK))
//
//        {
//            mShouldSwitchToDetail = true;
//            mSelectedTaskUri = intent.getData();
//        }
//
//        if (intent != null && intent.hasExtra(EXTRA_DISPLAY_TASK) && intent.getBooleanExtra(EXTRA_FORCE_LIST_SELECTION, true) && mTwoPane)
//        {
//            mShouldSwitchToDetail = true;
//            Uri newSelection = intent.getData();
//            mSelectedTaskUriOnLaunch = newSelection;
//            mShouldSelectTaskListItem = true;
//            if (mPagerAdapter != null)
//            {
//                mPagerAdapter.notifyDataSetChanged();
//            }
//        }
//        else
//        {
//            mSelectedTaskUriOnLaunch = null;
//            mShouldSelectTaskListItem = false;
//        }
    }
    /**
     * Callback method from {@link TaskListFragment.Callbacks} indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(Uri uri, boolean forceReload, int pagePosition)
    {
        // only accept selections from the current visible task fragment or the activity itself
        if (pagePosition == -1 || pagePosition == mCurrentPagePosition)
//        {
//            if (mTwoPane)
//            {
//                mShouldShowDetails = true;
//                if (forceReload)
//                {
//                    mSelectedTaskUri = null;
//                    mShouldSwitchToDetail = false;
//                }
//                loadTaskDetailFragment(uri);
//            }
//            else
                if (forceReload)
            {
                mSelectedTaskUri = uri;

                // In single-pane mode, simply start the detail activity
                // for the selected item ID.
//                Intent detailIntent = new Intent(Intent.ACTION_VIEW);
//                detailIntent.setData(uri);
//                startActivity(detailIntent);
//                mShouldSwitchToDetail = false;
                Intent vIntent = new Intent(
                        this,
                        DetailTarefaActivityMobile.class);
                long id= ContentUris.parseId(uri);
                vIntent.putExtra(OmegaConfiguration.SEARCH_FIELD_ID, String.valueOf(id));
                startActivity(vIntent);
//

            }
//        }
    }

    @Override
    public void onAddNewTask()
    {
//        Intent editTaskIntent = new Intent(Intent.ACTION_INSERT);
//        editTaskIntent.setData(Tasks.getContentUri(mAuthority));
//        startActivityForResult(editTaskIntent, REQUEST_CODE_NEW_TASK);
    }



    public Uri getSelectedTaskUri()
    {
        if (mShouldSelectTaskListItem)
        {
            return mSelectedTaskUriOnLaunch;
        }
        return null;
    }


    public boolean isInTransientState()
    {
        return mTransientState;
    }

}