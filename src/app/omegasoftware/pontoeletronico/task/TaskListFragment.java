package app.omegasoftware.pontoeletronico.task;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;

import com.google.android.gms.tasks.Tasks;

import app.omegasoftware.pontoeletronico.R;


/**
 * Created by W10 on 24/02/2018.
 */

public class TaskListFragment extends Fragment implements  LoaderManager.LoaderCallbacks<Cursor>, OnChildLoadedListener{


    @SuppressWarnings("unused")
    private static final String TAG = "org.dmfs.tasks.TaskListFragment";

    private final static String ARG_INSTANCE_ID = "instance_id";
    private final static String ARG_TWO_PANE_LAYOUT = "two_pane_layout";

    private static final long INTERVAL_LISTVIEW_REDRAW = 60000;

    /**
     * A filter to hide completed tasks.
     */
    private final static AbstractFilter COMPLETED_FILTER = null;//new ConstantFilter(Tasks.IS_CLOSED + "=0");

    /**
     * The group descriptor to use. At present this can be either {link ByDueDate#GROUP_DESCRIPTOR}, {link ByCompleted#GROUP_DESCRIPTOR} or
     * {link ByProgress#GROUP_DESCRIPTOR}.
     */
    private ExpandableGroupDescriptor mGroupDescriptor;

    /**
     * The fragment's current callback object, which is notified of list item clicks.
     */
    private Callbacks mCallbacks;

//    @Retain(permanent = true, instanceNSField = "mInstancePosition")
    private int mActivatedPositionGroup = ExpandableListView.INVALID_POSITION;
//    @Retain(permanent = true, instanceNSField = "mInstancePosition")
    private int mActivatedPositionChild = ExpandableListView.INVALID_POSITION;

    private ExpandableListView mExpandableListView;
    private Context mAppContext;
    private ExpandableGroupDescriptorAdapter mAdapter;
    private Handler mHandler;
//    @Retain(permanent = true, instanceNSField = "mInstancePosition")
    private long[] mSavedExpandedGroups = null;
//    @Retain(permanent = true, instanceNSField = "mInstancePosition")
    private boolean mSavedCompletedFilter;

//    @Parameter(key = ARG_INSTANCE_ID)
    private int mInstancePosition;

//    @Parameter(key = ARG_TWO_PANE_LAYOUT)
    private boolean mTwoPaneLayout;

    private Loader<Cursor> mCursorLoader;
    private String mAuthority;

    private Uri mSelectedTaskUri;

    /**
     * The child position to open when the fragment is displayed.
     **/
    private ListPosition mSelectedChildPosition;

//    @Retain
    private int mPageId = -1;

    public static TaskListFragment newInstance(int instancePosition)
    {
        TaskListFragment result = new TaskListFragment();
        Bundle args = new Bundle();
        args.putInt("id", instancePosition);


        return result;
    }



    /**
     * A runnable that periodically updates the list. We need that to update relative dates & times. TODO: we probably should move that to the adapter to update
     * only the date & times fields, not the entire list.
     */
    private Runnable mListRedrawRunnable = new SafeFragmentUiRunnable(this, new Runnable()
    {

        @Override
        public void run()
        {
            mExpandableListView.invalidateViews();
            mHandler.postDelayed(mListRedrawRunnable, INTERVAL_LISTVIEW_REDRAW);
        }
    });


    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
//        mAuthority = AuthorityUtil.taskAuthority(activity);

        mAppContext = activity.getBaseContext();

        // Activities containing this fragment must implement its callbacks.
//        if (!(activity instanceof Callbacks))
//        {
//            throw new IllegalStateException("Activity must implement fragment's callbacks.");
//        }

        mCallbacks = (Callbacks) activity;

        // load accounts early
//        Sources.loadModelAsync(activity, TaskContract.LOCAL_ACCOUNT_TYPE, this);

    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
//        setHasOptionsMenu(true);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_expandable_task_list, container, false);
        mExpandableListView = (ExpandableListView) rootView.findViewById(android.R.id.list);

        if (mGroupDescriptor == null)
        {
                loadGroupDescriptor();
        }


        prepareReload();

        return rootView;
    }

    public void loadGroupDescriptor()
    {
        if (getActivity() != null)
        {
            TaskListActivity activity = (TaskListActivity) getActivity();
            if (activity != null)
            {
                mGroupDescriptor = activity.getGroupDescriptor(mPageId);
            }
        }
    }

    /**
     * prepares the update of the view after the group descriptor was changed
     */
    public void prepareReload()
    {
        mAdapter = new ExpandableGroupDescriptorAdapter(getActivity(), getLoaderManager(), mGroupDescriptor);
        mExpandableListView.setAdapter(mAdapter);
        mExpandableListView.setOnChildClickListener((android.widget.ExpandableListView.OnChildClickListener) mTaskItemClickListener);
        mExpandableListView.setOnGroupCollapseListener((android.widget.ExpandableListView.OnGroupCollapseListener) mTaskListCollapseListener);
        mAdapter.setOnChildLoadedListener(this);
//        mAdapter.setChildCursorFilter(COMPLETED_FILTER);
        mAdapter.setChildCursorFilter(null);
        restoreFilterState();

    }


    /**
     * Remove the task with the given {@link Uri} and title, asking for confirmation first.
     *
     * @param taskUri
     *         The {@link Uri} of the atsk to remove.
     * @param taskTitle
     *         the title of the task to remove.
     *
     * @return
     */
    private void removeTask(final Uri taskUri, final String taskTitle)
    {
        new AlertDialog.Builder(getActivity()).setTitle(R.string.confirm_delete_title).setCancelable(true)
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        // nothing to do here
                    }
                }).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                // TODO: remove the task in a background task
                mAppContext.getContentResolver().delete(taskUri, null, null);
//                Snackbar.make(mExpandableListView, getString(R.string.toast_task_deleted, taskTitle), Snackbar.LENGTH_SHORT).show();
                mCallbacks.onItemSelected(null, false, -1);
            }
        }).setMessage(getString(R.string.confirm_delete_message_with_title, taskTitle)).create().show();
    }


    public void restoreFilterState()
    {
//        if (mSavedCompletedFilter)
//        {
//            mAdapter.setChildCursorFilter(mSavedCompletedFilter ? null : COMPLETED_FILTER);
//            // reload the child cursors only
//            for (int i = 0; i < mAdapter.getGroupCount(); ++i)
//            {
//                mAdapter.reloadGroup(i);
//            }
//        }

    }



    /**
     * Starts the automatic list view redraw (e.g. to display changing time values) on the next minute.
     */
    public void startAutomaticRedraw()
    {
        long now = System.currentTimeMillis();
        long millisToInterval = INTERVAL_LISTVIEW_REDRAW - (now % INTERVAL_LISTVIEW_REDRAW);
//
        mHandler.postDelayed(mListRedrawRunnable, millisToInterval);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        mExpandableListView.invalidateViews();
        startAutomaticRedraw();
        openSelectedChild();
//
//        if (mTwoPaneLayout)
//        {
//            setListViewScrollbarPositionLeft(true);
//            setActivateOnItemClick(true);
//        }
    }


    public void openSelectedChild()
    {
        if (mSelectedChildPosition != null)
        {
            // post delayed to allow the list view to finish creation
            mExpandableListView.postDelayed(new SafeFragmentUiRunnable(this, () ->
            {
                mExpandableListView.expandGroup(mSelectedChildPosition.groupPosition);
                mSelectedChildPosition.flatListPosition = mExpandableListView.getFlatListPosition(
                        ExpandableListView.getPackedPositionForChild(mSelectedChildPosition.groupPosition, mSelectedChildPosition.childPosition));

                setActivatedItem(mSelectedChildPosition.groupPosition, mSelectedChildPosition.childPosition);
                selectChildView(mExpandableListView, mSelectedChildPosition.groupPosition, mSelectedChildPosition.childPosition, true);
                mExpandableListView.smoothScrollToPosition(mSelectedChildPosition.flatListPosition);
            }), 0);
        }
    }
    /**
     * Stops the automatic list view redraw.
     */
    public void stopAutomaticRedraw()
    {
        mHandler.removeCallbacks(mListRedrawRunnable);
    }

    @Override
    public void onStart()
    {
        reloadCursor();
        super.onStart();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onPause()
    {
        // we can't rely on save instance state being called before onPause, so we get the expanded groups here again
//        if (!((TaskListActivity) getActivity()).isInTransientState())
//        {
//            mSavedExpandedGroups = mExpandableListView.getExpandedGroups();
//        }
        stopAutomaticRedraw();
        super.onPause();
    }


    private void reloadCursor()
    {
        getLoaderManager().restartLoader(-1, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mGroupDescriptor != null)
        {
            mCursorLoader = mGroupDescriptor.getGroupCursorLoader(mAppContext);
        }
        return mCursorLoader;
//        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
//        if (mSavedExpandedGroups == null)
//        {
//            mSavedExpandedGroups = mExpandableListView.getExpandedGroups();
//        }

        mAdapter.setGroupCursor(cursor);

//        if (mSavedExpandedGroups != null)
//        {
//            mExpandableListView.expandGroups(mSavedExpandedGroups);
//            if (!((TaskListActivity) getActivity()).isInTransientState())
//            {
//                mSavedExpandedGroups = null;
//            }
//        }
//
        mHandler.post(new SafeFragmentUiRunnable(this, () -> mAdapter.reloadLoadedGroups()));
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        mAdapter.changeCursor(null);
    }


    /**
     * A callback interface that all activities containing this fragment must implement. This mechanism allows activities to be notified of item selections.
     */
    public interface Callbacks
    {
        /**
         * Callback for when an item has been selected.
         *
         * @param taskUri
         *         The {@link Uri} of the selected task.
         * @param forceReload
         *         Whether to reload the task or not.
         *  sender
         *         The sender of the callback.
         */
        public void onItemSelected(Uri taskUri, boolean forceReload, int pagePosition);

        public ExpandableGroupDescriptor getGroupDescriptor(int position);

        public void onAddNewTask();
    }


    public void setPageId(int pageId)
    {
        mPageId = pageId;
    }

    private final ExpandableListView.OnGroupCollapseListener mTaskListCollapseListener = new ExpandableListView.OnGroupCollapseListener()
    {

        @Override
        public void onGroupCollapse(int groupPosition)
        {
            if (groupPosition == mActivatedPositionGroup)
            {
                mActivatedPositionChild = ExpandableListView.INVALID_POSITION;
                mActivatedPositionGroup = ExpandableListView.INVALID_POSITION;
            }

        }
    };


    @Override
    public void onChildLoaded(final int pos, Cursor childCursor)
    {
        if (mActivatedPositionChild != ExpandableListView.INVALID_POSITION)
        {
            if (pos == mActivatedPositionGroup && mActivatedPositionChild != ExpandableListView.INVALID_POSITION)
            {
                mHandler.post(setOpenHandler);
            }
        }
        // check for child to select
//        if (mTwoPaneLayout)
//        {
//            selectChild(pos, childCursor);
//        }
    }

    private void selectChild(final int groupPosition, Cursor childCursor)
    {
//        mSelectedTaskUri = ((TaskListActivity) getActivity()).getSelectedTaskUri();
//        if (mSelectedTaskUri != null)
//        {
//            new AsyncSelectChildTask().execute(new SelectChildTaskParams(groupPosition, childCursor, mSelectedTaskUri));
//        }
    }


    /**
     * Returns the position of the task in the cursor. Returns -1 if the task is not in the cursor
     **/
    private int getSelectedChildPostion(Uri taskUri, Cursor listCursor)
    {
        if (taskUri != null && listCursor != null && listCursor.moveToFirst())
        {
            Long taskIdToSelect = Long.valueOf(taskUri.getLastPathSegment());
            do
            {
                Long taskId = listCursor.getLong(0);
                if (taskId.equals(taskIdToSelect))
                {
                    return listCursor.getPosition();
                }
            } while (listCursor.moveToNext());
        }
        return -1;
    }


    private Runnable setOpenHandler = new SafeFragmentUiRunnable(this, new Runnable()
    {
        @Override
        public void run()
        {
            selectChildView(mExpandableListView, mActivatedPositionGroup, mActivatedPositionChild, false);
//            mExpandableListView.expandGroups(mSavedExpandedGroups);
            setActivatedItem(mActivatedPositionGroup, mActivatedPositionChild);
        }
    });

    private void selectChildView(ExpandableListView expandLV, int groupPosition, int childPosition, boolean force)
    {
        if (groupPosition < mAdapter.getGroupCount() && childPosition < mAdapter.getChildrenCount(groupPosition))
        {
            // a task instance element has been clicked, get it's instance id and notify the activity
            ExpandableListAdapter listAdapter = expandLV.getExpandableListAdapter();
            Cursor cursor = (Cursor) listAdapter.getChild(groupPosition, childPosition);

            if (cursor == null)
            {
                return;
            }
            // TODO: for now we get the id of the task, not the instance, once we support recurrence we'll have to change that
            Long selectTaskId = cursor.getLong(0);
//
            if (selectTaskId != null)
            {
//                // Notify the active callbacks interface (the activity, if the fragment is attached to one) that an item has been selected.
//
//                // TODO: use the instance URI one we support recurrence
                Uri taskUri = ContentUris.withAppendedId(TarefaProvider.getContentUri(), selectTaskId);
//
//                mCallbacks.onItemSelected(taskUri, force, mInstancePosition);
                mCallbacks.onItemSelected(taskUri, force, mInstancePosition);
            }
        }
    }

    public void setActivatedItem(int groupPosition, int childPosition)
    {
        if (groupPosition != ExpandableListView.INVALID_POSITION && groupPosition < mAdapter.getGroupCount()
                && childPosition != ExpandableListView.INVALID_POSITION && childPosition < mAdapter.getChildrenCount(groupPosition))
        {
            try
            {
                mExpandableListView
                        .setItemChecked(mExpandableListView.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition)),
                                true);
            }
            catch (NullPointerException e)
            {
                // for now we just catch the NPE until we've found the reason
                // just catching it won't hurt, it's just that the list selection won't be updated properly

                // FIXME: find the actual cause and fix it
            }
        }
    }


    private final ExpandableListView.OnChildClickListener mTaskItemClickListener = new ExpandableListView.OnChildClickListener()
    {

        @Override
        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id)
        {
            selectChildView(parent, groupPosition, childPosition, true);

            if (mExpandableListView.getChoiceMode() == ExpandableListView.CHOICE_MODE_SINGLE)
            {
                mActivatedPositionGroup = groupPosition;
                mActivatedPositionChild = childPosition;
            }
            /*
             * In contrast to a ListView an ExpandableListView does not set the activated item on it's own. So we have to do that here.
             */
            setActivatedItem(groupPosition, childPosition);
            return true;
        }

    };

    private static class SelectChildTaskParams
    {
        int groupPosition;
        Uri taskUriToSelect;
        Cursor childCursor;


        SelectChildTaskParams(int groupPosition, Cursor childCursor, Uri taskUriToSelect)
        {
            this.groupPosition = groupPosition;
            this.childCursor = childCursor;
            this.taskUriToSelect = taskUriToSelect;
        }
    }


    private static class ListPosition
    {
        int groupPosition;
        int childPosition;
        int flatListPosition;


        ListPosition(int groupPosition, int childPosition)
        {
            this.groupPosition = groupPosition;
            this.childPosition = childPosition;
        }
    }

    private class AsyncSelectChildTask extends AsyncTask<SelectChildTaskParams, Void, Void>
    {

        @Override
        protected Void doInBackground(SelectChildTaskParams... params)
        {
            int count = params.length;
            for (int i = 0; i < count; i++)
            {
                final SelectChildTaskParams param = params[i];

                final int childPosition = getSelectedChildPostion(param.taskUriToSelect, param.childCursor);
                if (childPosition > -1)
                {
                    mSelectedChildPosition = new ListPosition(param.groupPosition, childPosition);
                    openSelectedChild();
                }
            }
            return null;
        }

    }


    /**
     * Opens the task editor for the selected Task.
     *
     * @param taskUri
     *         The {@link Uri} of the task.
     * param taskTitle
     *         The name/title of the task.
     */
    private void openTaskEditor(final Uri taskUri, final String accountType)
    {
        Intent editTaskIntent = new Intent(Intent.ACTION_EDIT);
        editTaskIntent.setData(taskUri);
        //editTaskIntent.putExtra(EditTaskActivity.EXTRA_DATA_ACCOUNT_TYPE, accountType);
        startActivity(editTaskIntent);
    }


    public int getOpenChildPosition()
    {
        return mActivatedPositionChild;
    }


    public int getOpenGroupPosition()
    {
        return mActivatedPositionGroup;
    }


    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be given the 'activated' state when touched.
     * <p>
     * Note: this does not work 100% with {@link ExpandableListView}, it doesn't check touched items automatically.
     * </p>
     *
     * @param activateOnItemClick
     *         Whether to enable single choice mode or not.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick)
    {
        mExpandableListView.setChoiceMode(activateOnItemClick ? ListView.CHOICE_MODE_SINGLE : ListView.CHOICE_MODE_NONE);
    }


    public void setListViewScrollbarPositionLeft(boolean left)
    {
        if (left)
        {
            mExpandableListView.setVerticalScrollbarPosition(View.SCROLLBAR_POSITION_LEFT);
            // expandLV.setScrollBarStyle(style);
        }
        else
        {
            mExpandableListView.setVerticalScrollbarPosition(View.SCROLLBAR_POSITION_RIGHT);
        }
    }


    public void setExpandableGroupDescriptor(ExpandableGroupDescriptor groupDescriptor)
    {
        mGroupDescriptor = groupDescriptor;
    }



    /**
     * Mark the given task as completed.
     *
     * @param taskUri
     *         The {@link Uri} of the task.
     * @param taskTitle
     *         The name/title of the task.
     * @param completedValue
     *         The value to be set for the completed status.
     *
     * @return <code>true</code> if the operation was successful, <code>false</code> otherwise.
     */
    private boolean setCompleteTask(Uri taskUri, String taskTitle, boolean completedValue)
    {
        ContentValues values = new ContentValues();
//        values.put(Tasks.STATUS, completedValue ? Tasks.STATUS_COMPLETED : Tasks.STATUS_IN_PROCESS);
//        if (!completedValue)
//        {
//            values.put(Tasks.PERCENT_COMPLETE, 50);
//        }
//
//        boolean completed = mAppContext.getContentResolver().update(taskUri, values, null, null) != 0;
//        if (completed)
//        {
//            if (completedValue)
//            {
//                Snackbar.make(mExpandableListView, getString(R.string.toast_task_completed, taskTitle), Snackbar.LENGTH_SHORT).show();
//            }
//            else
//            {
//                Snackbar.make(mExpandableListView, getString(R.string.toast_task_uncompleted, taskTitle), Snackbar.LENGTH_SHORT).show();
//            }
//        }
//        return completed;
        return true;
    }


    public void setOpenChildPosition(int openChildPosition)
    {
        mActivatedPositionChild = openChildPosition;

    }


    public void setOpenGroupPosition(int openGroupPosition)
    {
        mActivatedPositionGroup = openGroupPosition;

    }


    public void notifyDataSetChanged(boolean expandFirst)
    {
        getLoaderManager().restartLoader(-1, null, this);
    }



    public void expandCurrentSearchGroup()
    {
//        if (mPageId == R.id.task_group_search && mAdapter.getGroupCount() > 0)
//        {
//            Cursor c = mAdapter.getGroup(0);
//            if (c != null && c.getInt(c.getColumnIndex(SearchHistoryColumns.HISTORIC)) < 1)
//            {
//                mExpandableListView.expandGroup(0);
//            }
//        }
    }

}
