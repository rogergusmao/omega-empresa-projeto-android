package app.omegasoftware.pontoeletronico.webservice;
public class ContainerProtocoloEntidade<E extends Enum<E>> {
	public E enumReferencia;
	public String nomeAtributo;
	public ContainerProtocoloEntidade(
		E enumReferencia,
		String nomeAtributo){
		this.enumReferencia = enumReferencia;
		this.nomeAtributo = nomeAtributo;
	}
}