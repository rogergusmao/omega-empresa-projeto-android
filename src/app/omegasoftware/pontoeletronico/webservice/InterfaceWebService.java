package app.omegasoftware.pontoeletronico.webservice;

import org.json.JSONException;
import org.json.JSONObject;


import android.content.Context;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemConfiguracao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.Sessao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.ParamGet;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA.TIPO;

public class InterfaceWebService {
	protected Context context;
	protected String mobileConectado;
	protected String mobileIdentificador;
	
	public InterfaceWebService (Context context) {
		this.context = context;
	}
	
	public InterfaceMensagem conecta(){
		if (OmegaSecurity.isAutenticacaoRealizada()) {
			ProtocoloConectaTelefone protocolo = Sessao.getSessao(
					context,
					OmegaSecurity.getIdCorporacao(),
					OmegaSecurity.getIdUsuario(), 
					OmegaSecurity.getCorporacao());
			if (protocolo == null)				
				return Mensagem.factoryMsgSemInternet(context);
			else {
				mobileConectado = protocolo
						.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileConectado);
				mobileIdentificador = protocolo
						.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador);
				
				return new Mensagem(TIPO.OPERACAO_REALIZADA_COM_SUCESSO, "");
			}

		} else {
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO_A_AREA_LOGADA,
					context.getResources().getString(R.string.login_nao_realizado));
		}
	}
	
	
}
