package app.omegasoftware.pontoeletronico.webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.database.InterfaceProtocolo;


public class PacoteCorpo extends InterfaceProtocolo{
	public String id;
	public String idSincronizador;
	//public String corpo;
	public String[] valores;
	
	public String[] idsDependentes;
	public String[] atributosDependentes;
    
	public static String TAG = "PacoteInsert";
	public PacoteCorpo() {
		
	}
	public PacoteCorpo(Context pContext,
			JSONObject pJsonObject) {

	}
	
	public JSONObject getJson(Context c) throws JSONException{
	
		JSONObject json = new JSONObject();
		JSONArray idsJSON = new JSONArray();
		if(idsDependentes != null && idsDependentes.length > 0){
			for(int i = 0 ; i < idsDependentes.length; i++){
				idsJSON.put(idsDependentes[i]);
			}	
		}
		
		JSONArray atributosJSON = new JSONArray();
		if(atributosDependentes != null && atributosDependentes.length > 0){
			for(int i = 0 ; i < atributosDependentes.length; i++){
				atributosJSON.put(atributosDependentes[i]);
			}	
		}
		
		
		
		JSONArray valoresJSON = new JSONArray();
		if(valores != null && valores.length > 0){
			for(int i = 0 ; i < valores.length; i++){
				valoresJSON.put(valores[i]);
			}	
		}
		
		
		//json.put("corpo", corpo);
		json.put("valores", valoresJSON);
		json.put("atributosDependentes", atributosJSON);
		json.put("idsDependentes", idsJSON);
		json.put("id", id);
		json.put("idSincronizador", idSincronizador);
		
		
		
		return json;
	
	}
	
    
}
