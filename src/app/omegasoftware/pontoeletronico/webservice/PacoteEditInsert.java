package app.omegasoftware.pontoeletronico.webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.database.InterfaceProtocolo;


public class PacoteEditInsert extends InterfaceProtocolo{
	public String campos[];
	public String tabela ;
	public String tipoOperacao;
	
	public PacoteCorpo[] protocolos;
	
    
	public static String TAG = "ProtocoloCabecalhoInsert";
	public PacoteEditInsert() {
		
		
	}
	
	public PacoteEditInsert(Context pContext,
			JSONObject pJsonObject) {
		
		
	}
	

	public JSONObject getJson(Context c) throws JSONException{
	
		JSONObject json = new JSONObject();
		json.put("tabela", tabela);
		JSONArray camposJSON = new JSONArray();
		
		for(int i = 0 ; i < campos.length; i++){
			camposJSON.put(campos[i]);
		}
		json.put("campos", camposJSON);
		JSONArray protocolosJSON = new JSONArray();
		for(int i = 0 ; i < protocolos.length; i++){
			protocolosJSON.put(protocolos[i].getJson(c));
		}
		json.put("protocolos", protocolosJSON);
		
		json.put("tipoOperacao", tipoOperacao);
		
		return json;
	
	}
}
