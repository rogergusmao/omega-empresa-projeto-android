package app.omegasoftware.pontoeletronico.webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.database.InterfaceProtocolo;


public class PacoteRemove extends InterfaceProtocolo{
	public String[] ids;
	public String[] idsSincronizador;
	public String tabela ;
	public String tipoOperacao;
	public static String TAG = "PacoteRemove";
	public PacoteRemove() {
		
		
	}
	
	public PacoteRemove(Context pContext,
			JSONObject pJsonObject) {
		
		
	}
	

	public JSONObject getJson(Context c) throws JSONException{
		JSONObject json = new JSONObject();
		json.put("tabela", tabela);
		JSONArray idsJSON = new JSONArray();
		for(int i = 0 ; i < ids.length; i++){
			idsJSON.put(ids[i]);
		}
		JSONArray idsSincronizadorJSON = new JSONArray();
		for(int i = 0 ; i < idsSincronizador.length; i++){
			idsSincronizadorJSON.put(idsSincronizador[i]);
		}		
		
		
		json.put("ids", idsJSON);
		json.put("idsSincronizador", idsSincronizadorJSON);
		json.put("tipoOperacao", tipoOperacao);
		
		return json;
	
	}
}
