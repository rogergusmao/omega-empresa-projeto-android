package app.omegasoftware.pontoeletronico.webservice;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemSharedPreference;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.BibliotecaNuvemSharedPreference.TIPO_STRING;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.http.RetornoJson;
import app.omegasoftware.pontoeletronico.phone.HelperPhone;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;

public class ServicosCobranca extends InterfaceWebService{
	
	public static final String BASE_URL() {
		return OmegaConfiguration.BASE_URL_OMEGA_SOFTWARE() + "client_area/webservice.php?class=Servicos_web&action=";}
	

	
	public enum TIPO_REQUEST{
		cadastrarClienteNaCorporacao,
		criarCorporacaoEUsuarioEmUmaAssinaturaGratuita,
		trocarEmailDoClientePeloSistema,
		verificaExistenciaDoCliente,
		relembrarSenhaPorEmail,
		removerCliente,
		getCorporacoesDoCliente
	}
	
	
	public ServicosCobranca(Context c) {
		super(c);
	}
	
	
	public static String getUrlWebService(TIPO_REQUEST pTipoRequest){
		String vUrl = BASE_URL() + pTipoRequest.toString();
		return vUrl;
	}

	
	public JSONObject criarCorporacaoEUsuarioEmUmaAssinaturaGratuita(
			String corporacao,
			String nome, 
			String email, 
			String senha) throws Exception {

		String strJson = null;
		JSONObject obj = null;
	
		strJson = HelperHttpPost.getConteudoStringPost(
				context, 
				getUrlWebService(TIPO_REQUEST.criarCorporacaoEUsuarioEmUmaAssinaturaGratuita), 
				new String[] {
					"nome",
					"email",
					"senha",
					"id_programa",
					"imei",
					"corporacao"
					
				},
				new String[] { 
					nome,
					email,
					senha,
					BibliotecaNuvemSharedPreference.getValor(context, TIPO_STRING.SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID),
					HelperPhone.getIMEI(context),
					corporacao
					}
				);
	
		if (strJson == null) return null;
		obj = new JSONObject(strJson);
		
		return obj;
	}
	
	public JSONObject trocarEmailDoClientePeloSistema(
			String nome, 
			String emailAntigo, 
			String emailNovo, 
			String senha) throws Exception {

	
		JSONObject obj =  ServicosPontoEletronico.getJsonObjectAreaRestritaPost(
				context, 
				getUrlWebService(TIPO_REQUEST.trocarEmailDoClientePeloSistema), 
				new String[] {
					"email_responsavel",
					"senha_responsavel",
					"nome",
					"email_antigo",
					"email_novo",
					"senha",
					"idPrograma",
					"imei",
					"corporacao"
				},
				new String[] { 
					OmegaSecurity.getEmail(),
					OmegaSecurity.getSenha(),
					nome,
					emailAntigo,
					emailNovo,
					senha,
					BibliotecaNuvemSharedPreference.getValor(context, TIPO_STRING.SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID),
					HelperPhone.getIMEI(context),
					OmegaSecurity.getCorporacao()
					}
				);
	
		return obj;
	}
	
	public JSONObject verificaExistenciaDoCliente(String email) throws Exception {

	
		return ServicosPontoEletronico.getJsonObjectAreaRestritaPost(
				context, 
				getUrlWebService(TIPO_REQUEST.verificaExistenciaDoCliente), 
				new String[] {
					"email"
				},
				new String[] { 
					email
				}
			);
	}
	
	public JSONObject cadastrarClienteNaCorporacao(String nome, String email, String senha) throws Exception {

	
		RetornoJson obj =HelperHttpPost.getRetornoJsonPost(
				context, 
				getUrlWebService(TIPO_REQUEST.cadastrarClienteNaCorporacao), 
				new String[] {
					"email_responsavel",
					"senha_responsavel",
					"nome",
					"email",
					"senha",
					"id_programa",
					"imei",
					"corporacao"
				},
				new String[] { 
					OmegaSecurity.getEmail(),
					OmegaSecurity.getSenha(),
					nome,
					email,
					senha,
					BibliotecaNuvemSharedPreference.getValor(context, TIPO_STRING.SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID),
					HelperPhone.getIMEI(),
					OmegaSecurity.getCorporacao()
					},
				null
				);
	
		return obj == null ? null : obj.json;
	}
	
	
	public String getCorporacoesDoCliente(String email, String senha) throws JSONException{

		
		String strJson =  HelperHttpPost.getConteudoStringPost(
				context, 
				getUrlWebService(TIPO_REQUEST.getCorporacoesDoCliente), 
				new String[] { "email" },
				new String[] { email }
			);
	
		return strJson;
		
	}
	
	public Mensagem relembrarSenhaPorEmail(String email){

		
		String strJson =  HelperHttpPost.getConteudoStringPost(
				context, 
				getUrlWebService(TIPO_REQUEST.relembrarSenhaPorEmail), 
				new String[] { "email" },
				new String[] { email }
			);
	
		if (strJson == null) return Mensagem.factoryMsgSemInternet(context);
		
		JSONObject obj;
		try {
			obj = new JSONObject(strJson);
			Mensagem msg = Mensagem.factory(obj);
			return msg;
		} catch (JSONException e) {
			SingletonLog.insereErro(e, TIPO.HTTP);
			return null;
		}
	}
	
	public Mensagem cancelarAssinaturaDoCliente(String motivo) throws Exception {
		//TODO desenvolver funcao
		throw new Exception("cancelarAssinaturaDoCliente - nao implementado");
//		String strJson = null;
//		JSONObject obj = null;
//	
//		strJson = HelperHttpPost.getConteudoStringPost(
//				context, 
//				getUrlWebService(TIPO_REQUEST.ca), 
//				new String[] {
//					"usuario",
//					"senha",
//					"id_corporacao",
//					"motivo"
//				},
//				new String[] { 
//					OmegaSecurity.getEmail(),
//					OmegaSecurity.getSenha(),
//					OmegaSecurity.getIdCorporacao(),
//					motivo
//					}
//				);
//	
//		if (strJson == null) return null;
//		obj = new JSONObject(strJson);
//		if(obj == null) return null;
//		return Mensagem.factory(obj);
	}
	
	public Mensagem removerCliente(String emailUsuarioRemovido) throws Exception {

	
		return ServicosPontoEletronico.getMensagemAreaRestritaPost(
				context, 
				getUrlWebService(TIPO_REQUEST.removerCliente), 
				new String[] {
					"usuario",
					"senha",
					"id_corporacao",
					"email_usuario_removido"
				},
				new String[] { 
					OmegaSecurity.getEmail(),
					OmegaSecurity.getSenha(),
					OmegaSecurity.getIdCorporacao(),
					emailUsuarioRemovido
					}
				);
	
	}
	

}
