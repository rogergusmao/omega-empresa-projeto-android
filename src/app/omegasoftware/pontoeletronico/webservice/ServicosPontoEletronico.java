package app.omegasoftware.pontoeletronico.webservice;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.http.ConfiguracaoHttpPost;
import app.omegasoftware.pontoeletronico.http.HelperHttpPost;
import app.omegasoftware.pontoeletronico.http.RetornoJson;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_STRING;
import app.omegasoftware.pontoeletronico.service.ReceiverBroadcast.ReceiveBroadcastAutenticacaoInvalida;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class ServicosPontoEletronico {
	final int MAXIMO_KB_ENVIO_LOG = 1024;
	public static final String BASE_URL() {
		return OmegaConfiguration.BASE_URL_PONTO_ELETRONICO()
				+ "adm/webservice.php?class=Servicos_web_ponto_eletronico&action=";
	}

	public enum TIPO_REQUEST {
		cadastrarUsuarioPontoEletronico, cadastrarUsuarioECorporacaoPontoEletronico,
		isCorporacaoExistente, alterarSenhaApp, isAdministradorDaCorporacao, 
		removerTodasAsPermissoesDaCategoriaDePermissao, 
		autoLogin, sugestaoDeMelhoria, relatoDeErro,
		rastreamento, getDadosMapa
	}

	Context context;

	public ServicosPontoEletronico(Context c) {
		this.context = c;
	}

	public static String getUrlWebService(TIPO_REQUEST pTipoRequest) {
		String vUrl = BASE_URL() + pTipoRequest.toString();
		return vUrl;
	}

	public JSONObject autoLogin() throws JSONException {
		ConfiguracaoHttpPost conf = new ConfiguracaoHttpPost();
//		if (OmegaSecurity.getCookieStoreComplete() != null) {
//
//			conf.cookieToSend = OmegaSecurity.getCookieStoreComplete();
//
//		}
		conf.setCookie = true;
		
		
		
		RetornoJson rj = HelperHttpPost.getRetornoJsonPost(
				context, 
				getUrlWebService(TIPO_REQUEST.autoLogin), 
				new String[]{
						OmegaSecurity.CHAVE_AUTO_LOGIN, 
						OmegaSecurity.CHAVE_SESSION,
						"corporacao",
						"id_corporacao"
					},
				new String[]{
						PontoEletronicoSharedPreference.getValor(context, TIPO_STRING.ID_LOGIN_AUTOMATICO),
						PontoEletronicoSharedPreference.getValor(context, TIPO_STRING.ID_SEGURANCA),
						OmegaSecurity.getCorporacao(),
						OmegaSecurity.getIdCorporacao(),
					},
				conf);

		if (rj == null)
			return null;
		if (rj.json.getInt("mCodRetorno") == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {
			JSONObject jsonObj = rj.json.getJSONObject("mObj");
			
			String idChaveAutoLogin = jsonObj.getString(OmegaSecurity.CHAVE_AUTO_LOGIN);
			String idSeguranca = jsonObj.getString(OmegaSecurity.CHAVE_SESSION);
			if(idChaveAutoLogin != null && idSeguranca != null){
				OmegaSecurity.initCookie(context, idSeguranca, idChaveAutoLogin);	
			}
		}else if (rj.json.getInt("mCodRetorno") == PROTOCOLO_SISTEMA.TIPO.AUTENTICACAO_INVALIDA.getId()) {
			ReceiveBroadcastAutenticacaoInvalida.sendBroadCast(context);
		}
		return rj.json;
	}

	
	public Integer sugestaoDeMelhoria(
			String sugestao, 
			String drawable, 
			String tagTela) throws JSONException {
		String strJson = null;		
		
		strJson = HelperHttpPost.getConteudoStringPost(context, getUrlWebService(TIPO_REQUEST.sugestaoDeMelhoria),
				new String[] { 
						"id_corporacao", 
						"corporacao", 
						"usuario", 
						"sugestao" ,
						"drawable",
						"tag_tela"}, 
				new String[] { 
						OmegaSecurity.getIdCorporacao(), 
						OmegaSecurity.getCorporacao(), 
						OmegaSecurity.getEmail(), 
						sugestao ,
						drawable,
						tagTela
						});

		if (strJson == null)
			return null;

		JSONObject obj = new JSONObject(strJson);
		return obj.getInt("mCodRetorno");
	}
	public Integer relatoDeErro(
			String relato, 
			String drawable, 
			String tagTela) throws JSONException {
		String strJson = null;		
		
		strJson = HelperHttpPost.getConteudoStringPost(context, getUrlWebService(TIPO_REQUEST.relatoDeErro),
				new String[] {
						"id_corporacao", 
						"corporacao", 
						"usuario", 
						"relato",
						"drawable",
						"tag_tela"
						}, 
				new String[] { 
						OmegaSecurity.getIdCorporacao(), 
						OmegaSecurity.getCorporacao(), 
						OmegaSecurity.getEmail(), 
						relato ,
						drawable,
						tagTela
						});

		if (strJson == null)
			return null;

		JSONObject obj = new JSONObject(strJson);
		return obj.getInt("mCodRetorno");
	}
	

	public Integer isCorporacaoExistente(String corporacao) throws JSONException {
		String strJson = null;

		strJson = HelperHttpPost.getConteudoStringPost(context, getUrlWebService(TIPO_REQUEST.isCorporacaoExistente),
				new String[] { "corporacao" }, new String[] { corporacao });

		if (strJson == null)
			return null;

		JSONObject obj = new JSONObject(strJson);
		return obj.getInt("mCodRetorno");
	}

	public Mensagem alterarSenhaApp(String senhaAntiga, String novaSenha, String confirmaNovaSenha) {

		return ServicosPontoEletronico.getMensagemAreaRestritaPost(context, getUrlWebService(TIPO_REQUEST.alterarSenhaApp),
				new String[] { "id_corporacao", "usuario", "senha", "nova_senha", "senha_confirmacao", "corporacao" },
				new String[] { OmegaSecurity.getIdCorporacao(), OmegaSecurity.getEmail(), senhaAntiga, novaSenha,
						confirmaNovaSenha, OmegaSecurity.getCorporacao() });

	}

	public Mensagem isAdministradorDaCorporacao() {

		return ServicosPontoEletronico.getMensagemAreaRestritaPost(context,
				getUrlWebService(TIPO_REQUEST.isAdministradorDaCorporacao),
				new String[] { "id_corporacao", "usuario", "senha" },
				new String[] { OmegaSecurity.getIdCorporacao(), OmegaSecurity.getEmail(), OmegaSecurity.getSenha() });

	}

	public Mensagem removerTodasAsPermissoesDaCategoriaDePermissao(String idCategoriaPermissao) {

		return ServicosPontoEletronico.getMensagemAreaRestritaPost(context,
				getUrlWebService(TIPO_REQUEST.removerTodasAsPermissoesDaCategoriaDePermissao),
				new String[] { "id_corporacao", "usuario", "senha", "id_categoria_permissao" },
				new String[] { OmegaSecurity.getIdCorporacao(), OmegaSecurity.getEmail(), OmegaSecurity.getSenha(),
						idCategoriaPermissao });

	}

	// public JSONObject cadastrarUsuarioECorporacao(String nome, String email,
	// String senha) throws Exception {
	//
	// String strJson = null;
	// JSONObject obj = null;
	//
	// strJson = HelperHttpPost.getConteudoStringPost(
	// context,
	// getUrlWebService(TIPO_REQUEST.cadastrarUsuarioPontoEletronico),
	// new String[] {
	// "email_responsavel",
	// "senha_responsavel",
	// "nome",
	// "email",
	// "senha",
	// "idPrograma",
	// "imei",
	// "corporacao"
	// },
	// new String[] {
	// OmegaSecurity.getEmail(),
	// OmegaSecurity.getSenha(),
	// nome,
	// email,
	// senha,
	// BibliotecaNuvemSharedPreference.getValor(context,
	// TIPO_STRING.SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID),
	// HelperPhone.getIMEI(),
	// OmegaSecurity.getCorporacao()
	// }
	// );
	//
	// if (strJson == null) return null;
	// obj = new JSONObject(strJson);
	// return obj;
	// }

	// public JSONObject cadastrarUsuario(String nome, String email, String
	// senha) throws Exception {
	//
	// String strJson = null;
	// JSONObject obj = null;
	//
	// strJson = HelperHttpPost.getConteudoStringPost(
	// context,
	// getUrlWebService(TIPO_REQUEST.cadastrarUsuarioPontoEletronico),
	// new String[] {
	// "email_responsavel",
	// "senha_responsavel",
	// "nome",
	// "email",
	// "senha",
	// "idPrograma",
	// "imei",
	// "corporacao"
	// },
	// new String[] {
	// OmegaSecurity.getEmail(),
	// OmegaSecurity.getSenha(),
	// nome,
	// email,
	// senha,
	// BibliotecaNuvemSharedPreference.getValor(context,
	// TIPO_STRING.SISTEMA_PROJETOS_VERSAO_SISTEMA_PRODUTO_MOBILE_ID),
	// HelperPhone.getIMEI(),
	// OmegaSecurity.getCorporacao()
	// }
	// );
	//
	// if (strJson == null) return null;
	// obj = new JSONObject(strJson);
	// return obj;
	// }

	
	public static JSONObject getJsonObjectAreaRestritaPost(
			Context pContext, 
			String p_url, 
			String[] p_vetorKey,
			String[] p_vetorContent) {
		try {
			ConfiguracaoHttpPost conf = new ConfiguracaoHttpPost();
			conf.cookieToSend = OmegaSecurity.getCookieStoreLogin();
			String strJson = HelperHttpPost.getConteudoStringPost(pContext, p_url, p_vetorKey, p_vetorContent, conf);
			if (strJson == null) {

				return Mensagem.factoryJSONObjectOffline(pContext);
			} else {
				Exception ex1 = null;
				JSONObject obj = null;
				try {
					obj = new JSONObject(strJson);

					if (obj != null) {
						if (obj.getInt("mCodRetorno") == PROTOCOLO_SISTEMA.TIPO.AUTENTICACAO_INVALIDA.getId()) {
							ServicosPontoEletronico spe = new ServicosPontoEletronico(pContext);
							JSONObject json = spe.autoLogin();
							
							if (json != null ) {
								obj = json;
								int intMCodRetorno = obj.getInt("mCodRetorno");
								if(InterfaceMensagem.checkOk(json)){
									conf.cookieToSend = OmegaSecurity.getCookieStoreLogin();
									strJson = HelperHttpPost.getConteudoStringPost(pContext, p_url, p_vetorKey,
											p_vetorContent, conf);
									
									if (strJson == null) 
										return Mensagem.factoryJSONObjectOffline(pContext);
									obj = new JSONObject(strJson);
									return obj;
								}else if (intMCodRetorno == PROTOCOLO_SISTEMA.TIPO.AUTENTICACAO_INVALIDA.getId()
										|| intMCodRetorno == PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO.getId()) {
									ReceiveBroadcastAutenticacaoInvalida.sendBroadCast(pContext);
									return Mensagem.factoryJSONObjectAreaRestrita(pContext);
								} 
								
							} else {
								return Mensagem.factoryJSONObjectOffline(pContext);
							}
						} else //ocorreu tudo bem na chamada
							return obj;
					}
				} catch (JSONException js) {
					ex1 = js;
				}
				if (ex1 != null || obj == null) {
					obj = new JSONObject();
					obj.put("mCodRetorno", PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId());
					obj.put("mMensagem", "Json invalido: " + strJson);
				}
				return obj;
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.HTTP);
			return null;
		}
	}

	public static Mensagem getMensagemAreaRestritaPost(Context pContext, String p_url, String[] p_vetorKey,
			String[] p_vetorContent) {
		try {
			ConfiguracaoHttpPost conf = new ConfiguracaoHttpPost();
			conf.cookieToSend = OmegaSecurity.getCookieStoreLogin();
			String strJson = HelperHttpPost.getConteudoStringPost(pContext, p_url, p_vetorKey, p_vetorContent, conf);
			if (strJson == null) {
				return Mensagem.factoryMsgSemInternet(pContext);

			} else {
				Exception ex1 = null;
				JSONObject obj = null;
				try {
					obj = new JSONObject(strJson);
					if (obj != null) {
						if (InterfaceMensagem.checkOk(obj)) {
							ServicosPontoEletronico spe = new ServicosPontoEletronico(pContext);
							JSONObject json = spe.autoLogin();
							if (json != null && InterfaceMensagem.checkOk(json)) {
								conf.cookieToSend = OmegaSecurity.getCookieStoreLogin();
								strJson = HelperHttpPost.getConteudoStringPost(pContext, p_url, p_vetorKey,
										p_vetorContent,
										conf);

								return Mensagem.factory(obj);
							}

						}
					}
					return Mensagem.factory(obj);
				} catch (JSONException js) {
					ex1 = js;
				}
				if (ex1 != null || obj == null) {
					Mensagem msg = new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId());

					msg.mMensagem = "Json invalido: " + strJson;
					return msg;
				}

			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, SingletonLog.TIPO.HTTP);
		}
		return null;
	}

	public Integer rastreamento(String strTotalTupla) throws JSONException {
		
		
		JSONObject obj =  getJsonObjectAreaRestritaPost(
				context, 
				getUrlWebService(TIPO_REQUEST.rastreamento),
				new String[] {
					"usuario",
					"id_corporacao",
					"corporacao", 
					"senha", 
					"lista_tupla"
				}, 
				new String[] {
					OmegaSecurity.getIdUsuario(), 
					OmegaSecurity.getIdCorporacao(),
					OmegaSecurity.getCorporacao(), 
					OmegaSecurity.getSenha(), 
					strTotalTupla
				});

		if (obj == null)
			return null;

		
		return obj.getInt("mCodRetorno");
	}

	public JSONObject getDadosMapa() throws JSONException {

		JSONObject obj =  getJsonObjectAreaRestritaPost(
				context,
				getUrlWebService(TIPO_REQUEST.getDadosMapa),
				new String[] {

						"id_corporacao",
						"corporacao"
				},
				new String[] {

						OmegaSecurity.getIdCorporacao(),
						OmegaSecurity.getCorporacao()
				});

		if (obj == null)
			return null;

		return obj;
	}


}
