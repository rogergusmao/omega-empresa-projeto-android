package app.omegasoftware.pontoeletronico.webservice;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.Sessao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone;
import app.omegasoftware.pontoeletronico.common.MCrypt;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.date.HelperDate;
import app.omegasoftware.pontoeletronico.file.CleanDir;
import app.omegasoftware.pontoeletronico.file.FileHttpPost;
import app.omegasoftware.pontoeletronico.file.HelperFile;
import app.omegasoftware.pontoeletronico.file.HelperZip;
import app.omegasoftware.pontoeletronico.file.OmegaFileConfiguration;
import app.omegasoftware.pontoeletronico.http.HelperHttp;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;

public class ServicosPontoEletronicoAnonimo {
	public static final int MAXIMO_KB_ENVIO_LOG_VIA_WIFI =  3 *1024;
	public static  final int MAXIMO_KB_ENVIO_LOG_VIA_MOBILE_INTERNET = 768;
	public static final String BASE_URL() {
		return OmegaConfiguration.BASE_URL_PONTO_ELETRONICO()
				+ "adm/webservice_anonimo.php?class=Servicos_web_ponto_eletronico_anonimo&action=";
	}

	public enum TIPO_REQUEST {
		uploadLogErro
	}

	Context context;

	public ServicosPontoEletronicoAnonimo(Context c) {
		this.context = c;
	}

	public static String getUrlWebService(TIPO_REQUEST pTipoRequest) {
		String vUrl = BASE_URL() + pTipoRequest.toString();
		return vUrl;
	}
	
	public InterfaceMensagem uploadLogErro(
			boolean enviarMesmoViaInternetMobile) throws JSONException {
		if(OmegaConfiguration.GRAVAR_LOG_SSD){
			return uploadLogErroExterno(enviarMesmoViaInternetMobile);
		} else if(OmegaConfiguration.GRAVAR_LOG_MODO_PRIVATE){
			return uploadLogErroInterno(enviarMesmoViaInternetMobile);
		} else 
			return new Mensagem(
					PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId(), 
					context.getString(R.string.log_enviado));
	}
	

	public InterfaceMensagem uploadLogErroInterno(
			boolean enviarMesmoViaInternetMobile) throws JSONException {
		String strJson = null;
		File fileZip = null;
		
		FileOutputStream fosPrivate = null;
		try {
			String[] files = context.fileList();
			if(files == null || files.length == 0 ){
				return new Mensagem(
						PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO,
						context.getString(R.string.nao_havia_dados_para_reportar));
			}
			String strTipo = OmegaFileConfiguration.TIPO.LOG.toString();
			List<File> fs = new ArrayList<File>();
			
			for(int i = 0 ; i < files.length; i++){
				if(files[i].startsWith(strTipo+"_")){
					fs.add( context.getFileStreamPath(files[i]));
				}
			}
			SingletonLog.writeLog();
			
			if(fs.size() == 0 || HelperFile.getFilesSize(fs) == 0){
				return new Mensagem(
						PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO,
						context.getString(R.string.nao_havia_dados_para_reportar));
			}
			File[] vfs = new File[fs.size()];
			fs.toArray(vfs );
			
			String path2 = OmegaFileConfiguration.TIPO.LOG_ENVIO.toString() + "_" + HelperDate.getDatetimeParaNomeDeArquivo() + ".zip";
			fosPrivate = context.openFileOutput( path2 , Context.MODE_PRIVATE);
			
			
			CleanDir cleanDir = new CleanDir();
			
			boolean isChipMobile= HelperHttp.isConnectedMobileChip(context);
			boolean isWifi = !isChipMobile ? HelperHttp.isConnectedWifi(context) : false;
			
			if(!isWifi && !isChipMobile) {
				cleanDir.deleteOldFiles(vfs,MAXIMO_KB_ENVIO_LOG_VIA_WIFI );
				return Mensagem.factoryMsgSemInternet(context);
			}
			else if(!isWifi && !enviarMesmoViaInternetMobile){
				cleanDir.deleteOldFiles(vfs,MAXIMO_KB_ENVIO_LOG_VIA_WIFI );
				return new Mensagem(
						PROTOCOLO_SISTEMA.TIPO.SOMENTE_VIA_WIFI, 
						"Envia o log nesse caso apenas via wifi");
			}else if(enviarMesmoViaInternetMobile){
				cleanDir.deleteOldFiles(vfs, MAXIMO_KB_ENVIO_LOG_VIA_MOBILE_INTERNET);
			} else  {
				cleanDir.deleteOldFiles(vfs, isWifi ? MAXIMO_KB_ENVIO_LOG_VIA_WIFI : MAXIMO_KB_ENVIO_LOG_VIA_MOBILE_INTERNET);	
			}
			HelperZip.zipVetorFile(vfs, fosPrivate);
			fosPrivate.close();
			
			
			fileZip = context.getFileStreamPath(path2);
			
			if (vfs == null || (fileZip != null && ( !fileZip.exists() ||fileZip.length() == 0))) {
				return new Mensagem(
						PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO,
						"Nuo havia dados para serem sincronizados.");

			} 
			
			String url = getUrlWebService(TIPO_REQUEST.uploadLogErro);
			FileHttpPost fileHttp = new FileHttpPost(
					context, 
					url,
					"Private");
			ProtocoloConectaTelefone protocolo = Sessao.getSessao(
					context, 
					OmegaSecurity.getIdCorporacao(),
					OmegaSecurity.getIdUsuario(), 
					OmegaSecurity.getCorporacao());
			String mobileIdentificador = null;
			if (protocolo != null){
				
				mobileIdentificador = protocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador);
			}
			
			String chave = MCrypt.getChaveCript(
					context, 
					new String[]{"palavra-secreta", "corporacao","id_mobile_identificador"}, 
					new String[] {"sakdmfwr42309rwfmvfc2034kr03wf0jkfvmo3m40tfjk3qefksdpfgmsodmgb409j5tg09w4ej5gksdlfkg34ijrt093",
							OmegaSecurity.getCorporacao(),
							mobileIdentificador});
			
			strJson = fileHttp.performUploadAnonimo(
					fileZip, 
					true,
					new String[] {  "chave" },
					new String[] { chave },
					false);
			JSONObject vObj=null;
			if(strJson != null){
				vObj = new JSONObject(strJson);
				if(vObj.getInt("mCodRetorno")==PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()){
					//se os logs tiverem sido enviados, entuo u melhor remover os arquivos ja enviados!
					HelperFile.removeDirectoryAndAllFiles(vfs);
				}
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
			
		} finally{
			if(fileZip != null)
				fileZip.delete();
		}

		return new Mensagem(
				PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
				context.getString(R.string.log_enviado));
	}
	
	public InterfaceMensagem uploadLogErroExterno(
			boolean enviarMesmoViaInternetMobile) throws JSONException {
		String strJson = null;
		File fileZip = null;
		try {
			
			OmegaFileConfiguration o = new OmegaFileConfiguration();
			String path = o.getPath(OmegaFileConfiguration.TIPO.LOG);
			//flush no log
			SingletonLog.writeLog();
			
			File f = new File(path);
			if(!f.exists() || HelperFile.getFilesSize(path) == 0){
				return new Mensagem(
						PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO,
						"Nuo havia dados para serem sincronizados.");
			}
			
			String path2 = o.getPath(OmegaFileConfiguration.TIPO.LOG_ENVIO) + HelperDate.getDatetimeParaNomeDeArquivo() + ".zip";
			CleanDir cleanDir = new CleanDir();
			
			boolean isChipMobile= HelperHttp.isConnectedMobileChip(context);
			boolean isWifi = !isChipMobile ? HelperHttp.isConnectedWifi(context) : false;
			
			if(!isWifi && !isChipMobile) {
				cleanDir.deleteOldFiles(path,MAXIMO_KB_ENVIO_LOG_VIA_WIFI );
				return Mensagem.factoryMsgSemInternet(context);
			}
			else if(!isWifi && !enviarMesmoViaInternetMobile){
				cleanDir.deleteOldFiles(path,MAXIMO_KB_ENVIO_LOG_VIA_WIFI );
				return new Mensagem(
						PROTOCOLO_SISTEMA.TIPO.SOMENTE_VIA_WIFI, 
						"Envia o log nesse caso apenas via wifi");
			}else if(enviarMesmoViaInternetMobile){
				cleanDir.deleteOldFiles(path, MAXIMO_KB_ENVIO_LOG_VIA_MOBILE_INTERNET);
			} else  {
				cleanDir.deleteOldFiles(path, isWifi ? MAXIMO_KB_ENVIO_LOG_VIA_WIFI : MAXIMO_KB_ENVIO_LOG_VIA_MOBILE_INTERNET);	
			}
			
			
			
			fileZip = new File(path2);
			try{
				HelperZip.zipDirectory(new File(path), fileZip);
				
			}catch(Exception ex){
				SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
			}
			
			f = fileZip;
			
			if (path == null || (f != null && f.length() == 0)) {
				return new Mensagem(
						PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO,
						"Nuo havia dados para serem sincronizados.");

			} 
			String url = getUrlWebService(TIPO_REQUEST.uploadLogErro);
			FileHttpPost fileHttp = new FileHttpPost(
					context, 
					url,
					path);
			ProtocoloConectaTelefone protocolo = Sessao.getSessao(
					context, 
					OmegaSecurity.getIdCorporacao(),
					OmegaSecurity.getIdUsuario(), 
					OmegaSecurity.getCorporacao());
			String mobileIdentificador = null;
			if (protocolo != null){
				
				mobileIdentificador = protocolo.getAtributo(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador);
			}
			
			String chave = MCrypt.getChaveCript(
					context, 
					new String[]{"palavra-secreta", "corporacao","id_mobile_identificador"}, 
					new String[] {"sakdmfwr42309rwfmvfc2034kr03wf0jkfvmo3m40tfjk3qefksdpfgmsodmgb409j5tg09w4ej5gksdlfkg34ijrt093",
							OmegaSecurity.getCorporacao(),
							mobileIdentificador});
			
			strJson = fileHttp.performUploadAnonimo(
					f, 
					true,
					new String[] {  "chave" },
					new String[] { chave },
					false);
			JSONObject vObj=null;
			if(strJson != null){
				vObj = new JSONObject(strJson);
				if(vObj.getInt("mCodRetorno")==PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()){
					//se os logs tiverem sido enviados, entuo u melhor remover os arquivos ja enviados!
					HelperFile.removeDirectoryAndAllFiles(path);
				}
			}
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
			
		} finally{
			if(fileZip != null)
				fileZip.delete();
		}

		return new Mensagem(
				PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
				context.getString(R.string.log_enviado));
	}
	
}
