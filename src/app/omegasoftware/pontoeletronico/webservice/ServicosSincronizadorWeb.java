package app.omegasoftware.pontoeletronico.webservice;

import java.io.File;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.appconfiguration.OmegaConfiguration;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.Sessao;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloConectaTelefone;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.webservice.protocolo.ProtocoloProcessoEmAberto;
import app.omegasoftware.pontoeletronico.common.MCrypt;
import app.omegasoftware.pontoeletronico.common.OmegaLog;
import app.omegasoftware.pontoeletronico.common.authenticator.OmegaSecurity;
import app.omegasoftware.pontoeletronico.database.synchronize.ParseSincronizacao;
import app.omegasoftware.pontoeletronico.database.synchronize.ParseSincronizacaoMobile;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho.ESTADO_SINCRONIZACAO_MOBILE;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho.Item;
import app.omegasoftware.pontoeletronico.file.FileHttpPost;
import app.omegasoftware.pontoeletronico.http.HelperHttpPostReceiveFile;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference;
import app.omegasoftware.pontoeletronico.pontoeletronico.PontoEletronicoSharedPreference.TIPO_STRING;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;
import app.omegasoftware.pontoeletronico.webservice.protocolo.InterfaceMensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.Mensagem;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemJSONObject;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemProtocolo;
import app.omegasoftware.pontoeletronico.webservice.protocolo.MensagemToken;
import app.omegasoftware.pontoeletronico.webservice.protocolo.PROTOCOLO_SISTEMA;
import app.omegasoftware.pontoeletronico.webservice.protocolo.ProtocoloStatusBancoSqlite;
import app.omegasoftware.pontoeletronico.webservice.protocolo.ProtocoloStatusSincronizacao;

public class ServicosSincronizadorWeb {

	public enum TIPO_REQUEST {
		uploadDadosSincronizadorMobile, 
		downloadDadosSincronizacao, 
		downloadDadosSincronizacaoMobile, 
		finalizaSincronizacaoMobile,
		statusSincronizacaoMobile, 
		retiraMobileDaFilaDeEspera, 
		erroSincronizacaoMobile, 
		mobileResetado, 
		statusSincronizacaoBancoSqliteDaCorporacao, 
		procedimentoGerarBancoSqlite, 
		downloadBancoSqliteDaCorporacao, 
		executaSincronizacaoCompleta,
		uploadDadosSincronizadorMobileAntesDoReset
	}

	String mobileConectado = null;
	String mobileIdentificador = null;
	Context context;

	public ServicosSincronizadorWeb(Context c, String mobileConectado, String mobileIdentificador) {
		this.mobileConectado = mobileConectado;
		this.mobileIdentificador = mobileIdentificador;
		this.context = c;
	}

	public boolean carregaDadosMobile(Context context) {

		if (this.mobileConectado == null || this.mobileIdentificador == null) {
			Sessao.getSessao(context, OmegaSecurity.getIdCorporacao(), OmegaSecurity.getIdUsuario(),
					OmegaSecurity.getCorporacao());
			this.mobileConectado = Sessao.getItem(ProtocoloConectaTelefone.ATRIBUTO.mobileConectado);
			;
			this.mobileIdentificador = Sessao.getItem(ProtocoloConectaTelefone.ATRIBUTO.mobileIdentificador);
			;
			if (this.mobileConectado != null && this.mobileIdentificador != null)
				return true;
			else
				return false;
		} else
			return true;

	}

	public static String getUrlWebService(TIPO_REQUEST pTipoRequest) {
		String vUrl = OmegaConfiguration.BASE_URL_SINCRONIZADOR_EMPRESA() + pTipoRequest.toString();
		return vUrl;
	}

	public static String getUrlDownload(TIPO_REQUEST pTipoRequest) {
		String vUrl = OmegaConfiguration.BASE_URL_SINCRONIZADOR_EMPRESA_DOWNLOAD() + pTipoRequest.toString();
		return vUrl;
	}

	public class RetornoStatus {
		public Mensagem msg;
		public ProtocoloStatusBancoSqlite protocolo;
	}

	public RetornoStatus statusSincronizacaoBancoSqliteDaCorporacao() throws Exception {
		// TODO retirar o null da passagem de parametro do POST
		

		JSONObject obj = ServicosPontoEletronico.getJsonObjectAreaRestritaPost(
				context,
				getUrlWebService(TIPO_REQUEST.statusSincronizacaoBancoSqliteDaCorporacao),
				new String[] { "mobile_identificador", "mobile_conectado", "id_corporacao", "corporacao" },
				new String[] { mobileIdentificador, mobileConectado, OmegaSecurity.getIdCorporacao(),
						OmegaSecurity.getCorporacao() });

		if (obj  == null)
			return null;
		
		RetornoStatus ret = new RetornoStatus();
		int vCodRetorno = obj.getInt("mCodRetorno");
		if (vCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {

			MensagemJSONObject vProtocolo = new MensagemJSONObject(context, obj);
			ret.msg = new Mensagem(vCodRetorno, obj.isNull("mMensagem") ? null : obj.getString("mMensagem"));
			if (vProtocolo.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {
				ProtocoloStatusBancoSqlite protocolo = new ProtocoloStatusBancoSqlite(context, vProtocolo.mObj);
				ret.protocolo = protocolo;
			}

			return ret;

		} else {
			throw new Exception(obj.isNull("mMensagem") ? null : obj.getString("mMensagem"));
		}
	}

	public Boolean retiraMobileDaFilaDeEspera(Context context, Long idSincronizacaoMobile, OmegaLog log)
			throws JSONException {
		
		

		JSONObject vObj = ServicosPontoEletronico.getJsonObjectAreaRestritaPost(context,
				getUrlWebService(TIPO_REQUEST.retiraMobileDaFilaDeEspera),
				new String[] { "id_sincronizacao_mobile", "mobile_conectado", "id_corporacao", "corporacao" },
				new String[] { idSincronizacaoMobile.toString(), mobileConectado, OmegaSecurity.getIdCorporacao(),
						OmegaSecurity.getCorporacao() });
		
		if (vObj == null)
			return null;

		
		int vCodRetorno = vObj.getInt("mCodRetorno");
		if (vCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {
			return true;
		} else {
			return false;

		}
	}

	public InterfaceMensagem downloadDadosSincronizacao(Context context, String path, Long idSincronizacao,
			Long idSincronizacaoMobile, String ultimaMigracao) {
		try {
			Mensagem msg = HelperHttpPostReceiveFile.DownloadFileFromHttpPost(context,
					getUrlDownload(TIPO_REQUEST.downloadDadosSincronizacao), path,
					new String[] { "id_sincronizacao_mobile", "id_corporacao", "corporacao", "ultima_migracao",
							"id_sincronizacao" },
					new String[] { idSincronizacaoMobile.toString(), OmegaSecurity.getIdCorporacao(),
							OmegaSecurity.getCorporacao(), ultimaMigracao, idSincronizacao.toString() });
			
			if (!msg.ok()) {

				return msg;
			} else { // if ok
				if (!ParseSincronizacao.validaArquivoSincronizacaoWeb(path)) {
					return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
							"Falha durante o upload do arquivo[b].");
				} else
					return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);

			}
		} catch (Exception ex) {
			return new Mensagem(ex);
		}

	}

	// realizando o download do sqlite vindo da web
	public InterfaceMensagem downloadBancoSqliteDaCorporacao(String path) {
		try {
			String url = getUrlDownload(TIPO_REQUEST.downloadBancoSqliteDaCorporacao);
			return HelperHttpPostReceiveFile.DownloadFileFromHttpPost(context, url, path,
					new String[] { "mobile_identificador", "id_corporacao", "corporacao" }, new String[] {
							mobileIdentificador, OmegaSecurity.getIdCorporacao(), OmegaSecurity.getCorporacao() });
		} catch (Exception ex) {
			return new Mensagem(ex);
		}

	}

	public InterfaceMensagem downloadDadosSincronizacaoMobile(Context context, String path, String idSincronizacaoMobile,
			String ultimaMigracao) {
		try {

			// tratar o retorno: - Deveru ser resetado o banco local
			// {"mCodRetorno":1,"mMensagem":"Arquivo de
			// sincroniza\u00c3\u00a7\u00c3\u00a3o inexistente:
			// ..\/conteudo\/sincronizacao_mobile\/TESTE6363_3381_sincronizacao.json"}
			Mensagem msg = HelperHttpPostReceiveFile.DownloadFileFromHttpPost(context,
					getUrlDownload(TIPO_REQUEST.downloadDadosSincronizacaoMobile), path,
					new String[] { "id_sincronizacao_mobile", "id_corporacao", "corporacao", "ultima_migracao" },
					new String[] { idSincronizacaoMobile, OmegaSecurity.getIdCorporacao(),
							OmegaSecurity.getCorporacao(), ultimaMigracao });
			if (msg == null)
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
						"O arquivo deveria ter sido transferido para o mobile");
			else if (!msg.ok())
				return msg;
			else {
				if (!ParseSincronizacaoMobile.validaArquivoSincronizacaoMobile(path)) {
					return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
							"O arquivo deveria ter sido transferido para o mobile");
				} else {
					return new Mensagem(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO);
				}

			}
		} catch (Exception ex) {
			return new Mensagem(ex);
		}

	}

//	public Boolean executaSincronizacaoCompleta(Context context) throws JSONException {
//		String strJson = null;
//		JSONObject vObj = null;
//
//		strJson = HelperHttpPost.getConteudoStringPost(context,
//				getUrlWebService(TIPO_REQUEST.executaSincronizacaoCompleta),
//				new String[] { "mobile_identificador", "mobile_conectado", "id_corporacao", "corporacao" },
//				new String[] { mobileIdentificador, mobileConectado, OmegaSecurity.getIdCorporacao(),
//						OmegaSecurity.getCorporacao() });
//
//		if (strJson == null)
//			return null;
//
//		vObj = new JSONObject(strJson);
//		int vCodRetorno = vObj.getInt("mCodRetorno");
//		if (vCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {
//			return true;
//		} else {
//			return false;
//		}
//	}

	public Mensagem mobileResetado(Context context, String idUltimaSincronizacao, String ultimaMigracao)
			throws JSONException {

		return ServicosPontoEletronico.getMensagemAreaRestritaPost(
				context, 
				getUrlWebService(TIPO_REQUEST.mobileResetado),
				new String[] { "id_sistema_sihop", "mobile_identificador", "mobile_conectado",
						"id_ultima_sincronizacao", "id_corporacao", "corporacao", "ultima_migracao" },
				new String[] { PontoEletronicoSharedPreference.getValor(context, TIPO_STRING.ID_SISTEMA_SIHOP),
						mobileIdentificador, mobileConectado, idUltimaSincronizacao, OmegaSecurity.getIdCorporacao(),
						OmegaSecurity.getCorporacao(), ultimaMigracao });


	}

	public Boolean erroSincronizacaoMobile(
			Context context, 
			String idSincronizacao, 
			String idSincronizacaoMobile,
			String pathLogSincronizador) throws JSONException {
		
		String strJson = null;
		JSONObject vObj = null;
		File f = new File(pathLogSincronizador);
		if (f.exists() && f.length() > 0) {
			FileHttpPost fileHttp = new FileHttpPost(context,getUrlWebService(TIPO_REQUEST.erroSincronizacaoMobile),
					pathLogSincronizador);

			strJson = fileHttp.performUpload(new File(pathLogSincronizador), true,
					new String[] { "id_sistema_sihop", "mobile_identificador", "mobile_conectado", "id_sincronizacao",
							"id_sincronizacao_mobile", "id_corporacao", "corporacao" },
					new String[] {
							PontoEletronicoSharedPreference.getValor(context,
									PontoEletronicoSharedPreference.TIPO_STRING.ID_SISTEMA_SIHOP),
							mobileIdentificador, mobileConectado, idSincronizacao, idSincronizacaoMobile,
							OmegaSecurity.getIdCorporacao(), OmegaSecurity.getCorporacao() });
			vObj = new JSONObject(strJson);
		} else {
			vObj = ServicosPontoEletronico.getJsonObjectAreaRestritaPost(
					context,
					getUrlWebService(TIPO_REQUEST.erroSincronizacaoMobile),
					new String[] { "id_sistema_sihop", "mobile_identificador", "mobile_conectado",
							"id_sincronizacao_mobile", "id_corporacao", "corporacao" },
					new String[] {
							PontoEletronicoSharedPreference.getValor(context,
									PontoEletronicoSharedPreference.TIPO_STRING.ID_SISTEMA_SIHOP),
							mobileIdentificador, mobileConectado, idSincronizacaoMobile,
							OmegaSecurity.getIdCorporacao(), OmegaSecurity.getCorporacao() });
		}
		if (vObj == null)
			return null;
		
		int vCodRetorno = vObj.getInt("mCodRetorno");
		if (vCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {
			return true;
		} else {
			return false;

		}
	}

	public Mensagem finalizaSincronizacaoMobile(Context context, String idSincronizacaoMobile,
			String pathLogSincronizador, String ultimaMigracao) throws JSONException {
		String strJson = null;
		JSONObject vObj = null;

		File f = new File(pathLogSincronizador);
		Long usableSpace = f.length();
		if (f.exists() && usableSpace > 0) {
			FileHttpPost fileHttp = new FileHttpPost(context, getUrlWebService(TIPO_REQUEST.finalizaSincronizacaoMobile),
					pathLogSincronizador);
			strJson = fileHttp.performUpload(new File(pathLogSincronizador), true,
					new String[] { "id_sincronizacao_mobile", "mobile_conectado", "id_corporacao", "corporacao",
							"ultima_migracao" },
					new String[] { idSincronizacaoMobile, mobileConectado, OmegaSecurity.getIdCorporacao(),
							OmegaSecurity.getCorporacao(), ultimaMigracao });

		} else {

			return ServicosPontoEletronico.getMensagemAreaRestritaPost(
					context,
					getUrlWebService(TIPO_REQUEST.finalizaSincronizacaoMobile),
					new String[] { "id_sincronizacao_mobile", "mobile_conectado", "id_corporacao", "corporacao",
							"ultima_migracao" },
					new String[] { idSincronizacaoMobile, mobileConectado, OmegaSecurity.getIdCorporacao(),
							OmegaSecurity.getCorporacao(), ultimaMigracao });
		}
		if (strJson == null)
			return null;

		vObj = new JSONObject(strJson);
		
		return Mensagem.factory(vObj);
				
	}

	public InterfaceMensagem uploadDadosSincronizadorMobile(
			String path, 
			Boolean resetando, 
			OmegaLog log,
			String primeiroId, 
			String ultimoId, 
			String ultimaMigracao) throws JSONException {
		String strJson = null;
		try {

			JSONObject vObj = null;
			File f = null;
			if(path != null){
				f = new File(path);
			}
			if (path == null || (f != null && f.length() == 0)) {
				if(resetando != null && resetando){
					log.escreveLog("Fim do processo reset! Sucesso!");
					return new Mensagem(
							PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
							"Nuo havia dados para serem sincronizados.");
				}
	
				// while(true){
				vObj = ServicosPontoEletronico.getJsonObjectAreaRestritaPost(
						context,
						getUrlWebService(TIPO_REQUEST.uploadDadosSincronizadorMobile),
						new String[] { "id_sistema_sihop", "mobile_identificador", "mobile_conectado", "resetando",
								"num_registro", "id_corporacao", "corporacao", "primeiro_id", "ultimo_id",
								"ultima_migracao" },
						new String[] {
								PontoEletronicoSharedPreference.getValor(context,
										PontoEletronicoSharedPreference.TIPO_STRING.ID_SISTEMA_SIHOP),
								mobileIdentificador, mobileConectado, resetando == null ? "0" : (resetando ? "1" : "0"),
								"1", OmegaSecurity.getIdCorporacao(), OmegaSecurity.getCorporacao(), primeiroId,
								ultimoId, ultimaMigracao });
				// if(strJson == null)
				// break;
				// }
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("uploadDadosSincronizadorMobile - sem enviar arquivo: "
							+ (strJson == null ? "-" : strJson));

			} else {
				
				// while(true){
				

			
				FileHttpPost fileHttp = new FileHttpPost(
						context, getUrlWebService(TIPO_REQUEST.uploadDadosSincronizadorMobile), path);
				strJson = fileHttp.performUpload(new File(path), true,
						new String[] { "id_sistema_sihop", "mobile_identificador", "mobile_conectado", "resetando",
								"num_registro", "id_corporacao", "corporacao", "primeiro_id", "ultimo_id",
								"ultima_migracao" },
						new String[] {
								PontoEletronicoSharedPreference.getValor(context,
										PontoEletronicoSharedPreference.TIPO_STRING.ID_SISTEMA_SIHOP),
								mobileIdentificador, mobileConectado,
								resetando == null ? "0 " : (resetando ? "1" : "0"), "1",
								OmegaSecurity.getIdCorporacao(), OmegaSecurity.getCorporacao(), primeiroId, ultimoId,
								ultimaMigracao });
				
				if(strJson != null)vObj = new JSONObject(strJson);
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("uploadDadosSincronizadorMobile - enviando arquivo: "
							+ (strJson == null ? "-" : strJson));
			}

			if (vObj == null ) {
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
						"Falha durante o envio do arquivo para inicio da sincronizacao. Retorno do webservice veio nulo/vazio: "
								+ strJson);
			}

			
			int mCodRetorno = vObj.getInt("mCodRetorno");
			if (mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId())
				return new MensagemToken(vObj.getInt("mCodRetorno"),
						vObj.isNull("mMensagem") ? null : vObj.getString("mMensagem"), vObj.getString("mValor"));
			else if (mCodRetorno == PROTOCOLO_SISTEMA.TIPO.PROCESSO_EM_ABERTO.getId()) {
				return new MensagemProtocolo(context, new ProtocoloProcessoEmAberto(), vObj);
			} else
				return new Mensagem(vObj.getInt("mCodRetorno"),
						vObj.isNull("mMensagem") ? null : vObj.getString("mMensagem"));
		} catch (Exception ex) {
			return new Mensagem(ex, strJson);
		}
		
	}
	public InterfaceMensagem uploadDadosSincronizadorMobileAntesDoReset(
			String path, 
			Boolean resetando, 
			OmegaLog log,
			String primeiroId, 
			String ultimoId, 
			String ultimaMigracao) throws JSONException {
		String strJson = null;
		try {
			String idCorporacao = OmegaSecurity.getIdCorporacao();
			String corporacao= OmegaSecurity.getCorporacao();
			JSONObject vObj = null;
			File f = null;
			if(path != null){
				f = new File(path);
			}
			if (path == null || (f != null && f.length() == 0)) {
				
				log.escreveLog("Fim do processo reset! Sucesso!");
				return new Mensagem(
						PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO,
						"Nuo havia dados para serem sincronizados.");

			} 
			
			FileHttpPost fileHttp = new FileHttpPost(
					context, 
					getUrlWebService(TIPO_REQUEST.uploadDadosSincronizadorMobileAntesDoReset),
					path);
			
			String chave = MCrypt.getChaveCript(
					context, 
					new String[]{"corporacao2","id_corporacao2"}, 
					new String[] {corporacao, idCorporacao});
			
			
			strJson = fileHttp.performUpload(new File(path), true,
					new String[] { "id_sistema_sihop", "mobile_identificador", "mobile_conectado", "resetando",
							"num_registro", "id_corporacao", "corporacao", "primeiro_id", "ultimo_id",
							"ultima_migracao", "chave" },
					new String[] {
							PontoEletronicoSharedPreference.getValor(context,
									PontoEletronicoSharedPreference.TIPO_STRING.ID_SISTEMA_SIHOP),
							mobileIdentificador, mobileConectado,
							resetando == null ? "0 " : (resetando ? "1" : "0"), "1",
							idCorporacao, corporacao, primeiroId, ultimoId,
							ultimaMigracao, chave },
					false);
			
			if(strJson != null)
				vObj = new JSONObject(strJson);
			if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
				log.escreveLog("uploadDadosSincronizadorMobileAntesDoReset - enviando arquivo: "
						+ (strJson == null ? "-" : strJson));
			
			if (vObj == null ) {
				return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR,
						"Falha durante o envio do arquivo para inicio da sincronizacao. Retorno do webservice veio nulo/vazio: "
								+ strJson);
			}			
			int mCodRetorno = vObj.getInt("mCodRetorno");
			if (mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId())
				return new MensagemToken(vObj.getInt("mCodRetorno"),
						vObj.isNull("mMensagem") ? null : vObj.getString("mMensagem"), vObj.getString("mValor"));
			else if (mCodRetorno == PROTOCOLO_SISTEMA.TIPO.PROCESSO_EM_ABERTO.getId()) {
				return new MensagemProtocolo(context, new ProtocoloProcessoEmAberto(), vObj);
			} else
				return new Mensagem(vObj.getInt("mCodRetorno"),
						vObj.isNull("mMensagem") ? null : vObj.getString("mMensagem"));
		} catch (Exception ex) {
			return new Mensagem(ex, strJson);
		}
	}

	/*
	 * {"mValor":"1","mCodRetorno":3,"mMensagem":null}
	 */
	public ProtocoloStatusSincronizacao statusSincronizacaoMobile(Item item, OmegaLog log) throws Exception {
		JSONObject obj =null;
		try {
			obj = ServicosPontoEletronico.getJsonObjectAreaRestritaPost(context,
					getUrlWebService(TIPO_REQUEST.statusSincronizacaoMobile),
					new String[] { "id_sistema_sihop", "id_sincronizacao_mobile", "mobile_conectado",
							"ultimo_id_sincronizacao", "id_corporacao", "corporacao", "ultima_migracao" },
					new String[] { PontoEletronicoSharedPreference.getValor(context, TIPO_STRING.ID_SISTEMA_SIHOP),
							item.idSincronizacaoMobile.toString(), mobileConectado,
							String.valueOf(item.idUltimaSincronizacao), OmegaSecurity.getIdCorporacao(),
							OmegaSecurity.getCorporacao(), item.ultimaMigracao });

			if (obj == null ) {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("statusSincronizacaoMobile: null");
				return null;
			} else {
				if (OmegaConfiguration.DEBUGGING_SINCRONIZACAO)
					log.escreveLog("statusSincronizacaoMobile: " + obj.toString() );
			}

			
			int vCodRetorno = obj.getInt("mCodRetorno");
			if (vCodRetorno == PROTOCOLO_SISTEMA.TIPO.ESTRUTURA_MIGRADA.getId()) {
				return new ProtocoloStatusSincronizacao(ESTADO_SINCRONIZACAO_MOBILE.ESTRUTURA_MIGRADA);
			} else if (vCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()
					|| vCodRetorno == PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO.getId()) {

				MensagemJSONObject msgJson = new MensagemJSONObject(context, obj);
				if (msgJson.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId()) {
					ProtocoloStatusSincronizacao protocolo = new ProtocoloStatusSincronizacao(context, msgJson.mObj,
							msgJson.mMensagem);

					String strIdsUltimasSincronizacoes = protocolo
							.getAtributo(ProtocoloStatusSincronizacao.ATRIBUTO.idsUltimasSincronizacoes);
					if (strIdsUltimasSincronizacoes != null) {
						String[] idsUltimasSincronizacoes = null;
						Long[] ids = null;
						if (strIdsUltimasSincronizacoes != null && strIdsUltimasSincronizacoes.length() > 0) {
							idsUltimasSincronizacoes = HelperString.splitWithNoneEmptyToken(strIdsUltimasSincronizacoes,
									",");
							ids = new Long[idsUltimasSincronizacoes.length];
							for (int i = 0; i < ids.length; i++) {
								ids[i] = HelperInteger.parserLong(idsUltimasSincronizacoes[i]);
							}
							protocolo.idsSincronizacao = ids;
						}
					}
					return protocolo;
				} else if (msgJson.mCodRetorno == PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO.getId()) {
					// Se nuo existem dados a serem sincronizados
					return new ProtocoloStatusSincronizacao(ESTADO_SINCRONIZACAO_MOBILE.PRIMEIRA_VEZ,
							"Os dados foram sincronizados, a web nuo tinham nenhum dado novo para o telefone.");
				} else {
					throw new Exception(obj.isNull("mMensagem") ? null : obj.getString("mMensagem"));
				}
			} else {
				throw new Exception(obj.isNull("mMensagem") ? null : obj.getString("mMensagem"));
			}
		} catch (JSONException ex) {
			SingletonLog.insereErro(ex, TIPO.WEBSERVICE_SINCRONIZADOR);
			String strObj =(obj != null ? obj.toString() : "-"); 
			throw new Exception("statusSincronizacaoMobile:: Falha ao tentar parsear os dados json: " 
			+ strObj);
		} catch (Exception ex) {
			SingletonLog.insereErro(ex, TIPO.WEBSERVICE_SINCRONIZADOR);
			throw ex;
		}

	}
}
