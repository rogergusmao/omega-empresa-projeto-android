package app.omegasoftware.pontoeletronico.webservice.protocolo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.content.Context;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.primitivetype.HelperString;


public abstract class InterfaceMensagem {
	public int mCodRetorno = -1;
    public String mMensagem;
    protected InterfaceProtocoloEnum protocoloFactory;
	public static String TAG = "InterfaceMensagem";
	public String getIdentificador(){
		return "[" + String.valueOf( this.mCodRetorno) + "] - " + this.mMensagem; 
				 
	}
	public static boolean checkOk(JSONObject json) throws JSONException{
		if(json==null) return false;
		else if(json.getInt("mCodRetorno") == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId())
			return true;
		else return false;
	}
	protected InterfaceMensagem(int mCodRetorno, String mMensagem){
		this.mCodRetorno = mCodRetorno;
		this.mMensagem = mMensagem;
	}
	
	protected InterfaceMensagem(PROTOCOLO_SISTEMA.TIPO mCodRetorno, String mMensagem){
		this.mCodRetorno = mCodRetorno.getId();
		this.mMensagem = mMensagem;
	}
	
	public boolean erroDownload(){
		return mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ERRO_DOWNLOAD.getId();
	}
	public boolean erro(){
		return mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR.getId();
	}
	public boolean ok(){
		return mCodRetorno == PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO.getId();
	}
	public boolean emManutencao(){
		return mCodRetorno == PROTOCOLO_SISTEMA.TIPO.EM_MANUTENCAO.getId();
	}
	public boolean estruturaMigrada(){
		return mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ESTRUTURA_MIGRADA.getId();
	}
	public boolean resultadoVazio(){
		return mCodRetorno == PROTOCOLO_SISTEMA.TIPO.RESULTADO_VAZIO.getId();
	}
	public boolean semConexaoAInternet(){
		return mCodRetorno == PROTOCOLO_SISTEMA.TIPO.SEM_CONEXAO_A_INTERNET.getId();
	}
	public boolean servidorForaDoAr(){
		return mCodRetorno == PROTOCOLO_SISTEMA.TIPO.SERVIDOR_FORA_DO_AR.getId();
	}
	public boolean erroBancoOmegaSoftware(){
		return mCodRetorno == PROTOCOLO_SISTEMA.TIPO.ERRO_BANCO_OMEGASOFTWARE.getId();
	}
	public boolean offline(){
		return semConexaoAInternet() || servidorForaDoAr();
	}
	public static Boolean validaChamadaWebservice(InterfaceMensagem msg){
		if(msg.ok()){
			return true;
		} else if(msg.offline())
			return null;
		else 
			return false;
	}
	protected InterfaceMensagem(Context context, JSONObject pJsonObject){
		inicializa(context, pJsonObject);
	}
	protected InterfaceMensagem(Context context, JSONObject pJsonObject, InterfaceProtocoloEnum protocoloFactory){
		this.protocoloFactory = protocoloFactory;
		inicializa(context, pJsonObject);
	}
	
	protected abstract void preInicializa(Context context, JSONObject pJsonObject)  throws JSONException ; 
	
	protected abstract ContainerJson procedimentoToJson() throws JSONException;
	
	private void inicializa(Context context, JSONObject pJsonObject) {

		try {
			preInicializa(context, pJsonObject);
			this.mCodRetorno = pJsonObject.getInt("mCodRetorno");
			this.mMensagem = HelperString.checkIfIsNull( pJsonObject.isNull("mMensagem") ? null : pJsonObject.getString("mMensagem"));
		} catch (JSONException e) {
			SingletonLog.insereErro(
					e, 
					SingletonLog.TIPO.BIBLIOTECA_NUVEM);
			
			
		}
	}
	
	public String toJson() throws JSONException{
		
		JSONObject json = new JSONObject();
		json.put("mCodRetorno", mCodRetorno);
		json.put("mMensagem", mMensagem);
		
		ContainerJson container= procedimentoToJson();
		if(container != null)
			container.appendJson(json);
		
		String strJson = json.toString();
		return strJson;
	}
	
	protected class ContainerJson{
		Object obj;
		String label;
		public ContainerJson(String label, JSONObject jsonObj){
			this.label = label; 
			this.obj = jsonObj;
		}
		
		public ContainerJson(String label, JSONArray jsonObj){
			this.label = label; 
			this.obj = jsonObj;
		}
		
		public void appendJson(JSONObject obj) throws JSONException{
			if(obj != null)
				obj.put(label, this.obj);
		}
	}
	
	
    
}
