package app.omegasoftware.pontoeletronico.webservice.protocolo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.database.InterfaceProtocolo;
import app.omegasoftware.pontoeletronico.primitivetype.HelperBoolean;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;



public abstract class InterfaceProtocoloEnum extends InterfaceProtocolo{
	
	public static String TAG = "InterfaceProtocolo";

	public HashMap<Enum<?>, String> hashNomePorValor = new HashMap<Enum<?>, String>();
	
//	public abstract JSONObject getJsonObject();
	

	public <E extends Enum<E>> InterfaceProtocoloEnum(JSONObject pJsonObject, Class<E> atributos){
		
		for (Enum<E> enumVal: atributos.getEnumConstants()) {  
            
            hashNomePorValor.put(enumVal, null);
        }  
		inicializa(pJsonObject);
	}
	
	public <E extends Enum<E>> InterfaceProtocoloEnum(){
	
	}
	
	
	public String getString(JSONObject obj, String chave) throws JSONException{
		
		if(obj.isNull( chave))
			return null;
		else{
			return obj.getString(chave);
		}
	}
	public void inicializa(JSONObject json){
		try {
			if(json == null) return;
		Iterator<Enum<?>> it = hashNomePorValor.keySet().iterator();
		Enum<?> key;
		
		while(it.hasNext()){
			key = it.next();
			if(json.has(key.toString())){
				String valor = json.getString(key.toString());
				if(valor != null && valor.equals("null"))
					valor = null;
				this.setAtributo(key, valor);	
			} else this.setAtributo(key, null);
		}	
		} catch (JSONException e) {

			SingletonLog.insereErro(e, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
		}
		
	}
	
	
	
	public void setAtributo(Enum<?> key, String valor){
		if(hashNomePorValor.containsKey(key)){
			hashNomePorValor.put(key, valor);
		}
	}
	
	public Enum<?> possuiItem(String pNome){
		Iterator<Enum<?>> it = hashNomePorValor.keySet().iterator();
		Enum<?> key;
		while(it.hasNext()){
			key = it.next();
			if(key.toString().compareTo(pNome) == 0) return key;
			
		}	
		return null;
	}
	
	public Set<Enum<?>> getEnum(){
		return hashNomePorValor.keySet();
	}
	
	public Integer getAtributoInteger(Enum<?> key){
		return HelperInteger.parserInteger(getAtributo(key));
	}
	
	public Boolean getBooleanAtributo(Enum<?> key){
		
		if(hashNomePorValor.containsKey(key)){
			String valor = hashNomePorValor.get(key);
			return HelperBoolean.valueOfStringSQL( valor);
		} else return null;
	}
	
	public Long getLongAtributo(Enum<?> key){
		
		if(hashNomePorValor.containsKey(key)){
			return HelperInteger.parserLong( hashNomePorValor.get(key));
		} else return null;
	}
	
	public Integer getIntegerAtributo(Enum<?> key){
		
		if(hashNomePorValor.containsKey(key)){
			return HelperInteger.parserInteger( hashNomePorValor.get(key));
		} else return null;
	}
	
	public String getAtributo(Enum<?> key){
		
		if(hashNomePorValor.containsKey(key)){
			return hashNomePorValor.get(key);
		} else return null;
	}
	
	
	
	public abstract InterfaceProtocoloEnum factory(
			Context pContext,
			JSONObject pJsonObject);
}
