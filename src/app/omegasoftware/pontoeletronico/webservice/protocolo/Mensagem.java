package app.omegasoftware.pontoeletronico.webservice.protocolo;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.R;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.common.util.HelperExcecao;


public class Mensagem extends InterfaceMensagem {
	
	
    
	public static String TAG = "Mensagem";
	

	public static Mensagem factory(JSONObject json){
		try{
			return new Mensagem(
					json.getInt("mCodRetorno"),
					json.isNull("mMensagem") ? null : json.getString("mMensagem"));	
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.UTIL_ANDROID);
			String desc = json == null ? "null" : json.toString(); 
			return new Mensagem(PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, "Json invalido: " + desc);
		}
		
	}
	public Mensagem(Context context,
			JSONObject pJsonObject) {
		super(context, pJsonObject);
	}
	
	public Mensagem(int codRetorno){
		super( codRetorno, null);
	}
	
	public Mensagem(int codRetorno, String mensagem){
		super( codRetorno, mensagem);
	}
	
	public Mensagem(PROTOCOLO_SISTEMA.TIPO mCodRetorno, String mMensagem) {
		super(mCodRetorno, mMensagem);
	}
	
	public Mensagem(PROTOCOLO_SISTEMA.TIPO mCodRetorno) {
		super(mCodRetorno, null);
	}
	
	@Override
	protected void preInicializa(Context context,
			JSONObject pJsonObject) {

	}
	public Mensagem(Exception ex){
		super(
				PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, 
				HelperExcecao.getDescricao(ex));
	}
	public Mensagem(Exception ex, String desc){
		super(
				PROTOCOLO_SISTEMA.TIPO.ERRO_COM_SERVIDOR, 
				"Desc.: "+ desc+". " + HelperExcecao.getDescricao(ex));
	}
	
	
	@Override
	protected ContainerJson procedimentoToJson() throws JSONException {
		return null;
	    
	}
    
	public static Mensagem factoryMsgSemInternet(Context c){
		return new Mensagem(
				PROTOCOLO_SISTEMA.TIPO.SEM_CONEXAO_A_INTERNET, 
				c.getString(R.string.sem_internet));
	}	
	public static Mensagem factoryServidorSobrecarregado(Context c){
		return new Mensagem(
				PROTOCOLO_SISTEMA.TIPO.SERVIDOR_FORA_DO_AR, 
				c.getString(R.string.error_servidor_offline));
	}
	public static Mensagem factoryBancoOmegaErro(Context c){
		return new Mensagem(
				PROTOCOLO_SISTEMA.TIPO.ERRO_BANCO_OMEGASOFTWARE, 
				c.getString(R.string.erro_banco_omega));
	}
	public static JSONObject factoryJSONObjectOffline(Context pContext) throws JSONException{

		
		JSONObject obj = new JSONObject( );
		obj.put("mCodRetorno", PROTOCOLO_SISTEMA.TIPO.SEM_CONEXAO_A_INTERNET.getId());
		obj.put("mMensagem", pContext.getString(R.string.server_offline));
		return obj;
	}
public static JSONObject factoryJSONObjectAreaRestrita(Context pContext) throws JSONException{

		
		JSONObject obj = new JSONObject( );
		obj.put("mCodRetorno", PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO_A_AREA_LOGADA.getId());
		obj.put("mMensagem", pContext.getString(R.string.area_restrita));
		return obj;
	}

	public static InterfaceMensagem factoryAreaRestrita(Context context){
		return new Mensagem(
				PROTOCOLO_SISTEMA.TIPO.ACESSO_NEGADO_A_AREA_LOGADA, 
				context.getString(R.string.area_restrita));
	}
}
