package app.omegasoftware.pontoeletronico.webservice.protocolo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;


public class MensagemInteger extends InterfaceMensagem {
	
	public Integer mToken;
    
	public static String TAG = "MensagemVetorToken";
	

	public MensagemInteger(PROTOCOLO_SISTEMA.TIPO mCodRetorno, Integer pToken) {
		super(mCodRetorno, null);
		this.mToken = pToken;
	}

	
	public MensagemInteger(Context context,
			JSONObject pJsonObject) {
		super(context, pJsonObject);
	}
	
	public MensagemInteger(PROTOCOLO_SISTEMA.TIPO mCodRetorno, String mMensagem) {
		super(mCodRetorno, mMensagem);
	}
	
	@Override
	protected void preInicializa(Context context,
			JSONObject pJsonObject) {
		//TODO
	}
	
	@Override
	protected ContainerJson procedimentoToJson() throws JSONException {
		JSONArray jsonArray = new JSONArray();
		jsonArray.put(mToken);
        return new ContainerJson("mVetorObj", jsonArray);
	    
	}
    
}
