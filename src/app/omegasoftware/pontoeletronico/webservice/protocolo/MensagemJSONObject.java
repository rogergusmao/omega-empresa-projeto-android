package app.omegasoftware.pontoeletronico.webservice.protocolo;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;


public class MensagemJSONObject extends InterfaceMensagem {
	
	public JSONObject mObj;
    
	public static String TAG = "MensagemJSONObject";
	
	public MensagemJSONObject(Context mContext, JSONObject vObj) {
		super(mContext, vObj);
		
	}
	@Override
	protected void preInicializa(Context context,
			JSONObject pJsonObject) {
		try {
			
			this.mObj = pJsonObject.getJSONObject("mObj");
			
		} catch (JSONException e) {
			SingletonLog.insereErro(e, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
		}
	}
	@Override
	protected ContainerJson procedimentoToJson() {
		
		return new ContainerJson("mObj", mObj);
	}
	
    
}
