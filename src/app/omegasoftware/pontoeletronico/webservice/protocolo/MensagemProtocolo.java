package app.omegasoftware.pontoeletronico.webservice.protocolo;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;


public class MensagemProtocolo extends InterfaceMensagem {
	
	public InterfaceProtocoloEnum mObj;
    
	public static String TAG = "MensagemProtocolo";
	
	public MensagemProtocolo(
			Context mContext, 
			InterfaceProtocoloEnum protocoloFactory, 
			JSONObject vObj) {
		
		super(mContext, vObj, protocoloFactory);

		
	}
	
	public InterfaceProtocoloEnum getProtocolo(){
		if(mObj == null ){
			return null;
		}
		return mObj;
	}
	
	@Override
	protected void preInicializa(Context context,
			JSONObject pJsonObject) {
		try {
			JSONObject obj = pJsonObject.getJSONObject("mObj");
			mObj = protocoloFactory.factory(context, obj);
		} catch (JSONException e) {
			SingletonLog.insereErro(e, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
		}
	}
	@Override
	protected ContainerJson procedimentoToJson() {
		
		

		return null;
	}
	
    
}
