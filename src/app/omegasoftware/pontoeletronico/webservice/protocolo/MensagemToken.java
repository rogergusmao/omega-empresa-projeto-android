package app.omegasoftware.pontoeletronico.webservice.protocolo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;


public class MensagemToken extends InterfaceMensagem {
	
	public String mValor;
    
	public static String TAG = "MensagemToken";
	

	public MensagemToken(String pToken) {
		super(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, null);
		this.mValor = pToken;
	}

	
	public MensagemToken(Context context,
			JSONObject pJsonObject) {
		super(context, pJsonObject);
	}
	
	public MensagemToken(PROTOCOLO_SISTEMA.TIPO mCodRetorno, String mMensagem) {
		super(mCodRetorno, mMensagem);
	}
	
	public MensagemToken(int mCodRetorno, String mMensagem, String mToken) {
		super(mCodRetorno, mMensagem);
		this.mValor = mToken;
	}
	
	public MensagemToken(PROTOCOLO_SISTEMA.TIPO mCodRetorno, String mMensagem, String mToken) {
		super(mCodRetorno, mMensagem);
		this.mValor = mToken;
	}
	
	@Override
	protected void preInicializa(Context context,
			JSONObject pJsonObject) {
		//TODO
	}
	
	@Override
	protected ContainerJson procedimentoToJson() throws JSONException {
		JSONArray jsonArray = new JSONArray();
		jsonArray.put(mValor);
        return new ContainerJson("mVetorObj", jsonArray);
	    
	}
    
}
