package app.omegasoftware.pontoeletronico.webservice.protocolo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.database.Table;


public class MensagemVetorEXTDAO extends InterfaceMensagem {
	
	public Table mVetorObj[];
    
	public static String TAG = "MensagemVetorProtocolo";
	

	public MensagemVetorEXTDAO(Table mVetorObj[]) {
		super(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, null);
		this.mVetorObj = mVetorObj;
	}

	
	public MensagemVetorEXTDAO(Context context,
			JSONObject pJsonObject) {
		super(context, pJsonObject);
	}
	
	public MensagemVetorEXTDAO(PROTOCOLO_SISTEMA.TIPO mCodRetorno, String mMensagem) {
		super(mCodRetorno, mMensagem);
	}
	
	@Override
	protected void preInicializa(Context context,
			JSONObject pJsonObject) {
		//TODO
	}
	
	@Override
	protected ContainerJson procedimentoToJson() throws JSONException {
		JSONArray jsonArray = new JSONArray();
    	for(int i = 0 ; i < mVetorObj.length; i++){
    		Table objP = mVetorObj[i];
    		JSONObject json = objP.getJsonObj();
    		jsonArray.put(json);
    	}
        return new ContainerJson("mVetorObj", jsonArray);
	    
	}
    
}
