package app.omegasoftware.pontoeletronico.webservice.protocolo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;


public class MensagemVetorInteger extends InterfaceMensagem {
	
	public Integer mVetorToken[];
    
	public static String TAG = "MensagemVetorInteger";
	

	public MensagemVetorInteger(Integer mVetorObj[]) {
		super(PROTOCOLO_SISTEMA.TIPO.OPERACAO_REALIZADA_COM_SUCESSO, null);
		this.mVetorToken = mVetorObj;
	}

	
	public MensagemVetorInteger(Context context,
			JSONObject pJsonObject) {
		super(context, pJsonObject);
	}
	
	public MensagemVetorInteger(PROTOCOLO_SISTEMA.TIPO mCodRetorno, String mMensagem) {
		super(mCodRetorno, mMensagem);
	}
	
	@Override
	protected void preInicializa(Context context,
			JSONObject pJsonObject) {
		//TODO
	}
	
	@Override
	protected ContainerJson procedimentoToJson() throws JSONException {
		JSONArray jsonArray = new JSONArray();
    	for(int i = 0 ; i < mVetorToken.length; i++){
    		Integer objP = mVetorToken[i];
    		
    		jsonArray.put(objP);
    	}
        return new ContainerJson("mVetorObj", jsonArray);
	    
	}
    
}
