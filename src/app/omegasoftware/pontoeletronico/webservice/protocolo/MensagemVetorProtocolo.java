package app.omegasoftware.pontoeletronico.webservice.protocolo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;


public class MensagemVetorProtocolo extends InterfaceMensagem {
	
	public InterfaceProtocoloEnum[] mVetorObj;
    
	public static String TAG = "MensagemVetorProtocolo";
	
	public MensagemVetorProtocolo(
			Context mContext, 
			InterfaceProtocoloEnum protocoloFactory, 
			JSONObject vObj) {
		
		super(mContext, vObj, protocoloFactory);

		
	}
	
	public InterfaceProtocoloEnum getPrimeiroProtocolo(){
		if(mVetorObj == null || mVetorObj.length == 0){
			return null;
		}
		return mVetorObj[0];
	}
	
	@Override
	protected void preInicializa(Context context,
			JSONObject pJsonObject) {
		try {
			JSONArray vetor = pJsonObject.getJSONArray("mVetorObj");
			if(vetor == null) return;
			mVetorObj = new InterfaceProtocoloEnum[vetor.length()];
			for(int i = 0 ; i < vetor.length(); i++){
				JSONObject json = vetor.getJSONObject(i);
				this.mVetorObj[i] = protocoloFactory.factory(context, json);
			}
		} catch (JSONException e) {
			SingletonLog.insereErro(e, SingletonLog.TIPO.BIBLIOTECA_NUVEM);
		}
	}
	@Override
	protected ContainerJson procedimentoToJson() {
		
		

		return null;
	}
	
    
}
