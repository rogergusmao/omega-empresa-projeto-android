package app.omegasoftware.pontoeletronico.webservice.protocolo;

import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;

public class PROTOCOLO_SISTEMA {
	public enum TIPO{
		ERRO_COM_SERVIDOR(1),
        ERRO_VERSAO_DE_SISTEMA_ANTIGA (2),
        OPERACAO_REALIZADA_COM_SUCESSO (-1),
		EMAIL_DO_USUARIO_INEXISTENTE(4),
		SENHA_DO_USUARIO_INCORRETA(5) ,
		USUARIO_NAO_PERTENCE_A_CORPORACAO(6) ,
		ERRO_SEM_SER_EXCECAO (7),
        USUARIO_INATIVO_NA_CORPORACAO (8),
        SISTEMA_EM_MANUTENCAO (9),
        ACESSO_NEGADO_A_AREA_LOGADA  (10),
        RESULTADO_VAZIO(11),
        ERRO_PARAMETRO_INVALIDO (12),
        ARQUIVO_INEXISTENTE(13),
        ARQUIVO_INVALIDO(14),
        REINSTALAR_VERSAO(15),
        ATUALIZACAO_DE_VERSAO_BLOQUEADA(16),
        SERVIDOR_FORA_DO_AR(17),
        ACESSO_NEGADO(19),
        SIM(20),
        NAO(21),
        OPERACAO_INCOMPLETA(22),
        ERRO_SINCRONIZACAO(24),
        AGUARDANDO(25),
        ERRO_DOWNLOAD(26),
        REGISTRO_REMOVIDO_DEVIDO_ON_DELETE_CASCADE(27),
        PROCESSO_EM_ABERTO(28),
        ERRO_PROCESSO_RESETAR(40),
        SEM_CONEXAO_A_INTERNET(41),
        REGISTRO_DUPLICADO_SEM_REGISTRO_DE_INSERT_NO_HISTORICO_SINC (42),
        
        EM_MANUTENCAO(-57),
        ESTRUTURA_MIGRADA(-58),
        AGUARDANDO_USUARIO_RESETAR(-59),
		ERRO_ARQUIVO_SINCRONIZACAO_INEXISTENTE(-62),
		AUTENTICACAO_INVALIDA(-71),
		NUMERO_USUARIOS_EXCEDEU(-5448),
		CORPORACAO_SELECIONADA_AUTOMATICAMENTE(-5449),
		TROCAR_CORPORACAO(-8888),
		ERRO_UPLOAD(-5571),
		ERRO_BANCO_OMEGASOFTWARE(-5599),
		SINCRONIZACAO_JA_FINALIZADA_PELO_MOBILE(273648),
		
		SOMENTE_VIA_WIFI(23215)
		;
		
		
		final int id;
		TIPO(int id){
			this.id = id;
		}
		
		public int getId(){
			return id;
		}
		
		public boolean isEqual(String strId){
			Integer id = HelperInteger.parserInteger(strId);
			if(id == null ) return false;
			else return id == this.id ? true : false;
		}
		public boolean isEqual(int id){
			return this.id == id ? true : false;
		}
	}
	
	public static TIPO getTipo(int id){
		TIPO[] vetor = TIPO.values();
		for(int i = 0 ; i < vetor.length;i++){
			int codigo = vetor[i].getId();
			if(codigo == id){
				return vetor[i];
			}
		}
		return null;
	}
	
}
