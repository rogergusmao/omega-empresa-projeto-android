package app.omegasoftware.pontoeletronico.webservice.protocolo;

import org.json.JSONObject;

import android.content.Context;

public class ProtocoloStatusBancoSqlite extends InterfaceProtocoloEnum {

	public static String TAG = "ProtocoloStatusBancoSqlite";
	// {"mCodRetorno":-1,"mMensagem":null,"
	// mObj":{"estado":"5","possuiArquivoCrudWebParaMobile":true,"possuiArquivoCrudMobile":false,
	// "idSincronizacaoAtual":"8","idSincronizacaoMobileAtual":"84"}}

	// {idsUltimasSincronizacoes=null, possuiArquivoCrudWebParaMobile=null,
	// estado=null,
	// idSincronizacaoAtual=null, idSincronizacaoMobileAtual=null,
	// possuiArquivoCrudMobile=null}
	public enum ATRIBUTO {
		data, idUltimaSincronizacao, tamanhoBytes, pathArquivoBancoZip, idUltimoRegistroAntesDeGerarBanco, idUltimoRegistroDepoisDeGerarBanco, idDoUltimoCrudProcessadoNoBancoWebAntesDeGerarBanco, idDoUltimoCrudProcessadoNoBancoWebDepoisDeGerarBanco, ultimaMigracao
	}

	public ProtocoloStatusBancoSqlite() {
		super();

	}

	public ProtocoloStatusBancoSqlite(Context pContext, JSONObject pJsonObject) {
		super(pJsonObject, ATRIBUTO.class);

	}

	@Override
	public InterfaceProtocoloEnum factory(Context pContext, JSONObject pJsonObject) {

		return new ProtocoloStatusBancoSqlite(pContext, pJsonObject);
	}

}
