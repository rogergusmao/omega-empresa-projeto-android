package app.omegasoftware.pontoeletronico.webservice.protocolo;

import org.json.JSONObject;

import android.content.Context;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog;
import app.omegasoftware.pontoeletronico.bibliotecanuvem.SingletonLog.TIPO;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho.ESTADO_SINCRONIZACAO;
import app.omegasoftware.pontoeletronico.database.synchronize.SincronizadorEspelho.ESTADO_SINCRONIZACAO_MOBILE;
import app.omegasoftware.pontoeletronico.primitivetype.HelperInteger;


public class ProtocoloStatusSincronizacao extends InterfaceProtocoloEnum {
	
	public static String TAG = "ProtocoloStatusSincronizacao";
	//{"mCodRetorno":-1,"mMensagem":null,"
	//mObj":{"estado":"5","possuiArquivoCrudWebParaMobile":true,"possuiArquivoCrudMobile":false,
	//"idSincronizacaoAtual":"8","idSincronizacaoMobileAtual":"84"}}
	
	//{idsUltimasSincronizacoes=null, possuiArquivoCrudWebParaMobile=null, estado=null, 
	//idSincronizacaoAtual=null, idSincronizacaoMobileAtual=null, possuiArquivoCrudMobile=null}
    public enum ATRIBUTO{
    	estado,
    	estadoSincronizacao,
    	possuiArquivoCrudMobile,
    	possuiArquivoCrudWebParaMobile,
    	idsUltimasSincronizacoes,
    	idSincronizacaoAtual,
    	idSincronizacaoMobileAtual,
		isPrimeiraVez
    }
	public Long[] idsSincronizacao;
	public ESTADO_SINCRONIZACAO_MOBILE estadoSincronizacaoMobile;
	public ESTADO_SINCRONIZACAO estadoSincronizacao;
	public String msg;
	
	public boolean possuiArquivoCrudWebParaMobile(){
		String atributo = getAtributo(ProtocoloStatusSincronizacao.ATRIBUTO.possuiArquivoCrudWebParaMobile);
		if(atributo == null)
			return false;
		else if (atributo.equalsIgnoreCase("true")) return true;
		else return false;
	}
	
	public boolean possuiArquivoCrudMobile(){
		String atributo = getAtributo(ProtocoloStatusSincronizacao.ATRIBUTO.possuiArquivoCrudMobile);
		if(atributo == null)
			return false;
		else if (atributo.equalsIgnoreCase("true")) return true;
		else return false;
	}
	
    public ProtocoloStatusSincronizacao(ESTADO_SINCRONIZACAO_MOBILE estado){
    	super();
    	this.estadoSincronizacaoMobile = estado;
    	
    }
    public ProtocoloStatusSincronizacao(ESTADO_SINCRONIZACAO_MOBILE estado,String msg){
    	super();
    	this.estadoSincronizacaoMobile = estado;
    	this.msg = msg;
    	
    }
    

    
	public ProtocoloStatusSincronizacao(Context pContext,
			JSONObject pJsonObject,
			String msg) throws Exception {
		super(pJsonObject, ATRIBUTO.class);
		String strIdEstado = getAtributo(ProtocoloStatusSincronizacao.ATRIBUTO.estado);
		// Se for a primeira sincronizacao do mobile

		Integer idEstadoSincronizacaoMobile = HelperInteger.parserInteger(strIdEstado);
		if(idEstadoSincronizacaoMobile != null){
			estadoSincronizacaoMobile = SincronizadorEspelho.getEstadoSincronizacaoMobile(idEstadoSincronizacaoMobile);
			if(estadoSincronizacaoMobile == null)
				throw new Exception("Falha ao recuperar o estado da sincronizacao.");
		}
		
		
		String strIdEstadoSincronizacao = getAtributo(ProtocoloStatusSincronizacao.ATRIBUTO.estadoSincronizacao);
		Integer idEstadoSincronizacao = HelperInteger.parserInteger(strIdEstadoSincronizacao);
		if(idEstadoSincronizacao != null ){
			estadoSincronizacao = SincronizadorEspelho.getEstadoSincronizacao(idEstadoSincronizacao);
		}
		this.msg = msg;
	}


	
	@Override
	public InterfaceProtocoloEnum factory(Context pContext,
			JSONObject pJsonObject) {
		try{
			return new ProtocoloStatusSincronizacao(pContext, pJsonObject, msg);
		}catch(Exception ex){
			SingletonLog.insereErro(ex, TIPO.SINCRONIZADOR);
			return null;
		}
		
	}

	
	
}
